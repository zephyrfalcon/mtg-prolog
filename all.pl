% Import the whole MtG database

:- discontiguous
      set/1,
      set_name/2,
      set_release_date/2,
      set_type/2,
      set_block/2,
      set_border/2,
      card/1,
      card_in_set/2,
      card_name/2,
      card_type/2,
      card_types/2,
      card_subtypes/2,
      card_colors/2,
      card_text/2,
      card_original_text/2,
      card_original_type/2,
      card_border/2,
      card_layout/2,
      card_number/2,
      card_mana_cost/2,
      card_cmc/2,
      card_uid/2,
      card_image_name/2,
      card_power/2,
      card_toughness/2,
      card_flavor_text/2,
      card_multiverse_id/2,
      card_artist/2,
      card_timeshifted/1,
      card_reserved/1,
      card_rarity/2,
      card_sides/2,
      card_loyalty/2,
      card_supertypes/2,
      card_watermark/2,
      card_first_print/2,
      card_ruling/3.

:- include('sets/10E.pl').
:- include('sets/2ED.pl').
:- include('sets/3ED.pl').
:- include('sets/4ED.pl').
:- include('sets/5DN.pl').
:- include('sets/5ED.pl').
:- include('sets/6ED.pl').
:- include('sets/7ED.pl').
:- include('sets/8ED.pl').
:- include('sets/9ED.pl').
:- include('sets/ALA.pl').
:- include('sets/ALL.pl').
:- include('sets/APC.pl').
:- include('sets/ARB.pl').
:- include('sets/ARC.pl').
:- include('sets/ARN.pl').
:- include('sets/ATH.pl').
:- include('sets/ATQ.pl').
:- include('sets/AVR.pl').
:- include('sets/BFZ.pl').
:- include('sets/BNG.pl').
:- include('sets/BOK.pl').
:- include('sets/BRB.pl').
:- include('sets/BTD.pl').
:- include('sets/C13.pl').
:- include('sets/C14.pl').
:- include('sets/CED.pl').
:- include('sets/CEI.pl').
:- include('sets/CHK.pl').
:- include('sets/CHR.pl').
:- include('sets/CM1.pl').
:- include('sets/CMD.pl').
:- include('sets/CNS.pl').
:- include('sets/CON.pl').
:- include('sets/CPK.pl').
:- include('sets/CSP.pl').
:- include('sets/CST.pl').
:- include('sets/DD2.pl').
:- include('sets/DD3_DVD.pl').
:- include('sets/DD3_EVG.pl').
:- include('sets/DD3_GVL.pl').
:- include('sets/DD3_JVC.pl').
:- include('sets/DDC.pl').
:- include('sets/DDD.pl').
:- include('sets/DDE.pl').
:- include('sets/DDF.pl').
:- include('sets/DDG.pl').
:- include('sets/DDH.pl').
:- include('sets/DDI.pl').
:- include('sets/DDJ.pl').
:- include('sets/DDK.pl').
:- include('sets/DDL.pl').
:- include('sets/DDM.pl').
:- include('sets/DDN.pl').
:- include('sets/DDO.pl').
:- include('sets/DDP.pl').
:- include('sets/DGM.pl').
:- include('sets/DIS.pl').
:- include('sets/DKA.pl').
:- include('sets/DKM.pl').
:- include('sets/DPA.pl').
:- include('sets/DRB.pl').
:- include('sets/DRK.pl').
:- include('sets/DST.pl').
:- include('sets/DTK.pl').
:- include('sets/EVE.pl').
:- include('sets/EVG.pl').
:- include('sets/EXO.pl').
:- include('sets/EXP.pl').
:- include('sets/FEM.pl').
:- include('sets/FRF.pl').
:- include('sets/FRF_UGIN.pl').
:- include('sets/FUT.pl').
:- include('sets/GPT.pl').
:- include('sets/GTC.pl').
:- include('sets/H09.pl').
:- include('sets/HML.pl').
:- include('sets/HOP.pl').
:- include('sets/ICE.pl').
:- include('sets/INV.pl').
:- include('sets/ISD.pl').
:- include('sets/ITP.pl').
:- include('sets/JOU.pl').
:- include('sets/JUD.pl').
:- include('sets/KTK.pl').
:- include('sets/LEA.pl').
:- include('sets/LEB.pl').
:- include('sets/LEG.pl').
:- include('sets/LGN.pl').
:- include('sets/LRW.pl').
:- include('sets/M10.pl').
:- include('sets/M11.pl').
:- include('sets/M12.pl').
:- include('sets/M13.pl').
:- include('sets/M14.pl').
:- include('sets/M15.pl').
:- include('sets/MBS.pl').
:- include('sets/MD1.pl').
:- include('sets/ME2.pl').
:- include('sets/ME3.pl').
:- include('sets/ME4.pl').
:- include('sets/MED.pl').
:- include('sets/MGB.pl').
:- include('sets/MIR.pl').
:- include('sets/MM2.pl').
:- include('sets/MMA.pl').
:- include('sets/MMQ.pl').
:- include('sets/MOR.pl').
:- include('sets/MRD.pl').
:- include('sets/NMS.pl').
:- include('sets/NPH.pl').
:- include('sets/ODY.pl').
:- include('sets/ONS.pl').
:- include('sets/ORI.pl').
:- include('sets/p15A.pl').
:- include('sets/p2HG.pl').
:- include('sets/pALP.pl').
:- include('sets/pARL.pl').
:- include('sets/PC2.pl').
:- include('sets/pCEL.pl').
:- include('sets/pCMP.pl').
:- include('sets/PCY.pl').
:- include('sets/PD2.pl').
:- include('sets/PD3.pl').
:- include('sets/pDRC.pl').
:- include('sets/pELP.pl').
:- include('sets/pFNM.pl').
:- include('sets/pGPX.pl').
:- include('sets/pGRU.pl').
:- include('sets/pGTW.pl').
:- include('sets/pHHO.pl').
:- include('sets/pJGP.pl').
:- include('sets/PLC.pl').
:- include('sets/pLGM.pl').
:- include('sets/pLPA.pl').
:- include('sets/PLS.pl').
:- include('sets/pMEI.pl').
:- include('sets/pMGD.pl').
:- include('sets/pMPR.pl').
:- include('sets/PO2.pl').
:- include('sets/POR.pl').
:- include('sets/pPOD.pl').
:- include('sets/pPRE.pl').
:- include('sets/pPRO.pl').
:- include('sets/pREL.pl').
:- include('sets/pSUM.pl').
:- include('sets/pSUS.pl').
:- include('sets/PTK.pl').
:- include('sets/pWCQ.pl').
:- include('sets/pWOR.pl').
:- include('sets/pWOS.pl').
:- include('sets/pWPN.pl').
:- include('sets/RAV.pl').
:- include('sets/ROE.pl').
:- include('sets/RQS.pl').
:- include('sets/RTR.pl').
:- include('sets/S00.pl').
:- include('sets/S99.pl').
:- include('sets/SCG.pl').
:- include('sets/SHM.pl').
:- include('sets/SOK.pl').
:- include('sets/SOM.pl').
:- include('sets/STH.pl').
:- include('sets/THS.pl').
:- include('sets/TMP.pl').
:- include('sets/TOR.pl').
:- include('sets/TPR.pl').
:- include('sets/TSB.pl').
:- include('sets/TSP.pl').
:- include('sets/UDS.pl').
:- include('sets/UGL.pl').
:- include('sets/ULG.pl').
:- include('sets/UNH.pl').
:- include('sets/USG.pl').
:- include('sets/V09.pl').
:- include('sets/V10.pl').
:- include('sets/V11.pl').
:- include('sets/V12.pl').
:- include('sets/V13.pl').
:- include('sets/V14.pl').
:- include('sets/V15.pl').
:- include('sets/VAN.pl').
:- include('sets/VIS.pl').
:- include('sets/VMA.pl').
:- include('sets/WTH.pl').
:- include('sets/WWK.pl').
:- include('sets/ZEN.pl').

:- include('cards/cards-0.pl').
:- include('cards/cards-a.pl').
:- include('cards/cards-b.pl').
:- include('cards/cards-c.pl').
:- include('cards/cards-d.pl').
:- include('cards/cards-e.pl').
:- include('cards/cards-f.pl').
:- include('cards/cards-g.pl').
:- include('cards/cards-h.pl').
:- include('cards/cards-i.pl').
:- include('cards/cards-j.pl').
:- include('cards/cards-k.pl').
:- include('cards/cards-l.pl').
:- include('cards/cards-m.pl').
:- include('cards/cards-n.pl').
:- include('cards/cards-o.pl').
:- include('cards/cards-p.pl').
:- include('cards/cards-q.pl').
:- include('cards/cards-r.pl').
:- include('cards/cards-s.pl').
:- include('cards/cards-t.pl').
:- include('cards/cards-u.pl').
:- include('cards/cards-v.pl').
:- include('cards/cards-w.pl').
:- include('cards/cards-x.pl').
:- include('cards/cards-y.pl').
:- include('cards/cards-z.pl').

:- include('rulings/rulings-0.pl').
:- include('rulings/rulings-a.pl').
:- include('rulings/rulings-b.pl').
:- include('rulings/rulings-c.pl').
:- include('rulings/rulings-d.pl').
:- include('rulings/rulings-e.pl').
:- include('rulings/rulings-f.pl').
:- include('rulings/rulings-g.pl').
:- include('rulings/rulings-h.pl').
:- include('rulings/rulings-i.pl').
:- include('rulings/rulings-j.pl').
:- include('rulings/rulings-k.pl').
:- include('rulings/rulings-l.pl').
:- include('rulings/rulings-m.pl').
:- include('rulings/rulings-n.pl').
:- include('rulings/rulings-o.pl').
:- include('rulings/rulings-p.pl').
:- include('rulings/rulings-q.pl').
:- include('rulings/rulings-r.pl').
:- include('rulings/rulings-s.pl').
:- include('rulings/rulings-t.pl').
:- include('rulings/rulings-u.pl').
:- include('rulings/rulings-v.pl').
:- include('rulings/rulings-w.pl').
:- include('rulings/rulings-x.pl').
:- include('rulings/rulings-y.pl').
:- include('rulings/rulings-z.pl').

:- include(rules).
