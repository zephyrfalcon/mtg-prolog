% Rulings

card_ruling('pack rat', '2012-10-01', 'Pack Rat\'s first ability counts any creature you control with the creature type Rat, not just Pack Rats.').
card_ruling('pack rat', '2012-10-01', 'The token will copy Pack Rat\'s two abilities. Its power and toughness will be equal to the number of Rats you control (not the number of Rats you controlled when the token entered the battlefield). It will also be able to create copies of itself.').
card_ruling('pack rat', '2012-10-01', 'The token won\'t copy counters on Pack Rat, nor will it copy other effects that have changed Pack Rat\'s power, toughness, types, color, or so on. Normally, this means the token will simply be a Pack Rat. But if any copy effects have affected that Pack Rat, they\'re taken into account.').
card_ruling('pack rat', '2012-10-01', 'If Pack Rat leaves the battlefield before its activated ability resolves, the token will still enter the battlefield as a copy of Pack Rat, using Pack Rat\'s copiable values from when it was last on the battlefield.').

card_ruling('pack\'s disdain', '2008-04-01', 'Note that this spell counts the number of permanents you control of the chosen type, not just the number of creatures you control. It will also take your tribal permanents into account.').
card_ruling('pack\'s disdain', '2008-04-01', 'You choose a target for this spell as you cast it, but you don\'t choose a creature type until the spell resolves.').

card_ruling('pact of negation', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('pact of the titan', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('pain magnification', '2006-05-01', 'This triggers only if all 3 or more damage is dealt at the same time. It doesn\'t trigger if damage that adds up to 3 or more is dealt at different times by the same source.').
card_ruling('pain magnification', '2006-05-01', 'If a source deals 3 or more damage to multiple opponents at the same time, this ability will trigger once for each of those opponents. They will each have to discard a card.').

card_ruling('pain seer', '2014-02-01', 'If the mana cost of the revealed card includes {X}, X is considered to be 0.').
card_ruling('pain seer', '2014-02-01', 'If the revealed card doesn’t have a mana cost (because it’s a land card, for example), its converted mana cost is 0.').
card_ruling('pain seer', '2014-02-01', 'Split cards in your library have two converted costs, one for each half. If you reveal a split card this way, you’ll lose life equal to the sum of those converted mana costs.').
card_ruling('pain seer', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('pain seer', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('pain seer', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('pain seer', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('pain\'s reward', '2005-06-01', 'Bidding continues until all players except the one with the current high bid have declined to top that bid.').
card_ruling('pain\'s reward', '2005-06-01', 'A player may bid more life than he or she has.').

card_ruling('painful quandary', '2011-01-01', 'Whenever an opponent casts a spell, Painful Quandary\'s ability triggers and goes on the stack on top of it. It will resolve before the spell does.').

card_ruling('painful truths', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('painful truths', '2015-08-25', '').
card_ruling('painful truths', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('painful truths', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('painful truths', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('painter\'s servant', '2008-05-01', 'This ability affects every card in every game zone, all tokens on the battlefield, and all spell copies on the stack, regardless of who controls or owns them.').
card_ruling('painter\'s servant', '2008-05-01', 'This ability doesn\'t overwrite any previous colors. Rather, it adds another color.').
card_ruling('painter\'s servant', '2008-05-01', 'The effects of multiple Painter\'s Servants are cumulative.').
card_ruling('painter\'s servant', '2008-05-01', 'While Painter\'s Servant is on the battlefield, an effect that changes an object\'s colors will overwrite Painter\'s Servant\'s effect. For example, casting Cerulean Wisps on a creature will turn it blue, regardless of the color chosen for Painter\'s Servant.').
card_ruling('painter\'s servant', '2008-05-01', 'Each card becomes a new object as it changes zones, so this effect will apply to it from scratch in the new zone. Zone-change replacement abilities that care about the new color (like \"[color] permanents enter the battlefield tapped\") won\'t work because those effects are applied as the card is entering its new zone. Zone-change triggered abilities that care about the new color (like \"when a [color] permanent enters the battlefield\" or \"when you cast a [color] spell\") will work because those effects apply after the card is already in its new zone.').
card_ruling('painter\'s servant', '2008-05-01', 'If something affected by Painter\'s Servant is normally colorless, it will simply be the new color. It won\'t be both the new color and colorless.').

card_ruling('palace siege', '2014-11-24', 'Each Siege will have one of the two listed abilities, depending on your choice as it enters the battlefield.').
card_ruling('palace siege', '2014-11-24', 'The words “Khans” and “Dragons” are anchor words, connecting your choice to the appropriate ability. Anchor words are a new rules concept. “[Anchor word] — [Ability]” means “As long as you chose [anchor word] as this permanent entered the battlefield, this permanent has [ability].” Notably, the anchor word “Dragons” has no connection to the creature type Dragon.').
card_ruling('palace siege', '2014-11-24', 'Each of the last two abilities is linked to the first ability. They each refer only to the choice made as a result of the first ability. If a permanent enters the battlefield as a copy of one of the Sieges, its controller will make a new choice for that Siege. Which ability the copy has won’t depend on the choice made for the original permanent.').

card_ruling('pale moon', '2004-10-04', 'The ability does not change the amount of mana produced, only the color.').

card_ruling('pale recluse', '2009-05-01', 'Unlike the normal cycling ability, landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a land card of the specified land type, reveal it, put it into your hand, then shuffle your library.').
card_ruling('pale recluse', '2009-05-01', 'Landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being landcycled. Any ability that stops a cycling ability from being activated also stops a landcycling ability from being activated.').
card_ruling('pale recluse', '2009-05-01', 'You can choose to find any card with the appropriate land type, including nonbasic lands. You can also choose not to find a card, even if there is a land card with the appropriate type in your library.').
card_ruling('pale recluse', '2009-05-01', 'A landcycling ability lets you search for any card in your library with the stated land type. It doesn\'t have to be a basic land.').
card_ruling('pale recluse', '2009-05-01', 'You may only activate one landcycling ability at a time. You must specify which landcycling ability you are activating as you cycle this card, not as the ability resolves.').

card_ruling('pale wayfarer', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('pale wayfarer', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('pale wayfarer', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('paleoloth', '2009-02-01', 'If Paleoloth and another creature with power 5 or greater enter the battlefield under your control at the same time, Paleoloth\'s triggered ability will trigger.').

card_ruling('paliano, the high city', '2014-05-29', '').
card_ruling('paliano, the high city', '2014-05-29', 'The three chosen colors must be different.').
card_ruling('paliano, the high city', '2014-05-29', 'If you control Paliano, the High City but you didn’t draft a card named Paliano, the High City, its second ability won’t add mana to your mana pool. Notably, Paliano won’t produce colorless mana.').

card_ruling('palinchron', '2004-10-04', 'You can untap 0 to 7 lands.').
card_ruling('palinchron', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('palinchron', '2004-10-04', 'This does not target the lands.').

card_ruling('palisade giant', '2012-10-01', 'Applying this redirection effect doesn\'t change whether the damage is combat damage.').
card_ruling('palisade giant', '2012-10-01', 'If noncombat damage would be dealt to a planeswalker you control, the planeswalker redirection effect and Palisade Giant\'s redirection effect apply in whichever order you choose. No matter which order you choose to apply them in, that damage will be dealt to Palisade Giant instead.').
card_ruling('palisade giant', '2012-10-01', 'If you control more than one Palisade Giant, you choose which redirection effect to apply. You can\'t divide damage dealt by one source. For example, if an attacking creature would deal 8 damage to you and you control two Palisade Giants, you may have that damage dealt to either of the Palisade Giants. You can\'t have 4 damage dealt to each Giant or choose to have the 8 damage dealt to you.').

card_ruling('pallid mycoderm', '2007-02-01', 'Pallid Mycoderm gives a creature that\'s both a Saproling and a Fungus (such as Mistform Ultimus) +1/+1, not +2/+2.').

card_ruling('pandemonium', '2004-10-04', 'The triggered ability does not check the creature\'s power until the ability resolves. If the creature is not on the battlefield when the ability resolves, it does damage equal to the power of the creature right before it left the battlefield. This means that if a creature enters the battlefield which can destroy itself as a \"enters the battlefield\" ability, the order of resolution of the triggered abilities does not matter.').
card_ruling('pandemonium', '2006-09-25', 'If player A controls Pandemonium when a creature enters the battlefield under player B\'s control, player A controls the triggered ability even though player B chooses the target and chooses whether to have the creature deal damage, which could be important if multiple abilities controlled by multiple players trigger.').

card_ruling('pang tong, \"young phoenix\"', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('panglacial wurm', '2006-07-15', 'Panglacial Wurm\'s ability works only while you\'re searching your own library. The effect that caused you to search needs to say \"search\" and \"library,\" and you need to be looking through your own library for this to work. Examples of effects that let you cast Panglacial Wurm are Rampant Growth and Gifts Ungiven. Examples of effects that don\'t are Bribery and Sage Owl.').
card_ruling('panglacial wurm', '2006-07-15', 'Casting Panglacial Wurm while searching your library follows all the normal rules for casting a creature spell, except for timing (casting the Wurm this way always occurs during the resolution of another spell or ability) and what zone the Wurm is being cast from. The spell goes on the stack. You have to pay the Wurm\'s mana cost and any applicable additional costs, which means you can activate mana abilities while you\'re casting the Wurm while you\'re searching your library.').
card_ruling('panglacial wurm', '2006-07-15', 'After you cast Panglacial Wurm, you pick up the search effect where you left off. When the search effect finishes resolving, the active player gets priority with Panglacial Wurm on the stack. Any abilities that triggered when the spell was cast are put on the stack now.').
card_ruling('panglacial wurm', '2006-07-15', 'If you have enough mana, you can cast multiple Panglacial Wurms during a single search effect.').
card_ruling('panglacial wurm', '2006-07-15', 'A Panglacial Wurm that you cast while searching your library can\'t be found by the search effect.').
card_ruling('panglacial wurm', '2006-07-15', 'If you want to cast Panglacial Wurm while searching your library, you must do so before you find any cards with the search effect.').
card_ruling('panglacial wurm', '2006-07-15', 'While searching your library, you must keep your library in the same order until you shuffle it. This order could matter if you tap Millikin for mana, for example, to pay for a Panglacial Wurm you cast from your library.').

card_ruling('panic', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s before the beginning of that phase\'s Declare Blockers Step.').

card_ruling('panoptic mirror', '2004-12-01', 'As Panoptic Mirror\'s triggered ability resolves, it allows you to create a copy of one of the instant or sorcery cards imprinted on Panoptic Mirror in the Exile zone (that\'s where the imprinted card is) and then cast it without paying its mana cost.').
card_ruling('panoptic mirror', '2004-12-01', 'You don\'t pay the spell\'s mana cost. If the spell has X in its mana cost, X is 0. You do pay any additional costs for that spell. You can\'t use any alternative costs.').
card_ruling('panoptic mirror', '2004-12-01', 'The triggered ability triggers only once each upkeep, not once per imprinted card. If no cards are imprinted on Panoptic Mirror when the triggered ability resolves, it does nothing.').
card_ruling('panoptic mirror', '2004-12-01', 'You may imprint a card on Panoptic Mirror in response to the upkeep-triggered ability. If you do, that card is available to copy when the triggered ability resolves.').
card_ruling('panoptic mirror', '2004-12-01', 'Exiling a card when the activated ability resolves is optional. If you choose to exile a card, you can exile only a card with converted mana cost equal to X.').
card_ruling('panoptic mirror', '2004-12-01', 'To imprint a split card, you pay X equal to either side\'s converted mana cost. If the copied card is a split card, you choose which one side of it to cast, but you can\'t cast both sides. You can choose the other side later in the game, though.').
card_ruling('panoptic mirror', '2005-03-01', 'The creation of the copy and then the casting of the copy are both optional.').

card_ruling('panopticon', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('panopticon', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('panopticon', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('panopticon', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('paradigm shift', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('paradox haze', '2006-09-25', 'Paradox Haze targets a player when it\'s cast, and it enters the battlefield attached to that player.').
card_ruling('paradox haze', '2006-09-25', 'If two Paradox Hazes enchant the same player, they\'ll both trigger when that player\'s first upkeep of the turn begins. The player will get two additional upkeep steps.').
card_ruling('paradox haze', '2006-09-25', 'Abilities that trigger \"at the beginning of [your] upkeep\" will trigger at the beginning of each additional upkeep as well.').
card_ruling('paradox haze', '2006-09-25', 'If echo triggers during the first upkeep, it won\'t trigger again during the second upkeep since the creature won\'t have enter the battlefield since that player\'s last upkeep.').
card_ruling('paradox haze', '2006-09-25', 'If an effect causes the enchanted player to skip his or her upkeep step, this ability won\'t trigger.').

card_ruling('paragon of gathering mists', '2014-07-18', 'Causing a creature to gain flying after it’s been blocked won’t change or undo that block.').

card_ruling('paragon of the amesha', '2009-02-01', 'When Paragon of the Amesha\'s activated ability resolves, Paragon of the Amesha will gain flying and lifelink in addition to its other abilities. However, becoming an Angel will make it lose all other creature types. It will no longer be a Human or a Knight.').
card_ruling('paragon of the amesha', '2009-02-01', 'If Paragon of the Amesha gains flying after blockers have been declared, it won\'t cause those blocks to change.').
card_ruling('paragon of the amesha', '2009-10-01', 'You can activate Paragon of the Amesha\'s ability more than once during a turn. The second time it resolves, it will get another +3/+3 and gain another instance of lifelink. However, multiple instances of lifelink are redundant so you won\'t gain extra life from the new one.').

card_ruling('parallax nexus', '2004-10-04', 'The target opponent chooses which card to exile.').

card_ruling('parallel lives', '2011-09-22', 'All of the tokens enter the battlefield simultaneously. They\'ll have the same name, color, type and subtype, abilities, power, toughness, and so on.').
card_ruling('parallel lives', '2011-09-22', 'If the effect is putting more than one kind of token onto the battlefield, it\'ll put twice as many of each kind onto the battlefield. For example, if you cast Bestial Menace while controlling Parallel Lives, you\'ll put two Snake tokens, two Wolf tokens, and two Elephant tokens onto the battlefield.').
card_ruling('parallel lives', '2011-09-22', 'If you control two Parallel Lives, then the number of tokens created is four times the original number. If you control three, then the number of tokens created is eight times the original number, and so on.').
card_ruling('parallel lives', '2011-09-22', 'If the effect creating the tokens instructs you to do something with those tokens at a later time, like exiling them at the end of combat, you\'ll do that for all the tokens.').

card_ruling('parallel thoughts', '2004-10-04', 'If you have fewer than 7 cards in your library, exile them all.').
card_ruling('parallel thoughts', '2004-10-04', 'You can choose to take the top card from the pile (and not draw) even if there are no cards in the pile. This allows you to skip draws.').
card_ruling('parallel thoughts', '2004-10-04', 'You can\'t look at the face down cards.').

card_ruling('paralyze', '2004-10-04', 'Paying the cost is optional.').
card_ruling('paralyze', '2004-10-04', 'Paralyze can be cast targeting a creature which is already tapped.').
card_ruling('paralyze', '2004-10-04', 'The creature can only be untapped using Paralyze once per turn.').
card_ruling('paralyze', '2004-10-04', 'Two Paralyzes are not cumulative. Paying either one will untap the creature.').
card_ruling('paralyze', '2007-09-16', 'This is a change from the most recent wording. As was the case in the past, Paralyze itself has the last triggered ability, rather than granting that ability to the enchanted creature.').

card_ruling('paralyzing grasp', '2009-10-01', 'Paralyzing Grasp may target, and may enchant, an untapped creature. If it does, it will have no effect unless that creature becomes tapped somehow.').
card_ruling('paralyzing grasp', '2012-10-01', 'The enchanted creature can still be untapped by a spell or ability at other times.').

card_ruling('paranoid delusions', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('paranoid delusions', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('paranoid delusions', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('paranoid delusions', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('paranoid delusions', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('paranoid delusions', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('paranoid delusions', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('paranoid delusions', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('paranoid delusions', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('paranoid delusions', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('paranoid delusions', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('parapet', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('parasitic implant', '2011-06-01', 'The controller of Parasitic Implant will control the Myr token no matter who controlled and sacrificed the enchanted creature.').

card_ruling('parasitic strix', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').

card_ruling('parch', '2004-10-04', 'You choose the mode when you announce the spell.').
card_ruling('parch', '2004-10-04', 'You can choose to use the 2 damage mode on a blue creature if you want.').

card_ruling('pardic dragon', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('pardic dragon', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('pardic dragon', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('pardic dragon', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('pardic dragon', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('pardic dragon', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('pardic dragon', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('pardic dragon', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('pardic dragon', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('pardic dragon', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('pardic miner', '2004-10-04', 'The ability won\'t stop lands from being put onto the battlefield by a spell or ability.').
card_ruling('pardic miner', '2004-10-04', 'The ability won\'t stop players from activating the abilities of lands. It only stops land cards from being played.').

card_ruling('pariah', '2004-10-04', 'You can attach this to an opponent\'s creature, and all damage done to you is instead done to their creature.').
card_ruling('pariah', '2004-10-04', 'All damage being dealt to you at one time gets redirected to one Pariah. If you take 3 damage at once, all 3 damage goes to one Pariah. If you take damage from multiple creatures in combat, all the combat damage goes to one Pariah.').

card_ruling('pariah\'s shield', '2005-10-01', 'If Pariah\'s Shield isn\'t attached to a creature, all damage that would be dealt to you is dealt to you normally.').

card_ruling('paroxysm', '2004-10-04', 'The ability is not optional.').

card_ruling('part the waterveil', '2015-08-25', 'Exiling Part the Waterveil is part of its effect. If Part the Waterveil is countered, including if it was cast for its awaken cost and the land you control became an illegal target, it will be put into its owner’s graveyard.').
card_ruling('part the waterveil', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('part the waterveil', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('part the waterveil', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('part the waterveil', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('part the waterveil', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('part the waterveil', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('past in flames', '2011-09-22', 'Only instant and sorcery cards in your graveyard when Past in Flames resolves will gain flashback. Instant and sorcery cards that are put into your graveyard later in the turn, including the resolving Past in Flames, won\'t gain flashback.').
card_ruling('past in flames', '2011-09-22', 'If you cast an instant or sorcery with {X} in its mana cost this way, you still choose the value of X as part of casting the spell.').
card_ruling('past in flames', '2011-09-22', 'If an instant or sorcery card in your graveyard already has flashback, you may use either flashback ability to cast it from your graveyard.').
card_ruling('past in flames', '2011-09-22', 'You may pay any optional additional costs the spell has, such as kicker costs. You must pay any mandatory additional costs the spell has.').
card_ruling('past in flames', '2013-07-01', 'For split cards, you pay only the cost for the half of the card you are casting. This is true because the cost is not looked at until after the card is on the stack, at which time it only has one of the two costs.').

card_ruling('patagia viper', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('path of bravery', '2013-07-01', 'Your starting life total is the life total you began the game with, keeping in mind the format you’re playing and, in some rare cases, anything that affects your starting life total (for example, a vanguard with a life modifier).').
card_ruling('path of bravery', '2013-07-01', 'In most games, your starting life total is 20. In a Commander game, your starting life total is 40 instead. In a Two-Headed Giant game, Path of Bravery compares your team’s life total to your team’s starting life total, which is 30.').
card_ruling('path of bravery', '2013-07-01', 'Path of Bravery’s triggered ability triggers only once, after attacking creatures are declared. For example, if you attack with five creatures, you’ll gain 5 life as a single life-gain event.').
card_ruling('path of bravery', '2013-07-01', 'If a creature enters the battlefield attacking after attacking creatures have been declared, the ability won’t trigger again because you didn’t declare that creature as an attacking creature. However, if enters the battlefield before the ability resolves, it will be counted when determining how much life is gained.').

card_ruling('path of peace', '2004-10-04', 'If the spell fails because the target is not there or isn\'t legal, the owner does not gain any life.').

card_ruling('path to exile', '2009-02-01', 'If the targeted creature becomes an illegal target by the time Path to Exile would resolve, the entire spell is countered. The creature\'s controller won\'t get to search for a basic land card.').

card_ruling('pathrazer of ulamog', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('pathrazer of ulamog', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('pathrazer of ulamog', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').

card_ruling('patrol signaler', '2008-08-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('patrol signaler', '2008-08-01', 'The \"summoning sickness\" rule applies to {Q}. If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability. Ignore this rule if the creature also has haste.').
card_ruling('patrol signaler', '2008-08-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('patron of the kitsune', '2005-02-01', 'You may gain one life per attacking creature when the ability resolves during each declare attackers step.').
card_ruling('patron of the kitsune', '2005-02-01', 'You may gain life when any creatures are declared as attackers, not just for attacking creatures you control.').

card_ruling('patron of the moon', '2005-02-01', 'You may put zero, one, or two lands onto the battlefield with Patron of the Moon\'s ability. The number is determined as the ability resolves. You don\'t need lands in hand to activate the ability.').

card_ruling('patron of the nezumi', '2005-02-01', 'Patron of the Nezumi\'s ability triggers even if an event puts this and multiple other permanents into the graveyard at the same time (Wrath of God, gang blocks, etc.)').

card_ruling('patron of the orochi', '2005-02-01', 'Patron of the Orochi\'s ability untaps nonbasic lands that are also Forests (such as some of the Revised dual lands) and multicolored creatures that are also green.').
card_ruling('patron of the orochi', '2005-02-01', 'The Patron\'s ability can be activated only once per turn, even if control of Patron of the Orochi changes.').

card_ruling('patron of the wild', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('patron wizard', '2004-10-04', 'Since the cost does not include the {Tap} symbol, it can tap itself even on the turn it enters the battlefield.').

card_ruling('pattern of rebirth', '2004-10-04', 'Searching for a creature card is optional. You can\'t go back and do it if you forget to do so, even if you normally do this.').
card_ruling('pattern of rebirth', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('pawn of ulamog', '2010-06-15', 'If multiple nontoken creatures you control (possibly including Pawn of Ulamog itself) are put into their owners\' graveyards at the same time, Pawn of Ulamog\'s triggered ability will trigger that many times.').
card_ruling('pawn of ulamog', '2010-06-15', 'If you control more than one Pawn of Ulamog and a nontoken creature you control is put into a graveyard, each of those Pawns\' abilities will trigger. You may put up to that many Eldrazi Spawn tokens onto the battlefield.').

card_ruling('pay no heed', '2013-07-01', 'Pay No Heed doesn’t target anything. You can choose a source with hexproof, for example. You choose the damage source as Pay No Heed resolves.').
card_ruling('pay no heed', '2013-07-01', 'If the chosen source is a permanent spell, damage dealt later in the turn by the permanent it becomes will also be prevented.').

card_ruling('peace and quiet', '2004-10-04', 'Must target two different enchantments.').

card_ruling('peace talks', '2004-10-04', 'Can be cast after your attack.').
card_ruling('peace talks', '2004-10-04', 'It affects the current turn and the next turn. This could affect the same player twice if they have two turns in a row, or it could affect different players. In a multiplayer game it affects exactly two turns, not a full round of turns.').
card_ruling('peace talks', '2006-02-01', 'Doesn\'t skip the Combat Phase, just renders creatures unable to attack.').
card_ruling('peace talks', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('peacekeeper', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('peacekeeper', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').
card_ruling('peacekeeper', '2008-04-01', 'Peacekeeper\'s ability doesn\'t cause the combat phase or the declare attackers step to be skipped.').

card_ruling('peak eruption', '2013-09-15', 'Peak Eruption can target any land with the subtype Mountain, not just those named Mountain. It can’t target a land that doesn’t have that subtype, even if the land can tap for {R}.').
card_ruling('peak eruption', '2013-09-15', 'If the target Mountain is an illegal target when Peak Eruption tries to resolve, it will be countered and none of its effects will happen. No damage will be dealt. However, if Peak Eruption resolves and the Mountain isn’t destroyed (perhaps because it has indestructible), damage will be dealt.').

card_ruling('pearl lake ancient', '2014-09-20', 'A spell or ability that counters spells can still target Pearl Lake Ancient. When one resolves, Pearl Lake Ancient won’t be countered, but any additional effects of that spell or ability will still happen.').
card_ruling('pearl lake ancient', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('pearl lake ancient', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('pearl lake ancient', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('pearl medallion', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('pearl medallion', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('pearl medallion', '2004-10-04', 'The effect is cumulative.').
card_ruling('pearl medallion', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('pearl medallion', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('pearl medallion', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('pearl shard', '2004-10-04', 'You can pay either of the two costs (but not both at the same time) to activate the ability.').

card_ruling('pearlspear courier', '2004-10-04', 'It checks the creature type when the ability is announced and resolved, but once the effect is placed on the creature, if its creature type changes the effect still continues.').

card_ruling('peer through depths', '2004-12-01', 'If you don\'t reveal an instant or sorcery card, put all the revealed cards on the bottom of your library in any order.').

card_ruling('pendelhaven', '2004-10-04', 'The current power/toughness being 1/1 is a targeting requirement so it is checked when announcing and again right before resolving.').

card_ruling('pendrell drake', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('pendrell flux', '2004-10-04', 'You have to even match the color requirements of the mana cost.').
card_ruling('pendrell flux', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice the creature. You can choose to not pay if you no longer control the creature on resolution.').

card_ruling('pendrell mists', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('pendrell mists', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('pennon blade', '2010-06-15', 'As long as you control Pennon Blade, its ability constantly counts the number of creatures you control. It doesn\'t matter who controls the creature it\'s equipping (in case an opponent somehow manages to take control of that creature). If you control the creature it\'s equipping, the bonus will include that creature too.').

card_ruling('pentavus', '2011-09-22', 'You can remove any +1/+1 counter on Pentavus to activate its first activated ability, not just ones created by Pentavus\'s other abilities.').
card_ruling('pentavus', '2011-09-22', 'You can sacrifice any Pentavite creature you control (such as an Adaptive Automaton with Pentavite as its chosen type) to activate Pentavus\'s second activated ability, not just ones created by Pentavus\'s other abilities.').

card_ruling('peppersmoke', '2013-06-07', 'Peppersmoke checks if you control a Faerie only when it resolves.').

card_ruling('peregrination', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('peregrination', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('peregrination', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('peregrination', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('peregrination', '2014-02-01', 'You can choose to search for only one basic land card with Peregrination. If you do, you’ll put that card onto the battlefield tapped.').

card_ruling('peregrine drake', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('peregrine drake', '2004-10-04', 'This does not target the lands.').

card_ruling('perilous forays', '2005-10-01', 'You may find any card with Plains, Island, Swamp, Mountain, or Forest on its type line. The land doesn\'t need to be a basic land, so you can find a land such as Overgrown Tomb or Tropical Island.').

card_ruling('perish the thought', '2010-06-15', 'If the targeted player\'s hand is empty, you can\'t choose a card for that player to shuffle into his or her library. No shuffle occurs.').

card_ruling('permafrost trap', '2010-03-01', 'You may target any two (or fewer) creatures. It\'s okay if any of them are already tapped. If you cast Permafrost Trap for {U}, you don\'t have to target the green creature that entered the battlefield under an opponent\'s control.').
card_ruling('permafrost trap', '2010-03-01', 'If a targeted creature is untapped at the time its controller\'s next untap step begins, the second part of the ability has no effect on it.').
card_ruling('permafrost trap', '2010-03-01', 'If an affected creature changes controllers before its previous controller\'s next untap step, this ability will prevent it from untapping during its new controller\'s next untap step.').

card_ruling('perplex', '2005-10-01', 'If the spell\'s controller has no cards in his or her hand, that player can still choose to discard his or her hand and prevent the spell from being countered.').

card_ruling('perplexing chimera', '2014-02-01', 'You may exchange control of Perplexing Chimera and any spell cast by an opponent, not just one with targets.').
card_ruling('perplexing chimera', '2014-02-01', 'You make the decision whether to exchange control of Perplexing Chimera and the spell as the triggered ability resolves.').
card_ruling('perplexing chimera', '2014-02-01', 'If Perplexing Chimera leaves the battlefield or the spell leaves the stack before the triggered ability resolves, you can’t make the exchange.').
card_ruling('perplexing chimera', '2014-02-01', 'Neither Perplexing Chimera nor the spell changes zones. Only control of them is exchanged.').
card_ruling('perplexing chimera', '2014-02-01', 'After the ability resolves, you control the spell. Any instance of “you” in that spell’s text now refers to you, “an opponent” refers to one of your opponents, and so on. The change of control happens before new targets are chosen, so any targeting restrictions such as “target opponent” or “target creature you control” are now made in reference to you, not the spell’s original controller. You may change those targets to be legal in reference to you, or, if those are the spell’s only targets, the spell will be countered on resolution for having illegal targets. When the spell resolves, any illegal targets are unaffected by it and you make all decisions the spell’s effect calls for.').
card_ruling('perplexing chimera', '2014-02-01', 'You may change any of the spell’s targets. If you change a target, you must choose a legal target for the spell. If you can’t, you must leave the target the same (even if that target is now illegal).').
card_ruling('perplexing chimera', '2014-02-01', 'Gaining control of a spell and changing its targets won’t cause any heroic abilities of the new targets to trigger.').
card_ruling('perplexing chimera', '2014-02-01', 'If you gain control of an instant or sorcery spell, it will be put into its owner’s graveyard as it resolves or is countered.').
card_ruling('perplexing chimera', '2014-02-01', 'In some unusual cases, you may not control Perplexing Chimera when its triggered ability resolves (perhaps because the triggered ability triggered again and resolved while the original ability was on the stack). In these cases, you can exchange control of Perplexing Chimera and the spell that causes the ability to trigger, even if you control neither of them. If you do, you’ll be able to change targets of the spell, not the spell’s new controller.').

card_ruling('persecute', '2004-10-04', 'The color is chosen on resolution.').

card_ruling('personal incarnation', '2004-10-04', 'You do not lose life if this card is exiled or sent someplace without going to the graveyard first.').
card_ruling('personal incarnation', '2004-10-04', 'The owner of the Incarnation loses life when it is destroyed, not the controller. So if you control your opponent\'s Incarnation you can let it die to make them lose life.').
card_ruling('personal incarnation', '2007-02-01', 'If you attempt to halve a negative life total, you halve 0. This means that the life total stays the same. A life total of -10 would remain -10.').

card_ruling('personal tutor', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('pestilence', '2004-10-04', 'Each activation is considered a new damage effect. An activation can only be 1 point of damage.').
card_ruling('pestilence', '2011-06-01', 'Note that \"until end of turn\" effects wear off after \"at the beginning of the end step\" triggered abilities, so an artifact that animates until end of turn can keep this on the battlefield.').
card_ruling('pestilence', '2011-06-01', 'It will stay on the battlefield if there is a creature that is put into the graveyard during the end step. This is because this ability will not trigger at all if there is at least one creature on the battlefield as the end step begins.').

card_ruling('pestilence demon', '2010-06-15', 'Pestilence Demon deals 1 damage to itself, as well as to each other creature and each player.').

card_ruling('pestilence rats', '2004-10-04', 'Does not count itself for determining power. It only counts other Rats.').

card_ruling('pestilent souleater', '2011-06-01', 'Multiple instances of infect are redundant.').

card_ruling('petals of insight', '2013-06-07', 'If you choose to put the three cards on the bottom of your library, Petals of Insight will return to your hand directly from the stack. It never goes to a graveyard.').

card_ruling('petra sphinx', '2004-10-04', 'If the targeted player\'s library is empty, that player still names a card, but nothing further happens. That player will not lose the game.').

card_ruling('petrahydrox', '2006-02-01', 'Petrahydrox will return to its owner\'s hand before the spell or ability targeting it can resolve.').

card_ruling('petrified plating', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('petrified plating', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('petrified plating', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('petrified plating', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('petrified plating', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('petrified plating', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('petrified plating', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('petrified plating', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('petrified plating', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('petrified plating', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('petrified wood-kin', '2006-02-01', '\"Protection from instants\" means instant spells can\'t target it, damage that would be dealt to it by instant sources is prevented, and it can\'t be the target of abilities of instants (such as Haunt).').
card_ruling('petrified wood-kin', '2006-02-01', 'Protection from instants works only while Petrified Wood-Kin is on the battlefield. It can be targeted by instant spells or abilities of instant cards while it\'s on the stack or in any other zone but on the battlefield.').

card_ruling('phage the untouchable', '2004-10-04', 'It is only considered \"cast from your hand\" if you cast it as a spell from your hand. Putting it onto the battlefield from your hand using a spell or ability will cause you to lose the game.').
card_ruling('phage the untouchable', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('phalanx formation', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('phalanx formation', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('phalanx formation', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('phalanx formation', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('phalanx formation', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('phalanx leader', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('phalanx leader', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('phalanx leader', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('phantasmagorian', '2007-02-01', 'When this spell is cast, its \"when you cast\" ability triggers and goes on the stack on top of it.').
card_ruling('phantasmagorian', '2007-02-01', 'As the triggered ability resolves, the active player gets the option to perform the action. If that player declines, the next player in turn order gets the option. As soon as any player performs the action, the spell is countered, but the remaining players still get the option. If all players decline, the spell remains on the stack.').
card_ruling('phantasmagorian', '2013-07-01', 'A player can\'t choose to discard unless he or she actually has three cards in his or her hand.').

card_ruling('phantasmal abomination', '2010-06-15', 'If Phantasmal Abomination becomes the target of a spell or ability, Phantasmal Abomination\'s ability triggers and goes on the stack on top of that spell or ability. Phantasmal Abomination\'s ability will resolve (causing it to be sacrificed) first. Unless the spell or ability has another target, it will then be countered when it tries to resolve for having no legal targets.').

card_ruling('phantasmal fiend', '2005-11-01', 'Note that the wording \"Effects that alter the creature\'s power alter its toughness instead, and vice versa, this turn\" has been removed.').
card_ruling('phantasmal fiend', '2008-10-01', 'Effects that switch power and toughness are always applied last when determining a creature\'s power and toughness, even if other power/toughness-changing effects are created later in the turn. For example, if you activate Phantasmal Fiend\'s first ability, it will become 2/4. Then if you activate its second ability, it will become 4/2. Then if you activate its first ability again, it will become 3/3.').
card_ruling('phantasmal fiend', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('phantasmal fiend', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('phantasmal image', '2011-09-22', 'Phantasmal Image\'s ability doesn\'t target the chosen creature.').
card_ruling('phantasmal image', '2011-09-22', 'Phantasmal Image copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('phantasmal image', '2011-09-22', 'If the chosen creature has {X} in its mana cost (such as Primordial Hydra), X is considered to be zero.').
card_ruling('phantasmal image', '2011-09-22', 'If the chosen creature is copying something else (for example, if the chosen creature is another Phantasmal Image), then Phantasmal Image enters the battlefield as whatever the chosen creature copied.').
card_ruling('phantasmal image', '2011-09-22', 'If the chosen creature is a token, Phantasmal Image copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Phantasmal Image is not a token.').
card_ruling('phantasmal image', '2011-09-22', 'Any enters-the-battlefield abilities of the copied creature will trigger when Phantasmal Image enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').
card_ruling('phantasmal image', '2011-09-22', 'If Phantasmal Image somehow enters the battlefield at the same time as another creature, Phantasmal Image can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('phantasmal image', '2011-09-22', 'You can choose not to copy anything. In that case, Phantasmal Image enters the battlefield as a 0/0 Illusion creature, and is probably put into the graveyard immediately. It doesn\'t have the ability \"When this creature becomes the target of a spell or ability, sacrifice it.\"').

card_ruling('phantasmal mount', '2008-10-01', 'If the ability resolves, then the targeted creature\'s toughness becomes greater than 2 later, Phantasmal Mount\'s effect will not end. (The ability checks whether the creature has toughness 2 or less only at the time you activate it and the time it resolves.)').

card_ruling('phantasmal terrain', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').

card_ruling('phantom beast', '2010-08-15', 'If Phantom Beast becomes the target of a spell or ability, Phantom Beast\'s ability triggers and goes on the stack on top of that spell or ability. Phantom Beast\'s ability will resolve (causing it to be sacrificed) first. Unless the spell or ability has another target, it will then be countered when it tries to resolve for having no legal targets.').

card_ruling('phantom centaur', '2004-10-04', 'If this card is going to take damage from a black source, you can choose to apply Protection from Black\'s damage prevention ability prior to applying this card\'s built-in ability. This means you don\'t have to remove a counter.').
card_ruling('phantom centaur', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom centaur', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom centaur', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom centaur', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('phantom flock', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom flock', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom flock', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom flock', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('phantom general', '2012-10-01', 'A creature token that\'s a copy of a nontoken creature is still a token. It will get +1/+1, but the original nontoken creature will not.').

card_ruling('phantom nantuko', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom nantuko', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom nantuko', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom nantuko', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('phantom nishoba', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom nishoba', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom nishoba', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom nishoba', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('phantom nomad', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom nomad', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom nomad', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom nomad', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('phantom tiger', '2004-10-04', 'If this card takes damage from multiple sources at once (for example if it is blocked by multiple creatures in combat), it only loses one counter.').
card_ruling('phantom tiger', '2004-10-04', 'If unpreventable damage is applied to this card, you still remove a counter even though the prevention fails.').
card_ruling('phantom tiger', '2004-10-04', 'Remove one counter each time it would be damaged. If it was going to take more than one point of damage, you prevent all the damage and still only remove one counter.').
card_ruling('phantom tiger', '2004-10-04', 'The damage prevention ability works even if it has no counters, as long as some effect keeps its toughness above zero.').

card_ruling('pharagax giant', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('pharagax giant', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('pharagax giant', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('pharagax giant', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('pharagax giant', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('pharika\'s cure', '2013-09-15', 'If the creature is an illegal target as Pharika’s Cure tries to resolve, it will be countered and none of its effects will happen. You won’t gain life. If, on the other hand, Pharika’s Cure does resolve but the damage is prevented, you’ll still gain life.').

card_ruling('pharika\'s disciple', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('pharika\'s disciple', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('pharika\'s disciple', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('pharika, god of affliction', '2014-04-26', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('pharika, god of affliction', '2014-04-26', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('pharika, god of affliction', '2014-04-26', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('pharika, god of affliction', '2014-04-26', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('pharika, god of affliction', '2014-04-26', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('pharika, god of affliction', '2014-04-26', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('pharika, god of affliction', '2014-04-26', 'Your devotion to two colors is equal to the number of mana symbols that are the first color, the second color, or both colors among the mana costs of permanents you control. Specifically, a hybrid mana symbol counts only once toward your devotion to its two colors. For example, if the only nonland permanents you control are Pharika, God of Affliction and Golgari Guildmage (whose mana cost is {B/G}{B/G}), your devotion to black and green is four.').
card_ruling('pharika, god of affliction', '2014-04-26', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('pharika, god of affliction', '2014-04-26', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('pharika, god of affliction', '2014-04-26', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('phelddagrif', '2005-11-01', 'All three abilities are now targeted.').

card_ruling('phenax, god of deception', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('phenax, god of deception', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('phenax, god of deception', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('phenax, god of deception', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('phenax, god of deception', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('phenax, god of deception', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('phenax, god of deception', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('phenax, god of deception', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('phenax, god of deception', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('phenax, god of deception', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('phenax, god of deception', '2014-02-01', 'If you activate the ability Phenax grants to creatures you control, the toughness of the creature is calculated as the ability resolves. If the creature is no longer on the battlefield at that time, use its toughness when it was last on the battlefield.').
card_ruling('phenax, god of deception', '2014-02-01', 'If Phenax is a creature, it will grant itself the activated ability.').
card_ruling('phenax, god of deception', '2014-02-01', 'If you tap Phenax to activate the ability it grants itself, and Phenax is no longer a creature but still on the battlefield as that ability resolves, no cards will be put into the target player’s graveyard. Similarly, if Phenax isn’t on the battlefield as the ability resolves and wasn’t a creature when it left the battlefield, no cards will be put into the graveyard.').

card_ruling('pheres-band raiders', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('pheres-band raiders', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('pheres-band raiders', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('pheres-band raiders', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('pheres-band thunderhoof', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('pheres-band thunderhoof', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('pheres-band thunderhoof', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('pheres-band tromper', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('pheres-band tromper', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('pheres-band tromper', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('pheres-band tromper', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('phobian phantasm', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('phosphorescent feast', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('phosphorescent feast', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').

card_ruling('phthisis', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('phthisis', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('phthisis', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('phthisis', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('phthisis', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('phthisis', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('phthisis', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('phthisis', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('phthisis', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('phthisis', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('phthisis', '2013-06-07', 'If the creature\'s power is less than 0, use its actual value to determine the total life loss. However, if this total is 0 or less, no life is lost. For example, if Phthisis destroys a -2/3 creature, its controller would lose 1 life. If it destroys a -4/3 creature, its controller would lose no life.').

card_ruling('phylactery lich', '2010-08-15', 'Phylactery Lich\'s first ability doesn\'t target the artifact. You can choose an artifact that has shroud, for example.').
card_ruling('phylactery lich', '2010-08-15', 'If you control no artifacts as Phylactery Lich enters the battlefield, its first ability won\'t do anything. As soon as it enters the battlefield, its last ability will trigger (unless you control some other permanent with a phylactery counter on it) and you\'ll have to sacrifice it.').
card_ruling('phylactery lich', '2010-08-15', 'If Phylactery Lich and an artifact are entering the battlefield under your control at the same time, you can\'t put a phylactery counter on that artifact. You must choose an artifact you control that\'s already on the battlefield.').
card_ruling('phylactery lich', '2010-08-15', 'Phylactery Lich\'s last ability is a \"state trigger.\" Once a state trigger triggers, it won\'t trigger again as long as the ability is on the stack. If the ability is countered and the trigger condition is still true, it will immediately trigger again.').
card_ruling('phylactery lich', '2010-08-15', 'Phylactery Lich\'s last ability checks whether you control any permanents with phylactery counters on them, not whether you control any artifacts with phylactery counters on them. If an artifact with a phylactery counter on it somehow ceases to be an artifact, Phylactery Lich doesn\'t care.').
card_ruling('phylactery lich', '2010-08-15', 'Phylactery Lich\'s last ability checks your permanents for any phylactery counters, not just the specific one that it caused you to put on an artifact. For example, say you put a phylactery counter on a Demon\'s Horn as Phylactery Lich enters the battlefield. Then you put a phylactery counter on a Crystal Ball as another Phylactery Lich enters the battlefield. Then Crystal Ball is destroyed. Since you still control the Demon\'s Horn, the last ability of neither Phylactery Lich triggers.').
card_ruling('phylactery lich', '2010-08-15', 'The game continually checks whether you control a permanent with a phylactery counter on it. The moment you don\'t, Phylactery Lich\'s last ability triggers. The ability doesn\'t check again, so you\'ll have to sacrifice Phylactery Lich when it resolves even if you wind up controlling a permanent with a phylactery counter on it by then.').
card_ruling('phylactery lich', '2013-07-01', 'Lethal damage, damage from a source with deathtouch, and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons Phylactery Lich would be put into a graveyard are if it\'s sacrificed (perhaps due to its last ability), or if its toughness is 0 or less.').

card_ruling('phyresis', '2011-06-01', 'Multiple instances of infect on the same creature are redundant.').

card_ruling('phyrexia\'s core', '2011-06-01', 'Once you activate the ability and sacrifice an artifact, it\'s too late for anyone to respond to try and prevent you from paying the cost.').

card_ruling('phyrexian crusader', '2011-06-01', 'If Phyrexian Crusader blocks or is blocked by a creature without first strike or double strike, the damage dealt by Phyrexian Crusader results in -1/-1 counters that will affect how much damage the other creature assigns (if it\'s still on the battlefield).').

card_ruling('phyrexian devourer', '2011-09-22', 'Phyrexian Devourer\'s first ability continually checks its power as a state trigger. Once it triggers, it won\'t trigger again as long as the ability is on the stack. If the ability is countered and the trigger condition is still true, it will immediately trigger again. If the ability resolves, you\'ll have to sacrifice Phyrexian Devourer even if its power has been reduced to 6 or less by then.').

card_ruling('phyrexian dreadnought', '2004-10-04', 'The sacrifices are simultaneous, not one at a time.').
card_ruling('phyrexian dreadnought', '2004-10-04', 'Phasing in does not trigger \"enters the battlefield\" abilities, so you don\'t have to sacrifice again if it phases in.').
card_ruling('phyrexian dreadnought', '2007-07-15', 'Reverted to its original wording, this now has an enters-the-battlefield triggered ability. During resolution of the triggered ability, you choose one option or the other.').
card_ruling('phyrexian dreadnought', '2007-07-15', 'If the Dreadnought is no longer on the battlefield when the triggered ability resolves, you may still choose either option.').
card_ruling('phyrexian dreadnought', '2013-04-15', 'If you are unable to sacrifice 12 power worth of creatures, then you cannot choose that option; in that case, you must sacrifice Phyrexian Dreadnought.').

card_ruling('phyrexian gremlins', '2004-10-04', 'If the Gremlins somehow become an artifact and then tap themselves, they can never be untapped during the untap phase. They must be untapped by an external effect.').
card_ruling('phyrexian gremlins', '2004-10-04', 'Only prevents the artifact from untapping during untap step. Ones that untap during upkeep are not inhibited.').
card_ruling('phyrexian gremlins', '2004-10-04', 'The target artifact can\'t untap if the Gremlins and the artifact are tapped during untap, even if it is your artifact, and you plan on untapping the Gremlins.').
card_ruling('phyrexian gremlins', '2004-10-04', 'The effect does not end if the target stops being valid. For example, if it stops being an artifact.').
card_ruling('phyrexian gremlins', '2004-10-04', 'Can target a tapped artifact.').

card_ruling('phyrexian grimoire', '2004-10-04', 'If there is only one card in your graveyard, it is exiled by the first part of the effect and you do not get to put any cards into your hand since the second part fails.').
card_ruling('phyrexian grimoire', '2006-10-15', 'It does not target the cards, but it targets the opponent. If you can\'t target an opponent, you can\'t activate this ability.').

card_ruling('phyrexian hydra', '2011-06-01', 'If multiple damage prevention effects, including Phyrexian Hydra\'s, are attempting to prevent damage that would be dealt to Phyrexian Hydra, its controller chooses which to apply.').
card_ruling('phyrexian hydra', '2011-06-01', 'If damage dealt to Phyrexian Hydra can\'t be prevented (due to Leyline of Punishment, for example), it will be dealt as normal and won\'t cause Phyrexian Hydra to put -1/-1 counters on itself.').

card_ruling('phyrexian infiltrator', '2004-10-04', 'It is possible to activate this ability in response to itself and generate some odd combinations. For example, if you control this card and another creature, you can use this card\'s ability and target the creature you control. You can then use this card\'s ability again and target a creature your opponent controls. The second usage resolves first and you get your opponent\'s creature in exchange for this one. The first usage then resolves and swaps your other creature for the Infiltrator so you get it back. The net effect is that you can swap any creature you have for any of theirs if you can pay this ability twice. Note that your opponent does get the chance to use the Infiltrator if they have the mana in between your two usages and can mess you up.').
card_ruling('phyrexian infiltrator', '2004-10-04', 'There is no effect if the same player controls both creatures when it resolves.').

card_ruling('phyrexian ingester', '2011-06-01', 'Phyrexian Ingester will get bonuses based on the card\'s power and toughness in exile.').
card_ruling('phyrexian ingester', '2011-06-01', 'If the exiled card has a characteristic-defining ability that defines its power and/or toughness (as Slag Fiend does), that ability will apply.').
card_ruling('phyrexian ingester', '2011-06-01', 'If the card in exile isn\'t a creature card (perhaps because it was a land that was only temporarily a creature while on the battlefield), Phyrexian Ingester doesn\'t get a bonus.').

card_ruling('phyrexian metamorph', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('phyrexian metamorph', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('phyrexian metamorph', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('phyrexian metamorph', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('phyrexian metamorph', '2011-06-01', 'Except for also being an artifact, Phyrexian Metamorph copies exactly what was printed on the original permanent and nothing more (unless that creature is itself copying something or is a token; see below). It doesn\'t copy whether that permanent is tapped or untapped, whether it has any counters on it or Auras attached to it, or any noncopy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If Phyrexian Metamorph copies a noncreature artifact, it is no longer a creature.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If the chosen permanent has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If the chosen creature is copying something else (for example, if the chosen creature is a Clone), then your Phyrexian Metamorph enters the battlefield as whatever the chosen creature copied, except it\'s also an artifact.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If the chosen permanent is a token, Phyrexian Metamorph copies the original characteristics of that token as stated by the effect that put the token onto the battlefield, except it\'s also an artifact. Phyrexian Metamorph is not a token.').
card_ruling('phyrexian metamorph', '2011-06-01', 'Any enters-the-battlefield abilities of the copied permanent will trigger when Phyrexian Metamorph enters the battlefield. Any \"as [this] enters the battlefield\" or \"[this] enters the battlefield with\" abilities of the chosen permanent will also work.').
card_ruling('phyrexian metamorph', '2011-06-01', 'If Phyrexian Metamorph somehow enters the battlefield at the same time as another permanent (due to Mass Polymorph or Liliana Vess\'s third ability, for example), Phyrexian Metamorph can\'t become a copy of that permanent. You may only choose a permanent that\'s already on the battlefield.').
card_ruling('phyrexian metamorph', '2011-06-01', 'You can choose not to copy anything. In that case, Phyrexian Metamorph simply enters the battlefield as a 0/0 artifact creature and is put into its owner\'s graveyard as a state-based action (unless something else is raising its toughness).').

card_ruling('phyrexian negator', '2004-10-04', 'Sacrificing this card does not prevent you from having to make the other sacrifices.').

card_ruling('phyrexian obliterator', '2011-06-01', 'Only permanents still on the battlefield when Phyrexian Obliterator\'s triggered ability resolves can be sacrificed.').

card_ruling('phyrexian portal', '2004-10-04', 'The two piles don\'t have to have the same number of cards in them. One of the piles may have zero cards.').

card_ruling('phyrexian processor', '2004-10-04', 'You can pay 0 life if you want, but it\'s not useful most of the time.').

card_ruling('phyrexian purge', '2009-10-01', 'The life payment is an additional cost, paid at the time you cast Phyrexian Purge. You don\'t get any life back if the spell is countered, or if one or more targets have become illegal.').

card_ruling('phyrexian rebirth', '2011-06-01', 'Any triggered abilities that trigger because a creature was destroyed won\'t be put onto the stack until after Phyrexian Rebirth finishes resolving. This means the Horror creature token can be targeted by those abilities, if applicable.').
card_ruling('phyrexian rebirth', '2013-07-01', 'If a creature regenerates or has indestructible, it won\'t be counted when determining the value of X.').

card_ruling('phyrexian reclamation', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('phyrexian revoker', '2014-07-18', 'Activated abilities are written in the form “Cost: Effect.” Some keywords are activated abilities and will have colons in their reminder texts. Static and triggered abilities of sources with the chosen name are unaffected.').
card_ruling('phyrexian revoker', '2014-07-18', 'Phyrexian Revoker’s ability affects sources with the chosen name no matter what zone they are in. For example, if Soul of Ravnica is the chosen name, the ability that exiles it from a player’s graveyard can’t be activated.').

card_ruling('phyrexian soulgorger', '2006-07-15', 'When Phyrexian Soulgorger\'s cumulative upkeep ability resolves, you must either sacrifice a different creature for each age counter on Phyrexian Soulgorger or sacrifice it. You may also sacrifice it as part of its own upkeep payment.').

card_ruling('phyrexian splicer', '2004-10-04', 'If the target which is having the ability removed does not have that ability during the resolution of this effect, then this effect still grants the chosen ability. The reason is that the second target is still legal even if the first one is not.').

card_ruling('phyrexian totem', '2006-09-25', 'When damage is dealt to Phyrexian Totem, its triggered ability will trigger. A number of permanents equal to the amount of damage dealt will be sacrificed when the ability resolves. It doesn\'t matter if Phyrexian Totem isn\'t on the battlefield when the ability resolves, as long as Phyrexian Totem was a creature as it left the battlefield. If one of the permanents sacrificed is Phyrexian Totem itself, the requisite number of other permanents must still be sacrificed.').
card_ruling('phyrexian totem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('phyrexian tyranny', '2004-10-04', 'If more than one card is drawn at a time, this triggers for each card drawn.').

card_ruling('phyrexian unlife', '2011-06-01', 'Phyrexian Unlife won\'t affect damage that reduces your life total from a positive number to 0 or less. For example, if you\'re at 3 life and are dealt 5 damage, you\'ll end up at -2 life. The next time you\'re dealt damage, it will be dealt as though its source had infect.').
card_ruling('phyrexian unlife', '2011-06-01', 'If you\'re at 0 or less life and Phyrexian Unlife leaves the battlefield, you\'ll lose the game (unless some other effect is keeping you from losing).').
card_ruling('phyrexian unlife', '2011-06-01', 'If you\'re at 0 or less life, you can\'t pay any amount of life except 0.').
card_ruling('phyrexian unlife', '2011-06-01', 'You can still lose the game for other reasons, including having ten or more poison counters or drawing a card from a library with no cards in it.').

card_ruling('phytohydra', '2005-10-01', 'If another effect would prevent damage from being dealt to Phytohydra or replace it with something else, Phytohydra\'s controller chooses which effect to apply first.').
card_ruling('phytohydra', '2005-10-01', 'If Phytohydra blocks or is blocked by a creature with first strike or double strike, Phytohydra will get the counters during the first-strike combat damage step before it deals its own combat damage during the second combat damage step.').

card_ruling('phytotitan', '2014-07-18', 'Phytotitan’s ability will return it to the battlefield only if it’s still in the graveyard when the delayed triggered ability resolves. If it’s not, it won’t return to the battlefield.').

card_ruling('pikemen', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('pikemen', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('pikemen', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('pikemen', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('pilfered plans', '2013-04-15', 'If you target yourself, you\'ll put the top two cards of your library into your graveyard before drawing cards.').

card_ruling('pili-pala', '2008-05-01', 'If Pili-Pala is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('pili-pala', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('pili-pala', '2008-05-01', 'Pili-Pala\'s ability is a mana ability. It doesn\'t use the stack and can\'t be responded to.').
card_ruling('pili-pala', '2008-05-01', 'Using this creature\'s ability means you\'re untapping it for mana, not tapping it for mana. Mana Reflection won\'t cause it to produce extra mana.').

card_ruling('pillar of flame', '2012-05-01', 'A creature dealt damage by Pillar of Flame that dies that turn will be exiled even if it wasn\'t the target of Pillar of Flame (because the damage was redirected somehow).').
card_ruling('pillar of flame', '2012-05-01', 'A creature dealt damage by Pillar of Flame that turn will be exiled no matter why the creature would die. It could have its toughness reduced to 0 or less or be destroyed by another spell or ability.').

card_ruling('pillar of war', '2014-02-01', 'Defender only matters when Pillar of War could be declared as an attacking creature. If Pillar of War is already attacking, it becoming not enchanted doesn’t cause it to be removed from combat.').

card_ruling('pillar tombs of aku', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('pillory of the sleepless', '2006-02-01', 'The controller of the enchanted creature loses the life.').

card_ruling('pine walker', '2014-09-20', 'If a face-down creature other than Pine Walker is turned face up and the resulting permanent isn’t a creature, Pine Walker’s last ability won’t trigger.').
card_ruling('pine walker', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('pine walker', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('pine walker', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('pine walker', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('pine walker', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('pine walker', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('pine walker', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('pine walker', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('pine walker', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('pinion feast', '2015-02-25', 'You must choose a creature with flying in order to cast Pinion Feast. You can’t cast it without a legal target just to bolster.').
card_ruling('pinion feast', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('pinion feast', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('pinnacle of rage', '2014-02-01', 'You must choose two legal targets to cast Pinnacle of Rage.').
card_ruling('pinnacle of rage', '2014-02-01', 'If one of the targets is a player, you can redirect the damage dealt by Pinnacle of Rage to a planeswalker that player controls. However, Pinnacle of Rage can’t be used to deal damage to both a player and a planeswalker he or she controls.').

card_ruling('piracy charm', '2007-02-01', 'This is the timeshifted version of Funeral Charm.').

card_ruling('piston sledge', '2011-06-01', 'If there are no creatures on the battlefield that Piston Sledge could be attached to when its enters-the-battlefield triggered ability resolves, it remains on the battlefield unattached.').
card_ruling('piston sledge', '2011-06-01', 'You can sacrifice Piston Sledge to pay its own equip cost. It won\'t become attached as it will be in your graveyard.').

card_ruling('pistus strike', '2011-06-01', 'If the creature is an illegal target when Pistus Strike tries to resolve, the spell is countered. No player gets a poison counter.').
card_ruling('pistus strike', '2013-07-01', 'If the creature with flying regenerates or has indestructible, its controller will still get a poison counter.').

card_ruling('pit fight', '2013-01-24', 'The second target of Pit Fight can be another creature you control, but it can\'t be the same creature as the first target.').
card_ruling('pit fight', '2013-01-24', 'If either target of Pit Fight is an illegal target when the ability tries to resolve, neither creature will deal or be dealt damage.').

card_ruling('pit spawn', '2004-10-04', 'Since this card has First Strike it will often deal damage during first strike damage dealing and its ability will exile the creature it damages before that creature can deal damage back.').

card_ruling('pitfall trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('pitfall trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('pitfall trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').

card_ruling('pith driller', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('pith driller', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('pith driller', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('pith driller', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('pith driller', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('pith driller', '2011-06-01', 'Pith Driller\'s ability is mandatory. If it\'s the only creature on the battlefield when it enters the battlefield, Pith Driller\'s ability will target itself.').

card_ruling('pithing needle', '2005-06-01', 'Pithing Needle affects cards regardless of what zone they\'re in. This includes cards in hand, cards in the graveyard, and exiled cards. For example, a player can\'t cycle Eternal Dragon or return an Eternal Dragon from his or her graveyard to hand if Pithing Needle naming Eternal Dragon is on the battlefield.').
card_ruling('pithing needle', '2005-06-01', 'You can name any card, even if that card doesn\'t normally have an activated ability. You can\'t name a token unless that token has the same name as a card.').
card_ruling('pithing needle', '2005-06-01', 'If you name a card that has both a mana ability and another activated ability, the mana ability can be activated but the other ability can\'t be activated.').
card_ruling('pithing needle', '2009-10-01', 'Once Pithing Needle has left the battlefield, activated abilities of sources with the chosen name can be activated again.').
card_ruling('pithing needle', '2012-10-01', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” Triggered abilities and static abilities of the named card work normally.').

card_ruling('pitiless horde', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('pitiless horde', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('pitiless horde', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('pitiless horde', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('plagiarize', '2004-10-04', 'If you target yourself, this spell has no useful effect. It will not cause an infinite loop since a replacement effect can\'t modify the same event more than once. This effect will not modify the draw that it has you perform.').
card_ruling('plagiarize', '2007-07-15', 'You draw the card from your library as normal, not from your opponent\'s library.').
card_ruling('plagiarize', '2007-07-15', 'If you and your opponent each cast Plagiarize on each other during the same turn, the two spells effectively cancel each other out. You will draw cards when you normally would and so will your opponent.').
card_ruling('plagiarize', '2007-07-15', 'If you target a player whose library is empty, any effect or turn-based action that would cause that player to draw a card will cause you to draw a card instead. It doesn\'t matter that the other player would be unable to draw.').

card_ruling('plague boiler', '2005-10-01', 'You choose whether to add or remove a counter when the second ability resolves. You can\'t choose to remove a counter if there isn\'t one there.').
card_ruling('plague boiler', '2005-10-01', 'If the third ability triggers, removing a counter in response won\'t stop the effect. However, somehow removing Plague Boiler from the battlefield in response would stop the effect because then you wouldn\'t be able to sacrifice Plague Boiler.').
card_ruling('plague boiler', '2005-10-01', 'The sacrifice is done on resolution of the triggered ability.').

card_ruling('plague fiend', '2004-10-04', 'The creature\'s controller gets the option to pay when the triggered ability resolves.').

card_ruling('plague of vermin', '2008-05-01', 'In a game of N players, the process ends when all N players in sequence (starting with you) choose not to pay life. It doesn\'t end the first time a player chooses not to pay life. If a player chooses not to pay life but the process continues, that player may pay life the next time the process gets around to him or her.').
card_ruling('plague of vermin', '2008-05-01', 'The amount of life a player may spend must be equal to or less than that player\'s life total; you can\'t spend life you haven\'t got.').
card_ruling('plague of vermin', '2008-05-01', 'If a player chooses to spend life, his or her life total goes down immediately. This will affect how much life that player can spend the next time the process gets around to him or her. If a player chooses to pay life three different times during this process, for example, that counts as three separate instances of losing life, not as one.').
card_ruling('plague of vermin', '2008-05-01', 'After the process stops, the total amount of life each player paid this way is calculated. That\'s how many tokens each player gets. All the tokens enter the battlefield at the same time.').
card_ruling('plague of vermin', '2008-05-01', 'In a Two-Headed Giant game, each player on a team gets a chance to pay life. The team\'s life total is adjusted in between.').

card_ruling('plague sliver', '2006-09-25', 'Each Sliver deals damage to its controller (not to Plague Sliver\'s controller).').
card_ruling('plague sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('plague sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('plague stinger', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('plague stinger', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('plague stinger', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('plague stinger', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('plague stinger', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('plague stinger', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('plaguemaw beast', '2011-06-01', 'You can sacrifice Plaguemaw Beast to pay the cost of its own activated ability.').
card_ruling('plaguemaw beast', '2011-06-01', 'You can activate the ability even if there are no players or permanents with counters on them.').

card_ruling('planar birth', '2004-10-04', 'They enter the battlefield tapped. They do not enter the battlefield and then tap afterwards.').

card_ruling('planar collapse', '2004-10-04', 'This ability does not trigger at all if there are not 4 or more creatures on the battlefield. It also checks this at the start of resolution and does nothing if this is not still true.').
card_ruling('planar collapse', '2004-10-04', 'The ability is not optional.').
card_ruling('planar collapse', '2010-06-15', 'If this card is not on the battlefield when the ability resolves, then you can\'t sacrifice it. However, you still destroy all creatures.').

card_ruling('planar despair', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('planar despair', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('planar despair', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('planar gate', '2004-10-04', 'Multiple Gates are cumulative. Two will reduce the cost by up to {4}, and so on.').
card_ruling('planar gate', '2004-10-04', 'Only works for its controller, not all players.').
card_ruling('planar gate', '2004-10-04', 'Only reduces the generic mana portion of a spell\'s cost. If the cost does not include generic mana or includes less than {2}, you get a reduced or null effect from this card.').
card_ruling('planar gate', '2008-04-01', 'Works on all spells with the type Creature, even if the spell has another type like Artifact or Enchantment.').

card_ruling('planar guide', '2004-10-04', 'All \"enters the battlefield\" abilities trigger as normal.').
card_ruling('planar guide', '2004-10-04', 'Creatures that were face down return to the battlefield face up.').
card_ruling('planar guide', '2004-10-04', 'If the ability is activated during the End step, then the creatures do not return until the End step of the following turn.').
card_ruling('planar guide', '2005-08-01', 'When the creatures leave the battlefield, all Auras on them go to their owners\' graveyards, any counters on them are removed, and effects on them end. Token creatures do not return to the battlefield.').

card_ruling('planar outburst', '2015-08-25', 'A “nonland creature” is a creature that isn’t also a land.').
card_ruling('planar outburst', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('planar outburst', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('planar outburst', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('planar outburst', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('planar outburst', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('planar outburst', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('planar overlay', '2004-10-04', 'If you have a land which counts as multiple land types, you can choose that land as each of those types. For example, a dual land could be chosen as two of your land types.').

card_ruling('planar void', '2004-10-04', 'If spell is used to destroy Planar Void, it goes to the graveyard, then the spell goes to the graveyard. The spell is not exiled, as Planar Void was not on the battlefield when it was put into the graveyard.').
card_ruling('planar void', '2004-10-04', 'The card does go to the graveyard before it is exiled, so other triggered abilities will trigger on the card going to the graveyard.').
card_ruling('planar void', '2008-05-01', 'It will not trigger on itself going to the graveyard, or on any other cards going to the graveyard at the same time. This means that neither itself nor other cards put into the graveyard at the same time as it will get exiled.').

card_ruling('planeswalker\'s favor', '2004-10-04', 'If the opponent has no cards in hand, then X is zero.').

card_ruling('planeswalker\'s fury', '2004-10-04', 'If the opponent has no cards in hand, then no damage is dealt.').

card_ruling('planeswalker\'s mirth', '2004-10-04', 'If the opponent has no cards in hand, then no life is gained.').

card_ruling('planeswalker\'s mischief', '2004-10-04', 'If the opponent has no cards in hand, then the ability does nothing.').
card_ruling('planeswalker\'s mischief', '2004-10-04', 'If Planeswalker\'s Mischief leaves the battlefield, any cards it exiled are still under the effect of the ability and can be cast (and if they are not cast, they are returned to the owner\'s hand).').
card_ruling('planeswalker\'s mischief', '2004-10-04', 'When cast, the card goes to its owner\'s graveyard after it resolves.').

card_ruling('planeswalker\'s scorn', '2004-10-04', 'If the opponent has no cards in hand, then X is zero.').

card_ruling('plasm capture', '2013-04-15', 'If the countered spell had {X} in its mana cost, include the value chosen for that X when determining how much mana to add to your pool. For example, if you counter a spell that costs {X}{R} with X equal to 5, you\'ll add six mana to your mana pool.').
card_ruling('plasm capture', '2013-04-15', 'Your precombat main phase is the main phase that happens immediately after your draw step, not any other main phase.').

card_ruling('plateau', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('plateau', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('plated geopede', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('plated geopede', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('plated pegasus', '2006-09-25', 'If a spell would deal damage to multiple creatures and/or players, Plated Pegasus will prevent 1 damage that the spell would deal to each of them.').

card_ruling('plated rootwalla', '2006-02-01', 'If this card\'s ability is activated by one player, then another player takes control of it on the same turn, the second player can\'t activate its ability that turn.').

card_ruling('plated sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('plated sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('plated spider', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('platinum angel', '2004-12-01', 'Effects that say the game is a draw, such as the _Legends_(TM) card Divine Intervention, are not affected by Platinum Angel. They\'ll still work.').
card_ruling('platinum angel', '2004-12-01', 'You can concede a game while Platinum Angel on the battlefield. A concession causes you to leave the game, which then causes you to lose the game (Once you concede, you no longer control a Platinum Angel, so its ability can\'t prevent you from losing the game).').
card_ruling('platinum angel', '2009-10-01', 'No game effect can cause you to lose the game or cause any opponent to win the game while you control Platinum Angel. It doesn\'t matter whether you have 0 or less life, you\'re forced to draw a card while your library is empty, you have ten or more poison counters, you\'re dealt combat damage by Phage the Untouchable, your opponent has Mortal Combat with twenty or more creature cards in his or her graveyard, or so on. You keep playing.').
card_ruling('platinum angel', '2009-10-01', 'Other circumstances can still cause you to lose the game, however. You will lose a game if you concede, if you\'re penalized with a Game Loss or a Match Loss during a sanctioned tournament due to a DCI rules infraction, or if your _Magic Online_(R) game clock runs out of time.').
card_ruling('platinum angel', '2009-10-01', 'If you control Platinum Angel in a Two-Headed Giant game, your team can\'t lose the game and the opposing team can\'t win the game.').

card_ruling('platinum emperion', '2011-01-01', 'The ability doesn\'t prevent damage. Rather, it changes the results of that damage. For example, if a creature with lifelink deals damage to you, you won\'t lose any life, but its controller will still gain that much life. Similarly, if a creature you control with lifelink deals damage to another player, that player will lose life but you won\'t gain any life. Notably, if a creature with infect deals damage to you, you\'ll get that many poison counters.').
card_ruling('platinum emperion', '2011-01-01', 'Abilities that trigger whenever damage is dealt to you will still trigger because that damage is still dealt, even though your life total doesn\'t change as a result.').
card_ruling('platinum emperion', '2011-01-01', 'Spells and abilities that would normally cause you to gain or lose life still resolve, but the life-gain or life-loss part simply has no effect.').
card_ruling('platinum emperion', '2011-01-01', 'You can\'t pay a cost that includes the payment of any amount of life other than 0 life.').
card_ruling('platinum emperion', '2011-01-01', 'If a cost would include causing you to gain life (like the alternative cost of an opponent\'s Invigorate does), that cost can\'t be paid.').
card_ruling('platinum emperion', '2011-01-01', 'Effects that would replace having you gain life with some other effect won\'t be able to be applied because it\'s impossible for you to gain life. The same is true for events that would replace having you lose life with some other effect.').
card_ruling('platinum emperion', '2011-01-01', 'Effects that replace an event with having you gain life (like Words of Worship\'s effect does) or having you lose life will end up replacing the event with nothing.').
card_ruling('platinum emperion', '2011-01-01', 'If an effect says to set your life total to a certain number that\'s different than your current life total, that part of the effect won\'t do anything.').
card_ruling('platinum emperion', '2011-01-01', 'If an effect would cause you to exchange life totals with another player, the exchange won\'t happen. Neither player\'s life total changes.').
card_ruling('platinum emperion', '2011-01-01', 'The ability won\'t preclude you from losing the game. It just precludes your life total from changing.').

card_ruling('plaxmanta', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('plea for power', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('plea for power', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('plea for power', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('plea for power', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('plea for power', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('plow under', '2004-10-04', 'The owner decides the order the two lands are stacked there.').

card_ruling('plunder', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('plunder', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('plunder', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('plunder', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('plunder', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('plunder', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('plunder', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('plunder', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('plunder', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('plunder', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('plunge into darkness', '2004-12-01', 'Everything other than the mode/entwine choice (and paying the {1}{B}, of course) is done on resolution.').
card_ruling('plunge into darkness', '2004-12-01', 'Assuming the entwine cost has been paid, the effect is (1) sacrifice any number of creatures, (2) gain 3 life for each sacrificed creature, (3) choose X, (4) pay X life, and (5) look at the top X cards of your library, put one into your hand, and exile the rest.').

card_ruling('poisonbelly ogre', '2006-02-01', 'The controller of the creature that entered the battlefield loses the life. This may or may not be the same player as Poisonbelly Ogre\'s controller.').

card_ruling('polar kraken', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('polis crusher', '2013-09-15', '“Protection from enchantments” means that Polis Crusher can’t be enchanted, it can’t be targeted by Aura spells or by abilities of enchantments, all damage that would be dealt to it by enchantments is prevented, and it can’t be blocked by enchantment creatures. Notably, it doesn’t have protection from enchanted creatures (unless those creatures are also enchantments).').
card_ruling('polis crusher', '2013-09-15', 'Polis Crusher must be monstrous as it deals combat damage to a player in order for its last ability to trigger. Once the ability triggers, it resolves even if Polis Crusher isn’t on the battlefield at that time.').
card_ruling('polis crusher', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('polis crusher', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('polis crusher', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('pollen remedy', '2004-10-04', 'You can\'t choose zero targets. You must choose between 1 and 3 (or 6) targets.').

card_ruling('polluted mire', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('polukranos, world eater', '2013-09-15', 'The value of X in Polukranos’s last ability is equal to the value chosen for X when its activated ability was activated.').
card_ruling('polukranos, world eater', '2013-09-15', 'The number of targets chosen for the triggered ability must be at least one (if X wasn’t 0) and at most X. You choose the division of damage as you put the ability on the stack, not as it resolves. Each target must be assigned at least 1 damage. In multiplayer games, you may choose creatures controlled by different opponents.').
card_ruling('polukranos, world eater', '2013-09-15', 'If some, but not all, of the ability’s targets become illegal, you can’t change the division of damage. Damage that would’ve been dealt to illegal targets simply isn’t dealt.').
card_ruling('polukranos, world eater', '2013-09-15', 'As Polukranos’s triggered ability resolves, Polukranos deals damage first, then the target creatures do. Although no creature will die until after the ability finishes resolving, the order could matter if Polukranos has wither or infect.').
card_ruling('polukranos, world eater', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('polukranos, world eater', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('polukranos, world eater', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('polymorph', '2004-10-04', 'If there are no creatures in the player\'s library, then the target creature is still destroyed, you see all the cards in that player\'s library, and then they shuffle and continue play.').
card_ruling('polymorph', '2009-10-01', 'If the targeted creature is an illegal target by the time Polymorph would resolve, the entire spell is countered. Nothing else happens.').
card_ruling('polymorph', '2009-10-01', 'If there are no creature cards in the player\'s library, all the cards in that library are revealed, then the library is shuffled. (The targeted creature remains destroyed.)').
card_ruling('polymorph', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('polymorph', '2013-07-01', 'If the targeted creature has indestructible, it\'s still a legal target -- it just isn\'t destroyed. The rest of Polymorph\'s effect happens as normal.').

card_ruling('polymorphist\'s jest', '2014-07-18', 'Creatures that enter the battlefield or come under the target player’s control after Polymorphist’s Jest resolves won’t be affected.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'The creatures will lose all other colors and creature types, but they will retain any other card types (such as artifact) or supertypes (such as legendary) they may have.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'Polymorphist’s Jest overwrites all previous effects that set the creatures’ base power and toughness to specific values. Any power- or toughness-setting effects that start to apply after Polymorphist’s Jest resolves will overwrite this effect.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'Polymorphist’s Jest doesn’t counter abilities that have already triggered or been activated. In particular, there is no way to cast this spell to stop a creature’s ability that says “At the beginning of your upkeep,” “When this creature enters the battlefield,” or similar from triggering.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'If one of the affected creatures gains an ability after Polymorphist’s Jest resolves, it will keep that ability.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'Effects that modify a creature’s power and/or toughness, such as the effect of Titanic Growth, will apply to the creatures no matter when they started to take effect. The same is true for any counters that change their power and/or toughness and effects that switch power and toughness.').
card_ruling('polymorphist\'s jest', '2014-07-18', 'If one of the Theros block Gods is affected by Polymorphist’s Jest, it will be a legendary 1/1 blue Frog enchantment creature with no abilities. If it stops being a creature, perhaps because your devotion to its color(s) decreased, it will be a legendary blue enchantment with no abilities. The way continuous effects work, the God’s type-changing ability is applied before the effect that removes that ability is applied.').

card_ruling('polymorphous rush', '2014-04-26', 'The creature that’s copied isn’t targeted by Polymorphous Rush. You choose that creature as the spell resolves. It can be one of the targeted creatures. A creature can become a copy of itself this way, though this usually won’t have any noticeable effect.').
card_ruling('polymorphous rush', '2014-04-26', 'The target creatures copy the printed values of the chosen creature, plus any copy effects that have been applied to it. They won’t copy counters on that creature or effects that have changed the creature’s power, toughness, types, color, or so on.').
card_ruling('polymorphous rush', '2014-04-26', 'If the creatures copy a creature that’s copying a creature, they will become whatever the chosen creature is copying.').
card_ruling('polymorphous rush', '2014-04-26', 'This effect can cause the targets to stop being creatures. For example, if they each become a copy of a land that has become a creature, the effect that turned that land into a creature won’t be copied. The targets will become noncreature lands.').
card_ruling('polymorphous rush', '2014-04-26', 'Effects that have already applied to the target creatures will continue to apply to them. For example, if Giant Growth had given one of those creatures +3/+3 earlier in the turn, then Polymorphous Rush makes it a copy of Pensive Minotaur (a 2/3 creature), it will become a 5/6 Pensive Minotaur.').
card_ruling('polymorphous rush', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('polymorphous rush', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('polymorphous rush', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('polymorphous rush', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('polymorphous rush', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('ponder', '2007-10-01', 'If you choose to shuffle your library, that includes the three cards you just looked at and put back on top of it.').

card_ruling('pongify', '2007-02-01', 'The creature\'s controller gets the Ape token even if the creature isn\'t actually destroyed.').

card_ruling('pontiff of blight', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('pontiff of blight', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('pontiff of blight', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('ponyback brigade', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('ponyback brigade', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('ponyback brigade', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('ponyback brigade', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('ponyback brigade', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ponyback brigade', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ponyback brigade', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('ponyback brigade', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('ponyback brigade', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('pooling venom', '2007-05-01', 'The triggered ability triggers whenever the enchanted land becomes tapped, not just when it\'s tapped for mana.').
card_ruling('pooling venom', '2007-05-01', 'Only Pooling Venom\'s controller can activate the activated ability.').

card_ruling('pools of becoming', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('pools of becoming', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('pools of becoming', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('pools of becoming', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('pools of becoming', '2009-10-01', 'The face-up plane card isn\'t currently part of its owner\'s planar deck. If the {C} ability is rolled by the owner of Pools of Becoming, Pools of Becoming is not one of the three cards that are revealed.').
card_ruling('pools of becoming', '2009-10-01', 'You may put the {C} abilities of the three revealed plane cards on the stack in any order. The last one you put on the stack will be the first one to resolve.').
card_ruling('pools of becoming', '2009-10-01', 'If one of the revealed plane cards is another Pools of Becoming, its {C} ability triggers. When it resolves, you\'ll reveal three more cards from the top of your planar deck, their {C} abilities will trigger, and you\'ll put them on the stack in any order on top of any remaining {C} abilities from the first Pools of Becoming\'s effect.').

card_ruling('porcelain legionnaire', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('porcelain legionnaire', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('porcelain legionnaire', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('porcelain legionnaire', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('porcelain legionnaire', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('porphyry nodes', '2006-07-15', 'This card\'s ability is not targeted, so even untargetable creatures or those with Protection can be chosen.').
card_ruling('porphyry nodes', '2007-02-01', 'This is the timeshifted version of Drop of Honey.').
card_ruling('porphyry nodes', '2013-07-01', 'If the creature with the least power has indestructible, the ability does nothing.').
card_ruling('porphyry nodes', '2013-07-01', 'If there are multiple creatures tied for least power and some but not all of them have indestructible, the ones with indestructible can\'t be chosen.').

card_ruling('portcullis', '2004-10-04', 'Creatures which are phasing in will not trigger this card\'s ability.').
card_ruling('portcullis', '2004-10-04', 'If the creature that triggered Portcullis is no longer on the battlefield when the ability resolves, then it fails to do anything.').
card_ruling('portcullis', '2004-10-04', 'You check the count of the number of creatures on the battlefield again during resolution. The creature will not be exiled if the count is 2 or less.').
card_ruling('portcullis', '2009-10-01', 'The creature does enter the battlefield, so any other abilities that trigger on it entering the battlefield trigger. This follows the normal rules for timing triggered abilities: first, the current player puts his or her abilities on the stack in any order, then each other player in turn order does the same. Portcullis\' own ability is included in this, so it could result in the creature being exiled after some abilities have resolved, but before others.').
card_ruling('portcullis', '2009-10-01', 'The \"when Portcullis leaves the battlefield\" ability is set up as part of the ability that exiles the creature. It will trigger when Portcullis leaves the battlefield even if Portcullis has somehow lost its abilities at that time.').

card_ruling('portent', '2004-10-04', 'This does not cause a player to lose if they have less than 3 cards in their library. It allows you to look at and reorder or shuffle whatever remaining cards there might be.').

card_ruling('portent of betrayal', '2013-09-15', 'Portent of Betrayal can target any creature, even one that’s untapped or one you already control.').
card_ruling('portent of betrayal', '2013-09-15', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it, although those permanents will stay attached to the creature.').
card_ruling('portent of betrayal', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('portent of betrayal', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('portent of betrayal', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('portent of betrayal', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('possessed skaab', '2015-06-22', 'Possessed Skaab’s last ability will apply no matter what caused it to die (lethal damage, being sacrificed, enough -1/-1 counters, and so on). Possessed Skaab won’t go to the graveyard. It will be put from the battlefield into exile. Abilities that trigger whenever a creature dies won’t trigger.').

card_ruling('possibility storm', '2013-04-15', 'Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types. Two cards share a card type if they have at least one card type in common.').
card_ruling('possibility storm', '2013-04-15', 'The original spell is part of the group of exiled cards put on the bottom of the library in a random order. If the exiled card that shared a card type with that card wasn\'t cast, it\'s also part of this group.').
card_ruling('possibility storm', '2013-04-15', 'If Possibility Storm\'s ability doesn\'t exile the original spell (perhaps because another Possibility Storm already exiled it), you\'ll still exile cards from the top of your library until you exile a card that shares a card type with it and have the opportunity to cast that spell.').
card_ruling('possibility storm', '2013-04-15', 'If you cast a spell without paying its mana cost, you can\'t pay any alternative costs, such as overload costs. You can pay additional costs, such as kicker costs. If the spell has any mandatory additional costs, you must pay those.').
card_ruling('possibility storm', '2013-04-15', 'If the spell you cast from exile has X in its mana cost, you must choose 0 as its value.').
card_ruling('possibility storm', '2013-04-15', 'Instant and sorcery cards with miracle allow a player to cast a card immediately upon drawing it. If a player casts a spell this way, the spell is cast from that player\'s hand. Possibility Storm will trigger.').

card_ruling('postmortem lunge', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('postmortem lunge', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('postmortem lunge', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('postmortem lunge', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('postmortem lunge', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('postmortem lunge', '2011-06-01', 'If the creature card has {X} in its mana cost, that X is 0. Remember that the value of X chosen for Postmortem Lunge is independent of the value of X on other cards.').
card_ruling('postmortem lunge', '2011-06-01', 'If the creature is no longer on the battlefield at the beginning of the next end step, it won\'t be exiled.').

card_ruling('poultice sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('poultice sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('pouncing kavu', '2004-10-04', 'If the Pouncing Kavu is kicked, it enters the battlefield with haste and will keep this ability until it leaves the battlefield.').

card_ruling('pouncing wurm', '2007-02-01', 'If the Pouncing Wurm is kicked, Pouncing Wurm has haste permanently.').

card_ruling('powder keg', '2004-10-04', 'Only destroys artifacts and creatures with exactly the specified cost. It does not mean \"less than or equal to\".').
card_ruling('powder keg', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('power armor', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('power armor', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('power armor', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('power artifact', '2004-10-04', 'Only affects the generic mana part of activation costs. Colored parts of mana costs are not affected.').
card_ruling('power artifact', '2004-10-04', 'Can be placed on artifacts with no or zero activation costs, but this has no effect on them. It does not increase the cost to one.').
card_ruling('power artifact', '2006-07-15', 'Can\'t reduce Snow mana costs.').
card_ruling('power artifact', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('power play', '2014-05-29', 'If you have Power Play but aren’t the starting player (because another player also had Power Play), your place in the turn order is unaffected. If you’re sitting to the right of the starting player, you’re going last. Not all power plays bear fruit.').
card_ruling('power play', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('power play', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('power play', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('power play', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('power play', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('power play', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('power play', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('power sink', '2004-10-04', 'Does not increase the mana cost of the spell. It just requires a separate expenditure in order for it to succeed.').
card_ruling('power sink', '2004-10-04', 'When this spell resolves, you either pay X mana or let your lands become tapped. The lands that become tapped are not \"tapped for mana\". If you choose to pay, you may pay the X mana using whatever mana abilities you want to use.').
card_ruling('power sink', '2010-03-01', 'Only lands that actually have mana abilities will get tappped. This includes basic lands and lands with mana abilities printed on them, as well as lands which have been granted a mana ability by some effect.').

card_ruling('power surge', '2004-10-04', 'This card knows how many were untapped even if it was not on the battlefield at the beginning of the turn.').

card_ruling('power taint', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('powerleech', '2004-10-04', 'In a multi-player game it affects all opponents.').

card_ruling('pox', '2007-09-16', 'The losses, discards, and sacrifices are rounded up. For example, if you have 5 life, you\'ll lose 2 life, leaving you at 3 life. (Five divided by three is one and two-thirds. Rounding that up to the nearest whole number gives you two.)').
card_ruling('pox', '2007-09-16', 'Each part of Pox\'s effect is processed separately. For each part, first the player whose turn it is makes all necessary choices (such as which cards to discard), then each other player in turn order chooses, then the actions happen at the same time. Then Pox\'s effect moves to the next stage.').
card_ruling('pox', '2007-09-16', 'The number you need to lose, discard, or sacrifice isn\'t calculated until it\'s time to perform that part of the effect. For example, if your opponent casts Pox and you discard Dodecapod as part of Pox\'s discard effect, Dodecapod will be put onto the battlefield instead of into your graveyard. Then, when determining how many creatures you need to sacrifice, the Dodecapod is taken into account.').

card_ruling('praetor\'s counsel', '2011-06-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then resolve Praetor\'s Counsel, you have no maximum hand size. However, if those events happened in the opposite order, your maximum hand size would be two.').
card_ruling('praetor\'s counsel', '2011-06-01', 'Later effects that modify your maximum hand size, but don\'t set it to a specific number, will have no effect.').

card_ruling('praetor\'s grasp', '2011-06-01', 'Other players, including the card\'s owner, can\'t look at the card while it remains exiled.').
card_ruling('praetor\'s grasp', '2011-06-01', 'Playing a card exiled with Praetor\'s Grasp follows all the normal rules for playing that card. You must pay its costs, and you must follow all timing restrictions, for example.').

card_ruling('prahv', '2012-06-01', 'Prahv only cares about the turn-based action of declaring attackers. If an effect allows you to put a creature onto the battlefield attacking, you can cast spells later that turn.').

card_ruling('prairie stream', '2015-08-25', 'Even though these lands have basic land types, they are not basic lands because “basic” doesn’t appear on their type line. Notably, controlling two or more of them won’t allow others to enter the battlefield untapped.').
card_ruling('prairie stream', '2015-08-25', 'However, because these cards have basic land types, effects that specify a basic land type without also specifying that the land be basic can affect them. For example, a spell or ability that reads “Destroy target Forest” can target Canopy Vista, while one that reads “Destroy target basic Forest” cannot.').
card_ruling('prairie stream', '2015-08-25', 'If one of these lands enters the battlefield at the same time as any number of basic lands, those other lands are not counted when determining if this land enters the battlefield tapped or untapped.').

card_ruling('preacher', '2004-10-04', 'The target of the ability is a creature the chosen opponent controls. As the ability resolves, if the target isn\'t a creature controlled by the chosen opponent, the ability will be countered. If the ability\'s target is changed somehow, the new target must be a creature controlled by the chosen opponent.').
card_ruling('preacher', '2007-09-16', 'Whether Preacher is tapped is checked continually, starting when the ability is activated. If, before the ability resolves, there\'s any point at which Preacher is untapped, the ability has no effect -- even if Preacher is tapped again by the time the ability resolves.').
card_ruling('preacher', '2007-09-16', 'When you activate the ability, you choose an opponent, then that opponent chooses the target.').

card_ruling('precursor golem', '2011-01-01', 'The second ability triggers whenever a player casts an instant or sorcery spell that targets only one Golem and no other object or player. That Golem can be Precursor Golem itself, one of the Golem tokens it created, or any other Golem. It doesn\'t matter who controls the Golem.').
card_ruling('precursor golem', '2011-01-01', 'If an instant or sorcery spell has multiple targets, but it\'s targeting the same Golem with all of them (such as Agony Warp targeting the same Golem twice), Precursor Golem\'s last ability will trigger.').
card_ruling('precursor golem', '2011-01-01', 'Any Golem that couldn\'t be targeted by the original spell (due to shroud, protection abilities, targeting restrictions, or any other reason) is just ignored by Precursor Golem\'s second ability.').
card_ruling('precursor golem', '2011-01-01', 'The controller of the spell that caused Precursor Golem\'s second ability to trigger also controls all the copies. That player chooses the order the copies are put onto the stack. The original spell will be on the stack beneath those copies and will resolve last.').
card_ruling('precursor golem', '2011-01-01', 'The copies that Precursor Golem\'s second ability creates are created on the stack, so they\'re not \"cast.\" Abilities that trigger when a player casts a spell (like Precursor Golem\'s second ability itself) won\'t trigger.').
card_ruling('precursor golem', '2011-01-01', 'If the spell that\'s copied is modal (that is, it says \"Choose one --\" or the like), the copies will have the same mode. Their controller can\'t choose a different one.').
card_ruling('precursor golem', '2011-01-01', 'If the spell that\'s copied has an X whose value was determined as it was cast (like Fireball does), the copies have the same value of X.').
card_ruling('precursor golem', '2011-01-01', 'The controller of a copy can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too.').

card_ruling('predator dragon', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('predator dragon', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('predator dragon', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('predator dragon', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').

card_ruling('predator ooze', '2011-01-22', 'Each time a creature dies, check whether Predator Ooze had dealt any damage to it at any time during that turn. If so, Predator Ooze\'s ability will trigger. It doesn\'t matter who controlled the creature or whose graveyard it was put into.').

card_ruling('predator\'s rapport', '2013-01-24', 'Use the creature\'s power and toughness when Predator\'s Rapport resolves to determine how much life you gain.').

card_ruling('predatory advantage', '2009-05-01', 'Predatory Advantage\'s ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless the opponent whose turn it is didn\'t cast a creature spell that turn, and (2) the ability will do nothing unless the opponent whose turn it is still hasn\'t cast a creature spell by the time it resolves.').
card_ruling('predatory advantage', '2009-05-01', 'Predatory Advantage checks whether a player cast a creature spell, not whether a creature entered the battlefield under that player\'s control. If that player cast a creature spell but you countered it, for example, you won\'t get a Lizard token that turn.').
card_ruling('predatory advantage', '2009-05-01', 'In a Two-Headed Giant game, Predatory Advantage\'s ability triggers twice at the end of the opposing team\'s turn: once for each player. If neither of those players cast a creature spell that turn, you\'ll get two Lizards. If one of those players cast a creature spell but the other didn\'t, you\'ll get one Lizard.').

card_ruling('predatory focus', '2006-04-01', 'Will even apply to creatures that entered the battlefield after it resolved (which obviously must have Haste in order to attack).').
card_ruling('predatory focus', '2008-04-01', 'If a creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('predatory focus', '2008-04-01', 'When Predatory Focus resolves, you choose whether to use its effect or not. If you choose to use it, all your creatures will deal their combat damage to the planeswalker or defending player this turn whether or not they become blocked. You can\'t have any of them deal combat damage to creatures that block them. If you choose not to use its effect, nothing happens.').

card_ruling('predatory hunger', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('predatory rampage', '2012-07-01', 'Only creatures you control when Predatory Rampage resolves will get +3/+3. Creatures that enter the battlefield or that you gain control of later in the turn are unaffected.').
card_ruling('predatory rampage', '2012-07-01', 'Each creature your opponents control blocks if able, even if that creature wasn\'t on the battlefield or wasn\'t controlled by an opponent when Predatory Rampage resolved.').
card_ruling('predatory rampage', '2012-07-01', 'Predatory Rampage doesn\'t force any creature to attack.').
card_ruling('predatory rampage', '2012-07-01', 'Each creature controlled by an opponent blocks only if it\'s able to do so as the declare blockers step begins. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t block, or no creatures are attacking that player or a planeswalker controlled by that player, then it doesn\'t block. If there\'s a cost associated with having the creature block, the player isn\'t forced to pay that cost, so it doesn\'t block in that case either.').
card_ruling('predatory rampage', '2012-07-01', 'Your opponent still chooses which attacking creature each creature he or she controls blocks.').
card_ruling('predatory rampage', '2012-07-01', 'If there are multiple combat phases in a turn, each affected creature must block only in the first one in which it\'s able to.').

card_ruling('predatory sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('predatory sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('predatory urge', '2009-10-01', 'The controller of the enchanted creature may activate the ability, not the controller of Predatory Urge.').
card_ruling('predatory urge', '2009-10-01', 'If the enchanted creature\'s ability is activated, that creature is the one that will deal and be dealt damage when the ability resolves. It doesn\'t matter if Predatory Urge leaves the battlefield or somehow becomes attached to another creature by that time.').
card_ruling('predatory urge', '2009-10-01', 'If the targeted creature leaves the battlefield (or otherwise becomes an illegal target) before the ability resolves, the ability is countered. The enchanted creature isn\'t dealt damage.').
card_ruling('predatory urge', '2009-10-01', 'On the other hand, if the enchanted creature leaves the battlefield before the ability resolves, the ability continues to resolve. The enchanted creature deals damage to the targeted creature equal to the power the enchanted creature had as it last existed on the battlefield.').
card_ruling('predatory urge', '2009-10-01', 'You may have the enchanted creature target itself with its own ability. If you do, it will deal damage to itself equal to its power, then immediately do it again.').

card_ruling('preeminent captain', '2014-07-18', 'You choose what the Soldier you put onto the battlefield is attacking. It doesn’t have to attack the same player or planeswalker as Preeminent Captain.').
card_ruling('preeminent captain', '2014-07-18', 'Any abilities of the Soldier creature card that trigger “Whenever [this creature] attacks” won’t trigger because the creature was never declared as an attacking creature.').

card_ruling('preferred selection', '2005-11-01', 'This is no longer a draw, it\'s simply putting the card into your hand.').

card_ruling('prescient chimera', '2013-09-15', 'The triggered ability will resolve and you’ll scry before the instant or sorcery spell resolves.').
card_ruling('prescient chimera', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('prescient chimera', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('prescient chimera', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('prescient chimera', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('presence of gond', '2008-05-01', 'The creature\'s controller, not Presence of Gond\'s controller, is the one who can activate the ability, so that\'s the player who will get the token.').

card_ruling('presence of the master', '2004-10-04', 'Does not stop enchantments from being put onto the battlefield by a spell or ability.').
card_ruling('presence of the master', '2004-10-04', 'This effect triggers when the spell is announced and prior to allowing responses to it.').

card_ruling('press the advantage', '2015-02-25', 'If you cast Press the Advantage with two targets, and only one of them is still a legal target as Press the Advantage resolves, that target will get +2/+2 and gain trample. Press the Advantage will have no effect on the illegal target.').

card_ruling('prey upon', '2012-07-01', 'If either or both targets are illegal when Prey Upon tries to resolve, no creature will deal or be dealt damage.').

card_ruling('prey\'s vengeance', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('prey\'s vengeance', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('prey\'s vengeance', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('prey\'s vengeance', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('prey\'s vengeance', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('prey\'s vengeance', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('prey\'s vengeance', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('prey\'s vengeance', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').

card_ruling('preyseizer dragon', '2012-06-01', 'Preyseizer Dragon\'s ability counts the number of +1/+1 counters on it when the ability resolves. It counts any +1/+1 counters, not just ones put on it due to devour.').

card_ruling('price of knowledge', '2013-10-17', 'Count the number of cards in that opponent’s hand when the ability resolves to determine how much damage is dealt.').

card_ruling('pride guardian', '2011-09-22', 'If Pride Guardian gains the ability to block additional creatures and does so, you\'ll still gain only 3 life.').

card_ruling('pride of lions', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('pride of lions', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('pride of lions', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('pride of lions', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('priest of gix', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('priest of titania', '2004-10-04', 'Since he is an Elf, he counts himself.').

card_ruling('priest of urabrask', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('primal beyond', '2008-04-01', 'You can use the mana produced by Primal Beyond\'s last ability to pay an alternative cost (such as evoke) or additional cost incurred while casting an Elemental spell. It\'s not limited to just that spell\'s mana cost.').
card_ruling('primal beyond', '2008-04-01', 'The mana can\'t be spent to activate activated abilities of Elemental sources that aren\'t on the battlefield (such as the reinforce ability of an Elemental card in your hand).').

card_ruling('primal boost', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('primal clay', '2012-07-01', 'In zones other than the battlefield, Primal Clay is 0/0.').
card_ruling('primal clay', '2012-07-01', 'If another permanent enters the battlefield as a copy of Primal Clay, the controller of that permanent will get to make a new choice. The copy won\'t necessarily have the same power, toughness, and abilities as the original.').
card_ruling('primal clay', '2012-07-01', 'If a creature that\'s already on the battlefield becomes a copy of Primal Clay, it copies the power, toughness, and abilities that were chosen for Primal Clay when it entered the battlefield.').

card_ruling('primal cocoon', '2010-08-15', 'Primal Cocoon\'s first triggered ability triggers at the beginning of the upkeep of Primal Cocoon\'s controller (who is not necessarily the enchanted creature\'s controller).').
card_ruling('primal cocoon', '2010-08-15', 'The counters are put on the creature, not on Primal Cocoon. They\'ll stay on the creature even after Primal Cocoon is sacrificed or otherwise stops enchanting that creature.').

card_ruling('primal command', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('primal growth', '2004-10-04', 'You may choose not to find any basic lands when you search.').
card_ruling('primal growth', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('primal plasma', '2007-02-01', 'If a creature that\'s already on the battlefield becomes a copy of Primal Plasma, it copies the power, toughness, and abilities that were chosen for Primal Plasma when it entered the battlefield.').
card_ruling('primal plasma', '2007-02-01', 'This is the timeshifted version of Primal Clay.').
card_ruling('primal plasma', '2012-07-01', 'In zones other than the battlefield, Primal Plasma is 0/0.').
card_ruling('primal plasma', '2012-07-01', 'If another permanent enters the battlefield as a copy of Primal Clay, the controller of that permanent will get to make a new choice. The copy won\'t necessarily have the same power, toughness, and abilities as the original.').

card_ruling('primal surge', '2012-05-01', 'A permanent card is an artifact, creature, enchantment, land, or planeswalker card.').
card_ruling('primal surge', '2012-05-01', 'Repeating the process includes the instruction to repeat the process. That is, you\'ll keep exiling cards and putting them onto the battlefield until you fail to put a permanent card onto the battlefield.').
card_ruling('primal surge', '2012-05-01', 'If you exile a permanent card that you can\'t put onto the battlefield (such as an Aura with nothing legal to enchant) or one you don\'t want to put onto the battlefield, Primal Surge finishes resolving.').
card_ruling('primal surge', '2012-05-01', 'If putting any of those permanent cards onto the battlefield causes abilities to trigger, those abilities will wait to go on the stack until Primal Surge has finished resolving. Starting with the active player, each player puts his or her abilities on the stack in any order.').

card_ruling('primal vigor', '2013-10-17', 'It doesn’t matter who controls the tokens or the creature that the +1/+1 counters are being placed on.').
card_ruling('primal vigor', '2013-10-17', 'Primal Vigor affects permanents that “enter the battlefield with” a certain number of counters. For example, if a creature would normally enter the battlefield with three +1/+1 counters on it, it will enter with six +1/+1 counters on it.').
card_ruling('primal vigor', '2013-10-17', 'If there are two Primal Vigors on the battlefield, the number of tokens or +1/+1 counters is four times the original number. If there are three on the battlefield, the number of tokens or +1/+1 counters is eight times the original number, and so on.').

card_ruling('primalcrux', '2008-08-01', 'This will count the number of symbols in its mana cost while it\'s on the battlefield. The ability functions while it\'s in other zones, but since it isn\'t a permanent while in those zones it won\'t count itself.').
card_ruling('primalcrux', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('primalcrux', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').

card_ruling('prime speaker zegana', '2013-01-24', 'The value of X is the greatest power among creatures you control as Prime Speaker Zegana enters the battlefield. If you control no creatures at that time, X will be 0.').
card_ruling('prime speaker zegana', '2013-01-24', 'The number of cards you draw equals Prime Speaker Zegana\'s power when the last ability resolves.').
card_ruling('prime speaker zegana', '2013-01-24', 'If Prime Speaker Zegana enters the battlefield at the same time as another creature you control, you won\'t consider that creature when determining the greatest power among creatures you control.').

card_ruling('primeval bounty', '2013-07-01', 'The last ability triggers whenever you play a land and also whenever a land enters the battlefield under your control for some other reason, such as the ability of Into the Wilds.').

card_ruling('primeval titan', '2010-08-15', 'You may find any land cards, not just basic land cards.').

card_ruling('primitive etchings', '2004-10-04', 'If you have more than one of these cards on the battlefield, only one card is revealed (both of them reveal the same card). Both of them will trigger if this card is a creature and thereby allow you to draw 2 cards.').

card_ruling('primitive justice', '2004-10-04', 'can\'t target the same artifact more than once, even with different parts of the spell.').

card_ruling('primoc escapee', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('primordial hydra', '2011-09-22', 'Consider only the number of +1/+1 counters on Primordial Hydra when determining if it has trample, not its power and toughness. For example, a Primordial Hydra with six +1/+1 counters on it that\'s been the target of Titanic Growth (giving it +4/+4) would not have trample.').

card_ruling('primordial ooze', '2004-10-04', 'It becomes tapped even if the damage is prevented.').

card_ruling('prince of thralls', '2008-10-01', 'It doesn\'t matter whose graveyard the permanent is put into as long as an opponent controlled it when it last existed on the battlefield.').
card_ruling('prince of thralls', '2008-10-01', 'If the ability triggers and your opponent chooses not to pay 3 life, you must return the card to the battlefield, even if you don\'t want to.').
card_ruling('prince of thralls', '2008-10-01', 'If the permanent that caused the ability to trigger leaves the graveyard before the ability resolves (because it\'s a token and it ceased to exist, or because a spell or ability removed it from the graveyard), the ability will still resolve. The opponent will have the option to pay 3 life. But whether or not that player pays the life, nothing will be returned to the battlefield.').

card_ruling('prism array', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('prism array', '2015-08-25', '').
card_ruling('prism array', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('prism array', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('prism array', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('prismatic omen', '2008-05-01', 'Each land you control will have the land types Plains, Island, Swamp, Mountain, and Forest. They\'ll also have the mana ability of each basic land type (for example, Forests can tap to produce {G}). They\'ll still have their other subtypes and abilities.').
card_ruling('prismatic omen', '2008-05-01', 'Giving a land extra basic land types doesn\'t change its name or whether it\'s legendary or basic.').

card_ruling('prismwake merrow', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('prismwake merrow', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('prismwake merrow', '2008-05-01', 'Colorless is not a color.').
card_ruling('prismwake merrow', '2008-05-01', 'You can choose any single color or any combination of more than one color. You can\'t choose colorless.').
card_ruling('prismwake merrow', '2008-05-01', 'This ability won\'t make an artifact stop being an artifact. It\'ll just be a colorful artifact.').

card_ruling('prison barricade', '2005-08-01', 'If the Prison Barricade is kicked, it enters the battlefield with the ability to attack as if it did not have Defender and will keep this ability until it leaves the battlefield.').

card_ruling('prison term', '2008-05-01', 'The last ability works only if Prison Term is already on the battlefield. You may move it from the creature it\'s currently enchanting onto the new creature.').
card_ruling('prison term', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('pristine talisman', '2011-06-01', 'Pristine Talisman has a mana ability. Its ability doesn\'t use the stack and can\'t be responded to.').

card_ruling('private research', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('prized unicorn', '2009-10-01', 'Prized Unicorn doesn\'t give a creature the ability to block it. It just forces those creatures that are already able to block it to do so. For example, it can\'t force a creature that\'s tapped or affected by a spell or ability that says it can\'t block to block it. If there\'s a cost associated with having a creature block, its controller isn\'t forced to pay that cost, so the creature doesn\'t have to block Prized Unicorn in that case either.').
card_ruling('prized unicorn', '2009-10-01', 'If a creature the defending player controls can\'t block Prized Unicorn, it can block any attacking creature, or not block at all.').
card_ruling('prized unicorn', '2009-10-01', 'If two Prized Unicorns are attacking, each creature the defending player controls can block either one of them. (The defending player chooses.) A creature that can block multiple creatures, like Palace Guard, must block both of them.').

card_ruling('processor assault', '2015-08-25', 'You must put exactly one card an opponent owns from exile into that player’s graveyard to cast Processor Assault. You can’t cast it without doing so, and you can’t put multiple exiled cards into their owners’ graveyards this way.').
card_ruling('processor assault', '2015-08-25', 'Players can respond to Processor Assault only after it’s been cast and all of its costs have been paid. No one can try to remove the card from exile to stop you from casting the spell.').
card_ruling('processor assault', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('processor assault', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('processor assault', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('processor assault', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('processor assault', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('processor assault', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('processor assault', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('processor assault', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('processor assault', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('prodigal pyromancer', '2007-02-01', 'This is the timeshifted version of Prodigal Sorcerer.').

card_ruling('profane command', '2007-10-01', 'The value chosen for X applies to each X in the spell\'s effect. You pay {X} only once.').

card_ruling('profane memento', '2014-07-18', 'Token creatures dying won’t cause Profane Memento’s ability to trigger.').
card_ruling('profane memento', '2014-07-18', 'Profane Memento’s ability will trigger when a nontoken creature you control but an opponent owns dies.').
card_ruling('profane memento', '2014-07-18', 'If Profane Memento is put into your graveyard at the same time as a creature card is put into an opponent’s graveyard, its ability won’t trigger.').

card_ruling('profaner of the dead', '2015-02-25', 'Use the toughness of the exploited creature as it last existed on the battlefield to determine which creatures to return to their owners’ hands.').
card_ruling('profaner of the dead', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('profaner of the dead', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('profaner of the dead', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('profaner of the dead', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('profaner of the dead', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('profit', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('profit', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('profit', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('profit', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('profit', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('profit', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('profit', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('profit', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('profit', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('profit', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('profit', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('profound journey', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('profound journey', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('profound journey', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('profound journey', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('profound journey', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('profound journey', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('profound journey', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('profound journey', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('progenitor mimic', '2013-04-15', 'Progenitor Mimic copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('progenitor mimic', '2013-04-15', 'The original Progenitor Mimic will create tokens, but those token copies will not.').
card_ruling('progenitor mimic', '2013-04-15', 'If you choose to have Progenitor Mimic enter the battlefield as a copy of a creature, the triggered ability it gains will become part of its copiable values. For example, suppose Progenitor Mimic enters the battlefield as a copy of Runeclaw Bear, a 2/2 green Bear creature with mana cost {1}{G}. The resulting object is a 2/2 green Bear creature named Runeclaw Bear with mana cost {1}{G} and with “At the beginning of your upkeep, if this creature isn\'t a token, put a token onto the battlefield that\'s a copy of this creature.” If another Progenitor Mimic enters the battlefield as a copy of that creature, it will be a Runeclaw Bear with two instances of the triggered ability.').
card_ruling('progenitor mimic', '2013-04-15', 'If the chosen creature is a token, Progenitor Mimic copies the original characteristics of that token as stated by the effect that put it onto the battlefield. Copying a token doesn\'t make Progenitor Mimic become a token.').
card_ruling('progenitor mimic', '2013-04-15', 'Any enters-the-battlefield abilities of the copied creature will trigger when Progenitor Mimic enters the battlefield. Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the chosen creature will also work.').
card_ruling('progenitor mimic', '2013-04-15', 'If the chosen creature has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('progenitor mimic', '2013-04-15', 'If Progenitor Mimic somehow enters the battlefield at the same time as another creature, it can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('progenitor mimic', '2013-04-15', 'You can choose not to copy anything. In that case, Progenitor Mimic enters the battlefield as a 0/0 Shapeshifter creature and is probably put into the graveyard immediately.').

card_ruling('progenitus', '2009-02-01', '\"Protection from everything\" means the following: Progenitus can\'t be blocked, Progenitus can\'t be enchanted or equipped, Progenitus can\'t be the target of spells or abilities, and all damage that would be dealt to Progenitus is prevented.').
card_ruling('progenitus', '2009-02-01', 'Progenitus can still be affected by effects that don\'t target it or deal damage to it (such as Day of Judgment).').

card_ruling('prognostic sphinx', '2013-09-15', 'You can activate Prognostic Sphinx’s ability even if it’s already tapped.').
card_ruling('prognostic sphinx', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('prognostic sphinx', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('prognostic sphinx', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('prognostic sphinx', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('promise of bunrei', '2005-06-01', 'Promise of Bunrei must be sacrificed in order to get tokens. If multiple creatures are put into a graveyard at the same time, only the first triggered ability to resolve causes Promise of Bunrei to be sacrificed and therefore put tokens onto the battlefield.').

card_ruling('promise of power', '2004-10-04', 'The power/toughness of the token is set when this spell resolves.').
card_ruling('promise of power', '2004-12-01', 'The power and toughness of the Demon token are set when Promise of Power resolves. They\'re unaffected if the number of cards in your hand changes later.').
card_ruling('promise of power', '2004-12-01', 'If you pay the entwine cost, you draw five cards, then lose five life, then put the Demon token onto the battlefield. The five cards you draw count toward the Demon\'s power and toughness.').

card_ruling('promised kannushi', '2005-06-01', 'Yes, Soulshift 7. Promised Kannushi can return any Spirit with converted mana cost of 7 or less. However, Promised Kannushi is a Human Druid, not a Spirit.').

card_ruling('propaganda', '2004-10-04', 'The payment is made during the declare attackers step at the same time you are declaring the attacker.').
card_ruling('propaganda', '2004-10-04', 'If there is more than one Propaganda on the battlefield, the cost is cumulative.').
card_ruling('propaganda', '2004-10-04', 'If there are multiple attacks in a turn, then you have to pay for each attack.').
card_ruling('propaganda', '2004-10-04', 'Paying this cost is not an instant or any other kind of ability, it is an additional cost on the declaration of the attacker.').
card_ruling('propaganda', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').
card_ruling('propaganda', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('prophecy', '2004-10-04', 'The shuffling and drawing of a card next upkeep happens even if the card is not a land.').

card_ruling('prophet of kruphix', '2013-09-15', 'Creatures and lands you control untap at the same time as the active player’s permanents. You can’t choose to not untap them at that time.').
card_ruling('prophet of kruphix', '2013-09-15', 'Effects that state a creature or land you control doesn’t untap during your untap step won’t apply during another player’s untap step.').
card_ruling('prophet of kruphix', '2013-09-15', 'Controlling more than one Prophet of Kruphix doesn’t allow you to untap any creature or land more than once during a single untap step.').
card_ruling('prophet of kruphix', '2013-09-15', 'If you’re casting a creature spell with bestow, whether or not it has flash will depend on if you’re casting it as a creature or as an Aura. Prophet of Kruphix won’t give flash to a spell with bestow you’re casting as an Aura.').
card_ruling('prophet of kruphix', '2013-09-15', 'The last ability applies to creature cards in any zone, provided something is allowing you to cast them. For example, if you control a Zombie, you could cast Gravecrawler (a creature with “You may cast Gravecrawler from your graveyard as long as you control a Zombie”) from your graveyard as though it had flash.').

card_ruling('prophetic flamespeaker', '2014-04-26', 'If Prophetic Flamespeaker deals combat damage to a player multiple times in a turn (if it wasn’t blocked, for example), its triggered ability will trigger each time.').
card_ruling('prophetic flamespeaker', '2014-04-26', 'The card is exiled face up. Playing it follows the normal rules for playing that card. You must pay its costs, and you must follow all applicable timing rules. For example, if it’s a creature card, you can cast it only during your main phase while the stack is empty.').
card_ruling('prophetic flamespeaker', '2014-04-26', 'If you exile a land card, you may play that land only if you have any available land plays. Normally, this means you can play the land only if you haven’t played a land yet that turn.').

card_ruling('prosperity', '2004-10-04', 'If both players run out of cards during this effect, the game is a draw.').

card_ruling('prossh, skyraider of kher', '2014-02-01', 'The amount of mana you spent to cast this creature is usually equal to its converted mana cost. However, you also include any additional costs you pay, including the cost imposed for casting your commander from the command zone.').
card_ruling('prossh, skyraider of kher', '2014-02-01', 'You can’t choose to pay extra mana to cast a creature spell unless something instructs you to.').
card_ruling('prossh, skyraider of kher', '2014-02-01', 'The first ability triggers when you cast Prossh, not when it enters the battlefield. This means that it will resolve before Prossh, so any Kobold tokens it creates will be put onto the battlefield before Prossh. If Prossh enters the battlefield without being cast, the ability will not trigger.').

card_ruling('protean hydra', '2009-10-01', 'All damage that would be dealt to Protean Hydra is prevented, even if it doesn\'t have that many +1/+1 counters on it. This is true even if Protean Hydra has no +1/+1 counters on it at all (and is able to remain on the battlefield because some other effect is boosting its toughness). If the amount of damage that would be dealt to Protean Hydra is greater than the number of +1/+1 counters on it, all the +1/+1 counters on Protean Hydra are removed from it.').
card_ruling('protean hydra', '2009-10-01', 'If unpreventable damage is dealt to Protean Hydra, the Hydra\'s second ability will try to prevent it and fail (meaning that damage has its normal results), and it will also remove that many +1/+1 counters from Protean Hydra.').
card_ruling('protean hydra', '2009-10-01', 'Protean Hydra\'s last ability triggers whenever a +1/+1 counter is removed from it for any reason, not just when a +1/+1 counter is removed by its second ability.').
card_ruling('protean hydra', '2009-10-01', 'If multiple +1/+1 counters are removed from Protean Hydra at once, its last ability will trigger that many times.').
card_ruling('protean hydra', '2009-10-01', 'If a -1/-1 counter is put on Protean Hydra while it has +1/+1 counters on it, that -1/-1 counter and a +1/+1 counter will be removed from it as a state-based action. This will cause Protean Hydra\'s last ability to trigger.').
card_ruling('protean hydra', '2009-10-01', 'If any +1/+1 counters are removed from Protean Hydra during the end step, its last ability will trigger and set up a delayed triggered ability that will trigger at the beginning of the next end step (which is near the end of the following turn).').

card_ruling('protect', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('protect', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('protect', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('protect', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('protect', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('protect', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('protect', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('protect', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('protect', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('protect', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('protect', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('protomatter powder', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('protomatter powder', '2008-10-01', 'You can\'t sacrifice Protomatter Powder to return itself to the battlefield. First you choose the target (at which time it\'s still on the battlefield), then you pay the costs (at which time you sacrifice it).').

card_ruling('prototype portal', '2011-01-01', 'If Prototype Portal has left the battlefield by the time its second ability resolves, you\'ll still put a token onto the battlefield that\'s a copy of the exiled card. On the other hand, if Prototype Portal is still on the battlefield at this time but there is no exiled card (because, perhaps, Riftsweeper\'s ability caused the exiled card to be put into its owner\'s library), no token is created.').
card_ruling('prototype portal', '2011-01-01', 'You don\'t choose the value of {X}. Rather, the value of X is defined by the activated ability.').
card_ruling('prototype portal', '2011-01-01', 'If the exiled card has {X} in its mana cost (such as Chalice of the Void), that X is considered to be 0. The {X} in Prototype Portal\'s activation cost takes this into account, though it may be greater than 0 if the exiled card has other mana symbols in its mana cost (such as Riptide Replicator).').
card_ruling('prototype portal', '2011-01-01', 'You may not activate the second ability if no card has been exiled with Prototype Portal. In that case, the value of {X} is undefined and can\'t be paid.').
card_ruling('prototype portal', '2011-01-01', 'Any enters-the-battlefield abilities of the exiled card will trigger when the token is put onto the battlefield. Any \"as [this permanent] enters the battlefield\" or \"[this permanent] enters the battlefield with\" abilities of the exiled card will also work.').

card_ruling('provoke', '2004-10-04', 'You can target an untapped creature.').

card_ruling('prowess of the fair', '2007-10-01', 'If Prowess of the Fair and another nontoken Elf are put into your graveyard simultaneously (by Akroma\'s Vengeance, for example), the other Elf will cause Prowess of the Fair\'s ability to trigger.').

card_ruling('psionic entity', '2004-10-04', 'The artist is actually Justin Hampton.').

card_ruling('psionic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('psionic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('psychic battle', '2004-10-04', 'With the Split Cards, use the higher of the two costs on the card to compare with the other card.').
card_ruling('psychic battle', '2004-10-04', 'You can change all, some, or none of the targets.').
card_ruling('psychic battle', '2004-10-04', 'You can\'t change the number of targets, choose an illegal target, or make something get targeted more than once.').
card_ruling('psychic battle', '2004-10-04', 'An empty library will have no card to reveal, so a player with an empty library has no card to compare. The effect looks for the highest cost card of the cards that actually are revealed.').

card_ruling('psychic miasma', '2011-01-01', 'If a land card is discarded this way, Psychic Miasma is returned to its owner\'s hand from the stack. It finishes resolving, but it isn\'t put into the graveyard.').

card_ruling('psychic possession', '2006-05-01', 'Enchanting an opponent works very much like enchanting a permanent. The Aura spell targets the opponent. When it resolves, it enters the battlefield \"attached\" to that player. Once it\'s on the battlefield, it no longer targets that player.').
card_ruling('psychic possession', '2006-05-01', 'Psychic Possession\'s controller, not the enchanted player, skips his or her draw step.').
card_ruling('psychic possession', '2006-05-01', 'If Psychic Possession\'s controller ever happens to be the player it\'s enchanting, Psychic Possession will be put into its owner\'s graveyard as a state-based action.').
card_ruling('psychic possession', '2006-05-01', 'An Aura with enchant opponent can\'t be attached to a permanent.').
card_ruling('psychic possession', '2006-05-01', 'In a multiplayer game, if the enchanted opponent leaves the game, Psychic Possession will be put into its owner\'s graveyard as a state-based action. If Psychic Possession\'s owner leaves the game, Psychic Possession immediately leaves the game; this is not a state-based action.').

card_ruling('psychic puppetry', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('psychic puppetry', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('psychic puppetry', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('psychic puppetry', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('psychic purge', '2004-10-04', 'The loss of life can\'t be prevented by any means. It is not damage.').
card_ruling('psychic purge', '2004-10-04', 'Discarding as a cost to cast a spell will not trigger the ability. Only discarding as an effect will trigger the ability.').

card_ruling('psychic rebuttal', '2015-06-22', 'The target instant or sorcery spell can have other targets as long as one of them is you.').
card_ruling('psychic rebuttal', '2015-06-22', 'If the spell mastery ability applies, you’ll create a copy of the instant or sorcery spell only if Psychic Rebuttal counters that spell. If that spell can’t be countered by spells or abilities, you won’t get a copy.').
card_ruling('psychic rebuttal', '2015-06-22', 'If Psychic Rebuttal creates a copy of the spell, you control the copy. That copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('psychic rebuttal', '2015-06-22', 'The copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('psychic rebuttal', '2015-06-22', 'If the copied spell is modal (that is, it says “Choose one –” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('psychic rebuttal', '2015-06-22', 'If the copied spell has an X whose value was determined as it was cast, the copy has the same value of X.').
card_ruling('psychic rebuttal', '2015-06-22', 'You can’t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if a player sacrifices a 3/3 creature to cast Fling, and you copy it, the copy of Fling will also deal 3 damage to its target.').
card_ruling('psychic rebuttal', '2015-06-22', 'If the copy says that it affects “you,” it affects the controller of the copy, not the controller of the original spell. Similarly, if the copy says that it affects an “opponent,” it affects an opponent of the copy’s controller, not an opponent of the original spell’s controller.').
card_ruling('psychic rebuttal', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('psychic spear', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('psychic spiral', '2012-10-01', 'Psychic Spiral won\'t be shuffled into your library, and it won\'t be counted when determining how many cards the target player puts into his or her graveyard. It will be put into your graveyard after you follow its instructions.').
card_ruling('psychic spiral', '2012-10-01', 'If you cast Psychic Spiral when there are no cards in your graveyard, you\'ll still shuffle your library.').

card_ruling('psychic strike', '2013-01-24', 'If the target spell is an illegal target when Psychic Strike tries to resolve, Psychic Strike will be countered and none of its effects will happen. The target\'s controller won\'t put any cards from the top of his or her library into his or her graveyard.').
card_ruling('psychic strike', '2013-01-24', 'You may target a spell that can\'t be countered. When Psychic Strike resolves, the target spell will be unaffected, but its controller will still put the top two cards of his or her library into his or her graveyard.').

card_ruling('psychic surgery', '2011-06-01', 'You look at the top two cards of that library as the triggered ability resolves. That happens after your opponent is done shuffling.').
card_ruling('psychic surgery', '2011-06-01', 'If your opponent\'s library has one card in it and he or she is instructed to shuffle that library, Psychic Surgery will trigger. You may\' exile the one card in that library.').
card_ruling('psychic surgery', '2011-06-01', 'In most cases, Green Sun\'s Zenith instructs you to shuffle your library twice. Although shuffling that library only once is an acceptable shortcut, Psychic Surgery\'s ability will still trigger twice.').

card_ruling('psychic theft', '2004-10-04', 'The card is revealed to all players while it is exiled.').
card_ruling('psychic theft', '2004-10-04', 'You can cast the instant or sorcery during the End step after you put the \"at the beginning of the end step\" trigger on the stack, as long as you do so before that trigger resolves.').
card_ruling('psychic theft', '2004-10-04', 'The card may only be cast once. Once it is announced and put on the stack, the card is no longer exiled and the effect of Psychic Theft ends.').
card_ruling('psychic theft', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('psychic transfer', '2008-08-01', 'If one of the players has a negative life total, you use their actual life total for determining the difference. If that difference is five or less, then the other player receives the negative life total.').
card_ruling('psychic transfer', '2011-01-01', 'If an effect says that a player can\'t lose life, that player can\'t exchange life totals with a player who has a lower life total; in that case, the exchange won\'t happen.').

card_ruling('psychic venom', '2004-10-04', 'Whenever the land is tapped for any reason, the ability triggers.').

card_ruling('psychic vortex', '2008-04-01', 'Psychic Vortex\'s cumulative upkeep ability has you draw cards as a cost. If you choose to do so, and some or all of those draws are replaced by replacement effects, you are still considered to have paid the cumulative upkeep cost.').
card_ruling('psychic vortex', '2008-04-01', 'You may choose to draw cards to pay the cumulative upkeep cost even if the number of cards you\'d have to draw exceeds the number of cards in your library. If enough of those draws are replaced by replacement effects so you don\'t actually attempt to draw a card with an empty library, everything\'s fine. However, if you do attempt to draw a card with an empty library this way, Psychic Vortex will remain on the battlefield but you\'ll lose the game as a state-based action as soon as the cumulative upkeep ability finishes resolving.').

card_ruling('psychogenic probe', '2009-02-01', 'This ability will trigger even if the player\'s library is empty at the time he or she is supposed to shuffle it.').

card_ruling('psychosis crawler', '2011-06-01', 'If an effect causes you to draw multiple cards, Psychosis Crawler will trigger that many times.').

card_ruling('public execution', '2012-07-01', 'Only creatures controlled by the player when Public Execution resolves will get -2/-0. Creatures that enter the battlefield under that player\'s control or that the player gains control of later in the turn are unaffected.').
card_ruling('public execution', '2012-07-01', 'Public Execution targets only the creature it destroys. If that creature is an illegal target when Public Execution tries to resolve, it will be countered and none of its effects will happen. No creature will get -2/-0.').
card_ruling('public execution', '2013-07-01', 'If the target creature regenerates or has indestructible, each other creature controlled by that creature\'s controller will still get -2/-0.').

card_ruling('puca\'s mischief', '2008-05-01', 'You choose two targets such that as a set, all of the targeting conditions are met. That means, for example, that if you control a nonland permanent whose converted mana cost is lower than any of your opponents\' nonland permanents, you can\'t choose it as a target. (If you did, you\'d be unable to choose a second target.)').
card_ruling('puca\'s mischief', '2008-05-01', 'Even if you don\'t plan on exchanging control of any permanents, if you can choose two legal targets when the ability triggers, you must do so. If you can\'t, then you don\'t choose any targets.').
card_ruling('puca\'s mischief', '2008-05-01', 'If either one of the targets becomes illegal (because it leaves the battlefield or for any other reason) by the time the ability resolves, the exchange won\'t happen. If both targets become illegal, the ability is countered.').

card_ruling('pull from eternity', '2006-09-25', 'Pull from Eternity targets a card in the Exile zone. A card is only face down in the Exile zone if the effect that put it there specifically says so.').
card_ruling('pull from eternity', '2006-09-25', 'If Pull from Eternity affects a suspended card, the card loses its time counters and is no longer suspended. (This doesn\'t trigger the last ability of suspend.)').
card_ruling('pull from eternity', '2006-09-25', 'If Pull from Eternity affects a card that\'s haunting a creature, the haunt effect ends.').

card_ruling('pulling teeth', '2008-04-01', 'The opponent you clash with doesn\'t have to be the player targeted by Pulling Teeth.').

card_ruling('pulmonic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('pulmonic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('pulse of llanowar', '2004-10-04', 'This does not change the amount of mana gained. It just makes all the mana generated by the basic land be of a single color of your choice.').
card_ruling('pulse of llanowar', '2004-10-04', 'You can choose a different color each time a basic land is tapped.').
card_ruling('pulse of llanowar', '2004-10-04', 'You can choose the same color or mana the land would normally produce.').

card_ruling('pulsemage advocate', '2004-10-04', 'You can\'t activate this ability unless a single opponent has at least three cards in his or her graveyard to target and you have a creature card in your graveyard to target.').

card_ruling('puncture blast', '2008-08-01', 'This is the only instant or sorcery spell that has wither. It works like anything else with wither: If it deals damage to a creature, the damage results in -1/-1 counters; if it deals damage to a player or planeswalker, the damage behaves normally.').

card_ruling('puncture bolt', '2008-05-01', 'Puncture Bolt will resolve fully (and give the creature a -1/-1 counter) even if the 1 damage would be enough to destroy it. (This would matter if a 3/3 creature with persist had already been dealt 2 damage, for example.)').
card_ruling('puncture bolt', '2008-05-01', 'If you target a creature with 2 toughness, Puncture Bolt deals 1 damage to it, then puts a -1/-1 counter on it, which reduces it to 1 toughness. The creature will be destroyed as a result of lethal damage. Regeneration could save it.').
card_ruling('puncture bolt', '2008-05-01', 'If you target a creature with 1 toughness, Puncture Bolt deals 1 damage to it, then puts a -1/-1 counter on it, which reduces it to 0 toughness. The creature will be put into its owner\'s graveyard as a result of having 0 toughness. Regeneration can\'t save it.').

card_ruling('puncturing light', '2010-06-15', 'The power of the targeted creature is checked both as you target it and as Puncturing Light resolves.').
card_ruling('puncturing light', '2010-06-15', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step.').
card_ruling('puncturing light', '2010-06-15', 'A \"blocking creature\" is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures that it was blocking are no longer on the battlefield or have otherwise left combat by then.').

card_ruling('punish ignorance', '2008-10-01', 'You may target a spell that \"can\'t be countered\" with Punish Ignorance. If you do, the first part of Punish Ignorance\'s effect won\'t do anything, but the life-loss and life-gain effects will still work.').

card_ruling('punish the enemy', '2013-04-15', 'You must be able to target a player and a creature in order to cast Punish the Enemy.').
card_ruling('punish the enemy', '2013-04-15', 'If one of Punish the Enemy\'s targets is illegal when it tries to resolve, it will still deal damage to the remaining legal target.').

card_ruling('punishing fire', '2009-10-01', 'Punishing Fire\'s triggered ability triggers only if it\'s already in your graveyard at the time an opponent gains life. For example, you can cast it in response to a spell or ability that will cause an opponent to gain life. In that case, Punishing Fire will resolve first, so it will be in the graveyard by the time the opponent gains life. However, if you wait until an opponent actually gains life and then cast Punishing Fire, you won\'t be able to return it to your hand at that time.').
card_ruling('punishing fire', '2009-10-01', 'You choose whether to pay {R} as the triggered ability resolves.').
card_ruling('punishing fire', '2009-10-01', 'If Punishing Fire has left your graveyard by the time its triggered ability resolves, you may still pay {R}, but you won\'t return it to your hand. This is true even if Punishing Fire has left your graveyard and returned to it by the time its triggered ability resolves.').

card_ruling('puppet conjurer', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('puppet conjurer', '2008-10-01', 'The Homunculus you sacrifice due to the second ability isn\'t limited to being a Homunculus token put onto the battlefield by Puppet Conjurer.').
card_ruling('puppet conjurer', '2008-10-01', 'If you control no Homunculi when the second ability resolves, nothing happens. There\'s no penalty for failing to sacrifice a Homunculus.').

card_ruling('puppet master', '2008-05-01', 'You only get the opportunity to pay {U}{U}{U} (and thus to return Puppet Master to its owner\'s hand) if the creature card actually gets returned to its owner\'s hand. If it isn\'t (for instance, if it was removed from the graveyard prior to this ability resolving), you don\'t get the option to pay.').

card_ruling('puppeteer', '2004-10-04', 'You decide on resolution whether or not to tap or untap the creature.').

card_ruling('puppeteer clique', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('puppeteer clique', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('puppeteer clique', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('puppeteer clique', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('puppeteer clique', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('puppeteer clique', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('puppeteer clique', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('pure intentions', '2005-06-01', 'The first part of Pure Intentions is the effect of casting Pure Intentions. The second part is a triggered ability that triggers whenever an opponent\'s spell or ability causes a player to discard Pure Intentions.').
card_ruling('pure intentions', '2005-06-01', 'Neither part cares who decides which card to discard -- only the controller of the spell or ability matters.').
card_ruling('pure intentions', '2005-06-01', 'Discarding Pure Intentions to your own spell or ability doesn\'t trigger its abilities.').
card_ruling('pure intentions', '2005-06-01', 'If a spell or ability an opponent controls causes a player to discard multiple cards, such as Waking Nightmare or Three Tragedies, Pure Intentions doesn\'t return to hand until its triggered ability has resolved. A player can\'t discard the same Pure Intentions twice for the same discard effect.').

card_ruling('puresight merrow', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('puresight merrow', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('puresight merrow', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('puresteel paladin', '2011-06-01', 'You may still activate the Equipment\'s other equip ability if you wish.').
card_ruling('puresteel paladin', '2011-06-01', 'Once the equip {0} ability is activated, destroying Puresteel Paladin or causing its controller to control fewer than three artifacts won\'t stop it from resolving.').

card_ruling('purgatory', '2004-10-04', 'If an opponent takes control of this card and pays for the upkeep ability, the creatures will enter the battlefield under their control even if you exiled them.').
card_ruling('purgatory', '2004-10-04', 'If destroyed at the same time some creatures are destroyed, the destroyed creatures are exiled and stay exiled.').
card_ruling('purgatory', '2008-04-01', 'This has been restored to a triggered ability that triggers when a nontoken creature is put into a graveyard from the battlefield, rather than a replacement effect. The creature does go to the graveyard, and will trigger any other abilities that look for this event.').
card_ruling('purgatory', '2008-08-01', 'If this card changes controllers between putting the upkeep triggered ability on the stack and resolving it, the creature is put onto the battlefield under the control of the player who controlled this card when the ability was put on the stack.').

card_ruling('purge the profane', '2013-01-24', 'If the opponent has fewer than two cards in his or her hand, he or she will discard them. You\'ll still gain 2 life, no matter how many cards were discarded.').

card_ruling('purging scythe', '2004-10-04', 'Does not target the creature it damages.').

card_ruling('purify the grave', '2011-09-22', 'Purify the Grave can\'t be used in response to a spell being cast with flashback or a spell that requires cards to exiled from a graveyard as an additional cost to try and counter the spell or prevent it from being cast.').
card_ruling('purify the grave', '2011-09-22', 'If it\'s your turn, and you cast a spell with flashback from your hand, you\'ll have priority to cast that spell again using flashback before any other player has priority to cast Purify the Grave in order to remove that card from your graveyard.').

card_ruling('purity', '2007-10-01', 'The last ability triggers when the Incarnation is put into its owner\'s graveyard from any zone, not just from on the battlefield.').
card_ruling('purity', '2007-10-01', 'Although this ability triggers when the Incarnation is put into a graveyard from the battlefield, it doesn\'t *specifically* trigger on leaving the battlefield, so it doesn\'t behave like other leaves-the-battlefield abilities. The ability will trigger from the graveyard.').
card_ruling('purity', '2007-10-01', 'If the Incarnation had lost this ability while on the battlefield (due to Lignify, for example) and then was destroyed, the ability would still trigger and it would get shuffled into its owner\'s library. However, if the Incarnation lost this ability when it was put into the graveyard (due to Yixlid Jailer, for example), the ability wouldn\'t trigger and the Incarnation would remain in the graveyard.').
card_ruling('purity', '2007-10-01', 'If the Incarnation is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('purphoros\'s emissary', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('purphoros\'s emissary', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('purphoros, god of the forge', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('purphoros, god of the forge', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('purphoros, god of the forge', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').

card_ruling('pursuit of flight', '2012-10-01', 'Only the controller of the enchanted creature can activate the ability that gives it flying.').

card_ruling('put away', '2008-05-01', 'If you choose to target a card in your graveyard, Put Away will have two targets. If either one of the targets becomes illegal, Put Away will continue to resolve and affect the other target.').
card_ruling('put away', '2008-05-01', 'If the targeted spell can\'t be countered, that spell will remain on the stack. Put Away will continue to resolve. If you targeted a card in your graveyard, you still get to shuffle it into your library.').
card_ruling('put away', '2008-05-01', 'You must choose all the targets at the time you cast Put Away. You can\'t use it to counter your own spell and then shuffle that card into your library because at the time you choose targets, the spell isn\'t in your graveyard yet.').

card_ruling('putrefax', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('putrefax', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('putrefax', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('putrefax', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('putrefax', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('putrefax', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('putrefy', '2006-02-01', 'This spell isn\'t Modal. When it resolves, it will destroy the target if it\'s a creature or an artifact, even if it changed from one to the other between targeting and resolution.').

card_ruling('putrid cyclops', '2007-05-01', 'If a split card is revealed, the -X/-X effect is applied twice: once for each side. For example, if Dead/Gone is revealed, Putrid Cyclops gets -1/-1 and -3/-3 until end of turn.').

card_ruling('putrid raptor', '2004-10-04', 'You can use a text-changing effect that changes creature types on this card while is it face down to change Zombie to something else. This will change the type of card you have to discard to turn it face up.').

card_ruling('pygmy hippo', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').
card_ruling('pygmy hippo', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('pyreheart wolf', '2011-01-22', 'Pyreheart Wolf\'s first triggered ability also affects creatures that weren\'t on the battlefield when it resolved but enter the battlefield later that turn. While this normally will have little impact, it could matter if those creatures enter the battlefield attacking or if there are multiple combat phases that turn.').

card_ruling('pyretic ritual', '2010-08-15', 'Pyretic Ritual is not a mana ability and doesn\'t behave like one. You cast it as a spell. It uses the stack. It can be responded to, and it can be countered.').
card_ruling('pyretic ritual', '2010-08-15', 'In the same vein, you can cast Pyretic Ritual only at a time when you could cast any other instant, not at a time when you could activate a mana ability. For example, you can\'t cast Pyretic Ritual in the midst of casting another spell; you\'d need to cast Pyretic Ritual first.').

card_ruling('pyrewild shaman', '2013-04-15', 'Pyrewild Shaman\'s last ability triggers only once for each time combat damage is dealt, no matter how many creatures are dealing damage at that time.').
card_ruling('pyrewild shaman', '2013-04-15', 'Pyrewild Shaman\'s ability can trigger during each combat damage step. For example, if a creature with first strike deals combat damage, you can return Pyrewild Shaman to your hand, and discard it using its bloodrush ability to pump up an attacking creature without first strike. Then, when that creature deals combat damage, you can return Pyrewild Shaman to your hand again.').
card_ruling('pyrewild shaman', '2013-04-15', 'Pyrewild Shaman\'s last ability triggers only if it\'s in your graveyard when the creatures deal combat damage to a player. It returns only to your hand only if it\'s still in your graveyard when the ability resolves. Notably, if Pyrewild Shaman is dealt lethal damage at the same time that a creature you control deals combat damage to a player, Pyrewild Shaman\'s ability won\'t trigger.').
card_ruling('pyrewild shaman', '2013-04-15', 'If creatures you control deal combat damage to more than one player at the same time (perhaps because it\'s a multiplayer game), Pyrewild Shaman\'s ability will trigger once for each of those players. However, only the first such ability that you pay for will return Pyrewild Shaman to your hand. Even if it returns to your graveyard before the other abilities resolve, it\'s considered a different Pyrewild Shaman than the one whose ability triggered.').

card_ruling('pyroblast', '2004-10-04', 'The decision to counter a spell or destroy a permanent is a decision made on announcement before the target is selected. If the spell is redirected, this mode can\'t be changed, so only targets of the selected type are valid.').
card_ruling('pyroblast', '2007-09-16', 'You can change the text of this spell after announcing it to change the color that it will affect. This works, because the color isn\'t part of the targeting requirement.').
card_ruling('pyroblast', '2007-09-16', 'Note that Pyroblast can target any spell or permanent, not just a blue one. It checks the color of the target only on resolution.').
card_ruling('pyroblast', '2007-09-16', 'If there are no legal targets for either of the modes, you can\'t choose that mode.').

card_ruling('pyroclast consul', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('pyroclast consul', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('pyroclast consul', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('pyroclast consul', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('pyroclast consul', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('pyroclast consul', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('pyroconvergence', '2012-10-01', 'The hybrid spells in the Return to Ravnica set are multicolored, even if you cast them with one color of mana.').
card_ruling('pyroconvergence', '2012-10-01', 'The triggered ability will resolve and Pyroconvergence will deal damage before that spell resolves.').

card_ruling('pyrohemia', '2004-10-04', 'Each activation is considered a new damage effect. An activation can only be 1 point of damage.').
card_ruling('pyrohemia', '2007-02-01', 'This is the timeshifted version of Pestilence.').
card_ruling('pyrohemia', '2011-06-01', 'Note that \"until end of turn\" effects wear off after \"at the beginning of the end step\" triggered abilities, so an artifact that animates until end of turn can keep this on the battlefield.').
card_ruling('pyrohemia', '2011-06-01', 'It will stay on the battlefield if there is a creature that is put into the graveyard during the end step. This is because this ability will not trigger at all if there is at least one creature on the battlefield as the end step begins.').

card_ruling('pyrokinesis', '2004-10-04', 'You can\'t do fractional points of damage.').
card_ruling('pyrokinesis', '2008-10-01', 'The number of targets must be at least 1 and at most 4. You divide the damage as you cast Pyrokinesis, not as it resolves. Each target must be assigned at least 1 damage.').

card_ruling('pyromancer ascension', '2009-10-01', 'If either of Pyromancer Ascension\'s abilities triggers, it will go on the stack on top of the spell that caused it to trigger. The ability will resolve first. If it\'s Pyromancer Ascension\'s second ability that triggered, the copy it creates will also resolve before the original spell. The copy is created even if the original spell has been countered.').
card_ruling('pyromancer ascension', '2009-10-01', 'If Pyromancer Ascension\'s first ability triggers, you\'ll put a quest counter on it even if the card in your graveyard leaves your graveyard by the time the ability resolves.').
card_ruling('pyromancer ascension', '2009-10-01', 'If you cast an instant or sorcery spell from your graveyard (due to an ability such as retrace, for example), Pyromancer Ascension\'s first ability won\'t trigger unless another card with the same name is in your graveyard.').
card_ruling('pyromancer ascension', '2009-10-01', 'The second ability triggers only if Pyromancer Ascension already has two quest counters on it at the time you cast an instant or sorcery spell. This means a spell can\'t both cause the second counter to be put on Pyromancer Ascension and become copied.').
card_ruling('pyromancer ascension', '2009-10-01', 'If you copy a spell, the copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them.').
card_ruling('pyromancer ascension', '2009-10-01', 'You may copy an instant or sorcery spell that has no targets.').
card_ruling('pyromancer ascension', '2009-10-01', 'If the original spell was kicked, the copy is kicked too. Other decisions made for the original spell, such as the value of X or the mode, are also copied.').
card_ruling('pyromancer ascension', '2011-09-22', 'Both of these abilities trigger when a spell is cast, not when it resolves. This means that their effects will occur even if the Ascension is destroyed in response to such a spell being cast.').

card_ruling('pyromancer\'s goggles', '2015-06-22', 'The mana produced by Pyromancer’s Goggles can be spent on anything, not just a red instant or sorcery spell.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'Any red instant or sorcery spell you spend the mana on will be copied, not just one that requires targets.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'The delayed triggered ability will trigger whether Pyromancer’s Goggles is still on the battlefield or not.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'If more than one red mana produced by a Pyromancer’s Goggles is spent to cast a single red instant or sorcery spell, the delayed triggered ability associated with each mana spent will trigger. That many copies will be created. It doesn’t matter if this red mana was produced by one Pyromancer’s Goggles or by multiple Pyromancer’s Goggles.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'If a copy is created, you control the copy. That copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'The copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'If the copied spell is modal (that is, it says “Choose one –” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'If the copied spell has an X whose value was determined as it was cast, the copy has the same value of X.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'You can’t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if you sacrifice a 3/3 creature to cast Fling, and you copy it, the copy of Fling will also deal 3 damage to its target.').
card_ruling('pyromancer\'s goggles', '2015-06-22', 'If the copy says that it affects “you,” it affects the controller of the copy, not the controller of the original spell. Similarly, if the copy says that it affects an “opponent,” it affects an opponent of the copy’s controller, not an opponent of the original spell’s controller.').

card_ruling('pyromancer\'s swath', '2007-05-01', 'Pyromancer\'s Swath doesn\'t change the source or recipient of the damage.').
card_ruling('pyromancer\'s swath', '2007-05-01', 'If all damage from a source is prevented (because of Pay No Heed, for example) or if a source would deal 0 damage, the effect of Pyromancer\'s Swath will not apply as that source is no longer dealing damage.').
card_ruling('pyromancer\'s swath', '2007-05-01', 'If damage is being dealt to multiple creatures and/or players at the same time, the damage being dealt to each one is increased by 2.').

card_ruling('pyromancy', '2004-10-04', 'A card with X in the cost is treated as X=0.').
card_ruling('pyromancy', '2004-10-04', 'You pick the target before you know which card is going to be discarded.').

card_ruling('pyrotechnics', '2014-11-24', 'The number of targets chosen for Pyrotechnics must be at least one and at most four. You choose how the damage is divided as you cast the spell, not as it resolves. Each target must be assigned at least 1 damage.').
card_ruling('pyrotechnics', '2014-11-24', 'If some but not all of Pyrotechnics’s targets become illegal, you can’t change the division of damage. Damage that would have been dealt to illegal targets simply isn’t dealt.').
card_ruling('pyrotechnics', '2014-11-24', 'You can’t deal damage to both a player and a planeswalker that player controls using Pyrotechnics. You also can’t deal damage to more than one planeswalker controlled by the same player. If you choose to redirect the damage being dealt to a player to a planeswalker, you must redirect all the damage to a single planeswalker.').
card_ruling('pyrotechnics', '2014-11-24', 'If an effect creates a copy of Pyrotechnics, the number of targets and division of damage can’t be changed. The effect that creates the copy may allow you to change the targets, however.').

card_ruling('pyrrhic revival', '2008-08-01', 'Pyrrhic Revival can cause creatures to be put onto the battlefield that have 0 toughness due to the -1/-1 counter from this effect. Those creatures are put right back into their owners\' graveyards as a state-based action. No spells or abilities that would remove counters or otherwise affect those creatures can be cast or activated before that happens. Still, the creatures did momentarily pop onto the battlefield, so any relevant triggered abilities (including their own enters-the-battlefield or leaves-the-battlefield abilities) will trigger. They\'ll be put on the stack after state-based actions are checked.').

card_ruling('pyxis of pandemonium', '2013-09-15', 'Neither player can look at cards exiled with Pyxis of Pandemonium until those cards are turned face up.').
card_ruling('pyxis of pandemonium', '2013-09-15', 'If Pyxis of Pandemonium leaves the battlefield, and later another Pyxis of Pandemonium enters the battlefield, it is a new object (even if the two were represented the same card). Cards exiled by the original one can’t be turned face up or put onto the battlefield by the second one.').

