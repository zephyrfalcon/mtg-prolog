% Rulings

card_ruling('qal sisma behemoth', '2015-02-25', 'If an effect says that Qal Sisma Behemoth must attack or block if able, you can choose not to pay the associated cost and ignore that requirement. Players are never forced to pay a cost to attack or block.').

card_ruling('qarsi deceiver', '2015-02-25', 'The mana produced by Qarsi Deceiver can’t be used to cast a spell that instructs you to manifest cards.').
card_ruling('qarsi deceiver', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('qarsi deceiver', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('qarsi deceiver', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('qarsi high priest', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('qarsi high priest', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('qarsi high priest', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('qarsi high priest', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('qarsi high priest', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('qarsi high priest', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('qarsi high priest', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('qarsi high priest', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('qarsi high priest', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('qarsi high priest', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('qarsi high priest', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('qarsi high priest', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('qarsi high priest', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('qarsi sadist', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('qarsi sadist', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('qarsi sadist', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('qarsi sadist', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('qarsi sadist', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('qasali ambusher', '2008-10-01', 'Casting Qasali Ambusher as described in its second ability still uses the stack. The spell can be countered.').
card_ruling('qasali ambusher', '2008-10-01', 'If you want to cast Qasali Ambusher by using its second ability, you don\'t necessarily have to do it during the declare attackers step. You can do it later in combat (including all the way up through the end of combat step) as long as there\'s still a creature attacking you. Of course, if you cast Qasali Ambusher later than the declare attackers step, it won\'t be able to block.').
card_ruling('qasali ambusher', '2008-10-01', 'For you to cast Qasali Ambusher as described in its second ability, a creature has to be attacking *you*. If creatures are attacking only planeswalkers, you won\'t be able to cast Qasali Ambusher this way. If you\'re playing a Two-Headed Giant game and a creature is attacking your team, you will be able to cast Qasali Ambusher this way.').
card_ruling('qasali ambusher', '2008-10-01', 'Qasali Ambusher\'s second ability is looking for lands with the subtypes Forest and Plains. Those lands don\'t need to be named Forest and Plains. It\'s okay if both subtypes are from the same land (such as Savannah or Temple Garden).').

card_ruling('quag sickness', '2010-08-15', 'This ability counts the number of Swamps controlled by Quag Sickness\'s controller, not the enchanted creature\'s controller (in case they\'re different players).').
card_ruling('quag sickness', '2010-08-15', 'This bonus is not fixed; it changes as the number of Swamps you control changes.').
card_ruling('quag sickness', '2010-08-15', 'The ability cares about lands with the land type Swamp, not necessarily lands named Swamp.').

card_ruling('quagnoth', '2007-05-01', 'If Quagnoth is discarded normally (it moves from your hand to your graveyard), its last ability triggers from your graveyard. If Yixlid Jailer is on the battlefield, it won\'t trigger.').
card_ruling('quagnoth', '2007-05-01', 'If an effect of a spell or ability an opponent controls causes Quagnoth to be discarded, but Quagnoth moves from your hand directly to the Exile zone (due toLeyline of the Void, for example), the ability will trigger from that zone. It will return Quagnoth to your hand from that zone when it resolves.').
card_ruling('quagnoth', '2007-05-01', 'If an effect of a spell or ability an opponent controls causes Quagnoth to be discarded, but Quagnoth leaves its new zone after its ability triggers, the ability will have no effect when it resolves.').
card_ruling('quagnoth', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('quagnoth', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('quagnoth', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('quagnoth', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('quagnoth', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('quagnoth', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('quarantine field', '2015-08-25', 'Use the number of isolation counters on Quarantine Field as its triggered ability is put on the stack to determine how many targets you may choose. Once the targets are chosen, the number of targets the ability has is locked in. Changing the number of isolation counters on Quarantine Field won’t change how many nonland permanents are exiled.').
card_ruling('quarantine field', '2015-08-25', 'If Quarantine Field exiles multiple permanents, those cards all return to the battlefield at the same time.').
card_ruling('quarantine field', '2015-08-25', 'If Quarantine Field leaves the battlefield before its triggered ability resolves, no permanents will be exiled.').
card_ruling('quarantine field', '2015-08-25', 'Auras attached to the exiled permanents will be put into their owners’ graveyards. Equipment attached to the exiled creatures will become unattached and remain on the battlefield. Any counters on the exiled permanents will cease to exist.').
card_ruling('quarantine field', '2015-08-25', 'If a token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('quarantine field', '2015-08-25', 'The exiled cards return to the battlefield immediately after Quarantine Field leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('quarantine field', '2015-08-25', 'In a multiplayer game, if Quarantine Field’s owner leaves the game, the exiled cards will return to the battlefield. Because the one-shot effect that returns the cards isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('quarry colossus', '2014-04-26', 'Quarry Colossus’s ability is mandatory. If you control the only creatures when that ability triggers, you must choose a creature you control (which could be Quarry Colossus itself) as the target.').
card_ruling('quarry colossus', '2014-04-26', 'The value of X is determined as the ability resolves. If you control no Plains at that time, put the creature on top of that library.').
card_ruling('quarry colossus', '2014-04-26', 'If there are fewer than X cards in that player’s library, put the creature on the bottom of that library.').

card_ruling('quarum trench gnomes', '2008-08-01', 'The ability only replaces white mana produced by the Plains. If a Plains somehow tries to produce mana of a different color (for instance, if you control a Joiner Adept), this effect will not cause it to produce colorless mana.').

card_ruling('quash', '2004-10-04', 'It exiles the countered spell. This is because the first sentence puts the spell into the graveyard before you continue to the second sentence.').
card_ruling('quash', '2004-10-04', 'If the spell is not countered (because the spell it targets can\'t be countered), then it does not get exiled but all the other copies in the graveyard, hand, and library are exiled.').
card_ruling('quash', '2005-02-01', 'Quash can exile cards even if the spell countered was a copy of a spell. For example, if you Quash a Boomerang cast from an Isochron Scepter, you may search for and exile cards named Boomerang.').
card_ruling('quash', '2005-02-01', 'If Quash counters a spell that has had text spliced onto it, only the base spell may be exiled. For example, if you use Quash to counter a Kodama\'s Might with a Glacial Ray spliced onto it, you may search for and exile only Kodama\'s Might.').
card_ruling('quash', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').

card_ruling('quenchable fire', '2009-02-01', 'After Quenchable Fire resolves, the targeted player becomes able to perform a special action of paying {U}. This action may be performed any time the player has priority, and it doesn\'t use the stack. It may be performed only once, and only before your next upkeep begins. If the player has performed the action by then, the \"deal an additional 3 damage\" ability won\'t trigger. Otherwise, it will. If it triggers, the player can\'t stop it by paying {U}.').
card_ruling('quenchable fire', '2009-02-01', 'Quenchable Fire\'s delayed triggered ability (and the special action of paying {U}) will affect the player who was targeted by the spell, even if the original 3 damage is prevented or redirected to a different creature or player.').

card_ruling('quest for pure flame', '2009-10-01', 'Both of Quest for Pure Flame\'s abilities interact with any kind of damage, not just combat damage.').
card_ruling('quest for pure flame', '2009-10-01', 'The source of combat damage is the creature that dealt it.').
card_ruling('quest for pure flame', '2009-10-01', 'If a spell causes damage to be dealt, that spell will always identify the source of the damage. In most cases, the source is the spell itself. For example, Lightning Bolt says \"Lightning Bolt deals 3 damage to target creature or player.\"').
card_ruling('quest for pure flame', '2009-10-01', 'If an ability causes damage to be dealt, that ability will always identify the source of the damage. The ability itself is never the source. However, the source of the ability is often the source of the damage. For example, Prodigal Pyromancer\'s ability says \"Prodigal Pyromancer deals 1 damage to target creature or player.\"').
card_ruling('quest for pure flame', '2009-10-01', 'If the source of the damage is a permanent, Quest for Pure Flame checks whether you control that permanent at the time that damage is dealt. If the permanent has left the battlefield by then, its last known information is used. If the source of the damage is a spell, whether you control it is obvious. If the source of the damage is a card in some other zone (such as a cycled Jund Sojourners), Quest for Pure Flame checks whether you\'re the card\'s owner, rather than whether you\'re its controller.').
card_ruling('quest for pure flame', '2009-10-01', 'When the first ability resolves, you may put just one quest counter on Quest for Pure Flame. It doesn\'t matter how much damage the source dealt.').
card_ruling('quest for pure flame', '2009-10-01', 'The first ability will trigger multiple times if multiple sources you control deal damage to a single opponent at once, or if a single source you control deals damage to multiple opponents at once.').
card_ruling('quest for pure flame', '2009-10-01', 'The second ability affects all sources you control that would deal damage to a creature or player at any point in the rest of the turn.').
card_ruling('quest for pure flame', '2009-10-01', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage or the controller of the creature that would be dealt damage chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn.\" Suppose a spell controlled by a player who has activated Quest for Pure Fire\'s second ability would deal 5 damage to a player who has cast Mending Hands targeting him or herself. The player who would be dealt damage can either (a) prevent 4 damage first and then let Quest for Pure Fire\'s effect double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').
card_ruling('quest for pure flame', '2009-10-01', 'Combat damage that a source you control would deal to a planeswalker is not doubled. However, if a source you control would deal noncombat damage to that player, and that player chooses to apply Quest for Pure Fire\'s replacement effect before applying the planeswalker redirection effect, the damage will be doubled before you choose whether to deal it to a planeswalker that player controls. (If your opponents always apply the planeswalker redirection effect first, Quest for Pure Fire\'s effect will never double damage that a source you control would deal to a planeswalker.)').
card_ruling('quest for pure flame', '2009-10-01', 'If a spell or ability divides damage among multiple recipients (such as Arrow Volley Trap does), the damage is divided before Quest for Pure Fire\'s effect doubles it. The same is true for combat damage.').
card_ruling('quest for pure flame', '2009-10-01', 'If you activate the second ability of more than one Quest for Pure Flame in the same turn, the effects are cumulative. Two such effects will cause damage from sources you control to be multiplied by four; three such effects will cause damage from sources you control to be multiplied by eight.').

card_ruling('quest for renewal', '2010-03-01', 'Creatures put onto the battlefield tapped don\'t cause Quest for Renewal\'s first ability to trigger.').
card_ruling('quest for renewal', '2010-03-01', 'As another player\'s untap step begins, if there are four or more quest counters on Quest for Renewal, all your creatures untap during that untap step. You have no choice about what untaps. Those creatures untap at the same time as the active player\'s permanents.').
card_ruling('quest for renewal', '2010-03-01', 'During another player\'s untap step, effects that would otherwise cause your creatures to stay tapped don\'t apply because they apply only during *your* untap step. For example, if you control a Quest for Renewal with four or more quest counters on it and a Deep-Slumber Titan (a creature that says \"Deep-Slumber Titan doesn\'t untap during your untap step\"), you untap Deep-Slumber Titan during each other player\'s untap step.').
card_ruling('quest for renewal', '2010-03-01', 'Controlling more than one Quest for Renewal with four or more quest counters on it is redundant. You can\'t untap your permanents more than once in a single untap step.').

card_ruling('quest for the gemblades', '2009-10-01', 'When the first ability resolves, you may put just one quest counter on Quest for the Gemblades. It doesn\'t matter how much damage the source dealt.').
card_ruling('quest for the gemblades', '2009-10-01', 'The first ability will trigger multiple times if a creature you control deals combat damage to multiple creatures, and/or if multiple creatures you control each deal combat damage to a single creature.').
card_ruling('quest for the gemblades', '2009-10-01', 'If there are no counters on Quest for the Gemblades yet, and simultaneously a creature you control deals combat damage to a creature and a creature you control is dealt lethal combat damage, the first ability will trigger too late for you to be able to activate the second ability and save your creature.').

card_ruling('quest for the gravelord', '2009-10-01', 'Quest for the Gravelord\'s first ability triggers regardless of who controlled the creature and whose graveyard it was put into.').

card_ruling('quest for the holy relic', '2009-10-01', 'If you cast a creature spell, Quest for the Holy Relic\'s triggered ability triggers and goes on the stack on top of it. The triggered ability will resolve before the spell does.').
card_ruling('quest for the holy relic', '2009-10-01', 'The activated ability doesn\'t target the creature the Equipment will be attached to. It may be attached to a creature with shroud, for example. You don\'t choose which creature it will be attached to until you actually attach it. Once you choose which creature to attach it to, it\'s too late for players to respond.').
card_ruling('quest for the holy relic', '2009-10-01', 'You may activate Quest for the Holy Relic\'s activated ability even if you control no creatures, or you control no creatures that can be equipped. In that case, you\'ll simply put the Equipment card you find onto the battlefield without attaching it to anything.').

card_ruling('quest for the nihil stone', '2010-03-01', 'The second ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless, as an opponent\'s upkeep starts, that player has no cards in hand and Quest for the Nihil Stone has two or more quest counters on it, and (2) the ability will do nothing if that player has at least one card in hand and/or Quest for the Nihil Stone has fewer than two quest counters on it by the time it resolves. (If Quest for the Nihil Stone is no longer on the battlefield by that point, its last existence on the battlefield is checked to see how many quest counters were on it.)').

card_ruling('quest for ula\'s temple', '2010-03-01', 'When the first ability resolves, you may look at the top card of your library. If you don\'t, nothing happens. If you look at it and it\'s not a creature card, it simply stays hidden. If you look at it and it is a creature card, you may reveal it and put a quest counter on Quest for Ula\'s Temple, or you may choose to leave it hidden. In all cases, the card remains on top of your library.').
card_ruling('quest for ula\'s temple', '2010-03-01', 'If an ability (from Future Sight or Oracle of Mul Daya, for example) causes you to play with the top card of your library revealed, you may still put a quest counter on Quest for Ula\'s Temple when the first ability resolves if the top card of your library is a creature card.').
card_ruling('quest for ula\'s temple', '2010-03-01', 'Although the first ability triggers only on your turn, the second ability may trigger each turn.').

card_ruling('quick sliver', '2004-10-04', 'The first ability applies when this card is not on the battlefield. The second ability applies when this card is on the battlefield.').
card_ruling('quick sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('quick sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('quickchange', '2005-10-01', 'You can choose any single color or any combination of more than one color. You can\'t choose colorless.').
card_ruling('quickchange', '2005-10-01', 'Quickchange won\'t make an artifact stop being an artifact. It\'ll just be a colorful artifact.').

card_ruling('quicken', '2006-02-01', 'You don\'t choose a sorcery card when Quicken resolves. Rather, this sets up a rule that is true for you until the turn ends or until you cast a sorcery card, even if you cast that sorcery at a time you normally could.').
card_ruling('quicken', '2006-02-01', 'If you cast multiple Quickens on the same turn, they\'ll all apply to the very next sorcery spell you cast.').
card_ruling('quicken', '2006-09-25', 'After Quicken resolves, you can Suspend a sorcery in your hand any time you can cast an instant. As soon as you actually cast a sorcery, you lose this capability.').
card_ruling('quicken', '2013-07-01', 'Casting a copy of a sorcery card won’t “use up” Quicken’s effect because the copy isn’t a card.').

card_ruling('quickening licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('quickening licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('quickling', '2014-07-18', 'If you control no other creatures when the enters-the-battlefield ability resolves, you must sacrifice Quickling.').

card_ruling('quicksilver amulet', '2004-10-04', 'Any \'X\' in the creature\'s cost is treated as zero.').
card_ruling('quicksilver amulet', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').
card_ruling('quicksilver amulet', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('quicksilver amulet', '2011-09-22', 'You don\'t pay any costs of that creature card, including additional costs.').

card_ruling('quicksilver elemental', '2004-10-04', 'If you use this card to gain Wall of Deceit\'s ability to turn itself face down, you will not be able to turn this card face up because it will not have the Morph ability.').
card_ruling('quicksilver elemental', '2004-10-04', 'If you make two copies of an ability that can be activated once a turn, you can activate each of them once a turn.').
card_ruling('quicksilver elemental', '2004-10-04', 'You can activate the ability more than once, collecting abilities from multiple creatures (or the same creature more than once).').
card_ruling('quicksilver elemental', '2004-12-01', 'Quicksilver Elemental gains only activated abilities. It doesn\'t gain keyword abilities (unless those keyword abilities are activated), triggered abilities, or static abilities.').
card_ruling('quicksilver elemental', '2004-12-01', 'Quicksilver Elemental can gain the activated abilities of any creature on the battlefield that you can target with its ability, even if you don\'t control that creature.').
card_ruling('quicksilver elemental', '2004-12-01', 'The granted abilities effectively use \"this permanent,\" rather than \"[that card\'s name],\" so you treat the abilities as if they were printed on Quicksilver Elemental. For example, you treat an ability that says \"Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn\" as \"Sacrifice a creature: Quicksilver Elemental gets +2/+2 until end of turn.\"').
card_ruling('quicksilver elemental', '2004-12-01', 'Quicksilver Elemental has a rather tricky interaction with Leonin Bola. You can actually unattach your opponent\'s Bola as part of the cost of activating the Elemental-copied ability.').
card_ruling('quicksilver elemental', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('quicksilver fountain', '2004-12-01', 'The land stays an Island until the flood counter is removed, even if Quicksilver Fountain leaves the battlefield.').
card_ruling('quicksilver fountain', '2004-12-01', 'The timestamp of the land-type-changing ability is set when the triggered ability resolves (so each one has a different timestamp).').

card_ruling('quicksilver gargantuan', '2011-01-01', 'Except for its power and toughness, Quicksilver Gargantuan copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'If the chosen creature has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'If the chosen creature is copying something else (for example, if the chosen creature is a Clone), then your Quicksilver Gargantuan enters the battlefield as whatever the chosen creature copied, except for its power and toughness.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'If the chosen creature is a token, your Quicksilver Gargantuan copies the original characteristics of that token as stated by the effect that put the token onto the battlefield, except for its power and toughness. Your Quicksilver Gargantuan is not a token.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'If Quicksilver Gargantuan is not a creature (for example, if it entered the battlefield as a copy of an animated land), it will not have the characteristics of power or toughness at all, so it won\'t be 7/7. If it later becomes a creature, its power and toughness will be determined by the effect that causes it to become a creature; again, it won\'t be 7/7.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'Any enters-the-battlefield abilities of the copied creature will trigger when Quicksilver Gargantuan enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'If Quicksilver Gargantuan somehow enters the battlefield at the same time as another creature (due to Mass Polymorph or Liliana Vess\'s third ability, for example), Quicksilver Gargantuan can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('quicksilver gargantuan', '2011-01-01', 'You can choose not to copy anything. In that case, Quicksilver Gargantuan simply enters the battlefield as a 7/7 creature.').
card_ruling('quicksilver gargantuan', '2011-06-01', 'If the chosen creature has a characteristic-defining ability that sets its power and/or toughness, that ability will not be copied. For example, if Quicksilver Gargantuan copies a Nightmare, it will not gain the ability that sets its power and toughness to the number of Swamps you control. As such, it will remain a 7/7.').

card_ruling('quicksilver sea', '2012-06-01', 'You play the card (meaning it play it if it\'s a land card or cast it if it\'s a nonland card) as part of the resolution of the chaos ability. Timing restrictions based on the card\'s type are ignored. Other restrictions, such as \"Cast [this card] only during combat,\" are not.').
card_ruling('quicksilver sea', '2012-06-01', 'If you reveal a land card when resolving the chaos ability, you may play it only if you haven\'t played a land yet that turn.').
card_ruling('quicksilver sea', '2012-06-01', 'If you cast a card without paying its mana cost, you can\'t pay any alternative costs, such as evoke or the alternative cost provided by the morph ability. If it has {X} in its mana cost, X must be 0. However, you can pay optional additional costs, such as kicker, and you must still pay mandatory additional costs, such as the one on Fling.').

card_ruling('quiet contemplation', '2014-09-20', 'You decide whether to pay {1} as the triggered ability resolves.').
card_ruling('quiet contemplation', '2014-09-20', 'The ability can target a creature that’s already tapped. It still won’t untap during its controller’s next untap step.').
card_ruling('quiet contemplation', '2014-09-20', 'The ability tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').

card_ruling('quiet disrepair', '2007-05-01', 'If you choose the \"destroy\" mode and Quiet Disrepair is moved to a different permanent while the ability is on the stack, the newly enchanted permanent will be destroyed when the ability resolves.').
card_ruling('quiet disrepair', '2007-05-01', 'If you choose the \"destroy\" mode and Quiet Disrepair leaves the battlefield while the ability is on the stack, the last permanent it enchanted will be destroyed.').

card_ruling('quiet speculation', '2004-10-04', 'You can choose not to find all the cards if you don\'t want to.').

card_ruling('quietus spike', '2008-10-01', 'That player loses half his or her life after combat damage has been subtracted from the player\'s life total. The amount of life the player loses is determined as the triggered ability resolves.').
card_ruling('quietus spike', '2008-10-01', 'If multiple Quietus Spikes trigger at the same time, that player loses half his or her life when the first ability resolves, then loses half of the remainder when the next ability resolves, and so on. The player does not lose the same amount each time.').
card_ruling('quietus spike', '2010-06-15', 'In a Two-Headed Giant game, if a creature equipped with Quietus Spike would assign combat damage to the defending team, its damage is assigned to only one of the defending players. After combat damage is dealt, Quietus Spike looks at that player\'s life total (which is the same as the team\'s life total) when determining how much life the team will lose, which basically means the team\'s life total is halved. Here\'s an example of the math: The team has 19 life, so the player has 19 life. Quietus Spike causes the team to lose 10 life (19 divided by 2, rounded up). The team\'s life total becomes 9 (19 minus 10).').

card_ruling('quilled sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('quilled sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('quirion druid', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('quirion dryad', '2012-07-01', 'Quirion Dryad\'s ability will trigger only once per spell you cast, as long as that spell is at least one of the listed colors.').
card_ruling('quirion dryad', '2012-07-01', 'Quirion Dryad\'s ability will trigger if you cast a spell that\'s green and at least one of the listed colors.').

card_ruling('quirion explorer', '2008-08-01', 'If the opponent only has lands that produce colorless or no mana, this card\'s ability can still be activated; it just won\'t produce any mana.').

card_ruling('quirion ranger', '2004-10-04', 'It can target an untapped creature.').

card_ruling('quirion sentinel', '2008-08-01', 'This is not a mana ability. It uses the stack, and can be responded to.').
card_ruling('quirion sentinel', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('quirion trailblazer', '2004-10-04', 'The land does not count toward your normal limit of playing one land per turn.').
card_ruling('quirion trailblazer', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

