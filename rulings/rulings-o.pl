% Rulings

card_ruling('o-naginata', '2005-06-01', 'O-Naginata becomes unattached if the equipped creature\'s power is less than 3 as state-based actions are checked.').

card_ruling('oakheart dryads', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('oakheart dryads', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('oakheart dryads', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('oath of druids', '2009-10-01', 'The ability can only target an opponent of the current player.').
card_ruling('oath of druids', '2009-10-01', 'The targeted player controlling more creatures than the current player is a part of the targeting requirement. A player can\'t be targeted by this ability unless it\'s true, and the ability will be countered on resolution if it\'s no longer true at that time.').

card_ruling('oath of ghouls', '2009-10-01', 'The ability can only target an opponent of the current player.').
card_ruling('oath of ghouls', '2009-10-01', 'The targeted player\'s graveyard having fewer creature cards in it than the current player\'s is a part of the targeting requirement. A player can\'t be targeted by this ability unless it\'s true, and the ability will be countered on resolution if it\'s no longer true at that time.').

card_ruling('oath of lieges', '2004-10-04', 'The land card enters the battlefield under the current player\'s control.').
card_ruling('oath of lieges', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('oath of lieges', '2008-08-01', 'Shuffling the library is part of the optional effect. The player only shuffles if he or she chose to search.').
card_ruling('oath of lieges', '2009-10-01', 'The ability can only target an opponent of the current player.').
card_ruling('oath of lieges', '2009-10-01', 'The targeted player controlling more lands than the current player is a part of the targeting requirement. A player can\'t be targeted by this ability unless it\'s true, and the ability will be countered on resolution if it\'s no longer true at that time.').

card_ruling('oath of mages', '2009-10-01', 'The ability can only target an opponent of the current player.').
card_ruling('oath of mages', '2009-10-01', 'The targeted player having more life than the current player is a part of the targeting requirement. A player can\'t be targeted by this ability unless it\'s true, and the ability will be countered on resolution if it\'s no longer true at that time.').

card_ruling('oath of scholars', '2009-10-01', 'The ability can only target an opponent of the current player.').
card_ruling('oath of scholars', '2009-10-01', 'The targeted player having more cards in his or her hand than the current player is a part of the targeting requirement. A player can\'t be targeted by this ability unless it\'s true, and the ability will be countered on resolution if it\'s no longer true at that time.').

card_ruling('oath of the ancient wood', '2013-07-01', 'If the enchantment that enters the battlefield is somehow also a creature, you may choose it as the target of Oath of the Ancient Wood’s ability.').

card_ruling('oathkeeper, takeno\'s daisho', '2004-12-01', 'Oathkeeper\'s second ability checks whether the card\'s creature type is Samurai, not whether the creature was a Samurai when it left the battlefield.').

card_ruling('ob nixilis reignited', '2015-08-25', 'The emblem created by Ob Nixilis Reignited’s last ability is both owned and controlled by the target opponent. In a multiplayer game, if you own Ob Nixilis Reignited and use him to give one of your opponents an emblem, the emblem remains even if you leave the game.').

card_ruling('ob nixilis, the fallen', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('ob nixilis, the fallen', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('ob nixilis, unshackled', '2014-07-18', 'Ob Nixilis’s first triggered ability won’t trigger if you search an opponent’s library or if an opponent searches another player’s library.').
card_ruling('ob nixilis, unshackled', '2014-07-18', 'If the opponent controls no creatures when the first triggered ability resolves, that player still loses 10 life.').
card_ruling('ob nixilis, unshackled', '2014-07-18', 'The first triggered ability won’t be put on the stack until after the spell or ability causing the opponent to search his or her library finishes resolving. Notably, if that spell or ability causes any other abilities to trigger (for example, if the opponent searched for a creature card and put it onto the battlefield), those abilities and Ob Nixilis’s triggered ability will go on the stack together. The active player puts all of his or her abilities on the stack in any order, then each other player in turn order does the same. The last ability put onto the stack this way will be the first to resolve, and so on.').

card_ruling('obelisk of urd', '2014-07-18', 'You must choose an existing creature type.').
card_ruling('obelisk of urd', '2014-07-18', 'The choice of creature type is made as Obelisk of Urd enters the battlefield. Players can’t respond to this choice. The bonus starts applying immediately.').
card_ruling('obelisk of urd', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('obelisk of urd', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('obelisk of urd', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('obelisk of urd', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('obelisk of urd', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('obelisk of urd', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('obelisk of urd', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('obliterate', '2004-10-04', 'Counterspells can be cast that target it, but when they resolve they simply don\'t counter it since it can\'t be countered.').

card_ruling('oblivion ring', '2007-10-01', 'If Oblivion Ring leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and exile the targeted nonland permanent forever.').
card_ruling('oblivion ring', '2007-10-01', 'If there are no nonland permanents on the battlefield other than an Oblivion Ring, and the card it exiled was another Oblivion Ring, casting a third Oblivion Ring will result in an involuntary infinite loop that will end the game in a draw (unless someone chooses to break it by putting another nonland permanent onto the battlefield or destroying one of the Oblivion Rings, for example).').
card_ruling('oblivion ring', '2008-10-01', 'If the exiled card is an Aura, that card\'s owner chooses what it will enchant as it comes back onto the battlefield. An Aura put onto the battlefield this way doesn\'t target anything, but the Aura\'s enchant ability restricts what it can be attached to. If the Aura can\'t legally be attached to anything, it remains exiled forever.').
card_ruling('oblivion ring', '2012-07-01', 'Auras attached to the exiled permanent will be put into their owners\' graveyards. Equipment attached to the exiled permanent will become unattached and remain on the battlefield. Any counters on the exiled permanent will cease to exist.').

card_ruling('oblivion sower', '2015-08-25', 'Oblivion Sower’s ability allows you to put any land cards the player owns from exile onto the battlefield, regardless of how those cards were exiled.').
card_ruling('oblivion sower', '2015-08-25', 'Cards that are face down in exile have no characteristics. Such cards can’t be put onto the battlefield with Oblivion Sower’s ability.').

card_ruling('oboro envoy', '2005-06-01', 'The errata makes the ability temporary. As printed, the effect didn\'t end.').
card_ruling('oboro envoy', '2005-06-01', 'The X will usually include the land returned to pay the ability\'s cost.').

card_ruling('obscuring æther', '2015-02-25', 'Obscuring Æther has no effect on spells that instruct you to manifest cards.').
card_ruling('obscuring æther', '2015-02-25', 'Note that Obscuring Æther doesn’t have a morph cost. Once it’s turned face down, it doesn’t have a way to turn itself face up.').

card_ruling('observant alseid', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('observant alseid', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('observant alseid', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('observant alseid', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('observant alseid', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('observant alseid', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('observant alseid', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('obsidian battle-axe', '2008-04-01', 'Each of these Equipment has two subtypes listed on its type line. The first one is a creature type, which in this case is also a subtype of tribal. The second one is Equipment, which is a subtype of artifact.').
card_ruling('obsidian battle-axe', '2008-04-01', 'Each of these Equipment has a triggered ability that says \"Whenever a [creature type] creature enters the battlefield, you may attach [this Equipment] to it.\" This triggers whenever any creature of the specified creature type enters the battlefield, no matter who controls it. You may attach your Equipment to another player\'s creature this way, even though you can\'t do so with the equip ability.').
card_ruling('obsidian battle-axe', '2008-04-01', 'If you attach an Equipment you control to another player\'s creature, you retain control of the Equipment, but you don\'t control the creature. Only you can activate the Equipment\'s equip ability, and if the Equipment\'s ability triggers again, you choose whether to move the Equipment. Only the creature\'s controller can activate any activated abilities the Equipment grants to the creature, and \"you\" in any abilities granted to the creature refers to that player.').

card_ruling('obsidian fireheart', '2009-10-01', 'As the reminder text indicates, whether the targeted land has the triggered ability that\'s been granted to it depends only on whether it has a blaze counter on it, not on whether Obsidian Fireheart is still on the battlefield.').
card_ruling('obsidian fireheart', '2009-10-01', 'The ability gained by the land triggers at the beginning of the upkeep of the land\'s controller, not the beginning of the upkeep of Obsidian Fireheart\'s controller. The player who controls the land at the time the ability triggers is the one who\'s dealt damage.').
card_ruling('obsidian fireheart', '2009-10-01', 'A land with a blaze counter on it is an illegal target for this ability. You may activate the ability targeting a land, then activate the ability in response to itself targeting the same land. Although the first ability to resolve will put a blaze counter on that land, the second ability to resolve will be countered.').
card_ruling('obsidian fireheart', '2009-10-01', 'If all blaze counters on a land are moved to a different land, the triggered ability doesn\'t follow them. The first land no longer has the ability because it no longer has a blaze counter on it. The second land doesn\'t have the ability because Obsidian Fireheart didn\'t target it.').
card_ruling('obsidian fireheart', '2009-10-01', 'If a land ends up with more than one blaze counter on it (thanks to Doubling Season or Gilder Bairn, for example), the ability still only causes it to deal 1 damage to its controller each turn.').

card_ruling('obstinate baloth', '2010-08-15', 'If a spell or ability an opponent controls causes you to discard Obstinate Baloth, and both Obstinate Baloth\'s ability and another ability (such as the one from an opponent\'s Leyline of the Void) instruct you to put Obstinate Baloth somewhere else instead of putting it into your graveyard, you choose which one to apply.').
card_ruling('obstinate baloth', '2010-08-15', 'If you discard Obstinate Baloth and wind up putting it onto the battlefield, you\'ve still discarded it. Abilities that trigger when you discard a card (such as the one from Liliana\'s Caress) will still trigger.').

card_ruling('obzedat\'s aid', '2013-04-15', 'A permanent card is an artifact card, a creature card, an enchantment card, a land card, or a planeswalker card.').
card_ruling('obzedat\'s aid', '2013-04-15', 'If the target is an Aura card, you choose what it will enchant as Obzedat\'s Aid resolves. If there\'s nothing legal for it to enchant, it stays in the graveyard.').

card_ruling('obzedat, ghost council', '2013-01-24', 'When Obzedat is returned to the battlefield by its last ability, it gains haste indefinitely. This may be relevant if a player gains control of it later using a spell or ability that doesn\'t give it haste.').

card_ruling('odds', '2006-05-01', 'The Odds coin flip has no winner or loser, and no player calls \"heads\" or \"tails.\"').

card_ruling('odric, master tactician', '2012-07-01', 'You can decide that a creature won\'t block.').
card_ruling('odric, master tactician', '2012-07-01', 'All blocking declarations must still be legal.').
card_ruling('odric, master tactician', '2012-07-01', 'If there\'s a cost associated with having a creature block and you choose for that creature to block, its controller can choose to pay that cost or not. If that player decides to not pay that cost, you must propose a new set of blocking creatures.').

card_ruling('odylic wraith', '2008-04-01', 'Odylic Wraith\'s ability triggers when it deals any kind of damage to a player, not just when it deals combat damage.').

card_ruling('off balance', '2004-10-04', 'The ability only does something if used before attackers or blockers (as appropriate) are declared during a turn. If used after the creature is declared as an attacker or blocker, nothing happens.').

card_ruling('offalsnout', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('offalsnout', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('offalsnout', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('offalsnout', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('offalsnout', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('offalsnout', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').

card_ruling('offering to asha', '2009-05-01', 'You gain 4 life whether or not the targeted spell\'s controller pays {4} and whether or not the targeted spell is countered.').

card_ruling('ogre enforcer', '2004-10-04', 'If its toughness falls to zero or less and it is undamaged, it will still be put into the graveyard.').
card_ruling('ogre enforcer', '2004-10-04', 'The check for nonlethal damage is done on a per-source basis. So each source\'s damage is checked independently and it is not possible to team up sources (such as creatures) in order to kill it.').

card_ruling('ogre geargrabber', '2011-01-01', 'If the targeted Equipment can\'t be attached to Ogre Geargrabber for some reason, you\'ll still gain control of it until end of turn.').
card_ruling('ogre geargrabber', '2011-01-01', 'Any ability of the targeted Equipment that triggers \"whenever equipped creature attacks\" won\'t trigger. That\'s because Ogre Geargrabber is already attacking at the time that Equipment becomes attached to it.').
card_ruling('ogre geargrabber', '2011-01-01', 'The delayed triggered ability (\"When you lose control of that Equipment, unattach it\") triggers the next time you lose control of the Equipment for any reason, not just when Ogre Geargrabber\'s control-changing effect ends. The Equipment will become unattached from whatever creature it happens to be attached to at that time. It doesn\'t matter if Ogre Geargrabber is still on the battlefield at this time.').
card_ruling('ogre geargrabber', '2011-01-01', 'Ogre Geargrabber\'s control-changing effect ends during the cleanup step, at the same time that damage marked on permanents is removed. If you still controlled the Equipment at that time, this causes the delayed triggered ability to trigger. It goes on the stack and players can respond (which normally doesn\'t happen during the cleanup step), then, once that cleanup step ends, another one begins.').

card_ruling('ogre jailbreaker', '2012-10-01', 'Defender only matters when Ogre Jailbreaker could be declared as an attacking creature. If Ogre Jailbreaker is already attacking, losing control of your only Gate won\'t cause Ogre Jailbreaker to leave combat.').

card_ruling('ogre marauder', '2005-02-01', 'Ogre Marauder\'s ability requires the defending player to sacrifice a creature as the ability resolves, or Ogre Marauder can\'t be blocked for the rest of the turn.').
card_ruling('ogre marauder', '2005-02-01', 'Attacking with Ogre Marauder a second time would cause a second trigger. If no creature was previously sacrificed, the Marauder already can\'t be blocked. Sacrificing a creature for the second trigger will not nullify the first.').

card_ruling('ogre savant', '2006-02-01', 'If {U} was spent to cast Ogre Savant and no other creatures are on the battlefield, the Ogre will have to return itself.').
card_ruling('ogre savant', '2006-02-01', 'The ability is not optional.').

card_ruling('ogre slumlord', '2013-01-24', 'All Rats you control have deathtouch while Ogre Slumlord is on the battlefield, not just the ones created by Ogre Slumlord.').

card_ruling('ojutai exemplars', '2015-02-25', 'If Ojutai Exemplars leaves the battlefield in response to its ability with the third mode chosen, that ability won’t return Ojutai Exemplars to the battlefield. It will stay wherever it is.').
card_ruling('ojutai exemplars', '2015-02-25', 'After the ability returns Ojutai Exemplars to the battlefield, it will be a new object with no connection to the Ojutai Exemplars that left the battlefield. It won’t be in combat or have any additional abilities it may have had when it left the battlefield. Any +1/+1 counters on it or Auras attached to it are removed.').

card_ruling('ojutai interceptor', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('ojutai interceptor', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('ojutai interceptor', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('ojutai monument', '2015-02-25', 'A Monument can’t attack on the turn it enters the battlefield.').
card_ruling('ojutai monument', '2015-02-25', 'Each Monument is colorless, although the last ability will make each of them two colors until end of turn.').
card_ruling('ojutai monument', '2015-02-25', 'If a Monument has any +1/+1 counters on it, those counters will remain on the permanent after it stops being a creature. Those counters will have no effect as long as the Monument isn’t a creature, but they will apply again if the Monument later becomes a creature.').
card_ruling('ojutai monument', '2015-02-25', 'Activating the last ability of a Monument while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

card_ruling('ojutai\'s breath', '2015-02-25', 'Ojutai’s Breath can target a creature that’s already tapped. It still won’t untap during its controller’s next untap step.').
card_ruling('ojutai\'s breath', '2015-02-25', 'Ojutai’s Breath tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('ojutai\'s breath', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('ojutai\'s breath', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('ojutai\'s breath', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('ojutai\'s breath', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('ojutai\'s breath', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('ojutai\'s breath', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('ojutai\'s breath', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('ojutai\'s breath', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('ojutai\'s command', '2015-02-25', 'You choose the two modes as you cast the spell. You must choose two different modes. Once modes are chosen, they can’t be changed.').
card_ruling('ojutai\'s command', '2015-02-25', 'You can choose a mode only if you can choose legal targets for that mode. Ignore the targeting requirements for modes that aren’t chosen. For example, you can cast Ojutai’s Command without targeting a creature spell provided you don’t choose the third mode.').
card_ruling('ojutai\'s command', '2015-02-25', 'As the spell resolves, follow the instructions of the modes you chose in the order they are printed on the card. For example, if you chose the second and fourth modes of Ojutai’s Command, you would gain 4 life and then draw a card. (The order won’t matter in most cases.)').
card_ruling('ojutai\'s command', '2015-02-25', 'If a Command is copied, the effect that creates the copy will usually allow you to choose new targets for the copy, but you can’t choose new modes.').
card_ruling('ojutai\'s command', '2015-02-25', 'If all targets for the chosen modes become illegal before the Command resolves, the spell will be countered and none of its effects will happen. If at least one target is still legal, the spell will resolve but will have no effect on any illegal targets.').

card_ruling('ojutai\'s summons', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('ojutai\'s summons', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('ojutai\'s summons', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('ojutai\'s summons', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('ojutai\'s summons', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('ojutai\'s summons', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('ojutai\'s summons', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('ojutai\'s summons', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('ojutai, soul of winter', '2014-11-24', 'The triggered ability can target a nonland permanent that’s already tapped. That permanent won’t untap during its controller’s next untap step.').
card_ruling('ojutai, soul of winter', '2014-11-24', 'The ability tracks the permanent, but not its controller. If the permanent changes controllers before its first controller’s next untap step, then it won’t untap during its new controller’s next untap step.').
card_ruling('ojutai, soul of winter', '2014-11-24', 'The ability only applies during the controller’s next untap step, even if the permanent has been the target of Ojutai’s triggered ability more than once. The permanent won’t stay tapped through multiple untap steps.').

card_ruling('okiba-gang shinobi', '2005-02-01', 'If the defending player has two or fewer cards in hand when Okiba-Gang Shinobi deals combat damage to that player, they are all discarded.').

card_ruling('old man of the sea', '2004-10-04', 'You lose control of the creature if the Old Man leaves the battlefield. This is because it is no longer tapped.').
card_ruling('old man of the sea', '2009-10-01', 'If Old Man of the Sea stops being tapped before its ability resolves -- even if it becomes tapped again right away -- you won\'t gain control of the targeted creature at all. The same is true if the targeted creature\'s power becomes greater than Old Man of the Sea\'s power before the ability resolves.').
card_ruling('old man of the sea', '2009-10-01', 'The ability doesn\'t care whether Old Man of the Sea remains under your control, only that Old Man of the Sea remains tapped and its power remains large enough. If an opponent gains control of Old Man of the Sea, you\'ll keep control of the affected creature (until Old Man of the Sea stops being tapped or it no longer has enough power).').
card_ruling('old man of the sea', '2009-10-01', 'Once the ability resolves, it doesn\'t care whether the targeted permanent remains a legal target for the ability, only that it\'s on the battlefield and its power remains small enough. The effect will continue to apply to it even if it gains shroud, stops being a creature, or would no longer be a legal target for any other reason. If it stops being a creature, its power is considered to be 0.').

card_ruling('olivia voldaren', '2011-09-22', 'If Olivia Voldaren deals lethal damage to a creature with its first activated ability, that creature will become a Vampire before dying.').
card_ruling('olivia voldaren', '2011-09-22', 'If you activate Olivia Voldaren\'s last ability, and before that ability resolves you lose control of Olivia Voldaren, the ability will resolve with no effect. You won\'t gain control of the targeted Vampire.').

card_ruling('oloro, ageless ascetic', '2013-10-17', 'Oloro’s first ability works only if Oloro is on the battlefield.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'If Oloro is your commander, its last ability will trigger at the beginning of the upkeep step on your first turn.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'Oloro’s second ability triggers just once for each life-gaining event, no matter how much life is gained.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'You decide whether to pay {1} as the second ability resolves. If you do, you’ll draw a card and each opponent will lose 1 life.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'If two creatures you control with lifelink deal combat damage at the same time, Oloro’s ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'In some unusual cases, you can gain life even though your life total actually decreases. For example, if you are being attacked by two 3/3 creatures and you block one with a 2/2 creature with lifelink, your life total will decrease by 1 even though you’ve gained 2 life. Oloro’s second ability would trigger.').
card_ruling('oloro, ageless ascetic', '2013-10-17', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('omen machine', '2011-06-01', 'Putting the land onto the battlefield or casting the card is not optional.').
card_ruling('omen machine', '2011-06-01', 'If the player is unable to put the land onto the battlefield (this is rare) or cast the spell (perhaps because there are no legal targets available) as the triggered ability of Omen Machine resolves, the card simply remains in exile.').

card_ruling('omenspeaker', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('omenspeaker', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('omenspeaker', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('omenspeaker', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('omnath, locus of mana', '2010-03-01', 'You can keep green mana in your mana pool indefinitely while Omnath is on the battlefield. That means if you add a green mana to your mana pool during one step or phase, you can spend it during a later step or phase, or even a later turn. Other types of mana will continue to empty from your mana pool as each step and phase ends.').
card_ruling('omnath, locus of mana', '2010-03-01', 'If a green mana you add to your mana pool has certain restrictions or riders associated with it (for example, if it was produced by Ancient Ziggurat), they\'ll apply to that mana no matter when you spend it.').
card_ruling('omnath, locus of mana', '2010-03-01', 'Once Omnath leaves the battlefield, you have until the end of the current step or phase to spend whatever green mana is in your mana pool before it too empties as normal. There is no penalty associated with this other than the loss of the mana.').

card_ruling('omnath, locus of rage', '2015-08-25', 'If Omnath dies at the same time as any other Elementals you control, Omnath’s last ability will trigger for each of them (and once for itself).').
card_ruling('omnath, locus of rage', '2015-08-25', 'Omanth doesn’t have the land type Locus.').
card_ruling('omnath, locus of rage', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('omnath, locus of rage', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('omnath, locus of rage', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('omnibian', '2006-05-01', 'If the target creature has any counters on it that modify its power and toughness (such as a +1/+1 counter), those counters will continue to affect the creature after it becomes 3/3. In the example given, it will be 4/4.').
card_ruling('omnibian', '2009-10-01', 'If the target creature is affected by any effects that modify its power or toughness without setting them to specific values (such as from Giant Growth), Omnibian\'s effect will be applied before those effects. In the example given, the creature will be 6/6.').
card_ruling('omnibian', '2013-04-15', 'This effect changes only the power, toughness, and creature type of the targeted creature. It does not cause that creature to lose any abilities.').

card_ruling('omniscience', '2012-07-01', 'You must follow the normal timing restrictions of each nonland card you cast.').
card_ruling('omniscience', '2012-07-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('omniscience', '2012-07-01', 'If the card has X in its mana cost, you must choose 0 as its value.').

card_ruling('onakke catacomb', '2012-06-01', 'Creatures will be only black, not any other color. Creature cards in other zones are not affected.').
card_ruling('onakke catacomb', '2012-06-01', 'Creatures that you gain control of after the chaos ability resolves won\'t get any of that ability\'s bonuses.').

card_ruling('ondu champion', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('ondu champion', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('ondu cleric', '2009-10-01', 'The ability counts the number of Allies you control as it resolves.').

card_ruling('ondu greathorn', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('ondu greathorn', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('ondu greathorn', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('ondu rising', '2015-08-25', 'Ondu Rising creates a delayed triggered ability. Any creature that attacks that turn will cause that ability to trigger, including creatures controlled by a teammate and creatures you didn’t control or that didn’t exist as Ondu Rising resolved. Notably, this includes the land creature created if Ondu Rising is cast for its awaken cost.').
card_ruling('ondu rising', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('ondu rising', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('ondu rising', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('ondu rising', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('ondu rising', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('ondu rising', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('one thousand lashes', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('only blood ends your nightmares', '2010-06-15', 'Each opponent who can sacrifice a creature must do so, and thus won\'t discard any cards. Each opponent who can\'t sacrifice a creature (either because he or she doesn\'t control any, or because he or she controls Tajuru Preserver) will wind up discarding two cards. Your opponents have no choice in the matter.').

card_ruling('onslaught', '2004-10-04', 'Can target an already tapped creature.').
card_ruling('onslaught', '2004-10-04', 'The ability is not optional. If you have the only targetable creatures, you have to tap one of your own.').
card_ruling('onslaught', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('onyx goblet', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('onyx mage', '2011-09-22', 'Multiple instances of deathtouch on the same creature provide no additional benefit. A creature dealt damage by a creature with multiple instances of deathtouch is destroyed only once.').

card_ruling('oona\'s blackguard', '2008-04-01', 'If a Rogue would normally enter the battlefield with a certain number of +1/+1 counters on it, it enters the battlefield with that many +1/+1 counters plus one on it instead. If a Rogue would normally enter the battlefield with no +1/+1 counters on it, it enters the battlefield with one +1/+1 counter on it instead.').
card_ruling('oona\'s blackguard', '2008-04-01', 'The creature gets the counter if it would enter the battlefield under your control. It doesn\'t matter who owns the creature or what zone it enters the battlefield from (such as your opponent\'s graveyard, for example).').
card_ruling('oona\'s blackguard', '2008-04-01', 'The creature gets the counter if its copiable characteristics as it would exist on the battlefield include the specified creature type. For example, say you control Conspiracy, and the chosen creature type is Rogue. If you put a creature onto the battlefield that isn\'t normally a Rogue, it won\'t get a counter from Oona\'s Blackguard.').
card_ruling('oona\'s blackguard', '2008-04-01', 'The effects from more than one of these cards are cumulative. That is, if you have two Oona\'s Blackguard on the battlefield, Rogue creatures you control enter the battlefield with two additional +1/+1 counters on them.').
card_ruling('oona\'s blackguard', '2008-04-01', 'If Oona\'s Blackguard enters the battlefield at the same time as another Rogue (due to Living End, for example), that creature doesn\'t get a +1/+1 counter.').

card_ruling('oona\'s grace', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('oona\'s grace', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('oona\'s grace', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('oona\'s grace', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('oona, queen of the fae', '2008-05-01', 'You target the opponent when you activate Oona\'s ability. You don\'t choose the color until the ability resolves.').
card_ruling('oona, queen of the fae', '2008-05-01', 'The tokens enter the battlefield under your control.').

card_ruling('ooze garden', '2008-10-01', 'X is equal to the power of the sacrificed creature as it last existed on the battlefield.').

card_ruling('opal acrolith', '2004-10-04', 'When it turns into an enchantment, it is no longer a creature.').
card_ruling('opal acrolith', '2004-10-04', 'Changes into a creature even if the spell is countered.').
card_ruling('opal acrolith', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal acrolith', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('opal acrolith', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal acrolith', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal archangel', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal archangel', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('opal archangel', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('opal archangel', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal archangel', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal avenger', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal avenger', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal caryatid', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal caryatid', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('opal caryatid', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('opal caryatid', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal caryatid', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal champion', '2004-10-04', 'It triggers when the spell is cast, so it becomes a creature before the spell resolves. A creature that would destroy an enchantment when it enters the battlefield can\'t be used to destroy this card, since it will no longer be an enchantment when the creature enters the battlefield.').
card_ruling('opal champion', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal champion', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('opal champion', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal champion', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal gargoyle', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal gargoyle', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('opal gargoyle', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('opal gargoyle', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal gargoyle', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal guardian', '2006-09-25', 'Guardian\'s effect doesn\'t wear off.').
card_ruling('opal guardian', '2006-09-25', 'After the triggered ability resolves, Opal Guardian is no longer an enchantment.').
card_ruling('opal guardian', '2006-09-25', 'A face-down creature spell will cause Opal Guardian\'s ability to trigger (if it\'s an enchantment).').
card_ruling('opal guardian', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opal palace', '2013-10-17', 'The “number of times it’s been cast from the command zone” includes the most recent time. For example, the first time you cast your commander from the command zone in a game, if you spent mana from Opal Palace’s last ability to do so, it will enter the battlefield with a +1/+1 counter.').
card_ruling('opal palace', '2013-10-17', 'The color identity of your commander is set before the game begins and doesn’t change during the game, even if your commander is in a hidden zone (like the hand or library) or an effect changes your commander’s color.').
card_ruling('opal palace', '2013-10-17', 'If your commander is a card like Kozilek, Butcher of Truth that has no colors in its color identity, Opal Palace’s last ability produces no mana.').
card_ruling('opal palace', '2013-10-17', 'In formats other than Commander, Opal Palace’s last ability produces no mana.').

card_ruling('opal titan', '2004-10-04', 'If the spell was colorless (an artifact), this card does not get any form of Protection.').
card_ruling('opal titan', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('opal titan', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('opal titan', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('opal titan', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('opal titan', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('opalescence', '2004-10-04', 'Does not animate itself. But can be animated by another Opalescence.').
card_ruling('opalescence', '2006-02-01', 'With a Humility and two Opalescences on the battlefield, if Humility has the latest timestamp, then all creatures are 1/1 with no abilities. If the timestamp order is Opalescence, Humility, Opalescence, the second Opalescence is 1/1, and the Humility and first Opalescence are 4/4. If Humility has the earliest timestamp, then everything is 4/4.').
card_ruling('opalescence', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('opalescence', '2009-10-01', 'This is the current interaction between Humility and Opalescence: The type-changing effect applies at layer 4, but the rest happens in the applicable layers. The rest of it will apply even if the permanent loses its ability before it\'s finished applying. So if Opalescence, Humility, and Worship are on the battlefield and Opalescence entered the battlefield before Humility, the following is true: Layer 4: Humility and Worship each become creatures that are still enchantments. (Opalescence). Layer 6: Humility and Worship each lose their abilities. (Humility) Layer 7b: Humility becomes 4/4 and Worship becomes 4/4. (Opalescence). Humility becomes 1/1 and Worship becomes 1/1 (Humility). But if Humility entered the battlefield before Opalescence, the following is true: Layer 4: Humility and Worship each become creatures that are still enchantments (Opalescence). Layer 6: Humility and Worship each lose their abilities (Humility). Layer 7b: Humility becomes 1/1 and Worship becomes 1/1 (Humility). Humility becomes 4/4 and Worship becomes 4/4 (Opalescence).').

card_ruling('opaline sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('opaline sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('open the vaults', '2009-10-01', 'If multiple players have Auras in their graveyards, the player whose turn it is announces what his or her Auras will enchant, then each other player in turn order does the same, then all artifacts and enchantments (including both Auras and non-Auras) enter the battlefield at the same time.').
card_ruling('open the vaults', '2009-10-01', 'An Aura put onto the battlefield this way can\'t enchant an artifact or enchantment that\'s also being put onto the battlefield with Open the Vaults.').
card_ruling('open the vaults', '2009-10-01', 'If an Aura can be returned to the battlefield with Open the Vaults, it must be, even if its owner doesn\'t like what it would enchant.').
card_ruling('open the vaults', '2009-10-01', 'Any abilities that trigger during the resolution of Open the Vaults will wait to be put onto the stack until Open the Vaults finishes resolving. The player whose turn it is will put all of his or her triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').

card_ruling('ophidian', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('ophidian eye', '2006-09-25', 'The third ability triggers when the enchanted creature deals damage to an opponent of the player who controls Ophidian Eye.').

card_ruling('ophiomancer', '2013-10-17', 'Ophiomancer’s ability checks if you control a Snake at the beginning of each upkeep. If you do, the ability won’t trigger.').
card_ruling('ophiomancer', '2013-10-17', 'If the ability does trigger, but you control a Snake when it tries to resolve, the ability will do nothing. No Snake token will be created.').
card_ruling('ophiomancer', '2013-10-17', 'Ophiomancer’s ability considers any creature you control with the creature type Snake, not just the tokens Ophiomancer creates.').

card_ruling('oppressive rays', '2014-07-18', 'The enchanted creature’s controller can choose to not attack or block with it even if it must attack or block if able. Players can’t be forced to pay a cost to attack or block.').
card_ruling('oppressive rays', '2014-07-18', 'Activated abilities are written in the form “Cost: Effect.” Some keywords are activated abilities and will have colons in their reminder texts. Static and triggered abilities are unaffected by Oppressive Rays.').

card_ruling('oracle en-vec', '2004-10-04', 'The way the card works is that the target player chooses which creatures will attack on their next turn. Creatures which are chosen to attack, but do not attack, are destroyed at the end of that turn. Creatures which are not chosen to be attackers are not affected in any way other than being unable to attack.').

card_ruling('oracle of bones', '2014-02-01', 'If you want to cast a card this way, you cast it as part of the resolution of the triggered ability. Timing restrictions based on the card’s type (such as sorcery) are ignored. Other casting restrictions (such as “Cast [this card] only before attackers are declared”) are not.').
card_ruling('oracle of bones', '2014-02-01', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('oracle of bones', '2014-02-01', 'If you cast a card “without paying its mana cost,” you can’t pay alternative costs such as overload costs. You can, however, pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('oracle of bones', '2014-02-01', 'If you choose to cast a split card with fuse without paying its mana cost, you may cast both halves.').
card_ruling('oracle of bones', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('oracle of bones', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('oracle of bones', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('oracle of bones', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('oracle of bones', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('oracle of dust', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('oracle of dust', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('oracle of dust', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('oracle of dust', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('oracle of dust', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('oracle of dust', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('oracle of dust', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('oracle of dust', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('oracle of dust', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('oracle of mul daya', '2009-10-01', 'As you play a land, announce whether it\'s your normal land play for the turn or you\'re playing it as the result of Oracle of Mul Daya\'s first ability (or another such effect that exists).').
card_ruling('oracle of mul daya', '2009-10-01', 'If you control more than one Oracle of Mul Daya, the effects of their first abilities are cumulative. If you control two, for example, you can play three lands on your turn.').
card_ruling('oracle of mul daya', '2009-10-01', 'Oracle of Mul Daya doesn\'t change the times when you can play a land card from the top of your library. You can play a land only during your main phase when you have priority and the stack is empty. Doing so counts as one of your land plays for the turn (either your normal one, the additional one Oracle of Mul Daya grants you, or an additional one from some other effect).').
card_ruling('oracle of mul daya', '2009-10-01', 'If you play your first land of the turn from the top of your library, and the new top card is another land card, you can play that one too.').
card_ruling('oracle of mul daya', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('oracle of mul daya', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('oracle of mul daya', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('oracle\'s insight', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('oracle\'s insight', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('oracle\'s insight', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('oracle\'s insight', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('oran-rief hydra', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('oran-rief hydra', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('oran-rief hydra', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('oran-rief survivalist', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('oran-rief, the vastwood', '2009-10-01', 'Oran-Rief\'s third ability cares about permanents\' characteristics at the time the ability resolves, not their characteristics at the time they entered the battlefield. For example, if a blue creature enters the battlefield, then is turned green by a spell or ability, then Oran-Rief\'s second ability resolves, you\'ll put a +1/+1 counter on that creature.').
card_ruling('oran-rief, the vastwood', '2009-10-01', 'Oran-Rief\'s second ability affects all permanents that are green creatures that entered the battlefield this turn, not just the ones you control.').

card_ruling('orator of ojutai', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('orator of ojutai', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('orator of ojutai', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('orbs of warding', '2015-06-22', 'As long as you have hexproof, your opponents can’t target you with spells that deal damage or abilities that cause their sources to deal damage, even if they intend to redirect the damage to a planeswalker you control.').
card_ruling('orbs of warding', '2015-06-22', 'Orbs of Warding won’t prevent damage dealt by creatures to a planeswalker you control. However, if a creature controlled by an opponent would deal noncombat damage to you, you can apply Orbs of Warding’s effect to prevent 1 of that damage before your opponent chooses whether to redirect that damage to a planeswalker you control.').
card_ruling('orbs of warding', '2015-06-22', 'The damage prevention effects of multiple Orbs of Warding are cumulative.').

card_ruling('orc sureshot', '2014-11-24', 'If Orc Sureshot enters the battlefield under your control at the same time as another creature, its ability will trigger for that creature.').

card_ruling('orchard warden', '2008-04-01', 'The creature\'s toughness is checked as this ability resolves. If the creature has left the battlefield, use its last known information.').

card_ruling('orcish artillery', '2004-10-04', 'You do not take the 3 points of damage if the ability is countered because the target is illegal.').

card_ruling('orcish captain', '2008-10-01', 'You choose the target when you activate the ability. You don\'t flip a coin until the ability resolves.').

card_ruling('orcish farmer', '2008-10-01', 'This ability changes the targeted land\'s land types. It doesn\'t affect its name, its supertypes (such as whether or not it\'s legendary, snow, or basic), or its other types (if it\'s also a creature, for example). The ability will cause the land to lose all of its printed abilities and gain the ability \"{T}: Add {B} to your mana pool.\"').

card_ruling('orcish librarian', '2004-10-04', 'You do get to look at the remaining 4 cards before deciding which order to put them back in.').

card_ruling('orcish squatters', '2004-10-04', 'The ability triggers on the declaration of blockers if no blockers are declared for it.').
card_ruling('orcish squatters', '2008-10-01', 'If you lose control of Orcish Squatters before its ability resolves, you won\'t gain control of the targeted land at all.').
card_ruling('orcish squatters', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('ordeal of erebos', '2013-09-15', 'The +1/+1 counter is put on the creature before blockers are declared and before combat damage is dealt.').
card_ruling('ordeal of erebos', '2013-09-15', 'The check of whether the enchanted creature has three or more +1/+1 counters on it happens as part of the resolution of the attack triggered ability. If the third +1/+1 counter is put on the enchanted creature any other way, you won’t sacrifice the Ordeal until the next time the creature attacks.').
card_ruling('ordeal of erebos', '2013-09-15', 'If you sacrifice the Ordeal in some other way, its last ability will trigger.').

card_ruling('ordeal of heliod', '2013-09-15', 'The +1/+1 counter is put on the creature before blockers are declared and before combat damage is dealt.').
card_ruling('ordeal of heliod', '2013-09-15', 'The check of whether the enchanted creature has three or more +1/+1 counters on it happens as part of the resolution of the attack triggered ability. If the third +1/+1 counter is put on the enchanted creature any other way, you won’t sacrifice the Ordeal until the next time the creature attacks.').
card_ruling('ordeal of heliod', '2013-09-15', 'If you sacrifice the Ordeal in some other way, its last ability will trigger.').

card_ruling('ordeal of nylea', '2013-09-15', 'The +1/+1 counter is put on the creature before blockers are declared and before combat damage is dealt.').
card_ruling('ordeal of nylea', '2013-09-15', 'The check of whether the enchanted creature has three or more +1/+1 counters on it happens as part of the resolution of the attack triggered ability. If the third +1/+1 counter is put on the enchanted creature any other way, you won’t sacrifice the Ordeal until the next time the creature attacks.').
card_ruling('ordeal of nylea', '2013-09-15', 'If you sacrifice the Ordeal in some other way, its last ability will trigger.').

card_ruling('ordeal of purphoros', '2013-09-15', 'The +1/+1 counter is put on the creature before blockers are declared and before combat damage is dealt.').
card_ruling('ordeal of purphoros', '2013-09-15', 'The check of whether the enchanted creature has three or more +1/+1 counters on it happens as part of the resolution of the attack triggered ability. If the third +1/+1 counter is put on the enchanted creature any other way, you won’t sacrifice the Ordeal until the next time the creature attacks.').
card_ruling('ordeal of purphoros', '2013-09-15', 'If you sacrifice the Ordeal in some other way, its last ability will trigger.').

card_ruling('ordeal of thassa', '2013-09-15', 'The +1/+1 counter is put on the creature before blockers are declared and before combat damage is dealt.').
card_ruling('ordeal of thassa', '2013-09-15', 'The check of whether the enchanted creature has three or more +1/+1 counters on it happens as part of the resolution of the attack triggered ability. If the third +1/+1 counter is put on the enchanted creature any other way, you won’t sacrifice the Ordeal until the next time the creature attacks.').
card_ruling('ordeal of thassa', '2013-09-15', 'If you sacrifice the Ordeal in some other way, its last ability will trigger.').

card_ruling('order of succession', '2013-10-17', 'Order of Succession doesn’t target any players or creatures. You can choose and gain control of a creature with protection from blue, for example.').
card_ruling('order of succession', '2013-10-17', 'It doesn’t matter whether the next player is a teammate or an opponent.').
card_ruling('order of succession', '2013-10-17', 'All players will gain control of the creatures they chose simultaneously.').
card_ruling('order of succession', '2013-10-17', 'If the next player in the chosen direction controls no creatures, you won’t gain control of anything.').

card_ruling('order of whiteclay', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('order of whiteclay', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('order of whiteclay', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('ordered migration', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('ordered migration', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('ordered migration', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('ordruun veteran', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('ordruun veteran', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('ordruun veteran', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('oreskos sun guide', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('oreskos sun guide', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('oreskos sun guide', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('oreskos sun guide', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('orgg', '2004-10-04', 'Whether or not the creatures could attack or block is not important, just whether or not the defending player controls such a creature.').

card_ruling('orim\'s chant', '2004-10-04', 'This spell does nothing to creatures which are already attacking. It does not remove attacking creatures from combat.').
card_ruling('orim\'s chant', '2004-10-04', 'This spell does not affect the abilities of permanents. It only affects spells.').
card_ruling('orim\'s chant', '2008-08-01', 'This can\'t be used as a counterspell. It will have no effect on spells which were on the stack when it was cast, nor on those cast in response to it.').

card_ruling('orim\'s prayer', '2008-04-01', 'The ability only triggers when one or more creatures attack you. It will not trigger on creatures attacking a planeswalker you control.').

card_ruling('ornate kanzashi', '2005-02-01', 'Normal restrictions apply to cards exiled by Ornate Kanzashi (you can\'t cast an instant unless you have priority, etc.).').
card_ruling('ornate kanzashi', '2005-02-01', 'Cards exiled by Ornate Kanzashi\'s ability are exiled face up.').
card_ruling('ornate kanzashi', '2005-02-01', 'You must pay all costs to play cards exiled by Ornate Kanzashi.').
card_ruling('ornate kanzashi', '2005-02-01', 'You can\'t play cards you exiled with Ornate Kanzashi on previous turns.').
card_ruling('ornate kanzashi', '2005-02-01', 'Cards exiled by Ornate Kanzashi\'s ability stay exiled even if the Kanzashi leaves the battlefield.').

card_ruling('ornitharch', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('ornitharch', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('ornitharch', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('ornitharch', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('ornitharch', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('orzhov basilica', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('orzhov charm', '2013-01-24', 'Orzhov Charm\'s first mode doesn\'t target any of the Auras attached to the target creature.').
card_ruling('orzhov charm', '2013-01-24', 'If you choose the first mode, any Aura controlled by another player attached to the creature will be put into its owner\'s graveyard as a state-based action after the creature leaves the battlefield.').
card_ruling('orzhov charm', '2013-01-24', 'If you choose the second mode, you\'ll lose life equal to the creature\'s toughness when it was last on the battlefield.').
card_ruling('orzhov charm', '2013-01-24', 'For each of the modes, if the target creature or creature card is an illegal target when Orzhov Charm tries to resolve, it will be countered and none of its effects will happen.').
card_ruling('orzhov charm', '2013-07-01', 'If you choose the second mode, and Orzhov Charm resolves but the creature isn\'t destroyed (perhaps because it has indestructible or it regenerates), you\'ll still lose life equal to the creature\'s toughness.').

card_ruling('orzhov guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('orzhov guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('orzhov keyrune', '2013-01-24', 'Multiple instances of lifelink are redundant. Activating the last ability multiple times won\'t cause you to gain more life.').
card_ruling('orzhov keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('orzhov keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('orzhov pontiff', '2006-02-01', 'The mode chosen when this creature enters the battlefield may be different than the mode chosen when the creature it haunts is put into a graveyard.').

card_ruling('osai vultures', '2004-10-04', 'Only gets one counter per turn, not one per creature.').

card_ruling('ostracize', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('otaria', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('otaria', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('otaria', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('otaria', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('otaria', '2009-10-01', 'If you use a card\'s flashback ability, you\'re actually casting that card. It moves from your graveyard to the stack. Abilities that trigger when you cast a spell will trigger. That spell can be countered.').
card_ruling('otaria', '2009-10-01', 'If you cast a card by using flashback, you cast that card from your graveyard rather than your hand, and you pay an alternative cost rather than its mana cost, but everything else about casting that spell works normally. You must follow timing restrictions based on the card\'s type (if it\'s a sorcery), as well as other restrictions (such as \"Cast [this card] only before blockers are declared\"). You may pay additional costs (such as kicker). Effects that cause you to pay more or less for a spell will apply.').
card_ruling('otaria', '2009-10-01', 'As a spell cast with flashback resolves, it never goes to its owner\'s graveyard, so abilities that trigger on cards being put in a graveyard won\'t trigger. The card is exiled instead.').
card_ruling('otaria', '2009-10-01', 'If a spell cast by using flashback is countered, it\'s still exiled rather than being put into its owner\'s graveyard.').
card_ruling('otaria', '2009-10-01', 'If you use the flashback ability granted by Otaria to cast a split card from your graveyard, the flashback cost you pay is equal to the mana cost of the half that you\'re casting.').
card_ruling('otaria', '2009-10-01', 'If you use the flashback ability granted by Otaria to cast a card with X in its mana cost from your graveyard, you choose the value of X as you cast it. You\'ll still have to pay that X.').
card_ruling('otaria', '2009-10-01', 'Otaria may cause an instant or sorcery card in a graveyard to have multiple flashback abilities. Its owner may cast it using any one of those abilities.').
card_ruling('otaria', '2009-10-01', 'If you roll {C} multiple times in the same turn, you\'ll take that many extra turns after this one.').

card_ruling('otherworldly journey', '2004-12-01', 'The creature comes back onto the battlefield as a new creature, with one +1/+1 counter on it (plus any other counters that it would normally enter the battlefield with).').
card_ruling('otherworldly journey', '2005-08-01', 'Any Aura attached to the creature is put into its owner\'s graveyard as a state-based action. Equipment attached to the creature become unattached but remain on the battlefield.').
card_ruling('otherworldly journey', '2005-08-01', 'If Otherworldly Journey is cast during an end step, the creature won\'t return until the beginning of the next end step.').

card_ruling('oubliette', '2007-09-16', 'If Oubliette leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and exile the targeted creature forever.').
card_ruling('oubliette', '2007-09-16', 'If the targeted creature is a token, it will cease to exist after being exiled. Any Auras that were attached to it will remain exiled forever.').
card_ruling('oubliette', '2007-09-16', 'If the exiled card is returned to the battlefield and, for some reason, it now can\'t be enchanted by an Aura that was also exiled by Oubliette, that Aura will remain exiled.').

card_ruling('ouphe vandals', '2004-12-01', 'The ability targets an ability on the stack, not the artifact, so it can destroy an artifact with protection from green or one that can\'t be targeted by spells and abilities.').
card_ruling('ouphe vandals', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('oust', '2010-06-15', 'If the targeted creature\'s owner has one or more cards in his or her library, that creature is put into that library directly under the top card.').
card_ruling('oust', '2010-06-15', 'If the targeted creature\'s owner has no cards left in his or her library, that creature is put into that library as the only card there.').
card_ruling('oust', '2010-06-15', 'If the targeted creature is a token, it will cease to exist after it\'s put into its owner\'s library.').
card_ruling('oust', '2010-06-15', 'Note that the creature\'s controller, not its owner, is the one who gains life.').
card_ruling('oust', '2010-06-15', 'If the targeted creature is an illegal target by the time Oust resolves, the spell is countered. No one gains life.').

card_ruling('outland colossus', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('outland colossus', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('outland colossus', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('outmaneuver', '2008-04-01', 'If a creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('outmaneuver', '2008-04-01', 'Creatures damage the defending player or planeswalker even if the blocking creatures are no longer there at that time.').

card_ruling('outnumber', '2015-08-25', 'Count the number of creatures you control as Outnumber resolves to determine how much damage Outnumber deals.').

card_ruling('outpost siege', '2014-11-24', 'The card exiled by the “Khans” ability is exiled face up. Playing a card exiled with the “Khans” ability follows the normal rules for playing the card. You must pay its costs, and you must follow all applicable timing rules. For example, if it’s a creature card, you can cast it only during your main phase while the stack is empty.').
card_ruling('outpost siege', '2014-11-24', 'If you exile a land card using the “Khans” ability, you may play that land only if you have any available land plays. Normally, this means you can play the land only if you haven’t played a land yet that turn.').
card_ruling('outpost siege', '2014-11-24', 'If a noncreature card is manifested and then leaves the battlefield while face down, the “Dragons” ability will trigger.').
card_ruling('outpost siege', '2014-11-24', 'Each Siege will have one of the two listed abilities, depending on your choice as it enters the battlefield.').
card_ruling('outpost siege', '2014-11-24', 'The words “Khans” and “Dragons” are anchor words, connecting your choice to the appropriate ability. Anchor words are a new rules concept. “[Anchor word] — [Ability]” means “As long as you chose [anchor word] as this permanent entered the battlefield, this permanent has [ability].” Notably, the anchor word “Dragons” has no connection to the creature type Dragon.').
card_ruling('outpost siege', '2014-11-24', 'Each of the last two abilities is linked to the first ability. They each refer only to the choice made as a result of the first ability. If a permanent enters the battlefield as a copy of one of the Sieges, its controller will make a new choice for that Siege. Which ability the copy has won’t depend on the choice made for the original permanent.').

card_ruling('outrage shaman', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('outrage shaman', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').
card_ruling('outrage shaman', '2008-08-01', 'The effect counts the mana symbols in this cards mana cost as well.').

card_ruling('outrider en-kor', '2006-09-25', 'If the targeted creature leaves the battlefield before the redirection shield is used, the damage is not redirected.').
card_ruling('outrider en-kor', '2006-09-25', 'The damage is combat damage or unpreventable damage, it will still be combat damage or unpreventable damage after it has been redirected.').
card_ruling('outrider en-kor', '2006-09-25', 'When damage would be dealt to Outrider en-Kor, its controller chooses which redirection shields to apply out of all the shields that have been created that turn. It doesn\'t matter what order they were created in.').

card_ruling('outrider of jhess', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('outrider of jhess', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('outrider of jhess', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('outrider of jhess', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('outrider of jhess', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('outrider of jhess', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('outwit', '2012-05-01', 'The spell you target could also target other things, as long as it also targets a player.').

card_ruling('overabundance', '2004-10-04', 'The ability deals one damage even if the land produces no mana when it was tapped to use a mana ability. This can happen with lands that produce a variable amount of mana.').
card_ruling('overabundance', '2004-10-04', 'This card\'s ability is a mana ability. There is no chance to respond to it in order to prevent the damage. If you want to prevent the damage, you need to use a prevention spell or ability prior to tapping the land for mana.').

card_ruling('overblaze', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('overblaze', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('overblaze', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('overblaze', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('overgrown battlement', '2010-06-15', 'Overgrown Battlement\'s activated ability is a mana ability. No player can respond to it (such as by destroying a creature with defender).').

card_ruling('overgrown tomb', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('overgrown tomb', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('overgrown tomb', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('overgrowth', '2004-10-04', 'This is a triggered mana ability.').

card_ruling('overlaid terrain', '2004-10-04', 'You sacrifice your lands right before this card enters the battlefield. There is no way to tap your lands using the 2-mana ability before they are sacrificed.').

card_ruling('overrule', '2006-05-01', 'You gain the life whether or not the spell\'s controller pays {X} and whether or not the spell is countered.').

card_ruling('overrun', '2009-10-01', 'Overrun affects only creatures you control at the time it resolves. It won\'t affect creatures that come under your control later in the turn.').

card_ruling('oversold cemetery', '2004-10-04', 'The check for four or more creature cards is performed on resolution as well as at the beginning of upkeep. So the trigger may not do anything if there are not enough creatures any longer.').

card_ruling('overwhelm', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('overwhelm', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('overwhelm', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('overwhelm', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('overwhelm', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('overwhelm', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('overwhelm', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('overwhelming stampede', '2010-08-15', 'You check the power of your creatures as Overwhelming Stampede resolves. For example, if you control a 2/1 creature, a 2/2 creature, a 2/4 creature, a 4/1 creature, a 5/5 creature, and a 5/6 creature at that time, each of your creatures gets +5/+5 and gains trample until end of turn.').

card_ruling('ovinize', '2007-02-01', 'Ovinize doesn\'t counter abilities that have already triggered or been activated. In particular, you can\'t cast this fast enough to stop a creature\'s \"At the beginning of your upkeep\" or \"When this creature enters the battlefield\" abilities from triggering, since they trigger before players get priority.').
card_ruling('ovinize', '2007-02-01', 'If the affected creature gains an ability after Ovinize resolves, it will keep that ability.').
card_ruling('ovinize', '2007-02-01', 'If a face-down creature is affected by Ovinize, it won\'t be able to be turned face up for its morph cost. If some other effect turns it face up, it will remain a 0/1 creature with no abilities until the turn ends. Its \"When this creature turns face up\" or \"As this creature turns face up\" abilities won\'t work.').
card_ruling('ovinize', '2007-02-01', 'Ovinize can be used to disable a creature\'s vanishing ability permanently by casting it in response to the vanishing upkeep trigger when there\'s one time counter left. The last time counter will be removed as that ability resolves, but since the creature no longer has vanishing, the \"sacrifice\" triggered ability won\'t exist. When Ovinize\'s effect ends and the creature regains vanishing, its triggered ability will no longer be able to trigger because it has no time counters.').
card_ruling('ovinize', '2007-02-01', 'If Ovinize affects Mistform Ultimus, the Ultimus will lose its ability that says \"Mistform Ultimus is every creature type,\" but it will still *be* all creature types. The way continuous effects work, Mistform Ultimus\'s type-changing ability is applied before Ovinize\'s ability removes it.').
card_ruling('ovinize', '2007-02-01', 'This is the timeshifted version of Humble.').
card_ruling('ovinize', '2007-02-01', 'Removes all creature abilities. This includes mana abilities. Animated lands will also lose the ability to tap for mana.').

card_ruling('ovinomancer', '2004-10-04', 'Can target itself. This will cause it to be returned to your hand during announcement and will result in no sheep since it will not be on the battlefield at resolution so the effect fails since it target is illegal.').

card_ruling('oxidda scrapmelter', '2011-01-01', 'This ability is mandatory. If you\'re the only player who controls any artifacts, you must target one of them.').

