% Rulings

card_ruling('naar isle', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('naar isle', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('naar isle', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('naar isle', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('nacatl hunt-pride', '2009-02-01', 'If either ability is activated after blockers have been declared, it won\'t cause those blocks to change.').
card_ruling('nacatl hunt-pride', '2009-02-01', 'If you attack with Nacatl Hunt-Pride, you get the chance to activate one of its abilities before blockers are declared.').

card_ruling('nacatl savage', '2009-02-01', '\"Protection from artifacts\" means the following: -- Nacatl Savage can\'t be blocked by artifact creatures. -- Nacatl Savage can\'t be equipped. It also can\'t be enchanted by Auras that have somehow become artifacts in addition to enchantments. -- Nacatl Savage can\'t be targeted by abilities from artifact sources. -- All damage that would be dealt to Nacatl Savage by artifact sources is prevented.').

card_ruling('nacatl war-pride', '2007-05-01', 'If it\'s impossible for Nacatl War-Pride to be blocked by exactly one creature, then the defending player may block it with multiple creatures or may leave it unblocked.').
card_ruling('nacatl war-pride', '2007-05-01', 'If Nacatl War-Pride has left the battlefield by the time the triggered ability resolves, the tokens will still enter the battlefield as copies of Nacatl War-Pride.').
card_ruling('nacatl war-pride', '2007-05-01', 'If Nacatl War-Pride becomes a copy of something else after the ability triggers but before it resolves, the tokens will enter the battlefield as copies of whatever the ex-Nacatl War-Pride is now. If Nacatl War-Pride became a copy of something else and then left the battlefield, the tokens will enter the battlefield as copies of whatever it was copying when it last existed on the battlefield.').
card_ruling('nacatl war-pride', '2007-05-01', 'The tokens enter the battlefield attacking, even if the attack couldn\'t legally be declared.').
card_ruling('nacatl war-pride', '2007-05-01', 'Putting an attacking creature onto the battlefield doesn\'t trigger \"When this creature attacks\" abilities. It also won\'t check attacking restrictions, costs, or requirements.').
card_ruling('nacatl war-pride', '2007-05-01', 'In a multiplayer game in which you\'re allowed to attack only one player each combat, the new creatures will be attacking the same player as Nacatl War-Pride. In a multiplayer game in which you may attack multiple players, you choose the player each new creature is attacking. It must be a player you\'re allowed to attack.').

card_ruling('nafs asp', '2004-10-04', 'If its damage gets redirected to its controller, it will still trigger the ability.').
card_ruling('nafs asp', '2004-10-04', 'The player can pay the {1} mana at any time after damage is done before the draw step of that player\'s turn. People commonly pay during upkeep.').
card_ruling('nafs asp', '2004-10-04', 'If it damages a player twice, they get to pay or take damage twice during their next draw step.').
card_ruling('nafs asp', '2004-10-04', 'The ability does not cause itself to trigger again. It cause loss of life, and not damage.').

card_ruling('nakaya shade', '2004-10-04', 'Each player gets the option to pay when the ability resolves.').

card_ruling('naked singularity', '2004-10-04', 'The ability generates a replacement effect.').
card_ruling('naked singularity', '2004-10-04', 'If a player uses a text-changing effect to make a land type be listed as producing two different colors, the player tapping the land for mana can choose to produce mana of either color. But if it produces more than one mana, all mana is of the same color.').
card_ruling('naked singularity', '2013-04-15', 'If a land has more than one basic land type, the player tapping the land for mana can choose to produce mana of either color. But if it produces more than one mana, all mana is of the same color.').

card_ruling('nalathni dragon', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('nalathni dragon', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('nalathni dragon', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('nalathni dragon', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('nameless inversion', '2007-10-01', 'If a creature loses all creature types but then gains a new creature type later in the turn, it will be that new creature type.').
card_ruling('nameless inversion', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('nameless race', '2004-10-04', 'If put onto the battlefield by a spell or ability, you pay life during the resolution of that spell or ability.').
card_ruling('nameless race', '2004-10-04', 'The life payment is made just as the card is being put onto the battlefield.').
card_ruling('nameless race', '2007-10-01', 'This currently has no creature type. It\'s Nameless.').

card_ruling('nantuko husk', '2015-06-22', 'You can sacrifice Nantuko Husk itself to activate its own ability. However, the only thing that will do is put Nantuko Husk into the graveyard.').

card_ruling('nantuko mentor', '2004-10-04', 'The value of X is determined when the ability resolves. The value will not change over time.').

card_ruling('nantuko monastery', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('nantuko monastery', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('nantuko shaman', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('nantuko shaman', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('nantuko shaman', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('nantuko shaman', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('nantuko shaman', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('nantuko shaman', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('nantuko shaman', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('nantuko shaman', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('nantuko shaman', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('nantuko shaman', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('nantuko vigilante', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('narcolepsy', '2010-06-15', 'Narcolepsy\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability triggers only if the enchanted creature is untapped as any player\'s upkeep begins, and (2) the ability does nothing if the enchanted creature is already tapped by the time it resolves.').
card_ruling('narcolepsy', '2010-06-15', 'Narcolepsy doesn\'t keep the enchanted creature continually tapped. The creature untaps during its controller\'s untap step as normal, then is tapped when Narcolepsy\'s ability resolves during that upkeep. The creature may be tapped in response (for example, if it has an activated ability with {T} in its cost).').

card_ruling('narcomoeba', '2007-05-01', 'If Narcomoeba is revealed or looked at while moving from your library to your graveyard (such as with Mind Funeral), its ability will trigger when it\'s put into your graveyard. It was still in your library while it was revealed or being looked at.').
card_ruling('narcomoeba', '2007-05-01', 'If Narcomoeba is removed from your graveyard before its ability resolves, it won\'t be put onto the battlefield.').

card_ruling('narset transcendent', '2015-02-25', 'When resolving Narset’s first ability, if the top card of your library is a creature or a land card, or if you don’t wish to put it into your hand, it isn’t revealed.').
card_ruling('narset transcendent', '2015-02-25', 'If a spell that exiles itself as part of its own resolution, such as Volcanic Vision, gains rebound, the ability that lets you cast the spell again won’t be created. The spell will simply be exiled.').

card_ruling('narset, enlightened master', '2014-09-20', 'The cards are exiled face up. Casting the noncreature cards exiled this way follows the normal rules for casting those cards. You must follow all applicable timing rules. For example, if one of the exiled cards is a sorcery card, you can cast it only during your main phase while the stack is empty.').
card_ruling('narset, enlightened master', '2014-09-20', 'You can’t play any land cards exiled with Narset.').
card_ruling('narset, enlightened master', '2014-09-20', 'Because you’re already casting the card using an alternative cost (by casting it without paying its mana cost), you can’t pay any other alternative costs for the card, including casting it face down using the morph ability. You can pay additional costs, such as kicker costs. If the card has any mandatory additional costs, you must pay those.').
card_ruling('narset, enlightened master', '2014-09-20', 'If the card has {X} in its mana cost, you must choose 0 as the value for X when casting it.').
card_ruling('narset, enlightened master', '2014-09-20', 'Any exiled cards you don’t cast remain in exile.').

card_ruling('natural affinity', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('natural balance', '2004-10-04', 'You are not required to find all the basic lands you are entitled to.').

card_ruling('natural emergence', '2004-10-04', 'You choose the enchantment to return on resolution of the triggered ability. This is not targeted.').
card_ruling('natural emergence', '2009-10-01', 'A noncreature permanent that turns into a creature is subject to the \"summoning sickness\" rule: It can only attack, and its {T} abilities can only be activated, if its controller has continuously controlled that permanent since the beginning of his or her most recent turn.').

card_ruling('natural end', '2012-05-01', 'If the artifact or enchantment is an illegal target when Natural End tries to resolve, Natural End will be countered and none of its effects will happen. You won\'t gain 3 life.').

card_ruling('natural order', '2004-10-04', 'The sacrifice of a green creature is part of the cost and is paid on casting. You do not have a choice to pay this cost zero times or more than one time in order to multiply the effect.').
card_ruling('natural order', '2004-10-04', 'You do not have to find a green creature card if you do not want to.').

card_ruling('natural selection', '2007-07-15', 'This is now targeted.').

card_ruling('nature demands an offering', '2010-06-15', 'The targeted opponent may choose a creature he or she controls, or may choose a creature one of your other opponents controls. The same is true for each of the other permanent types listed.').
card_ruling('nature demands an offering', '2010-06-15', 'The four permanents are put on top of their owners\' libraries in sequence. That means that a single artifact creature, for example, can\'t be chosen as both the creature and as the artifact.').
card_ruling('nature demands an offering', '2010-06-15', 'If there are no applicable choices for one of the permanent types, it\'s simply skipped. The process is still repeated for the other listed permanent types.').
card_ruling('nature demands an offering', '2010-06-15', 'If a player owns multiple permanents chosen this way, that player shuffles his or her library just once.').
card_ruling('nature demands an offering', '2010-06-15', 'The owner of a token permanent chosen this way still shuffles his or her library, even though the token ceases to exist.').
card_ruling('nature demands an offering', '2010-06-15', 'If one of the chosen permanents is controlled by an opponent but owned by you, you\'ll wind up shuffling your library.').

card_ruling('nature shields its own', '2010-06-15', 'You\'re the defending player if a creature is attacking you or a planeswalker you control.').
card_ruling('nature shields its own', '2010-06-15', 'For each unblocked creature attacking you, you must put a Plant token onto the battlefield blocking it, even if you don\'t want to.').
card_ruling('nature shields its own', '2010-06-15', 'The Plant token blocks the attacking creature even if the block couldn\'t legally be declared (for example, if the attacking creature has flying).').
card_ruling('nature shields its own', '2010-06-15', 'Putting a blocking creature onto the battlefield doesn\'t trigger \"Whenever a creature blocks\" abilities. It also won\'t check blocking restrictions, costs, or requirements.').
card_ruling('nature shields its own', '2010-06-15', 'Putting a blocking creature onto the battlefield will trigger \"When this creature becomes blocked by a creature\" abilities. It will also trigger \"When this creature becomes blocked\" abilities in this case, because the attacking creature had not yet been blocked that combat.').
card_ruling('nature shields its own', '2010-06-15', 'The last ability triggers only if four or more creatures were declared as attackers during your opponents\' declare attackers step. Only creatures attacking you are counted; creatures attacking your planeswalkers are not. The creatures may be controlled by different players.').

card_ruling('nature\'s blessing', '2008-10-01', 'This ability has no duration. The affected creature will have banding, first strike, or trample until the game ends, it leaves the battlefield, or some other effect causes it to lose its abilities.').

card_ruling('nature\'s chosen', '2004-10-04', 'It is put into the graveyard if you lose control of the creature.').

card_ruling('nature\'s claim', '2010-03-01', 'If the targeted artifact or enchantment is an illegal target by the time Nature\'s Claim resolves, the entire spell is countered. No one gains any life.').

card_ruling('nature\'s kiss', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('nature\'s kiss', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('nature\'s kiss', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('nature\'s kiss', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('nature\'s kiss', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('nature\'s lore', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('nature\'s lore', '2008-08-01', 'You can find any card with the land type Forest, including Snow-covered Forest or non-Basic lands like Tropical Island, Murmuring Bosk, or Stomping Ground. You can even find a card that is a land in addition to other types, such as Dryad Arbor.').

card_ruling('nature\'s panoply', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('nature\'s panoply', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('nature\'s panoply', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('nature\'s panoply', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('nature\'s panoply', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('nature\'s revolt', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('nature\'s will', '2004-12-01', 'The lands are tapped and untapped once each time combat damage is dealt (usually once each turn, but first strike, double strike, and multiple combat phases can change that), not once for each creature that deals combat damage.').

card_ruling('nature\'s wrath', '2004-10-04', 'A single permanent may cause both the second and third abilities to trigger. For example, Underground Sea is both an Island and a Swamp, and Skeleton Ship is both blue and black.').
card_ruling('nature\'s wrath', '2008-10-01', 'Nature\'s Wrath cares about who puts the permanent onto the battlefield, not under whose control the permanent enters the battlefield under.').

card_ruling('nausea', '2004-10-04', 'All creatures which are on the battlefield when it resolves are affected. Ones that enter the battlefield later in the turn are not.').

card_ruling('nav squad commandos', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('nav squad commandos', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('nav squad commandos', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('naya', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('naya', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('naya', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('naya', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('naya', '2009-10-01', 'If you play a land during your turn, you must declare whether you\'re using your standard land play or Naya\'s first ability (or another similar ability).').
card_ruling('naya', '2009-10-01', 'If you play lands using Naya\'s first ability, and then you planeswalk away from Naya, the lands you played still count as additional land plays for the turn. If you haven\'t already, you can still play a land using your standard land play.').
card_ruling('naya', '2009-10-01', 'The bonus granted by the {C} ability is determined at the time it resolves. It won\'t change if the number of lands you control changes later in the turn.').

card_ruling('naya charm', '2008-10-01', 'You can choose a mode only if you can choose legal targets for that mode. If you can\'t choose legal targets for any of the modes, you can\'t cast the spell.').
card_ruling('naya charm', '2008-10-01', 'While the spell is on the stack, treat it as though its only text is the chosen mode. The other two modes are treated as though they don\'t exist. You don\'t choose targets for those modes.').
card_ruling('naya charm', '2008-10-01', 'If this spell is copied, the copy will have the same mode as the original.').

card_ruling('naya sojourners', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('naya sojourners', '2009-05-01', 'The triggered ability acts as a cycle-triggered ability or as a leaves-the-battlefield ability, as appropriate. The player who controls the triggered ability is the player who cycled the Sojourner, or the player who last controlled the Sojourner on the battlefield.').
card_ruling('naya sojourners', '2009-05-01', 'If you cycle this card, the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('naya sojourners', '2009-05-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('naya sojourners', '2009-05-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('naya soulbeast', '2013-10-17', 'Naya Soulbeast’s last ability triggers while Naya Soulbeast is on the stack. After that ability resolves, players will be able to cast spells and activate abilities knowing how many counters Naya Soulbeast will enter the battlefield with if it resolves.').

card_ruling('near-death experience', '2010-06-15', 'This ability has an \"intervening \'if\' clause.\" That means (1) the ability triggers only if you have exactly 1 life as your upkeep begins, and (2) the ability does nothing if your life total is anything other than 1 by the time it resolves.').
card_ruling('near-death experience', '2010-06-15', 'In a Two-Headed Giant game, anything that cares about your life total checks your team\'s life total. (This is a change from previous rules.) You\'ll win the game if your team has exactly 1 life.').

card_ruling('nebuchadnezzar', '2009-10-01', 'You don\'t name a card until the ability resolves. Once you name a card, it\'s too late for the targeted opponent to respond.').
card_ruling('nebuchadnezzar', '2009-10-01', 'You may activate the ability only during your turn, but you may do so any time you have priority. (In other words, it\'s not restricted to only the times you could cast a sorcery.)').

card_ruling('necra sanctuary', '2004-10-04', 'It can make a player lose 0, 1, or 3 life, but never 4.').
card_ruling('necra sanctuary', '2004-10-04', 'One permanent that is both white and green will cause the player to be affected by the lose 3 life portion.').

card_ruling('necratog', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('necratog', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('necratog', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('necratog', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('necratog', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('necrite', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('necrogenesis', '2008-10-01', 'If the targeted card is removed from the graveyard before the ability resolves, the ability is countered. You won\'t get a Saproling token.').

card_ruling('necrologia', '2004-10-04', 'This spell is cast during the End step. Since you discard down to 7 cards in the cleanup step, this card is used before the discard.').

card_ruling('necromancer\'s covenant', '2009-05-01', 'The Zombie tokens are put onto the battlefield under your control.').
card_ruling('necromancer\'s covenant', '2009-05-01', 'Necromancer\'s Covenant grants lifelink to all Zombies you control, not just the tokens put onto the battlefield with its first ability.').

card_ruling('necromancer\'s magemark', '2006-02-01', 'Necromancer\'s Magemark returns only the creatures. The Auras attached to those creatures fall off and are put in their owner\'s graveyard.').
card_ruling('necromancer\'s magemark', '2006-02-01', 'Necromancer\'s Magemark\'s effect prevents the creature from ever reaching a graveyard, so abilities such as Haunt never trigger.').

card_ruling('necromancy', '2004-10-04', 'The bringing of the creature onto the battlefield and then putting Necromancy on it is all done as part of the resolution.').
card_ruling('necromancy', '2004-10-04', 'When putting a card onto the battlefield that requires a definition for its value or some other choice, you do what is needed to define the value or make the choice.').
card_ruling('necromancy', '2005-08-01', 'Necromancy enters the battlefield as an enchantment and then becomes an Enchant Creature Aura as a triggered ability upon entering the battlefield. It follows all the rules for Auras from then on.').
card_ruling('necromancy', '2008-04-01', 'If the creature card put onto the battlefield has Protection from Black (or anything that prevents this from legally being attached), this won\'t be able to attach to it. Then this will go to the graveyard as a State-Based Action, causing the creature to be sacrificed.').
card_ruling('necromancy', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('necromantic selection', '2014-11-07', 'Necromantic Selection doesn’t overwrite any of the creature’s colors or types. Rather, it adds another color and another creature type.').
card_ruling('necromantic selection', '2014-11-07', 'If the creature is normally colorless, it will simply become black. It can’t be both black and colorless.').

card_ruling('necromantic summons', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('necromantic thirst', '2005-10-01', 'The target is chosen just after any creatures dealt lethal damage at the same time that the enchanted creature dealt damage have been put into the graveyard. That might include the enchanted creature itself, if it had trample and was blocked, for example.').

card_ruling('necromaster dragon', '2015-02-25', 'Necromaster Dragon’s last ability will trigger once for each player it deals combat damage to. For each of these abilities, you may pay {2} only once.').

card_ruling('necropede', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('necropede', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('necropede', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('necropede', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('necropede', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('necropede', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('necroplasm', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('necroplasm', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('necroplasm', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('necropolis', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('necropolis fiend', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('necropolis fiend', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('necropolis fiend', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('necropolis fiend', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('necropolis fiend', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('necropolis regent', '2012-10-01', 'If that creature deals combat damage to a player at the same time it\'s dealt lethal damage (perhaps because it has trample and was blocked), it will die before the triggered ability resolves and puts +1/+1 counters on it.').

card_ruling('necropotence', '2004-10-04', 'Removing Necropotence from the battlefield will not stop the cards from the life-payment ability from being put into your hand in the end step.').

card_ruling('necrosavant', '2004-10-04', 'The ability is activated while this card is in the graveyard.').

card_ruling('necroskitter', '2008-08-01', 'It doesn\'t matter whose graveyard the creature is put into. It matters only that an opponent controlled it when it left the battlefield.').
card_ruling('necroskitter', '2008-08-01', 'It doesn\'t matter how many -1/-1 counters were on the creature when it was put into a graveyard from the battlefield, as long as it had at least one.').
card_ruling('necroskitter', '2008-08-01', 'The card returns to the battlefield without any -1/-1 counters on it (unless some other effect causes it to enter the battlefield with some).').

card_ruling('necrotic ooze', '2011-01-01', 'Necrotic Ooze gains only activated abilities. It doesn\'t gain keyword abilities (unless those keyword abilities are activated), triggered abilities, or static abilities.').
card_ruling('necrotic ooze', '2011-01-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities; they have colons in their reminder text.').
card_ruling('necrotic ooze', '2011-01-01', 'If an activated ability of a card in a graveyard references the card it\'s printed on by name, treat Necrotic Ooze\'s version of that ability as though it referenced Necrotic Ooze by name instead. For example, if Cudgel Troll (which says \"{G}: Regenerate Cudgel Troll\") is in a graveyard, Necrotic Ooze has the ability \"{G}: Regenerate Necrotic Ooze.\"').

card_ruling('necrotic plague', '2010-08-15', 'Necrotic Plague grants the sacrifice ability to the creature it\'s enchanting. That means it triggers at the beginning of the upkeep of the creature\'s controller (not necessarily Necrotic Plague\'s controller).').
card_ruling('necrotic plague', '2010-08-15', 'Necrotic Plague\'s last ability triggers when the enchanted creature is put into a graveyard for any reason, not just when it\'s sacrificed due to the ability it grants to that creature. It triggers if it and the creature it\'s enchanting are both put into the graveyard at the same time, or if the creature it\'s enchanting is put into the graveyard but Necrotic Plague isn\'t. (In the second case, Necrotic Plague is then put into the graveyard as a state-based action.)').
card_ruling('necrotic plague', '2010-08-15', 'The player that controls Necrotic Plague at the time the enchanted creature is put into a graveyard is the player who controls the last ability (even though that creature\'s controller is the one who chooses its target). The first player returns Necrotic Plague to the battlefield under his or her control, regardless of whose graveyard it was put into. Note that the ability is mandatory; a target must be chosen if able, and Necrotic Plague must be returned to the battlefield if able.').
card_ruling('necrotic plague', '2010-08-15', 'If no legal target can be chosen for Necrotic Plague\'s last ability, it just remains in its owner\'s graveyard. Similarly, if the targeted creature becomes an illegal target by the time the ability resolves, the ability is countered and Necrotic Plague remains in its owner\'s graveyard.').

card_ruling('necrotic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('necrotic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('nectar faerie', '2007-10-01', 'If you grant lifelink to a creature controlled by another player, that other player will gain life when the creature deals damage.').

card_ruling('needlebite trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('needlebite trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('needlebite trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('needlebite trap', '2009-10-01', 'Needlebite Trap\'s alternative cost condition checks only whether life was gained. It doesn\'t care whether life was also lost. For example, if an opponent gained 4 life and lost 6 life during the turn, that player will have a lower life total than he or she started the turn with -- but you can still cast Needlebite Trap by paying {B}.').

card_ruling('nefarious lich', '2004-10-04', 'If you were going to gain life and you also have a replacement that can turn card drawing into life gain on the battlefield, you can gain life by using the Lich to turn the original life gain into card drawing, and then using the other replacement to turn a card draw into life gain. Nefarious Lich will not replace the life gain with a card draw again because it has already acted on this event.').
card_ruling('nefarious lich', '2004-10-04', 'This card will not prevent you from losing the game due to zero or less life.').

card_ruling('nefarox, overlord of grixis', '2012-07-01', 'Nefarox\'s last ability will trigger and resolve before blockers are declared.').
card_ruling('nefarox, overlord of grixis', '2012-07-01', 'In a Two-Headed Giant game, you\'ll choose one player on the defending team to sacrifice a creature.').

card_ruling('negate', '2009-10-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('nekrataal', '2004-10-04', 'Nothing happens if there are no non-artifact, non-black creatures on the battlefield when it enters the battlefield.').

card_ruling('nemesis of mortals', '2013-09-15', 'Neither the cost-reducing ability of Nemesis of Mortals nor the condition of its monstrosity ability can reduce the colored mana requirement of those costs.').
card_ruling('nemesis of mortals', '2013-09-15', 'Once you declare you’re casting Nemesis of Mortals or activating its ability, it’s too late for players to try to change the number of creature cards in your graveyard to affect the cost of doing so.').
card_ruling('nemesis of mortals', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('nemesis of mortals', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('nemesis of mortals', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('nemesis of reason', '2009-05-01', 'If the defending player has fewer than ten cards in his or her library, that player\'s entire library is put into his or her graveyard.').

card_ruling('nemesis trap', '2010-03-01', 'If you cast Nemesis Trap for {B}{B}, you may still target any attacking creature, not just the white attacking creature.').
card_ruling('nemesis trap', '2010-03-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('nemesis trap', '2010-03-01', 'If you cast Nemesis Trap during your opponent\'s declare attackers step, you\'ll put the token onto the battlefield before the declare blockers step begins and you can block with it this combat. If you cast Nemesis Trap later in the combat phase a, you won\'t be able to block with the token.').
card_ruling('nemesis trap', '2010-03-01', 'The token you put onto the battlefield copies exactly what was printed on the exiled creature and nothing more (unless it was copying something else or it was a token; see below). It doesn\'t copy whether the exiled creature was tapped or untapped, whether it had any counters on it or Auras attached to it, or any non-copy effects that changed its power, toughness, types, color, or so on. For example, if Nemesis Trap targets an animated Celestial Colonnade, the token you put onto the battlefield will be a Celestial Colonnade that\'s just a land. (Because of this, Nemesis Trap has received minor errata to specify that you put a token, not a \"creature token,\" onto the battlefield.)').
card_ruling('nemesis trap', '2010-03-01', 'If the exiled creature was copying something else (for example, if it was a Jwari Shapeshifter), then your token enters the battlefield as a copy of whatever the exiled creature was copying.').
card_ruling('nemesis trap', '2010-03-01', 'If the exiled creature was a token, your token copies the original characteristics of that token as stated by the effect that put it onto the battlefield.').
card_ruling('nemesis trap', '2010-03-01', 'If the exiled creature has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('nemesis trap', '2010-03-01', 'Any enters-the-battlefield abilities of the exiled creature will trigger when the token is put onto the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').
card_ruling('nemesis trap', '2010-03-01', 'If the targeted creature is an illegal target by the time Nemesis Trap resolves, the entire spell is countered. You won\'t get a token.').

card_ruling('nephalia', '2012-06-01', 'Nephalia\'s first ability doesn\'t target any card, and players don\'t get priority to cast spells or activate abilities in between putting cards into your graveyard and choosing a card to return.').

card_ruling('nessian asp', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('nessian asp', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('nessian asp', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('nessian demolok', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('nessian demolok', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('nessian demolok', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('nessian demolok', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('nessian demolok', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('nessian game warden', '2014-04-26', 'The value of X is calculated as the ability resolves.').

card_ruling('nessian wilds ravager', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('nessian wilds ravager', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('nessian wilds ravager', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('nessian wilds ravager', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('nessian wilds ravager', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('nested ghoul', '2011-06-01', 'If multiple sources deal damage to Nested Ghoul simultaneously (because it was blocked by multiple creatures, for example), the ability will trigger for each source dealing damage to Nested Ghoul.').
card_ruling('nested ghoul', '2011-06-01', 'Nested Ghoul\'s ability will trigger if a source with infect deals damage to it.').

card_ruling('nether shadow', '2004-10-04', 'Note that bringing the Shadow back onto the battlefield from the graveyard is not a spell, it is an ability. It can\'t be countered with something that counters spells.').
card_ruling('nether shadow', '2004-10-04', 'Since it enters the battlefield due to triggering at the beginning of upkeep, it is not possible to get an infinite loop with four Nether Shadows.').
card_ruling('nether shadow', '2008-10-01', 'A card is \"above\" another card in your graveyard if it was put into that graveyard later.').
card_ruling('nether shadow', '2008-10-01', 'layers may not rearrange the cards in their graveyards.').
card_ruling('nether shadow', '2008-10-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('nether shadow', '2008-10-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('nether shadow', '2008-10-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('nether traitor', '2006-09-25', 'If Nether Traitor and another creature are put into your graveyard at the same time, Nether Traitor\'s ability won\'t trigger.').

card_ruling('nether void', '2004-10-04', 'This is not an additional cost, it is a triggered ability.').
card_ruling('nether void', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('nether void', '2009-10-01', 'The ability triggers whenever any player (including you) casts a spell of any type.').

card_ruling('netter en-dal', '2004-10-04', 'The ability only does something if used before attackers are declared during a turn. If used after the creature is declared as an attacker, nothing happens.').

card_ruling('nettle drone', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('nettle drone', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('nettle drone', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('nettle drone', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('nettle drone', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('nettlevine blight', '2007-10-01', 'Nettlevine Blight grants the triggered ability to the enchanted permanent, so \"you\" refers to that permanent\'s controller. The ability will trigger at the end of that player\'s turn, and that player chooses the new creature or land to attach Nettlevine Blight to. The player must choose a creature or land he or she controls.').

card_ruling('nettling imp', '2004-10-04', 'If the Imp leaves the battlefield before the end of the turn, the creature still is destroyed.').
card_ruling('nettling imp', '2004-10-04', 'You can use this effect on a creature you know won\'t be able to attack. For example, you can use it on a tapped creature.').
card_ruling('nettling imp', '2004-10-04', 'The creature is destroyed if it does not attack because it simply can\'t do so legally.').
card_ruling('nettling imp', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('neurok commando', '2011-06-01', 'Only one card is drawn no matter how much combat damage was dealt.').

card_ruling('neurok replica', '2011-01-01', 'You may target Neurok Replica with its own ability. However, if you do, the ability will be countered for having an illegal target.').

card_ruling('neurok transmuter', '2004-12-01', 'Neurok Transmuter\'s second ability removes the type \"artifact\" -- and any artifact subtypes -- from the artifact creature it targets. It doesn\'t remove any other types or any subtypes of other types, and it doesn\'t remove supertypes. If the artifact was legendary, it\'s still legendary, and the \"Legend rule\" applies normally.').
card_ruling('neurok transmuter', '2004-12-01', 'Equipment that stops being an artifact loses the subtype \"Equipment.\" Permanents without the subtype Equipment can\'t equip creatures. Of course, artifact creatures can\'t equip anything either.').
card_ruling('neurok transmuter', '2004-12-01', 'Neurok Transmuter\'s second ability interacts strangely with March of the Machines from the Mirrodin set. If an artifact is an artifact creature only because March of the Machines is on the battlefield and you then activate Neurok Transmuter\'s second ability on that artifact creature, the result is a permanent with no types whatsoever. Neurok Transmuter\'s ability removes the type \"artifact.\" March of the Machines depends on knowing what is and isn\'t an artifact. The permanent won\'t be an artifact when March of the Machine\'s effect is applied and therefore it won\'t be turned into a creature.').

card_ruling('nevermaker', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('nevermaker', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('nevermaker', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('nevermaker', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('nevermaker', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('nevermaker', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').

card_ruling('nevermore', '2011-09-22', 'No one can cast spells or activate abilities between the time a card is named and the time that Nevermore\'s ability starts to work.').
card_ruling('nevermore', '2011-09-22', 'Spells with the chosen name that somehow happen to already be on the stack when Nevermore enters the battlefield are not affected by Nevermore\'s ability.').
card_ruling('nevermore', '2011-09-22', 'Although the named card can\'t be cast, it can still be put onto the battlefield by a spell or ability (if it\'s a permanent card).').
card_ruling('nevermore', '2011-09-22', 'You can name either half of a split card, but not both. If you do so, that half (and both halves, if the split card has fuse) can\'t be cast. The other half is unaffected.').
card_ruling('nevermore', '2011-09-22', 'If you want to name a double-faced card, remember to name the front face of that card. (The back face can\'t be cast anyway.)').
card_ruling('nevermore', '2011-09-22', 'The named card can be cast again once Nevermore leaves the battlefield.').

card_ruling('nevinyrral\'s disk', '2005-08-01', 'Creatures can be regenerated although the Auras on the creatures will still be destroyed by the Disk.').
card_ruling('nevinyrral\'s disk', '2013-07-01', 'The destruction of the Disk is not a sacrifice. It is destroyed as part of the resolution if it is still on the battlefield. If an effect gives Nevinyrral\'s Disk indestructible or regenerates it, it won\'t be destroyed by its ability.').

card_ruling('new prahv guildmage', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('new prahv guildmage', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('new prahv guildmage', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('new prahv guildmage', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('new prahv guildmage', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('new prahv guildmage', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('nezumi bone-reader', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('nicol bolas', '2009-10-01', 'Nicol Bolas\'s third ability triggers whenever it deals damage to an opponent for any reason. It\'s not limited to combat damage.').

card_ruling('nicol bolas, planeswalker', '2009-02-01', 'The control-change effect has no duration. It will last until the game ends or the affected creature leaves the battlefield. It may be superseded by a later control-change effect.').
card_ruling('nicol bolas, planeswalker', '2012-07-01', 'If the damage from Nicol Bolas\'s third ability is prevented or redirected, the rest of the effect will still happen. Specifically, the number of cards discarded and permanents sacrificed isn\'t tied to the amount of damage dealt.').
card_ruling('nicol bolas, planeswalker', '2012-07-01', 'For the third ability, if the player is an illegal target when the ability tries to resolve, the ability is countered and none of its effects will happen. No damage will be dealt, no cards will be discarded, and no permanents will be sacrificed.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('nicol bolas, planeswalker', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('night soil', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('night soil', '2008-10-01', 'The two creature cards are exiled as a cost. No one can respond to this. If no graveyard has two creature cards in it, the cost can\'t be paid.').

card_ruling('night terrors', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('nightcreep', '2006-05-01', 'This effect overwrites all creatures\' colors. Creatures will be black instead of any other color or colors.').
card_ruling('nightcreep', '2006-05-01', 'This effect overwrites all lands\' land types. Lands will be Swamps instead of any other land types. This won\'t affect a land\'s supertypes (such as basic or legendary). Lands will lose their normal abilities and have the ability \"{T}: Add {B} to your mana pool\" instead.').
card_ruling('nightcreep', '2006-05-01', 'If a permanent is both a creature and a land, Nightcreep replaces all of its land types with Swamp, but it doesn\'t affect its creature types. The creature land loses all abilities generated by its rules text and its previous land types but retains any abilities granted to it by other effects. For example, an animated Blinkmoth Nexus becomes a black creature land with the land subtype Swamp whose only abilities are flying and \"{T}: Add {B} to your mana pool.\"').
card_ruling('nightcreep', '2006-05-01', 'A land or creature that enters the battlefield after Nightcreep resolves won\'t be affected by it.').

card_ruling('nighthaze', '2010-06-15', 'If the targeted creature is an illegal target by the time the ability resolves, the ability is countered. You won\'t draw a card.').

card_ruling('nighthowler', '2013-09-15', 'Nighthowler’s last ability functions only from the battlefield. In other zones, Nighthowler is a 0/0 enchantment creature.').
card_ruling('nighthowler', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nighthowler', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nighthowler', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nighthowler', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nighthowler', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nighthowler', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nighthowler', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nightmare', '2008-04-01', 'If you control 0 swamps, then the Nightmare has 0 toughness and will be put into its owner\'s graveyard as a state-based action right before the next player gains priority.').
card_ruling('nightmare', '2009-10-01', 'Nightmare\'s power and toughness changes as the number of Swamps you control changes.').
card_ruling('nightmare', '2013-07-01', 'The ability that defines Nightmare’s power and toughness works everywhere, not just on the battlefield.').
card_ruling('nightmare', '2013-07-01', 'Nightmare’s ability counts all lands you control with the subtype Swamp, not just ones named Swamp.').

card_ruling('nightmare incursion', '2008-08-01', 'The cards are exiled face up, so everyone can see what they are. (This is true of every effect that exiles cards, unless the effect specifically says it exiles those cards face down.)').

card_ruling('nightmare void', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('nightmare void', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('nightmare void', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').
card_ruling('nightmare void', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('nightmarish end', '2014-04-26', 'The value of X is calculated as the ability resolves. The effect won’t change later in the turn, even if the number of cards in your hand does.').

card_ruling('nightscape familiar', '2004-10-04', 'If a spell is both blue and red, you pay {1} less, not {2} less.').
card_ruling('nightscape familiar', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('nightscape familiar', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('nightscape familiar', '2004-10-04', 'The effect is cumulative.').
card_ruling('nightscape familiar', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('nightscape familiar', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('nightscape familiar', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('nightshade assassin', '2006-09-25', 'You choose a target creature whether or not you choose to reveal any black cards in your hand.').
card_ruling('nightshade assassin', '2006-09-25', 'You may choose to reveal X black cards in your hand, then choose X to be 0.').

card_ruling('nightshade schemers', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('nightshade schemers', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('nightshade schemers', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('nightshade schemers', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('nightshade schemers', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('nightshade schemers', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('nightshade seer', '2004-10-04', 'You can reveal zero cards to give -0/-0 to a creature.').

card_ruling('nightsky mimic', '2008-08-01', 'The ability triggers whenever you cast a spell that\'s both of its listed colors. It doesn\'t matter whether that spell also happens to be any other colors.').
card_ruling('nightsky mimic', '2008-08-01', 'If you cast a spell that\'s the two appropriate colors for the second time in a turn, the ability triggers again. The Mimic will once again become the power and toughness stated in its ability, which could overwrite power- and toughness-setting effects that have been applied to it in the meantime.').
card_ruling('nightsky mimic', '2008-08-01', 'Any other abilities the Mimic may have gained are not affected.').
card_ruling('nightsky mimic', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('nightsnare', '2015-06-22', 'If you don’t choose a nonland card for the player to discard, that player chooses which two cards he or she will discard.').

card_ruling('nightveil specter', '2013-01-24', 'The card is exiled face up. All players may look at it.').
card_ruling('nightveil specter', '2013-01-24', 'Playing a card exiled with Nightveil Specter follows all the normal rules for playing that card. You must pay its costs, and you must follow all timing restrictions, for example.').
card_ruling('nightveil specter', '2013-01-24', 'Nightveil Specter\'s last ability applies to cards exiled with that specific Nightveil Specter, not any other creature named Nightveil Specter. You should keep cards exiled by different Nightveil Specters separate.').

card_ruling('nihil spellbomb', '2011-01-01', 'You may activate Nihil Spellbomb\'s first ability targeting any player, even one whose graveyard has no cards in it.').

card_ruling('nihilith', '2007-05-01', 'If a token is put into an opponent\'s graveyard from the battlefield, or a copy of a spell is put into an opponent\'s graveyard from the stack, Nihilith\'s ability will not trigger, because the ability references “a card.”').
card_ruling('nihilith', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('nihilith', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('nihilith', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('nihilith', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('nihilith', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('nihilith', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('nihilith', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('nihilith', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('nihilith', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('nihilith', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('nim deathmantle', '2011-01-01', 'Once Nim Deathmantle returns a card from your graveyard to the battlefield, it will remain on the battlefield indefinitely, even if Nim Deathmantle becomes unattached from it.').
card_ruling('nim deathmantle', '2011-01-01', 'Nim Deathmantle\'s color-changing and type-changing effects override the equipped creature\'s previous colors and creature types. After Nim Deathmantle becomes equipped to a creature, that creature will be a black Zombie, not any other colors or creature types.').
card_ruling('nim deathmantle', '2011-01-01', 'Nim Deathmantle causes the equipped creature to be a black Zombie even if it didn\'t return that creature to the battlefield from the graveyard.').
card_ruling('nim deathmantle', '2011-01-01', 'Once Nim Deathmantle becomes unattached from a creature, its color-changing and type-changing effects stop affecting that creature. The creature will no longer be black and will no longer be a Zombie (unless its printed characteristics or some other effects still cause it to be black and/or a Zombie, of course). This is true even if Nim Deathmantle returned that creature to the battlefield from the graveyard.').
card_ruling('nim deathmantle', '2011-01-01', 'You choose whether to pay {4} as Nim Deathmantle\'s second ability resolves. Although players may respond to this ability, once it begins to resolve and you decide whether to pay, it\'s too late for players to respond.').
card_ruling('nim deathmantle', '2011-01-01', 'If the nontoken creature that caused Nim Deathmantle\'s second ability to trigger is somehow removed from your graveyard before that ability resolves, you may still pay {4} as it resolves. Even if you do, however, no card will be returned to the battlefield.').
card_ruling('nim deathmantle', '2011-01-01', 'Nim Deathmantle\'s second ability may return a card it can\'t equip to the battlefield. For example, if a nontoken artifact that\'s become a creature is put into your graveyard from the battlefield, Nim Deathmantle\'s second ability triggers. If you pay {4} as it resolves, you\'ll return that card to the battlefield. However, Nim Deathmantle can\'t equip it, so Nim Deathmantle remains attached to whatever it was already equipping (or, if it was unattached, it remains so). The same is true if a nontoken creature with protection from artifacts is put into your graveyard from the battlefield, for example.').
card_ruling('nim deathmantle', '2011-01-01', 'If multiple nontoken creatures are put into your graveyard from the battlefield at the same time, Nim Deathmantle\'s second ability triggers that many times. You put the triggered abilities on the stack in any order, so you\'ll determine in which order they resolve. If you pay {4} more than once, each card you paid {4} for will end up on the battlefield under your control, and Nim Deathmantle will end up attached to the last card that returned to the battlefield this way that it could equip.').

card_ruling('nimana sell-sword', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('nimbus naiad', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nimbus naiad', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nimbus naiad', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nimbus naiad', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nimbus naiad', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nimbus naiad', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nimbus naiad', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nin, the pain artist', '2011-09-22', 'You may choose a value for X that\'s greater than the creature\'s toughness. Its controller will draw that many cards.').
card_ruling('nin, the pain artist', '2011-09-22', 'The effect is not optional. The creature\'s controller can\'t choose to draw fewer than X cards.').
card_ruling('nin, the pain artist', '2011-09-22', 'If the creature is an illegal target when Nin\'s ability tries to resolve, it will be countered and none of its effects will happen. No player will draw cards.').
card_ruling('nin, the pain artist', '2011-09-22', 'If the ability resolves, but the damage is prevented or redirected, the controller of the targeted creature will draw X cards, regardless of how much damage was dealt or what the damage was ultimately dealt to.').
card_ruling('nin, the pain artist', '2011-09-22', 'If the damage is lethal, the cards are drawn before the creature is destroyed (the next time state-based actions are checked).').

card_ruling('nine-ringed bo', '2004-12-01', 'The damaged creature will be exiled even if Nine-Ringed Bo isn\'t on the battlefield when that creature would be put into a graveyard.').

card_ruling('nirkana assassin', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('nirkana assassin', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('nirkana assassin', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('nirkana cutthroat', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('nirkana cutthroat', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('nirkana cutthroat', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('nirkana cutthroat', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('nirkana cutthroat', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('nissa revane', '2009-10-01', 'You may activate Nissa\'s first ability even if you know you have no cards named Nissa\'s Chosen in your library. You must still shuffle your library in that situation.').
card_ruling('nissa revane', '2009-10-01', 'You may activate Nissa\'s second ability even if you control no Elves.').
card_ruling('nissa revane', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('nissa revane', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('nissa revane', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('nissa revane', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('nissa revane', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('nissa revane', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('nissa revane', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('nissa revane', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('nissa revane', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('nissa revane', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('nissa revane', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('nissa\'s expedition', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('nissa\'s expedition', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('nissa\'s expedition', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('nissa\'s expedition', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('nissa\'s expedition', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('nissa\'s expedition', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('nissa\'s expedition', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('nissa\'s pilgrimage', '2015-06-22', 'If you only find one basic Forest card, you’ll put it onto the battlefield tapped. You won’t be able to put it into your hand, even if you want to.').
card_ruling('nissa\'s pilgrimage', '2015-06-22', 'If the spell mastery ability applies, and you find three basic Forest cards, one of them will be put onto the battlefield tapped and two of them will be put into your hand.').
card_ruling('nissa\'s pilgrimage', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('nissa\'s revelation', '2015-06-22', 'If the top card of your library isn’t a creature card or it’s a creature card with power 0 or less, you won’t draw any cards. Otherwise, the first card you draw will be the card you revealed.').

card_ruling('nissa, sage animist', '2015-06-22', 'You can activate Nissa, Sage Animist’s second ability even if you already control an Ashaya, the Awoken World. Just after the second token is created, you’ll choose one to remain on the battlefield. The other will be put into your graveyard and subsequently cease to exist.').
card_ruling('nissa, sage animist', '2015-06-22', 'If a land becomes a creature but hasn’t continuously been under its controller’s control since that player’s most recent turn began, it won’t be able to attack and its abilities with {T} in the cost (including mana abilities) won’t be able to be activated. In other words, look at how long the permanent itself has been under your control, not how long it’s been a creature.').

card_ruling('nissa, vastwood seer', '2015-06-22', 'Nissa, Vastwood Seer is exiled as a result of her second triggered ability. If she enters the battlefield while you control seven or more lands, she won\'t automatically be exiled and transform.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'Each face of a double-faced card has its own set of characteristics: name, types, subtypes, power and toughness, loyalty, abilities, and so on. While a double-faced card is on the battlefield, consider only the characteristics of the face that’s currently up. The other set of characteristics is ignored. While a double-faced card isn’t on the battlefield, consider only the characteristics of its front face.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'The converted mana cost of a double-faced card not on the battlefield is the converted mana cost of its front face.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'The back face of a double-faced card doesn’t have a mana cost. A double-faced permanent with its back face up has a converted mana cost of 0. Each back face has a color indicator that defines its color.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'The back face of a double-faced card (in the case of Magic Origins, the planeswalker face) can’t be cast.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'Although the two rules are similar, the “legend rule” and the “planeswalker uniqueness rule” affect different kinds of permanents. You can control two of this permanent, one front face-up and the other back-face up at the same time. However, if the former is exiled and enters the battlefield transformed, you’ll then control two planeswalkers with the same subtype. You’ll choose one to remain on the battlefield, and the other will be put into its owner’s graveyard.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'A double-faced card enters the battlefield with its front face up by default, unless a spell or ability instructs you to put it onto the battlefield transformed, in which case it enters with its back face up.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'A Magic Origins planeswalker that enters the battlefield because of the ability of its front face will enter with loyalty counters as normal.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'In some rare cases, a spell or ability may cause one of these five cards to transform while it’s a creature (front face up) on the battlefield. If this happens, the resulting planeswalker won’t have any loyalty counters on it and will subsequently be put into its owner’s graveyard.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'You can activate one of the planeswalker’s loyalty abilities the turn it enters the battlefield. However, you may do so only during one of your main phases when the stack is empty. For example, if the planeswalker enters the battlefield during combat, there will be an opportunity for your opponent to remove it before you can activate one of its abilities.').
card_ruling('nissa, vastwood seer', '2015-06-22', 'If a double-faced card is manifested, it will be put onto the battlefield face down (this is also true if it’s put onto the battlefield face down some other way). Note that “face down” is not synonymous with “with its back face up.” A manifested double-faced card is a 2/2 creature with no name, mana cost, creature types, or abilities. While face down, it can’t transform. If the front face of a manifested double-faced card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced card on the battlefield can’t be turned face down.').

card_ruling('nissa, worldwaker', '2014-07-18', 'The type-changing effects of the first and third abilities don’t have a duration. They last indefinitely.').
card_ruling('nissa, worldwaker', '2014-07-18', 'If the land targeted by the first ability entered the battlefield this turn, it won’t be able to attack and its activated abilities with {T} in the cost (including its mana ability) can’t be activated (unless something gives it haste). The same is true for the lands put onto the battlefield with the third ability.').

card_ruling('nivix cyclops', '2013-04-15', 'Nivix Cyclops still has defender after its triggered ability resolves, although it will be able to attack.').

card_ruling('nivix guildmage', '2012-10-01', 'Nivix Guildmage\'s second ability can target (and copy) any instant or sorcery spell you control, not just one with targets.').
card_ruling('nivix guildmage', '2012-10-01', 'When the second ability resolves, it creates a copy of a spell. That copy is created on the stack, so it\'s not “cast.” Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('nivix guildmage', '2012-10-01', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('nivix guildmage', '2012-10-01', 'If the copied spell is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('nivix guildmage', '2012-10-01', 'If the copied spell has an X whose value was determined as it was cast (like Sphinx\'s Revelation does), the copy has the same value of X.').
card_ruling('nivix guildmage', '2012-10-01', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional or alternative costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if you copy a spell that you cast by paying its overload cost, the copy will resolve as though its overload cost had been paid as well.').

card_ruling('nivix, aerie of the firemind', '2006-02-01', '\"Until your next turn\" means the effect wears off as soon as your next turn starts, even before you untap. It\'s basically the same as \"until the end of the turn of the player who immediately precedes you.\"').
card_ruling('nivix, aerie of the firemind', '2006-02-01', 'The card is cast using all normal rules and restrictions for casting spells. Casting it means you\'ll have to pay its mana cost, and it will go to the graveyard after it resolves or is countered. The only thing that\'s different is you\'re casting it from the Exile zone.').
card_ruling('nivix, aerie of the firemind', '2006-02-01', 'If you don\'t cast the card, it stays exiled.').

card_ruling('nivmagus elemental', '2012-10-01', 'You exile an instant or sorcery spell you control from the stack, not an instant or sorcery card in any other zone.').
card_ruling('nivmagus elemental', '2012-10-01', 'Exiling a spell is not the same as countering it, although the spell won\'t resolve in both cases. You may exile a spell that can\'t be countered.').
card_ruling('nivmagus elemental', '2012-10-01', 'You can exile a copy of an instant or sorcery spell to activate Nivmagus Elemental\'s ability if you control the copy.').

card_ruling('nix', '2007-05-01', 'Nix can target any spell. The condition is checked only on resolution.').
card_ruling('nix', '2007-05-01', 'Nix doesn\'t check whether a spell\'s mana cost is 0. Rather, it checks how much mana was actually spent to cast it. It can counter a spell with mana cost 0 (such as Slaughter Pact), a spell cast via an alternative nonmana cost (such as Allosaurus Rider) or alternative cost of {0} (such as Basking Rootwalla), a spell whose cost to cast it was reduced to 0 (such as Frogmite), a spell cast without paying its cost due to an effect (such as suspend or Mindleech Mass\'s ability), or a copy of a spell, among other possibilities. It will not counter a spell that had an additional or alternative mana cost paid to cast it, regardless of that spell\'s converted mana cost (such as a face-down spell cast with morph, or an Ornithopter cast while Trinisphere is on the battlefield, for example).').
card_ruling('nix', '2007-05-01', 'Nix checks whether any player paid mana to cast the spell, not whether its current controller did. If Commandeer is cast on a spell, then Nix is cast on that spell, Nix won\'t counter it unless the player who originally cast that spell spent no mana to cast it.').

card_ruling('no mercy', '2004-10-04', 'If the damage to you is all prevented or redirected, then damage was not dealt to you, and this does not trigger.').

card_ruling('no rest for the wicked', '2007-07-15', 'It doesn\'t matter who controlled the creature cards when they were on the battlefield. As long as they were put into your graveyard, you get to put them into your hand.').
card_ruling('no rest for the wicked', '2007-07-15', 'If you sacrifice No Rest for the Wicked on the same turn you cast it, all creature cards that were put into your graveyard from the battlefield that turn will be returned to your hand, including any that were put into your graveyard before No Rest for the Wicked entered the battlefield.').

card_ruling('nobilis of war', '2008-08-01', 'This is a static ability. Attacking creatures you control will get +2/+0 from the moment they\'re declared as attackers (or enter the battlefield attacking) until the moment the combat phase ends, they\'re removed from combat, or Nobilis of War leaves the battlefield, whichever comes first.').

card_ruling('noble benefactor', '2008-04-01', 'The player whose turn it is searches his or her library (or chooses not to), then each other player in turn order does the same. Then the player whose turn it is shuffles his or her library (if that player searched), then each other player in turn order does the same.').

card_ruling('noble elephant', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('noble elephant', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('noble elephant', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('noble elephant', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('noble quarry', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('noble quarry', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('noble quarry', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('noble quarry', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('noble quarry', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('noble quarry', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('noble quarry', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('noble stand', '2004-10-04', 'Triggers once per creature you have that blocks. Does not trigger multiple times when a single creature blocks more than one attacker.').

card_ruling('nocturnal raid', '2004-10-04', 'On resolution, all black creatures on the battlefield get the bonus. Ones that enter the battlefield later in the turn do not get it.').

card_ruling('noetic scales', '2004-10-04', 'The number of cards in hand is counted on resolution of the ability.').

card_ruling('noggin whack', '2008-04-01', 'If the player has fewer than three cards in his or her hand, the player reveals all of them, you choose all of them, and they\'re all discarded.').

card_ruling('noggle hedge-mage', '2008-08-01', 'Each of the triggered abilities has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control two or more lands of the appropriate land type when the Hedge-Mage enters the battlefield, and (2) the ability will do nothing if you don\'t control two or more lands of the appropriate land type by the time it resolves.').
card_ruling('noggle hedge-mage', '2008-08-01', 'Both abilities trigger at the same time. You can put them on the stack in any order.').
card_ruling('noggle hedge-mage', '2008-08-01', 'Each of the triggered abilities look at your lands individually. This means that if you only control two dual-lands of the appropriate types, both of the abilities will trigger').
card_ruling('noggle hedge-mage', '2008-08-01', 'Both abilities are optional; you choose whether to use them when they resolve. If an ability has a target, you must choose a target even if you don\'t plan to use the ability.').

card_ruling('noggle ransacker', '2008-08-01', 'First the player whose turn it is draws two cards, then each other player in turn order does so. Then the player whose turn it is randomly selects a card from his or her hand, then each other player in turn order does so. Finally, all cards selected this way are discarded at the same time.').

card_ruling('nomad mythmaker', '2004-10-04', 'You don\'t choose which creature the Aura will enter the battlefield attached to until the ability resolves. You must choose a creature the Aura can legally enchant. (For example, you can\'t choose a creature with protection from black if the targeted Aura card is black.) If you don\'t control any creatures that the Aura can enchant, it remains in the graveyard.').
card_ruling('nomad mythmaker', '2007-07-15', 'Nomad Mythmaker\'s ability can target an Aura card in any graveyard, not just yours.').

card_ruling('nomads en-kor', '2004-10-04', 'The ability of this card does not do anything to stop Trample damage from being assigned to the defending player.').
card_ruling('nomads en-kor', '2004-10-04', 'It can redirect damage to itself.').
card_ruling('nomads en-kor', '2004-10-04', 'It is possible to redirect more damage to a creature than that creature\'s toughness.').
card_ruling('nomads en-kor', '2004-10-04', 'You can use this ability as much as you want prior to damage being dealt.').
card_ruling('nomads en-kor', '2013-07-01', 'When you redirect combat damage it is still combat damage.').

card_ruling('nomads\' assembly', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('nomads\' assembly', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('nomads\' assembly', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('nomads\' assembly', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('nomads\' assembly', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('nomads\' assembly', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('nomads\' assembly', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('nomads\' assembly', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').

card_ruling('norin the wary', '2006-09-25', 'Norin the Wary will be exiled if it attacks.').

card_ruling('norn\'s annex', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('norn\'s annex', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('norn\'s annex', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('norn\'s annex', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('norn\'s annex', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('norn\'s annex', '2011-06-01', 'The controller of each creature attacking you or a planeswalker you control pays either {W} or 2 life as attackers are being declared.').
card_ruling('norn\'s annex', '2011-06-01', 'If a player attacks with more than one creature, that player chooses how to pay each cost individually. For example, if you attack with two creatures, you may pay {W} for one cost and 2 life for the other.').
card_ruling('norn\'s annex', '2011-06-01', 'Multiple Norn\'s Annexes controlled by the same player will each impose a cost to attack. Players choose how they pay each cost individually. For example, if a creature you control is attacking a player who controls two Norn\'s Annexes, you may pay {W} for one cost and 2 life for the other.').
card_ruling('norn\'s annex', '2011-06-01', 'If you control Norn\'s Annex, your opponents can choose not to attack with a creature with an ability that says it must attack.').
card_ruling('norn\'s annex', '2011-06-01', 'In a Two-Headed Giant game, if one player controls Norn\'s Annex, creatures can\'t attack that player\'s team or a planeswalker that player controls unless their controller pays {W/P} for each of those creatures he or she controls. Creatures can attack planeswalkers controlled by that player\'s teammate without having to pay this cost.').

card_ruling('norn\'s dominion', '2012-06-01', 'Fate counters put on permanents in other ways (such as with Oblivion Stone) will cause those permanents to not be destroyed and will be removed when the first ability resolves.').

card_ruling('norritt', '2004-10-04', 'The creature is destroyed if it does not attack because it simply can\'t do so legally.').
card_ruling('norritt', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('norwood priestess', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('nosy goblin', '2004-10-04', 'If the target is turned face up before the ability resolves, the creature is no longer a legal target and is not destroyed.').

card_ruling('not of this world', '2010-06-15', 'The spell or ability you target with Not of This World may have any number of targets, as long as one of them is a permanent you control.').
card_ruling('not of this world', '2010-06-15', 'Not of This World\'s first ability checks the current state of the targets of the spell or ability it\'s targeting to determine whether one of them is a permanent you control. For example, say a player casts Forked Bolt targeting you and a creature you control. If, in response, another spell or ability causes that creature to no longer be under your control, then you can\'t cast Not of This World targeting Forked Bolt. Alternately, if you cast Not of This World targeting Forked Bolt and, in response, another spell or ability causes that creature to no longer be under your control, Not of This World will be countered when it tries to resolve for having an illegal target.').
card_ruling('not of this world', '2010-06-15', 'Not of This World\'s second ability checks the current state of the targets of the spell or ability it\'s targeting to determine whether one of them is a creature you control with power 7 or greater. This check is made as you cast Not of This World, before you activate mana abilities. It doesn\'t matter what the power of the creature was at the time the targeted spell or ability was cast.').
card_ruling('not of this world', '2010-06-15', 'Not of This World doesn\'t check whether the targets of the spell or ability it\'s targeting are currently legal for that spell or ability, only who controls them and what their power is. For example, say a player casts Forked Bolt targeting you and a creature you control. If, in response, another spell or ability causes that creature to gain shroud, then you can still cast Not of This World targeting Forked Bolt.').
card_ruling('not of this world', '2010-06-15', 'If a spell or ability targets you, but not a permanent you control, you can\'t cast Not of This World targeting it. Keep in mind that if a player casts a spell or activates an ability with the intent of dealing damage to a planeswalker you control, that spell or ability is actually targeting you, not that planeswalker.').

card_ruling('nothing can stop me now', '2010-06-15', 'The prevention effect prevents 1 damage from each source an opponent controls each time that source would deal damage to you. It prevents damage of any kind, not just combat damage.').
card_ruling('nothing can stop me now', '2010-06-15', 'The prevention effect doesn\'t affect damage dealt directly to a planeswalker you control (such as combat damage). It can prevent noncombat damage that\'s redirected from you to a planeswalker you control if you apply this effect first.').
card_ruling('nothing can stop me now', '2010-06-15', 'The last ability won\'t trigger at all unless, as an end step starts, you have already been dealt 5 or more damage that turn. It doesn\'t matter whether any of it was combat damage or not, nor does it matter who controlled the sources of that damage. (In other words, it will count damage dealt to you by sources you control.)').
card_ruling('nothing can stop me now', '2010-06-15', 'The effects from more than one of these schemes are cumulative.').

card_ruling('notion thief', '2013-04-15', 'If you and an opponent each control a Notion Thief, the replacement effects generated by their abilities will essentially cancel each other out. If the opponent would draw a card other than the first of his or her draw step, that draw will be skipped and you\'ll draw a card. The opponent\'s Notion Thief will then cause that draw to be skipped and the opponent will draw a card instead. Neither replacement effect can apply to this event again, so your opponent draws a card.').

card_ruling('notorious throng', '2008-04-01', 'To determine the value of X, this spell looks back over the course of the turn and counts all damage dealt by all sources to players who are currently your opponents, as well as damage dealt by all sources to players who were your opponents at the time they left the game.').

card_ruling('nova pentacle', '2004-10-04', 'In multi-player games you can choose a different opposing player each time it is used.').
card_ruling('nova pentacle', '2009-10-01', 'As you activate the ability, you choose an opponent, then that opponent chooses a creature for the ability to target. It doesn\'t have to be a creature that opponent controls. If there are no creatures that can be targeted with the ability, it can\'t be activated.').
card_ruling('nova pentacle', '2009-10-01', 'As the ability resolves, you choose a damage source.').
card_ruling('nova pentacle', '2009-10-01', 'The next time damage from the chosen source would be dealt to you this turn, that damage is dealt to the targeted creature instead. Whether it\'s still a legal target is not checked at this time. However, if it\'s not able to be dealt damage because it\'s neither a creature nor a planeswalker, or it\'s no longer on the battlefield, then the damage is not redirected and is dealt to you as normal.').

card_ruling('novablast wurm', '2010-03-01', 'Novablast Wurm\'s ability triggers and resolves during the declare attackers step, before blockers can be declared.').
card_ruling('novablast wurm', '2010-03-01', 'Novablast Wurm\'s ability destroys all players\' creatures (except Novablast Wurm), including yours.').
card_ruling('novablast wurm', '2010-03-01', 'If two Novablast Wurms attack, they\'ll destroy each other. This happens before they can deal combat damage.').

card_ruling('novijen sages', '2006-05-01', 'To activate the activated ability, either remove two +1/+1 counters from a single creature you control or remove one +1/+1 counter from each of two different creatures you control.').

card_ruling('novijen, heart of progress', '2006-05-01', 'The second ability puts a counter on each applicable creature regardless of who controls it.').

card_ruling('noxious dragon', '2014-11-24', 'A face-down creature has a converted mana cost of 0. However, its converted mana cost changes when it’s turned face up. Thus, if a creature is turned face up in response to being targeted by Noxious Dragon’s ability, it may become an illegal target.').

card_ruling('noxious field', '2004-10-04', 'Land are normally colorless, so effects that care about the color of the damage\'s source will not find a color.').

card_ruling('noxious hatchling', '2008-08-01', 'If you cast a spell that\'s both of the listed colors, both abilities will trigger. You\'ll remove a total of two -1/-1 counters from the Hatchling.').
card_ruling('noxious hatchling', '2008-08-01', 'If there are no -1/-1 counters on it when the triggered ability resolves, the ability does nothing. There is no penalty for not being able to remove a counter.').

card_ruling('noxious revival', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('noxious revival', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('noxious revival', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('noxious revival', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('noxious revival', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('noyan dar, roil shaper', '2015-08-25', 'Although Noyan Dar’s ability is very similar to awaken, it is not the same ability. Any card that refers to a “card with awaken” doesn’t include Noyan Dar.').
card_ruling('noyan dar, roil shaper', '2015-08-25', 'The ability will resolve before the instant or sorcery spell that caused it to trigger.').

card_ruling('null brooch', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('null chamber', '2006-02-01', 'If you somehow manage to cast this while other spells are on the stack, those spells won\'t be countered.').
card_ruling('null chamber', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('null chamber', '2013-04-15', 'You choose which opponent makes the choice. Null Chamber does not target that opponent.').

card_ruling('null champion', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('null champion', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('null champion', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('null champion', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('null champion', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('null profusion', '2007-02-01', 'The triggered ability will trigger when you play a land card or cast a nonland card as a spell. It won\'t trigger when you play a copy of a card, such as with Isochron Scepter.').
card_ruling('null profusion', '2007-02-01', 'Your maximum hand size is checked only during the cleanup step of your turn. At any other time, you may have any number of cards in hand.').
card_ruling('null profusion', '2007-02-01', 'This is the timeshifted version of Recycle.').
card_ruling('null profusion', '2007-02-01', 'Countering a spell that has been cast will not prevent you from drawing the card.').
card_ruling('null profusion', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Spellbook (an artifact that says you have no maximum hand size) onto the battlefield and then put Null Profusion onto the battlefield, your maximum hand size will be two. However, if those permanents entered the battlefield in the opposite order, you would have no maximum hand size.').

card_ruling('null rod', '2008-04-01', 'This covers only artifacts that are on the battlefield.').
card_ruling('null rod', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').
card_ruling('null rod', '2013-07-01', 'This effect applies to all activated abilities of artifacts, including mana abilities.').
card_ruling('null rod', '2013-07-01', 'This effect does not prevent triggered abilities from triggering and it doesn\'t stop the effects of static abilities.').

card_ruling('nullmage advocate', '2004-10-04', 'You can\'t activate this ability unless a single opponent has at least two cards in their graveyard to target.').

card_ruling('nullstone gargoyle', '2005-10-01', 'Nullstone Gargoyle cares about the first noncreature spell cast during a turn, not the first one it \"sees.\" If a noncreature spell was cast before Nullstone Gargoyle entered the battlefield, it won\'t counter the next one.').
card_ruling('nullstone gargoyle', '2005-10-01', 'If another spell is cast in response to the first spell, Nullstone Gargoyle doesn\'t affect that second spell.').

card_ruling('nulltread gargantuan', '2009-05-01', 'Nulltread Gargantuan\'s ability doesn\'t target the creature you put on top of its owner\'s library. You don\'t choose one until the ability resolves. No one can respond to the choice.').
card_ruling('nulltread gargantuan', '2009-05-01', 'If you control no other creatures, you\'ll have to put Nulltread Gargantuan on top of its owner\'s library.').

card_ruling('numbing dose', '2011-06-01', 'Numbing Dose may target and may enchant an untapped artifact or creature.').

card_ruling('numot, the devastator', '2007-02-01', 'You choose the targets when the ability is put on the stack, and you choose whether to pay when it resolves. You may choose zero targets. You can pay the mana even if you chose zero targets.').

card_ruling('nurturing licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('nurturing licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('nykthos, shrine to nyx', '2013-09-15', 'The second ability is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('nykthos, shrine to nyx', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('nykthos, shrine to nyx', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('nykthos, shrine to nyx', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('nykthos, shrine to nyx', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('nylea\'s disciple', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('nylea\'s disciple', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('nylea\'s disciple', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('nylea\'s disciple', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('nylea\'s emissary', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nylea\'s emissary', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nylea\'s emissary', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nylea\'s emissary', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nylea\'s emissary', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nylea\'s emissary', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nylea\'s emissary', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nylea\'s presence', '2013-09-15', 'The enchanted land will have the land types Plains, Island, Swamp, Mountain, and Forest. It will also have the mana ability of each basic land type (for example, Forests can tap to produce {G}). It still has its other subtypes and abilities.').
card_ruling('nylea\'s presence', '2013-09-15', 'Giving a land extra basic land types doesn’t change its name or whether it’s legendary or basic.').

card_ruling('nylea, god of the hunt', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('nylea, god of the hunt', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('nylea, god of the hunt', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').

card_ruling('nyx infusion', '2014-04-26', 'Nyx Infusion continuously checks to see whether the creature it’s enchanting is an enchantment or not. If the creature isn’t an enchantment and becomes one, or it is an enchantment and stops being one, the effect will change.').

card_ruling('nyxathid', '2009-02-01', 'Nyxathid tracks the hand size of the chosen player even if that player ceases to be an opponent of its controller. (This could happen if that player gains control of Nyxathid, for example.)').
card_ruling('nyxathid', '2009-02-01', 'In a multiplayer game, if the chosen player leaves the game, Nyxathid\'s second ability simply won\'t do anything anymore.').
card_ruling('nyxathid', '2009-02-01', 'Nyxathid\'s two abilities are linked: The second one refers only to the player chosen by the first one. If another creature becomes a copy of Nyxathid (due to Mirrorweave, for example), the second ability won\'t do anything because a player was never chosen as a result of the first ability. This is true even if a different ability allowed a player to be chosen as that creature entered the battlefield.').
card_ruling('nyxathid', '2009-02-01', 'If a spell or ability causes the chosen player to draw cards and then discard cards, Nyxathid\'s power and toughness changes accordingly as that spell or ability is resolving. For example, if the chosen player has five cards in hand and a spell causes that player to draw two cards then discard two cards, Nyxathid will start out as 2/2, then become 1/1, 0/0, and finally 2/2. This is because cards are always drawn one at a time, but multiple cards may be discarded at once. Although Nyxathid momentarily had 0 toughness while that spell was resolving, it isn\'t put into its owner\'s graveyard as a state-based action because state-based actions aren\'t checked until the spell has finished resolving.').

card_ruling('nyxborn eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nyxborn eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nyxborn eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nyxborn eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nyxborn eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nyxborn eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nyxborn eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nyxborn rollicker', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nyxborn rollicker', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nyxborn rollicker', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nyxborn rollicker', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nyxborn rollicker', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nyxborn rollicker', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nyxborn rollicker', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nyxborn shieldmate', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nyxborn shieldmate', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nyxborn triton', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nyxborn triton', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nyxborn triton', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nyxborn triton', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nyxborn triton', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nyxborn triton', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nyxborn triton', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('nyxborn wolf', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('nyxborn wolf', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('nyxborn wolf', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('nyxborn wolf', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('nyxborn wolf', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('nyxborn wolf', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('nyxborn wolf', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

