% Rulings

card_ruling('xanthic statue', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('xathrid demon', '2009-10-01', 'When the ability resolves, if you control any other creatures, you must sacrifice one of them. You lose life only if no creature can be sacrificed.').
card_ruling('xathrid demon', '2009-10-01', 'If you have two Xathrid Demons, both abilities will trigger at the beginning of your upkeep. When the first Demon\'s ability resolves, you can sacrifice the second Demon to satisfy the requirement (\"Sacrifice a creature other than Xathrid Demon\" really means \"Sacrifice a creature other than this creature\"). When the second Demon\'s ability resolves, you must sacrifice another creature. If you can\'t (because the first Demon has somehow left the battlefield and you control no other creatures), you lose 7 life, regardless of whether the second Demon is still on the battlefield at this time.').

card_ruling('xathrid gorgon', '2012-07-01', 'The effects of Xathrid Gorgon\'s ability don\'t depend on the petrification counter. If that counter is removed, the creature remains a colorless artifact with defender, and so on.').
card_ruling('xathrid gorgon', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('xathrid necromancer', '2013-07-01', 'If Xathrid Necromancer dies at the same time as another Human creature you control, its ability will trigger twice.').

card_ruling('xenagos, god of revels', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('xenagos, god of revels', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('xenagos, god of revels', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('xenagos, god of revels', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('xenagos, god of revels', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('xenagos, god of revels', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('xenagos, god of revels', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('xenagos, god of revels', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('xenagos, god of revels', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('xenagos, god of revels', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('xenagos, god of revels', '2014-02-01', 'The value of X is calculated only once, as the ability resolves.').

card_ruling('xenagos, the reveler', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('xenagos, the reveler', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('xenagos, the reveler', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('xenagos, the reveler', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('xenagos, the reveler', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('xenagos, the reveler', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('xenagos, the reveler', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('xenagos, the reveler', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('xenagos, the reveler', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('xenagos, the reveler', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('xenagos, the reveler', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').
card_ruling('xenagos, the reveler', '2013-09-15', 'Xenagos’s first ability isn’t a mana ability. It uses the stack and can be responded to. Count the number of creatures you control as the ability resolves to determine how much mana to add to your mana pool. You choose how much of that mana is red and how much is green at that time.').
card_ruling('xenagos, the reveler', '2013-09-15', 'Creature cards with bestow put onto the battlefield with the third ability will be creatures on the battlefield, not Auras.').

card_ruling('xenic poltergeist', '2004-10-04', 'Wears off at the beginning of the upkeep, not during upkeep.').
card_ruling('xenic poltergeist', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('xenograft', '2011-06-01', 'The choice of creature type is made when the enters-the-battlefield ability resolves.').
card_ruling('xenograft', '2011-06-01', 'You must choose an existing _Magic_ creature type.').
card_ruling('xenograft', '2011-06-01', 'Creature cards not on the battlefield and creature spells are not affected.').
card_ruling('xenograft', '2011-06-01', 'Replacement effects that modify creatures of a certain type as they enter the battlefield will apply (or not apply) before you apply Xenograft\'s effect. For example, if Warrior is the chosen creature type and you control Bramblewood Paragon, a Runeclaw Bear would not enter the battlefield with an additional +1/+1 counter.').

card_ruling('xiahou dun, the one-eyed', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').
card_ruling('xiahou dun, the one-eyed', '2009-10-01', 'You can\'t activate Xiahou Dun\'s ability to return Xiahou Dun itself to your hand. You must choose a target before you sacrifice Xiahou Dun, so it\'s not a black card in your graveyard yet.').
card_ruling('xiahou dun, the one-eyed', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('xun yu, wei advisor', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

