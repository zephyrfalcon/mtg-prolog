% Rulings

card_ruling('jabari\'s banner', '2008-04-01', 'Giving a creature flanking after blockers have been declared won\'t have any effect. Specifically: -- If you give flanking to an attacking creature after blockers have been declared, it\'s too late for flanking to trigger. -- If you give flanking to a creature that\'s blocking an attacking creature with flanking, it\'s too late to prevent the blocking creature from getting -1/-1, even if the attacker\'s flanking ability hasn\'t resolved yet.').

card_ruling('jace beleren', '2009-10-01', 'If there are fewer than twenty cards in the targeted player\'s library, that player puts all the cards from his or her library into his or her graveyard.').
card_ruling('jace beleren', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('jace beleren', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('jace beleren', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('jace beleren', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('jace beleren', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('jace beleren', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('jace beleren', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('jace beleren', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('jace beleren', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('jace beleren', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('jace beleren', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('jace\'s archivist', '2011-09-22', 'If this ability causes a player to draw more cards than are left in his or her library, that player loses the game as a state-based action. If this ability causes all players to do this, the game is a draw.').

card_ruling('jace\'s erasure', '2010-08-15', 'If a spell or ability causes you to draw multiple cards, Jace\'s Erasure\'s ability triggers that many times. (The spell or ability finishes resolving before you put Jace\'s Erasure\'s abilities on the stack.)').
card_ruling('jace\'s erasure', '2010-08-15', 'If a spell or ability causes you to put cards in your hand without specifically using the word \"draw,\" Jace\'s Erasure\'s ability won\'t trigger.').

card_ruling('jace\'s phantasm', '2012-07-01', 'The last ability constantly monitors each opponent\'s graveyard to see if the bonus applies. If the stated condition becomes no longer true, the bonus immediately stops applying.').
card_ruling('jace\'s phantasm', '2012-07-01', 'In a multiplayer game, a single opponent must have ten or more cards in his or her graveyard for the bonus to apply, although this opponent may change over the course of the game. You don\'t have to pick a single opponent; the ability will monitor each opponent\'s graveyard.').

card_ruling('jace\'s sanctum', '2015-06-22', 'The first ability of Jace’s Sanctum can’t reduce the colored mana requirement of an instant or sorcery spell.').
card_ruling('jace\'s sanctum', '2015-06-22', 'If there are additional costs to cast an instant or sorcery spell, apply those before applying cost reductions.').
card_ruling('jace\'s sanctum', '2015-06-22', 'Jace’s Sanctum can reduce alternative costs such as miracle or overload costs.').
card_ruling('jace\'s sanctum', '2015-06-22', 'Jace’s Sanctum’s scry ability will resolve before the instant or sorcery spell that caused it to trigger.').

card_ruling('jace, architect of thought', '2012-10-01', 'Jace\'s first ability creates a delayed triggered ability that triggers whenever a creature an opponent controls attacks. It doesn\'t matter which player or planeswalker that creature is attacking.').
card_ruling('jace, architect of thought', '2012-10-01', 'You pick one of your opponents when Jace\'s second ability resolves. The ability doesn\'t target that opponent. All players may see the revealed cards and offer opinions. You (not your opponent) choose which pile is put into your hand and which pile is put on the bottom of your library.').
card_ruling('jace, architect of thought', '2012-10-01', 'Piles can be empty. If one of the piles is empty, you choose to put all the revealed cards in your hand or on the bottom of your library.').
card_ruling('jace, architect of thought', '2012-10-01', 'When resolving Jace\'s third ability, you search each player\'s library (including yours) and exile the nonland cards before casting any of them.').
card_ruling('jace, architect of thought', '2012-10-01', 'For each library, the search is complete only when you explicitly say it is. For example, you can look through one player\'s library, set that library down, look at another player\'s library, choose a nonland card in the first library, then choose a nonland card in the second library. Don\'t reveal any cards from those libraries to any other player until you exile them.').
card_ruling('jace, architect of thought', '2012-10-01', 'You cast the cards by putting them on the stack one at a time, choosing modes, targets, and so on. The last card you cast will be the first one to resolve.').
card_ruling('jace, architect of thought', '2012-10-01', 'When casting a card this way, ignore timing restrictions based on the card\'s type. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('jace, architect of thought', '2012-10-01', 'If you can\'t cast a card, perhaps because there are no legal targets available, or if you choose not to cast one, it will remain exiled. Jace\'s ability won\'t allow you to cast it later.').
card_ruling('jace, architect of thought', '2012-10-01', 'If you cast a card “without paying its mana cost,” you can\'t pay alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('jace, architect of thought', '2012-10-01', 'If a card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('jace, architect of thought', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('jace, architect of thought', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('jace, architect of thought', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('jace, architect of thought', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('jace, architect of thought', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('jace, architect of thought', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('jace, architect of thought', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('jace, architect of thought', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('jace, architect of thought', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('jace, architect of thought', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('jace, architect of thought', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('jace, memory adept', '2011-09-22', 'If you target yourself with Jace\'s first ability, you\'ll draw a card first, then put the top card of your library into your graveyard.').
card_ruling('jace, memory adept', '2011-09-22', 'If you activate Jace\'s first ability, and the player is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').
card_ruling('jace, memory adept', '2011-09-22', 'If Jace\'s third ability causes a player to draw more cards than are left in his or her library, that player loses the game as a state-based action. If this ability causes all players to do this, the game is a draw.').
card_ruling('jace, memory adept', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('jace, memory adept', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('jace, memory adept', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('jace, memory adept', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('jace, memory adept', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('jace, memory adept', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('jace, memory adept', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('jace, memory adept', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('jace, memory adept', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('jace, memory adept', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('jace, memory adept', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('jace, telepath unbound', '2015-06-22', 'If you activate the second ability of Jace, Telepath Unbound, you must follow the timing rules for the card’s types. For example, if you target a sorcery card, you may cast it during your main phase when the stack is empty. You pay all the spell’s costs.').
card_ruling('jace, telepath unbound', '2015-06-22', 'If you don’t cast the card that turn, nothing happens. It remains in your graveyard.').
card_ruling('jace, telepath unbound', '2015-06-22', 'The card is exiled only if it’s cast from the graveyard and put back into the graveyard (either by resolving or being countered). If, at any time, the card goes to a hidden zone (such as your hand or your library), the effect loses track of the card. It won’t be exiled, even if that card is put into your graveyard later that turn.').

card_ruling('jace, the living guildpact', '2014-07-18', 'When the first ability resolves, the card you don’t put into your graveyard stays on top of your library.').
card_ruling('jace, the living guildpact', '2014-07-18', 'If a player has no cards in his or her hand and no cards in his or her graveyard when the third ability resolves, that player won’t shuffle his or her library.').

card_ruling('jace, the mind sculptor', '2010-03-01', 'Jace, the Mind Sculptor is the first planeswalker with a loyalty ability that costs [0]. Activating this ability follows the same rules as activating any other planeswalker ability, though you\'ll neither put loyalty counters on Jace nor remove loyalty counters from Jace to do so.').
card_ruling('jace, the mind sculptor', '2010-03-01', 'If you activate Jace\'s second ability, the two cards you put on top of your library may be any two cards from your hand. They don\'t have to be cards you drew with the ability.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('jace, the mind sculptor', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('jace, the mind sculptor', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'The activated ability of Jace, Vryn’s Prodigy checks to see if there are five or more cards in your graveyard after you discard a card. Putting a fifth card into your graveyard at other times won’t cause Jace to be exiled, nor will Jace entering the battlefield while there are five or more cards in your graveyard.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'Each face of a double-faced card has its own set of characteristics: name, types, subtypes, power and toughness, loyalty, abilities, and so on. While a double-faced card is on the battlefield, consider only the characteristics of the face that’s currently up. The other set of characteristics is ignored. While a double-faced card isn’t on the battlefield, consider only the characteristics of its front face.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'The converted mana cost of a double-faced card not on the battlefield is the converted mana cost of its front face.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'The back face of a double-faced card doesn’t have a mana cost. A double-faced permanent with its back face up has a converted mana cost of 0. Each back face has a color indicator that defines its color.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'The back face of a double-faced card (in the case of Magic Origins, the planeswalker face) can’t be cast.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'Although the two rules are similar, the “legend rule” and the “planeswalker uniqueness rule” affect different kinds of permanents. You can control two of this permanent, one front face-up and the other back-face up at the same time. However, if the former is exiled and enters the battlefield transformed, you’ll then control two planeswalkers with the same subtype. You’ll choose one to remain on the battlefield, and the other will be put into its owner’s graveyard.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'A double-faced card enters the battlefield with its front face up by default, unless a spell or ability instructs you to put it onto the battlefield transformed, in which case it enters with its back face up.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'A Magic Origins planeswalker that enters the battlefield because of the ability of its front face will enter with loyalty counters as normal.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'In some rare cases, a spell or ability may cause one of these five cards to transform while it’s a creature (front face up) on the battlefield. If this happens, the resulting planeswalker won’t have any loyalty counters on it and will subsequently be put into its owner’s graveyard.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'You can activate one of the planeswalker’s loyalty abilities the turn it enters the battlefield. However, you may do so only during one of your main phases when the stack is empty. For example, if the planeswalker enters the battlefield during combat, there will be an opportunity for your opponent to remove it before you can activate one of its abilities.').
card_ruling('jace, vryn\'s prodigy', '2015-06-22', 'If a double-faced card is manifested, it will be put onto the battlefield face down (this is also true if it’s put onto the battlefield face down some other way). Note that “face down” is not synonymous with “with its back face up.” A manifested double-faced card is a 2/2 creature with no name, mana cost, creature types, or abilities. While face down, it can’t transform. If the front face of a manifested double-faced card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced card on the battlefield can’t be turned face down.').

card_ruling('jackal familiar', '2009-10-01', '\"Can\'t attack alone\" means Jackal Familiar can\'t be declared as an attacker during the declare attackers step unless at least one other creature is also declared as an attacker at that time (either by you or your Two-Headed Giant teammate).').
card_ruling('jackal familiar', '2009-10-01', '\"Can\'t block alone\" has a similar meaning. Note that the other blocker(s) doesn\'t have to block the same attacker as Jackal Familiar.').
card_ruling('jackal familiar', '2009-10-01', 'Once Jackal Familiar has been legally declared as an attacking or blocking creature, how many other creatures it\'s attacking or blocking with no longer matters. For example, if Jackal Familiar and Canyon Minotaur both attack, then Canyon Minotaur leaves the battlefield, Jackal Familiar continues to attack even though it\'s now attacking alone.').
card_ruling('jackal familiar', '2009-10-01', 'An effect may put Jackal Familiar onto the battlefield attacking even if no other creatures are attacking. That\'s because it wasn\'t declared as an attacker in this case. Similarly, an effect may put Jackal Familiar onto the battlefield blocking even if no other creatures are blocking.').

card_ruling('jackalope herd', '2004-10-04', 'It will only return if it is still on the battlefield when the triggered ability resolves.').

card_ruling('jacques le vert', '2004-10-04', 'Gives himself +0/+2.').

card_ruling('jaddi lifestrider', '2010-06-15', 'You don\'t choose which creatures to tap until the ability resolves. You may tap Jaddi Lifestrider itself this way, assuming it\'s still on the battlefield untapped by this time.').

card_ruling('jaddi offshoot', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('jaddi offshoot', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('jaddi offshoot', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('jade idol', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('jade statue', '2004-10-04', 'It can only make itself into a creature during combat phases and therefore it is difficult to get Auras placed on it.').
card_ruling('jade statue', '2005-08-01', 'A delayed triggered ability that refers to Jade Statue still affects it even if it\'s no longer a creature. For example, if Jade Statue blocks the Kashi-Tribe Warriors (\"Whenever Kashi-Tribe Warriors deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.\"), the Warriors\' effect stops Jade Statue from untapping.').
card_ruling('jade statue', '2005-08-01', 'If Jade Statue\'s ability has been activated, abilities that trigger \"at end of combat\" will see Jade Statue as an artifact creature.').
card_ruling('jade statue', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('jade statue', '2013-09-20', 'If Jade Statue is animated by some other effect, you get an artifact creature with whatever power, toughness, and creature types (if any) are specified by that effect. If you subsequently use Jade Statue\'s ability during combat, it will become 3/6 and gain the creature type Golem in addition to those creature types until end of combat.').

card_ruling('jaded response', '2004-10-04', 'It does not check colors until it resolves.').

card_ruling('jalira, master polymorphist', '2014-07-18', 'If your library has no nonlegendary creature cards in it, you’ll reveal all the cards in your library, then put them in a random order. Although this is not technically shuffling your library, no player is allowed to know the order of those cards.').

card_ruling('jandor\'s ring', '2004-10-04', 'If you do not have the card still in your hand, you can\'t pay the cost. There is currently no way to prove that it was the card you drew except to get a judge or 3rd party involved, or to put cards you draw aside until you decide whether or not to use this.').
card_ruling('jandor\'s ring', '2004-10-04', 'If you draw more than one card due to a spell or ability, you must discard the last one of those drawn.').

card_ruling('jarad\'s orders', '2012-10-01', 'You can choose to find just one creature card. If you do, you\'ll put that card into your hand.').

card_ruling('jarad, golgari lich lord', '2012-10-01', 'Jarad\'s first ability applies only when Jarad is on the battlefield.').
card_ruling('jarad, golgari lich lord', '2012-10-01', 'The life loss is based on the power of the sacrificed creature as it last existed on the battlefield.').
card_ruling('jarad, golgari lich lord', '2012-10-01', 'To activate Jarad\'s last ability, you must sacrifice two lands. It doesn\'t matter what other land types the two lands have, as long as one is a Swamp and one is a Forest. Even if one of the lands you sacrifice is both a Swamp and a Forest, you have to sacrifice another land that is a Swamp or a Forest.').
card_ruling('jarad, golgari lich lord', '2012-10-01', 'You can sacrifice any land with the land type Swamp or Forest, not just ones named Swamp or Forest. For example, you could sacrifice two Overgrown Tombs.').

card_ruling('jasmine seer', '2004-10-04', 'You can reveal zero cards and gain zero life.').

card_ruling('jaws of stone', '2008-05-01', 'The amount of damage is locked in as you cast Jaws of Stone. It won\'t change, even if the number of Mountains you control changes.').
card_ruling('jaws of stone', '2008-05-01', 'If you control no Mountains as you cast Jaws of Stone, the number of targets must be zero.').
card_ruling('jaws of stone', '2008-05-01', 'If you control any Mountains as you cast Jaws of Stone, the number of targets must be at least one and at most X. You divide the damage among the targets as you cast Jaws of Stone. Each target must be assigned at least 1 damage.').

card_ruling('jedit ojanen of efrava', '2007-02-01', 'If Jedit Ojanen of Efrava attacks or blocks and a token is created, it will be too late for that token to be declared as an attacker (if it somehow gets haste) or a blocker during that combat phase.').

card_ruling('jeering instigator', '2014-09-20', 'Jeering Instigator’s last ability can target any creature, even one that’s untapped or one you already control.').
card_ruling('jeering instigator', '2014-09-20', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to that creature.').
card_ruling('jeering instigator', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('jeering instigator', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('jeering instigator', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('jeering instigator', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('jeering instigator', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('jeering instigator', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('jeering instigator', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('jeering instigator', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('jeering instigator', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('jeskai elder', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('jeskai elder', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('jeskai elder', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('jeskai infiltrator', '2014-11-24', 'The pile is shuffled to disguise from your opponents which manifested creature is which. After you manifest the cards, you can look at them.').
card_ruling('jeskai infiltrator', '2014-11-24', 'If Jeskai Infiltrator isn’t on the battlefield as its triggered ability resolves, you’ll manifest just the top card of your library.').
card_ruling('jeskai infiltrator', '2014-11-24', 'A card’s owner is public information at all times. If the two cards you exile are owned by different players (perhaps because you gained control of a Jeskai Infiltrator owned by your opponent), which card is which is no longer hidden from your opponent. That player will know which face-down creature he or she owns.').
card_ruling('jeskai infiltrator', '2014-11-24', 'If you manifest a card owned by an opponent and you leave the game, that card is exiled.').
card_ruling('jeskai infiltrator', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('jeskai infiltrator', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('jeskai infiltrator', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('jeskai infiltrator', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('jeskai infiltrator', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('jeskai infiltrator', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('jeskai infiltrator', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('jeskai infiltrator', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('jeskai runemark', '2014-11-24', 'Whether a creature has flying is checked as blocking creatures are declared. Once the enchanted creature blocks or is blocked, losing flying won’t change or undo that block.').

card_ruling('jeskai sage', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('jeskai sage', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('jeskai sage', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('jeskai sage', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('jeskai student', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('jeskai student', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('jeskai student', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('jeskai windscout', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('jeskai windscout', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('jeskai windscout', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('jester\'s cap', '2004-10-04', 'If the player has less than 3 cards in their library, just exile all the cards that are there.').

card_ruling('jester\'s scepter', '2006-07-15', 'You can see the cards that are exiled with Jester\'s Scepter, but no one else can.').
card_ruling('jester\'s scepter', '2006-07-15', 'Because the cards that are exiled are not hidden to you, you can choose which one to put into its owner\'s graveyard when you activate the second ability.').
card_ruling('jester\'s scepter', '2006-07-15', 'When Jester\'s Scepter\'s second ability is activated, any of the exiled cards may be put into its owner\'s graveyard, and any spell may be targeted, whether or not the card and the spell have the same name. If they don\'t have the same name, the effect does nothing. The spell\'s controller and the owner of the card put into a graveyard this way may be different.').
card_ruling('jester\'s scepter', '2006-07-15', 'If half of a split card is cast (for example, Hit), and you put one of those split cards into its owner\'s graveyard with Jester\'s Scepter (for example, Hit/Run), the spell is countered.').

card_ruling('jet medallion', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('jet medallion', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('jet medallion', '2004-10-04', 'The effect is cumulative.').
card_ruling('jet medallion', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('jet medallion', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('jet medallion', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('jeweled amulet', '2008-10-01', 'The types of mana are white, blue, black, red, green, and colorless.').
card_ruling('jeweled amulet', '2008-10-01', 'Charge counters are indistinguishable from one another. If some other spell or ability (such as Power Conduit\'s ability) puts a charge counter on Jeweled Amulet, activating Jeweled Amulet\'s second ability will still check the type of mana last noted by its first ability. If there is no type of mana noted by its first ability (because it hasn\'t been activated by that point), the second ability will produce no mana.').

card_ruling('jeweled bird', '2004-10-04', 'The card is exchanged for your entire contribution to the ante. This means that it replaces all the cards if you have more than one already contributed.').

card_ruling('jeweled spirit', '2004-10-04', 'You choose the color (or artifacts) on resolution.').

card_ruling('jhessian balmgiver', '2009-02-01', 'After blockers have been declared, activating the second ability won\'t cause those blocks to change.').

card_ruling('jhessian thief', '2015-06-22', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('jhessian thief', '2015-06-22', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('jhessian thief', '2015-06-22', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('jhessian zombies', '2009-05-01', 'Unlike the normal cycling ability, landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a land card of the specified land type, reveal it, put it into your hand, then shuffle your library.').
card_ruling('jhessian zombies', '2009-05-01', 'Landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being landcycled. Any ability that stops a cycling ability from being activated also stops a landcycling ability from being activated.').
card_ruling('jhessian zombies', '2009-05-01', 'You can choose to find any card with the appropriate land type, including nonbasic lands. You can also choose not to find a card, even if there is a land card with the appropriate type in your library.').
card_ruling('jhessian zombies', '2009-05-01', 'A landcycling ability lets you search for any card in your library with the stated land type. It doesn\'t have to be a basic land.').
card_ruling('jhessian zombies', '2009-05-01', 'You may only activate one landcycling ability at a time. You must specify which landcycling ability you are activating as you cycle this card, not as the ability resolves.').

card_ruling('jhoira of the ghitu', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('jhoira of the ghitu', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('jihad', '2004-10-04', 'In a multi-player game, it tracks only the chosen player. This is not changed even if this card changes controllers.').

card_ruling('jin-gitaxias, core augur', '2011-06-01', 'Jin-Gitaxias doesn\'t affect the maximum hand size of its controller.').
card_ruling('jin-gitaxias, core augur', '2011-06-01', 'If a player has more cards in his or her hand than his or her maximum hand size during the cleanup step (after the end step) of that player\'s turn, that player discards until he or she has that many cards. A player\'s maximum hand size isn\'t checked at any time other than his or her own cleanup step.').
card_ruling('jin-gitaxias, core augur', '2011-06-01', 'Unless another spell or ability is affecting your opponent\'s maximum hand size, Jin-Gitaxias\'s ability will result in your opponent having a maximum hand size of zero. He or she will discard each card from his or her hand during the cleanup step at the end of his or her turn.').

card_ruling('jinxed choker', '2004-12-01', '\"You\" is always Jinxed Choker\'s current controller.').
card_ruling('jinxed choker', '2004-12-01', 'If you activate Jinxed Choker\'s activated ability, you choose to either add or remove a counter when the ability resolves.').

card_ruling('jinxed idol', '2004-10-04', 'Your opponent can use the ability to give this card back to you.').
card_ruling('jinxed idol', '2010-08-15', 'The control-change effect has no duration. The targeted player retains control of Jinxed Idol until the game ends, Jinxed Idol leaves the battlefield, or an effect causes someone else to gain control of it (perhaps because Jinxed Idol\'s ability is activated again).').
card_ruling('jinxed idol', '2010-08-15', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that creature is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('join the ranks', '2010-03-01', 'When a Join the Ranks you control resolves, each ability that triggers whenever an Ally enters the battlefield under your control will trigger twice.').

card_ruling('joiner adept', '2004-12-01', 'Each land you control gets this ability.').

card_ruling('joint assault', '2012-05-01', 'Joint Assault targets only one creature. If that creature is an illegal target when Joint Assault tries to resolve, it will be countered and none of its effects will happen. No creature will get +2/+2.').
card_ruling('joint assault', '2012-05-01', 'Joint Assault checks whether the target creature is paired only when it resolves. If it becomes unpaired later in the turn, neither creature will lose the bonus. Similarly, if it becomes paired later in the turn, the creature it\'s paired with won\'t gain the bonus.').

card_ruling('jolrael, empress of beasts', '2004-10-04', 'Only affects lands the target player controls when the ability resolves.').
card_ruling('jolrael, empress of beasts', '2004-10-04', 'The lands do not get a creature type.').
card_ruling('jolrael, empress of beasts', '2004-10-04', 'The lands retain their abilities.').
card_ruling('jolrael, empress of beasts', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('joraga bard', '2009-10-01', 'The ability affects only Allies you control at the time the ability resolves.').

card_ruling('joraga invocation', '2015-06-22', 'If multiple attacking creatures must be blocked if able, the defending player must assign at least one blocker to each of them if possible. For example, if two such creatures were attacking and there were two potential blockers, they couldn’t both be assigned to block the same attacker.').
card_ruling('joraga invocation', '2015-06-22', 'Joraga Invocation doesn’t force any specific creature to block any specific attacking creature. The defending player still chooses how creatures he or she controls block.').

card_ruling('joraga treespeaker', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('joraga treespeaker', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('joraga treespeaker', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('joraga treespeaker', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('joraga treespeaker', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('joraga treespeaker', '2010-06-15', 'If Joraga Treespeaker is level 5 or greater, it grants the mana ability to each Elf you control, including itself.').

card_ruling('joraga warcaller', '2010-03-01', 'The last ability counts the total number of +1/+1 counters on Joraga Warcaller, not just the ones placed on it as a result of its enters-the-battlefield ability.').

card_ruling('jorubai murk lurker', '2014-07-18', 'Multiple instances of lifelink on a creature are redundant. Giving the same creature lifelink more than once won’t cause its controller to gain additional life.').

card_ruling('journey of discovery', '2004-12-01', 'If you cast an entwined Journey of Discovery using Vedalken Orrery during an opponent\'s turn, you can\'t play two lands during that turn.').

card_ruling('journey to nowhere', '2009-10-01', 'If Journey to Nowhere leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and exile the targeted creature forever.').

card_ruling('joven\'s ferrets', '2008-10-01', 'The second ability will trigger only if Joven\'s Ferrets is still on the battlefield at end of combat. If it\'s been dealt lethal combat damage and destroyed during that combat (or has left the battlefield during that combat by any other means), its ability won\'t trigger and the creatures that blocked it won\'t be affected.').
card_ruling('joven\'s ferrets', '2008-10-01', 'If a creature affected by the second ability is untapped at the time its controller\'s next untap step begins, the \"doesn\'t untap\" effect doesn\'t do anything. It won\'t apply at some later time when that creature is tapped.').
card_ruling('joven\'s ferrets', '2008-10-01', 'The second ability doesn\'t track the creatures\' controllers. If an affected creature changes controllers before its old controller\'s next untap step, Joven\'s Ferrets will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('judge of currents', '2007-10-01', 'This is a triggered ability, not an activated ability. It doesn\'t allow you to tap the creature whenever you want; rather, you need some other way of tapping it, such as by attacking with the creature.').
card_ruling('judge of currents', '2007-10-01', 'For the ability to trigger, the creature has to actually change from untapped to tapped. If an effect attempts to tap the creature, but it was already tapped at the time, this ability won\'t trigger.').

card_ruling('jugan, the rising star', '2013-06-07', 'You announce the target creatures and how you\'re distributing the +1/+1 counters as you put the triggered ability on the stack. If one of the target creatures becomes illegal, you can\'t change how the counters will be distributed.').

card_ruling('juggernaut', '2010-08-15', 'If, during your declare attackers step, Juggernaut is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then it doesn\'t attack. If there\'s a cost associated with having Juggernaut attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('juggernaut', '2010-08-15', 'If there are multiple combat phases in a turn, Juggernaut must attack only in the first one in which it\'s able to.').
card_ruling('juggernaut', '2014-07-18', 'An effect that says “can’t be blocked except by Walls” (for example, the effect of Invisibility) doesn’t override the text on Juggernaut’s last ability. If Juggernaut is affected by such an effect, it can’t be blocked.').

card_ruling('juju bubble', '2004-10-04', 'It does not matter if the card is played from your hand or from somewhere else.').

card_ruling('jump', '2009-10-01', 'To work as an evasion ability, an attacking creature must already have flying when the declare blockers step begins. Once a creature has become blocked, giving it flying won\'t change that.').

card_ruling('jund', '2012-06-01', 'Casting a multicolor spell will cause the first ability to trigger as long as that spell is black, red, or green. It can be other colors as well.').
card_ruling('jund', '2012-06-01', 'If the creature spell already has devour, you may sacrifice creatures using either ability. Each creature you sacrifice counts for only one devour ability. Under most circumstances, you\'ll pick the devour ability that gives the greatest number of +1/+1 counters.').

card_ruling('jund charm', '2008-10-01', 'You can choose a mode only if you can choose legal targets for that mode. If you can\'t choose legal targets for any of the modes, you can\'t cast the spell.').
card_ruling('jund charm', '2008-10-01', 'While the spell is on the stack, treat it as though its only text is the chosen mode. The other two modes are treated as though they don\'t exist. You don\'t choose targets for those modes.').
card_ruling('jund charm', '2008-10-01', 'If this spell is copied, the copy will have the same mode as the original.').

card_ruling('jund sojourners', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('jund sojourners', '2009-05-01', 'The triggered ability acts as a cycle-triggered ability or as a leaves-the-battlefield ability, as appropriate. The player who controls the triggered ability is the player who cycled the Sojourner, or the player who last controlled the Sojourner on the battlefield.').
card_ruling('jund sojourners', '2009-05-01', 'If you cycle this card, the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('jund sojourners', '2009-05-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('jund sojourners', '2009-05-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('jungle basin', '2006-02-01', 'This has a triggered ability when it enters the battlefield, not a replacement effect, as previously worded.').

card_ruling('jungle weaver', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('juniper order advocate', '2008-10-01', 'If Juniper Order Advocate somehow becomes green, its ability will affect itself.').

card_ruling('juniper order ranger', '2006-07-15', 'The creature doesn\'t enter the battlefield with a +1/+1 counter on it. It enters the battlefield, then the ability triggers. If either that creature or Juniper Order Ranger leaves the battlefield before the ability resolves, the remaining creature will still get a +1/+1 counter.').

card_ruling('justice', '2004-10-04', 'If a single source does damage to multiple targets at once, Justice will add up all the damage done and deal damage to the source\'s controller at one time (not multiple separate damagings).').
card_ruling('justice', '2004-10-04', 'The damage done by Justice is done as a triggered ability, which is added to the stack just after the red creature or spell deals its damage.').

card_ruling('juxtapose', '2007-09-16', 'First creatures are exchanged, then artifacts are exchanged. It\'s possible that the same artifact creature may be involved in both exchanges.').
card_ruling('juxtapose', '2007-09-16', 'No permanents are targeted by this effect, so permanents with Shroud may be exchanged this way.').
card_ruling('juxtapose', '2007-09-16', 'If one of the players doesn\'t control a creature at the time the exchange would be made, that part of the effect does nothing, but the exchange of artifacts will still happen. Similarly, if one of the players doesn\'t control an artifact at the time the exchange would be made, that part of the effect does nothing.').

card_ruling('jwari shapeshifter', '2010-03-01', 'Jwari Shapeshifter copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('jwari shapeshifter', '2010-03-01', 'If the chosen creature is copying something else (for example, if the chosen creature is another Jwari Shapeshifter), then your Jwari Shapeshifter enters the battlefield as whatever the chosen creature copied.').
card_ruling('jwari shapeshifter', '2010-03-01', 'If the chosen creature is a token, your Jwari Shapeshifter copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Your Jwari Shapeshifter is not a token.').
card_ruling('jwari shapeshifter', '2010-03-01', 'Any enters-the-battlefield abilities of the copied creature will trigger when Jwari Shapeshifter enters the battlefield.').
card_ruling('jwari shapeshifter', '2010-03-01', 'You can choose not to copy anything. In that case, Jwari Shapeshifter enters the battlefield as a 0/0 creature, and is probably put into the graveyard immediately.').

card_ruling('jötun grunt', '2006-07-15', 'When paying Jötun Grunt\'s cumulative upkeep, you may choose a different graveyard for each age counter. For example, if it has three age counters on it, you may put four cards from your graveyard on the bottom of your library and two cards from your opponent\'s graveyard on the bottom of his or her library. However, you can\'t put five cards from your graveyard on the bottom of your library and one card from your opponent\'s graveyard on the bottom of his or her library.').

