% Rulings

card_ruling('sabertooth cobra', '2004-10-04', 'The ability still gives a poison counter even if Sabertooth Cobra leaves the battlefield before the next upkeep.').

card_ruling('sabertooth outrider', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('sabertooth outrider', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('sabertooth outrider', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('sacellum godspeaker', '2008-10-01', 'This is a mana ability. It doesn\'t use the stack, and it can\'t be responded to.').
card_ruling('sacellum godspeaker', '2008-10-01', 'You can activate this ability even if you have no creature cards with power 5 or greater in your hand, or you have some but choose not to reveal any of them. In those cases, the ability won\'t produce any mana.').
card_ruling('sacellum godspeaker', '2008-10-01', 'While you\'re casting a creature spell, that card is on the stack, so you can\'t reveal it with Sacellum Godspeaker\'s ability to produce mana. You can, however, activate this mana ability and reveal that creature card from your hand before you begin to cast the card as a spell.').

card_ruling('sacred foundry', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('sacred foundry', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('sacred foundry', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('sacred mesa', '2004-10-04', 'It can be used during upkeep to create a Pegasus token in time for it to be sacrificed.').

card_ruling('sacrifice', '2004-10-04', 'Sacrificing an animated land gives no mana since the converted mana cost was zero.').
card_ruling('sacrifice', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('sacrifice', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('sadistic hypnotist', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('safe haven', '2004-10-04', 'When creatures return to the battlefield, they are put onto the battlefield as if just cast. Creatures with X in the mana cost are treated as X is zero. Creatures which can pay costs when put onto the battlefield to determine abilities must have those costs paid at this time.').
card_ruling('safe haven', '2004-10-04', 'Token creatures cease to exist when they leave the battlefield, so this effect just exiles them with no chance to bring them back like you can with cards.').
card_ruling('safe haven', '2004-10-04', 'If changed to another land type, the creature cards are not lost but can\'t be released until the land reverts to normal.').
card_ruling('safe haven', '2004-10-04', 'When the creature leaves the battlefield any damage or \"will be destroyed at some future time\" effects are removed from the creature.').
card_ruling('safe haven', '2004-10-04', 'Auras on creatures are put into the graveyard and counters on creatures are removed when the creatures are sent to the Haven.').
card_ruling('safe haven', '2004-10-04', 'Creatures return to the control of their owners, regardless of who controls the Haven when it is sacrificed.').
card_ruling('safe haven', '2004-10-04', 'All cards in the Haven stay there even if they cease to be creatures. When the Haven is sacrificed, the cards come back onto the battlefield whether or not they are creatures.').
card_ruling('safe haven', '2004-10-04', 'Creatures return to the battlefield simultaneously.').

card_ruling('safe passage', '2009-10-01', 'Safe Passage prevents all damage, not just combat damage, that would be dealt to you and creatures you control this turn.').
card_ruling('safe passage', '2009-10-01', 'Safe Passage will prevent damage dealt to creatures that weren\'t on the battlefield at the time it resolved.').
card_ruling('safe passage', '2009-10-01', 'Safe Passage doesn\'t prevent damage that would be dealt to planeswalkers you control. Although it can\'t prevent combat damage that would be dealt to your planeswalkers, it can still prevent noncombat damage that your opponent would want to redirect from you to one of your planeswalkers. Just apply Safe Passage\'s prevention effect to that damage first, and there won\'t be any damage to redirect.').
card_ruling('safe passage', '2009-10-01', 'Safe Passage has no effect on damage that\'s already been dealt.').

card_ruling('safehold duo', '2008-05-01', 'An object that\'s both of the listed colors will cause both abilities to trigger. You can put them on the stack in either order.').

card_ruling('safehold elite', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('safehold elite', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('safehold elite', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('safehold elite', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('safehold elite', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('safehold elite', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('safehold elite', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('safehold sentry', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('safehold sentry', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('safehold sentry', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('saffi eriksdotter', '2006-09-25', 'If a token creature is targeted, it will not be returned to the battlefield.').
card_ruling('saffi eriksdotter', '2006-09-25', 'The creature is returned to the battlefield under your control if it\'s put into *your* graveyard from the battlefield. It doesn\'t matter who controlled it when Saffi targeted it.').
card_ruling('saffi eriksdotter', '2006-09-25', 'Saffi can target itself, but it won\'t return itself to the battlefield. It will already be in the graveyard when the ability resolves, so the ability will be countered.').

card_ruling('sage of fables', '2008-04-01', 'If a Wizard would normally enter the battlefield with a certain number of +1/+1 counters on it, it enters the battlefield with that many +1/+1 counters plus one on it instead. If a Wizard would normally enter the battlefield with no +1/+1 counters on it, it enters the battlefield with one +1/+1 counter on it instead.').
card_ruling('sage of fables', '2008-04-01', 'The creature gets the counter if it would enter the battlefield under your control. It doesn\'t matter who owns the creature or what zone it enters the battlefield from (such as your opponent\'s graveyard, for example).').
card_ruling('sage of fables', '2008-04-01', 'The creature gets the counter if its copiable characteristics as it would exist on the battlefield include the specified creature type. For example, say you control Conspiracy, and the chosen creature type is Wizard. If you put a creature onto the battlefield that isn\'t normally a Wizard, it won\'t get a counter from Sage of Fables.').
card_ruling('sage of fables', '2008-04-01', 'The effects from more than one Sage of Fables are cumulative. That is, if you have two Sage of Fables on the battlefield, Wizard creatures you control enter the battlefield with two additional +1/+1 counters on them.').
card_ruling('sage of fables', '2008-04-01', 'If Sage of Fables enters the battlefield at the same time as another Wizard (due to Living End, for example), that creature doesn\'t get a +1/+1 counter.').

card_ruling('sage of hours', '2014-04-26', 'You must remove all +1/+1 counters from Sage of Hours to activate its ability, even if the number of counters isn’t a multiple of five. For example, if you remove twelve counters to activate the ability, you’ll take two extra turns. If you remove three counters, you won’t take any extra turns.').
card_ruling('sage of hours', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('sage of hours', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('sage of hours', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('sage\'s dousing', '2008-04-01', 'If you control a Wizard, you draw a card regardless of whether the targeted spell was countered or {3} was paid.').

card_ruling('sage\'s reverie', '2014-11-24', 'An Aura doesn’t necessarily need the enchant creature ability for the abilities of Sage’s Reverie to count it. For example, an Aura with enchant permanent that’s attached to a creature will count.').
card_ruling('sage\'s reverie', '2014-11-24', 'Count the number of Auras you control attached to creatures as the enters-the-battlefield ability resolves to determine how many cards to draw. This will include Sage’s Reverie as long as it’s still on the battlefield at that time.').

card_ruling('sage-eye avengers', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('sage-eye avengers', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('sage-eye avengers', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('sage-eye avengers', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('sage-eye harrier', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('sage-eye harrier', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('sage-eye harrier', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('sage-eye harrier', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('sage-eye harrier', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('sage-eye harrier', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('sage-eye harrier', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('sage-eye harrier', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('sage-eye harrier', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('sages of the anima', '2009-05-01', 'Other players don\'t get to see the order that you put the cards on the bottom of your library.').
card_ruling('sages of the anima', '2009-05-01', 'If an effect would cause you to draw multiple cards, each individual draw is replaced by Sages of the Anima\'s effect. Process the draws one at a time.').
card_ruling('sages of the anima', '2009-05-01', 'If there are fewer than three cards in your library, you\'ll reveal all of them.').
card_ruling('sages of the anima', '2009-05-01', 'This effect is mandatory. Even if there are no more creature cards in your library, you have to do this instead of drawing a card.').
card_ruling('sages of the anima', '2009-05-01', 'If a spell or ability causes you to put cards in your hand without specifically using the word \"draw,\" Sages of the Anima\'s ability won\'t affect it.').
card_ruling('sages of the anima', '2009-05-01', 'If two or more replacement effects would apply to a card-drawing event, the player who\'s drawing the card chooses what order to apply them. It\'s possible that after applying one of them, the others will no longer be applicable because the player would no longer draw a card. For example, if you have more than one Sages of the Anima on the battlefield and you would draw a card, each Sages of the Anima\'s replacement effect could apply. Once you use one, the rest are no longer applicable.').

card_ruling('sagu archer', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('sagu archer', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('sagu archer', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('sagu archer', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('sagu archer', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('sagu archer', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('sagu archer', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('sagu archer', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('sagu archer', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('sagu mauler', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('sagu mauler', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('sagu mauler', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('sagu mauler', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('sagu mauler', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('sagu mauler', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('sagu mauler', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('sagu mauler', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('sagu mauler', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('sai of the shinobi', '2012-06-01', 'If you choose not to attach Sai of the Shinobi to the creature, or if you can\'t (perhaps because the creature has protection from artifacts or has left the battlefield by the time the trigger resolves), Sai of the Shinobi doesn\'t move.').

card_ruling('sakashima the impostor', '2005-06-01', 'If there is no creature on the battlefield or if you don\'t choose a creature as Sakashima enters the battlefield, Sakashima remains a 3/1 Human Rogue.').
card_ruling('sakashima the impostor', '2005-06-01', 'Sakashima doesn\'t get the return to hand ability unless a creature is copied as Sakashima enters the battlefield. Sakashima can\'t copy itself because the choice is made as part of the event that puts it onto the battlefield, and whatever will be copied must already be on the battlefield.').
card_ruling('sakashima the impostor', '2005-06-01', 'Sakashima must be on the battlefield at the end of turn in order to return to hand. If Sakashima leaves the battlefield after the ability is activated but before the at-end-of-turn ability resolves, the ability does nothing.').

card_ruling('sakashima\'s student', '2012-06-01', 'Because you return the unblocked attacking creature to its owner\'s hand while activating the ninjutsu ability, you can\'t have Sakashima\'s Student enter the battlefield as a copy of that creature.').
card_ruling('sakashima\'s student', '2012-06-01', 'Sakashima\'s Student copies exactly what was printed on the original creature (unless that creature is copying something else or is a token; see below) and it is a Ninja in addition to its other types. It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or any Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('sakashima\'s student', '2012-06-01', 'If the chosen creature has {X} in its mana cost, X is zero.').
card_ruling('sakashima\'s student', '2012-06-01', 'If the chosen creature is copying something else (for example, if the chosen creature is another Sakashima\'s Student), then your Sakashima\'s Student enters the battlefield as whatever the chosen creature copied.').
card_ruling('sakashima\'s student', '2012-06-01', 'If the chosen creature is a token, Sakashima\'s Student copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Sakashima\'s Student is not a token.').
card_ruling('sakashima\'s student', '2012-06-01', 'Any enters-the-battlefield abilities of the copied creature will trigger when Sakashima\'s Student enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature (like devour) will also work.').
card_ruling('sakashima\'s student', '2012-06-01', 'If Sakashima\'s Student somehow enters the battlefield at the same time as another creature, Sakashima\'s Student can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('sakashima\'s student', '2012-06-01', 'You can choose not to copy anything. In that case, Sakashima\'s Student enters the battlefield as a 0/0 creature, and is probably put into the graveyard immediately.').

card_ruling('salt road ambushers', '2015-02-25', 'The face-down permanent must be a creature both before it’s turned face up and when the triggered ability resolves to have +1/+1 counters placed on it.').
card_ruling('salt road ambushers', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('salt road ambushers', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('salt road ambushers', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('salt road patrol', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('salt road patrol', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('saltskitter', '2007-05-01', 'If a creature enters the battlefield after \"at the beginning of the end step\" abilities have triggered during that turn, Saltskitter won\'t return to the battlefield until the end of the following turn.').

card_ruling('salvage drone', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('salvage drone', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('salvage drone', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('salvage drone', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('salvage drone', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('salvage drone', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('salvage drone', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('salvage titan', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('salvage titan', '2008-10-01', 'Casting Salvage Titan by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast a creature spell.').
card_ruling('salvage titan', '2008-10-01', 'You may activate the second ability only if Salvage Titan is in your graveyard. To pay this ability\'s cost, you may exile any three artifact cards in your graveyard -- including Salvage Titan itself. If you exile it to pay the cost, however, it won\'t be returned to your hand when the ability resolves.').

card_ruling('samite censer-bearer', '2007-05-01', 'This ability sets up a separate 1-point damage prevention shield on each creature you control at the time the ability resolves.').

card_ruling('samite elder', '2004-10-04', 'If the color of that permanent changes after the ability takes effect, the color of protection that was granted does not change.').
card_ruling('samite elder', '2004-10-04', 'The color is determined on resolution.').

card_ruling('samite pilgrim', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('samite pilgrim', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('samite pilgrim', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('sanctimony', '2004-10-04', 'Gaining life is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('sanctum gargoyle', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('sanctum of serra', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('sanctum of serra', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('sanctum of serra', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('sanctum of serra', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('sanctum of serra', '2009-10-01', 'If you planeswalk away from Sanctum of Serra because the player who owns Sanctum of Serra leaves the game, its first ability will still trigger and resolve.').
card_ruling('sanctum of serra', '2009-10-01', 'For a player\'s life total to become 20, what actually happens is that the player gains or loses the appropriate amount of life. For example, if your life total is 14 when this ability resolves, it will cause you to gain 6 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').

card_ruling('sanctum plowbeast', '2009-05-01', 'Unlike the normal cycling ability, landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a land card of the specified land type, reveal it, put it into your hand, then shuffle your library.').
card_ruling('sanctum plowbeast', '2009-05-01', 'Landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being landcycled. Any ability that stops a cycling ability from being activated also stops a landcycling ability from being activated.').
card_ruling('sanctum plowbeast', '2009-05-01', 'You can choose to find any card with the appropriate land type, including nonbasic lands. You can also choose not to find a card, even if there is a land card with the appropriate type in your library.').
card_ruling('sanctum plowbeast', '2009-05-01', 'A landcycling ability lets you search for any card in your library with the stated land type. It doesn\'t have to be a basic land.').
card_ruling('sanctum plowbeast', '2009-05-01', 'You may only activate one landcycling ability at a time. You must specify which landcycling ability you are activating as you cycle this card, not as the ability resolves.').

card_ruling('sand silos', '2004-10-04', 'It is considered \"tapped for mana\" if you activate its mana ability, even if you choose to take zero mana from it.').
card_ruling('sand silos', '2004-10-04', 'This enters the battlefield tapped even if a continuous effect immediately changes it to something else.').
card_ruling('sand silos', '2004-10-04', 'If the land is tapped by some external effect, no counters are removed from it.').
card_ruling('sand silos', '2004-10-04', 'Counters are not lost if the land is changed to another land type. They wait around for the land to change back.').
card_ruling('sand silos', '2004-10-04', 'Whether or not it is tapped is checked at the beginning of upkeep. If it is not tapped, the ability does not trigger. It also checks during resolution and you only get a counter if it is still tapped then.').

card_ruling('sandbar merfolk', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('sandbar serpent', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('sandcrafter mage', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('sandcrafter mage', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('sands of delirium', '2012-07-01', 'If the player has fewer than X cards in his or her library, he or she puts all of them into his or her graveyard.').

card_ruling('sands of time', '2004-10-04', 'This card still affects players when it is tapped.').
card_ruling('sands of time', '2008-08-01', 'Skipping the untap step means that phased out cards won\'t phase in, and permanents with phasing won\'t phase out.').
card_ruling('sands of time', '2009-10-01', 'You can tap lands for mana before Sands of Time toggles your permanents. The mana you gain is in your mana pool during your upkeep step, but will disappear before your draw step if you don\'t spend it. You cannot save it to be spent on the card you will draw this turn.').

card_ruling('sandsower', '2013-06-07', 'Because the cost of Sandsower\'s ability doesn\'t include the {T} symbol, you can tap any three untapped creatures you control (including Sandsower itself), even if those creatures haven\'t been under your control continuously since the beginning of your most recent turn.').

card_ruling('sandsteppe mastodon', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('sandsteppe mastodon', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('sandsteppe outcast', '2014-11-24', 'You choose which mode you’re using as you put the ability on the stack, after the creature has entered the battlefield. Once you’ve chosen a mode, you can’t change that mode even if the creature leaves the battlefield in response to that ability.').
card_ruling('sandsteppe outcast', '2014-11-24', 'If a mode requires a target and there are no legal targets available, you must choose the mode that adds a +1/+1 counter.').

card_ruling('sandsteppe scavenger', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('sandsteppe scavenger', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('sandstorm charger', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('sandstorm charger', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('sandstorm charger', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('sangromancer', '2011-06-01', 'If one or more creatures an opponent controls are put into the graveyard at the same time as Sangromancer, Sangromancer\'s first ability will trigger that many times.').

card_ruling('sanguimancy', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('sanguimancy', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('sanguimancy', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('sanguimancy', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('sanguine bond', '2009-10-01', 'Sanguine Bond\'s ability triggers just once for each life-gaining event, whether it\'s 1 life from Soul Warden or 8 life from Tendrils of Corruption.').

card_ruling('sanity grinding', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('sanity grinding', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').
card_ruling('sanity grinding', '2008-08-01', 'If there are fewer than ten cards in your library, reveal them all. For each blue mana symbol in those cards\' mana costs, the targeted opponent puts the top card of his or her library into his or her graveyard. Then you reorder your entire library as you choose.').

card_ruling('sapling of colfenor', '2008-08-01', 'The triggered ability results in three sequential events: you gain some life (subject to effects that might alter life gain), then lose some life (subject to other effects that might alter life loss), and then put the card into your hand.').
card_ruling('sapling of colfenor', '2008-08-01', 'If a creature card has \"*\" in its power or toughness, the ability that defines \"*\" works in all zones. Note that the card is in your library when Sapling of Colfenor\'s ability checks the card\'s power and toughness.').
card_ruling('sapling of colfenor', '2008-08-01', 'If the revealed card is not a creature card, it remains on top of your library.').
card_ruling('sapling of colfenor', '2014-02-01', 'Lethal damage and effects that say \"destroy\" won\'t cause Sapling of Colfenor to be put into the graveyard. However, Sapling of Colfenor could be put into the graveyard if its toughness becomes 0 or less, if it\'s sacrificed, or if you control another Sapling of Colfenor (due to the \"legend rule\").').

card_ruling('sapphire medallion', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('sapphire medallion', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('sapphire medallion', '2004-10-04', 'The effect is cumulative.').
card_ruling('sapphire medallion', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('sapphire medallion', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('sapphire medallion', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('saprazzan heir', '2004-10-04', 'It triggers only once even if blocked by more than one creature.').

card_ruling('saprazzan raider', '2004-10-04', 'It returns as soon as it is blocked. It does not remain on the battlefield long enough to deal damage.').

card_ruling('saproling burst', '2004-10-04', 'Each token knows which Saproling Burst it came from.').

card_ruling('sarcomite myr', '2007-05-01', 'Sarcomite Myr is an artifact creature, just like Ornithopter. The only difference between this and any other artifact creature is that Sarcomite Myr is blue since it has blue mana in its mana cost.').

card_ruling('sarkhan the mad', '2010-06-15', 'Unlike previous planeswalkers, Sarkhan the Mad has no way to raise his loyalty.').
card_ruling('sarkhan the mad', '2010-06-15', 'Once the first ability starts to resolve and you reveal the top card of your library (and see how much damage Sarkhan the Mad deals to himself), it\'s too late to respond.').
card_ruling('sarkhan the mad', '2010-06-15', 'The first ability causes Sarkhan to deal damage directly to himself, resulting in that many loyalty counters being removed from him. The damage would never have been dealt to you, so prevention effects that would prevent damage from being dealt to you won\'t affect it.').
card_ruling('sarkhan the mad', '2010-06-15', 'If the targeted creature is an illegal target by the time the second ability resolves, the ability is countered. The player won\'t get a Dragon token.').
card_ruling('sarkhan the mad', '2010-06-15', 'If the targeted creature is a legal target by the time the second ability resolves but its controller can\'t sacrifice it (due to Tajuru Preserver, perhaps), the ability continues to resolve. That player gets a Dragon token.').
card_ruling('sarkhan the mad', '2010-06-15', 'You choose a single target as you activate the third ability. All your Dragons deal damage to that player.').
card_ruling('sarkhan the mad', '2010-06-15', 'If you target an opponent with the third ability, you choose separately for each Dragon creature you control whether or not to redirect the damage dealt by that Dragon to a planeswalker that player controls.').
card_ruling('sarkhan the mad', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('sarkhan the mad', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('sarkhan the mad', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('sarkhan the mad', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('sarkhan the mad', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('sarkhan the mad', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('sarkhan the mad', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('sarkhan the mad', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('sarkhan the mad', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('sarkhan the mad', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('sarkhan the mad', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('sarkhan vol', '2008-10-01', 'You may activate the first ability even if you control no creatures. Sarkhan Vol gains a loyalty counter, but the ability will have no effect.').
card_ruling('sarkhan vol', '2008-10-01', 'Sarkhan Vol\'s first ability affects only creatures you control when that ability resolves.').
card_ruling('sarkhan vol', '2008-10-01', 'You may target any creature with the second ability, not just a creature an opponent controls. If you target a creature you already control, you\'ll gain control of it (which will usually have no visible effect), it\'ll untap, and it\'ll gain haste until end of turn.').
card_ruling('sarkhan vol', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('sarkhan vol', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('sarkhan vol', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('sarkhan vol', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('sarkhan vol', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('sarkhan vol', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('sarkhan vol', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('sarkhan vol', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('sarkhan vol', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('sarkhan vol', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('sarkhan vol', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('sarkhan\'s rage', '2015-02-25', 'Sarkhan’s Rage checks whether you control a Dragon as it resolves.').

card_ruling('sarkhan, the dragonspeaker', '2014-09-20', 'If Sarkhan, the Dragonspeaker becomes a creature due to his first ability, that doesn’t count as having a creature enter the battlefield. Sarkhan was already on the battlefield; he only changed his types. Abilities that trigger whenever a creature enters the battlefield won’t trigger.').
card_ruling('sarkhan, the dragonspeaker', '2014-09-20', 'Any loyalty counters on Sarkhan when his first ability resolves will remain on him while he’s a creature. Because he’s not a planeswalker at this time, damage dealt to him won’t cause those counters to be removed.').
card_ruling('sarkhan, the dragonspeaker', '2014-09-20', 'After the first ability resolves, Sarkhan won’t be a planeswalker until the end of the turn. Later that turn, if you cast another Sarkhan, the Dragonspeaker, neither permanent will be put into its owner’s graveyard. The “legend rule” sees only the original Sarkhan and the “planeswalker uniqueness rule” sees only the new Sarkhan. However, if you activate the first ability of the new Sarkhan, you would then control two legendary permanents with the same name. You’d choose one to remain on the battlefield and the other would be put into its owner’s graveyard. If you don’t activate the first ability of the new Sarkhan, the original Sarkhan will return to being a planeswalker during the cleanup step. You would then control two planeswalkers with the same planeswalker type. You’d choose one to remain on the battlefield and the other would be put into its owner’s graveyard. There would then be another cleanup step before the turn ended.').
card_ruling('sarkhan, the dragonspeaker', '2014-09-20', 'If a permanent enters the battlefield as a copy of Sarkhan after his first ability has resolved, the copy will be a Sarkhan planeswalker. It will enter the battlefield with four loyalty counters, no matter how many loyalty counters are on the original Sarkhan. You’ll then find yourself in the same situation described above.').

card_ruling('sasaya, orochi ascendant', '2005-06-01', 'You don\'t need to have seven lands in hand to reveal your hand. You leave your hand revealed until the ability resolves. If you don\'t have seven or more land cards in your hand at that time, the effect does nothing.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'You don\'t need to tap Sasaya to use its ability, so you can activate the ability on the turn Sasaya comes under your control.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'Sasaya\'s Essence has a triggered mana ability. Like activated mana abilities, the triggered mana ability doesn\'t use the stack.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'The triggered mana ability counts the number of other lands with that same name and adds that much mana to the player\'s mana pool. The type (that is, the color of mana or colorless mana) is the same as the mana the land just produced.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'If Sasaya\'s Essence\'s controller has four Forests and taps one of them for {G}, the Essence will add {G}{G}{G} to that player\'s mana pool for a total of {G}{G}{G}{G}.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'If Sasaya\'s Essence\'s controller has four Mossfire Valley and taps one of them for {R}{G}, the Essence will add three mana (one for each other Mossfire Valley) of any combination of {R} and/or {G} to that player\'s mana pool.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'If Sasaya\'s Essence\'s controller has two Brushlands and taps one of them for {W}, Sasaya\'s Essence adds another {W} to that player\'s mana pool. It won\'t produce {G} or {1} unless the land was tapped for {G} or {1} instead.').
card_ruling('sasaya, orochi ascendant', '2005-06-01', 'Each Ascendant is legendary in both its unflipped and flipped forms. This means that effects that look for legendary creature cards, such as Time of Need and Captain Sisay, can find an Ascendant.').

card_ruling('satyr hedonist', '2013-09-15', 'Satyr Hedonist’s ability is a mana ability. It doesn’t use the stack and can’t be responded to.').

card_ruling('satyr hoplite', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('satyr hoplite', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('satyr hoplite', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('satyr nyx-smith', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('satyr nyx-smith', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('satyr nyx-smith', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('satyr nyx-smith', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('satyr piper', '2013-09-15', 'During the declare blockers step, the defending player must assign at least one blocker to each creature that must be blocked if that player controls any creatures that could block it.').
card_ruling('satyr piper', '2013-09-15', 'If multiple attacking creatures must be blocked, the defending player must assign blockers in such a way that the greatest number of those attacking creatures are blocked.').

card_ruling('savage beating', '2004-12-01', 'The additional combat phase is directly after the combat phase in which Savage Beating is cast. There isn\'t a main phase between the two combat phases.').

card_ruling('savage conception', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('savage conception', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('savage conception', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('savage conception', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('savage gorilla', '2004-10-04', 'You do not get to draw a card if the target is not legal on resolution.').

card_ruling('savage hunger', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('savage offensive', '2004-10-04', 'Only affects creatures you control when this spell resolves.').

card_ruling('savage punch', '2014-09-20', 'You can’t cast Savage Punch unless you target both a creature you control and a creature you don’t control.').
card_ruling('savage punch', '2014-09-20', 'If either target is an illegal target when Savage Punch tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('savage punch', '2014-09-20', 'Assuming the ferocious ability applies, if just the creature you control is an illegal target when Savage Punch tries to resolve, it won’t get +2/+2 until end of turn. If the creature you control is a legal target but the creature you don’t control isn’t, the creature you control will get +2/+2 until end of turn.').
card_ruling('savage punch', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('savage punch', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('savage silhouette', '2009-10-01', 'The controller of the enchanted creature may activate the regeneration ability, not the controller of Savage Silhouette.').

card_ruling('savage summoning', '2013-07-01', 'You don’t choose a creature card when Savage Summoning resolves. Rather, it applies to the next creature card you cast that turn, even if you cast it at a time when you normally could cast a creature and even if you cast it from a zone other than your hand.').
card_ruling('savage summoning', '2013-07-01', 'If you cast multiple Savage Summonings in succession, they’ll apply to the next creature spell you cast. That creature will enter the battlefield with that many additional +1/+1 counters on it.').
card_ruling('savage summoning', '2013-07-01', 'Spells that would counter Savage Summoning or the relevant creature spell can still be cast. They won’t counter the spell, but any additional effects they have will happen.').

card_ruling('savage surge', '2013-09-15', 'Savage Surge can target an untapped creature.').

card_ruling('savage ventmaw', '2015-02-25', 'This mana won’t empty from your mana pool as steps and phases end for the remainder of the turn even if Savage Ventmaw leaves the battlefield during the turn.').

card_ruling('savannah', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('savannah', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('saving grasp', '2011-01-22', 'Saving Grasp can target any creature you own, even one controlled by another player.').

card_ruling('savra, queen of the golgari', '2005-10-01', 'These abilities trigger specifically when you \"sacrifice\" a creature. They won\'t trigger when a creature is put into your graveyard for any other reason.').
card_ruling('savra, queen of the golgari', '2005-10-01', 'Savra itself doesn\'t let you sacrifice creatures, and you can\'t sacrifice them spontaneously. You must have another spell or ability that lets you sacrifice creatures.').
card_ruling('savra, queen of the golgari', '2005-10-01', 'Sacrificing a creature that\'s both black and green will make both abilities trigger. You may put them on the stack in whichever order you want.').

card_ruling('sawtooth loon', '2004-10-04', 'Both triggered abilities trigger at the same time so you can decide which order they go on the stack. If you have no cards in hand, you would likely choose the order where you draw and put on the library before unsummoning the creature that is on the battlefield.').
card_ruling('sawtooth loon', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('sawtooth ogre', '2004-10-04', 'The damage happens even if this card is not on the battlefield at the end of combat.').

card_ruling('scab-clan berserker', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('scab-clan berserker', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('scab-clan berserker', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('scab-clan giant', '2013-04-15', 'To choose a target at random, all legal targets must have an equal chance of being chosen. There are many ways to do this, including assigning each possible legal target a number and rolling a die.').
card_ruling('scab-clan giant', '2013-04-15', 'The target is chosen at random as part of putting the ability on the stack. Players can respond to the ability knowing what the target of the ability is.').
card_ruling('scab-clan giant', '2013-04-15', 'If Scab-Clan Giant leaves the battlefield before its ability resolves, no fight will happen and neither creature will deal damage. Also, if the target creature is an illegal target when the ability tries to resolve, it will be countered and no fight will happen.').
card_ruling('scab-clan giant', '2013-04-15', 'In a multiplayer game, you don\'t choose a single opponent at random. Any creature controlled by any of your opponents may be chosen at random.').

card_ruling('scalding salamander', '2004-10-04', 'The ability triggers on the declaration of attackers and will resolve before blockers are declared.').

card_ruling('scalding tongs', '2004-10-04', 'You target one opposing player each time the ability triggers.').
card_ruling('scalding tongs', '2004-10-04', 'It checks the number of cards in your hand at the beginning of upkeep and does not trigger at all if you have more than three. It checks again on resolution and does not deal damage unless you still have 3 or fewer cards.').

card_ruling('scale blessing', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('scale blessing', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('scaleguard sentinels', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('scaleguard sentinels', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('scaleguard sentinels', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('scalpelexis', '2004-10-04', 'The \"repeat this process\" means to repeat as often as it takes to not get a matching pair.').

card_ruling('scapegoat', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('scapegoat', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('scapeshift', '2008-04-01', 'You sacrifice the lands as part of the resolution of Scapeshift. It isn\'t an additional cost.').

card_ruling('scarecrow', '2004-10-04', 'Even prevents non-combat damage from flying creatures.').

card_ruling('scarred puma', '2004-10-04', 'It means \"a black or green creature other than itself attacks\".').

card_ruling('scarred vinebreeder', '2007-10-01', 'If you activate the ability, the Elf card is exiled as a cost. This action can\'t be responded to.').

card_ruling('scarscale ritual', '2008-05-01', 'If you don\'t control any creatures, you can\'t cast Scarscale Ritual.').

card_ruling('scatter the seeds', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('scatter the seeds', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('scatter the seeds', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('scatter the seeds', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('scatter the seeds', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('scatter the seeds', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('scatter the seeds', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('scatter to the winds', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('scatter to the winds', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('scatter to the winds', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('scatter to the winds', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('scatter to the winds', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('scatter to the winds', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('scattering stroke', '2007-10-01', 'You decide whether or not to add {X} to your mana pool as the delayed triggered ability resolves at the beginning of your next main phase. You don\'t decide before then.').
card_ruling('scattering stroke', '2007-10-01', 'You can\'t add just some of the mana to your mana pool. You either add all the {X} to your mana pool or none of it. All the mana you get this way is colorless.').

card_ruling('scattershot', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('scattershot', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('scattershot', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('scattershot', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('scattershot', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('scavenging ghoul', '2004-10-04', 'The Ghoul still gets to claim counters even if it enters the battlefield after the creatures died.').

card_ruling('scavenging ooze', '2013-07-01', 'If the target card is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. No counter will be put on Scavenging Ooze and you won’t gain life.').

card_ruling('scent of brine', '2004-10-04', 'You reveal cards during resolution.').

card_ruling('scent of cinder', '2004-10-04', 'You reveal cards, and thereby set the value of X, on resolution.').

card_ruling('scent of ivy', '2004-10-04', 'You reveal cards, and thereby set the value of X, on resolution.').

card_ruling('scent of jasmine', '2004-10-04', 'You reveal cards, and thereby the amount of life gained, on resolution.').

card_ruling('scent of nightshade', '2004-10-04', 'You reveal cards, and thereby the value of X, on resolution.').

card_ruling('scepter of empires', '2011-09-22', 'Whether or not you control the correct artifacts is determined when the ability resolves.').
card_ruling('scepter of empires', '2011-09-22', 'If any of the named cards stops being an artifact, it won\'t be considered by these abilities.').

card_ruling('scion of darkness', '2004-10-04', 'If this card destroys a creature in combat and at the same time (using its Trample ability) it damages a player, you will be able to target the destroyed creature (if it was a card and not a token) to be brought back.').
card_ruling('scion of darkness', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('scion of the ur-dragon', '2006-09-25', 'Removing the other Dragon card from your graveyard has no effect on Scion of the Ur-Dragon.').
card_ruling('scion of the ur-dragon', '2006-09-25', 'When Scion of the Ur-Dragon becomes a copy of another Dragon, it loses its copy ability for the rest of the turn.').

card_ruling('scion of ugin', '2015-02-25', 'Scion of Ugin is colorless. It isn’t monocolored, nor is it an artifact.').

card_ruling('scion of vitu-ghazi', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('scion of vitu-ghazi', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('scion of vitu-ghazi', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('scion of vitu-ghazi', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('scion of vitu-ghazi', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('scion of vitu-ghazi', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').
card_ruling('scion of vitu-ghazi', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('scorch the fields', '2011-01-22', 'If the targeted land is an illegal target when Scorch the Fields tries to resolve, it will be countered and none of its effects will happen. No damage will be dealt to any Human.').
card_ruling('scorch the fields', '2011-01-22', 'Scorch the Fields doesn\'t target any Humans. You can cast Scorch the Fields even if there aren\'t any Humans on the battlefield.').

card_ruling('scorched earth', '2004-10-04', 'X can be zero to destroy no lands.').

card_ruling('scorched ruins', '2008-04-01', 'If you don\'t sacrifice the lands, Scorched Ruins never enters the battlefield -- it goes directly to your graveyard.').

card_ruling('scour', '2004-10-04', 'Does not exile other cards of the same name that are on the battlefield. Just from the graveyard, hand, and library.').
card_ruling('scour', '2005-02-01', 'Scour can target and exile cards that aren\'t always enchantments, but are enchantments when Scour is cast and resolves (such as creatures turned into enchantments by Soul Sculptor). Scour\'s exile effect only looks for cards by name, not type.').
card_ruling('scour', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').

card_ruling('scourge devil', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('scourge devil', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('scourge devil', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('scourge devil', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('scourge devil', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('scourge of fleets', '2014-04-26', 'The value of X is determined as the ability resolves.').

card_ruling('scourge of kher ridges', '2007-05-01', 'If Scourge of Kher Ridges has lost flying, the first activated ability will cause it to deal damage to itself.').

card_ruling('scourge of skola vale', '2014-02-01', 'Use the toughness of the sacrificed creature as it last existed on the battlefield to determine how many +1/+1 counters to put on Scourge of Skola Vale.').

card_ruling('scourge of the nobilis', '2008-08-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').
card_ruling('scourge of the nobilis', '2008-08-01', 'The controller of the enchanted creature, not the controller of Scourge of the Nobilis, can activate the +1/+0 ability.').
card_ruling('scourge of the nobilis', '2008-08-01', 'Once the +1/+0 ability is activated, it exists independently of the creature and Scourge of the Nobilis. It will resolve, and the creature will retain the bonus for the rest of the turn, even if Scourge of the Nobilis stops enchanting that creature.').

card_ruling('scourge of the throne', '2014-05-29', 'Scourge of the Throne’s ability has an intervening “if” clause. It must be attacking the player with the most life or tied with the most life both when it’s declared as an attacker and as it starts to resolve for it to have any effect. If, at either time, the player isn’t the one with the most life or tied for the most life, the ability will have no effect. Notably, this is different than how dethrone works. (Dethrone checks only once to see if the ability triggers.)').
card_ruling('scourge of the throne', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('scourge of the throne', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('scourge of the throne', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('scourge of the throne', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('scourge of valkas', '2013-07-01', 'The amount of damage dealt by the Dragon that entered the battlefield is based on the number of Dragons you control when the ability resolves.').

card_ruling('scourgemark', '2013-09-15', 'If the target of an Aura is illegal when it tries to resolve, the Aura will be countered. The Aura doesn’t enter the battlefield, so you won’t get to draw a card.').

card_ruling('scourglass', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('scourglass', '2008-10-01', 'Any permanent that\'s an artifact or a land won\'t be destroyed, regardless of what other card types it may have.').

card_ruling('scouring sands', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('scouring sands', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('scouring sands', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('scouring sands', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('scout\'s warning', '2007-05-01', 'Scout\'s Warning works just like Quicken, except for creatures instead of sorceries.').
card_ruling('scout\'s warning', '2007-05-01', 'During the turn you cast Scout\'s Warning, you can suspend creature cards from your hand at the time you could cast an instant. This ends as soon as you actually cast a creature card.').
card_ruling('scout\'s warning', '2007-05-01', 'Scout\'s Warning affects only the next creature card cast that turn, even if that creature already had flash.').
card_ruling('scout\'s warning', '2007-05-01', 'Scout\'s Warning allows you to cast a noncreature card with morph (such as Lumithread Field) face down as though it had flash. It won\'t affect that card if you cast it face up.').
card_ruling('scout\'s warning', '2013-04-15', 'You don\'t choose a creature card when Scout\'s Warning resolves. Rather, this sets up a rule that is true for you until the turn ends or until you cast a creature card, even if you cast that creature at a time you normally could.').
card_ruling('scout\'s warning', '2013-04-15', 'If you cast multiple Scout\'s Warnings on the same turn, they\'ll all apply to the very next creature spell you cast.').
card_ruling('scout\'s warning', '2013-04-15', 'After Scout\'s Warning resolves, you can suspend a creature card in your hand any time you can cast an instant. As soon as you actually cast a creature card, you lose this capability.').
card_ruling('scout\'s warning', '2013-04-15', 'You cannot use this effect to play Dryad Arbor during an opponent\'s turn.').

card_ruling('scragnoth', '2004-10-04', 'Side-effects of spells that would counter this card will still happen, because they happen even if the spell they\'re trying to counter can\'t be countered.').
card_ruling('scragnoth', '2004-10-04', 'Spells can target Scragnoth and affect it while it is on the stack. Only effects that counter spells are ignored.').
card_ruling('scragnoth', '2004-10-04', 'Spells with a side effect that only happens if a spell is countered do not have their side effect happen when the countering fails.').

card_ruling('scrambleverse', '2011-09-22', 'Once Scrambleverse starts to resolve, no player may respond until it has finished resolving. For example, you can\'t wait to see who will control a creature before deciding whether or not to activate one of its abilities.').
card_ruling('scrambleverse', '2011-09-22', 'For each nonland permanent, any player may be randomly chosen, including the permanent\'s current controller.').
card_ruling('scrambleverse', '2011-09-22', 'All of the control-change effects happen at the same time.').
card_ruling('scrambleverse', '2011-09-22', 'Each nonland permanent will untap, no matter which player was randomly chosen to gain control of it.').

card_ruling('scrap', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('scrap mastery', '2014-11-07', '“Exiled this way” refers to the exile instruction that’s part of Scrap Mastery’s effect. If the sacrificed artifacts are instead exiled because of a replacement effect, those cards won’t be put onto the battlefield.').

card_ruling('scrapbasket', '2008-05-01', 'Scrapbasket\'s ability won\'t stop it from being an artifact. It\'ll just be a colorful artifact.').
card_ruling('scrapbasket', '2008-05-01', 'After Scrapbasket\'s ability resolves, an effect that changes its colors will overwrite that ability\'s effect. For example, casting Cerulean Wisps on it will turn it just blue.').

card_ruling('scrapdiver serpent', '2011-01-01', 'Whether Scrapdiver Serpent can be blocked is relevant only as the declare blockers step begins. If the defending player gains control of an artifact after Scrapdiver Serpent has become blocked, the Serpent will remain blocked.').

card_ruling('scrapyard salvo', '2011-06-01', 'The number of artifact cards in your graveyard is counted when Scrapyard Salvo resolves.').

card_ruling('screaming seahawk', '2004-10-04', 'The \"If you do\" means \"If you search\".').

card_ruling('screamreach brawler', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('screamreach brawler', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('screamreach brawler', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('screamreach brawler', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('screams from within', '2004-12-01', 'If another player has gained control of Screams from Within, its triggered ability does nothing when it\'s put into a graveyard. Cards are always put into their owner\'s graveyard, but Screams from Within\'s ability looks for the card only in its controller\'s graveyard.').
card_ruling('screams from within', '2005-08-01', 'If Screams from Within returns to the battlefield from your graveyard, you choose the creature that Screams from Within enchants (just as you would for any Aura that you\'re putting onto the battlefield without casting it). If your opponent doesn\'t control any creatures, you must choose one of your creatures. If there\'s nothing at all that Screams from Within can enchant, it remains in your graveyard.').

card_ruling('screeching sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('screeching sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('scrib nibblers', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('scrib nibblers', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('scroll of avacyn', '2012-05-01', 'Whether you control an Angel is checked when the ability resolves.').

card_ruling('scroll of griselbrand', '2012-05-01', 'Whether you control a Demon is checked when the ability resolves.').

card_ruling('scroll of the masters', '2014-11-24', 'Count the number of lore counters on Scroll of the Masters as the activated ability resolves to determine how big a bonus the target creature gets. If Scroll of the Masters isn’t on the battlefield at that time, use the number of lore counters on it when it left the battlefield.').
card_ruling('scroll of the masters', '2014-11-24', 'Once the activated ability has resolved, changing the number of lore counters on Scroll of the Masters won’t affect the bonus that activation granted.').

card_ruling('scroll rack', '2004-10-04', 'If there are not enough cards in the library, you do not lose. This is not a draw effect. You just get as many cards as are there, even if there are zero cards in the library.').

card_ruling('scroll thief', '2010-08-15', 'The ability triggers just once each time Scroll Thief deals combat damage to a player, regardless of how much damage it deals.').
card_ruling('scroll thief', '2010-08-15', 'The ability is mandatory. You must draw a card when it resolves.').

card_ruling('scrubland', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('scrubland', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('scrying glass', '2004-10-04', 'The only valid colors are white, black, blue, red, and green.').
card_ruling('scrying glass', '2004-10-04', 'You choose a target opponent on announcement. You choose a number and color during resolution, then the targeted opponent reveals their hand.').

card_ruling('scrying sheets', '2006-07-15', 'If the card isn\'t snow, you can\'t reveal it. If the card is snow, you don\'t have to reveal it. An unrevealed card stays on top of your library.').

card_ruling('sculpting steel', '2004-12-01', 'Any enters-the-battlefield abilities of the copied artifact will trigger when Sculpting Steel enters the battlefield. Any \"as enters the battlefield\" or \"enters the battlefield with\" abilities of the chosen artifact will also work. For example, if Sculpting Steel copies an artifact creature with sunburst, the copy will get +1/+1 counters based on the number of different colors of mana used to pay Sculpting Steel\'s total cost. If Sculpting Steel copies a noncreature artifact with sunburst, the copy will get charge counters based on the number of different colors of mana used to pay Sculpting Steel\'s total cost.').
card_ruling('sculpting steel', '2007-07-15', 'Sculpting Steel doesn\'t copy whether the original artifact is tapped or untapped. It also doesn\'t copy any counters on that artifact, any Auras or Equipment attached to that artifact, or any effects that are currently affecting that artifact -- you get exactly what\'s printed on the chosen card and nothing more. So if you copy an animated Chimeric Staff, for example, you get a normal, nonanimated Chimeric Staff.').
card_ruling('sculpting steel', '2007-07-15', 'If the chosen artifact is copying something else (for example, if the chosen artifact is another Sculpting Steel), then your Sculpting Steel enters the battlefield as whatever the chosen artifact copied.').
card_ruling('sculpting steel', '2007-07-15', 'If the chosen artifact is a token, your Sculpting Steel copies the original characteristics of that token as stated by the effect that put it onto the battlefield. Your Sculpting Steel is not considered to be a token.').
card_ruling('sculpting steel', '2007-07-15', 'You can choose not to copy anything. In that case, Sculpting Steel stays on the battlefield as an artifact that doesn\'t do much of anything.').

card_ruling('scute mob', '2009-10-01', 'Scute Mob\'s ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control five or more lands as your upkeep starts, and (2) the ability will do nothing if you control fewer than five lands by the time it resolves.').

card_ruling('scuttlemutt', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('scuttlemutt', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('scuttlemutt', '2008-05-01', 'Colorless is not a color.').
card_ruling('scuttlemutt', '2008-05-01', 'For the second ability, you can choose any single color or any combination of more than one color. You can\'t choose colorless.').
card_ruling('scuttlemutt', '2008-05-01', 'The second ability won\'t make an artifact creature stop being an artifact. It\'ll just be a colorful artifact.').

card_ruling('scuttling doom engine', '2014-07-18', 'Once Scuttling Doom Engine has been blocked by a creature, reducing that creature’s power to 2 or less won’t change or undo that block.').

card_ruling('scuzzback marauders', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('scuzzback marauders', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('scuzzback marauders', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('scuzzback marauders', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('scuzzback marauders', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('scuzzback marauders', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('scuzzback marauders', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('scythe leopard', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('scythe leopard', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('scythe leopard', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('scythe of the wretched', '2004-12-01', 'The Scythe\'s ability triggers even if something other than the damage dealt by the equipped creature causes the creature to be put into a graveyard that turn.').
card_ruling('scythe of the wretched', '2004-12-01', 'Scythe of the Wretched needs to equip your creature when the other creature goes to the graveyard for the ability to trigger, but it doesn\'t matter whether the Scythe equipped your creature when the damage was actually dealt.').
card_ruling('scythe of the wretched', '2004-12-01', 'If the card isn\'t a creature when it comes back onto the battlefield, then Scythe of the Wretched won\'t move onto it.').

card_ruling('scythe specter', '2011-09-22', 'Each opponent in turn order chooses a card to discard without revealing it, then all the chosen cards are discarded simultaneously. No one sees what the other players are discarding before deciding which card to discard.').
card_ruling('scythe specter', '2011-09-22', 'If there are multiple cards tied for the highest converted mana cost, each player who discarded one of those cards will lose life equal to that converted mana cost.').
card_ruling('scythe specter', '2011-09-22', 'If an opponent discards a split card, each side will be considered separately. For example, if the first opponent discards Wax/Wane (a split card with converted mana costs 1 and 1), a second opponent discards Giant Growth (converted mana cost 1), and all other opponents discard land cards, the first and second opponents each lose 1 life.').
card_ruling('scythe specter', '2011-09-22', 'If Scythe Specter deals combat damage to multiple players simultaneously (perhaps because some of the combat damage was redirected to another player), its ability will trigger for each of those players.').
card_ruling('scythe specter', '2011-09-22', 'In a Two-Headed Giant game, each member of the opposing team will be able to see the other\'s hand before deciding what to discard. If each member discards a card with the same converted mana cost, each player will lose that much life, resulting in the team losing life equal to twice that converted mana cost.').

card_ruling('sea drake', '2008-10-01', 'When the ability resolves, if one of the lands has become an illegal target (because it\'s no longer on the battlefield, for example), you return the other one to your hand. If both of the lands have become illegal targets, the ability is countered. This has no effect on Sea Drake.').

card_ruling('sea gate oracle', '2010-06-15', 'If there\'s only one card in your library as this ability resolves, you\'ll put it into your hand.').

card_ruling('sea god\'s revenge', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('sea god\'s revenge', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('sea god\'s revenge', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('sea god\'s revenge', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('sea kings\' blessing', '2004-10-04', 'Can target zero creatures if you want.').

card_ruling('sea of sand', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('sea of sand', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('sea of sand', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('sea of sand', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('seahunter', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('seal of cleansing', '2004-10-04', 'This is not modal. If the target changes from an artifact to an enchantment or vice versa, this still destroys it.').

card_ruling('seal of primordium', '2004-10-04', 'This is not modal. If the target changes from an artifact to an enchantment or vice versa, this still destroys it.').
card_ruling('seal of primordium', '2007-02-01', 'This is the timeshifted version of Seal of Cleansing.').

card_ruling('sealed fate', '2004-10-04', 'The controller of this spell decides the order of the cards on the library.').

card_ruling('sealock monster', '2013-09-15', 'The effect making the land an Island doesn’t have a duration and will last indefinitely. The land retains any land types and abilities it already had. An Island has the ability “{T}: Add {U} to your mana pool.”').
card_ruling('sealock monster', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('sealock monster', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('sealock monster', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('search for tomorrow', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('search for tomorrow', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('search for tomorrow', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('search for tomorrow', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('search for tomorrow', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('search for tomorrow', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('search for tomorrow', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('search for tomorrow', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('search for tomorrow', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('search for tomorrow', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('search the city', '2012-10-01', 'To “play a card” is to play that card as a land or cast that card as a spell, whichever is appropriate.').
card_ruling('search the city', '2012-10-01', 'You can put only one card into its owner\'s hand for each card you play.').
card_ruling('search the city', '2012-10-01', 'If you don\'t sacrifice Search the City, perhaps because it was destroyed in response to its ability triggering, you won\'t take an extra turn.').

card_ruling('search warrant', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('searing blaze', '2010-03-01', 'Whether you had a land enter the battlefield under your control this turn is checked as this spell resolves, not as you cast it.').
card_ruling('searing blaze', '2010-03-01', 'Having more than one land enter the battlefield under your control this turn provides no additional benefit.').
card_ruling('searing blaze', '2010-03-01', 'The landfall ability checks for an action that has happened in the past. It doesn\'t matter if a land that entered the battlefield under your control previously in the turn is still on the battlefield, is still under your control, or is still a land.').
card_ruling('searing blaze', '2010-03-01', 'Once the spell resolves, having a land enter the battlefield under your control provides no further benefit.').
card_ruling('searing blaze', '2010-03-01', 'The effect of this spell’s landfall ability replaces its normal effect. If you had a land enter the battlefield under your control this turn, only the landfall-based effect happens.').
card_ruling('searing blaze', '2010-03-01', 'You must choose two targets as you cast Searing Blaze: a player and a creature that player controls. If you can\'t (because there are no creatures on the battlefield, perhaps), then you can\'t cast the spell.').
card_ruling('searing blaze', '2010-03-01', 'If either target is illegal by the time Searing Blaze resolves, it still deals damage to the other target.').

card_ruling('searing blood', '2014-02-01', 'If the target creature dies that turn, Searing Blood will deal 3 damage to whoever controls the creature when it dies, who isn’t necessarily the player who controlled it when Searing Blood resolved. It doesn’t matter what causes the creature to die.').

card_ruling('searing meditation', '2005-10-01', 'Searing Meditation doesn\'t care how much life you gain. It triggers just once for each life-gaining event, whether it\'s 1 life from Firemane Angel or 15 life from Brightflame.').

card_ruling('searing rays', '2004-10-04', 'You choose the color on resolution. Your opponent does not get to respond between the color choice and the damage happening. They need to use any damage prevention they want before you choose a color.').

card_ruling('searing spear askari', '2004-10-04', 'The ability only does something if used before blockers are declared. You can use it afterwards but to no useful effect.').

card_ruling('seascape aerialist', '2009-10-01', 'The ability affects only Allies you control at the time the ability resolves.').

card_ruling('seasinger', '2004-10-04', 'You do not lose control of the stolen creature if it stops being a creature. You only lose control if Seasinger leaves your control or becomes untapped.').
card_ruling('seasinger', '2007-09-16', 'If a creature\'s controller doesn\'t control an Island, that creature is an illegal target. Whether that player controls an Island is checked both when Seasinger\'s ability is activated and when it resolves. However, targeting restrictions aren\'t checked continually while a spell or ability is on the stack.').
card_ruling('seasinger', '2007-09-16', 'Whether you control Seasinger is checked continually, starting when the ability is activated. If, before the ability resolves, there\'s any point at which you don\'t control Seasinger, the ability has no effect -- even if you control Seasinger again by the time the ability resolves. The same is true regarding whether Seasinger remains tapped.').

card_ruling('season of the witch', '2013-09-20', 'At the beginning of every end step, regardless of whose turn it is, the second ability triggers. When it resolves every creature that could have been declared as an attacker during that turn\'s Declare Attackers Step but wasn\'t will be destroyed.').
card_ruling('season of the witch', '2013-09-20', 'A creature won\'t be destroyed if it was unable to attack that turn, even if you had a way to enable it to attack. For example, a creature that had summoning sickness wouldn\'t be destroyed even if you had a way to give it haste.').

card_ruling('seasoned marshal', '2004-10-04', 'Can target an already tapped creature if you want.').

card_ruling('secluded steppe', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('second chance', '2004-10-04', 'It is not optional.').
card_ruling('second chance', '2004-10-04', 'The ability will only trigger if you have 5 or less life at the beginning of upkeep. This is checked again at the start of resolution and nothing happens if you do not still have 5 or less life.').
card_ruling('second chance', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').
card_ruling('second chance', '2010-06-15', 'On resolution of the trigger you have to sacrifice this card if you have 5 life or less. If you can\'t (because it is not on the battlefield, for example), then you still take an extra turn after this one.').

card_ruling('second guess', '2012-05-01', 'Second Guess can\'t be cast unless the spell it targets is the second spell cast this turn.').
card_ruling('second guess', '2012-05-01', 'It doesn\'t matter if the first spell cast that turn has resolved yet or not. It also doesn\'t matter how many spells have been cast after the second one, as long as the second spell cast is still on the stack.').
card_ruling('second guess', '2012-05-01', 'Most spells and abilities that copy a spell create that copy on the stack. These copies are not cast and won\'t count toward the number of spells cast that turn. However, a few (like Isochron Scepter\'s ability) instruct you to copy a spell and \"cast the copy.\" These spells are cast and will count toward the number of spells cast that turn.').

card_ruling('second wind', '2007-05-01', 'These abilities are abilities of the Aura, not abilities granted to the creature. Only the Aura\'s controller can activate them.').

card_ruling('secret plans', '2014-09-20', 'As soon as a creature turns face up, it loses the +0/+1 from Secret Plans. If damage marked on that creature is equal to or greater than its new toughness, the creature will be destroyed.').

card_ruling('secret summoning', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('secret summoning', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('secret summoning', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('secret summoning', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('secret summoning', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('secret summoning', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('secret summoning', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('secret summoning', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('secret summoning', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('secret summoning', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('secret summoning', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('secret summoning', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('secret summoning', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('secrets of paradise', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('secrets of paradise', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('secrets of paradise', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('secrets of paradise', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('secrets of paradise', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('secrets of paradise', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('secrets of paradise', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('secrets of paradise', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('secrets of paradise', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('secrets of paradise', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('secrets of paradise', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('secrets of paradise', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('secrets of paradise', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('secrets of the dead', '2011-01-22', 'Secrets of the Dead\'s triggered ability resolves before the spell you cast from your graveyard does.').

card_ruling('security blockade', '2012-10-01', 'You must target a land to cast Security Blockade. If that land is an illegal target when Security Blockade tries to resolve, it will be countered and won\'t enter the battlefield. You won\'t get the Knight token.').

card_ruling('sedge sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('sedge sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('sedraxis alchemist', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').
card_ruling('sedraxis alchemist', '2009-02-01', 'This ability isn\'t optional. When Sedraxis Alchemist enters the battlefield, if you control a blue permanent and no other player controls a nonland permanent, you\'ll have to target a nonland permanent you control.').

card_ruling('sedraxis specter', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('sedraxis specter', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('sedraxis specter', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('sedraxis specter', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('sedraxis specter', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('sedris, the traitor king', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('sedris, the traitor king', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('sedris, the traitor king', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('sedris, the traitor king', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('sedris, the traitor king', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').
card_ruling('sedris, the traitor king', '2008-10-01', 'Despite the appearance of the reminder text, the unearth abilities that Sedris grants are activated abilities of each individual creature card in your graveyard. They\'re not activated abilities of Sedris.').
card_ruling('sedris, the traitor king', '2008-10-01', 'Sedris may cause a creature card in your graveyard to have multiple unearth abilities. (For example, a Fatestitcher in your graveyard would have unearth {U} and unearth {2}{B}.) You may activate either of those abilities.').

card_ruling('see beyond', '2010-06-15', 'The card you shuffle into your library may be any card from your hand. It doesn\'t have to be one of the cards you drew with the spell.').
card_ruling('see beyond', '2010-06-15', 'You have to shuffle a card from your hand into your library even if you don\'t draw any cards (due to replacement effects or Maralen of the Mornsong\'s ability, for example). If your hand is empty, you don\'t shuffle your library.').

card_ruling('see the unwritten', '2014-09-20', 'You determine whether the ferocious ability applies before any creature cards are put onto the battlefield. If you put two creature cards onto the battlefield this way, those creatures enter the battlefield at the same time.').
card_ruling('see the unwritten', '2014-09-20', 'Because you’re not casting the creature cards, you can’t pay any additional or alternative costs. You can’t put the cards onto the battlefield face down.').
card_ruling('see the unwritten', '2014-09-20', 'If a card you put onto the battlefield this way has {X} in its mana cost, X is considered to be 0.').
card_ruling('see the unwritten', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('see the unwritten', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('seedborn muse', '2004-10-04', 'Other effects can prevent a permanent from untapping during an untap step. You do need to look carefully, however, as many effects say that the permanent does not untap during its controller\'s untap step, and this card\'s ability occurs during other players\' untap steps. If a card does say this, then Seedborn Muse can untap it. But some other abilities are not written this way and can still prevent a card from untapping.').
card_ruling('seedborn muse', '2007-07-15', 'All your permanents untap during each other player\'s untap step. You have no choice about what untaps.').

card_ruling('seeds of strength', '2005-10-01', 'You may choose the same creature as a target multiple times since the card says \"target creature\" multiple times. You may give three different creatures +1/+1 each, one creature +2/+2 and another creature +1/+1, or a single creature +3/+3.').

card_ruling('seedtime', '2004-10-04', 'You only get an extra turn if an opponent cast a blue spell this turn prior to this spell resolving. Note that \"casting\" a spell only requires it to be announced, so an unresolved spell still counts as having been cast.').

card_ruling('seeker of skybreak', '2004-10-04', 'It can use its tap ability to untap itself.').

card_ruling('seeker of the way', '2014-09-20', 'Multiple instances of lifelink are redundant. If Seeker of the Way’s last ability resolves more than once in a turn, damage Seeker of the Way deals won’t cause you to gain additional life.').
card_ruling('seeker of the way', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('seeker of the way', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('seeker of the way', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('seer\'s sundial', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('seer\'s sundial', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('seer\'s sundial', '2010-03-01', 'You choose whether to pay {2} as the ability resolves. You may pay {2} only once per resolution.').

card_ruling('segmented krotiq', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('segmented krotiq', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('segmented krotiq', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('seismic elemental', '2015-06-22', 'No creature without flying will be able to block that turn, including creatures that lose flying after Seismic Elemental’s ability resolves and creatures without flying that enter the battlefield after that ability resolves.').

card_ruling('seismic stomp', '2013-07-01', 'No creature without flying will be able to block that turn, including creatures that lose flying after Seismic Stomp resolves and creatures without flying that enter the battlefield after Seismic Stomp resolves.').

card_ruling('seize the day', '2004-10-04', 'It only creates an additional combat and main phase if it resolves during a main phase.').

card_ruling('seize the initiative', '2011-01-01', 'A creature that gains first strike after other creatures with first strike or double strike deal combat damage in the first combat damage step will still deal damage in the second combat damage step.').

card_ruling('seize the soul', '2006-02-01', 'A \"nonwhite nonblack\" creature is a creature that\'s neither white nor black. It can be any other color or combination of colors, or it can be colorless.').
card_ruling('seize the soul', '2006-02-01', 'If you can\'t choose a nonwhite nonblack creature to target when the haunted creature is put into a graveyard, or if the target becomes illegal, you don\'t get a token.').

card_ruling('sekki, seasons\' guide', '2005-06-01', 'Damage that can\'t be prevented still causes counters to be removed from Sekki and puts creature tokens onto the battlefield.').
card_ruling('sekki, seasons\' guide', '2005-06-01', 'Creature tokens are put onto the battlefield even if there are no more +1/+1 counters to remove. For example, attempting to deal 9 damage to Sekki causes its replacement effect to 1) prevent the 9 damage, 2) remove all eight of the +1/+1 counters, and 3) put nine Spirit creature tokens onto the battlefield.').
card_ruling('sekki, seasons\' guide', '2005-06-01', 'Increasing Sekki\'s toughness with a card like Indomitable Will allows Sekki to prevent damage and make tokens even it if has no +1/+1 counters to be removed.').
card_ruling('sekki, seasons\' guide', '2005-08-01', 'You can sacrifice any eight Spirits to pay for Sekki\'s activated ability, not just the Spirit creature tokens.').

card_ruling('selesnya guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('selesnya guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('selesnya keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('selesnya keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('selesnya loft gardens', '2012-06-01', 'Effects that place a counter on a permanent include that permanent entering the battlefield. For example, a creature that normally enters the battlefield with one or more +1/+1 counters will enter with twice that many +1/+1 counters on it. A planeswalker will enter the battlefield with twice its starting loyalty counters.').
card_ruling('selesnya loft gardens', '2012-06-01', 'Adding loyalty counters to a planeswalker in order to activate a loyalty ability isn\'t an effect, so those counters are not doubled.').
card_ruling('selesnya loft gardens', '2012-06-01', 'Any -1/-1 counters that are placed on a creature because of wither or infect are also not doubled.').
card_ruling('selesnya loft gardens', '2012-06-01', 'The chaos ability is cumulative. For example, if you roll {C} twice in a turn, tapping a land for mana will cause two mana abilities to trigger.').

card_ruling('selesnya sanctuary', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('selfless cathar', '2011-09-22', 'You can activate Selfless Cathar\'s ability even if you control no other creatures.').
card_ruling('selfless cathar', '2011-09-22', 'Only creatures you control when Selfless Cathar\'s ability resolves will be affected. Creatures that enter the battlefield or that you gain control of later in the turn will not.').

card_ruling('selfless exorcist', '2013-04-15', 'The reminder text printed on Selfless Exorcist is no longer correct. Abilities that define a creature\'s power (and toughness) apply in every zone. The value of * is defined by such an ability.').

card_ruling('selkie hedge-mage', '2008-08-01', 'Each of the triggered abilities has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control two or more lands of the appropriate land type when the Hedge-Mage enters the battlefield, and (2) the ability will do nothing if you don\'t control two or more lands of the appropriate land type by the time it resolves.').
card_ruling('selkie hedge-mage', '2008-08-01', 'Both abilities trigger at the same time. You can put them on the stack in any order.').
card_ruling('selkie hedge-mage', '2008-08-01', 'Each of the triggered abilities look at your lands individually. This means that if you only control two dual-lands of the appropriate types, both of the abilities will trigger').
card_ruling('selkie hedge-mage', '2008-08-01', 'Both abilities are optional; you choose whether to use them when they resolve. If an ability has a target, you must choose a target even if you don\'t plan to use the ability.').

card_ruling('selvala\'s charge', '2014-05-29', 'Except in some very rare cases, the card each player draws will be the card revealed from the top of his or her library.').

card_ruling('selvala\'s enforcer', '2014-05-29', 'If Selvala’s Enforcer leaves the battlefield before its ability resolves, the top card of each library will still be revealed and drawn, although no +1/+1 counters will be placed.').
card_ruling('selvala\'s enforcer', '2014-05-29', 'Except in some very rare cases, the card each player draws will be the card revealed from the top of his or her library.').

card_ruling('selvala, explorer returned', '2014-05-29', 'Selvala’s parley ability is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('selvala, explorer returned', '2014-05-29', 'If you activate Selvala’s ability while casting a spell, and you discover you can\'t produce enough mana to pay that spell’s costs, the spell is reversed. The spell returns to whatever zone you were casting it from. You may reverse other mana abilities you activated while casting the spell, but Selvala’s ability can\'t be reversed. Whatever mana that ability produced will be in your mana pool and each player will have drawn a card.').
card_ruling('selvala, explorer returned', '2014-05-29', 'Except in some very rare cases, the card each player draws will be the card revealed from the top of his or her library.').

card_ruling('semblance anvil', '2011-01-01', 'Artifact, creature, enchantment, instant, planeswalker, sorcery, and tribal are card types.').
card_ruling('semblance anvil', '2011-01-01', 'Spells you cast that share multiple card types with the exiled card still cost just {2} less to cast. They don\'t get a greater cost reduction.').
card_ruling('semblance anvil', '2011-01-01', 'Semblance Anvil takes the total cost to cast a spell into account, not just its mana cost. This includes additional costs (such as kicker) and alternative costs (such as evoke). It also includes cost increases from other sources (such as Lodestone Golem\'s ability).').
card_ruling('semblance anvil', '2011-01-01', 'Semblance Anvil won\'t affect the part of a spell\'s cost represented by colored mana symbols. For example, a spell that normally costs {1}{U} to cast would cost {U} if it shared a card type with the exiled card.').
card_ruling('semblance anvil', '2011-01-01', 'Semblance Anvil can reduce the amount you pay for a spell\'s cost that includes {X}. For example, Blaze is a sorcery that costs {X}{R}. If the card exiled with Semblance Anvil is a sorcery, and you want to cast Blaze with X equal to 5, you\'ll have to pay only {3}{R}. This is true even if the ability states that {X} must be paid with a certain color of mana (as Drain Life does).').
card_ruling('semblance anvil', '2011-01-01', 'Semblance Anvil\'s cost reduction effect is mandatory.').

card_ruling('sen triplets', '2009-05-01', 'Playing a card this way works just like playing any other card, with one exception: You\'re playing the card from your opponent\'s hand rather than your own. To be specific: -- A card played this way follows the normal timing rules for its card type, as well as any other applicable restrictions such as \"Cast [this card] only during combat.\" For example, you can\'t play a card from the targeted opponent\'s hand during your end step unless it\'s an instant or has flash. -- To cast a nonland card from your opponent\'s hand this way, you must pay its mana cost. -- You can play a land card from the targeted opponent\'s hand only if you haven\'t played a land yet that turn.').
card_ruling('sen triplets', '2009-05-01', 'You control the spells you cast from your opponent\'s hand, so you make decisions for them as appropriate. For example, if one of those spells says \"target creature you control,\" you\'ll target one of your creatures, not one of your opponent\'s creatures.').
card_ruling('sen triplets', '2009-05-01', 'If you cast a spell with cascade this way, you\'ll control the cascade ability. When it resolves, you\'ll find a new card to cast from your own library.').
card_ruling('sen triplets', '2009-05-01', 'You can cast spells from the targeted player\'s hand, but you can\'t activate abilities. Specifically, you can\'t cycle cards in that player\'s hand.').
card_ruling('sen triplets', '2009-05-01', 'This ability doesn\'t prevent the targeted opponent\'s triggered abilities from triggering. If one does, that player puts it on the stack and, if applicable, chooses targets for it. Those abilities will resolve as normal.').
card_ruling('sen triplets', '2009-05-01', 'If the resolution of a spell or ability involves having the targeted player cast a spell, that part of the effect simply won\'t work.').
card_ruling('sen triplets', '2009-05-01', 'After this ability resolves, the targeted player still gets priority at the appropriate times, and can still perform special actions (such as turning a face-down creature face up). However, since that player can\'t cast any spells or activate abilities -- including mana abilities -- there\'s not much he or she can actually accomplish this way.').
card_ruling('sen triplets', '2009-05-01', 'If you start to play a card from the targeted opponent\'s hand, there\'s no way for the opponent to get rid of that card in response (like discarding it to pay Gathan Raiders\'s morph cost, for example). That\'s because the first thing that happens when you start to cast a spell is that it moves to the stack, so it\'s not in your opponent\'s hand anymore.').
card_ruling('sen triplets', '2009-05-01', 'Once Sen Triplets\'s ability resolves, it remains in effect for the rest of the turn whether or not you still control Sen Triplets.').

card_ruling('send to sleep', '2015-06-22', 'If the spell mastery ability applies and a creature affected by Send to Sleep changes controllers before its old controller’s next untap step, Send to Sleep will prevent it from becoming untapped during its new controller’s next untap step.').
card_ruling('send to sleep', '2015-06-22', 'Send to Sleep can target tapped creatures. If a targeted creature is already tapped when the spell resolves (and the spell mastery ability applies), that creature remains tapped and doesn’t untap during its controller’s next untap step.').
card_ruling('send to sleep', '2015-06-22', 'If you chose two targets and one is an illegal target when Send to Sleep resolves, that creature won’t become tapped and it won’t be stopped from untapping during its controller’s next untap step (if the spell mastery ability applies). It won’t be affected by Send to Sleep in any way.').
card_ruling('send to sleep', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('sengir autocrat', '2004-10-04', 'When it leaves the battlefield, it exiles all Serf tokens on the battlefield and not just the ones it generated or just the ones under your control.').

card_ruling('sengir nosferatu', '2006-09-25', 'The Sengir Nosferatu that the Bat token\'s ability returns to the battlefield doesn\'t have to be the same one that created it, and it doesn\'t even have to be owned by the same player.').
card_ruling('sengir nosferatu', '2007-05-01', 'If the Nosferatu is copied (by Clone, for example) and then the copy is exiled by the first activated ability, it won\'t be able to come back using the second activated ability, since it\'s no longer named Sengir Nosferatu in the Exile zone.').

card_ruling('sengir vampire', '2007-07-15', 'If Sengir Vampire deals nonlethal damage to a creature and then a different effect or damage source causes that creature to be put into a graveyard later in the turn, Sengir Vampire\'s ability will trigger and it will get a +1/+1 counter.').
card_ruling('sengir vampire', '2011-09-22', 'Each time a creature is put into a graveyard from the battlefield, check whether Sengir Vampire had dealt any damage to it at any time during that turn. If so, Sengir Vampire\'s ability will trigger. It doesn\'t matter who controlled the creature or whose graveyard it was put into.').
card_ruling('sengir vampire', '2011-09-22', 'If Sengir Vampire and a creature it dealt damage to are both put into a graveyard at the same time, Sengir Vampire\'s ability will trigger, but it will do nothing when it resolves.').

card_ruling('sensation gorger', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('sensation gorger', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('sensation gorger', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('sensation gorger', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('sensation gorger', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('sensation gorger', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('sensei golden-tail', '2004-12-01', 'The training counter just marks which creatures have been changed; an effect that removes the counter doesn\'t change the creature\'s types or abilities.').
card_ruling('sensei golden-tail', '2004-12-01', 'Note that multiple instances of the bushido ability each trigger separately.').

card_ruling('sentinel', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('sentinel dispatch', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('sentinel dispatch', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('sentinel dispatch', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('sentinel dispatch', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('sentinel dispatch', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('sentinel dispatch', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('sentinel dispatch', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('sentinel sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('sentinel sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('sentry oak', '2007-10-01', 'Specifically, this ability triggers when the beginning of combat step starts during your turn.').

card_ruling('separatist voidmage', '2015-06-22', 'Separatist Voidmage’s ability can target itself.').

card_ruling('septic rats', '2011-06-01', 'A player is poisoned if that player has one or more poison counters.').
card_ruling('septic rats', '2011-06-01', 'Septic Rats gets a maximum of +1/+1 from its ability, no matter how many poison counters the defending player has.').

card_ruling('sepulchral primordial', '2013-01-24', 'You can choose a number of targets up to the number of opponents you have, one target per opponent.').
card_ruling('sepulchral primordial', '2013-01-24', 'The creature cards enter the battlefield simultaneously.').

card_ruling('seraph', '2004-10-04', 'You do not get the creature back if the creature is not still in the graveyard at the end of the turn.').
card_ruling('seraph', '2007-09-16', 'If Seraph isn\'t on the battlefield at the time the other creature is put into a graveyard, the ability won\'t trigger. However, the ability does trigger if Seraph and the other creature are put into the graveyard at the same time.').
card_ruling('seraph', '2007-09-16', 'If the other creature is a token, it will cease to exist after being exiled. It won\'t be returned to the battlefield.').
card_ruling('seraph', '2007-09-16', 'If the other creature ceases to be a creature card after it leaves the battlefield (it\'s an animated Mishra\'s Factory, for example), it will be returned to the battlefield by this ability and you will have to sacrifice it when you lose control of Seraph.').
card_ruling('seraph', '2007-09-16', 'Once the ability triggers, you\'ll return the card to the battlefield under your control at the end of the turn even if you no longer control Seraph at that time. In fact, if Seraph leaves the battlefield before the card is returned to the battlefield, you\'ll never have to sacrifice it as a result of this ability. If Seraph remains on the battlefield but changes controllers before the card is returned, you\'ll still get the returned card. However, in that scenario, you\'ll still have to sacrifice it if you later regain control Seraph and then lose control of it once more.').

card_ruling('seraph of the masses', '2014-07-18', 'The ability that defines Seraph of the Masses’s power and toughness works in all zones, not just the battlefield.').
card_ruling('seraph of the masses', '2014-07-18', 'As long as Seraph of the Masses is on the battlefield, its ability will count itself.').
card_ruling('seraph of the masses', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('seraph of the masses', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('seraph of the masses', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('seraph of the masses', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('seraph of the masses', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('seraph of the masses', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('seraph of the masses', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('seraph of the sword', '2013-07-01', 'If Seraph of the Sword blocks a creature with trample, that creature’s controller must assign 3 damage to Seraph of the Sword (assuming it’s blocking no other creatures, there was no damage marked on it, and nothing has changed its toughness). The remainder can be assigned to the defending player or planeswalker.').

card_ruling('serendib djinn', '2004-10-04', 'The sacrifice of the land is done during the resolution of the triggered ability. You also choose the land at that time.').

card_ruling('serendib sorcerer', '2007-02-01', 'This is the timeshifted version of Sorceress Queen.').

card_ruling('serene master', '2013-10-17', 'When Serene Master’s ability resolves, its power becomes equal to the former power of the target creature. At the same time, that creature’s power will become equal to Serene Master’s former power.').
card_ruling('serene master', '2013-10-17', 'Any power-modifying effects, counters, Auras, or Equipment will apply to the creatures’ new powers. For example, say Serene Master is enchanted with Lightning Talons, which gives it +3/+0, and it blocks a 5/5 creature. After the exchange, Serene Master would be an 8/2 creature (its power became 5, which was then modified by Lightning Talons), and the other creature would be 3/5.').
card_ruling('serene master', '2013-10-17', 'The exchange is made only if both Serene Master and the target creature are on the battlefield when the ability resolves.').
card_ruling('serene master', '2013-10-17', 'If Serene Master blocks multiple creatures, its ability will trigger only once, and it will exchange power with only the target creature. (Note that Serene Master can’t block multiple creatures normally.)').

card_ruling('serene remembrance', '2013-01-24', 'You may choose zero targets when you cast Serene Remembrance. If you do, you\'ll shuffle Serene Remembrance into its owner\'s library when it resolves.').
card_ruling('serene remembrance', '2013-01-24', 'If you choose at least one target for Serene Remembrance, and all of its targets are illegal when Serene Remembrance tries to resolve, it will be countered and none of its effects will happen. You won\'t shuffle any library and Serene Remembrance will be put into its owner\'s graveyard.').
card_ruling('serene remembrance', '2013-01-24', 'If you give Serene Remembrance the flashback ability and cast it from your graveyard, it will be exiled instead of being shuffled into your library. Any other targets will still be shuffled into their owners\' library, however.').

card_ruling('serene steward', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('serene steward', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('serene steward', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('serenity', '2008-04-01', 'Serenity\'s triggered ability destroys itself too.').

card_ruling('serpent warrior', '2004-10-04', 'You can cast this if you have less than 3 life, since the life is lost as an effect and not a payment.').

card_ruling('serpentine spike', '2015-08-25', 'Each target must be a different creature. You can’t cast Serpentine Spike without three different creatures available.').
card_ruling('serpentine spike', '2015-08-25', 'If one or two of those targets become illegal by the time Serpentine Spike resolves, you can’t change how much damage will be dealt to the remaining legal targets.').
card_ruling('serpentine spike', '2015-08-25', 'A creature doesn’t necessarily have to be dealt lethal damage by Serpentine Spike to be exiled. After being dealt damage, if it would die for any reason that turn, it’ll be exiled instead.').
card_ruling('serpentine spike', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('serpentine spike', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('serpentine spike', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('serpentine spike', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('serpentine spike', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('serra ascendant', '2010-08-15', 'In a Two-Headed Giant game, anything that cares about your life total checks your team\'s life total. (This is a change from previous rules.) Serra Ascendant gets +5/+5 and has flying as long as your team has 30 or more life.').

card_ruling('serra avatar', '2008-10-01', 'Whether or not the ability triggers depends on if the Avatar has the ability while in the graveyard. If it doesn\'t (thanks to Yixlid Jailer, for instance), then the ability won\'t trigger. It doesn\'t matter whether the Avatar had the ability in its previous zone (it will trigger even if it had been on the battlefield with Humility).').
card_ruling('serra avatar', '2012-07-01', 'Serra Avatar\'s power and toughness are constantly updated as your life total changes.').
card_ruling('serra avatar', '2012-07-01', 'The ability that defines Serra Avatar\'s power and toughness works in all zones, not just the battlefield.').
card_ruling('serra avatar', '2012-07-01', 'If Serra Avatar is no longer in your graveyard when its triggered ability resolves, you won\'t shuffle your library.').

card_ruling('serra avenger', '2006-09-25', 'You can cast Serra Avenger during another player\'s first, second, or third turns of the game if some other effect (such as Vedalken Orrery) enables that.').
card_ruling('serra avenger', '2012-07-01', 'Serra Avenger cares about how many turns you have taken, not necessarily how many turns the game has had, in case you take an extra turn.').
card_ruling('serra avenger', '2012-07-01', 'If the game is restarted (by Karn Liberated), you can\'t cast Serra Avenger in your first, second, or third turn in the new game.').

card_ruling('serra aviary', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('serra bestiary', '2004-10-04', 'Does prevent the use of activated mana abilities with {Tap} in the cost.').
card_ruling('serra bestiary', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('serra sphinx', '2007-02-01', 'This is the timeshifted version of Serra Angel.').

card_ruling('serra\'s hymn', '2004-10-04', 'You count the number of counters, then divide up the prevention during the announcement of the ability. Each targeted creature receives the appropriate amount of damage prevention when the ability resolves.').
card_ruling('serra\'s hymn', '2004-10-04', 'If there is at least one verse counter on this, you can\'t choose zero targets. You must choose between 1 and X targets.').
card_ruling('serra\'s hymn', '2004-10-04', 'You can activate the ability (choosing no targets) even if there are no verse counters on it. It doesn\'t do anything useful if you do this.').
card_ruling('serra\'s hymn', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('serra\'s liturgy', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('serrated arrows', '2008-08-01', 'The upkeep trigger checks the number of counters at the start of upkeep, and only goes on the stack if there are no arrowhead counters at that time. It will check again on resolution, and will do nothing if you\'ve somehow manage to get a new arrowhead counter on the Arrows.').

card_ruling('serrated biskelion', '2008-04-01', 'If Serrated Biskelion leaves the battlefield before its ability resolves, you\'ll still put a -1/-1 counter on the targeted creature. If the target leaves the battlefield or otherwise becomes an illegal target before the ability resolves, the ability is countered and you won\'t put a -1/-1 counter on Serrated Biskelion.').

card_ruling('serum powder', '2004-12-01', 'You can use Serum Powder\'s second ability only while it\'s in your hand. If this card is in your hand, you can choose either to mulligan or to use Serum Powder\'s ability. Using the ability doesn\'t prevent you from taking further mulligans, and taking a mulligan doesn\'t prevent you from using a Serum Powder\'s ability if you happen to draw one.').
card_ruling('serum powder', '2004-12-01', 'Unlike a normal mulligan, Serum Powder lets you draw the same number of cards that were in your hand, not one less. If you have already taken one mulligan and your hand contains six cards including a Serum Powder, you can choose to exile those six cards and draw six new cards.').
card_ruling('serum powder', '2004-12-01', 'The cards in your hand are exiled for the rest of the game; they aren\'t shuffled back into your library if you take another mulligan. Cards exiled are always face up unless the effect that exiled them says they aren\'t. Be sure to return those cards to your deck for your next game.').

card_ruling('servant of tymaret', '2014-02-01', 'A creature taps when its regeneration shield is used, not when the shield is created. Notably, you won’t be able to use Servant of Tymaret’s last ability to tap it so that its inspired ability will trigger unless Servant of Tymaret would be destroyed.').
card_ruling('servant of tymaret', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('servant of tymaret', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('servant of tymaret', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('servant of tymaret', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('serve', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('serve', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('serve', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('serve', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('serve', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('serve', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('serve', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('serve', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('serve', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('serve', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('serve', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('set adrift', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('set adrift', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('set adrift', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('set adrift', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('set adrift', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('setessan battle priest', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('setessan battle priest', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('setessan battle priest', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('setessan griffin', '2013-09-15', 'If Setessan Griffin’s ability is activated by one player, then another player gains control of it during the same turn, the second player can’t activate its ability that turn.').

card_ruling('setessan oathsworn', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('setessan oathsworn', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('setessan oathsworn', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('setessan tactics', '2014-04-26', 'Each of the two creatures that fight deals damage equal to its power to the other. If either one of the creatures isn’t on the battlefield when the ability that instructs them to fight resolves, no damage is dealt.').
card_ruling('setessan tactics', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('setessan tactics', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('setessan tactics', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('setessan tactics', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('setessan tactics', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('sever the bloodline', '2011-09-22', 'Only creatures on the battlefield will be exiled. In other zones, they\'re \"creature cards,\" not \"creatures.\"').
card_ruling('sever the bloodline', '2011-09-22', 'A double-faced creature only has the name of the face that\'s up. For example, if Village Ironsmith is targeted by Sever the Bloodline, Ironfang wouldn\'t be exiled.').
card_ruling('sever the bloodline', '2011-09-22', 'If the targeted creature is an illegal target by the time Sever the Bloodline resolves, it will be countered and none of its effects will happen. No creatures will be exiled.').
card_ruling('sever the bloodline', '2011-09-22', 'Unless a token is a copy of another creature or was explicitly given a name by the effect that created it, its name is the creature types it was given when it was created. For example, the Zombie tokens created by Army of the Damned and by Cellar Door are all named \"Zombie.\"').

card_ruling('sewer nemesis', '2011-09-22', 'If the chosen player leaves the game, Sewer Nemesis\'s power and toughness each become 0. Unless another effect is raising its toughness above 0, it will be put into its owner\'s graveyard as a state-based action.').

card_ruling('sewer shambler', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('sewers of estark', '2008-08-01', 'This spell can target any creature. The check for whether that creature is attacking or blocking made on resolution, and it will have the appropriate effect at that time.').
card_ruling('sewers of estark', '2008-08-01', 'If the creature is neither an attacking creature nor a blocking creature at the time the spell resolves, it will have no effect.').

card_ruling('shackles', '2004-10-04', 'Shackles does not tap the creature when it enters the battlefield.').

card_ruling('shade of trokair', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('shade of trokair', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('shade of trokair', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('shade of trokair', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('shade of trokair', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('shade of trokair', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shade of trokair', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('shade of trokair', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shade of trokair', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('shade of trokair', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('shadow alley denizen', '2013-01-24', 'The black creature that entered the battlefield can be chosen as the target of Shadow Alley Denizen\'s ability, but it won\'t be able to attack that turn unless it also has haste.').
card_ruling('shadow alley denizen', '2013-01-24', 'Shadow Alley Denizen can be chosen as the target of its own ability.').

card_ruling('shadow of doubt', '2005-10-01', 'If an effect says \"You may search your library . . . If you do, shuffle your library,\" you can\'t choose to search since it\'s impossible, and you won\'t shuffle.').
card_ruling('shadow of doubt', '2005-10-01', 'If an effect says \"Search your library . . . Then shuffle your library,\" the search effect fails, but you will have to shuffle.').
card_ruling('shadow of doubt', '2005-10-01', 'Since players can\'t search, players won\'t be able to find any cards in a library. The effect applies to all players and all libraries. If a spell or ability\'s effect has other parts that don\'t depend on searching for or finding cards, they will still work normally.').
card_ruling('shadow of doubt', '2005-10-01', 'Effects that tell a player to reveal cards from a library or look at cards from the top of a library will still work. Only effects that use the word \"search\" will fail.').

card_ruling('shadow slice', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('shadow slice', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('shadow slice', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('shadow slice', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('shadow slice', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('shadow slice', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('shadow slice', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('shadow slice', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('shadow slice', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('shadow slice', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('shadow slice', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('shadow sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('shadow sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('shadowbane', '2008-08-01', 'This only prevents damage the chosen source would deal to you or a creature you control. If the creature would deal damage to another player, a planeswalker, or a creature you don\'t control, it does not get prevented.').

card_ruling('shadowblood egg', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('shadowborn apostle', '2013-07-01', 'Shadowborn Apostle’s first ability lets you ignore only the “four-of” rule. It doesn’t let you ignore format legality.').
card_ruling('shadowborn apostle', '2013-07-01', 'You sacrifice the six creatures when you activate the ability. You can’t respond to that ability by activating the ability of one of the other Shadowborn Apostles you just sacrificed.').
card_ruling('shadowborn apostle', '2013-07-01', 'You don’t have to sacrifice the Shadowborn Apostle whose ability you activated. It can be six other Shadowborn Apostles.').

card_ruling('shadowborn demon', '2013-07-01', 'Shadowborn Demon’s enters-the-battlefield ability is mandatory. If you control the only non-Demon creature, you must choose it as the target.').
card_ruling('shadowborn demon', '2013-07-01', 'The last ability checks whether you have fewer than six creature cards in your graveyard when it would trigger. If you have six or more, it won’t trigger at all. The ability will check again when it tries to resolve. If at that time you have six or more, the ability won’t do anything.').

card_ruling('shadowfeed', '2008-10-01', 'If the targeted card is removed from the graveyard before Shadowfeed resolves, Shadowfeed is countered. You won\'t gain 3 life.').

card_ruling('shadows of the past', '2015-06-22', 'Once you legally activate the last ability, it doesn’t matter how many creature cards are in your graveyard as it resolves.').

card_ruling('shah of naar isle', '2007-05-01', 'When the echo ability resolves, you may pay {0} or choose not pay. The first option means Shah of Naar Isle stays on the battlefield and its last ability triggers. The second option means Shah of Naar Isle is sacrificed.').
card_ruling('shah of naar isle', '2007-05-01', 'If you use Thick-Skinned Goblin\'s ability to pay {0} instead of Shah of Naar Isle\'s echo cost, the Shah\'s last ability will trigger.').
card_ruling('shah of naar isle', '2007-05-01', 'If you have multiple opponents, each one in turn order chooses whether or not to draw cards, and if so, how many to draw (from zero to three).').

card_ruling('shahrazad', '2004-10-04', 'At the start of the sub-game both players draw their initial hand (usually 7 cards). If one player has fewer cards than required, that player loses. If both have fewer than required, both players lose.').
card_ruling('shahrazad', '2004-10-04', 'Events in a Shahrazad sub-game do not normally trigger abilities in the main game. And continuous effects in the main game do not carry over into the sub-game.').
card_ruling('shahrazad', '2004-10-04', 'You randomly chose which player chooses to go first or draw first.').
card_ruling('shahrazad', '2007-07-15', 'At the end of a subgame, each player puts all cards he or she owns that are in the subgame into his or her library in the main game, then shuffles them. This includes cards in the subgame\'s Exile zone (this is a change from previous rulings).').

card_ruling('shaleskin plower', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('shallow grave', '2004-10-04', 'Only exiles the creature if the creature is still on the battlefield at the beginning of the end step.').

card_ruling('shaman en-kor', '2004-10-04', 'The ability of this card does not do anything to stop Trample damage from being assigned to the defending player.').
card_ruling('shaman en-kor', '2004-10-04', 'It can redirect damage to itself.').
card_ruling('shaman en-kor', '2004-10-04', 'It is possible to redirect more damage to a creature than that creature\'s toughness.').
card_ruling('shaman en-kor', '2004-10-04', 'You can use this ability as much as you want prior to damage being dealt.').
card_ruling('shaman en-kor', '2013-07-01', 'When you redirect combat damage it is still combat damage.').

card_ruling('shaman of forgotten ways', '2015-02-25', 'The first ability of Shaman of Forgotten Ways is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'The mana produced by Shaman of Forgotten Ways can be used to cast face-down creature spells with morph or megamorph.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'Count the number of creatures each player controls when the last ability resolves to determine what number each player’s life total will become.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'For a player’s life total to become a specific number, the player gains or loses the appropriate amount of life. For example, if a player’s life total is 12 and that player controls three creatures as the ability resolves, he or she will lose 9 life. Other effects that interact with life gain or loss will interact with this effect accordingly.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'If a player controls no creatures as the last ability resolves, his or her life total will become 0, causing that player to lose the game. If all players’ life totals become 0 this way, the game will be a draw.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('shaman of forgotten ways', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('shaman of the pack', '2015-06-22', 'Count the number of Elves you control as Shaman of the Pack’s ability resolves, including Shaman of the Pack if it’s still on the battlefield, to determine how much life is lost.').

card_ruling('shaman\'s trance', '2004-10-04', 'This card does not allow you to activate the abilities of cards in other people\'s graveyards.').
card_ruling('shaman\'s trance', '2004-10-04', 'You can use Flashback on cards to cast them.').

card_ruling('shamanic revelation', '2014-11-24', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('shambleshark', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('shambleshark', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('shambleshark', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('shambleshark', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('shambleshark', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('shambleshark', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('shambling attendants', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('shambling attendants', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('shambling attendants', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('shambling attendants', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('shambling attendants', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('shambling swarm', '2008-04-01', 'The -1/-1 counters you remove don\'t have to be the same counters put on the creature by Shambling Swarm.').

card_ruling('shambling vent', '2015-08-25', 'Multiple instances of lifelink are redundant. Activating the last ability more than once won’t cause you to gain additional life if Shambling Vent deals damage.').
card_ruling('shambling vent', '2015-08-25', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature nor when it entered the battlefield.').
card_ruling('shambling vent', '2015-08-25', 'This land is colorless until the last ability gives it colors.').
card_ruling('shambling vent', '2015-08-25', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won’t trigger.').
card_ruling('shambling vent', '2015-08-25', 'An ability that turns a land into a creature also sets that creature’s power and toughness. If the land was already a creature (for example, if it was the target of a spell with awaken), this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness will continue to apply no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness. For example, if Shambling Vent has been made a 0/0 creature with three +1/+1 counters on it, activating its last ability will turn it into a 5/6 creature that’s still a land.').

card_ruling('shape anew', '2011-01-01', 'If the targeted artifact is an illegal target by the time Shape Anew resolves, the spell is countered. Nothing else happens.').
card_ruling('shape anew', '2011-01-01', 'If the first card the player reveals is an artifact card, he or she will still have to shuffle his or her library even though no other cards were revealed this way.').
card_ruling('shape anew', '2011-01-01', 'If there are no artifact cards in the player\'s library, all the cards in that library are revealed, then the library is shuffled. (The targeted artifact remains sacrificed.)').
card_ruling('shape anew', '2011-01-01', 'If the targeted artifact\'s controller can\'t sacrifice it (due to Tajuru Preserver, perhaps), the other effects of the spell will still happen.').

card_ruling('shape of the wiitigo', '2006-07-15', 'If Shape of the Wiitigo leaves the battlefield but the creature doesn\'t, the +1/+1 counters stay on the creature.').

card_ruling('shape stealer', '2005-06-01', 'This ability triggers once for each creature blocked by or blocking Shape Stealer. If multiple creatures block it, Shape Stealer\'s power and toughness will change for each one in succession. The first trigger put on the stack will be the last to resolve, so that will set Shape Stealer\'s final power and toughness.').
card_ruling('shape stealer', '2005-06-01', 'The other creature\'s power and toughness are determined as the triggered ability resolves.').
card_ruling('shape stealer', '2005-06-01', 'If the other creature has bushido, whether Shape Stealer gets the bushido bonus depends on whose turn it is. If Shape Stealer is blocking, it won\'t get the attacker\'s bushido bonus. If the Shape Stealer is being blocked, the bushido ability will resolve just before Shape Stealer determines what the creature\'s power and toughness are.').
card_ruling('shape stealer', '2005-06-01', 'If Shape Stealer\'s new toughness is less than any damage on Shape Stealer, Shape Stealer will be destroyed as a state-based action once the triggered ability resolves.').
card_ruling('shape stealer', '2005-06-01', 'Once the ability resolves, changing the other creature\'s power or toughness doesn\'t affect Shape Stealer.').
card_ruling('shape stealer', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('shaper parasite', '2007-02-01', 'You choose whether the targeted creature gets +2/-2 or -2/+2 as the ability resolves.').

card_ruling('shapesharer', '2007-10-01', 'If the targeted Shapeshifter becomes an illegal target but the targeted creature doesn\'t, the ability will resolve but have no effect.').
card_ruling('shapesharer', '2007-10-01', 'If the targeted creature becomes an illegal target but the targeted Shapeshifter doesn\'t, the Shapeshifter will still become a copy of the object. The effect will use the illegal target\'s current copiable characteristics if it\'s still on the battlefield, or its last known copiable characteristics if it has left the battlefield.').
card_ruling('shapesharer', '2007-10-01', 'The copy effect doesn\'t wear off until just before your next untap step (even if an effect will cause that untap step to be skipped).').
card_ruling('shapesharer', '2007-10-01', 'If Shapesharer itself becomes a copy of another creature, it loses both changeling and its activated ability (unless it\'s copying another creature with changeling and/or another Shapesharer, of course).').
card_ruling('shapesharer', '2007-10-01', 'This ability can cause a creature to become a copy of itself. This will usually have no visible effect.').
card_ruling('shapesharer', '2007-10-01', 'The targeted Shapeshifter copies the printed values of the targeted creature, plus any copy effects that have been applied to it. It won\'t copy counters on that creature. It won\'t copy effects that have changed the creature\'s power, toughness, types, color, and so on.').
card_ruling('shapesharer', '2007-10-01', 'If the targeted Shapeshifter copies a creature that\'s copying a creature, it will become whatever the chosen creature is copying.').
card_ruling('shapesharer', '2007-10-01', 'If the targeted Shapeshifter becomes a copy of a face-down creature, it will become a 2/2 creature with no name, creature type, abilities, mana cost, or color. It will not become face down and thus can\'t be turned face up.').
card_ruling('shapesharer', '2007-10-01', 'Effects that have already applied to the targeted Shapeshifter will continue to apply to it. For example, if Giant Growth had given it +3/+3 earlier in the turn, then this ability makes it a copy of Grizzly Bears, it will be a 5/5 Grizzly Bears.').

card_ruling('shapeshifter', '2008-08-01', 'The ability that sets its power and toughness is a characteristic-defining ability. This means it applies before any other effects that modify power and/or toughness.').
card_ruling('shapeshifter', '2008-08-01', 'The ability that lets you choose a new number is a triggered ability that triggers only once each upkeep.').

card_ruling('shapeshifter\'s marrow', '2007-05-01', 'If the revealed card isn\'t a creature card, it stays on top of that player\'s library. Shapeshifter\'s Marrow remains an enchantment.').
card_ruling('shapeshifter\'s marrow', '2007-05-01', 'If the revealed card is a creature card, Shapeshifter\'s Marrow becomes a copy of that card permanently. It\'s no longer an enchantment.').
card_ruling('shapeshifter\'s marrow', '2007-05-01', 'In a Two-Headed Giant game, this ability triggers separately for each player at the start of the opposing team\'s turn. If the first ability to resolve reveals a creature card, Shapeshifter\'s Marrow becomes a creature. If the second ability to resolve also reveals a creature card, the ex-Shapeshifter\'s Marrow then becomes a copy of that creature.').

card_ruling('shard convergence', '2009-02-01', 'You don\'t have to find all four cards.').
card_ruling('shard convergence', '2009-02-01', 'Shard Convergence checks for cards with the subtypes Plains, Island, Swamp, and Mountain, not for cards with those names. You may find nonbasic land cards this way, as long as they have appropriate subtypes.').
card_ruling('shard convergence', '2009-02-01', 'You may find \"dual lands\" with Shard Convergence. The Plains card you find, for example, can have a second subtype. What subtypes it has won\'t impact what other cards you can find. For example, you may find a Hallowed Fountain (which has subtypes Plains and Island) as the Plains card, and another Hallowed Fountain as the Island card.').

card_ruling('shard phoenix', '2005-08-01', 'You can return Shard Phoenix any time during your upkeep; this is not a beginning of upkeep triggered ability.').
card_ruling('shard phoenix', '2005-08-01', 'Shard Phoenix uses Last Known Information. If, for example, it was blue when it was sacrificed, the damage will come from a blue source.').

card_ruling('sharding sphinx', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('sharding sphinx', '2008-10-01', 'If combat damage is dealt to players by multiple artifact creatures you control (including, perhaps, Sharding Sphinx itself), Sharding Sphinx\'s ability will trigger that many times. You\'ll get that many Thopter tokens. You\'ll get just one token per creature, no matter how much combat damage that creature dealt.').

card_ruling('shared animosity', '2008-04-01', 'This ability counts creatures, not creature types. For example, if you attack with five creatures -- an Elf Shaman, an Elf Warrior, a Goblin Shaman, an Elemental, and a creature with all creature types -- the ability will trigger five times. Those creatures will get +3/+0, +2/+0, +2/+0, +1/+0, and +4/+0, respectively.').
card_ruling('shared animosity', '2008-04-01', 'In a Two-Headed Giant game, only creatures you control trigger the ability and get the bonus, but your teammate\'s attacking creatures are included in the calculation of those bonuses.').

card_ruling('shared fate', '2004-12-01', 'You need to pay the costs of any cards you play from the Exile zone. This could be a problem if you don\'t have the right colors of mana available.').
card_ruling('shared fate', '2004-12-01', 'Replacing your draws isn\'t optional. You can\'t draw cards from your own library, even if all your opponents\' libraries are empty.').
card_ruling('shared fate', '2004-12-01', 'If more than one Shared Fate is on the battlefield, you choose which one replaces each card draw, but you can replace a draw only once.').
card_ruling('shared fate', '2004-12-01', 'The cards are exiled, not put onto the players\' hands. Players can look at and play the exiled cards, but can\'t do anything else with them (the exiled cards can\'t be discarded or cycled, for example).').
card_ruling('shared fate', '2008-08-01', 'Each Shared Fate tracks only the cards it exiled. If the Shared Fate which was responsible for a card being exiled leaves the battlefield, putting another Shared Fate onto the battlefield will not allow you to look at or play that card again.').

card_ruling('shatter', '2004-10-04', 'Regenerating artifacts can regenerate from this.').

card_ruling('sheer drop', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('sheer drop', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('sheer drop', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('sheer drop', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('sheer drop', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('sheer drop', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('shelkin brownie', '2004-10-04', 'Can only remove \"bands with other\" and not normal \"banding\" ability.').

card_ruling('shell of the last kappa', '2004-12-01', 'Shell of the Last Kappa\'s first ability can only target a spell that targets you.').
card_ruling('shell of the last kappa', '2004-12-01', 'Exiling a spell prevents that spell from resolving, but it doesn\'t technically \"counter\" anything. This means that Shell of the Last Kappa can exile spells which \"can\'t be countered.\"').

card_ruling('shelldock isle', '2007-10-01', 'It doesn\'t matter which library has twenty or fewer cards in it, and you don\'t have to specify a library.').

card_ruling('sheltering ancient', '2006-07-15', 'If no opponent controls a creature, you can\'t pay the upkeep and must sacrifice Sheltering Ancient.').
card_ruling('sheltering ancient', '2006-07-15', 'You may choose a different creature to put each counter on, but you can also choose the same creature multiple times. The creatures don\'t have to be controlled by the same opponent.').
card_ruling('sheltering ancient', '2006-07-15', 'Creatures that can\'t be the target of abilities can have +1/+1 counters put on them by Sheltering Ancient\'s cumulative upkeep ability because it doesn\'t target them. The same is true for creatures with protection from green or protection from any other quality Sheltering Ancient has.').
card_ruling('sheltering ancient', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('sheltering word', '2012-05-01', 'If a creature gains hexproof in response to a spell or ability controlled by an opponent that targets only it, that spell or ability will countered for having an illegal target when it tries to resolve.').

card_ruling('sheoldred, whispering one', '2011-06-01', 'The player whose upkeep it is chooses a creature to sacrifice when the ability resolves.').
card_ruling('sheoldred, whispering one', '2011-06-01', 'In a Two-Headed Giant game, the last ability will trigger twice at the beginning of the opposing team\'s upkeep, once for each player on that team. Each player sacrifices only a creature he or she controls.').

card_ruling('shield bearer', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('shield bearer', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('shield bearer', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('shield bearer', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('shield dancer', '2004-10-04', 'If the ability is used twice, it will cause the next two attacks this turn (which only happens if something gives a player two attacks) from the creature to \"reflect\" back to itself. It will not result in double damage back from a single attack.').

card_ruling('shield of the avatar', '2014-07-18', 'To determine how much damage to prevent, use the number of creatures you control when damage would be dealt, including the equipped creature. Notably, if damage would be dealt to multiple creatures at the same time, you’ll count the number of creatures you control before any of that damage is dealt.').
card_ruling('shield of the avatar', '2014-07-18', 'If multiple effects modify how damage is dealt, the player being dealt damage or the controller of the permanent being dealt damage chooses the order to apply the effects. For example, Dictate of the Twin Gods says, “If a source would deal damage to a permanent or player, it deals double that damage to that permanent or player instead.” Suppose the equipped creature would be dealt 3 damage while Dictate of the Twin Gods is on the battlefield and you control two creatures. You can either (a) prevent 2 damage first and then let Dictate of the Twin Gods’s effect apply, for a result of the equipped creature being dealt 2 damage, or (b) double the damage to 6 and then prevent 2 damage, for a result of the creature being dealt 4 damage.').

card_ruling('shield of the oversoul', '2008-05-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').
card_ruling('shield of the oversoul', '2008-05-01', 'If an effect would simultaneously destroy Shield of the Oversoul and a green creature it\'s enchanting, only the Shield is destroyed.').
card_ruling('shield of the oversoul', '2013-07-01', 'If a green creature enchanted by Shield of the Oversoul is dealt lethal damage, the creature isn\'t destroyed, but the damage remains on the creature. If Shield of the Oversoul stops enchanting that creature later in the turn, the creature will lose indestructible and will be destroyed.').

card_ruling('shield of the righteous', '2009-05-01', 'When the equipped creature blocks a creature, Shield of the Righteous\'s second ability doesn\'t tap the blocked creature (in case it happens to be untapped due to vigilance, for example). It just causes that creature to not untap during its controller\'s next untap step.').
card_ruling('shield of the righteous', '2009-05-01', 'If the blocked creature is already untapped at the time its controller\'s next untap step begins, this ability has no effect. It won\'t apply at some later time when that creature is tapped.').
card_ruling('shield of the righteous', '2009-05-01', 'Shield of the Righteous\'s second ability doesn\'t track the blocked creature\'s controller. If that creature changes controllers before its old controller\'s next untap step, this ability will prevent it from untapping during its new controller\'s next untap step.').

card_ruling('shieldhide dragon', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('shieldhide dragon', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('shieldhide dragon', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('shielding plax', '2013-04-15', 'This is not the same as hexproof. If, for example, you target one of your opponent\'s creatures, your opponents won\'t be able to target their own creature with spells or abilities.').

card_ruling('shieldmage elder', '2004-10-04', 'If the ability is activated targeting a spell that becomes a permanent, it also prevents any damage from that permanent this turn.').
card_ruling('shieldmage elder', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('shieldmage elder', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('shields of velis vel', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('shifting borders', '2005-06-01', 'If one of the targeted lands isn\'t a legal target as Shifting Borders resolves (say, because it has left the battlefield or become untargetable), the exchange doesn\'t occur.').
card_ruling('shifting borders', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('shifting borders', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('shifting borders', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('shifting borders', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('shifting loyalties', '2014-11-24', 'If one of the target permanents is an illegal target when Shifting Loyalties resolves, the exchange won’t happen. If both permanents are illegal targets (perhaps because they no longer share a card type), Shifting Loyalties will be countered.').
card_ruling('shifting loyalties', '2014-11-24', 'Either target can have card types the other does not, as long as they share at least one card type. For example, you could target a creature and an artifact creature.').
card_ruling('shifting loyalties', '2014-11-24', 'You don’t have to control either target permanent.').
card_ruling('shifting loyalties', '2014-11-24', 'If the same player controls both permanents when Shifting Loyalties resolves, nothing happens.').
card_ruling('shifting loyalties', '2014-11-24', 'Gaining control of an Aura or Equipment doesn’t cause it to move, though gaining control of an Equipment will allow you to activate its equip ability later to attach it to a creature you control.').
card_ruling('shifting loyalties', '2014-11-24', 'If another spell or ability allows you to change the targets of Shifting Loyalties (or perhaps copy it and choose new targets for the copy), you can change the targets only such that the final set of targets is still legal. For example, if Shifting Loyalties targets a creature you control and a creature an opponent controls, you couldn’t change just the second target to a noncreature permanent controlled by that player. You could, however, change just the second target to a creature controlled by a different opponent. The two new targets can share a different card type than the two original targets did.').

card_ruling('shifting sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('shifting sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('shifty doppelganger', '2004-10-04', 'If the creature is not still on the battlefield at beginning of the End step, then you do not return this card to the battlefield.').
card_ruling('shifty doppelganger', '2008-08-01', 'This card has received errata such that putting a creature card from your hand onto the battlefield is now optional. This means you no longer need a third-party to verify that you don\'t have any creature cards in your hand if you don\'t put something onto the battlefield.').

card_ruling('shimatsu the bloodcloaked', '2004-12-01', 'You sacrifice the permanents before Shimatsu enters the battlefield, so you can\'t sacrifice any creatures that enter the battlefield at the same time it does.').

card_ruling('shimian night stalker', '2004-10-04', 'Can redirect damage from combat and from abilities, but the creature must be attacking before you can use this card\'s ability.').

card_ruling('shimian specter', '2012-07-01', 'The cards you\'re searching for must be found and exiled if they\'re in the graveyard because it\'s a public zone. Finding those cards in the hand and library is optional, because those zones are hidden (even if the hand is temporarily revealed).').
card_ruling('shimian specter', '2012-07-01', 'If the player has no nonland cards in his or her hand, you can still search that player\'s library and have him or her shuffle it.').

card_ruling('shimmer', '2004-10-04', 'Can affect basic or non-basic types, but it must be for a specific type. The chosen type must be an existing land type. See the glossary of the comprehensive rulebook for more details on existing land types.').

card_ruling('shimmer myr', '2011-06-01', 'Because the artifact lands of the original _Mirrodin_(R) block aren\'t cast, Shimmer Myr\'s ability doesn\'t affect them.').

card_ruling('shimmering barrier', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('shimmering efreet', '2004-10-04', 'If it phases in and no other creatures are on the battlefield, it will phase itself back out.').
card_ruling('shimmering efreet', '2004-10-04', 'If two of these phase in, they can both target the same creature and have just that one creature phase out.').

card_ruling('shining shoal', '2005-02-01', 'If the player or creature that was targeted is no longer playing or on the battlefield when the damage would occur, the damage isn\'t redirected.').
card_ruling('shining shoal', '2005-02-01', 'The redirection protects both you and your creatures from the source. If the source would deal more than X damage to multiple objects, you decide which damage is redirected until X is used up. For example, your opponent casts a spell that deals 1 damage to you and 1 damage to two of your creatures. If you cast Shining Shoal with X = 2, you can have damage to you and one creature redirected to the target, or you could have damage to both creatures redirected.').

card_ruling('shipbreaker kraken', '2013-09-15', 'If you lose control of Shipbreaker Kraken between when its last ability triggers and when that ability resolves, the target creatures will become tapped. However, they won’t be affected by the other effect and will untap as normal during their controllers’ untap steps.').
card_ruling('shipbreaker kraken', '2013-09-15', 'If another player gains control of Shipbreaker Kraken, the creatures will no longer be affected by the effect preventing them from untapping, even if you later regain control of Shipbreaker Kraken.').
card_ruling('shipbreaker kraken', '2013-09-15', 'If you gain control of a creature that’s being prevented from untapping, that creature won’t untap during your untap step for as long as you control Shipbreaker Kraken.').
card_ruling('shipbreaker kraken', '2013-09-15', 'The ability can target a tapped creature. If a targeted creature is already tapped when it resolves, that creature just remains tapped and doesn’t untap during its controller’s untap step.').
card_ruling('shipbreaker kraken', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('shipbreaker kraken', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('shipbreaker kraken', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('shipwreck singer', '2013-09-15', 'The controller of each attacking creature still chooses which player or planeswalker that creature attacks.').
card_ruling('shipwreck singer', '2013-09-15', 'If, during a player’s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under that player’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, the player isn’t forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('shipwreck singer', '2013-09-15', 'The last ability affects creatures that are already attacking when it resolves. Activating it before combat won’t affect any creatures that attack later that turn.').

card_ruling('shirei, shizo\'s caretaker', '2005-02-01', 'Shirei\'s ability only triggers if the creature\'s power immediately before leaving the battlefield is 1 or less.').
card_ruling('shirei, shizo\'s caretaker', '2005-02-01', 'If Shirei leaves the battlefield and returns to the battlefield after a creature with power 1 or less is put into your graveyard, it won\'t return that creature to the battlefield under your control.').
card_ruling('shirei, shizo\'s caretaker', '2005-02-01', 'Shirei\'s ability doesn\'t affect creatures put into your graveyard before Shirei enters the battlefield. The \"if\" clause modifies the conditions on which creatures are returned.').

card_ruling('shiv', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('shiv', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('shiv', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('shiv', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('shiv\'s embrace', '2010-08-15', 'Only Shiv\'s Embrace\'s controller (who is not necessarily the enchanted creature\'s controller) can activate its activated ability.').
card_ruling('shiv\'s embrace', '2010-08-15', 'When Shiv\'s Embrace\'s activated ability resolves, the creature Shiv\'s Embrace is enchanting at that time will get +1/+0 (regardless of what creature Shiv\'s Embrace was enchanting when the ability was activated). If Shiv\'s Embrace has left the battlefield by then, the creature it was enchanting at the time it left the battlefield will get +1/+0.').

card_ruling('shivan gorge', '2004-10-04', 'The damage has no color. It is not red.').

card_ruling('shivan meteor', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('shivan meteor', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('shivan meteor', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('shivan meteor', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('shivan meteor', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('shivan meteor', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shivan meteor', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('shivan meteor', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shivan meteor', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('shivan meteor', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('shivan reef', '2015-06-22', 'The damage dealt to you is part of the second mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('shivan reef', '2015-06-22', 'Like most lands, each land in this cycle is colorless. The damage dealt to you is dealt by a colorless source.').

card_ruling('shivan sand-mage', '2007-05-01', 'When Shivan Sand-Mage enters the battlefield, if there are no suspended cards and no permanents with time counters on them, you can\'t choose the second mode. You\'ll have to choose the first mode, and will have to choose a permanent as a target (though, in this case, the ability won\'t do anything when it resolves).').
card_ruling('shivan sand-mage', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('shivan sand-mage', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('shivan sand-mage', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('shivan sand-mage', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('shivan sand-mage', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('shivan sand-mage', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shivan sand-mage', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('shivan sand-mage', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('shivan sand-mage', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('shivan sand-mage', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('shivan wumpus', '2007-02-01', 'When Shivan Wumpus enters the battlefield, first the active player gets the option to sacrifice a land. If he or she declines, the next player in turn order gets the option. If a player elects to sacrifice a land, Shivan Wumpus is put on top of its owner\'s library, but then all remaining players still get the option. If all players decline, then nothing happens and Shivan Wumpus stays on the battlefield.').
card_ruling('shivan wumpus', '2007-02-01', 'This is the timeshifted version of Argothian Wurm.').

card_ruling('shivan wurm', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('shoal serpent', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('shoal serpent', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('shorecrasher elemental', '2015-02-25', 'After Shorecrasher Elemental’s first ability returns it to the battlefield, it will be a new object with no connection to the Shorecrasher Elemental that left the battlefield. It won’t be in combat or have any additional abilities it may have had when it left the battlefield. Any +1/+1 counters on it or Auras attached to it are removed.').
card_ruling('shorecrasher elemental', '2015-02-25', 'You choose whether Shorecrasher Elemental gets +1/-1 or -1/+1 as the last activated ability resolves.').
card_ruling('shorecrasher elemental', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('shorecrasher elemental', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('shorecrasher elemental', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('shorecrasher mimic', '2008-08-01', 'The ability triggers whenever you cast a spell that\'s both of its listed colors. It doesn\'t matter whether that spell also happens to be any other colors.').
card_ruling('shorecrasher mimic', '2008-08-01', 'If you cast a spell that\'s the two appropriate colors for the second time in a turn, the ability triggers again. The Mimic will once again become the power and toughness stated in its ability, which could overwrite power- and toughness-setting effects that have been applied to it in the meantime.').
card_ruling('shorecrasher mimic', '2008-08-01', 'Any other abilities the Mimic may have gained are not affected.').
card_ruling('shorecrasher mimic', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('shoreline ranger', '2009-02-01', 'Unlike the normal cycling ability, Islandcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for an Island card. After you find an Island card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('shoreline ranger', '2009-02-01', 'Islandcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Islandcycling this card. Any ability that stops a cycling ability from being activated also stops Islandcycling from being activated.').
card_ruling('shoreline ranger', '2009-02-01', 'Islandcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Islandcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('shoreline ranger', '2009-02-01', 'You can choose to find any card with the Island land type, including nonbasic lands. You can also choose not to find a card, even if there is an Island card in your library.').

card_ruling('show and tell', '2004-10-04', 'If the cards being put onto the battlefield also require choices, those choices are made after all players choose their card. The active player makes choices for their card (if any), then the other players (if any) in turn order.').
card_ruling('show and tell', '2004-10-04', 'Players choose cards during resolution, not announcement.').
card_ruling('show and tell', '2008-04-01', 'The current player chooses first, then each other player chooses in turn order. A player does not have to reveal the chosen card, so long as it is clear *which* card was chosen. After all choices are made, the cards are put onto the battlefield simultaneously.').

card_ruling('shower of sparks', '2004-10-04', 'If the creature leaves the battlefield before this resolves, it still damages the player.').

card_ruling('showstopper', '2013-04-15', 'Creatures that come under your control after Showstopper resolves won\'t have the ability.').
card_ruling('showstopper', '2013-04-15', 'If another player gains control of a creature you control after Showstopper resolves, and then later that turn that creature dies, the creature\'s controller when it died controls the triggered ability. That player chooses a creature that one of his or her opponents controls as a target.').

card_ruling('shrapnel blast', '2014-07-18', 'You must sacrifice exactly one artifact to cast this spell; you can’t cast it without sacrificing an artifact, and you can’t sacrifice additional artifacts.').
card_ruling('shrapnel blast', '2014-07-18', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the artifact you sacrificed to prevent you from casting this spell.').

card_ruling('shrewd hatchling', '2008-08-01', 'If you cast a spell that\'s both of the listed colors, both abilities will trigger. You\'ll remove a total of two -1/-1 counters from the Hatchling.').
card_ruling('shrewd hatchling', '2008-08-01', 'If there are no -1/-1 counters on it when the triggered ability resolves, the ability does nothing. There is no penalty for not being able to remove a counter.').

card_ruling('shrieking affliction', '2012-10-01', 'Shrieking Affliction\'s ability will trigger only if an opponent begins his or her upkeep with one or fewer cards in hand. The ability will check the number of cards in that player\'s hand again when it tries to resolve. If that player has two or more cards in hand at that time, that player won\'t lose life.').

card_ruling('shrieking drake', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('shriekmaw', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('shrike harpy', '2014-02-01', 'The opponent you target with the triggered ability doesn’t have to be the same opponent you chose to pay tribute or not.').
card_ruling('shrike harpy', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('shrike harpy', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('shrike harpy', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('shrike harpy', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('shrike harpy', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('shrine of limitless power', '2011-06-01', 'Unlike most abilities that force a player to discard cards, this ability may be activated whenever you could cast an instant, including during your opponent\'s draw step after he or she has drawn a card.').
card_ruling('shrine of limitless power', '2011-06-01', 'You can\'t force an opponent to discard a card he or she casts by activating the last ability in response. The spell is on the stack by then.').

card_ruling('shrivel', '2010-06-15', 'Only creatures on the battlefield as Shrivel resolves are affected.').

card_ruling('shrouded lore', '2007-02-01', 'You target an opponent when you cast Shrouded Lore. The opponent chooses a card in your graveyard as Shrouded Lore resolves. After that choice is made, you\'re given the option to pay {B}. If you do, the targeted opponent must choose a different card, if there is one. (If there isn\'t one, this part is skipped.) Then you\'re given the option to pay {B} again. As long as you keep paying {B}, the process continues. As soon as you decline to pay {B}, the last card that was chosen by the opponent is put into your hand.').
card_ruling('shrouded lore', '2007-02-01', 'The spell has no knowledge of cards chosen for other spells named Shrouded Lore.').
card_ruling('shrouded lore', '2007-02-01', 'This is the timeshifted version of Forgotten Lore.').

card_ruling('shu cavalry', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('shu elite companions', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('shu farmer', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('shu general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('shu yun, the silent tempest', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('shu yun, the silent tempest', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('shu yun, the silent tempest', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('shu yun, the silent tempest', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('shuko', '2013-04-15', 'You may activate this card\'s equip ability targeting the creature that it is currently attached to, even if it\'s the only creature you control. Since the equip ability costs zero, you can do this as many times as you like during your main phases, though it generally won\'t do anything.').

card_ruling('shunt', '2004-10-04', 'Once the spell resolves, the new target is considered to be targeted by the deflected spell. This will trigger any effects which trigger on being targeted.').
card_ruling('shunt', '2004-10-04', 'This only targets the spell being changed, not the original or new target of the spell it is affecting.').
card_ruling('shunt', '2004-10-04', 'The target of a spell that targets another spell on the stack can be changed to any other spell on the stack, even if the new target will resolve before the spell does.').
card_ruling('shunt', '2004-10-04', 'You can\'t make a spell which is on the stack target itself.').
card_ruling('shunt', '2004-10-04', 'You can choose to make a spell on the stack target this spell (if such a target choice would be legal had the spell been cast while this spell was on the stack). The new target for the deflected spell is not chosen until this spell resolves. This spell is still on the stack when new targets are selected for the spell.').
card_ruling('shunt', '2004-10-04', 'If there is no other legal target for the spell, this does not change the target.').
card_ruling('shunt', '2004-10-04', 'This does not check if the current target is legal. It just checks if the spell has a single target.').
card_ruling('shunt', '2004-10-04', 'You choose the spell to target on announcement, but you pick the new target for that spell on resolution.').
card_ruling('shunt', '2007-07-15', 'If a spell targets multiple things, you can\'t target it with Shunt, even if all but one of those targets has become illegal.').
card_ruling('shunt', '2007-07-15', 'If a spell targets the same player or object multiple times, you can\'t target it with Shunt.').

card_ruling('shyft', '2008-10-01', 'Shyft\'s ability won\'t let you make it colorless. Colorless is not a color.').
card_ruling('shyft', '2008-10-01', 'The ability\'s effect has no duration. Shyft will remain the chosen color or colors until the game ends, it leaves the battlefield, or some other effect (such as the next time its ability resolves) changes its colors.').

card_ruling('sibilant spirit', '2004-10-04', 'Your opponent does not have to draw a card if they don\'t want to.').

card_ruling('sibsig muckdraggers', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('sibsig muckdraggers', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('sibsig muckdraggers', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('sibsig muckdraggers', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('sibsig muckdraggers', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('sick and tired', '2004-10-04', 'Must target two different creatures.').

card_ruling('sicken', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('sidewinder sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('sidewinder sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('sidisi\'s faithful', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('sidisi\'s faithful', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('sidisi\'s faithful', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('sidisi\'s faithful', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('sidisi\'s faithful', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('sidisi\'s pet', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('sidisi\'s pet', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('sidisi\'s pet', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('sidisi\'s pet', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('sidisi\'s pet', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('sidisi\'s pet', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('sidisi\'s pet', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('sidisi\'s pet', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('sidisi\'s pet', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('sidisi, brood tyrant', '2014-09-20', 'Sidisi’s last ability triggers any time creature cards are put into your graveyard from anywhere in your library, not just from the top.').
card_ruling('sidisi, brood tyrant', '2014-09-20', 'Any instruction, including Sidisi’s first ability, that tells you to put multiple cards from a library into a graveyard moves all the cards at the same time. For example, if Sidisi enters the battlefield and you put three creature cards into your graveyard, Sidisi’s last ability will trigger once and you’ll put one Zombie token onto the battlefield.').

card_ruling('sidisi, undead vizier', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('sidisi, undead vizier', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('sidisi, undead vizier', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('sidisi, undead vizier', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('sidisi, undead vizier', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('siege behemoth', '2014-11-07', 'You decide whether to assign a creature’s combat damage as though it weren’t blocked just before it assigns that damage. You may make a different choice for each creature you control—that is, you may have none, some, or all of those creatures assign combat damage as though they weren’t blocked.').

card_ruling('siege of towers', '2006-02-01', 'The Mountain will remain a creature for the rest of the game. It doesn\'t have a creature type.').
card_ruling('siege of towers', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('siege wurm', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('siege wurm', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('siege wurm', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('siege wurm', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('siege wurm', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('siege wurm', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('siege wurm', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('siege-gang commander', '2009-10-01', 'You can sacrifice any Goblin you control to activate Siege-Gang Commander\'s activated ability, not just the ones its triggered ability puts onto the battlefield. You can even sacrifice Siege-Gang Commander itself.').
card_ruling('siege-gang commander', '2009-10-01', 'If you sacrifice an attacking or blocking Goblin during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that Goblin has been dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('sigarda, host of herons', '2012-05-01', 'As a spell or ability an opponent controls resolves, if it would force you to sacrifice a permanent, you just don\'t. That part of the effect does nothing. If that spell or ability gives you the option to sacrifice a permanent (as Brain Gorger\'s ability does), you can\'t take that option.').
card_ruling('sigarda, host of herons', '2012-05-01', 'If a spell or ability an opponent controls instructs you to perform an action unless you sacrifice a permanent (as Ogre Marauder does), you can\'t choose to sacrifice a permanent. You must perform the action. On the other hand, if a spell or ability an opponent controls instructs you to sacrifice a permanent unless you perform an action (as Killing Wave does), you can choose whether or not to perform the action. If you don\'t perform the action, nothing happens, since you can\'t sacrifice any permanents.').
card_ruling('sigarda, host of herons', '2012-05-01', 'You may still sacrifice permanents to pay the costs of spells you cast and abilities you activate, or because a resolving spell or ability you control instructs or allows you to do so.').
card_ruling('sigarda, host of herons', '2012-05-01', 'You may sacrifice a permanent to pay the activation cost of an ability, even if that ability comes from a permanent an opponent controls (such as Excavation).').
card_ruling('sigarda, host of herons', '2012-05-01', 'You may sacrifice a permanent as a special action, even if the effect that allows you to do so comes from an opponent\'s permanent (such as Damping Engine or Volrath\'s Curse). No one controls special actions.').
card_ruling('sigarda, host of herons', '2012-05-01', 'This ability affects only sacrifices. It won\'t stop a creature from being put into the graveyard due to lethal damage or having 0 toughness, and it won\'t stop a permanent from being put into the graveyard due to the \"legend rule\" or the \"planeswalker uniqueness rule.\" None of these are sacrifices; they\'re the result of game rules.').

card_ruling('sight beyond sight', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('sight beyond sight', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('sight beyond sight', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('sight beyond sight', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('sight beyond sight', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('sight beyond sight', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('sight beyond sight', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('sight beyond sight', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('sighted-caste sorcerer', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('sighted-caste sorcerer', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('sighted-caste sorcerer', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('sighted-caste sorcerer', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('sighted-caste sorcerer', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('sighted-caste sorcerer', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('sightless brawler', '2014-04-26', 'Sightless Brawler or the creature it enchants can be declared as an attacker only if another creature is declared as an attacker at the same time.').
card_ruling('sightless brawler', '2014-04-26', 'If you control more than one creature that can’t attack alone, they can both attack together, even if no other creatures attack.').
card_ruling('sightless brawler', '2014-04-26', 'Although Sightless Brawler or the creature it enchants can’t attack alone, other attacking creatures don’t have to attack the same player or planeswalker. For example, Sightless Brawler could attack an opponent and another creature could attack a planeswalker that opponent controls.').
card_ruling('sightless brawler', '2014-04-26', 'If a creature that can’t attack alone also must attack if able, its controller must attack with it and another creature if able.').
card_ruling('sightless brawler', '2014-04-26', 'In a Two-Headed Giant game (or in another format using the shared team turns option), Sightless Brawler (or the creature it enchants) can attack with a creature controlled by your teammate, even if no other creatures you control attack.').
card_ruling('sightless brawler', '2014-04-26', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('sightless brawler', '2014-04-26', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('sightless brawler', '2014-04-26', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('sightless brawler', '2014-04-26', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('sightless brawler', '2014-04-26', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('sightless brawler', '2014-04-26', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('sightless brawler', '2014-04-26', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('sightless brawler', '2014-04-26', 'You still control the Aura, even if it’s enchanting a creature controlled by another player.').
card_ruling('sightless brawler', '2014-04-26', 'If the enchanted creature leaves the battlefield, the Aura stops being an Aura and remains on the battlefield. Control of that permanent doesn’t change; you’ll control the resulting enchantment creature.').
card_ruling('sightless brawler', '2014-04-26', 'Similarly, if you cast an Aura spell with bestow targeting a creature controlled by another player, and that creature is an illegal target when the spell tries to resolve, it will finish resolving as an enchantment creature spell. It will enter the battlefield under your control.').

card_ruling('sigil blessing', '2008-10-01', 'If the targeted creature becomes an illegal target by the time Sigil Blessing resolves, the entire spell will be countered. Your other creatures won\'t get +1/+1.').

card_ruling('sigil captain', '2009-05-01', 'Sigil Captain\'s ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless the creature that enters the battlefield under your control is 1/1, and (2) the ability will do nothing unless that creature is still 1/1 at the time the ability resolves.').
card_ruling('sigil captain', '2009-05-01', 'For that reason, two Sigil Captains don\'t work well together. When a 1/1 creature enters the battlefield under your control, the abilities of both Sigil Captains will trigger. When the first resolves, two +1/+1 counters are put on the 1/1 creature. When the second resolves, it will do nothing because the creature that entered the battlefield is now 3/3.').
card_ruling('sigil captain', '2009-05-01', 'Sigil Captain\'s ability checks a creature\'s initial power and toughness upon being put on the battlefield, so it will take into account counters that it enters the battlefield with and static abilities that may give it a continuous power boost once it\'s on the battlefield (such as the one on Glorious Anthem). After the creature is already on the battlefield, changing its power and toughness to 1/1 with a spell, activated ability, or triggered ability won\'t allow this ability to trigger; it\'s too late by then.').
card_ruling('sigil captain', '2009-05-01', 'If, for some reason, Sigil Captain enters the battlefield as a 1/1 creature (due to a pair of Engineered Plagues, for example), it will cause its own ability to trigger.').

card_ruling('sigil of distinction', '2008-10-01', 'Removing a charge counter from Sigil of Distinction is the only way to pay for its equip ability. If it has no charge counters on it, its equip ability can\'t be activated.').
card_ruling('sigil of distinction', '2008-10-01', 'If Sigil of Distinction is attached to a creature when its equip ability is activated targeting a different creature, the first creature will immediately get a little smaller because a charge counter has been removed from Sigil of Distinction as a cost. Then, when the equip ability resolves, Sigil of Distinction will move to the new creature and the first creature will lose the entire bonus the Equipment was giving it.').

card_ruling('sigil of the empty throne', '2009-02-01', 'The triggered ability will resolve before the enchantment spell that caused it to trigger.').
card_ruling('sigil of the empty throne', '2009-02-01', 'When you cast an Aura spell, you have to choose a target for it before this ability puts an Angel token onto the battlefield. In other words, you can\'t play an Aura spell intending to enchant the Angel that will be created as a result.').
card_ruling('sigil of the empty throne', '2009-02-01', 'Casting Sigil of the Empty Throne won\'t trigger its own ability. It has to be on the battlefield for its ability to work.').

card_ruling('sigil of the nayan gods', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('sigil of the nayan gods', '2009-05-01', 'Sigil of the Nayan Gods\'s ability counts the number of creatures you control, regardless of who controls the creature the Aura is enchanting. If you control the creature it\'s enchanting, the bonus will include that creature too.').

card_ruling('sigil of valor', '2015-06-22', 'Count the number of creatures you control other than the equipped creature as Sigil of Valor’s ability resolves to determine the amount of the bonus. Once the ability resolves, the bonus won’t change, even if the number of creatures you control does.').
card_ruling('sigil of valor', '2015-06-22', 'A creature attacks alone if it’s the only creature declared as an attacker during the declare attackers step (including creatures controlled by your teammates, if applicable). For example, Sigil of Valor’s ability won’t trigger if you attack with multiple creatures and all but one of them are removed from combat.').

card_ruling('sigil tracer', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap creatures (including Sigil Tracer itself) that haven\'t been under your control since your most recent turn began to pay the cost.').
card_ruling('sigil tracer', '2008-04-01', 'If the spell is modal (that is, it says \"choose one --\" or \"choose two --\"), the choice of mode(s) can\'t be changed.').
card_ruling('sigil tracer', '2008-04-01', 'Sigil Tracer\'s ability can copy an instant or sorcery spell that isn\'t targeted.').
card_ruling('sigil tracer', '2008-04-01', 'If the controller of the original spell chose to pay an alternative cost (such as prowl) or an additional cost, that choice is also copied.').

card_ruling('sigiled paladin', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('sigiled paladin', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('sigiled paladin', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('sigiled paladin', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('sigiled paladin', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('sigiled paladin', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('signal the clans', '2013-01-24', 'If you don\'t reveal three creature cards with different names, you\'ll simply shuffle your library (including any cards you revealed).').
card_ruling('signal the clans', '2013-01-24', 'All players will see the card you put into your hand, if any.').

card_ruling('silence', '2009-10-01', 'Silence won\'t affect spells that your opponents cast before you cast Silence. (In other words, it can\'t be used as a retroactive Cancel.) Silence also won\'t stop your opponents from casting spells after you cast Silence but before Silence resolves.').
card_ruling('silence', '2009-10-01', 'The only thing Silence does is prevent your opponents from casting spells. They can still activate abilities, including abilities of cards in their hands (like cycling). Their triggered abilities work as normal, they can still play lands, and so on.').

card_ruling('silence the believers', '2014-04-26', 'The creatures and Auras are exiled at the same time. Specifically, if an Aura is a permanent with bestow, it will be exiled. It won’t stop being an Aura and remain on the battlefield.').
card_ruling('silence the believers', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('silence the believers', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('silence the believers', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('silence the believers', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('silence the believers', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('silent assassin', '2013-04-15', 'If you activate the ability during the end of combat step, the creature will be destroyed at the beginning of the next end of combat step, even if that step occurs during a different turn.').

card_ruling('silent sentinel', '2014-02-01', 'If you return an enchantment creature card with bestow to the battlefield, you can’t pay its bestow cost. It won’t be an Aura and can’t be attached to a creature.').
card_ruling('silent sentinel', '2014-02-01', 'If the enchantment card is an Aura, you choose a legal player or object for that Aura to enchant as it enters the battlefield. This doesn’t target the player or object, so it’s possible to enchant an opposing creature with hexproof this way, for example. If there’s no player or object the Aura can legally enchant, it stays in the graveyard.').

card_ruling('silent skimmer', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('silent skimmer', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('silent skimmer', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('silent skimmer', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('silent skimmer', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('silent-blade oni', '2012-06-01', 'If you cast a permanent spell this way, it will enter the battlefield under your control when it resolves. If you cast an instant or sorcery spell this way, that card will be put into its owner\'s graveyard when it resolves.').
card_ruling('silent-blade oni', '2012-06-01', 'The nonland card you cast via the triggered ability is cast as part of the resolution of that ability. Timing restrictions based on the card\'s type are ignored. Other restrictions, such as \"Cast [this card] only during an opponent\'s turn,\" are not.').
card_ruling('silent-blade oni', '2012-06-01', 'If you cast a card without paying its mana cost, you can\'t pay any alternative costs, such as evoke or the alternative cost provided by the morph ability. If it has {X} in its mana cost, X must be 0. However, you can pay optional additional costs, such as kicker, and you must still pay mandatory additional costs, such as the one on Fling.').

card_ruling('silhana ledgewalker', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) don\'t actually have Flying, so they can\'t block this.').

card_ruling('silk net', '2004-10-04', 'Both the +1/+1 and reach only last until end of turn.').
card_ruling('silk net', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('silkbind faerie', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('silkbind faerie', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('silkbind faerie', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('silkwrap', '2015-02-25', 'Face-down creatures have a converted mana cost of 0. If one is exiled by Silkwrap, it will return to the battlefield face up.').
card_ruling('silkwrap', '2015-02-25', 'Silkwrap’s ability causes a zone change with a duration, a style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Silkwrap have a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Silkwrap leaves the battlefield.').
card_ruling('silkwrap', '2015-02-25', 'If Silkwrap leaves the battlefield before its triggered ability resolves, the target creature won’t be exiled.').
card_ruling('silkwrap', '2015-02-25', 'Auras attached to the exiled creature will be put into their owners’ graveyards. Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('silkwrap', '2015-02-25', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('silkwrap', '2015-02-25', 'The exiled card returns to the battlefield immediately after Silkwrap leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('silkwrap', '2015-02-25', 'In a multiplayer game, if Silkwrap’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('silumgar assassin', '2015-02-25', 'The powers of Silumgar Assassin and the creature trying to block it are checked only as blockers are declared. Changing the powers of either of those creatures after Silumgar Assassin is legally blocked won’t change or undo the block.').
card_ruling('silumgar assassin', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('silumgar assassin', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('silumgar assassin', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('silumgar butcher', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('silumgar butcher', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('silumgar butcher', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('silumgar butcher', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('silumgar butcher', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('silumgar monument', '2015-02-25', 'A Monument can’t attack on the turn it enters the battlefield.').
card_ruling('silumgar monument', '2015-02-25', 'Each Monument is colorless, although the last ability will make each of them two colors until end of turn.').
card_ruling('silumgar monument', '2015-02-25', 'If a Monument has any +1/+1 counters on it, those counters will remain on the permanent after it stops being a creature. Those counters will have no effect as long as the Monument isn’t a creature, but they will apply again if the Monument later becomes a creature.').
card_ruling('silumgar monument', '2015-02-25', 'Activating the last ability of a Monument while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

card_ruling('silumgar sorcerer', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('silumgar sorcerer', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('silumgar sorcerer', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('silumgar sorcerer', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('silumgar sorcerer', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('silumgar spell-eater', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('silumgar spell-eater', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('silumgar spell-eater', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('silumgar\'s command', '2015-02-25', 'You choose the two modes as you cast the spell. You must choose two different modes. Once modes are chosen, they can’t be changed.').
card_ruling('silumgar\'s command', '2015-02-25', 'You can choose a mode only if you can choose legal targets for that mode. Ignore the targeting requirements for modes that aren’t chosen. For example, you can cast Ojutai’s Command without targeting a creature spell provided you don’t choose the third mode.').
card_ruling('silumgar\'s command', '2015-02-25', 'As the spell resolves, follow the instructions of the modes you chose in the order they are printed on the card. For example, if you chose the second and fourth modes of Ojutai’s Command, you would gain 4 life and then draw a card. (The order won’t matter in most cases.)').
card_ruling('silumgar\'s command', '2015-02-25', 'If a Command is copied, the effect that creates the copy will usually allow you to choose new targets for the copy, but you can’t choose new modes.').
card_ruling('silumgar\'s command', '2015-02-25', 'If all targets for the chosen modes become illegal before the Command resolves, the spell will be countered and none of its effects will happen. If at least one target is still legal, the spell will resolve but will have no effect on any illegal targets.').

card_ruling('silumgar\'s scorn', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('silumgar\'s scorn', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('silumgar\'s scorn', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('silumgar, the drifting death', '2014-11-24', 'The creatures affected by Silumgar’s triggered ability are determined relative to the Dragon that attacked. For example, if Silumgar attacked one opponent and two other Dragons attacked another opponent, creatures controlled by the first opponent would get -1/-1 until end of turn and creatures controlled by the second opponent would get -2/-2 until end of turn. In a Two-Headed Giant game, each creature controlled by either member of the defending team will get -1/-1.').

card_ruling('silver drake', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('silver wyvern', '2004-10-04', 'You must pick a legal target when redirecting the spell or ability.').

card_ruling('silvergill douser', '2007-10-01', 'A permanent that\'s both a Merfolk and a Faerie is counted only once when determining X.').
card_ruling('silvergill douser', '2007-10-01', 'The value of X is determined when Silvergill Douser\'s ability resolves. It won\'t change later, even if the number of Merfolk and/or Faeries you control changes.').

card_ruling('silverglade elemental', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('silverglade pathfinder', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('silverskin armor', '2011-06-01', 'The equipped creature will remain whatever color(s) it was previously. Becoming an artifact doesn\'t make the creature colorless.').

card_ruling('simian grunts', '2004-10-04', 'This card is always a creature spell and is never an instant spell, no matter when you cast it.').

card_ruling('simian spirit guide', '2007-02-01', 'This is the timeshifted version of Elvish Spirit Guide.').
card_ruling('simian spirit guide', '2007-02-01', 'This ability is a mana ability.').

card_ruling('simic charm', '2013-01-24', 'If you choose the second mode, only permanents you control when Simic Charm resolves will gain hexproof. Permanents that come under your control later that turn will not.').

card_ruling('simic fluxmage', '2013-01-24', 'To move a counter from one creature to another, the counter is removed from the first creature and placed on the second. Any abilities that care about a counter being placed on the second creature will apply.').
card_ruling('simic fluxmage', '2013-01-24', 'If the creature is an illegal target when Simic Fluxmage\'s ability tries to resolve, it will be countered and none of its effects will happen. No counters will be removed from Simic Fluxmage.').
card_ruling('simic fluxmage', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('simic fluxmage', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('simic fluxmage', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('simic fluxmage', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('simic fluxmage', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('simic fluxmage', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('simic growth chamber', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('simic guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('simic guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('simic guildmage', '2006-05-01', 'For the first ability, the first target creature doesn\'t need to have a +1/+1 counter on it. If it doesn\'t, the ability does nothing.').
card_ruling('simic guildmage', '2006-05-01', 'For the first ability, if the two target creatures aren\'t controlled by the same player when the ability resolves, the ability does nothing. The player who controls the two creatures doesn\'t have to be the same player who controlled them when the ability was activated, and that player doesn\'t have to be Simic Guildmage\'s controller.').
card_ruling('simic guildmage', '2006-05-01', 'For the second ability, only the Aura is targeted. When the ability resolves, you choose a permanent to move the Aura onto. It can\'t be the permanent the Aura is already attached to, it must be controlled by the player who controls the permanent the Aura is attached to, and it must be able to be enchanted by the Aura. (It doesn\'t matter who controls the Aura or who controls Simic Guildmage.) If no such permanent exists, the Aura doesn\'t move.').

card_ruling('simic keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('simic keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('simic manipulator', '2013-01-24', 'The power of the target creature is checked both as you target it and as the ability resolves. If the power of the target creature when the ability resolves is greater than the number of +1/+1 counters removed from Simic Manipulator, the ability will be countered and none of its effects will happen. You won\'t gain control of any creature, but the counters removed as a cost remain removed.').
card_ruling('simic manipulator', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('simic manipulator', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('simic manipulator', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('simic manipulator', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('simic manipulator', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('simic manipulator', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('simulacrum', '2004-10-04', 'You can\'t use Simulacrum on Loss of Life, just damage.').
card_ruling('simulacrum', '2004-10-04', 'This is not damage prevention or redirection. It actually is life gain and newly dealt damage.').
card_ruling('simulacrum', '2008-08-01', 'Simulacrum is the source of the damage. If an effect needs to know a characteristic of the damage\'s source (Protection from Black, for instance), it will see the damage coming from Simulacrum.').

card_ruling('sin collector', '2013-04-15', 'No player can do anything between you choosing a card and that card being exiled. Notably, your opponent can\'t cast the card once Sin Collector\'s ability has started to resolve.').

card_ruling('sindbad', '2006-09-25', 'If the draw is replaced by another effect, none of the rest of Sindbad\'s ability applies, even if the draw is replaced by another draw (such as with Enduring Renewal).').
card_ruling('sindbad', '2006-09-25', 'To avoid confusion, reveal the card you draw before putting it with the rest of the cards in your hand.').

card_ruling('sinew sliver', '2004-10-04', 'Yes, it does give the +1/+1 bonus to itself.').
card_ruling('sinew sliver', '2007-02-01', 'This is the timeshifted version of Muscle Sliver.').
card_ruling('sinew sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('sinew sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('singing bell strike', '2014-09-20', 'Singing Bell Strike can be cast targeting any creature, including one that’s already tapped.').
card_ruling('singing bell strike', '2014-09-20', 'The activated ability that untaps the creature is granted to the enchanted creature. Only the creature’s controller can activate this ability.').

card_ruling('singing tree', '2005-11-01', 'Changes creature\'s current power to zero (as opposed to giving it -X/-0, like before) but does not prevent raising it after the Tree has been used on it.').
card_ruling('singing tree', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('sinking feeling', '2008-05-01', 'Sinking Feeling grants the untap ability to the creature it\'s enchanting. Only the creature\'s controller can activate that ability, not Sinking Feeling\'s controller.').

card_ruling('sins of the past', '2005-10-01', 'The instant or sorcery card is cast from your graveyard, not your hand. You choose modes, pay additional costs, choose targets, etc. for the spell as normal when casting it. Any X in the mana cost will be 0. Alternative costs can\'t be paid.').
card_ruling('sins of the past', '2005-10-01', 'The instant or sorcery card never gets put back into your graveyard, even if it\'s countered. You can\'t cast it more than once.').

card_ruling('sip of hemlock', '2013-09-15', 'If the creature is an illegal target when Sip of Hemlock tries to resolve, it will be countered and none of its effects will occur. The creature’s controller won’t lose 2 life. However, if Sip of Hemlock resolves and the creature isn’t destroyed (perhaps because it regenerated or had indestructible), the creature’s controller will still lose 2 life.').

card_ruling('sire of stagnation', '2015-08-25', 'The cards are exiled from the library face up.').
card_ruling('sire of stagnation', '2015-08-25', 'If that player has one card in his or her library when Sire of Stagnation’s triggered ability resolves, that card will be exiled. If his or her library has zero cards, no cards are exiled. In both cases, you’ll still draw two cards. That player won’t lose the game (until he or she attempts to draw a card from an empty library).').
card_ruling('sire of stagnation', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('sire of stagnation', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('sire of stagnation', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('sire of stagnation', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('sire of stagnation', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('siren of the fanged coast', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('siren of the fanged coast', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('siren of the fanged coast', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('siren of the fanged coast', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('siren of the fanged coast', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('siren of the silent song', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('siren of the silent song', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('siren of the silent song', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('siren of the silent song', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('siren\'s call', '2004-10-04', 'It will require creatures with Haste to attack since they are able, but it won\'t destroy them if they don\'t for some reason.').
card_ruling('siren\'s call', '2004-10-04', 'The creature is destroyed if it does not attack because it simply can\'t do so legally.').
card_ruling('siren\'s call', '2009-02-01', 'This will destroy creatures that weren\'t able to attack because they had been previously tapped.').
card_ruling('siren\'s call', '2013-09-20', 'If a turn has multiple combat phases, this spell can only be cast before the beginning of the Declare Attackers Step of the first combat phase in that turn.').

card_ruling('sirocco', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('sisters of stone death', '2005-10-01', 'The second ability can only exile a creature that\'s currently blocking or being blocked by Sisters of Stone Death. If Sisters of Stone Death isn\'t on the battlefield or have been removed from combat, the ability will be countered.').
card_ruling('sisters of stone death', '2005-10-01', 'The third ability can get back any creature card exiled by the second ability, no matter when it was exiled. If a card that\'s not a creature card (such as Svogthos, the Restless Tomb) was exiled, the third ability can\'t return it.').

card_ruling('skaab ruinator', '2011-09-22', 'Skaab Ruinator is on the stack when you pay its costs. It can\'t be exiled to pay for itself.').
card_ruling('skaab ruinator', '2011-09-22', 'You must exile three creature cards from your graveyard no matter what zone you\'re casting Skaab Ruinator from.').

card_ruling('skarrg guildmage', '2013-01-24', 'Only creatures you control when the first ability resolves will gain trample. Creatures that come under your control later in the turn won\'t have trample. Lands you control that aren\'t creatures also won\'t gain trample, even if you use the second ability to turn them into creatures later in the turn.').
card_ruling('skarrg guildmage', '2013-01-24', 'Skarrg Guildmage\'s second ability doesn\'t affect the land\'s name or any other types, subtypes, or supertypes (such as basic or legendary) the land may have. The land will also keep any abilities it had.').

card_ruling('skarrgan firebird', '2006-02-01', 'You can activate Skarrgan Firebird\'s last ability if an opponent was dealt damage by any source, even if you didn\'t control that source.').

card_ruling('skarrgan pit-skulk', '2006-02-01', 'Skarrgan Pit-Skulk compares each blocker\'s power against its own as blockers are being declared.').

card_ruling('skeletal kathari', '2008-10-01', 'You may sacrifice Skeletal Kathari to pay for its own regeneration ability. If you do, however, it won\'t regenerate. It\'ll just end up in its owner\'s graveyard as a result of the sacrifice.').

card_ruling('skeletal vampire', '2006-02-01', 'You may pay the cost of Skeletal Vampire\'s activated abilities by sacrificing any Bat, not just a Bat token it created.').

card_ruling('skeleton shard', '2004-10-04', 'You can pay either of the two costs (but not both at the same time) to activate the ability.').

card_ruling('skeletonize', '2008-10-01', 'Each time a creature is put into a graveyard from the battlefield, check whether Skeletonize had dealt damage to it during that turn. If so, Skeletonize\'s delayed triggered ability will trigger.').
card_ruling('skeletonize', '2008-10-01', 'You get the token, regardless of who controlled the creature or whose graveyard it was put into.').

card_ruling('skill borrower', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('skill borrower', '2008-10-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities; they have colons in their reminder text.').
card_ruling('skill borrower', '2008-10-01', 'If an effect tells you to draw several cards, reveal each one before you draw it. Skill Borrower will momentarily gain the activated abilities of artifact and creature cards revealed this way, but you won\'t be able to activate those abilities while the effect is still happening.').
card_ruling('skill borrower', '2008-10-01', 'If the top card of your library changes while you\'re activating one of Skill Borrower\'s activated abilities gained from that card, the ability will still be activated and will resolve normally even though Skill Borrower has lost that ability.').
card_ruling('skill borrower', '2008-10-01', 'Skill Borrower may gain activated abilities that it can\'t use. For example, if the top card of your library is an artifact or creature card with cycling, Skill Borrower will have cycling. However, since cycling can\'t be activated from the battlefield, this won\'t have any significant benefit. The same is true for unearth.').

card_ruling('skinrender', '2011-01-01', 'This ability is mandatory. If there are no other creatures on the battlefield, you must target Skinrender itself.').

card_ruling('skinshifter', '2011-09-22', 'You choose the ability\'s mode when you activate it.').
card_ruling('skinshifter', '2011-09-22', 'No matter which mode you choose, Skinshifter will cease to have any creature types it previously had. If you activate its ability choosing the first mode, it will become only a Rhino; it\'s no longer a Human or a Shaman.').
card_ruling('skinshifter', '2011-09-22', 'No matter which mode you choose, Skinshifter will retain any other card types it may have. For example, if an effect causes Skinshifter to be an artifact in addition to its other types and you activate its ability choosing the first mode, it will be both an artifact and a Rhino creature.').
card_ruling('skinshifter', '2011-09-22', 'Skinshifter\'s ability doesn\'t cause it to lose any other abilities it may have.').
card_ruling('skinshifter', '2011-09-22', 'Skinshifter\'s ability doesn\'t affect its copiable values. Creatures that enter the battlefield as a copy of Skinshifter will enter as 1/1 Human Shaman creatures.').
card_ruling('skinshifter', '2011-09-22', 'You can activate Skinshifter\'s ability during any player\'s turn, but only once on each turn.').

card_ruling('skinthinner', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('skirk alarmist', '2004-10-04', 'If this ability is used during the end step, the creature is not sacrificed until the end of the next turn.').

card_ruling('skirk fire marshal', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('skirk fire marshal', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('skirk marauder', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('skirk shaman', '2007-02-01', 'This is the timeshifted version of Severed Legion.').

card_ruling('skirsdag flayer', '2011-01-22', 'You can sacrifice any Human you control, including Skirsdag Flayer itself.').
card_ruling('skirsdag flayer', '2011-01-22', 'Because targets are chosen before costs are paid, you can activate Skirsdag Flayer\'s ability targeting itself and then sacrifice Skirsdag Flayer to pay the cost.').

card_ruling('skirsdag high priest', '2011-09-22', 'Unlike Skirsdag High Priest itself, the two other creatures you tap to activate the ability aren\'t required to have been under your control continuously since the beginning of your most recent turn.').

card_ruling('skithiryx, the blight dragon', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('skithiryx, the blight dragon', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('skithiryx, the blight dragon', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('skithiryx, the blight dragon', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('skithiryx, the blight dragon', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('skithiryx, the blight dragon', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('skittering horror', '2004-10-04', 'Triggers when you announce a creature spell, and will resolve before that creature is put onto the battlefield.').

card_ruling('skittering skirge', '2004-10-04', 'The triggered ability triggers when the creature spell is cast, and it is put on the stack before any responses can be cast or activated. This ability will resolve before that creature enters the battlefield.').
card_ruling('skittering skirge', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('skitterskin', '2015-08-25', 'Once you legally activate the ability, it doesn’t matter what happens to the other colorless creature. The ability will resolve and create a regeneration shield even if you don’t control another colorless creature at that time.').
card_ruling('skitterskin', '2015-08-25', 'Skitterskin’s activated ability doesn’t depend on Skitterskin being colorless. If it gains one or more colors, you can still activate the ability if you control another creature that’s colorless.').
card_ruling('skitterskin', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('skitterskin', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('skitterskin', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('skitterskin', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('skitterskin', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('skulking ghost', '2004-10-04', 'The sacrifice is triggered on the announcement of a spell that targets it and is placed on the stack before responses can be announced.').
card_ruling('skulking ghost', '2005-08-01', 'You can move an Aura onto this card using an untargeted spell or ability without causing this card to be sacrificed.').

card_ruling('skull of orm', '2004-10-04', 'Can bring back any enchantment.').

card_ruling('skull of ramos', '2004-10-04', 'You can activate the sacrifice ability while this card is tapped.').

card_ruling('skull rend', '2012-10-01', 'Skull Rend doesn\'t require any targets.').
card_ruling('skull rend', '2012-10-01', 'Each of your opponents will discard cards, even if some or all of the damage is prevented or redirected.').

card_ruling('skullbriar, the walking grave', '2011-09-22', 'Skullbriar will get one +1/+1 counter each time it deals combat damage to a player (including you if its combat damage gets redirected), regardless of how much damage is dealt.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'Skullbriar retains all counters, not just +1/+1 counters.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'The counters that remain on Skullbriar as it changes zones aren\'t \"placed\" on Skullbriar. Effects like Doubling Season\'s and Melira, Sylvok Outcast\'s won\'t affect those counters.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'Counters that adjust power and/or toughness affect Skullbriar\'s power and/or toughness in zones other than the battlefield. For example, a Skullbriar in the command zone with a +1/+1 counter on it will be 2/2.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'Effects that last \"for as long as that creature has a [kind of] counter on it,\" such as Aven Mimeomancer\'s, stop applying to Skullbriar once it leaves the battlefield. Even though Skullbriar retains the counters, it becomes a new object with no relation to its last existence in its previous zone.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'Skullbriar\'s last ability only works if it has that ability in the zone it\'s moving from. For example, with Yixlid Jailer (\"Cards in graveyards lose all abilities\") on the battlefield, a Skullbriar with a counter on it in a graveyard loses that counter when it\'s put onto the battlefield. Conversely, that Skullbriar moving from the graveyard to the battlefield would retain that counter if Humility (\"All creatures lose all abilities and are 1/1\") were on the battlefield; if Skullbriar then left the battlefield with Humility still on the battlefield, it would lose the counter.').
card_ruling('skullbriar, the walking grave', '2011-09-22', 'If a card becomes a copy of Skullbriar, counters will remain on that card when it leaves the battlefield (unless it goes to your hand or library). Once it does so, it stops being a copy of Skullbriar, so those counters will cease to exist when that card next changes zones.').

card_ruling('skullcage', '2004-12-01', 'Skullcage\'s ability deals damage if the player has zero, one, two, five, or more than five cards in hand when its ability resolves.').
card_ruling('skullcage', '2004-12-01', 'Skullcage doesn\'t check hand size when it triggers, only when it resolves.').

card_ruling('skullcrack', '2013-01-24', 'Skullcrack targets only the player. If that player is an illegal target when Skullcrack tries to resolve, it will be countered and none of its effects will happen.').
card_ruling('skullcrack', '2013-01-24', 'Spells and abilities that would cause a player to gain life or that would prevent damage still resolve, but the life-gain and damage-prevention parts have no effect.').
card_ruling('skullcrack', '2013-01-24', 'Effects that would replace gaining life with another effect won\'t apply because it\'s impossible for players to gain life.').
card_ruling('skullcrack', '2013-01-24', 'If an effect says to set a player\'s life total to a certain number and that number is higher than the player\'s current life total, that part of the effect won\'t do anything. (If the number is lower than the player\'s current life total, the effect will work as normal.)').

card_ruling('skullmulcher', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('skullmulcher', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('skullmulcher', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('skullmulcher', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').

card_ruling('skullsnatcher', '2005-02-01', 'Skullsnatcher\'s ability can target zero, one, or two cards in that player\'s graveyard.').

card_ruling('skulltap', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('skulltap', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('sky swallower', '2006-02-01', 'Auras your opponent gains control of won\'t move from where they are.').
card_ruling('sky swallower', '2006-02-01', 'The permanents don\'t return to your control when Sky Swallower leaves the battlefield.').

card_ruling('skybind', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('skybind', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('skybind', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('skyblinder staff', '2013-01-24', 'The equipped creature can still be blocked by a creature with reach.').
card_ruling('skyblinder staff', '2013-01-24', 'If the equipped creature is blocked by a creature and then that creature gains flying, the equipped creature will remain blocked.').

card_ruling('skybreen', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('skybreen', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('skybreen', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('skybreen', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('skybreen', '2009-10-01', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').
card_ruling('skybreen', '2009-10-01', 'If the top card of your library changes during the process of casting a spell or activating an ability, the new top card won\'t be revealed until the process of casting the spell or activating the ability ends (all targets are chosen, all costs are paid, and so on).').
card_ruling('skybreen', '2009-10-01', 'Skybreen\'s second ability prevents each player from casting spells that share a card type with a card on top of any library, not just the one on top of that player\'s own library. This includes permanent spells (artifacts, creatures, enchantments, and planeswalkers), not just instant and sorcery spells. It doesn\'t stop players from playing lands or activating abilities (such as cycling or unearth).').

card_ruling('skyclaw thrash', '2009-05-01', 'If you lose the flip, nothing happens. Skyclaw Thrash continues to attack as normal.').

card_ruling('skycloud egg', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('skyfire kirin', '2005-06-01', 'If the Spirit or Arcane spell has {X} in the mana cost, then you use the value of {X} on the stack. For example, Shining Shoal costs {X}{W}{W}. If you choose X = 2, then Shining Shoal\'s converted mana cost is 4. Shining Shoal also has an ability that says \"You may exile a white card with converted mana cost X from your hand rather than pay Shining Shoal\'s mana cost\"; if you choose to pay the alternative cost and exile a card with converted mana cost 2, then X is 2 while Shining Shoal is on the stack and its converted mana cost is 4.').

card_ruling('skyline cascade', '2015-08-25', 'Skyline Cascade’s triggered ability doesn’t tap the creature. It can target any creature, tapped or untapped. If that creature is already untapped at the beginning of its controller’s next untap step, the effect won’t do anything.').
card_ruling('skyline cascade', '2015-08-25', 'The triggered ability tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').

card_ruling('skyreaping', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('skyreaping', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('skyreaping', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('skyreaping', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('skyrider elf', '2015-08-25', 'You can choose any value for X. The value chosen for X doesn’t directly affect the number of +1/+1 counters Skyrider Elf enters the battlefield with, but it does let you pay more mana and thus spend more colors of mana to cast it. For example, you can choose 0 for the value of X and pay {G}{U} to cast Skyrider Elf for two +1/+1 counters. You can also choose 3 for the value of X, making its mana cost {3}{G}{U}, and pay {W}{U}{B}{R}{G} for five +1/+1 counters.').
card_ruling('skyrider elf', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('skyrider elf', '2015-08-25', '').
card_ruling('skyrider elf', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('skyrider elf', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('skyrider elf', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('skyship weatherlight', '2004-10-04', 'If this card leaves the battlefield, the remaining cards that were exiled don\'t come back.').
card_ruling('skyship weatherlight', '2004-10-04', 'The card goes to its owner\'s hand as per the errata.').
card_ruling('skyship weatherlight', '2004-10-04', 'You may choose to not find anything when searching your library.').
card_ruling('skyship weatherlight', '2004-10-04', 'The cards are exiled face up.').

card_ruling('skyshroud blessing', '2004-10-04', 'This affects your lands and your opponent\'s.').

card_ruling('skyshroud falcon', '2004-10-04', 'No it is not a Falcon creature type. It is a Bird.').

card_ruling('skyshroud poacher', '2004-10-04', 'You do not have to find an Elf card if you do not want to.').

card_ruling('skyshroud ranger', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('skyshroud war beast', '2004-10-04', 'The power and toughness are continuously recalculated.').

card_ruling('skywatcher adept', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('skywatcher adept', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('skywatcher adept', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('skywatcher adept', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('skywatcher adept', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('skywise teachings', '2015-02-25', 'The ability will trigger only once for each noncreature spell you cast. You can pay {1}{U} only once and create one Djinn Monk.').

card_ruling('slag fiend', '2011-06-01', 'Slag Fiend\'s power- and toughness-setting ability works in all zones, not just on the battlefield.').

card_ruling('slash panther', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('slash panther', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('slash panther', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('slash panther', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('slash panther', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('slashing tiger', '2009-10-01', 'When Slashing Tiger becomes blocked, the ability will trigger just once no matter how many creatures are blocking it.').

card_ruling('slaughter', '2004-10-04', 'You can\'t pay the buyback if you have less than 4 life, even if an effect would keep you from losing for having 0 or less life.').

card_ruling('slaughter cry', '2009-10-01', 'If a creature doesn\'t have first strike, granting it first strike after combat damage has been dealt in the first combat damage step won\'t prevent it from dealing combat damage. It will still assign and deal its combat damage in the second combat damage step.').

card_ruling('slaughter games', '2012-10-01', 'You can leave any cards with that name in the zone they are in. You don\'t have to exile them.').

card_ruling('slaughter pact', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('slave of bolas', '2009-05-01', 'You may target a creature you already control.').
card_ruling('slave of bolas', '2009-05-01', 'If you don\'t control the targeted creature when the \"at end of turn\" ability resolves, it won\'t be sacrificed. If it\'s still on the battlefield but under another player\'s control, the ability won\'t try to have you sacrifice it again at the end of the turn after that.').

card_ruling('slay', '2004-10-04', 'You don\'t draw a card if this spell is countered.').

card_ruling('slayer of the wicked', '2011-09-22', 'If you control the only Vampire, Werewolf, or Zombie, you must target it with Slayer of the Wicked\'s ability. You choose whether or not to destroy the target when the ability resolves.').

card_ruling('sleep', '2009-10-01', 'The second part of Sleep\'s effect affects all creatures the targeted player controls as Sleep resolves, not just the ones that Sleep actually caused to become tapped.').
card_ruling('sleep', '2009-10-01', 'Sleep tracks both the player and the creatures. If one of the creatures controlled by the targeted player as Sleep resolves changes controllers, the creature will untap as normal during its new controller\'s next untap step.').

card_ruling('sleeper agent', '2004-10-04', 'This card is a bit weird. When it enters the battlefield under your control, you give control of it to an opponent. After that it damages them each turn because the \"you\" on the card means its controller.').

card_ruling('sleight of hand', '2004-10-04', 'If there is only one card in your library, it goes to your hand.').

card_ruling('sleight of mind', '2004-10-04', 'You can\'t Sleight proper nouns (i.e. card names). This means that you can\'t affect a \"Black Vise\".').
card_ruling('sleight of mind', '2004-10-04', 'Can target a card with no color words on it.').
card_ruling('sleight of mind', '2004-10-04', 'Can\'t change a color word to the same color word. It must be a different word.').
card_ruling('sleight of mind', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('sleight of mind', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('sleight of mind', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('sleight of mind', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('sleight of mind', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('sleight of mind', '2004-10-04', 'Changing the text of a spell will not allow you to change the targets of the spell because the targets were chosen when the spell was cast. The text change will (probably) cause it to be countered since the targets will be illegal.').
card_ruling('sleight of mind', '2004-10-04', 'If you change the text of a spell which is to become a permanent, the permanent will retain the text change until the effect wears off.').

card_ruling('slice and dice', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('slice and dice', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('slice and dice', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('slingbow trap', '2010-03-01', 'If you cast Slingbow Trap for {G}, you may target any attacking creature with flying, not just a black one.').
card_ruling('slingbow trap', '2010-03-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').

card_ruling('slippery karst', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('slipstream eel', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('slitherhead', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('slithering shade', '2006-05-01', 'If you declare Slithering Shade as an attacker, then a card is put into your hand, the Shade won\'t be removed from combat. It continues to attack.').

card_ruling('slithermuse', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('slithermuse', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('slithermuse', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('slithermuse', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('slithermuse', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('slithermuse', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').
card_ruling('slithermuse', '2008-04-01', 'You choose an opponent when the ability resolves. Once you determine how many more cards than you that player has, that number is locked in as the amount you\'ll draw.').

card_ruling('sliver construct', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('sliver construct', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('sliver hive', '2014-07-18', 'Whether you control a Sliver is checked only when you activate the last ability, not as that ability resolves. If you control no Slivers at the time the ability resolves, you’ll still get a Sliver token.').

card_ruling('sliver hivelord', '2014-07-18', 'Damage dealt to a Sliver with indestructible is still marked on that creature. If it has lethal damage marked on it and it loses indestructible (perhaps because Sliver Hivelord leaves the battlefield), it will be destroyed.').
card_ruling('sliver hivelord', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('sliver hivelord', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('sliver hivelord', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('sliver legion', '2007-05-01', 'If Sliver Legion and two other Slivers are on the battlefield, for example, each of them gets +2/+2.').
card_ruling('sliver legion', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('sliver legion', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('slobad, goblin tinkerer', '2004-12-01', 'Damage wears off simultaneously with this effect ending, so it does save an artifact creature from lethal damage.').

card_ruling('slow motion', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice the creature. You can choose to not pay if you no longer control the creature on resolution.').

card_ruling('sludge crawler', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('sludge crawler', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('sludge crawler', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('sludge crawler', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('sludge crawler', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('sludge crawler', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('sludge crawler', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('sludge strider', '2009-02-01', 'If Sludge Strider and another artifact enter the battlefield under your control at the same time, Sludge Strider\'s triggered ability will trigger. Similarly, if Sludge Strider and another artifact you control leave the battlefield at the same time, Sludge Strider\'s triggered ability will trigger.').
card_ruling('sludge strider', '2009-02-01', 'If an artifact creature with 0 toughness enters the battlefield under your control, Sludge Strider\'s ability will trigger a total of twice: Once when that creature enters the battlefield, and once when it\'s put into a graveyard. Both abilities will be put on the stack after the state-based action check that puts the 0-toughness creature into the graveyard is finished.').

card_ruling('sluiceway scorpion', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('slum reaper', '2012-10-01', 'When the ability resolves, you may sacrifice Slum Reaper itself. If you control no other creatures, you\'ll have to sacrifice Slum Reaper.').

card_ruling('slumbering dragon', '2012-07-01', 'Slumbering Dragon\'s second ability counts all +1/+1 counters on it, not just the ones added by its third ability.').
card_ruling('slumbering dragon', '2012-07-01', 'The +1/+1 counter is put on Slumbering Dragon before blockers are declared. This may allow Slumbering Dragon to \"wake up\" and block the creature that attacked and caused its ability to trigger.').
card_ruling('slumbering dragon', '2012-07-01', 'Slumbering Dragon\'s third ability will trigger once for each creature that attacks you or a planeswalker you control.').

card_ruling('slumbering tora', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('smallpox', '2006-09-25', 'Each player loses 1 life. Then, starting with the active player and proceeding in turn order, each player chooses a card to discard (normally without seeing what other players have chosen) and all discards happen simultaneously. Then, starting with the active player and proceeding in turn order, each player chooses a creature to sacrifice (this time players can see what other players have chosen) and all sacrifices happen simultaneously. Then the process that happened for creatures is repeated for lands.').

card_ruling('smash to smithereens', '2015-06-22', 'Smash to Smithereens targets only the artifact, not any player. If that artifact becomes an illegal target by the time Smash to Smithereens tries to resolve, Smash to Smithereens will be countered and none of its effects will happen. No damage will be dealt.').

card_ruling('smite', '2010-06-15', 'A \"blocked creature\" is an attacking creature that has been blocked by a creature this combat, or has become blocked as the result of a spell or ability this combat. Unless the attacking creature leaves combat, it continues to be a blocked creature through the end of combat step, even if the creature or creatures that blocked it are no longer on the battlefield or have otherwise left combat by then.').

card_ruling('smogsteed rider', '2006-02-01', 'The creatures gain fear before blockers can be declared.').
card_ruling('smogsteed rider', '2006-02-01', 'Smogsteed Rider doesn\'t give itself fear, but if two are attacking, each will give the other one fear.').

card_ruling('smoke', '2004-10-04', 'Animated lands are affected by this spell. If on the battlefield with an effect that limits the number of land you untap, untapping an animated land will count as the one creature and the one land you can untap... thereby limiting you to one thing to be untapped.').

card_ruling('smoke teller', '2014-09-20', 'Smoke Teller’s ability lets you look at the creature as the ability resolves. It doesn’t let you look at it later, although you should be given a reasonable amount of time to look at it. In a multiplayer game, be sure not to reveal the card to other players.').
card_ruling('smoke teller', '2014-09-20', 'You could target a creature you control with Smoke Teller’s ability, but there’s no benefit to doing so. Remember, you may look at face-down spells you control on the stack and face-down permanents you control at any time.').

card_ruling('smokebraider', '2007-10-01', 'The mana can be two mana of the same color, or one mana of each of two different colors. The mana can\'t be colorless.').
card_ruling('smokebraider', '2007-10-01', 'You can use this mana to pay an alternative cost (such as evoke) or additional cost incurred while casting an Elemental spell. It\'s not limited to just that spell\'s mana cost.').
card_ruling('smokebraider', '2007-10-01', 'The mana can\'t be spent to activate activated abilities of Elemental sources that aren\'t on the battlefield.').

card_ruling('smokestack', '2004-10-04', 'If a player does not have enough permanents, they sacrifice all the ones they have.').
card_ruling('smokestack', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('smoldering crater', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('smoldering marsh', '2015-08-25', 'Even though these lands have basic land types, they are not basic lands because “basic” doesn’t appear on their type line. Notably, controlling two or more of them won’t allow others to enter the battlefield untapped.').
card_ruling('smoldering marsh', '2015-08-25', 'However, because these cards have basic land types, effects that specify a basic land type without also specifying that the land be basic can affect them. For example, a spell or ability that reads “Destroy target Forest” can target Canopy Vista, while one that reads “Destroy target basic Forest” cannot.').
card_ruling('smoldering marsh', '2015-08-25', 'If one of these lands enters the battlefield at the same time as any number of basic lands, those other lands are not counted when determining if this land enters the battlefield tapped or untapped.').

card_ruling('smoldering tar', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('smother', '2010-03-01', 'A creature\'s converted mana cost is determined solely by the mana symbols printed in its upper right corner (unless that creature is copying something else; see below). If its mana cost includes {X}, X is considered to be 0. If it has no mana symbols in its upper right corner (because it\'s an animated land, for example), its converted mana cost is 0. Ignore any alternative costs or additional costs (such as kicker) paid when the creature was cast.').
card_ruling('smother', '2010-03-01', 'A token has a converted mana cost of 0, unless it is copying something else.').
card_ruling('smother', '2010-03-01', 'If a creature is copying something else, its converted mana cost is the converted mana cost of whatever it\'s copying.').

card_ruling('smothering abomination', '2015-08-25', 'You choose which creature you control to sacrifice as the first triggered ability resolves. Players can respond to this ability triggering, but once you choose a creature to sacrifice, it’s too late for anyone to respond.').
card_ruling('smothering abomination', '2015-08-25', 'If Smothering Abomination is the only creature you control as its first triggered ability resolves, you must sacrifice it.').
card_ruling('smothering abomination', '2015-08-25', 'Sacrificing Smothering Abomination will cause its last ability to trigger.').
card_ruling('smothering abomination', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('smothering abomination', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('smothering abomination', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('smothering abomination', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('smothering abomination', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('snag', '2008-08-01', 'You may discard any card with the land type Forest, including Snow-covered Forest or non-Basic lands like Tropical Island, Murmuring Bosk, or Stomping Ground. You can even discard a card that is a land in addition to other types, such as Dryad Arbor.').

card_ruling('snake basket', '2006-02-01', 'It used to make Cobra tokens. Now it makes Snake tokens.').

card_ruling('snake of the golden grove', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('snake of the golden grove', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('snake of the golden grove', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('snake of the golden grove', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('snake of the golden grove', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('snake umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('snake umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('snake umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('snake umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('snake umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('snake umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('snake umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('snake umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('snake umbra', '2010-06-15', 'Snake Umbra grants the triggered ability to the creature. It triggers whenever the enchanted creature deals damage to an opponent of its controller (who is not necessarily an opponent of the Aura\'s controller). In other words, if your Snake Umbra winds up enchanting your opponent\'s creature, that opponent will draw a card whenever that creature damages you.').
card_ruling('snake umbra', '2010-06-15', 'The ability triggers when the enchanted creature deals any damage, not just combat damage.').
card_ruling('snake umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('snake umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('snake umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('snakeform', '2008-08-01', 'Snakeform doesn\'t counter abilities that have already triggered or been activated. In particular, there is no way to cast this to stop a creature\'s \"At the beginning of your upkeep\" or \"When this creature enters the battlefield\" abilities from triggering.').
card_ruling('snakeform', '2008-08-01', 'If the affected creature gains an ability after Snakeform resolves, it will keep that ability.').
card_ruling('snakeform', '2008-08-01', 'If Snakeform affects a creature with changeling, the creature will lose its changeling ability, and will no longer be all creature types.').
card_ruling('snakeform', '2009-10-01', 'The power/toughness-setting effect overwrites other effects that set power and toughness only if those effects existed before this spell resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after this spell resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('snap', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('snap', '2004-10-04', 'This does not target the lands.').
card_ruling('snap', '2004-10-04', 'You can untap 0, 1, or 2 lands.').

card_ruling('snapcaster mage', '2011-09-22', 'If the targeted instant or sorcery card already has flashback, you may use either flashback ability to cast it from your graveyard.').
card_ruling('snapcaster mage', '2012-10-01', 'You can\'t pay other alternative costs of a card you\'re casting from your graveyard using flashback. For example, if the instant or sorcery has an overload cost, you can\'t pay that cost.').
card_ruling('snapcaster mage', '2013-07-01', 'For split cards, you pay only the cost for the half of the card you are casting. This is true because the cost is not looked at until after the card is on the stack, at which time it only has one of the two costs.').

card_ruling('snapping creeper', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('snapping creeper', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('snapping gnarlid', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('snapping gnarlid', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('snapping gnarlid', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('snapsail glider', '2011-01-01', 'For the purposes of combat, whether Snapsail Glider has flying is relevant only as the declare blockers step begins. If an attacking Snapsail Glider without flying becomes blocked, gaining control of enough artifacts to cause it to have flying won\'t change that. Similarly, if a Snapsail Glider with flying blocks another creature with flying, losing control of enough artifacts to cause the Glider to lose flying won\'t change that.').

card_ruling('sneak attack', '2004-10-04', 'You only sacrifice the creature if you still control it at end of turn. If that creature has left the battlefield and come back, you don\'t sacrifice it.').
card_ruling('sneak attack', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('snow-covered forest', '2008-10-01', '\"Snow\" has no particular meaning or rules associated with it.').
card_ruling('snow-covered forest', '2008-10-01', 'Since this is a basic land, you may have any number of it in a Constructed deck in any format in which the _Masters Edition II_ set, the _Coldsnap_ set, or the _Ice Age_ set is legal.').
card_ruling('snow-covered forest', '2008-10-01', 'In Limited events, you can\'t add basic snow lands to your deck from outside your card pool. You may add only lands named Plains, Island, Swamp, Mountain, and Forest, as normal.').
card_ruling('snow-covered forest', '2008-10-01', 'Effects that target nonbasic lands can\'t target basic snow lands.').

card_ruling('snow-covered island', '2008-10-01', '\"Snow\" has no particular meaning or rules associated with it.').
card_ruling('snow-covered island', '2008-10-01', 'Since this is a basic land, you may have any number of it in a Constructed deck in any format in which the _Masters Edition II_ set, the _Coldsnap_ set, or the _Ice Age_ set is legal.').
card_ruling('snow-covered island', '2008-10-01', 'In Limited events, you can\'t add basic snow lands to your deck from outside your card pool. You may add only lands named Plains, Island, Swamp, Mountain, and Forest, as normal.').
card_ruling('snow-covered island', '2008-10-01', 'Effects that target nonbasic lands can\'t target basic snow lands.').

card_ruling('snow-covered mountain', '2008-10-01', '\"Snow\" has no particular meaning or rules associated with it.').
card_ruling('snow-covered mountain', '2008-10-01', 'Since this is a basic land, you may have any number of it in a Constructed deck in any format in which the _Masters Edition II_ set, the _Coldsnap_ set, or the _Ice Age_ set is legal.').
card_ruling('snow-covered mountain', '2008-10-01', 'In Limited events, you can\'t add basic snow lands to your deck from outside your card pool. You may add only lands named Plains, Island, Swamp, Mountain, and Forest, as normal.').
card_ruling('snow-covered mountain', '2008-10-01', 'Effects that target nonbasic lands can\'t target basic snow lands.').

card_ruling('snow-covered plains', '2008-10-01', '\"Snow\" has no particular meaning or rules associated with it.').
card_ruling('snow-covered plains', '2008-10-01', 'Since this is a basic land, you may have any number of it in a Constructed deck in any format in which the _Masters Edition II_ set, the _Coldsnap_ set, or the _Ice Age_ set is legal.').
card_ruling('snow-covered plains', '2008-10-01', 'In Limited events, you can\'t add basic snow lands to your deck from outside your card pool. You may add only lands named Plains, Island, Swamp, Mountain, and Forest, as normal.').
card_ruling('snow-covered plains', '2008-10-01', 'Effects that target nonbasic lands can\'t target basic snow lands.').

card_ruling('snow-covered swamp', '2008-10-01', '\"Snow\" has no particular meaning or rules associated with it.').
card_ruling('snow-covered swamp', '2008-10-01', 'Since this is a basic land, you may have any number of it in a Constructed deck in any format in which the _Masters Edition II_ set, the _Coldsnap_ set, or the _Ice Age_ set is legal.').
card_ruling('snow-covered swamp', '2008-10-01', 'In Limited events, you can\'t add basic snow lands to your deck from outside your card pool. You may add only lands named Plains, Island, Swamp, Mountain, and Forest, as normal.').
card_ruling('snow-covered swamp', '2008-10-01', 'Effects that target nonbasic lands can\'t target basic snow lands.').

card_ruling('snowfall', '2008-08-01', 'This mana is in addition to any mana the Island itself produces, and is blue regardless of what type of mana the Island produces.').

card_ruling('snowhorn rider', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('snowhorn rider', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('snowhorn rider', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('snowhorn rider', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('snowhorn rider', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('snowhorn rider', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('snowhorn rider', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('snowhorn rider', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('snowhorn rider', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('soar', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('soilshaper', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('sokenzan', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('sokenzan', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('sokenzan', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('sokenzan', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('sokenzan', '2009-10-01', 'If you roll {C} twice in the same main phase, two new combat phases will be created. However, all creatures that attacked this turn untap as the {C} abilities resolve, not as the combat phases start. Any creature that attacks in the second combat phase will remain tapped during the third combat phase (unless you roll {C} again).').

card_ruling('sokenzan renegade', '2005-06-01', 'If multiple players are tied for the most cards in hand, either when Sokenzan Renegade\'s ability triggers or as it resolves, the ability does nothing.').

card_ruling('sol\'kanar the swamp king', '2004-10-04', 'Will not trigger off itself being cast.').

card_ruling('solar blast', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('solar blast', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('solar blast', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('solar blast', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('solarion', '2004-12-01', '\"Double\" has its normal English meaning. If Solarion had three +1/+1 counters on it, it would end up with six +1/+1 counters on it.').

card_ruling('soldevi excavations', '2004-10-04', 'This card produces two different types of mana. If an effect that adds one mana to your mana pool of the same type as the mana produced by this card, you choose which of the two types it produces.').

card_ruling('soldevi golem', '2004-10-04', 'The ability to untap a creature can be used even if the Golem is already untapped.').

card_ruling('soldevi machinist', '2008-08-01', 'Mana from the ability can only be spent on activation costs for abilities from artifacts. It can\'t be spent on artifact spells, or on costs resulting from triggered abilities.').

card_ruling('soldevi simulacrum', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('soldier of the pantheon', '2013-09-15', 'Soldier of the Pantheon can’t be enchanted or equipped by multicolored Auras and Equipment, it can’t be blocked by multicolored creatures, it can’t be targeted by multicolored spells or abilities from multicolored sources, and all damage dealt to it by multicolored sources is prevented.').
card_ruling('soldier of the pantheon', '2013-09-15', 'Most hybrid spells and permanents, including the ones in the Return to Ravnica block, are multicolored, even if you cast them with one color of mana.').

card_ruling('solemn offering', '2009-10-01', 'If the targeted permanent is an illegal target by the time Solemn Offering would resolve, the entire spell is countered. You won\'t gain any life.').

card_ruling('solfatara', '2004-10-04', 'Prevents all land playing that turn, not just one land.').
card_ruling('solfatara', '2004-10-04', 'Does not prevent effects from putting a land onto the battlefield.').

card_ruling('solidarity of heroes', '2014-04-26', 'Solidarity of Heroes can target any creatures, not just ones with +1/+1 counters on them. Notably, heroic abilities of any target creatures that put +1/+1 counters on that creature will resolve before Solidarity of Heroes.').
card_ruling('solidarity of heroes', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('solidarity of heroes', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('solidarity of heroes', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('solidarity of heroes', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('solidarity of heroes', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('soltari guerrillas', '2008-08-01', 'Regardless of when the ability is activated, it will apply to the defending player at the time Soltari Guerrillas tries to deal combat damage to him or her.').

card_ruling('soltari lancer', '2008-04-01', 'For a time, the ability that granted it first strike was a triggered ability. It has been restored to a static ability that applies continuously while the Lancer is attacking. There is never a time when the Lancer is attacking but doesn\'t have first strike.').

card_ruling('soltari visionary', '2004-10-04', 'If the damaged player controls any enchantments, you must choose one to be destroyed. If they do not, nothing happens.').

card_ruling('somberwald alpha', '2015-06-22', 'Somberwald Alpha’s first ability will give each creature you control that becomes blocked +1/+1 until end of turn. It doesn’t matter how many of your opponent’s creatures are blocking each of your creatures.').

card_ruling('somberwald sage', '2012-05-01', 'Mana produced by Somberwald Sage can be spent on any part of a creature spell\'s total cost. This includes additional costs (such as kicker) and alternative costs (such as evoke costs).').
card_ruling('somberwald sage', '2012-05-01', 'Mana produced by Somberwald Sage can\'t be spent on activated abilities, even ones that put a creature card directly onto the battlefield, such as unearth or ninjutsu.').
card_ruling('somberwald sage', '2012-05-01', 'Mana produced by Somberwald Sage can\'t be spent on noncreature spells that would put creature tokens onto the battlefield.').

card_ruling('somberwald vigilante', '2012-05-01', 'Somberwald Vigilante\'s ability will trigger for each creature it becomes blocked by.').
card_ruling('somberwald vigilante', '2012-05-01', 'The ability resolves before combat damage is dealt, so it could cause blocking creatures to be destroyed and thus not deal combat damage.').

card_ruling('somnophore', '2004-10-04', 'It can end up holding more than one creature tapped.').
card_ruling('somnophore', '2004-10-04', 'You can choose the same creature more than once (for no additional effect).').

card_ruling('song of blood', '2004-10-04', 'If a creature attacks more than once in a turn, it gets the bonus each time.').

card_ruling('song of the dryads', '2014-11-07', 'The enchanted permanent loses any card types, subtypes, and colors it previously had. It keeps any supertypes it had and its name remains unchanged. It gains “{T}: Add {G} to your mana pool” and loses all other abilities from its rules text. It will still have any abilities it gained from other effects.').
card_ruling('song of the dryads', '2014-11-07', 'If the permanent was an Aura or an Equipment, it becomes unattached from whatever it was attached to.').

card_ruling('songs of the damned', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('sonic burst', '2004-10-04', 'You can\'t discard more than once to target more than one creature (or player) or to do multiple amounts of damage to a single creature (or player).').
card_ruling('sonic burst', '2004-10-04', 'You pick the target before you know which card is going to be discarded.').

card_ruling('soot imp', '2008-08-01', 'This ability affects all players, including you.').
card_ruling('soot imp', '2008-08-01', 'A hybrid spell that\'s black and another color is *not* a nonblack spell. Since it\'s black, it can\'t be nonblack!').

card_ruling('soraya the falconer', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('soraya the falconer', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('soraya the falconer', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('soraya the falconer', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('sorcerer\'s strongbox', '2010-08-15', 'If you lose the flip, nothing happens.').
card_ruling('sorcerer\'s strongbox', '2010-08-15', 'If you no longer control Sorcerer\'s Strongbox by the time you win the flip, you won\'t be able to sacrifice it. You\'ll still draw three cards, though.').

card_ruling('sorin markov', '2009-10-01', 'If the targeted creature or player is an illegal target by the time Sorin\'s first ability resolves, the entire ability is countered. You won\'t gain life.').
card_ruling('sorin markov', '2009-10-01', 'For a player\'s life total to become 10, what actually happens is that the player gains or loses the appropriate amount of life. For example, if the targeted opponent\'s life total is 4 when this ability resolves, it will cause that player to gain 6 life; alternately, if the targeted player\'s life total is 17 when this ability resolves, it will cause that player to lose 7 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('sorin markov', '2009-10-01', 'Sorin\'s third ability allows you to control another player. This effect applies to the next turn that the affected player actually takes.').
card_ruling('sorin markov', '2009-10-01', 'The player who is being controlled is still the active player.').
card_ruling('sorin markov', '2009-10-01', 'While controlling another player, you also continue to make your own choices and decisions.').
card_ruling('sorin markov', '2009-10-01', 'While controlling another player, you make all choices and decisions that player is allowed to make or is told to make during that turn. For example: -- You choose which lands the other player plays. -- You choose which spells the other player casts, and make all decisions as those spells are cast and as they resolve. For example, you choose the value of X for that player\'s Earthquake, the target for that player\'s Lightning Bolt, what mana that player spends to cast Day of Judgment, and what card that player gets with Diabolic Tutor. -- You choose which activated abilities the other player activates, and make all decisions as those abilities are activated and as they resolve. For example, you can have your opponent sacrifice his or her creatures to his or her Vampire Aristocrat or have your opponent\'s Caller of Gales give one of your creatures flying. -- You make all decisions for the other player\'s triggered abilities, including what they target and any decisions made when they resolve. -- You choose which creatures controlled by the other player attack, who or what they attack, and how they assign their combat damage. -- You make any choices and decisions that player would make for any other reason. For example, you could cast Fact or Fiction, choose that player to divide the revealed cards into piles, and thus divide those cards into piles yourself.').
card_ruling('sorin markov', '2009-10-01', 'You can\'t make the affected player concede. That player may choose to concede at any time, even while you\'re controlling his or her turn.').
card_ruling('sorin markov', '2009-10-01', 'You can\'t make any illegal decisions or illegal choices -- you can\'t do anything that player couldn\'t do. You can\'t make choices or decisions for that player that aren\'t called for by the game rules or by any cards, permanents, spells, abilities, and so on. If an effect causes another player to make decisions that the affected player would normally make (such as Master Warcraft does), that effect takes precedence. (In other words, if the affected player wouldn\'t make a decision, you wouldn\'t make that decision on his or her behalf.) You also can\'t make any choices or decisions for the player that would be called for by the tournament rules (such as whether to take an intentional draw or whether to call a judge).').
card_ruling('sorin markov', '2009-10-01', 'You can use only the affected player\'s resources (cards, mana, and so on) to pay costs for that player; you can\'t use your own. Similarly, you can use the affected player\'s resources only to pay that player\'s costs; you can\'t spend them on your costs.').
card_ruling('sorin markov', '2009-10-01', 'You only control the player. You don\'t control any of the other player\'s permanents, spells, or abilities.').
card_ruling('sorin markov', '2009-10-01', 'If the player affected by Sorin\'s third ability skips his or her next turn, the ability will wait. You\'ll control the next turn the affected player actually takes.').
card_ruling('sorin markov', '2009-10-01', 'Multiple player-controlling effects that affect the same player overwrite each other. The last one to be created is the one that works.').
card_ruling('sorin markov', '2009-10-01', 'You could gain control of yourself using Sorin\'s third ability, but unless you do so to overwrite someone else\'s player-controlling effect, this doesn\'t do anything.').
card_ruling('sorin markov', '2010-06-15', 'In a Two-Headed Giant game, Sorin\'s second ability causes the targeted opponent\'s team\'s life-total to become 10. Only the targeted player is actually considered to have actually gained or lost life.').
card_ruling('sorin markov', '2012-07-01', 'While controlling another player, you can see all cards that player can see. This includes cards in that player\'s hand, face-down cards that player controls, his or her sideboard, and any cards in his or her library that he or she looks at.').
card_ruling('sorin markov', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('sorin markov', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('sorin markov', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('sorin markov', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('sorin markov', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('sorin markov', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('sorin markov', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('sorin markov', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('sorin markov', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('sorin markov', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('sorin markov', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('sorin\'s thirst', '2011-09-22', 'If the creature is an illegal target when Sorin\'s Thirst tries to resolve, Sorin\'s Thirst will be countered and none of its effects will happen. You won\'t gain life.').

card_ruling('sorin\'s vengeance', '2011-09-22', 'If the player is an illegal target when Sorin\'s Vengeance tries to resolve, Sorin\'s Vengeance will be countered and none of its effects will happen. You won\'t gain life.').

card_ruling('sorin, lord of innistrad', '2011-01-22', 'The emblems are cumulative. If you get two of them, creatures you control will get +2/+0.').
card_ruling('sorin, lord of innistrad', '2011-01-22', 'You can target a token creature with Sorin, Lord of Innistrad\'s third ability, but it won\'t return to the battlefield.').
card_ruling('sorin, lord of innistrad', '2011-01-22', 'If a creature with undying is destroyed by Sorin, Lord of Innistrad\'s third ability, the undying ability will trigger, but the creature will already have been returned to the battlefield by Sorin. The undying ability won\'t do anything when it resolves.').
card_ruling('sorin, lord of innistrad', '2011-01-22', 'If Sorin, Lord of Innistrad somehow becomes a creature, it can be chosen as a target of its own third ability. If Sorin, Lord of Innistrad is still on the battlefield when that ability resolves, it will then destroy Sorin and return him to the battlefield under your control.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('sorin, lord of innistrad', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('sorin, solemn visitor', '2014-09-20', 'Only creatures you control as the first ability resolves will get the bonuses. Creatures you come to control after that happens will not.').
card_ruling('sorin, solemn visitor', '2014-09-20', 'When the emblem’s ability resolves, if the player controls no creatures, the player won’t sacrifice anything.').

card_ruling('sorrow\'s path', '2009-02-01', 'This has two abilities. The second ability triggers any time it becomes tapped, whether to pay for its ability or not.').
card_ruling('sorrow\'s path', '2009-10-01', 'The first ability can target any two blocking creatures a single opponent controls. Whether those creatures could block all creatures the other is blocking isn\'t determined until the ability resolves.').
card_ruling('sorrow\'s path', '2009-10-01', 'A \"blocking creature\" is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures that it was blocking are no longer on the battlefield or have otherwise left combat by then.').
card_ruling('sorrow\'s path', '2009-10-01', 'When Sorrow\'s Path\'s first ability is activated, its second ability triggers and goes on the stack on top of the first ability. The second ability resolves first, and may cause some of the attacking creatures to be dealt lethal damage.').
card_ruling('sorrow\'s path', '2009-10-01', 'When determining whether a creature could block all creatures the other is blocking, take into account evasion abilities (like flying), protection abilities, and other blocking restrictions, as well as abilities that allow a creature to block multiple creatures or block as though a certain condition were true. Take into account whether those creatures are tapped, but not whether they have costs to block (since those apply only as blockers are declared).').
card_ruling('sorrow\'s path', '2009-10-01', 'When the first ability resolves, if all the creatures that one of the targeted creatures was blocking have left combat, then the other targeted creature is considered to be able to block all creatures the first creature is blocking. If the ability has its full effect, the second creature will be removed from combat but not returned to combat; it doesn\'t block anything.').
card_ruling('sorrow\'s path', '2009-10-01', 'Abilities that trigger whenever one of the targeted creatures blocks will trigger when the first ability resolves, because those creatures will change from not blocking (since they\'re removed from combat) to blocking. It doesn\'t matter if those abilities triggered when those creatures blocked the first time. Abilities that trigger whenever one of the attacking creatures becomes blocked will not trigger again, because they never stopped being blocked creatures. Abilities that trigger whenever a creature blocks one of the attacking creatures will trigger again, though; those kinds of abilities trigger once for each creature that blocks.').

card_ruling('soul barrier', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('soul burn', '2004-10-04', 'Will give 1 life for each black mana used, but not more life than the amount of unprevented damage that is dealt.').

card_ruling('soul collector', '2004-10-04', 'The ability only triggers if this card is face up at the time the creature is put into the graveyard.').

card_ruling('soul conduit', '2011-06-01', 'If one of the targeted players is an illegal target when the activated ability tries to resolve, the exchange won\'t happen. Neither player\'s life total will change.').
card_ruling('soul conduit', '2011-06-01', 'When the life totals are exchanged, each player gains or loses the amount of life necessary to equal the other player\'s previous life total. For example, if Player A has 5 life and Player B has 3 life before the exchange, Player A will lose 2 life and Player B will gain 2 life. Replacement effects may modify these gains and losses, and triggered abilities may trigger on them.').
card_ruling('soul conduit', '2011-06-01', 'If a player can\'t gain life, that player can\'t exchange life totals with a player with a higher life total. If a player can\'t lose life, that player can\'t exchange life totals with a player with a lower life total. In either of these cases, neither player\'s life total will change.').
card_ruling('soul conduit', '2011-06-01', 'In a Two-Headed Giant game, each player is considered to have the same life total as his or her team. If the two targeted players are on different teams, the teams will end up exchanging life totals. If the two targeted players are teammates, nothing will happen.').

card_ruling('soul echo', '2004-10-04', 'If you have more than one Soul Echo, the opponent chooses which ones will be replacing damage that turn (if any). And when damage happens, you decide which one of the ones the opponent selected that you want the damage to be replaced with, but the damage all goes to one of them.').
card_ruling('soul echo', '2004-10-04', 'If this card runs out of counters before your next upkeep, the effect still replaces the damage and uselessly tries to remove counters from the now empty card.').
card_ruling('soul echo', '2004-10-04', 'In a multi-player game, you choose a new opponent during each of your upkeeps.').
card_ruling('soul echo', '2004-10-04', 'If you take damage between the start of the upkeep step and the time in which the upkeep trigger resolves, then Soul Echo will not affect that damage and you will be affected by the damage as normal.').

card_ruling('soul exchange', '2004-10-04', 'Putting a counter on the creature put onto the battlefield is not a targeted effect and so Protection from Black will not prevent it.').
card_ruling('soul exchange', '2004-10-04', 'You exile exactly one creature. You can\'t exile more than one in an attempt to get a larger effect.').
card_ruling('soul exchange', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('soul foundry', '2004-10-04', 'You do not get a chance to pay any optional costs, such as Kicker, on the imprinted card.').
card_ruling('soul foundry', '2004-12-01', 'Soul Foundry puts a token copy of the imprinted card onto the battlefield. The token is put onto the battlefield, not cast.').
card_ruling('soul foundry', '2004-12-01', 'The token is an exact copy in every way, except that it\'s a token, not a card.').
card_ruling('soul foundry', '2004-12-01', 'Most creature tokens have no mana cost and a converted mana cost of 0, but a creature token put onto the battlefield by Soul Foundry has the same mana cost and converted mana cost as the card it copies.').

card_ruling('soul link', '2004-10-04', 'If the creature damages itself, both abilities trigger.').

card_ruling('soul manipulation', '2009-05-01', 'You may choose just the first mode (targeting a creature spell), just the second mode (targeting a creature card in your graveyard), or both modes (targeting a creature spell and a creature card in your graveyard). You can\'t choose a mode unless there\'s a legal target for it.').
card_ruling('soul manipulation', '2009-05-01', 'If you choose both modes, you choose their targets at the same time. You can\'t counter your own creature spell and then return that card from your graveyard to your hand.').

card_ruling('soul net', '2004-10-04', 'If animated so it is a creature, it can be triggered off its own destruction.').

card_ruling('soul of innistrad', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of innistrad', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').

card_ruling('soul of new phyrexia', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of new phyrexia', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').

card_ruling('soul of ravnica', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of ravnica', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').
card_ruling('soul of ravnica', '2014-07-18', 'To determine how many cards to draw, count the number of colors among permanents you control as either activated ability resolves. You’ll draw between zero and five cards.').

card_ruling('soul of shandalar', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of shandalar', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').
card_ruling('soul of shandalar', '2014-07-18', 'To activate either ability, you must target a player, but you can choose to not target any creatures.').

card_ruling('soul of the harvest', '2012-05-01', 'A creature card that enters the battlefield as a copy of a token creature is still a nontoken creature. You\'ll be able to draw a card.').

card_ruling('soul of theros', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of theros', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').

card_ruling('soul of zendikar', '2014-07-18', 'You can activate the last ability only if the Soul is in your graveyard.').
card_ruling('soul of zendikar', '2014-07-18', 'Exiling the Soul from your graveyard is part of the last ability’s activation cost. A player can’t remove the Soul in response to prevent you from activating the ability.').

card_ruling('soul parry', '2011-01-01', 'Soul Parry prevents all damage the targeted creatures would deal this turn, not just combat damage.').

card_ruling('soul ransom', '2013-01-24', 'Only an opponent of Soul Ransom\'s controller can activate its last ability.').
card_ruling('soul ransom', '2013-01-24', 'In most cases, you\'ll enchant a creature controlled by an opponent, which will cause you to gain control of that creature. Any of your opponents can activate the last ability of Soul Ransom by discarding two cards. When that ability resolves, you\'ll sacrifice Soul Ransom and draw two cards.').

card_ruling('soul reap', '2008-08-01', 'A hybrid creature that\'s green and another color is *not* a nongreen creature. Since it\'s green, it can\'t be nongreen!').
card_ruling('soul reap', '2008-08-01', 'Soul Reap doesn\'t check whether you\'ve cast another black spell until Soul Reap resolves. If you cast Soul Reap, then cast a black instant in response, the creature\'s controller will lose the life when Soul Reap resolves.').
card_ruling('soul reap', '2008-08-01', 'Although Soul Reap says it looks for \"another black spell,\" there\'s no requirement that Soul Reap actually be cast (or be black) for this part of its ability to work. For example, if a spell such as Commandeer causes you to gain control of Soul Reap, it\'ll still check whether you cast a black spell this turn, even though you didn\'t cast Soul Reap itself.').

card_ruling('soul rend', '2004-10-04', 'Only checks the color of the creature on resolution.').

card_ruling('soul sculptor', '2004-10-04', 'The creature stops being a creature (or any other permanent type) and is just an enchantment with no abilities.').
card_ruling('soul sculptor', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('soul seizer', '2011-01-22', 'You choose the target for Soul Seizer\'s triggered ability when that ability triggers and goes on the stack. You choose whether or not to transform it when that ability resolves. If the creature is an illegal target by that time, the ability is countered and none of its effects happen. You can\'t have Soul Seizer transform.').
card_ruling('soul seizer', '2011-01-22', 'If Soul Seizer leaves the battlefield in response to its triggered ability, you won\'t be able to transform it and it won\'t return to the battlefield or become attached to the target.').
card_ruling('soul seizer', '2011-01-22', 'If Soul Seizer has any +1/+1 counters on it when it transforms, they remain on Ghastly Haunting. They won\'t affect the power or toughness of the enchanted creature, however.').
card_ruling('soul seizer', '2011-01-22', 'If Soul Seizer deals combat damage to more than one player at the same time, perhaps because some of its combat damage was redirected, its triggered ability will trigger once for each of those players. Say you transform Soul Seizer into Ghastly Haunting for the first such ability to resolve. For the next ability, you may choose to transform it back into Soul Seizer. If you do, it will be a creature again and won\'t attach to anything; you aren\'t able to do anything between choosing whether to transform it and unattaching it.').

card_ruling('soul snuffers', '2008-08-01', 'The ability puts a -1/-1 counter on Soul Snuffers, too.').

card_ruling('soul spike', '2006-07-15', 'You may pay the alternative cost rather than the card\'s mana cost. Any additional costs are paid as normal.').
card_ruling('soul spike', '2006-07-15', 'If you don\'t have two cards of the right color in your hand, you can\'t choose to cast the spell using the alternative cost.').
card_ruling('soul spike', '2006-07-15', 'You can\'t exile a card from your hand to pay for itself. At the time you would pay costs, that card is on the stack, not in your hand.').

card_ruling('soul stair expedition', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('soul stair expedition', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('soul strings', '2004-10-04', 'Each player gets the option to pay when this spell resolves.').

card_ruling('soul summons', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('soul summons', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('soul summons', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('soul summons', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('soul summons', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('soul summons', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('soul summons', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('soul summons', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('soul summons', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('soul summons', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('soul summons', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('soul summons', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('soul summons', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('soul tithe', '2012-10-01', 'The converted mana cost of a creature token is 0, unless that token is a copy of another creature, in which case it copies that creature\'s mana cost.').
card_ruling('soul tithe', '2012-10-01', 'If the permanent has {X} in its mana cost, X is 0.').

card_ruling('soul warden', '2004-10-04', 'Does not trigger on a card on the battlefield being changed into a creature.').
card_ruling('soul warden', '2004-10-04', 'The ability will not trigger on itself entering the battlefield, but it will trigger on any other creature that is put onto the battlefield at the same time.').
card_ruling('soul warden', '2005-08-01', 'Two Soul Wardens entering the battlefield at the same time will each cause the other\'s ability to trigger.').
card_ruling('soul warden', '2005-08-01', 'The life gain is mandatory.').

card_ruling('soul\'s attendant', '2010-06-15', 'If Soul\'s Attendant and another creature enter the battlefield at the same time, Soul\'s Attendant\'s ability will trigger.').

card_ruling('soul\'s fire', '2008-10-01', 'If either target has become illegal by the time Soul\'s Fire resolves, the spell will still resolve but have no effect. If the first target is illegal, it can\'t perform any actions, so it can\'t deal damage. If the second target is illegal, there\'s nothing for the first target to deal damage to. If both targets have become illegal, the spell will be countered.').

card_ruling('soul\'s majesty', '2009-02-01', 'Soul\'s Majesty\'s only target is the creature. If that creature becomes an illegal target by the time Soul\'s Majesty would resolve, the entire spell is countered. You won\'t draw any cards.').

card_ruling('soulblade djinn', '2015-06-22', 'Any spell you cast that doesn’t have the type creature will cause Soulblade Djinn’s ability to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause the ability to trigger. Playing a land also won’t cause it to trigger.').
card_ruling('soulblade djinn', '2015-06-22', 'Soulblade Djinn’s ability goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').

card_ruling('soulbright flamekin', '2007-10-01', 'Soulbright Flamekin\'s ability uses the stack and can be responded to. It\'s not a mana ability because it has a target.').
card_ruling('soulbright flamekin', '2007-10-01', 'Counts resolutions, not activations. Any such abilities that are still on the stack won\'t count toward the total.').
card_ruling('soulbright flamekin', '2007-10-01', 'When the ability resolves, it counts the number of times that same ability from that this creature has already resolved that turn. It doesn\'t matter who controlled the creature or the previous abilities when they resolved. A copy of this ability (created by Rings of Brighthearth, for example) will count toward the total. Abilities from other creatures with the same name don\'t count towards the total. Neither does an ability that\'s been countered.').
card_ruling('soulbright flamekin', '2007-10-01', 'You get the bonus only the third time the ability resolves. You won\'t get the bonus the fourth, fifth, sixth, or any subsequent times.').

card_ruling('soulfire grand master', '2014-11-24', 'An instant or sorcery spell with lifelink causes its controller to gain life only if it’s the source of any damage that’s dealt. An instant or sorcery spell with lifelink that causes another source to deal damage won’t cause its controller to gain life.').
card_ruling('soulfire grand master', '2014-11-24', 'After you activate Soulfire Grand Master’s last ability, if the next instant or sorcery spell you cast from your hand is countered, it won’t be put into your hand and the effect won’t be applied to the next spell you cast.').
card_ruling('soulfire grand master', '2014-11-24', 'A spell that instructs you to exile it (for example, Temporal Trespass) won’t be put into your hand because it’s not put into your graveyard as it resolves.').

card_ruling('soulflayer', '2014-11-24', 'Creature cards that have abilities that grant the listed keyword abilities to themselves won’t count. For example, exiling a Battle Brawler (a creature with “As long as you control a red or white permanent, Battle Brawler gets +1/+0 and has first strike”) with Soulflayer’s delve ability won’t cause Soulflayer to have first strike, even if you control a red or white permanent.').
card_ruling('soulflayer', '2014-11-24', 'You can’t exile more cards from your graveyard than you’d need to pay Soulflayer’s generic mana requirement. In most situations, this means you can’t exile more than four cards, even if you want to exile more to give it extra abilities.').
card_ruling('soulflayer', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('soulflayer', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('soulflayer', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('soulflayer', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('soulflayer', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('soulless revival', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('soulless revival', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('soulless revival', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('soulless revival', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('souls of the faultless', '2006-02-01', 'If Souls of the Faultless somehow attacks and is dealt combat damage, you will both gain and lose life when its ability resolves.').
card_ruling('souls of the faultless', '2006-02-01', 'If Souls of the Faultless is dealt combat damage in a Two-Headed Giant game, you may choose either member of the attacking team as the player who will lose life. The player you choose doesn\'t have to be the controller of a creature that dealt damage to Souls of the Faultless.').

card_ruling('soulsurge elemental', '2010-06-15', 'Soulsurge Elemental\'s power is continuously calculated, regardless of where it is. If it\'s on the battlefield, it counts itself.').

card_ruling('soulsworn spirit', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('soulsworn spirit', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('soulsworn spirit', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('soulsworn spirit', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('soulsworn spirit', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('soulsworn spirit', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('sound the call', '2006-07-15', 'If Sound the Call resolves while all graveyards are empty, first a 1/1 token is created, then the Sound the Call card is put into your graveyard, which makes the token 2/2. Although the token is momentarily 1/1, state-based actions aren\'t checked until after the Sound the Call card used to create the token is in a graveyard. For example, if Night of Souls\' Betrayal is on the battlefield, the Wolf will momentarily be 0/0. By the time state-based actions are checked, it will be 1/1, so it will remain on the battlefield.').

card_ruling('sovereigns of lost alara', '2009-05-01', 'Any Aura card you find with Sovereigns of Lost Alara\'s second ability must be able to enchant the attacking creature as it currently exists. You need to check the Aura\'s enchant ability as well as any effects, such as protection, that would make it illegal to attach that Aura to the attacking creature. For example, if the attacking creature were an artifact creature with protection from blue, you could find an Aura with \"enchant artifact,\" but you could not find a blue Aura.').
card_ruling('sovereigns of lost alara', '2009-05-01', 'If you choose to search your library but no applicable Aura card is left in your deck (or you choose not to find one), then nothing enters the battlefield and you simply shuffle your library.').
card_ruling('sovereigns of lost alara', '2009-05-01', 'The attacking creature is not targeted by either this ability or the Aura, so a creature with shroud can be enchanted this way.').
card_ruling('sovereigns of lost alara', '2009-05-01', 'If the attacking creature has left the battlefield by the time Sovereigns of Lost Alara\'s second ability resolves, you can\'t put an Aura onto the battlefield this way. You may still choose to search your library just to shuffle it.').

card_ruling('sower of temptation', '2007-10-01', 'You retain control of the targeted creature as long as Sower of Temptation remains on the battlefield, even if a different player gains control of Sower of Temptation itself.').
card_ruling('sower of temptation', '2007-10-01', 'If Sower of Temptation leaves the battlefield before the ability resolves, the ability will have no effect.').

card_ruling('sowing salt', '2004-10-04', 'Does not exile other cards of the same name that are on the battlefield. Just from the graveyard, hand, and library.').
card_ruling('sowing salt', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').

card_ruling('spare from evil', '2011-09-22', 'Only creatures you control when Spare from Evil resolves will be affected. Creatures that enter the battlefield or that you gain control of later in the turn won\'t be.').
card_ruling('spare from evil', '2011-09-22', 'A \"non-Human creature\" is any creature that doesn\'t have the creature type Human. A creature that is a Human in addition to other creature types is not a non-Human creature.').

card_ruling('spark jolt', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('spark jolt', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('spark jolt', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('spark jolt', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('spark spray', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('sparkcaster', '2004-10-04', 'It probably won\'t matter much, but you choose the order in which the two triggered abilities are placed on the stack.').
card_ruling('sparkcaster', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('spatial binding', '2012-07-01', 'If used on an Aura and the permanent enchanted by that Aura phases out, the Aura will not phase out. It will stay on the battlefield and then immediately be put into the graveyard since the permanent it was enchanting is phased out.').

card_ruling('spatial merging', '2012-06-01', 'Until a player planeswalks, the game will exist on both planes. The abilities of both plane cards can affect the game.').
card_ruling('spatial merging', '2012-06-01', 'If a player rolls {C}, both chaos abilities will trigger. The player may put those abilities on the stack in any order.').
card_ruling('spatial merging', '2012-06-01', 'The next time a player planeswalks, he or she planeswalks away from both planes. Their owner puts them on the bottom of his or her planar deck in an order of his or her choice. This order isn\'t revealed to other players.').

card_ruling('spawn of rix maadi', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('spawn of rix maadi', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('spawn of rix maadi', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('spawn of rix maadi', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('spawn of thraxes', '2014-04-26', 'Count the number of Mountains you control when the last ability resolves to determine how much damage is dealt. This includes any land with the subtype Mountain, not just those named Mountain.').

card_ruling('spawnbroker', '2005-10-01', 'If either target becomes illegal (say, if a creature\'s power has changed such that the creature you control now has less power than the other creature), then the exchange won\'t happen.').

card_ruling('spawning bed', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('spawning bed', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('spawning bed', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('spawning bed', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('spawning pool', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('spawning pool', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('spawnsire of ulamog', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('spawnsire of ulamog', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('spawnsire of ulamog', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('spawnsire of ulamog', '2010-06-15', 'You cast Eldrazi cards from outside the game as part of the resolution of Spawnsire of Ulamog\'s last ability. You may cast those cards in any order. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law). You cast all of the cards you like, putting them onto the stack, then the ability finishes resolving. The spells you cast this way will then resolve as normal, one at a time, in the opposite order that they were put on the stack. They\'ll remain in the game for the remainder of that game.').
card_ruling('spawnsire of ulamog', '2010-06-15', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. On the other hand, if the card has additional costs, you may pay those.').
card_ruling('spawnsire of ulamog', '2010-06-15', 'In a casual game, the cards you cast from outside the game come from your personal collection. In a sanctioned event, those cards must come from your sideboard. Note that you can\'t cast cards from exile this way; exile is a zone in the game.').

card_ruling('spawnwrithe', '2008-05-01', 'As the token is created, it checks the printed values of the Spawnwrithe it\'s copying -- or, if the Spawnwrithe whose ability triggered was itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the Spawnwrithe, nor will it copy other effects that have changed Spawnwrithe\'s power, toughness, types, color, or so on. Normally, this means the token will simply be a Spawnwrithe. But if any copy effects have affected that Spawnwrithe, they\'re taken into account.').
card_ruling('spawnwrithe', '2008-05-01', 'If Spawnwrithe\'s ability triggers, then Spawnwrithe becomes a copy of another creature before its ability resolves (due to Mirrorweave, perhaps), the token will be a copy of whatever creature the Spawnwrithe is currently a copy of. At the end of the turn, Spawnwrithe will revert back to what it was, but the token will stay as it is.').
card_ruling('spawnwrithe', '2008-05-01', 'If a copy effect such as Mirrorweave causes some other creature to become a copy of Spawnwrithe, then that creature deals combat damage to a player, the token that\'s put onto the battlefield is simply a copy of Spawnwrithe.').
card_ruling('spawnwrithe', '2008-05-01', 'A token created by a Cemetery Puca that\'s copying a Spawnwrithe will be a Spawnwrithe with the Cemetery Puca ability.').

card_ruling('spearbreaker behemoth', '2008-10-01', 'The ability checks the targeted creature\'s power twice: when the creature becomes the target, and when the ability resolves. Once the ability resolves, it will continue to apply to the affected creature no matter what its power may become later in the turn.').
card_ruling('spearbreaker behemoth', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('spearpoint oread', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('spearpoint oread', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('spearpoint oread', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('spearpoint oread', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('spearpoint oread', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('spearpoint oread', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('spearpoint oread', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('spectra ward', '2014-07-18', 'The protection granted by Spectra Ward won’t cause any Aura to be put into its owner’s graveyard, including Spectra Ward itself. However, if the enchanted creature gains protection from white in another way, Spectra Ward will be put into its owner’s graveyard as a state-based action.').
card_ruling('spectra ward', '2014-07-18', 'Although Auras that are already attached to the creature aren’t affected by Spectra Ward, the enchanted creature can’t be the target of further Aura spells that have one or more colors.').

card_ruling('spectral bears', '2008-08-01', 'The checks for black cards is made both at the time you declare attackers and as the ability resolves. If there are any at the time attackers are declared, it won\'t trigger at all. If there are any at the time it resolves, the ability will do nothing.').

card_ruling('spectral cloak', '2005-08-01', 'Does not destroy Auras already on the creature.').

card_ruling('spectral force', '2007-05-01', '”Your next untap…” refers to the player who controls Spectral Force when the triggered ability resolves. That means if it\'s temporarily stolen (such as with Word of Seizing) and attacked with, it will untap in its previous controller\'s untap step.').

card_ruling('spectral procession', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('spectral procession', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Spectral Procession is white even if you spend six blue mana to cast it.').
card_ruling('spectral procession', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Spectral Procession has a converted mana cost of 6, even if you spend {W}{W}{W} to cast it.').
card_ruling('spectral procession', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Spectral Procession by spending {W}{W}{W}, {2}{W}{W}, {4}{W}, or {6}.').

card_ruling('spectral searchlight', '2005-10-01', 'You may choose yourself.').

card_ruling('spectral shield', '2009-10-01', 'The enchanted creature can still be the target of abilities.').

card_ruling('spectral shift', '2005-04-01', 'Because of updated targeting rulings, it\'s possible to target the same spell or permanent with both abilities when cast with Entwine.').

card_ruling('spectral sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('spectral sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('spell blast', '2013-07-01', 'Alternative costs, additional costs, cost increases, and cost reductions don’t affect a spell’s converted mana cost.').
card_ruling('spell blast', '2013-07-01', 'If the target spell has {X} in its converted mana cost, include the value chosen for that X when determining the value of the {X} in Spell Blast’s mana cost. For example, if you wish to counter Volcanic Geyser (a spell with mana cost {X}{R}{R}) whose X is 7, you’ll need to choose 9 for Spell Blast’s X and pay {9}{U}.').

card_ruling('spell contortion', '2010-03-01', 'If the targeted spell is an illegal target by the time Spell Contortion resolves, Spell Contortion is countered. You won\'t draw any cards.').
card_ruling('spell contortion', '2010-03-01', 'If Spell Contortion resolves, you draw a card for each time Spell Contortion was kicked, regardless of whether the targeted spell\'s controller pays {2} or whether the targeted spell is countered this way.').

card_ruling('spell crumple', '2011-09-22', 'If a commander is countered this way, it will be put on the bottom of its owner\'s library.').
card_ruling('spell crumple', '2011-09-22', 'If the spell is an illegal target when Spell Crumple tries to resolve, it will be countered and none of its effects will happen. Spell Crumple will be put into its owner\'s graveyard.').
card_ruling('spell crumple', '2011-09-22', 'If the targeted spell can\'t be countered, it won\'t be put onto the bottom of its owner\'s library. Spell Crumple will still be put on the bottom of its owner\'s library.').

card_ruling('spell rupture', '2013-01-24', 'The value of X is the greatest power among creatures you control when Spell Rupture resolves. If you control no creatures at that time, X will be 0. The target spell\'s controller can choose to pay {0} (simply by indicating he or she wishes to do so). That player can also choose to not pay {0} and the spell will be countered.').

card_ruling('spell shrivel', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('spell shrivel', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('spell shrivel', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('spell shrivel', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('spell shrivel', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('spell snip', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('spellbinder', '2004-12-01', 'Spellbinder\'s second ability allows you to create a copy of the imprinted instant card in the Exile zone (that\'s where the imprinted instant card is) and then cast it without paying its mana cost.').
card_ruling('spellbinder', '2004-12-01', 'You don\'t pay the spell\'s mana cost. If the spell has X in its mana cost, X is 0. You do pay any additional costs for that spell. You can\'t use any alternative costs.').
card_ruling('spellbinder', '2004-12-01', 'If the copied card is a split card, you choose which one side of it to cast, but you can\'t cast both sides. You can choose the other side later in the game, though.').
card_ruling('spellbinder', '2004-12-01', 'Spellbinder imprints a card only once, when it enters the battlefield. Attaching Spellbinder to a creature doesn\'t trigger its enters-the-battlefield ability.').
card_ruling('spellbinder', '2005-03-01', 'The creation of the copy and then the casting of the copy are both optional.').

card_ruling('spellbook', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Spellbook onto the battlefield, you\'ll have no maximum hand size. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('spellbound dragon', '2009-05-01', 'You\'ll discard a card even if some other effect replaces the draw.').

card_ruling('spellbreaker behemoth', '2009-05-01', 'Any spells or abilities that would counter Spellbreaker Behemoth or (once it\'s on the battlefield) a creature spell you control with power 5 or greater can still be cast or activated and will still resolve. They just won\'t counter that spell. Any other effects they have will happen as normal.').
card_ruling('spellbreaker behemoth', '2009-05-01', 'Effects that affect a creature\'s power (such as the one from Glorious Anthem, for example) apply only to creatures on the battlefield, not to creature spells on the stack.').
card_ruling('spellbreaker behemoth', '2009-05-01', 'If a creature card has \"*\" in its power, the ability that defines \"*\" works in all zones, including the stack.').

card_ruling('spellheart chimera', '2013-09-15', 'The ability that defines Spellheart Chimera’s power functions in all zones, not just the battlefield.').

card_ruling('spelljack', '2004-10-04', 'If you cast an instant or sorcery, it goes to its owner\'s graveyard when it resolves.').
card_ruling('spelljack', '2004-10-04', 'This spell can be used on a spell cast using Flashback.').
card_ruling('spelljack', '2008-08-01', 'If you use this on an opponent\'s spell which has Buyback, you may pay the Buyback cost as you cast it. If you do so, the card will go to its owner\'s hand upon resolution. It will not go to your hand, nor to any player\'s graveyard.').

card_ruling('spellshift', '2007-02-01', 'If the targeted spell can\'t be countered, it remains on the stack and the rest of Spellshift\'s effect continues to happen.').
card_ruling('spellshift', '2007-02-01', 'If the revealed instant or sorcery card isn\'t cast, it gets shuffled back into the library.').
card_ruling('spellshift', '2007-02-01', 'If there are no instant or sorcery cards in the player\'s library, the entire library is revealed and then shuffled.').

card_ruling('spellshock', '2004-10-04', 'It affects all players, including you.').

card_ruling('spellskite', '2011-06-01', 'You can activate Spellskite\'s ability even if Spellskite wouldn\'t be a legal target for the spell or ability. However, the target of that spell or ability will remain unchanged.').
card_ruling('spellskite', '2011-06-01', 'If changing one target of a spell or ability to Spellskite would make other targets of that spell or ability illegal, the targets remain unchanged.').
card_ruling('spellskite', '2011-06-01', 'If the spell or ability has multiple instances of the word \"target,\" you choose which target you\'re changing to Spellskite when Spellskite\'s ability resolves.').
card_ruling('spellskite', '2011-06-01', 'By activating Spellskite\'s ability multiple times, you can change each target of a spell or ability with multiple instances of the word \"target\" to Spellskite, one at a time.').
card_ruling('spellskite', '2011-06-01', 'The target of the spell or ability won\'t change unless Spellskite fulfills all the targeting criteria, even if multiple instances of the word \"target\" are used. For example, you can\'t change both targets of Arc Trail to Spellskite.').
card_ruling('spellskite', '2011-06-01', 'If a spell or ability has multiple targets but doesn\'t use the word \"target\" multiple times, such as Fulgent Distraction, you can only successfully change one of the targets to Spellskite.').
card_ruling('spellskite', '2011-06-01', 'If a spell or ability has a variable number of targets, you can\'t change the number of targets.').
card_ruling('spellskite', '2011-06-01', 'If Spellskite leaves the battlefield before its ability resolves, the targets remain unchanged.').

card_ruling('spellstutter sprite', '2007-10-01', 'The value of X needs to be determined both when the ability triggers (so you can choose a target) and again when the ability resolves (to check if that target is still legal). If the number of Faeries you control has decreased enough in that time to make the target illegal, Spellstutter Sprite\'s ability will be countered (and the targeted spell will resolve as normal).').

card_ruling('spelltwine', '2012-07-01', 'Spelltwine has two targets: the instant or sorcery card in your graveyard and the one in an opponent\'s graveyard. You can\'t cast Spelltwine unless you can choose both legal targets.').
card_ruling('spelltwine', '2012-07-01', 'If one of Spelltwine\'s targets is illegal when Spelltwine tries to resolve, you\'ll still exile and copy the remaining legal target.').
card_ruling('spelltwine', '2012-07-01', 'The copies are created in exile and cast from exile. The cards remain exiled.').
card_ruling('spelltwine', '2012-07-01', 'You can cast the copies in either order.').
card_ruling('spelltwine', '2012-07-01', 'The copy you cast first will already be on the stack when you cast the other copy. If that spell targets a spell, you may choose the first copy as a legal target.').
card_ruling('spelltwine', '2012-07-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('spelltwine', '2012-07-01', 'If the card has X in its mana cost, you must choose 0 as its value.').

card_ruling('spellweaver helix', '2004-10-04', 'If the imprinted card leaves the Exile zone while the ability to make a copy is on the stack, then no copy will be made.').
card_ruling('spellweaver helix', '2004-10-04', 'If this card leaves the battlefield while the ability to make a copy is on the stack, the ability will still make a copy using the last-known-information rule.').
card_ruling('spellweaver helix', '2004-12-01', 'If there\'s only one imprinted sorcery card, nothing happens.').
card_ruling('spellweaver helix', '2004-12-01', 'If the two imprinted sorcery cards have the same name and a card with that name is cast, only one copy is created, not two.').
card_ruling('spellweaver helix', '2004-12-01', 'Spellweaver Helix\'s second ability creates a copy of the imprinted card in the Exile zone (that\'s where the imprinted sorcery card is), then allows you to cast it without paying its mana cost.').
card_ruling('spellweaver helix', '2004-12-01', 'You cast the copy while this ability is resolving, and still on the stack. Normally, you\'re not allowed to cat spells or activate abilities at this time. Spellweaver Helix\'s ability breaks this rule. (The card that triggered this ability is also still on the stack.)').
card_ruling('spellweaver helix', '2004-12-01', 'You don\'t pay the spell\'s mana cost. If a spell has X in its mana cost, X is 0. You do pay any additional costs for that spell. You can\'t use any alternative costs.').
card_ruling('spellweaver helix', '2004-12-01', 'A split-card spell\'s name is only half the card\'s name, so Spellweaver Helix never triggers when a split card is cast.').
card_ruling('spellweaver helix', '2004-12-01', 'You can\'t cast the copy if an effect prevents you from casting sorceries or from casting that particular sorcery.').
card_ruling('spellweaver helix', '2004-12-01', 'You can\'t cast the copy unless all of its targets can be chosen.').
card_ruling('spellweaver helix', '2004-12-01', 'If you don\'t want to cast the copy, you can choose not to; the copy ceases to exist the next time state-based actions are checked.').
card_ruling('spellweaver helix', '2005-03-01', 'The creation of the copy and then the casting of the copy are both optional.').

card_ruling('spellweaver volute', '2007-05-01', 'This Aura targets, and enchants, an instant card in a graveyard. This is the first Aura that enchants something that\'s neither a permanent nor a player.').
card_ruling('spellweaver volute', '2007-05-01', 'The Aura will be on the battlefield attached to a card in a different zone. The Aura won\'t be in the graveyard. The enchanted card won\'t be on the battlefield.').
card_ruling('spellweaver volute', '2007-05-01', 'If the enchanted card changes zones (due to being cast with flashback, or being exiled with Cremate, for example), Spellweaver Volute \"falls off\" and is put into its owner\'s graveyard as a state-based action.').
card_ruling('spellweaver volute', '2007-05-01', 'When you cast a sorcery spell, Spellweaver Volute\'s ability triggers. It will make a copy of the enchanted card. The copy will exist in the graveyard. You\'ll have the option to cast the copy. If you don\'t, the copy remains in the graveyard and ceases to exist the next time state-based actions are checked. If you do cast the copy, the copy is cast from the graveyard. After you finish casting it, the enchanted card is exiled and you must choose a new instant card in a graveyard to attach Spellweaver Volute to. If you can\'t, Spellweaver Volute remains on the battlefield attached to nothing and is then put into the graveyard the next time state-based actions are checked.').
card_ruling('spellweaver volute', '2007-05-01', 'Say Spellweaver Volute\'s ability triggers, then the enchanted instant card is exiled in response. Spellweaver Volute, which is now enchanting nothing, is put into its owner\'s graveyard as a state-based action. When the Volute\'s ability resolves, it will check its last existence on the battlefield and identify the \"enchanted card\" as \"nothing,\" so no copy is created.').
card_ruling('spellweaver volute', '2007-05-01', 'Say Spellweaver Volute\'s ability triggers, then Spellweaver Volute leaves the battlefield in response. Then the instant card that was enchanted is removed from the graveyard in response. When the Volute\'s ability resolves, it will check its last existence on the battlefield and identify the \"enchanted card\" as that instant card, so the card is copied and controller of the triggered ability may cast it.').

card_ruling('spellwild ouphe', '2007-05-01', 'The cost reduction applies to spells cast by any player.').
card_ruling('spellwild ouphe', '2007-05-01', 'The cost reduction will be {2} no matter how many times the spell targets the same Spellwild Ouphe.').
card_ruling('spellwild ouphe', '2007-05-01', 'The spell may have any number of other targets; that won\'t affect the cost reduction unless the spell targets other Spellwild Ouphes as well. In that case, the spell costs {2} less to cast for each different Spellwild Ouphe it targets.').

card_ruling('sphere of resistance', '2004-10-04', 'It affects all players, including you.').

card_ruling('sphere of safety', '2012-10-01', 'If you control Sphere of Safety, your opponents can choose not to attack with a creature with an ability that says it attacks if able.').
card_ruling('sphere of safety', '2012-10-01', 'In a Two-Headed Giant game, if one player controls Sphere of Safety, creatures can\'t attack that player\'s team or a planeswalker that player controls unless their controller pays {X} for each of those creatures he or she controls. Creatures can attack planeswalkers controlled by that player\'s teammate without having to pay this cost.').

card_ruling('sphinx ambassador', '2009-10-01', 'You may search for any card, not just a creature card. Of course, if you choose a noncreature card, you won\'t be able to put it onto the battlefield no matter what card the player names.').
card_ruling('sphinx ambassador', '2009-10-01', 'Once you\'ve chosen a card during your search, you must clearly indicate which card you\'ve chosen (preferably without showing the front of that card to the player!). Then the player names a card.').
card_ruling('sphinx ambassador', '2009-10-01', 'After the player names a card, check if the card you chose has that name. If it doesn\'t, but it\'s a creature card, you may put it onto the battlefield. If you don\'t want to put that card onto the battlefield, it\'s not a creature card, or it does have the name the player said, it simply stays in that player\'s library without ever being revealed. The player then shuffles his or her library, including that card.').
card_ruling('sphinx ambassador', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('sphinx of jwar isle', '2009-10-01', 'Essentially, Sphinx of Jwar Isle lets you play with the top card of your library revealed only to you. Knowing what that card is becomes part of the information you have access to, just like you can look at the cards in your hand. You may look at the top card of your library whenever you want, even if you don\'t have priority. This action doesn\'t use the stack.').
card_ruling('sphinx of jwar isle', '2009-10-01', 'If the top card of your library changes during the process of casting a spell or activating an ability, you can\'t look at the new top card until the process of casting the spell or activating the ability ends (all targets are chosen, all costs are paid, and so on).').

card_ruling('sphinx of lost truths', '2009-10-01', 'Sphinx of Lost Truths behaves differently than other cards with kicker. Its enters-the-battlefield ability always triggers, and it checks whether it *wasn\'t* kicked. The two possible results are as follows: -- When an unkicked Sphinx of Lost Truths enters the battlefield, you draw three cards, then discard three cards. -- When a kicked Sphinx of Lost Truths enters the battlefield, you draw three cards.').

card_ruling('sphinx of uthuun', '2011-09-22', 'You (not your opponent) choose which pile to put into your hand and which to put into your graveyard.').
card_ruling('sphinx of uthuun', '2011-09-22', 'A pile can have no cards in it. In this case, you\'ll choose whether to put all the revealed cards into your hand or into your graveyard.').
card_ruling('sphinx of uthuun', '2011-09-22', 'If your library has fewer than five cards, you reveal all the cards in it, then your opponent separates them into two piles.').
card_ruling('sphinx of uthuun', '2011-09-22', 'In multiplayer games, you choose an opponent to separate the cards when the ability resolves. This doesn\'t target that opponent. Because the cards are revealed, all players may see the cards and offer opinions.').

card_ruling('sphinx sovereign', '2008-10-01', 'The ability checks whether Sphinx Sovereign is tapped or untapped at the time the ability resolves. If Sphinx Sovereign is no longer on the battlefield by then, the ability checks whether Sphinx Sovereign was tapped or untapped at the time it left the battlefield.').

card_ruling('sphinx summoner', '2009-02-01', 'The card you find must be both an artifact and a creature.').

card_ruling('sphinx\'s disciple', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('sphinx\'s disciple', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('sphinx\'s disciple', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('sphinx\'s disciple', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('sphinx\'s herald', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('sphinx\'s herald', '2008-10-01', 'To activate the ability, you must sacrifice three different creatures. You can\'t sacrifice just one three-colored creature, for example.').
card_ruling('sphinx\'s herald', '2008-10-01', 'You may sacrifice multicolored creatures as part of the cost of the search ability. If you sacrifice a multicolored creature this way, specify which part of the cost that creature is satisfying.').
card_ruling('sphinx\'s herald', '2008-10-01', 'You may sacrifice the Herald itself to help pay for the cost of its ability.').

card_ruling('sphinx\'s revelation', '2012-10-01', 'If the value chosen for X is larger than the number of cards left in your library, you\'ll lose the game the next time state-based actions are performed.').

card_ruling('sphinx\'s tutelage', '2015-06-22', 'Colorless is not a color, so putting two colorless cards into a graveyard won’t cause the process to repeat.').
card_ruling('sphinx\'s tutelage', '2015-06-22', 'The process will keep repeating as long as both cards share a color and neither is a land card.').

card_ruling('sphinx-bone wand', '2010-06-15', 'If you cast an instant or sorcery spell, Sphinx-Bone Wand\'s ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').
card_ruling('sphinx-bone wand', '2010-06-15', 'If the targeted creature or player is an illegal target by the time the ability resolves, the ability is countered. You won\'t put a charge counter on Sphinx-Bone Wand and it won\'t deal damage.').
card_ruling('sphinx-bone wand', '2010-06-15', 'If Sphinx-Bone Wand leaves the battlefield before its ability resolves, you won\'t be able to put a charge counter on it. As a result, it won\'t deal damage.').

card_ruling('spider climb', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('spider spawning', '2011-09-22', 'The number of creature cards in your graveyard is counted when Spider Spawning resolves.').

card_ruling('spider umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('spider umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('spider umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('spider umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('spider umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('spider umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('spider umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('spider umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('spider umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('spider umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('spider umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('spidery grasp', '2011-09-22', 'Spidery Grasp can target a creature that\'s already untapped. It will still get +2/+4 and gain reach.').

card_ruling('spike cannibal', '2004-10-04', 'It will move its own +1/+1 counter onto itself, but it does not die during this move since it never is without a counter at a time when State-Based Actions are checked.').

card_ruling('spike tiller', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('spike weaver', '2004-10-04', 'The second ability includes your own creatures as well.').

card_ruling('spikeshot elder', '2011-01-01', 'How much damage Spikeshot Elder deals as a result of its ability is determined by its power as that ability resolves. If Spikeshot Elder is no longer on the battlefield at that time, its last existence on the battlefield is checked to determine its power.').

card_ruling('spiketail drake', '2004-10-04', 'The spell\'s controller gets the option to pay when this ability resolves.').

card_ruling('spiketail hatchling', '2004-10-04', 'The spell\'s controller gets the option to pay when this ability resolves.').

card_ruling('spin into myth', '2007-05-01', 'The opponent chosen for the fateseal action doesn\'t have to be the player who controlled the target creature.').

card_ruling('spinal embrace', '2004-10-04', 'The sacrifice is mandatory. The last \"if you do\" only fails if for some reason you could not sacrifice.').

card_ruling('spinal parasite', '2004-12-01', 'Yes, this card\'s base power/toughness is -1/-1. If it has five +1/+1 counters on it, it\'s a 4/4 creature.').
card_ruling('spinal parasite', '2004-12-01', 'If Spinal Parasite is a 1/1 because it has two +1/+1 counters on it, you can remove both counters to pay for its activated ability.').
card_ruling('spinal parasite', '2004-12-01', 'Any permanent is a legal target for Spinal Parasite\'s ability, not just one with a counter on it. The player who controls Spinal Parasite chooses which counter to remove from the targeted permanent.').

card_ruling('spine of ish sah', '2011-06-01', 'Spine of Ish Sah\'s last ability doesn\'t allow you to sacrifice it. You must find another way to get Spine of Ish Sah into the graveyard.').

card_ruling('spined sliver', '2004-10-04', 'If two Spined Slivers are on the battlefield then each blocked sliver gets +2/+2 for each creature blocking it.').
card_ruling('spined sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('spined sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('spined thopter', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('spined thopter', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('spined thopter', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('spined thopter', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('spined thopter', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('spinerock knoll', '2007-10-01', 'At the time the last ability resolves, you\'ll get to play the card if a player who is currently your opponent, or a player who was your opponent at the time he or she left the game, has been dealt 7 damage over the course of the turn.').
card_ruling('spinerock knoll', '2007-10-01', 'It doesn\'t matter how the opponent was dealt damage or by whom, as long as the total damage is 7 or more. You don\'t specify an opponent when you activate the ability.').
card_ruling('spinerock knoll', '2007-10-01', 'You\'ll get to play the card even if Spinerock Knoll wasn\'t on the battlefield at the time some or all of the 7 damage was dealt.').

card_ruling('spinneret sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('spinneret sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('spinning darkness', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('spinning darkness', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('spinning darkness', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('spinning darkness', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('spinning darkness', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('spiny starfish', '2004-10-04', 'The token is put onto the battlefield even if this card is not on the battlefield at the beginning of the end step.').
card_ruling('spiny starfish', '2009-10-01', 'Spiny Starfish\'s first ability creates a regeneration shield for it. Spiny Starfish\'s second ability checks whether it _used_ any regeneration shields that turn, not whether any regeneration shields were created for it.').
card_ruling('spiny starfish', '2009-10-01', 'The second ability doesn\'t care where the regeneration shields Spiny Starfish used that turn came from. If it regenerated due to a regeneration shield from Refresh, for example, the ability will still put a Starfish token onto the battlefield.').
card_ruling('spiny starfish', '2009-10-01', 'The second ability has an \"intervening \'if\' clause,\" so it won\'t trigger at all unless Spiny Starfish had regenerated that turn. However, the number of tokens that are created isn\'t determined until the ability resolves (in case it regenerates again before that time).').
card_ruling('spiny starfish', '2009-10-01', 'If Spiny Starfish regenerates during the end step after its second ability has resolved, no tokens will be created as a result.').

card_ruling('spiraling duelist', '2011-06-01', 'If Spiraling Duelist has double strike as the combat damage step begins, there will be a second combat damage step. As that second combat damage step begins, if Spiraling Duelist no longer has double strike (perhaps because an artifact creature you controlled was destroyed), Spiraling Duelist will not assign combat damage a second time.').

card_ruling('spire serpent', '2011-06-01', 'Spire Serpent still has defender if you control three or more artifacts, although it will be able to attack.').
card_ruling('spire serpent', '2011-06-01', 'If Spire Serpent has been declared as an attacker, and you stop controlling three or more artifacts later in combat, Spire Serpent continues being an attacking creature until combat ends.').

card_ruling('spirit bonds', '2014-07-18', 'You decide whether to pay {W} as the triggered ability resolves. You may only pay {W} and create a Spirit token once per nontoken creature.').
card_ruling('spirit bonds', '2014-07-18', 'You can sacrifice any Spirit to activate the last ability, not just a Spirit token created by Spirit Bonds.').

card_ruling('spirit en-kor', '2004-10-04', 'The ability of this card does not do anything to stop Trample damage from being assigned to the defending player.').
card_ruling('spirit en-kor', '2004-10-04', 'It can redirect damage to itself.').
card_ruling('spirit en-kor', '2004-10-04', 'It is possible to redirect more damage to a creature than that creature\'s toughness.').
card_ruling('spirit en-kor', '2004-10-04', 'You can use this ability as much as you want prior to damage being dealt.').
card_ruling('spirit en-kor', '2013-07-01', 'When you redirect combat damage it is still combat damage.').

card_ruling('spirit flare', '2004-10-04', 'If either target is illegal when the spell resolves, it will do no damage.').

card_ruling('spirit link', '2004-10-04', 'Damage that is redirected is not dealt to the original creature or player, but it is dealt to the new creature or player.').
card_ruling('spirit link', '2004-10-04', 'You only gain the life when the triggered ability resolves. If you are reduced to zero life before the ability resolves, you will lose before gaining the life.').
card_ruling('spirit link', '2008-04-01', 'You gain life for all damage which is not prevented, regardless of what the creature damages (player or another creature) or the toughness of the blocking creature. If you put this on a 5/5 and it is blocked by a 1/1, you still gain 5 life.').

card_ruling('spirit mirror', '2004-10-04', 'It checks if there are any Reflection tokens on the battlefield at the beginning of upkeep, and if there are it will not trigger at all. It will check again on resolution and will do nothing if a Reflection is on the battlefield at that time.').

card_ruling('spirit of resistance', '2004-10-04', 'A permanent which is of multiple colors counts as each of its colors.').

card_ruling('spirit of the labyrinth', '2014-02-01', 'You can draw a maximum of one card on each player’s turn. Subsequent card draws are ignored.').
card_ruling('spirit of the labyrinth', '2014-02-01', 'If you haven’t drawn any cards in a turn, and a spell or ability would cause you to draw multiple cards, you’ll just draw one card.').
card_ruling('spirit of the labyrinth', '2014-02-01', 'If you draw a card, and then Spirit of the Labyrinth enters the battlefield, you won’t be able to draw more cards that turn.').
card_ruling('spirit of the labyrinth', '2014-02-01', 'If a replacement effect would try to replace a card that you can’t draw, that effect can’t apply.').

card_ruling('spirit shackle', '2009-10-01', 'Spirit Shackle\'s ability triggers only when the enchanted creature changes from untapped to tapped. If Spirit Shackle enters the battlefield attached to an already tapped creature, it won\'t do anything (until that creature untaps and becomes tapped again, that is).').

card_ruling('spiritual visit', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('spiritual visit', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('spiritual visit', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('spiritual visit', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('spite of mogis', '2014-04-26', 'Spite of Mogis isn’t counted among the number of instant and sorcery cards in your graveyard. It isn’t put there until after it deals damage and you scry 1.').

card_ruling('spitebellows', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('spitebellows', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('spitebellows', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('spitebellows', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('spitebellows', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('spitebellows', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').

card_ruling('spiteful blow', '2014-04-26', 'You can’t cast Spiteful Blow without a legal creature and land to target. If one of those targets is illegal when Spiteful Below tries to resolve, the other one will still be destroyed.').

card_ruling('spiteful bully', '2004-10-04', 'It can damage itself. In fact, it has to if it is the only creature you control.').

card_ruling('spiteful returned', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('spiteful returned', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('spiteful returned', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('spiteful returned', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('spiteful returned', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('spiteful returned', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('spiteful returned', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('spiteful shadows', '2011-01-22', 'Spiteful Shadows causes the enchanted creature to deal damage to its controller. Abilities like lifelink and infect will apply.').

card_ruling('spiteful visions', '2008-05-01', 'Spiteful Visions\'s second ability triggers any time any player draws a card, not just when a player draws a card as a result of its first ability.').
card_ruling('spiteful visions', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('spitemare', '2008-08-01', 'Even if the amount of damage dealt to Spitemare is enough to destroy it, Spitemare\'s ability will trigger and it will deal the full amount of damage to the target.').

card_ruling('spitting image', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('spitting image', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('spitting image', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('spitting image', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').
card_ruling('spitting image', '2008-08-01', 'As the token is created, it checks the printed values of the creature it\'s copying -- or, if that creature is itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, or so on.').

card_ruling('spitting sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('spitting sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('splatter thug', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('splatter thug', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('splatter thug', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('splatter thug', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('splinter', '2004-10-04', 'Does not exile other cards of the same name that are on the battlefield. Just from the graveyard, hand, and library.').
card_ruling('splinter', '2005-02-01', 'Splinter can target and exile cards that aren\'t always artifacts, but are artifacts when Splinter is cast and resolves (such as an animated Stalking Stones). Splinter\'s exile effect only looks for cards by name, not type.').
card_ruling('splinter', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').

card_ruling('splinter twin', '2010-06-15', 'Splinter Twin grants an activated ability to the enchanted creature. That creature\'s controller (who is not necessarily the Aura\'s controller) can activate it. The creature\'s controller is the player who gets the token.').
card_ruling('splinter twin', '2010-06-15', 'The token that\'s put onto the battlefield copies exactly what\'s printed on the enchanted creature (unless that creature is copying something else or it\'s a token; see below), plus it has haste. It doesn\'t copy whether the enchanted creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or whether it\'s been affected by any noncopy effects that changed its power, toughness, types, color, or so on. In particular, it doesn\'t copy the ability granted to the enchanted creature by Splinter Twin.').
card_ruling('splinter twin', '2010-06-15', 'If the enchanted creature is copying something else when the ability resolves (for example, if it\'s a Renegade Doppelganger), then the token enters the battlefield as a copy of whatever the enchanted creature is copying, plus it has haste.').
card_ruling('splinter twin', '2010-06-15', 'If the enchanted creature is a token, the new token copies the characteristics of the original token as stated by the effect that put it onto the battlefield, plus it has haste.').
card_ruling('splinter twin', '2010-06-15', 'If the enchanted creature has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('splinter twin', '2010-06-15', 'Any enters-the-battlefield abilities of the enchanted creature will trigger when the token is put onto the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the enchanted creature will also work.').
card_ruling('splinter twin', '2010-06-15', 'The token will not be kicked, even if the creature it\'s copying was.').
card_ruling('splinter twin', '2010-06-15', 'If you activate the ability and the enchanted creature leaves the battlefield before the ability resolves, you still get a token. The enchanted creature\'s last existence on the battlefield is checked to see what it was (specifically, if it was itself or if it was copying something else).').
card_ruling('splinter twin', '2010-06-15', 'The token is exiled at the beginning of the next end step regardless of who controls it at that time, or whether Splinter Twin or the enchanted creature is still on the battlefield at that time.').
card_ruling('splinter twin', '2010-06-15', 'If the ability is activated during a turn\'s end step, the token will be exiled at the beginning of the following turn\'s end step.').
card_ruling('splinter twin', '2010-06-15', 'If the token isn\'t exiled when the delayed triggered ability resolves (due to Stifle, perhaps), it remains on the battlefield indefinitely. It continues to have haste.').
card_ruling('splinter twin', '2013-07-01', 'If the enchanted creature is legendary, you will have to put either the original creature or the token into the graveyard as a state-based action.').

card_ruling('splinterfright', '2011-09-22', 'The ability that defines Splinterfright\'s power and toughness works in all zones, not just the battlefield. If Splinterfright is in your graveyard, it will count itself.').
card_ruling('splinterfright', '2011-09-22', 'If Splinterfright\'s controller has only one card in his or her library, he or she puts that card into his or her graveyard.').

card_ruling('split decision', '2014-05-29', 'If duplication gets more votes, Split Decision creates a copy of the spell. You control the copy. That copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('split decision', '2014-05-29', 'The copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('split decision', '2014-05-29', 'If the spell being copied is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('split decision', '2014-05-29', 'If the spell being copied has an X whose value was determined as it was cast (like Skeletal Scrying has), the copy will have the same value of X.').
card_ruling('split decision', '2014-05-29', 'Effects based on any additional costs that were paid for the original spell will be copied as though those same costs were paid for the copy. For example, if a player exiles three cards from his or her graveyard to cast Skeletal Scrying, and you copy it, the copy will also cause you to draw three cards and lose 3 life (but you won’t exile any cards from your graveyard).').
card_ruling('split decision', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('split decision', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('split decision', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('split decision', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('split decision', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('spoils of blood', '2014-11-07', 'Use the number of creatures that died that turn as Spoils of Blood resolves to determine the value of X. In other words, creatures that die in response to Spoils of Blood will count.').

card_ruling('spoils of evil', '2004-10-04', 'The text \"for each artifact or creature\" means the sum of cards which are either creature and/or artifact. Artifact creatures are not double counted.').
card_ruling('spoils of evil', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('spoils of victory', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('spoils of victory', '2009-10-01', 'Spoils of Victory allows you to search your library for any card with the land type Plains, Island, Swamp, Mountain, or Forest, not just a card with one of those names.').

card_ruling('spoils of war', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('spoils of war', '2008-08-01', 'X is set as you cast the spell to be exactly the number of appropriate cards in an opponent\'s graveyard. You can\'t choose a higher or lower value for X.').

card_ruling('spontaneous combustion', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('spontaneous combustion', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('spore burst', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('spore burst', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('spore burst', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('spore cloud', '2004-10-04', 'Only makes attackers or blockers that have been declared before the spell is cast unable to untap as normal next turn. So, if used before attackers or blockers are declared, it is simply a Fog-like effect. If used after attackers are declared but before blockers are declared, it does the Fog effect and makes attackers not untap as normal. If done after blockers are declared, it has full effect.').
card_ruling('spore cloud', '2008-10-01', 'If a creature affected by Spore Cloud is untapped at the time its controller\'s next untap step begins, the \"doesn\'t untap\" effect doesn\'t do anything. It won\'t apply at some later time when that creature is tapped.').
card_ruling('spore cloud', '2008-10-01', 'Spore Cloud doesn\'t track the creatures\' controllers. If an affected creature changes controllers before its old controller\'s next untap step, Spore Cloud will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('sporemound', '2013-07-01', 'Sporemound’s ability triggers whenever you play a land and also whenever a land enters the battlefield under your control for some other reason, such as the ability of Into the Wilds.').

card_ruling('sporesower thallid', '2006-09-25', 'Sporesower Thallid\'s triggered ability will put a spore counter on each Fungus creature you control, even if that Fungus doesn\'t have any abilities that makes use of them.').

card_ruling('sporogenesis', '2004-10-04', 'If a non-creature with fungus counters on it leaves the battlefield, there is no effect.').
card_ruling('sporogenesis', '2004-10-04', 'If more than one Sporogenesis is on the battlefield and a creature with fungus counters on it leaves the battlefield, each Sporogenesis will put a Saproling onto the battlefield for each counter. In other words if there are N copies of Sporogenesis and the creature had M fungus counters, you get N * M Saprolings.').
card_ruling('sporogenesis', '2004-10-04', 'If one Sporogenesis leaves the battlefield, all fungus counters are removed even if other Sporogenesis cards are on the battlefield.').
card_ruling('sporogenesis', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('sporoloth ancient', '2007-05-01', 'All creatures you control gain the ability, regardless of whether they have spore counters on them, which means for example, they would lose the bonus from Muraganda Petroglyphs.').
card_ruling('sporoloth ancient', '2007-05-01', 'Your other creatures that produce spore counters (such as Thallids) can create a Saproling by removing two spore counters (by using this ability) or by removing three spore counters (by using their own ability).').

card_ruling('spread the sickness', '2011-06-01', 'If the creature is an illegal target when Spread the Sickness tries to resolve, the spell is countered. You won\'t proliferate.').
card_ruling('spread the sickness', '2013-07-01', 'If the creature regenerates or has indestructible when Spread the Sickness resolves, you\'ll still proliferate.').

card_ruling('spreading algae', '2008-04-01', 'This card now has Enchant Swamp, which works exactly like any other Enchant ability. This means it can only be cast targeting a Swamp, and it will be put into its owner\'s graveyard if the permanent it\'s attached to ever stops being a Swamp.').

card_ruling('spreading seas', '2009-10-01', 'The enchanted land loses its existing land types and any abilities printed on it. It now has the land type Island and has the ability to tap to add {U} to its controller\'s mana pool. Spreading Seas doesn\'t change the enchanted land\'s name or whether it\'s legendary, basic, or snow.').

card_ruling('spring cleaning', '2007-10-01', 'If you win the clash, you destroy all enchantments all of your opponents control, not just enchantments controlled by the opponent you clashed with.').
card_ruling('spring cleaning', '2013-09-20', 'If you win the clash and the targeted enchantment is controlled by an opponent, it will be destroyed a second time if it\'s still on the battlefield (for instance, if it somehow regenerated).').

card_ruling('springjack pasture', '2008-08-01', 'Springjack Pasture\'s last ability is a mana ability. The whole ability -- including the life gain -- doesn\'t use the stack and therefore can\'t be responded to.').
card_ruling('springjack pasture', '2008-08-01', 'If you sacrifice no Goats, you\'ll add no mana to your mana pool and you\'ll gain no life.').
card_ruling('springjack pasture', '2008-08-01', 'You can sacrifice any Goats you control to activate the third ability, not just Goats created by Springjack Pasture.').

card_ruling('springjack shepherd', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('springjack shepherd', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').
card_ruling('springjack shepherd', '2008-08-01', 'The effect counts the mana symbols in this cards mana cost as well.').

card_ruling('springleaf drum', '2014-02-01', 'You can tap a creature that hasn’t been under your control since your most recent turn began to activate the ability.').

card_ruling('sprinting warbrute', '2015-02-25', 'You still choose which player or planeswalker Sprinting Warbrute attacks.').
card_ruling('sprinting warbrute', '2015-02-25', 'If, during your declare attackers step, Sprinting Warbrute is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('sprinting warbrute', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('sprinting warbrute', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('sprinting warbrute', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('sprinting warbrute', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('sprout swarm', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('sprout swarm', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('sprout swarm', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('sprout swarm', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('sprout swarm', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('sprout swarm', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('sprout swarm', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('sprouting phytohydra', '2006-05-01', 'A token copy of Sprouting Phytohydra is created even if the damage dealt to Sprouting Phytohydra destroys it.').
card_ruling('sprouting phytohydra', '2006-05-01', 'If a token copy of Sprouting Phytohydra is dealt damage, it will create another token copy.').
card_ruling('sprouting phytohydra', '2006-05-01', 'If Cytoshape turns a creature into a copy of Sprouting Phytohydra, and that creature is dealt damage, the token that is put onto the battlefield is simply a copy of Sprouting Phytohydra. At the end of the turn, the Cytoshaped creature will revert back to what it was, but the token copying it will not.').

card_ruling('sprouting vines', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('sprouting vines', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('sprouting vines', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('sprouting vines', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('spurnmage advocate', '2004-10-04', 'The ability is not countered if both cards are missing from the graveyard on resolution and the creature target is there. This makes it possible to have two Spurnmage Advocates both target the same two cards and end up destroying two creatures.').
card_ruling('spurnmage advocate', '2004-10-04', 'You can\'t activate this ability unless a single opponent has at least two cards in their graveyard to target.').

card_ruling('spurred wolverine', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('spurred wolverine', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('spy network', '2004-10-04', 'Only you get to look. You can\'t show them to others.').

card_ruling('squandered resources', '2004-10-04', 'Can sacrifice a land which can\'t produce mana, but you don\'t get any mana from this ability.').

card_ruling('squeaking pie grubfellows', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('squeaking pie grubfellows', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('squeaking pie grubfellows', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('squeaking pie grubfellows', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('squeaking pie grubfellows', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('squeaking pie grubfellows', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('squealing devil', '2006-05-01', 'For this creature to not be sacrificed, {B} must have been spent on the {1}{R} mana cost. It doesn\'t matter if {B} is spent on {X}.').
card_ruling('squealing devil', '2006-05-01', 'You choose the value of X as Squealing Devil\'s ability resolves. X can be 0.').
card_ruling('squealing devil', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('squee, goblin nabob', '2013-06-07', 'Squee\'s ability triggers only if it\'s in your graveyard as your upkeep begins.').

card_ruling('squelch', '2004-12-01', 'Squelch can\'t target a mana ability because mana abilities don\'t use the stack.').
card_ruling('squelch', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('squelching leeches', '2014-04-26', 'Squelching Leeches’s power and toughness will change as the number of Swamps you control changes. Its ability counts all lands you control with the subtype Swamp, not just those named Swamp.').
card_ruling('squelching leeches', '2014-04-26', 'The ability that defines Squelching Leeches’s power and toughness works in all zones, not just the battlefield.').

card_ruling('squirrel nest', '2005-08-01', 'The land receives this ability and not the actual text. Text-changing effects can be used on this Aura, but would not change this effect if cast on the land.').

card_ruling('squirrel wrangler', '2004-10-04', 'Only affects Squirrels on the battlefield when the ability resolves.').

card_ruling('stabbing pain', '2010-08-15', 'You may target any creature with Stabbing Pain. It\'s okay if it\'s already tapped.').
card_ruling('stabbing pain', '2010-08-15', 'If Stabbing Pain targets an untapped creature with 1 toughness, that creature gets -1/-1, then it becomes tapped, then (after Stabbing Pain resolves) it\'s put into the graveyard as a state-based action. Similarly, if Stabbing Pain targets an untapped creature with 1 less than lethal damage marked on it (such as a 3/3 creature with 2 damage marked on it), that creature gets -1/-1, then it becomes tapped, then (after Stabbing Pain resolves) it\'s destroyed as a state-based action. In each case, any abilities that trigger when that creature becomes tapped will trigger.').

card_ruling('stabilizer', '2004-10-04', 'This affects both Cycling and Landcycling.').

card_ruling('staff of the ages', '2004-10-04', 'It does not remove Landwalk from creatures. It just makes creatures with landwalk blockable as if they did not have the ability.').

card_ruling('staff of the death magus', '2014-07-18', 'Each of these artifacts cares about any land with the appropriate basic land type. For example, Staff of the Sun Magus’s ability triggers when any land with the subtype Plains enters the battlefield under your control, not just lands named Plains.').
card_ruling('staff of the death magus', '2014-07-18', 'Most nonbasic lands don’t have basic land types, even if they produce colored mana. For example, Caves of Koilos is neither a Plains nor a Swamp. Having one enter the battlefield under your control won’t cause Staff of the Sun Magus’s ability to trigger.').
card_ruling('staff of the death magus', '2014-07-18', 'If you cast a copy of a card that’s a certain color, the appropriate Staff’s ability will trigger. However, if you copy a spell on the stack without casting that copy, it will not.').

card_ruling('staff of the flame magus', '2014-07-18', 'Each of these artifacts cares about any land with the appropriate basic land type. For example, Staff of the Sun Magus’s ability triggers when any land with the subtype Plains enters the battlefield under your control, not just lands named Plains.').
card_ruling('staff of the flame magus', '2014-07-18', 'Most nonbasic lands don’t have basic land types, even if they produce colored mana. For example, Caves of Koilos is neither a Plains nor a Swamp. Having one enter the battlefield under your control won’t cause Staff of the Sun Magus’s ability to trigger.').
card_ruling('staff of the flame magus', '2014-07-18', 'If you cast a copy of a card that’s a certain color, the appropriate Staff’s ability will trigger. However, if you copy a spell on the stack without casting that copy, it will not.').

card_ruling('staff of the mind magus', '2014-07-18', 'Each of these artifacts cares about any land with the appropriate basic land type. For example, Staff of the Sun Magus’s ability triggers when any land with the subtype Plains enters the battlefield under your control, not just lands named Plains.').
card_ruling('staff of the mind magus', '2014-07-18', 'Most nonbasic lands don’t have basic land types, even if they produce colored mana. For example, Caves of Koilos is neither a Plains nor a Swamp. Having one enter the battlefield under your control won’t cause Staff of the Sun Magus’s ability to trigger.').
card_ruling('staff of the mind magus', '2014-07-18', 'If you cast a copy of a card that’s a certain color, the appropriate Staff’s ability will trigger. However, if you copy a spell on the stack without casting that copy, it will not.').

card_ruling('staff of the sun magus', '2014-07-18', 'Each of these artifacts cares about any land with the appropriate basic land type. For example, Staff of the Sun Magus’s ability triggers when any land with the subtype Plains enters the battlefield under your control, not just lands named Plains.').
card_ruling('staff of the sun magus', '2014-07-18', 'Most nonbasic lands don’t have basic land types, even if they produce colored mana. For example, Caves of Koilos is neither a Plains nor a Swamp. Having one enter the battlefield under your control won’t cause Staff of the Sun Magus’s ability to trigger.').
card_ruling('staff of the sun magus', '2014-07-18', 'If you cast a copy of a card that’s a certain color, the appropriate Staff’s ability will trigger. However, if you copy a spell on the stack without casting that copy, it will not.').

card_ruling('staff of the wild magus', '2014-07-18', 'Each of these artifacts cares about any land with the appropriate basic land type. For example, Staff of the Sun Magus’s ability triggers when any land with the subtype Plains enters the battlefield under your control, not just lands named Plains.').
card_ruling('staff of the wild magus', '2014-07-18', 'Most nonbasic lands don’t have basic land types, even if they produce colored mana. For example, Caves of Koilos is neither a Plains nor a Swamp. Having one enter the battlefield under your control won’t cause Staff of the Sun Magus’s ability to trigger.').
card_ruling('staff of the wild magus', '2014-07-18', 'If you cast a copy of a card that’s a certain color, the appropriate Staff’s ability will trigger. However, if you copy a spell on the stack without casting that copy, it will not.').

card_ruling('stag beetle', '2004-10-04', 'The value of X is calculated once, as Stag Beetle is entering the battlefield.').

card_ruling('staggershock', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('staggershock', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('staggershock', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('staggershock', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('staggershock', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('staggershock', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('staggershock', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('staggershock', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').

card_ruling('stain the mind', '2014-07-18', 'You can leave any cards with that name in the zone they’re in. You don’t have to exile them.').
card_ruling('stain the mind', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('stain the mind', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('stain the mind', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('stain the mind', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('stain the mind', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('stain the mind', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('stain the mind', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('stalking stones', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('stalking stones', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('stalking yeti', '2006-07-15', 'If either Stalking Yeti or the targeted creature is no longer on the battlefield when the enters-the-battlefield triggered ability resolves, the ability does nothing.').

card_ruling('stalwart aven', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('stalwart aven', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('stalwart aven', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('stalwart shield-bearers', '2010-06-15', 'Stalwart Shield-Bearers continually checks which creatures you control have defender and gives the bonus only to them. If an effect causes a creature with defender to lose defender (as Shoal Serpent does), Stalwart Shield-Bearers stops giving it the bonus for as long as it doesn\'t have defender. On the other hand, if a spell or ability causes a creature to be able to attack as though it didn\'t have defender (as Warmonger\'s Chariot does), Stalwart Shield-Bearers continues to give it the bonus because it never actually loses the defender ability.').

card_ruling('stampeding elk herd', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('stampeding elk herd', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('stampeding elk herd', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('stampeding serow', '2014-02-01', 'The ability that returns a creature doesn\'t target anything. You can return a creature with shroud or protection from green, for example. You don\'t decide which creature to return until the ability resolves. If you control no other green creatures, you must return Stampeding Serow itself.').
card_ruling('stampeding serow', '2014-02-01', 'You choose a creature and return it all during resolution of the triggered ability. This means that if you control two of these, there is no way to end up returning only one creature.').

card_ruling('stampeding wildebeests', '2004-10-04', 'The ability that returns a creature doesn\'t target anything. You can return a creature with shroud or protection from green, for example. You don\'t decide which creature to return until the ability resolves. If you control no other green creatures, you must return Stampeding Wildebeests itself.').
card_ruling('stampeding wildebeests', '2004-10-04', 'You choose a creature and return it all during resolution of the triggered ability. This means that if you have two Wildebeests, there is no way to end up returning only one creature.').

card_ruling('standardize', '2004-10-04', 'This spell only affects creatures on the battlefield when it resolves.').

card_ruling('standstill', '2004-10-04', 'It only works once. If someone casts another spell after it triggers, but before that trigger resolves, then it will trigger again. The first time one of these triggers resolves, it will be sacrificed for the full effect. Any additional triggers on the stack will do nothing when they resolve because you will be unable to sacrifice it additional times.').

card_ruling('stangg', '2009-10-01', 'Stangg\'s delayed triggered abilities track only the token created by that particular Stangg.').
card_ruling('stangg', '2009-10-01', 'If Stangg leaves the battlefield before its ability has resolved, that ability will still create a Stangg Twin token, but its delayed triggered abilities will never have a chance to trigger. That token will remain on the battlefield indefinitely (until it\'s dealt lethal damage, or anything else causes it to leave the battlefield).').

card_ruling('starfield of nyx', '2015-06-22', 'If the first ability returns an Aura card to the battlefield, you choose what that Aura will enchant as it enters the battlefield. If the Aura can’t legally enchant anything, it stays in the graveyard.').
card_ruling('starfield of nyx', '2015-06-22', 'Note that if an Aura is returned to the battlefield this way, whatever the Aura enchants isn’t a target of Starfield of Nyx’s ability, nor is it a target of the Aura card itself. You could put an Aura onto the battlefield this way enchanting a creature with hexproof controlled by an opponent, for example.').
card_ruling('starfield of nyx', '2015-06-22', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn’t matter how long the permanent has been a creature.').

card_ruling('starstorm', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('stasis', '2004-10-04', 'Since there is no untap step, Phasing in/out won\'t happen.').
card_ruling('stasis', '2004-10-04', 'Does not prevent cards from being untapped outside the untap step.').

card_ruling('stasis cocoon', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('stasis snare', '2015-08-25', 'If Stasis Snare leaves the battlefield before its triggered ability resolves, the target creature won’t be exiled.').
card_ruling('stasis snare', '2015-08-25', 'Auras attached to the exiled creature will be put into their owners’ graveyards. Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('stasis snare', '2015-08-25', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('stasis snare', '2015-08-25', 'The exiled card returns to the battlefield immediately after Stasis Snare leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('stasis snare', '2015-08-25', 'In a multiplayer game, if Stasis Snare’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('static orb', '2004-10-04', 'If there are two of these on the battlefield, they do not add together. The result is that only two permanents can be untapped.').

card_ruling('statute of denial', '2014-07-18', 'Whether you control a blue creature is checked only when Statute of Denial resolves. If you do, drawing and discarding a card is mandatory. You can’t choose to not do it.').

card_ruling('staunch-hearted warrior', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('staunch-hearted warrior', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('staunch-hearted warrior', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('stave off', '2011-09-22', 'You choose the target as part of casting the spell. You choose what color the target gains protection from when the spell resolves.').

card_ruling('steady progress', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('steady progress', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('steady progress', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('steady progress', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('steady progress', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').
card_ruling('steady progress', '2011-01-01', 'This spell has no targets. You can cast it even if there are no players or permanents with counters on them. If you do, you\'ll simply draw a card as it resolves.').

card_ruling('steal artifact', '2004-10-04', 'Can be used on artifact creatures.').

card_ruling('steal enchantment', '2004-10-04', 'When a player takes control of an enchantment, they do not get to change anything about the enchantment (such as what creature it is on, what choices it has or anything) at that time. They just become its controller.').

card_ruling('steam augury', '2013-09-15', 'Steam Augury doesn’t target any opponent. In a multiplayer game, you choose the opponent as the spell resolves.').
card_ruling('steam augury', '2013-09-15', 'If one of the piles is empty, the opponent will choose whether to put the cards from the other pile into your hand or into your graveyard.').

card_ruling('steam catapult', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('steam vents', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('steam vents', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('steam vents', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('steam vines', '2004-10-04', 'If this card leaves the battlefield after it triggers but before it resolves, then the trigger will deal damage and destroy the land, but it will not bring this card back to the battlefield.').
card_ruling('steam vines', '2004-10-04', 'Moving this card to another land does not target the land.').
card_ruling('steam vines', '2005-08-01', 'If there are no other lands to move it to, this card goes to the graveyard as would any Aura that lost the thing it enchants.').

card_ruling('steamflogger boss', '2007-05-01', 'Contraption is a new artifact type. There are currently no artifacts with this type. And there\'s no current game meaning of \"assemble.\"').

card_ruling('steel golem', '2004-10-04', 'You can put creatures onto the battlefield by means other than casting them.').
card_ruling('steel golem', '2004-10-04', 'Yes, this only affects you.').
card_ruling('steel golem', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('steel hellkite', '2011-01-01', 'Steel Hellkite\'s last ability destroys only nonland permanents whose converted mana cost is exactly equal to X, and only those controlled by players who have been dealt combat damage by Steel Hellkite this turn.').
card_ruling('steel hellkite', '2011-01-01', 'It doesn\'t matter who controlled those permanents at the time Steel Hellkite dealt combat damage, or if those permanents were even on the battlefield at that time.').
card_ruling('steel hellkite', '2011-01-01', 'You may activate the last ability even if Steel Hellkite hasn\'t dealt combat damage to any players that turn. If you do, the ability won\'t do anything.').
card_ruling('steel hellkite', '2011-01-01', 'If Steel Hellkite\'s third ability is activated with X equal to 0, it will destroy each nonland permanent with converted mana cost 0 the appropriate players control.').
card_ruling('steel hellkite', '2011-01-01', 'The converted mana cost of a permanent is determined solely by the mana symbols printed in its upper right corner, unless it\'s copying something else (see below). The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('steel hellkite', '2011-01-01', 'If a permanent is copying something else, its converted mana cost is the converted mana cost of whatever it\'s copying.').
card_ruling('steel hellkite', '2011-01-01', 'In all cases, ignore any alternative costs or additional costs (such as kicker) paid when the permanent was cast.').
card_ruling('steel hellkite', '2011-01-01', 'If the mana cost of a permanent includes {X}, X is considered to be 0.').
card_ruling('steel hellkite', '2011-01-01', 'If a nonland permanent has no mana symbols in its upper right corner (because it\'s a token that\'s not copying of something else, for example), its converted mana cost is 0.').

card_ruling('steel leaf paladin', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('steel of the godhead', '2008-05-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('steel overseer', '2010-08-15', 'Steel Overseer is affected by its own ability (assuming that, by the time the ability resolves, you still control Steel Overseer and its types haven\'t changed). You\'ll put a +1/+1 counter on it, as well as on each other artifact creature you control.').

card_ruling('steel sabotage', '2011-06-01', 'An artifact spell exists only on the stack. An artifact exists only on the battlefield.').

card_ruling('steelclad serpent', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('steelclad serpent', '2008-10-01', 'If you control two Steelclad Serpents, they\'ll enable each other to attack.').

card_ruling('steelform sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('steelform sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('stenchskipper', '2008-04-01', 'This ability checks whether you control any Goblins twice each turn: once when the ability would trigger, and once when it resolves. If you control any Goblins at either time, you won\'t sacrifice Stenchskipper. (If you control any Goblins at the time the ability would trigger, it doesn\'t trigger at all.)').

card_ruling('stensia', '2012-06-01', 'Each creature can have only one +1/+1 counter put on it this way per turn.').
card_ruling('stensia', '2012-06-01', 'Unlike the Vampires in the _Innistrad_(TM) block, Stensia\'s ability triggers whenever a creature deals any damage, not just combat damage.').

card_ruling('stensia bloodhall', '2011-09-22', 'Like other lands, Stensia Bloodhall is colorless. The damage it deals is from a colorless source, even though activating its ability requires colored mana.').

card_ruling('steppe lynx', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('steppe lynx', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('sterling grove', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('stern marshal', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('stern proctor', '2004-10-04', 'If there is an artifact or enchantment on the battlefield (even your own) when this card enters the battlefield, you must return one to owner\'s hand. If not, then simply ignore the \"enters the battlefield\" ability.').
card_ruling('stern proctor', '2004-10-04', 'You can cast this card if there are no artifacts or enchantments on the battlefield.').

card_ruling('stifle', '2004-10-04', 'An activated ability has a \"Cost: Effect\" format. Look for the colon. A triggered ability starts with \"when\", \"whenever\", or \"at\".').
card_ruling('stifle', '2004-10-04', 'Turn-based actions and special actions like the normal card draw, combat damage, or turning a face-down creature face up can\'t be targeted.').
card_ruling('stifle', '2004-10-04', 'It can target delayed triggered abilities. For example, a card that says \"Whenever a player cycles a card, you may exile target creature. If you do, return that creature to the battlefield at the beginning of the end step.\" triggers when a player cycles a card and creates a delayed trigger that happens at the beginning of the end step. You can choose to target the \"at the beginning of the end step\" trigger when it is placed on the stack at end of turn.').

card_ruling('stigma lasher', '2008-08-01', 'Once the last ability resolves, it will be in effect for the rest of the game. It doesn\'t matter if Stigma Lasher remains on the battlefield or not.').
card_ruling('stigma lasher', '2008-08-01', 'If a player has been dealt damage by Stigma Lasher, the following things are true: -- Spells and abilities that would normally cause that player to gain life still resolve, but the life-gain part simply has no effect. -- If a cost includes having that player gain life (like Invigorate\'s alternative cost does), that cost can\'t be paid. -- Effects that would replace having that player gain life with some other effect won\'t be able to do anything because it\'s impossible for that player to gain life. -- Effects that replace an event with having that player gain life (like Words of Worship\'s effect does) will end up replacing the event with nothing. -- If an effect says to set that player\'s life total to a certain number, and that number is higher than the player\'s current life total, that part of the effect won\'t do anything. (If the number is lower than that player\'s current life total, the effect will work as normal.)').

card_ruling('still life', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('stingerfling spider', '2011-09-22', 'You choose the target creature with flying when the ability goes on the stack. When it resolves, you choose whether or not to destroy the creature.').

card_ruling('stinging licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('stinging licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('stinkdrinker bandit', '2008-04-01', 'This ability triggers after blockers are declared during the declare blockers step. Each attacking Rogue you control gets +2/+1 if no creatures were assigned to block it. That creature will get the bonus even if an effect causes it to become blocked.').
card_ruling('stinkdrinker bandit', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('stinkweed imp', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('stinkweed imp', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('stinkweed imp', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').
card_ruling('stinkweed imp', '2013-06-07', 'Notably, Stinkweed Imp\'s ability isn\'t deathtouch. It\'s a triggered ability that triggers only on combat damage.').

card_ruling('stir the pride', '2013-06-07', 'Only creatures you control when Stir the Pride resolves will get the bonuses.').
card_ruling('stir the pride', '2013-06-07', 'Notably, the ability granted by the second mode isn\'t lifelink. It\'s a triggered ability that triggers whenever the creature with that ability deals damage. If that creature also has lifelink, you\'ll gain life as a result of that damage and also when the triggered ability resolves. Multiple instances of this ability trigger separately.').

card_ruling('stirring wildwood', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('stirring wildwood', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').

card_ruling('stitch together', '2004-10-04', 'Checks the number of cards in your graveyard when it starts resolving. You may drop below seven cards during resolution, but that won\'t stop it from resolving.').

card_ruling('stitcher geralf', '2014-11-07', '“Exiled this way” refers to the exile instruction that’s part of the ability’s effect. If the cards each player would put into his or her graveyard are exiled because of a replacement effect, those cards won’t be in the graveyards to be exiled by the next part of the ability. In this case, the value of X will be 0. You’ll put a 0/0 token onto the battlefield, then that token will die and subsequently cease to exist (unless something else raises its toughness above 0).').

card_ruling('stitcher\'s apprentice', '2011-09-22', 'You can\'t do anything in between the Homunculus token being created and having to sacrifice a creature.').
card_ruling('stitcher\'s apprentice', '2011-09-22', 'If you control no creatures when the ability resolves, you\'ll put the Homunculus token onto the battlefield and immediately sacrifice it.').

card_ruling('stoic angel', '2008-10-01', 'If multiple Stoic Angels are on the battlefield, the abilities are redundant. Each player will still be able to untap no more than one creature during his or her untap step.').

card_ruling('stoic rebuttal', '2011-01-01', 'Stoic Rebuttal\'s metalcraft ability functions while Stoic Rebuttal is on the stack.').
card_ruling('stoic rebuttal', '2011-01-01', 'For the purpose of determining whether the cost reduction applies, the number of artifacts you control is checked as you cast Stoic Rebuttal, before your last chance to activate mana abilities to pay for it. For example, if you control three artifacts, you could determine that Stoic Rebuttal costs {U}{U} to cast, then sacrifice one of those artifacts to activate a mana ability.').

card_ruling('stoke the flames', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('stoke the flames', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('stoke the flames', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('stoke the flames', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('stoke the flames', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('stoke the flames', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('stoke the flames', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('stolen goods', '2012-05-01', 'You cast the card from exile. Each land card exiled this way will remain exiled.').
card_ruling('stolen goods', '2012-05-01', 'If you can\'t cast the nonland card (perhaps because there are no legal targets available) or if you choose not to cast it, it will remain exiled.').
card_ruling('stolen goods', '2012-05-01', 'You must follow the normal timing restrictions of the exiled card.').
card_ruling('stolen goods', '2012-05-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('stolen goods', '2012-05-01', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('stolen goods', '2012-05-01', 'If there are no nonland cards left in that library, the opponent will exile all cards from his or her library, and Stolen Goods will finish resolving.').

card_ruling('stolen grain', '2009-10-01', 'If the targeted opponent is an illegal target by the time Stolen Grain resolves, the ability is countered. You won\'t gain life.').

card_ruling('stolen identity', '2013-01-24', 'If the token you create is a copy of a creature, you can exile Stolen Identity encoded on that token.').
card_ruling('stolen identity', '2013-01-24', 'The token copies exactly what was printed on the original artifact or creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn\'t copy whether it is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('stolen identity', '2013-01-24', 'If the copied permanent has {X} in its mana cost, X is considered to be zero.').
card_ruling('stolen identity', '2013-01-24', 'If the copied permanent is copying something else (for example, if the copied creature is a Clone), then the token enters the battlefield as whatever that creature copied.').
card_ruling('stolen identity', '2013-01-24', 'If the copied permanent is a token, the token that\'s created copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('stolen identity', '2013-01-24', 'Any enters-the-battlefield abilities of the copied permanent will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the chosen permanent will also work.').
card_ruling('stolen identity', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('stolen identity', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('stolen identity', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('stolen identity', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('stolen identity', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('stolen identity', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('stolen identity', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('stolen identity', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('stolen identity', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('stolen identity', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('stolen identity', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('stomp and howl', '2006-05-01', 'This spell can\'t be cast unless there are both an artifact and an enchantment on the battlefield to target. If a permanent is both an artifact and an enchantment, it can be both targets.').

card_ruling('stomping ground', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('stomping ground', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('stomping ground', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('stomping slabs', '2008-04-01', 'If a card named Stomping Slabs wasn\'t revealed this way, Stomping Slabs deals no damage to its target.').

card_ruling('stone calendar', '2004-10-04', 'Does not change the mana cost of the spell, it just reduces what you pay for it.').
card_ruling('stone calendar', '2004-10-04', 'You may choose not to apply Stone Calendar\'s cost reduction effect.').

card_ruling('stone catapult', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('stone giant', '2008-08-01', 'If the Giant\'s power and/or toughness change so that its toughness is less than its power, you can have the ability target the Giant itself.').
card_ruling('stone giant', '2009-10-01', 'To work as an evasion ability, an attacking creature must already have flying when the declare blockers step begins. Once a creature has become blocked, giving it flying won\'t change that.').
card_ruling('stone giant', '2009-10-01', 'If Stone Giant\'s ability is activated during a turn\'s end step, the delayed triggered ability won\'t trigger until the beginning of the next turn\'s end step. The targeted creature will be destroyed at that time.').
card_ruling('stone giant', '2009-10-01', 'When the delayed triggered ability resolves, the targeted creature is destroyed, even if it\'s no longer a creature, no longer under your control, or no longer has toughness less than Stone Giant\'s power at that time.').

card_ruling('stone idol trap', '2010-03-01', 'This Trap doesn\'t work the same way as other Traps. Rather than having an optional alternative cost, Stone Idol Trap has a mandatory cost-reduction ability.').
card_ruling('stone idol trap', '2010-03-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('stone idol trap', '2010-03-01', 'If you cast Stone Idol Trap during your opponent\'s declare attackers step, the Construct token you put onto the battlefield will be able to block that turn.').
card_ruling('stone idol trap', '2010-03-01', 'The Construct token isn\'t exiled until the beginning of *your* end step. If you cast Stone Idol Trap during another player\'s turn, the token remains on the battlefield after that player\'s turn ends. During your next turn, it will no longer have \"summoning sickness\" and you\'ll be able to attack with it. Then, at the beginning of that turn\'s end step, it\'ll be exiled.').
card_ruling('stone idol trap', '2010-03-01', 'The cost-reduction ability doesn\'t care who controls the attacking creatures. If you like, you may cast Stone Idol Trap during your own combat phase, in which case it will count your attacking creatures. If you do, however, the token you put onto the battlefield will be unable to attack that combat and will be exiled later that turn.').

card_ruling('stone spirit', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) can block this because they don\'t actually have Flying.').

card_ruling('stonecloaker', '2007-02-01', 'Both triggered abilities trigger when Stonecloaker enters the battlefield. They can be put on the stack in either order.').
card_ruling('stonecloaker', '2007-02-01', 'You may return this creature itself to its owner\'s hand. If you control no other creatures, you must return it.').
card_ruling('stonecloaker', '2007-02-01', 'The ability doesn\'t target what you return. You don\'t choose what to return until the ability resolves. No one can respond to the choice.').

card_ruling('stonefare crocodile', '2012-10-01', 'Multiple instances of lifelink are redundant. Activating Stonefare Crocodile\'s ability more than once during a single turn won\'t cause you to gain more life.').

card_ruling('stoneforge mystic', '2010-03-01', 'When Stoneforge Mystic\'s second ability resolves, you may put any Equipment card from your hand onto the battlefield, not just the one you searched for with its first ability. The Equipment is put onto the battlefield unattached.').

card_ruling('stonefury', '2015-08-25', 'Count the number of lands you control as Stonefury resolves to determine how much damage is dealt.').

card_ruling('stonehewer giant', '2008-04-01', 'This ability doesn\'t target a creature. You may attach the Equipment to a creature that has shroud, for example. However, the creature must be able to be legally equipped by the Equipment. You can\'t attach the Equipment to a creature that has protection from artifacts, for example.').
card_ruling('stonehewer giant', '2008-04-01', 'If there is no legal creature for you to attach the Equipment to, it remains on the battlefield unattached.').

card_ruling('stonehorn chanter', '2013-07-01', 'Multiple instances of vigilance and lifelink are redundant. There’s usually no reason to activate Stonehorn Chanter’s ability more than once in a single turn.').

card_ruling('stonehorn dignitary', '2011-09-22', 'If more than one Stonehorn Dignitary enters the battlefield during the same turn, and you target the same opponent with each ability, that opponent will skip that many combat phases (over multiple turns if necessary).').
card_ruling('stonehorn dignitary', '2011-09-22', 'In a Two-Headed Giant game, the targeted opponent\'s team skips its next combat phase.').

card_ruling('stoneshaker shaman', '2005-10-01', 'If the player doesn\'t have an untapped land as Stoneshaker Shaman\'s ability resolves, that player does nothing.').

card_ruling('stoneshock giant', '2013-09-15', 'If blockers have already been declared when Stoneshock Giant’s last ability resolves, those blocks won’t change or become undone.').
card_ruling('stoneshock giant', '2013-09-15', 'Your opponents can’t assign any creature without flying to block that turn, even if that creature had flying or wasn’t on the battlefield under one of your opponent’s control when Stoneshock Giant’s ability resolved.').
card_ruling('stoneshock giant', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('stoneshock giant', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('stoneshock giant', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('stonewood invocation', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('stonewood invocation', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('stonewood invocation', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('stonewood invocation', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('stonewood invocation', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('stonewood invocation', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('stony silence', '2011-09-22', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords, such as equip, are activated abilities; they have colons in their reminder text.').
card_ruling('stony silence', '2011-09-22', 'Mana abilities of artifacts also can\'t be activated.').
card_ruling('stony silence', '2011-09-22', 'Triggered abilities and static abilities of artifacts are unaffected. Also, activated abilities (such as cycling or unearth) of artifact cards not on the battlefield are unaffected.').

card_ruling('stonybrook banneret', '2008-04-01', 'A spell you cast that\'s both creature types costs {1} less to cast, not {2} less.').
card_ruling('stonybrook banneret', '2008-04-01', 'The effect reduces the total cost of the spell, regardless of whether you chose to pay additional or alternative costs. For example, if you cast a Rogue spell by paying its prowl cost, Frogtosser Banneret causes that spell to cost {1} less.').

card_ruling('storage matrix', '2004-10-04', 'This card\'s ability does not override effects which prevent a permanent from untapping.').
card_ruling('storage matrix', '2005-08-01', 'Permanents with multiple types untap if any of their types is chosen. For example, an artifact creature untaps if either artifact or creature is chosen.').
card_ruling('storage matrix', '2005-08-01', 'If another effect limits the number of permanents that untap, that number combines with Storage Matrix\'s effect. For example, Imi Statue says \"players can\'t untap more than one artifact during their untap steps.\" If both Imi Statue and an untapped Storage Matrix are on the battlefield, and you choose \"artifact,\" only one artifact untaps.').

card_ruling('storm cauldron', '2004-10-04', 'If a land is tapped for mana and sacrificed all in one action, it goes to the graveyard before the Cauldron can return it to the player\'s hand.').
card_ruling('storm cauldron', '2004-10-04', 'If a land is tapped for mana, it is returned to its owner\'s hand as a triggered ability.').
card_ruling('storm cauldron', '2004-10-04', 'Having multiples of these on the battlefield means you can play an additional land for each one.').

card_ruling('storm elemental', '2008-10-01', 'Even though the two activated abilities have the same cost, they\'re separate. You choose which one you\'re activating before you exile the top card of your library (and see what it is).').
card_ruling('storm elemental', '2008-10-01', 'If you activate Storm Elemental\'s third ability and the exiled card is not a snow land, the ability has no effect.').

card_ruling('storm entity', '2007-05-01', 'The number of other spells cast this turn is determined as Storm Entity enters the battlefield. It will count spells that were cast while Storm Entity was on the stack.').

card_ruling('storm world', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('storm world', '2009-10-01', 'If the player whose turn it is has four or more cards in his or her hand as Storm World\'s ability resolves, the ability just won\'t do anything that turn.').

card_ruling('stormbind', '2004-10-04', 'Since the discard is a cost, it can\'t be used with Library of Leng.').

card_ruling('stormbreath dragon', '2013-09-15', 'For each opponent, count the number of cards in that player’s hand when the last ability resolves to determine how much damage Stormbreath Dragon does to him or her.').
card_ruling('stormbreath dragon', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('stormbreath dragon', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('stormbreath dragon', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('stormcaller of keranos', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('stormcaller of keranos', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('stormcaller of keranos', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('stormcaller of keranos', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('stormcaller\'s boon', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('stormcaller\'s boon', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('stormcrag elemental', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('stormcrag elemental', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('stormcrag elemental', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('stormfront riders', '2007-02-01', 'If the first ability of Stormfront Riders causes you to return two creatures to your hand, its second ability will trigger twice, even if Stormfront Riders is one of the returned creatures.').
card_ruling('stormfront riders', '2007-02-01', 'The second ability of Stormfront Riders triggers when a creature token you own is returned to your hand.').
card_ruling('stormfront riders', '2007-02-01', 'If Stormfront Riders and multiple creatures are returned to your hand at the same time, such as with Evacuation or Upheaval, then Stormfront Riders will trigger once for each creature returned, including itself.').
card_ruling('stormfront riders', '2007-02-01', 'You may return this creature itself to its owner\'s hand. If you control no other creatures, you must return it.').
card_ruling('stormfront riders', '2007-02-01', 'The ability doesn\'t target what you return. You don\'t choose what to return until the ability resolves. No one can respond to the choice.').
card_ruling('stormfront riders', '2007-02-01', 'If you are instructed to return more creatures than you control, you must return all the creatures you control to their owner\'s hand.').
card_ruling('stormfront riders', '2009-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').

card_ruling('stormrider rig', '2015-02-25', 'If the creature that entered the battlefield is no longer on the battlefield as Stormrider Rig’s second ability resolves, Stormrider Rig doesn’t move. This is also true if that creature can’t legally be equipped by Stormrider Rig.').

card_ruling('stormscale anarch', '2006-05-01', 'You can\'t discard a card at random, see what it is, and then decide whether or not to pay the mana. Mana abilities must be activated before costs are paid. At the time you pay the cost of discarding a card, you either have enough mana in your mana pool to pay the {2}{R}, in which case you must continue no matter what you discarded, or you don\'t have enough mana in your mana pool to pay the {2}{R}, in which case you must back up no matter what you discarded.').

card_ruling('stormscape familiar', '2004-10-04', 'If a spell is both white and black, you pay {1} less, not {2} less.').
card_ruling('stormscape familiar', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('stormscape familiar', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('stormscape familiar', '2004-10-04', 'The effect is cumulative.').
card_ruling('stormscape familiar', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('stormscape familiar', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('stormscape familiar', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('stormsurge kraken', '2014-11-07', 'Lieutenant abilities apply only if your commander is on the battlefield and under your control.').
card_ruling('stormsurge kraken', '2014-11-07', 'Lieutenant abilities refer only to whether you control your commander, not any other player’s commander.').
card_ruling('stormsurge kraken', '2014-11-07', 'If you gain control of a creature with a lieutenant ability owned by another player, that ability will check to see if you control your commander and will apply if you do. It won’t check whether its owner controls his or her commander.').
card_ruling('stormsurge kraken', '2014-11-07', 'If you lose control of your commander, lieutenant abilities of creatures you control will immediately stop applying. If this causes a creature’s toughness to become less than or equal to the amount of damage marked on it, the creature will be destroyed.').
card_ruling('stormsurge kraken', '2014-11-07', 'If a triggered ability granted by a lieutenant ability triggers, and in response to that trigger you lose control of your commander (causing the lieutenant to lose that ability), that triggered ability will still resolve.').

card_ruling('stormtide leviathan', '2014-07-18', '').
card_ruling('stormtide leviathan', '2014-07-18', 'Stormtide Leviathan’s second ability causes each land on the battlefield to have the land type Island. Each land thus has the ability “{T}: Add {U} to your mana pool.” Nothing else changes about those lands, including their names, other subtypes, other abilities, and whether they’re legendary or basic.').
card_ruling('stormtide leviathan', '2014-07-18', 'If Stormtide Leviathan loses its abilities, all lands on the battlefield (including those that enter the battlefield later on) will still be Islands in addition to their other types and will still be able to tap to produce {U}. The way continuous effects work, Stormtide Leviathan’s type-changing ability is applied before the effect that removes that ability is applied.').
card_ruling('stormtide leviathan', '2014-07-18', 'Stormtide Leviathan’s third ability affects all creatures with neither flying nor islandwalk, regardless of who controls them. They can’t attack any player or planeswalker.').
card_ruling('stormtide leviathan', '2014-07-18', 'Islandwalk cares about lands with the land type Island, not necessarily lands named Island.').

card_ruling('stormwing dragon', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('stormwing dragon', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('stormwing dragon', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('strange inversion', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('strange inversion', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').
card_ruling('strange inversion', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('strange inversion', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('strange inversion', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('strange inversion', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('stranglehold', '2011-09-22', 'If your opponent controls a Stranglehold and an effect says \"You may search your library . . . If you do, shuffle your library,\" you can\'t choose to search, so you won\'t shuffle.').
card_ruling('stranglehold', '2011-09-22', 'If your opponent controls a Stranglehold and an effect says \"Search your library . . . Then shuffle your library,\" the search effect fails, but you will still have to shuffle.').
card_ruling('stranglehold', '2011-09-22', 'Since opponents can\'t search libraries, they won\'t be able to find any cards in a library. The effect applies to all opponents and all libraries, including your library. If a spell or ability\'s effect has other parts that don\'t depend on searching for or finding cards, they will still work normally.').
card_ruling('stranglehold', '2011-09-22', 'Effects that tell an opponent to reveal cards from a library or look at cards from the top of a library will still work. Only effects that use the word \"search\" will fail.').
card_ruling('stranglehold', '2011-09-22', 'An \"extra turn\" means a turn created by a spell or ability. It doesn\'t refer to a turn taken in a sanctioned tournament after the time limit for the round has expired.').
card_ruling('stranglehold', '2011-09-22', 'If an opponent\'s extra turn is created while Stranglehold is on the battlefield, but Stranglehold leaves the battlefield before that turn would begin, that opponent takes the extra turn as normal.').

card_ruling('strata scythe', '2011-01-01', 'The bonus counts all lands on the battlefield with the same name as the exiled card, not just lands you control.').

card_ruling('stratadon', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('stratadon', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('stratadon', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('strategic planning', '2009-10-01', 'If there are fewer than three cards in your library, you look at all of them, put one of them into your hand, and put the rest into your graveyard.').

card_ruling('stratus dancer', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('stratus dancer', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('stratus dancer', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('straw golem', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('stream of consciousness', '2005-02-01', 'Stream of Consciousness can target from zero to four cards.').
card_ruling('stream of consciousness', '2005-02-01', 'Stream of Consciousness targets the player as well as the cards. The shuffle occurs even if zero cards are targeted.').

card_ruling('street savvy', '2006-05-01', '\"Landwalk abilities\" are abilities such as \"forestwalk\" or \"legendary landwalk\" that have \"walk\" in the name.').

card_ruling('street spasm', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('street spasm', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('street spasm', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('street spasm', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('street spasm', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('street spasm', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('street spasm', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('street sweeper', '2012-10-01', 'Street Sweeper\'s ability is mandatory, but you can choose a land with no Auras attached to it as the target.').

card_ruling('street wraith', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('strength from the fallen', '2014-04-26', 'The value of X is determined when the ability resolves.').
card_ruling('strength from the fallen', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('strength from the fallen', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('strength from the fallen', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('strength in numbers', '2006-09-25', 'The bonus is determined when the spell resolves.').

card_ruling('strength of the tajuru', '2010-03-01', 'The number of targets you choose for Strength of the Tajuru is one more than the number of times it\'s kicked. First you declare how many times you\'re going to kick the spell (at the same time you declare the value of X), then you choose the targets accordingly, then you pay the costs. No player can respond between the time you declare how many times you\'ll kick the spell and the time you choose the targets.').
card_ruling('strength of the tajuru', '2010-03-01', 'Each target you choose must be different.').
card_ruling('strength of the tajuru', '2010-03-01', 'For example, if you want to put four +1/+1 counters on each of three different targets, that means X is 4 and you\'re kicking the spell twice. You\'ll pay a mana cost of {4}{G}{G}, plus a kicker cost of {1}, plus another kicker cost of {1}, for a total of {6}{G}{G}.').
card_ruling('strength of the tajuru', '2010-03-01', 'As long as any of its targets are legal at the time Strength of the Tajuru resolves, you\'ll put X +1/+1 counters on each of those legal targets.').

card_ruling('strength of unity', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('strength of unity', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('strength of unity', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('striking sliver', '2013-07-01', 'If Striking Sliver leaves the battlefield after Sliver creatures you control have dealt first-strike damage but before regular combat damage, those Slivers won’t deal regular combat damage (unless they have double strike for some reason).').
card_ruling('striking sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('striking sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('strionic resonator', '2013-07-01', 'Strionic Resonator targets a triggered ability that has triggered and is on the stack.').
card_ruling('strionic resonator', '2013-07-01', 'The source of the copy is the same as the source of the original ability.').
card_ruling('strionic resonator', '2013-07-01', 'If the triggered ability is modal (that is, if it says “Choose one —” or similar), the mode is copied and can’t be changed.').
card_ruling('strionic resonator', '2013-07-01', 'If the triggered ability divides damage or distributes counters among a number of targets (for example, the ability of Bogardan Hellkite), the division and number of targets can’t be changed. If you choose new targets, you must choose the same number of targets.').
card_ruling('strionic resonator', '2013-07-01', 'Any choices made when the triggered ability resolves won’t have been made yet when it’s copied. Any such choices will be made separately when the copy resolves. If the triggered ability asks you to pay a cost (for example, as the extort ability from the Return to Ravnica block does), you pay that cost for the copy.').
card_ruling('strionic resonator', '2013-07-01', 'If a triggered ability is linked to a second ability, copies of that triggered ability are also linked to that second ability. If the second ability refers to “the exiled card,” it refers to all cards exiled by the triggered ability and the copy. For example, if Exclusion Ritual’s enters-the-battlefield ability is copied and two nonland permanents are exiled, players can’t cast spells with the same name as either exiled card.').
card_ruling('strionic resonator', '2013-07-01', 'In some cases involving linked abilities, an ability requires information about “the exiled card.” When this happens, the ability gets multiple answers. If these answers are being used to determine the value of a variable, the sum is used. For example, if Elite Arcanist’s enters-the-battlefield ability is copied, two cards are exiled. The value of X in the activation cost of Elite Arcanist’s other ability is the sum of the two cards’ converted mana costs. As the ability resolves, you create copies of both cards and can cast none, one, or both of the copies in any order.').

card_ruling('stromgald spy', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('stromkirk captain', '2011-01-22', 'If a creature with first strike deals damage in the first combat damage step, but then loses first strike before regular combat damage, it won\'t deal combat damage a second time (unless it also has double strike).').

card_ruling('stronghold furnace', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('stronghold furnace', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('stronghold furnace', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('stronghold furnace', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('stronghold furnace', '2009-10-01', 'Stronghold Furnace\'s first ability interacts with all damage, not just combat damage. Notably, it doubles the damage from its own {C} ability.').
card_ruling('stronghold furnace', '2009-10-01', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage or the controller of the creature that would be dealt damage chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn.\" Suppose a spell would deal 5 damage to a player who has cast Mending Hands targeting him or herself. The player who would be dealt damage can either (a) prevent 4 damage first and then let Stronghold Furnace\'s effect double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').
card_ruling('stronghold furnace', '2009-10-01', 'Combat damage that a source would deal to a planeswalker is not doubled. However, if a source a player controls would deal noncombat damage to an opponent who controls a planeswalker, and that opponent chooses to apply Stronghold Furnace\'s replacement effect before applying the planeswalker redirection effect, the damage will be doubled before the source\'s controller chooses whether to deal it to a planeswalker that opponent controls. (If players always apply the planeswalker redirection effect first, Stronghold Furnace\'s effect will never double damage that a source would deal to a planeswalker.)').
card_ruling('stronghold furnace', '2009-10-01', 'If a spell or ability divides damage among multiple recipients (such as Pyrotechnics does), the damage is divided before Stronghold Furnace\'s effect doubles it. The same is true for combat damage.').

card_ruling('stronghold gambit', '2004-10-04', 'Creature cards which are revealed but are not the lowest cost, or any non-creature cards revealed remain in their owners\' hands and stop being revealed.').
card_ruling('stronghold gambit', '2004-10-04', 'You don\'t have to choose a creature card, but choosing a non creature does nothing except maybe bluff your opponent.').

card_ruling('stronghold rats', '2007-05-01', 'First the active player chooses a card to discard, then each other player in turn order chooses, then all cards are discarded simultaneously. Although the chosen cards will be indicated (so a player can\'t change his or her mind later), they\'ll remain face down until they\'re discarded. You won\'t know what your opponent is going to discard when making your choice.').

card_ruling('structural collapse', '2013-01-24', 'Structural Collapse targets only the player. The player can sacrifice an artifact or land with hexproof, for example.').
card_ruling('structural collapse', '2013-01-24', 'The target player needn\'t control an artifact or a land. Structural Collapse will still deal 2 damage to that player.').
card_ruling('structural collapse', '2013-01-24', 'If the player controls an artifact land, he or she can\'t choose to sacrifice that permanent for both the artifact and the land. If that\'s the only artifact the player controls, he or she must sacrifice another land. If it\'s the only land the player controls, he or she must sacrifice another artifact. If it\'s the only artifact and the only land the player controls, he or she sacrifices just that permanent.').

card_ruling('stubborn denial', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('stubborn denial', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('student of warfare', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('student of warfare', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('student of warfare', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('student of warfare', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('student of warfare', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('stun', '2004-10-04', 'Must be used before blockers are declared in order to affect blocking decisions. You can\'t wait to see what your opponent declares and then try to stop them.').
card_ruling('stun', '2004-10-04', 'You can use it after combat or even on a creature which can\'t possibly block this turn because it\'s that player\'s turn to attack, but it generally has no effect other than to let you draw a card.').

card_ruling('stupefying touch', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('sturdy hatchling', '2008-08-01', 'If you cast a spell that\'s both of the listed colors, both abilities will trigger. You\'ll remove a total of two -1/-1 counters from the Hatchling.').
card_ruling('sturdy hatchling', '2008-08-01', 'If there are no -1/-1 counters on it when the triggered ability resolves, the ability does nothing. There is no penalty for not being able to remove a counter.').

card_ruling('sturmgeist', '2011-09-22', 'The ability that defines Sturmgeist\'s power and toughness works in all zones, not just the battlefield.').

card_ruling('stymied hopes', '2013-09-15', 'If Stymied Hopes resolves, you’ll scry whether the controller of the target spell pays {1} or not.').
card_ruling('stymied hopes', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('stymied hopes', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('stymied hopes', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('stymied hopes', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('su-chi', '2009-10-01', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('sublime archangel', '2012-07-01', 'Whenever a creature you control attacks alone, count the number of instances of exalted among permanents you control. After those abilities resolve, that\'s how many times the creature will get +1/+1.').

card_ruling('subversion', '2004-10-04', 'This is loss of life, not damage. It can\'t be prevented.').

card_ruling('sudden death', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('sudden death', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('sudden death', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('sudden death', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('sudden death', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('sudden death', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('sudden demise', '2013-10-17', 'You choose the value for X as you cast Sudden Demise. You don’t choose the color until it resolves.').
card_ruling('sudden demise', '2013-10-17', 'Sudden Demise will deal damage to a multicolored creature if one of its colors is the chosen color.').

card_ruling('sudden disappearance', '2011-01-22', 'If Sudden Disappearance\'s delayed triggered ability is countered or otherwise removed from the stack after triggering, the exiled cards will remain in exile indefinitely. The delayed triggered ability will only trigger once.').

card_ruling('sudden reclamation', '2014-11-24', 'You choose which creature card and land card to return to your hand as Sudden Reclamation resolves. This doesn’t target either card.').
card_ruling('sudden reclamation', '2014-11-24', 'If there are no land cards in your graveyard, you still choose a creature card to return, and vice versa.').

card_ruling('sudden shock', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('sudden shock', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('sudden shock', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('sudden shock', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('sudden shock', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('sudden shock', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('sudden spoiling', '2006-09-25', 'Sudden Spoiling doesn\'t counter abilities that have already triggered or been activated. In particular, you can\'t cast this fast enough to stop a creature\'s \"At the beginning of your upkeep\" or \"When this creature enters the battlefield\" abilities from triggering.').
card_ruling('sudden spoiling', '2006-09-25', 'Sudden Spoiling affects only permanents that are creatures on the battlefield under the targeted player\'s control at the time Sudden Spoiling resolves. It won\'t affect creatures that enter the battlefield later or noncreature permanents that later become creatures.').
card_ruling('sudden spoiling', '2006-09-25', 'If a creature controlled by the targeted player gains an ability after Sudden Spoiling resolves, it will keep that ability.').
card_ruling('sudden spoiling', '2006-09-25', 'If a face-down creature is affected by Sudden Spoiling, it won\'t be able to be turned face up for its morph cost. If some other effect turns it face up, it will remain a 0/2 creature with no abilities until the turn ends. Its \"When this creature turns face up\" or \"As this creature turns face up\" abilities won\'t work.').
card_ruling('sudden spoiling', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('sudden spoiling', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('sudden spoiling', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('sudden spoiling', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('sudden spoiling', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('sudden spoiling', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('sudden storm', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('sudden storm', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('sudden storm', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('sudden storm', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('sudden storm', '2014-02-01', 'If a creature affected by Sudden Storm changes controllers before its old controller’s next untap step, Sudden Storm will prevent it from becoming untapped during its new controller’s next untap step.').
card_ruling('sudden storm', '2014-02-01', 'This spell can target tapped creatures. If a targeted creature is already tapped when the spell resolves, that creature remains tapped and doesn’t untap during its controller’s next untap step.').
card_ruling('sudden storm', '2014-02-01', 'If you chose two targets and one is an illegal target when Sudden Storm resolves, that creature won’t become tapped and it won’t be stopped from untapping during its controller’s next untap step. It won’t be affected by Sudden Storm in any way.').
card_ruling('sudden storm', '2014-02-01', 'If you choose zero targets, you’ll just scry 1 when the ability resolves. However, if you choose at least one target and all of Sudden Storm’s targets are illegal as it tries to resolve, the spell will be countered and none of its effects will happen. You won’t scry in that case.').

card_ruling('suffer the past', '2010-06-15', 'The life that\'s lost and the life that\'s gained are based on the number of cards that are actually exiled, not on the value of X (in case any of the targeted cards somehow leave the graveyard in response and thus can\'t be exiled this way).').
card_ruling('suffer the past', '2010-06-15', 'If the targeted player becomes an illegal target by the time Suffer the Past resolves but the targeted cards don\'t, you still exile the cards and gain life. The player won\'t be lose any life, though.').
card_ruling('suffer the past', '2010-06-15', 'Suffer the Past\'s effect causes a single life-gain event, not a number of separate ones. This matters for cards such as Cradle of Vitality.').

card_ruling('suffocation', '2004-10-04', 'It means \"red instant or red sorcery\" not \"red instant or any sorcery\".').

card_ruling('suicidal charge', '2009-02-01', 'Suicidal Charge\'s ability affects only the creatures your opponents control at the time it resolves. If another creature comes under the control of one of those players by the time that turn\'s combat step begins, it won\'t have to attack.').

card_ruling('suleiman\'s legacy', '2004-10-04', 'Does nothing to Djinns or Efreets that phase in while it is on the battlefield.').

card_ruling('sulfur elemental', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('sulfur elemental', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('sulfur elemental', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('sulfur elemental', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('sulfur elemental', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('sulfur elemental', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('sulfuric vapors', '2004-10-04', 'If the red spell damages more than one target, add 1 to each target.').
card_ruling('sulfuric vapors', '2004-10-04', 'The ability is a replacement effect which is applied when damage would become dealt.').

card_ruling('sultai charm', '2014-09-20', 'A monocolored creature is exactly one color. Colorless creatures aren’t monocolored.').

card_ruling('sultai emissary', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('sultai emissary', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('sultai emissary', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('sultai emissary', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('sultai emissary', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('sultai emissary', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('sultai emissary', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('sultai emissary', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('sultai emissary', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('sultai emissary', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('sultai emissary', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('sultai emissary', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('sultai emissary', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('sultai flayer', '2014-09-20', 'If Sultai Flayer dies, and its toughness is still 4 or greater, its own ability will trigger.').

card_ruling('sultai scavenger', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('sultai scavenger', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('sultai scavenger', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('sultai scavenger', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('sultai scavenger', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('summer bloom', '2004-10-04', 'This spell increases the number of lands you can play in a turn. The land cards are played as you would normally play lands.').

card_ruling('summit apes', '2010-03-01', 'Whether you control a Mountain matters only as the defending player declares blockers. Once it\'s blocked, gaining or losing control of a Mountain won\'t affect Summit Apes\'s blockers.').

card_ruling('summoner\'s bane', '2009-10-01', 'If the targeted spell is an illegal target by the time Summoner\'s Bane resolves, the entire spell is countered. You won\'t get a creature token.').
card_ruling('summoner\'s bane', '2009-10-01', 'You get the token creature, not the controller of the creature spell.').

card_ruling('summoner\'s egg', '2004-12-01', 'The card is imprinted face down. This means that other players don\'t know what the card is and you can\'t look at the card once it\'s imprinted (until it\'s turned face up, of course). Note that effects that exile cards, including all previous imprint cards, exile those cards face up unless the effect says otherwise.').
card_ruling('summoner\'s egg', '2004-12-01', 'If the card is not a creature card when it\'s turned face up, it remains exiled face up.').
card_ruling('summoner\'s egg', '2004-12-01', 'Any \"When this creature is turned face up\" abilities that the exiled card may have don\'t trigger because the card isn\'t being turned face up while it\'s on the battlefield.').

card_ruling('summoner\'s pact', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('summoning trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('summoning trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('summoning trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('summoning trap', '2009-10-01', 'If there are seven or fewer cards in your library, you\'ll look at all of them.').

card_ruling('sun ce, young conquerer', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('sun droplet', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('sun quan, lord of wu', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').
card_ruling('sun quan, lord of wu', '2009-10-01', 'Sun Quan\'s ability affects Sun Quan itself.').

card_ruling('sun titan', '2010-08-15', 'A permanent card is an artifact, creature, enchantment, land, or planeswalker card.').
card_ruling('sun titan', '2010-08-15', 'The converted mana cost of a card in your graveyard is determined solely by the mana symbols printed in its upper right corner. The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('sun titan', '2010-08-15', 'If the mana cost of a card in your graveyard includes {X}, X is considered to be 0.').
card_ruling('sun titan', '2010-08-15', 'If a card in your graveyard has no mana symbols in its upper right corner (because it\'s a land card, for example), its converted mana cost is 0.').

card_ruling('sunblast angel', '2011-01-01', 'The second ability destroys each creature that\'s tapped at the time it resolves, including creatures you control. If Sunblast Angel has become tapped by the time its ability resolves, it will be destroyed too.').

card_ruling('sunbond', '2014-02-01', 'In some unusual cases, you can gain life even though your life total actually decreases. For example, if you are being attacked by two 3/3 creatures and you block one with a 2/2 creature with lifelink, your life total will decrease by 1 even though you’ve gained 2 life. You’ll put two +1/+1 counters on the enchanted creature.').
card_ruling('sunbond', '2014-02-01', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('sunbringer\'s touch', '2015-02-25', 'Count the number of cards in your hand as Sunbringer’s Touch resolves to determine the value of X.').
card_ruling('sunbringer\'s touch', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('sunbringer\'s touch', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('sunder', '2004-10-04', 'Only affects lands on the battlefield.').

card_ruling('sundering growth', '2012-10-01', 'You must target an artifact or enchantment to cast Sundering Growth. If that artifact or creature is an illegal target when Sundering Growth tries to resolve, it will be countered and none of its effects will happen. You won\'t populate.').
card_ruling('sundering growth', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('sundering growth', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('sundering growth', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('sundering growth', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('sundering growth', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('sundering growth', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('sundering titan', '2004-12-01', 'Sundering Titan\'s two abilities aren\'t targeted. When one of the abilities resolves, the Titan\'s controller must choose one land for each basic land type (Plains, Island, Swamp, Mountain, and Forest).').
card_ruling('sundering titan', '2004-12-01', 'If one of the basic land types isn\'t present, it isn\'t chosen. If the only land of a certain type is one you control, you must choose it.').
card_ruling('sundering titan', '2004-12-01', 'If a land has more than one basic land type, it can be chosen more than once.').
card_ruling('sundering titan', '2004-12-01', 'All the chosen lands are destroyed simultaneously.').

card_ruling('sundering vitae', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('sundering vitae', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('sundering vitae', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('sundering vitae', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('sundering vitae', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('sundering vitae', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('sundering vitae', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('sundial of the infinite', '2011-09-22', 'Ending the turn this way means the following things happen in order: 1) All spells and abilities on the stack are exiled. This includes spells and abilities that can\'t be countered. 2) All attacking and blocking creatures are removed from combat. 3) State-based actions are checked. No player gets priority, and no triggered abilities are put onto the stack. 4) The current phase and/or step ends. The game skips straight to the cleanup step. The cleanup step happens in its entirety.').
card_ruling('sundial of the infinite', '2011-09-22', 'If any triggered abilities do trigger during this process, they\'re put onto the stack during the cleanup step. If this happens, players will have a chance to cast spells and activate abilities, then there will be another cleanup step before the turn finally ends.').
card_ruling('sundial of the infinite', '2011-09-22', 'Though spells and abilities that are exiled won\'t get a chance to resolve, they don\'t count as being \"countered.\"').
card_ruling('sundial of the infinite', '2011-09-22', 'If Sundial of the Infinite\'s ability is activated before the end step, any \"at the beginning of the end step\"-triggered abilities won\'t get the chance to trigger that turn because the end step is skipped. Those abilities will trigger at the beginning of the end step of the next turn. The same is true of abilities that trigger at the beginning of other phases or steps (except upkeep).').
card_ruling('sundial of the infinite', '2011-09-22', 'The earliest that you can activate Sundial of the Infinite\'s ability is during your upkeep step, after abilities that trigger \"at the beginning of your upkeep\" have been put onto the stack but before they resolve.').

card_ruling('sunfire balm', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('sunfire balm', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('sunfire balm', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('sunfire balm', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('sunflare shaman', '2008-04-01', 'If Sunflare Shaman leaves the battlefield before its ability resolves, it will still deal damage to the targeted creature or player. If Sunflare Shaman is put into your graveyard, it even counts toward the amount of damage dealt.').
card_ruling('sunflare shaman', '2008-04-01', 'If the target leaves the battlefield or otherwise becomes an illegal target before the ability resolves, the ability is countered and Sunflare Shaman won\'t deal any damage to itself.').

card_ruling('sunforger', '2005-10-01', 'You can\'t pay the cost of the second ability unless Sunforger is attached to a creature.').
card_ruling('sunforger', '2005-10-01', 'Any card you find must be legally castable (for example, you have to be able to choose a legal target for it). If you can\'t find a castable card (or choose not to), nothing happens and you shuffle your library.').
card_ruling('sunforger', '2005-10-01', 'The card is cast from your library, not your hand. You choose modes, pay additional costs, choose targets, etc. for the spell as normal when casting it. Any X in the mana cost will be 0. Alternative costs can\'t be paid.').

card_ruling('sungrass egg', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('sunhome guildmage', '2013-01-24', 'Only creatures you control when Sunhome Guildmage\'s first ability resolves will get +1/+0. Creatures that come under your control later in the turn will not.').

card_ruling('sunhome, fortress of the legion', '2005-10-01', 'If you give double strike to a creature with first strike after first-strike combat damage has been dealt, that creature will also deal damage during the normal combat damage step.').
card_ruling('sunhome, fortress of the legion', '2005-10-01', 'If you give double strike to a creature after either first-strike or normal combat damage has been dealt, it won\'t help that creature deal any additional combat damage.').

card_ruling('sunken field', '2004-10-04', 'The spell\'s controller gets the option to pay when the ability resolves.').

card_ruling('sunken hollow', '2015-08-25', 'Even though these lands have basic land types, they are not basic lands because “basic” doesn’t appear on their type line. Notably, controlling two or more of them won’t allow others to enter the battlefield untapped.').
card_ruling('sunken hollow', '2015-08-25', 'However, because these cards have basic land types, effects that specify a basic land type without also specifying that the land be basic can affect them. For example, a spell or ability that reads “Destroy target Forest” can target Canopy Vista, while one that reads “Destroy target basic Forest” cannot.').
card_ruling('sunken hollow', '2015-08-25', 'If one of these lands enters the battlefield at the same time as any number of basic lands, those other lands are not counted when determining if this land enters the battlefield tapped or untapped.').

card_ruling('sunlance', '2007-02-01', 'This is the timeshifted version of Strafe.').

card_ruling('sunpetal grove', '2009-10-01', 'This checks for lands you control with the land type Forest or Plains, not for lands named Forest or Plains. The lands it checks for don\'t have to be basic lands. For example, if you control Hallowed Fountain (a nonbasic land with the land types Plains and Island), Sunpetal Grove will enter the battlefield untapped.').
card_ruling('sunpetal grove', '2009-10-01', 'As this is entering the battlefield, it checks for lands that are already on the battlefield. It won\'t see lands that are entering the battlefield at the same time (due to Warp World, for example).').

card_ruling('sunscape familiar', '2004-10-04', 'If a spell is both green and blue, you pay {1} less, not {2} less.').
card_ruling('sunscape familiar', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('sunscape familiar', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('sunscape familiar', '2004-10-04', 'The effect is cumulative.').
card_ruling('sunscape familiar', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('sunscape familiar', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('sunscape familiar', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('sunscape master', '2004-10-04', 'The first ability only affects creatures you control at the time the ability resolves.').

card_ruling('sunscour', '2006-07-15', 'Paying the alternative cost doesn\'t change when you can cast the spell. A creature spell you cast this way, for example, can still only be cast during your main phase while the stack is empty.').
card_ruling('sunscour', '2006-07-15', 'You may pay the alternative cost rather than the card\'s mana cost. Any additional costs are paid as normal.').
card_ruling('sunscour', '2006-07-15', 'If you don\'t have two cards of the right color in your hand, you can\'t choose to cast the spell using the alternative cost.').
card_ruling('sunscour', '2006-07-15', 'You can\'t exile a card from your hand to pay for itself. At the time you would pay costs, that card is on the stack, not in your hand.').

card_ruling('sunseed nurturer', '2008-10-01', 'The first ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control a creature with power 5 or greater as your end step begins, and (2) the ability will do nothing if you don\'t control a creature with power 5 or greater by the time it resolves. (It doesn\'t have to be the same creature as the one that allowed the ability to trigger.) Power-boosting effects that last \"until end of turn\" will still be in effect when this kind of ability triggers and resolves. An ability like this will trigger a maximum of once per turn, no matter how many applicable creatures you control.').

card_ruling('sunspear shikari', '2011-01-01', 'If all Equipment attached to Sunspear Shikari somehow becomes unequipped after it deals combat damage in the first combat damage step, it won\'t assign combat damage in the second combat damage step.').

card_ruling('sunspring expedition', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('sunspring expedition', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('superior numbers', '2008-08-01', 'As you cast this, you target a creature and an opponent. The opponent does not have to be that creature\'s controller.').
card_ruling('superior numbers', '2008-08-01', 'If the opponent becomes an illegal target before the spell resolves (but the creature doesn\'t), the spell will still be able to access information about that opponent. It will resolve as normal and deal the appropriate amount of damage to the creature.').
card_ruling('superior numbers', '2008-08-01', 'If the creature becomes an illegal target before the spell resolves (but the opponent doesn\'t), the spell will be unable to affect that creature. Although the spell will still resolve, it won\'t deal any damage.').

card_ruling('supplant form', '2014-11-24', 'The token copies exactly what was printed on the creature and nothing else (unless that creature was copying something else or was a token; see below). It doesn’t copy whether that creature was tapped or untapped, whether it had any counters on it or Auras and/or Equipment attached to it, or any non-copy effects that changed its power, toughness, types, color, and so on.').
card_ruling('supplant form', '2014-11-24', 'If you return a face-down creature to its owner’s hand, the token will be a face-up colorless 2/2 creature with no name, no abilities, and no creature types. (The face-down creature card will be revealed to all players as it leaves the battlefield.)').
card_ruling('supplant form', '2014-11-24', 'If the copied creature had {X} in its mana cost, X is 0.').
card_ruling('supplant form', '2014-11-24', 'If the copied creature was copying something else, the token enters the battlefield as whatever that creature was copying.').
card_ruling('supplant form', '2014-11-24', 'If the copied creature is a token, the token created by Supplant Form copies the original characteristics of that token as stated by the effect that put it onto the battlefield.').
card_ruling('supplant form', '2014-11-24', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “As [this creature] enters the battlefield” or “[This creature] enters the battlefield with” abilities of the copied creature will also work.').

card_ruling('suppression bonds', '2015-06-22', 'Suppression Bonds can enchant any nonland permanent, not just a creature.').
card_ruling('suppression bonds', '2015-06-22', 'Activated abilities contain a colon. They’re generally written “[Cost]: [Effect].” Some keywords are activated abilities and will have colons in their reminder texts. The loyalty abilities of planeswalkers are activated abilities.').

card_ruling('suppression field', '2005-10-01', 'Suppression Field\'s ability doesn\'t affect static abilities, triggered abilities, or mana abilities. (A \"mana ability\" is an ability that produces mana, not an ability that costs mana.)').
card_ruling('suppression field', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('supreme inquisitor', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('supreme inquisitor', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('suq\'ata assassin', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('surestrike trident', '2004-12-01', 'Surestrike Trident grants an ability to the equipped creature.').
card_ruling('surestrike trident', '2004-12-01', 'The equipped creature\'s controller chooses whether or not to activate the activated ability. It doesn\'t matter who controls Surestrike Trident.').
card_ruling('surestrike trident', '2004-12-01', '\"Unattach Surestrike Trident\" means just that -- Surestrike Trident moves off the creature it was equipping and remains on the battlefield.').

card_ruling('surge node', '2011-06-01', 'You can target any artifact, not just one with an ability that references charge counters.').

card_ruling('surge of righteousness', '2015-02-25', 'If Surge of Righteousness resolves but the creature isn’t destroyed (perhaps because it regenerated or it has indestructible), you’ll still gain 2 life.').
card_ruling('surge of righteousness', '2015-02-25', 'However, if the creature becomes an illegal target before Surge of Righteousness resolves, it will be countered and none of its effects will happen. You won’t gain life.').

card_ruling('surgespanner', '2007-10-01', 'This is a triggered ability, not an activated ability. It doesn\'t allow you to tap the creature whenever you want; rather, you need some other way of tapping it, such as by attacking with the creature.').
card_ruling('surgespanner', '2007-10-01', 'For the ability to trigger, the creature has to actually change from untapped to tapped. If an effect attempts to tap the creature, but it was already tapped at the time, this ability won\'t trigger.').

card_ruling('surgical extraction', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('surgical extraction', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('surgical extraction', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('surgical extraction', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('surgical extraction', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('surgical extraction', '2011-06-01', '\"Any number of cards\" means just that. If you wish, you can choose to leave some or all of the cards with the same name as the targeted card, including that card, in the zone they\'re in.').

card_ruling('surprise deployment', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('surrak dragonclaw', '2014-09-20', 'A spell or ability that counters spells can still target a creature spell you control. When that spell or ability resolves, the creature spell won’t be countered, but any additional effects of that spell or ability will still happen.').

card_ruling('surrak, the hunt caller', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('surrak, the hunt caller', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('surrak, the hunt caller', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('surrakar marauder', '2009-10-01', 'Intimidate looks at the current colors of a creature that has it. Normally, if Surrakar Marauder has intimidate, it can\'t be blocked except by artifact creatures and/or black creatures. If it\'s turned white, then it can\'t be blocked except by artifact creatures and/or white creatures.').
card_ruling('surrakar marauder', '2009-10-01', 'If an attacking creature has intimidate, what colors it is matters only as the defending player declares blockers. Once it\'s blocked, changing its colors won\'t change that.').
card_ruling('surrakar marauder', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('surrakar marauder', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('surrakar spellblade', '2010-06-15', 'If you cast an instant or sorcery spell, Surrakar Spellblade\'s first ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').
card_ruling('surrakar spellblade', '2010-06-15', 'When Surrakar Spellblade\'s second ability resolves, you either draw a card for each charge counter on it or you draw no cards at all.').
card_ruling('surrakar spellblade', '2010-06-15', 'If Surrakar Spellblade leaves the battlefield after its second ability triggers but before it resolves, its last existence on the battlefield is checked to determine how many charge counters were on it.').

card_ruling('surreal memoir', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('surreal memoir', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('surreal memoir', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('surreal memoir', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('surreal memoir', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('surreal memoir', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('surreal memoir', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('surreal memoir', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('surreal memoir', '2010-06-15', 'Surreal Memoir isn\'t targeted. You don\'t choose an instant card at random from your graveyard until it resolves. Once you randomly select a card, it\'s too late for players to respond.').
card_ruling('surreal memoir', '2010-06-15', 'If you have only one instant card in your graveyard as Surreal Memoir resolves, that\'s the one you\'ll return to your hand.').
card_ruling('surreal memoir', '2010-06-15', 'If you\'re playing a format involving only cards from the _Urza\'s Saga_(TM) set and later, you may change the order of your graveyard at any time. That means the easiest way to choose an instant card at random from your graveyard is to take all the instant cards in your graveyard, turn them face down, shuffle them, and pick a card. Then you just put the rest back.').
card_ruling('surreal memoir', '2010-06-15', 'If you\'re playing a format involving cards printed earlier than the _Urza\'s Saga_ set, you may not reorder your graveyard. In this case, you need to be more careful when selecting an instant card at random, perhaps by using dice, writing the names of the instant cards on slips of paper and choosing one of them randomly, or carefully noting the existing graveyard order so you can reestablish it after performing the method suggested above, for example.').
card_ruling('surreal memoir', '2010-06-15', 'If you have multiple instant cards in your graveyard with the same name, and one of them is being targeted by another spell on the stack or is enchanted (with Spellweaver Volute, for example), you must differentiate them so you know which one (if any) is chosen at random. In that case, it may be better to use dice or another method that allows you to differentiate between the instant cards to choose one at random.').
card_ruling('surreal memoir', '2010-06-15', 'All players get to see which card you chose at random.').

card_ruling('surrender your thoughts', '2010-06-15', 'The targeted player may choose \"self\" even if he or she can\'t perform the resulting action. For example, a player targeted with Feed the Machine may choose \"self\" even if he or she controls no creatures.').
card_ruling('surrender your thoughts', '2010-06-15', 'The targeted player may choose \"others\" even if there are no others (because all of his or her teammates have lost the game, for example), or the archenemy\'s other opponents can\'t perform the resulting action.').
card_ruling('surrender your thoughts', '2010-06-15', 'In a Supervillain Rumble game, the targeted player may still choose \"others.\" Each player who isn\'t the active player or the targeted player will thus be affected.').

card_ruling('survey the wreckage', '2012-10-01', 'If the target land is an illegal target when Survey the Wreckage tries to resolve, it will be countered and none of its effects will happen. You won\'t get the creature token.').

card_ruling('surveyor\'s scope', '2013-10-17', 'You count the number of players who control at least two more lands than you when the ability resolves.').
card_ruling('surveyor\'s scope', '2013-10-17', 'If no players control at least two more lands than you when the ability resolves, you’ll still search and shuffle your library.').

card_ruling('survival cache', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('survival cache', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('survival cache', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('survival cache', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('survival cache', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('survival cache', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('survival cache', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('survival cache', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('survival cache', '2010-06-15', 'In a Two-Headed Giant game, anything that cares about your life total checks the life total of your team. (This is a change from previous rules.) For example, say that your team has 10 life and your opponent\'s team has 11 life when you cast Survival Cache. You\'ll gain 2 life, so your team\'s life total becomes 12. Since your life total (12) is greater than an opponent\'s life total (11), you\'ll draw a card.').

card_ruling('survival of the fittest', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('survivor of the unseen', '2006-07-15', 'If you don\'t draw a card because all the draws were replaced (with dredge, for example), you still have to put a card from your hand on top of your library.').
card_ruling('survivor of the unseen', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('suspension field', '2014-09-20', 'Suspension Field’s ability causes a zone change with a duration. It’s a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Suspension Field leaves the battlefield.').
card_ruling('suspension field', '2014-09-20', 'If the target creature becomes an illegal target before the ability resolves, perhaps because its toughness was lowered, the ability will be countered and none of its effects will happen. No creature will be exiled, and Suspension Field will remain on the battlefield.').
card_ruling('suspension field', '2014-09-20', 'If Suspension Field leaves the battlefield before its enters-the-battlefield ability resolves, the target creature won’t be exiled.').
card_ruling('suspension field', '2014-09-20', 'Auras attached to the exiled creature will be put into their owners’ graveyards. Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('suspension field', '2014-09-20', 'If a token is exiled this way, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('suspension field', '2014-09-20', 'The exiled card returns to the battlefield immediately after Suspension Field leaves the battlefield. Nothing happens between these two events, including state-based actions. Suspension Field and the returned card aren’t on the battlefield at the same time.').
card_ruling('suspension field', '2014-09-20', 'In a multiplayer game, if Suspension Field’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('sustainer of the realm', '2004-10-04', 'It gets the bonus only once, not once per creature it blocks.').

card_ruling('sustaining spirit', '2004-10-04', 'Does not affect damage if you are already at zero or negative life. You still take it all.').
card_ruling('sustaining spirit', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').
card_ruling('sustaining spirit', '2008-10-01', 'The second ability applies only if your life total is being reduced by damage. Other effects or costs (such as \"lose 1 life\" or \"pay 1 life\") can reduce your life total below 1 as normal.').
card_ruling('sustaining spirit', '2008-10-01', 'If an effect asks you to pay life, you can\'t pay more life than you have.').
card_ruling('sustaining spirit', '2008-10-01', 'The second ability is not a prevention effect. It stops unpreventable damage from reducing your life total below 1.').
card_ruling('sustaining spirit', '2008-10-01', 'The ability doesn\'t change how much damage is dealt; it just changes how much life that damage makes you lose. An effect such as Spirit Link will see the full amount of damage being dealt.').
card_ruling('sustaining spirit', '2008-10-01', 'Sustaining Spirit won\'t prevent you from losing the game if your life total is 0 or less or some other effect causes you to lose the game.').

card_ruling('sutured ghoul', '2011-09-22', 'If any of the creature cards you exile has a characteristic-defining ability that defines its power and/or toughness, that ability will apply. For example, if Dungrove Elder is exiled this way, its power and toughness while it\'s in exile are equal to the number of Forests you control, and Sutured Ghoul\'s power and toughness will change as the number of Forests you control changes. If the characteristic-defining ability can\'t be applied (for instance, it relies on a choice made as the card enters the battlefield), then use 0.').
card_ruling('sutured ghoul', '2011-09-22', 'In zones other than the battlefield, Sutured Ghoul\'s power and toughness are each 0.').
card_ruling('sutured ghoul', '2011-09-22', 'You can\'t have Sutured Ghoul exile itself, even if it\'s entering the battlefield from your graveyard.').

card_ruling('svogthos, the restless tomb', '2005-10-01', 'While animated, Svogthos\'s power and toughness changes each time a creature card enters or leaves its controller\'s graveyard.').
card_ruling('svogthos, the restless tomb', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('svogthos, the restless tomb', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('svyelunite priest', '2004-10-04', 'Does not cause Auras on the creature to be removed when the ability is activated. An Aura on the battlefield is neither a spell nor an ability.').
card_ruling('svyelunite priest', '2005-08-01', 'The effect prevents spells or the effects of permanents from targeting the creature. Auras which confer an ability (such as Lance or Firebreathing) are not prevented.').

card_ruling('swamp mosquito', '2004-10-04', 'Triggers immediately after blocking is declared if at that time no blockers are assigned to it.').
card_ruling('swamp mosquito', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('swan song', '2013-09-15', 'Swan Song can target a spell that can’t be countered. That spell won’t be countered when Swan Song resolves, but its controller will get a Bird token.').

card_ruling('swans of bryn argoll', '2008-05-01', 'The source of combat damage is the creature that deals it.').
card_ruling('swans of bryn argoll', '2008-05-01', 'If a spell is causing damage to be dealt, that spell will always identify the source of the damage. In most cases, the source is the spell itself. For example, Burn Trail says \"Burn Trail deals 3 damage to target creature or player.\"').
card_ruling('swans of bryn argoll', '2008-05-01', 'If an ability is causing damage to be dealt, that ability will always identify the source of the damage. The ability itself is never the source. However, the source of the ability is often the source of the damage. For example, Knollspine Invocation\'s ability says \"Knollspine Invocation deals X damage to target creature or player.\"').
card_ruling('swans of bryn argoll', '2008-05-01', 'If the source of the damage is a permanent, Swans of Bryn Argoll checks who that permanent\'s controller is at the time that damage is prevented. If the permanent has left the battlefield by then, its last known information is used. If the source of the damage is a spell, its controller is obvious. If the source of the damage is a card from some other zone (such as a cycled Gempalm Incinerator), Swans of Bryn Argoll checks its owner rather than its controller.').
card_ruling('swans of bryn argoll', '2008-05-01', 'If a creature with wither would deal damage to Swans of Bryn Argoll, that damage is treated just like any other damage. It\'s prevented, and the creature\'s controller draws cards. Swans of Bryn Argoll doesn\'t get any -1/-1 counters.').

card_ruling('swarm surge', '2015-08-25', 'A colorless creature given first strike by Swarm Surge won’t lose first strike if it stops being colorless later in the turn.').
card_ruling('swarm surge', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('swarm surge', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('swarm surge', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('swarm surge', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('swarm surge', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('swarmborn giant', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('swarmborn giant', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('swarmborn giant', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('swat', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('sway of illusion', '2004-10-04', 'You can cast this and choose to target zero creatures and effectively just draw a card.').

card_ruling('sway of the stars', '2005-02-01', 'Changes in life total are treated as life gain and life loss.').
card_ruling('sway of the stars', '2005-02-01', 'Tokens are included in the effect but shouldn\'t actually be included in the shuffle. Tokens will cease to exist immediately after the spell resolves.').
card_ruling('sway of the stars', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('swell of growth', '2015-08-25', 'Putting a land onto the battlefield this way is not the same as playing a land. Swell of Growth doesn’t affect how many lands you can play from your hand during your turn.').

card_ruling('swelter', '2004-10-04', 'You can\'t cast this spell unless you have two different legal targets.').

card_ruling('swerve', '2008-10-01', 'Swerve targets only the spell whose target will be changed. It doesn\'t directly affect the original target of that spell or the new target of that spell.').
card_ruling('swerve', '2008-10-01', 'You don\'t choose the new target for the spell until Swerve resolves. You must change the target if possible. However, you can\'t change the target to an illegal target. If there are no legal targets, the target isn\'t changed. It doesn\'t matter if the original target of that spell has somehow become illegal.').
card_ruling('swerve', '2008-10-01', 'If you cast Swerve on a spell that targets a spell on the stack (like Cancel does, for example), you can\'t change that spell\'s target to itself. You can, however, change that spell\'s target to Swerve. If you do, that spell will be countered when it tries to resolve because Swerve will have left the stack by then.').
card_ruling('swerve', '2008-10-01', 'If a spell targets multiple things, you can\'t target it with Swerve, even if all but one of those targets has become illegal.').
card_ruling('swerve', '2008-10-01', 'If a spell targets the same player or object multiple times, you can\'t target it with Swerve.').
card_ruling('swerve', '2010-03-01', 'An Aura spell on the stack targets the object or player which it will enchant upon entering the battlefield. Thus, an Aura spell is a \"spell with a single target\", which means you may use Swerve to change that target. Doing so will cause it to enter the battlefield enchanting the new target rather than the original one.').

card_ruling('swift kick', '2014-09-20', 'You can’t cast Swift Kick unless you target both a creature you control and a creature you don’t control.').
card_ruling('swift kick', '2014-09-20', 'If either target is an illegal target when Swift Kick tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('swift kick', '2014-09-20', 'If just the creature you control is an illegal target when Swift Kick tries to resolve, it won’t get +1/+0 until end of turn. If the creature you control is a legal target but the creature you don’t control isn’t, the creature you control will get +1/+0 until end of turn.').

card_ruling('swift reckoning', '2015-06-22', 'The number of instant and/or sorcery cards in your graveyard matters only as you begin to cast Swift Reckoning. Once it’s cast, that number doesn’t matter and will have no effect on Swift Reckoning resolving.').
card_ruling('swift reckoning', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('swift warkite', '2015-02-25', 'You can’t put creatures onto the battlefield face down this way.').
card_ruling('swift warkite', '2015-02-25', 'The creature you put onto the battlefield will be returned to your hand only if it’s still on the battlefield as the delayed triggered ability resolves during the next end step. If it leaves the battlefield before then, it will remain wherever it is.').
card_ruling('swift warkite', '2015-02-25', 'The delayed triggered ability that returns the creature to your hand will trigger even if Swift Warkite leaves the battlefield before the next end step.').

card_ruling('swiftfoot boots', '2011-09-22', 'If an opponent gains control of a creature equipped by your Swiftfoot Boots, that creature can\'t be the target of spells and abilities you control.').

card_ruling('swirl the mists', '2004-12-01', 'This effect lasts as long as Swirl the Mists is on the battlefield. It ends as soon as Swirl the Mists leaves the battlefield.').
card_ruling('swirl the mists', '2004-12-01', 'Swirl the Mists\' ability also affects spells that are cast and permanents that enter the battlefield after Swirl the Mists enters the battlefield.').

card_ruling('swirling sandstorm', '2004-10-04', 'This card does nothing if you don\'t have seven cards in your graveyard when it resolves.').

card_ruling('swirling spriggan', '2008-08-01', 'You can choose any color or combination of colors. You can\'t choose colorless.').

card_ruling('switcheroo', '2012-07-01', 'If one of the target creatures is an illegal target when Switcheroo resolves, the exchange won\'t happen. If both creatures are illegal targets, Switcheroo will be countered.').
card_ruling('switcheroo', '2012-07-01', 'You don\'t have to control either target.').
card_ruling('switcheroo', '2012-07-01', 'If the same player controls both creatures when Switcheroo resolves, nothing happens.').

card_ruling('sword of body and mind', '2011-01-01', 'If there are fewer than ten cards in that player\'s library, that player puts all the cards from his or her library into his or her graveyard.').

card_ruling('sword of feast and famine', '2011-06-01', 'You\'ll untap all lands you control even if the player doesn\'t discard a card.').

card_ruling('sword of fire and ice', '2013-06-07', 'The triggered ability has one target: the creature or player to be dealt 2 damage. If that creature or player is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').

card_ruling('sword of kaldra', '2004-10-04', 'You exile the creature that gets damaged.').

card_ruling('sword of light and shadow', '2013-06-07', 'You may put the triggered ability with no targets. However, If you choose a target creature in your graveyard, and that card is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 3 life.').

card_ruling('sword of the ages', '2009-10-01', 'The activated ability checks the power the exiled creatures had as they last existed on the battlefield.').

card_ruling('sword of the meek', '2007-05-01', 'The triggered ability checks the power and toughness of a creature that enters the battlefield as it exists once it\'s on the battlefield. For example, Triskelion has 1/1 printed on it, but it enters the battlefield with three +1/+1 counters on it, so it actually enters the battlefield as a 4/4 creature. Sword of the Meek\'s ability wouldn\'t trigger. Ditto for a Llanowar Elves that enters the battlefield while you control Gaea\'s Anthem.').
card_ruling('sword of the meek', '2007-05-01', 'If a 1/1 creature that could not be equipped by Sword of the Meek enters the battlefield under your control, you may return Sword of the Meek to the battlefield when the ability resolves. The Sword will fail to become attached to the creature, but it will remain on the battlefield.').
card_ruling('sword of the meek', '2007-05-01', 'If multiple 1/1 creatures enter the battlefield at the same time, the Sword\'s ability will trigger that many times. Only the first ability to resolve will return the Sword to the battlefield and attach it to a creature.').

card_ruling('sword of war and peace', '2011-06-01', 'You count the cards in the each player\'s hand when the triggered ability resolves.').

card_ruling('swords to plowshares', '2004-10-04', 'If the creature has a negative power, the player does not lose life. It acts the same as if it had a power of zero.').
card_ruling('swords to plowshares', '2008-10-01', 'The amount of life that\'s gained is equal to the power of the targeted creature as it last existed on the battlefield.').

card_ruling('sworn defender', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('sydri, galvanic genius', '2013-10-17', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('sydri, galvanic genius', '2013-10-17', 'If the noncreature artifact had any other supertypes, types, or subtypes, it will keep those. In most cases, the artifact creature won’t have any creature types.').
card_ruling('sydri, galvanic genius', '2013-10-17', 'If an Equipment attached to a creature becomes a creature, it becomes unattached. An Equipment that is also a creature can’t equip another creature.').
card_ruling('sydri, galvanic genius', '2013-10-17', 'Multiple instances of deathtouch or lifelink on the same creature are redundant.').

card_ruling('sygg, river cutthroat', '2008-05-01', 'As the end step begins, Sygg\'s ability checks whether a player who is currently your opponent, or a player who was your opponent at the time he or she left the game, has lost 3 or more life over the course of the turn. If so, the ability will trigger. If not, it won\'t.').
card_ruling('sygg, river cutthroat', '2008-05-01', 'It doesn\'t matter how the opponent lost life or who caused it, as long as the total loss of life is 3 or more.').
card_ruling('sygg, river cutthroat', '2008-05-01', 'Sygg\'s ability checks only whether life was lost. It doesn\'t care whether life was also gained. For example, if an opponent lost 4 life and gained 6 life during the turn, that player will have a higher life total than he or she started the turn with -- but Sygg\'s ability will trigger anyway.').
card_ruling('sygg, river cutthroat', '2008-05-01', 'You don\'t specify an opponent when the ability triggers. If multiple opponents have lost 3 or more life over the course of the turn, the ability will trigger only once.').
card_ruling('sygg, river cutthroat', '2008-05-01', 'Sygg\'s ability will trigger even if Sygg wasn\'t on the battlefield at the time some or all of the life was lost.').
card_ruling('sygg, river cutthroat', '2008-05-01', 'Sygg\'s ability checks to see if it triggers at the end of every turn. It doesn\'t have to be your turn, and it doesn\'t have to be the turn of the opponent that lost life.').

card_ruling('sylvan bounty', '2009-02-01', 'Unlike the normal cycling ability, basic landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a basic land card. You don\'t choose the type of basic land card you\'ll find until you\'re performing the search. After you choose a basic land card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('sylvan bounty', '2009-02-01', 'Basic landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being basic landcycled. Any ability that stops a cycling ability from being activated also stops a basic landcycling ability from being activated.').
card_ruling('sylvan bounty', '2009-02-01', 'Basic landcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with basic landcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('sylvan bounty', '2009-02-01', 'You can choose not to find a basic land card, even if there is one in your library.').

card_ruling('sylvan echoes', '2013-09-20', 'If you win a clash initiated by a spell or ability an opponent controls, the ability will still trigger.').

card_ruling('sylvan hierophant', '2007-05-01', 'It targets a creature card in the graveyard. This target is selected when putting the triggered ability on the stack. The wording specifies it has to be a card other than the Hierophant').
card_ruling('sylvan hierophant', '2008-04-01', 'If you control Sylvan Hierophant when it goes to the graveyard, you exile the Hierophant and return a creature card from your graveyard to your hand. It doesn\'t matter whose graveyard the Hierophant goes to.').
card_ruling('sylvan hierophant', '2008-04-01', 'If Sylvan Hierophant is removed from the graveyard before the ability resolves, you still return the targeted creature card from your graveyard to your hand.').

card_ruling('sylvan library', '2004-10-04', 'If you have drawn cards prior to your draw step, they can be ones chosen to be put back using this effect. For example, a cantrip draw or an instant used during upkeep.').
card_ruling('sylvan library', '2004-10-04', 'Spells and abilities are resolved one at a time, so if you use multiple Sylvan Libraries in one stack, each will resolve in sequence. You do not get to draw all the cards at once then put them all back at once.').
card_ruling('sylvan library', '2004-10-04', 'You can return zero, one, or two cards.').
card_ruling('sylvan library', '2004-10-04', 'This will count as 2 draws for anything that affects \"drawn cards\".').
card_ruling('sylvan library', '2004-10-04', 'If you are going to draw cards due to triggered abilities during your draw step, your regular draw resolves first, but you can choose to order the Sylvan Library before or after any other draws.').
card_ruling('sylvan library', '2007-09-16', 'If you choose to draw two cards, then replace one or more of those draws with some other effect, the rest of Sylvan Library\'s ability still happens. If you\'ve actually drawn only one card that turn, you must choose that card and either pay 4 life or put it on top of your library. If you haven\'t actually drawn any cards that turn, the rest of the ability has no effect.').
card_ruling('sylvan library', '2013-04-15', 'You always perform your normal draw before this ability because the normal draw occurs before anything is placed on the stack.').

card_ruling('sylvan offering', '2014-11-07', 'You may choose the same opponent for each of the effects, or you may choose different opponents. None of the affected players are targets of the spell.').
card_ruling('sylvan offering', '2014-11-07', 'You choose the opponents for each effect as the spell resolves.').

card_ruling('sylvan paradise', '2004-10-04', 'You can choose to target zero creatures.').

card_ruling('sylvan primordial', '2013-01-24', 'You search and shuffle your library only once.').
card_ruling('sylvan primordial', '2013-07-01', 'If Sylvan Primordial\'s enters-the-battlefield ability resolves but one of the target noncreature permanents isn\'t destroyed (perhaps because it has indestructible or it regenerated), it won\'t count toward the number of Forest cards you can put onto the battlefield.').
card_ruling('sylvan primordial', '2014-02-01', 'If an opponent has any legal permanents to target, you must target one of them.').

card_ruling('sylvan tutor', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('sylvok lifestaff', '2011-01-01', 'If Sylvok Lifestaff and the equipped creature are put into their owners\' graveyards at the same time (due to Planar Cleansing, perhaps), the triggered ability will still trigger. You\'ll gain 3 life.').

card_ruling('sylvok replica', '2011-01-01', 'You may target Sylvok Replica with its own ability. However, if you do, the ability will be countered for having an illegal target.').

card_ruling('synapse sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('synapse sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('synchronous sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('synchronous sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('syndic of tithes', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('syndic of tithes', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('syndic of tithes', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('syndicate enforcer', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('syndicate enforcer', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('syndicate enforcer', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('syphon life', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('syphon life', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('syphon life', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('syphon life', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('syphon sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('syphon sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('szadek, lord of secrets', '2005-10-01', 'If another effect would prevent Szadek\'s combat damage from being dealt to the defending player or replace it with something else, that player chooses which effect applies first. For example, the player can choose to first have Mending Hands prevent 4 of the damage and then apply Szadek\'s ability so that it gets only one counter.').

card_ruling('séance', '2011-01-22', 'The copy doesn\'t have haste (unless the card it\'s copying has haste) and usually won\'t be able to attack.').

