% Rulings

card_ruling('fa\'adiyah seer', '2007-02-01', 'If the draw is replaced by another effect, none of the rest of Fa\'adiyah Seer\'s ability applies, even if the draw is replaced by another draw (such as with Enduring Renewal).').
card_ruling('fa\'adiyah seer', '2007-02-01', 'To avoid confusion, reveal the card you draw before putting it with the rest of the cards in your hand.').
card_ruling('fa\'adiyah seer', '2007-02-01', 'This is the timeshifted version of Sindbad.').

card_ruling('fabled hero', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('fabled hero', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('fabled hero', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('faceless butcher', '2006-09-25', 'Three Faceless Butchers cast in succession with no other creatures on the battlefield will result in an infinite loop and the game will be a draw, unless a player somehow interrupts the loop.').

card_ruling('faceless devourer', '2006-09-25', 'Three Faceless Devourers cast in succession with no other creatures with shadow on the battlefield will result in an infinite loop and the game will be a draw, unless a player somehow interrupts the loop.').

card_ruling('faces of the past', '2004-10-04', 'You choose whether to tap or untap during resolution.').
card_ruling('faces of the past', '2004-10-04', 'You either tap all of them or untap all of them. You can\'t tap some and untap others.').

card_ruling('fact or fiction', '2004-10-04', 'You choose which pile to put into your hand.').
card_ruling('fact or fiction', '2004-10-04', 'The revealed cards are revealed in order, so that all players know what order they were on the library.').

card_ruling('fade away', '2004-10-04', 'At the beginning of the resolution, each player counts up the number of creatures they control, then they make the required number of payments and/or sacrifices. The active player announces their choices first followed by the other players, but all the sacrifices are done at the same time. Because of this, you can\'t sacrifice one creature in order to pay for another and thereby avoid payment for the first.').
card_ruling('fade away', '2004-10-04', 'You can choose to pay some mana and sacrifice some permanents if you have more than one creature.').
card_ruling('fade away', '2004-10-04', 'You can activate mana abilities during the resolution if you decide to pay mana.').

card_ruling('fade from memory', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('faerie conclave', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('faerie conclave', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('faerie impostor', '2012-10-01', 'Returning another creature you control to its owner\'s hand is optional. You may choose to sacrifice Faerie Impostor, even if you could return a creature.').

card_ruling('faerie macabre', '2008-05-01', 'Faerie Macabre\'s second ability can be activated only while it\'s in your hand. You can do this any time you could cast an instant.').
card_ruling('faerie macabre', '2008-05-01', 'The targeted cards may be in the same graveyard or in different graveyards.').

card_ruling('faerie miscreant', '2015-06-22', 'Faerie Miscreant’s triggered ability checks to see if you control another creature named Faerie Miscreant at the time the new Faerie Miscreant enters the battlefield. If you don’t, the ability won’t trigger at all. The ability will check again as it tries to resolve. If, at that time, you don’t control another creature named Faerie Miscreant, the ability will have no effect.').
card_ruling('faerie miscreant', '2015-06-22', 'If multiple Faerie Miscreants enter the battlefield at the same time, all of their triggered abilities will trigger.').

card_ruling('faerie tauntings', '2007-10-01', 'You choose either to have each opponent lose 1 life or to have no opponent lose any life. You don\'t choose for each opponent individually.').

card_ruling('faith\'s fetters', '2005-10-01', 'Faith\'s Fetters doesn\'t stop static abilities, triggered abilities, or mana abilities from working. (A \"mana ability\" is an ability that produces mana, not an ability that costs mana.)').
card_ruling('faith\'s fetters', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('faith\'s reward', '2012-07-01', 'A permanent card is a card that could be put onto the battlefield. Specifically, it means an artifact, creature, enchantment, land, or planeswalker card (instant and sorcery cards aren\'t permanent cards).').
card_ruling('faith\'s reward', '2012-07-01', 'Permanent cards that were put into your graveyard from anywhere else, such as a card that was discarded, stay in the graveyard. Permanent cards that you gained control of and went to another player\'s graveyard from the battlefield will likewise stay in the graveyard.').
card_ruling('faith\'s reward', '2012-07-01', 'You choose what an Aura card put onto the battlefield this way will enchant. You can\'t choose any permanent cards entering the battlefield at the same time as that Aura. If there\'s nothing legal for the Aura to enchant, it stays in the graveyard.').
card_ruling('faith\'s reward', '2012-07-01', 'Permanent spells that were countered earlier in the turn never entered the battlefield, so they won\'t return to the battlefield because of Faith\'s Reward.').

card_ruling('faith\'s shield', '2011-01-22', 'Faith\'s Shield targets a permanent regardless of how much life you have. If that permanent is an illegal target when Faith\'s Shield tries to resolve, Faith\'s Shield will be countered and none of its effects will happen. Neither you nor any of your permanents will gain protection from a color.').
card_ruling('faith\'s shield', '2011-01-22', 'You choose only one color (not one color per player and permanent). You choose the color as Faith\'s Shield resolves.').
card_ruling('faith\'s shield', '2011-01-22', 'A player with protection from a color can\'t be damaged, enchanted (including by Curses), or targeted by anything of that color.').

card_ruling('falkenrath aristocrat', '2011-01-22', 'The ability checks whether the sacrificed creature as it last existed on the battlefield was a Human. (It doesn\'t matter what its creature types are in the graveyard.)').
card_ruling('falkenrath aristocrat', '2011-01-22', 'You can sacrifice Falkenrath Aristocrat itself to activate its ability.').

card_ruling('falkenrath torturer', '2011-01-22', 'The ability checks whether the sacrificed creature as it last existed on the battlefield was a Human. (It doesn\'t matter what its creature types are in the graveyard.)').
card_ruling('falkenrath torturer', '2011-01-22', 'You can sacrifice Falkenrath Torturer itself to activate its ability.').

card_ruling('fall of the gavel', '2012-10-01', 'You must be able to target a spell to cast Fall of the Gavel.').
card_ruling('fall of the gavel', '2012-10-01', 'If the target spell is an illegal target when Fall of the Gavel tries to resolve (perhaps because it\'s been countered by another spell), Fall of the Gavel will be countered and none of its effects will happen. You won\'t gain 5 life.').

card_ruling('fall of the hammer', '2014-02-01', 'As Fall of the Hammer tries to resolve, if only one of the targets is legal, Fall of the Hammer will still resolve but will have no effect: If the first target creature is illegal, it can’t deal damage to anything. If the second target creature is illegal, it can’t be dealt damage.').
card_ruling('fall of the hammer', '2014-02-01', 'The amount of damage dealt is based on the first target creature’s power as Fall of the Hammer resolves.').

card_ruling('fallen angel', '2004-10-04', 'Can sacrifice itself.').

card_ruling('fallen cleric', '2004-10-04', 'Protection from clerics prevents damage from cleric sources, can\'t have any clerics assigned to block it, and can\'t be targeted by the abilities of clerics.').

card_ruling('fallen ferromancer', '2011-06-01', 'If Fallen Ferromancer\'s ability deals 1 damage to a creature, that damage will cause a -1/-1 counter to be placed on the creature. If it deals 1 damage to a player, that player gets a poison counter.').

card_ruling('fallen ideal', '2006-09-25', 'The creature\'s controller (not Fallen Ideal\'s controller) can activate the \"sacrifice a creature\" ability.').

card_ruling('falling star', '2004-10-04', 'It must flip like a coin and not like a Frisbee.').
card_ruling('falling star', '2004-10-04', 'Only cards touched when it stops moving are affected. Not ones touched while it is moving.').

card_ruling('falling timber', '2004-10-04', 'You pick a second target only if you choose to kick Falling Timber.').

card_ruling('fallow wurm', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('fallow wurm', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('fallowsage', '2007-10-01', 'This is a triggered ability, not an activated ability. It doesn\'t allow you to tap the creature whenever you want; rather, you need some other way of tapping it, such as by attacking with the creature.').
card_ruling('fallowsage', '2007-10-01', 'For the ability to trigger, the creature has to actually change from untapped to tapped. If an effect attempts to tap the creature, but it was already tapped at the time, this ability won\'t trigger.').

card_ruling('false dawn', '2004-10-04', 'This card\'s ability does not change the color of any permanents.').
card_ruling('false dawn', '2004-10-04', 'This card\'s ability does not change any lands into Plains.').

card_ruling('false orders', '2004-10-04', 'If a creature is removed from being a blocker of a given attacker, any triggered abilities that would have happened because it was declared as a blocker still happen.').

card_ruling('false prophet', '2004-10-04', 'False Prophet only exiles creatures, which means only creature permanents which are on the battlefield. It won\'t exile itself since it is already in the graveyard.').

card_ruling('familiar ground', '2004-10-04', 'When combined with an effect that reads \"Each creature you control can\'t be blocked except by two or more creatures,\" none of your creatures can be blocked.').

card_ruling('fanatic of mogis', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('fanatic of mogis', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('fanatic of mogis', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('fanatic of mogis', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('fanatic of xenagos', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('fanatic of xenagos', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('fanatic of xenagos', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('fanatic of xenagos', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('fanatic of xenagos', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('fangren marauder', '2011-06-01', 'If Fangren Marauder has itself become an artifact for some reason, you\'ll gain 5 life when it is put into a graveyard from the battlefield.').

card_ruling('far', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('far', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('far', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('far', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('far', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('far', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('far', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('far', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('far', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('far', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('far', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('farbog boneflinger', '2011-01-22', 'Farbog Boneflinger\'s ability is mandatory. If it\'s the only creature on the battlefield, you must choose it as the target and give it -2/-2 until end of turn.').

card_ruling('farrel\'s mantle', '2004-10-04', 'If you put this on your opponent\'s creature, they decide whether or not to make their creature deal no damage to you.').
card_ruling('farrel\'s mantle', '2008-10-01', 'This ability resolves during the declare blockers step. If the damage dealt this way is enough to destroy an attacking or blocking creature, that other creature won\'t be around to deal its combat damage.').
card_ruling('farrel\'s mantle', '2008-10-01', 'If the same creature is enchanted with more than one Farrel\'s Mantle, they\'ll each trigger if that creature attacks and isn\'t blocked. The creature\'s controller may have it deal damage multiple times. As long as the enchanted creature\'s controller chooses to use Farrel\'s Mantle\'s ability at least once to deal damage, the enchanted creature will deal no combat damage.').
card_ruling('farrel\'s mantle', '2008-10-01', 'If the enchanted creature\'s controller chooses to have it deal no combat damage, that combat damage is not prevented. It\'s simply not dealt. A creature whose damage can\'t be prevented (such as Excruciator) would deal no combat damage this way.').
card_ruling('farrel\'s mantle', '2011-01-25', 'The enchanted creature\'s controller cannot choose to have the enchanted creature deal damage to itself. If there are no other creatures on the battlefield, then a legal target cannot be chosen so the ability does nothing.').
card_ruling('farrel\'s mantle', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('farrel\'s zealot', '2004-10-04', 'In a multiplayer game, the target creature does not need to be one of the ones controlled by the player you actually attacked.').
card_ruling('farrel\'s zealot', '2008-10-01', 'This ability resolves during the declare blockers step. If the damage dealt this way is enough to destroy an attacking or blocking creature, that other creature won\'t be around to deal its combat damage.').
card_ruling('farrel\'s zealot', '2008-10-01', 'If there are no other creatures on the battlefield, Farrel\'s Zealot will have to target itself. Of course, you can choose not to use the ability when it resolves.').
card_ruling('farrel\'s zealot', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('farseek', '2012-07-01', 'The card can have other land types, including Forest, as long as it also has one of the listed land types.').

card_ruling('farsight mask', '2004-12-01', 'This triggers each time a source an opponent controls deals damage to you. If the same source deals damage more than once in a turn, it triggers for each of those times.').
card_ruling('farsight mask', '2004-12-01', 'You draw no more than one card each time a source an opponent controls damages you, no matter how much damage the source deals.').
card_ruling('farsight mask', '2004-12-01', 'Farsight Mask must be untapped both when the damage is dealt and when you would draw the card.').
card_ruling('farsight mask', '2004-12-01', 'Each instance of each creature\'s combat damage is counted separately. If three creatures with double strike attack you and all of them are unblocked, you may draw up to six cards.').

card_ruling('fastbond', '2004-10-04', 'You take damage when you play a land using the \"play a land\" action. Such an action can be your regular \"play a land\", one enabled by Fastbond, or ones enabled through other effects.').
card_ruling('fastbond', '2004-10-04', 'You do not take damage when you \"put a land onto the battlefield\" through the effect of a spell or ability.').

card_ruling('fasting', '2008-10-01', 'When you resolve the beginning-of-upkeep triggered ability, you first put a hunger counter on Fasting, then you check how many hunger counters it has. If there are five or more, Fasting is destroyed.').
card_ruling('fasting', '2008-10-01', 'You choose whether or not to skip your draw step as that step begins. This ability is now separate from the beginning-of-upkeep triggered ability.').

card_ruling('fatal blow', '2008-04-01', 'Fatal Blow can\'t target a creature that hasn\'t been dealt damage this turn.').
card_ruling('fatal blow', '2008-04-01', 'Fatal Blow cares whether the creature had been dealt damage during the turn, not whether there is still damage on it. If the creature had regenerated, causing all damage to clear, Fatal Blow can still target and destroy it.').

card_ruling('fatal frenzy', '2007-02-01', 'If you don\'t control the affected creature at end of turn, it isn\'t sacrificed.').

card_ruling('fatal lore', '2004-10-04', 'The opponent chooses which option they want (this is a mode choice). If they choose the \"destroy\" option, you choose the zero, one, or two targets after that.').

card_ruling('fate foretold', '2013-09-15', 'If the target of an Aura is illegal when it tries to resolve, the Aura will be countered. The Aura doesn’t enter the battlefield, so you won’t get to draw a card.').

card_ruling('fate transfer', '2008-05-01', 'Fate Transfer moves any kind of counters, not just -1/-1 counters.').
card_ruling('fate transfer', '2008-05-01', 'This effect may result in useless counters being placed on a creature. For example, if an age counter is moved from a creature with cumulative upkeep to a creature without cumulative upkeep, it will have no effect on the new creature.').
card_ruling('fate transfer', '2008-05-01', 'If either one of the targets becomes illegal (because it leaves the battlefield or for any other reason) by the time Fate Transfer resolves, the counters don\'t move. If both targets become illegal, Fate Transfer is countered.').

card_ruling('fated conflagration', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('fated conflagration', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('fated conflagration', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('fated conflagration', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('fated infatuation', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('fated infatuation', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('fated infatuation', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('fated infatuation', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('fated infatuation', '2014-02-01', 'The token copies exactly what was printed on the original creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('fated infatuation', '2014-02-01', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('fated infatuation', '2014-02-01', 'If the copied creature is copying something else (for example, if the copied creature is a Clone), then the token enters the battlefield as whatever that creature copied.').
card_ruling('fated infatuation', '2014-02-01', 'If the copied creature is a token, the token created by Fated Infatuation copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('fated infatuation', '2014-02-01', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').

card_ruling('fated intervention', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('fated intervention', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('fated intervention', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('fated intervention', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('fated retribution', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('fated retribution', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('fated retribution', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('fated retribution', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('fated return', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('fated return', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('fated return', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('fated return', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('fated return', '2014-02-01', 'The effect that causes the creature to gain indestructible doesn’t have a duration. It lasts until the creature leaves the battlefield.').
card_ruling('fated return', '2014-02-01', 'The indestructible granted by Fated Return isn’t part of the creature’s copiable values. If the creature is copied, the copy won’t have indestructible (unless the creature otherwise has indestructible).').

card_ruling('fatespinner', '2004-12-01', 'That opponent skips only the phases or steps that are chosen.').
card_ruling('fatespinner', '2004-12-01', 'If there are multiple phases with the same name in the turn, your opponent skips them all. This is true even if a new step or phase is added to the turn after the ability resolves.').
card_ruling('fatespinner', '2004-12-01', 'If more than one Fatespinner affects you at the start of your upkeep, you can choose the same step or phase for each one.').

card_ruling('fatestitcher', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('fatestitcher', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('fatestitcher', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('fatestitcher', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('fatestitcher', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('fathom feeder', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('fathom feeder', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('fathom feeder', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('fathom feeder', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('fathom feeder', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('fathom feeder', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('fathom feeder', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('fathom mage', '2013-01-24', 'Fathom Mage\'s last ability will trigger whenever any +1/+1 counter is placed on it, not just ones due to the evolve ability.').
card_ruling('fathom mage', '2013-01-24', 'If multiple +1/+1 counters are placed on Fathom Mage simultaneously, its last ability will trigger once for each of those counters.').
card_ruling('fathom mage', '2013-01-24', 'If Fathom Mage enters the battlefield with a +1/+1 counter on it (perhaps due to Master Biomancer), its last ability will trigger once for each +1/+1 counter it entered the battlefield with.').
card_ruling('fathom mage', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('fathom mage', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('fathom mage', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('fathom mage', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('fathom mage', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('fathom mage', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('fathom trawl', '2007-10-01', 'If there are fewer than three nonland cards in your library, you will reveal your entire library, put all nonland cards into your hand, and put the rest back in any order.').

card_ruling('faultgrinder', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('favor of the mighty', '2007-10-01', 'If there\'s a tie for highest converted mana cost, all of those creatures have protection from all colors.').
card_ruling('favor of the mighty', '2007-10-01', 'This effect is checked continuously. If a new creature enters the battlefield with the highest converted mana cost, it gains protection from all colors and the previous highest loses it.').
card_ruling('favor of the mighty', '2007-10-01', 'If a creature has X in its mana cost, that X is treated as 0 for the purposes of this effect. It doesn\'t matter what the value of X was while the creature was on the stack.').

card_ruling('favor of the overbeing', '2008-08-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('favor of the woods', '2011-01-22', 'The ability will trigger only once per combat, even if the enchanted creature somehow blocks multiple attacking creatures.').

card_ruling('favored hoplite', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('favored hoplite', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('favored hoplite', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('fearsome awakening', '2014-11-24', 'No player can cast spells or activate abilities between returning the creature card to the battlefield and checking whether it’s a Dragon.').

card_ruling('fearsome temper', '2014-02-01', 'The activated ability granted by Fearsome Temper can’t change or undo a block that’s already happened. For it to have an effect, you must activate it no later than the declare attackers step.').
card_ruling('fearsome temper', '2014-02-01', 'The target creature can still block other attacking creatures.').

card_ruling('feast of blood', '2009-10-01', 'Whether you control two or more Vampires is checked only as you try to move Feast of Blood to the stack as the first step of casting it. It doesn\'t matter whether you still control two or more Vampires as you finish casting Feast of Blood (in case you somehow sacrifice one to produce mana) or as Feast of Blood resolves.').
card_ruling('feast of blood', '2009-10-01', 'If the targeted creature is an illegal target by the time Feast of Blood resolves, the entire spell is countered. You don\'t gain life.').

card_ruling('feast on the fallen', '2014-07-18', 'Damage dealt to an opponent causes that player to lose that much life.').
card_ruling('feast on the fallen', '2014-07-18', 'Feast on the Fallen looks at the entire previous turn to determine whether its ability triggers or not. It doesn’t matter whether Feast on the Fallen was on the battlefield when the opponent lost life.').
card_ruling('feast on the fallen', '2014-07-18', 'Feast on the Fallen checks only if an opponent lost life during the turn, not whether that player’s life total decreased over the course of the turn. For example, if an opponent lost 2 life and then gained 8 life last turn, Feast on the Fallen’s ability will trigger.').
card_ruling('feast on the fallen', '2014-07-18', 'Only one +1/+1 counter will be put on the target creature no matter how much life was lost.').

card_ruling('feat of resistance', '2014-09-20', 'You choose the color as Feat of Resistance resolves.').

card_ruling('feed the clan', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('feed the clan', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('feed the machine', '2010-06-15', 'The targeted player may choose \"self\" even if he or she can\'t perform the resulting action. For example, a player targeted with Feed the Machine may choose \"self\" even if he or she controls no creatures.').
card_ruling('feed the machine', '2010-06-15', 'The targeted player may choose \"others\" even if there are no others (because all of his or her teammates have lost the game, for example), or the archenemy\'s other opponents can\'t perform the resulting action.').
card_ruling('feed the machine', '2010-06-15', 'In a Supervillain Rumble game, the targeted player may still choose \"others.\" Each player who isn\'t the active player or the targeted player will thus be affected.').

card_ruling('feed the pack', '2011-01-22', 'You choose whether to sacrifice a nontoken creature and which creature to sacrifice when Feed the Pack\'s ability resolves.').
card_ruling('feed the pack', '2011-01-22', 'Check the sacrificed creature\'s toughness as it last existed on the battlefield to determine how many Wolves you put onto the battlefield.').

card_ruling('feeding grounds', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('feeding grounds', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('feeding grounds', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('feeding grounds', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('feeding grounds', '2009-10-01', 'Spells that are both red and green cost {2} less to cast.').

card_ruling('feeling of dread', '2011-09-22', 'If Feeling of Dread targets two creatures, and one of them is an illegal target by the time Feeling of Dread resolves, the other creature will still be tapped.').

card_ruling('feldon of the third path', '2014-11-07', 'The token copies exactly what was printed on the original creature card (except that the copy is also an artifact) and nothing else.').
card_ruling('feldon of the third path', '2014-11-07', 'If the copied creature card has {X} in its mana cost, X is 0.').
card_ruling('feldon of the third path', '2014-11-07', 'Any enters-the-battlefield abilities of the copied creature card will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature card will also work.').
card_ruling('feldon of the third path', '2014-11-07', 'If another creature becomes a copy of, or enters the battlefield as a copy of, the token, that creature will copy the creature card the token is copying, except it will also be an artifact. This is true even if the creature card that was copied by Feldon of the Third Path is no longer in the graveyard at that time. However, the new copy won’t gain haste and you won’t sacrifice it at the beginning of the next end step.').
card_ruling('feldon of the third path', '2014-11-07', 'If Feldon’s ability creates multiple tokens due to a replacement effect (such as the one Doubling Season creates), each of those tokens will gain haste and you’ll sacrifice each of them.').

card_ruling('felhide brawler', '2014-02-01', 'Whether you control another Minotaur is checked only as you declare blockers. The other Minotaur doesn’t have to block.').

card_ruling('felhide spiritbinder', '2014-02-01', 'The token copies exactly what was printed on the original creature (except that the copy is also an enchantment) and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If the copied creature is copying something else (for example, if the copied creature is a Clone), then the token enters the battlefield as whatever that creature copied.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If the copied creature is a token, the token created by Felhide Spiritbinder copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('felhide spiritbinder', '2014-02-01', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If another creature becomes or enters the battlefield as a copy of the token, that creature won’t have haste and you won’t exile it. However, if Felhide Spiritbinder creates multiple tokens due to a replacement effect (like the one Doubling Season creates), each of those tokens will have haste and you’ll exile each of them.').
card_ruling('felhide spiritbinder', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('felhide spiritbinder', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('felhide spiritbinder', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('felidar sovereign', '2015-08-25', 'Felidar Sovereign’s triggered ability checks to see if you have 40 or more life as your upkeep begins. If you don’t, the ability won’t trigger at all. If you do, the ability will check again as it tries to resolve. If you don’t have 40 or more life at that time, the ability won’t do anything.').
card_ruling('felidar sovereign', '2015-08-25', 'In a Two-Headed Giant game, your life total is the same as your team’s life total. As such, the ability triggers if your team has 40 or more life.').

card_ruling('felidar umbra', '2012-06-01', 'If Felidar Umbra is enchanting a creature with first strike, you can activate its ability and attach it to a creature without first strike during the first combat damage step. Each of those creatures will have lifelink when dealing combat damage and you\'ll gain life accordingly.').
card_ruling('felidar umbra', '2012-06-01', 'It\'s not possible for Felidar Umbra to save two creatures that are being destroyed simultaneously.').

card_ruling('fell shepherd', '2013-10-17', 'Creature cards that were put into your graveyard from any zone other than the battlefield, such as a card that was discarded, won’t return to your hand.').
card_ruling('fell shepherd', '2013-10-17', 'Creatures dealt lethal damage at the same time Fell Shepherd deals combat damage to a player will also be returned to your hand from your graveyard. If Fell Shepherd is such a creature (perhaps because it gained trample, attacked a player, and was blocked), it will be one of the creature cards returned to your hand.').
card_ruling('fell shepherd', '2013-10-17', 'You can choose a creature as the target of the last ability and sacrifice that same creature to activate the ability.').

card_ruling('fell the mighty', '2014-11-07', 'Use the power of the target creature as Fell the Mighty resolves to determine which creatures are destroyed. Fell the Mighty doesn’t target any of those creatures, so a creature with hexproof or protection from white will be destroyed if its power is high enough.').
card_ruling('fell the mighty', '2014-11-07', 'Fell the Mighty won’t destroy the target creature.').
card_ruling('fell the mighty', '2014-11-07', 'If the target creature is an illegal target when Fell the Mighty tries to resolve, Fell the Mighty will be countered and none of its effects will happen. No creatures will be destroyed.').

card_ruling('fellwar stone', '2004-10-04', 'It only produces one mana even if the land can produce more than one.').
card_ruling('fellwar stone', '2004-10-04', 'This works even if the opponent\'s lands are tapped. It only checks what kinds of mana can be produced, not if the abilities that produce them are usable right now.').
card_ruling('fellwar stone', '2004-10-04', 'The ability can be activated if the opponent has no lands that produce mana, but the effect will not be able to generate any mana.').
card_ruling('fellwar stone', '2009-10-01', 'The colors of mana are white, blue, black, red, and green. Fellwar Stone can\'t be tapped for colorless mana, even if a land an opponent controls could produce colorless mana.').
card_ruling('fellwar stone', '2009-10-01', 'Fellwar Stone checks the effects of all mana-producing abilities of lands your opponents control, but it doesn\'t check their costs. For example, Vivid Crag has the ability \"{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.\" If an opponent controls Vivid Crag and you control Fellwar Stone, you can tap Fellwar Stone for any color of mana. It doesn\'t matter whether Vivid Crag has a charge counter on it, and it doesn\'t matter whether it\'s untapped.').
card_ruling('fellwar stone', '2009-10-01', 'When determining what colors of mana your opponents\' lands could produce, take into account any applicable replacement effects that would apply to those lands\' mana abilities (such as Contamination\'s effect, for example). If there is more than one, consider them in any possible order.').
card_ruling('fellwar stone', '2009-10-01', 'Fellwar Stone doesn\'t care about any restrictions or riders your opponents\' lands (such as Ancient Ziggurat or Hall of the Bandit Lord) put on the mana they produce. It just cares about colors of mana.').

card_ruling('femeref enchantress', '2005-08-01', 'If it goes to the graveyard, its ability will not trigger because of any Auras on herself unless those Auras are being destroyed by the same effect that is destroying the Enchantress. This is because those Auras are not put into the graveyard until after she is in the graveyard.').

card_ruling('fencer\'s magemark', '2006-02-01', 'Each Magemark gives its bonus to the creature it\'s attached to, as long as the same player controls both the creature and the Aura.').

card_ruling('fend off', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('fendeep summoner', '2008-04-01', 'You may target zero, one, or two Swamps with this ability.').
card_ruling('fendeep summoner', '2008-04-01', 'A Swamp affected by this ability is still a land, still a Swamp, and may still be tapped for black mana. If it had any other card types or subtypes, it retains those as well. This ability doesn\'t affect the permanent\'s colors, if any.').
card_ruling('fendeep summoner', '2008-04-01', 'A Swamp affected by this ability is subject to the \"summoning sickness\" rule: If its controller hasn\'t continuously controlled it since the beginning of his or her most recent turn, it can\'t attack or be tapped for mana (or use any other {T} abilities).').

card_ruling('feral animist', '2013-04-15', 'You don\'t choose the value of X. The bonus Feral Animist gets is based on its power when the ability resolves. For example, if you activate the ability twice, the first ability will give it +2/+0 and the second one will give it +4/+0.').
card_ruling('feral animist', '2013-04-15', 'If Feral Animist\'s power is negative when its ability resolves, it will lose power. For example, if it\'s somehow -3/1 when the ability resolves, it will become -6/1.').

card_ruling('feral contest', '2010-03-01', 'You must choose two targets as you cast Feral Contest: a creature you control and any other creature. If you can\'t (because there\'s only one creature on the battlefield, perhaps), then you can\'t cast the spell. Note that the second target may also be a creature you control.').
card_ruling('feral contest', '2010-03-01', 'If just the first targeted creature is an illegal target by the time Feral Contest resolves, it won\'t get a +1/+1 counter. However, the second targeted creature is still affected by the blocking restriction the spell imposes, so it\'ll have to block the first targeted creature that turn if able.').
card_ruling('feral contest', '2010-03-01', 'If just the second targeted creature is an illegal target by the time Feral Contest resolves, the first targeted creature will get a +1/+1 counter, but the second targeted creature won\'t have to block it that turn.').
card_ruling('feral contest', '2010-03-01', 'Casting Feral Contest doesn\'t force you to attack with the first targeted creature that turn. If that creature doesn\'t attack, the second targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('feral contest', '2010-03-01', 'If you attack with the first targeted creature but the second targeted creature isn\'t able to block it (for example, because the first targeted creature has flying and the second one doesn\'t), the requirement to block does nothing. The second targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('feral contest', '2010-03-01', 'Tapped creatures, creatures that can\'t block as the result of an effect, creatures with unpaid costs to block (such as those from War Cadence), and creatures that aren\'t controlled by the defending player are exempt from effects that would require them to block. Such creatures can be targeted by Feral Contest, but the requirement to block does nothing.').

card_ruling('feral incarnation', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('feral incarnation', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('feral incarnation', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('feral incarnation', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('feral incarnation', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('feral incarnation', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('feral incarnation', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('feroz\'s ban', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('ferropede', '2004-12-01', 'Any permanent is a legal target for Ferropede\'s last ability, not just one with a counter on it.').
card_ruling('ferropede', '2004-12-01', 'If there\'s more than one type of counter on the targeted permanent, Ferropede\'s controller chooses which counter to remove from it.').

card_ruling('fertile ground', '2004-10-04', 'The color of mana to be generated is chosen when tapping the land for mana.').

card_ruling('fertile thicket', '2015-08-25', 'You don’t have to reveal a basic land card from among the five cards you look at, even if one is there. If you don’t, you won’t put a card on top of your library.').

card_ruling('fertilid', '2008-04-01', 'Although the targeted player doesn\'t need to find a basic land card if he or she doesn\'t want to, that player must shuffle his or her library.').

card_ruling('fervor', '2012-07-01', 'If an attacking creature loses haste, perhaps because Fervor leaves the battlefield after attackers have been declared, it won\'t be removed from combat.').

card_ruling('festering march', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('festering march', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('festering march', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('festering march', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('festering march', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('festering march', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('festering march', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('festering march', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('festering march', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('festering march', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('festering newt', '2013-07-01', 'Whether you control a creature named Bogbrew Witch is checked only as the triggered ability resolves.').

card_ruling('festering wound', '2004-10-04', 'If it is on your creature, you can decide whether to add a counter before or after it damages you.').
card_ruling('festering wound', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('festival of the guildpact', '2005-10-01', 'You draw a card when Festival of the Guildpact resolves, not when damage is prevented.').

card_ruling('fettergeist', '2012-05-01', 'The amount of mana you need to pay is determined when the triggered ability resolves.').

card_ruling('feudkiller\'s verdict', '2013-06-07', 'Feudkiller\'s Verdict doesn\'t target an opponent. If you have more life than any opponent when that part of the effect happens, you\'ll put the token onto the battlefield.').

card_ruling('field of dreams', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('field of dreams', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('field of dreams', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('field of dreams', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('field of souls', '2004-10-04', 'If you have two of these on the battlefield, both will trigger when a non-token creature goes to the graveyard.').
card_ruling('field of souls', '2004-10-04', 'This only triggers on creatures going to the graveyard from the battlefield.').

card_ruling('fieldmist borderpost', '2009-05-01', 'Casting this card by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast an artifact spell. It also doesn\'t change the spell\'s mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('fieldmist borderpost', '2009-05-01', 'Effects that increase or reduce the cost to cast this card will apply to whichever cost you chose to pay.').
card_ruling('fieldmist borderpost', '2009-05-01', 'To satisfy the alternative cost, you may return any basic land you control to its owner\'s hand, regardless of that land\'s subtype or whether it\'s tapped.').
card_ruling('fieldmist borderpost', '2009-05-01', 'As you cast a spell, you get a chance to activate mana abilities before you pay that spell\'s costs. Therefore, you may tap a basic land for mana, then both spend that mana and return that land to your hand to pay this card\'s alternative cost. (Of course, you can return a different basic land instead.)').

card_ruling('fields of summer', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('fields of summer', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('fields of summer', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('fields of summer', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('fiend hunter', '2011-09-22', 'If Fiend Hunter leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and exile the targeted creature indefinitely.').

card_ruling('fiend of the shadows', '2011-01-22', 'Playing a card exiled with Fiend of the Shadows follows all the normal rules for playing that card. You must pay its costs, and you must follow all timing restrictions, for example.').
card_ruling('fiend of the shadows', '2011-01-22', 'You may only play the card, meaning play it as a land or cast it as a spell. It\'s not in your hand. You can\'t discard it, activate its cycling abilities, and so on.').
card_ruling('fiend of the shadows', '2011-01-22', 'As long as the card remains exiled, you may still play it, even if Fiend of the Shadows leaves the battlefield or changes controllers.').
card_ruling('fiend of the shadows', '2011-01-22', 'If this creature deals combat damage to multiple players simultaneously, perhaps because some combat damage was redirected, its ability will trigger for each of those players.').

card_ruling('fiendslayer paladin', '2013-07-01', 'Fiendslayer Paladin can be affected by black or red spells that don’t target it. For example, a red spell that “deals 3 damage to each creature” would deal damage to Fiendslayer Paladin.').

card_ruling('fierce invocation', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('fierce invocation', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('fierce invocation', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('fierce invocation', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('fierce invocation', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('fierce invocation', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('fierce invocation', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('fierce invocation', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('fierce invocation', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('fierce invocation', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('fierce invocation', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('fierce invocation', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('fierce invocation', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('fiery bombardment', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('fiery bombardment', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').

card_ruling('fiery conclusion', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('fiery conclusion', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('fiery fall', '2009-02-01', 'Unlike the normal cycling ability, basic landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a basic land card. You don\'t choose the type of basic land card you\'ll find until you\'re performing the search. After you choose a basic land card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('fiery fall', '2009-02-01', 'Basic landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being basic landcycled. Any ability that stops a cycling ability from being activated also stops a basic landcycling ability from being activated.').
card_ruling('fiery fall', '2009-02-01', 'Basic landcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with basic landcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('fiery fall', '2009-02-01', 'You can choose not to find a basic land card, even if there is one in your library.').

card_ruling('fiery gambit', '2004-12-01', 'You must choose a target creature when you cast Fiery Gambit. If that target isn\'t legal on resolution, Fiery Gambit has no effect and you don\'t even flip a coin.').
card_ruling('fiery gambit', '2004-12-01', 'You can flip any number of coins (you can even flip more than three), but Fiery Gambit has no effect if you lose any of the flips. You can\'t continue flipping if you lose a flip.').
card_ruling('fiery gambit', '2004-12-01', 'If you win three flips, Fiery Gambit deals 3 damage to the target creature and 6 damage to each opponent, and you draw nine cards and untap all lands you control.').
card_ruling('fiery gambit', '2004-12-01', 'After each flip, you choose whether to continue flipping.').

card_ruling('fiery impulse', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('fiery justice', '2004-10-04', 'You must assign at least 1 damage to each target.').
card_ruling('fiery justice', '2004-10-04', 'You can\'t choose zero targets. You must choose at least one target.').
card_ruling('fiery justice', '2006-09-25', 'The opponent targeted for the life gain effect may also have been targeted by the damage effect. If this happens and the damage effect drops that player\'s life total to 0 or less, the life gain effect will raise his or her life total above 0 again before the player would lose the game.').

card_ruling('fight or flight', '2008-08-01', 'Creatures which enter the battlefield (or cards which become creatures) after the beginning of combat ability resolves are not placed in either pile. Since they weren\'t in the chosen pile, they won\'t be able to attack.').
card_ruling('fight or flight', '2008-08-01', 'If you have two of these, then you will separate the creatures into piles twice, and the opponent will choose a pile each time. Only creatures that were in both of the chosen piles can attack.').
card_ruling('fight or flight', '2008-08-01', 'The effect is best understood if you read it as \"creatures not in the chosen pile can\'t attack.\"').

card_ruling('fight to the death', '2009-05-01', 'A \"blocking creature\" is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures that it was blocking are no longer on the battlefield or have otherwise left combat by then.').
card_ruling('fight to the death', '2009-05-01', 'A \"blocked creature\" is an attacking creature that has been blocked by a creature this combat, or has become blocked as the result of a spell or ability this combat. Unless the attacking creature leaves combat, it continues to be a blocked creature through the end of combat step, even if the creature or creatures that blocked it are no longer on the battlefield or have otherwise left combat by then.').
card_ruling('fight to the death', '2009-05-01', 'Attacking creatures that haven\'t been blocked are unaffected by Fight to the Death.').
card_ruling('fight to the death', '2009-05-01', 'If Fight to the Death is cast before blockers are declared or after combat ends, it won\'t do anything.').

card_ruling('figure of destiny', '2008-08-01', 'None of these abilities has a duration. If one of them resolves, it will remain in effect until the game ends, Figure of Destiny leaves the battlefield, or some subsequent effect changes its characteristics, whichever comes first.').
card_ruling('figure of destiny', '2008-08-01', 'Figure of Destiny\'s abilities overwrite its power, toughness, and creature types. Typically, those abilities are activated in the order they appear on the card. However, if Figure of Destiny is an 8/8 Kithkin Spirit Warrior Avatar with flying and first strike, and you activate its first ability, it will become a 2/2 Kithkin Spirit that still has flying and first strike.').
card_ruling('figure of destiny', '2008-08-01', 'You can activate Figure of Destiny\'s second and third abilities regardless of what creature types it is. Each of those abilities checks Figure of Destiny\'s creature types when that ability resolves. If Figure of Destiny isn\'t the appropriate creature type at that time, the ability does nothing.').
card_ruling('figure of destiny', '2008-08-01', 'Figure of Destiny\'s second ability checks whether it\'s a Spirit, and its third ability checks whether it\'s a Warrior. It doesn\'t matter how it became the appropriate creature type.').
card_ruling('figure of destiny', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('filigree angel', '2009-05-01', 'Filigree Angel\'s enters-the-battlefield ability counts Filigree Angel itself, assuming that it\'s still on the battlefield and still an artifact by the time the ability resolves.').

card_ruling('filigree sages', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('final fortune', '2004-10-04', 'If you end up skipping the extra turn that is gained, you do not lose the game.').
card_ruling('final fortune', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('final punishment', '2004-10-04', 'Damage that is prevented does not count toward the total.').

card_ruling('final strike', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('final strike', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('final-sting faerie', '2008-04-01', 'The effect is mandatory. If you control the only creature that\'s been dealt damage this turn, you must target that creature.').
card_ruling('final-sting faerie', '2008-04-01', 'The ability cares whether a creature has been dealt damage, not whether it still has damage on it. You can target a creature that was damaged and regenerated.').

card_ruling('finest hour', '2009-05-01', 'Finest Hour\'s second ability has an \"intervening \'if\' clause.\" The ability won\'t trigger at all unless it\'s the first combat phase of the turn.').
card_ruling('finest hour', '2009-05-01', 'Unlike other similar cards, Finest Hour doesn\'t create a main phase in between the two combat phases. The second combat phase will begin immediately after the current one ends.').

card_ruling('fire at will', '2008-08-01', 'The number of targets must be at least 1 and at most 3. You divide the damage as you cast Fire at Will, not as it resolves. Each target must be assigned at least 1 damage.').
card_ruling('fire at will', '2008-08-01', 'You may target both attacking creatures and blocking creatures as you cast Fire at Will.').

card_ruling('fire bowman', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('fire covenant', '2007-09-16', 'If X is 0, the number of targets must also be 0. If X is greater than 0, the number of targets must be at least 1 and at most X. You must distribute at least 1 damage to each target.').
card_ruling('fire covenant', '2007-09-16', 'You divide the damage as you cast the spell. You can\'t redistribute the damage if any of the target creatures becomes illegal before the spell resolves.').

card_ruling('fire dragon', '2008-10-01', 'If there are no other creatures on the battlefield, Fire Dragon will have to target itself. The ability is mandatory.').

card_ruling('fire servant', '2010-08-15', 'Fire Servant\'s ability applies no matter who or what the damage would be dealt to: a creature, a player, or a planeswalker.').
card_ruling('fire servant', '2010-08-15', 'If a red instant or sorcery spell you control causes damage to be dealt, that spell will always identify the source of the damage. In most cases, the source is the spell itself. For example, Lightning Bolt says \"Lightning Bolt deals 3 damage to target creature or player.\" Such a spell is affected by Fire Servant\'s ability. If the source of the damage is something else (for example, if the spell is Soul\'s Fire, which says \"Target creature you control on the battlefield deals damage equal to its power to target creature or player\"), it isn\'t affected by Fire Servant\'s ability.').
card_ruling('fire servant', '2010-08-15', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage or the controller of the permanent that would be dealt damage chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn\" and Lava Axe is a red sorcery that says \"Lava Axe deals 5 damage to target player.\" Suppose a Lava Axe controlled by a player who controls Fire Servant would deal 5 damage to a player who has cast Mending Hands targeting him or herself. The player who would be dealt damage can either (a) prevent 4 damage first and then let Fire Servant\'s effect double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').
card_ruling('fire servant', '2010-08-15', 'If a red instant or sorcery spell you control divides damage among multiple recipients (such as Fireball does), the damage is divided before Fire Servant\'s effect doubles it.').
card_ruling('fire servant', '2010-08-15', 'If you control more than one Fire Servant, the effects are cumulative. Two such effects will cause damage from red instant or sorcery spells you control to be multiplied by four; three such effects will cause damage from red instant or sorcery spells you control to be multiplied by eight.').

card_ruling('fire whip', '2004-10-04', 'Remember that you tap the creature as part of the cost of activating Fire Whip\'s granted ability. So, if you have two Fire Whips on a creature activating the first one will tap the creature, so you can\'t use the second one or any other ability which requires tapping the creature until you find a way to untap it.').
card_ruling('fire whip', '2004-10-04', 'Fire Whip is put into the graveyard if you lose control of the creature since the card text says it can only enchant a creature you control.').

card_ruling('fire-field ogre', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('fire-field ogre', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('fire-field ogre', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('fire-field ogre', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('fire-field ogre', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('fireball', '2004-12-01', 'The additional cost is paid when you pay the rest of Fireball\'s costs, even though that text appears second on the card (to make the card easier to read). The additional cost doesn\'t count toward Fireball\'s converted mana cost.').
card_ruling('fireball', '2004-12-01', 'If, for example, X is 5 and you choose three target creatures, Fireball has a total cost of {7}{R} (even though its mana cost is just {5}{R}). If those creatures are all still legal targets as Fireball resolves, it deals 1 damage to each of them.').
card_ruling('fireball', '2009-05-01', 'You may cast Fireball with zero targets, regardless of the value chosen for X. If you do so, it will not be a targeted spell, and no damage will actually be dealt when it resolves.').
card_ruling('fireball', '2009-10-01', 'Fireball\'s damage is divided as Fireball resolves, not as it\'s cast, because there are no choices involved. The division involves only targets that are still legal at that time.').
card_ruling('fireball', '2009-10-01', 'You can target more than X creatures. However, if the number of legal targets at the time Fireball resolves is greater than X, none of them will be dealt any damage.').

card_ruling('firebrand ranger', '2004-10-04', 'This does not count as your one land you can normally play each turn.').
card_ruling('firebrand ranger', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('firebreathing', '2009-10-01', 'This ability can be activated by Firebreathing\'s controller, not the enchanted creature\'s controller (in case they\'re different players).').
card_ruling('firebreathing', '2009-10-01', 'The ability affects whichever creature is enchanted by Firebreathing at the time the ability resolves. The bonus remains even if Firebreathing stops enchanting that creature.').

card_ruling('firedrinker satyr', '2013-09-15', 'Firedrinker Satyr’s first ability will trigger even if it’s dealt lethal damage. For example, if it blocks a 7/7 creature, its ability will trigger and Firedrinker Satyr will deal 7 damage to you.').
card_ruling('firedrinker satyr', '2013-09-15', 'Damage dealt by Firedrinker Satyr due to its first ability isn’t combat damage, even if combat damage caused the ability to trigger.').

card_ruling('firefiend elemental', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('firefiend elemental', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('firefiend elemental', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('firefist striker', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('firefist striker', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('firefist striker', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('firemane angel', '2005-10-01', 'If you return Firemane Angel from your graveyard to the battlefield during your upkeep before its life-gain ability resolves, you won\'t gain 1 life. (Most of the time, players will let the triggered ability resolve before activating its activated ability.)').
card_ruling('firemane angel', '2006-07-01', 'If Firemane Angel changes zones after its ability has triggered but before it resolves, the ability will do nothing.').

card_ruling('firemane avenger', '2013-01-24', 'If the target creature or player is an illegal target when Firemane Avenger\'s battalion ability tries to resolve, the ability will be countered and none of its effects will happen. You won\'t gain life.').
card_ruling('firemane avenger', '2013-01-24', 'If Firemane Avenger\'s battalion ability resolves but some or all of the damage is prevented, you\'ll still gain 3 life.').
card_ruling('firemane avenger', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('firemane avenger', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('firemane avenger', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('firemantle mage', '2015-08-25', 'Multiple instances of menace are redundant.').
card_ruling('firemantle mage', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('firemantle mage', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('firemind\'s foresight', '2012-10-01', 'You don\'t have to find any instant cards in your library, even if they are there.').
card_ruling('firemind\'s foresight', '2012-10-01', 'If a card in your library has {X} in its mana cost, X is 0. For example, a card with mana cost {X}{R} has a converted mana cost of 1, and so on.').

card_ruling('firespout', '2008-05-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('firespout', '2008-05-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('firespout', '2008-05-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').

card_ruling('firestorm', '2008-04-01', 'For example, if you discard four cards, you must target four different creatures and/or players. Firestorm will deal 4 damage to each of them.').
card_ruling('firestorm', '2008-04-01', 'The value of X can\'t exceed the number of cards in your hand or the number of available legal targets, whichever is smaller. X can be 0.').
card_ruling('firestorm', '2008-04-01', 'The same creature or player can\'t be targeted more than once.').

card_ruling('firestorm hellkite', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('firestorm phoenix', '2004-10-04', 'It may not be cast again until your next turn, but it can be put onto the battlefield by other effects.').
card_ruling('firestorm phoenix', '2004-10-04', 'The \"rebirth\" is a replacement effect.').
card_ruling('firestorm phoenix', '2009-10-01', 'The Firestorm Phoenix you return to your hand is tracked by its ability. As long as it remains in your hand, that particular card can\'t be played before your next turn begins, but a different Firestorm Phoenix in your hand can.').

card_ruling('firewake sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('firewake sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('firewild borderpost', '2009-05-01', 'Casting this card by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast an artifact spell. It also doesn\'t change the spell\'s mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('firewild borderpost', '2009-05-01', 'Effects that increase or reduce the cost to cast this card will apply to whichever cost you chose to pay.').
card_ruling('firewild borderpost', '2009-05-01', 'To satisfy the alternative cost, you may return any basic land you control to its owner\'s hand, regardless of that land\'s subtype or whether it\'s tapped.').
card_ruling('firewild borderpost', '2009-05-01', 'As you cast a spell, you get a chance to activate mana abilities before you pay that spell\'s costs. Therefore, you may tap a basic land for mana, then both spend that mana and return that land to your hand to pay this card\'s alternative cost. (Of course, you can return a different basic land instead.)').

card_ruling('firewing phoenix', '2012-07-01', 'Firewing Phoenix\'s ability can be activated only if Firewing Phoenix is in your graveyard.').

card_ruling('first response', '2014-07-18', 'Damage dealt to you causes you to lose that much life.').
card_ruling('first response', '2014-07-18', 'First Response looks at the entire previous turn to determine whether its ability triggers or not. It doesn’t matter whether First Response was on the battlefield when you lost life.').
card_ruling('first response', '2014-07-18', 'First Response checks only if you lost life during the turn, not whether your life total decreased over the course of the turn. For example, if you lost 2 life and then gained 8 life last turn, First Response’s ability will trigger.').
card_ruling('first response', '2014-07-18', 'Only one Soldier will be created no matter how much life was lost.').
card_ruling('first response', '2014-07-18', 'The Soldier won’t be able to attack the turn it’s created (unless something gives it haste).').

card_ruling('fissure vent', '2010-06-15', 'You may choose just the first mode (targeting an artifact), just the second mode (targeting a nonbasic land), or both modes (targeting an artifact and a nonbasic land). You can\'t choose a mode unless there\'s a legal target for it.').

card_ruling('fist of suns', '2004-12-01', 'This is an alternative cost to cast a spell.').
card_ruling('fist of suns', '2004-12-01', 'You can\'t pay more than one alternative cost for a spell. For example, you can\'t cast a spell using its flashback ability by paying {W}{U}{B}{R}{G}. Likewise, you can\'t pay {W}{U}{B}{R}{G} to cast a creature with morph face down.').
card_ruling('fist of suns', '2004-12-01', 'Note that combining this card with Mycosynth Lattice from the Darksteel set means that all spells can be cast for five mana of any type (colored or colorless).').
card_ruling('fist of suns', '2014-02-01', 'If you pay {W}{U}{B}{R}{G} rather than pay the mana cost of a spell with {X} in its mana cost, the only legal choice for X is 0.').

card_ruling('fists of ironwood', '2005-10-01', 'You control the Saprolings, even if Fists of Ironwood enters the battlefield attached to another player\'s creature.').
card_ruling('fists of ironwood', '2005-10-01', 'The Saprolings don\'t appear until after Fists of Ironwood is on the battlefield, so you can\'t cast Fists of Ironwood on one of those Saprolings.').

card_ruling('fists of the demigod', '2008-05-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('fit of rage', '2004-10-04', 'The +3/+3 is also only until end of turn.').

card_ruling('five-alarm fire', '2013-01-24', 'Five-Alarm Fire\'s first ability triggers once for each time a creature you control deals combat damage, even if it deals combat damage to more than one creature, planeswalker, or player. For example, an attacking creature with trample that deals combat damage to a blocking creature and the defending player will cause Five-Alarm Fire\'s ability to trigger once. A creature with double strike, however, may cause Five-Alarm Fire\'s ability to trigger twice during a single combat.').

card_ruling('flame fusillade', '2005-10-01', '\"Summoning sickness\" applies only to creatures, not to other permanents. You may tap a noncreature permanent to pay for an ability with {T} in its cost even if the permanent entered the battlefield that turn.').
card_ruling('flame fusillade', '2005-10-01', 'Tapping an Aura or Equipment doesn\'t tap the creature it\'s attached to and vice versa. Tapped enchantments work normally. Tapped artifacts work normally unless they say otherwise.').

card_ruling('flame jab', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('flame jab', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('flame jab', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('flame jab', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('flame javelin', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('flame javelin', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Flame Javelin is red even if you spend six green mana to cast it.').
card_ruling('flame javelin', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Flame Javelin has a converted mana cost of 6, even if you spend {R}{R}{R} to cast it.').
card_ruling('flame javelin', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Flame Javelin by spending {R}{R}{R}, {2}{R}{R}, {4}{R}, or {6}.').

card_ruling('flame jet', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('flame-kin war scout', '2006-05-01', 'If multiple creatures enter the battlefield at the same time, Flame-Kin War Scout\'s ability will trigger multiple times. When the first of these triggers resolves, if Flame Kin War Scout can be sacrificed by the ability\'s controller, it will be sacrificed and deal 4 damage. When the other triggers resolve, it won\'t be on the battlefield, so it can\'t be sacrificed and the other creatures won\'t be dealt any damage.').

card_ruling('flame-wreathed phoenix', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('flame-wreathed phoenix', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('flame-wreathed phoenix', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('flame-wreathed phoenix', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('flame-wreathed phoenix', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('flameblast dragon', '2008-10-01', 'You choose the target when the ability triggers. When the ability resolves, you choose a value for X and decide whether to pay {X}{R}. If you do decide to pay {X}{R}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('flameborn hellion', '2011-01-01', 'If, during your declare attackers step, Flameborn Hellion is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then it doesn\'t attack. If there\'s a cost associated with having Flameborn Hellion attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('flameborn hellion', '2011-01-01', 'If there are multiple combat phases in a turn, Flameborn Hellion must attack only in the first one in which it\'s able to.').

card_ruling('flamerush rider', '2014-11-24', 'You choose which opponent or opposing planeswalker the token is attacking as you put it onto the battlefield. It doesn’t have to be the same player or planeswalker Flamerush Rider is attacking.').
card_ruling('flamerush rider', '2014-11-24', 'Although the token is attacking, it was never declared as an attacking creature (for purposes of abilities that trigger whenever a creature attacks, for example).').
card_ruling('flamerush rider', '2014-11-24', 'The token copies exactly what was printed on the original creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature has any counters on it or Auras and/or Equipment attached to it, or any non-copy effects that changed its power, toughness, types, color, and so on. Notably, it doesn’t copy any effects that may have turned a noncreature permanent into a creature. If the token isn’t a creature as it enters the battlefield, it won’t be attacking.').
card_ruling('flamerush rider', '2014-11-24', 'If the copied creature had {X} in its mana cost, X is 0.').
card_ruling('flamerush rider', '2014-11-24', 'If the copied creature was copying something else, the token enters the battlefield as whatever that creature was copying.').
card_ruling('flamerush rider', '2014-11-24', 'If the copied creature is a token, the token created by Flamerush Rider copies the original characteristics of that token as stated by the effect that put it onto the battlefield.').
card_ruling('flamerush rider', '2014-11-24', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “As [this creature] enters the battlefield” or “[This creature] enters the battlefield with” abilities of the copied creature will also work.').
card_ruling('flamerush rider', '2014-11-24', 'If another creature becomes or enters the battlefield as a copy of the token, you won’t exile that creature at end of combat. However, if Flamerush Rider creates multiple tokens due to a replacement effect (like the one Doubling Season creates), you’ll exile each of those tokens.').
card_ruling('flamerush rider', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('flamerush rider', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('flamerush rider', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('flamerush rider', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('flames of the blood hand', '2005-02-01', 'The damage is unpreventable, and the targeted player\'s life gain that turn is replaced with nothing.').
card_ruling('flames of the blood hand', '2005-02-01', 'The replacement effect applies only to the target player, not to a creature or player the damage might be redirected to.').

card_ruling('flames of the firebrand', '2012-07-01', 'You divide the damage as you cast Flames of the Firebrand, not as it resolves. Each target must be assigned at least 1 damage. (In other words, as you put the spell on the stack, you choose whether to have it deal 3 damage to a single target, 2 damage to one target and 1 damage to another target, or 1 damage to each of three targets.)').
card_ruling('flames of the firebrand', '2012-07-01', 'You can\'t deal damage to both a player and a planeswalker controlled by that player with Flames of the Firebrand.').

card_ruling('flameshadow conjuring', '2015-06-22', 'The token copies exactly what’s printed on the original creature and nothing else (unless that creature is copying something else; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('flameshadow conjuring', '2015-06-22', 'If the copied creature has {X} in its mana cost, X is 0.').
card_ruling('flameshadow conjuring', '2015-06-22', 'If the copied creature is copying something else when the ability resolves, then the token enters the battlefield as a copy of whatever that creature is copying.').
card_ruling('flameshadow conjuring', '2015-06-22', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').
card_ruling('flameshadow conjuring', '2015-06-22', 'The token is exiled at the beginning of the next end step regardless of who controls it at that time.').
card_ruling('flameshadow conjuring', '2015-06-22', 'If the ability resolves during a turn’s end step, the token will be exiled at the beginning of the next turn’s end step.').
card_ruling('flameshadow conjuring', '2015-06-22', 'If the token isn’t exiled at the beginning of the next end step (perhaps because the delayed triggered ability is countered), it remains on the battlefield indefinitely. It continues to have haste.').
card_ruling('flameshadow conjuring', '2015-06-22', 'If another creature becomes or enters the battlefield as a copy of the token, that creature won’t have haste and it won’t be exiled.').

card_ruling('flameshot', '2004-10-04', 'You can\'t choose zero targets. You must choose at least one target.').

card_ruling('flamespeaker adept', '2013-09-15', 'Flamespeaker Adept’s ability triggers once for each scry instruction, no matter how many cards you’re scrying.').
card_ruling('flamespeaker adept', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('flamespeaker adept', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('flamespeaker adept', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('flamespeaker adept', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('flamespeaker\'s will', '2014-04-26', 'You decide whether to sacrifice Flamespeaker’s Will as its triggered ability resolves. If it’s not on the battlefield at that time, you can’t sacrifice it and destroy the target artifact.').
card_ruling('flamespeaker\'s will', '2014-04-26', 'If another player gains control of the enchanted creature, Flamespeaker’s Will will be put into its owner’s graveyard.').

card_ruling('flamestick courier', '2004-10-04', 'It checks the creature type when the ability is announced and resolved, but once the effect is placed on the creature, if its creature type changes the effect still continues.').

card_ruling('flametongue kavu', '2004-10-04', 'If it enters the battlefield as the only targetable creature, it must target itself.').

card_ruling('flamewake phoenix', '2014-11-24', 'Flamewake Phoenix’s ferocious ability triggers only if Flamewake Phoenix is in your graveyard and you control a creature with power 4 or greater at the beginning of combat on your turn. Additionally, the ability will check to see if you control a creature with power 4 or greater again as it resolves. If you don’t, the ability will have no effect.').
card_ruling('flamewake phoenix', '2014-11-24', 'You still choose which player or planeswalker Flamewake Phoenix attacks.').
card_ruling('flamewake phoenix', '2014-11-24', 'If, during your declare attackers step, Flamewake Phoenix is tapped or is affected by a spell or ability that says it can’t attack, then it doesn’t attack. If there’s a cost associated with having Flamewake Phoenix attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('flamewake phoenix', '2014-11-24', 'If Flamewake Phoenix enters the battlefield before the combat phase, it will attack that turn if able. If it enters the battlefield after combat, it won’t attack that turn and will usually be available to block on the following turn.').

card_ruling('flaring pain', '2004-10-04', 'Spells and abilities that replace or redirect damage are not affected by this card.').
card_ruling('flaring pain', '2004-10-04', 'The \"damage can\'t be prevented\" statement overrides all forms of preventing damage. Damage prevention spells and abilities can still be cast and activated, they just don\'t do anything.').

card_ruling('flash', '2004-10-04', 'Any X in the creature\'s mana cost is zero.').
card_ruling('flash', '2004-10-04', 'The mana cost you pay includes colored mana.').
card_ruling('flash', '2007-05-01', 'The creature enters the battlefield, so it will trigger enters-the-battlefield abilities even if you have chosen to not pay.').
card_ruling('flash', '2009-10-01', 'If you choose not to pay, the creature is sacrificed immediately. No player will get priority in between the creature entering the battlefield and being sacrificed. Sacrificing the creature this way will trigger any abilities that trigger when a creature leaves the battlefield.').

card_ruling('flash conscription', '2005-10-01', 'If white mana was paid, the player who controls the creature when it deals combat damage will be the one who gains life.').

card_ruling('flash foliage', '2006-05-01', 'The Saproling token is blocking the attacking creature, even if the block couldn\'t legally be declared (for example, if the attacking creature has flying, or is Vindictive Mob).').
card_ruling('flash foliage', '2006-05-01', 'Putting a blocking creature onto the battlefield doesn\'t trigger \"When this creature blocks\" abilities. It also won\'t check blocking restrictions, costs, or requirements.').
card_ruling('flash foliage', '2006-05-01', 'Putting a blocking creature onto the battlefield will trigger \"When this creature becomes blocked by a creature\" abilities. It may trigger \"When this creature becomes blocked\" abilities, but only if the creature the Saproling is blocking had not yet been blocked that combat.').
card_ruling('flash foliage', '2006-05-01', 'The target creature can be blocked by other creatures.').
card_ruling('flash foliage', '2006-05-01', 'Flash Foliage can be cast only during an opponent\'s combat phase, after attackers have been declared. If it\'s cast after combat damage is dealt, the Saproling will enter the battlefield blocking the attacking creature, but will neither deal combat damage to it nor be dealt combat damage by it.').
card_ruling('flash foliage', '2006-07-01', 'With the latest Oracle update, you can only cast this during combat, after blockers have been declared.').
card_ruling('flash foliage', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s after the beginning of that phase\'s Declare Blockers Step.').

card_ruling('flay', '2004-10-04', 'The player gets to see the first discard before choosing whether or not to pay.').
card_ruling('flay', '2004-10-04', 'The player gets the option to pay when this spell resolves.').

card_ruling('flayer of the hatebound', '2011-01-22', 'Flayer of the Hatebound\'s last ability will trigger even if a creature enters the battlefield from your graveyard under another player\'s control.').
card_ruling('flayer of the hatebound', '2011-01-22', 'The creature that entered the battlefield from your graveyard deals damage equal to its current power (including any +1/+1 counters it entered the battlefield with) to the target creature or player. If it\'s no longer on the battlefield when the ability resolves, its last known existence on the battlefield is checked to determine its power.').
card_ruling('flayer of the hatebound', '2011-01-22', 'Flayer of the Hatebound is the source of the ability, but it may be another creature that is the source of the damage. If a black creature enters the battlefield from your graveyard, the ability could target a creature with protection from black, although the damage will be prevented. It couldn\'t target a creature with protection from red.').
card_ruling('flayer of the hatebound', '2011-01-22', 'Since damage is dealt by the creature, abilities like lifelink and deathtouch are taken into account, even if the creature has left the battlefield by the time it deals damage.').
card_ruling('flayer of the hatebound', '2011-01-22', 'If you cast a creature card from your graveyard, that card will be put on the stack before entering the battlefield. Flayer of the Hatebound won\'t trigger.').

card_ruling('fledgling griffin', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('fledgling griffin', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('fleecemane lion', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('fleecemane lion', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('fleecemane lion', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('fleetfeather cockatrice', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('fleetfeather cockatrice', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('fleetfeather cockatrice', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('fleetfoot panther', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('fleeting distraction', '2010-06-15', 'If the targeted creature is an illegal target by the time Fleeting Distraction resolves, the spell is countered. You won\'t draw a card.').
card_ruling('fleeting distraction', '2012-05-01', 'If the targeted creature is an illegal target when Fleeting Distraction tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').

card_ruling('flesh', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('flesh', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('flesh', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('flesh', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('flesh', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('flesh', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('flesh', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('flesh', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('flesh', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('flesh', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('flesh', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('flesh', '2013-04-15', 'As Flesh is resolving, if the creature is an illegal target but the creature card is still a legal target, the creature card will be exiled but no counters will be put onto the creature. If the creature is the only legal target, Flesh will still resolve but will have no effect as you didn\'t exile a card.').
card_ruling('flesh', '2013-04-15', 'The number of +1/+1 counters is based on the power of the creature card as it last existed in the graveyard.').
card_ruling('flesh', '2013-04-15', 'If you cast Flesh // Blood as a fused split card, the +1/+1 counters will have been placed before the amount of damage is determined.').

card_ruling('flesh allergy', '2011-01-01', 'Flesh Allergy counts each creature put into a graveyard from the battlefield that turn, including token creatures, the creature sacrificed to cast the spell, and the targeted creature (if it is destroyed).').
card_ruling('flesh allergy', '2011-01-01', 'If the targeted creature is an illegal target by the time Flesh Allergy resolves, the spell is countered. No player will lose life.').
card_ruling('flesh allergy', '2011-01-01', 'You can target a creature you control, then sacrifice that creature to pay the additional cost. However, if you do, Flesh Allergy will be countered for having an illegal target. You won\'t lose any life.').
card_ruling('flesh allergy', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('flesh allergy', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').
card_ruling('flesh allergy', '2013-07-01', 'If Flesh Allergy resolves but the targeted creature is not destroyed (because it has indestructible or enchanted by an Aura with totem armor, for example), its controller will still lose life.').

card_ruling('flesh carver', '2014-11-07', 'Use Flesh Carver’s power when it died (including any +1/+1 counters it had) to determine the value of X.').

card_ruling('fleshbag marauder', '2008-10-01', 'When the ability resolves, you may sacrifice Fleshbag Marauder itself. If you control no other creatures, you\'ll have to sacrifice Fleshbag Marauder.').
card_ruling('fleshbag marauder', '2015-06-22', 'As Fleshbag Marauder’s ability resolves, first you choose a creature to sacrifice, then each other player in turn order chooses a creature to sacrifice, then all those creatures are sacrificed simultaneously.').

card_ruling('fleshformer', '2009-02-01', 'Fleshformer\'s ability has only one target: the creature. If that creature becomes an illegal target by the time Fleshformer\'s ability would resolve, the entire ability is countered. Fleshformer won\'t get +2/+2 and won\'t gain fear.').
card_ruling('fleshformer', '2009-02-01', 'Fleshformer can target itself with its ability. The result is that Fleshformer gains fear, since the +2/+2 and -2/-2 parts of the effect will balance each other out. (Fleshformer\'s power and toughness will each momentarily go up by 2, in case anything cares about that.)').

card_ruling('fleshpulper giant', '2013-07-01', 'If there are no legal targets available, the ability is simply removed from the stack. If you control the only creature with toughness 2 or less, you must choose it as the target, though you can choose to not destroy it.').
card_ruling('fleshpulper giant', '2013-07-01', 'If the target creature’s toughness is greater than 2 when the ability tries to resolve, the ability will be countered and the creature won’t be destroyed.').

card_ruling('fleshwrither', '2007-05-01', 'Transfigure is similar to Transmute, though it can be activated only if the permanent with Transfigure is on the battlefield, and it can fetch only a creature.').

card_ruling('flicker', '2004-10-04', 'It will trigger \"enters the battlefield\" abilities and \"leaves the battlefield\" abilities. Note that the card does not go to the graveyard, so it will not trigger \"goes to the graveyard\" abilities.').
card_ruling('flicker', '2004-10-04', 'This could be used to make the target of another spell or ability illegal if this wasn\'t a sorcery (which makes it really hard to do that). This is because the Flickered permanent leaves the battlefield and then returns as a completely different permanent, and the targeted spell will not recognize it.').
card_ruling('flicker', '2005-08-01', 'This spell effectively \"resets\" the permanent to being just like it was freshly cast. All counters, Auras, effects, and so on are removed when it is exiled. Then it comes back onto the battlefield like new.').

card_ruling('flickerform', '2005-10-01', 'The card that was enchanted comes back onto the battlefield first, regardless of whether it\'s still a creature. Then any Auras exiled that can legally enchant that card come back. Any Auras that can\'t enchant that permanent remain exiled.').
card_ruling('flickerform', '2005-10-01', 'If the enchanted creature was a token, it ceased to exist when it was exiled. Any Auras that were attached to it (including Flickerform) remain exiled.').
card_ruling('flickerform', '2013-07-01', 'If the enchanted creature was also enchanted by Copy Enchantment, Copy Enchantment will return to the battlefield attached to that permanent. As Copy Enchantment returns to the battlefield, its controller may choose any enchantment on the battlefield for it to copy. It can\'t copy Flickerform or any other Aura returning to the battlefield at the same time. If it copies a non-Aura enchantment, it returns to the battlefield unattached instead.').
card_ruling('flickerform', '2014-02-01', 'If the enchanted creature was enchanted by any Auras with bestow or any Licids, those cards will return to the battlefield unattached as creatures.').

card_ruling('flickering spirit', '2006-09-25', 'When the ability resolves, Flickering Spirit is exiled, then immediately returned to the battlefield. It returns as a \"new\" creature. Any counters, Auras, and so on are removed. Any spells or abilities targeting Flickering Spirit no longer target it.').
card_ruling('flickering spirit', '2006-09-25', 'If Flickering Spirit\'s ability is activated, any \"as this enters the battlefield\" choices for it are made by its owner, not its previous controller.').
card_ruling('flickering spirit', '2007-02-01', 'If a token copy of Flickering Spirit uses its activated ability, the token will not return to the battlefield, because of the rule that says tokens that leave the battlefield can\'t return.').

card_ruling('flickering ward', '2009-10-01', 'If you choose White, the enchanted creature will have Protection from White. This will cause any other white Auras attached to that creature to be put into their owner\'s graveyard, but Flickering Ward will remain on the battlefield.').

card_ruling('flickerwisp', '2008-08-01', 'The exiled card will return to the battlefield at the beginning of the end step even if Flickerwisp is no longer on the battlefield.').
card_ruling('flickerwisp', '2008-08-01', 'If the permanent that returns to the battlefield has any abilities that trigger at the beginning of the end step, those abilities won\'t trigger that turn.').

card_ruling('flight spellbomb', '2011-01-01', 'For flying to work as an evasion ability, an attacking creature must be granted flying before the declare blockers step begins. Once an attacking creature has become blocked, giving it flying won\'t change that.').

card_ruling('fling', '2010-08-15', 'The sacrificed creature\'s last known existence on the battlefield is checked to determine its power.').
card_ruling('fling', '2010-08-15', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that creature is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').
card_ruling('fling', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('fling', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('flitterstep eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('flitterstep eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('flitterstep eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('flitterstep eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('flitterstep eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('flitterstep eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('flitterstep eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('flood plain', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('floodbringer', '2005-02-01', 'The land Floodbringer returns to its owner\'s hand can be the targeted land.').

card_ruling('floodtide serpent', '2014-02-01', 'You must return an enchantment you control to its owner’s hand during each combat in which you attack with Floodtide Serpent.').

card_ruling('floral spuzzem', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('flourishing defenses', '2008-05-01', 'This ability triggers both when a -1/-1 counter is put on a creature on the battlefield and when a creature enters the battlefield with a -1/-1 counter on it. This includes when a creature returns to the battlefield as a result of persist.').
card_ruling('flourishing defenses', '2008-05-01', 'This ability triggers once for each -1/-1 counter. For example, if Leech Bonder (a creature that enters the battlefield with two -1/-1 counters on it) entered the battlefield, Flourishing Defenses\'s ability would trigger twice.').

card_ruling('flow of maggots', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('flowering field', '2004-10-04', 'The land\'s controller gets to activate the ability.').

card_ruling('flowering lumberknot', '2012-05-01', 'Whether Flowering Lumberknot is paired is checked only when attackers or blockers are declared. After that point, Flowering Lumberknot becoming unpaired won\'t cause it to stop attacking or blocking.').
card_ruling('flowering lumberknot', '2012-05-01', 'If the creature Flowering Lumberknot is paired with loses soulbond, Flowering Lumberknot will remain paired but won\'t be able to attack or block.').

card_ruling('flowstone crusher', '2005-08-01', 'Flowstone Crusher\'s ability can be activated multiple times, but Flowstone Crusher is put into the graveyard after an effect resolves that makes its toughness 0.').

card_ruling('flowstone embrace', '2007-05-01', 'This ability is an ability of the Aura, not an ability granted to the creature. Only the Aura\'s controller can activate it.').

card_ruling('flowstone flood', '2004-10-04', 'You can\'t pay the Buyback cost unless you have at least one card in your hand to discard.').

card_ruling('flowstone hellion', '2004-10-04', 'It will be put into its owner\'s graveyard as soon as one of its abilities resolves that puts its toughness at zero (or less than the current amount of damage on it). This is a state-based action, so there is no way to activate its ability any more to increase its power further before it is put into the graveyard.').

card_ruling('flurry of wings', '2009-05-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('flurry of wings', '2009-05-01', 'Flurry of Wings counts the number of creatures that are attacking as it resolves. It doesn\'t matter how many creatures were originally declared as attackers that turn. It also doesn\'t matter who controls those creatures, or who or what they\'re attacking.').
card_ruling('flurry of wings', '2009-05-01', 'If Flurry of Wings is cast before attackers have been declared or after combat ends, it won\'t do anything.').

card_ruling('flusterstorm', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('flusterstorm', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('flusterstorm', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('flusterstorm', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('flusterstorm', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('flux', '2004-10-04', 'The discard is forced even though you choose how many to discard.').
card_ruling('flux', '2008-04-01', 'Each player chooses how many cards he or she would like to discard. That number may be 0.').

card_ruling('fluxcharger', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('fluxcharger', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('flying crane technique', '2014-09-20', 'Only creatures you control as Flying Crane Technique resolves will gain flying and double strike. Creatures that come under your control later in the turn will not.').

card_ruling('foe-razer regent', '2015-02-25', 'You choose the target of the first triggered ability as it goes on the stack, but you choose whether or not to fight as that ability resolves.').
card_ruling('foe-razer regent', '2015-02-25', 'If the target creature you don’t control becomes an illegal target of the first triggered ability before it resolves, Foe-Razer Regent won’t fight and its last ability won’t trigger.').
card_ruling('foe-razer regent', '2015-02-25', 'If a creature you control fights during an end step, the delayed triggered ability that puts +1/+1 counters on that creature won’t trigger until the beginning of the following turn’s end step.').

card_ruling('fog', '2009-10-01', 'Fog has no effect on damage that\'s already been dealt.').

card_ruling('fog bank', '2012-07-01', 'If Fog Bank blocks a creature with trample, that creature\'s controller must assign 2 damage to Fog Bank (assuming it\'s blocking no other creatures, there was no damage marked on it, and nothing has changed its toughness). The remainder can be assigned to the defending player or planeswalker.').

card_ruling('fold into æther', '2004-12-01', 'If the spell isn\'t countered (because it\'s a spell that can\'t be countered, for example), nothing happens.').
card_ruling('fold into æther', '2004-12-01', 'If your Fold into AEther counters a spell you control, you get to put a creature card from your hand onto the battlefield.').

card_ruling('followed footsteps', '2005-10-01', 'Any enters-the-battlefield abilities of the copied creature trigger when the creature tokens enter the battlefield. The creature tokens also have any \"this enters the battlefield with\" or \"as this enters the battlefield\" abilities that the copied creature has.').
card_ruling('followed footsteps', '2007-02-01', 'If Followed Footsteps moves after it has triggered, you get a copy of the newly-enchanted creature.').
card_ruling('followed footsteps', '2007-02-01', 'If Followed Footsteps and the enchanted creature leave the battlefield simultaneously (say via Upheaval or Akroma\'s Vengence somehow cast in response to the triggered ability), then a token copy of the creature that was enchanted when it was last on the battlefield is created. If Followed Foosteps did not enchant a creature when last on the battlefield as it went to the graveyard as an SBA when the enchanted creature left the battlefield in response to the trigger, then no copy will get created.').

card_ruling('font of mythos', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('food chain', '2004-10-04', 'This mana may not be used to pay costs imposed after the spell is initially cast.').
card_ruling('food chain', '2004-10-04', 'This mana may be used on additional costs to cast the spell, such as Kicker.').

card_ruling('fool\'s demise', '2006-09-25', 'If Fool\'s Demise and the enchanted creature go to the graveyard from the battlefield at the same time, both abilities will trigger.').

card_ruling('footbottom feast', '2007-10-01', 'You choose the order of the cards you put on top of your library.').
card_ruling('footbottom feast', '2007-10-01', 'You can cast this with zero targets. If you do, you\'ll get to draw a card. However, if you cast this with at least one target and all of those targets become illegal, the spell will be countered and you won\'t get to draw a card.').

card_ruling('forbidden alchemy', '2011-09-22', 'If you have fewer than four cards in your library, you\'ll look at all the cards there and put one into your hand and the rest into your graveyard.').

card_ruling('forbidden crypt', '2004-10-04', 'It will replace itself being sent to the graveyard, and exile itself.').
card_ruling('forbidden crypt', '2004-10-04', 'The second ability is a replacement effect, not a triggered ability. The card never gets to the graveyard.').
card_ruling('forbidden crypt', '2004-10-04', 'The \"if you can\'t\" in the first ability refers to putting the card in your hand, as well as being unable to choose a card. Thus, if you can\'t choose a card during resolution, you lose the game.').
card_ruling('forbidden crypt', '2004-10-04', 'If more than one card is being drawn due to a single effect, apply the replacement as if the cards were being drawn one at a time.').

card_ruling('forbidden lore', '2008-10-01', 'Forbidden Lore grants an activated ability to the enchanted land. Only that land\'s controller can activate the ability.').

card_ruling('forbidden ritual', '2004-10-04', 'If you choose to repeat the process, you repeat the effect but do not get to pick a different target opponent.').
card_ruling('forbidden ritual', '2004-10-04', 'You choose the target opponent on casting.').
card_ruling('forbidden ritual', '2004-10-04', 'You sacrifice during resolution, not during casting.').

card_ruling('forbidding watchtower', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('forbidding watchtower', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('force away', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('force away', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('force bubble', '2004-10-04', 'If you control more than one, each time you take damage you can decide which one replaces the damage, and that one replaces all of the damage. You can\'t split the damage between them. You decide each time you take damage. Note that damage from multiple sources that happens at one time (such as combat damage) gives one choice, not one per source.').
card_ruling('force bubble', '2004-10-04', 'If you take 6 damage at once, it will replace all 6 damage and put 6 counters on it. Then the second ability will trigger. Any additional damage you take before this ability resolves also results in counters, but once the ability resolves, this card is sacrificed.').
card_ruling('force bubble', '2004-10-04', 'The effect does not prevent damage, it replaces it so the damage is neither dealt or prevented.').

card_ruling('force of savagery', '2007-05-01', 'Yes, Force of Savagery has 0 toughness. It will be put into its owner\'s graveyard as a state-based action immediately upon entering the battlefield unless an effect puts it onto the battlefield with a counter on it (such as Chorus of the Conclave would) or a static ability boosts its toughness (such as Glorious Anthem would). A triggered or activated ability that boosts toughness won\'t have its effect fast enough to save it.').

card_ruling('force spike', '2004-10-04', 'The payment is optional.').

card_ruling('forced adaptation', '2013-01-24', 'The creature that gets the +1/+1 counter is the creature enchanted by Forced Adaptation when the ability resolves.').
card_ruling('forced adaptation', '2013-01-24', 'If Forced Adaptation\'s ability triggers but Forced Adaptation isn\'t on the battlefield when the ability resolves, put a +1/+1 counter on the creature it was enchanting when it left the battlefield.').

card_ruling('forced fruition', '2007-10-01', 'Yes, the opponent draws seven cards. It\'s not optional.').

card_ruling('forced worship', '2011-06-01', 'Forced Worship\'s activated ability may only be activated if Forced Worship is on the battlefield. If it\'s no longer on the battlefield when the ability resolves, the ability has no effect.').

card_ruling('forcefield', '2004-10-04', 'This can\'t be used to prevent damage caused by a blocked creature with Trample ability.').

card_ruling('forerunner of slaughter', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('forerunner of slaughter', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('forerunner of slaughter', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('forerunner of slaughter', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('forerunner of slaughter', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('foreshadow', '2004-10-04', 'You name the card on resolution.').

card_ruling('forethought amulet', '2004-10-04', 'This does not reduce damage which is already 2 or 1.').

card_ruling('forge devil', '2014-07-18', 'Forge Devil\'s ability is mandatory. If it\'s the only creature on the battlefield, you\'ll have to choose it as the target and have it deal 1 damage to itself and 1 damage to you.').

card_ruling('forgeborn oreads', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('forgeborn oreads', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('forgeborn oreads', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('forgestoker dragon', '2014-02-01', 'Forgestoker Dragon’s activated ability can target any creature, not just creatures controlled by the defending player or ones that could block.').
card_ruling('forgestoker dragon', '2014-02-01', 'If you don’t want the target creature to be able to block, Forgestoker Dragon’s activated ability must be activated during the declare attackers step.').

card_ruling('forgotten ancient', '2004-10-04', 'It does not get counters for copies of spells (as with Storm), just for spells that are actually cast.').
card_ruling('forgotten ancient', '2004-10-04', 'The moving of counters is not a targeted ability.').
card_ruling('forgotten ancient', '2004-10-04', 'You decide how many counters to move and where to move them when the ability resolves.').
card_ruling('forgotten ancient', '2004-10-04', 'Putting on a counter is optional. If you forget to do so, it means you chose not to do so.').

card_ruling('forgotten cave', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('forgotten lore', '2008-10-01', 'You target an opponent when you cast Forgotten Lore. The opponent chooses a card in your graveyard as Forgotten Lore resolves. After that choice is made, you\'re given the option to pay {G}. If you do, the targeted opponent must choose a different card, if there is one. (If there isn\'t one, this part is skipped.) Then you\'re given the option to pay {G} again. As long as you keep paying {G}, the process continues. As soon as you decline to pay {G}, the last card that was chosen by the opponent is put into your hand.').
card_ruling('forgotten lore', '2008-10-01', 'The spell has no knowledge of cards chosen for other spells named Forgotten Lore.').

card_ruling('foriysian interceptor', '2006-09-25', 'The third ability normally lets Foriysian Interceptor block two creatures. If another effect also lets it block an additional creature, the effects are cumulative.').
card_ruling('foriysian interceptor', '2006-09-25', 'If an effect says Foriysian Interceptor can\'t block, then it can\'t block any creatures.').

card_ruling('foriysian totem', '2006-09-25', 'The third ability normally lets Foriysian Totem block two creatures. If another effect also lets it block an additional creature, the effects are cumulative.').
card_ruling('foriysian totem', '2006-09-25', 'If an effect says Foriysian Totem can\'t block, then it can\'t block any creatures.').
card_ruling('foriysian totem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('fork', '2004-10-04', 'When a spell with additional costs is copied, you don\'t have to pay those costs again.').
card_ruling('fork', '2004-10-04', 'For spells that can have a variable number of targets, the controller of the copy must use the same number of targets the original spell did.').
card_ruling('fork', '2004-10-04', 'Forking a spell with an X in the cost requires you to use the same X value.').
card_ruling('fork', '2004-10-04', 'Fork will not copy changes made by modifying effects to the spell prior to the use of Fork, such as text-changing effects.').
card_ruling('fork', '2004-10-04', 'If mana or other costs need to be spent at resolution of the spell, the player of Fork would still be responsible for paying that cost.').
card_ruling('fork', '2004-10-04', 'You need not (and may not) pay any additional mana or other costs (like sacrifices) to use the spell which is Forked. You get control over a complete copy but can change nothing except the targets.').
card_ruling('fork', '2004-10-04', 'Fork does not let you make non-targeting choices about the spell.').
card_ruling('fork', '2004-10-04', 'If the spell being copied targets a spell on the stack, it is possible to target Fork itself since Fork is still on the stack when you pick the target(s) for the copy. Note that the copy\'s target will be illegal when it resolves.').
card_ruling('fork', '2004-10-04', 'If you copy a spell for which Buyback has been paid, you get nothing back since the copied spell does not have a card to give you.').
card_ruling('fork', '2004-10-04', 'The copy that is placed on the stack is not considered to have been \"cast\".').
card_ruling('fork', '2004-10-04', 'The Fork card goes to the graveyard when it resolves and leaves the copy on the stack. There is no card representing the copy on the stack.').
card_ruling('fork', '2004-10-04', 'It does copy the mana symbols in the mana cost for the card it is copying, but it uses its own color definition and not the one from those mana symbols. This is so it maintains its color just like the text says.').
card_ruling('fork', '2004-12-01', 'If you Fork a Spliced spell, the spliced text is added during the announcement of the original spell, and therefore is fully copied by Fork.').
card_ruling('fork', '2009-05-01', 'The copy will have the same mana cost as the original spell, but will be red rather than whatever color that mana cost would have made it.').
card_ruling('fork', '2009-10-01', 'If the targeted spell was kicked, then the copy will be kicked as well. If the targeted spell was not kicked, then the copy will not be kicked.').

card_ruling('forked bolt', '2010-06-15', 'You divide the damage as you cast Forked Bolt, not as it resolves. Each target must be assigned at least 1 damage. (In other words, as you cast Forked Bolt, you choose whether to have it deal 2 damage to a single target, or deal 1 damage to each of two targets.)').

card_ruling('forked lightning', '2009-10-01', 'You divide the damage as you cast Forked Lightning, not as it resolves. Each target you choose must be assigned at least 1 damage.').

card_ruling('forked-branch garami', '2005-02-01', 'Forked-Branch Garami is special in that it has two soulshift abilities. When it is put into a graveyard from the battlefield, both soulshift abilities trigger.').
card_ruling('forked-branch garami', '2005-02-01', 'Each soulshift ability can return one Spirit, or both abilities can attempt to return just one Spirit (causing the second ability to be countered).').

card_ruling('forlorn pseudamma', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('forlorn pseudamma', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('forlorn pseudamma', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('forlorn pseudamma', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('form of the dragon', '2004-10-04', 'If you life total was above 5 at end of turn, then you lose life to make your total 5. If it was less than 5, you gain life to bring it to 5.').
card_ruling('form of the dragon', '2004-10-04', 'It sets your life total at the beginning of the end step of every player\'s turn, not just your own.').
card_ruling('form of the dragon', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('formation', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('formation', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('formation', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('formation', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('formless nurturing', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('formless nurturing', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('formless nurturing', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('formless nurturing', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('formless nurturing', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('formless nurturing', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('formless nurturing', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('formless nurturing', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('formless nurturing', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('formless nurturing', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('formless nurturing', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('formless nurturing', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('formless nurturing', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('forsaken city', '2004-10-04', 'You may choose to exile a card in your hand even if Forsaken City is already untapped.').

card_ruling('forsaken wastes', '2004-10-04', 'The loss of life from targeting this card does not work until after this card enters the battlefield, so you can counter it without losing life.').
card_ruling('forsaken wastes', '2008-05-01', 'If a cost includes life gain (like Invigorate\'s alternative cost does), that cost can\'t be paid.').
card_ruling('forsaken wastes', '2008-05-01', 'Effects that would replace gaining life with some other effect won\'t be able to do anything because it\'s impossible for players to gain life.').
card_ruling('forsaken wastes', '2008-05-01', 'Effects that replace an event with gaining life (like Words of Worship\'s effect does) will end up replacing the event with nothing.').
card_ruling('forsaken wastes', '2008-05-01', 'If an effect says to set your life total to a certain number, and that number is higher than your current life total, that effect will normally cause you to gain life equal to the difference. With Forsaken Wastes on the battlefield, that part of the effect won\'t do anything. (If the number is lower than your current life total, the effect will work as normal.)').
card_ruling('forsaken wastes', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('fortify', '2013-07-01', 'You choose the mode as you cast Fortify, not as it resolves.').
card_ruling('fortify', '2013-07-01', 'Creatures that come under your control after Fortify resolves won’t get the chosen bonus.').

card_ruling('fortune thief', '2006-09-25', 'The first ability applies only if your life total is being reduced by damage. Other effects or costs (such as \"lose 1 life\" or \"pay 1 life\") can reduce your life total below 1 as normal.').
card_ruling('fortune thief', '2006-09-25', 'If an effect asks you to pay life, you can\'t pay more life than you have.').
card_ruling('fortune thief', '2006-09-25', 'The last sentence is not a prevention effect. It stops unpreventable damage from reducing your life total below 1.').
card_ruling('fortune thief', '2006-09-25', 'The ability doesn\'t change how much damage is dealt; it just changes how much life that damage makes you lose. An effect such as Spirit Link will see the full amount of damage being dealt.').
card_ruling('fortune thief', '2006-09-25', 'Thief won\'t prevent you from losing the game if your life total is 0 or less or some other effect causes you to lose the game.').

card_ruling('fossil find', '2008-05-01', 'The easiest way to choose a card at random from your graveyard is to turn your graveyard face down, shuffle it, and pick a card. You\'re not normally allowed to reorder your graveyard, but since this card lets you do it anyway, it\'s okay to mix the cards up while you\'re picking randomly.').
card_ruling('fossil find', '2008-05-01', 'If you have multiple cards in your graveyard with the same name, and one of them is being targeted by another spell on the stack or enchanted (with Spellweaver Volute, for example), you must differentiate them so you know which one (if any) is chosen at random. In that case, it may be better to use dice to choose a card at random, or temporarily use a proxy card to represent the targeted card.').
card_ruling('fossil find', '2008-05-01', 'Except for the card that you return to your hand, reordering your graveyard won\'t affect anything that\'s currently targeting or enchanting a card in your graveyard. Whatever it is will be able to track the card it\'s targeting or enchanting as your graveyard is reordered.').
card_ruling('fossil find', '2008-05-01', 'All players get to see which card you chose at random.').

card_ruling('foul familiar', '2004-10-04', 'The ability can only be activated while this card is on the battlefield.').

card_ruling('foul imp', '2004-10-04', 'You can cast this if you have less than 2 life, since the life is lost as an effect and not a payment.').

card_ruling('foul renewal', '2015-02-25', 'You must target a creature card in your graveyard and a creature on the battlefield to cast Foul Renewal. If the creature becomes an illegal target before Foul Renewal resolves, you’ll still return the creature card to your hand. If the creature card in your graveyard becomes an illegal target, the creature on the battlefield will be unaffected as there is no “card returned this way.”').

card_ruling('foul-tongue invocation', '2015-02-25', 'You may cast Foul-Tongue Invocation targeting a player who controls no creatures. If you qualify for the “Dragon bonus,” you’ll just gain 4 life.').
card_ruling('foul-tongue invocation', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('foul-tongue invocation', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('foul-tongue invocation', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('foundry champion', '2013-01-24', 'The number of creatures you control is counted when Foundry Champion\'s triggered ability resolves. Foundry Champion will count itself if it\'s still on the battlefield under your control at that time.').

card_ruling('fractured powerstone', '2012-06-01', 'In non-Planechase games, Fractured Powerstone\'s second ability will have no effect.').
card_ruling('fractured powerstone', '2012-06-01', 'Rolling the planar die this way doesn\'t count when determining the cost of the special action of rolling the planar die. For example, if you roll the planar die twice in a turn, then activate Fractured Powerstone to roll it a third time, rolling the planar die again that turn will cost {2}.').

card_ruling('fracturing gust', '2013-07-01', 'If an artifact or enchantment remains on the battlefield because it was regenerated or has indestructible, you won\'t gain life for it.').

card_ruling('frankenstein\'s monster', '2004-10-04', 'X can be any number and it does not have to match the total contents of your graveyard.').
card_ruling('frankenstein\'s monster', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('frantic salvage', '2011-06-01', 'You choose the order of the cards you put on top of your library.').
card_ruling('frantic salvage', '2011-06-01', 'You can cast this with zero targets. If you do, you\'ll get to draw a card. However, if you cast this with at least one target and all of those targets become illegal (because another effect has removed them from the graveyard, perhaps), the spell will be countered and you won\'t get to draw a card.').

card_ruling('frantic search', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('frantic search', '2004-10-04', 'This does not target the lands.').

card_ruling('frenetic sliver', '2007-02-01', 'For any given Sliver, this ability can be activated numerous times in response to itself, but the end result is that only one coin will be flipped for that Sliver.').
card_ruling('frenetic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('frenetic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('frenzied tilling', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('frenzied tilling', '2013-01-24', 'If the target land is an illegal target when Frenzied Tilling tries to resolve, the spell will be countered and none of its effects will happen. You won\'t search your library for a basic land card.').

card_ruling('frenzy sliver', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').
card_ruling('frenzy sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('frenzy sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('fresh meat', '2011-06-01', 'Fresh Meat counts all creatures you owned that went to the graveyard this turn, including creature tokens that entered the battlefield under your control and nontoken creatures you owned but didn\'t control.').

card_ruling('freyalise\'s winds', '2009-10-01', 'During your untap step, first you determine which of your permanents will untap, then you untap them. If you control a permanent that says \"You may choose not to untap [this permanent] during your untap step\" and you decide not to untap it, no wind counters will be removed from it because it wouldn\'t untap. The same is true for a permanent you control that wouldn\'t untap due to a spell or ability (such as the one from Barl\'s Cage).').

card_ruling('friendly fire', '2014-11-24', 'Cards that don’t have mana costs, such as land cards, have a converted mana cost of 0.').
card_ruling('friendly fire', '2014-11-24', 'If the card has {X} in its mana cost, X is 0.').

card_ruling('frightful delusion', '2011-09-22', 'You must target a spell in order to cast Frightful Delusion. You can\'t cast it without a legal target just to make a player discard a card.').
card_ruling('frightful delusion', '2011-09-22', 'The player discards a card even if he or she pays {1}.').

card_ruling('frightshroud courier', '2004-10-04', 'It checks the creature type when the ability is announced and resolved, but once the effect is placed on the creature, if its creature type changes the effect still continues.').

card_ruling('frog tongue', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('frogtosser banneret', '2008-04-01', 'A spell you cast that\'s both creature types costs {1} less to cast, not {2} less.').
card_ruling('frogtosser banneret', '2008-04-01', 'The effect reduces the total cost of the spell, regardless of whether you chose to pay additional or alternative costs. For example, if you cast a Rogue spell by paying its prowl cost, Frogtosser Banneret causes that spell to cost {1} less.').

card_ruling('from beyond', '2015-08-25', 'An Eldrazi card is one with the creature type Eldrazi. Just having “Eldrazi” in its name or being an Eldrazi-themed card doesn’t count.').
card_ruling('from beyond', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('from beyond', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('from beyond', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('from beyond', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('from beyond', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('from beyond', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('from beyond', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('from beyond', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('from beyond', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('frontier mastodon', '2014-11-24', 'Frontier Mastodon’s ferocious ability checks if you control a creature with power 4 or greater as Frontier Mastodon enters the battlefield. Because Frontier Mastodon isn’t on the battlefield at this time, it won’t count itself.').

card_ruling('frontier siege', '2014-11-24', 'The “Khans” ability triggers at the beginning of both your precombat main phase and your postcombat main phase. Unused mana empties from players’ mana pools at the end of each step and phase.').
card_ruling('frontier siege', '2014-11-24', 'For the “Dragons” ability, you decide whether the creature with flying will fight the target creature you don’t control as the ability resolves.').
card_ruling('frontier siege', '2014-11-24', 'Each Siege will have one of the two listed abilities, depending on your choice as it enters the battlefield.').
card_ruling('frontier siege', '2014-11-24', 'The words “Khans” and “Dragons” are anchor words, connecting your choice to the appropriate ability. Anchor words are a new rules concept. “[Anchor word] — [Ability]” means “As long as you chose [anchor word] as this permanent entered the battlefield, this permanent has [ability].” Notably, the anchor word “Dragons” has no connection to the creature type Dragon.').
card_ruling('frontier siege', '2014-11-24', 'Each of the last two abilities is linked to the first ability. They each refer only to the choice made as a result of the first ability. If a permanent enters the battlefield as a copy of one of the Sieges, its controller will make a new choice for that Siege. Which ability the copy has won’t depend on the choice made for the original permanent.').

card_ruling('frontline medic', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('frontline medic', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('frontline medic', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').
card_ruling('frontline medic', '2013-07-01', 'Creatures that come under your control after Frontline Medic\'s battalion ability resolves will not be granted indestructible by this effect.').

card_ruling('frontline strategist', '2004-10-04', 'The creature type is checked for \"non-Soldier\" at the time the damage would be dealt. If the creature that is dealing the damage is not on the battlefield, use its creature type right before it left the battlefield.').

card_ruling('frost breath', '2011-09-22', 'If a creature affected by Frost Breath changes controllers before its old controller\'s next untap step, Frost Breath will prevent it from becoming untapped during its new controller\'s next untap step.').
card_ruling('frost breath', '2013-04-15', 'This spell can target tapped creatures. If a targeted creature is already tapped when the spell resolves, that creature just remains tapped and doesn\'t untap during its controller\'s next untap step.').

card_ruling('frost lynx', '2014-07-18', 'Frost Lynx’s ability can target a creature that’s already tapped. That creature still won’t untap during its controller’s next untap step.').
card_ruling('frost lynx', '2014-07-18', 'Frost Lynx’s ability tracks the creature, not the creature’s controller. That is, if the creature changes controllers before its first controller’s next untap step, then it won’t untap during its new controller’s next untap step.').
card_ruling('frost lynx', '2014-07-18', 'If the creature isn’t tapped during its controller’s next untap step (perhaps because it was untapped by a spell), Frost Lynx’s ability has no effect at that time. It won’t try to keep the creature tapped on subsequent turns.').

card_ruling('frost titan', '2010-08-15', 'Frost Titan\'s first ability affects each spell (including Aura spells), activated ability, and triggered ability that\'s controlled by an opponent and that Frost Titan becomes a target of.').
card_ruling('frost titan', '2010-08-15', 'You may target any permanent with Frost Titan\'s second ability. It\'s okay if that permanent is already tapped.').
card_ruling('frost titan', '2010-08-15', 'If the permanent affected by Frost Titan\'s second ability is untapped at the time that permanent\'s controller\'s next untap step begins, the last part of Frost Titan\'s second ability has no effect.').
card_ruling('frost titan', '2010-08-15', 'If the permanent affected by Frost Titan\'s second ability changes controllers before its old controller\'s next untap step, Frost Titan\'s second ability will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('frost walker', '2014-11-24', 'If Frost Walker becomes the target of a spell or ability, its ability triggers and is put on the stack on top of that spell or ability. Frost Walker’s ability will resolve (causing it to be sacrificed) first. Unless the spell or ability has another target, it will be countered as it tries to resolve for having no legal targets.').

card_ruling('frostweb spider', '2006-07-15', 'If Frostweb Spider blocks a creature with flying that\'s just powerful enough to kill it, it will die before it can get the +1/+1 counter.').
card_ruling('frostweb spider', '2006-07-15', 'If Frostweb Spider somehow blocks more than one creature with flying during the same combat, its ability will trigger multiple times.').

card_ruling('frostwielder', '2004-12-01', 'Frostwielder must be on the battlefield when the creature would be put into a graveyard in order to exile it.').

card_ruling('frostwind invoker', '2010-06-15', 'Only creatures you control as the activated ability resolves are affected.').

card_ruling('frozen æther', '2007-02-01', 'This is the timeshifted version of Kismet.').
card_ruling('frozen æther', '2007-02-01', 'Cards enter the battlefield tapped. They do not enter the battlefield untapped and then immediately tap, therefore they do not trigger any effects due to tapping.').
card_ruling('frozen æther', '2007-02-01', 'It affects all opponents.').
card_ruling('frozen æther', '2007-02-01', 'Does not affect cards that phase in.').
card_ruling('frozen æther', '2007-02-01', 'It applies to cards and tokens that are put onto the battlefield by an effect, as well as ones that are played from the player\'s hand.').

card_ruling('fruit of the first tree', '2014-11-24', 'Use the creature’s toughness when it left the battlefield to determine the value of X. If that number is 0 or less, you won’t gain life or draw cards. (You won’t lose life or discard cards either.)').
card_ruling('fruit of the first tree', '2014-11-24', 'Fruit of the First Tree can enchant any creature, but Fruit of the First Tree’s controller will gain life and draw cards.').

card_ruling('fuel for the cause', '2011-06-01', 'You must be able to target another spell in order to cast Fuel for the Cause. You can\'t just cast it in order to proliferate, and it can\'t target itself.').
card_ruling('fuel for the cause', '2011-06-01', 'If the spell is an illegal target when Fuel for the Cause tries to resolve, it will be countered. You won\'t proliferate.').

card_ruling('fulgent distraction', '2011-01-01', 'You must target two creatures as you cast Fulgent Distraction. If you can\'t (because there\'s just one creature on the battlefield, perhaps), you can\'t cast the spell.').
card_ruling('fulgent distraction', '2011-01-01', 'Fulgent Distraction can target tapped and/or unequipped creatures.').
card_ruling('fulgent distraction', '2011-01-01', 'If one of the targeted creatures is an illegal target by the time Fulgent Distraction resolves, that creature won\'t become tapped and no Equipment will be unattached from it. The other creature will still be affected. If both of the targeted creatures are illegal targets by the time Fulgent Distraction resolves, the spell is countered.').

card_ruling('full moon\'s rise', '2011-09-22', 'In order to regenerate Werewolves involved in combat, you must sacrifice Full Moon\'s Rise before combat damage is assigned. This means they will lose the +1/+0 and trample bonuses before combat damage assignment.').

card_ruling('fumarole', '2008-10-01', 'Fumarole can\'t be cast unless you can target both a creature and a land. If there\'s a permanent on the battlefield that\'s both a creature and a land, it can be both targets.').

card_ruling('fume spitter', '2011-01-01', 'You may target Fume Spitter with its own ability. However, if you do, the ability will be countered for having an illegal target.').

card_ruling('fumiko the lowblood', '2005-02-01', 'X is variable. The bushido bonus is calculated each time Fumiko\'s bushido trigger resolves, based on the number of attackers at that time.').
card_ruling('fumiko the lowblood', '2005-02-01', 'Fumiko\'s ability doesn\'t force an opponent to pay for effects in order to be able to attack with his or her creatures (such as Ghostly Prison, for example).').
card_ruling('fumiko the lowblood', '2005-02-01', 'Fumiko\'s ability doesn\'t force tapped creatures or creatures with summoning sickness to attack.').

card_ruling('fungal behemoth', '2007-02-01', 'Any +1/+1 counters on Fungal Behemoth itself count toward its base power and toughness, then give it a power and toughness bonus. For example, if there are two +1/+1 counters on Fungal Behemoth and three on other creatures you control, Fungal Behemoth is a 7/7 creature.').
card_ruling('fungal behemoth', '2007-02-01', 'Both instances of X in the suspend ability are the same. You determine the value of X as you suspend the card from your hand. The value you choose must be at least 1.').
card_ruling('fungal behemoth', '2007-02-01', 'If this is suspended, then when the last time counter is removed from it, both its triggered ability and the \"play this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('fungal behemoth', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('fungal behemoth', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('fungal behemoth', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('fungal behemoth', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('fungal behemoth', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('fungal behemoth', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('fungal behemoth', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('fungal behemoth', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('fungal behemoth', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('fungal behemoth', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('fungal sprouting', '2012-07-01', 'Fungal Sprouting doesn\'t target any creature. The greatest power among creatures you control is determined when Fungal Sprouting resolves.').

card_ruling('fungus sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('fungus sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('fungusaur', '2004-10-04', 'If more than one creature damages it at one time, it only gets one counter.').

card_ruling('furious assault', '2004-10-04', 'You can choose a different target player each time this triggers.').

card_ruling('furnace celebration', '2011-01-01', 'Furnace Celebration itself doesn\'t allow you to sacrifice any permanents. Its ability triggers whenever you sacrifice a permanent because some other spell, ability, or cost instructed you to.').
card_ruling('furnace celebration', '2011-01-01', 'You choose the target when the ability triggers. You choose whether or not to pay {2} as the ability resolves. You can\'t pay {2} more than once for a single sacrificed permanent.').
card_ruling('furnace celebration', '2011-01-01', 'Although players may respond to the ability, once it starts to resolve and you decide to pay {2}, it\'s too late for players to respond.').
card_ruling('furnace celebration', '2011-01-01', 'Sacrificing a Furnace Celebration will cause the abilities of other Furnace Celebrations to trigger, but not its own ability.').

card_ruling('furnace dragon', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('furnace layer', '2012-06-01', 'You can be selected as the target of the first ability, not just your opponents.').
card_ruling('furnace layer', '2012-06-01', 'If the target player has no cards in his or her hand when the ability resolves, nothing happens. That player won\'t lose any life.').

card_ruling('furnace of rath', '2004-10-04', 'If a spell or ability damages multiple things, divide up the damage before applying this effect. This means you can\'t normally end up with an odd amount of damage on something.').
card_ruling('furnace of rath', '2004-10-04', 'The trample rules cause damage to be divided before it is doubled.').
card_ruling('furnace of rath', '2004-10-04', 'If you have two of these on the battlefield, the damage is multiplied by 4.').
card_ruling('furnace of rath', '2004-10-04', 'The multiplied damage counts in all ways as if it came from the original source. Furnace of Rath is not the source.').
card_ruling('furnace of rath', '2005-08-01', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage or the controller of the creature that would be dealt damage chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn.\" Suppose a spell would deal 5 damage to a player who has cast Mending Hands targeting himself or herself. That player can either (a) prevent 4 damage first and then let Furnace of Rath double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').

card_ruling('furor of the bitten', '2011-09-22', 'The enchanted creature\'s controller still chooses which player or planeswalker that creature attacks.').
card_ruling('furor of the bitten', '2011-09-22', 'If there are multiple combat phases in a turn, the enchanted creature must attack only in the first one in which it\'s able to.').
card_ruling('furor of the bitten', '2011-09-22', 'If, during its controller\'s declare attackers step, the enchanted creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having the creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('fury of the horde', '2006-07-15', 'If it\'s somehow not a main phase when Fury of the Horde resolves, all it does is untap all creatures that attacked that turn. No new phases are created.').
card_ruling('fury of the horde', '2006-07-15', 'Paying the alternative cost doesn\'t change when you can cast the spell. A creature spell you cast this way, for example, can still only be cast during your main phase while the stack is empty.').
card_ruling('fury of the horde', '2006-07-15', 'You may pay the alternative cost rather than the card\'s mana cost. Any additional costs are paid as normal.').
card_ruling('fury of the horde', '2006-07-15', 'If you don\'t have two cards of the right color in your hand, you can\'t choose to cast the spell using the alternative cost.').
card_ruling('fury of the horde', '2006-07-15', 'You can\'t exile a card from your hand to pay for itself. At the time you would pay costs, that card is on the stack, not in your hand.').

card_ruling('fury sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('fury sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('furystoke giant', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('furystoke giant', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('furystoke giant', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('furystoke giant', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('furystoke giant', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('furystoke giant', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('furystoke giant', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('future sight', '2007-05-01', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('future sight', '2007-05-01', 'If the top card of your library is a land, you can play it if you could play a land.').
card_ruling('future sight', '2007-05-01', 'If the top card of your library has morph, you can cast that card face down for {3}. However, all players will have seen which card it is before the spell is cast.').
card_ruling('future sight', '2007-05-01', 'As soon as you finish playing the card on top of your library, reveal the next card in your library.').
card_ruling('future sight', '2007-05-01', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').

