% Rulings

card_ruling('yasova dragonclaw', '2014-11-24', 'The powers of the target creature and Yasova Dragonclaw are checked as you put the ability on the stack and again as the ability resolves. Once the ability resolves, it doesn’t matter what happens to either creature’s power.').
card_ruling('yasova dragonclaw', '2014-11-24', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('yavimaya ants', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('yavimaya coast', '2015-06-22', 'The damage dealt to you is part of the second mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('yavimaya coast', '2015-06-22', 'Like most lands, each land in this cycle is colorless. The damage dealt to you is dealt by a colorless source.').

card_ruling('yavimaya dryad', '2006-09-25', 'You may target yourself with Yavimaya Dryad\'s ability.').

card_ruling('yavimaya granger', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('yawgmoth demon', '2004-10-04', 'The sacrificing of an artifact is not mandatory. You can choose not to sacrifice an artifact but will pay the consequences.').

card_ruling('yawgmoth\'s agenda', '2004-10-04', 'The cards in your graveyard are not considered to be in your hand for any reason. For example, you can\'t discard them.').
card_ruling('yawgmoth\'s agenda', '2004-10-04', 'You count spells that were cast this turn before this card enters the battlefield. (The Invasion FAQ is not correct on this point.)').
card_ruling('yawgmoth\'s agenda', '2004-10-04', 'It will exile itself if it is going to the graveyard from the battlefield.').

card_ruling('yawgmoth\'s will', '2004-10-04', 'The second ability creates a replacement effect. It applies to both costs and effects.').
card_ruling('yawgmoth\'s will', '2004-10-04', 'If you cast a Buyback spell, then there will be two effects trying to replace where the card goes. You get to choose if the Buyback returns the card to your hand or the card gets exiled.').
card_ruling('yawgmoth\'s will', '2004-10-04', 'If an effect asks you to discard a card, you can\'t \"discard\" something that is in your graveyard. Those cards are not in your hand. Thus, Cycling abilities of cards in the graveyard can\'t be activated.').
card_ruling('yawgmoth\'s will', '2004-10-04', 'It will exile itself since it goes to the graveyard after its effect starts.').
card_ruling('yawgmoth\'s will', '2004-10-04', 'If you play a card using Yawgmoth\'s Will and something triggers only when \"cast from your hand\", that something will not trigger. Such things trigger based on where the card came from.').
card_ruling('yawgmoth\'s will', '2004-10-04', 'To \"play a card\" is to either cast a spell or to put a land onto the battlefield using the main phase special action.').
card_ruling('yawgmoth\'s will', '2006-10-15', 'You cannot Suspend a card from your graveyard, only your hand.').

card_ruling('yawning fissure', '2009-10-01', 'First the opponent whose turn it is -- or, more likely, the first opponent to left of the player whose turn it is -- chooses and announces which land he or she will sacrifice, then each other opponent in turn order does the same, then all lands are sacrificed at the same time.').

card_ruling('ydwen efreet', '2009-10-01', 'The ability triggers any time Ydwen Efreet blocks.').

card_ruling('yellow scarves cavalry', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('yellow scarves general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('yeva\'s forcemage', '2012-07-01', 'Yeva\'s Forcemage\'s ability is mandatory, although you can choose Yeva\'s Forcemage as the target.').

card_ruling('yeva, nature\'s herald', '2012-07-01', 'You still pay the costs for green creature cards you cast.').
card_ruling('yeva, nature\'s herald', '2012-07-01', 'Yeva\'s ability applies to green creature cards in any zone, provided something is allowing you to cast them. For example, if the top card of your library is a green creature card and you control Garruk\'s Horde, you can cast that card as though it had flash.').

card_ruling('yew spirit', '2012-05-01', 'The value of X is determined when the ability resolves. The bonus won\'t change later in the turn if the creature\'s power changes.').
card_ruling('yew spirit', '2012-05-01', 'If Yew Spirit has power less than 0, the \"bonus\" will be negative. For example, if it\'s somehow become -10/3, it would get -10/-10 and become -20/-7.').

card_ruling('yisan, the wanderer bard', '2014-07-18', 'If Yisan isn’t on the battlefield when its ability resolves, use the number of verse counters on it when it left the battlefield to determine which creature cards may be put onto the battlefield. This number will include the counter you put on Yisan to activate the ability.').

card_ruling('yixlid jailer', '2007-05-01', 'This removes all abilities on all cards in all graveyards -- both the ones that matter in a graveyard (dredge, Chronosavant\'s ability, etc.) and those that don\'t.').
card_ruling('yixlid jailer', '2007-05-01', 'If an ability triggers when the object that has it is put into a graveyard from the battlefield, that ability triggers from the battlefield (such as Deadwood Treefolk or Epochrasite, for example), they won\'t be affected by Yixlid Jailer.').
card_ruling('yixlid jailer', '2007-05-01', 'If an ability triggers when the object that has it is put into a graveyard from anywhere other than the battlefield, that ability triggers from the graveyard, (such as Krosan Tusker, Narcomoeba, or Quagnoth, for example). Yixlid Jailer will prevent those abilities from triggering at all.').
card_ruling('yixlid jailer', '2007-05-01', 'If an ability triggers when the object that has it is put into a hidden zone from a graveyard, that ability triggers from the graveyard, (such as Golgari Brownscale), Yixlid Jailer will prevent that ability from triggering.').
card_ruling('yixlid jailer', '2007-05-01', 'Some replacement effects cause a card to be put somewhere else instead of being put into a graveyard (such as Legacy Weapon). These effects mean the card is never actually put into the graveyard, so Yixlid Jailer can\'t affect it.').
card_ruling('yixlid jailer', '2007-05-01', 'Some cards have abilities that modify how they\'re put onto the battlefield. For example, Scarwood Treefolk says \"Scarwood Treefolk is put onto the battlefield tapped\" and Triskelion says \"Triskelion enters the battlefield with three +1/+1 counters on it.\" Although these cards won\'t have these abilities in the graveyard, they will be applied if the cards are put onto the battlefield from the graveyard (due to Zombify, perhaps). What matters is that these cards will have these abilities on the battlefield.').
card_ruling('yixlid jailer', '2007-05-01', 'If Mistform Ultimus is in the graveyard, the Ultimus will lose its ability that says \"Mistform Ultimus is every creature type,\" but it will still *be* all creature types. The way continuous effects work, Mistform Ultimus\'s type-changing ability is applied before Yixlid Jailer\'s ability removes it.').

card_ruling('yoked plowbeast', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('yomiji, who bars the way', '2005-02-01', 'Yomiji\'s ability triggers whenever any legendary permanent is put into a graveyard from the battlefield, not just legendary creatures.').
card_ruling('yomiji, who bars the way', '2005-02-01', 'Yomiji\'s ability triggers even if Yomiji is going to the graveyard at the same time as the other legendary permanent.').

card_ruling('yore-tiller nephilim', '2006-02-01', 'The creature you put onto the battlefield from your graveyard is attacking, even if the attack couldn\'t legally be declared (for example, if that creature has defender or an effect says that no more than one creature can attack).').
card_ruling('yore-tiller nephilim', '2006-02-01', 'Putting an attacking creature onto the battlefield doesn\'t trigger \"When this creature attacks\" abilities. It also won\'t check attacking restrictions, costs, or requirements.').
card_ruling('yore-tiller nephilim', '2006-02-01', 'In a multiplayer game in which you\'re allowed to attack only one player each combat, the new creature will be attacking the same player as Yore-Tiller Nephilim. In a multiplayer game in which you may attack multiple players, you choose the player the new creature is attacking. It must be a player you\'re allowed to attack.').

card_ruling('yosei, the morning star', '2013-06-07', 'The triggered ability targets the player and up to five permanents that player controls. If all targets are illegal as the ability tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('young pyromancer', '2013-07-01', 'You’ll put the Elemental token onto the battlefield before the instant or sorcery spell you cast resolves but after any targets of that spell are chosen.').

card_ruling('your puny minds cannot fathom', '2010-06-15', 'What your maximum hand size is (or whether you even have one) matters only during the cleanup step during the ending phase of your turn.').
card_ruling('your puny minds cannot fathom', '2010-06-15', 'The effect doesn\'t wear off until just before your next untap step (even if an effect will cause that untap step to be skipped). Effectively, that means it removes your maximum hand size just during the turn in which you set it in motion.').
card_ruling('your puny minds cannot fathom', '2010-06-15', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and later set this scheme in motion, you\'ll have no maximum hand size until your next turn. However, if you set this scheme in motion and then put Null Profusion onto the battlefield that turn, your maximum hand size would be two.').

card_ruling('your will is not your own', '2010-06-15', 'You may target any creature an opponent controls, even one that\'s already untapped.').

card_ruling('yuan shao, the indecisive', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

