% Rulings

card_ruling('zada, hedron grinder', '2015-08-25', 'The ability triggers whenever you cast an instant or sorcery spell that targets only Zada and no other object or player.').
card_ruling('zada, hedron grinder', '2015-08-25', 'If you cast an instant or sorcery spell that has multiple targets, but it’s targeting only Zada with all of them, Zada’s ability will trigger. The copies will similarly each be targeting only one of your other creatures. You can’t change any of the copy’s targets to other creatures.').
card_ruling('zada, hedron grinder', '2015-08-25', 'Any creature you control that couldn’t be targeted by the original spell (due to shroud, protection abilities, targeting restrictions, or any other reason) is just ignored by Zada’s ability.').
card_ruling('zada, hedron grinder', '2015-08-25', 'You control all the copies. You choose the order the copies are put onto the stack. The original spell will be on the stack beneath those copies and will resolve last.').
card_ruling('zada, hedron grinder', '2015-08-25', 'The copies that the ability creates are created on the stack, so they’re not cast. Abilities that trigger when a player casts a spell (like Zada’s ability itself) won’t trigger.').
card_ruling('zada, hedron grinder', '2015-08-25', 'If the spell that’s copied is modal (that is, it says “Choose one —” or the like), the copies will have the same mode. You can’t choose a different one.').
card_ruling('zada, hedron grinder', '2015-08-25', 'If the spell that’s copied has an X whose value was determined as it was cast (like Rolling Thunder does), the copies have the same value of X.').
card_ruling('zada, hedron grinder', '2015-08-25', 'The controller of a copy can’t choose to pay any alternative or additional costs for the copy. However, effects based on any alternative or additional costs that were paid for the original spell are copied as though those same costs were paid for the copy.').

card_ruling('zameck guildmage', '2013-01-24', 'Zameck Guildmage\'s first ability is cumulative. For example, if it resolves twice in a turn, each creature that enters the battlefield under your control that turn will do so with two +1/+1 additional counters on it.').
card_ruling('zameck guildmage', '2013-01-24', 'Zameck Guildmage\'s first ability has no effect on creatures already on the battlefield when it resolves.').

card_ruling('zanikev locust', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('zealot il-vec', '2006-09-25', 'When Zealot il-Vec attacks and isn\'t blocked, you must choose a target for its ability even if you don\'t use it.').
card_ruling('zealot il-vec', '2006-09-25', 'If you choose to have Zealot il-Vec deal 1 damage to a creature and that damage is prevented or replaced, Zealot il-Vec\'s combat damage will still be prevented.').
card_ruling('zealot il-vec', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('zealous conscripts', '2012-05-01', 'The enters-the-battlefield ability can target any permanent, including one that\'s untapped or one that you already control.').

card_ruling('zedruu the greathearted', '2011-09-22', 'The owner of a card is the player who started the game with that card in his or her deck.').
card_ruling('zedruu the greathearted', '2011-09-22', 'The owner of a token is the player under whose control the token was put onto the battlefield.').
card_ruling('zedruu the greathearted', '2011-09-22', 'If either the opponent or the permanent you control becomes an illegal target by the time Zedruu\'s last ability tries to resolve, the ability does nothing.').

card_ruling('zektar shrine expedition', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('zektar shrine expedition', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('zendikar incarnate', '2015-06-22', 'The ability defining Zendikar Incarnate’s power works in all zones, not just the battlefield. Zendikar Incarnate’s power changes as the number of lands you control does.').

card_ruling('zhang fei, fierce warrior', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('zhang he, wei general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('zhao zilong, tiger general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('zhuge jin, wu strategist', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('zhur-taa druid', '2013-04-15', 'Zhur-Taa Druid\'s last ability doesn\'t trigger if it becomes tapped for any other reason. However, if Zhur-Taa Druid gains another mana ability that requires you tap it, its ability will trigger.').
card_ruling('zhur-taa druid', '2013-04-15', 'If you tap Zhur-Taa Druid for mana during the process of casting a spell or activating an ability, its triggered ability will resolve before that spell or ability.').
card_ruling('zhur-taa druid', '2013-04-15', 'Zhur-Taa Druid\'s last ability isn\'t a mana ability. It uses the stack and can be responded to.').

card_ruling('zirilan of the claw', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('zirilan of the claw', '2009-10-01', 'If the Dragon is put onto the battlefield and then phases out, the \"exile it\" ability will still apply. It will trigger at the beginning of the next End step and exile the Dragon if it has phased back in. If the Dragon is still phased out, then it cannot be exiled so ability won\'t do anything.').

card_ruling('zodiac dragon', '2009-10-01', 'If Zodiac Dragon is removed from the graveyard after the ability triggers but before it resolves, you can\'t return it to your hand. Similarly, if a replacement effect has Zodiac Dragon move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').
card_ruling('zodiac dragon', '2009-10-01', 'Even though Zodiac Dragon is a Dragon, it doesn\'t have flying.').

card_ruling('zombie apocalypse', '2011-01-22', 'All Humans are destroyed, not just ones you control.').
card_ruling('zombie apocalypse', '2011-01-22', 'If a creature card in your graveyard is both a Zombie and a Human, it will be returned to the battlefield tapped, then be destroyed.').

card_ruling('zombie mob', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('zombie musher', '2006-07-15', 'Zombie Musher can\'t be blocked as long as defending player controls a snow land.').

card_ruling('zombie scavengers', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('zombie scavengers', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('zombie scavengers', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('zombie scavengers', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('zombie scavengers', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('zulaport enforcer', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('zulaport enforcer', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('zulaport enforcer', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('zulaport enforcer', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('zulaport enforcer', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('zur the enchanter', '2006-07-15', 'If you use Zur the Enchanter\'s triggered ability to search for an Aura, it will be put onto the battlefield attached to an appropriate permanent. It doesn\'t target that permanent. If no appropriate permanent exists for it to be attached to, that Aura can\'t be put onto the battlefield and stays in your library.').

card_ruling('zur\'s weirding', '2004-10-04', 'If someone pays the cost, then the card was never drawn or discarded. It just gets put into the graveyard directly from the library.').
card_ruling('zur\'s weirding', '2004-10-04', 'This is continuous replacement effect that modifies a draw.').
card_ruling('zur\'s weirding', '2004-10-04', 'If a spell or ability causes more than one card to be drawn, draw the cards one at a time and deal with this replacement before drawing the next card.').
card_ruling('zur\'s weirding', '2006-04-01', 'When using Dredge with this, you have two choices. You can immediately replace the draw with Dredge, or you can apply Zur\'s Weirding\'s replacement. If the latter, you first resolve Zur\'s Weirding\'s ability, then if you\'re going to draw the card, decide whether or not to replace that draw with Dredge.').

card_ruling('zurgo bellstriker', '2015-02-25', 'Once Zurgo Bellstriker has legally blocked a creature, raising that creature’s power to 2 or greater won’t undo the block.').
card_ruling('zurgo bellstriker', '2015-02-25', 'Even though both cards portray the same character, Zurgo Bellstriker and Zurgo Helmsmasher have different names. Controlling both of them won’t invoke the “legend rule.”').
card_ruling('zurgo bellstriker', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('zurgo bellstriker', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('zurgo bellstriker', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('zurgo bellstriker', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('zurgo helmsmasher', '2014-09-20', 'You still choose which player or planeswalker Zurgo Helmsmasher attacks.').
card_ruling('zurgo helmsmasher', '2014-09-20', 'If, during your declare attackers step, Zurgo is tapped or is affected by a spell or ability that says it can’t attack, then it doesn’t attack. If there’s a cost associated with having Zurgo attack, you aren’t forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('zurgo helmsmasher', '2014-09-20', 'If Zurgo enters the battlefield before the combat phase, it will attack that turn if able. If it enters the battlefield after combat, it won’t attack that turn and will usually be available to block on the following turn.').
card_ruling('zurgo helmsmasher', '2014-09-20', 'Each time a creature dies, check whether Zurgo had dealt any damage to it at any time during that turn. If so, Zurgo’s ability will trigger. It doesn’t matter who controlled the creature or whose graveyard it went to.').

