% Rulings

card_ruling('uba mask', '2004-12-01', 'You can\'t play cards you exiled with Uba Mask on previous turns.').
card_ruling('uba mask', '2004-12-01', 'Any cards you don\'t play just remain exiled when the turn ends.').

card_ruling('ugin\'s construct', '2014-11-24', 'If you control only colorless permanents as the ability resolves (for example, basic lands and face-down permanents), you won’t sacrifice anything.').

card_ruling('ugin\'s insight', '2015-08-25', 'Use the highest converted mana cost among permanents you control as Ugin’s Insight resolves to determine how many cards you scry.').
card_ruling('ugin\'s insight', '2015-08-25', 'If X is 0, you won’t scry at all. Any abilities that trigger whenever you scry won’t trigger.').

card_ruling('ugin, the spirit dragon', '2014-11-24', 'If you choose to put permanent cards with morph onto the battlefield with the third ability, you must put them onto the battlefield face up.').
card_ruling('ugin, the spirit dragon', '2014-11-24', 'Ugin, the Spirit Dragon is not a Dragon card; that is, he doesn’t have the creature type Dragon. Spells and abilities that refer to Dragon cards or Dragons don’t apply to Ugin.').

card_ruling('uktabi efreet', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('ulamog\'s crusher', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('ulamog\'s crusher', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('ulamog\'s crusher', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('ulamog\'s crusher', '2010-06-15', 'If, during your declare attackers step, Ulamog\'s Crusher is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then it doesn\'t attack. If there\'s a cost associated with having Ulamog\'s Crusher attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('ulamog\'s crusher', '2010-06-15', 'If there are multiple combat phases in a turn, Ulamog\'s Crusher must attack only in the first one in which it\'s able to.').

card_ruling('ulamog\'s despoiler', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('ulamog\'s despoiler', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('ulamog\'s despoiler', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('ulamog\'s despoiler', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('ulamog\'s nullifier', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('ulamog\'s nullifier', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('ulamog\'s reclaimer', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('ulamog\'s reclaimer', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('ulamog, the ceaseless hunger', '2015-08-25', 'Ulamog’s first ability resolves independently of Ulamog once they’ve both been put on the stack. If Ulamog is countered, that triggered ability will still resolve. That triggered ability will always resolve before Ulamog does.').
card_ruling('ulamog, the ceaseless hunger', '2015-08-25', 'Cards exiled by Ulamog’s first and third abilities are exiled face up.').
card_ruling('ulamog, the ceaseless hunger', '2015-08-25', 'If the player has less than twenty cards in his or her library, exile all of them. That player won’t lose the game until he or she has to draw a card from an empty library.').

card_ruling('ulamog, the infinite gyre', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('ulamog, the infinite gyre', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('ulamog, the infinite gyre', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('ulamog, the infinite gyre', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('ulasht, the hate seed', '2006-02-01', 'Creatures that are both red and green provide two +1/+1 counters.').

card_ruling('ulcerate', '2014-07-18', 'The loss of life isn’t a cost. If the target creature is an illegal target when Ulcerate tries to resolve, it will be countered and none of its effects will happen. You won’t lose any life.').

card_ruling('ultimate price', '2015-02-25', 'A monocolored creature has exactly one color.').
card_ruling('ultimate price', '2015-02-25', 'Face-down creatures and most artifact creatures are not monocolored.').

card_ruling('ulvenwald primordials', '2011-09-22', 'You can regenerate Ulvenwald Primordials in response to the triggered ability that would transform it. If you do, the regeneration shield will apply to Ulvenwald Mystics that turn.').

card_ruling('ulvenwald tracker', '2012-05-01', 'The second target may be a creature any player controls, including you.').
card_ruling('ulvenwald tracker', '2012-05-01', 'If either or both targets are illegal when Ulvenwald Tracker\'s ability tries to resolve, no creature will deal or be dealt damage.').

card_ruling('umara raptor', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('umbilicus', '2004-10-04', 'If you only have 1 life, you can\'t choose the pay 2 life option.').
card_ruling('umbilicus', '2004-10-04', 'If you don\'t have a permanent to choose, you can still choose to not pay the life and then just do nothing since there is no permanent to choose.').
card_ruling('umbilicus', '2004-10-04', 'You choose to pay the life or not pay the life during the resolution of the ability.').

card_ruling('umbra mystic', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('umbra mystic', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('umbra mystic', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('umbra mystic', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('umbra mystic', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('umbra mystic', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('umbra mystic', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('umbra mystic', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('umbra mystic', '2010-06-15', 'All _Rise of the Eldrazi_ cards printed with totem armor are Auras with \"enchant creature.\" But Umbra Mystic grants totem armor to Auras attached to any permanent you control, not just creatures. The ability works the same way even if the Aura is enchanting a land, an artifact, or any other permanent.').
card_ruling('umbra mystic', '2010-06-15', 'Umbra Mystic grants totem armor to Auras attached to permanents you control, regardless of who controls those Auras. Conversely, it doesn\'t grant totem armor to Auras you control that are attached to permanents controlled by other players.').
card_ruling('umbra mystic', '2010-06-15', 'If a permanent you control would be destroyed, and it\'s enchanted by an Aura with totem armor, it doesn\'t matter who controls that Aura. The totem armor effect is mandatory. If that permanent is enchanted by multiple Auras, you choose which one is destroyed, regardless of who controls them.').
card_ruling('umbra mystic', '2010-06-15', 'Say you control a permanent that\'s enchanted by an Aura you control, and that Aura is itself enchanted by an Aura. If the permanent would be destroyed, instead the first Aura is destroyed... but since that Aura would be destroyed, instead the second Aura is destroyed.').
card_ruling('umbra mystic', '2010-06-15', 'Multiple instances of totem armor on the same Aura are redundant.').
card_ruling('umbra mystic', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('umbra mystic', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('umbra mystic', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('umbra stalker', '2008-08-01', 'This is a characteristic-defining ability, which means it functions in all zones. You include the symbols in the Stalker\'s own mana cost if you need to determine its power and/or toughness while it\'s in the graveyard.').
card_ruling('umbra stalker', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('umbra stalker', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').

card_ruling('umbral mantle', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('umbral mantle', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('umbral mantle', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('umezawa\'s jitte', '2005-02-01', 'Umezawa\'s Jitte\'s activated ability generates a modal choice. The choice is made when the ability is activated.').
card_ruling('umezawa\'s jitte', '2005-02-01', 'If the \"target creature gets -1/-1 until end of turn\" mode is chosen, the target creature must also be announced.').
card_ruling('umezawa\'s jitte', '2005-02-01', 'The ability can be used any time Umezawa\'s Jitte\'s controller has priority -- only the \"target creature\" choice has additional requirements. Choosing the \"Equipped creature gets +2/+2 until end of turn\" mode does nothing if the Jitte isn\'t equipped to a creature when the ability resolves.').
card_ruling('umezawa\'s jitte', '2005-02-01', 'If the Jitte is moved after the \"+2/+2\" mode is announced but before it resolves, the bonus is given to the creature that is equipped when the ability resolves.').
card_ruling('umezawa\'s jitte', '2005-02-01', 'If the Jitte leaves the battlefield after the \"+2/+2\" mode is announced but before it resolves, the bonus is given to the creature that was most recently equipped once the ability resolves.').

card_ruling('unbreathing horde', '2011-09-22', 'Only one +1/+1 counter will be removed, no matter how much damage is prevented.').
card_ruling('unbreathing horde', '2011-09-22', 'If Unbreathing Horde has no +1/+1 counters on it (but its toughness is raised above 0 by another effect), any damage dealt to it will still be prevented, even though no counter will be removed.').
card_ruling('unbreathing horde', '2011-09-22', 'If Unbreathing Horde enters the battlefield from a graveyard, it will count itself when determining how many +1/+1 counters it enters the battlefield with.').

card_ruling('unburden', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('uncle istvan', '2004-10-04', 'Works even on damage from activated abilities of creatures such as Prodigal Sorcerer.').

card_ruling('uncontrolled infestation', '2004-10-04', 'It only triggers if the land becomes tapped after this Aura is on it. Attaching it to a tapped land will not destroy the land until the next time the land is untapped and then tapped again.').

card_ruling('undead alchemist', '2011-09-22', 'If you control multiple Undead Alchemists, the multiple replacement abilities will have no added effect. Combat damage dealt to a player by a Zombie you control will be replaced only once with cards being put into that player\'s graveyard.').
card_ruling('undead alchemist', '2011-09-22', 'Whenever a creature card is put into an opponent\'s graveyard from his or her library, the triggered ability of each Undead Alchemist you control will trigger. The first such ability to resolve will exile that creature card and create a Zombie token. Subsequent abilities won\'t exile the creature card, but each will create another Zombie token.').

card_ruling('undead gladiator', '2004-10-04', 'The ability to return to your hand can only be activated while this card is in your graveyard.').
card_ruling('undead gladiator', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('undead leotau', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('undead leotau', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('undead leotau', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('undead leotau', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('undead leotau', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('undead servant', '2015-06-22', 'Count the number of Undead Servants in your graveyard as the ability resolves to determine how many tokens to put onto the battlefield. If Undead Servant dies in response to its own triggered ability and is in your graveyard as that ability resolves, it will count toward the number of Zombies you get.').

card_ruling('undead slayer', '2009-10-01', 'Undead Slayer\'s ability must target a permanent (not a card in the graveyard) with at least one of the listed creature types.').

card_ruling('undercity informer', '2013-01-24', 'If the target player has no land cards in his or her library, all cards from that library will be revealed and put into his or her graveyard.').

card_ruling('undercity plague', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('undercity plague', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('undercity plague', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('undercity plague', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('undercity plague', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('undercity plague', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('undercity plague', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('undercity plague', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('undercity plague', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('undercity plague', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('undercity plague', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('undercity reaches', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('undercity reaches', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('undercity reaches', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('undercity reaches', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('undercity reaches', '2009-10-01', 'If multiple creatures deal combat damage to a player, the first ability of Undercity Reaches triggers multiple times.').
card_ruling('undercity reaches', '2009-10-01', 'If a creature deals combat damage to a player, the controller of Undercity Reaches controls the ability that triggers, but the controller of the creature decides whether to draw a card.').
card_ruling('undercity reaches', '2009-10-01', 'The {C} ability\'s effect continues to apply even after Undercity Reaches stops being the face-up plane card.').
card_ruling('undercity reaches', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then roll {C} while Undercity Reaches is the face-up plane card, you\'ll have no maximum hand size. However, if those events had happened in the opposite order, your maximum hand size would be two for as long as Null Profusion is on the battlefield.').

card_ruling('undercity troll', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('undercity troll', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('undercity troll', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('underground sea', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('underground sea', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('undergrowth', '2004-10-04', 'It means that red creatures still deal damage if you pay the additional cost.').

card_ruling('undergrowth champion', '2015-08-25', 'If Undergrowth Champion is dealt damage from multiple sources at the same time (for example, if it’s blocked by multiple creatures), its prevention effect will apply once to all that damage. You’ll remove only one +1/+1 counter.').
card_ruling('undergrowth champion', '2015-08-25', 'If damage that can’t be prevented is dealt to Undergrowth Champion while it has a +1/+1 counter on it, you’ll still remove a +1/+1 counter on it.').
card_ruling('undergrowth champion', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('undergrowth champion', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('undergrowth champion', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('undergrowth scavenger', '2014-07-18', 'If Undergrowth Scavenger enters the battlefield from a graveyard, it will count itself when determining how many +1/+1 counters it enters with.').

card_ruling('underworld cerberus', '2013-09-15', 'If you can’t exile Underworld Cerberus when its last ability resolves, perhaps because it’s been exiled by another spell or ability, each player will still return all creature cards from his or her graveyard to his or her hand.').
card_ruling('underworld cerberus', '2013-09-15', 'If two of these die at the same time, the last ability of each will trigger. If each player controlled one, the nonactive player’s ability will resolve first. The Underworld Cerberus controlled by that player will be exiled, and all other creature cards in graveyards (including the Underworld Cerberus controlled by the active player) will be returned to their owners’ hands. Then the active player’s ability will resolve. That Underworld Cerberus won’t be exiled, but any creature cards in graveyards at that time, perhaps because they died in response to this ability, will be returned to their owners’ hands.').

card_ruling('underworld coinsmith', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('underworld coinsmith', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('underworld coinsmith', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('underworld dreams', '2004-10-04', 'If multiple cards are drawn at once, this card triggers once for each card drawn.').
card_ruling('underworld dreams', '2004-10-04', 'In a multi-player game, it affects all opponents.').

card_ruling('undiscovered paradise', '2004-10-04', 'It will return even if some effect prevents you from untapping it. The ability is not replacing the untapping of the land in any way.').
card_ruling('undiscovered paradise', '2004-10-04', 'Only returns to owner\'s hand if it is still on the battlefield at the beginning of its controller\'s next untap.').

card_ruling('undo', '2004-10-04', 'The two creatures may have different owners and return to their respective owner\'s hands.').

card_ruling('undying flames', '2005-06-01', 'If you have no cards in your library, Undying Flames does nothing.').

card_ruling('unearth', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('unearth', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('unexpected potential', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('unexpected potential', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('unexpected potential', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('unexpected potential', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('unexpected potential', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('unexpected potential', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('unexpected potential', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('unexpected potential', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('unexpected potential', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('unexpected potential', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('unexpected potential', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('unexpected potential', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('unexpected potential', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('unexpected results', '2013-01-24', 'If you reveal a nonland card, you may cast it during the resolution of Unexpected Results. Ignore timing restrictions based on the card\'s type. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('unexpected results', '2013-01-24', 'If you can\'t cast the card (perhaps because there are no legal targets), or if you choose not to, the card will remain on top of the library.').
card_ruling('unexpected results', '2013-01-24', 'If you cast a spell “without paying its mana cost,” you can\'t pay alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('unexpected results', '2013-01-24', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('unexpected results', '2013-01-24', 'If you reveal a land card, Unexpected Results will be returned to your hand only if you put that land card onto the battlefield. If you don\'t, Unexpected Results will be put into its owner\'s graveyard.').
card_ruling('unexpected results', '2013-01-24', 'If you reveal a land card and put that card onto the battlefield, Unexpected Results will be put into its owner\'s hand directly from the stack. It won\'t be put into any graveyard.').

card_ruling('unexpectedly absent', '2013-10-17', 'If there are fewer than X cards in that player’s library, put that permanent on the bottom of that library.').
card_ruling('unexpectedly absent', '2013-10-17', 'If you choose 0 as the value for X, put that permanent on top of that library.').

card_ruling('unflinching courage', '2013-04-15', 'Multiple instances of lifelink on the same creature are redundant.').

card_ruling('unforge', '2004-12-01', 'Unforge deals damage only to the creature that the Equipment was attached to when the Equipment was destroyed. It doesn\'t deal damage to creatures that the Equipment had previously equipped.').

card_ruling('unhallowed pact', '2012-05-01', 'The card will return to the battlefield under your control only if it\'s still in the graveyard when Unhallowed Pact\'s ability resolves. If it\'s not (perhaps because an ability like undying has already returned it to the battlefield), nothing happens.').
card_ruling('unhallowed pact', '2012-05-01', 'If Unhallowed Pact is enchanting a token creature, that creature can\'t return to the battlefield.').

card_ruling('unholy hunger', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('unified front', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('unified front', '2015-08-25', '').
card_ruling('unified front', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('unified front', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('unified front', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('unified will', '2010-06-15', 'Whether you control more creatures than the targeted spell\'s controller is checked only as Unified Will resolves. If that player controls more creatures than you at this time, or you each control the same amount, Unified Will simply doesn\'t do anything.').

card_ruling('unlikely alliance', '2004-10-04', 'The bonus is not lost if the creature later becomes an attacker or blocker. The limitation is just there to prevent the effect from being used late in an attack.').

card_ruling('unliving psychopath', '2006-05-01', 'The creature must have less power than Unliving Psychopath both when the ability is activated and when it resolves.').

card_ruling('unmake the graves', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('unmake the graves', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('unmake the graves', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('unmake the graves', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('unmake the graves', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('unmake the graves', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('unmake the graves', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('unmask', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('unnatural aggression', '2015-08-25', 'If the creature an opponent controls survives the fight but would die later in the turn for another reason, it will be exiled instead, whether your creature dealt it damage or not.').
card_ruling('unnatural aggression', '2015-08-25', 'If the creature you control becomes an illegal target (perhaps due to being destroyed by another spell or ability), Unnatural Aggression will resolve, but the target creature an opponent controls will neither deal nor receive any damage. If that creature would die later in the turn for another reason, it will be exiled instead.').
card_ruling('unnatural aggression', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('unnatural aggression', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('unnatural aggression', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('unnatural aggression', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('unnatural aggression', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('unnatural selection', '2004-10-04', 'You must choose a creature type that already exists in Magic.').

card_ruling('unnerve', '2004-10-04', 'Players with less than 2 cards discard all they have.').

card_ruling('unnerving assault', '2008-08-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('unnerving assault', '2008-08-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('unnerving assault', '2008-08-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('unnerving assault', '2008-08-01', 'Unnerving Assault affects only creatures on the battlefield at the time it resolves. If a creature enters the battlefield later that turn, it won\'t be affected.').
card_ruling('unnerving assault', '2008-08-01', 'If a creature changes controllers later in the turn, that won\'t change how Unnerving Assault affected it.').

card_ruling('unravel the æther', '2014-02-01', 'If the artifact or enchantment is an illegal target as Unravel the Æther tries to resolve, it will be countered and none of its effects will happen. No library will be shuffled.').
card_ruling('unravel the æther', '2014-02-01', 'If the artifact or enchantment is a token, it will be shuffled into its owner’s library when Unravel the Æther resolves, then will cease to exist. For practical purposes, whatever item is representing the token should not actually be put into the library, though the library is still shuffled.').

card_ruling('unruly mob', '2011-09-22', 'If Unruly Mob and another creature you control die simultaneously (perhaps because they were both attacking or blocking), Unruly Mob will die before its triggered ability resolves. It can\'t be saved by the +1/+1 counter that would have been put on it.').

card_ruling('unscythe, killer of kings', '2009-05-01', 'Whenever a creature is put into a graveyard from the battlefield, check whether that creature had been dealt damage this turn by the creature that Unscythe is currently attached to. If so, Unscythe\'s second ability will trigger. This is true even if Unscythe wasn\'t attached to the creature at the time it dealt that damage. Only the currently equipped creature is checked, not any creatures that Unscythe may have been attached to earlier in the turn.').
card_ruling('unscythe, killer of kings', '2009-05-01', 'Unscythe\'s second ability cares about any damage, not just combat damage.').
card_ruling('unscythe, killer of kings', '2009-05-01', 'If Unscythe\'s second ability triggers, but the creature that was put into a graveyard is somehow removed from the graveyard in response (or it was a token), it won\'t be able to be exiled with Unscythe\'s ability. You won\'t get a Zombie token.').

card_ruling('unstable footing', '2009-10-01', 'Unstable Footing\'s effect causes damage from all sources to be unpreventable, not just damage from Unstable Footing itself.').
card_ruling('unstable footing', '2009-10-01', 'If damage can\'t be prevented, damage prevention shields don\'t have any effect. If a prevention effect has an additional effect, the additional effect will still work (if possible). Spells that create prevention effects can still be cast, and abilities that create prevention effects can still be activated.').
card_ruling('unstable footing', '2009-10-01', 'If damage can\'t be prevented, static abilities that prevent damage (including protection abilities) don\'t do so. If they have additional effects that don\'t depend on the amount of damage prevented, those additional effects will still work. Such effects are applied only once per source of damage.').
card_ruling('unstable footing', '2009-10-01', 'Effects that replace or redirect damage without using the word \"prevent\" are not affected by Unstable Footing.').
card_ruling('unstable footing', '2009-10-01', 'If a creature is dealt lethal damage, it can still regenerate. If it does, the damage marked on it will be removed from it.').
card_ruling('unstable footing', '2009-10-01', 'If Unstable Footing is kicked and the targeted player is an illegal target by the time it resolves, the entire spell is countered. Damage can still be prevented.').

card_ruling('unstable frontier', '2009-02-01', 'You choose a basic land type as the ability resolves.').
card_ruling('unstable frontier', '2009-02-01', 'The affected land loses its existing land types and any abilities printed on it. It becomes the chosen basic land type and now has the ability to tap to add one mana of the appropriate color to your mana pool. Unstable Frontier\'s ability doesn\'t change the affected land\'s name or whether it\'s legendary or basic.').

card_ruling('unstable hulk', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('unstable mutation', '2005-08-01', 'The -1/-1 counters stay even if the Aura is removed, and the +3/+3 goes away when the Aura does.').

card_ruling('unstable shapeshifter', '2004-10-04', 'The copy effect is not targeted.').
card_ruling('unstable shapeshifter', '2004-10-04', 'If a creature enters the battlefield that has some characteristic or ability set when it enters the battlefield, the copy does not get to make that choice (because it is not entering the battlefield). All values are zero and all checks for choices consider the choice not to have been made.').
card_ruling('unstable shapeshifter', '2004-10-04', 'If the creature has power/toughness that is continuously recalculated, so does this copy of it.').
card_ruling('unstable shapeshifter', '2004-10-04', 'When it changes \"shape\" to a new creature, any \"enters the battlefield\" abilities of the creature do not trigger.').
card_ruling('unstable shapeshifter', '2008-08-01', 'The ability is granted as part of the copy effect, which means it will be removed by any effects that would remove abilities regardless of when the effect was created.').
card_ruling('unstable shapeshifter', '2008-08-01', 'If an Unstable Shapeshifter is on the battlefield when another Unstable Shapeshifter enters the battlefield, the first one gets the second one\'s triggered ability, so will copy the next creature that enters the battlefield twice. It will only do so the next time a creature enters the battlefield, because the second \"becomes a copy\" effect to resolve will erase the extra triggered ability.').

card_ruling('untamed wilds', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('untamed wilds', '2006-10-15', 'Can fetch a snow land.').

card_ruling('unwilling recruit', '2008-08-01', 'You may target a creature you already control with Unwilling Recruit. If you do, you\'ll gain control of that creature (which usually won\'t have any visible effect), untap it, and it\'ll get +X/+0 until end of turn.').

card_ruling('unwinding clock', '2011-06-01', 'Those artifacts untap at the same time as the active player\'s permanents. You have no choice about what untaps and can\'t choose to not untap an artifact you control.').
card_ruling('unwinding clock', '2011-06-01', 'Some effects, such as the one generated by Rust Tick\'s ability, state that an artifact doesn\'t untap during its controller\'s untap step. These effects won\'t apply and stop the artifact from untapping during another player\'s untap step.').
card_ruling('unwinding clock', '2011-06-01', 'Even if you control more than one Unwinding Clock, you\'ll only untap your artifacts once during each other player\'s untap step.').

card_ruling('upwelling', '2009-10-01', 'Players can keep mana in their pool indefinitely while this is on the battlefield, but once it leaves the battlefield, they have until the end of the current step (or phase) to use the mana before it disappears.').

card_ruling('ur-drago', '2004-10-04', 'Allows any creature controlled by any player to block Swampwalking creatures as if they did not have this ability. It is not limited to just Ur-Drago.').

card_ruling('urabrask the hidden', '2011-06-01', 'Urabrask the Hidden gives itself haste while it\'s on the battlefield.').

card_ruling('urban burgeoning', '2012-10-01', 'The enchanted land untaps at the same time as the active player\'s permanents. You can\'t choose to not untap it at that time.').
card_ruling('urban burgeoning', '2012-10-01', 'Effects that state that the enchanted land doesn\'t untap during your untap step won\'t apply during another player\'s untap step.').

card_ruling('urban evolution', '2013-01-24', 'Urban Evolution\'s effect allows you to play an additional land during your main phase. Doing so follows the normal timing rules for playing lands. In particular, you don\'t get to play a land as Urban Evolution resolves; Urban Evolution fully resolves (and you\'ll draw three cards, perhaps including a land you\'ll play later) first.').
card_ruling('urban evolution', '2013-01-24', 'The effects of multiple Urban Evolutions in the same turn are cumulative. They\'re also cumulative with other effects that let you play additional lands, such as the one from Rites of Flourishing.').
card_ruling('urban evolution', '2013-01-24', 'If you somehow manage to cast Urban Evolution when it\'s not your turn, you\'ll draw three cards when it resolves, but you won\'t be able to play a land that turn.').

card_ruling('urborg', '2004-10-04', 'The ability can be used on a creature without First Strike or Swampwalk but has no effect.').
card_ruling('urborg', '2008-04-01', 'If a creature loses first strike after first strike combat damage has been assigned, it won\'t deal damage during the second combat damage step.').
card_ruling('urborg', '2009-10-01', 'You choose whether the targeted creature loses first strike or swampwalk as the ability resolves. If the creature has both abilities, it will lose just one of them.').
card_ruling('urborg', '2009-10-01', 'Causing a creature to lose swampwalk is useful only during the declare attackers step or earlier. Once the declare blockers step begins, it\'s too late to block the creature.').
card_ruling('urborg', '2009-10-01', 'Urborg\'s second ability affects only the first strike ability, not the part of the double strike ability that acts like first strike.').

card_ruling('urborg justice', '2004-10-04', 'Does not target the creatures.').
card_ruling('urborg justice', '2004-10-04', 'The creatures are chosen on resolution, not on announcement.').

card_ruling('urborg mindsucker', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('urborg stalker', '2008-04-01', 'Urborg Stalker\'s ability will trigger at the beginning of your upkeep as well.').
card_ruling('urborg stalker', '2008-04-01', 'This ability checks whether the player controls any nonblack, nonland permanents twice: once when it would trigger, and once when it resolves. If all the player\'s nonland permanents are black at the time the ability would trigger, it doesn\'t trigger at all. If all the player\'s nonland permanents are black at the time the ability would resolve, it\'s countered.').

card_ruling('urborg uprising', '2004-10-04', 'You can choose 0, 1, or 2 targets.').
card_ruling('urborg uprising', '2004-10-04', 'You do not get to draw a card if you chose at least one target and all targets are illegal on resolution.').

card_ruling('urborg, tomb of yawgmoth', '2008-04-01', 'If Urborg, Tomb of Yawgmoth and either Blood Moon or Magus of the Moon are on the battlefield, all nonbasic lands will be Mountains, not Swamps. Urborg, Tomb of Yawgmoth will be a Mountain as well. It will have \"{T}: Add {R} to your mana pool\" and no other abilities.').
card_ruling('urborg, tomb of yawgmoth', '2014-07-18', 'Urborg, Tomb of Yawgmoth isn’t a Swamp while it’s not on the battlefield.').
card_ruling('urborg, tomb of yawgmoth', '2014-07-18', 'Urborg’s ability causes each land on the battlefield to have the land type Swamp. Each land thus has the ability “{T}: Add {B} to your mana pool.” Nothing else changes about those lands, including their names, other subtypes, other abilities, and whether they’re legendary or basic.').
card_ruling('urborg, tomb of yawgmoth', '2014-07-18', 'If Urborg loses its abilities (for example, if it becomes a creature and then Turn to Frog targets it), all lands on the battlefield, including Urborg, will still be Swamps, but Urborg won’t have the ability “Each land is a Swamp in addition to its other land types.” Urborg also won’t be able to tap to produce {B}, but other lands (including those that enter the battlefield later in the turn) will. The way continuous effects work, Urborg’s type-changing ability is applied before the effect that removes both the type-changing ability and its own mana ability.').

card_ruling('urge to feed', '2010-03-01', 'You don\'t choose which untapped Vampire creatures you control to tap (if any) until Urge to Feed resolves.').
card_ruling('urge to feed', '2010-03-01', 'If you cast Urge to Feed targeting an untapped Vampire creature you control with toughness 3, its toughness will be reduced to 0 by the -3/-3 effect. However, state-based actions aren\'t checked until the spell has finished resolving. You may then tap that Vampire and put a +1/+1 counter on it, raising its toughness to 1. It will remain on the battlefield.').
card_ruling('urge to feed', '2010-03-01', 'If the targeted creature is an illegal target by the time Urge to Feed resolves, the entire spell is countered. You can\'t tap any Vampires.').

card_ruling('urza\'s armor', '2004-10-04', 'If a spell or ability damages multiple things, divide up the damage before applying this effect, and prevent the 1 damage to you only.').
card_ruling('urza\'s armor', '2004-10-04', 'It is a prevention effect applied when damage would be dealt.').

card_ruling('urza\'s avenger', '2004-10-04', 'The -1/-1 is not permanent. It lasts until the end of the turn as do the abilities that are granted.').

card_ruling('urza\'s chalice', '2004-10-04', 'This will not trigger on its own casting. It must be on the battlefield at the time the artifact is cast.').

card_ruling('urza\'s engine', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('urza\'s engine', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('urza\'s engine', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('urza\'s engine', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('urza\'s factory', '2006-09-25', 'Although Urza\'s Factory has the Urza\'s land type, it doesn\'t interact with Urza\'s Tower, Urza\'s Mine, or Urza\'s Power Plant.').

card_ruling('urza\'s incubator', '2004-10-04', 'Multiple Incubators are cumulative.').
card_ruling('urza\'s incubator', '2004-10-04', 'You choose a creature type right as it enters the battlefield, before any continuous effects are applied or trigged abilities trigger.').

card_ruling('urza\'s mine', '2004-10-04', 'If you have at least one of each of the three Urza\'s lands on the battlefield, you must take the 2 mana instead of just one.').

card_ruling('urza\'s power plant', '2004-10-04', 'If you have at least one of each of the three Urza\'s lands on the battlefield, you must take the 2 mana instead of just one.').

card_ruling('urza\'s rage', '2004-10-04', 'This spell has a self-replacement in it . This means that if Urza\'s Rage is kicked, the 3 damage is replaced with 10 unpreventable damage before any other replacements can be applied. For example, you can\'t use Healing Salve to prevent the 3 damage before it gets changed.').
card_ruling('urza\'s rage', '2004-10-04', 'You can target this spell with spells and abilities, so you can change its target. Those spells and abilities just can\'t counter it.').
card_ruling('urza\'s rage', '2004-10-04', 'The damage is unpreventable, but it can be affected by replacement effects.').
card_ruling('urza\'s rage', '2004-10-04', 'It can be countered by game rules, such as by having all its targets be illegal. Game rules are neither spells or abilities.').
card_ruling('urza\'s rage', '2004-10-04', 'Counterspells can be cast that target it, but when they resolve they simply don\'t counter it since it can\'t be countered.').

card_ruling('uyo, silent prophet', '2004-12-01', 'If you copy an Arcane spell that has cards spliced onto it, you\'ll copy the spliced text as well as the original spell text.').

