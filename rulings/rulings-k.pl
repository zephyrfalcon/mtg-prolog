% Rulings

card_ruling('kaalia of the vast', '2011-09-22', 'Kaalia\'s ability doesn\'t trigger if it attacks a planeswalker.').
card_ruling('kaalia of the vast', '2011-09-22', 'The creature card is already tapped and attacking as it\'s put onto the battlefield. Any abilities that trigger when a creature becomes tapped or when a creature attacks won\'t trigger for that card.').
card_ruling('kaalia of the vast', '2011-09-22', 'If the opponent Kaalia attacked is no longer in the game when its ability resolves, you may put an Angel, Demon, or Dragon creature card onto the battlefield tapped, but it won\'t be attacking anyone and it won\'t be an attacking creature.').

card_ruling('kabira evangel', '2009-10-01', 'You choose a color (or choose not to) as the triggered ability resolves. Once the color is chosen, it\'s too late for players to respond.').

card_ruling('kabira vindicator', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('kabira vindicator', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('kabira vindicator', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('kabira vindicator', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('kabira vindicator', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('kaboom!', '2004-10-04', 'If all the cards in your library are lands, this card deals zero damage.').
card_ruling('kaboom!', '2004-10-04', 'If you target more than one player, you do the whole thing for each player and you do each player in turn order.').

card_ruling('kaervek\'s hex', '2013-04-15', 'If a creature is both black and green, Kaervek\'s Hex will deal 1 damage to that creature.').

card_ruling('kaervek\'s spite', '2004-10-04', 'The sacrifice of all your permanents and discarding of your hand is part of the cost and is paid on casting. You do not have a choice to pay this cost zero times or more than one time in order to multiply the effect.').
card_ruling('kaervek\'s spite', '2004-10-04', 'As always, you can\'t sacrifice things you do not control.').

card_ruling('kagemaro, first to suffer', '2005-06-01', 'The value of X is determined as the ability resolves.').

card_ruling('kaho, minamo historian', '2005-06-01', 'If an effect takes a card Kaho exiled out of the Exile zone, then you can\'t use Kaho\'s ability to cast that card.').
card_ruling('kaho, minamo historian', '2005-06-01', 'If another player gains control of Kaho, that player may cast any of the cards exiled with Kaho\'s ability.').
card_ruling('kaho, minamo historian', '2005-06-01', 'Kaho\'s activated ability refers only to cards exiled by its triggered ability. If another effect exiled the cards, you won\'t be able to use Kaho to cast those cards.').

card_ruling('kalastria healer', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('kalastria healer', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('kalastria highborn', '2010-03-01', 'If multiple Vampires you control (possibly including Kalastria Highborn itself) are put into their owners\' graveyards at the same time, Kalastria Highborn\'s ability triggers that many times.').
card_ruling('kalastria highborn', '2010-03-01', 'You target a player when the ability triggers. You choose whether to pay {B} as the ability resolves. You may pay {B} only once per resolution.').
card_ruling('kalastria highborn', '2010-03-01', 'If the targeted player is an illegal target by the time the ability resolves (for example, if that player has left a multiplayer game), the entire ability is countered. You can\'t pay {B} and you don\'t gain life.').

card_ruling('kalastria nightwatch', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('kalastria nightwatch', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('kalastria nightwatch', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('kalitas, bloodchief of ghet', '2009-10-01', 'You get the token, not the player who controlled the creature.').
card_ruling('kalitas, bloodchief of ghet', '2009-10-01', 'The power and toughness of the token are based on the destroyed creature\'s last existence on the battlefield. The token doesn\'t have any of the destroyed creature\'s other characteristics.').
card_ruling('kalitas, bloodchief of ghet', '2013-07-01', 'If the targeted creature has indestructible or regenerates, you won\'t get a Vampire token. Similarly, if the targeted creature is destroyed but a replacement effect moves it to a different zone instead of its owner\'s graveyard, you won\'t get a Vampire token.').

card_ruling('kalonian hydra', '2013-07-01', 'To double the number of +1/+1 counters on a creature, determine how many +1/+1 counters are on the creature and put that many more on it. Effects that interact with counters (such as the one created by Corpsejack Menace’s ability) may change the number of counters ultimately put on the creature.').

card_ruling('kalonian twingrove', '2014-07-18', 'The ability that defines Kalonian Twingrove’s power and toughness works in all zones, not just the battlefield.').
card_ruling('kalonian twingrove', '2014-07-18', 'The power and toughness of both Kalonian Twingrove and the token it creates will change as the number of Forests you control changes.').

card_ruling('kamahl, fist of krosa', '2004-10-04', 'The second ability only applies to creatures you control at the time it resolves.').
card_ruling('kamahl, fist of krosa', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').
card_ruling('kamahl, fist of krosa', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('kamahl, pit fighter', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').

card_ruling('kami of the crescent moon', '2005-06-01', 'The additional draw triggers just after the normal draw for the turn happens.').

card_ruling('kapsho kitefins', '2014-07-18', 'The triggered ability will trigger for any creature entering the battlefield under your control at the same time as Kapsho Kitefins.').

card_ruling('karador, ghost chieftain', '2011-09-22', 'If Karador is your commander, calculate any cost increases, including the one based on the number of times you\'ve cast Karador from the command zone, before considering Karador\'s cost-reduction ability.').
card_ruling('karador, ghost chieftain', '2011-09-22', 'If you somehow cast Karador from a graveyard, its cost-reduction ability won\'t count itself.').
card_ruling('karador, ghost chieftain', '2011-09-22', 'Although Karador\'s second ability allows you to cast one creature card from your graveyard on each of your turns, it doesn\'t change when you can cast that card. Unless it has flash, the creature card must be cast during one of your main phases when the stack is empty.').
card_ruling('karador, ghost chieftain', '2011-09-22', 'Karador\'s second ability doesn\'t affect the costs you need to pay to cast that creature spell.').
card_ruling('karador, ghost chieftain', '2011-09-22', 'Karador\'s first ability functions on the stack. Karador\'s second ability functions only on the battlefield.').
card_ruling('karador, ghost chieftain', '2011-09-22', 'If you cast a creature card from your graveyard, then Karador leaves the battlefield, then returns to the battlefield, you may cast another creature card from your graveyard that turn. If you didn\'t cast a creature card from your graveyard while Karador was on the battlefield the first time, you may still cast only one creature card from your graveyard when it returns to the battlefield.').

card_ruling('karametra\'s acolyte', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('karametra\'s acolyte', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('karametra\'s acolyte', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('karametra\'s acolyte', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('karametra, god of harvests', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('karametra, god of harvests', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('karametra, god of harvests', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('karametra, god of harvests', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('karametra, god of harvests', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('karametra, god of harvests', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('karametra, god of harvests', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('karametra, god of harvests', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('karametra, god of harvests', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('karametra, god of harvests', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('karametra, god of harvests', '2014-02-01', 'If you cast a creature card with bestow for its bestow cost, it becomes an Aura spell and not a creature spell. Karametra’s last ability won’t trigger.').
card_ruling('karametra, god of harvests', '2014-02-01', 'You can use the last ability to put any land card with the subtype Forest or Plains onto the battlefield, not just ones named Forest or Plains.').

card_ruling('kargan dragonlord', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('kargan dragonlord', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('kargan dragonlord', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('kargan dragonlord', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('kargan dragonlord', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('karma', '2004-10-04', 'Amount of damage is determined when the ability resolves and not when it is placed on the stack.').

card_ruling('karmic guide', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('karmic justice', '2004-10-04', 'A spell or ability that asks for a sacrifice will not trigger this card.').
card_ruling('karmic justice', '2004-10-04', 'If an opponent destroys Karmic Justice, it will trigger on its own destruction.').
card_ruling('karmic justice', '2004-10-04', 'Karmic Justice will not trigger off of its ability destroying a permanent since you destroy the permanent and you are not one of your opponents.').
card_ruling('karmic justice', '2004-10-04', 'Karmic Justice will not trigger if an opponent\'s spell or ability causes your permanent to be destroyed indirectly. For example, if the spell caused an ability you control to trigger, and thereby destroy your permanent.').

card_ruling('karn liberated', '2011-06-01', 'Karn\'s first and third abilities are linked. Similarly, Karn\'s second and third abilities are linked. Only cards exiled by either of Karn\'s first two abilities will remain in exile when the game restarts.').
card_ruling('karn liberated', '2011-06-01', 'A game that restarts immediately ends. The players in that game then immediately begin a new game. No player wins, loses, or draws the original game as a result of Karn\'s ability.').
card_ruling('karn liberated', '2011-06-01', 'In a multiplayer games (a game that started with three or more players in it), any player that left the game before it was restarted with Karn\'s ability won\'t be involved in the new game.').
card_ruling('karn liberated', '2011-06-01', 'The player who controlled the ability that restarted the game is the starting player in the new game. The new game starts like a game normally does: -- Each player shuffles his or her deck (except the cards left in exile by Karn\'s ability). -- Each player\'s life total becomes 20 (or the starting life total for whatever format you\'re playing). -- Players draw a hand of seven cards. Players may take mulligans. -- Players may take actions based on cards in their opening hands, such as Chancellors and Leylines.').
card_ruling('karn liberated', '2011-06-01', 'After the pre-game procedure is complete, but before the new game\'s first turn, Karn\'s ability finishes resolving and the cards left in exile are put onto the battlefield. If this causes any triggered abilities to trigger, those abilities are put onto the stack at the beginning of the first upkeep step.').
card_ruling('karn liberated', '2011-06-01', 'Creatures put onto the battlefield due to Karn\'s ability will have been under their controller\'s control continuously since the beginning of the first turn. They can attack and their activated abilities with {T} in the cost can be activated.').
card_ruling('karn liberated', '2011-06-01', 'Any permanents put onto the battlefield with Karn\'s ability that entered the battlefield tapped will untap during their controller\'s first untap step.').
card_ruling('karn liberated', '2011-06-01', 'No actions taken in the game that was restarted apply to the new game. For example, if you were dealt damage by Stigma Lasher in the original game, the effect that states you can\'t gain life doesn\'t carry over to the new game.').
card_ruling('karn liberated', '2011-06-01', 'Players won\'t have any poison counters or emblems they had in the original game.').
card_ruling('karn liberated', '2011-06-01', 'In a Commander game, players put their commanders into the command zone before shuffling their deck.').
card_ruling('karn liberated', '2011-06-01', 'The number of times a player has cast his or her commander from the command zone resets to zero. Also, the amount of combat damage dealt to players by each commander is reset to 0.').
card_ruling('karn liberated', '2011-06-01', 'If a player\'s commander was exiled with Karn at the game restarted, that commander won\'t be put into the command zone at the beginning of the game. It will be put onto the battlefield when Karn\'s ability finishes resolving.').
card_ruling('karn liberated', '2011-06-01', 'In a multiplayer game using the limited range of influence option, all players are affected and will restart the game, not just those within the range of influence of the ability\'s controller.').
card_ruling('karn liberated', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('karn liberated', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('karn liberated', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('karn liberated', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('karn liberated', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('karn liberated', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('karn liberated', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('karn liberated', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('karn liberated', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('karn liberated', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('karn liberated', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('karn\'s touch', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('karn, silver golem', '2004-10-04', 'The blocking ability triggers only once no matter how many blockers are declared.').
card_ruling('karn, silver golem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('karona\'s zealot', '2004-10-04', 'If the targeted creature leaves the battlefield or stops being a creature before the damage would be dealt, then the damage is not redirected. For example, if you choose a 1/1 creature, then this card would take 3 damage, all 3 is directed to the 1/1 creature, which then dies. If this card would take an additional 3 damage later in the turn, the 1/1 creature is no longer on the battlefield so the damage is not redirected.').

card_ruling('karoo', '2006-02-01', 'This has a triggered ability when it enters the battlefield, not a replacement effect, as previously worded.').

card_ruling('karplusan minotaur', '2006-07-15', 'The coin flip rules have changed. You now win or lose a flip only if *you* flipped the coin. If your opponent loses a flip, that no longer means that you win that flip. Only coin flips caused by cards that say \"win\" and/or \"lose\" anywhere on them have a winner or loser.').
card_ruling('karplusan minotaur', '2006-07-15', 'If a spell or ability uses a coin flip to determine what happens on a heads result versus a tails result, the flipping player doesn\'t call \"heads\" or \"tails.\" Such flips have no winner or loser.').
card_ruling('karplusan minotaur', '2006-07-15', 'When Karplusan Minotaur\'s cumulative upkeep ability resolves, you either flip a number of coins equal to the number of age counters on it, or you sacrifice it. Once you start to flip, you can\'t stop; you must continue until all flips are made. If you flip, you call heads or tails for each flip. Each time you\'re right, the Minotaur\'s second ability triggers. Each time you\'re wrong, the Minotaur\'s third ability triggers. The triggers wait until after you\'re done flipping, then they all go on the stack in whatever order you choose. Each may have a different target.').
card_ruling('karplusan minotaur', '2006-07-15', 'The Minotaur\'s last two abilities will trigger whenever you win or lose any coin flip. For example, if you cast Stitch in Time, one of the Minotaur\'s abilities will trigger.').

card_ruling('karplusan yeti', '2004-10-04', 'Giving either creature first strike does not affect the ability.').
card_ruling('karplusan yeti', '2004-10-04', 'If this leaves the battlefield before its activated ability resolves, it will still deal damage to the targeted creature. On the other hand, if the targeted creature leaves the battlefield before the ability resolves, the ability will be countered and no damage will be dealt.').

card_ruling('karrthus, tyrant of jund', '2009-05-01', 'The triggered ability\'s control-change effect has no duration. Each Dragon you gain control of this way will remain under your control until the game ends, that permanent leaves the battlefield, or some other effect changes its controller.').
card_ruling('karrthus, tyrant of jund', '2009-05-01', 'When the triggered ability resolves, you\'ll gain control of all Dragons, including the Dragons you already control (such as Karrthus itself). Usually this will have no appreciable effect, though it will override any previous control-change abilities (such as if you had gained control of one of those Dragons only for the rest of the turn). Then you\'ll untap all Dragons, including the ones you already controlled and Karrthus itself (if it had somehow managed to become tapped by then).').

card_ruling('katabatic winds', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('kathari bomber', '2009-05-01', 'When the triggered ability resolves, you put two Goblin tokens onto the battlefield regardless of how much combat damage Kathari Bomber dealt to the player. You get the tokens even if you can\'t sacrifice Kathari Bomber (because it\'s no longer under your control).').

card_ruling('kathari remnant', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('kathari remnant', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('kathari remnant', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('kathari remnant', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('kathari remnant', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('kathari remnant', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('kathari remnant', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('kathari screecher', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('kathari screecher', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('kathari screecher', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('kathari screecher', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('kathari screecher', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('kavu chameleon', '2004-10-04', 'The new color replaces all previous colors.').
card_ruling('kavu chameleon', '2004-10-04', 'Counterspells can be cast that target it, but when they resolve they simply don\'t counter it since it can\'t be countered.').

card_ruling('kavu monarch', '2004-10-04', 'Does give itself Trample, but does not give itself a +1/+1 counter.').

card_ruling('kavu primarch', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('kavu primarch', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('kavu primarch', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('kavu primarch', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('kavu primarch', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('kavu primarch', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('kavu primarch', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('kavu scout', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('kavu scout', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('kavu scout', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('kavu titan', '2004-10-04', 'If the Kavu Titan was kicked, it enters the battlefield with trample and will keep this ability until it leaves the battlefield.').

card_ruling('kaysa', '2008-10-01', 'Kaysa\'s ability affects itself.').

card_ruling('kazandu blademaster', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('kazandu tuskcaller', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('kazandu tuskcaller', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('kazandu tuskcaller', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('kazandu tuskcaller', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('kazandu tuskcaller', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('kazuul warlord', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').
card_ruling('kazuul warlord', '2009-10-01', 'Unlike other Allies with similar abilities, Kazuul Warlord\'s ability lets you put +1/+1 counters on all your Ally creatures, not just itself.').

card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'You\'re the defending player if a creature is attacking you or a planeswalker you control.').
card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'If you\'re the defending player, Kazuul\'s ability triggers once for each creature that attacks, not just once per combat. The attacking player chooses whether to pay {3} or let you have an Ogre token each time one of those abilities resolves.').
card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'As the ability resolves, you can\'t put the Ogre token onto the battlefield without explicitly giving the attacking player the option to pay {3}, even if the attacking player has forgotten about this ability.').
card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'The ability triggers and resolves during the declare attackers step. If you put an Ogre token onto the battlefield as a result, you may block with it during that combat phase.').
card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'If an attacking creature has a \"Whenever this creature attacks\" ability, that ability is put on the stack first, then Kazuul\'s ability is put on the stack. Although Kazuul\'s ability will resolve first, and potentially create an Ogre token, any target of the other ability will already have been chosen at this point, and thus could not have been the new Ogre token.').
card_ruling('kazuul, tyrant of the cliffs', '2010-03-01', 'In a multiplayer game, the ability checks whether you\'re the defending player for each individual attacking creature. For example, if one creature attacks you and two creatures attack another player, Kazuul\'s ability triggers just once.').

card_ruling('kederekt leviathan', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('kederekt leviathan', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('kederekt leviathan', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('kederekt leviathan', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('kederekt leviathan', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('kederekt parasite', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').

card_ruling('keen sense', '2007-02-01', 'This is the timeshifted version of Curiosity.').
card_ruling('keen sense', '2007-02-01', 'You draw one card each time the enchanted creature damages the opponent. This is not one card per point of damage.').
card_ruling('keen sense', '2007-02-01', 'If put on your opponent\'s creature, you do not draw a card when that creature damages you. The creature has to damage your opponent in order to have this work.').
card_ruling('keen sense', '2007-02-01', 'Drawing a card is optional. If you forget, you can\'t go back later and do it, even if it is something you normally do.').

card_ruling('keeneye aven', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('keening stone', '2010-06-15', 'The value of X is determined as the ability starts to resolve. If the targeted player has five cards in his or her graveyard at that time, for example, this ability will put five more cards into that graveyard from the top of that player\'s library.').

card_ruling('keeper of the beasts', '2009-10-01', 'A different opposing player may be targeted each time the ability is activated.').
card_ruling('keeper of the beasts', '2009-10-01', 'It is only necessary that the condition be true as you activate the ability. The ability will still resolve normally even if the condition is no longer true at that time.').

card_ruling('keeper of the dead', '2009-10-01', 'A different opposing player may be targeted each time the ability is activated.').
card_ruling('keeper of the dead', '2009-10-01', 'It is only necessary that the condition be true as you activate the ability. The ability will still resolve normally even if the condition is no longer true at that time.').

card_ruling('keeper of the flame', '2009-10-01', 'A different opposing player may be targeted each time the ability is activated.').
card_ruling('keeper of the flame', '2009-10-01', 'It is only necessary that the condition be true as you activate the ability. The ability will still resolve normally even if the condition is no longer true at that time.').

card_ruling('keeper of the lens', '2015-02-25', 'Keeper of the Lens allows you to look at face-down creatures you don’t control whenever you want to, even if you don’t have priority. This action doesn’t use the stack.').
card_ruling('keeper of the lens', '2015-02-25', 'Keeper of the Lens doesn’t stop your opponents from looking at face-down creatures they control.').
card_ruling('keeper of the lens', '2015-02-25', 'Keeper of the Lens doesn’t let you look at other players’ face-down spells while they’re on the stack.').

card_ruling('keeper of the light', '2009-10-01', 'A different opposing player may be targeted each time the ability is activated.').
card_ruling('keeper of the light', '2009-10-01', 'It is only necessary that the condition be true as you activate the ability. The ability will still resolve normally even if the condition is no longer true at that time.').

card_ruling('keeper of the mind', '2009-10-01', 'A different opposing player may be targeted each time the ability is activated.').
card_ruling('keeper of the mind', '2009-10-01', 'It is only necessary that the condition be true as you activate the ability. The ability will still resolve normally even if the condition is no longer true at that time.').

card_ruling('keeper of tresserhorn', '2004-10-04', 'The ability triggers on declaration of blockers if the criteria is met.').
card_ruling('keeper of tresserhorn', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('keepsake gorgon', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('keepsake gorgon', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('keepsake gorgon', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('keldon halberdier', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('keldon halberdier', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('keldon halberdier', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('keldon halberdier', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('keldon halberdier', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('keldon halberdier', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('keldon halberdier', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('keldon halberdier', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('keldon halberdier', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('keldon halberdier', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('keldon megaliths', '2007-05-01', 'It doesn\'t matter how many cards you have in your hand when the ability resolves.').

card_ruling('keldon warlord', '2008-08-01', 'This is a Characteristic-Defining Ability. It checks the number of non-Wall creatures you control continuously, and applies in all zones. It is never \"locked in\".').

card_ruling('kemba\'s legion', '2011-06-01', 'The number of creatures Kemba\'s Legion can block is calculated when blocking creatures are declared. Take into account the number of Equipment attached to Kemba\'s Legion at that time, as well as any other effects that increase the number of creatures Kemba\'s Legion can block.').

card_ruling('kemba, kha regent', '2011-01-01', 'The number of Equipment attached to Kemba is determined as the ability resolves. If Kemba is no longer on the battlefield at that time, its last existence on the battlefield is checked to determine the number of Equipment attached to it.').

card_ruling('kentaro, the smiling cat', '2005-02-01', 'Kentaro doesn\'t change when you can cast Samurai. It just makes Samurai castable for generic mana.').
card_ruling('kentaro, the smiling cat', '2005-02-01', 'Kentaro\'s ability only applies while Kentaro is on the battlefield. You have to pay for Kentaro normally. It still costs {1}{W}.').

card_ruling('keranos, god of storms', '2014-04-26', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('keranos, god of storms', '2014-04-26', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('keranos, god of storms', '2014-04-26', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('keranos, god of storms', '2014-04-26', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('keranos, god of storms', '2014-04-26', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('keranos, god of storms', '2014-04-26', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('keranos, god of storms', '2014-04-26', 'Your devotion to two colors is equal to the number of mana symbols that are the first color, the second color, or both colors among the mana costs of permanents you control. Specifically, a hybrid mana symbol counts only once toward your devotion to its two colors. For example, if the only nonland permanents you control are Pharika, God of Affliction and Golgari Guildmage (whose mana cost is {B/G}{B/G}), your devotion to black and green is four.').
card_ruling('keranos, god of storms', '2014-04-26', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('keranos, god of storms', '2014-04-26', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('keranos, god of storms', '2014-04-26', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('kessig', '2012-06-01', 'A \"non-Werewolf creature\" is a creature that doesn\'t have the Werewolf creature type. A creature with additional types, such as a Human Werewolf, is not a non-Werewolf creature.').
card_ruling('kessig', '2012-06-01', 'Creatures that you gain control of after the chaos ability resolves won\'t get any of that ability\'s bonuses.').

card_ruling('kessig cagebreakers', '2011-09-22', 'You count the number of creature cards in your graveyard when the triggered ability resolves.').
card_ruling('kessig cagebreakers', '2011-09-22', 'You declare which player or planeswalker each token is attacking as you put it onto the battlefield. It doesn\'t have to be the same player or planeswalker Kessig Cagebreakers is attacking.').
card_ruling('kessig cagebreakers', '2011-09-22', 'Although the tokens are attacking, they were never declared as attacking creatures (for purposes of abilities that trigger whenever a creature attacks, for example).').

card_ruling('kessig malcontents', '2012-05-01', 'The number of Humans you control is counted when the enters-the-battlefield ability resolves.').

card_ruling('keymaster rogue', '2013-01-24', 'Keymaster Rogue\'s last ability isn\'t optional. If Keymaster Rogue is the only creature you control when the ability resolves, you\'ll have to return it to its owner\'s hand.').

card_ruling('khabál ghoul', '2007-09-16', 'The effect counts all creatures put into all graveyards from the battlefield during the turn. This includes creature tokens put into a graveyard as well as creatures put into a graveyard before Khabál Ghoul entered the battlefield.').

card_ruling('khalni gem', '2009-10-01', 'If you don\'t control any lands by the time Khalni Gem\'s triggered ability resolves, nothing happens. If you control just one, you\'ll have to return it to its owner\'s hand. Khalni Gem isn\'t affected in either case.').

card_ruling('khalni heart expedition', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('khalni heart expedition', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('khalni hydra', '2010-06-15', 'For the purpose of determining the cost reduction, the number of green creatures you control is checked as you cast Khalni Hydra, before your last chance to activate mana abilities to pay for it. For example, if you control Wild Cantor (a red and green creature with the ability \"Sacrifice Wild Cantor: Add one mana of any color to your mana pool\") as you cast Khalni Hydra, first the cost to cast Khalni Hydra is reduced by {G}, then you could sacrifice Wild Cantor for mana to help pay for it.').
card_ruling('khalni hydra', '2010-06-15', 'Khalni Hydra\'s cost reduction effect doesn\'t change its mana cost or converted mana cost.').
card_ruling('khalni hydra', '2010-06-15', 'If an effect (such as the one from Lodestone Golem) imposes an additional generic mana cost to casting Khalni Hydra, the Hydra\'s ability will reduce it too. It\'ll reduce the amount of green mana you need to spend first, though.').

card_ruling('kharasha foothills', '2012-06-01', 'The first ability doesn\'t trigger when a creature attacks a planeswalker, and the token copies can\'t be put onto the battlefield attacking a planeswalker.').
card_ruling('kharasha foothills', '2012-06-01', 'As each token is created, it checks the printed values of the creature it\'s copying, as well as any copy effects that have been applied to it.').
card_ruling('kharasha foothills', '2012-06-01', 'The copiable values of each token\'s characteristics are the same as the copiable values of the characteristics of the creature it\'s copying.').
card_ruling('kharasha foothills', '2012-06-01', 'You choose the target of the chaos ability as you put that ability on the stack. You don\'t sacrifice any creatures until that ability resolves.').

card_ruling('kheru bloodsucker', '2014-09-20', 'Kheru Bloodsucker’s triggered ability will trigger when it dies if its toughness is 4 or greater.').

card_ruling('kheru dreadmaw', '2014-09-20', 'Use the toughness of the creature as it last existed on the battlefield to determine how much life you gain.').

card_ruling('kheru lich lord', '2014-09-20', 'You decide whether to pay {2}{B} as the ability resolves.').
card_ruling('kheru lich lord', '2014-09-20', 'The creature card returned to the battlefield is chosen at random as the ability resolves. If any player responds to the ability, that player won’t yet know what card will be returned.').
card_ruling('kheru lich lord', '2014-09-20', 'Because the ability doesn’t target any creature card, any creature card (including Kheru Lich Lord itself) put into the graveyard in response to that ability may be returned to the battlefield.').
card_ruling('kheru lich lord', '2014-09-20', 'The ability that exiles the card at the beginning of your next end step is a delayed triggered ability. If the delayed triggered ability is countered, the creature will stay on the battlefield and the ability won’t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('kheru lich lord', '2014-09-20', 'Kheru Lich Lord grants flying, trample, and haste to the creature that’s returned to the battlefield. However, neither of the “exile” abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of your next end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('kheru lich lord', '2014-09-20', 'If a creature card returned to the battlefield with Kheru Lich Lord would leave the battlefield for any reason, it’s exiled instead. However, if that creature is already being exiled, then the replacement effect won’t apply. If the spell or ability that exiles it later returns it to the battlefield (as Suspension Field might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The effects from Kheru Lich Lord will no longer apply to it.').
card_ruling('kheru lich lord', '2014-09-20', 'The exiled creature is never put into the graveyard. Any abilities the creature has that trigger when it dies won’t trigger.').

card_ruling('kheru spellsnatcher', '2014-09-20', 'You can target a spell you control with Kheru Spellsnatcher’s triggered ability. This will give you the ability to cast the card later without paying its mana cost.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'Because you’re already casting the card using an alternative cost (by casting it without paying its mana cost), you can’t pay any other alternative costs for the card, including casting it face down using the morph ability. You can pay additional costs, such as kicker costs. If the card has any mandatory additional costs, you must pay those.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'If the card has {X} in its mana cost, you must choose 0 as the value for X when casting it.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('kheru spellsnatcher', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('kiki-jiki, mirror breaker', '2013-06-07', 'As the token is created, it checks the printed values of the creature it\'s copying -- or, if that creature is itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, or so on.').
card_ruling('kiki-jiki, mirror breaker', '2013-06-07', 'Haste is part of the token\'s copiable values. Copies of that token will also have haste.').

card_ruling('kill switch', '2004-10-04', 'Note that you can\'t choose to leave this card tapped during your untap step (unless some other effect says so).').
card_ruling('kill switch', '2004-10-04', 'Only the artifacts that it tried to tap when the ability resolved are prevented from untapping. Artifacts that enter the battlefield after that or permanents that become artifacts after that are not affected.').

card_ruling('kill-suit cultist', '2006-05-01', 'This ability causes a creature to be destroyed instead of being dealt damage. Damage replaced this way isn\'t dealt, and effects that would happen based on this damage (such as Demonfire\'s \"exile it\" effect) don\'t happen.').

card_ruling('killer instinct', '2006-02-01', 'If the top card of your library isn\'t a creature card, it remains on top of your library. You won\'t have to sacrifice it at end of turn.').

card_ruling('killing glare', '2013-01-24', 'The power of the target creature is checked both as you target it and as Killing Glare resolves. If its power is greater than the value chosen for X when Killing Glare tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('killing wave', '2012-05-01', 'First, the active player chooses whether to pay X life for each creature he or she controls. Then each other player in turn order chooses for his or her creatures. Then each player pays life and sacrifices creatures at the same time. Players will know the decisions of players who chose before them.').
card_ruling('killing wave', '2012-05-01', 'A player may choose to pay life for some creatures and sacrifice the rest. It\'s not an all-or-nothing decision.').
card_ruling('killing wave', '2012-05-01', 'You can\'t pay more life than you have.').
card_ruling('killing wave', '2012-05-01', 'If you can\'t sacrifice a creature (perhaps because of Sigarda, Host of Herons), you can choose not to pay life and nothing will happen.').

card_ruling('kiln fiend', '2010-06-15', 'If you cast an instant or sorcery spell, Kiln Fiend\'s ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').

card_ruling('kilnspire district', '2012-06-01', 'When you count the number of charge counters on Kilnspire District, count all charge counters on it, not just ones you put there.').
card_ruling('kilnspire district', '2012-06-01', 'You choose the target of the chaos ability as you put that ability on the stack. When the ability resolves, you choose a value for X and decide whether to pay {X}. If you do decide to pay {X}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('kin-tree invocation', '2014-09-20', 'The value of X is determined only once, as Kin-Tree Invocation resolves. The power and toughness of the Spirit Warrior token doesn’t later change if the greatest toughness among creatures you control changes.').
card_ruling('kin-tree invocation', '2014-09-20', 'If you control no creatures as Kin-Tree Invocation resolves, you’ll put a 0/0 token onto the battlefield. Unless something else is raising its toughness above 0, it will immediately be put into its owner’s graveyard and then cease to exist. Any abilities that trigger when a creature enters the battlefield or dies will trigger.').

card_ruling('kin-tree warden', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('kin-tree warden', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('kin-tree warden', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('kin-tree warden', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('kin-tree warden', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('kin-tree warden', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('kin-tree warden', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('kin-tree warden', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('kin-tree warden', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('kindle', '2004-10-04', 'Counts the Kindles in graveyard on resolution.').

card_ruling('kindle the carnage', '2006-05-01', 'You decide whether to repeat the process after each iteration. In other words, you discard a card, see what it is, Kindle the Carnage deals damage, then you decide whether to continue. If so, you discard a card, see what it is, Kindle the Carnage deals damage, then you decide whether to continue, and so on.').
card_ruling('kindle the carnage', '2006-05-01', 'Any damage prevention or regeneration abilities must be activated before Kindle the Carnage starts resolving in order to have any effect. You can\'t wait to see which (if any) cards are discarded.').
card_ruling('kindle the carnage', '2006-05-01', 'A creature that\'s been dealt lethal damage by an iteration of this effect will remain on the battlefield and be dealt more damage by any further iterations. After you stop, all creatures that have been dealt lethal damage are put into their owners\' graveyards at the same time.').
card_ruling('kindle the carnage', '2006-05-01', 'All the damage has the same source. However, each iteration of this process is a separate instance of damage. An effect that prevents damage the \"next time\" this card would deal it will prevent damage from only one iteration.').
card_ruling('kindle the carnage', '2006-05-01', 'A single regeneration shield is sufficient to save a particular creature, no matter how many instances of damage are on it.').

card_ruling('kindled fury', '2012-07-01', 'Giving a creature first strike after creatures with first strike deal combat damage doesn\'t prevent that creature from dealing combat damage.').

card_ruling('king cheetah', '2005-08-01', 'King Cheetah is always a Creature spell, never an instant, regardless of when you cast it.').

card_ruling('king macar, the gold-cursed', '2014-04-26', 'The player who controlled King Macar when its ability triggered will control the artifact token, not the player who controlled the creature that was exiled.').
card_ruling('king macar, the gold-cursed', '2014-04-26', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('king macar, the gold-cursed', '2014-04-26', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('king macar, the gold-cursed', '2014-04-26', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('king macar, the gold-cursed', '2014-04-26', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('kingpin\'s pet', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('kingpin\'s pet', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('kingpin\'s pet', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('kinsbaile borderguard', '2008-04-01', 'The last ability puts a token creature onto the battlefield for each counter of any kind that was on Kinsbaile Borderguard when it left the battlefield, not just for each +1/+1 counter that was on it.').

card_ruling('kinscaer harpoonist', '2008-05-01', 'You may target a creature that doesn\'t have flying.').

card_ruling('kiora\'s dismissal', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('kiora\'s dismissal', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('kiora\'s dismissal', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('kiora\'s dismissal', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('kiora\'s dismissal', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('kiora, master of the depths', '2015-08-25', 'You can activate Kiora’s first ability with no targets just to put a loyalty counter on her.').
card_ruling('kiora, master of the depths', '2015-08-25', 'With Kiora’s second ability, you could put no cards, a creature card, a land card, or a creature card and a land card into your hand.').
card_ruling('kiora, master of the depths', '2015-08-25', 'You will have the emblem by the time the Octopus creature tokens enter the battlefield—they’ll be looking to fight!').

card_ruling('kiora, the crashing wave', '2014-02-01', 'Kiora’s first ability can target any permanent an opponent controls, not just one that can deal or be dealt damage.').
card_ruling('kiora, the crashing wave', '2014-02-01', 'Once the first ability resolves, the damage prevention continues to apply even if that permanent changes controllers.').
card_ruling('kiora, the crashing wave', '2014-02-01', 'Kiora’s second ability allows you to play an additional land during your main phase. Doing so follows the normal timing rules for playing lands. In particular, you won’t play a land as that ability resolves. The ability will fully resolve (and you’ll draw a card, perhaps a land you’ll play later) first.').
card_ruling('kiora, the crashing wave', '2014-02-01', 'The effect of the second ability is cumulative with other effects that let you play additional lands, such as the one from Rites of Flourishing.').
card_ruling('kiora, the crashing wave', '2014-02-01', 'If Kiora leaves the battlefield after her second ability is activated but before it resolves, you’ll still be able to play an additional land after the ability resolves.').

card_ruling('kiri-onna', '2005-06-01', 'If there are no other creatures on the battlefield as Kiri-Onna enters the battlefield, you must target Kiri-Onna with its own ability.').

card_ruling('kismet', '2007-02-01', 'Cards enter the battlefield tapped. They do not enter the battlefield untapped and then immediately tap, therefore they do not trigger any effects due to tapping.').
card_ruling('kismet', '2007-02-01', 'It affects all opponents.').
card_ruling('kismet', '2007-02-01', 'Does not affect cards that phase in.').

card_ruling('kitchen finks', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('kitchen finks', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('kitchen finks', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('kitchen finks', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('kitchen finks', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('kitchen finks', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('kitchen finks', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('kithkin greatheart', '2013-06-07', 'If, after dealing first-strike combat damage, Kithkin Greatheart loses first strike (perhaps because the Giant you controlled was destroyed by first-strike combat damage), it won\'t deal regular combat damage.').

card_ruling('kithkin mourncaller', '2007-10-01', 'If an attacking creature that\'s both a Kithkin and an Elf is put into your graveyard from the battlefield, Kithkin Mourncaller\'s ability triggers only once.').

card_ruling('kithkin spellduster', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('kithkin spellduster', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('kithkin spellduster', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('kithkin spellduster', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('kithkin spellduster', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('kithkin spellduster', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('kithkin spellduster', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('kithkin zealot', '2008-08-01', 'Each permanent is counted only once. For example, if the targeted player controls a black creature, a red enchantment, and a black-red creature, you\'ll gain 3 life.').

card_ruling('kithkin zephyrnaut', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('kithkin zephyrnaut', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('kithkin zephyrnaut', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('kithkin zephyrnaut', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('kithkin zephyrnaut', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('kithkin zephyrnaut', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('kitsune palliator', '2005-02-01', 'Kitsune Palliator\'s ability creates prevention shields for every creature and player when it resolves. It doesn\'t affect creatures that enter the battlefield (or become creatures) later.').

card_ruling('kiyomaro, first to stand', '2005-06-01', 'Kiyomaro\'s power and toughness don\'t affect whether it has vigilance or the life-gain ability.').
card_ruling('kiyomaro, first to stand', '2005-06-01', 'Losing vigilance after attackers are declared doesn\'t cause Kiyomaro to tap.').
card_ruling('kiyomaro, first to stand', '2005-06-01', 'If you have seven or more cards in hand when Kiyomaro\'s triggered ability goes on the stack but less than seven when the ability resolves, the ability does nothing.').

card_ruling('kjeldoran dead', '2004-10-04', 'The sacrifice is a triggered ability and not a cost.').
card_ruling('kjeldoran dead', '2008-10-01', 'When Kjeldoran Dead\'s enters-the-battlefield ability resolves, if you don\'t control any other creatures, you must sacrifice Kjeldoran Dead itself.').

card_ruling('kjeldoran escort', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran escort', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran escort', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran escort', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('kjeldoran frostbeast', '2009-10-01', 'Kjeldoran Frostbeast\'s ability triggers only if it\'s still on the battlefield when the end of combat step begins (after the combat damage step). For example, if it blocks a 7/7 creature and is destroyed, its ability won\'t trigger at all. The 7/7 will remain on the battlefield.').
card_ruling('kjeldoran frostbeast', '2009-10-01', 'Its ability will destroy all creatures it\'s currently blocking or blocked by, not necessarily the creatures it had blocked or become blocked by during that combat phase. For example, if Kjeldoran Frostbeast is dealt lethal combat damage and regenerates, it\'s removed from combat. Its ability will trigger at end of combat, but since it\'s no longer blocking or blocked by any creatures, the ability won\'t do anything.').

card_ruling('kjeldoran home guard', '2008-10-01', 'The ability will trigger only if Kjeldoran Home Guard is still on the battlefield at end of combat. If it\'s been dealt lethal combat damage and destroyed during that combat (or has left the battlefield during that combat by any other means), its ability won\'t trigger and you won\'t get a token.').
card_ruling('kjeldoran home guard', '2008-10-01', 'You put the Deserter token onto the battlefield even if you can\'t put a -0/-1 counter on Kjeldoran Home Guard (because it left the battlefield after its ability triggered but before it resolved, for example).').

card_ruling('kjeldoran javelineer', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('kjeldoran knight', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran knight', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran knight', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran knight', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('kjeldoran outpost', '2004-10-04', 'You have to sacrifice a plains before this card is put onto the battlefield. You have to do this no matter how it is put onto the battlefield.').

card_ruling('kjeldoran phalanx', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran phalanx', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran phalanx', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran phalanx', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('kjeldoran pride', '2004-10-04', 'The activated ability moves this card to a new permanent.').

card_ruling('kjeldoran royal guard', '2004-10-04', 'If you activate the ability but Kjeldoran Royal Guard leaves the battlefield before combat damage is dealt, the combat damage from unblocked creatures won\'t be redirected. It will be dealt to you as normal.').

card_ruling('kjeldoran skycaptain', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran skycaptain', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran skycaptain', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran skycaptain', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('kjeldoran skyknight', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran skyknight', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran skyknight', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran skyknight', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('kjeldoran warrior', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('kjeldoran warrior', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('kjeldoran warrior', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('kjeldoran warrior', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('knacksaw clique', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('knacksaw clique', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('knacksaw clique', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('knacksaw clique', '2008-05-01', 'The exiled card is played using the normal timing rules for its card type, as well as any other applicable restrictions such as \"Cast [this card] only during combat.\" For example, you can\'t play the card during an opponent\'s turn unless it\'s an instant or has flash. Similarly, if the exiled card is a land, you can\'t play it if you\'ve already played a land that turn. If it\'s a nonland card, you\'ll have to pay its mana cost. The only thing that\'s different is you\'re playing it from the Exile zone.').
card_ruling('knacksaw clique', '2008-05-01', 'If you don\'t play the exiled card that turn, it remains exiled but you can\'t play it.').

card_ruling('knight exemplar', '2010-08-15', 'If an effect would simultaneously destroy Knight Exemplar and another Knight creature you control, only Knight Exemplar is destroyed.').
card_ruling('knight exemplar', '2013-07-01', 'Lethal damage, damage from a source with deathtouch, and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('knight exemplar', '2013-07-01', 'If another Knight creature you control is dealt lethal damage, the creature isn\'t destroyed, but the damage remains marked on it. If, at some point later in that turn, you no longer control Knight Exemplar or it loses its abilities, the other Knight creature will lose indestructible and will be destroyed.').
card_ruling('knight exemplar', '2013-07-01', 'If you control two Knight Exemplars, each one causes the other to get +1/+1 and have indestructible.').

card_ruling('knight of cliffhaven', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('knight of cliffhaven', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('knight of cliffhaven', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('knight of cliffhaven', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('knight of cliffhaven', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('knight of new alara', '2009-05-01', 'For example, a white creature gets no bonus. A white and blue creature gets +2/+2. A white, blue, and black creature gets +3/+3, and so on.').

card_ruling('knight of obligation', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('knight of obligation', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('knight of obligation', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('knight of sursi', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('knight of sursi', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('knight of sursi', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('knight of sursi', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('knight of sursi', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('knight of sursi', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('knight of sursi', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('knight of sursi', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('knight of sursi', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('knight of sursi', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('knight of the holy nimbus', '2006-09-25', 'The second ability essentially means that Knight of the Holy Nimbus always has a regeneration shield. It retains that shield even if it\'s used.').
card_ruling('knight of the holy nimbus', '2006-09-25', 'After the third ability resolves, regeneration shields don\'t work on Knight of the Holy Nimbus for the remainder of the turn. This includes its own automatic shield as well as shields from other sources.').

card_ruling('knight of the mists', '2004-10-04', 'If no other Knights are on the battlefield, pay the mana or destroy this card.').
card_ruling('knight of the mists', '2004-10-04', 'Can destroy an opponent\'s knight.').

card_ruling('knight of the pilgrim\'s road', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('knight of the pilgrim\'s road', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('knight of the pilgrim\'s road', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('knight of the reliquary', '2009-02-01', 'The activated ability\'s cost checks the land\'s subtype, not its name. You can sacrifice a nonbasic land this way as long as it has the subtype Forest or Plains.').
card_ruling('knight of the reliquary', '2009-02-01', 'Sacrificing a Forest or Plains is part of the cost of Knight of the Reliquary\'s activated ability. Assuming that sacrificing the land puts it into your graveyard, Knight of the Reliquary\'s first ability will immediately give it an additional +1/+1 when that cost is paid because there\'ll be a new land card in your graveyard. Paying a cost can\'t be responded to (with Shock, for example).').
card_ruling('knight of the reliquary', '2009-02-01', 'Knight of the Reliquary\'s activated ability lets you find any land card, not just a basic land card.').

card_ruling('knight of the white orchid', '2008-10-01', 'Knight of the White Orchid\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless any one of your opponents controls more lands than you, and (2) the ability will do nothing if you control at least as many lands as each of your opponents by the time it resolves.').
card_ruling('knight of the white orchid', '2015-06-22', 'The Plains you search for doesn’t have to be basic. For example, you could put a Sacred Foundry onto the battlefield.').

card_ruling('knight of valor', '2004-10-04', 'The second ability is not Flanking and is not removed by an effect that removes Flanking.').

card_ruling('knight-captain of eos', '2008-10-01', 'You can sacrifice any Soldier to activate the second ability. You\'re not limited to just the Soldier tokens put onto the battlefield by the first ability.').

card_ruling('knightly valor', '2015-06-22', 'You must target a creature to cast Knightly Valor. If that creature is an illegal target when Knightly Valor tries to resolve, it will be countered and won’t enter the battlefield. You won’t get the Knight token.').

card_ruling('knights of thorn', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('knights of thorn', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('knights of thorn', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('knights of thorn', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('knollspine dragon', '2008-05-01', 'You target an opponent when the ability triggers. You don\'t decide whether you want to discard and draw until the ability resolves.').
card_ruling('knollspine dragon', '2008-05-01', 'This ability counts the total amount of damage (both combat and noncombat) dealt to the targeted opponent by all sources (including ones you controlled and ones you didn\'t) over the course of the turn. Damage that was prevented or replaced doesn\'t count. Damage that resolved but didn\'t cause loss of life (due to Worship, for example) will count.').
card_ruling('knollspine dragon', '2008-05-01', 'Knollspine Dragon\'s ability checks only whether damage was dealt. It doesn\'t care whether the player\'s life total also changed for other reasons (such as if the player paid life or gained life). For example, if an opponent was dealt 4 damage and gained 6 life during the turn, that player will have a higher life total than he or she started the turn with -- but Knollspine Dragon\'s ability will let you discard your hand and draw four cards.').

card_ruling('knotvine paladin', '2009-05-01', 'The number of untapped creatures you control is counted as the ability resolves, and the bonus is locked in at that time. By this point, other attacking creatures will already be tapped (unless they have vigilance).').

card_ruling('knowledge and power', '2014-04-26', 'Knowledge and Power’s ability triggers only once each time you scry, no matter how many cards you look at.').
card_ruling('knowledge and power', '2014-04-26', 'You choose the target of the ability as it’s put on the stack. You choose whether to pay {2} as it resolves. You can pay {2} only once each time you scry.').

card_ruling('knowledge exploitation', '2008-04-01', 'You cast the instant or sorcery card as part of the resolution of this spell. It\'s cast from your opponent\'s library, not your hand. You choose modes, pay additional costs, choose targets, etc. for the spell as normal when casting it. Any X in the mana cost will be 0. Alternative costs can\'t be paid.').
card_ruling('knowledge exploitation', '2008-04-01', 'If you can\'t find an instant or sorcery card that can be legally cast, or choose not to find one, skip that part of the effect. Then the opponent shuffles his or her library.').

card_ruling('knowledge pool', '2011-06-01', 'You may cast any other card exiled by Knowledge Pool, including one owned by an opponent. Any card exiled by Knowledge Pool\'s enters-the-battlefield ability or its other triggered ability may be cast.').
card_ruling('knowledge pool', '2011-06-01', 'The spell a player casts from his or her hand won\'t resolve if it\'s exiled, even if that spell can\'t be countered by spells or abilities.').
card_ruling('knowledge pool', '2011-06-01', 'If the original spell isn\'t exiled (perhaps because it\'s countered by another spell or ability before Knowledge Pool\'s second triggered ability resolves), the rest of the ability does nothing. The player doesn\'t get to cast an exiled card.').
card_ruling('knowledge pool', '2011-06-01', 'If there are no nonland cards exiled by Knowledge Pool, the original spell is still exiled.').
card_ruling('knowledge pool', '2011-06-01', 'You may pay additional costs, such as kicker costs, of the exiled card.').
card_ruling('knowledge pool', '2011-06-01', 'If the card has any mandatory additional costs, as Kuldotha Rebirth does, you must pay them in order to cast the spell.').
card_ruling('knowledge pool', '2011-06-01', 'Timing restrictions based on the card\'s type are ignored. For example, you can cast an exiled creature card this way. Other restrictions, such as Spinal Embrace\'s \"Cast Spinal Embrace only during combat\" are not ignored.').
card_ruling('knowledge pool', '2011-06-01', 'If the card you cast without paying its mana cost has an X in its mana cost, you must choose 0 as its value.').
card_ruling('knowledge pool', '2011-06-01', 'Any other abilities that trigger when you cast the original spell (for example, if you cast Emrakul, the Aeons Torn) will still trigger and go on the stack.').
card_ruling('knowledge pool', '2011-06-01', 'If multiple Knowledge Pools are on the battlefield, keep track of which cards are exiled by each of them. Whenever a player casts a spell from his or her hand: -- If all Knowledge Pools are controlled by the same player, that player chooses the order in which the triggered abilities are put onto the stack. The last one put onto the stack will be the first to resolve. -- If multiple players each control one or more Knowledge Pools, the active player put his or her triggered abilities on the stack in any order, then each other player in turn order does the same. The last ability put onto the stack this way will be the first to resolve. -- The first triggered ability to resolve will exile the original spell, then the player who cast that spell may cast one of the nonland cards exiled by the Knowledge Pool that generated that triggered ability. The abilities of other Knowledge Pools will do nothing when they resolve, as the original spell will already have been exiled.').

card_ruling('knowledge vault', '2008-10-01', 'You can activate the second ability any time you have priority, including in response to itself. However, you will only be able to sacrifice the Vault as the first such ability resolves.').
card_ruling('knowledge vault', '2009-10-01', 'You don\'t get to look at the cards exiled with Knowledge Vault.').
card_ruling('knowledge vault', '2009-10-01', 'If you activate Knowledge Vault\'s second ability, all that happens at that time is that you pay a cost of {0}. As the ability resolves, you must sacrifice Knowledge Vault. This is mandatory. If you are able to do so, you discard your hand and put all cards exiled with Knowledge Vault into their owners\' hands. If you aren\'t able to do so because Knowledge Vault left the battlefield or changed controllers before the ability resolves, nothing happens.').

card_ruling('kobolds of kher keep', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('kodama of the center tree', '2005-02-01', 'Soulshift is a leaves the battlefield trigger, so the gamestate is referenced immediately before the Soulshift trigger to determine the value of X. Soulshift X includes Kodama of the Center Tree. So, X is always at least 1.').
card_ruling('kodama of the center tree', '2005-02-01', 'Kodama of the Center Tree can return itself to its owner\'s hand if you control five or more Spirits when it is put into a graveyard from the battlefield.').

card_ruling('kodama\'s might', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('kodama\'s might', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('kodama\'s might', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('kodama\'s might', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('kodama\'s reach', '2004-12-01', 'You can get just one basic land card if you wish (or even none at all). If you get only one land card, you put it onto the battlefield tapped.').

card_ruling('kolaghan aspirant', '2015-02-25', 'If Kolaghan Aspirant becomes blocked by multiple creatures, its ability will trigger once for each of them.').
card_ruling('kolaghan aspirant', '2015-02-25', 'The damage Kolaghan Aspirant deals because of its triggered ability isn’t combat damage. If that damage causes each creature blocking it to leave combat, Kolaghan Aspirant will remain blocked. It won’t be dealt combat damage in this case.').

card_ruling('kolaghan forerunners', '2015-02-25', 'The ability that defines Kolaghan Forerunners’s power functions in all zones, not just the battlefield. If Kolaghan Forerunners is on the battlefield, its ability will count itself.').
card_ruling('kolaghan forerunners', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('kolaghan forerunners', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('kolaghan forerunners', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('kolaghan forerunners', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('kolaghan monument', '2015-02-25', 'A Monument can’t attack on the turn it enters the battlefield.').
card_ruling('kolaghan monument', '2015-02-25', 'Each Monument is colorless, although the last ability will make each of them two colors until end of turn.').
card_ruling('kolaghan monument', '2015-02-25', 'If a Monument has any +1/+1 counters on it, those counters will remain on the permanent after it stops being a creature. Those counters will have no effect as long as the Monument isn’t a creature, but they will apply again if the Monument later becomes a creature.').
card_ruling('kolaghan monument', '2015-02-25', 'Activating the last ability of a Monument while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

card_ruling('kolaghan skirmisher', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('kolaghan skirmisher', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('kolaghan skirmisher', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('kolaghan skirmisher', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('kolaghan stormsinger', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('kolaghan stormsinger', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('kolaghan stormsinger', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('kolaghan\'s command', '2015-02-25', 'You choose the two modes as you cast the spell. You must choose two different modes. Once modes are chosen, they can’t be changed.').
card_ruling('kolaghan\'s command', '2015-02-25', 'You can choose a mode only if you can choose legal targets for that mode. Ignore the targeting requirements for modes that aren’t chosen. For example, you can cast Ojutai’s Command without targeting a creature spell provided you don’t choose the third mode.').
card_ruling('kolaghan\'s command', '2015-02-25', 'As the spell resolves, follow the instructions of the modes you chose in the order they are printed on the card. For example, if you chose the second and fourth modes of Ojutai’s Command, you would gain 4 life and then draw a card. (The order won’t matter in most cases.)').
card_ruling('kolaghan\'s command', '2015-02-25', 'If a Command is copied, the effect that creates the copy will usually allow you to choose new targets for the copy, but you can’t choose new modes.').
card_ruling('kolaghan\'s command', '2015-02-25', 'If all targets for the chosen modes become illegal before the Command resolves, the spell will be countered and none of its effects will happen. If at least one target is still legal, the spell will resolve but will have no effect on any illegal targets.').

card_ruling('kolaghan, the storm\'s fury', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('kolaghan, the storm\'s fury', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('kolaghan, the storm\'s fury', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('kolaghan, the storm\'s fury', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('konda\'s banner', '2004-12-01', 'A creature can\'t get more than +2/+2 from Konda\'s Banner. Sharing more than one color or more than one creature type with the equipped creature does nothing.').
card_ruling('konda\'s banner', '2004-12-01', 'The equipped creature shares a color with itself, as long as it has one or more colors.').
card_ruling('konda\'s banner', '2004-12-01', 'The equipped creature shares a creature type with itself, as long as it has one or more creature types. \"Artifact\" is not a creature type.').

card_ruling('kor bladewhirl', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('kor bladewhirl', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('kor castigator', '2015-08-25', 'A creature that’s an Eldrazi but not a Scion (or vice versa) can block Kor Castigator.').

card_ruling('kor dirge', '2007-02-01', 'This is the timeshifted version of Kor Chant.').

card_ruling('kor entanglers', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('kor entanglers', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('kor hookmaster', '2009-10-01', 'You may target any creature an opponent controls. It\'s okay if it\'s already tapped.').
card_ruling('kor hookmaster', '2009-10-01', 'If the targeted creature is untapped at the time its controller\'s next untap step begins, this ability has no effect. It won\'t apply at some later time when the targeted creature is tapped.').
card_ruling('kor hookmaster', '2009-10-01', 'If the affected creature changes controllers before its old controller\'s next untap step, this ability will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('kor line-slinger', '2010-06-15', 'The power of the targeted creature is checked both as you target it and as the ability resolves.').

card_ruling('kor outfitter', '2009-10-01', 'You may target Kor Outfitter with its ability.').
card_ruling('kor outfitter', '2009-10-01', 'If either target is illegal by the time the ability resolves, the ability won\'t do anything. If both targets are illegal, the ability is countered.').

card_ruling('kor skyfisher', '2009-10-01', 'The triggered ability doesn\'t target a permanent. You choose which one to return to its owner\'s hand as the ability resolves. No one can respond to the choice.').
card_ruling('kor skyfisher', '2009-10-01', 'If Kor Skyfisher is still on the battlefield as its triggered ability resolves, you may return Kor Skyfisher itself.').

card_ruling('kor spiritdancer', '2010-06-15', 'If you cast an Aura spell, Kor Spiritdancer\'s second ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').
card_ruling('kor spiritdancer', '2010-06-15', 'The second ability triggers when you cast any Aura spell, not just one that targets Kor Spiritdancer.').

card_ruling('kormus bell', '2004-10-04', 'The lands are both lands and creatures at the same time. They are affected by anything that affects either permanent type.').
card_ruling('kormus bell', '2004-10-04', 'It affects Swamps controlled by any and all players.').
card_ruling('kormus bell', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('korozda guildmage', '2012-10-01', 'Use the sacrificed creature\'s toughness when it was last on the battlefield to determine the value of X.').

card_ruling('korozda monitor', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('koskun falls', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').
card_ruling('koskun falls', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('koskun falls', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('koth of the hammer', '2011-01-01', 'Emblems behave similarly to enchantments: They have an ability that, in a general sense, continually affects the game. The primary difference between them is that emblems aren\'t permanents and don\'t exist on the battlefield. Nothing in the game can remove an emblem, simply because no other spell or ability references them. Once you get an emblem, you keep it for the rest of the game. Emblems have no color, name, card type, or other characteristics beyond the listed ability').
card_ruling('koth of the hammer', '2011-01-01', 'Koth\'s first ability can target any Mountain, including an untapped Mountain and/or a Mountain another player controls.').
card_ruling('koth of the hammer', '2011-01-01', 'If Koth\'s first ability animates a Mountain that came under your control that turn, it will have \"summoning sickness\" and be unable to attack. It will also be unable to be tapped to activate an ability with the {T} symbol in its cost, such as the Mountain\'s mana ability or the ability granted to it by Koth\'s emblem.').
card_ruling('koth of the hammer', '2011-01-01', 'Loyalty abilities can\'t be mana abilities. Koth\'s second ability uses the stack and can be countered or otherwise responded to. Like all loyalty abilities, it can be activated only once per turn, during your main phase, when the stack is empty, and only if no other loyalty abilities of this permanent have been activated this turn.').
card_ruling('koth of the hammer', '2011-01-01', 'Koth\'s emblem grants an activated ability to each Mountain you control at any given time for the rest of the game. It will continuously check which permanents you control are Mountains to determine what has the ability. For example, a Mountain that comes under your control later in the game will have the ability, while a Mountain you controlled at the time the emblem was created, but that later came under the control of another player, will no longer have the ability.').
card_ruling('koth of the hammer', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('koth of the hammer', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('koth of the hammer', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('koth of the hammer', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('koth of the hammer', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('koth of the hammer', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('koth of the hammer', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('koth of the hammer', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('koth of the hammer', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('koth of the hammer', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('koth of the hammer', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('kothophed, soul hoarder', '2015-06-22', 'It doesn’t matter who controlled the permanent when it was put into a graveyard.').
card_ruling('kothophed, soul hoarder', '2015-06-22', 'The triggered ability is mandatory. You can’t decline to draw the card and lose life, even if you want to.').

card_ruling('kozilek\'s sentinel', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('kozilek\'s sentinel', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('kozilek\'s sentinel', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('kozilek\'s sentinel', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('kozilek\'s sentinel', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('kozilek, butcher of truth', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('kozilek, butcher of truth', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('kozilek, butcher of truth', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').

card_ruling('kragma butcher', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('kragma butcher', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('kragma butcher', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('kragma butcher', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('kraken of the straits', '2014-02-01', 'The number of Islands you control is counted as blockers are declared. Creatures with power less than that number at that time can’t block Kraken of the Straits. Once blockers have been declared, the power of the blocking creatures and the number of Islands you control don’t matter.').

card_ruling('kraken\'s eye', '2009-10-01', 'The ability triggers whenever any player, not just you, casts a blue spell.').
card_ruling('kraken\'s eye', '2009-10-01', 'If a player casts a blue spell, Kraken\'s Eye\'s ability triggers and is put on the stack on top of that spell. Kraken\'s Eye\'s ability will resolve (causing you to gain 1 life) before the spell does.').

card_ruling('krark\'s thumb', '2004-10-04', 'If you and your opponent both flip at the same time, you can see your opponent\'s result before choosing which result to keep.').
card_ruling('krark\'s thumb', '2013-04-15', 'If an effect tells you to flip more than one coin at once, this replace each individual coin flip. For example, if an effect tells you to flip two coins, you\'ll first flip two coins and ignore one, then flip two more coins and ignore one of those. You don\'t flip four coins and ignore two.').

card_ruling('krasis incubation', '2013-04-15', 'Returning Krasis Incubation to its owner\'s hand is part of the cost to activate its last ability. Once that ability is announced, players can\'t respond to it until after you\'ve paid its activation cost and returned Krasis Incubation to hand.').
card_ruling('krasis incubation', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('krenko, mob boss', '2012-07-01', 'The ability counts each Goblin you control, including Krenko itself, not just the tokens it creates.').

card_ruling('kresh the bloodbraided', '2008-10-01', 'X is equal to the power of the creature as it last existed on the battlefield.').

card_ruling('krond the dawn-clad', '2012-06-01', 'If Krond isn\'t enchanted when it\'s declared as an attacking creature, the ability won\'t trigger. If Krond isn\'t enchanted when the ability resolves, the ability won\'t do anything.').

card_ruling('krosa', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('krosa', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('krosa', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('krosa', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('krosan grip', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('krosan grip', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('krosan grip', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('krosan grip', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('krosan grip', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('krosan grip', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('krosan tusker', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('krosan tusker', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('krosan tusker', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('krosan wayfarer', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('krovikan fetish', '2005-08-01', 'You do not get a card if the target becomes illegal and Krovikan Fetish is countered upon resolution. This is because it never successfully enters the battlefield.').
card_ruling('krovikan fetish', '2005-08-01', 'If it is moved to a new creature by something that moves Auras, the controller of the Fetish does not get to draw another card.').

card_ruling('krovikan plague', '2004-10-04', 'The Aura will be put into the graveyard if you lose control of the creature or if it somehow becomes a Wall.').

card_ruling('krovikan vampire', '2004-10-04', 'At the beginning of each end step, the game checks all creatures that died that turn to see if Krovikan Vampire had dealt damage to them during the turn. It doesn\'t matter whether the damage was combat damage, and it doesn\'t matter whether the damage was what caused the creatures to die.').
card_ruling('krovikan vampire', '2004-10-04', 'If a creature Krovikan Vampire dealt damage to dies, then leaves the graveyard by some means, the game loses track of it. Krovikan Vampire\'s ability won\'t be able to return it to the battlefield, even if it\'s put back into the graveyard before the end step.').
card_ruling('krovikan vampire', '2008-08-01', 'Whoever controls Krovikan Vampire when the ability triggers will return the applicable cards to the battlefield under his or her control. It doesn\'t matter who controlled it at the time the damage was dealt.').
card_ruling('krovikan vampire', '2008-08-01', 'If Krovikan Vampire isn\'t on the battlefield at the beginning of the end step, its ability won\'t trigger. No cards will be returned to the battlefield, even if Krovikan Vampire dealt damage to some creatures that died that turn.').
card_ruling('krovikan vampire', '2008-10-01', 'If a creature that Krovikan Vampire dealt damage to that turn dies in response to the triggered ability, that creature card will be put onto the battlefield. Note that an appropriate creature must have died prior to the beginning of the end step in order for the ability to trigger in the first place.').

card_ruling('kruin outlaw', '2011-09-22', 'If Kruin Outlaw somehow transforms after blockers have been declared but before combat ends, any Werewolves you control that are blocked by a single creature will remain blocked.').

card_ruling('krumar bond-kin', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('krumar bond-kin', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('krumar bond-kin', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('krumar bond-kin', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('krumar bond-kin', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('krumar bond-kin', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('krumar bond-kin', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('krumar bond-kin', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('krumar bond-kin', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('kruphix, god of horizons', '2014-04-26', 'As long as Kruphix is on the battlefield, unused mana will remain in your mana pool as steps and phases end (although it will become colorless). This means you can add mana to your mana pool and spend it during a future step, phase, or turn. Once Kruphix leaves the battlefield, you have until the end of the current step or phase to use the mana before it disappears.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'If unused mana in your mana pool has any restrictions or riders associated with it (for example, if it was produced by Cavern of Souls), those restrictions or riders will remain associated with that mana when it becomes colorless.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('kruphix, god of horizons', '2014-04-26', 'Your devotion to two colors is equal to the number of mana symbols that are the first color, the second color, or both colors among the mana costs of permanents you control. Specifically, a hybrid mana symbol counts only once toward your devotion to its two colors. For example, if the only nonland permanents you control are Pharika, God of Affliction and Golgari Guildmage (whose mana cost is {B/G}{B/G}), your devotion to black and green is four.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('kruphix, god of horizons', '2014-04-26', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('kudzu', '2004-10-04', 'You can move it to any other player\'s land whenever you get to move it.').
card_ruling('kudzu', '2005-08-01', 'If Kudzu is destroyed directly, or the land is destroyed by a spell or ability, then Kudzu goes to the graveyard like any Aura would.').
card_ruling('kudzu', '2008-04-01', 'The controller of the destroyed land must attach it to another land if possible. If there are no valid choices, Kudzu is simply put into the graveyard.').
card_ruling('kudzu', '2008-04-01', 'Because the ability isn\'t targeted, the controller of the destroyed land may attach it to a land that can\'t be the target of abilities.').

card_ruling('kukemssa pirates', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('kuldotha flamefiend', '2011-06-01', 'Kuldotha Flamefiend\'s ability may target up to four creatures and/or players. Each must be chosen to receive at least 1 damage.').
card_ruling('kuldotha flamefiend', '2011-06-01', 'You choose the target(s) and how the damage will be divided when you put the triggered ability on the stack. You don\'t choose whether to sacrifice an artifact or which artifact you\'re sacrificing until the ability resolves. Once you decide to sacrifice an artifact (or not), it\'s too late for players to respond.').

card_ruling('kuldotha forgemaster', '2011-01-01', 'Kuldotha Forgemaster can be one of the three artifacts you sacrifice to activate the ability.').

card_ruling('kuldotha phoenix', '2011-01-01', 'You can activate Kuldotha Phoenix\'s ability only if it\'s in your graveyard.').

card_ruling('kulrath knight', '2008-05-01', 'This checks your opponents\' creatures for any kind of counters, not just -1/-1 counters.').

card_ruling('kumano\'s pupils', '2004-12-01', 'If a creature dealt damage by Kumano\'s Pupils this turn would be put into a graveyard, exile it instead, even if it\'s going to the graveyard at the same time as Kumano\'s Pupils.').
card_ruling('kumano\'s pupils', '2004-12-01', 'Kumano\'s Pupils must be on the battlefield when the creature would be put into a graveyard in order to exile it.').

card_ruling('kumano, master yamabushi', '2004-12-01', 'Kumano, Master Yamabushi must be on the battlefield when the creature would be put into a graveyard in order to exile it.').
card_ruling('kumano, master yamabushi', '2004-12-01', 'If a creature dealt damage by Kumano this turn would be put into a graveyard, exile it, even if it\'s going to the graveyard at the same time as Kumano.').

card_ruling('kuon, ogre ascendant', '2005-06-01', 'Kuon\'s flip ability looks at everything that happened during that turn, even before Kuon entered the battlefield.').
card_ruling('kuon, ogre ascendant', '2005-06-01', 'Creatures put into graveyards after the end-of-turn step begins have no particular effect on Kuon.').
card_ruling('kuon, ogre ascendant', '2005-06-01', 'Each Ascendant is legendary in both its unflipped and flipped forms. This means that effects that look for legendary creature cards, such as Time of Need and Captain Sisay, can find an Ascendant.').

card_ruling('kurkesh, onakke ancient', '2014-07-18', 'Activated abilities are written in the form “Cost: Effect.” Some keywords are activated abilities and will have colons in their reminder texts.').
card_ruling('kurkesh, onakke ancient', '2014-07-18', 'A mana ability is an ability that (1) could put mana into a player’s mana pool when it resolves, (2) isn’t a loyalty ability, and (3) doesn’t target.').
card_ruling('kurkesh, onakke ancient', '2014-07-18', 'The copy will have the same targets as the ability it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('kurkesh, onakke ancient', '2014-07-18', 'If the ability is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('kurkesh, onakke ancient', '2014-07-18', 'If the ability has {X} in its cost, the value of X is copied.').
card_ruling('kurkesh, onakke ancient', '2014-07-18', 'If paying the activation cost of the ability includes sacrificing Kurkesh, the ability won’t be copied. At the time the ability is considered activated (after all costs are paid), Kurkesh is no longer on the battlefield.').

card_ruling('kyoki, sanity\'s eclipse', '2005-02-01', 'Cards exiled by Kyoki\'s ability are exiled face up.').
card_ruling('kyoki, sanity\'s eclipse', '2005-02-01', 'Cards exiled by Kyoki\'s ability stay exiled even if Kyoki leaves the battlefield.').

card_ruling('kyscu drake', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('kytheon\'s irregulars', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('kytheon\'s irregulars', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('kytheon\'s irregulars', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('kytheon\'s tactics', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('kytheon, hero of akros', '2015-06-22', 'Kytheon’s first ability will count creatures that attacked but are no longer on the battlefield (perhaps because they didn’t survive combat damage being dealt). It will not count any creatures that were put onto the battlefield attacking, as those creatures were never declared as attackers.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'Each face of a double-faced card has its own set of characteristics: name, types, subtypes, power and toughness, loyalty, abilities, and so on. While a double-faced card is on the battlefield, consider only the characteristics of the face that’s currently up. The other set of characteristics is ignored. While a double-faced card isn’t on the battlefield, consider only the characteristics of its front face.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'The converted mana cost of a double-faced card not on the battlefield is the converted mana cost of its front face.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'The back face of a double-faced card doesn’t have a mana cost. A double-faced permanent with its back face up has a converted mana cost of 0. Each back face has a color indicator that defines its color.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'The back face of a double-faced card (in the case of Magic Origins, the planeswalker face) can’t be cast.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'Although the two rules are similar, the “legend rule” and the “planeswalker uniqueness rule” affect different kinds of permanents. You can control two of this permanent, one front face-up and the other back-face up at the same time. However, if the former is exiled and enters the battlefield transformed, you’ll then control two planeswalkers with the same subtype. You’ll choose one to remain on the battlefield, and the other will be put into its owner’s graveyard.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'A double-faced card enters the battlefield with its front face up by default, unless a spell or ability instructs you to put it onto the battlefield transformed, in which case it enters with its back face up.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'A Magic Origins planeswalker that enters the battlefield because of the ability of its front face will enter with loyalty counters as normal.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'In some rare cases, a spell or ability may cause one of these five cards to transform while it’s a creature (front face up) on the battlefield. If this happens, the resulting planeswalker won’t have any loyalty counters on it and will subsequently be put into its owner’s graveyard.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'You can activate one of the planeswalker’s loyalty abilities the turn it enters the battlefield. However, you may do so only during one of your main phases when the stack is empty. For example, if the planeswalker enters the battlefield during combat, there will be an opportunity for your opponent to remove it before you can activate one of its abilities.').
card_ruling('kytheon, hero of akros', '2015-06-22', 'If a double-faced card is manifested, it will be put onto the battlefield face down (this is also true if it’s put onto the battlefield face down some other way). Note that “face down” is not synonymous with “with its back face up.” A manifested double-faced card is a 2/2 creature with no name, mana cost, creature types, or abilities. While face down, it can’t transform. If the front face of a manifested double-faced card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced card on the battlefield can’t be turned face down.').

