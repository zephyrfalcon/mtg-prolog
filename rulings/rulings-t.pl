% Rulings

card_ruling('tablet of the guilds', '2012-10-01', 'You must choose two different colors. Colorless is not a color.').

card_ruling('tahngarth\'s rage', '2004-10-04', 'The effect does not receive a new timestamp when the creature goes from attacking to not attacking or the other way around.').

card_ruling('taiga', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('taiga', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('taigam\'s strike', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('taigam\'s strike', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('taigam\'s strike', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('taigam\'s strike', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('taigam\'s strike', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('taigam\'s strike', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('taigam\'s strike', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('taigam\'s strike', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('tail slash', '2015-02-25', 'If either creature is an illegal target as Tail Slash tries to resolve, the creature you control won’t deal damage.').

card_ruling('tainted remedy', '2015-06-22', 'If more than one replacement effect tries to apply to a life gain event, the player who would gain life chooses the order in which they apply. For example, if a player who controls Alhammarret’s Archive would gain 3 life while Tainted Remedy is on the battlefield, that player may choose to have the 3 life become doubled to 6 life and then lose 6 life. The player may also choose to apply Tainted Remedy first, turning “gain 3 life” into “lose 3 life.” Alhammarret’s Archive would then not apply.').
card_ruling('tainted remedy', '2015-06-22', 'Having more than one Tainted Remedy on the battlefield doesn’t have any noticeable effect on life gain. Once the effect of one Tainted Remedy applies, there is no life gain for the others to apply to.').

card_ruling('tainted sigil', '2009-05-01', 'Tainted Sigil\'s ability counts the life lost by all players, including you.').
card_ruling('tainted sigil', '2009-05-01', 'When Tainted Sigil\'s ability resolves, it checks how much life each player lost over the course of the turn, then it totals that number and you gain that much life in one shot. It doesn\'t matter how a player lost life or what caused it. It also doesn\'t matter if Tainted Sigil wasn\'t on the battlefield when some or all of the life was lost.').
card_ruling('tainted sigil', '2009-05-01', 'If you lose enough life to lower your life total to 0 or less, you\'ll lose the game as a state-based actions before you activate Tainted Sigil\'s ability.').
card_ruling('tainted sigil', '2009-05-01', 'Tainted Sigil\'s ability checks only whether life was lost. It doesn\'t care whether life was also gained. For example, say one player lost 4 life during the turn and no one else lost any, but that same player also gained 6 life that turn. That player has a higher life total than he or she started the turn with -- but Tainted Sigil\'s ability counts only the life loss, so you\'ll gain 4 life.').

card_ruling('tainted specter', '2009-02-01', 'Tainted Specter used to have errata causing the card to be put into the graveyard without being discarded. The current wording returns it to being a discard, so it will trigger abilities appropriately.').

card_ruling('tainted strike', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('tainted strike', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('tainted strike', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('tainted strike', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('tainted strike', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('tainted strike', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('tainted æther', '2004-10-04', 'If the creature changes controllers before this ability resolves, its new controller has to sacrifice something.').
card_ruling('tainted æther', '2004-10-04', 'If more than one of these is on the battlefield, the controller sacrifices a creature for each one of these.').
card_ruling('tainted æther', '2004-10-04', 'If the controller has no other lands or creatures on the battlefield, they must sacrifice the creature that just entered the battlefield.').
card_ruling('tainted æther', '2004-10-04', 'Tainted AEther\'s controller is the one that controls its triggered ability, but the controller of the creature that entered the battlefield chooses what creature they sacrifice during resolution.').

card_ruling('tajic, blade of the legion', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('tajic, blade of the legion', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('tajic, blade of the legion', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('tajuru archer', '2009-10-01', 'The ability counts the number of Allies you control as it resolves.').
card_ruling('tajuru archer', '2009-10-01', 'You target a creature when the ability triggers. You decide whether to have Tajuru Archer deal damage to that creature as the ability resolves.').

card_ruling('tajuru beastmaster', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('tajuru beastmaster', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('tajuru preserver', '2010-06-15', 'As a spell or ability an opponent controls resolves, if it would force you to sacrifice a permanent (as the annihilator ability does), you just don\'t. That part of the effect does nothing. If that spell or ability gives you the option to sacrifice a permanent (as Prowling Pangolin\'s ability does), you can\'t take that option.').
card_ruling('tajuru preserver', '2010-06-15', 'If a spell or ability an opponent controls instructs you to perform an action unless you sacrifice a permanent (as Ogre Marauder does), you can\'t choose to sacrifice a permanent. You must perform the action. On the other hand, if a spell or ability an opponent controls instructs you to sacrifice a permanent unless you perform an action (as Rishadan Brigand does), you can choose whether or not to perform the action. If you don\'t perform the action, nothing happens, since you can\'t sacrifice any permanents.').
card_ruling('tajuru preserver', '2010-06-15', 'You may still sacrifice permanents to pay the costs of spells you cast and abilities you activate, or because a resolving spell or ability you control instructs or allows you to do so. Notably, you may still sacrifice Eldrazi Spawn creature tokens for mana.').
card_ruling('tajuru preserver', '2010-06-15', 'You may sacrifice a permanent to pay the activation cost of an ability, even if that ability comes from a permanent an opponent controls (such as Excavation).').
card_ruling('tajuru preserver', '2010-06-15', 'You may sacrifice a permanent as a special action, even if the effect that allows you to do so comes from an opponent\'s permanent (such as Damping Engine or Volrath\'s Curse). No one controls special actions.').
card_ruling('tajuru preserver', '2010-06-15', 'This ability affects only sacrifices. It won\'t stop a creature from being put into the graveyard due to lethal damage or having 0 toughness, and it won\'t stop a permanent from being put into the graveyard due to the \"legend rule\" or the \"planeswalker uniqueness rule.\" None of these are sacrifices; they\'re the result of game rules.').

card_ruling('tajuru stalwart', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('tajuru stalwart', '2015-08-25', '').
card_ruling('tajuru stalwart', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('tajuru stalwart', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('tajuru stalwart', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('tajuru warcaller', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('tajuru warcaller', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('take', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('take', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('take', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('take', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('take', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('take', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('take', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('take', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('take', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('take', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('take', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('take', '2013-04-15', 'You can target any creature you control with Take; the target does not need to have any +1/+1 counters on it.').
card_ruling('take', '2013-04-15', 'If you cast Give // Take as a fused split spell, you can target the same creature for both parts. If you do so, you will first put three +1/+1 counters on that creature, then remove all +1/+1 counters from it, then draw that many cards.').

card_ruling('take possession', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('take possession', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('take possession', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('take possession', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('take possession', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('take possession', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('takklemaggot', '2004-10-04', 'If the creature leaves the battlefield without going to the graveyard, Takklemaggot is simply put into its owner\'s graveyard.').
card_ruling('takklemaggot', '2008-05-01', 'Takklemaggot\'s controller no longer changes as it returns to the battlefield. The player who controlled it when it left the battlefield is the same player that controls it when it returns to the battlefield, even though a different player may be choosing what it will be attached to after returning onto the battlefield.').
card_ruling('takklemaggot', '2009-10-01', 'When the enchanted creature is put into a graveyard, Takklemaggot\'s second ability triggers, then Takklemaggot is put into its owner\'s graveyard as a state-based action. When the ability resolves, if Takklemaggot is still in that graveyard, it\'s returned to the battlefield.').
card_ruling('takklemaggot', '2009-10-01', 'The player who controlled the creature that went to the graveyard (and caused Takklemaggot\'s second ability to trigger) chooses what creature Takklemaggot will return to the battlefield attached to. The creature isn\'t chosen until the ability resolves, so no player can respond between the time the creature is chosen and the time Takklemaggot returns to the battlefield attached to it. The ability doesn\'t target a creature, so a creature with shroud, for example, can be chosen. However, only a creature that can be legally enchanted by Takklemaggot can be chosen, so a creature with protection from black, for example, can\'t be chosen. If the player can choose a creature, he or she must do so, no matter who controls it. If the player can\'t choose a creature, Takklemaggot returns to the battlefield as a non-Aura enchantment, and that player is the player that Takklemaggot will deal damage to.').

card_ruling('talara\'s bane', '2008-08-01', 'If the creature card has \"*\" in its power or toughness, the ability that defines \"*\" works in all zones. Note that the card is in the opponent\'s hand when Talara\'s Bane checks its toughness.').

card_ruling('talas researcher', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('talent of the telepath', '2015-06-22', 'If the spell mastery ability applies, you’ll cast the two cards in order. The one you cast last will be the one that resolves first.').
card_ruling('talent of the telepath', '2015-06-22', 'You cast the instant and/or sorcery card(s) from your opponent’s library as Talent of the Telepath is resolving. Ignore timing restrictions based on the card’s type. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('talent of the telepath', '2015-06-22', 'The cards will be put into their owner’s graveyard after they resolve, not yours.').
card_ruling('talent of the telepath', '2015-06-22', 'If you can’t cast any instant or sorcery cards (perhaps because there are no legal targets available) or if you choose not to cast one, then Talent of the Telepath finishes resolving. Any of the revealed cards you didn’t cast will be put into that player’s graveyard.').
card_ruling('talent of the telepath', '2015-06-22', 'If you cast a card “without paying its mana cost,” you can’t pay any alternative costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('talent of the telepath', '2015-06-22', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('talent of the telepath', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('talon gates', '2012-06-01', 'Exiling a nonland card from your hand this way is a special action. It doesn\'t use the stack and doesn\'t count as casting a spell or activating an ability. Players can\'t respond to this action, although they can respond when you cast the spell after the last time counter is removed.').
card_ruling('talon gates', '2012-06-01', 'The first ability may be used any number of times.').
card_ruling('talon gates', '2012-06-01', 'Even after the game leaves Talon Gates, each of the suspended cards will continue to have suspend; the owners of those cards will continue to remove a time counter from each during their upkeeps.').

card_ruling('talon sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('talon sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('talrand, sky summoner', '2012-07-01', 'You\'ll put the Drake token onto the battlefield before the spell that caused the ability to trigger resolves. However, that Drake token isn\'t on the battlefield when you choose targets for that spell.').
card_ruling('talrand, sky summoner', '2012-07-01', 'The ability will still resolve if the instant or sorcery spell gets countered.').

card_ruling('talus paladin', '2010-03-01', 'The choices you make as Talus Paladin\'s ability resolves are made individually. First you decide whether to have Allies you control gain lifelink until end of turn. Then, regardless of that decision, you decide whether to put a +1/+1 counter on Talus Paladin.').

card_ruling('tamanoa', '2006-07-15', 'A noncreature source is a source that doesn\'t have the type creature. If a creature card that\'s not on the battlefield deals damage (for example, a cycled Gempalm Incinerator), Tamanoa\'s ability won\'t trigger.').
card_ruling('tamanoa', '2006-07-15', 'Tamanoa\'s ability triggers no matter who the recipient of the damage is: another player, a creature, or even you. If a noncreature source you control deals damage to you that drops your life total to 0 or less, you\'ll lose the game before Tamanoa\'s ability can resolve.').

card_ruling('tamiyo, the moon sage', '2012-05-01', 'Tamiyo\'s first ability tracks the permanent, but not its controller. If the permanent changes controllers before its first controller\'s next untap step has come around, then it won\'t untap during its new controller\'s next untap step.').
card_ruling('tamiyo, the moon sage', '2012-05-01', 'The number of tapped creatures the player controls is determined when Tamiyo\'s second ability resolves.').
card_ruling('tamiyo, the moon sage', '2012-05-01', 'Tamiyo\'s third ability creates one emblem with two abilities.').
card_ruling('tamiyo, the moon sage', '2012-05-01', 'If you activate Tamiyo\'s third ability when she has eight loyalty counters on her, she\'ll be put into your graveyard before the emblem is created. She won\'t return to your hand.').
card_ruling('tamiyo, the moon sage', '2012-05-01', 'Cards that are put into your graveyard can be returned to your hand by the emblem\'s ability even if they were never in your hand.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'The ability can target a tapped permanent. If the targeted permanent is already tapped when it resolves, that permanent just remains tapped and doesn\'t untap during its controller\'s next untap step.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('tamiyo, the moon sage', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('tandem tactics', '2015-08-25', 'You can cast Tandem Tactics with no targets. When it resolves, you’ll gain 2 life. However, if you cast Tandem Tactics with any targets and all of those targets are illegal as it tries to resolve, it will be countered and none of its effects will happen. You won’t gain 2 life in that case.').

card_ruling('tangle', '2004-10-04', 'It identifies the creatures that are not going to untap at the time this spell resolves.').

card_ruling('tangle angler', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('tangle angler', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('tangle angler', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('tangle angler', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('tangle angler', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('tangle angler', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('tangle angler', '2011-01-01', 'You may activate Tangle Angler\'s second ability multiple times in a turn to force multiple creatures to block it.').
card_ruling('tangle angler', '2011-01-01', 'Activating Tangle Angler\'s second ability doesn\'t force you to attack with Tangle Angler that turn. If it doesn\'t attack, the targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('tangle angler', '2011-01-01', 'If you attack with Tangle Angler but a creature you targeted with its activated ability isn\'t able to block it (for example, because an effect has granted Tangle Angler intimidate and the other creature is white), the other creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('tangle angler', '2011-01-01', 'Tapped creatures, creatures that can\'t block as the result of an effect, creatures with unpaid costs to block (such as those from War Cadence), and creatures that aren\'t controlled by the defending player are exempt from effects that would require them to block. Such creatures can be targeted by Tangle Angler\'s activated ability, but the requirement to block does nothing.').

card_ruling('tangleroot', '2011-01-25', 'The player gets the mana whether they want it or not. If it isn\'t spent, it will disappear at the end of the current step (or phase).').

card_ruling('tanglesap', '2009-10-01', 'Tanglesap prevents combat damage that would be dealt by all creatures without trample, regardless of whether they\'re attacking or blocking.').
card_ruling('tanglesap', '2009-10-01', 'Creatures are checked to see whether they have trample at the time they\'d deal combat damage, not at the time Tanglesap resolves.').

card_ruling('tar fiend', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('tar fiend', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('tar fiend', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('tar fiend', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').

card_ruling('tar pit warrior', '2005-08-01', 'You can move an Aura onto this card using an untargeted spell or ability without causing this card to be sacrificed.').

card_ruling('tariel, reckoner of souls', '2011-09-22', 'The only target of this ability is the opponent.').
card_ruling('tariel, reckoner of souls', '2011-09-22', 'The creature card is chosen at random from among creature cards in the target opponent\'s graveyard as the ability resolves. Players can respond to the ability knowing the targeted player, but they won\'t know which creature card is about to enter the battlefield.').

card_ruling('tariff', '2004-10-04', 'The choice of creatures is made on resolution and not on announcement.').
card_ruling('tariff', '2004-10-04', 'You can use mana abilities during the resolution of this spell if the player decides to pay the mana cost.').
card_ruling('tariff', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice the creature. You can choose to not pay if you control no creatures on resolution.').
card_ruling('tariff', '2008-04-01', 'First the active player identifies the creature he or she controls with the highest mana cost (and chooses one if there\'s a tie) and chooses whether to sacrifice it or pay mana. Then each other player in turn order does the same. All creatures sacrificed this way are sacrificed at the same time.').
card_ruling('tariff', '2008-04-01', 'Although Tariff checks the converted mana cost of the creatures on the battlefield, you need to pay the creature\'s actual mana cost (including color) to keep it on the battlefield.').

card_ruling('tarmogoyf', '2007-05-01', 'Tarmogoyf\'s ability works in all zones, not just while Tarmogoyf is on the battlefield.').
card_ruling('tarmogoyf', '2007-05-01', 'Tarmogoyf counts card types, not cards. If the only card in all graveyards is a single artifact creature, Tarmogoyf will be 2/3. If the only cards in all graveyards are ten artifact creatures, Tarmogoyf will still be 2/3.').
card_ruling('tarmogoyf', '2013-07-01', 'The card types that can appear on cards in a graveyard are artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal.').

card_ruling('tarox bladewing', '2007-05-01', 'The +X/+X bonus is based on Tarox Bladewing\'s power at the time the ability resolves. It won\'t change if Tarox\'s power changes later in the turn.').

card_ruling('tasigur\'s cruelty', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('tasigur\'s cruelty', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('tasigur\'s cruelty', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('tasigur\'s cruelty', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('tasigur\'s cruelty', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('tasigur, the golden fang', '2014-11-24', 'The last ability doesn’t target any card or player. You choose an opponent as the ability resolves and that player chooses a nonland card.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'The chosen opponent can choose any nonland card in your graveyard, not just one of the cards that was just put there. The player must choose a nonland card if there’s one in your graveyard.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('tasigur, the golden fang', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('task force', '2004-10-04', 'The +0/+3 bonus is added as a triggered ability upon the casting/activating of a spell or ability which targets this card.').

card_ruling('taste of blood', '2011-09-22', 'If the player is an illegal target when Taste of Blood tries to resolve, Taste of Blood will be countered and none of its effects will happen. You won\'t gain life.').

card_ruling('tatterkite', '2008-05-01', 'Tatterkite\'s ability prevents any kind of counter from being placed on it, not just -1/-1 counters.').
card_ruling('tatterkite', '2008-05-01', 'A spell or ability whose effect would put a counter on Tatterkite simply doesn\'t affect it. The spell or ability isn\'t countered. If it would put counters on any other creatures, it continues to do so.').
card_ruling('tatterkite', '2008-05-01', 'If Tatterkite is dealt damage by a creature with wither, that damage has no effect. It\'s not prevented; it just doesn\'t do anything. Abilities that trigger on damage being dealt will still trigger.').
card_ruling('tatterkite', '2008-05-01', 'You can\'t pay a cost that includes putting a counter on Tatterkite. For example, if Tatterkite is the only creature you control, you can\'t cast Scarscale Ritual because you can\'t pay the additional cost.').
card_ruling('tatterkite', '2008-05-01', 'If Tatterkite would enter the battlefield with counters on it, it enters the battlefield but doesn\'t have those counters. For example, if an effect grants it persist and then it\'s destroyed, it returns to the battlefield without a -1/-1 counter on it.').

card_ruling('tattermunge duo', '2008-05-01', 'An object that\'s both of the listed colors will cause both abilities to trigger. You can put them on the stack in either order.').

card_ruling('tavern swindler', '2012-10-01', 'You can\'t activate the ability if your life total is less than 3.').
card_ruling('tavern swindler', '2012-10-01', 'You can activate the ability if your life total is 3, but it\'s probably a bad idea. You\'ll lose the game before the ability resolves.').

card_ruling('tawnos\'s coffin', '2004-10-04', 'The creature returns to the battlefield tapped. It does not return to the battlefield and then tap afterwards.').
card_ruling('tawnos\'s coffin', '2007-09-16', 'If the targeted creature is a token, it will cease to exist after being exiled. Any Auras that were attached to it will remain exiled forever.').
card_ruling('tawnos\'s coffin', '2007-09-16', 'If the exiled card is returned to the battlefield and, for some reason, it now can\'t be enchanted by an Aura that was also exiled by Tawnos\'s Coffin, that Aura will remain exiled.').
card_ruling('tawnos\'s coffin', '2007-09-16', 'If Tawnos\'s Coffin leaves the battlefield before its ability has resolved, it will exile the targeted creature forever, since its delayed triggered ability will never trigger.').
card_ruling('tawnos\'s coffin', '2008-04-01', 'Because the new wording doesn\'t use phasing, the exiled card will suffer from summoning sickness upon its return to the battlefield.').
card_ruling('tawnos\'s coffin', '2008-04-01', 'The effect doesn\'t care what types the card has after it is exiled, only that it have been a creature while on the battlefield.').

card_ruling('tazeem', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('tazeem', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('tazeem', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('tazeem', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('tear', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('tear', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('tear', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('tear', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('tear', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('tear', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('tear', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('tear', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('tear', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('tear', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('tear', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('tectonic edge', '2010-03-01', 'Tectonic Edge\'s second ability checks how many lands an opponent controls only at the time you activate the ability.').
card_ruling('tectonic edge', '2010-03-01', 'Assuming you can activate Tectonic Edge\'s second ability, you can target any nonbasic land with it (not just one controlled by an opponent that controls four or more lands).').

card_ruling('tectonic rift', '2011-09-22', 'If the land is an illegal target when Tectonic Rift tries to resolve, Tectonic Rift will be countered and none of its effects will happen. Creatures without flying will be able to block as normal.').
card_ruling('tectonic rift', '2011-09-22', 'The set of affected creatures isn\'t locked in when Tectonic Rift resolves. Creatures without flying that enter the battlefield later that turn or creatures that lose flying later that turn won\'t be able to block.').

card_ruling('teferi\'s imp', '2004-10-04', 'There is no negative effect if you can\'t discard when it phases out. You still get to draw a card when it phases in.').

card_ruling('teferi\'s puzzle box', '2004-10-04', 'You do your normal draw before this ability is put on the stack.').
card_ruling('teferi\'s puzzle box', '2004-10-04', 'If you have more than one of these, each effect triggers separately.').

card_ruling('teferi\'s realm', '2004-10-04', 'Does not affect token permanents, only cards.').
card_ruling('teferi\'s realm', '2005-08-01', 'Can affect itself if you choose \"non-Aura enchantment\".').
card_ruling('teferi\'s realm', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('teferi\'s response', '2006-01-01', 'Remember that a spell only targets something if it is an Aura or uses the phrase \"target\" in its text.').
card_ruling('teferi\'s response', '2006-01-01', 'If none of the targeted spell\'s targets is a land when Teferi\'s Response would resolve, Teferi\'s Response is countered on resolution due to having no legal targets.').

card_ruling('teferi\'s veil', '2008-04-01', 'The “at end of combat” ability triggers after combat damage resolves. Creatures dealt lethal damage in combat won\'t be saved this way.').

card_ruling('teferi, mage of zhalfir', '2006-09-25', 'The last ability means that in order for an opponent to cast a spell, it must be that opponent\'s turn, during a main phase, and the stack must be empty. This is true even if the player doesn\'t have a sorcery he or she is able to cast, or if a rule or effect allows a spell to be cast at another time.').
card_ruling('teferi, mage of zhalfir', '2006-09-25', 'If you have Teferi on the battlefield, you may exile creature cards in your hand with suspend any time you could cast an instant.').
card_ruling('teferi, mage of zhalfir', '2006-09-25', 'If an ability lets an opponent cast a spell as part of its effect (such as Isochron Scepter\'s ability does), that opponent can\'t cast that spell since the currently resolving ability is still on the stack.').
card_ruling('teferi, mage of zhalfir', '2006-09-25', 'Your opponents can\'t cast their suspended cards, no matter what phase it is when the last time counter is removed. Your opponents can\'t cast cards using the madness ability either, no matter what phase it is when the card is discarded.').
card_ruling('teferi, mage of zhalfir', '2007-05-01', 'If you have Teferi on the battlefield and there\'s a Split Second spell on the stack (such as Sudden Death), you can\'t Suspend creatures until after the Split Second spell resolves.').

card_ruling('teferi, temporal archmage', '2014-11-07', 'If you control Teferi’s emblem, the rule that says you can activate a loyalty ability only if none of that permanent’s loyalty abilities have been activated that turn still applies. In other words, you could activate a planeswalker’s loyalty ability once on your turn and once on each of your opponents’ turns.').
card_ruling('teferi, temporal archmage', '2014-11-07', 'If you control The Chain Veil (from the Magic 2015 core set) and its activated ability has resolved, and you also control Teferi’s emblem, for each planeswalker you control, you can activate one of its loyalty abilities during that turn any time you could cast an instant, and then you may activate a loyalty ability again that turn (choosing the same loyalty ability or a different one) any time you could cast an instant.').

card_ruling('tel-jilad defiance', '2011-01-01', '\"Protection from artifacts\" means the following: -- The affected creature can\'t be blocked by artifact creatures. -- The affected creature can\'t be equipped. If any Equipment are attached to it, they become unattached. The affected creature also can\'t be enchanted by Auras that have somehow become artifacts in addition to enchantments. -- The affected creature can\'t be targeted by abilities from artifact sources. -- All damage that would be dealt to the affected creature by artifact sources is prevented.').

card_ruling('tel-jilad fallen', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('tel-jilad fallen', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('tel-jilad fallen', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('tel-jilad fallen', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('tel-jilad fallen', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('tel-jilad fallen', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('tel-jilad fallen', '2011-01-01', '\"Protection from artifacts\" means the following: -- Tel-Jilad Fallen can\'t be blocked by artifact creatures. -- Tel-Jilad Fallen can\'t be equipped. It also can\'t be enchanted by Auras that have somehow become artifacts in addition to enchantments. -- Tel-Jilad Fallen can\'t be targeted by abilities from artifact sources. -- All damage that would be dealt to Tel-Jilad Fallen by artifact sources is prevented.').

card_ruling('tel-jilad stylus', '2004-12-01', 'Tel-Jilad Stylus\'s ability may target a permanent you own but do not control.').
card_ruling('tel-jilad stylus', '2004-12-01', 'Tel-Jilad Stylus\'s ability can put tokens on the bottom of your library (where they then cease to exist).').
card_ruling('tel-jilad stylus', '2004-12-01', 'You own all cards that started the game in your deck, and all cards that you \"Wished\" into the game (with one of the Judgment Wish cards or Ring of Maruf).').
card_ruling('tel-jilad stylus', '2009-10-01', 'You own a token if it entered the battlefield under your control.').

card_ruling('telekinesis', '2007-09-16', 'Telekinesis\' ability doesn\'t lock in who the creature\'s controller is when it resolves. If the creature changes controllers, it won\'t untap during its controller\'s untap step until it has not done so twice, regardless of who its controller was during any of those untap steps.').

card_ruling('telekinetic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('telekinetic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('telemin performance', '2009-02-01', 'If there are no creature cards in the targeted opponent\'s library, that player will put his or her entire library into his or her graveyard.').

card_ruling('teleportal', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('teleportal', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('teleportal', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('teleportal', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('teleportal', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('teleportal', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('teleportal', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('teller of tales', '2004-12-01', 'You choose whether or not to tap or untap the creature when the ability resolves.').

card_ruling('telling time', '2005-10-01', 'If there are fewer than three cards in your library, follow the instructions in the order given for any cards that are there.').

card_ruling('temper', '2004-10-04', 'You can make X be larger than the amount of damage, but you only get one counter for each actual point of damage prevented.').
card_ruling('temper', '2004-10-04', 'The +1/+1 counters do show up at the same time as any unprevented damage, which means they are there before you check for lethal damage.').

card_ruling('tempest owl', '2009-10-01', 'If Tempest Owl was kicked, you may target zero, one, two, or three permanents. They don\'t have to be untapped.').

card_ruling('temple elder', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('temple garden', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('temple garden', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('temple garden', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('temple of abandon', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of abandon', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of abandon', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of abandon', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of deceit', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of deceit', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of deceit', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of deceit', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of enlightenment', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of enlightenment', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of enlightenment', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of enlightenment', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of malice', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of malice', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of malice', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of malice', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of mystery', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of mystery', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of mystery', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of mystery', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of plenty', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of plenty', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of plenty', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of plenty', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of silence', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of silence', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of silence', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of silence', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temple of triumph', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('temple of triumph', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('temple of triumph', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('temple of triumph', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('temporal aperture', '2004-10-04', 'While you do not have to pay the mana cost, you do have to pay any other costs described in the text that are paid when playing the card. If the cost in the card text is optional, such as with Buyback, you can optionally pay that cost.').
card_ruling('temporal aperture', '2004-10-04', 'If your library is shuffled or that card otherwise leaves the top of your library, the effect ends.').
card_ruling('temporal aperture', '2004-10-04', 'The card is removed from the top of the library when you play it, just like a card is removed from your hand when you play it.').

card_ruling('temporal cascade', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('temporal extortion', '2007-02-01', 'When this spell is cast, its \"when you cast\" ability triggers and goes on the stack on top of it.').
card_ruling('temporal extortion', '2007-02-01', 'As the triggered ability resolves, the active player gets the option to perform the action. If that player declines, the next player in turn order gets the option. As soon as any player performs the action, the spell is countered, but the remaining players still get the option. If all players decline, the spell remains on the stack.').

card_ruling('temporal fissure', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('temporal fissure', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('temporal fissure', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('temporal fissure', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('temporal fissure', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('temporal manipulation', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('temporal trespass', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('temporal trespass', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('temporal trespass', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('temporal trespass', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('temporal trespass', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('tempt with discovery', '2013-10-17', 'Your opponents decide in turn order whether or not they accept the offer, starting with the opponent on your left. Each opponent will know the decisions of previous opponents in turn order when making his or her decision.').
card_ruling('tempt with discovery', '2013-10-17', 'After each opponent has decided, the effect happens simultaneously for each one who accepted the offer. Then, the effect happens again for you a number of times equal to the number of opponents who accepted.').

card_ruling('tempt with glory', '2013-10-17', 'Your opponents decide in turn order whether or not they accept the offer, starting with the opponent on your left. Each opponent will know the decisions of previous opponents in turn order when making his or her decision.').
card_ruling('tempt with glory', '2013-10-17', 'After each opponent has decided, the effect happens simultaneously for each one who accepted the offer. Then, the effect happens again for you a number of times equal to the number of opponents who accepted.').

card_ruling('tempt with immortality', '2013-10-17', 'Your opponents decide in turn order whether or not they accept the offer, starting with the opponent on your left. Each opponent will know the decisions of previous opponents in turn order when making his or her decision.').
card_ruling('tempt with immortality', '2013-10-17', 'After each opponent has decided, the effect happens simultaneously for each one who accepted the offer. Then, the effect happens again for you a number of times equal to the number of opponents who accepted.').
card_ruling('tempt with immortality', '2013-10-17', 'None of the creature cards in graveyards are targeted. The cards are chosen as Tempt with Immortality resolves.').

card_ruling('tempt with reflections', '2013-10-17', 'Your opponents decide in turn order whether or not they accept the offer, starting with the opponent on your left. Each opponent will know the decisions of previous opponents in turn order when making his or her decision.').
card_ruling('tempt with reflections', '2013-10-17', 'After each opponent has decided, the effect happens simultaneously for each one who accepted the offer. Then, the effect happens again for you a number of times equal to the number of opponents who accepted.').
card_ruling('tempt with reflections', '2013-10-17', 'If the creature you control is an illegal target when Tempt with Reflections tries to resolve, it will be countered and none of its effects will happen. No copies will be created.').
card_ruling('tempt with reflections', '2013-10-17', 'If the target creature has an enters-the-battlefield triggered ability, the abilities of all the copies will trigger as Tempt with Reflections resolves. All of those abilities will be put on the stack after Tempt with Reflections finishes resolving. Abilities controlled by the active player will be put on the stack first (in an order of his or her choice), followed by abilities controlled by each other player in turn order. The last ability to be put on the stack will resolve first, and so on.').
card_ruling('tempt with reflections', '2013-10-17', 'The tokens copy exactly what was printed on the original creature and nothing else (unless that creature is copying something else or is a token; see below). They don’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('tempt with reflections', '2013-10-17', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('tempt with reflections', '2013-10-17', 'If the copied creature is copying something else (for example, if the copied creature is a Clone), then the tokens enter the battlefield as whatever that creature copied.').
card_ruling('tempt with reflections', '2013-10-17', 'If the copied creature is a token, the tokens created by Tempt with Reflections copy the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('tempt with reflections', '2013-10-17', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the copied creature will also work.').

card_ruling('tempt with vengeance', '2013-10-17', 'Your opponents decide in turn order whether or not they accept the offer, starting with the opponent on your left. Each opponent will know the decisions of previous opponents in turn order when making his or her decision.').
card_ruling('tempt with vengeance', '2013-10-17', 'After each opponent has decided, the effect happens simultaneously for each one who accepted the offer. Then, the effect happens again for you a number of times equal to the number of opponents who accepted.').

card_ruling('tempting licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('tempting licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('temur battle rage', '2014-11-24', 'If all creatures blocking a creature with double strike and trample are destroyed during the first combat damage step, the creature with double strike and trample will deal all its combat damage to the defending player or planeswalker during the second combat damage step.').
card_ruling('temur battle rage', '2014-11-24', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('temur charger', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('temur charger', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('temur charger', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('temur charger', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('temur charger', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('temur charger', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('temur charger', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('temur charger', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('temur charger', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('temur charm', '2014-09-20', 'You can’t cast Temur Charm choosing the first mode unless you target both a creature you control and a creature you don’t control.').
card_ruling('temur charm', '2014-09-20', 'If the first mode was chosen and either target is an illegal target when Temur Charm tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('temur charm', '2014-09-20', 'If the first mode was chosen and just the creature you control is an illegal target when Temur Charm tries to resolve, it won’t get +1/+1 until end of turn. If the creature you control is a legal target but the creature you don’t control isn’t, the creature you control will get +1/+1 until end of turn.').
card_ruling('temur charm', '2014-09-20', 'If the third mode was chosen, creatures with power 3 or less that enter the battlefield after Temur Charm resolves also can’t block. A creature whose power was 4 or greater when Temur Charm resolved but is 3 or less as the declare blockers step begins also can’t block.').

card_ruling('temur sabertooth', '2014-11-24', 'You choose whether to return a creature and which creature to return as the activated ability resolves. This doesn’t target any creature.').

card_ruling('temur war shaman', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('temur war shaman', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('temur war shaman', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('temur war shaman', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('temur war shaman', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('temur war shaman', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('temur war shaman', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('temur war shaman', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('temur war shaman', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('temur war shaman', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('temur war shaman', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('temur war shaman', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('temur war shaman', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('tenacious dead', '2013-07-01', 'You decide whether to pay {1}{B} when the ability resolves. If you choose not to, you won’t get another chance later.').
card_ruling('tenacious dead', '2013-07-01', 'If you control a Tenacious Dead owned by another player when it dies, you’ll control the triggered ability. However, if you choose to pay {1}{B}, it will be put onto the battlefield under its owner’s control.').

card_ruling('tendrils of agony', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('tendrils of agony', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('tendrils of agony', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('tendrils of agony', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('tendrils of agony', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('tendrils of corruption', '2009-10-01', 'The amount of life you gain is equal to the number of Swamps you control, not the amount of damage Tendrils of Corruption deals (in case some of it is prevented).').
card_ruling('tendrils of corruption', '2009-10-01', 'If the targeted creature is an illegal target by the time Tendrils of Corruption would resolve, the entire spell is countered. You won\'t gain any life.').

card_ruling('tendrils of despair', '2004-10-04', 'If the player has fewer than 2 cards, they discard whatever they have.').
card_ruling('tendrils of despair', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('tendrils of despair', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('teneb, the harvester', '2007-02-01', 'You choose the target when the ability is put on the stack, and you choose whether to pay when it resolves.').

card_ruling('tephraderm', '2004-10-04', 'The second ability triggers even if this card is dealt lethal damage.').

card_ruling('terastodon', '2010-03-01', 'The targeted permanents may be controlled by different players.').
card_ruling('terastodon', '2013-07-01', 'If a targeted permanent has indestructible or regenerates, its controller won\'t get an Elephant token for it. Similarly, if the targeted permanent is destroyed but a replacement effect moves it to a different zone instead of its owner\'s graveyard, its controller won\'t get an Elephant token for it.').

card_ruling('teremko griffin', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('teremko griffin', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('teremko griffin', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('teremko griffin', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('terminal moraine', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('terminus', '2012-05-01', 'Each player chooses the order for his or her own creatures. This order isn\'t revealed to other players.').

card_ruling('terra eternal', '2013-07-01', 'Effects that say \"destroy\" won\'t cause a land with indestructible to be put into its owner\'s graveyard. If that land is also a creature, lethal damage won\'t cause it to be put into its owner\'s graveyard either. However, a land with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary land with the same name is controlled by the same player, or if it\'s also a creature and its toughness is 0 or less.').

card_ruling('terra ravager', '2013-10-17', 'Count the number of lands the defending player controls when the ability resolves to determine the value of X.').

card_ruling('terra stomper', '2009-10-01', 'Terra Stomper can be targeted by spells that try to counter it (such as Cancel). Those spells will resolve, but the part of their effect that would counter Terra Stomper won\'t do anything. Any other effects those spells have will work as normal.').

card_ruling('terraformer', '2005-10-01', 'Terraformer\'s effect overwrites the land type of all lands you control that had other types and gives a land type to all lands you control that don\'t normally have one.').
card_ruling('terraformer', '2005-10-01', 'Changing a land\'s type doesn\'t change its name or whether it\'s legendary or basic.').
card_ruling('terraformer', '2005-10-01', 'Lands you control will have the mana ability of the basic land type you choose (for example, Forests can tap to produce green mana) and will lose all other innate abilities they had.').

card_ruling('terrain generator', '2004-10-04', 'Putting a land onto the battlefield this way does not count as your one land play for the turn.').
card_ruling('terrain generator', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('terrarion', '2005-10-01', 'You can get either two mana of the same color or one mana each of different colors.').
card_ruling('terrarion', '2005-10-01', 'You get the mana before you draw the card.').
card_ruling('terrarion', '2005-10-01', 'You draw a card no matter how Terrarion winds up in a graveyard from the battlefield.').

card_ruling('terrifying presence', '2012-05-01', 'Although Terrifying Presence doesn\'t affect the target creature, if that creature is an illegal target when Terrifying Presence tries to resolve, it will be countered and none of its effects will happen. No damage will be prevented.').

card_ruling('territorial baloth', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('territorial baloth', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('territorial baloth', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('territorial baloth', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('territorial baloth', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('territorial dispute', '2004-10-04', 'Does not stop lands from being put onto the battlefield by a spell or ability. It only stops players from taking the action of playing a land.').

card_ruling('terrus wurm', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('test of endurance', '2004-10-04', 'To win the game, you must have 50 or more life both at the beginning of upkeep and when the ability resolves.').

card_ruling('test of faith', '2004-12-01', 'The +1/+1 counters are put onto the creature at the same time the damage is prevented. If a 1/1 creature would be dealt 6 damage, 3 damage is prevented and three +1/+1 counters are put on the creature. The creature ends up as a 4/4 creature with 3 damage marked on it.').

card_ruling('testament of faith', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('tethered griffin', '2004-10-04', 'The ability will trigger if you don\'t control an enchantment, even for a brief moment during the resolution of another spell or ability.').
card_ruling('tethered griffin', '2004-10-04', 'Only checks if you control no enchantments at the time it triggers. It does not check again on resolution. Gaining control of an enchantment before then will not save the Griffin.').

card_ruling('tethmos high priest', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('tethmos high priest', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('tethmos high priest', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('tetravus', '2004-10-04', 'If the Tetravus is destroyed when the Tetravites are off the card, they are not destroyed, they are just orphaned.').
card_ruling('tetravus', '2004-10-04', 'Any +1/+1 counter which is on this card can be turned into a Tetravite token. It does not care where the +1/+1 counter came from.').
card_ruling('tetravus', '2004-10-04', 'Only Tetravites from this specific Tetravus may be used for the ability. Ones from a different Tetravus can\'t.').
card_ruling('tetravus', '2008-08-01', 'This card now has two upkeep-triggered abilities. Its controller chooses the order they are put on the stack, and thus the order in which they resolve each upkeep.').

card_ruling('tetsuo umezawa', '2004-10-04', 'This card can be enchanted by moving Auras onto it. The \"can\'t be the target\" text is different from \"can\'t be enchanted\" in that it only applies to Aura spells when they are being cast.').
card_ruling('tetsuo umezawa', '2009-10-01', 'Tetsuo Umezawa can be targeted by abilities from Auras.').

card_ruling('teysa, envoy of ghosts', '2013-04-15', 'Protection from creatures means that Teysa, Envoy of Ghosts can\'t be the target of abilities of creatures or abilities of creature cards in other zones, such as bloodrush abilities. Teysa also can\'t be blocked by creatures, and all damage that would be dealt to Teysa by creatures is prevented.').
card_ruling('teysa, envoy of ghosts', '2013-04-15', 'Teysa\'s last ability doesn\'t target the creature. It will destroy a creature with hexproof or protection from white, for example.').
card_ruling('teysa, envoy of ghosts', '2013-04-15', 'Teysa\'s controller gets the Spirit creature token, not the controller of the creature that dealt combat damage.').
card_ruling('teysa, envoy of ghosts', '2013-07-01', 'You get a Spirit creature token even if Teysa\'s triggered ability doesn\'t destroy the creature (perhaps because it regenerated or has indestructible).').

card_ruling('teysa, orzhov scion', '2006-02-01', 'You may sacrifice Teysa itself to help pay for its first ability, but unlike other white and black creatures, it won\'t cause its second ability to trigger. Any other white and black creatures you sacrifice to pay for the first ability will cause the second ability to trigger.').

card_ruling('tezzeret the seeker', '2008-10-01', 'The first ability can target zero, one, or two artifacts. You may activate it with no targets just to put a loyalty counter on Tezzeret.').
card_ruling('tezzeret the seeker', '2008-10-01', 'For the second ability, you choose the value of X when you activate it. You don\'t look through your library until the ability resolves. (In other words, you can\'t look through your library, decide what artifact card you want, and then determine what X is.) You can\'t choose an X that\'s greater than the number of loyalty counters on Tezzeret.').
card_ruling('tezzeret the seeker', '2008-10-01', 'The third ability affects all artifacts you control, including artifacts that are already creatures.').
card_ruling('tezzeret the seeker', '2008-10-01', 'The third ability causes artifacts you control to become creatures in addition to their other card types.').
card_ruling('tezzeret the seeker', '2009-10-01', 'A noncreature permanent that turns into a creature is subject to the \"summoning sickness\" rule: It can only attack, and its {T} abilities can only be activated, if its controller has continuously controlled that permanent since the beginning of his or her most recent turn.').
card_ruling('tezzeret the seeker', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').
card_ruling('tezzeret the seeker', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('tezzeret the seeker', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('tezzeret the seeker', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('tezzeret the seeker', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('tezzeret the seeker', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('tezzeret the seeker', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('tezzeret the seeker', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('tezzeret the seeker', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('tezzeret the seeker', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('tezzeret the seeker', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('tezzeret the seeker', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('tezzeret\'s gambit', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('tezzeret\'s gambit', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('tezzeret\'s gambit', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('tezzeret\'s gambit', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('tezzeret\'s gambit', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('tezzeret, agent of bolas', '2011-06-01', 'The artifact targeted by the second ability will retain any types, subtypes, or supertypes it has. Notably, if an Equipment becomes an artifact creature, it can\'t be attached to another creature. If it was attached to a creature, it becomes unattached.').
card_ruling('tezzeret, agent of bolas', '2011-06-01', 'If the target of the second ability is already an artifact creature, its power and toughness will each become 5. This overwrites all previous effects that set the creature\'s power and toughness to specific values. Any power- or toughness-setting effects that start to apply after this ability resolves will overwrite this effect.').
card_ruling('tezzeret, agent of bolas', '2011-06-01', 'Effects that modify that creature\'s power or toughness, such as the effects of Giant Growth, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as -1/-1 counters) and effects that switch its power and toughness.').
card_ruling('tezzeret, agent of bolas', '2011-06-01', 'The number of artifacts you control is counted when the last ability resolves, not when it is activated.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('tezzeret, agent of bolas', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('thada adel, acquisitor', '2010-03-01', 'When the triggered ability resolves, you must search that player\'s library (and he or she must shuffle it) even if there are no artifact cards in it or you choose not to find one.').
card_ruling('thada adel, acquisitor', '2010-03-01', 'The exiled card is played using the normal timing rules for an artifact, as well as any other applicable restrictions. For example, you can\'t play the card during your combat phase unless it has flash. Similarly, if the exiled card is an artifact land, you can\'t play it if you\'ve already played a land that turn. If it\'s a nonland card, you\'ll have to pay its mana cost. The only thing that\'s different is you\'re playing it from the exile zone.').
card_ruling('thada adel, acquisitor', '2010-03-01', 'If you don\'t play the card before the turn ends, it simply remains exiled.').

card_ruling('thalakos deceiver', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('thalakos dreamsower', '2004-10-04', 'Can be used on a creature that is already tapped.').

card_ruling('thalakos drifters', '2004-10-04', 'The reminder text is only accurate when this card has Shadow.').

card_ruling('thalia, guardian of thraben', '2011-01-22', 'The ability affects each spell that\'s not a creature spell, including your own.').
card_ruling('thalia, guardian of thraben', '2011-01-22', 'The ability affects the total cost of each noncreature spell, but it doesn\'t change that spell\'s mana cost or converted mana cost.').
card_ruling('thalia, guardian of thraben', '2011-01-22', 'When determining a spell\'s total cost, effects that increase the cost are applied before effects that reduce the cost.').

card_ruling('thassa\'s devourer', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('thassa\'s devourer', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('thassa\'s devourer', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('thassa\'s emissary', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('thassa\'s emissary', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('thassa\'s emissary', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('thassa\'s emissary', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('thassa\'s emissary', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('thassa\'s emissary', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('thassa\'s emissary', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('thassa, god of the sea', '2013-09-15', 'Activating Thassa’s last ability after the target creature has been blocked won’t change or undo the block.').
card_ruling('thassa, god of the sea', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('thassa, god of the sea', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('thassa, god of the sea', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('thassa, god of the sea', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('thassa, god of the sea', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('thassa, god of the sea', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('thassa, god of the sea', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('thassa, god of the sea', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('thassa, god of the sea', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('thassa, god of the sea', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('thassa, god of the sea', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('thassa, god of the sea', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('thassa, god of the sea', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('thassa, god of the sea', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').

card_ruling('that which was taken', '2005-02-01', 'The divinity counters created by That Which Was Taken are the same type of counter as those used by the Myojin creatures in the Champions of Kamigawa set.').
card_ruling('that which was taken', '2013-07-01', 'Permanents with divinity counters on them have indestructible only while That Which Was Taken is on the battlefield.').
card_ruling('that which was taken', '2014-02-01', 'One That Which Was Taken can put divinity counters on another That Which Was Taken.').

card_ruling('thawing glaciers', '2004-10-04', 'The land put onto the battlefield does not count toward your one per turn limit because it was put onto the battlefield by an effect.').
card_ruling('thawing glaciers', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('the abyss', '2004-10-04', 'The controller of The Abyss controls the ability, but the current player chooses what gets destroyed.').
card_ruling('the abyss', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('the chain veil', '2014-07-18', 'For the first ability, it doesn’t matter whether the planeswalker is still on the battlefield as your end step begins. If you activated one of its loyalty abilities that turn, The Chain Veil’s triggered ability won’t trigger.').
card_ruling('the chain veil', '2014-07-18', 'Because the last ability modifies the rules of the game, it affects not only planeswalkers you control when it resolves, but also planeswalkers that come under your control later in the turn.').
card_ruling('the chain veil', '2014-07-18', 'After the last ability resolves, you’ll essentially be able to activate a loyalty ability of each planeswalker you control a total of twice during your turn. The timing rules for when you can activate loyalty abilities apply each time; it must be your main phase and the stack must be empty.').
card_ruling('the chain veil', '2014-07-18', 'The second loyalty ability you activate doesn’t have to be the same as the first ability. For example, you could activate a planeswalker’s first ability twice, or you could activate a planeswalker’s first ability, then activate its second ability.').
card_ruling('the chain veil', '2014-07-18', 'Each additional time The Chain Veil’s last ability resolves will allow you to activate a loyalty ability of each planeswalker you control an additional time. For example, if you activate The Chain Veil’s last ability, untap it, then activate it again, you can activate a loyalty ability of a planeswalker you control three times that turn.').

card_ruling('the dark barony', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the dark barony', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the dark barony', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the dark barony', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('the dark barony', '2009-10-01', 'The Dark Barony\'s first ability doesn\'t behave like a leaves-the-battlefield triggered ability, since the card put into a graveyard may come from anywhere. If a card is put into a graveyard from the battlefield, its color is checked in the graveyard, not as it last existed on the battlefield.').

card_ruling('the dead shall serve', '2010-06-15', 'For each opponent, you may choose not to target any creature cards in that player\'s graveyard.').
card_ruling('the dead shall serve', '2010-06-15', 'Each of the creatures you put onto the battlefield this way must attack its owner, not a planeswalker that player controls.').
card_ruling('the dead shall serve', '2010-06-15', 'If, during your declare attackers step, one of these creatures is tapped or is affected by a spell or ability that says it can\'t attack, then it doesn\'t attack. If there\'s a cost associated with having that creature attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('the dead shall serve', '2010-06-15', 'If one of these creatures can\'t attack its owner during any given turn (due to a spell or ability such as Chronomantic Escape, or because a player on the opposing team has gained control of it, for example), it may attack another player, attack a planeswalker an opponent controls, or not attack at all. If there\'s a cost associated with having that creature attack its owner, you aren\'t forced to pay that cost, so it may attack another player, attack a planeswalker an opponent controls, or not attack at all.').
card_ruling('the dead shall serve', '2010-06-15', 'If there are multiple combat phases in a turn, each of these creatures must attack its owner in each of them that it\'s able to.').

card_ruling('the eon fog', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the eon fog', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the eon fog', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the eon fog', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('the fallen', '2004-10-04', 'It stops damaging players and forgets which players it damaged when it leaves the battlefield.').
card_ruling('the fallen', '2007-09-16', 'If The Fallen phases out and back in, it remembers which players it has dealt damage to this game.').
card_ruling('the fallen', '2007-09-16', 'The Fallen\'s ability causes it to deal damage only to opponents of its current controller. If The Fallen has dealt damage to you, and then you gain control of it, you won\'t be damaged during your upkeep.').
card_ruling('the fallen', '2007-09-16', 'The effect isn\'t cumulative. When it resolves, The Fallen deals 1 damage to each opponent previously dealt damage by it, regardless of how many times The Fallen dealt damage to that opponent before.').

card_ruling('the fate of the flammable', '2010-06-15', 'The targeted player may choose \"self\" even if he or she can\'t perform the resulting action. For example, a player targeted with Feed the Machine may choose \"self\" even if he or she controls no creatures.').
card_ruling('the fate of the flammable', '2010-06-15', 'The targeted player may choose \"others\" even if there are no others (because all of his or her teammates have lost the game, for example), or the archenemy\'s other opponents can\'t perform the resulting action.').
card_ruling('the fate of the flammable', '2010-06-15', 'In a Supervillain Rumble game, the targeted player may still choose \"others.\" Each player who isn\'t the active player or the targeted player will thus be affected.').

card_ruling('the fourth sphere', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the fourth sphere', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the fourth sphere', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the fourth sphere', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('the great aurora', '2015-06-22', 'The number of cards you draw is equal to the number of permanents on the battlefield that you shuffle into your library plus the number of cards from your hand that you shuffle into your library.').
card_ruling('the great aurora', '2015-06-22', 'A token is owned by the player under whose control it entered the battlefield. Tokens shuffled into a library count toward the number of cards drawn, though they cease to exist upon becoming part of a library. Regardless of what you’re using to represent tokens, you won’t shuffle that physical object into your library.').
card_ruling('the great aurora', '2015-06-22', 'If a player is required to draw more cards than his or her library contains, that player loses the game. If all players lose the game this way, the game is a draw.').
card_ruling('the great aurora', '2015-06-22', 'As The Great Aurora resolves, first you choose any number of land cards in your hand to put onto the battlefield, and then each other player in turn order does the same. The lands enter the battlefield simultaneously.').

card_ruling('the great forest', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the great forest', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the great forest', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the great forest', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('the great forest', '2009-10-01', 'The Great Forest\'s first ability means, for example, that a 2/3 creature will assign 3 damage in combat instead of 2.').
card_ruling('the great forest', '2009-10-01', 'The Great Forest\'s first ability doesn\'t actually change creatures\' power; it changes only the value of the combat damage they assign. All other rules and effects that check power or toughness use the real values.').

card_ruling('the hippodrome', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the hippodrome', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the hippodrome', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the hippodrome', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('the hippodrome', '2009-10-01', 'Creatures may have negative power. For example, a 3/4 creature that gets -5/-0 becomes -2/4. The total of its power and toughness is 2. It would need +3/+0 to raise its power to 1.').
card_ruling('the hippodrome', '2009-10-01', 'A creature that would assign 0 or less combat damage (due to having power 0 or less) doesn\'t assign combat damage at all.').
card_ruling('the hippodrome', '2009-10-01', 'You may target any creature with The Hippodrome\'s {C} ability. Whether its power is 0 or less isn\'t checked until the ability resolves.').

card_ruling('the maelstrom', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the maelstrom', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the maelstrom', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the maelstrom', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('the maelstrom', '2009-10-01', 'A permanent card is an artifact, creature, enchantment, land, or planeswalker card.').
card_ruling('the maelstrom', '2009-10-01', 'As The Maelstrom\'s first ability resolves, you choose whether or not to reveal the top card of your library. If you reveal it and it\'s a permanent card, you have two choices: Put it onto the battlefield, or put it on the bottom of your library. It can\'t remain on top of your library. On the other hand, if you reveal it and it\'s an instant or sorcery card, it must be put on the bottom of your library. There is no way you can leave a card revealed this way on top of your library.').
card_ruling('the maelstrom', '2009-10-01', 'If you reveal a permanent card that can\'t enter the battlefield (because it\'s an Aura and there\'s nothing it can enchant, for example), then you must put it on the bottom of your library.').

card_ruling('the mimeoplasm', '2011-09-22', 'You can\'t choose to exile just one creature card.').
card_ruling('the mimeoplasm', '2011-09-22', 'The two creature cards may come from the same graveyard or different graveyards.').
card_ruling('the mimeoplasm', '2011-09-22', 'You choose which one The Mimeoplasm is copying and which one determines how many +1/+1 counters are placed on The Mimeoplasm as The Mimeoplasm enters the battlefield.').
card_ruling('the mimeoplasm', '2011-09-22', 'Treat The Mimeoplasm as though it were the creature card it\'s copying entering the battlefield. Any \"As [this card] enters the battlefield,\" \"[This card] enters the battlefield with,\" and \"When [this card] enters the battlefield\" abilities of that creature card will work.').
card_ruling('the mimeoplasm', '2011-09-22', 'Specifically, if The Mimeoplasm enters the battlefield as a copy of a creature card that enters the battlefield with a number of +1/+1 counters, The Mimeoplasm will enter the battlefield with those +1/+1 counters and +1/+1 counters equal to the power of the other exiled card.').

card_ruling('the rack', '2004-10-04', 'You choose one opposing player as this card enters the battlefield and it only affects that one player. This choice is not changed even if this card changes controllers. It becomes useless but stays on the battlefield if the chosen player leaves the battlefield.').

card_ruling('the wretched', '2009-05-01', 'The ability grants you control of all creatures that are blocking it as the ability resolves. This will include any creatures that were put onto the battlefield blocking it.').
card_ruling('the wretched', '2009-05-01', 'Any blocking creatures that regenerated during combat will have been removed from combat. Since such creatures are no longer in combat, they cannot be blocking The Wretched, which means you won\'t be able to gain control of them.').
card_ruling('the wretched', '2009-05-01', 'If The Wretched itself regenerated during combat, then it will have been removed from combat. Since it is no longer in combat, there cannot be any creatures blocking it, which means you won\'t be able to gain control of any creatures.').
card_ruling('the wretched', '2009-10-01', 'The Wretched\'s ability triggers only if it\'s still on the battlefield when the end of combat step begins (after the combat damage step). For example, if it\'s blocked by a 7/7 creature and is destroyed, its ability won\'t trigger at all.').
card_ruling('the wretched', '2009-10-01', 'If The Wretched leaves the battlefield, you no longer control it, so the duration of its control-change effect ends.').
card_ruling('the wretched', '2009-10-01', 'If you lose control of The Wretched before its ability resolves, you won\'t gain control of the creatures blocking it at all.').
card_ruling('the wretched', '2009-10-01', 'Once the ability resolves, it doesn\'t care whether the permanents you gained control of remain creatures, only that they remain on the battlefield.').

card_ruling('the æther flues', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('the æther flues', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('the æther flues', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('the æther flues', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('the æther flues', '2009-10-01', 'The first ability of The Æther Flues doesn\'t target a creature. You don\'t choose a creature to sacrifice until the ability resolves. Once you choose a creature to sacrifice, it\'s too late for players to respond.').
card_ruling('the æther flues', '2009-10-01', 'If you use the first ability of The Æther Flues but there are no creatures in your library, you\'ll reveal your entire library then shuffle it.').

card_ruling('thelon of havenwood', '2006-09-25', 'All creatures with Thallid in their name happen to have the creature type Fungus, but not all Fungus creatures have Thallid in their names.').

card_ruling('thelon\'s chant', '2004-10-04', 'Damages the player putting the land onto the battlefield, even if they are not the controller of the land after it is on the battlefield.').
card_ruling('thelon\'s chant', '2004-10-04', 'Only checks the type of the land as it is put onto the battlefield. If the land gets changed after it enters the battlefield (even by a continuous effect), the Chant causes no additional effect.').

card_ruling('thelonite druid', '2004-10-04', 'The most recent land animating ability takes precedence.').
card_ruling('thelonite druid', '2004-10-04', 'Can sacrifice the Druid to itself.').
card_ruling('thelonite druid', '2004-10-04', 'Only affects Forests that are on the battlefield when the effect resolves.').
card_ruling('thelonite druid', '2004-10-04', 'The land animation ability does not wear off if the land stops being a Forest. It continues until the end of the turn.').
card_ruling('thelonite druid', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('thelonite monk', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').

card_ruling('thermal flux', '2006-07-15', 'Thermal Flux adds or removes the supertype snow from a permanent. Adding or removing a supertype doesn\'t affect any other types, supertypes, or subtypes that permanent has.').

card_ruling('thespian\'s stage', '2013-01-24', 'The copy effect created by the last activated ability doesn\'t have a duration. It will last until Thespian\'s Stage leaves the battlefield or another copy effect overwrites it. The permanent will no longer have the first ability of Thespian\'s Stage.').
card_ruling('thespian\'s stage', '2013-01-24', 'A land\'s copiable values are those printed on it, as modified by other copy effects. Counters and other effects aren\'t copied. Notably, if you copy a land that is also a creature because of a temporary effect (such as Celestial Colonnade), Thespian\'s Stage will become just the “unanimated” land.').
card_ruling('thespian\'s stage', '2013-01-24', 'No enters-the-battlefield abilities of the land Thespian\'s Stage is copying will trigger. Thespian\'s Stage was already on the battlefield.').

card_ruling('thick-skinned goblin', '2006-09-25', 'When an echo ability you control resolves, you have three options: pay the echo cost, pay {0}, or sacrifice the permanent.').

card_ruling('thicket basilisk', '2004-10-04', 'Protection from Green does not stop the Basilisk\'s ability because the ability is not targeted.').
card_ruling('thicket basilisk', '2004-10-04', 'The ability destroys the creature at the end of the combat, which is after all first strike and normal damage dealing is done. This means that a creature may have to regenerate twice to survive the combat, once from damage and once again at end of combat.').

card_ruling('thicket elemental', '2004-10-04', 'The \"if you do\" means \"if you choose to reveal cards\". You still shuffle if you have no creature card in your library.').

card_ruling('thieves\' auction', '2004-10-04', 'In multi-player games, it affects all players and players make choices in turn order.').
card_ruling('thieves\' auction', '2004-10-04', 'An Aura put onto the battlefield this way does not target the permanent it is placed upon.').
card_ruling('thieves\' auction', '2004-10-04', 'Triggers from the permanents leaving the battlefield and then being put back onto the battlefield are not put on the stack until the entire effect is done.').
card_ruling('thieves\' auction', '2004-10-04', 'You can\'t pick a card that can\'t legally be returned to the battlefield. If there are no cards that can be legally returned, then you can\'t pick one.').
card_ruling('thieves\' auction', '2005-08-01', 'If you choose an Aura, you can put it on any permanent it can legally enchant. If there is nothing it can legally enchant, it remains exiled, and it can still be chosen by you or another player later in the auction. If there comes a point where only Auras and no legal permanents left, they stay exiled permanently.').

card_ruling('thieving sprite', '2013-06-07', 'The number of cards that are revealed is equal to the number of Faeries you control when the ability resolves.').

card_ruling('thirst for knowledge', '2004-12-01', 'You can discard either one artifact card or two cards which may or may not be artifacts. If you really want to, you can discard two artifact cards.').

card_ruling('thistledown duo', '2008-05-01', 'An object that\'s both of the listed colors will cause both abilities to trigger. You can put them on the stack in either order.').

card_ruling('thistledown liege', '2008-05-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('thopter assembly', '2011-06-01', 'The triggered ability looks for any permanent you control with the creature type Thopter, not just the Thopter tokens previously created by a Thopter Assembly.').

card_ruling('thopter foundry', '2009-05-01', 'The nontoken artifact you sacrifice may be Thopter Foundry itself.').

card_ruling('thopter spy network', '2015-06-22', 'Thopter Spy Network’s first ability has an “intervening ‘if’ clause.” That means (1) the ability won’t trigger at all unless you control an artifact as your upkeep begins, and (2) the ability will do nothing if you don’t control an artifact as it resolves.').
card_ruling('thopter spy network', '2015-06-22', 'The last ability will trigger, at most, once per combat damage step. However, if at least one artifact creature you control has first strike and others don’t, or if an artifact creature you control has double strike, the ability could trigger twice per combat: once in each combat damage step.').

card_ruling('thorn elemental', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('thorn elemental', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('thorn elemental', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('thorn elemental', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('thorn-thrash viashino', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('thorn-thrash viashino', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('thorn-thrash viashino', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('thorn-thrash viashino', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').

card_ruling('thornbite staff', '2008-04-01', 'Each of these Equipment has two subtypes listed on its type line. The first one is a creature type, which in this case is also a subtype of tribal. The second one is Equipment, which is a subtype of artifact.').
card_ruling('thornbite staff', '2008-04-01', 'Each of these Equipment has a triggered ability that says \"Whenever a [creature type] creature enters the battlefield, you may attach [this Equipment] to it.\" This triggers whenever any creature of the specified creature type enters the battlefield, no matter who controls it. You may attach your Equipment to another player\'s creature this way, even though you can\'t do so with the equip ability.').
card_ruling('thornbite staff', '2008-04-01', 'If you attach an Equipment you control to another player\'s creature, you retain control of the Equipment, but you don\'t control the creature. Only you can activate the Equipment\'s equip ability, and if the Equipment\'s ability triggers again, you choose whether to move the Equipment. Only the creature\'s controller can activate any activated abilities the Equipment grants to the creature, and \"you\" in any abilities granted to the creature refers to that player.').

card_ruling('thorncaster sliver', '2013-07-01', 'If you control multiple Thorncaster Slivers and a Sliver you control attacks, each ability will trigger separately.').
card_ruling('thorncaster sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('thorncaster sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('thornling', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('thornscape familiar', '2004-10-04', 'If a spell is both red and white, you pay {1} less, not {2} less.').
card_ruling('thornscape familiar', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('thornscape familiar', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('thornscape familiar', '2004-10-04', 'The effect is cumulative.').
card_ruling('thornscape familiar', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('thornscape familiar', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('thornscape familiar', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('thought devourer', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you are affected by Cursed Rack (an artifact that sets a player\'s maximum hand size to four) and then put Thought Devourer onto the battlefield, your maximum hand size would be zero. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be four.').

card_ruling('thought dissector', '2004-12-01', 'You put the artifact card onto the battlefield even if Thought Dissector has already left the battlefield and you can\'t sacrifice it.').

card_ruling('thought eater', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you are affected by Cursed Rack (an artifact that sets a player\'s maximum hand size to four) and then put Thought Eater onto the battlefield, your maximum hand size would be one. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be four.').

card_ruling('thought gorger', '2010-06-15', 'As Thought Gorger\'s first ability resolves, putting a +1/+1 counter on it for each card in your hand is mandatory. The effect says \"if you do\" because performing that action might be impossible. If Thought Gorger has left the battlefield by the time the ability resolves, you can\'t put any +1/+1 counters on it, so you won\'t discard your hand.').
card_ruling('thought gorger', '2010-06-15', 'When Thought Gorger\'s second ability resolves, its last existence on the battlefield is checked to see how many +1/+1 counters it had on it. All of them are counted, not just the ones put on it by its enters-the-battlefield ability.').
card_ruling('thought gorger', '2010-06-15', 'Once an ability starts to resolve, it\'s too late to respond to it. For example, you can\'t count a card in your hand while determining how many +1/+1 counters Thought Gorger gets and then cast it in response. If a card is counted, it\'ll be discarded.').

card_ruling('thought hemorrhage', '2009-05-01', 'The card is named as Thought Hemorrhage resolves. Spells and abilities can\'t be cast or activated between the time a card is named and the time that Thought Hemorrhage has the rest of its effect.').
card_ruling('thought hemorrhage', '2009-05-01', 'If the targeted player has multiple cards with the chosen name in his or her hand, the damage dealt by Thought Hemorrhage is all considered to be the same instance of damage. A single effect that prevents \"the next time a source would deal damage,\" for example, will prevent all of it.').
card_ruling('thought hemorrhage', '2009-05-01', 'Thought Hemorrhage has no effect on cards on the battlefield that have the same name as the named card.').
card_ruling('thought hemorrhage', '2009-05-01', 'The cards you\'re searching for must be found if they\'re in the graveyard, because that\'s a zone everyone can see. Finding those cards in the hand and library is optional, though. This is true even though the targeted player has revealed his or her hand.').
card_ruling('thought hemorrhage', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('thought lash', '2004-10-04', 'If you don\'t pay Thought Lash\'s cumulative upkeep, you\'ll sacrifice Thought Lash as normal, then, when its triggered ability resolves, you\'ll exile all cards in your library.').
card_ruling('thought lash', '2008-10-01', 'If your library has no cards in it, you can\'t activate the last ability.').

card_ruling('thought nibbler', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you are affected by Cursed Rack (an artifact that sets a player\'s maximum hand size to four) and then put Thought Nibbler onto the battlefield, your maximum hand size would be two. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be four.').

card_ruling('thought prison', '2004-12-01', 'The second ability triggers only once per spell. If the spell shares both a color and a converted mana cost with the imprinted card, Thought Prison deals 2 damage, not 4.').

card_ruling('thought reflection', '2008-05-01', 'Thought Reflection doesn\'t cause you to draw cards. Rather, it causes card-drawing effects to have you draw more cards. It also causes the draw action during your draw step to have you draw two cards.').
card_ruling('thought reflection', '2008-05-01', 'If a spell or ability causes you to draw multiple cards, Thought Reflection\'s effect doubles the total number you would draw. For example, if you cast Concentrate (\"Draw three cards\"), you\'ll draw six cards.').
card_ruling('thought reflection', '2008-05-01', 'The effects of multiple Thought Reflections are cumulative. For example, if you have three Thought Reflections on the battlefield, you\'ll draw eight times the original number of cards.').
card_ruling('thought reflection', '2008-05-01', 'If two or more replacement effects would apply to a card-drawing event, the player who\'s drawing the card chooses what order to apply them.').

card_ruling('thought scour', '2011-01-22', 'If the target player is an illegal target when Thought Scour tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').
card_ruling('thought scour', '2011-01-22', 'Follow the instructions in the order listed on the card: if you target yourself, you\'ll put the top two cards of your library into your graveyard and then draw a card.').

card_ruling('thoughtrender lamia', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('thoughtrender lamia', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('thoughtrender lamia', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('thoughtseize', '2013-09-15', 'The loss of life is part of the spell’s effect. It’s not an additional cost. If Thoughtseize is countered, you won’t lose life.').
card_ruling('thoughtseize', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('thousand winds', '2014-09-20', 'It doesn’t matter if Thousand Winds is tapped or untapped (or even on the battlefield) when its last ability resolves. Tapped creatures except Thousand Winds will be returned to their owners’ hands.').
card_ruling('thousand winds', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('thousand winds', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('thousand winds', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('thousand winds', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('thousand winds', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('thousand winds', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('thousand winds', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('thousand winds', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('thousand winds', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('thousand-year elixir', '2007-10-01', 'Thousand-Year Elixir doesn\'t actually grant haste to creatures you control, nor does it let you attack with them as though they had haste.').

card_ruling('thraben doomsayer', '2011-01-22', 'A creature whose toughness is being increased by Thraben Doomsayer\'s fateful hour ability loses that bonus as soon as its controller has 6 or more life (or as soon as Thraben Doomsayer leaves the battlefield). If damage marked on that creature is greater than or equal to its toughness after the fateful hour ability stops applying, the creature is destroyed and put into its owner\'s graveyard.').
card_ruling('thraben doomsayer', '2011-01-22', 'If Thraben Doomsayer\'s activated ability resolves while you have 5 or less life, the creature token will be 3/3 as it enters the battlefield.').

card_ruling('thraben sentry', '2011-09-22', 'If multiple creatures you control die simultaneously, Thraben Sentry\'s ability will trigger that many times. Each time one of those abilities resolves, you may transform the creature, even if it\'s Thraben Militia at the time.').

card_ruling('thragtusk', '2012-07-01', 'Thragtusk\'s second ability will trigger no matter what zone Thragtusk goes to.').

card_ruling('thran forge', '2005-11-01', 'Now becomes an artifact in addition to its other types.').
card_ruling('thran forge', '2008-04-01', 'If Thran Forge\'s ability is activated targeting a nonartifact creature, then it\'s activated again in response targeting the same creature, the second instance will resolve. The first instance will then be countered for having an illegal target, since the target is no longer a nonartifact creature.').

card_ruling('thran lens', '2004-10-04', 'Does not make the permanents into artifacts. They are simply without color.').
card_ruling('thran lens', '2004-10-04', 'Does not prevent a spell or ability from adding color to permanents after this effect is applied.').

card_ruling('thran quarry', '2004-10-04', 'Triggers at the end of every player\'s turn.').

card_ruling('thran tome', '2006-10-15', 'It does not target the cards, but it targets the opponent. If you can\'t target an opponent, you can\'t activate this ability.').
card_ruling('thran tome', '2007-05-01', 'After the opponent chooses which card to put into your graveyard, you draw two cards regardless of how many were actually revealed or how many you have left in your library.').

card_ruling('thran turbine', '2004-10-04', 'The ability is optional. And you can add 0, 1, or 2 mana. You decide how much during resolution.').
card_ruling('thran turbine', '2011-01-25', 'Any of this mana that isn\'t spent during your upkeep step will be removed before your draw step begins. You cannot save it to be spent on the card you will draw this turn.').

card_ruling('thrashing mossdog', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('thraximundar', '2009-05-01', 'Thraximundar\'s middle ability resolves during the declare attackers step. The defending player sacrifices a creature before it would get a chance to block.').
card_ruling('thraximundar', '2009-05-01', 'Thraximundar\'s last ability triggers whenever any player (including you) sacrifices a creature for any reason. It doesn\'t trigger only when a creature is sacrificed due to its middle ability. Note that Thraximundar itself doesn\'t allow you to sacrifice any creatures.').

card_ruling('threads of disloyalty', '2005-02-01', 'Threads of Disloyalty can\'t target a creature with converted mana cost 3 or greater. If it somehow enchants a creature with converted mana cost 3 or greater, Threads of Disloyalty is put into the graveyard the next time state-based actions are checked.').
card_ruling('threads of disloyalty', '2005-02-01', 'Creatures on the battlefield treat {X} in their mana cost as 0 for the purposes of calculating converted mana costs.').
card_ruling('threads of disloyalty', '2005-02-01', 'Tokens usually have a converted mana cost of 0.').

card_ruling('three visits', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('three visits', '2009-10-01', 'Three Visits allows you to search your library for any card with the land type Forest, not just a card with the name Forest.').

card_ruling('three wishes', '2004-10-04', 'You can look at the three exiled cards at any time.').
card_ruling('three wishes', '2004-10-04', 'You can only play them before the start of your upkeep. Between the start of your upkeep and putting the extra cards into the graveyard, you can\'t play them.').
card_ruling('three wishes', '2004-10-04', 'To \"play a card\" is to either cast a spell or to put a land onto the battlefield using the main phase special action.').

card_ruling('thrill-kill assassin', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('thrill-kill assassin', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('thrill-kill assassin', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('thrill-kill assassin', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('thrive', '2004-10-04', 'It means X different creatures. You can\'t give more than one +1/+1 counter to a creature with this spell.').

card_ruling('thromok the insatiable', '2012-06-01', 'For example, if Thromok devours one creature, it will enter the battlefield with one +1/+1 counter on it. If it devours two creatures, it will enter with two +1/+1 counters for each of them, for a total of four +1/+1 counters. Devouring three creatures will produce nine +1/+1 counters, and so on.').

card_ruling('throne of empires', '2011-09-22', 'Whether or not you control the correct artifacts is determined when the ability resolves.').
card_ruling('throne of empires', '2011-09-22', 'If any of the named cards stops being an artifact, it won\'t be considered by these abilities.').

card_ruling('throne of geth', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('throne of geth', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('throne of geth', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('throne of geth', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('throne of geth', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').
card_ruling('throne of geth', '2011-01-01', 'You may sacrifice Throne of Geth to activate its ability.').

card_ruling('through the breach', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').
card_ruling('through the breach', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('through the breach', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('through the breach', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('through the breach', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('throwing knife', '2015-06-22', 'Throwing Knife’s triggered ability triggers before blockers are declared. If you use the ability and destroy the targeted creature with lethal damage, it won’t be on the battlefield to block.').
card_ruling('throwing knife', '2015-06-22', 'Throwing Knife is the source of the damage dealt because of its triggered ability. For example, you could sacrifice Throwing Knife to deal 2 damage to a creature with protection from red, even if Throwing Knife is equipped to a red creature.').

card_ruling('thrull champion', '2004-10-04', 'Can be used to steal another Thrull Champion.').
card_ruling('thrull champion', '2007-09-16', 'Gives himself +1/+1.').
card_ruling('thrull champion', '2007-09-16', 'Unlike many similar creatures, Thrull Champion untaps as normal during your untap step. Whether it\'s tapped or untapped has no bearing on the control-change effect.').

card_ruling('thrull parasite', '2013-01-24', 'The nonland permanent you choose as the target doesn\'t have to have any counters on it. If it has more than one kind of counter, you\'ll choose one of those counters when the ability resolves.').
card_ruling('thrull parasite', '2013-01-24', 'You can\'t activate Thrull Parasite\'s last ability to stop a player from paying the cost of a spell or an ability that requires removing a counter from a permanent (such as a planeswalker\'s loyalty ability).').
card_ruling('thrull parasite', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('thrull parasite', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('thrull parasite', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('thrull retainer', '2004-10-04', 'This leaves the battlefield when you activate its activated ability, but the enchanted creature won\'t be regenerated until the ability resolves. In the intervening time, the creature will no longer have the bonus that this had been giving it. This may cause the creature to be destroyed before the regeneration shield is created.').

card_ruling('thrull wizard', '2004-10-04', 'You can use this ability multiple times on one spell to force your opponent to use more than one mana to prevent the spell from being countered. You can even use this ability, let them pay, then use this ability repeatedly.').

card_ruling('thrumming stone', '2006-07-15', 'If you cast a spell that already has ripple (such as Surging Flame, which has ripple 4) while Thrumming Stone is on the battlefield, both ripple abilities will trigger separately. Assuming you choose to do all \"you may\" actions, when the first instance of ripple resolves, you reveal the top four cards of your library, cast all Surging Flames, and put the rest of the cards on the bottom of your library. Any new Surging Flames are on the stack on top of the other instance of ripple, so they\'ll resolve next. Each of them also has two instances of ripple, which trigger separately, and the process continues until the ripple abilities don\'t reveal any Surging Flames.').
card_ruling('thrumming stone', '2006-07-15', 'If you cast half of a split card (for example, Hit) and reveal another one of those split cards when the ripple ability resolves (for example, Hit//Run), you may cast either half of that split card (Hit or Run) without paying its mana cost.').

card_ruling('thrummingbird', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('thrummingbird', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('thrummingbird', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('thrummingbird', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('thrummingbird', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').

card_ruling('thumbscrews', '2004-10-04', 'It checks the number of cards in your hand at the beginning of upkeep, and if you have 4 or fewer cards in hand it will not trigger. It checks again when it resolves and will not deal damage unless you still have 5 or more cards in hand.').

card_ruling('thunder brute', '2014-02-01', 'If the opponent pays tribute, the creature will enter the battlefield with the specified number of +1/+1 counters on it. It won’t enter the battlefield and then have the +1/+1 counters placed on it.').
card_ruling('thunder brute', '2014-02-01', 'The choice of whether to pay tribute is made as the creature with tribute is entering the battlefield. At that point, it’s too late to respond to the creature spell. For example, in a multiplayer game, opponents won’t know whether tribute will be paid or which opponent will be chosen to pay tribute or not when deciding whether to counter the creature spell.').
card_ruling('thunder brute', '2014-02-01', 'If the triggered ability has a target, that target will not be known while the creature spell with tribute is on the stack.').
card_ruling('thunder brute', '2014-02-01', 'Players can’t respond to the tribute decision before the creature enters the battlefield. That is, if the opponent doesn’t pay tribute, the triggered ability will trigger before any player has a chance to remove the creature.').
card_ruling('thunder brute', '2014-02-01', 'The triggered ability will resolve even if the creature with tribute isn’t on the battlefield at that time.').

card_ruling('thunder strike', '2010-08-15', 'Thunder Strike can target any creature, even one that already has first strike or double strike. (If it affects such a creature, it will, in effect, just give it +2/+0.)').
card_ruling('thunder strike', '2010-08-15', 'If a creature doesn\'t have first strike, granting it first strike after combat damage has been dealt in the first-strike combat damage step won\'t prevent it from dealing combat damage. It will still assign and deal its combat damage in the second combat damage step.').

card_ruling('thunder totem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('thunder-thrash elder', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('thunder-thrash elder', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('thunder-thrash elder', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('thunder-thrash elder', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').

card_ruling('thunderblade charge', '2007-05-01', 'This card has minor errata to make casting the spell optional. When the ability triggers, first you choose whether to pay {2}{R}{R}{R}. If you paid, then you choose whether to cast the spell.').
card_ruling('thunderblade charge', '2007-05-01', 'Thunderblade Charge triggers only once per combat damage step no matter how many of your creatures deal combat damage to a player, how much damage is dealt, or who that player is. (It might be you if the combat damage is redirected.) However, the ability could trigger twice in the same turn if there are multiple combat damage steps due to first strike or double strike.').

card_ruling('thunderblust', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('thunderblust', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('thunderblust', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('thunderblust', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('thunderblust', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('thunderblust', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('thunderblust', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('thunderbreak regent', '2015-02-25', 'Thunderbreak Regent’s ability will resolve before the spell or ability that caused it to trigger.').

card_ruling('thunderfoot baloth', '2014-11-07', 'Lieutenant abilities apply only if your commander is on the battlefield and under your control.').
card_ruling('thunderfoot baloth', '2014-11-07', 'Lieutenant abilities refer only to whether you control your commander, not any other player’s commander.').
card_ruling('thunderfoot baloth', '2014-11-07', 'If you gain control of a creature with a lieutenant ability owned by another player, that ability will check to see if you control your commander and will apply if you do. It won’t check whether its owner controls his or her commander.').
card_ruling('thunderfoot baloth', '2014-11-07', 'If you lose control of your commander, lieutenant abilities of creatures you control will immediately stop applying. If this causes a creature’s toughness to become less than or equal to the amount of damage marked on it, the creature will be destroyed.').
card_ruling('thunderfoot baloth', '2014-11-07', 'If a triggered ability granted by a lieutenant ability triggers, and in response to that trigger you lose control of your commander (causing the lieutenant to lose that ability), that triggered ability will still resolve.').

card_ruling('thundering tanadon', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('thundering tanadon', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('thundering tanadon', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('thundering tanadon', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('thundering tanadon', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('thundermaw hellkite', '2012-07-01', 'Thundermaw Hellkite will deal 1 damage to each creature with flying your opponents control whether those creatures are tapped or untapped.').
card_ruling('thundermaw hellkite', '2012-07-01', 'If the damage that would be dealt to a creature with flying is prevented, that creature will still be tapped. If that damage is redirected to another creature, the creature with flying will be tapped, not necessarily the creature that ultimately was dealt damage.').

card_ruling('thunderous might', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('thunderous might', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('thunderous might', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('thunderous might', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('thunderscape familiar', '2004-10-04', 'If a spell is both black and green, you pay {1} less, not {2} less.').
card_ruling('thunderscape familiar', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('thunderscape familiar', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('thunderscape familiar', '2004-10-04', 'The effect is cumulative.').
card_ruling('thunderscape familiar', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('thunderscape familiar', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('thunderscape familiar', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('thunderstaff', '2004-12-01', 'If Thunderstaff is untapped, it prevents 1 damage from each creature, each time it would deal combat damage to you. So if three creatures with double strike attack you and aren\'t blocked, Thunderstaff will prevent a total of 6 damage.').

card_ruling('tibalt, the fiend-blooded', '2012-05-01', 'The number of cards in the target player\'s hand is determined when Tibalt\'s second ability resolves.').
card_ruling('tibalt, the fiend-blooded', '2012-05-01', 'When Tibalt\'s third ability resolves, all creatures will become untapped and gain haste, including ones you controlled before the ability resolved.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('tibalt, the fiend-blooded', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('tibor and lumia', '2006-02-01', 'A spell you cast that\'s blue and red will trigger both abilities. You can put them on the stack in either order.').

card_ruling('tidal flats', '2004-10-04', 'Can be used more than once in a turn. Each time, the opponent can pay to prevent giving first strike to a creature, but this payment only stops the current activation and not future activations of Tidal Flats.').
card_ruling('tidal flats', '2004-10-04', 'The payment is made when the Tidal Flats effect resolves.').
card_ruling('tidal flats', '2004-10-04', 'First Strike is granted to your creatures currently blocking non-Flyers when this effect resolves.').

card_ruling('tidal influence', '2004-10-04', 'As soon as the number of counters on this card changes, so does the power of blue creatures.').

card_ruling('tide drifter', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('tide drifter', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('tide drifter', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('tide drifter', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('tide drifter', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('tide of war', '2004-12-01', 'Tide of War\'s ability triggers only once each combat and only if at least one blocker is declared. The effect does nothing to unblocked attacking creatures.').
card_ruling('tide of war', '2004-12-01', 'Tide of War\'s controller always flips the coin. This means that you want to win the flip when you\'re attacking but lose the flip when you\'re defending.').
card_ruling('tide of war', '2004-12-01', 'Even if the blockers are sacrificed, any attackers that were blocked are still considered blocked.').

card_ruling('tidebinder mage', '2013-07-01', 'If you lose control of Tidebinder Mage between when it entered the battlefield and when the ability resolves, the target creature will become tapped. However, it won’t be affected by the other effect and will untap as normal during its controller’s untap step.').
card_ruling('tidebinder mage', '2013-07-01', 'If another player gains control of Tidebinder Mage, the creature will no longer be affected by the ability preventing it from untapping, even if you later regain control of Tidebinder Mage.').
card_ruling('tidebinder mage', '2013-07-01', 'If you gain control of the creature that’s being prevented from untapping, that creature won’t untap during your untap step for as long as you control Tidebinder Mage.').
card_ruling('tidebinder mage', '2013-07-01', 'The ability can target a tapped creature. If the targeted creature is already tapped when it resolves, that creature just remains tapped and doesn’t untap during its controller’s next untap step.').

card_ruling('tideforce elemental', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('tideforce elemental', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('tidehollow sculler', '2013-06-07', 'If Tidehollow Sculler leaves the battlefield before its first ability has resolved, its second ability will trigger. This ability will do nothing when it resolves. Then its first ability will resolve and exile the chosen card forever.').

card_ruling('tidewalker', '2007-02-01', 'Unless something is boosting Tidewalker\'s toughness, when the last time counter is removed, it will be put into the graveyard as a state-based action for having 0 toughness before it can be sacrificed as the result of vanishing.').
card_ruling('tidewalker', '2007-02-01', 'If you don\'t control any Islands when Tidewalker enters the battlefield, its vanishing ability has no effect. (It enters the battlefield with no time counters on it, so neither of the vanishing triggers can trigger. See \"Vanishing\" in the General Notes section above.) On the other hand, it will be 0/0 and will be put into its owner\'s graveyard unless something is boosting its toughness.').

card_ruling('tightening coils', '2015-08-25', 'If an effect gives the enchanted creature flying after Tightening Coils became attached to it, the creature will have flying.').

card_ruling('timber protector', '2013-07-01', 'If Timber Protector somehow becomes a Forest, it doesn\'t give itself indestructible.').

card_ruling('timber wolves', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('timber wolves', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('timber wolves', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('timber wolves', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('timberland guide', '2012-05-01', 'You may choose Timberland Guide as the target of its own ability.').

card_ruling('time distortion', '2012-06-01', 'This effect reverses the turn order established at the beginning of the game. While the order is reversed, anything that cares about this order (such as determining in what order choices are made) will use the new reversed').
card_ruling('time distortion', '2012-06-01', 'If the game\'s turn order is reversed again, it will return to the default order used at the beginning of the game.').

card_ruling('time elemental', '2004-10-04', 'You both sacrifice it and deal the 5 damage at end of combat.').
card_ruling('time elemental', '2005-08-01', 'A \"permanent that isn\'t enchanted\" is a permanent with no Auras on it.').
card_ruling('time elemental', '2007-09-16', 'If Time Elemental attacks or blocks, but you no longer control it at end of combat, you can\'t sacrifice it. However, it will still deal 5 damage to you.').

card_ruling('time of heroes', '2010-06-15', 'Each creature you control with a level counter on it gets just +2/+2, regardless of how many level counters it has. It doesn\'t get the bonus for each counter.').
card_ruling('time of heroes', '2010-06-15', 'Time of Heroes affects any creature you control with a level counter on it, not just levelers. (Of course, it\'s pretty hard to get level counters onto a creature that\'s not a leveler, but it can be done with copy effects, Fate Transfer, or Quicksilver Elemental, among other possibilities.)').

card_ruling('time sieve', '2009-05-01', 'One of the five artifacts you sacrifice may be Time Sieve itself.').

card_ruling('time spiral', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('time spiral', '2004-10-04', 'This does not target the lands.').

card_ruling('time stop', '2004-12-01', 'Exiling a spell or ability prevents that spell or ability from resolving, but it doesn\'t technically \"counter\" anything. This means that Time Stop can exile spells and abilities that \"can\'t be countered.\"').
card_ruling('time stop', '2004-12-01', 'Unless Time Stop is cast during the Ending phase, any \"at the beginning of the end step\"-triggered abilities don\'t get the chance to trigger on the turn Time Stop is cast. These abilities will trigger at the beginning of the next end step.').
card_ruling('time stop', '2007-07-15', 'Ending the turn this way means the following things happen in order: 1) All spells and abilities on the stack are exiled. This includes Time Stop, though it will continue to resolve. It also includes spells and abilities that can\'t be countered. 2) All attacking and blocking creatures are removed from combat. 3) State-based actions are checked. No player gets priority, and no triggered abilities are put onto the stack. 4) The current phase and/or step ends. The game skips straight to the cleanup step. The cleanup step happens in its entirety.').
card_ruling('time stop', '2007-07-15', 'If any triggered abilities do trigger during this process, they\'re put onto the stack during the cleanup step. If this happens, players will have a chance to cast spells and activate abilities, then there will be another cleanup step before the turn finally ends.').

card_ruling('time stretch', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('time to feed', '2013-09-15', 'Time to Feed has two targets: a creature an opponent controls and a creature you control. If only one of those creatures is a legal target when Time to Feed tries to resolve, the creatures won’t fight and neither will deal or be dealt damage. If the creature you don’t control is the illegal target, you won’t gain life when it dies.').
card_ruling('time to feed', '2013-09-15', 'If neither creature is a legal target when Time to Feed tries to resolve, the spell will be countered and none of its effects will happen.').
card_ruling('time to feed', '2013-09-15', 'If the first target creature dies that turn, you’ll gain 3 life no matter what caused the creature to die or who controls the creature at that time.').

card_ruling('time vault', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').
card_ruling('time vault', '2008-10-01', 'The choice of whether or not to skip a turn is made as that turn would begin, and only if Time Vault is tapped at that time. If you choose to skip your turn, Time Vault untaps before anything else happens in the next turn.').
card_ruling('time vault', '2008-10-01', 'This wording means that you no longer end up skipping multiple turns if you find a way to activate the ability multiple times.').

card_ruling('time walk', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('time warp', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('timebender', '2007-02-01', 'The mode and target of the triggered ability are chosen when it\'s put on the stack.').

card_ruling('timely hordemate', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('timely hordemate', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('timely reinforcements', '2011-09-22', 'You compare life totals with each of your opponents when Timely Reinforcements resolves. At that time, if you have less life than an opponent, you\'ll gain 6 life. Similarly, you compare the number of creatures you control with the number of creatures each opponent controls when Timely Reinforcements resolves.').
card_ruling('timely reinforcements', '2011-09-22', 'In a multiplayer game, you consider each opponent. If you have less life than one of your opponents and fewer creatures than one of your opponents (although this may be a different opponent), you\'ll get both bonuses.').
card_ruling('timely reinforcements', '2011-09-22', 'In a Two-Headed Giant game, you\'ll compare your team\'s life total with the opposing team\'s life total. You\'ll then compare the number of creatures you control (ignoring creatures your teammate controls) to the number of creatures each opponent controls separately.').

card_ruling('timesifter', '2004-12-01', 'Each player must exile the top card of his or her library at the start of each upkeep step.').
card_ruling('timesifter', '2004-12-01', 'Each time there\'s a tie for the highest cost, only the players involved in the tie continue.').
card_ruling('timesifter', '2004-12-01', 'If a player fails to exile a card, he or she can\'t take the extra turn.').
card_ruling('timesifter', '2004-12-01', 'If multiple extra turns are created in a game, the most recently created extra turn is taken first.').
card_ruling('timesifter', '2004-12-01', 'Remember which player would have taken the next turn if Timesifter\'s ability hadn\'t triggered the first time. After Timesifter leaves the battlefield and all extra turns have been taken, that player takes the next turn.').
card_ruling('timesifter', '2004-12-01', 'If two or more Timesifters are on the battlefield, keep a careful record of which players get which extra turns. With two Timesifters on the battlefield, two extra turns are created for each turn taken.').
card_ruling('timesifter', '2004-12-01', 'A land card has a converted mana cost of 0.').
card_ruling('timesifter', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('timetwister', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('timmerian fiends', '2004-10-04', 'Both this card and the target must be on the battlefield when announcing the ability. The target must be on the battlefield when the ability resolves or nothing happens.').

card_ruling('tinker', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('titan\'s presence', '2015-08-25', 'The colorless creature card stays revealed until Titan’s Presence leaves the stack.').
card_ruling('titan\'s presence', '2015-08-25', 'The colorless creature card you reveal doesn’t have to still be in your hand as Titan’s Presence resolves. If it’s not, use its power as it last existed in your hand.').
card_ruling('titan\'s presence', '2015-08-25', 'Check the target creature’s power as Titan’s Presence resolves to see if it’s less than or equal to revealed creature card’s power. If it’s greater, Titan’s Presence will finish resolving with no effect.').

card_ruling('titan\'s revenge', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('titan\'s revenge', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').

card_ruling('titania\'s song', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('titanic ultimatum', '2008-10-01', 'The set of creatures affected is fixed as Titanic Ultimatum resolves.').
card_ruling('titanic ultimatum', '2008-10-01', 'Multiple instances of first strike and trample are redundant.').
card_ruling('titanic ultimatum', '2009-10-01', 'If a creature you control already has lifelink, Titanic Ultimatum will give it a second instance of lifelink; however, multiple instances of lifelink are redundant. You still only gain life equal to the amount of damage dealt, regardless of how many instances of lifelink the creature has.').

card_ruling('tithe', '2004-10-04', 'Counts lands on resolution, not on announcement.').
card_ruling('tithe', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('tithe drinker', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('tithe drinker', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('tithe drinker', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('toil', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('toil', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('toil', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('toil', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('toil', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('toil', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('toil', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('toil', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('toil', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('toil', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('toil', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('toil', '2013-04-15', 'If you cast Toil // Trouble as a fused split card and choose to target the same player twice, the two drawn cards are counted when determining how much damage is dealt. The player can\'t do anything between the effects of Toil and Trouble to have fewer cards in his or her hand.').

card_ruling('toils of night and day', '2005-02-01', 'You must choose two different targets when you cast Toils of Night and Day. The targets are chosen on announcement. The tap or untap choice is made on resolution.').

card_ruling('tolaria west', '2007-05-01', 'Cards with mana cost {0} (like Pact of Negation) and cards with no mana cost (like Living End and every land) have converted mana cost 0.').

card_ruling('tolarian entrancer', '2008-04-01', 'The control change has no duration; you\'ll retain control of that creature until the game ends or until some other effect causes it to change control. It doesn\'t matter if Tolarian Entrancer leaves the battlefield, or if Tolarian Entrancer has already left the battlefield by the time the “at end of combat” ability triggers.').

card_ruling('tolarian serpent', '2004-10-04', 'If less than seven cards in the library, move all that are there.').

card_ruling('tolarian winds', '2004-10-04', 'It counts how many cards you discard. Since this card will not be in your hand at that time, this card is not counted.').

card_ruling('tolsimir wolfblood', '2005-10-01', 'Other creatures you control that are both green and white, including Voja, get +2/+2.').
card_ruling('tolsimir wolfblood', '2005-10-01', 'The token is named \"Voja\" and has creature type \"Wolf.\" This is different from most creature tokens, where the name and creature type are the same.').
card_ruling('tolsimir wolfblood', '2013-07-01', 'The \"legend rule\" means that creating a second Voja while one is already under your control will result in one of them being put into its owner\'s graveyard (where it promptly ceases to exist). You choose which of the two remains on the battlefield and which is put into the graveyard.').

card_ruling('tomb hex', '2010-03-01', 'Whether you had a land enter the battlefield under your control this turn is checked as this spell resolves, not as you cast it.').
card_ruling('tomb hex', '2010-03-01', 'Having more than one land enter the battlefield under your control this turn provides no additional benefit.').
card_ruling('tomb hex', '2010-03-01', 'The landfall ability checks for an action that has happened in the past. It doesn\'t matter if a land that entered the battlefield under your control previously in the turn is still on the battlefield, is still under your control, or is still a land.').
card_ruling('tomb hex', '2010-03-01', 'Once the spell resolves, having a land enter the battlefield under your control provides no further benefit.').
card_ruling('tomb hex', '2010-03-01', 'The effect of this spell’s landfall ability replaces its normal effect. If you had a land enter the battlefield under your control this turn, only the landfall-based effect happens.').

card_ruling('tomb of the spirit dragon', '2014-09-20', 'Count the number of colorless creatures you control (including face-down creatures) as the last ability resolves to determine how much life you gain.').

card_ruling('tomb of urami', '2005-06-01', 'Activating Tomb of Urami\'s second ability requires sacrificing all lands you control, including Tomb of Urami.').

card_ruling('tombstalker', '2013-06-07', 'Delve reduces only the generic mana requirement of the spell.').
card_ruling('tombstalker', '2013-06-07', 'If you sacrifice permanents or discard cards while casting the spell (for example, activating the mana ability of Blood Pet), those cards can be exiled using delve to reduce the spell\'s cost.').

card_ruling('tombstone stairwell', '2004-10-04', 'All the tokens are put onto the battlefield simultaneously.').
card_ruling('tombstone stairwell', '2004-10-04', 'Both the cumulative upkeep and the triggered ability trigger at the beginning of upkeep, so you can choose what order they resolve.').
card_ruling('tombstone stairwell', '2004-10-04', 'You control the triggered ability, but each player controls the token creature they put onto the battlefield due to the ability\'s effect.').
card_ruling('tombstone stairwell', '2004-10-04', 'Does not put the Zombies onto the battlefield if this card is not still on the battlefield when its ability resolves.').
card_ruling('tombstone stairwell', '2004-10-04', 'If something changes the names of the tokens, then the \"at end of turn\" ability will still destroy them.').
card_ruling('tombstone stairwell', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('tome scour', '2009-10-01', 'If there are fewer than five cards in the targeted player\'s library, that player puts all the cards from his or her library into his or her graveyard.').

card_ruling('tomorrow, azami\'s familiar', '2005-02-01', 'Tomorrow\'s ability makes you look at the top three cards, not draw them. Therefore it won\'t trigger abilities that trigger when cards are drawn.').
card_ruling('tomorrow, azami\'s familiar', '2005-02-01', 'Having multiple Tomorrows on the battlefield does nothing extra.').
card_ruling('tomorrow, azami\'s familiar', '2005-02-01', 'If you would draw two cards, you look at the top three cards of your library, keep one, and put the rest on the bottom of your library in any order. Then you look at the new top three cards of your library, keep one, and put the rest on the bottom of your library in any order.').
card_ruling('tomorrow, azami\'s familiar', '2005-02-01', 'If you have fewer than three cards in your library, you keep one and put the rest back. If you have no cards in your library, you do nothing.').
card_ruling('tomorrow, azami\'s familiar', '2005-02-01', 'Since Tomorrow replaces all your card drawing with looking at cards, you can\'t lose the game by being required to draw more cards than are in your library with Tomorrow on the battlefield.').

card_ruling('tooth of ramos', '2004-10-04', 'You can activate the sacrifice ability while this card is tapped.').

card_ruling('tooth, claw, and tail', '2010-06-15', 'You may choose zero, one, two, or three targets.').

card_ruling('topan ascetic', '2008-10-01', 'Since Topan Ascetic\'s activated ability doesn\'t have a tap symbol in its cost, you can tap a creature (including Topan Ascetic itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('topan freeblade', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('topan freeblade', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('topan freeblade', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('topple', '2004-10-04', 'It can only target a creature with the greatest power. This means you pick such a creature on announcement, and if that creature is not still the one with greatest power on resolution, then this spell is countered since its target is illegal.').

card_ruling('torch song', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('torchling', '2007-02-01', 'When the third ability resolves, you must change the target if able. If you can\'t choose a legal target for the spell when this ability resolves, that spell\'s target doesn\'t change.').
card_ruling('torchling', '2007-02-01', 'If a spell targets multiple things, you can\'t target it with Torchling\'s third ability, even if all but one of those targets has become illegal.').
card_ruling('torchling', '2007-02-01', 'If a spell has multiple targets, but it\'s targeting Torchling with all of them (such as Seeds of Strength targeting Torchling three times), you can target that spell with Torchling\'s third ability. In that case, you change all of those targets to the same new target, as long as it meets each of the spell\'s targeting conditions.').

card_ruling('tormented hero', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('tormented hero', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('tormented hero', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('tormented thoughts', '2014-04-26', 'The targeted player will discard a number of cards equal to the power of the sacrificed creature as it last existed on the battlefield.').
card_ruling('tormented thoughts', '2014-04-26', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('tormented thoughts', '2014-04-26', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').
card_ruling('tormented thoughts', '2015-06-22', 'You must sacrifice exactly one creature to cast Tormented Thoughts. You can’t cast it without sacrificing a creature, and you can’t sacrifice additional creatures.').
card_ruling('tormented thoughts', '2015-06-22', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrifice to stop you from casting this spell.').

card_ruling('tormenting voice', '2015-02-25', 'Because discarding a card is an additional cost, you can’t cast Tormenting Voice if you have no other cards in hand.').

card_ruling('tormentor\'s trident', '2012-05-01', 'You still decide which player or planeswalker the equipped creature attacks.').
card_ruling('tormentor\'s trident', '2012-05-01', 'If, during your declare attackers step, the equipped creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under your control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, you\'re not forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('tornado elemental', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('tornado elemental', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('tornado elemental', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('tornado elemental', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('torpor orb', '2011-06-01', 'Torpor Orb stops a creature\'s own enters-the-battlefield triggered abilities as well as other triggered abilities that would trigger when a creature enters the battlefield.').
card_ruling('torpor orb', '2011-06-01', 'The trigger event doesn\'t have to specify \"creatures\" entering the battlefield. For example, Amulet of Vigor says \"Whenever a permanent enters the battlefield tapped and under your control, untap it.\" If a creature enters the battlefield tapped and under your control, Amulet of Vigor would not trigger. If a land (that isn\'t also a creature) enters the battlefield tapped and under your control, Amulet of Vigor would trigger.').
card_ruling('torpor orb', '2011-06-01', 'Abilities that create replacement effects, such as a permanent entering the battlefield tapped or with counters on it, are unaffected.').
card_ruling('torpor orb', '2011-06-01', 'Abilities that apply \"as [this creature] enters the battlefield,\" such as choosing a creature to copy with Clone, are unaffected.').
card_ruling('torpor orb', '2011-06-01', 'Look at the permanent as it exists on the battlefield, taking into account continuous effects, to determine whether any triggered abilities will trigger. For example, if you control March of the Machines, which says, in part, \"Each noncreature artifact is an artifact creature,\" Torpor Orb will be a creature when it enters the battlefield and will not cause triggered abilities to trigger.').
card_ruling('torpor orb', '2011-06-01', 'If Torpor Orb and a creature enter the battlefield at the same time, that creature entering the battlefield won\'t cause triggered abilities to trigger.').

card_ruling('torrent elemental', '2014-11-24', '“Defending player” refers to the player Torrent Elemental is attacking or the controller of the planeswalker Torrent Elemental is attacking. In multiplayer formats that allow attacking multiple players, creatures controlled by other players won’t be tapped, even if you control other creatures attacking those players or planeswalkers those players control. In a Two-Headed Giant game, choose one member of the opposing team as the triggered ability resolves.').

card_ruling('torrent of lava', '2004-10-04', 'If X is zero, it deals no damage (not -1 damage) to non-flying creatures it taps.').
card_ruling('torrent of lava', '2008-08-01', 'It grants the activated ability to creatures while on the stack. This allows the ability to be activated and resolved before the Torrent itself resolves.').

card_ruling('torrent of souls', '2008-05-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('torrent of souls', '2008-05-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('torrent of souls', '2008-05-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('torrent of souls', '2008-05-01', 'When you cast the spell, you choose its targets before you pay for it.').
card_ruling('torrent of souls', '2008-05-01', 'The effects of the spell happen in order. If you spent both {B} and {R} to cast Torrent of Souls, first you\'ll return a creature card from your graveyard to the battlefield, then all creatures the target player controls -- including the one you just returned if that player is you -- will get +2/+0 and gain haste until end of turn.').

card_ruling('torrent of stone', '2005-02-01', 'You can sacrifice any two lands that have the subtype Mountain to splice Torrent of Stone onto an Arcane spell, not just lands named Mountain.').
card_ruling('torrent of stone', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('torrent of stone', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('torrent of stone', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('torrent of stone', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('torture', '2008-05-01', 'The -1/-1 counters that are put on the enchanted creature are independent of Torture. If Torture leaves the battlefield or is moved to another creature, the counters will stay put.').

card_ruling('tortured existence', '2004-10-04', 'You can\'t return the same card you are discarding. This is because you pick the target before paying the costs.').

card_ruling('toshiro umezawa', '2005-02-01', 'You must still pay the mana cost of the target instant in your graveyard to cast it using Toshiro\'s ability.').
card_ruling('toshiro umezawa', '2011-01-01', 'Creatures destroyed by an instant that you cast normally can cause a trigger that allows the instant to be cast again. For example, cast Terror on an opponent\'s creature. The creature is destroyed, triggering Toshiro\'s ability, and then the Terror is put into your graveyard. You can put Toshiro\'s ability on the stack targeting the Terror and then recast it when the ability resolves.').

card_ruling('total war', '2004-10-04', 'Destroys creatures as a triggered ability on the declaration of attackers.').

card_ruling('touch of darkness', '2004-10-04', 'You can choose to target zero creatures.').

card_ruling('touch of moonglove', '2015-06-22', 'The controller of the creature dealt damage by the target creature is the one who loses 2 life, not the controller of the target creature.').

card_ruling('touch of the eternal', '2012-07-01', 'This change of life total counts as life gain or life loss, as appropriate. Other effects that interact with life gain or life loss will interact with this effect accordingly.').

card_ruling('touch of the void', '2015-08-25', 'A creature doesn’t necessarily have to be dealt lethal damage by Touch of the Void to be exiled. After being dealt damage, if it would die for any reason that turn, it’ll be exiled instead.').
card_ruling('touch of the void', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('touch of the void', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('touch of the void', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('touch of the void', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('touch of the void', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('tourach\'s chant', '2004-10-04', 'Only checks the type of the land as it is put onto the battlefield. If the land gets changed after it enters the battlefield (even by a continuous effect), the Chant causes no additional effect.').

card_ruling('tower above', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('tower above', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Tower Above is green even if you spend six white mana to cast it.').
card_ruling('tower above', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Tower Above has a converted mana cost of 6, even if you spend {G}{G}{G} to cast it.').
card_ruling('tower above', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Tower Above by spending {G}{G}{G}, {2}{G}{G}, {4}{G}, or {6}.').
card_ruling('tower above', '2008-05-01', 'When the creature affected by Tower Above attacks, you can target any creature on the battlefield with the triggered ability. However, if the creature you target can\'t block the creature affected by Tower Above (for example, because the attacking creature has flying and the other one doesn\'t, or because both creatures are controlled by the same player), the triggered ability does nothing. In that case, the creature you targeted is free to block whichever creature its controller chooses, or block no creatures at all.').

card_ruling('tower defense', '2013-01-24', 'Only creatures you control when Tower Defense resolves will get the bonuses. Creatures that come under your control later that turn will not.').

card_ruling('tower geist', '2011-01-22', 'If there\'s only one card in your library when Tower Geist enters the battlefield, you\'ll look at that card and put it into your hand.').

card_ruling('towering thunderfist', '2013-01-24', 'You must activate Towering Thunderfist\'s ability before you declare attackers (at the latest, during the beginning of combat step) in order to have it attack without becoming tapped.').

card_ruling('toxic deluge', '2013-10-17', 'The payment of life is an additional cost. You lose the life even if Toxic Deluge is countered.').

card_ruling('toxic iguanar', '2009-02-01', 'As long as at least one permanent you control is the specified color, the ability will \"work\" and grant this creature the bonus. Otherwise, it won\'t have the bonus.').
card_ruling('toxic iguanar', '2009-02-01', 'Whether Toxic Iguanar has deathtouch matters only when it deals damage (that is, at the point when its combat damage resolves, or at the point when a spell or ability that causes it to deal damage resolves). For example, if your only green permanent is a creature that receives lethal damage at the same time Toxic Iguanar deals damage to a creature, the deathtouch ability will still apply and the creature Toxic Iguanar dealt damage to will be destroyed.').

card_ruling('toxin sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('toxin sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('toymaker', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('tracker', '2004-10-04', 'Giving either creature first strike does not affect the ability.').
card_ruling('tracker', '2004-10-04', 'If this leaves the battlefield before its activated ability resolves, it will still deal damage to the targeted creature. On the other hand, if the targeted creature leaves the battlefield before the ability resolves, the ability will be countered and no damage will be dealt.').
card_ruling('tracker', '2009-10-01', 'You may have Tracker target itself with its own ability. If you do, Tracker will deal damage to itself equal to its power, then immediately do it again.').

card_ruling('tracker\'s instincts', '2011-01-22', 'You must put a creature card from among the revealed cards into your hand if one is there, even if you\'d rather put all the revealed cards into your graveyard.').

card_ruling('tragic arrogance', '2015-06-22', 'None of the chosen permanents are targets of Tragic Arrogance.').
card_ruling('tragic arrogance', '2015-06-22', 'If a permanent has more than one of the affected types, it can count for any of them. For example, you could choose an artifact creature as the artifact you’re sparing, another creature as the creature, and an enchantment creature as the enchantment. Similarly, you could choose an enchantment creature as both the creature and the enchantment that you’re sparing, even if the player controls another creature and/or another enchantment.').

card_ruling('trail of the mage-rings', '2012-06-01', 'Rebound\'s delayed triggered ability will let you cast the spell at the beginning of your next upkeep even if the game has left Trail of the Mage-Rings by then.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('trail of the mage-rings', '2013-04-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('trail of the mage-rings', '2013-04-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('trail of the mage-rings', '2013-04-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('trail of the mage-rings', '2013-04-15', 'Multiple instances of rebound on the same spell are redundant.').
card_ruling('trail of the mage-rings', '2013-04-15', 'The rebound effect is not optional. Each instant and sorcery spell you cast from your hand is exiled instead of being put into your graveyard as it resolves, whether you want it to be or not. Casting the spell during your next upkeep is optional, however.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If a spell moves itself into another zone as part of its resolution (as Arc Blade, All Suns\' Dawn, and Beacon of Unrest do), rebound won\'t get a chance to apply.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If a spell you cast from your hand has both rebound and buyback (and the buyback cost was paid), you choose which effect to apply as it resolves.').
card_ruling('trail of the mage-rings', '2013-04-15', 'You\'ll be able to cast a spell with flashback three times this way. First you can cast it from your hand. It will be exiled due to rebound as it resolves. Then you can cast it from exile due to rebound\'s delayed triggered ability. It will be put into your graveyard as it resolves. Then you can cast it from your graveyard due to flashback. It will be exiled due to flashback as it resolves.').
card_ruling('trail of the mage-rings', '2013-04-15', 'For the rebound effect to happen, Trail of the Mage-Rings needs to be on the battlefield as the spell _finishes_ resolving. For example, if you cast Warp World from your hand, and as part of its resolution it puts Trail of the Mage-Rings onto the battlefield, Warp World will rebound. Conversely, if Warp World shuffles your Trail of the Mage-Rings into your library as part of its resolution, and doesn\'t put another one onto the battlefield, it will not rebound.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast an instant or sorcery spell from your hand and it\'s exiled due to rebound, the delayed triggered ability will allow you to cast it during your next upkeep even if Trail of the Mage-Rings has left the battlefield by then.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast a card from exile \"without paying its mana cost,\" you can\'t pay any alternative costs. Any X in the mana cost will be 0. On the other hand, if the card has optional additional costs (such as kicker or multikicker), you may pay those when you cast the card. If the card has mandatory additional costs (such as Momentous Fall does), you must pay those if you choose to cast the card.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If a spell has restrictions on when it can be cast (for example, \"Cast [this spell] only during the declare blockers step\"), those restrictions may prevent you from casting it from exile during your upkeep.').
card_ruling('trail of the mage-rings', '2013-04-15', 'If you cast a spell using the madness or suspend abilities, you\'re casting it from exile, not from your hand. Although those spells will have rebound, the ability won\'t have any effect.').
card_ruling('trail of the mage-rings', '2013-04-15', 'Similarly, if you gain control of an instant or sorcery spell with Commandeer, it will have rebound, but the ability won\'t do anything because that spell wasn\'t cast from your hand.').

card_ruling('training drone', '2011-06-01', 'If Training Drone has been declared as an attacking or blocking creature, and it stops being equipped later in combat, it won\'t stop attacking or blocking.').

card_ruling('training grounds', '2010-06-15', 'Training Grounds won\'t affect a cost that isn\'t the cost to activate a creature\'s activated ability. For example, it won\'t affect Flameblast Dragon\'s {X}{R} cost, since that\'s a cost paid when a triggered ability resolves, and it won\'t affect a kicker cost, since that\'s an additional cost to cast a spell. Activated ability costs appear before a colon (:) in a card\'s rules text, or, in the case of some keywords, before a colon in reminder text.').
card_ruling('training grounds', '2010-06-15', 'Training Grounds affects only creatures you control on the battlefield. The costs of activated abilities that work in other zones (such as cycling or unearth) won\'t be reduced.').
card_ruling('training grounds', '2010-06-15', 'Training Grounds won\'t affect the part of an activation cost represented by colored mana symbols or snow mana symbols. It also won\'t affect nonmana parts of an activation cost, if there are any.').
card_ruling('training grounds', '2010-06-15', 'Training Grounds can reduce the part of an activation cost represented by generic mana symbols down to nothing, as long as it still costs at least one mana. For example, if an activation cost is {2}{G}, you\'d have to pay only {G}. If an activation cost is {2}, though, you\'d still have to pay {1}.').
card_ruling('training grounds', '2010-06-15', 'Training Grounds can reduce the amount you pay for a creature\'s activated ability cost that includes {X}. For example, Drana, Kalastria Bloodchief has an activated ability that costs {X}{B}{B}. If you control Training Grounds and you activate the ability with X equal to 5, you\'ll have to pay only {3}{B}{B}. This is true even if the ability states that {X} must be paid with a certain color of mana, as Crimson Hellkite\'s ability does.').
card_ruling('training grounds', '2010-06-15', 'If an activated ability of a creature you control costs no generic mana to activate (for example, if it costs {R}{R}, it costs {0}, or it costs only nonmana actions such as {T} or \"Sacrifice a creature\"), Training Grounds simply won\'t affect it. In particular, it won\'t increase the cost to include a mana payment of {1}.').
card_ruling('training grounds', '2010-06-15', 'Training Grounds takes the total cost to activate a creature\'s activated ability into account, not just the cost printed on it. For example, Furnace Spirit has an activated ability that costs {R}, and Suppression Field says \"Activated abilities cost {2} more to activate unless they\'re mana abilities.\" Since activating Furnace Spirit\'s activated ability would now cost {2}{R}, Training Grounds reduces that cost back to {R}.').
card_ruling('training grounds', '2010-06-15', 'You may choose not to apply Training Ground\'s cost reduction effect. You may also choose to apply only part of it (causing an activated ability of a creature you control to cost just {1} less to activate).').

card_ruling('trait doctoring', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('trait doctoring', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('trait doctoring', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('trait doctoring', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('trait doctoring', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('trait doctoring', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('trait doctoring', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('trait doctoring', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('trait doctoring', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('trait doctoring', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('trait doctoring', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').
card_ruling('trait doctoring', '2013-04-15', 'Trait Doctoring changes the color word or basic land type each time it appears in the permanent\'s type line and/or rules text. It doesn\'t change the name of the card or any instances of the word being used as part of a card\'s name.').
card_ruling('trait doctoring', '2013-04-15', 'You choose what word you\'re changing and what word you\'re changing it to as Trait Doctoring resolves.').
card_ruling('trait doctoring', '2013-04-15', 'You can target any permanent with Trait Doctoring, including one with no color words or basic land types on it.').
card_ruling('trait doctoring', '2013-04-15', 'Trait Doctoring\'s effect changes only the text printed on the permanent. It can\'t change words found in abilities it\'s been granted. For example, if you\'ve changed “green” to “blue,” and the permanent gains protection from green (as opposed to having protection from green printed on it), that protection ability is unaffected. The permanent will have protection from green.').
card_ruling('trait doctoring', '2013-04-15', 'If the basic land type of a land is changed, the associated mana ability of that land will also change, but its name will not. For example, if you\'ve changed “Plains” to “Island,” a card named Plains will have the basic land type Island and can be tapped for {U}, and it can no longer be tapped for {W}.').
card_ruling('trait doctoring', '2013-04-15', 'The type-changing effect can change part of a word such as “nonblack” or “swampwalk” if the part of the word is being used to refer to a color or basic land type.').

card_ruling('traitor\'s roar', '2008-05-01', 'If you use conspire to copy Traitor\'s Roar, but you don\'t change its target, the copy will resolve just fine but the original will be countered on resolution. That\'s because the copy will resolve first, and as part of its resolution, it will tap the targeted creature. Then, when the original Traitor\'s Roar tries to resolve, it will have an illegal target (since it must target an untapped creature).').

card_ruling('traitorous blood', '2011-09-22', 'Traitorous Blood can target any creature, even one that\'s tapped or one you already control.').
card_ruling('traitorous blood', '2011-09-22', 'Gaining control of a creature doesn\'t cause you gain control of any Auras or Equipment attached to it.').

card_ruling('traitorous instinct', '2010-06-15', 'You may target a creature you already control. You may target a creature that\'s already untapped.').

card_ruling('tranquil thicket', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('transcendence', '2004-10-04', 'If your life total is 20 or more and you control an effect that states \"you can\'t lose the game\", then an infinite loop is formed. If neither player wants to end the loop by removing one of these two cards from the battlefield, the game ends in a draw.').

card_ruling('transcendent master', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('transcendent master', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('transcendent master', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('transcendent master', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('transcendent master', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('transcendent master', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('transgress the mind', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('transgress the mind', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('transgress the mind', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('transgress the mind', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('transgress the mind', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('transguild courier', '2006-05-01', 'Transguild Courier appears in a multicolored card frame, but it\'s still an artifact.').
card_ruling('transguild courier', '2011-09-22', 'This card is white, blue, black, red, and green in all zones. It is multicolored.').
card_ruling('transguild courier', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('transluminant', '2005-10-01', 'If you use this ability during a turn\'s end phase, the chance to put \"at end of turn\"-triggered abilities on the stack has passed. You won\'t get the Spirit creature token until the beginning of the next end step.').

card_ruling('transmogrifying licid', '2004-10-04', 'If the permanent it is on was only a creature due to an effect, when that effect ends, the Licid will find itself enchanting something illegal and be put into the graveyard.').
card_ruling('transmogrifying licid', '2008-04-01', 'The enchanted creature gains the artifact type in addition to any other card types it may already have.').
card_ruling('transmogrifying licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('transmogrifying licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').
card_ruling('transmogrifying licid', '2013-09-20', 'When the activated ability resolves, Transmogrifying Licid becomes an Aura enchantment and ceases to be either a creature or an artifact. Since it is no longer a creature, it also loses the creature type Licid. When the effect is ended, it goes back to being an artifact creature with the creature type Licid and ceases to be an Aura enchantment.').

card_ruling('transmutation', '2005-11-01', 'Note that the wording \"Effects that alter the creature\'s power alter its toughness instead, and vice versa, this turn\" has been removed.').
card_ruling('transmutation', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('transmutation', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('transmute artifact', '2004-10-04', 'Additional mana spent to cover the differences in mana costs is not part of the mana cost of this spell. It is spent during spell resolution.').
card_ruling('transmute artifact', '2004-10-04', 'The one from the library enters the battlefield when the spell is resolved, and this does not count as the casting of an artifact.').
card_ruling('transmute artifact', '2004-10-04', 'You can\'t sacrifice more than one artifact to this spell.').
card_ruling('transmute artifact', '2004-10-04', 'If the chosen artifact costs less than or the same as the sacrificed artifact, you simply put it onto the battlefield without paying any additional costs.').
card_ruling('transmute artifact', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('transmute artifact', '2011-01-01', 'The sacrifice of an artifact is an effect of the spell, not a cost, which means it happens as the spell resolves. If Transmute Artifact is countered, you do not have to sacrifice an artifact.').

card_ruling('trap essence', '2014-09-20', 'You don’t have to target a creature to cast Trap Essence, although you must target a creature spell on the stack.').

card_ruling('trap runner', '2004-10-04', 'If the ability is used on a creature with Trample, the damage still comes through.').
card_ruling('trap runner', '2004-10-04', 'This ability will trigger any creature\'s \"whenever this becomes blocked\" ability.').
card_ruling('trap runner', '2013-09-20', 'If a turn has multiple combat phases, the ability can be activated during any of them as long as it\'s after the beginning of that phase\'s Declare Blockers Step.').

card_ruling('trapjaw kelpie', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('trapjaw kelpie', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('trapjaw kelpie', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('trapjaw kelpie', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('trapjaw kelpie', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('trapjaw kelpie', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('trapjaw kelpie', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('traumatic visions', '2009-02-01', 'Unlike the normal cycling ability, basic landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a basic land card. You don\'t choose the type of basic land card you\'ll find until you\'re performing the search. After you choose a basic land card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('traumatic visions', '2009-02-01', 'Basic landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being basic landcycled. Any ability that stops a cycling ability from being activated also stops a basic landcycling ability from being activated.').
card_ruling('traumatic visions', '2009-02-01', 'Basic landcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with basic landcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('traumatic visions', '2009-02-01', 'You can choose not to find a basic land card, even if there is one in your library.').

card_ruling('traumatize', '2004-10-04', 'The player can put the cards in their graveyard in any order they choose.').

card_ruling('travel preparations', '2011-09-22', 'You can\'t target the same creature twice to put two +1/+1 counters on it.').
card_ruling('travel preparations', '2011-09-22', 'If Travel Preparations targets two creatures, and one of them is an illegal target by the time Travel Preparations resolves, you\'ll still put a +1/+1 counter on the other creature.').

card_ruling('traveler\'s cloak', '2004-10-04', 'Can affect basic or non-basic types, but it must be for a specific type. The chosen type must be an existing land type. See the glossary of the comprehensive rulebook for more details on existing land types.').

card_ruling('traveling plague', '2004-10-04', 'A counter is placed on every player\'s upkeep, not just the controller\'s upkeep.').
card_ruling('traveling plague', '2004-10-04', 'The controller of the dead creature chooses the new creature.').
card_ruling('traveling plague', '2005-08-01', 'When the enchanted creature leaves the battlefield, this card is placed into the graveyard as is normal for an Aura when it loses what it enchants. The triggered ability returns this card to the battlefield with no counters on it.').
card_ruling('traveling plague', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('treacherous urge', '2007-02-01', 'If you don\'t control the creature when the ability resolves, the creature won\'t be sacrificed. It will have haste permanently.').
card_ruling('treacherous urge', '2007-02-01', 'If you cast Treacherous Urge during the End step, you won\'t sacrifice the creature until the end of the following turn.').

card_ruling('treacherous werewolf', '2004-10-04', 'The Threshold ability can\'t count this card (soon to be in the graveyard) when checking for Threshold.').

card_ruling('treachery', '2004-10-04', 'You can untap 0 to 5 lands.').
card_ruling('treachery', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('treachery', '2004-10-04', 'This does not target the lands.').

card_ruling('treasonous ogre', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('treasonous ogre', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('treasonous ogre', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('treasonous ogre', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('treasure cruise', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('treasure cruise', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('treasure cruise', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('treasure cruise', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('treasure cruise', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('treasure hunt', '2010-03-01', 'The cards you put into your hand this way include one nonland card, plus all the land cards on top of it in your library (if any). If the top card of your library is a nonland card, you just get that one.').
card_ruling('treasure hunt', '2010-03-01', 'If all the cards left in your library are lands, you\'ll reveal all of them and put them all into your hand.').

card_ruling('treasure mage', '2011-06-01', 'If a card in your library has X in its mana cost, X is considered to be 0.').

card_ruling('treasury thrull', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('treasury thrull', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('treasury thrull', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('tree of redemption', '2011-09-22', 'When the ability resolves, Tree of Redemption\'s toughness will become your former life total and you will gain or lose an amount of life necessary so that your life total equals Tree of Redemption\'s former toughness. Other effects that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('tree of redemption', '2011-09-22', 'Any toughness-modifying effects, counters, Auras, or Equipment will apply after its toughness is set to your former life total. For example, say Tree of Redemption is enchanted with Spectral Flight (which makes it 2/15) and your life total is 7. After the exchange, Tree of Redemption would be a 2/9 creature (its toughness became 7, which was then modified by Spectral Flight) and your life total would be 15.').
card_ruling('tree of redemption', '2011-09-22', 'If Tree of Redemption isn\'t on the battlefield when the ability resolves, the exchange can\'t happen and the ability will have no effect.').

card_ruling('treefolk seedlings', '2004-10-04', 'The toughness is continuously calculated.').

card_ruling('treetop bracers', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) don\'t actually have Flying, so they can\'t block a creature enchanted with this.').

card_ruling('treetop rangers', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) don\'t actually have Flying, so they can\'t block this.').

card_ruling('treetop scout', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) don\'t actually have Flying, so they can\'t block this.').

card_ruling('treetop village', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('treetop village', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('trench gorger', '2011-09-22', 'If you choose not to search, Trench Gorger will simply be 6/6.').
card_ruling('trench gorger', '2011-09-22', 'While Trench Gorger\'s ability is on the stack, it\'s on the battlefield as a 6/6.').
card_ruling('trench gorger', '2011-09-22', 'If Trench Gorger leaves the battlefield before its triggered ability resolves, you may still search your library.').
card_ruling('trench gorger', '2011-09-22', 'After the ability resolves, Trench Gorger\'s power and toughness won\'t change if one of the exiled cards leaves exile.').

card_ruling('trepanation blade', '2011-09-22', 'The land card is counted when calculating the bonus, and it will be put into the graveyard with the other revealed cards.').
card_ruling('trepanation blade', '2011-09-22', 'If the equipped creature is attacking a planeswalker in any game except a Two-Headed Giant game, the controller of the planeswalker is the defending player.').
card_ruling('trepanation blade', '2011-09-22', 'If the equipped creature is attacking in a Two-Headed Giant game, the controller of the attacking creature chooses one of the two defending players, even if the creature is attacking a planeswalker.').

card_ruling('tresserhorn skyknight', '2006-07-15', 'Damage that would be dealt to Tresserhorn Skyknight by creatures with double strike is dealt as normal.').
card_ruling('tresserhorn skyknight', '2006-07-15', 'All damage (not just combat damage and not just first-strike combat damage) that would be dealt to Tresserhorn Skyknight by creatures with first strike is prevented.').

card_ruling('treva\'s ruins', '2004-10-04', 'If you don\'t want to unsummon a land, you can play this card then tap it for mana before the enters the battlefield ability resolves. You may then choose to sacrifice it instead of unsummoning a land.').
card_ruling('treva\'s ruins', '2005-08-01', 'This land is of type \"Lair\" only; other subtypes have been removed. It is not a basic land.').

card_ruling('treva, the renewer', '2004-10-04', 'You choose the color during resolution. This means your opponent does not get to react after knowing the color you chose.').

card_ruling('triad of fates', '2013-09-15', 'The creature that returns to the battlefield during the resolution of Triad of the Fate’s middle ability does so as a new object, with no memory of its previous existence. It won’t have a fate counter on it.').

card_ruling('triangle of war', '2004-10-04', 'If either target is illegal when the ability resolves, it will do nothing.').

card_ruling('triassic egg', '2004-10-04', 'You can use the sacrifice ability even if it is tapped.').
card_ruling('triassic egg', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('tribal flames', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('tribal flames', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('tribal flames', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('tribal forcemage', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('tribute to hunger', '2011-09-22', 'The amount of life you gain is equal to the creature\'s toughness as it last existed on the battlefield.').

card_ruling('trickbind', '2006-09-25', 'If Trickbind counters an activated ability of a permanent, no activated abilities of that permanent can be activated for the remainder of the turn. This includes activated abilities other than the one that was countered, as well as any mana abilities that permanent may have. Other abilities of that permanent, including triggered abilities and static abilities, are unaffected.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can be used to counter the storm triggered ability. Doing so prevents the copies from being created, but doesn\'t counter the original storm spell.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can be used to counter the madness triggered ability. The affected card will remain exiled.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can be used to counter the suspend \"remove a counter\" ability. If so, the counter won\'t get removed, but the ability will trigger again as normal later on. Trickbind can also be used to counter the suspend \"play this card\" ability. If so, the card stays exiled (the ability won\'t trigger again). Using suspend to exile a card in your hand is neither an activated nor a triggered ability, so it can\'t be countered this way.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can be used to counter the echo triggered ability. If so, the echo ability won\'t trigger again so the echo cost is never paid.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can be used to counter the flanking triggered ability.').
card_ruling('trickbind', '2006-09-25', 'Trickbind can\'t counter or prevent paying the morph cost to turn a face-down creature face up, since that action doesn\'t use the stack.').
card_ruling('trickbind', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('trickbind', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('trickbind', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('trickbind', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('trickbind', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('trickbind', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').
card_ruling('trickbind', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('trigon of infestation', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('trigon of infestation', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('trigon of infestation', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('trigon of infestation', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('trigon of infestation', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('trigon of infestation', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('trinisphere', '2004-12-01', 'Trinisphere\'s ability affects the total cost of the spell. It is applied *after* any other cost increasers or cost reducers are applied: First apply any cost increases. Next apply any cost reducers. Finally look at the amount of mana you have to pay. If it\'s less than three mana, you\'ll pay three mana.').
card_ruling('trinisphere', '2004-12-01', 'Even with a cost reducer on the battlefield, spells can\'t cost less than three mana to cast.').
card_ruling('trinisphere', '2004-12-01', 'If a spell costs at least three mana due to additional costs, such as kicker costs, that\'s fine.').
card_ruling('trinisphere', '2004-12-01', 'You still need to pay any additional nonmana costs the spell has, such as sacrificing a creature or discarding cards.').
card_ruling('trinisphere', '2004-12-01', 'Casting a creature with morph face down already costs three mana, even though the converted mana cost of the face-down spell is zero, so Trinisphere normally doesn\'t modify the total cost of a face-down creature spell. However, if Dream Chisel is reducing that cost while Trinisphere is on the battlefield, you\'ll still have to pay three mana for the spell.').

card_ruling('trinket mage', '2011-01-01', 'If the mana cost of a card in your library includes {X}, X is considered to be 0.').

card_ruling('triplicate spirits', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('triplicate spirits', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('triplicate spirits', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('triplicate spirits', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('triplicate spirits', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('triplicate spirits', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('triplicate spirits', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('triskelion', '2010-08-15', 'If you activate the ability of an attacking or blocking Triskelion during the declare blockers step, it will deal less combat damage as a result. If you wait until the combat damage step, but Triskelion is dealt lethal damage, it\'ll be destroyed before you get a chance to activate its ability.').
card_ruling('triskelion', '2010-08-15', 'If Triskelion has damage marked on it, activating its ability may cause that damage to become lethal. For example, say a Triskelion with two +1/+1 counters on it has been dealt 2 damage earlier in the turn. If you activate its ability by removing a +1/+1 counter from it, it now has 2 toughness and is destroyed as a state-based action. You won\'t be able to remove the other +1/+1 counter from it to activate its ability again. The first activation of the ability will still resolve.').

card_ruling('triton cavalry', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('triton cavalry', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('triton cavalry', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('triton fortune hunter', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('triton fortune hunter', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('triton fortune hunter', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('triumph of cruelty', '2012-05-01', 'Triumph of Cruelty\'s ability will trigger at the beginning of each of your upkeeps regardless of who controls the creature with the greatest power. The ability checks whether the opponent will discard a card when it resolves.').

card_ruling('triumph of ferocity', '2012-05-01', 'Triumph of Ferocity\'s ability will trigger at the beginning of each of your upkeeps regardless of who controls the creature with the greatest power. The ability checks whether you\'ll draw a card when it resolves.').

card_ruling('triumph of the hordes', '2011-06-01', 'Multiple instances of infect are redundant.').

card_ruling('trollhide', '2011-09-22', 'Activating the ability granted to the enchanted creature causes a \"regeneration shield\" to be created for it. The next time that creature would be destroyed that turn, the regeneration shield is used up instead. This works only if the creature is dealt lethal damage, dealt damage from a source with deathtouch, or affected by a spell or ability that says to \"destroy\" it. Other effects that cause the creature to be put into the graveyard (such as reducing its toughness to 0 or sacrificing it) don\'t destroy it, so regeneration won\'t save it. If it hasn\'t been used, the regeneration shield goes away as the turn ends.').
card_ruling('trollhide', '2011-09-22', 'To work, the regeneration shield must be created before the enchanted creature is destroyed. This usually means activating its ability during the declare blockers step, or in response to a spell or ability that would destroy it.').
card_ruling('trollhide', '2011-09-22', 'If the enchanted creature is dealt lethal damage and is dealt damage by a source with deathtouch during the same combat damage step, a single regeneration shield will save it.').
card_ruling('trollhide', '2013-07-01', 'Trollhide needs to be attached to the creature as the ability that creates the regeneration shield is activated, but after that, the regeneration shield will be created and remain in effect even if Trollhide is no longer attached to the creature.').

card_ruling('tromokratis', '2014-02-01', 'The defending player may block Tromokratis with each creature he or she controls. If that player can’t, perhaps because one of those creatures is tapped, or chooses not to, Tromokratis can’t be blocked.').
card_ruling('tromokratis', '2014-02-01', 'Tromokratis (like all attacking and blocking creatures) remains an attacking or blocking creature until the end of combat step is over. Notably, it will still be an attacking or blocking creature during the combat damage step, so any abilities an opponent controls that trigger when a creature dies as a result of combat damage could target Tromokratis.').

card_ruling('tromp the domains', '2006-09-25', 'The creatures get trample once; they don\'t get trample for each basic land type.').
card_ruling('tromp the domains', '2006-09-25', 'The effect counts basic land *types*, not the number of basic lands. Therefore the maximum bonus is +5/+5.').
card_ruling('tromp the domains', '2006-09-25', 'The affected creatures and the power/toughness bonus are fixed when Tromp the Domains resolves.').
card_ruling('tromp the domains', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('tromp the domains', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('tromp the domains', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('trophy hunter', '2005-10-01', 'The second ability checks whether a creature being put into a graveyard (a) currently has flying and (b) was dealt damage earlier this turn by Trophy Hunter. It doesn\'t matter whether Trophy Hunter dealt the damage that caused the creature to be destroyed, whether the damage from Trophy Hunter was dealt in combat or via its activated ability, or who controlled the creature.').

card_ruling('tropical island', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('tropical island', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('trostani\'s judgment', '2012-10-01', 'You must target a creature to cast Trostani\'s Judgment. If that creature is an illegal target when Trostani\'s Judgment tries to resolve, it will be countered and none of its effects will happen. You won\'t populate.').
card_ruling('trostani\'s judgment', '2012-10-01', 'If you choose a creature token you control as the target, it will be exiled before you populate and you won\'t be able to choose that creature to copy.').
card_ruling('trostani\'s judgment', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('trostani\'s judgment', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('trostani\'s judgment', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('trostani\'s judgment', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('trostani\'s judgment', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('trostani\'s judgment', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('trostani, selesnya\'s voice', '2012-10-01', 'Trostani\'s first ability checks the creature\'s toughness as it resolves. If that creature has left the battlefield, use its toughness from when it was last on the battlefield. You can\'t lose life this way if that creature\'s toughness was less than 0.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('trostani, selesnya\'s voice', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('trouble', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('trouble', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('trouble', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('trouble', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('trouble', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('trouble', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('trouble', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('trouble', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('trouble', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('trouble', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('trouble', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('trouble', '2013-04-15', 'If you cast Toil // Trouble as a fused split card and choose to target the same player twice, the two drawn cards are counted when determining how much damage is dealt. The player can\'t do anything between the effects of Toil and Trouble to have fewer cards in his or her hand.').

card_ruling('true-name nemesis', '2013-10-17', 'Protection from a player is a new variant of the protection ability. It means the following:\n-- True-Name Nemesis can’t be the target of spells or abilities controlled by the chosen player.\n-- True-Name Nemesis can’t be enchanted by Auras or equipped by Equipment controlled by the chosen player. (The same is true for Fortifications controlled by the chosen player, if True-Name Nemesis becomes a land.)\n-- True-Name Nemesis can’t be blocked by creatures controlled by the chosen player.\n-- All damage that would be dealt to True-Name Nemesis by sources controlled by the chosen player is prevented. (The same is true for sources owned by the chosen player that don’t have controllers.)').

card_ruling('trumpet blast', '2009-10-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('trumpet blast', '2009-10-01', 'Even though Trumpet Blast affects only attacking creatures, the bonus will remain for the rest of the turn.').
card_ruling('trumpet blast', '2014-09-20', 'Only creatures that are attacking as Trumpet Blast resolves will receive the bonus. In other words, casting it before you’ve declared attackers usually won’t do anything.').

card_ruling('trusted advisor', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Trusted Advisor onto the battlefield, your maximum hand size will be four. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('truth or tale', '2006-09-25', 'Here\'s how this works: Step 1: You reveal the top five cards of your library. Step 2: You separate those cards into two piles. (A pile can have zero cards.) Step 3: An opponent of your choice chooses one of those piles. Step 4: You choose one of the cards in the chosen pile. Step 5: You put the chosen card in your hand. Step 6: You put all other revealed cards (from both piles) on the bottom of your library in any order.').

card_ruling('tsabo tavoc', '2005-08-01', 'The destroy ability only works on legendary creatures, not on other legendary permanents.').

card_ruling('tsabo\'s web', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').
card_ruling('tsabo\'s web', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('tuktuk grunts', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('tuktuk scrapper', '2010-03-01', 'A token permanent that\'s destroyed is put into its owner\'s graveyard before it ceases to exist. If a token is destroyed by Tuktuk Scrapper\'s ability, Tuktuk Scrapper deals damage to that token\'s controller.').
card_ruling('tuktuk scrapper', '2013-07-01', 'If the targeted artifact hass indestructible or regenerates (or you choose not to destroy it), Tuktuk Scrapper doesn\'t deal damage to that artifact\'s controller. Similarly, if the targeted artifact is destroyed but a replacement effect moves it to a different zone instead of its owner\'s graveyard, Tuktuk Scrapper doesn\'t deal damage to that artifact\'s controller.').

card_ruling('tuktuk the explorer', '2010-06-15', 'Both Tuktuk the Explorer and Tuktuk the Returned are legendary creatures. However, since they have different names, one of each may coexist without being affected by the \"legend rule.\"').

card_ruling('tundra', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('tundra', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('tunnel ignus', '2011-01-01', 'Whenever a land enters the battlefield under an opponent\'s control, this ability triggers only if another land had already entered the battlefield under that opponent\'s control this turn. It doesn\'t matter if that other land is still on the battlefield, is still under that player\'s control, or is still a land.').
card_ruling('tunnel ignus', '2011-01-01', 'If multiple lands enter the battlefield under an opponent\'s control at the same time, Tunnel Ignus\'s ability triggers that many times, even if no other lands had entered the battlefield under that player\'s control earlier in the turn. That\'s because each ability takes into consideration the other land(s) that entered the battlefield at the same time as the one that caused it to trigger.').

card_ruling('tunnel vision', '2005-10-01', 'If the named card is found, everything from the top of the library down to the named card is put into the graveyard and the library isn\'t shuffled. If the named card isn\'t in the library, no cards are put into the graveyard and the library is shuffled.').

card_ruling('tunneling geopede', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('tunneling geopede', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('tunneling geopede', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('turf wound', '2004-10-04', 'Stops the player from playing a land, but not from putting a land onto the battlefield using a spell or ability.').

card_ruling('turn', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('turn', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('turn', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('turn', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('turn', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('turn', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('turn', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('turn', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('turn', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('turn', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('turn', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('turn', '2013-04-15', 'Turn will cause the creature lose all other colors and creature types, but it will retain any other card types (such as artifact) it may have.').
card_ruling('turn', '2013-04-15', 'Turn overwrites all previous effects that set the targeted creature\'s power or toughness to specific values. Effects that set its power or toughness to specific values that start to apply after Turn resolves will overwrite this effect.').
card_ruling('turn', '2013-04-15', 'Turn doesn\'t counter abilities that have already triggered or been activated. In particular, there is no way to cast this to stop a creature\'s “At the beginning of your upkeep” or “When this creature enters the battlefield” abilities from triggering.').
card_ruling('turn', '2013-04-15', 'If the affected creature gains an ability after Turn resolves, it will keep that ability.').
card_ruling('turn', '2013-04-15', 'Effects that modify the targeted creature\'s power or toughness, such as the effects of Phytoburst or Legion\'s Initiative, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('turn against', '2015-08-25', 'Turn Against can target any creature, even one that’s untapped or one you already control.').
card_ruling('turn against', '2015-08-25', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').
card_ruling('turn against', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('turn against', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('turn against', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('turn against', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('turn against', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('turn aside', '2011-01-01', 'Turn Aside can target a spell that has multiple targets, as long as at least one of those targets is a permanent you control.').

card_ruling('turn the tables', '2004-12-01', 'If the targeted creature isn\'t on the battlefield when the combat damage would be dealt, the damage isn\'t redirected.').
card_ruling('turn the tables', '2004-12-01', 'If the targeted creature has been removed from combat but is still on the battlefield, the damage will be redirected.').

card_ruling('turn the tide', '2011-06-01', 'Only creatures your opponents control when Turn the Tide resolves are affected. Creatures that enter the battlefield later in the turn are not.').

card_ruling('turn to frog', '2014-07-18', 'The creature will lose all other colors and creature types, but it will retain any other card types (such as artifact) or supertypes (such as legendary) it may have.').
card_ruling('turn to frog', '2014-07-18', 'Turn to Frog overwrites all previous effects that set the creature’s base power and toughness to specific values. Any power- or toughness-setting effects that start to apply after Turn to Frog resolves will overwrite this effect.').
card_ruling('turn to frog', '2014-07-18', 'Turn to Frog doesn’t counter abilities that have already triggered or been activated. In particular, there is no way to cast this spell to stop a creature’s ability that says “At the beginning of your upkeep,” “When this creature enters the battlefield,” or similar from triggering.').
card_ruling('turn to frog', '2014-07-18', 'If the affected creature gains an ability after Turn to Frog resolves, it will keep that ability.').
card_ruling('turn to frog', '2014-07-18', 'Effects that modify a creature’s power and/or toughness, such as the effect of Titanic Growth, will apply to the creature no matter when they started to take effect. The same is true for any counters that change its power and/or toughness and effects that switch its power and toughness.').
card_ruling('turn to frog', '2014-07-18', 'If one of the Theros block Gods is affected by Turn to Frog, it will be a legendary 1/1 blue Frog enchantment creature with no abilities. If it stops being a creature, perhaps because your devotion to its color(s) decreased, it will be a legendary blue enchantment with no abilities. The way continuous effects work, the God’s type-changing ability is applied before the effect that removes that ability is applied.').

card_ruling('turn to slag', '2011-01-01', 'You may target any creature, not just one with Equipment attached to it. The targeted creature will still be dealt damage even if no Equipment is attached to it.').
card_ruling('turn to slag', '2011-01-01', 'If the targeted creature is an illegal target by the time Turn to Slag resolves, the spell is countered. No Equipment is destroyed.').

card_ruling('turnabout', '2004-10-04', 'You decide on resolution if you are tapping or untapping, and which kind of permanent you are affecting.').

card_ruling('turntimber basilisk', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('turntimber basilisk', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('turntimber basilisk', '2009-10-01', 'You decide whether to have the targeted creature block Turntimber Basilisk this turn if able at the time the landfall ability resolves, not at the time Turntimber Basilisk attacks.').
card_ruling('turntimber basilisk', '2009-10-01', 'Deciding to have the targeted creature block doesn\'t force you to attack with Turntimber Basilisk that turn. If Turntimber Basilisk doesn\'t attack, the targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('turntimber basilisk', '2009-10-01', 'If you choose to have the targeted creature block but that creature isn\'t able to block Turntimber Basilisk (for example, because the targeted creature can block only creatures with flying), the requirement to block does nothing. The targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('turntimber basilisk', '2009-10-01', 'Tapped creatures, creatures that can\'t block as the result of an effect, creatures with unpaid costs to block (such as those from War Cadence), and creatures that aren\'t controlled by the defending player are exempt from effects that would require them to block. Such creatures can be targeted by Turntimber Basilisk\'s landfall ability, but the requirement to block does nothing.').
card_ruling('turntimber basilisk', '2009-10-01', 'If Turntimber Basilisk\'s landfall ability triggers multiple times during the same turn, you can have multiple creatures block it that turn if able.').

card_ruling('turntimber ranger', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('turri island', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('turri island', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('turri island', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('turri island', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('turri island', '2009-10-01', 'Turri Island\'s first ability can\'t reduce the cost to cast a creature spell to less than {0}.').
card_ruling('turri island', '2009-10-01', 'If there are fewer than three cards in your library as the {C} ability resolves, you\'ll reveal all the cards in your library.').

card_ruling('turtleshell changeling', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('turtleshell changeling', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('tuskguard captain', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('tuskguard captain', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('twiddle', '2004-10-04', 'This is not a toggle effect. If you use Twiddle to tap a card and before it takes effect your opponent taps it, Twiddle will not automatically untap the card.').
card_ruling('twiddle', '2004-10-04', 'The decision whether or not to tap or untap is made on resolution. This is not a modal spell.').

card_ruling('twilight drover', '2005-10-01', 'The first ability only cares that creature tokens leave the battlefield. It doesn\'t matter where they went.').
card_ruling('twilight drover', '2005-10-01', 'If a player leaves a multiplayer game, all creature tokens that player owns also leave the game. Twilight Drover\'s ability will trigger once per token.').
card_ruling('twilight drover', '2005-10-01', 'If Spirit tokens created by Twilight Drover leave the battlefield, they will trigger its first ability.').
card_ruling('twilight drover', '2005-10-01', 'Note that Twilight Drover doesn\'t have any way to sacrifice tokens or otherwise cause them to leave the battlefield.').

card_ruling('twilight shepherd', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('twilight shepherd', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('twilight shepherd', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('twilight shepherd', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('twilight shepherd', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('twilight shepherd', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('twilight shepherd', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('twilight\'s call', '2004-10-04', 'All the creature cards enter the battlefield simultaneously.').
card_ruling('twilight\'s call', '2004-10-04', 'For purposes of ordering any continuous effects of the creatures, the current player (not the controller of this spell) decides the ordering regardless of which player is putting the creatures onto the battlefield.').

card_ruling('twin bolt', '2015-02-25', 'You choose whether Twin Bolt has one target that will be dealt 2 damage or two targets that will each be dealt 1 damage as you cast Twin Bolt.').
card_ruling('twin bolt', '2015-02-25', 'If you cast Twin Bolt with two targets and one of those targets becomes illegal before Twin Bolt resolves, the remaining legal target will be dealt 1 damage. You can’t change the original division of damage.').

card_ruling('twincast', '2005-06-01', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs paid for the targeted spell are copied as though those same costs were paid for the copy too.').
card_ruling('twincast', '2005-06-01', 'Twincast copies any text spliced onto the targeted spell, but you can\'t splice additional text onto the copy.').
card_ruling('twincast', '2009-10-01', 'Twincast can target (and copy) any instant or sorcery spell, not just one with targets. It doesn\'t matter who controls it.').
card_ruling('twincast', '2009-10-01', 'As Twincast resolves, it creates a copy of a spell. That copy is created on the stack, so it\'s not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('twincast', '2009-10-01', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('twincast', '2009-10-01', 'If the spell Twincast copies is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('twincast', '2009-10-01', 'If the spell Twincast copies has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('twincast', '2009-10-01', 'If the copy says that it affects \"you,\" it affects the controller of the copy, not the controller of the original spell. Similarly, if the copy says that it affects an \"opponent,\" it affects an opponent of the copy\'s controller, not an opponent of the original spell\'s controller.').

card_ruling('twinflame', '2014-04-26', 'The tokens copy exactly what was printed on the original creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('twinflame', '2014-04-26', 'If the copied creature has {X} in its mana cost, X is considered to be 0.').
card_ruling('twinflame', '2014-04-26', 'If the copied creature is copying something else (for example, if the copied creature is a Clone), then the token enters the battlefield as whatever that creature copied.').
card_ruling('twinflame', '2014-04-26', 'If the copied creature is a token, the token created by Twinflame copies the original characteristics of that token as stated by the effect that put that token onto the battlefield.').
card_ruling('twinflame', '2014-04-26', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').
card_ruling('twinflame', '2014-04-26', 'The tokens see each other enter the battlefield. If any of them have a triggered ability that triggers whenever a creature enters the battlefield, they’ll trigger for one another.').
card_ruling('twinflame', '2014-04-26', 'If another creature becomes or enters the battlefield as a copy of the token, that creature will have haste, but you won’t exile it. However, if Twinflame creates multiple tokens copying a single creature due to a replacement effect (like the one Doubling Season creates), you’ll exile each of them.').
card_ruling('twinflame', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('twinflame', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('twinflame', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('twinflame', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('twinflame', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('twinning glass', '2007-10-01', 'Activating Twinning Glass\'s ability allows you to cast a single card from your hand as it resolves. It doesn\'t create a continuous effect, and it doesn\'t let you cast multiple cards for free.').
card_ruling('twinning glass', '2007-10-01', 'It doesn\'t matter who cast the first spell.').
card_ruling('twinning glass', '2007-10-01', 'You can\'t pay any alternative costs (such as those from evoke or morph) when casting the card. On the other hand, if the card has additional costs (such as those from kicker or buyback), you may pay those.').
card_ruling('twinning glass', '2007-10-01', 'If one half of a split card has been cast this turn, you may use Twinning Glass\'s ability to cast either half of a split card with that name. For example, if Dead from Dead/Gone has been cast, you may use Twinning Glass\'s ability to cast either Dead or Gone.').

card_ruling('twisted abomination', '2009-02-01', 'Unlike the normal cycling ability, Swampcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Swamp card. After you find a Swamp card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('twisted abomination', '2009-02-01', 'Swampcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Swampcycling this card. Any ability that stops a cycling ability from being activated also stops Swampcycling from being activated.').
card_ruling('twisted abomination', '2009-02-01', 'Swampcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Swampcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('twisted abomination', '2009-02-01', 'You can choose to find any card with the Swamp land type, including nonbasic lands. You can also choose not to find a card, even if there is a Swamp card in your library.').

card_ruling('twisted image', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('twisted image', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('twitch', '2010-03-01', 'If the targeted permanent is an illegal target by the time Twitch resolves, the entire spell is countered. You don\'t draw a card.').

card_ruling('two-headed sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('two-headed sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('tymaret, the murder king', '2013-09-15', 'Tymaret’s last ability can be activated only if Tymaret is in your graveyard. Notably, Tymaret can’t be sacrificed to return itself.').

card_ruling('typhoon', '2004-10-04', 'Number of Islands is counted on resolution and not on announcement.').

card_ruling('tyrant of discord', '2012-05-01', 'Repeating the process includes the instruction to repeat the process. That is, the opponent will keep sacrificing permanents until he or she sacrifices a land.').
card_ruling('tyrant of discord', '2012-05-01', 'Repeating the process does not include targeting an opponent. Only one opponent will have to sacrifice permanents.').
card_ruling('tyrant of discord', '2012-05-01', 'If any abilities trigger while Tyrant of Discord\'s enters-the-battlefield ability is resolving, those abilities will wait until the enters-the-battlefield ability finishes resolving before going on the stack. Starting with the active player, each player puts his or her abilities on the stack in any order.').
card_ruling('tyrant of discord', '2012-05-01', 'If the target opponent can\'t sacrifice permanents (perhaps because of Sigarda, Host of Herons), the opponent will choose a permanent at random but not sacrifice it. The ability will then finish resolving.').

card_ruling('tyrant\'s choice', '2014-05-29', 'If death gets more votes, each opponent chooses and sacrifices a creature he or she controls. None of these creatures are targeted. An opponent could sacrifice a creature with protection from black, for example.').
card_ruling('tyrant\'s choice', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('tyrant\'s choice', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('tyrant\'s choice', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('tyrant\'s choice', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('tyrant\'s choice', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('tyrant\'s familiar', '2014-11-07', 'Lieutenant abilities apply only if your commander is on the battlefield and under your control.').
card_ruling('tyrant\'s familiar', '2014-11-07', 'Lieutenant abilities refer only to whether you control your commander, not any other player’s commander.').
card_ruling('tyrant\'s familiar', '2014-11-07', 'If you gain control of a creature with a lieutenant ability owned by another player, that ability will check to see if you control your commander and will apply if you do. It won’t check whether its owner controls his or her commander.').
card_ruling('tyrant\'s familiar', '2014-11-07', 'If you lose control of your commander, lieutenant abilities of creatures you control will immediately stop applying. If this causes a creature’s toughness to become less than or equal to the amount of damage marked on it, the creature will be destroyed.').
card_ruling('tyrant\'s familiar', '2014-11-07', 'If a triggered ability granted by a lieutenant ability triggers, and in response to that trigger you lose control of your commander (causing the lieutenant to lose that ability), that triggered ability will still resolve.').

