% Rulings

card_ruling('gabriel angelfire', '2009-10-01', 'Gabriel Angelfire will have the ability it gains through your next untap step, but will no longer have it during your next upkeep step.').

card_ruling('gaddock teeg', '2007-10-01', 'If one half of a split card has a converted mana cost of 3 or less and doesn\'t have an {X} in its mana cost, Gaddock Teeg lets you cast that half.').

card_ruling('gaea\'s anthem', '2007-02-01', 'This is the timeshifted version of Glorious Anthem.').

card_ruling('gaea\'s avenger', '2004-10-04', 'In multi-player games it counts artifacts of all opponents.').

card_ruling('gaea\'s balance', '2004-10-04', 'You can find a nonbasic land card this way as long as it has a basic land type, such as Stomping Ground.').
card_ruling('gaea\'s balance', '2004-10-04', 'You can fail to find a land card for any or all basic land types, even if a land card with that basic land type is in your library.').

card_ruling('gaea\'s blessing', '2004-10-04', 'It targets the player and each of the cards. Thus, it is only a \"spell with one target\" if you choose to target zero cards.').
card_ruling('gaea\'s blessing', '2004-10-04', 'Does not trigger the \"if moved to the graveyard\" ability if this card is discarded from your hand.').
card_ruling('gaea\'s blessing', '2004-10-04', 'When cast as a spell, you can\'t choose this card as one of the targets. This is because you choose the targets when casting this spell, and this spell does not go to the graveyard until after it resolves.').
card_ruling('gaea\'s blessing', '2008-04-01', 'You must target a player. You may target zero, one, two, or three cards in that player\'s graveyard.').

card_ruling('gaea\'s bounty', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('gaea\'s herald', '2004-10-04', 'The ability applies to your opponent\'s spells as well as your own.').

card_ruling('gaea\'s liege', '2004-10-04', 'When being declared as an attacker, use the \"not attacking\" power and toughness. It only changes after declaration is complete.').
card_ruling('gaea\'s liege', '2006-09-25', 'The activated ability doesn\'t affect whether the land is basic or not. It overwrites any other land types. Being a Forest gives the affected land the ability \"{T}: Add {G} to your mana pool.\"').
card_ruling('gaea\'s liege', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').

card_ruling('gaea\'s might', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('gaea\'s might', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('gaea\'s might', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('gaea\'s revenge', '2010-08-15', 'Gaea\'s Revenge\'s first ability works only while it\'s a spell on the stack. Gaea\'s Revenge\'s last ability works only while it\'s on the battlefield.').
card_ruling('gaea\'s revenge', '2010-08-15', 'A Gaea’s Revenge spell can be targeted by spells and abilities that would counter it. The part of their effect that would counter Gaea’s Revenge won’t do anything, but any other effects those spells or abilities may have will still happen, if applicable.').
card_ruling('gaea\'s revenge', '2010-08-15', 'Gaea\'s Revenge\'s last ability applies to all nongreen spells and abilities from nongreen sources, including ones you control. For example, you can\'t target it with the equip ability of an Equipment you control (unless the Equipment is somehow green).').
card_ruling('gaea\'s revenge', '2015-06-22', 'If a spell is one or more colors, and one of those colors is green, that spell can target Gaea’s Revenge.').

card_ruling('gaea\'s touch', '2014-02-01', 'The land enters the battlefield directly from your hand, but it is not \"played\". This means it does not count towards the number of lands played this turn and doesn\'t trigger abilities that would trigger if the Forest had been played.').

card_ruling('galepowder mage', '2007-10-01', 'Galepowder Mage\'s ability can target a creature controlled by any player. It\'s not optional.').
card_ruling('galepowder mage', '2007-10-01', 'The exiled card will be returned to the battlefield at the beginning of the end step even if Galepowder Mage is no longer on the battlefield at that time.').
card_ruling('galepowder mage', '2007-10-01', 'If Galepowder Mage is the only creature on the battlefield when it attacks, its ability has no effect.').

card_ruling('galerider sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('galerider sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('gallowbraid', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('gallows at willow hill', '2012-05-01', 'You can tap any three untapped Humans you control, including ones you haven\'t controlled continuously since the beginning of your most recent turn, to pay the cost of the activated ability.').
card_ruling('gallows at willow hill', '2012-05-01', 'If the targeted creature is an illegal target when the ability tries to resolve, the ability will be countered and none of its effects will happen. The creature\'s controller won\'t put a Spirit token onto the battlefield.').

card_ruling('galvanoth', '2011-06-01', 'If the card you look at isn\'t an instant or sorcery card, it remains on top of your library. You don\'t reveal it.').
card_ruling('galvanoth', '2011-06-01', 'You may pay additional costs, such as kicker costs, of the instant or sorcery card.').
card_ruling('galvanoth', '2011-06-01', 'If the card has any mandatory additional costs, as Kuldotha Rebirth does, you must pay them in order to cast the spell.').
card_ruling('galvanoth', '2011-06-01', 'If the card has an X in its mana cost, you must choose 0 as its value.').

card_ruling('gamble', '2004-10-04', 'You might end up discarding the card you searched for.').

card_ruling('game of chaos', '2004-10-04', 'To \"double the life stakes\" means to double the amount of life lost or gained.').

card_ruling('gamekeeper', '2004-10-04', 'If you have no creature cards in your library, all the cards from your library end up in your graveyard and you do not get to put a creature onto the battlefield.').
card_ruling('gamekeeper', '2004-10-04', 'Exiling this card is optional. If you forget to do so, you can\'t go back later and do so, even if that is what you normally do.').

card_ruling('gang of devils', '2012-05-01', 'You divide the damage as you put Gang of Devils\'s triggered ability on the stack, not as it resolves. Each target must be assigned at least 1 damage. (In other words, as you put the ability on the stack, you choose whether to have it deal 3 damage to a single target, 2 damage to one target and 1 damage to another target, or 1 damage to each of three targets.)').

card_ruling('gang of elk', '2004-10-04', 'Gang of Elk gets the bonus once for each blocker.').

card_ruling('gangrenous goliath', '2004-10-04', 'The ability can only be activated while this card is in your graveyard.').

card_ruling('gangrenous zombies', '2004-10-04', 'Whether it does 1 or 2 damage is checked on resolution.').

card_ruling('gargantuan gorilla', '2004-10-04', 'Giving either creature first strike does not affect the ability.').
card_ruling('gargantuan gorilla', '2004-10-04', 'If this leaves the battlefield before its activated ability resolves, it will still deal damage to the targeted creature. On the other hand, if the targeted creature leaves the battlefield before the ability resolves, the ability will be countered and no damage will be dealt.').

card_ruling('garruk relentless', '2011-09-22', 'Garruk Relentless\'s first ability is a state-triggered ability. It triggers once Garruk has two or fewer loyalty counters on him and it can\'t retrigger while that ability is on the stack.').
card_ruling('garruk relentless', '2011-09-22', 'You don\'t add or remove loyalty counters from Garruk Relentless when he transforms into Garruk, the Veil-Cursed. In most cases, he\'ll have one or two loyalty counters on him.').
card_ruling('garruk relentless', '2011-09-22', 'You can\'t activate a loyalty ability of Garruk Relentless and later that turn after he transforms activate a loyalty ability of Garruk, the Veil-Cursed.').
card_ruling('garruk relentless', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('garruk relentless', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('garruk relentless', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('garruk relentless', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('garruk relentless', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('garruk relentless', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('garruk relentless', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('garruk relentless', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('garruk relentless', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('garruk relentless', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('garruk relentless', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('garruk wildspeaker', '2009-10-01', 'The first ability can target any two lands. They don\'t have to be tapped.').
card_ruling('garruk wildspeaker', '2009-10-01', 'The third ability affects only creatures you control at the time it resolves. It won\'t affect creatures that come under your control later in the turn.').
card_ruling('garruk wildspeaker', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('garruk wildspeaker', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('garruk wildspeaker', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('garruk wildspeaker', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('garruk wildspeaker', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('garruk wildspeaker', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('garruk wildspeaker', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('garruk wildspeaker', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('garruk wildspeaker', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('garruk wildspeaker', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('garruk wildspeaker', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('garruk\'s horde', '2011-09-22', 'Normally, Garruk\'s Horde allows you to cast the top card of your library if it\'s a creature card, it\'s your main phase, and the stack is empty. If that creature card has flash, you\'ll be able to cast it at the time you could cast an instant, even on an opponent\'s turn.').
card_ruling('garruk\'s horde', '2011-09-22', 'You\'ll still pay all costs for that spell, including additional costs. You may also pay alternative costs.').
card_ruling('garruk\'s horde', '2011-09-22', 'If the top card of your library is Dryad Arbor (the only card that\'s both a creature and a land), you can\'t play it this way. Dryad Arbor can\'t be cast as a spell.').
card_ruling('garruk\'s horde', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('garruk\'s horde', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('garruk\'s horde', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('garruk\'s packleader', '2010-08-15', 'If Garruk\'s Packleader and another creature with power 3 or greater enter the battlefield under your control at the same time, the ability will trigger.').
card_ruling('garruk\'s packleader', '2012-07-01', 'The ability checks the power of each creature as it enters the battlefield. It will take into account counters the permanent enters the battlefield with and static abilities that affect its power (such as Flinthoof Boar\'s ability). After the creature is on the battlefield, raising its power with a spell (such as Titanic Growth), an activated ability, or a triggered ability won\'t cause this ability to retroactively trigger.').
card_ruling('garruk\'s packleader', '2012-07-01', 'The creature\'s power is checked only to see if the ability triggers. It doesn\'t matter what the creature\'s power is when the ability resolves.').

card_ruling('garruk, apex predator', '2014-07-18', 'The first ability can target and destroy a Garruk planeswalker controlled by another player.').
card_ruling('garruk, apex predator', '2014-07-18', 'If you activate the third ability, and the target creature is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. You won’t gain any life.').
card_ruling('garruk, apex predator', '2014-07-18', 'The emblem’s ability won’t trigger if a creature attacks a planeswalker.').
card_ruling('garruk, apex predator', '2014-07-18', 'The emblem is owned and controlled by the target of the ability that created it. In a multiplayer game, if Garruk’s owner leaves the game, the emblem does not.').

card_ruling('garruk, caller of beasts', '2013-07-01', 'You choose what green creature card to put onto the battlefield, if any, when the second ability resolves.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'If you have Garruk’s emblem, the creature card you search for will be put onto the battlefield before the creature spell you cast resolves. The creature card you put onto the battlefield isn’t cast, so the ability of the emblem won’t retrigger.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('garruk, caller of beasts', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('garruk, caller of beasts', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('garruk, primal hunter', '2011-09-22', 'For Garruk\'s second ability, the greatest power among creatures you control is determined when that ability begins resolving.').
card_ruling('garruk, primal hunter', '2011-09-22', 'If the greatest power among creatures you control is 0 or less, you\'ll draw no cards. You won\'t discard any cards this way.').
card_ruling('garruk, primal hunter', '2011-09-22', 'For Garruk\'s third ability, the number of Wurm tokens you put onto the battlefield is determined when that ability begins resolving.').
card_ruling('garruk, primal hunter', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('garruk, primal hunter', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('garruk, primal hunter', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('garruk, primal hunter', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('garruk, primal hunter', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('garruk, primal hunter', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('garruk, primal hunter', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('garruk, primal hunter', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('garruk, primal hunter', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('garruk, primal hunter', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('garruk, primal hunter', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('garruk, the veil-cursed', '2011-09-22', 'The second ability of Garruk, the Veil-Cursed doesn\'t target a creature. However, when that ability resolves, you must sacrifice a creature if you control one.').
card_ruling('garruk, the veil-cursed', '2011-09-22', 'The number of creature cards in your graveyard is counted when the third ability of Garruk, the Veil-Cursed resolves. Once the ability resolves, the bonus doesn\'t change if that number changes later in the turn.').
card_ruling('garruk, the veil-cursed', '2011-09-22', 'Only creatures you control when the third ability of Garruk, the Veil-Cursed resolves will receive the bonus. Creatures that enter the battlefield or that you gain control of later in the turn won\'t be affected.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('garruk, the veil-cursed', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('gate smasher', '2015-02-25', 'Gate Smasher’s equip ability can target any creature. However, if that creature’s toughness is 3 or less as the equip ability resolves, Gate Smasher won’t become attached to it.').
card_ruling('gate smasher', '2015-02-25', 'If Gate Smasher is attached to a creature whose toughness falls below 4, Gate Smasher will become unattached as a state-based action.').

card_ruling('gateway shade', '2013-01-24', 'You can\'t tap an untapped Gate you control for mana and tap it to activate Gateway Shade\'s ability at the same time. You must choose one or the other.').

card_ruling('gather courage', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('gather courage', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('gather courage', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('gather courage', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('gather courage', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('gather courage', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('gather courage', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('gather specimens', '2008-10-01', 'Gather Specimens isn\'t targeted. It affects creatures that would enter the battlefield under any opponent\'s control.').
card_ruling('gather specimens', '2008-10-01', 'Gather Specimens affects both token creatures and nontoken creatures. It affects creatures that would enter the battlefield by any means. This includes, of course, creature spells that resolve. It also includes creatures put onto the battlefield as a result of a resolving spell (such as Call of the Herd or Zombify), resolving ability (such as Verdant Force\'s ability or Doomed Necromancer\'s ability), cost (such as Varchild\'s War-Riders\'s cumulative upkeep cost), replacement effect (such as the one created by Words of Wilding), or any other means.').
card_ruling('gather specimens', '2008-10-01', 'If a creature spell controlled by an opponent would resolve, it resolves, but the creature enters the battlefield under your control instead of the opponent\'s control. Choices made when casting that spell (such as whether it was kicked or the value of X in the spell\'s cost) are remembered. Any \"enters the battlefield\" triggered abilities will trigger after the creature is on the battlefield under your control.').
card_ruling('gather specimens', '2008-10-01', 'The Gather Specimens replacement effect is applied before any other replacement effects that would also modify how the creature enters the battlefield. These are usually worded \"as [this creature] enters the battlefield\" or “[this creature] enters the battlefield with.” For example, if your Gather Specimens has resolved, then the following things are true: -- If a creature with devour would enter the battlefield under an opponent\'s control, you choose and sacrifice your creatures as it enters the battlefield under your control. -- If Voice of All would enter the battlefield under an opponent\'s control, you choose a color as it enters the battlefield under your control. -- If Clone would enter the battlefield under an opponent\'s control, you choose which creature it copies as it enters the battlefield under your control. -- If a Wizard would enter the battlefield under an opponent\'s control and that player controls Sage of Fables, the Wizard will not enter the battlefield with a +1/+1 counter on it as it enters the battlefield under your control. -- If a Wizard would enter the battlefield under an opponent\'s control and you control Sage of Fables, the Wizard will enter the battlefield with a +1/+1 counter on it as it enters the battlefield under your control.').
card_ruling('gather specimens', '2008-10-01', 'Gather Specimens won\'t retroactively change the control of creatures that have already enter the battlefield that turn.').
card_ruling('gather specimens', '2008-10-01', 'Some effects that put creatures onto the battlefield continue to affect those creatures later on. Although Gather Specimens changes whose control the creature enters the battlefield under, the rest of the effect works as normal. For example, if your opponent activates a creature card\'s unearth ability and you cast Gather Specimens, that creature enters the battlefield under your control, but the rest of the unearth ability is unchanged. The creature has haste. It\'s exiled at the beginning of the end step. If it would leave the battlefield, it\'s exiled instead of being put anywhere else.').
card_ruling('gather specimens', '2008-10-01', 'If the effect that puts a creature onto the battlefield also creates a delayed triggered ability, Gather Specimens doesn\'t change who controls that ability. In the unearth example above, your opponent controls the ability that exiles the creature at the beginning of the end step. On the other hand, if the effect that puts a creature onto the battlefield grants a triggered ability to the creature (with \"gains\" or \"has\"), the player who controls the creature at the time the ability triggers will be the player who controls that ability. For example, if your opponent casts Makeshift Mannequin and you cast Gather Specimens in response, the creature will return to the battlefield under your control with a mannequin counter and it will have the ability \"When this creature becomes the target of a spell or ability, sacrifice it.\" If the ability triggers, you\'ll control it, so you\'ll have to sacrifice the creature.').
card_ruling('gather specimens', '2008-10-01', 'If two or more players have each cast Gather Specimens during the same turn and a creature would enter the battlefield, the creature\'s would-be controller (the controller of the creature spell, for example) chooses one of the applicable Gather Specimens to apply. Then the new would-be controller of the creature repeats this process among the remaining Gather Specimens, and so on, until there are no more possible Gather Specimens effects to apply.').
card_ruling('gather specimens', '2008-10-01', 'The above procedure means that if two opposing players have each cast Gather Specimens during the same turn and a creature would enter the battlefield under the control of one of them, it really will enter the battlefield under that player\'s control. (The creature would enter the battlefield under player A\'s control, so player B\'s Gather Specimens affects it. Now that creature would enter the battlefield under player B\'s control, so player A\'s Gather Specimens affects it. Each replacement effect has now been used, so the creature will enter the battlefield under player A\'s control.)').

card_ruling('gather the pack', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('gatherer of graces', '2006-02-01', 'The Aura you sacrifice doesn\'t have to be attached to Gatherer of Graces.').
card_ruling('gatherer of graces', '2006-02-01', 'If Gatherer of Graces already has damage on it equal to its toughness minus 1, then sacrificing an Aura attached to it results in Gatherer of Graces being put into its owner\'s graveyard before the ability that would regenerate it resolves.').

card_ruling('gauntlet of might', '2004-10-04', 'Dual lands which have Mountain as one of their types produce an extra red mana when tapped for either color.').

card_ruling('gauntlets of chaos', '2009-10-01', 'As you activate the ability, the targets you choose must be two artifacts, two creatures, or two lands. As the ability resolves, both targets will be legal only if they are two artifacts, two creatures, or two lands at that time as well, though they may be a different type than they were at the time the ability was activated. For example, if the targets were a creature and an artifact creature when the ability was activated, but the first target became a noncreature artifact by the time the ability resolves, both targets will still be legal (since they\'re both artifacts).').
card_ruling('gauntlets of chaos', '2009-10-01', 'If one of the targets is illegal by the time the ability resolves (because the wrong player controls it, or it\'s the wrong card type, or for any other reason), the exchange doesn\'t happen. The target that\'s still legal will remain under its controller\'s control. Since the exchange doesn\'t happen, no Auras are destroyed. (If both targets are illegal, the ability is countered.)').

card_ruling('gavony', '2013-07-01', 'The chaos ability grants indestructible only to creatures that you control at the time it resovles. Creatures that you gain control of later in the turn will not have indestructible.').
card_ruling('gavony', '2013-07-01', 'If another player gains control of one of your creatures after the chaos ability resolves, that creature will continue to have indestructible until end of turn.').

card_ruling('gaze of granite', '2013-04-15', 'If a permanent has {X} in its mana cost, the value of that X is 0.').
card_ruling('gaze of granite', '2013-04-15', 'The converted mana cost of a creature token is 0 unless that token is a copy of another creature, in which case it copies that creature\'s mana cost.').

card_ruling('gaze of justice', '2006-09-25', 'The additional cost must be paid if Gaze of Justice is cast with flashback.').

card_ruling('gaze of pain', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('gaze of the gorgon', '2005-10-01', 'At the next end of combat step in the turn, all creatures that had blocked the targeted creature at any point during the turn will be destroyed. This includes creatures that blocked it before Gaze of the Gorgon was cast. It doesn\'t matter whether the targeted creature is still on the battlefield at that point.').

card_ruling('geist of saint traft', '2011-09-22', 'You declare which player or planeswalker the token is attacking as you put it onto the battlefield. It doesn\'t have to be the same player or planeswalker Geist of Saint Traft is attacking.').
card_ruling('geist of saint traft', '2011-09-22', 'Although the token is attacking, it was never declared as an attacking creature (for purposes of abilities that trigger whenever a creature attacks, for example).').

card_ruling('geist snatch', '2012-05-01', 'If the target creature spell is an illegal target when Geist Snatch tries to resolve, Geist Snatch will be countered and none of its effects will happen. You won\'t put a Spirit token onto the battlefield.').
card_ruling('geist snatch', '2012-05-01', 'If Geist Snatch resolves but the creature spell can\'t be countered, you\'ll still put the Spirit token onto the battlefield.').

card_ruling('geist-honored monk', '2011-09-22', 'The ability that defines Geist-Honored Monk\'s power and toughness works in all zones, not just the battlefield.').
card_ruling('geist-honored monk', '2011-09-22', 'As long as Geist-Honored Monk is on the battlefield, its second ability will count itself.').

card_ruling('geistcatcher\'s rig', '2011-09-22', 'The target creature with flying is chosen when the ability triggers and goes on the stack. You choose whether or not Geistcatcher\'s Rig will deal 4 damage to it when then ability resolves.').

card_ruling('gelatinous genesis', '2010-06-15', 'For example: -- If you spend {2}{G} to cast Gelatinous Genesis, you\'ll get a single 1/1 creature token. -- If you spend {4}{G}, you\'ll get two 2/2 creatures. -- If you spend {6}{G}, you\'ll get three 3/3 creatures. And so on.').

card_ruling('gelid shackles', '2006-07-15', 'Gelid Shackles doesn\'t stop static abilities or triggered abilities from working. It also won\'t prevent the creature from attacking unless the enchanted creature is given defender before attackers are declared.').
card_ruling('gelid shackles', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('gem of becoming', '2012-07-01', 'You don\'t have to find all three cards, even if they\'re present in your library. For example, you can activate the ability just to find an Island card and a Swamp card.').

card_ruling('gemhide sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('gemhide sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('gemini engine', '2004-12-01', 'The Twin token enters the battlefield as an untapped attacking creature. It\'s not subject to any costs to attack or restrictions on attacking.').
card_ruling('gemini engine', '2004-12-01', 'The Twin token won\'t trigger any abilities that trigger \"when [a creature] attacks.\"').
card_ruling('gemini engine', '2004-12-01', 'The token\'s power and toughness are set equal to Gemini Engine\'s power and toughness. If there\'s a Glorious Anthem on the battlefield (which gives your creatures +1/+1), Gemini Engine is 4/5. The Twin token starts as a 4/5, and then the Anthem gives it +1/+1, making the Twin a 5/6 creature.').
card_ruling('gemini engine', '2004-12-01', 'Once the token has been created, its power and toughness aren\'t dependent on Gemini Engine\'s power and toughness, and they won\'t change if Gemini Engine\'s power and toughness change.').

card_ruling('gempalm avenger', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gempalm avenger', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('gempalm avenger', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('gempalm incinerator', '2004-10-04', 'The target of the ability can\'t be a creature with Protection from Creatures, with Protection from Goblins, or with Protection from Red.').
card_ruling('gempalm incinerator', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gempalm incinerator', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('gempalm incinerator', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('gempalm incinerator', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('gempalm polluter', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gempalm polluter', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('gempalm polluter', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('gempalm polluter', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('gempalm sorcerer', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gempalm sorcerer', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('gempalm sorcerer', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('gempalm strider', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gempalm strider', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('gempalm strider', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('gempalm strider', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('gemstone caverns', '2006-09-25', 'After all players have decided not to take any more mulligans, players choose whether to put Leylines and/or Gemstone Caverns onto the battlefield, starting with the player who will be going first and proceeding in turn order. Each player may choose any number of Leylines and/or Gemstone Caverns. The cards are revealed and put onto the battlefield at the same time.').
card_ruling('gemstone caverns', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('gemstone caverns', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').
card_ruling('gemstone caverns', '2013-07-01', 'If multiple Gemstone Caverns are put onto the battlefield under a single player\'s control before the game begins, the \"legend rule\" won\'t put the extras into that player\'s graveyard until just before the first player gets priority during his or her first upkeep step. There\'s no opportunity to tap the extras for mana.').

card_ruling('gemstone mine', '2004-10-04', 'Sacrificing this card when the last counter is removed is part of the effect of tapping it for mana.').
card_ruling('gemstone mine', '2006-09-25', 'If the last mining counter is removed from Gemstone Mine in some way other than activating its ability (such as Chisei, Heart of Oceans), Gemstone Mine won\'t be sacrificed but its ability can\'t be activated.').

card_ruling('general jarkeld', '2004-10-04', 'Does not undo any abilities which triggered due to the declaration of blockers. A creature which is switched away still has any triggered abilities resolve on it.').
card_ruling('general jarkeld', '2004-10-04', 'The new blocker does not trigger any abilities which trigger on creatures becoming blockers, because the creatures were already blockers and the simple change of who is blocking does not trigger such abilities.').

card_ruling('generator servant', '2014-07-18', 'If the mana is spent on two different creature spells, each of those spells (and resulting creatures) will gain haste until end of turn.').
card_ruling('generator servant', '2014-07-18', 'If the mana is spent on additional or alternative costs of a creature spell, the creature will still gain haste.').
card_ruling('generator servant', '2014-07-18', 'An instant or sorcery spell is not a creature spell, even if that spell creates creature tokens.').

card_ruling('genesis wave', '2011-01-01', 'If you have fewer than X cards in your library, you reveal all of them.').
card_ruling('genesis wave', '2011-01-01', 'A permanent card is an artifact, creature, enchantment, land, or planeswalker card.').
card_ruling('genesis wave', '2011-01-01', 'The converted mana cost of a card in your library is determined solely by the mana symbols printed in its upper right corner. The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('genesis wave', '2011-01-01', 'If the mana cost of a card in your library includes {X}, X is considered to be 0.').
card_ruling('genesis wave', '2011-01-01', 'If a card in your library has no mana symbols in its upper right corner (because it\'s a land card, for example), its converted mana cost is 0. Such cards can always be put onto the battlefield with Genesis Wave.').
card_ruling('genesis wave', '2011-01-01', 'You don\'t have to put permanent cards revealed this way onto the battlefield if you choose not to, regardless of their converted mana costs.').

card_ruling('genju of the cedars', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('genju of the cedars', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('genju of the falls', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('genju of the falls', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('genju of the fens', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('genju of the fens', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('genju of the fields', '2005-08-01', 'When activated multiple times, it will have the triggered ability multiple times. When the animated Plains deals damage, the ability will trigger once for each copy of the ability. If, for example, you activate the ability three times, the Plains will deal two damage, and you\'ll gain six life.').
card_ruling('genju of the fields', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('genju of the fields', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('genju of the realm', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('genju of the realm', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('genju of the spires', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('gerrard\'s command', '2004-10-04', 'This can target a creature which is already untapped.').

card_ruling('geth\'s verdict', '2011-06-01', 'As long as the player is a legal target when Geth\'s Verdict resolves, that player loses 1 life even if he or she didn\'t sacrifice a creature (most likely because he or she didn\'t control any).').

card_ruling('geth, lord of the vault', '2011-01-01', 'The converted mana cost of a card in a graveyard is determined solely by the mana symbols printed in its upper right corner. The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5. If the mana cost of a card in a graveyard includes {X}, X is considered to be 0.').
card_ruling('geth, lord of the vault', '2011-01-01', 'The X in Geth\'s activation cost must be exactly the converted mana cost of the targeted artifact or creature card. You can\'t pay more than X to put more cards from the opponent\'s library into his or her graveyard.').
card_ruling('geth, lord of the vault', '2011-01-01', 'If the targeted artifact or creature card leaves the graveyard by the time the ability resolves, the ability is countered. The player won\'t put any cards into his graveyard.').
card_ruling('geth, lord of the vault', '2011-01-01', 'If the player has fewer than X cards in his or her library, he or she puts all cards from his or her library into his or her graveyard.').

card_ruling('geyser glider', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('geyser glider', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('geyserfield stalker', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('geyserfield stalker', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('geyserfield stalker', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('ghastly conscription', '2014-11-24', 'The pile is shuffled to disguise from your opponents which manifested creature is which. After you manifest the cards, you can look at them.').
card_ruling('ghastly conscription', '2014-11-24', 'If you manifest a card owned by an opponent and you leave the game, that card is exiled.').
card_ruling('ghastly conscription', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('ghastly conscription', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('ghastly conscription', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('ghastly conscription', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('ghastly conscription', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ghastly conscription', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('ghastly conscription', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ghastly conscription', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('ghastly conscription', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('ghastly conscription', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('ghastly conscription', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('ghastly conscription', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('ghastly conscription', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('ghastly demise', '2004-10-04', 'This card is not in the graveyard until after you destroy the creature. So it does not count itself when determining the maximum toughness.').

card_ruling('ghirapur æther grid', '2015-06-22', 'You may tap any two untapped artifacts you control, including artifact creatures that haven’t been under your control continuously since the beginning of your most recent turn.').

card_ruling('ghitu encampment', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('ghitu encampment', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('ghitu fire', '2004-10-04', 'The extra 2 mana to cast it as an instant does not count as part of the mana cost.').

card_ruling('ghitu firebreathing', '2006-09-25', 'If you return Ghitu Firebreathing to its owner\'s hand while the +1/+0 ability is on the stack, that ability will still give the creature that was last enchanted by Ghitu Firebreathing +1/+0.').

card_ruling('ghor-clan bloodscale', '2006-02-01', 'If this card\'s ability is activated by one player, then another player takes control of it on the same turn, the second player can\'t activate its ability that turn.').

card_ruling('ghost council of orzhova', '2006-02-01', 'Sacrificing Ghost Council of Orzhova to its own ability means it will wind up in the graveyard, not exiled.').
card_ruling('ghost council of orzhova', '2006-02-01', 'When Ghost Council of Orzhova is exiled, any Auras or Equipment on it fall off and any counters on it are removed. When it returns to the battlefield, it comes back untapped and is treated as a new version of the card.').
card_ruling('ghost council of orzhova', '2006-02-01', 'Normally, Ghost Council of Orzhova will return to the battlefield at the end of the same turn it\'s exiled. But if it\'s exiled during the End step, it\'s too late to return it this turn. It has to wait to return to the battlefield until the next End step.').

card_ruling('ghost quarter', '2006-05-01', 'If you target Ghost Quarter with its own ability, the ability will be countered because its target is no longer on the battlefield. You won\'t get to search for a land card.').
card_ruling('ghost quarter', '2011-09-22', 'If the targeted land is an illegal target by the time Ghost Quarter\'s ability resolves, it will be countered and none of its effects will happen. The land\'s controller won\'t get to search for a basic land card.').
card_ruling('ghost quarter', '2013-07-01', 'The target land\'s controller gets to search for a basic land card even if that land wasn\'t destroyed by Ghost Quarter\'s ability. This may happen because the land has indestructible or because it was regenerated.').

card_ruling('ghostblade eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('ghostblade eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('ghostblade eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('ghostblade eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('ghostblade eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('ghostblade eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('ghostblade eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('ghostfire', '2007-05-01', 'Ghostfire is colorless in all zones. Its damage comes from a colorless source.').

card_ruling('ghostfire blade', '2014-09-20', 'Face-down creatures and most artifact creatures are colorless.').

card_ruling('ghostflame sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('ghostflame sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('ghosthelm courier', '2004-10-04', 'It checks the creature type when the ability is announced and resolved, but once the effect is placed on the creature, if its creature type changes the effect still continues.').

card_ruling('ghostly flame', '2004-10-04', 'It does not change the color of the source, so that things that trigger on a red spell doing damage will still trigger. The damage itself thinks it came from a colorless source, however.').
card_ruling('ghostly flame', '2004-10-04', 'Does not make red and black spells and permanents colorless. They still have color. A red spell can\'t target a creature with Protection from Red due to this. The spells just act like colorless sources when dealing damage.').
card_ruling('ghostly flame', '2004-10-04', 'If the source has more than one color but at least one is red or black, then the damage is colorless and all the other colors are forgotten.').
card_ruling('ghostly flame', '2004-10-04', 'The effect is continuous and applies whenever something looks at the damage. If this card leaves the battlefield, damage from red and black spells will appear as its normal color. In other words, the color was not removed from the damage, the color just could not be determined while this card is in effect.').

card_ruling('ghostly flicker', '2012-05-01', 'The two targets can have different card types. For example, you can target one artifact and one creature with Ghostly Flicker.').

card_ruling('ghostly prison', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').
card_ruling('ghostly prison', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('ghosts of the innocent', '2005-10-01', 'If a damage prevention effect and Ghosts of the Innocent\'s effect would apply to the same damage, the player or the controller of the creature being dealt damage may apply the effects in either order (most likely applying the halving effect first).').
card_ruling('ghosts of the innocent', '2005-10-01', 'Half of 1 rounded down is 0. A source that would deal 1 damage to a creature or player won\'t deal damage to that creature or player at all.').
card_ruling('ghosts of the innocent', '2005-10-01', 'Multiple Ghosts of the Innocent effects are cumulative, and each will halve the damage, rounded down. For example, with three on the battlefield, 14 damage becomes 7, then 3, then finally 1, and only 1 damage would actually be dealt.').
card_ruling('ghosts of the innocent', '2005-10-01', 'This isn\'t a damage prevention effect. If Excruciator (\"Damage that would be dealt by Excruciator can\'t be prevented\") would deal 7 damage to a creature or player, it deals 3 damage instead.').
card_ruling('ghosts of the innocent', '2005-10-01', 'If damage is redirected, it\'s only halved once.').
card_ruling('ghosts of the innocent', '2005-10-01', 'If both Ghosts of the Innocent and Furnace of Rath (which doubles damage) are on the battlefield, the controller of the creature being dealt damage or the player being dealt damage can apply the effects in either order. This can matter if the original amount of damage is odd. For example, if a source would deal 3 damage, Ghosts of the Innocent would make it 1 damage, then Furnace of Rath would make it 2 damage, if the player chose to apply the effects in that order.').

card_ruling('ghostway', '2006-02-01', 'When the creatures are exiled, any Auras or Equipment on them fall off and any counters on them are removed. When they return to the battlefield, they come back untapped and are treated as new versions of the cards.').
card_ruling('ghostway', '2006-02-01', 'Normally, the creatures will return to the battlefield at the end of the same turn they were exiled. But if they\'re exiled during the End step, it\'s too late to return them this turn. They have to wait to return to the battlefield until the next End step.').
card_ruling('ghostway', '2006-02-01', 'All cards exiled with Ghostway will return to the battlefield, even if they\'re no longer creatures.').

card_ruling('ghoulflesh', '2012-05-01', 'If Ghoulflesh causes a creature to have 0 toughness or have damage marked on it equal to or greater than its toughness, it will be a black Zombie (in addition to its other colors and types) when it dies.').

card_ruling('ghoultree', '2011-01-22', 'Ghoultree\'s ability can\'t reduce the total cost to cast the spell below {G}.').
card_ruling('ghoultree', '2011-01-22', 'Although players may respond to Ghoultree once it\'s been cast, once it\'s announced, they can\'t respond before the cost is calculated and paid.').

card_ruling('giant adephage', '2013-01-24', 'As the token is created, it checks the printed values of the Giant Adephage it\'s copying—or, if the Giant Adephage whose ability triggered was itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield—as well as any copy effects that have been applied to it. It won\'t copy counters on the Giant Adephage, nor will it copy other effects that have changed Giant Adephage\'s power, toughness, types, color, or so on. Normally, this means the token will simply be a Giant Adephage. But if any copy effects have affected that Giant Adephage, they\'re taken into account.').

card_ruling('giant albatross', '2004-10-04', 'The ability works no matter how it goes to the graveyard from the battlefield. It seeks out all creatures that damaged it during the turn and causes them to be destroyed.').
card_ruling('giant albatross', '2004-10-04', 'The ability works even if the damage is redirected to the Albatross.').
card_ruling('giant albatross', '2004-10-04', 'The ability is a triggered ability and is only put on the stack once just after this card goes to the graveyard.').

card_ruling('giant ambush beetle', '2009-05-01', 'You decide whether to have the targeted creature block Giant Ambush Beetle this turn if able at the time the enters-the-battlefield ability resolves, not at the time Giant Ambush Beetle attacks.').
card_ruling('giant ambush beetle', '2009-05-01', 'Deciding to have the targeted creature block doesn\'t force you to attack with Giant Ambush Beetle that turn. If Giant Ambush Beetle doesn\'t attack, the targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('giant ambush beetle', '2009-05-01', 'If you choose to have the targeted creature block but that creature isn\'t able to block Giant Ambush Beetle (for example, because the targeted creature can block only creatures with flying), the requirement to block does nothing. The targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('giant ambush beetle', '2009-05-01', 'Tapped creatures, creatures that can\'t block as the result of an effect, creatures with unpaid costs to block (such as those from War Cadence), and creatures that aren\'t controlled by the defending player are exempt from effects that would require them to block. Such creatures can be targeted by Giant Ambush Beetle\'s ability, but the requirement to block does nothing.').

card_ruling('giant caterpillar', '2006-02-01', 'It used to make Butterfly tokens. Now it makes Insect tokens named Butterfly.').

card_ruling('giant dustwasp', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('giant dustwasp', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('giant dustwasp', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('giant dustwasp', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('giant dustwasp', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('giant dustwasp', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('giant dustwasp', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('giant dustwasp', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('giant dustwasp', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('giant dustwasp', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('giant oyster', '2004-10-04', 'If the Oyster untaps during the draw step before its ability to add a -1/-1 counter resolves, the -1/-1 counter effect resolves and adds a -1/-1 counter after the -1/-1 counters are removed due to being untapped.').
card_ruling('giant oyster', '2006-09-25', 'The activated ability can target only a tapped creature.').
card_ruling('giant oyster', '2006-09-25', 'Activating the ability creates a continuous effect that works only as long as Giant Oyster remains tapped, a repeating delayed triggered ability that\'s in effect only as long as Giant Oyster remains tapped, and a delayed triggered ability that triggers when Giant Oyster untaps or when it leaves the battlefield.').
card_ruling('giant oyster', '2006-09-25', 'Giant Oyster\'s ability can be activated during your upkeep, and the creature will get a -1/-1 counter during the draw step that follows it.').

card_ruling('giant shark', '2004-10-04', 'Giant Shark only gains its bonus if the blocking creature was damaged before blocking was declared. Damaging the creature later will not give the Shark the bonus.').
card_ruling('giant shark', '2004-10-04', 'The bonus happens when a previously damaged creature becomes a blocker and the bonus is not removed if that creature is removed from the blocking situation by any means.').
card_ruling('giant shark', '2004-10-04', 'If a creature enters a blocking situation with the Giant Shark due to being part of a banded group of attackers, the Shark will get the bonus if that creature is damaged at that time.').
card_ruling('giant shark', '2004-10-04', 'Giant Shark gets the bonus once for each creature it blocks or is blocked by that meets the criteria.').

card_ruling('giant slug', '2004-10-04', 'If the Slug changes controllers after the mana is spent, the player who activates this ability selects a landwalk during their next upkeep even if they don\'t control it at the time.').
card_ruling('giant slug', '2004-10-04', 'When the ability resolves, it creates another ability that triggers at the beginning of your next upkeep. When that ability resolves, you choose a basic land type.').

card_ruling('giant spider', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('giant trap door spider', '2004-10-04', 'If Giant Trap Door Spider leaves the battlefield before its ability resolves, the ability will still resolve as normal. The targeted creature will be exiled.').
card_ruling('giant trap door spider', '2004-10-04', 'If the targeted creature leaves the battlefield (or otherwise becomes an illegal target) before the ability resolves, the ability will be countered. Giant Trap Door Spider will remain on the battlefield.').

card_ruling('giant turtle', '2004-10-04', 'It only cares if it attacked on _your_ last turn, and not your opponent\'s. This makes a difference if you take control of the Turtle during your opponent\'s turn after it attacks. You can use it on your turn because it began your turn on the battlefield and because you did not attack with it last turn.').

card_ruling('gibbering descent', '2007-05-01', 'If you have no cards in hand at the time your upkeep step would start, instead that step is skipped and your draw step starts.').

card_ruling('gideon jura', '2010-06-15', 'Gideon Jura\'s first ability doesn\'t lock in what it applies to. That\'s because the effect states a true thing about a set of creatures, but doesn\'t actually change the characteristics of those creatures. As a result, whatever creatures the targeted opponent controls during the declare attackers step of his or her next turn must attack Gideon Jura if able. This includes creatures that come under that player\'s control after the ability has resolved and creatures that have lost all abilities.').
card_ruling('gideon jura', '2010-06-15', 'Gideon Jura\'s first ability causes creatures to attack him if able. If, during the affected player\'s declare attackers step, a creature he or she controls is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then that creature doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so the creature doesn\'t have to attack in that case either.').
card_ruling('gideon jura', '2010-06-15', 'If a creature controlled by the affected player can\'t attack Gideon Jura (because he\'s no longer on the battlefield, for example), that player may have it attack you, another one of your planeswalkers, or nothing at all.').
card_ruling('gideon jura', '2010-06-15', 'Gideon Jura\'s first ability applies during each combat phase of the affected player\'s next turn (as opposed to applying during the affected player\'s next combat phase). The distinction is relevant if there are no combat phases during that turn (due to Fatespinner\'s effect, for example) or there are multiples (due to World at War, for example).').
card_ruling('gideon jura', '2010-06-15', 'If Gideon Jura becomes a creature due to his third ability, that doesn\'t count as having a creature enter the battlefield. Gideon Jura was already on the battlefield; he only changed his types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('gideon jura', '2010-06-15', 'If Gideon Jura becomes a creature, he may be affected by \"summoning sickness.\" You can\'t attack with him or use any of his {T} abilities (if he gains any) unless he began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when Gideon Jura came under your control, not when he became a creature.').
card_ruling('gideon jura', '2010-06-15', 'Gideon Jura\'s third ability causes him to become a creature with the creature types Human Soldier. He remains a planeswalker with the planeswalker type Gideon. (He also retains any other card types or subtypes he may have had.) Each subtype is correlated to the proper card type: Gideon is just a planeswalker type (not a creature type), and Human and Soldier are just creature types (not planeswalker types).').
card_ruling('gideon jura', '2010-06-15', 'If you activate Gideon Jura\'s third ability and then unpreventable damage is dealt to him (due to Unstable Footing, for example), that damage has all applicable results: specifically, the damage is marked on Gideon Jura (since he\'s a creature) and that damage causes that many loyalty counters to be removed from him (since he\'s a planeswalker). If the total amount of damage marked on Gideon Jura is lethal damage, he\'s destroyed as a state-based action. If Gideon Jura has no loyalty counters on him, he\'s put into his owner\'s graveyard as a state-based action.').
card_ruling('gideon jura', '2010-06-15', 'Say you activate Gideon Jura\'s third ability, then an opponent gains control of him before combat. You may have any of your creatures attack Gideon Jura (since he\'s still a planeswalker). Then Gideon Jura may block (since he\'s a creature). He may block any eligible attacking creature, including one that\'s attacking him! During combat, he behaves as an attacked planeswalker and/or a blocking creature, as appropriate. For example, he deals combat damage to any creatures he\'s blocking, but he doesn\'t deal combat damage to any unblocked creatures that are attacking him.').
card_ruling('gideon jura', '2011-01-22', 'The first ability only affects the declaration of attackers. If a creature is put onto the battlefield attacking (thanks to Hero of Bladehold, Preeminent Captain, or the Ninjutsu ability, for example), that creature\'s controller may choose the defending player or planeswalker that it will be attacking in the normal way.').
card_ruling('gideon jura', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('gideon jura', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('gideon jura', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('gideon jura', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('gideon jura', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('gideon jura', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('gideon jura', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('gideon jura', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('gideon jura', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('gideon jura', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('gideon jura', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('gideon\'s avenger', '2011-09-22', 'If an opponent\'s creature enters the battlefield tapped, Gideon\'s Avenger\'s ability doesn\'t trigger.').

card_ruling('gideon\'s phalanx', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('gideon, ally of zendikar', '2015-08-25', 'Gideon’s first ability doesn’t count as a creature entering the battlefield. Gideon was already on the battlefield; he only changed his types. Specifically, your rally abilities won’t trigger. (Of course, Gideon’s second ability will cause rally abilities to trigger.)').
card_ruling('gideon, ally of zendikar', '2015-08-25', 'If Gideon becomes a creature the same turn he enters the battlefield, you can’t attack with him or use any of his {T} abilities (if he gains any).').
card_ruling('gideon, ally of zendikar', '2015-08-25', 'Gideon’s first ability causes him to become a creature with the creature types Human, Soldier, and Ally. He remains a planeswalker with the planeswalker type Gideon. (He also retains any other card types or subtypes he may have had.) Each subtype is correlated to the proper card type: planeswalker is only a type (not a creature type), and Human, Soldier, and Ally are just creature types (not planeswalker types).').
card_ruling('gideon, ally of zendikar', '2015-08-25', 'If damage that can’t be prevented is dealt to Gideon after his first ability has resolved, that damage will have all applicable results: specifically, the damage is marked on Gideon (since he’s a creature) and that damage causes that many loyalty counters to be removed from him (since he’s a planeswalker). Even though he has indestructible, if Gideon has no loyalty counters on him, he’s put into his owner’s graveyard.').

card_ruling('gideon, battle-forged', '2015-06-22', 'Gideon’s first ability causes a creature to attack him if able. If, during its controller’s declare attackers step, that creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under its controller’s control continuously since that player’s turn began, then that creature doesn’t attack. If there’s a cost associated with having that creature attack, its controller isn’t forced to pay that cost. If he or she doesn’t, the creature doesn’t have to attack.').
card_ruling('gideon, battle-forged', '2015-06-22', 'If Gideon can’t be attacked, perhaps because he has left the battlefield before the creature’s controller’s next combat, the creature targeted by Gideon’s first ability can attack you or another planeswalker you control, or its controller can choose to have it not attack at all.').
card_ruling('gideon, battle-forged', '2015-06-22', 'If the creature targeted by Gideon’s first ability changes controllers before it has the chance to attack Gideon, the ability will apply to it during its new controller’s next turn.').
card_ruling('gideon, battle-forged', '2015-06-22', 'If Gideon becomes a creature due to his third ability, that doesn’t count as having a creature enter the battlefield. Gideon was already on the battlefield; he only changed his types. Abilities that trigger whenever a creature enters the battlefield won’t trigger.').
card_ruling('gideon, battle-forged', '2015-06-22', 'Gideon’s third ability causes him to become a creature with the creature types Human Soldier. He remains a planeswalker with the planeswalker type Gideon. (He also retains any other card types or subtypes he may have had.) Each subtype is correlated to the proper card type: Gideon is just a planeswalker type (not a creature type), and Human and Soldier are just creature types (not planeswalker types).').
card_ruling('gideon, battle-forged', '2015-06-22', 'If you activate Gideon’s third ability and then damage is dealt to him that can’t be prevented, that damage has all applicable results: specifically, the damage is marked on Gideon (since he’s a creature) and that damage causes that many loyalty counters to be removed from him (since he’s a planeswalker). If Gideon has no loyalty counters on him, he’s put into his owner’s graveyard as a state-based action. (As long as he still has indestructible, the marked damage won’t cause him to be destroyed.)').
card_ruling('gideon, battle-forged', '2015-06-22', 'Say you activate Gideon’s third ability, then an opponent gains control of him before combat. You may have any of your creatures attack Gideon (since he’s still a planeswalker). Then Gideon may block (since he’s a creature). He may block any eligible attacking creature, including one that’s attacking him. During combat, he behaves as an attacked planeswalker and/or a blocking creature, as appropriate. For example, he deals combat damage to any creatures he’s blocking, but he doesn’t deal combat damage to any unblocked creatures that are attacking him.').

card_ruling('gideon, champion of justice', '2013-01-24', 'You put a loyalty counter on Gideon, Champion of Justice as part of the cost of activating the first ability. When that ability resolves, you count the number of creatures controlled by the target opponent and put that many additional loyalty counters on Gideon.').
card_ruling('gideon, champion of justice', '2013-01-24', 'If the first ability is countered (perhaps because the target opponent is an illegal target when the ability tries to resolve), you won\'t put any additional loyalty counters on Gideon, although the loyalty counter you put on him to activate the ability will remain.').
card_ruling('gideon, champion of justice', '2013-01-24', 'If Gideon, Champion of Justice becomes a creature due to his second ability, that doesn\'t count as having a creature enter the battlefield. Gideon was already on the battlefield; he only changed his types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('gideon, champion of justice', '2013-01-24', 'Gideon, Champion of Justice\'s power and toughness are set to the number of loyalty counters on him when his second ability resolves. They won\'t change later in the turn if the number of loyalty counters on him changes.').
card_ruling('gideon, champion of justice', '2013-01-24', 'If Gideon, Champion of Justice becomes a creature the same turn he enters the battlefield, you can\'t attack with him or use any of his {T} abilities (if he gains any).').
card_ruling('gideon, champion of justice', '2013-01-24', 'Gideon, Champion of Justice\'s second ability causes him to become a creature with the creature types Human and Soldier. He remains a planeswalker with the planeswalker type Gideon. (He also retains any other card types or subtypes he may have had.) Each subtype is correlated to the proper card type: Gideon is just a planeswalker type (not a creature type), and Human and Soldier are just creature types (not planeswalker types).').
card_ruling('gideon, champion of justice', '2013-01-24', 'Say you activate Gideon, Champion of Justice\'s second ability, and then an opponent gains control of him before combat. You may have any of your creatures attack Gideon, Champion of Justice (since he\'s still a planeswalker). Then Gideon, Champion of Justice may block (since he\'s a creature). He may block any eligible attacking creature, including one that\'s attacking him! During combat, he behaves as an attacked planeswalker and/or a blocking creature, as appropriate. For example, he deals combat damage to any creatures he\'s blocking, but he doesn\'t deal combat damage to any unblocked creatures that are attacking him.').
card_ruling('gideon, champion of justice', '2013-07-01', 'If damage that can\'t be prevented is dealt to Gideon, Champion of Justice after his second ability has resolved, that damage will have all applicable results: specifically, the damage is marked on Gideon (since he\'s a creature) and that damage causes that many loyalty counters to be removed from him (since he\'s a planeswalker). Even though he has indestructible, if Gideon, Champion of Justice has no loyalty counters on him, he\'s put into his owner\'s graveyard as a state-based action.').
card_ruling('gideon, champion of justice', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('gideon, champion of justice', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('gideon, champion of justice', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('gideon, champion of justice', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('gideon, champion of justice', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('gideon, champion of justice', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('gideon, champion of justice', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('gideon, champion of justice', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('gideon, champion of justice', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('gideon, champion of justice', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('gideon, champion of justice', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('gift of estates', '2005-08-01', 'Gift of Estates does nothing if each of your opponents has the same number of lands or fewer as you.').
card_ruling('gift of estates', '2005-08-01', 'You can find nonbasic lands if they have the Plains land type, such as dual lands (Tundra, et al).').

card_ruling('gift of immortality', '2013-09-15', 'If the creature is no longer on the battlefield at the beginning of the next end step after it’s returned to the battlefield, Gift of Immortality will remain in its owner’s graveyard.').
card_ruling('gift of immortality', '2013-09-15', 'If the enchanted creature is a token, neither it nor Gift of Immortality will return to the battlefield.').

card_ruling('gift of the deity', '2008-08-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('gift of the gargantuan', '2008-10-01', 'You may reveal a creature card, or you may reveal a land card, or you may reveal both a creature card and a land card.').
card_ruling('gift of the gargantuan', '2008-10-01', 'If one of the cards is Dryad Arbor (a creature land card), you may reveal Dryad Arbor and another land card or Dryad Arbor and another creature card.').

card_ruling('gifts ungiven', '2004-12-01', 'You can choose to find fewer than four cards if you want, but your opponent will still put two of those cards into your graveyard.').

card_ruling('gigantiform', '2009-10-01', 'If the creature targeted by the Gigantiform spell is an illegal target by the time Gigantiform resolves, the spell is countered. It won\'t enter the battlefield, so you won\'t get to search your library for another Gigantiform.').
card_ruling('gigantiform', '2009-10-01', 'Gigantiform overwrites all previous effects that set the enchanted creature\'s power and toughness to specific values. Other effects that set its power or toughness to specific values that start to apply after Gigantiform becomes attached to it will overwrite Gigantiform.').
card_ruling('gigantiform', '2009-10-01', 'Effects that modify the enchanted creature\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('gigantiform', '2009-10-01', 'Although the Gigantiform cast as a spell targets a creature, the Gigantiform put onto the battlefield as a result of the first one\'s triggered ability does not. It may be attached to a creature with shroud, for example. However, it may not be attached to a creature that couldn\'t be enchanted by it, such as a creature with protection from green. You don\'t choose which creature it will be attached to until it enters the battlefield. Once you choose which creature to attach it to, it\'s too late for players to respond.').

card_ruling('gigantomancer', '2010-06-15', 'This ability overwrites all previous effects that set the targeted creature\'s power and toughness to specific values. Other effects that set its power or toughness to specific values that start to apply after this ability resolves will overwrite this effect.').
card_ruling('gigantomancer', '2010-06-15', 'If a leveler is affected by Gigantomancer\'s ability, then enough level counters are put on it to reach a level indicated by a new level symbol, it will still be 7/7, not the power and toughness indicated by that level symbol.').
card_ruling('gigantomancer', '2010-06-15', 'Effects that modify the targeted creature\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('gild', '2014-02-01', 'You will control the token, no matter who controls the target creature.').
card_ruling('gild', '2014-02-01', 'If the creature isn’t a legal target as Gild tries to resolve, the spell will be countered and none of its effects will happen. No token will be created.').

card_ruling('gilded drake', '2004-10-04', 'If the chosen creature to exchange is not on the battlefield at resolution, then sacrifice this card.').
card_ruling('gilded drake', '2004-10-04', 'This ability is targeted, so Protection can prevent targeting if it exists when the ability would be put on the stack. Once the ability is on the stack, adding Protection will not counter the ability but the exchange will not take place since the target is illegal and you sacrifice this card on resolution.').

card_ruling('gilded light', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('gilder bairn', '2008-08-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('gilder bairn', '2008-08-01', 'The \"summoning sickness\" rule applies to {Q}. If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability. Ignore this rule if the creature also has haste.').
card_ruling('gilder bairn', '2008-08-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('gilder bairn', '2008-08-01', 'Gilder Bairn\'s ability will replicate each kind of counter on the targeted permanent, not just +1/+1 or -1/-1 counters.').

card_ruling('gilt-leaf archdruid', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap creatures (including Gilt-Leaf Archdruid itself) that haven\'t been under your control since your most recent turn began to pay the cost.').
card_ruling('gilt-leaf archdruid', '2008-04-01', 'The second ability\'s effect has no duration. You retain control of those lands until the game ends or until some other effect causes them to change control.').

card_ruling('gilt-leaf winnower', '2015-06-22', 'Once an attacking creature with menace is legally blocked by two or more creatures, removing one or more of those blockers from combat won’t change or undo that block.').

card_ruling('gisela, blade of goldnight', '2012-05-01', 'Gisela doubles damage dealt to opponents and permanents your opponents control from any source, including sources controlled by those opponents.').
card_ruling('gisela, blade of goldnight', '2012-05-01', 'If multiple replacement effects would modify how damage would be dealt, the player being dealt damage (or the controller of the permanent being dealt damage) chooses the order in which to apply those effects.').
card_ruling('gisela, blade of goldnight', '2012-05-01', 'If damage dealt by a source you control is being divided among multiple permanents an opponent controls or among an opponent and one or more permanents he or she controls simultaneously, divide the original amount and double the results. For example, if you attack with a 5/5 creature with trample and your opponent blocks with a 2/2 creature, you can assign 2 damage to the blocker and 3 damage to the defending player. These amounts are then doubled to 4 and 6 damage, respectively. You can\'t double the damage to 10 first and then assign 2 to the creature and 8 to the player.').

card_ruling('gitaxian probe', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('gitaxian probe', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('gitaxian probe', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('gitaxian probe', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('gitaxian probe', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('gitaxian probe', '2011-06-01', 'The targeted player may have no cards in his or her hand. You\'ll still draw a card.').

card_ruling('give', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('give', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('give', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('give', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('give', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('give', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('give', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('give', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('give', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('give', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('give', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('give', '2013-04-15', 'If you cast Give // Take as a fused split spell, you can target the same creature for both parts. If you do so, you will first put three +1/+1 counters on that creature, then remove all +1/+1 counters from it, then draw that many cards.').

card_ruling('glacial chasm', '2008-08-01', 'When Glacial Chasm\'s enters-the-battlefield ability resolves, if you don\'t control any other lands, you must sacrifice Glacial Chasm itself.').

card_ruling('glacial crasher', '2014-07-18', 'It doesn’t matter who controls the Mountain.').
card_ruling('glacial crasher', '2014-07-18', 'If the only Mountains on the battlefield leave the battlefield after Glacial Crasher attacks, it stays attacking.').

card_ruling('glacial fortress', '2009-10-01', 'This checks for lands you control with the land type Plains or Island, not for lands named Plains or Island. The lands it checks for don\'t have to be basic lands. For example, if you control Watery Grave (a nonbasic land with the land types Island and Swamp), Glacial Fortress will enter the battlefield untapped.').
card_ruling('glacial fortress', '2009-10-01', 'As this is entering the battlefield, it checks for lands that are already on the battlefield. It won\'t see lands that are entering the battlefield at the same time (due to Warp World, for example).').

card_ruling('glacial ray', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('glacial ray', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('glacial ray', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('glacial ray', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('glacial stalker', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('glacial stalker', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('glacial stalker', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('glacial stalker', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('glacial stalker', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('glacial stalker', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('glacial stalker', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('glacial stalker', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('glacial stalker', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('glade watcher', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('glade watcher', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('glade watcher', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('glamer spinners', '2008-05-01', 'When Glamer Spinners enters the battlefield, you target only one permanent: the one that will be losing its Auras. You don\'t choose the permanent that will be receiving the Auras until the ability resolves.').
card_ruling('glamer spinners', '2008-05-01', 'You may target a permanent that has no Auras enchanting it.').
card_ruling('glamer spinners', '2008-05-01', 'When the ability resolves, you choose the permanent that will be receiving the Auras. It can\'t be the targeted permanent, it must have the same controller as the targeted permanent, and it must be able to be enchanted by all the Auras attached to the targeted permanent. If you can\'t choose a permanent that meets all those criteria, the Auras won\'t move.').

card_ruling('glamerdye', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('glamerdye', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('glamerdye', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('glamerdye', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').
card_ruling('glamerdye', '2008-08-01', 'Glamerdye\'s effect has no duration. It will last until the game ends or the affected spell or permanent leaves the relevant zone, with the following exception: If Glamerdye affects an artifact, creature, enchantment, or planeswalker spell, it will also affect the permanent that spell becomes when it resolves.').
card_ruling('glamerdye', '2008-08-01', 'You choose the two color words when Glamerdye resolves. You can\'t change a word to the same word; you must choose a different word.').
card_ruling('glamerdye', '2008-08-01', 'Glamerdye affects each instance of the chosen word in the text box of the spell or permanent, as long as it\'s being used as a color word. Glamerdye won\'t change such words in card names, such as White Knight. It also won\'t change mana symbols.').
card_ruling('glamerdye', '2008-08-01', 'Glamerdye can change color words preceded by \"non.\" For example, if you have it change \"red\" to \"green,\" it will also change \"nonred\" to \"nongreen.\"').
card_ruling('glamerdye', '2008-08-01', 'Glamerdye changes only words actually printed on the spell or permanent (if it\'s a card), words that are part of the spell or permanent\'s original characteristics as stated by the effect that created it (if it\'s a token or a copy of a spell), or words that were granted to the spell or permanent as part of a copy effect. It won\'t change words that are granted to the spell or permanent by any other effects. For example, if an Aura grants a creature protection from blue, you can change that to protection from red by targeting the Aura, but not by targeting the creature. Glamerdye also can\'t change what color was chosen for a permanent (such as Painter\'s Servant) as it entered the battlefield.').
card_ruling('glamerdye', '2008-08-01', 'Glamerdye can\'t affect the keyword fear.').

card_ruling('glarecaster', '2004-10-04', 'If both you and this card would be dealt damage at the same time, or if either you or this card would be dealt damage from multiple sources at the same time, the redirection will apply to both chunks of damage.').

card_ruling('glarewielder', '2007-10-01', 'You may choose zero, one, or two target creatures.').
card_ruling('glarewielder', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('glaring aegis', '2015-02-25', 'If you cast Glaring Aegis, and the creature being targeted by the enchant creature ability becomes an illegal target before Glaring Aegis resolves, Glaring Aegis will be countered. It won’t enter the battlefield, and its triggered ability won’t trigger.').
card_ruling('glaring aegis', '2015-02-25', 'If no opponent controls a creature when Glaring Aegis enters the battlefield, the triggered ability will be removed from the stack and have no effect.').

card_ruling('glaring spotlight', '2013-01-24', 'Creatures your opponents control don\'t actually lose hexproof, although you will ignore hexproof for purposes of choosing targets of spells and abilities you control.').
card_ruling('glaring spotlight', '2013-01-24', 'Creatures that come under your control after Glaring Spotlight\'s last ability resolves won\'t have hexproof but they can\'t be blocked that turn.').

card_ruling('glass asp', '2006-09-25', 'After Glass Asp\'s triggered ability resolves, the affected player has the ability to perform a special action of paying {2}. This action may be performed any time the player has priority, and it doesn\'t use the stack. It may be performed only once. Once that player\'s next draw step begins, the player no longer has the ability to perform this action. If the player performed the action, the delayed \"lose 2 life\" triggered ability won\'t trigger. Otherwise, it will.').

card_ruling('glassdust hulk', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('glaze fiend', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('gleam of authority', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('gleam of authority', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('gleam of battle', '2013-04-15', 'The +1/+1 counter is put on the creature before blockers are declared and combat damage is dealt.').
card_ruling('gleam of battle', '2013-04-15', 'If a creature enters the battlefield attacking, it won\'t cause Gleam of Battle to trigger.').

card_ruling('gleam of resistance', '2009-02-01', 'Unlike the normal cycling ability, basic landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a basic land card. You don\'t choose the type of basic land card you\'ll find until you\'re performing the search. After you choose a basic land card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('gleam of resistance', '2009-02-01', 'Basic landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being basic landcycled. Any ability that stops a cycling ability from being activated also stops a basic landcycling ability from being activated.').
card_ruling('gleam of resistance', '2009-02-01', 'Basic landcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with basic landcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('gleam of resistance', '2009-02-01', 'You can choose not to find a basic land card, even if there is one in your library.').

card_ruling('gleancrawler', '2005-10-01', 'When this ability resolves, it returns all creature cards currently in your graveyard that were put there directly from the battlefield sometime during the turn. This includes cards that were put into your graveyard before Gleancrawler entered the battlefield.').

card_ruling('glen elendra', '2012-06-01', 'If either target is no longer on the battlefield or is otherwise an illegal target when the chaos ability resolves, the exchange won\'t happen. No creatures will change controllers. If both targets are illegal, the ability will be countered.').

card_ruling('glen elendra archmage', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('glen elendra archmage', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('glen elendra archmage', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('glen elendra archmage', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('glen elendra archmage', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('glen elendra archmage', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('glen elendra archmage', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('glen elendra liege', '2008-05-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('gliding licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('gliding licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('glimmerdust nap', '2007-10-01', 'Glimmerdust Nap can target only a tapped creature.').
card_ruling('glimmerdust nap', '2007-10-01', 'If the targeted creature has become untapped by the time Glimmerdust Nap would resolve, Glimmerdust Nap is countered for having an illegal target.').
card_ruling('glimmerdust nap', '2007-10-01', 'If the enchanted creature becomes untapped, Glimmerdust Nap is put into its owner\'s graveyard as a state-based action.').

card_ruling('glimmering angel', '2004-10-04', 'You can activate the ability that grants shroud in response to this card being targeted by a spell or ability. If you do so, then (if this is the spell or abilities only target) the spell or ability will be countered since this card will now be an illegal target.').

card_ruling('glimmerpoint stag', '2011-01-01', 'If a token is exiled this way, it will cease to exist and will not return to the battlefield.').
card_ruling('glimmerpoint stag', '2011-01-01', 'If the exiled card is an Aura, that card\'s owner chooses what it will enchant as it comes back onto the battlefield. An Aura put onto the battlefield this way doesn\'t target anything (so it could be attached to a permanent with shroud, for example), but the Aura\'s enchant ability restricts what it can be attached to. If the Aura can\'t legally be attached to anything, it remains exiled.').
card_ruling('glimmerpoint stag', '2011-01-01', 'The exiled card will be returned to the battlefield at the beginning of the next end step even if Glimmerpoint Stag is no longer on the battlefield at that time.').
card_ruling('glimmerpoint stag', '2011-01-01', 'If Glimmerpoint Stag somehow enters the battlefield during a turn\'s end step, the exiled card won\'t be returned to the battlefield until the beginning of the following turn\'s end step.').

card_ruling('glimmerpost', '2011-01-01', 'As the triggered ability resolves, count each Locus on the battlefield regardless of who controls it.').

card_ruling('glimmervoid basin', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('glimmervoid basin', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('glimmervoid basin', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('glimmervoid basin', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('glimmervoid basin', '2009-10-01', 'If a spell targets multiple things, it won\'t cause Glimmervoid Basin\'s first ability to trigger, even if all but one of those targets has become illegal.').
card_ruling('glimmervoid basin', '2009-10-01', 'If a spell targets the same player or object multiple times, it won\'t cause Glimmervoid Basin\'s first ability to trigger.').
card_ruling('glimmervoid basin', '2009-10-01', 'Other than choices involving modes or additional costs, the copies are created based on what they could target if the spell were cast anew. For example, if a player casts Naturalize (\"Destroy target artifact or enchantment\") targeting an artifact, Glimmervoid Basin\'s first ability will copy it for each artifact and enchantment it could target (and each copy will target a different one of those), not just for each artifact it could target.').
card_ruling('glimmervoid basin', '2009-10-01', 'Anything that couldn\'t be targeted by the original spell (due to shroud, protection abilities, targeting restrictions, or any other reason) is just ignored by Glimmervoid Basin\'s first ability.').
card_ruling('glimmervoid basin', '2009-10-01', 'The controller of the spell that caused Glimmervoid Basin\'s first ability to trigger also controls all the copies. That player chooses the order the copies are put onto the stack. The original spell will be on the stack beneath those copies and resolves last.').
card_ruling('glimmervoid basin', '2009-10-01', 'The copies that Glimmervoid Basin\'s first ability creates are created on the stack, so they\'re not \"cast.\" Abilities that trigger when a player casts a spell (like Glimmervoid Basin\'s first ability itself) won\'t trigger.').
card_ruling('glimmervoid basin', '2009-10-01', 'If the spell that\'s copied is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. Its controller can\'t choose a different one.').
card_ruling('glimmervoid basin', '2009-10-01', 'If the spell that\'s copied has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('glimmervoid basin', '2009-10-01', 'The controller of a copy can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too.').
card_ruling('glimmervoid basin', '2009-10-01', 'As a token is created by the {C} ability, it checks the printed values of the creature it\'s copying, as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, and so on.').

card_ruling('glimpse the sun god', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('glimpse the sun god', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('glimpse the sun god', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('glimpse the sun god', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('glimpse the sun god', '2014-02-01', 'If you choose zero targets, you’ll just scry 1 when the spell resolves. However, if you choose at least one target and all of Glimpse the Sun God’s targets are illegal as it tries to resolve, the spell will be countered and none of its effects will happen. You won’t scry in that case.').

card_ruling('glint hawk', '2011-01-01', 'You may choose to sacrifice Glint Hawk as its triggered ability resolves even if you control an artifact.').
card_ruling('glint hawk', '2011-01-01', 'You choose which artifact to return to its owner\'s hand as the triggered ability resolves. If you control no artifacts at that time, you must sacrifice Glint Hawk. Although players can respond to the ability, once it starts to resolve and you choose an artifact you control to return to its owner\'s hand, it\'s too late for players to respond.').

card_ruling('glint hawk idol', '2011-01-01', 'The creature type \"Bird\" was inadvertently omitted from each of Glint Hawk Idol\'s abilities. It has received errata to correct this omission; when either of its abilities resolves, it will become a Bird artifact creature.').
card_ruling('glint hawk idol', '2011-01-01', 'If Glint Hawk Idol becomes a creature before you begin a turn with it under your control, it will be affected by \"summoning sickness.\"').
card_ruling('glint hawk idol', '2011-01-01', 'If Glint Hawk Idol and another artifact enter the battlefield under your control at the same time, Glint Hawk Idol\'s first ability will trigger. You may have it become a creature, but since it will be affected by \"summoning sickness,\" it won\'t be able to attack that turn.').
card_ruling('glint hawk idol', '2011-01-01', 'If either of Glint Hawk Idol\'s two abilities resolve while it\'s a creature, that ability will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').
card_ruling('glint hawk idol', '2011-01-01', 'For example, say one of Glint Hawk Idol\'s abilities has resolved and it\'s a 2/2 creature. You cast Giant Growth targeting it. It\'s now 5/5. Then Diminish (\"Target creature becomes 1/1 until end of turn\") is cast targeting it. Once Diminish resolves, Glint Hawk Idol would be 4/4. Activating Glint Hawk Idol\'s last ability at this point would make it 5/5 again until end of turn.').

card_ruling('glissa sunseeker', '2004-12-01', 'The artifact\'s converted mana cost must be exactly equal to the amount of mana in your mana pool when the ability resolves. If there\'s less mana or more mana, the artifact won\'t be destroyed.').
card_ruling('glissa sunseeker', '2004-12-01', 'The mana has to be in your mana pool before the ability resolves. The ability doesn\'t allow you to activate mana abilities while it\'s resolving.').

card_ruling('glissa, the traitor', '2011-06-01', 'If an artifact you own and a creature an opponent controls go to the graveyard at the same time, you may choose that artifact card as the target of Glissa, the Traitor\'s ability (even if they\'re the same card: an artifact creature you own that an opponent controlled).').

card_ruling('glistening oil', '2011-06-01', 'You choose a target creature for Glistening Oil as you cast it, just as you would with any Aura. If that creature is an illegal target when Glistening Oil tries to resolve, Glistening Oil will be countered and be put into its owner\'s graveyard. It won\'t enter the battlefield and its last ability won\'t trigger.').
card_ruling('glistening oil', '2011-06-01', 'The last ability will trigger no matter how Glistening Oil is put into the graveyard from the battlefield, not just when the enchanted creature is put into the graveyard.').
card_ruling('glistening oil', '2011-06-01', 'Multiple instances of infect are redundant.').

card_ruling('glittering wish', '2007-05-01', 'The choice is optional.').
card_ruling('glittering wish', '2007-05-01', 'If you fail to find something, you still exile this.').
card_ruling('glittering wish', '2009-10-01', 'This works like the Wishes from the _Judgment_(TM) set. In a sanctioned event, you may choose a card from your sideboard. You choose which card to get as the spell resolves.').
card_ruling('glittering wish', '2009-10-01', 'You can\'t have Glittering Wish retrieve itself. You can have it retrieve a Glittering Wish from your sideboard.').
card_ruling('glittering wish', '2009-10-01', 'You can\'t get an exiled card because those cards are still in one of the game zones.').

card_ruling('global ruin', '2004-10-04', 'If a land counts as multiple basic land types, you can choose it for either or for both of those land types.').
card_ruling('global ruin', '2010-08-15', 'You sacrifice all lands other than the chosen ones. Since you can only choose lands with basic land types, any non-basic lands that don\'t have a basic land type (even if they have some other land type, as in the case of Cloudpost) are necessarily going to be sacrificed.').

card_ruling('gloom', '2004-10-04', 'The extra mana is not considered part of the activation cost. It is a separate cost.').
card_ruling('gloom', '2004-10-04', 'The additional mana is paid at the same time the spell/ability is cast/activated.').
card_ruling('gloom', '2004-10-04', 'The extra mana is part of the total cost, so cost reducers can be applied to this cost.').
card_ruling('gloom', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('gloom surgeon', '2012-05-01', 'Combat damage that would be dealt to Gloom Surgeon will be prevented even if you can\'t exile that many cards from the top of your library.').
card_ruling('gloom surgeon', '2012-05-01', 'If damage that can\'t be prevented (such as the damage Malignus deals) is dealt to Gloom Surgeon, you\'ll still exile cards from your library.').

card_ruling('glorious charge', '2009-10-01', 'Glorious Charge affects only creatures you control at the time it resolves. It won\'t affect creatures that come under your control later in the turn. It won\'t affect permanents that become creatures later in the turn.').

card_ruling('glory of warfare', '2009-05-01', 'There is never a point at which Glory of Warfare will give a creature you control +2/+2 or +0/+0. A creature that\'s normally 1/1, for example, will change from being 3/1 to being 1/3 when your turn ends and another player\'s turn begins. At no time will it be 3/3 or 1/1.').

card_ruling('gluttonous cyclops', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('gluttonous cyclops', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('gluttonous cyclops', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('glyph of delusion', '2004-10-04', 'Can be cast at any time after the wall blocked a creature and can affect any creature that the wall was assigned to block or that was blocked by the wall due to being in a band or by being switched into or out of the block by an ability.').

card_ruling('glyph of life', '2004-10-04', 'You can cast this targeting your opponent\'s walls to good effect.').
card_ruling('glyph of life', '2004-10-04', 'This is not redirection of damage. The wall still takes the damage.').

card_ruling('glyph of reincarnation', '2004-10-04', 'First place all the affected creatures in the graveyard, then choose the appropriate number of creatures from the attacker\'s graveyard and put them onto the battlefield. Some or all of them may be the same creatures which were just destroyed.').
card_ruling('glyph of reincarnation', '2004-10-04', 'Yes, you can cast this targeting your opponent\'s Walls to good effect.').
card_ruling('glyph of reincarnation', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('gnarled scarhide', '2014-04-26', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('gnarled scarhide', '2014-04-26', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('gnarled scarhide', '2014-04-26', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('gnarled scarhide', '2014-04-26', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('gnarled scarhide', '2014-04-26', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('gnarled scarhide', '2014-04-26', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('gnarled scarhide', '2014-04-26', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('gnarled scarhide', '2014-04-26', 'You still control the Aura, even if it’s enchanting a creature controlled by another player.').
card_ruling('gnarled scarhide', '2014-04-26', 'If the enchanted creature leaves the battlefield, the Aura stops being an Aura and remains on the battlefield. Control of that permanent doesn’t change; you’ll control the resulting enchantment creature.').
card_ruling('gnarled scarhide', '2014-04-26', 'Similarly, if you cast an Aura spell with bestow targeting a creature controlled by another player, and that creature is an illegal target when the spell tries to resolve, it will finish resolving as an enchantment creature spell. It will enter the battlefield under your control.').

card_ruling('gnat alley creeper', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) can block this because they don\'t actually have Flying.').

card_ruling('gnat miser', '2009-10-01', 'If multiple effects modify a player\'s hand size, apply them in timestamp order. For example, if you put Cursed Rack (an artifact that sets a chosen player\'s maximum hand size to four) onto the battlefield choosing your opponent and then put Gnat Miser onto the battlefield, your opponent will have a maximum hand size of three. However, if those permanents entered the battlefield in the opposite order, your opponent\'s maximum hand size would be four.').

card_ruling('go for the throat', '2011-06-01', 'If the targeted creature is also an artifact when Go for the Throat tries to resolve, it will be countered.').

card_ruling('goatnapper', '2007-10-01', 'Yes, target Goat. Goat is a creature type. Keep in mind that each permanent with changeling is a Goat, as is any permanent affected by a spell or ability that gives it every creature type.').

card_ruling('goblin artillery', '2009-10-01', 'If the targeted creature or player is an illegal target by the time Goblin Artillery\'s ability would resolve, the entire ability is countered. You won\'t be dealt any damage.').

card_ruling('goblin assassin', '2004-10-04', 'This coin flip has no winner or loser.').

card_ruling('goblin assault', '2008-10-01', 'The second ability affects all Goblin creatures controlled by all players. It\'s not limited to the tokens put onto the battlefield by the first ability.').

card_ruling('goblin balloon brigade', '2008-08-01', 'You can activate the ability repeatedly during a turn. This generally has no additional effect, unless some other effect (like that from Adarkar Windform has caused it to lose flying since the last time the ability resolved.').
card_ruling('goblin balloon brigade', '2010-08-15', 'For flying to work as an evasion ability, an attacking Goblin Balloon Brigade\'s ability must be activated before the declare blockers step begins. Once Goblin Balloon Brigade has become blocked, giving it flying won\'t change that.').

card_ruling('goblin bangchuckers', '2011-09-22', 'You choose the target creature or player when you activate the ability. You don\'t flip the coin until the ability resolves. Players may respond to the ability, but they won\'t know the results of the coin flip.').
card_ruling('goblin bangchuckers', '2011-09-22', 'If the creature or player is an illegal target when Goblin Bangchuckers\' ability tries to resolve, it will be countered and none of its effects will happen. You won\'t flip a coin and no damage will be dealt.').

card_ruling('goblin bomb', '2004-10-04', 'Nothing happens if you are instructed to remove a fuse counter from an empty card.').

card_ruling('goblin brawler', '2004-12-01', 'You can activate an equip ability that targets Goblin Brawler, but the Equipment will fail to move onto it.').

card_ruling('goblin bushwhacker', '2009-10-01', 'Only creatures you control as Goblin Bushwhacker\'s ability resolves are affected. This includes Goblin Bushwhacker itself, unless it already left the battlefield by then.').

card_ruling('goblin cannon', '2004-12-01', 'You can activate Goblin Cannon\'s ability multiple times in response to the first activation. Goblin Cannon will be sacrificed the first time the ability resolves, but each activation still deals damage when it resolves.').

card_ruling('goblin caves', '2004-10-04', 'Works even if placed on one of your opponent\'s Mountains.').
card_ruling('goblin caves', '2004-10-04', 'Works if enchanting a Snow-Covered Mountain.').

card_ruling('goblin chirurgeon', '2004-10-04', 'Can sacrifice itself.').
card_ruling('goblin chirurgeon', '2007-09-16', 'Can also regenerate itself. If it sacrifices itself in an attempt to regenerate itself, the ability will be countered for having an illegal target.').

card_ruling('goblin clearcutter', '2004-10-04', 'The ability can grant {G}{G}{G}, {R}{G}{G}, {R}{R}{G}, or {R}{R}{R}.').

card_ruling('goblin diplomats', '2013-07-01', 'The controller of each creature still decides which player or planeswalker that creature attacks.').
card_ruling('goblin diplomats', '2013-07-01', 'Creatures that enter the battlefield (or noncreature permanents that become creatures) after the ability resolves are affected by the ability and must attack if able.').
card_ruling('goblin diplomats', '2013-07-01', 'If, during a player’s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under that player’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, the player isn’t forced to pay that cost, so it doesn’t have to attack in that case either.').

card_ruling('goblin electromancer', '2012-10-01', 'Two Goblin Electromancers will make instant and sorcery spells you cast cost {2} less to cast, and so on.').
card_ruling('goblin electromancer', '2012-10-01', 'Goblin Electromancer can\'t reduce the colored mana requirement of an instant or sorcery spell.').
card_ruling('goblin electromancer', '2012-10-01', 'If there are additional costs to cast a spell, such as a kicker cost or a cost imposed by another effect (such as the one imposed by Thalia, Guardian of Thraben\'s ability), apply those increases before applying cost reductions.').
card_ruling('goblin electromancer', '2012-10-01', 'Goblin Electromancer can reduce alternative costs such as overload costs.').

card_ruling('goblin festival', '2004-10-04', 'If you activate this multiple times in a row so the stack has more than one of this ability on it, the abilities will still resolve even if Goblin Festival leaves the battlefield or leaves your control before they resolve. For example, if activate the ability twice, and after the first flip you lose control of it because you lost the toss, or it leaves the battlefield, then you still flip for the second ability.').
card_ruling('goblin festival', '2004-10-04', 'The controller of the ability chooses one of his or her opponents. If this card changes controllers before the ability resolves, you still pick an opponent of the player who controlled it when it was activated.').

card_ruling('goblin fire fiend', '2005-10-01', 'If Goblin Fire Fiend is attacking, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block Goblin Fire Fiend.').

card_ruling('goblin firestarter', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('goblin flectomancer', '2006-02-01', 'The ability can target any instant or sorcery spell, even if it has no targets.').
card_ruling('goblin flectomancer', '2006-02-01', 'If the spell has multiple targets, you may either change all the targets or none of them. Each target is treated individually, and must be changed to a different legal target. For example, Seeds of Strength targeting Atog (target #1), the same Atog (target #2), and Scryb Sprites (target #3) can be changed so it targets the same Scryb Sprites (target #1), Eager Cadet (target #2), and Alpha Myr (target #3). If changing one of the targets would be impossible, then you can\'t change any of the others.').
card_ruling('goblin flectomancer', '2006-02-01', 'If a spell has a variable number of targets (such as Electrolyze), the number of targets chosen can\'t be changed.').

card_ruling('goblin furrier', '2006-07-15', 'If a spell or ability is cast or activated that would have Goblin Furrier deal damage to a snow creature, and Goblin Furrier leaves the battlefield before it resolves, the damage won\'t be prevented.').

card_ruling('goblin game', '2004-10-04', 'When it comes to choice of items, use common sense. Items should be small enough to hide but large enough to count. Make it clear what kind of object you are hiding beforehand. For example, one player may choose coins and the other may choose dice. If you can\'t find something convenient to hide, write numbers on a piece of paper and reveal the numbers. Make sure the numbers are unambiguous (for example, underline a 6 or 9 so it can\'t be misread).').
card_ruling('goblin game', '2007-02-01', 'If you attempt to halve a negative life total, you halve 0. This means that the life total stays the same. A life total of -10 would remain -10.').

card_ruling('goblin gardener', '2004-10-04', 'The ability is not optional. You have to choose one of your own lands if your opponent has none.').

card_ruling('goblin gaveleer', '2011-01-01', 'Goblin Gaveleer\'s bonus is in addition to whatever bonus the Equipment gives it.').

card_ruling('goblin glory chaser', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('goblin glory chaser', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('goblin glory chaser', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').
card_ruling('goblin glory chaser', '2015-06-22', 'Once an attacking creature with menace is legally blocked by two or more creatures, removing one or more of those blockers from combat won’t change or undo that block.').

card_ruling('goblin grenade', '2004-10-04', 'You can\'t sacrifice more than one Goblin to get a greater effect.').
card_ruling('goblin grenade', '2011-09-22', 'Players can only respond once Goblin Grenade has been cast and all its costs have been paid. No one can try and destroy the Goblin to prevent you from casting Goblin Grenade.').
card_ruling('goblin grenade', '2011-09-22', 'The Goblin you sacrifice to cast Goblin Grenade doesn\'t have to be a creature. For example, you could sacrifice Boggart Shenanigans (a tribal enchantment with the subtype Goblin).').

card_ruling('goblin grenadiers', '2008-04-01', 'Both a creature and a land must be targeted when the ability triggers. If there aren\'t enough legal targets, the ability doesn\'t go on the stack and Goblin Grenadiers can\'t be sacrificed this way.').
card_ruling('goblin grenadiers', '2008-04-01', 'A permanent that\'s both a land and a creature may be chosen as both of this ability\'s targets.').
card_ruling('goblin grenadiers', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('goblin heelcutter', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('goblin heelcutter', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('goblin heelcutter', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('goblin heelcutter', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('goblin kaboomist', '2014-07-18', 'You flip the coin after the Land Mine token is created, not after you sacrifice a Land Mine. The token is created even if you lose the flip.').
card_ruling('goblin kaboomist', '2014-07-18', 'The Land Mine tokens have the ability even if Goblin Kaboomist blows himself up or otherwise leaves the battlefield.').

card_ruling('goblin king', '2005-08-01', 'Goblin King now has the Goblin creature type and its ability has been reworded to affect *other* Goblins. This means that if two Goblin Kings are on the battlefield, each gives the other a bonus.').

card_ruling('goblin lackey', '2008-08-01', 'A Goblin permanent card is an artifact, creature, land, and/or enchantment card with the creature type \"Goblin\". Usually such cards will be creatures in addition to any other types, but they may be tribal instead. For example, you could put Boggart Shenanigans onto the battlefield with this effect.').

card_ruling('goblin machinist', '2004-10-04', 'If all the cards in your library are lands, X is zero.').

card_ruling('goblin masons', '2008-08-01', 'If there is no Wall to target, then the ability just gets removed from the stack before any player gets priority.').

card_ruling('goblin matron', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('goblin matron', '2008-04-01', 'You can search for any card with the creature type Goblin, including Creature or Tribal cards. Older cards of type \"Summon Goblin\" and \"Summon Goblins\" also count. You can\'t search for cards that have Goblin in the name but don\'t have the creature type Goblin.').

card_ruling('goblin medics', '2004-10-04', 'The ability does trigger when it taps to attack.').

card_ruling('goblin offensive', '2004-10-04', 'The tokens are named \"Goblin\" and are of creature type \"Goblin\".').

card_ruling('goblin piledriver', '2004-10-04', 'The number of Goblins is counted when this ability resolves.').

card_ruling('goblin psychopath', '2004-10-04', 'The ability triggers when it attacks or block and resolves before combat damage is assigned. This gives you a chance to possibly get rid of it before it hurts you.').

card_ruling('goblin pyromancer', '2004-10-04', 'It only destroys goblins if it is on the battlefield at the end of turn.').

card_ruling('goblin rabblemaster', '2014-07-18', 'Although Goblin Rabblemaster doesn’t force itself to attack, if you control two of them, they’ll force each other to attack if able.').
card_ruling('goblin rabblemaster', '2014-07-18', 'If, during your declare attackers step, a creature that must attack if able is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('goblin rabblemaster', '2014-07-18', 'The number of attacking Goblins is counted as the last ability resolves, and the bonus is locked in at that time.').

card_ruling('goblin razerunners', '2009-02-01', 'You don\'t count how many +1/+1 counters are on Goblin Razerunners until its triggered ability resolves.').
card_ruling('goblin razerunners', '2009-02-01', 'If Goblin Razerunners leaves the battlefield after its ability triggers but before it resolves, the amount of damage it deals is equal to the number of +1/+1 counters that were on it as it last existed on the battlefield.').

card_ruling('goblin recruiter', '2008-04-01', 'You can search for any card with the creature type Goblin, including Creature or Tribal cards. Older cards of type \"Summon Goblin\" and \"Summon Goblins\" also count. You can\'t search for cards that have Goblin in the name but don\'t have the creature type Goblin.').

card_ruling('goblin sappers', '2013-04-15', 'If you activate the ability during the end of combat step, the creature will be destroyed at the beginning of the next end of combat step, even if that step occurs during a different turn.').

card_ruling('goblin scouts', '2004-10-04', 'The tokens are of creature type Goblin and Scout. They are affected by things that affect Goblins.').
card_ruling('goblin scouts', '2008-08-01', 'The tokens have \'Mountainwalk\' in their text, and will be affected by text-changing effects accordingly.').

card_ruling('goblin shrine', '2004-10-04', 'Works even if placed on one of your opponent\'s Mountains.').

card_ruling('goblin ski patrol', '2008-10-01', 'Goblin Ski Patrol\'s ability may be activated only once in the entire game. If its ability is activated and then it changes controllers, its ability may not be activated again, and its new controller will still have to sacrifice it at the beginning of the End step.').

card_ruling('goblin spy', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('goblin spy', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('goblin spy', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('goblin test pilot', '2013-04-15', 'To choose a target at random, all possible legal targets (including creatures and players) must have an equal chance of being chosen. There are many ways to do this, including assigning each possible legal target a number and rolling a die.').
card_ruling('goblin test pilot', '2013-04-15', 'The target is chosen at random as part of activating the ability. Players can respond to the ability knowing what the target of the ability is.').
card_ruling('goblin test pilot', '2013-04-15', 'Goblin Test Pilot is a legal target of its own ability.').

card_ruling('goblin trenches', '2004-10-04', 'The tokens are named \"Goblin Soldier\" and have the creature types \"Goblin\" and \"Soldier\".').

card_ruling('goblin tunneler', '2010-06-15', 'The power of the targeted creature is checked both as you target it and as the ability resolves. After the ability resolves, the creature can\'t be blocked that turn even if its power becomes greater than 2.').
card_ruling('goblin tunneler', '2010-06-15', 'The ability doesn\'t grant an ability to the targeted creature. Rather, it affects the game rules and states something that\'s now true about that creature. After the ability resolves, the creature can\'t be blocked that turn even if it loses all abilities.').

card_ruling('goblin vandal', '2008-04-01', 'If the defending player controls no artifacts that you can target, the ability doesn\'t go on the stack and you can\'t pay {R}.').
card_ruling('goblin vandal', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('goblin war drums', '2004-10-04', 'When combined with an effect that reads \"Each creature you control can\'t be blocked by more than one creature,\" none of your creatures can be blocked.').
card_ruling('goblin war drums', '2004-10-04', 'Multiple War Drums are not cumulative. They are redundant.').

card_ruling('goblin warrens', '2004-10-04', 'The token Goblins can be sacrificed to the Warrens to generate new Goblins.').

card_ruling('goblin welder', '2008-08-01', 'As the new wording makes clear, both targets must still be legal as the ability resolves in order for it to do anything. If either one or both has become illegal, nothing gets sacrificed and nothing gets returned to the battlefield.').

card_ruling('goblin wizard', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('goblinslide', '2014-09-20', 'You make the decision whether to pay {1} as the ability resolves.').

card_ruling('god-favored general', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('god-favored general', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('god-favored general', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('god-favored general', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('godhead of awe', '2008-05-01', 'This doesn\'t cause creatures to lose their abilities.').
card_ruling('godhead of awe', '2008-05-01', 'If two Godheads of Awe are on the battlefield, they\'ll each make the other one 1/1.').
card_ruling('godhead of awe', '2009-10-01', 'Godhead of Awe\'s ability overwrites other effects that set a creature\'s power and toughness to specific values only if those effects existed before the Godhead entered the battlefield. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the Godhead enters the battlefield. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including that of the Godhead.').

card_ruling('godhunter octopus', '2014-04-26', 'Whether the defending player controls an enchantment or enchanted permanent is checked only as attackers are declared. Once Godhunter Octopus has been declared as an attacker, it will continue to attack even if the defending player loses control of each enchantment or enchanted permanent he or she controlled.').

card_ruling('godless shrine', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('godless shrine', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('godless shrine', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('godo, bandit warlord', '2004-12-01', 'Unlike other effects that grant additional combat phases, you don\'t get an additional main phase with Godo, Bandit Warlord. The additional combat phase happens immediately after the first combat phase.').

card_ruling('gods willing', '2013-09-15', 'You choose the color as Gods Willing resolves. Once the color is chosen, it’s too late for players to respond.').
card_ruling('gods willing', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('gods willing', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('gods willing', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('gods willing', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('godsend', '2014-04-26', 'Godsend’s triggered ability will trigger only once no matter how many creatures the equipped creature blocks or becomes blocked by.').
card_ruling('godsend', '2014-04-26', 'Godsend’s triggered ability doesn’t target any creature. You could exile a creature with protection from white, for example. You choose which creature to exile when the ability resolves.').
card_ruling('godsend', '2014-04-26', 'If Godsend leaves the battlefield, and later another Godsend enters the battlefield, it is a new object (even if the two were represented by the same card). The new Godsend won’t stop opponents from casting cards with the same name as cards exiled with the first one.').
card_ruling('godsend', '2014-04-26', 'If Godsend exiles a land card (perhaps because it was turned into a creature and then blocked or was blocked by the equipped creature), opponents can still play land cards with the same name as the exiled card. Godsend stops only the casting of spells.').

card_ruling('godtoucher', '2008-10-01', 'The ability checks the targeted creature\'s power twice: when the creature becomes the target, and when the ability resolves. Once the ability resolves, it will continue to apply to the affected creature no matter what its power may become later in the turn.').

card_ruling('golden urn', '2011-01-01', 'As Golden Urn\'s last ability resolves, its last existence on the battlefield is checked to determine how many charge counters were on it.').

card_ruling('golden wish', '2004-10-04', 'Can\'t acquire the Ante cards. They are considered still \"in the game\" as are cards in the library and the graveyard.').
card_ruling('golden wish', '2007-09-16', 'Can\'t acquire cards that are phased out.').
card_ruling('golden wish', '2009-10-01', 'You can\'t acquire exiled cards because those cards are still in one of the game\'s zones.').
card_ruling('golden wish', '2009-10-01', 'In a sanctioned event, a card that\'s \"outside the game\" is one that\'s in your sideboard. In an unsanctioned event, you may choose any card from your collection.').

card_ruling('goldenhide ox', '2014-04-26', 'The defending player must assign at least one blocker to the target creature during the declare blockers step if that player controls any creatures that could block it.').
card_ruling('goldenhide ox', '2014-04-26', 'The constellation ability doesn’t force any specific creature to block the target creature.').
card_ruling('goldenhide ox', '2014-04-26', 'If multiple attacking creatures must be blocked if able, the defending player must assign at least one blocker to each of them if possible. For example, if two such creatures were attacking and there were two potential blockers, they couldn’t both be assigned to block the same attacker.').
card_ruling('goldenhide ox', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('goldenhide ox', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('goldenhide ox', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('goldmeadow', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('goldmeadow', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('goldmeadow', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('goldmeadow', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('goldmeadow dodger', '2007-10-01', 'After Goldmeadow Dodger becomes blocked, increasing the blocking creature\'s power to 4 or greater has no effect on the block.').

card_ruling('goldnight commander', '2012-05-01', 'The creature that entered the battlefield will also get +1/+1 until end of turn if it\'s still on the battlefield when the ability resolves.').

card_ruling('goldnight redeemer', '2012-05-01', 'The amount of life you gain is based on the number of other creatures you control when the ability resolves.').

card_ruling('golem artisan', '2011-01-01', 'Both of Golem Artisan\'s abilities may target any artifact creature, including itself.').
card_ruling('golem artisan', '2011-01-01', 'You don\'t choose whether the targeted artifact creature gains flying, trample, or haste until Golem Artisan\'s second ability resolves.').

card_ruling('golem foundry', '2011-01-01', 'Whenever you cast an artifact spell, Golem Foundry\'s first ability triggers and goes on the stack on top of it. It will resolve before the artifact spell does.').

card_ruling('golem\'s heart', '2011-01-01', 'Unlike Golem Foundry and the Smith cycle, Golem\'s Heart\'s ability triggers whenever any player casts an artifact spell, not just when you do.').
card_ruling('golem\'s heart', '2011-01-01', 'Whenever a player casts an artifact spell, Golem\'s Heart\'s ability triggers and goes on the stack on top of it. It will resolve before the artifact spell does.').

card_ruling('golgari brownscale', '2005-10-01', 'The first ability triggers when Golgari Brownscale returns to your hand from your graveyard for any reason, not just when you use its dredge ability.').
card_ruling('golgari brownscale', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('golgari brownscale', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('golgari brownscale', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('golgari decoy', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('golgari grave-troll', '2005-10-01', 'If an effect puts Golgari Grave-Troll onto the battlefield from your graveyard, Golgari Grave-Troll counts itself as one of the cards in your graveyard and gets a counter accordingly.').
card_ruling('golgari grave-troll', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('golgari grave-troll', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('golgari grave-troll', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('golgari guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('golgari guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('golgari keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('golgari keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('golgari rot farm', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('golgari thug', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('golgari thug', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('golgari thug', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('gomazoa', '2009-10-01', 'Gomazoa\'s ability can be activated any time its controller has priority. It doesn\'t need to be blocking any creatures.').
card_ruling('gomazoa', '2009-10-01', 'If Gomazoa isn\'t blocking a creature at the time its ability resolves, only Gomazoa is put on top of its owner\'s library, and only that player shuffles his or her library.').

card_ruling('gore vassal', '2011-06-01', 'You can activate Gore Vassal\'s ability targeting itself, but it won\'t get a -1/-1 counter or regenerate because it\'ll be an illegal target when the ability resolves.').
card_ruling('gore vassal', '2011-06-01', 'The regeneration shield will last until the turn ends or until it\'s used.').
card_ruling('gore vassal', '2011-06-01', 'Let\'s say you activate Gore Vassal\'s ability targeting a creature with 3 toughness and 2 damage marked on it. The regeneration shield will be created just before the creature is destroyed for having damage marked on it equal to or greater than its toughness. The creature will then regenerate. That is, all damage will be removed from it and it will become tapped; any counters on the creature, including the -1/-1 counter put on by this ability, remain.').

card_ruling('gore-house chainwalker', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('gore-house chainwalker', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('gore-house chainwalker', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('gore-house chainwalker', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('gorilla shaman', '2008-10-01', 'A tip on how this math works: The cost to destroy a noncreature artifact with converted mana cost 0 is {0} + {0} + {1} = {1}. The cost to destroy a noncreature artifact with converted mana cost 1 is {1} + {1} + {1} = {3}. The cost to destroy a noncreature artifact with converted mana cost 2 is {2} + {2} + {1} = {5}. And so on.').

card_ruling('goryo\'s vengeance', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('goryo\'s vengeance', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('goryo\'s vengeance', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('goryo\'s vengeance', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('gossamer chains', '2004-10-04', 'The return of this card to your hand is part of the activation cost and is paid on activation. You do not have a choice to pay this cost zero times or more than one time in order to multiply the effect.').
card_ruling('gossamer chains', '2004-10-04', 'A creature is only considered an \"unblocked creature\" if it is an attacker during combat after blockers are declared.').

card_ruling('gossamer phantasm', '2004-10-04', 'The sacrifice is triggered on the announcement of a spell that targets it and is placed on the stack before responses can be announced.').
card_ruling('gossamer phantasm', '2007-02-01', 'This is the timeshifted version of Skulking Ghost.').

card_ruling('grab the reins', '2004-12-01', 'If you pay the entwine cost, you can sacrifice the creature you gain control of with Grab the Reins.').
card_ruling('grab the reins', '2004-12-01', 'If you choose the \"sacrifice a creature\" mode, you choose the target of the damage on announcement, but don\'t choose what to sacrifice until resolution. You must sacrifice a creature when Grab the Reins resolves, even if you don\'t want to. If you don\'t control any creatures at that time, Grab the Reins deals no damage.').
card_ruling('grab the reins', '2004-12-01', 'If you cast this with Entwine, you don\'t get priority in the middle of resolution. You can\'t steal something, do anything (like attack with it), and then sacrifice it.').

card_ruling('graceful adept', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Graceful Adept onto the battlefield, you\'ll have no maximum hand size. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('graceful reprieve', '2008-04-01', 'This effect works once. If the targeted creature leaves the battlefield and is then returned to the battlefield, it\'s considered a different object. If that permanent is put into a graveyard, it won\'t come back a second time.').
card_ruling('graceful reprieve', '2008-04-01', 'Graceful Reprieve can target a token creature, but since token creatures cease to exist after they leave the battlefield, it won\'t be returned to the battlefield.').

card_ruling('grafdigger\'s cage', '2011-01-22', 'If a creature card tries to enter the battlefield from a graveyard or library, it stays in its current zone.').
card_ruling('grafdigger\'s cage', '2013-04-15', 'The first ability checks only whether the card is a creature card in the graveyard or library. A card such as Sculpting Steel can still enter the battlefield from a graveyard or library as a copy of a creature.').
card_ruling('grafdigger\'s cage', '2013-04-15', 'The second ability doesn\'t stop players from playing land cards from graveyards or libraries since lands aren\'t cast.').

card_ruling('grafted exoskeleton', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('grafted exoskeleton', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('grafted exoskeleton', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('grafted exoskeleton', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('grafted exoskeleton', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('grafted exoskeleton', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('grafted exoskeleton', '2011-01-01', 'In addition to the effect of its equip ability, Grafted Exoskeleton becomes unattached from the creature it\'s equipping if a spell or ability (such as Fulgent Distraction) causes it to become unattached, if Grafted Exoskeleton leaves the battlefield, if the equipped creature ceases to be a creature, or if Grafted Exoskeleton ceases to be an Equipment. (It also becomes unattached if the equipped creature leaves the battlefield, but the triggered ability won\'t do anything in that case.)').
card_ruling('grafted exoskeleton', '2011-01-01', 'If Grafted Exoskeleton\'s last ability triggers, but you don\'t control the permanent it became unattached from at the time that ability resolves (perhaps because another player has somehow gained control of it), you won\'t be able to sacrifice it.').

card_ruling('grafted wargear', '2004-12-01', 'The \"becomes unattached\" ability triggers if (a) Grafted Wargear leaves the battlefield, (b) the equip ability moves it onto another creature, (c) another effect moves it onto another creature, (d) an effect causes it to become unattached, or (e) the creature leaves the battlefield.').
card_ruling('grafted wargear', '2004-12-01', 'Note that if (e) happens, the creature won\'t be on the battlefield to be sacrificed. In all cases, it\'s the creature that was equipped when the ability triggered that will be sacrificed.').
card_ruling('grafted wargear', '2004-12-01', 'If your Grafted Wargear somehow ends up on an opponent\'s creature and it then becomes unattached from that creature, you won\'t be able to sacrifice that creature because you don\'t control it.').
card_ruling('grafted wargear', '2013-04-15', 'You may activate this card\'s equip ability targeting the creature that it is currently attached to, even if it\'s the only creature you control. Since the equip ability costs zero, you can do this as many times as you like during your main phases, though it generally won\'t do anything.').

card_ruling('grand abolisher', '2011-09-22', 'Grand Abolisher doesn\'t stop your opponents from activating abilities of artifact, creature, or enchantment cards in zones other than the battlefield (like cycling abilities, for example).').
card_ruling('grand abolisher', '2011-09-22', 'Grand Abolisher doesn\'t affect triggered abilities or static abilities.').

card_ruling('grand arbiter augustin iv', '2006-05-01', 'Spells you cast that are both blue and white cost {2} less to cast.').
card_ruling('grand arbiter augustin iv', '2006-05-01', 'This reduces only generic mana portion of mana costs. A spell you cast with mana cost {1}{W}{U} will cost {W}{U} to cast.').

card_ruling('grand architect', '2011-01-01', 'Other blue creatures you control will get +1/+1 whether they are artifacts or not.').
card_ruling('grand architect', '2011-01-01', 'Turning an artifact creature blue with the second ability will override any other color(s) the creature previously had. It will still be an artifact. You may target a blue artifact creature with this ability.').
card_ruling('grand architect', '2011-01-01', 'Since Grand Architect\'s last ability doesn\'t have a tap symbol in its cost, you can tap a blue creature (including Grand Architect itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('grand ossuary', '2012-06-01', 'You choose the number of target creatures and declare how you are distributing the counters as you put the first ability on the stack. Each target must receive at least one +1/+1 counter.').
card_ruling('grand ossuary', '2012-06-01', 'Any target that is illegal when the first ability resolves won\'t get any +1/+1 counters. Those counters are essentially lost; you won\'t get to put them on another creature.').
card_ruling('grand ossuary', '2012-06-01', 'For the chaos ability, the total power of the creatures a player exiled is the total power of those creatures as they last existed on the battlefield, including any bonuses from +1/+1 counters, Auras, Equipment, and other applicable effects.').

card_ruling('granite shard', '2004-10-04', 'You can pay either of the two costs (but not both at the same time) to activate the ability.').

card_ruling('grapeshot', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('grapeshot', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('grapeshot', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('grapeshot', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('grapeshot', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('grappling hook', '2009-10-01', 'If you control the triggered ability, you decide whether to have the targeted creature block the attacking creature at the time the ability resolves, not at the time blockers are declared.').
card_ruling('grappling hook', '2009-10-01', 'If you choose to have the targeted creature block, the creature it must block is the one that was equipped by Grappling Hook at the time the ability triggered. It doesn\'t matter if Grappling Hook leaves the battlefield or somehow becomes attached to another creature.').
card_ruling('grappling hook', '2009-10-01', 'If you choose to have the targeted creature block the creature that was equipped with Grappling Hook, but it isn\'t able to do so as blockers are declared (for example, because the attacking creature has flying and the targeted creature doesn\'t), the requirement to block does nothing. The targeted creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('grappling hook', '2009-10-01', 'Tapped creatures, creatures that can\'t block as the result of an effect, creatures with unpaid costs to block (such as those from War Cadence), and creatures that aren\'t controlled by the defending player are exempt from effects that would require them to block. Such creatures can be targeted by the ability, but the requirement to block does nothing.').

card_ruling('grasp of the hieromancer', '2015-06-22', 'The triggered ability granted to the enchanted creature will tap the creature before blockers are declared.').
card_ruling('grasp of the hieromancer', '2015-06-22', 'The enchanted creature is the source of the triggered ability it gains, not Grasp of the Hieromancer. If the enchanted creature isn’t white, that ability can target a creature with protection from white, for example.').

card_ruling('grasslands', '2004-10-04', 'You do not have to find a plains or forest card if you do not want to.').

card_ruling('grave betrayal', '2012-10-01', 'Grave Betrayal doesn\'t overwrite any previous colors or types. Rather, it adds another color and another creature type.').
card_ruling('grave betrayal', '2012-10-01', 'If the creature is normally colorless, it will simply be black. It won\'t be both black and colorless.').
card_ruling('grave betrayal', '2012-10-01', 'If a creature you don\'t control dies during the end step, that creature won\'t return to the battlefield until the beginning of the next end step.').
card_ruling('grave betrayal', '2012-10-01', 'Each creature you don\'t control that dies causes a delayed triggered ability to trigger at the beginning of the next end step. If there are multiple such abilities, you can put them on the stack in any order. The creatures will return to the battlefield one at a time as each ability resolves. The last ability you put on the stack will be the first one to resolve.').
card_ruling('grave betrayal', '2012-10-01', 'If the creature card leaves the graveyard before the delayed triggered ability resolves, it won\'t return to the battlefield. This is also true if the card leaves the graveyard and returns to the graveyard before that ability resolves.').
card_ruling('grave betrayal', '2012-10-01', 'The phrase “with an additional +1/+1 counter on it” means that the creature enters the battlefield with one more +1/+1 counter than it would normally enter the battlefield with. It doesn\'t keep any counters it had on it when it died.').
card_ruling('grave betrayal', '2012-10-01', 'In a multiplayer game, if more than one player controls a Grave Betrayal and a creature controlled by none of those players dies, each Grave Betrayal will create a delayed triggered ability at the beginning of the next end step. The first such ability to resolve will return the creature. Usually, this will be one controlled by the player furthest from the active player in turn order.').

card_ruling('grave birthing', '2015-08-25', 'The target opponent chooses which card to exile as Grave Birthing resolves.').
card_ruling('grave birthing', '2015-08-25', 'You can cast Grave Birthing targeting an opponent with an empty graveyard. Even though that player won’t exile a card from his or her graveyard, you’ll still get an Eldrazi Scion and draw a card.').
card_ruling('grave birthing', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('grave birthing', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('grave birthing', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('grave birthing', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('grave birthing', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('grave birthing', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('grave birthing', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('grave birthing', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('grave birthing', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('grave exchange', '2012-05-01', 'Grave Exchange targets both a creature card in your graveyard and a player. You can\'t cast it unless there is a legal target to choose for both. You can target a player who controls no creatures.').
card_ruling('grave exchange', '2012-05-01', 'If both targets are illegal when Grave Exchange tries to resolve, it will be countered and none of its effects will happen. If only one target is legal, the effect relevant to that target will happen and the other will not.').

card_ruling('grave pact', '2004-10-04', 'Each other player sacrifices a creature for each creature you control that goes to a graveyard.').

card_ruling('grave peril', '2007-05-01', 'This ability is not optional.').
card_ruling('grave peril', '2007-05-01', 'If multiple nonblack creatures enter the battlefield at the same time, Grave Peril will destroy only one of them. Grave Peril\'s ability will trigger multiple times, but only the first one to resolve will do anything.').
card_ruling('grave peril', '2007-05-01', 'If a nonblack creature enters the battlefield and Grave Peril triggers, then another nonblack creature enters the battlefield, Grave Peril will trigger again and destroy only the last creature to enter the battlefield.').

card_ruling('grave robbers', '2004-10-04', 'If the target is not there on resolution, you do not gain the life.').
card_ruling('grave robbers', '2004-10-04', 'This exiles the artifact on resolution, but you choose it as a target when activating the ability.').

card_ruling('grave scrabbler', '2007-05-01', 'The second ability checks if Grave Scrabbler was cast as a result of its madness ability. It doesn\'t matter if the specific cost wasn\'t paid (for example, if the cost was reduced due to a effect like Cloud Key).').
card_ruling('grave scrabbler', '2007-05-01', 'When Grave Scrabbler enters the battlefield, its ability triggers only if it was cast with madness. If it triggers, you must choose a target, but you don\'t decide whether to actually return the card until the ability resolves.').
card_ruling('grave scrabbler', '2007-05-01', 'You can return a card in a different player\'s graveyard to that player\'s hand.').

card_ruling('grave servitude', '2004-10-04', 'The color change lasts only while this card is on the creature.').
card_ruling('grave servitude', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('grave sifter', '2014-11-07', 'The creature type each player chooses applies only to that player. For example, if another player chose Elf, you wouldn’t return Elf cards to your hand unless you also chose Elf (or if an Elf card in your graveyard also had the creature type you chose).').

card_ruling('grave-shell scarab', '2005-10-01', 'You may sacrifice Grave-Shell Scarab to pay for its first ability, then replace that draw using the Scarab\'s dredge ability. The result is that the top card of your library is put into your graveyard and Grave-Shell Scarab returns to your hand (after a brief trip to the graveyard).').

card_ruling('graveblade marauder', '2015-06-22', 'Count the number of creature cards in your graveyard as the ability resolves to determine how much life is lost. Notably, this could include any other creature you owned that died in combat, and perhaps even Graveblade Marauder itself.').

card_ruling('gravecrawler', '2011-01-22', 'Gravecrawler\'s ability doesn\'t change when you could cast it.').
card_ruling('gravecrawler', '2011-01-22', 'Once you\'ve cast Gravecrawler and it\'s on the stack, it doesn\'t matter if you lose control of the other Zombie.').

card_ruling('gravedigger', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('gravelgill axeshark', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('gravelgill axeshark', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('gravelgill axeshark', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('gravelgill axeshark', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('gravelgill axeshark', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('gravelgill axeshark', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('gravelgill axeshark', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('graven cairns', '2007-05-01', '{(b/r)}, {T}\" is the same as saying \"{B}, {T} or {R}, {T}.').

card_ruling('graven dominator', '2006-02-01', 'If Night of Souls\' Betrayal is on the battlefield when Graven Dominator\'s ability resolves, all other creatures will become 0/0 and die.').
card_ruling('graven dominator', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('gravepurge', '2011-01-22', 'You choose the order of the cards you put on top of your library.').
card_ruling('gravepurge', '2011-01-22', 'You can cast Gravepurge with zero targets. If you do, you\'ll draw a card as the spell resolves.').
card_ruling('gravepurge', '2011-01-22', 'If you cast Gravepurge with at least one target and all of those targets become illegal, the spell will be countered and you won\'t get to draw a card.').
card_ruling('gravepurge', '2015-02-25', 'You can cast Gravepurge with no targets. If you do, you’ll just draw a card. However, if you cast it with at least one target and all of those targets become illegal, the spell will be countered and you won’t draw a card.').

card_ruling('graverobber spider', '2014-02-01', 'The number of creature cards in your graveyard is counted only as the ability resolves. Once the ability resolves, the bonus won’t change, even if the number of creature cards in your graveyard changes later in the turn.').

card_ruling('gravespawn sovereign', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('gravespawn sovereign', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('graveyard shovel', '2011-09-22', 'The targeted player chooses which card to exile when the ability resolves.').

card_ruling('gravity sphere', '2004-10-04', 'It removes Flying from all creatures on the battlefield when it enters the battlefield and from each creature as that creature enters the battlefield. It does not prevent the Flying ability being given to the creature *after* the Sphere is on the battlefield.').
card_ruling('gravity sphere', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('gravity well', '2010-06-15', 'This ability triggers and resolves during the declare attackers step. The creature will no longer have flying by the time blockers are declared.').
card_ruling('gravity well', '2010-06-15', 'After attackers are declared and any Gravity Well abilities have resolved, a spell or ability that grants flying to a creature will work normally. This includes other abilities that trigger \"whenever [a creature] attacks\" (such as the one Order of the Golden Cricket has).').

card_ruling('gray merchant of asphodel', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('gray merchant of asphodel', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('gray merchant of asphodel', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('gray merchant of asphodel', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('grazing gladehart', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('grazing gladehart', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('grazing kelpie', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('grazing kelpie', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('grazing kelpie', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('grazing kelpie', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('grazing kelpie', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('grazing kelpie', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('grazing kelpie', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('great sable stag', '2009-10-01', 'Great Sable Stag can be targeted by spells that try to counter it (such as Cancel). Those spells will resolve, but the part of their effect that would counter Great Sable Stag won\'t do anything. Any other effects those spells have will work as normal.').
card_ruling('great sable stag', '2009-10-01', 'Although the reminder text doesn\'t say so, Great Sable Stag also can\'t be equipped by blue or black Equipment.').

card_ruling('great teacher\'s decree', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('great teacher\'s decree', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('great teacher\'s decree', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('great teacher\'s decree', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('great teacher\'s decree', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('great teacher\'s decree', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('great teacher\'s decree', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('great teacher\'s decree', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('great whale', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('great whale', '2004-10-04', 'This does not target the lands.').

card_ruling('greatbow doyen', '2008-04-01', 'The second ability triggers only when an Archer permanent you control deals damage to a creature. It won\'t trigger when an Archer spell you control deals damage.').
card_ruling('greatbow doyen', '2008-04-01', 'The ability will trigger whether the damage is combat damage or not.').

card_ruling('greater auramancy', '2008-05-01', 'If you have two on the battlefield, they\'ll each grant the other one shroud.').

card_ruling('greater gargadon', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('greater gargadon', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('greater gargadon', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('greater gargadon', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('greater gargadon', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('greater gargadon', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('greater gargadon', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('greater gargadon', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('greater gargadon', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('greater gargadon', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('greater good', '2005-08-01', 'You must discard three cards regardless of how many cards you drew. If you have less than three cards in hand, you discard your hand.').
card_ruling('greater good', '2005-08-01', 'You draw cards equal to the creature\'s power when you sacrificed it. This includes any bonuses from effects like Giant Growth.').

card_ruling('greater mossdog', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('greater mossdog', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('greater mossdog', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('greater stone spirit', '2006-07-15', 'After Greater Stone Spirit\'s activated ability is activated, the affected creature gains an activated ability that only that creature\'s controller can activate.').
card_ruling('greater stone spirit', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) can block this because they don\'t actually have Flying.').

card_ruling('green mana battery', '2004-10-04', 'Can be tapped even if it has no counters.').

card_ruling('green sun\'s zenith', '2011-06-01', 'If this spell is countered, none of its effects occur. In particular, it will go to the graveyard rather than to its owner\'s library.').
card_ruling('green sun\'s zenith', '2011-06-01', 'In most cases, if you own Green Sun\'s Zenith and cast it, you\'ll shuffle your library twice. In practice, shuffling once is sufficient, but effects that care about you shuffling your library (like Psychogenic Probe, for example) will see that you\'ve shuffled twice.').
card_ruling('green sun\'s zenith', '2011-06-01', 'If you own Green Sun\'s Zenith, but an opponent casts it (due to Knowledge Pool\'s effect, for example), that opponent searches his or her library for an appropriate creature card, then shuffles his or her library. That opponent then shuffles Green Sun\'s Zenith into your library. You won\'t shuffle any library in this case.').

card_ruling('greener pastures', '2004-10-04', 'The ability does not trigger unless the player has more lands at the beginning of upkeep. This is checked again on resolution, and if the player does not still have more lands, no token is generated.').

card_ruling('greenhilt trainee', '2011-06-01', 'Once Greenhilt Trainee\'s ability is activated, it will resolve as normal even if Greenhilt Trainee\'s power is less than 4 at that time (perhaps because an Equipment attached to it was destroyed).').

card_ruling('greenside watcher', '2013-01-24', 'Greenside Watcher\'s ability is not a mana ability. It uses the stack and can be responded to.').

card_ruling('greenwarden of murasa', '2015-08-25', 'You decide whether to exile Greenwarden of Murasa as the last ability resolves. Players can respond to this ability triggering, but once it starts resolving and you decide to exile Greenwarden of Murasa, it’s too late for anyone to respond.').
card_ruling('greenwarden of murasa', '2015-08-25', 'If the target card becomes illegal before the last ability resolves, it will be countered. You can’t exile Greenwarden of Murasa in that case, even if you want to.').

card_ruling('grenzo\'s cutthroat', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('grenzo\'s cutthroat', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('grenzo\'s cutthroat', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('grenzo\'s cutthroat', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('grenzo\'s rebuttal', '2014-05-29', 'The choices are made in turn order starting with you. However, all chosen permanents are destroyed simultaneously.').
card_ruling('grenzo\'s rebuttal', '2014-05-29', 'None of the chosen permanents are targeted. A player can choose a creature with protection from red, for example.').
card_ruling('grenzo\'s rebuttal', '2014-05-29', 'If a permanent has more than one of the listed types, it can be chosen for any of them. For example, if the player to your left controls an artifact creature, you could choose it as the creature, the artifact, or both.').
card_ruling('grenzo\'s rebuttal', '2014-05-29', 'The player on your right can choose the Ogre that Grenzo’s Rebuttal creates.').

card_ruling('grenzo, dungeon warden', '2014-05-29', 'Compare Grenzo’s power when the ability resolves with the power of the creature card in your graveyard to determine if you put it onto the battlefield. If Grenzo isn’t on the battlefield at this time, use its power from when it was last on the battlefield.').
card_ruling('grenzo, dungeon warden', '2014-05-29', 'If the card you put into your graveyard isn’t a creature card or it’s a creature card with power greater than Grenzo’s power, it stays in your graveyard.').

card_ruling('grid monitor', '2004-12-01', 'You can still put creature cards onto the battlefield or create creature tokens.').
card_ruling('grid monitor', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('grief tyrant', '2008-05-01', 'The triggered ability is mandatory. If you\'re the only player who controls any creatures when it triggers, you must target one of them.').

card_ruling('griffin canyon', '2004-10-04', 'This can target an already untapped Griffin, and gives the bonus regardless.').

card_ruling('griffin guide', '2006-09-25', 'If Griffin Guide and the enchanted creature go to the graveyard at the same time, Griffin Guide\'s ability will trigger.').

card_ruling('griffin protector', '2012-07-01', 'If Griffin Protector enters the battlefield under your control at the same time as other creatures, its ability will trigger once for each of those creatures.').

card_ruling('grifter\'s blade', '2005-10-01', 'Grifter\'s Blade must enter the battlefield attached to a creature you control, if possible.').
card_ruling('grifter\'s blade', '2005-10-01', 'If you don\'t control a creature Grifter\'s Blade could be attached to, it simply enters the battlefield unattached.').

card_ruling('grim affliction', '2011-06-01', 'You may choose the creature you put the -1/-1 counter on to get another -1/-1 counter when proliferating.').
card_ruling('grim affliction', '2011-06-01', 'The state-based action that removes matching +1/+1 and -1/-1 counters won\'t check until after you proliferate and Grim Affliction finishes resolving. It\'s possible to remove up to two +1/+1 counters from the creature by choosing to put a second -1/-1 counter on the creature when proliferating.').

card_ruling('grim contest', '2014-11-24', 'If either target is an illegal target as Grim Contest tries to resolve, neither creature will deal or be dealt damage.').

card_ruling('grim feast', '2004-10-04', 'It affects all opposing players in a multiplayer game.').

card_ruling('grim flowering', '2011-01-22', 'Count the number of creature cards in your graveyard when Grim Flowering resolves.').

card_ruling('grim guardian', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('grim guardian', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('grim guardian', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('grim haruspex', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('grim haruspex', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('grim haruspex', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('grim haruspex', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('grim haruspex', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('grim haruspex', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('grim haruspex', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('grim haruspex', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('grim haruspex', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('grim lavamancer', '2011-09-22', 'The two cards are exiled as the cost of Grim Lavamancer\'s ability is paid. Players can\'t respond to the paying of costs by trying to move those cards to another zone.').

card_ruling('grim poppet', '2008-05-01', 'The second ability can target any creature except the Grim Poppet whose ability is being activated. It can target a different Grim Poppet. It can be activated multiple times targeting the same creature.').

card_ruling('grim return', '2013-07-01', 'You can choose a creature card in any player’s graveyard as long as that card was put there from the battlefield during the turn. A creature card that was put into a graveyard from anywhere else, such as a card that was discarded, can’t be chosen as the target.').
card_ruling('grim return', '2013-07-01', 'Creature spells that are countered never entered the battlefield, so those cards can’t be chosen as the target of Grim Return.').

card_ruling('grim roustabout', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('grim roustabout', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('grim roustabout', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('grim roustabout', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('grimgrin, corpse-born', '2011-09-22', 'If the targeted creature is an illegal target by the time Grimgrin\'s last ability resolves, the entire ability is countered and none of its effects will occur. You won\'t put a +1/+1 counter on Grimgrin.').
card_ruling('grimgrin, corpse-born', '2011-09-22', 'If the defending player controls no creatures when Grimgrin attacks, the last ability will be removed from the stack and have no effect.').
card_ruling('grimgrin, corpse-born', '2013-07-01', 'If Grimgrin\'s last ability resolves, but the targeted creature isn\'t destroyed (perhaps because it regenerated or has indestructible), you\'ll still put a +1/+1 on Grimgrin.').

card_ruling('grimoire of the dead', '2011-09-22', '\"Creature cards\" includes each card with the type creature, even if it has additional types, such as artifact.').

card_ruling('grimoire thief', '2008-04-01', 'The only cards you turn face up are the cards exiled with the Grimoire Thief that was sacrificed.').
card_ruling('grimoire thief', '2008-04-01', 'The last ability counters all spells on the stack with any of the appropriate names, no matter who controls them.').
card_ruling('grimoire thief', '2008-04-01', 'If a split card is turned face up, the ability counters all spells with the same name as either side of that split card.').
card_ruling('grimoire thief', '2013-07-01', 'Once a player gains control of Grimoire Thief, that player can look at all cards it exiled, even if those cards were exiled while someone else controlled Grimoire Thief.').
card_ruling('grimoire thief', '2013-07-01', 'Once a player is allowed to look at the exiled face-down cards, that player may continue to do so even after Grimoire Thief leaves the battlefield or another player gains control of it.').

card_ruling('grindclock', '2014-07-18', 'If Grindclock isn’t on the battlefield when its last ability resolves, use the number of charge counters on it when it left the battlefield to determine how many cards to put into the graveyard.').

card_ruling('grindstone', '2004-10-04', 'If the two cards are both colorless, then the effect does not repeat. It only repeats if both cards have at least one color and at least one color is shared between them.').
card_ruling('grindstone', '2004-10-04', 'The effect repeats as many times as it takes to meet the end condition.').

card_ruling('grinning ignus', '2007-05-01', 'Although Grinning Ignus\'s ability has a timing restriction, it\'s still a mana ability. It doesn\'t use the stack and it can\'t be responded to.').
card_ruling('grinning ignus', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('grinning totem', '2006-09-25', 'You may choose to find any kind of card, including a land. You must exile a card (unless the opponent\'s library is empty); you can\'t fail to find anything.').
card_ruling('grinning totem', '2006-09-25', '\"Until the beginning of your next upkeep\" means the effect wears off as soon as your next upkeep starts. Your last chance to play the card is usually the End step of the player who immediately precedes you.').
card_ruling('grinning totem', '2006-09-25', 'The exiled card is played using the normal rules for timing and priority for its card type, as well as any other applicable restrictions. You\'ll have to pay its mana cost. The only thing that\'s different is you\'re casting it from the Exile zone.').

card_ruling('grip of amnesia', '2004-10-04', 'You can choose to exile all cards from your graveyard even if it has no cards in it.').

card_ruling('grip of chaos', '2004-10-04', 'If there are multiple legal targets, it may still choose the original one.').
card_ruling('grip of chaos', '2004-10-04', 'If there are no other legal targets, it keeps its target.').
card_ruling('grip of chaos', '2004-10-04', 'The ability triggers when a spell or ability is put on the stack, and this target-changing ability itself goes on the stack. This means that the spell or ability keeps its original target until this ability resolves. You determine the new target when this ability resolves, at which time the set of legal targets may have changed.').

card_ruling('grip of desolation', '2015-08-25', 'You must target a creature and a land in order to cast Grip of Desolation. A permanent that’s both a land and a creature can be chosen for both targets.').
card_ruling('grip of desolation', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('grip of desolation', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('grip of desolation', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('grip of desolation', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('grip of desolation', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('griselbrand', '2012-05-01', 'You can\'t activate Griselbrand\'s ability if you have 6 or less life.').

card_ruling('grisly spectacle', '2013-01-24', 'Use the creature\'s power the last time it was on the battlefield to determine how many cards its controller puts into his or her graveyard.').
card_ruling('grisly spectacle', '2013-01-24', 'If the creature is an illegal target when Grisly Spectacle tries to resolve, it will be countered and none of its effects will happen. The creature\'s controller won\'t put any cards into his or her graveyard.').

card_ruling('grixis', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('grixis', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('grixis', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('grixis', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('grixis', '2009-10-01', 'Despite the appearance of the reminder text, the unearth abilities that Grixis grants are activated abilities of each individual blue, black, and/or red creature card in a graveyard. They\'re not activated abilities of Grixis.').
card_ruling('grixis', '2009-10-01', 'A card\'s mana cost includes its color.').
card_ruling('grixis', '2009-10-01', 'Grixis may cause a creature card in a graveyard to have multiple unearth abilities. Its owner may activate any one of those abilities.').
card_ruling('grixis', '2009-10-01', 'If you activate a creature card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('grixis', '2009-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Essence Scatter) will not.').
card_ruling('grixis', '2009-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the ability won\'t trigger again. However, the replacement effect will still exile the creature if it would eventually leave the battlefield.').
card_ruling('grixis', '2009-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the exile abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('grixis', '2009-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new permanent with no relation to its previous existence. The unearth effect will no longer apply to it.').
card_ruling('grixis', '2009-10-01', 'Unlike the unearth ability, the {C} ability returns a creature card from a graveyard to the battlefield indefinitely.').

card_ruling('grixis charm', '2008-10-01', 'You can choose a mode only if you can choose legal targets for that mode. If you can\'t choose legal targets for any of the modes, you can\'t cast the spell.').
card_ruling('grixis charm', '2008-10-01', 'While the spell is on the stack, treat it as though its only text is the chosen mode. The other two modes are treated as though they don\'t exist. You don\'t choose targets for those modes.').
card_ruling('grixis charm', '2008-10-01', 'If this spell is copied, the copy will have the same mode as the original.').

card_ruling('grixis illusionist', '2009-02-01', 'You choose a basic land type as the ability resolves.').
card_ruling('grixis illusionist', '2009-02-01', 'The affected land loses its existing land types and any abilities printed on it. It becomes the chosen basic land type and now has the ability to tap to add one mana of the appropriate color to your mana pool. Grixis Illusionist\'s ability doesn\'t change the affected land\'s name or whether it\'s legendary or basic.').

card_ruling('grixis sojourners', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('grixis sojourners', '2009-05-01', 'The triggered ability acts as a cycle-triggered ability or as a leaves-the-battlefield ability, as appropriate. The player who controls the triggered ability is the player who cycled the Sojourner, or the player who last controlled the Sojourner on the battlefield.').
card_ruling('grixis sojourners', '2009-05-01', 'If you cycle this card, the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('grixis sojourners', '2009-05-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('grixis sojourners', '2009-05-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').
card_ruling('grixis sojourners', '2009-05-01', 'Grixis Sojourner\'s triggered ability may target Grixis Sojourners itself. By the time the ability triggers, Grixis Sojourner will already be in your graveyard.').

card_ruling('grizzled wolverine', '2004-10-04', 'It can only get +2/+0 even if more than one creature blocks it.').

card_ruling('grizzly fate', '2013-04-15', 'At the time this spell is resolving, it is on the stack and not in the graveyard. This means that you should not count this card when determining whether the Threshold effect occurs.').

card_ruling('grotag siege-runner', '2010-06-15', 'If the targeted creature is an illegal target by the time the ability resolves, the ability is countered. Its controller won\'t be dealt damage.').

card_ruling('grotag thrasher', '2010-03-01', 'The targeted creature is prohibited from blocking any creature this turn, not just Grotag Thrasher.').
card_ruling('grotag thrasher', '2010-03-01', 'Grotag Thrasher\'s ability can target any creature, not just one the defending player controls. For example, if you want that player\'s creatures to be able to block this turn, you can target Grotag Thrasher with its own ability.').

card_ruling('ground rift', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('ground rift', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('ground rift', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('ground rift', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('ground rift', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('ground seal', '2012-07-01', 'The last ability only works when Ground Seal is on the battlefield.').
card_ruling('ground seal', '2012-07-01', 'Only spells and abilities that target cards in graveyards will be affected. Abilities that don\'t target cards in graveyards (like the one Tormod\'s Crypt has) can still affect those cards.').

card_ruling('groundbreaker', '2007-02-01', 'This is the timeshifted version of Ball Lightning.').
card_ruling('groundbreaker', '2007-02-01', 'The creature is sacrificed at the end of every turn in which it is on the battlefield. There is no choice about what turn to sacrifice it.').

card_ruling('grounded', '2012-05-01', 'The creature can later gain flying from another effect. That will override Grounded\'s effect.').

card_ruling('groundling pouncer', '2008-08-01', 'It matters that an opponent controls a creature with flying only as you activate this ability. Once you\'ve activated it, Groundling Pouncer will still get +1/+3 and gain flying, even if all of your opponents\' creatures with flying leave the battlefield in response to the ability or after the ability resolves.').

card_ruling('groundshaker sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('groundshaker sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('groundswell', '2010-03-01', 'Whether you had a land enter the battlefield under your control this turn is checked as this spell resolves, not as you cast it.').
card_ruling('groundswell', '2010-03-01', 'Having more than one land enter the battlefield under your control this turn provides no additional benefit.').
card_ruling('groundswell', '2010-03-01', 'The landfall ability checks for an action that has happened in the past. It doesn\'t matter if a land that entered the battlefield under your control previously in the turn is still on the battlefield, is still under your control, or is still a land.').
card_ruling('groundswell', '2010-03-01', 'Once the spell resolves, having a land enter the battlefield under your control provides no further benefit.').
card_ruling('groundswell', '2010-03-01', 'The effect of this spell’s landfall ability replaces its normal effect. If you had a land enter the battlefield under your control this turn, only the landfall-based effect happens.').

card_ruling('grove of the dreampods', '2012-06-01', 'Both abilities are mandatory. You must put the creature card onto the battlefield, even if you don\'t want to.').
card_ruling('grove of the dreampods', '2012-06-01', 'If you don\'t reveal any creature cards from your library, you\'ll put the cards from your library on the bottom of your library in a random order. Essentially, this will shuffle your library, although you are not shuffling it for purposes of effects that care about a player shuffling a library.').

card_ruling('grove of the guardian', '2012-10-01', 'To activate Grove of the Guardian\'s second ability you can tap any two untapped creatures you control, including ones you haven\'t controlled continuously since the beginning of your most recent turn.').
card_ruling('grove of the guardian', '2012-10-01', 'You can\'t tap Grove of the Guardian to activate both abilities at the same time.').

card_ruling('grove rumbler', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('grove rumbler', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('grove rumbler', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('grovetender druids', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('grovetender druids', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('growing ranks', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('growing ranks', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('growing ranks', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('growing ranks', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('growing ranks', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('growing ranks', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('growth spasm', '2010-06-15', 'You put an Eldrazi Spawn token onto the battlefield whether or not you put a basic land card onto the battlefield.').

card_ruling('grozoth', '2005-10-01', 'The search ability lets you find cards whose converted mana costs are each equal to exactly 9, such as other Grozoths.').

card_ruling('grudge keeper', '2014-05-29', 'The spell or ability that caused you to vote will finish resolving before Grudge Keeper’s ability is put on the stack. The ability will be put on the stack even if the results of the vote cause Grudge Keeper to leave the battlefield.').
card_ruling('grudge keeper', '2014-05-29', 'If one of your opponents has additional votes because of Brago’s Representative, and that player votes for more than one choice you didn’t vote for, that player will lose only 2 life.').
card_ruling('grudge keeper', '2014-05-29', 'If you have additional votes because of Brago’s Representative, and you vote for more than one choice, an opponent will lose 2 life only if he or she voted for none of those choices.').
card_ruling('grudge keeper', '2014-05-29', 'Grudge Keeper’s ability considers only the vote that caused it to trigger, not any previous votes, even if they were caused by the same permanent.').

card_ruling('gruul charm', '2013-01-24', 'If you choose the first mode, no creature without flying will be able to block that turn, including creatures that lose flying and creatures without flying that enter the battlefield after Gruul Charm resolves.').
card_ruling('gruul charm', '2013-01-24', 'If you choose the second mode, Gruul Charm\'s effect will override any other control-changing effects on those permanents.').
card_ruling('gruul charm', '2013-01-24', 'A token is owned by the player under whose control it entered the battlefield.').

card_ruling('gruul guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('gruul guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('gruul keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('gruul keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('gruul nodorog', '2006-02-01', 'Activating Gruul Nodorog\'s ability a second time in the same turn has no further effect.').
card_ruling('gruul nodorog', '2006-02-01', 'Activating Gruul Nodorog\'s ability after blockers have been declared has no effect on the creatures already blocking it.').

card_ruling('gruul ragebeast', '2013-01-24', 'Gruul Ragebeast\'s triggered ability isn\'t optional. However, if no opponent controls a creature when a creature enters the battlefield under your control, the ability will be removed from the stack.').
card_ruling('gruul ragebeast', '2013-01-24', 'If one or both of the creatures that are supposed to fight are no longer on the battlefield when the ability resolves, the fight won\'t happen. Neither creature will deal or be dealt damage.').
card_ruling('gruul ragebeast', '2013-01-24', 'If the target creature is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. Neither creature will deal or be dealt damage.').

card_ruling('gruul turf', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('gruul war plow', '2006-02-01', 'While it\'s a creature, Gruul War Plow gives itself trample.').
card_ruling('gruul war plow', '2006-02-01', 'While it\'s a creature, Gruul War Plow has creature type Juggernaut. It doesn\'t have any abilities associated with the card named Juggernaut.').
card_ruling('gruul war plow', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('guan yu, sainted warrior', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').
card_ruling('guan yu, sainted warrior', '2009-10-01', 'If Guan Yu, Sainted Warrior is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into your library.').
card_ruling('guan yu, sainted warrior', '2009-10-01', 'If a replacement effect has Guan Yu move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('guard dogs', '2004-10-04', 'You choose the target on activation and choose the permanent which is compared for color on resolution.').
card_ruling('guard dogs', '2004-10-04', 'You only check colors on resolution and not later when the damage prevention actually is applied.').

card_ruling('guardian automaton', '2015-06-22', 'If Guardian Automaton dies at the same time as your life total is reduced to 0 or less, you’ll lose the game before the triggered ability resolves.').

card_ruling('guardian beast', '2004-10-04', 'This does not stop sacrifices.').

card_ruling('guardian idol', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('guardian of tazeem', '2015-08-25', 'Guardian of Tazeem’s landfall ability can target a creature that’s already tapped. If the land that caused it to trigger was an Island, that creature won’t untap during its controller’s next untap step.').
card_ruling('guardian of tazeem', '2015-08-25', 'The ability tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('guardian of tazeem', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('guardian of tazeem', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('guardian of tazeem', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('guardian of the ages', '2013-07-01', 'The effect of Guardian of the Ages’s triggered ability doesn’t wear off as the turn ends. Losing defender and gaining trample last indefinitely.').
card_ruling('guardian of the ages', '2013-07-01', 'Once Guardian of the Ages has lost defender, its triggered ability will no longer trigger when a creature attacks you or a planeswalker you control (unless it has somehow gained defender again).').

card_ruling('guardian of the gateless', '2013-01-24', 'Guardian of the Gateless\'s last ability triggers only once when it\'s declared as a blocker no matter how many creatures it\'s blocking.').

card_ruling('guardian of vitu-ghazi', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('guardian of vitu-ghazi', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('guardian seraph', '2009-10-01', 'This ability prevents 1 damage from each source an opponent controls each time that source would deal damage to you. It prevents 1 of any damage, not just combat damage.').
card_ruling('guardian seraph', '2009-10-01', 'Spells and permanents have controllers, but cards that aren\'t on the stack or the battlefield don\'t. If a source without a controller (such as a cycled Jund Sojourners, for example) would deal damage to you, 1 of that damage is prevented if an opponent owns that source.').
card_ruling('guardian seraph', '2009-10-01', 'The effects from multiple Guardian Seraphs are cumulative.').

card_ruling('guardian shield-bearer', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('guardian shield-bearer', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('guardian shield-bearer', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('guardian zendikon', '2010-03-01', 'The enchanted permanent will be both a land and a creature and can be affected by anything that affects either a land or a creature.').
card_ruling('guardian zendikon', '2010-03-01', 'When a land becomes a creature, that doesn\'t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('guardian zendikon', '2010-03-01', 'An attacking or blocking creature that stops being a creature is removed from combat. This can happen if a Zendikon enchanting an attacking or blocking creature leaves the battlefield, for example. The permanent that was removed from combat neither deals nor is dealt combat damage. Any attacking creature that the land creature was blocking remains blocked, however.').
card_ruling('guardian zendikon', '2010-03-01', 'An ability that turns a land into a creature also sets that creature\'s power and toughness. If the land was already a creature, this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness, such as the effects of Disfigure or Glorious Anthem, will continue to apply, no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('guardian zendikon', '2010-03-01', 'If a Zendikon and the land it\'s enchanting are destroyed at the same time (due to Akroma\'s Vengeance, for example), the Zendikon\'s last ability will still trigger.').
card_ruling('guardian zendikon', '2010-03-01', 'If a Zendikon\'s last ability triggers, but the land card it refers to leaves the graveyard before it resolves, it will resolve but do nothing.').

card_ruling('guardians of akrasa', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('guardians of akrasa', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('guardians of akrasa', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('guardians of akrasa', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('guardians of akrasa', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('guardians of akrasa', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('guardians\' pledge', '2011-09-22', 'Guardians\' Pledge only gives a bonus to white creatures you control when it resolves. Creatures that enter the battlefield later in the turn and nonwhite creatures that become white later in the turn will not get the bonus.').
card_ruling('guardians\' pledge', '2011-09-22', 'If a white creature that gets the bonus stops being white later in the turn, it will continue to get +2/+2 until end of turn.').

card_ruling('gudul lurker', '2015-02-25', 'If a face-down Gudul Lurker attacks and is blocked, turning it face up won’t cause it to become unblocked.').
card_ruling('gudul lurker', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('gudul lurker', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('gudul lurker', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('guerrilla tactics', '2004-10-04', 'If it is discarded, the damage done by it is done by this card due to an ability and not by a spell.').
card_ruling('guerrilla tactics', '2004-10-04', 'The second ability will trigger even on an optional discard caused by an opponent.').
card_ruling('guerrilla tactics', '2004-10-04', 'Discarding as a cost to cast a spell will not trigger the ability. Only discarding as an effect will trigger the ability.').

card_ruling('guided passage', '2004-10-04', 'If you have no cards of any of the specified card types, then ignore those types and the opponent only selects cards of the types you do have.').
card_ruling('guided passage', '2004-10-04', 'Yes, your opponents get to look at all the cards in your library.').

card_ruling('guild feud', '2012-10-01', 'Guild Feud has one target: the opponent. You\'ll see what creature that player puts onto the battlefield before revealing the top three cards of your library and deciding whether to put a creature onto the battlefield yourself.').
card_ruling('guild feud', '2012-10-01', 'If no creatures or only one creature is put onto the battlefield this way, no fight happens.').
card_ruling('guild feud', '2012-10-01', 'Players can\'t cast spells or activate abilities between the creatures being put onto the battlefield and them fighting each other. Notably, you can\'t respond with a spell like Giant Growth.').
card_ruling('guild feud', '2012-10-01', 'Any abilities that trigger when the two creatures enter the battlefield will be put on the stack after the fight, even if one or both creatures die.').

card_ruling('guildscorn ward', '2013-01-24', 'The enchanted creature can\'t be enchanted or equipped by multicolored Auras and Equipment, it can\'t be blocked by multicolored creatures, it can\'t be targeted by multicolored spells or abilities from multicolored sources, and all damage dealt to it by multicolored sources is prevented.').
card_ruling('guildscorn ward', '2013-01-24', 'Hybrid spells and permanents in the Return to Ravnica block are multicolored, even if you cast them with one color of mana.').

card_ruling('guile', '2007-10-01', 'Guile\'s second ability replaces \"counter [a certain spell]\" with \"exile [a certain spell] and you may cast it without paying its mana cost.\" You have the option to cast it immediately upon its exile. If you choose not to, it remains exiled and you don\'t get another chance to cast it. If the spell or ability that tried to counter the spell has additional effects, it then continues to resolve.').
card_ruling('guile', '2007-10-01', 'Exiling the spell is mandatory. Casting it is not.').
card_ruling('guile', '2007-10-01', 'A spell exiled this way was never actually countered.').
card_ruling('guile', '2007-10-01', 'If a spell or ability you control attempts to counter a spell that can\'t be countered, it doesn\'t. Since the spell wouldn\'t be countered, Guile\'s ability has no effect on it. The spell will continue to resolve normally.').
card_ruling('guile', '2007-10-01', 'The last ability triggers when the Incarnation is put into its owner\'s graveyard from any zone, not just from on the battlefield.').
card_ruling('guile', '2007-10-01', 'Although this ability triggers when the Incarnation is put into a graveyard from the battlefield, it doesn\'t *specifically* trigger on leaving the battlefield, so it doesn\'t behave like other leaves-the-battlefield abilities. The ability will trigger from the graveyard.').
card_ruling('guile', '2007-10-01', 'If the Incarnation had lost this ability while on the battlefield (due to Lignify, for example) and then was destroyed, the ability would still trigger and it would get shuffled into its owner\'s library. However, if the Incarnation lost this ability when it was put into the graveyard (due to Yixlid Jailer, for example), the ability wouldn\'t trigger and the Incarnation would remain in the graveyard.').
card_ruling('guile', '2007-10-01', 'If the Incarnation is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('guiltfeeder', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('guise of fire', '2012-05-01', 'The controller of the enchanted creature still decides which player or planeswalker the creature attacks.').
card_ruling('guise of fire', '2012-05-01', 'If, during its controller\'s declare attackers step, the enchanted creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('gurmag angler', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('gurmag angler', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('gurmag angler', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('gurmag angler', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('gurmag angler', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('gurmag drowner', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('gurmag drowner', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('gurmag drowner', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('gurmag drowner', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('gurmag drowner', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('gurzigost', '2008-04-01', 'You decide how it will assign its damage at the time the ability resolves. This will be prior to actually assigning combat damage.').
card_ruling('gurzigost', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').

card_ruling('gustha\'s scepter', '2004-10-04', 'The last ability will trigger when a different player gains control of Gustha\'s Scepter, as well as when Gustha\'s Scepter leaves the battlefield.').

card_ruling('gustrider exuberant', '2008-10-01', 'The ability doesn\'t target, so it checks the creatures\' powers only once: when the ability resolves. Once the ability resolves, it will continue to apply to the affected creatures no matter what their powers may become later in the turn.').

card_ruling('gut shot', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('gut shot', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('gut shot', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('gut shot', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('gut shot', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('gutter grime', '2011-09-22', 'The power and toughness of the Ooze tokens will constantly update as Gutter Grime accumulates slime counters.').
card_ruling('gutter grime', '2011-09-22', 'If you control more than one Gutter Grime, each Ooze token remembers which one created it. The power and toughness of that Ooze will be equal to the number of slime counters on that Gutter Grime only.').
card_ruling('gutter grime', '2011-09-22', 'If Gutter Grime leaves the battlefield, the power and toughness of each Ooze token it created will become 0. Unless another effect is raising its toughness above 0, each of these Ooze tokens will be put into its owner\'s graveyard the next time state-based actions are checked.').

card_ruling('guttersnipe', '2012-10-01', 'Guttersnipe\'s ability resolves and Guttersnipe deals damage before the instant or sorcery spell resolves.').

card_ruling('guul draz assassin', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('guul draz assassin', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('guul draz assassin', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('guul draz assassin', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('guul draz assassin', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('guul draz overseer', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('guul draz overseer', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('guul draz overseer', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('guul draz vampire', '2009-10-01', 'Intimidate looks at the current colors of a creature that has it. Normally, if Guul Draz Vampire has intimidate, it can\'t be blocked except by artifact creatures and/or black creatures. If it\'s turned white, then it can\'t be blocked except by artifact creatures and/or white creatures.').
card_ruling('guul draz vampire', '2009-10-01', 'If an attacking creature has intimidate, what colors it is matters only as the defending player declares blockers. Once it\'s blocked, changing its colors won\'t change that.').

card_ruling('gwafa hazid, profiteer', '2009-02-01', 'If Gwafa Hazid leaves the battlefield, any bribery counters will remain where they are. However, without a Gwafa Hazid on the battlefield, creatures with bribery counters on them can attack and block as normal. If another Gwafa Hazid enters the battlefield, its ability will affect all creatures with bribery counters on them, including the ones that received counters from a different Gwafa Hazid.').
card_ruling('gwafa hazid, profiteer', '2009-02-01', 'Creatures with bribery counters on them can\'t be declared as attackers or blockers. However, if a bribery counter is put on a creature that\'s already been declared as an attacker or blocker, that creature will continue to attack or block that combat.').

card_ruling('gwyllion hedge-mage', '2008-08-01', 'Each of the triggered abilities has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control two or more lands of the appropriate land type when the Hedge-Mage enters the battlefield, and (2) the ability will do nothing if you don\'t control two or more lands of the appropriate land type by the time it resolves.').
card_ruling('gwyllion hedge-mage', '2008-08-01', 'Both abilities trigger at the same time. You can put them on the stack in any order.').
card_ruling('gwyllion hedge-mage', '2008-08-01', 'Each of the triggered abilities look at your lands individually. This means that if you only control two dual-lands of the appropriate types, both of the abilities will trigger').
card_ruling('gwyllion hedge-mage', '2008-08-01', 'Both abilities are optional; you choose whether to use them when they resolve. If an ability has a target, you must choose a target even if you don\'t plan to use the ability.').

card_ruling('gyre sage', '2013-01-24', 'Gyre Sage\'s last ability is a mana ability. It doesn\'t use the stack and can\'t be responded to.').
card_ruling('gyre sage', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('gyre sage', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('gyre sage', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('gyre sage', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('gyre sage', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('gyre sage', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

