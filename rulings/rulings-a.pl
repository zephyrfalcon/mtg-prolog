% Rulings

card_ruling('a display of my dark power', '2010-06-15', 'The ability affects all players, not just you.').
card_ruling('a display of my dark power', '2010-06-15', 'The effect doesn\'t wear off until just before your next untap step (even if an effect will cause that untap step to be skipped).').
card_ruling('a display of my dark power', '2010-06-15', 'The types of mana are white, blue, black, red, green, and colorless.').
card_ruling('a display of my dark power', '2010-06-15', 'If a land produces more than one type of mana at a single time (as Boros Garrison does, for example), the land\'s controller chooses which one of those types of mana is produced by the delayed triggered ability.').
card_ruling('a display of my dark power', '2010-06-15', 'If a land is tapped for mana but doesn\'t produce any (for example, if you tap Gaea\'s Cradle for mana while you control no creatures), the delayed triggered ability won\'t produce any mana either.').

card_ruling('abattoir ghoul', '2011-09-22', 'You\'ll gain life equal to the creature\'s last known toughness before it died. For example, if Abattoir Ghoul deals 3 first-strike damage to a 7/7 creature and then you give the creature -5/-5 before the regular combat damage step, you\'ll gain 2 life.').

card_ruling('abbot of keral keep', '2015-06-22', 'The card exiled by Abbot of Keral Keep’s ability is exiled face up.').
card_ruling('abbot of keral keep', '2015-06-22', 'You may play that card that turn even if Abbot of Keral Keep is no longer on the battlefield or under your control.').
card_ruling('abbot of keral keep', '2015-06-22', 'Playing the card exiled with Abbot of Keral Keep’s ability follows the normal rules for playing that card. You must pay its costs, and you must follow all applicable timing rules. For example, if the card is a creature card, you can cast that card by paying its mana cost only during your main phase while the stack is empty.').
card_ruling('abbot of keral keep', '2015-06-22', 'Unless an effect allows you to play additional lands that turn, you can play a land card exiled with Abbot of Keral Keep’s ability only if you haven’t played a land yet that turn.').
card_ruling('abbot of keral keep', '2015-06-22', 'If you don’t play the card, it will remain exiled.').
card_ruling('abbot of keral keep', '2015-06-22', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('abbot of keral keep', '2015-06-22', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('abbot of keral keep', '2015-06-22', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('abduction', '2009-10-01', 'Abduction may be cast targeting an untapped creature.').

card_ruling('abeyance', '2004-10-04', 'Abeyance prohibits activated abilities of cards which are not on the battlefield.').
card_ruling('abeyance', '2004-10-04', 'Abeyance never prevents mana abilities from being activated.').
card_ruling('abeyance', '2004-10-04', 'Abeyance does not affect abilities which are not activated, such as static abilities and triggered abilities. It also doesn\'t stop the declaration of attackers or blockers.').
card_ruling('abeyance', '2008-08-01', 'This can\'t be used as a counterspell. It will have no effect on spells which were on the stack when it was cast, nor on those cast in response to it.').

card_ruling('abhorrent overlord', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('abhorrent overlord', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('abhorrent overlord', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('abhorrent overlord', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('abjure', '2009-10-01', 'You can\'t sacrifice more than one permanent.').

card_ruling('abomination', '2004-10-04', 'The color of the blocking/blocked creature is checked only at declaration of the block. If the creature is green or white at that time, it will be destroyed at end of combat even if it changes color before combat ends.').

card_ruling('abomination of gudul', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('abomination of gudul', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('abomination of gudul', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('abomination of gudul', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('abomination of gudul', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('abomination of gudul', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('abomination of gudul', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('abomination of gudul', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('abomination of gudul', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('aboroth', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('about face', '2005-11-01', 'Note that the wording \"Effects that alter the creature\'s power alter its toughness instead, and vice versa, this turn\" has been removed.').
card_ruling('about face', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('about face', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('abrupt decay', '2012-10-01', 'The converted mana cost of a creature token is 0, unless that token is a copy of another creature, in which case it copies that creature\'s mana cost.').
card_ruling('abrupt decay', '2012-10-01', 'If the permanent has {X} in its mana cost, X is 0.').

card_ruling('absolute grace', '2004-10-04', 'This affects all creatures, not just your own.').

card_ruling('absolute law', '2004-10-04', 'This affects all creatures, not just your own.').

card_ruling('absorb', '2004-10-04', 'If cast targeting a spell that can\'t be countered, the life gain still happens, but that spell is not countered.').

card_ruling('absorb vis', '2009-02-01', 'Unlike the normal cycling ability, basic landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a basic land card. You don\'t choose the type of basic land card you\'ll find until you\'re performing the search. After you choose a basic land card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('absorb vis', '2009-02-01', 'Basic landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being basic landcycled. Any ability that stops a cycling ability from being activated also stops a basic landcycling ability from being activated.').
card_ruling('absorb vis', '2009-02-01', 'Basic landcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with basic landcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('absorb vis', '2009-02-01', 'You can choose not to find a basic land card, even if there is one in your library.').

card_ruling('abundance', '2004-10-04', 'This replacement effect replaces the draw, so nothing that triggers on a draw will trigger.').
card_ruling('abundance', '2004-10-04', 'If you use this on a multi-card draw, each replaced draw is handled separately. In other words, you reveal and then put on the bottom of the library for the first card, then do the same for the second, and so on. In a multi-card draw you do not have to choose how many of those draws will be replaced before you do any drawing or use of this card.').
card_ruling('abundance', '2004-10-04', 'If no card of the chosen type is found before your library empties, you don\'t get a card, but you do get to order all the cards in your library any way you choose.').
card_ruling('abundance', '2007-07-15', 'If your library is empty, Abundance can prevent you from losing the game for being unable to draw a card. If an effect or turn-based action would cause you to draw a card, you can replace that draw with Abundance\'s replacement effect. (It doesn\'t matter that you\'d be unable to actually draw a card.) Since Abundance\'s effect has you put a card into your hand instead of drawing a card, you\'ll never be forced to draw a card with an empty library.').

card_ruling('abundant growth', '2012-05-01', 'The land retains any other abilities it has.').

card_ruling('abyssal gatekeeper', '2004-10-04', 'The \"sacrifice a creature\" effect is not targeted. It can affect creatures with protection from black, for example.').
card_ruling('abyssal gatekeeper', '2008-04-01', 'The player whose turn it is chooses a creature to sacrifice, then each other player in turn order chooses a creature to sacrifice, then all chosen creatures are sacrificed at the same time.').

card_ruling('abyssal hunter', '2004-10-04', 'The ability can target an already tapped creature.').

card_ruling('abyssal nightstalker', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('abyssal nocturnus', '2006-02-01', 'If your opponent discards any cards during his or her cleanup step to get down to the maximum hand size, the ability will trigger and players will receive priority. After that cleanup step ends, a new one will begin and Abyssal Nocturnus\'s effect will end. It won\'t carry over into the next turn.').
card_ruling('abyssal nocturnus', '2006-02-01', 'If your opponent discards multiple cards to a single effect, this will trigger multiple times.').

card_ruling('abyssal persecutor', '2010-03-01', 'No game effect can cause you to win the game or cause any opponent to lose the game while you control Abyssal Persecutor. It doesn\'t matter whether an opponent has 0 or less life, an opponent is forced to draw a card while his or her library is empty, an opponent has ten or more poison counters, an opponent is dealt combat damage by Phage the Untouchable, you control Felidar Sovereign and have 40 or more life, or so on. You keep playing.').
card_ruling('abyssal persecutor', '2010-03-01', 'Other circumstances can still cause an opponent to lose the game, however. An opponent will lose a game if he or she concedes, if that player is penalized with a Game Loss or a Match Loss during a sanctioned tournament due to a DCI rules infraction, or if that player\'s _Magic Online_(R) game clock runs out of time.').
card_ruling('abyssal persecutor', '2010-03-01', 'Effects that say the game is a draw, such as the _Legends_(TM) card Divine Intervention, are not affected by Abyssal Persecutor.').
card_ruling('abyssal persecutor', '2010-03-01', 'Abyssal Persecutor won\'t preclude an opponent\'s life total from reaching 0 or less. It will just preclude that player from losing the game as a result.').
card_ruling('abyssal persecutor', '2010-03-01', 'If Abyssal Persecutor leaves the battlefield while an opponent has 0 or less life, that opponent will lose the game as a state-based action. No player can respond between the time Abyssal Persecutor leaves the battlefield and the time that player loses the game.').
card_ruling('abyssal persecutor', '2010-03-01', 'Even though your opponents can\'t lose the game, a player can\'t pay an amount of life that\'s greater than his or her life total. If a player\'s life total is 0 or less, that player can\'t pay life at all, with one exception: a player may always pay 0 life.').
card_ruling('abyssal persecutor', '2010-03-01', 'If you control Abyssal Persecutor in a Two-Headed Giant game, your team can\'t win the game and the opposing team can\'t lose the game.').

card_ruling('abzan advantage', '2014-11-24', 'You can target any player with Abzan Advantage, even if that player doesn’t control an enchantment.').
card_ruling('abzan advantage', '2014-11-24', 'The enchantment will have already left the battlefield when you bolster 1.').
card_ruling('abzan advantage', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('abzan advantage', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('abzan battle priest', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('abzan battle priest', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('abzan beastmaster', '2014-11-24', 'The ability checks the creatures on the battlefield as it resolves to determine if you will draw a card.').
card_ruling('abzan beastmaster', '2014-11-24', 'You’ll draw a maximum of one card, even if you control multiple creatures tied for the greatest toughness.').

card_ruling('abzan charm', '2014-09-20', 'If you choose the third mode, you choose how the counters will be distributed as you cast the spell. Notably, if you choose to put one +1/+1 counter on each of two target creatures, and one of those creatures becomes an illegal target in response, the +1/+1 counter that would have been put on that creature is lost. It can’t be put on the remaining legal target.').

card_ruling('abzan falconer', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('abzan falconer', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('abzan guide', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('abzan guide', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('abzan guide', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('abzan guide', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('abzan guide', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('abzan guide', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('abzan guide', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('abzan guide', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('abzan guide', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('abzan skycaptain', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('abzan skycaptain', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('academy at tolaria west', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('academy at tolaria west', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('academy at tolaria west', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('academy at tolaria west', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('academy at tolaria west', '2009-10-01', 'Academy at Tolaria West\'s first ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you have no cards in hand as your end step begins, and (2) the ability will do nothing unless you have no cards in hand by the time it resolves.').
card_ruling('academy at tolaria west', '2009-10-01', 'If you discard your hand as a result of rolling {C}, Academy at Tolaria West\'s first ability will then trigger at the beginning of your end step (unless you planeswalk or somehow put a card in your hand before then).').

card_ruling('academy raider', '2013-07-01', 'The triggered ability triggers just once each time Academy Raider deals combat damage to a player, regardless of how much damage it deals.').

card_ruling('academy rector', '2004-10-04', 'You do not have to find an enchantment card if you do not want to, even if you have one in your library.').
card_ruling('academy rector', '2005-08-01', 'An enchantment card is any Enchantment card, including Auras.').

card_ruling('academy researchers', '2007-07-15', 'You can\'t put an Aura card from your hand onto the battlefield this way if that Aura can\'t legally enchant Academy Researchers. For example, you can\'t put an Aura with \"enchant land\" or \"enchant green creature\" onto the battlefield attached to Academy Researchers (unless Academy Researchers somehow turned into a land or a green creature before the ability resolved).').

card_ruling('accumulated knowledge', '2004-10-04', 'The count does not include the card that is currently resolving. The card does not go to the graveyard until after it is done resolving.').

card_ruling('accursed centaur', '2004-10-04', 'If Accursed Centaur is the only creature on the battlefield, you must sacrifice it.').

card_ruling('acid-spewer dragon', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('acid-spewer dragon', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('acid-spewer dragon', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('acidic dagger', '2004-10-04', 'Acidic Dagger does not affect the creature if all damage is prevented or redirected.').
card_ruling('acidic dagger', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the Declare Blockers Step of the first combat phase in that turn.').

card_ruling('acidic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('acidic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('acidic soil', '2004-10-04', 'This counts the lands on resolution.').

card_ruling('acolyte of the inferno', '2015-06-22', 'Acolyte of the Inferno’s last ability will trigger once for each creature that blocks it. Each of those creatures will be dealt 2 damage.').
card_ruling('acolyte of the inferno', '2015-06-22', 'Acolyte of the Inferno’s last ability triggers and resolves before combat damage is dealt. If that causes each creature blocking Acolyte of the Inferno to be destroyed, Acolyte of the Inferno will remain blocked and neither deal nor be dealt combat damage.').
card_ruling('acolyte of the inferno', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('acolyte of the inferno', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('acolyte of the inferno', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('acolyte\'s reward', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('acolyte\'s reward', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('acolyte\'s reward', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('acolyte\'s reward', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('acolyte\'s reward', '2014-02-01', 'Acolyte’s Reward has two targets: the creature that would be dealt damage and the creature or player that Acolyte’s Reward will deal damage to. These targets are chosen as you cast Acolyte’s Reward.').
card_ruling('acolyte\'s reward', '2014-02-01', 'The amount of damage the prevention shield will prevent is based on your devotion to white as Acolyte’s Reward resolves. That amount won’t change later in the turn, even if your devotion to white does.').
card_ruling('acolyte\'s reward', '2014-02-01', 'You don’t choose a source of damage. The prevention shield will apply to the next X damage that would be dealt to the first target, no matter where that damage comes from. It also doesn’t matter whether the damage is dealt at the same time. For example, if the shield prevents the next 5 damage to the first target, and that creature would be dealt 3 damage by Lightning Strike, that 3 damage is prevented and Acolyte’s Reward deals 3 damage to the second target. The prevention effect will still apply to the next 2 damage the first target would be dealt that turn.').
card_ruling('acolyte\'s reward', '2014-02-01', 'The effect of Acolyte’s Reward isn’t a redirection effect. If it prevents damage, Acolyte’s Reward (not the source of that damage) deals damage to the second target as part of that prevention effect. Acolyte’s Reward is the source of the new damage, so the characteristics of the original source (such as its color or whether it had lifelink) don’t apply. The new damage isn’t combat damage, even if the prevented damage was. Since you control the source of the new damage, if the second target is an opponent, you may have Acolyte’s Reward deal its damage to a planeswalker that opponent controls.').
card_ruling('acolyte\'s reward', '2014-02-01', 'As Acolyte’s Reward tries to resolve, if only the first target is illegal, Acolyte’s Reward won’t prevent any damage that would be dealt to that creature and, because of this, Acolyte’s Reward won’t deal damage to the second target. If only the second target is illegal, damage that would be dealt to the first target will be prevented, but Acolyte’s Reward won’t deal damage. If both targets are illegal, Acolyte’s Reward will be countered.').
card_ruling('acolyte\'s reward', '2014-02-01', 'After Acolyte’s Reward resolves, it no longer matters whether either target is still legal. For example, if the second target is a creature controlled by an opponent, and it gains hexproof after Acolyte’s Reward resolves but before it prevents damage, Acolyte’s Reward will still deal damage to that creature. If Acolyte’s Reward can’t deal damage to the second target (perhaps because it’s a creature that has left the battlefield), Acolyte’s Reward will still prevent damage; it just won’t deal any damage itself.').
card_ruling('acolyte\'s reward', '2014-02-01', 'If Acolyte’s Reward prevents damage, it deals its damage immediately afterward as part of that same prevention effect. This happens before state-based actions are performed, and before any player can cast spells or activate abilities. If the source of the original damage was a spell or ability, this happens before that spell or ability resumes its resolution.').
card_ruling('acolyte\'s reward', '2014-02-01', 'If the amount of damage that would be dealt to the first target is in excess of the amount of damage that Acolyte’s Reward would prevent, the source deals its excess damage to the first target at the same time that the rest of it is prevented. Then Acolyte’s Reward deals its damage.').
card_ruling('acolyte\'s reward', '2014-02-01', 'The damage will be dealt by Acolyte’s Reward as it existed on the stack, not as it exists when the damage is dealt. That is, it’s an instant spell that’s dealing the damage, in case an ability cares about that (such as Satyr Firedancer’s, which includes the phrase “Whenever an instant or sorcery spell you control deals damage to an opponent”).').
card_ruling('acolyte\'s reward', '2014-02-01', 'If the first target would be dealt combat damage by multiple creatures, you choose which of that damage to prevent. (For example, if one of those creatures has deathtouch, you could choose to prevent the damage from that creature specifically.) You don’t decide until the point at which the creatures would deal their damage.').
card_ruling('acolyte\'s reward', '2014-02-01', '').

card_ruling('acorn catapult', '2011-09-22', 'If the creature or player is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. No Squirrel will be created.').
card_ruling('acorn catapult', '2011-09-22', 'If the ability resolves, but the damage is prevented or redirected, the targeted player or the controller of the targeted creature puts the Squirrel token onto the battlefield, regardless of how much damage was dealt or what the damage was ultimately dealt to.').

card_ruling('act of aggression', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('act of aggression', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('act of aggression', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('act of aggression', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('act of aggression', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('act of aggression', '2011-06-01', 'Act of Aggression can target any creature an opponent controls, even one that\'s untapped.').
card_ruling('act of aggression', '2011-06-01', 'Gaining control of a creature doesn\'t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('act of authority', '2013-10-17', 'For either ability, if you are the only player who controls an artifact or enchantment, you must choose one of them as the target. However, you may choose to not exile it when the ability resolves.').
card_ruling('act of authority', '2013-10-17', 'If you choose to not exile a permanent with the second ability, you’ll retain control of Act of Authority.').

card_ruling('act of treason', '2009-10-01', 'Act of Treason can target any creature, even one that\'s untapped or one you already control.').
card_ruling('act of treason', '2010-08-15', 'Gaining control of a creature doesn\'t cause you to gain control of any Auras or Equipment attached to it.').
card_ruling('act of treason', '2013-07-01', 'If you control a legendary creature and gain control of another legendary creature with the same name, you’ll choose one to remain on the battlefield and put the other into its owner’s graveyard.').

card_ruling('act on impulse', '2014-07-18', 'The cards are exiled face up.').
card_ruling('act on impulse', '2014-07-18', 'Playing a card this way follows the normal rules for playing the card. You must pay its costs, and you must follow all applicable timing rules. For example, if one of the cards is a creature card, you can cast that card only during your main phase while the stack is empty.').
card_ruling('act on impulse', '2014-07-18', 'Under normal circumstances, you can play a land card exiled with Act on Impulse only if you haven’t played a land yet that turn.').
card_ruling('act on impulse', '2014-07-18', 'Any cards you don’t play will remain exiled.').

card_ruling('ad nauseam', '2008-10-01', 'Each time you put the revealed card into your hand and lose the appropriate amount of life, you decide whether to continue by revealing another card. In other words, you don\'t decide in advance how many cards to put into your hand this way.').
card_ruling('ad nauseam', '2008-10-01', 'You may continue to reveal cards with Ad Nauseam even if your life total has been reduced to 0 or less. If you continue, you will continue to lose life, dropping your life total into negative numbers. As soon as you stop, you\'ll lose the game as a state-based action.').

card_ruling('adaptive automaton', '2011-09-22', 'The choice of creature type is made as Adaptive Automaton enters the battlefield. Players can\'t respond to this choice. The bonus starts applying immediately.').
card_ruling('adaptive automaton', '2011-09-22', 'You must choose an existing _Magic_ creature type.').
card_ruling('adaptive automaton', '2011-09-22', 'Even though Adaptive Automaton is a Construct, other Construct creatures you control won\'t get +1/+1 (unless you chose Construct as Adaptive Automaton entered the battlefield).').

card_ruling('adaptive snapjaw', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('adaptive snapjaw', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('adaptive snapjaw', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('adaptive snapjaw', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('adaptive snapjaw', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('adaptive snapjaw', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('adarkar valkyrie', '2006-07-15', 'Adarkar Valkyrie\'s ability can target a token creature, but since token creatures cease to exist when they leave the battlefield, it won\'t be returned to the battlefield.').

card_ruling('admonition angel', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('admonition angel', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('admonition angel', '2010-03-01', 'Admonition Angel\'s landfall ability and its last ability are linked. The last ability refers only to cards exiled with its landfall ability.').
card_ruling('admonition angel', '2010-03-01', 'If Admonition Angel\'s landfall ability triggers, but the Angel leaves the battlefield before it resolves, its last ability will trigger and resolve first. All previously exiled cards will be returned to the battlefield. Then the landfall ability will resolve and exile the targeted permanent forever.').

card_ruling('adun oakenshield', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('advantageous proclamation', '2014-05-29', 'In Limited formats (including Conspiracy Draft), the minimum deck size is generally 40 cards. With Advantageous Proclamation, your Conspiracy Draft deck must contain at least 35 cards.').
card_ruling('advantageous proclamation', '2014-05-29', 'The effect of Advantageous Proclamation is cumulative. If you have two in your command zone to start the game, your minimum deck size is reduced by ten, and so on.').
card_ruling('advantageous proclamation', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('advantageous proclamation', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('advantageous proclamation', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('advantageous proclamation', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('advantageous proclamation', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('advantageous proclamation', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('advantageous proclamation', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('adventuring gear', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('adventuring gear', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('adventuring gear', '2009-10-01', 'The landfall ability gives +2/+2 to the creature that\'s equipped by Adventuring Gear at the time that ability resolves. It doesn\'t matter what creature Adventuring Gear was attached to (if any) when it triggered. That bonus remains with that creature even if Adventuring Gear leaves the battlefield or becomes attached to a different creature later in the turn.').
card_ruling('adventuring gear', '2009-10-01', 'If Adventuring Gear leaves the battlefield before its landfall ability resolves, the creature it was attached to at the time it left the battlefield gets +2/+2. If it wasn\'t attached to a creature at that time, nothing gets the bonus.').

card_ruling('adverse conditions', '2015-08-25', 'You can cast Adverse Conditions with no targets. When it resolves, you’ll get an Eldrazi Scion. However, if you cast Adverse Conditions with any targets and all of those targets are illegal as it tries to resolve, it will be countered and none of its effects will happen. You won’t get the Eldrazi Scion in that case.').
card_ruling('adverse conditions', '2015-08-25', 'Adverse Conditions tracks the creatures, but not their controller. If either of the creatures changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('adverse conditions', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('adverse conditions', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('adverse conditions', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('adverse conditions', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('adverse conditions', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('adverse conditions', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('adverse conditions', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('adverse conditions', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('adverse conditions', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('advice from the fae', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('advice from the fae', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Advice from the Fae is blue, even if you spend six black mana to cast it.').
card_ruling('advice from the fae', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Advice from the Fae has a converted mana cost of 6, even if you spend {U}{U}{U} to cast it.').
card_ruling('advice from the fae', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Advice from the Fae by spending {U}{U}{U}, {2}{U}{U}, {4}{U}, or {6}.').
card_ruling('advice from the fae', '2008-05-01', 'In a multiplayer game, compare the number of creatures you control with the number of creatures each other player controls. If any single player controls at least as many creatures as you, you get to put only one card into your hand.').

card_ruling('advocate of the beast', '2013-07-01', 'If you don’t control any Beast creatures as your end step begins, the ability will be removed from the stack and will have no effect.').

card_ruling('aegis angel', '2011-09-22', 'If Aegis Angel leaves the battlefield, you no longer control it.').
card_ruling('aegis angel', '2011-09-22', 'If Aegis Angel and the permanent would be destroyed simultaneously, only Aegis Angel will be destroyed.').
card_ruling('aegis angel', '2013-07-01', 'If another player gains control of Aegis Angel, the permanent will no longer have indestructible, even if you regain control of Aegis Angel.').

card_ruling('aegis of the gods', '2014-04-26', 'Your opponent can’t target you with spells that deal damage or abilities that cause their sources to deal damage, even if he or she intends to redirect the damage to a planeswalker you control.').

card_ruling('aeon chronicler', '2007-02-01', 'Both instances of X in the suspend ability are the same. You determine the value of X as you suspend the card from your hand. The value you choose must be at least 1.').
card_ruling('aeon chronicler', '2007-02-01', 'If this is suspended, then when the last time counter is removed from it, both its triggered ability and the \"play this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('aeon chronicler', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('aeon chronicler', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('aeon chronicler', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('aeon chronicler', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('aeon chronicler', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('aeon chronicler', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('aeon chronicler', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('aeon chronicler', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('aeon chronicler', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('aeon chronicler', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('aerial caravan', '2004-10-04', 'If you do not play the card by end of turn, it remains exiled. If you play it, it goes wherever the it would normally go when played.').

card_ruling('aerial formation', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('aerial formation', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('aerial formation', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('aerial formation', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('aerial formation', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('aerial predation', '2012-10-01', 'You must be able to target a creature with flying to cast Aerial Predation.').
card_ruling('aerial predation', '2012-10-01', 'If the creature with flying is an illegal target when Aerial Predation tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 2 life.').

card_ruling('aerial volley', '2015-06-22', 'You choose how many targets Aerial Volley has and how the damage is divided as you cast the spell. Each target must receive at least 1 damage.').
card_ruling('aerial volley', '2015-06-22', 'If some (but not all) of the targets become illegal before Aerial Volley tries to resolve, the original division of damage still applies, but no damage is dealt to illegal targets. If all targets become illegal, Aerial Volley will be countered.').

card_ruling('aerie bowmasters', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('aerie bowmasters', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('aerie bowmasters', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('aerie ouphes', '2008-08-01', 'If Aerie Ouphes has no -1/-1 counters on it when you sacrifice it to activate its first ability, persist will return Aerie Ouphes to the battlefield with a -1/-1 counter on it before that ability resolves. The ability will check the power of Aerie Ouphes at the time you sacrificed it, not the power of the returned Aerie Ouphes (which is actually a new object with no relation to its previous existence).').
card_ruling('aerie ouphes', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('aerie ouphes', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('aerie ouphes', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('aerie ouphes', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('aerie ouphes', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('aerie ouphes', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('aerie ouphes', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('aerie worshippers', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('aerie worshippers', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('aerie worshippers', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('aerie worshippers', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('afflicted deserter', '2011-01-22', 'You choose the target artifact when Werewolf Ransacker\'s first triggered ability goes on the stack. You choose whether or not to destroy the artifact when that ability resolves.').
card_ruling('afflicted deserter', '2011-01-22', 'An artifact token that\'s destroyed is put into its owner\'s graveyard before it ceases to exist. If a token is destroyed by Werewolf Ransacker ability, Werewolf Ransacker deals damage to that token\'s controller.').
card_ruling('afflicted deserter', '2011-01-22', 'If something becomes a copy of Werewolf Ransacker, that doesn\'t count as \"transforming into Werewolf Ransacker.\" The first triggered ability of the new Werewolf Ransacker doesn\'t trigger.').
card_ruling('afflicted deserter', '2013-07-01', 'If the targeted artifact has indestructible or regenerates (or you choose not to destroy it), Werewolf Ransacker doesn\'t deal damage to that artifact\'s controller. Similarly, if the targeted artifact is destroyed but a replacement effect moves it to a different zone instead of its owner\'s graveyard, Werewolf Ransacker doesn\'t deal damage to that artifact\'s controller.').

card_ruling('afiya grove', '2004-10-04', 'If there are any creatures on the battlefield, even if they are just your opponent\'s, you must put a +1/+1 on one of them. If there are no creatures on the battlefield, then no counter is removed from this card.').

card_ruling('agadeem occultist', '2010-03-01', 'You may target any creature card in an opponent\'s graveyard with Agadeem Occultist\'s ability. You check whether the targeted card\'s converted mana cost is less than or equal to the number of Allies you control when the ability resolves. If you control too few Allies at that point, the ability does nothing.').

card_ruling('ageless sentinels', '2004-10-04', 'It becomes both creature type Giant and creature type Bird.').
card_ruling('ageless sentinels', '2005-08-01', 'Once it stops being a Wall, it can attack because it also loses Defender.').

card_ruling('agent of acquisitions', '2014-05-29', 'You choose the order the cards in that booster pack are drafted. In many cases this won’t matter, but it could be relevant if one of the cards cares about when it was drafted or what the next card you draft is.').
card_ruling('agent of acquisitions', '2014-05-29', 'The second ability of Agent of Acquisitions works only if you’re drafting a card. For example, if you’ve already used one Agent of Acquisitions in a draft round, you can’t use a second Agent of Acquisitions later in that same draft round.').

card_ruling('agent of erebos', '2014-04-26', 'The constellation ability is mandatory. If you are the only legal target (perhaps because your opponent has hexproof), you must target yourself and exile all the cards from your graveyard.').
card_ruling('agent of erebos', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('agent of erebos', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('agent of erebos', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('agent of horizons', '2013-09-15', 'Activating the ability of Agent of Horizons after it’s been blocked won’t change or undo the block.').

card_ruling('agent of the fates', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('agent of the fates', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('agent of the fates', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('aggravate', '2012-05-01', 'Aggravate targets only the player. Creatures with hexproof that player controls will be dealt damage by Aggravate if it resolves.').
card_ruling('aggravate', '2012-05-01', 'The controller of a creature dealt damage by Aggravate still decides which player or planeswalker the creature attacks.').
card_ruling('aggravate', '2012-05-01', 'If, during a player\'s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('aggravated assault', '2004-10-04', 'If you have enough mana, the ability may be activated more than once in a turn.').
card_ruling('aggravated assault', '2004-10-04', 'You will normally use this during your post-combat main phase so you can untap any creatures that attacked.').

card_ruling('aggression', '2006-10-15', 'Moving the enchantment after the ability triggers will not affect which creatures are affected.').

card_ruling('aggressive mining', '2014-07-18', 'You can activate the ability once on each player’s turn, not just your own.').
card_ruling('aggressive mining', '2014-07-18', 'Aggressive Mining doesn’t stop lands from being put onto the battlefield by a spell or ability. It only stops the special action of playing a land.').

card_ruling('agility', '2004-10-04', 'If the enchanted creature already has Flanking, its effect is cumulative.').

card_ruling('agony warp', '2008-10-01', 'The two targets may be the same creature or they may be different creatures.').

card_ruling('agoraphobia', '2013-01-24', 'Only Agoraphobia\'s controller can activate its last ability, no matter who controls the creature Agoraphobia\'s attached to.').
card_ruling('agoraphobia', '2013-01-24', 'Agoraphobia\'s last ability can be activated only while it\'s on the battlefield.').
card_ruling('agoraphobia', '2013-01-24', 'Players don\'t have priority to cast spells and activate abilities between combat damage being assigned and being dealt. This means that if you want to return Agoraphobia to its owner\'s hand before combat damage is dealt, you must do so before combat damage is assigned (and the creature will no longer get -5/-0).').

card_ruling('agrus kos, wojek veteran', '2005-10-01', 'Attacking creatures (including Agrus Kos) that are both red and white get +2/+2 until end of turn.').

card_ruling('agyrem', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('agyrem', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('agyrem', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('agyrem', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('agyrem', '2009-10-01', 'Agyrem\'s first and second abilities each set up a delayed triggered ability. These abilities will trigger at the beginning of the next end step even if Agyrem is no longer the face-up plane card.').
card_ruling('agyrem', '2009-10-01', 'Agyrem\'s first ability will return a card to the battlefield only if it\'s still in the graveyard by the time the delayed triggered ability resolves. Similarly, Agyrem\'s second ability will return a card to its owner\'s hand only if it\'s still in the graveyard by the time the delayed triggered ability resolves.').
card_ruling('agyrem', '2009-10-01', 'If a creature is put into a graveyard from the battlefield, its last existence on the battlefield is checked to determine its color.').
card_ruling('agyrem', '2009-10-01', 'A creature that caused either of Agyrem\'s first two abilities to trigger will be returned at the beginning of the next end step even if it\'s not a creature card. (For example, if it\'s a Treetop Village.)').
card_ruling('agyrem', '2009-10-01', 'If a token creature is put into a graveyard from the battlefield, one of Agyrem\'s first two abilities will trigger, but the token will cease to exist long before it would be returned somewhere.').
card_ruling('agyrem', '2009-10-01', 'If multiple creatures are put into their owners\' graveyards at the same time (due to combat damage or Planar Cleansing, for example), Agyrem\'s first two abilities trigger that many times. At the beginning of the next end step, the player whose turn it is at that time puts all of his or her Agyrem delayed triggered abilities on the stack in any order, then each other player in turn order does the same. The last ability put on the stack is the first one that resolves. The creatures are returned one at a time.').
card_ruling('agyrem', '2009-10-01', 'If you\'re affected by the {C} ability, creatures will still be able to attack planeswalkers you control.').

card_ruling('ainok bond-kin', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('ainok bond-kin', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('ainok guide', '2014-11-24', 'You choose which mode you’re using as you put the ability on the stack, after the creature has entered the battlefield. Once you’ve chosen a mode, you can’t change that mode even if the creature leaves the battlefield in response to that ability.').
card_ruling('ainok guide', '2014-11-24', 'If a mode requires a target and there are no legal targets available, you must choose the mode that adds a +1/+1 counter.').

card_ruling('ainok survivalist', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('ainok survivalist', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('ainok survivalist', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('ainok tracker', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('ainok tracker', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('ainok tracker', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('ainok tracker', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('ainok tracker', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ainok tracker', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ainok tracker', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('ainok tracker', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('ainok tracker', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('aisling leprechaun', '2004-10-04', 'Creatures change color as a triggered ability upon the block being declared.').
card_ruling('aisling leprechaun', '2004-10-04', 'The change to green does not end even if the Leprechaun leaves the battlefield.').
card_ruling('aisling leprechaun', '2004-10-04', 'A Leprechaun with Protection from Green can be blocked by a creature (since it is not green at that time) but will not take damage from the creature (since it is green during the combat damage step).').

card_ruling('ajani goldmane', '2007-10-01', 'The vigilance granted to a creature by the second ability remains until the end of the turn even if the +1/+1 counter is removed.').
card_ruling('ajani goldmane', '2007-10-01', 'The power and toughness of the Avatar created by the third ability will change as your life total changes.').
card_ruling('ajani goldmane', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('ajani goldmane', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('ajani goldmane', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('ajani goldmane', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('ajani goldmane', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('ajani goldmane', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('ajani goldmane', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('ajani goldmane', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('ajani goldmane', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('ajani goldmane', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('ajani goldmane', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('ajani steadfast', '2014-07-18', 'You may activate the first ability with no targets. If you do, you simply put a loyalty counter on Ajani to pay the cost of the ability.').
card_ruling('ajani steadfast', '2014-07-18', 'If a permanent you control is both a creature and a planeswalker, Ajani’s second ability will put both a +1/+1 counter and a loyalty counter on it.').
card_ruling('ajani steadfast', '2014-07-18', 'The emblem’s ability doesn’t consider who controls the source dealing damage to you or a planeswalker you control.').
card_ruling('ajani steadfast', '2014-07-18', 'Each attacking creature is a separate source of damage. For example, if two 3/3 creatures attack you and are unblocked, the emblem will prevent 2 damage from each creature. You’ll be dealt a total of 2 damage.').
card_ruling('ajani steadfast', '2014-07-18', 'If multiple effects modify how damage is dealt, the player being dealt damage or the controller of the permanent being dealt damage chooses the order to apply the effects. For example, Dictate of the Twin Gods says, “If a source would deal damage to a permanent or player, it deals double that damage to that permanent or player instead.” Suppose you would be dealt 3 damage while Dictate of the Twin Gods is on the battlefield and you control Ajani’s emblem. You can either (a) prevent all but 1 damage first and then let Dictate of the Twin Gods’s effect double that, for a result of being dealt 2 damage, or (b) double the damage to 6 and then prevent all but 1 of that damage.').

card_ruling('ajani vengeant', '2008-10-01', 'You may target any permanent with Ajani Vengeant\'s first ability. It doesn\'t have to be tapped.').
card_ruling('ajani vengeant', '2008-10-01', 'The first ability tracks the permanent, but not its controller. If the permanent changes controllers before its first controller\'s next untap step has come around, then it won\'t untap during its new controller\'s next untap step.').
card_ruling('ajani vengeant', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('ajani vengeant', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('ajani vengeant', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('ajani vengeant', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('ajani vengeant', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('ajani vengeant', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('ajani vengeant', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('ajani vengeant', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('ajani vengeant', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('ajani vengeant', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('ajani vengeant', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('ajani\'s presence', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('ajani\'s presence', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('ajani\'s presence', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('ajani\'s presence', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('ajani\'s presence', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('ajani\'s pridemate', '2014-07-18', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Soulmender or 8 life from Meditation Puzzle.').
card_ruling('ajani\'s pridemate', '2014-07-18', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, Ajani’s Pridemate’s ability will trigger twice. However, if a single creature you control with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').

card_ruling('ajani, caller of the pride', '2012-07-01', 'You can activate the first ability without a target. You\'ll still put one loyalty counter on Ajani when you activate the ability.').
card_ruling('ajani, caller of the pride', '2012-07-01', 'The number of Cat tokens you put onto the battlefield is equal to your life total when the third ability resolves.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('ajani, caller of the pride', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('ajani, caller of the pride', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('ajani, mentor of heroes', '2014-04-26', 'You announce how many counters will be placed on each target creature as you activate Ajani’s first ability. Each target must receive at least one counter.').
card_ruling('ajani, mentor of heroes', '2014-04-26', 'If one or more, but not all, of the targets of the first ability are illegal when that ability tries to resolve, no counters will be placed on illegal targets. You can’t change the distribution you announced when you activated the ability; the excess counters are just lost.').

card_ruling('akki avalanchers', '2004-12-01', 'The \"activate this ability only once each turn\" restriction applies even if the creature changes controllers.').

card_ruling('akki rockspeaker', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('akoum', '2012-06-01', 'A creature that\'s enchanted can\'t be chosen as the target of the chaos ability.').
card_ruling('akoum', '2012-06-01', 'If a creature that isn\'t enchanted is targeted by the chaos ability, but that creature is enchanted when that ability tries to resolve, the ability will be countered for having an illegal target.').

card_ruling('akoum firebird', '2015-08-25', 'If, during its controller’s declare attackers step, Akoum Firebird is tapped or is affected by a spell or ability that says it can’t attack, then it doesn’t attack. If there’s a cost associated with having it attack, its controller isn’t forced to pay that cost. If he or she doesn’t, Akoum Firebird doesn’t have to attack.').
card_ruling('akoum firebird', '2015-08-25', 'The landfall ability triggers only if Akoum Firebird is in your graveyard at the moment the land enters the battlefield.').
card_ruling('akoum firebird', '2015-08-25', 'Players can respond to the triggered ability, but once it starts resolving and you decide whether to pay {4}{R}{R}, it’s too late for anyone to respond.').
card_ruling('akoum firebird', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('akoum firebird', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('akoum firebird', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('akoum hellkite', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('akoum hellkite', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('akoum hellkite', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('akoum stonewaker', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('akoum stonewaker', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('akoum stonewaker', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('akrasan squire', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('akrasan squire', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('akrasan squire', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('akrasan squire', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('akrasan squire', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('akrasan squire', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('akroan conscriptor', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('akroan conscriptor', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('akroan conscriptor', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('akroan conscriptor', '2014-02-01', 'The ability can target any creature except Akroan Conscriptor, including one that’s untapped or one you already control.').
card_ruling('akroan conscriptor', '2014-02-01', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it. Notably, if that creature is enchanted by an Aura with bestow and dies while under your control, the Aura’s controller will continue to control the creature that Aura becomes.').
card_ruling('akroan conscriptor', '2014-02-01', 'If you control a legendary creature and gain control of another legendary creature with the same name, you’ll choose one to remain on the battlefield and put the other into its owner’s graveyard.').

card_ruling('akroan crusader', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('akroan crusader', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('akroan crusader', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('akroan hoplite', '2013-09-15', 'Count the number of attacking creatures you control when the ability resolves to determine the value of X.').
card_ruling('akroan hoplite', '2013-09-15', 'Once the ability resolves, the bonus won’t change even if the number of attacking creatures you control does.').

card_ruling('akroan horse', '2013-09-15', 'Akroan Horse’s enters-the-battlefield ability doesn’t target any opponent. In a multiplayer game, you choose the opponent as the ability resolves.').
card_ruling('akroan horse', '2013-09-15', 'In the last ability, “each opponent” refers to opponents of Akroan Horse’s controller. In most situations in two-player games, your opponent will control Akroan Horse and you’ll put Soldier tokens onto the battlefield.').

card_ruling('akroan line breaker', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('akroan line breaker', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('akroan line breaker', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('akroan sergeant', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('akroan sergeant', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('akroan sergeant', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('akroan skyguard', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('akroan skyguard', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('akroan skyguard', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('akroma\'s blessing', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('akroma\'s vengeance', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('akroma, angel of fury', '2007-02-01', 'If Akroma, Angel of Fury is cast face down, it can be countered.').
card_ruling('akroma, angel of fury', '2007-02-01', 'Akroma, Angel of Fury and Akroma, Angel of Wrath do not have the same name, so the legend rule does not apply.').
card_ruling('akroma, angel of fury', '2013-07-01', 'In a Commander game where this card is your commander, you may cast it face-down from the Command zone by paying {3} plus {2} for each time it has been cast from your command zone. The cost to turn it face-up is unaffected.').

card_ruling('akron legionnaire', '2004-10-04', 'If you have two or more Legionnaires, they can all attack.').

card_ruling('aku djinn', '2006-02-01', 'Only puts counters on creatures your opponents control, so in the Two-Headed Giant format, won\'t put counters on your teammate\'s creatures.').

card_ruling('akuta, born of ash', '2005-06-01', 'Akuta must already be in your graveyard as your upkeep begins in order for its ability to trigger.').
card_ruling('akuta, born of ash', '2005-06-01', 'In multiplayer games, it checks for each opponent.').

card_ruling('alabaster dragon', '2007-05-01', 'This will always be shuffled into its owner\'s library.').

card_ruling('alabaster mage', '2011-09-22', 'Multiple instances of lifelink on the same creature provide no additional benefit. The creature\'s controller gains life equal the damage dealt and no more.').

card_ruling('alaborn veteran', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('aladdin', '2004-10-04', 'Aladdin\'s ability can take control of more than one artifact, although only one each time the ability is used.').
card_ruling('aladdin', '2008-08-01', 'You do not lose control of the permanent if it stops being an artifact. The validity of the target is checked only on announcement and resolution of the ability.').

card_ruling('aladdin\'s lamp', '2004-10-04', 'If you use more than one Lamp effect, the second one will end up replacing the card being drawn with the first one.').

card_ruling('alchemist\'s refuge', '2012-05-01', 'You can cast a nonland card with flash any time you could cast an instant.').
card_ruling('alchemist\'s refuge', '2012-05-01', 'The last ability applies to nonland cards in any zone, provided something is allowing you to cast them. For example, you could cast a sorcery with flashback as though it had flash.').

card_ruling('alchemist\'s vial', '2015-06-22', 'Activating the last ability of Alchemist’s Vial targeting a creature that’s already attacking or blocking won’t cause that creature to stop attacking or blocking. It will prevent that creature from attacking or blocking in any additional combat phases the turn may have (although this is unusual).').

card_ruling('aleatory', '2004-10-04', 'You pick the target on announcement and flip the coin on resolution.').
card_ruling('aleatory', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s after the beginning of that phase\'s Declare Blockers Step.').

card_ruling('alesha\'s vanguard', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('alesha\'s vanguard', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('alesha\'s vanguard', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('alesha\'s vanguard', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('alesha, who smiles at death', '2014-11-24', 'You choose which opponent or opposing planeswalker the creature is attacking as you put it onto the battlefield. It doesn’t have to be the same player or planeswalker Alesha is attacking.').
card_ruling('alesha, who smiles at death', '2014-11-24', 'Although the creature you return is attacking, it was never declared as an attacking creature (for purposes of abilities that trigger whenever a creature attacks, for example).').

card_ruling('alexi\'s cloak', '2005-08-01', 'Any Auras already on the creature remain.').

card_ruling('alhammarret\'s archive', '2015-06-22', 'If an effect would set your life total to a specific number that’s higher than your current life total, that effect would cause you to gain life equal to the difference. Alhammarret’s Archive will then double the amount of life that effect would cause you to gain. For example, if you have 3 life and an effect says that your life total “becomes 10,” your life total will actually become 17.').
card_ruling('alhammarret\'s archive', '2015-06-22', 'If two or more replacement effects would apply to a card-drawing event, the player drawing the card chooses the order in which to apply them.').
card_ruling('alhammarret\'s archive', '2015-06-22', 'Because Alhammarret’s Archive is legendary, it’s unlikely that one player will control two. However, if that happens, life gained by that player will be multiplied by four. Three Archives will multiply that life gain by eight, and so on.').
card_ruling('alhammarret\'s archive', '2015-06-22', 'Similarly, the effects of the last abilities of multiple Archives are cumulative. If you control two, you’ll draw four times the number of cards, and so on.').
card_ruling('alhammarret\'s archive', '2015-06-22', 'In a Two-Headed Giant game, only the controller of Alhammarret’s Archive is affected by it. If that player’s teammate gains life, Alhammarret’s Archive will have no effect, even when that life gain is applied to the team’s shared life total.').

card_ruling('alhammarret, high arbiter', '2015-06-22', 'Alhammarret’s second ability happens as Alhammarret enters the battlefield. No one can cast spells or activate abilities between the time a card is named and the time that Alhammarret’s last ability starts to work.').
card_ruling('alhammarret, high arbiter', '2015-06-22', 'You choose one card name, not one name per opponent.').
card_ruling('alhammarret, high arbiter', '2015-06-22', 'Although spells with the chosen name can’t be cast, permanent cards with that name can still be put onto the battlefield by a spell or ability.').
card_ruling('alhammarret, high arbiter', '2015-06-22', 'If your opponents have no nonland cards in their hands, you can’t choose a card name. Alhammarret’s last ability won’t stop any spells from being cast in that case.').
card_ruling('alhammarret, high arbiter', '2015-06-22', 'Your opponents can still cast a card with the chosen name face down if it has a morph ability. A card with the chosen name can also be manifested.').

card_ruling('ali baba', '2004-10-04', 'This ability may be used to tap more than one Wall per turn if you have enough mana, since tapping isn\'t part of the activation cost.').
card_ruling('ali baba', '2004-10-04', 'The ability may tap walls even when Ali Baba is tapped or just entered the battlefield, since the activation cost doesn\'t include {T}.').

card_ruling('ali from cairo', '2004-10-04', 'This effect does not apply to effects which reduce your life without doing damage.').
card_ruling('ali from cairo', '2004-10-04', 'The ability works up until Ali enters the graveyard, so if he takes lethal damage or is destroyed at the same time you take damage, the ability helps you. If the damage occurs after it goes to the graveyard, however, it is not affected by the Ali which is no longer on the battlefield.').
card_ruling('ali from cairo', '2004-10-04', 'This effect does not prevent damage, it prevents the damage from turning into loss of life. So the full damage is dealt (and abilities that trigger on damage being dealt still trigger), but the full loss of life is not applied.').

card_ruling('aligned hedron network', '2015-08-25', 'Check the power of each creature as Aligned Hedron Network’s ability resolves to determine if it’s exiled. It doesn’t matter who controls the creature.').
card_ruling('aligned hedron network', '2015-08-25', 'If Aligned Hedron Network exiles multiple creatures, those cards all return to the battlefield at the same time.').
card_ruling('aligned hedron network', '2015-08-25', 'If Aligned Hedron Network leaves the battlefield before its triggered ability resolves, no creatures will be exiled.').
card_ruling('aligned hedron network', '2015-08-25', 'Auras attached to the exiled creatures will be put into their owners’ graveyards. Equipment attached to the exiled creatures will become unattached and remain on the battlefield. Any counters on the exiled creatures will cease to exist.').
card_ruling('aligned hedron network', '2015-08-25', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('aligned hedron network', '2015-08-25', 'The exiled cards return to the battlefield immediately after Aligned Hedron Network leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('aligned hedron network', '2015-08-25', 'In a multiplayer game, if Aligned Hedron Network’s owner leaves the game, the exiled cards will return to the battlefield. Because the one-shot effect that returns the cards isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').
card_ruling('aligned hedron network', '2015-08-25', 'In some very rare situations, Aligned Hedron Network may enter the battlefield as a creature with power 5 or greater. If this happens, Aligned Hedron Network will exile itself along with other creatures with power 5 or greater. Those cards will immediately return to the battlefield. If this causes a loop with Aligned Hedron Network continually exiling and returning itself, the game will be a draw unless a player breaks the loop somehow.').

card_ruling('alive', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('alive', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('alive', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('alive', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('alive', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('alive', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('alive', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('alive', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('alive', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('alive', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('alive', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('alive', '2013-04-15', 'If you cast Alive/Well as a fused split spell, the Centaur creature token will count toward the amount of life you gain.').

card_ruling('all hallow\'s eve', '2004-10-04', 'Since you can respond to triggered abilities, it is legal to sacrifice creatures using some spell or ability prior to resolving the ability that removes the final counter from All Hallow\'s Eve.').
card_ruling('all hallow\'s eve', '2007-02-01', 'This card is once again a sorcery, and its ability looks very much like suspend (although it doesn\'t have the suspend ability.').
card_ruling('all hallow\'s eve', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('all hallow\'s eve', '2009-10-01', 'The only thing that happens when All Hallow\'s Eve resolves is that it\'s exiled with two scream counters on it.').
card_ruling('all hallow\'s eve', '2009-10-01', 'All Hallow\'s Eve\'s triggered ability functions from the exile zone. This ability has an \"intervening \'if\' clause.\" It won\'t trigger at all unless All Hallow\'s Eve is exiled and has a scream counter on it (which can happen only if it resolves as a spell).').
card_ruling('all hallow\'s eve', '2009-10-01', 'Each creature returned to the battlefield by All Hallow\'s Eve enters the battlefield under its owner\'s control.').

card_ruling('all is dust', '2010-06-15', 'A colored permanent is a permanent with at least one colored mana symbol in its mana cost. Note that effects may cause a colored permanent to become colorless (as Moonlace could), or a colorless permanent to become colored (as Crimson Wisps could).').
card_ruling('all is dust', '2010-06-15', 'Tokens may also be colored permanents. The effect that creates a token states what color it is or whether it\'s colorless.').
card_ruling('all is dust', '2010-06-15', 'Lands have no mana cost, so they are colorless unless an effect states otherwise.').
card_ruling('all is dust', '2010-06-15', 'All the colored permanents are sacrificed at the same time.').
card_ruling('all is dust', '2013-07-01', 'All Is Dust doesn\'t destroy permanents. Rather, it causes them to be sacrificed. Regeneration, totem armor, and indestructibile can\'t save permanents from All Is Dust.').

card_ruling('all shall smolder in my wake', '2010-06-15', 'You may choose zero, one, two, or three targets.').

card_ruling('all suns\' dawn', '2004-12-01', 'All Suns\' Dawn can have from zero to five targets, one for each color.').
card_ruling('all suns\' dawn', '2004-12-01', 'You can target two blue-and-white multicolored cards by choosing one as the blue card and one as the white card.').
card_ruling('all suns\' dawn', '2004-12-01', 'You choose the color each target must be when you choose it as a target, so if the card you targeted as a blue card is red when All Suns\' Dawn resolves, the card is an illegal target, even if the spell doesn\'t target another red card.').

card_ruling('alley grifters', '2004-10-04', 'It triggers only once even if blocked by more than one creature.').

card_ruling('allied strategies', '2004-10-04', 'You draw one card per basic land type, not for each basic land. This makes it a maximum of 5 cards.').
card_ruling('allied strategies', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('allied strategies', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('allied strategies', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('allosaurus rider', '2006-07-15', 'Paying the alternative cost doesn\'t change when you can cast the spell. A creature spell you cast this way, for example, can still only be cast during your main phase while the stack is empty.').
card_ruling('allosaurus rider', '2006-07-15', 'You may pay the alternative cost rather than the card\'s mana cost. Any additional costs are paid as normal.').
card_ruling('allosaurus rider', '2006-07-15', 'If you don\'t have two cards of the right color in your hand, you can\'t choose to cast the spell using the alternative cost.').
card_ruling('allosaurus rider', '2006-07-15', 'You can\'t exile a card from your hand to pay for itself. At the time you would pay costs, that card is on the stack, not in your hand.').

card_ruling('alloy golem', '2004-10-04', 'If Alloy Golem phases out, you do not pick a new color when it phases in.').

card_ruling('alluring siren', '2009-10-01', 'After the ability resolves, the targeted creature attacks you only if it\'s able to do so as that turn\'s declare attackers step begins. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" it can\'t attack. If there\'s a cost associated with having that creature attack, its controller isn\'t forced to pay that cost, so the creature doesn\'t have to attack in that case either.').
card_ruling('alluring siren', '2009-10-01', 'If the targeted creature would be able to attack either you or a planeswalker you control, it must attack you, not the planeswalker.').
card_ruling('alluring siren', '2009-10-01', 'Activating the ability during your opponent\'s turn after attackers have been declared will have no effect. The same is true if you activate the ability during your own turn.').
card_ruling('alluring siren', '2011-09-22', 'If there are multiple combat phases in a turn, the creature must attack only in the first one in which it\'s able to.').

card_ruling('ally encampment', '2015-08-25', 'An “Ally spell” is any spell with the subtype Ally. Mana produced by the second ability can’t be spent to pay the costs of abilities of Allies you control.').
card_ruling('ally encampment', '2015-08-25', 'The last ability can only target an Ally on the battlefield.').

card_ruling('alms', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('alms', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('alms', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('alms', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('alms', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('alms beast', '2013-01-24', 'Multiple instances of lifelink are redundant. If a creature that had lifelink already is blocking or blocked by Alms Beast, the controller of that creature won\'t gain extra life.').

card_ruling('alpha brawl', '2011-01-22', 'The two parts of Alpha Brawl\'s effect happen sequentially: first the targeted creature deals damage, then the other creatures deal damage. State-based actions aren\'t checked in the middle, so creatures that are dealt lethal damage by the targeted creature will still deal damage.').
card_ruling('alpha brawl', '2011-01-22', 'None of the damage dealt due to Alpha Brawl is combat damage.').
card_ruling('alpha brawl', '2011-01-22', 'Combat abilities like first strike, double strike, and trample don\'t apply and will have no impact on Alpha Brawl\'s effect.').
card_ruling('alpha brawl', '2011-01-22', 'Abilities like lifelink and deathtouch that care about any kind of damage are taken into account.').
card_ruling('alpha brawl', '2011-01-22', 'If the targeted creature has infect or wither, the damage it deals will result in -1/-1 counters being put on each creature it deals damage to. The power of those creatures will be reduced when they deal damage back to the targeted creature.').
card_ruling('alpha brawl', '2011-01-22', 'The damage dealt by the targeted creature is not divided in any way. For example, if a 3/3 creature is targeted, it will deal 3 damage to each other creature controlled by its controller.').

card_ruling('alpha status', '2004-10-04', 'Alpha Status counts each creature once if that creature shares at least one creature type with the enchanted creature.').

card_ruling('altac bloodseeker', '2014-07-18', 'If a combat phase has two combat damage steps (because there’s an attacker or blocker with first strike or double strike), Altac Bloodseeker may gain first strike during the first one. If this happens, it will assign combat damage in the second combat damage step along with other creatures that haven’t assigned combat damage during that combat.').
card_ruling('altac bloodseeker', '2014-07-18', 'If the ability triggers multiple times in a single turn, the power bonuses are cumulative, but the additional instances of first strike and haste are redundant.').

card_ruling('altar of bone', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('altar of bone', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('altar of bone', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('altar of bone', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('altar of shadows', '2004-10-04', 'The precombat main phase is the first main phase of the turn. All others are postcombat main phases, even if they technically occur before combat.').
card_ruling('altar of shadows', '2004-12-01', 'You put the counter on Altar of Shadows even if the creature regenerates.').
card_ruling('altar of shadows', '2004-12-01', 'The \"precombat main phase\" is the first main phase of the turn. All other main phases are \"postcombat main phases.\"').

card_ruling('altar of the brood', '2014-09-20', 'If Altar of the Brood enters the battlefield under your control at the same time as other permanents, its ability will trigger for each of those permanents.').

card_ruling('altar of the lost', '2011-01-22', 'Altar of the Lost doesn\'t allow you to cast spells from any other player\'s graveyard, although you can spend the mana it produces on such spells if something else allows you to.').
card_ruling('altar of the lost', '2011-01-22', 'You can spend mana produced by Altar of the Lost to cast any spell with flashback that you cast from a graveyard. You don\'t have to be using flashback to cast that spell, however, as long as something else is allowing you to cast that spell.').

card_ruling('altar\'s reap', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('altar\'s reap', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('alter reality', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('alter reality', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('alter reality', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('alter reality', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('alter reality', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('alter reality', '2004-10-04', 'Changing the text of a spell will not allow you to change the targets of the spell because the targets were chosen when the spell was cast. The text change will (probably) cause it to be countered since the targets will be illegal.').
card_ruling('alter reality', '2004-10-04', 'If you change the text of a spell which is to become a permanent, the permanent will retain the text change until the effect wears off.').

card_ruling('aluren', '2004-10-04', 'The mana cost of the creatures being cast is still the stated cost on the card, even though you did not pay the cost.').
card_ruling('aluren', '2004-10-04', 'Aluren checks the actual printed cost on the creature card, and is not affected by things which allow you to cast the spell for less.').
card_ruling('aluren', '2004-10-04', 'You can\'t choose to cast a creature as though it had flash via Aluren and still pay the mana cost. You either cast the creature normally, or via Aluren without paying the mana cost.').
card_ruling('aluren', '2004-10-04', 'You can\'t use Aluren when casting a creature using another alternate means, such as the Morph ability.').
card_ruling('aluren', '2008-08-01', 'If creature with X in its cost is cast this way, X can only be 0.').

card_ruling('amass the components', '2012-05-01', 'If you cast Amass the Components with fewer than three cards in your library, you\'ll draw the remaining cards in your library, put a card from your hand on the bottom of your library, then lose the game for drawing a card from a library with no cards in it the next time state-based actions are performed.').

card_ruling('ambuscade shaman', '2015-02-25', 'If Ambuscade Shaman enters the battlefield at the same time as other creatures you control, its ability will trigger for each of those creatures.').
card_ruling('ambuscade shaman', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('ambuscade shaman', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('ambuscade shaman', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('ambuscade shaman', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('ambush commander', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('ambush krotiq', '2014-11-24', 'If you control no other creatures as Ambush Krotiq’s triggered ability resolves, nothing happens. Ambush Krotiq remains on the battlefield.').
card_ruling('ambush krotiq', '2014-11-24', 'The triggered ability doesn’t target any permanent. You choose which one to return as the ability resolves. No player can respond to this choice once the ability starts resolving.').

card_ruling('amnesia', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('amoeboid changeling', '2007-10-01', 'If a creature loses all creature types but then gains a new creature type later in the turn, it will be that new creature type.').
card_ruling('amoeboid changeling', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('amphibious kavu', '2004-10-04', 'The ability only triggers once per combat.').

card_ruling('amphin pathmage', '2014-07-18', 'Activating the ability after blockers have been declared won’t have any effect. It won’t change or undo any blocks.').

card_ruling('amrou kithkin', '2009-10-01', 'A creature with power 3 or greater can\'t be declared as a blocker for Amrou Kithkin. If the power of a creature that\'s already blocking Amrou Kithkin is increased to 3 or greater, it will continue to block Amrou Kithkin.').

card_ruling('amulet of unmaking', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('amulet of vigor', '2010-03-01', 'If you control more than one Amulet of Vigor, each Amulet\'s ability triggers when a permanent enters the battlefield tapped and under your control. The first ability that resolves will untap that permanent. If the permanent somehow becomes tapped again before the next ability resolves, the next ability will untap it as well (and so on).').
card_ruling('amulet of vigor', '2010-03-01', 'For Amulet of Vigor\'s ability to trigger, a permanent must enter the battlefield tapped due to an effect that says \"put [the permanent] onto the battlefield tapped,\" \"[this permanent] enters the battlefield tapped,\" or the like. If it enters the battlefield untapped, the ability won\'t trigger, even if you tap that permanent afterward.').

card_ruling('ana battlemage', '2007-02-01', 'The second triggered ability targets an untapped creature. If there are no other untapped creatures when Ana Battlemage enters the battlefield, it must target Ana Battlemage. If the target becomes tapped by the time the ability tries to resolve, the ability will be countered.').

card_ruling('ana sanctuary', '2004-10-04', 'The creature either gets +1/+1 or +5/+5, never +6/+6.').

card_ruling('anaba spirit crafter', '2009-10-01', 'Anaba Spirit Crafter\'s ability affects all Minotaurs, including Anaba Spirit Crafter itself.').

card_ruling('anafenza, kin-tree spirit', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('anafenza, kin-tree spirit', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('anafenza, the foremost', '2014-09-20', 'Anafenza’s first ability can target any tapped creature you control. That creature doesn’t necessarily have to be attacking.').
card_ruling('anafenza, the foremost', '2014-09-20', 'While Anafenza is on the battlefield, abilities that trigger whenever a nontoken creature your opponent owns dies won’t trigger, as that card will never reach that player’s graveyard. (Token creatures will still go to the graveyard briefly before ceasing to exist.)').
card_ruling('anafenza, the foremost', '2014-09-20', 'If your opponent discards a creature card while Anafenza is on the battlefield, abilities that function when a card is discarded (such as madness) still work, even though that card never reaches a graveyard. In addition, spells or abilities that check the characteristics of a discarded card (such as Chandra Ablaze’s first ability) can find that card in exile.').
card_ruling('anafenza, the foremost', '2014-09-20', 'Anafenza’s last ability cares only what the card would be in the zone it’s moving from, not what it would be in the graveyard. For example, if a land card you control becomes a creature due to an effect and then dies, the land card will be exiled. But if a creature card with bestow is an Aura when it would be put into the graveyard, it ends up in the graveyard.').

card_ruling('anax and cymede', '2013-09-15', 'Only creatures you control when the heroic ability resolves will get the bonuses. Creatures that come under your control later in the turn will not.').
card_ruling('anax and cymede', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('anax and cymede', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('anax and cymede', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('ancestor\'s prophet', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('ancestor\'s prophet', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('ancestral memories', '2004-10-04', 'If there are less than 7 cards in the library, look at all of them. Put two into your hand, and the rest in the graveyard.').
card_ruling('ancestral memories', '2004-10-04', 'This is not considered a draw or a discard.').

card_ruling('ancestral statue', '2015-02-25', 'Ancestral Statue’s ability is mandatory. If Ancestral Statue is the only nonland permanent you control when its ability resolves, you must return it to its owner’s hand.').
card_ruling('ancestral statue', '2015-02-25', 'The triggered ability doesn’t target any permanent. You choose which one to return as the ability resolves. No player can respond to this choice once the ability starts resolving.').

card_ruling('ancestral vengeance', '2014-11-24', 'If the creature targeted by Ancestral Vengeance’s enchant creature ability is an illegal target as Ancestral Vengeance tries to resolve, Ancestral Vengeance will be countered. It won’t enter the battlefield, and its enters-the-battlefield ability won’t trigger.').
card_ruling('ancestral vengeance', '2014-11-24', 'If Ancestral Vengeance causes the enchanted creature to have toughness 0, that creature and Ancestral Vengeance will be put into their owners’ graveyards. The enters-the-battlefield ability will still apply.').

card_ruling('ancestral vision', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('ancestral vision', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('ancestral vision', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('ancestral vision', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('ancestral vision', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('ancestral vision', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('ancestral vision', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('ancestral vision', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ancestral vision', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('ancestral vision', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ancestral vision', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('ancestral vision', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('ancestral vision', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('ancient hellkite', '2010-08-15', 'After Ancient Hellkite is declared as an attacker, or is put onto the battlefield attacking, its second ability may be activated. Unless Ancient Hellkite leaves combat, it continues to be attacking (so its ability may still be activated) through the end of combat step.').

card_ruling('ancient kavu', '2004-10-04', 'Making a permanent colorless does not make it into an artifact.').

card_ruling('ancient ooze', '2004-10-04', 'You add up the converted mana cost of all other creatures you control. Remember that tokens and face-down creatures have a cost of zero.').

card_ruling('ancient ziggurat', '2009-02-01', 'Mana produced by Ancient Ziggurat can be spent on any part of a creature spell\'s total cost. This includes additional costs (such as kicker) and alternative costs (such as evoke or Worldheart Phoenix\'s alternative cost).').
card_ruling('ancient ziggurat', '2009-02-01', 'Mana produced by Ancient Ziggurat can\'t be spent on activated abilities that put a creature card directly onto the battlefield, such as unearth or ninjutsu.').

card_ruling('angel of flight alabaster', '2011-09-22', 'The Spirit card must already be in your graveyard when the ability triggers at the beginning of your upkeep. If there is no Spirit card in your graveyard when your upkeep begins, the ability will be removed from the stack with no effect.').

card_ruling('angel of fury', '2008-10-01', 'If Angel of Fury is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').
card_ruling('angel of fury', '2008-10-01', 'Angel of Fury\'s second ability is a leaves-the-battlefield ability. Because it works only from on the battlefield, it behaves differently than the _Lorwyn_ Incarnations. When Angel of Fury is put into a graveyard from the battlefield, it will look back in time (to the point just before it left the battlefield) to see if its ability triggers. That means the following: -- If Angel of Fury lost its triggered ability while on the battlefield (due to Snakeform, for example) and then was destroyed, the ability would not trigger. Angel of Fury would stay in its owner\'s graveyard. -- If Angel of Fury had its triggered ability when it left the battlefield, but lost it when it was put into the graveyard (due to Yixlid Jailer, for example), the ability would still trigger. Angel of Fury would be shuffled into its owner\'s library.').

card_ruling('angel of glory\'s rise', '2012-05-01', 'Only Zombie permanents on the battlefield are exiled. Zombie cards in other zones are not.').

card_ruling('angel of jubilation', '2012-05-01', 'If a spell or activated ability has a cost that requires a player to pay life (as Griselbrand\'s activated ability does) or sacrifice a creature (as Fling does), that spell or ability can\'t be cast or activated.').
card_ruling('angel of jubilation', '2012-05-01', 'Other things may still cause players to pay life or sacrifice creatures, such as a resolving spell or ability.').
card_ruling('angel of jubilation', '2012-05-01', 'Angel of Jubilation\'s last ability doesn\'t look for specific words on other cards. Rather, it stops players from taking specific actions. For example, you couldn\'t activate an ability with an activation of cost of \"Sacrifice an artifact:\" by sacrificing an artifact creature (although you could sacrifice a noncreature artifact).').

card_ruling('angel of renewal', '2015-08-25', 'Count the number of creatures you control as the ability resolves to determine how much life you gain. Angel of Renewal will count toward this number if it’s still on the battlefield.').

card_ruling('angel of salvation', '2007-05-01', 'You choose how the damage prevention shields will be divided when you put the triggered ability on the stack. The number of targets must be at least one and at most five.').
card_ruling('angel of salvation', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('angel of salvation', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('angel of salvation', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('angel of salvation', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('angel of salvation', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('angel of salvation', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('angel of salvation', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('angel of serenity', '2012-10-01', 'If Angel of Serenity leaves the battlefield before its enters-the-battlefield ability has resolved, its leaves-the-battlefield ability will trigger and do nothing. Then the enters-the-battlefield ability will resolve and exile the targeted creatures and/or creature cards indefinitely.').
card_ruling('angel of serenity', '2013-01-24', 'You choose up to three total targets from among other creatures on the battlefield and creature cards in graveyards. You may choose zero targets.').
card_ruling('angel of serenity', '2013-01-24', 'You choose the targets as part of putting the enters-the-battlefield trigger on the stack. Because that ability includes “you may,” you choose whether to exile the targets when the ability resolves.').
card_ruling('angel of serenity', '2013-01-24', 'If a creature targeted by the enters-the-battlefield ability dies before that ability resolves, it will become an illegal target even though it may be a creature card in a graveyard when the ability resolves. It won\'t be exiled.').

card_ruling('angel of the dire hour', '2014-11-07', 'Creatures that were declared as attackers or entered the battlefield attacking continue to be attacking creatures until the combat phase ends. It’s possible to exile all attacking creatures after they’ve dealt combat damage if you cast Angel of the Dire Hour during the combat damage step or end of combat step.').
card_ruling('angel of the dire hour', '2014-11-07', 'If you cast Angel of the Dire Hour outside of combat, its triggered ability will trigger but have no effect.').
card_ruling('angel of the dire hour', '2014-11-07', 'If a spell or ability instructs you to put Angel of the Dire Hour onto the battlefield from your hand, that means you won’t have cast it, so its triggered ability won’t trigger.').

card_ruling('angel\'s feather', '2009-10-01', 'The ability triggers whenever any player, not just you, casts a white spell.').
card_ruling('angel\'s feather', '2009-10-01', 'If a player casts a white spell, Angel\'s Feather\'s ability triggers and is put on the stack on top of that spell. Angel\'s Feather\'s ability will resolve (causing you to gain 1 life) before the spell does.').

card_ruling('angel\'s grace', '2006-09-25', 'The last sentence applies only if your life total is being reduced by damage. Other effects or costs (such as \"lose 1 life\" or \"pay 1 life\") can reduce your life total below 1 as normal.').
card_ruling('angel\'s grace', '2006-09-25', 'You can\'t pay more life than you have.').
card_ruling('angel\'s grace', '2006-09-25', 'If your life total goes below 1, being dealt damage will not increase it back up to 1.').
card_ruling('angel\'s grace', '2006-09-25', 'After the effect wears off during the cleanup step, you\'ll lose the game if your total is 0 or less (or you have ten or more poison counters).').
card_ruling('angel\'s grace', '2006-09-25', 'Angel\'s Grace doesn\'t prevent damage. It only changes the result of damage dealt to you. For example, a creature with lifelink that deals damage to you will still cause its controller to gain life, even if that damage would reduce your life total to less than 1.').
card_ruling('angel\'s grace', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('angel\'s grace', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('angel\'s grace', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('angel\'s grace', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('angel\'s grace', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('angel\'s grace', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('angel\'s herald', '2008-10-01', 'To activate the ability, you must sacrifice three different creatures. You can\'t sacrifice just one three-colored creature, for example.').
card_ruling('angel\'s herald', '2008-10-01', 'You may sacrifice multicolored creatures as part of the cost of the search ability. If you sacrifice a multicolored creature this way, specify which part of the cost that creature is satisfying.').
card_ruling('angel\'s herald', '2008-10-01', 'You may sacrifice the Herald itself to help pay for the cost of its ability.').

card_ruling('angel\'s tomb', '2015-06-22', 'If Angel’s Tomb is already a creature when a creature enters the battlefield under your control, its ability will override any effects that set its base power and toughness to specific values, but other changes to its power and toughness (such as the one created by Titanic Growth) will still apply.').
card_ruling('angel\'s tomb', '2015-06-22', 'Effects setting Angel’s Tomb’s base power and toughness to specific values that begin to apply after Angel’s Tomb has become a creature will override the effect of Angel’s Tomb. For example, if an effect causes a 3/3 Angel’s Tomb to become 0/1, it will remain 0/1 until another effect (such as triggering the ability of Angel’s Tomb a second time or targeting it with Titanic Growth) causes those values to change.').

card_ruling('angel\'s trumpet', '2004-10-04', 'Angel\'s Trumpet affects creatures with Defender and other creatures that can\'t attack for some reason.').
card_ruling('angel\'s trumpet', '2004-10-04', 'It does not affect creatures which did not attack, but which are already tapped at the time the ability resolves.').

card_ruling('angelheart vial', '2010-06-15', 'If you\'re dealt enough damage to reduce your life total to 0 or less, you\'ll lose the game before you can put charge counters on Angelheart Vial.').

card_ruling('angelic accord', '2013-07-01', 'Angelic Accord’s ability checks how much life you’ve gained during the turn, not what your life total is compared to what it was when the turn began. For example, if you start the turn at 10 life, gain 6 life during the turn, then lose 6 life later that turn, the ability will trigger.').
card_ruling('angelic accord', '2013-07-01', 'If you haven’t gained 4 or more life during the turn when the end step begins, the ability won’t trigger at all. Gaining life during the end step won’t cause the ability to trigger.').
card_ruling('angelic accord', '2013-07-01', 'In a Two-Headed Giant game, life gained by your teammate isn’t considered, even though it causes your team’s life total to increase.').

card_ruling('angelic arbiter', '2010-08-15', 'During your turn, Angelic Arbiter\'s abilities have no effect on the game.').
card_ruling('angelic arbiter', '2010-08-15', 'During an opponent\'s turn, that opponent may either cast spells or attack with creatures, but not both (assuming that Angelic Arbiter is on the battlefield for the entirety of that turn). The player may perform other actions, such as activating abilities and playing lands.').
card_ruling('angelic arbiter', '2010-08-15', 'If Angelic Arbiter leaves the battlefield during an opponent\'s turn, its abilities cease to affect the game. For example, if an opponent casts Doom Blade to destroy Angelic Arbiter, that player may then attack with creatures.').
card_ruling('angelic arbiter', '2010-08-15', 'If Angelic Arbiter enters the battlefield during an opponent\'s turn, its abilities take actions that player performed earlier in the turn into account, even though it wasn\'t on the battlefield at the time. For example, if an opponent casts a spell, then you use Leyline of Anticipation\'s ability to cast Angelic Arbiter as though it had flash, that player won\'t be able to attack with creatures that turn.').
card_ruling('angelic arbiter', '2010-08-15', 'Angelic Arbiter\'s last ability applies if an opponent attacked with at least one creature during the current turn.').

card_ruling('angelic benediction', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('angelic benediction', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('angelic benediction', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('angelic benediction', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('angelic benediction', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('angelic benediction', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').
card_ruling('angelic benediction', '2012-07-01', 'The creature you target with the last ability will become tapped before blockers are declared.').

card_ruling('angelic captain', '2015-08-25', 'Count the number of other attacking Allies as the ability resolves to determine the size of the bonus. Attacking Allies controlled by a teammate will count toward this total.').
card_ruling('angelic captain', '2015-08-25', 'Once the ability resolves, the bonus won’t change later in the turn, even if the number of other attacking Allies does.').

card_ruling('angelic chorus', '2004-10-04', 'This does not trigger on a permanent being turned into a creature. That is just a permanent changing type, not something entering the battlefield.').
card_ruling('angelic chorus', '2004-10-04', 'Angelic Chorus does not trigger when creatures phase in or change controllers.').

card_ruling('angelic destiny', '2011-09-22', 'If the creature dies before the Angelic Destiny spell resolves, Angelic Destiny will go to its owner\'s graveyard. It won\'t return to its owner\'s hand.').
card_ruling('angelic destiny', '2011-09-22', 'If Angelic Destiny is no longer in a graveyard when its triggered ability resolves, it won\'t be returned to its owner\'s hand.').

card_ruling('angelic field marshal', '2014-11-07', 'Lieutenant abilities apply only if your commander is on the battlefield and under your control.').
card_ruling('angelic field marshal', '2014-11-07', 'Lieutenant abilities refer only to whether you control your commander, not any other player’s commander.').
card_ruling('angelic field marshal', '2014-11-07', 'If you gain control of a creature with a lieutenant ability owned by another player, that ability will check to see if you control your commander and will apply if you do. It won’t check whether its owner controls his or her commander.').
card_ruling('angelic field marshal', '2014-11-07', 'If you lose control of your commander, lieutenant abilities of creatures you control will immediately stop applying. If this causes a creature’s toughness to become less than or equal to the amount of damage marked on it, the creature will be destroyed.').
card_ruling('angelic field marshal', '2014-11-07', 'If a triggered ability granted by a lieutenant ability triggers, and in response to that trigger you lose control of your commander (causing the lieutenant to lose that ability), that triggered ability will still resolve.').

card_ruling('angelic gift', '2015-08-25', 'If the target of an Aura becomes illegal before the spell resolves, the Aura will be countered and put into its owner’s graveyard. It won’t enter the battlefield and you won’t draw a card.').

card_ruling('angelic overseer', '2011-09-22', 'If you control a Human, and an effect tries to destroy each Human you control and Angelic Overseer simultaneously, Angelic Overseer won\'t be destroyed.').
card_ruling('angelic overseer', '2011-09-22', 'If you control a Human, and lethal damage is dealt to Angelic Overseer, that damage will remain marked on it that turn. If later in the turn you no longer control a Human, Angelic Overseer will be destroyed.').

card_ruling('angelic protector', '2009-10-01', 'The ability triggers when Angelic Protector is chosen as a target for a spell or ability. The Protectors ability will be put on the stack above that spell or ability, and so resolve first.').

card_ruling('angelic renewal', '2004-10-04', 'If multiple creatures go to the graveyard at once, Angelic Renewal triggers multiple times, but it can only be sacrificed once and only brings back one creature.').
card_ruling('angelic renewal', '2004-10-04', 'If this card goes to the graveyard at the same time as a creature or sometime between when the creature left the battlefield and when the triggered ability resolves, then you will not be able to sacrifice this card and the creature can\'t be returned to the battlefield.').
card_ruling('angelic renewal', '2008-04-01', 'If an animated permanent that\'s not normally a creature is put into your graveyard from the battlefield, Angelic Renewal can return that card to the battlefield.').
card_ruling('angelic renewal', '2008-04-01', 'If a token creature is put into your graveyard from the battlefield, you can sacrifice Angelic Renewal, but nothing will be returned to the battlefield.').

card_ruling('angelic shield', '2009-10-01', 'Angelic Shield goes to the graveyard when you activate the ability, but the targeted creature does not get returned until the ability resolves. All your creatures lose the bonus while the ability is on the stack which may result in the targeted creature being destroyed before the return can take effect.').

card_ruling('angelic skirmisher', '2013-01-24', 'You choose which ability creatures you control will gain when Angelic Skirmisher\'s ability resolves. This happens before attacking creatures are declared.').
card_ruling('angelic skirmisher', '2013-01-24', 'Only creatures you control when the ability resolves will gain the chosen ability. Creatures that come under your control later in the turn will not gain the ability.').

card_ruling('angelic voices', '2004-10-04', 'You get the bonus if all of your creatures are white or artifact even if they also have other colors or qualities. So, if your only creature is a white and green creature, or a red artifact creature, you would get the bonus.').

card_ruling('angelsong', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('anger', '2004-10-04', 'The timestamp for the \"in your graveyard\" ability is set at the time that this card goes to your graveyard, regardless of whether you control a Mountain at that time.').

card_ruling('anger of the gods', '2013-09-15', 'Creatures don’t necessarily have to be dealt lethal damage by Anger of the Gods to be exiled. After being dealt damage, if they would die for any reason that turn, they’ll be exiled instead.').

card_ruling('animar, soul of elements', '2011-09-22', 'The triggered ability triggers only when a creature spell is cast, after costs are paid. The counter put on Animar for each creature spell cast won\'t affect the cost of that creature spell, only future ones.').
card_ruling('animar, soul of elements', '2011-09-22', 'The triggered ability will resolve before the creature spell does.').
card_ruling('animar, soul of elements', '2011-09-22', 'If the creature spell is countered, you\'ll still put a +1/+1 counter on Animar.').

card_ruling('animate artifact', '2008-08-01', 'This card has been returned to its original functionality. If it is enchanting an artifact that\'s already a creature, it won\'t change its power and toughness.').
card_ruling('animate artifact', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('animate dead', '2004-10-04', 'When putting a card onto the battlefield that requires a definition for its value or some other choice, you do what is needed to define the value or make the choice.').
card_ruling('animate dead', '2007-09-16', 'This is a new wording. Animate Dead is now an Aura. You target a creature card in a graveyard when you cast it. It enters the battlefield attached to that card. Then it returns that card to the battlefield, and attaches itself to that card again (since the card is treated as a new object on the battlefield).').
card_ruling('animate dead', '2007-09-16', 'Once the creature is returned to the battlefield, Animate Dead can\'t be attached to anything other than it (unless Animate Dead somehow manages to put a different creature onto the battlefield). Attempting to move Animate Dead to another creature won\'t work.').
card_ruling('animate dead', '2008-04-01', 'If the creature card put onto the battlefield has Protection from Black (or anything that prevents this from legally being attached), this won\'t be able to attach to it. Then this will go to the graveyard as a State-Based Action, causing the creature to be sacrificed.').
card_ruling('animate dead', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('animate land', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('animate wall', '2007-09-16', 'This is a change from the most recent wording. As was the case in the past, Animate Wall can now enchant only a Wall.').

card_ruling('animist\'s awakening', '2015-06-22', 'No player will know the order of the cards put on the bottom of your library. Practically speaking, the cards should be shuffled (although this is not the game action of “shuffling”).').
card_ruling('animist\'s awakening', '2015-06-22', 'If the spell mastery ability applies, you untap only the lands put onto the battlefield with Animist’s Awakening.').
card_ruling('animist\'s awakening', '2015-06-22', 'If you have X or less cards in your library, you’ll reveal all the cards from your library, put all land cards from among them onto the battlefield tapped, and then put the rest of the cards back in your library in a random order. (This is effectively the same as shuffling your library, although it’s still not technically a shuffle.)').
card_ruling('animist\'s awakening', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('ankh of mishra', '2004-10-04', 'This triggers on any land entering the battlefield. This includes playing a land or putting a land onto the battlefield using a spell or ability.').
card_ruling('ankh of mishra', '2004-10-04', 'It determines the land\'s controller at the time the ability resolves. If the land leaves the battlefield before the ability resolves, the land\'s last controller before it left is used.').

card_ruling('annihilating fire', '2012-10-01', 'The creature can still regenerate.').
card_ruling('annihilating fire', '2012-10-01', 'A creature dealt damage by Annihilating Fire that dies this turn will be exiled even if it wasn\'t the target of Annihilating Fire (say, because the damage was redirected somehow).').
card_ruling('annihilating fire', '2012-10-01', 'A creature dealt damage by Annihilating Fire will be exiled no matter why the creature would die. It could be destroyed by another spell or ability.').

card_ruling('anowon, the ruin sage', '2010-03-01', 'When the triggered ability resolves, first you choose which creature you\'ll sacrifice (if you control any non-Vampire creatures), then each other player in turn order does the same, then all chosen creatures are sacrificed at the same time.').
card_ruling('anowon, the ruin sage', '2010-03-01', 'If a player controls no creatures, or if all creatures a player controls are Vampires, that player simply doesn\'t sacrifice anything.').

card_ruling('antagonism', '2004-10-04', 'It looks back over the entire turn even if this card was not on the battlefield at the time.').

card_ruling('anthem of rakdos', '2006-05-01', 'The first ability triggers once for each creature you attack with. If you attack with three creatures, for example, they\'ll each get +2/+0 and you\'ll be dealt 3 damage.').
card_ruling('anthem of rakdos', '2006-05-01', 'The hellbent ability doubles the damage dealt to you by Anthem of Rakdos itself.').
card_ruling('anthem of rakdos', '2006-05-01', 'If damage that would be dealt to a player or creature is affected by both Anthem of Rakdos and a damage prevention effect, that player or that creature\'s controller chooses the order to apply the effects. In most cases that player will want to prevent damage, then double what\'s left.').
card_ruling('anthem of rakdos', '2006-05-01', 'If a source you control would deal damage to multiple creatures and/or players simultaneously, all of that damage is doubled.').

card_ruling('anthousa, setessan hero', '2013-09-15', 'If a land that hasn’t been under your control continuously since your last turn began becomes a creature, it can’t attack and its activated abilities that include {T} can’t be activated. Notably, it can’t be tapped for mana that turn.').
card_ruling('anthousa, setessan hero', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('anthousa, setessan hero', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('anthousa, setessan hero', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('anthroplasm', '2009-10-01', 'While Anthroplasm does become 0/0 for a moment while its ability resolves, state-based actions are not checked until the ability has finished resolving. By that time, it will have some +1/+1 counters on it (assuming X was greater than 0), and so will not be put into its owner\'s graveyard.').

card_ruling('antler skulkin', '2008-08-01', 'If a nontoken creature that gains persist this way is put into a graveyard, that card will be returned to the battlefield with a -1/-1 counter on it. However, because it\'s a new object with no relation to its previous existence, that permanent will not have persist.').
card_ruling('antler skulkin', '2008-08-01', 'If a creature has multiple instances of persist, the result is largely the same as having just one instance of persist. When the creature is put into a graveyard, each persist ability will trigger. The first one to resolve will return the creature to the battlefield with a -1/-1 counter on it. The rest will do nothing.').
card_ruling('antler skulkin', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('antler skulkin', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('antler skulkin', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('antler skulkin', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('antler skulkin', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('antler skulkin', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('antler skulkin', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('anvil of bogardan', '2004-10-04', 'If the draw part of the effect is skipped due to a replacement effect, the discard part of the effect is not skipped.').
card_ruling('anvil of bogardan', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Anvil of Bogardan onto the battlefield, you\'ll have no maximum hand size. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').
card_ruling('anvil of bogardan', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').
card_ruling('anvil of bogardan', '2013-04-15', 'If there are two Anvils on the battlefield, they trigger and resolve separately. This means that you first draw one card and discard one card, and then do it again. You do not draw two cards and then discard two cards.').

card_ruling('apathy', '2008-04-01', 'The player may discard a card at random whether the enchanted creature is tapped or not.').

card_ruling('aphetto dredging', '2004-10-04', 'You need to choose the creature type before choosing the targets.').

card_ruling('aphetto exterminator', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('aphetto grifter', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('aphetto grifter', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('aphetto runecaster', '2007-05-01', 'Triggers when any permanent is turned face up, not just a creature.').

card_ruling('aphetto vulture', '2004-10-04', 'The ability can be used to return itself to the top of your library if it is still in your graveyard when the ability resolves.').

card_ruling('aphotic wisps', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('aphotic wisps', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('aphotic wisps', '2008-05-01', 'Colorless is not a color.').

card_ruling('apocalypse hydra', '2009-02-01', 'For example, if X is 4, Apocalypse Hydra enters the battlefield with four +1/+1 counters on it, but if X is 5, it enters the battlefield with ten +1/+1 counters on it.').
card_ruling('apocalypse hydra', '2009-02-01', 'The first ability checks the number you chose for X, not the amount of mana you actually spent.').

card_ruling('apostle\'s blessing', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('apostle\'s blessing', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('apostle\'s blessing', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('apostle\'s blessing', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('apostle\'s blessing', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('apostle\'s blessing', '2011-06-01', 'You choose the target as part of casting the spell. You choose what attribute the target gains protection from when the spell resolves.').
card_ruling('apostle\'s blessing', '2011-06-01', 'Any Equipment attached to a creature that gains protection from artifacts will become unattached.').

card_ruling('appetite for brains', '2012-05-01', 'If a card in a player\'s hand has {X} in its mana cost, X is 0.').

card_ruling('apprentice sorcerer', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('approach my molten realm', '2010-06-15', 'The effect doesn\'t wear off until just before your next untap step (even if an effect will cause that untap step to be skipped).').
card_ruling('approach my molten realm', '2010-06-15', 'This ability applies no matter who or what the damage would be dealt to: a creature, a player, or a planeswalker. It also doesn\'t matter who controls the source.').
card_ruling('approach my molten realm', '2010-06-15', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage or the controller of the permanent that would be dealt damage chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn\" and Lava Axe says \"Lava Axe deals 5 damage to target player.\" Suppose a Lava Axe would deal 5 damage to a player who has cast Mending Hands targeting him or herself. The player who would be dealt damage can either (a) prevent 4 damage first and then let this scheme\'s effect double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').
card_ruling('approach my molten realm', '2010-06-15', 'If a spell or ability divides damage among multiple recipients (such as Fireball does), the damage is divided before this effect doubles it.').

card_ruling('aquamoeba', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('aquamoeba', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('aquamorph entity', '2007-02-01', 'The first ability is a replacement effect. The choice is made as Aquamorph Entity is turned face up (or enters the battlefield); it turns face up (or enters the battlefield) as a creature of that size. It doesn\'t become a 0/0 and then a creature of the chosen size.').
card_ruling('aquamorph entity', '2007-02-01', 'If a creature enters the battlefield as a copy of a face-up Aquamorph Entity, or a Vesuvan Shapeshifter turns face up as a copy of one, the controller of the creature entering the battlefield gets to make the choice.').
card_ruling('aquamorph entity', '2007-02-01', 'If a creature that\'s already on the battlefield becomes a copy of a face-up Aquamorph Entity, its power and toughness become the power and toughness that were chosen for the Entity.').

card_ruling('aqueous form', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('aqueous form', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('aqueous form', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('aqueous form', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('arachnus spinner', '2011-09-22', 'You can tap any untapped Spider you control, including one you haven\'t controlled continuously since the beginning of your most recent turn, to pay the cost of Arachnus Spinner\'s activated ability. You can even tap Arachnus Spinner itself to pay this cost.').
card_ruling('arachnus spinner', '2011-09-22', 'If you choose to search your library this way, you don\'t have to find a card named Arachnus Web, even if one is there. You\'ll still shuffle your library after searching it.').
card_ruling('arachnus spinner', '2011-09-22', 'If you choose to search your graveyard this way, you must find a card named Arachnus Web if one is there.').
card_ruling('arachnus spinner', '2011-09-22', 'You may search both your graveyard and your library, but you can find a maximum of one card named Arachnus Web.').

card_ruling('arachnus web', '2011-09-22', 'The last ability checks at the beginning of each turn\'s end step, not just your turn\'s.').
card_ruling('arachnus web', '2011-09-22', 'If the enchanted creature\'s power isn\'t 4 or greater when the end step begins, the last ability won\'t trigger at all.').
card_ruling('arachnus web', '2011-09-22', 'If the ability triggers, but the enchanted creature\'s power is reduced to 3 or less before the ability resolves, the ability will have no effect. Arachnus Web won\'t be destroyed.').
card_ruling('arachnus web', '2011-09-22', 'If Arachnus Web enters the battlefield attached to an attacking or blocking creature (due to Arachnus Spinner\'s ability, for example), that creature will continue to attack or block.').
card_ruling('arachnus web', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('arashin sovereign', '2015-02-25', 'The player who controlled Arashin Sovereign when it died chooses whether to put it on the top or bottom of its owner’s library. The player may also choose to leave Arashin Sovereign in the graveyard. This choice is made as the ability resolves.').
card_ruling('arashin sovereign', '2015-02-25', 'Arashin Sovereign will be put on the top or bottom of that library only if it’s still in the graveyard when its ability resolves. If it leaves the graveyard before that point, it will stay in whatever zone it’s in, even if it’s returned to the graveyard before the ability resolves.').

card_ruling('arashin war beast', '2014-11-24', 'Arashin War Beast’s ability will trigger only once per combat damage step, no matter how many creatures are blocking it.').
card_ruling('arashin war beast', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('arashin war beast', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('arashin war beast', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('arashin war beast', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('arashin war beast', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('arashin war beast', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('arashin war beast', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('arashin war beast', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('arashin war beast', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('arashin war beast', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('arashin war beast', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('arashin war beast', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('arashin war beast', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('arbalest elite', '2011-09-22', 'If the creature is an illegal target when Arbalest Elite\'s ability tries to resolve, the ability will be countered and none of its effects will happen. Arbalest Elite will untap during its controller\'s next untap step.').
card_ruling('arbalest elite', '2011-09-22', 'If another player controls Arbalest Elite during your next untap step, the part of the effect that keeps it from untapping does nothing. It won\'t try to apply during a future untap step.').

card_ruling('arbiter of knollridge', '2007-10-01', 'Abilities that trigger when a player gains life may trigger as a result of this ability.').
card_ruling('arbiter of knollridge', '2007-10-01', 'In other multiplayer formats, each player\'s life total is set to the highest life total within his or her range of influence. These changes happen at the same time. For example, if each player has a range of influence of one, and the players in the game are Alice (4 life), Barry (8 life), Carrie (11 life), and Doug (7 life), in that order, then Alice\'s life total will become 8 and Barry and Doug\'s life totals will each become 11.').
card_ruling('arbiter of knollridge', '2010-06-15', 'In a Two-Headed Giant game, each team chooses one person on that team to gain the appropriate amount of life. This basically means that the triggered ability will set the life total of each team to that of the team with the highest life total.').

card_ruling('arbiter of the ideal', '2014-02-01', 'The manifestation counter is a memory aid only. The permanent will continue to be an enchantment in addition to its other types even if that counter is removed.').
card_ruling('arbiter of the ideal', '2014-02-01', 'If you choose to not put the card onto the battlefield, or if the card isn’t one of the listed types, it will remain on top of your library. (Note that revealing the card is not optional.)').
card_ruling('arbiter of the ideal', '2014-02-01', 'The permanent will be an enchantment as it enters the battlefield and will cause constellation abilities of permanents you control to trigger.').
card_ruling('arbiter of the ideal', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('arbiter of the ideal', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('arbiter of the ideal', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('arbiter of the ideal', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('arbor colossus', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('arbor colossus', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('arbor colossus', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('arboria', '2004-10-04', 'This effect modifies the announcement of an attack, so it only works if it is on the battlefield at that time.').
card_ruling('arboria', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('arboria', '2009-10-01', 'Arboria doesn\'t stop creatures from attacking planeswalkers, regardless of what their controllers did or didn\'t do during their last turns.').
card_ruling('arboria', '2009-10-01', 'Arboria\'s effect cares whether a player put a nontoken permanent onto the battlefield. It\'s unusual for an ability to care who did the putting (as opposed to whose control the permanent entered the battlefield under), and requires careful reading of spells and abilities to see which player is instructed to put something onto the battlefield. For example, Exhume causes each player to return a creature card from his or her graveyard to the battlefield. If Exhume\'s controller has no creature cards in his or her graveyard, but another player does, only that other player puts a permanent onto the battlefield. It doesn\'t matter who controls the effect.').
card_ruling('arboria', '2009-10-01', 'Arboria\'s effect cares about the actions taken by players, not their results. If a player cast a spell during his or her last turn but that spell was countered, that player may still be attacked. The same is true if a player put a nontoken permanent onto the battlefield during his or her last turn, even if it\'s no longer on the battlefield by the time that player is attacked.').

card_ruling('arc blade', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('arc blade', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('arc blade', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('arc blade', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('arc blade', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('arc blade', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('arc blade', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('arc blade', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('arc blade', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('arc blade', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('arc lightning', '2014-09-20', 'You choose how many targets Arc Lightning has and how the damage is divided as you cast the spell. Each target must receive at least 1 damage.').
card_ruling('arc lightning', '2014-09-20', 'If some of the creatures are illegal targets as Arc Lightning tries to resolve, the original division of damage still applies but no damage is dealt to the illegal targets. If all targets are illegal, Arc Lightning is countered.').

card_ruling('arc mage', '2004-10-04', 'You can\'t choose zero targets. You must choose between 1 and 2 targets.').

card_ruling('arcades sabboth', '2009-10-01', 'As long as Arcades Sabboth is untapped and isn\'t attacking, it\'ll get +0/+2 from its own third ability.').

card_ruling('arcane denial', '2007-09-16', 'The controller of the countered spell doesn\'t choose how many cards to draw until the relevant ability resolves. The player may draw 0, 1, or 2 cards. He or she chooses the number before drawing any cards.').

card_ruling('arcane laboratory', '2004-10-04', 'When Arcane Laboratory enters the battlefield, it will look back over the turn to see if any spell was cast before it entered the battlefield. It will notice itself being cast.').
card_ruling('arcane laboratory', '2004-10-04', 'If a spell is countered, it still counts as the one spell cast.').
card_ruling('arcane laboratory', '2004-10-04', 'When it leaves the battlefield, the effect ends and all players are able to cast spells unrestricted again.').

card_ruling('arcane lighthouse', '2014-11-07', 'The second ability applies only to creatures controlled by your opponents when it resolves. Creatures that enter the battlefield or come under an opponent’s control later in the turn won’t be affected.').
card_ruling('arcane lighthouse', '2014-11-07', 'After the second ability resolves, continuous effects generated by the resolution of spells and abilities that would give hexproof or shroud to one of the affected creatures aren’t created. For example, after the second ability resolves, a spell cast by an opponent that gives creatures he or she controls hexproof wouldn’t cause the creatures to have hexproof. (If that spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('arcane lighthouse', '2014-11-07', 'Continuous effects generated by static abilities (such as an Aura that grants hexproof to a creature) will not apply during the turn, but will resume applying once the turn ends.').

card_ruling('arcane melee', '2012-05-01', 'Arcane Melee can\'t reduce the colored mana requirement of an instant or sorcery spell.').
card_ruling('arcane melee', '2012-05-01', 'If there are additional costs to cast a spell, such as a kicker cost or a cost imposed by another effect (such as Thalia, Guardian of Thraben\'s ability), apply those increases before applying cost reductions.').
card_ruling('arcane melee', '2012-05-01', 'Arcane Melee can reduce alternative costs such as miracle costs.').
card_ruling('arcane melee', '2012-05-01', 'The cost to cast an instant or sorcery spell with one generic mana in its mana cost will be reduced by {1}. For example, an instant spell with a mana cost of {1}{G} would cost {G} to cast.').

card_ruling('arcanum wings', '2007-05-01', 'The exchange is simultaneous, and happens on resolution.').
card_ruling('arcanum wings', '2007-05-01', 'If on resolution, half the exchange can\'t be completed (such as if the only Aura in your hand can\'t enchant the permanent), nothing happens.').

card_ruling('arcbond', '2014-11-24', 'The creature is the source of the new damage. If you control that creature as it deals damage, you may choose to redirect that damage from an opponent to a planeswalker he or she controls.').
card_ruling('arcbond', '2014-11-24', 'The delayed triggered ability Arcbond creates will trigger even if the target creature is dealt lethal damage. For example, if a 3/3 creature is targeted by Arcbond and later in the turn it blocks a 7/7 creature, the 3/3 creature will deal 7 damage each other creature and each player.').
card_ruling('arcbond', '2014-11-24', 'Damage dealt by the creature because of the ability Arcbond creates isn’t combat damage, even if it was combat damage that caused the ability to trigger.').
card_ruling('arcbond', '2014-11-24', 'If two Arcbonds have resolved targeting different creatures, and damage is dealt to one of them, the delayed triggered ability will cause that creature to deal damage to each other creature and player. This will cause the delayed triggered ability from the other Arcbond to trigger. The second creature will then deal damage to each other creature and player. This will continue until one or both of the creatures die or the game ends. If this doesn’t happen (perhaps because both creatures have indestructible and damage dealt to the players is being prevented), and no player breaks the loop, the game will be a draw.').

card_ruling('arcbound bruiser', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound crusher', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound fiend', '2004-12-01', 'The creature that\'s targeted by Arcbound Fiend\'s middle ability doesn\'t have to have any +1/+1 counters on it. If it doesn\'t have a counter on it when the ability resolves, nothing happens.').
card_ruling('arcbound fiend', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound hybrid', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound lancer', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound overseer', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound ravager', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound reclaimer', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound slith', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound stinger', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound wanderer', '2004-12-01', 'This card has modular--sunburst, a combination of two keyword abilities. The sunburst ability sets the number for the modular ability. Basically, the number of +1/+1 counters the Wanderer gets as it enters the battlefield is equal to the number of colors of mana used to pay its cost.').
card_ruling('arcbound wanderer', '2004-12-01', 'For the leaves-the-battlefield ability, the number of counters on Arcbound Wanderer at the time it left the battlefield is what\'s relevant; the number of colors of mana used to pay its cost doesn\'t matter anymore.').
card_ruling('arcbound wanderer', '2004-12-01', 'When the Wanderer leaves the battlefield, you may put the counters onto a single target artifact creature. You can\'t split them among two or more artifact creatures.').
card_ruling('arcbound wanderer', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('arcbound worker', '2006-09-25', 'If this creature gets enough -1/-1 counters put on it to cause its toughness to be 0 or less (or the damage marked on it to be lethal), modular will put a number of +1/+1 counters on the target artifact creature equal to the number of +1/+1 counters on this creature before it left the battlefield.').

card_ruling('archaeomancer', '2012-07-01', 'If an instant or sorcery spell puts Archaeomancer onto the battlefield, you can return that card to your hand.').

card_ruling('archangel of strife', '2011-09-22', 'Archangel of Strife\'s second and third abilities are linked. Similarly, its second and fourth abilities are linked. The bonus a player\'s creatures get depends only on the choice that player made when Archangel of Strife entered the battlefield.').
card_ruling('archangel of strife', '2011-09-22', 'The active player chooses war or peace first, followed by each other player in turn order. Each player will know what previous players chose when making his or her choice.').
card_ruling('archangel of strife', '2011-09-22', 'Archangel of Strife\'s bonuses apply immediately. Depending on your choice, Archangel of Strife will enter the battlefield as either a 9/6 or 6/9 creature.').
card_ruling('archangel of strife', '2011-09-22', 'If there are two Archangels of Strife on the battlefield, and you choose war for one and peace for the other, your creatures get +3/+3.').
card_ruling('archangel of strife', '2011-09-22', 'As soon as Archangel of Strife leaves the battlefield, the bonus stops applying. If it returns to the battlefield, players will be able to make new choices.').

card_ruling('archangel of thune', '2013-07-01', 'The ability triggers just once from each life-gaining event, whether it’s 1 life from Soulmender or 5 life from Elixir of Immortality.').
card_ruling('archangel of thune', '2013-07-01', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, Archangel of Thune’s ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('archangel of thune', '2013-07-01', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('archangel of tithes', '2015-06-22', 'If you control an untapped Archangel of Tithes, your opponents can choose to not attack with a creature that must attack if able. The same is true with respect to an attacking Archangel of Tithes and a creature that must block if able.').
card_ruling('archangel of tithes', '2015-06-22', 'In a Two-Headed Giant game, if one player controls Archangel of Tithes, creatures can’t attack that player’s team or a planeswalker that player controls unless their controller pays {1} for each of those creatures he or she controls. Creatures can attack planeswalkers controlled by that player’s teammate without having to pay this cost.').

card_ruling('archangel\'s light', '2011-01-22', 'Archangel\'s Light isn\'t put into the graveyard until after its instructions are followed. You won\'t gain 2 life for it, and it won\'t be shuffled into your library.').
card_ruling('archangel\'s light', '2011-01-22', 'If you have no cards in your graveyard, you won\'t gain any life, but you\'ll still shuffle your library.').

card_ruling('archdemon of unx', '2008-10-01', 'If Archdemon of Unx is the only non-Zombie creature you control when the triggered ability resolves, you\'ll have to sacrifice it. You\'ll still get a Zombie token.').
card_ruling('archdemon of unx', '2008-10-01', 'If you somehow control no non-Zombie creatures when the triggered ability resolves, you won\'t have to sacrifice anything. You\'ll still get a Zombie token.').

card_ruling('archery training', '2004-10-04', 'If there is more than one Archery Training on a creature, each granted ability counts the counters on the Archery Training that granted that ability.').
card_ruling('archery training', '2004-10-04', 'You do not remove counters when using the ability.').
card_ruling('archery training', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('archetype of aggression', '2014-02-01', 'The Archetype’s second ability applies to each creature controlled by any of your opponents, no matter when it entered the battlefield.').
card_ruling('archetype of aggression', '2014-02-01', 'While you control an Archetype, continuous effects generated by the resolution of spells and abilities that would give the specified ability to creatures your opponents control aren’t created. For example, if you control Archetype of Courage, a spell cast by an opponent that gives creatures he or she controls first strike wouldn’t cause the creatures to have first strike, even if later in the turn Archetype of Courage left the battlefield. (If the spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('archetype of aggression', '2014-02-01', 'Conversely, continuous effects generated by static abilities (such as an Aura that granted the appropriate ability) would resume applying if the Archetype left the battlefield.').
card_ruling('archetype of aggression', '2014-02-01', 'If you and an opponent each control the same Archetype, no creature controlled by any player will have the appropriate ability.').

card_ruling('archetype of courage', '2014-02-01', 'The Archetype’s second ability applies to each creature controlled by any of your opponents, no matter when it entered the battlefield.').
card_ruling('archetype of courage', '2014-02-01', 'While you control an Archetype, continuous effects generated by the resolution of spells and abilities that would give the specified ability to creatures your opponents control aren’t created. For example, if you control Archetype of Courage, a spell cast by an opponent that gives creatures he or she controls first strike wouldn’t cause the creatures to have first strike, even if later in the turn Archetype of Courage left the battlefield. (If the spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('archetype of courage', '2014-02-01', 'Conversely, continuous effects generated by static abilities (such as an Aura that granted the appropriate ability) would resume applying if the Archetype left the battlefield.').
card_ruling('archetype of courage', '2014-02-01', 'If you and an opponent each control the same Archetype, no creature controlled by any player will have the appropriate ability.').

card_ruling('archetype of endurance', '2014-02-01', 'The Archetype’s second ability applies to each creature controlled by any of your opponents, no matter when it entered the battlefield.').
card_ruling('archetype of endurance', '2014-02-01', 'While you control an Archetype, continuous effects generated by the resolution of spells and abilities that would give the specified ability to creatures your opponents control aren’t created. For example, if you control Archetype of Courage, a spell cast by an opponent that gives creatures he or she controls first strike wouldn’t cause the creatures to have first strike, even if later in the turn Archetype of Courage left the battlefield. (If the spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('archetype of endurance', '2014-02-01', 'Conversely, continuous effects generated by static abilities (such as an Aura that granted the appropriate ability) would resume applying if the Archetype left the battlefield.').
card_ruling('archetype of endurance', '2014-02-01', 'If you and an opponent each control the same Archetype, no creature controlled by any player will have the appropriate ability.').

card_ruling('archetype of finality', '2014-02-01', 'The Archetype’s second ability applies to each creature controlled by any of your opponents, no matter when it entered the battlefield.').
card_ruling('archetype of finality', '2014-02-01', 'While you control an Archetype, continuous effects generated by the resolution of spells and abilities that would give the specified ability to creatures your opponents control aren’t created. For example, if you control Archetype of Courage, a spell cast by an opponent that gives creatures he or she controls first strike wouldn’t cause the creatures to have first strike, even if later in the turn Archetype of Courage left the battlefield. (If the spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('archetype of finality', '2014-02-01', 'Conversely, continuous effects generated by static abilities (such as an Aura that granted the appropriate ability) would resume applying if the Archetype left the battlefield.').
card_ruling('archetype of finality', '2014-02-01', 'If you and an opponent each control the same Archetype, no creature controlled by any player will have the appropriate ability.').

card_ruling('archetype of imagination', '2014-02-01', 'The Archetype’s second ability applies to each creature controlled by any of your opponents, no matter when it entered the battlefield.').
card_ruling('archetype of imagination', '2014-02-01', 'While you control an Archetype, continuous effects generated by the resolution of spells and abilities that would give the specified ability to creatures your opponents control aren’t created. For example, if you control Archetype of Courage, a spell cast by an opponent that gives creatures he or she controls first strike wouldn’t cause the creatures to have first strike, even if later in the turn Archetype of Courage left the battlefield. (If the spell has additional effects, such as raising the power of the creatures, those effects will apply as normal.)').
card_ruling('archetype of imagination', '2014-02-01', 'Conversely, continuous effects generated by static abilities (such as an Aura that granted the appropriate ability) would resume applying if the Archetype left the battlefield.').
card_ruling('archetype of imagination', '2014-02-01', 'If you and an opponent each control the same Archetype, no creature controlled by any player will have the appropriate ability.').

card_ruling('archfiend of depravity', '2014-11-24', 'The opponent chooses which creatures to spare, if any, as the ability resolves. This choice doesn’t target the creatures.').
card_ruling('archfiend of depravity', '2014-11-24', 'The opponent may choose to spare one or no creatures. If the player doesn’t choose any creatures, he or she will sacrifice all creatures he or she controls.').

card_ruling('architects of will', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('archive trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('archive trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('archive trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('archive trap', '2009-10-01', 'Archive Trap checks only whether an opponent searched his or her library. It doesn\'t matter whether that player found a card during the search.').
card_ruling('archive trap', '2009-10-01', 'A spell or ability causes a player to search his or her library only if it specifically contains the word \"search\" in its text. For example, if a spell or ability lets a player look at the top four cards of his or her library and do something with one of those cards, that\'s not a search.').
card_ruling('archive trap', '2009-10-01', 'If a search effect is affected by Aven Mindcensor\'s ability (which causes a player to search the top four cards of his or her library instead), that still counts as searching that library.').
card_ruling('archive trap', '2009-10-01', 'The opponent you target doesn\'t have to be the opponent who searched his or her library.').
card_ruling('archive trap', '2009-10-01', 'If the targeted opponent has fewer than thirteen cards in his or her library, that player puts his or her entire library into his or her graveyard.').

card_ruling('archmage ascension', '2009-10-01', 'Archmage Ascension\'s first ability has an \"intervening \'if\' clause.\" It won\'t trigger at all unless you\'ve already drawn two or more cards by the time the end step begins.').
card_ruling('archmage ascension', '2009-10-01', 'If an effect would cause you to draw multiple cards while Archmage Ascension has six or more quest counters on it, each individual draw may be replaced by Archmage Ascension\'s effect. Process the draws one at a time. Even if you use the reasonable shortcut of finding all of the cards at once and only physically shuffling once, the game will consider you to have searched and shuffled once per card.').
card_ruling('archmage ascension', '2009-10-01', 'If a spell or ability causes you to put cards in your hand without specifically using the word \"draw,\" Archmage Ascension\'s abilities ignore it.').
card_ruling('archmage ascension', '2009-10-01', 'If two or more replacement effects would apply to a card-drawing event, the player who\'s drawing the card chooses what order to apply them. It\'s possible that after applying one of them, the others will no longer be applicable because the player would no longer draw a card. For example, if you control more than one Archmage Ascension with six quest counters on it and you would draw a card, each Archmage Ascension\'s replacement effect could apply. Once you use one, the rest are no longer applicable.').

card_ruling('archon of redemption', '2010-03-01', 'The creature\'s power is determined as the triggered ability resolves, so it will take into account any effects that modify it. If the creature has left the battlefield by the time the triggered ability resolves, its last existence on the battlefield is checked to determine its power.').

card_ruling('archon of the triumvirate', '2012-10-01', 'The two nonland permanents can be controlled by the same opponent or by different opponents.').
card_ruling('archon of the triumvirate', '2012-10-01', 'The nonland permanents will be detained before blockers are chosen. Notably, a creature detained this way can\'t block before it is detained.').
card_ruling('archon of the triumvirate', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('archon of the triumvirate', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('archon of the triumvirate', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('archon of the triumvirate', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('archon of the triumvirate', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('archon of the triumvirate', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('archwing dragon', '2012-05-01', 'Archwing Dragon is only returned to its owner\'s hand if it\'s on the battlefield.').

card_ruling('arctic aven', '2012-07-01', 'Multiple instances of lifelink aren\'t cumulative. Activating Arctic Aven\'s ability more than once during a single turn won\'t cause you to gain more life.').

card_ruling('arctic nishoba', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('arctic wolves', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('arcum dagsson', '2006-07-15', 'Arcum Dagsson\'s ability targets an artifact creature, not the controller of one.').
card_ruling('arcum dagsson', '2006-07-15', 'Arcum Dagsson\'s ability can target any permanent that\'s both an artifact and a creature, such as a Grizzly Bears while Mycosynth Lattice is on the battlefield.').

card_ruling('arcum\'s weathervane', '2006-10-15', 'Adds the snow or removes the snow supertype to or from a land. Adding snow doesn\'t overwrite other supertypes.').
card_ruling('arcum\'s weathervane', '2006-10-15', 'Can\'t be used on a dual land to add the Snow Supertype even though it is a basic land subtype. You can use it on any land to remove the snow supertype.').

card_ruling('arcum\'s whistle', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('ardent plea', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('ardent plea', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('ardent plea', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('ardent plea', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('ardent plea', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('ardent plea', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('ardent plea', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('arena', '2004-10-04', 'The power of the creatures when the effect resolves is used to determine how much damage is done.').
card_ruling('arena', '2004-10-04', 'In multiplayer games, you can choose a different opposing player each time it the ability is activated.').
card_ruling('arena', '2004-10-04', 'Either or both creatures can be tapped prior to using the ability and it still works.').
card_ruling('arena', '2006-09-25', 'If either target is illegal at the time the ability resolves (whether because it has left the battlefield, because it has gained Shroud or Protection, or for any other reason), the ability taps the remaining target, but no damage is dealt. If both targets are illegal, the ability is countered.').

card_ruling('arena athlete', '2013-09-15', 'The heroic ability won’t cause a creature that’s already blocking to stop blocking or be removed from combat.').
card_ruling('arena athlete', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('arena athlete', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('arena athlete', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('aretopolis', '2012-06-01', 'For both abilities that count the number of scroll counters on Aretopolis, count all scroll counters on it, not just ones you put there.').

card_ruling('argent mutation', '2011-06-01', 'You can target an artifact with Argent Mutation. When it resolves, you\'ll simply draw a card.').
card_ruling('argent mutation', '2011-06-01', 'If the permanent is an illegal target when Argent Mutation tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').

card_ruling('argent sphinx', '2011-01-01', 'If you activate Argent Sphinx\'s metalcraft ability, Argent Sphinx will return to the battlefield at the beginning of the next end step no matter how many artifacts you control at that time.').
card_ruling('argent sphinx', '2011-01-01', 'If you activate Argent Sphinx\'s metalcraft ability during a turn\'s end step, Argent Sphinx will return to the battlefield at the beginning of the following turn\'s end step.').
card_ruling('argent sphinx', '2011-01-01', 'If you control an Argent Sphinx owned by another player and activate its ability, Argent Sphinx will return to the battlefield under your control at the beginning of the next end step. You\'ll retain control of it indefinitely.').

card_ruling('argentum armor', '2011-01-01', 'The second ability triggers and resolves during the declare attackers step. The target permanent will be destroyed before blockers are declared.').
card_ruling('argentum armor', '2011-01-01', 'You may target any permanent with the triggered ability, not just one controlled by the defending player. If no other player controls a permanent, you must target one of your own.').

card_ruling('argothian elder', '2004-10-04', 'The ability can target an already untapped land.').
card_ruling('argothian elder', '2004-10-04', 'The ability must target two different lands. It can\'t be activated if there is just one land to target.').

card_ruling('argothian enchantress', '2005-08-01', 'The ability triggers when you announce an Enchantment spell.').

card_ruling('argothian wurm', '2007-02-01', 'When Argothian Wurm enters the battlefield, first the active player gets the option to sacrifice a land. If he or she declines, the next player in turn order gets the option. If a player elects to sacrifice a land, Argothian Wurm is put on top of its owner\'s library, but then all remaining players still get the option. If all players decline, then nothing happens and Argothian Wurm stays on the battlefield.').

card_ruling('arm with æther', '2011-06-01', 'Only creatures you control on the battlefield when Arm with AEther resolves gain the ability. Creatures you gain control of later in the turn won\'t be affected.').
card_ruling('arm with æther', '2011-06-01', 'The affected creatures keep the ability even if another player gains control of them that turn. If such a creature deals damage to you, the ability will trigger, targeting a creature you control.').
card_ruling('arm with æther', '2011-06-01', 'The granted ability triggers on all damage dealt by the creature to one of your opponents, not just combat damage.').
card_ruling('arm with æther', '2011-06-01', 'In a multiplayer game, if an affected creature deals damage to two of your opponents simultaneously, the ability will trigger twice.').

card_ruling('armageddon clock', '2004-10-04', 'Players can remove as many counters as they can pay for each turn.').

card_ruling('armament corps', '2014-09-20', 'The enters-the-battlefield ability can target Armament Corps itself.').
card_ruling('armament corps', '2014-09-20', 'You choose how the counters will be distributed as you put the ability on the stack. Notably, if you choose to put one +1/+1 counter on each of two target creatures, and one of those creatures becomes an illegal target in response, the +1/+1 counter that would’ve gone on that creature is lost. It can’t be put on the remaining legal target.').

card_ruling('armament of nyx', '2014-04-26', 'Armament of Nyx continuously checks to see whether the creature it’s enchanting is an enchantment or not. If the creature isn’t an enchantment and becomes one, or it is an enchantment and stops being one, the effect will change.').

card_ruling('armed', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('armed', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('armed', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('armed', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('armed', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('armed', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('armed', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('armed', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('armed', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('armed', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('armed', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('armor of thorns', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('armor sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('armor sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('armored ascension', '2009-10-01', 'This ability counts the number of Plains controlled by Armored Ascension\'s controller, not the enchanted creature\'s controller (in case they\'re different players).').
card_ruling('armored ascension', '2009-10-01', 'This bonus is not fixed; it changes as the number of Plains you control changes.').
card_ruling('armored ascension', '2010-08-15', 'The ability cares about lands with the land type Plains, not necessarily lands named Plains.').

card_ruling('armored guardian', '2004-10-04', 'You can activate the ability that grants shroud in response to this card being targeted by a spell or ability. If you do so, then (if this is the spell or abilities only target) the spell or ability will be countered since this card will now be an illegal target.').

card_ruling('armored skaab', '2011-09-22', 'If you have fewer than four cards in your library when Armored Skaab enters the battlefield, you\'ll put all of them into your graveyard.').

card_ruling('armory guard', '2012-10-01', 'Vigilance only matters when Armory Guard is being declared as an attacking creature. If Armory Guard is already attacking, losing control of your only Gate won\'t cause Armory Guard to become tapped or to leave combat.').

card_ruling('armory of iroas', '2014-04-26', 'The +1/+1 counter is put on the equipped creature before combat damage is dealt.').
card_ruling('armory of iroas', '2014-04-26', 'If you move Armory of Iroas from one creature to another, any +1/+1 counters on the first creature remain where they are.').

card_ruling('arms dealer', '2012-07-01', 'You can sacrifice Arms Dealer to activate its own ability.').

card_ruling('arrest', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('arrogant bloodlord', '2010-06-15', 'The power of the other creature is checked only when it blocks or becomes blocked by Arrogant Bloodlord. Once the Bloodlord\'s ability triggers, it will be destroyed at end of combat (after combat damage has been dealt) even if the other creature\'s power changes before then.').
card_ruling('arrogant bloodlord', '2010-06-15', 'If Arrogant Bloodlord blocks or becomes blocked by a creature whose power is 2 or greater, Arrogant Bloodlord\'s ability won\'t trigger. It still won\'t trigger if the other creature\'s power changes to 1 or less later in combat.').
card_ruling('arrogant bloodlord', '2010-06-15', 'If Arrogant Bloodlord becomes blocked by multiple creatures with power 1 or less, its ability triggers that many times. Each of those abilities will cause it to be destroyed separately (in case Arrogant Bloodlord isn\'t destroyed the first time due to an Aura with totem armor, for example). The same is true if it somehow blocks multiple creatures with power 1 or less.').

card_ruling('arrow storm', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('arrow storm', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('arrow volley trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('arrow volley trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('arrow volley trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('arrow volley trap', '2009-10-01', 'The number of targets must be at least 1 and at most 5. You divide the damage as you cast Arrow Volley Trap, not as it resolves. Each target must be assigned at least 1 damage.').

card_ruling('arrows of justice', '2013-01-24', 'An “attacking creature” or “blocking creature” is one that has been declared as an attacker or blocker this combat, or one that was put onto the battlefield attacking or blocking this combat. Unless that creature leaves combat, it continues to be an attacking or blocking creature through the end of combat step, even if the player it was attacking has left the game, the planeswalker it was attacking has left combat, or the creature or creatures it was blocking have left combat, as appropriate.').
card_ruling('arrows of justice', '2013-01-24', 'Arrows of Justice may be cast during the end of combat step, after combat damage has been dealt.').

card_ruling('arsenal thresher', '2009-05-01', 'If multiple Arsenal Threshers are entering the battlefield at the same time (for example, if Warp World is resolving), you may reveal the same artifact cards from your hand for each of them.').
card_ruling('arsenal thresher', '2009-05-01', 'If Arsenal Thresher and another artifact are entering the battlefield from your hand at the same time, you may reveal that other artifact for Arsenal Thresher. If that artifact is a second Arsenal Thresher, you may reveal each Arsenal Thresher for the other.').

card_ruling('artful maneuver', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('artful maneuver', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('artful maneuver', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('artful maneuver', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('artful maneuver', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('artful maneuver', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('artful maneuver', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('artful maneuver', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('artifact mutation', '2004-10-04', 'This spell is countered and you get no Saprolings if the artifact is an illegal target on resolution.').

card_ruling('artificer\'s epiphany', '2015-06-22', 'If you control at least one artifact as Artificer’s Epiphany resolves, you can’t discard a card, even if you want to.').

card_ruling('artificial evolution', '2004-10-04', 'If a creature\'s creature type has been changed by a spell or ability, changing the card\'s text won\'t affect or alter the new creature type.').
card_ruling('artificial evolution', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('artificial evolution', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('artificial evolution', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('artificial evolution', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('artificial evolution', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('artificial evolution', '2004-10-04', 'You can\'t change proper nouns (i.e. card names) such as \"Island Fish Jasconius\".').
card_ruling('artificial evolution', '2004-10-04', 'Changing the text of a spell will not allow you to change the targets of the spell because the targets were chosen when the spell was cast. The text change will (probably) cause it to be countered since the targets will be illegal.').
card_ruling('artificial evolution', '2004-10-04', 'If you change the text of a spell which is to become a permanent, the permanent will retain the text change until the effect wears off.').

card_ruling('artillerize', '2011-06-01', 'The artifact or creature is sacrificed as costs are paid. Players can\'t respond to the paying of costs by trying to destroy that artifact or creature.').

card_ruling('artisan of forms', '2013-09-15', 'You choose the target for the triggered ability when the ability is put onto the stack. You choose whether or not Artisan of Forms becomes a copy of that creature when the ability resolves.').
card_ruling('artisan of forms', '2013-09-15', 'Artisan of Forms copies the printed values of the creature plus any copy effects that have been applied to it. It won’t copy any other effects that have changed that creature’s power, toughness, color, and so on. Artisan of Forms won’t copy any counters on the creature, but Artisan of Forms will retain any counters it already had on it.').
card_ruling('artisan of forms', '2013-09-15', 'If Artisan of Forms becomes a copy of a token creature, it copies the original characteristics of that token as defined by the effect that put it onto the battlefield. It won’t become a token creature.').
card_ruling('artisan of forms', '2013-09-15', 'The copy effect lasts indefinitely. Often, it will last until it is overwritten by another copy effect (if it copies another creature on a future turn, perhaps).').
card_ruling('artisan of forms', '2013-09-15', 'If the creature is an illegal target when the ability tries to resolve, the ability will be countered. Artisan of Forms won’t become a copy of that creature. It remains whatever it was a copy of.').
card_ruling('artisan of forms', '2013-09-15', 'When Artisan of Forms becomes a copy of a creature, it’s neither entering nor leaving the battlefield. Any enters-the-battlefield or leaves-the-battlefield abilities won’t trigger.').
card_ruling('artisan of forms', '2013-09-15', 'If another creature becomes a copy of Artisan of Forms, it will become a copy of whatever Artisan of Forms is currently copying (if anything), plus it will have the triggered ability.').
card_ruling('artisan of forms', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('artisan of forms', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('artisan of forms', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('artisan of kozilek', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('artisan of kozilek', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('artisan of kozilek', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').

card_ruling('artisan\'s sorrow', '2013-09-15', 'The artifact or enchantment won’t be on the battlefield when you scry, unless it regenerated or had indestructible.').
card_ruling('artisan\'s sorrow', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('artisan\'s sorrow', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('artisan\'s sorrow', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('artisan\'s sorrow', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('ascendant evincar', '2007-07-15', 'If Ascendant Evincar stops being black, it gives itself -1/-1.').

card_ruling('asceticism', '2011-01-01', 'You may target any creature with the regeneration ability, not just one you control.').

card_ruling('ash zealot', '2012-10-01', 'Ash Zealot\'s ability doesn\'t allow any player to cast cards from a graveyard. Such cards need another way to be cast, such as the flashback ability.').
card_ruling('ash zealot', '2012-10-01', 'Ash Zealot\'s triggered ability will resolve before that spell resolves.').
card_ruling('ash zealot', '2012-10-01', 'Ash Zealot\'s ability doesn\'t trigger if you put a card onto the battlefield from a graveyard (using Grave Betrayal\'s ability, for example).').

card_ruling('ashcloud phoenix', '2014-09-20', 'If Ashcloud Phoenix is face down, you can turn it face up for its morph cost, even if you didn’t cast Ashcloud Phoenix face down using its morph ability.').
card_ruling('ashcloud phoenix', '2014-09-20', 'If Ashcloud Phoenix leaves the graveyard before its “dies” ability resolves, it won’t return to the battlefield.').
card_ruling('ashcloud phoenix', '2014-09-20', 'Once Ashcloud Phoenix returns to the battlefield face down, each player will know which face-down creature it is. You can’t mix up the positions of your face-down permanents to disguise this.').
card_ruling('ashcloud phoenix', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('ashcloud phoenix', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('ashcloud phoenix', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('ashcloud phoenix', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('ashcloud phoenix', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ashcloud phoenix', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ashcloud phoenix', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('ashcloud phoenix', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('ashcloud phoenix', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('ashen ghoul', '2008-10-01', 'A card is \"above\" another card in your graveyard if it was put into that graveyard later.').
card_ruling('ashen ghoul', '2008-10-01', 'layers may not rearrange the cards in their graveyards.').
card_ruling('ashen ghoul', '2008-10-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('ashen ghoul', '2008-10-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('ashen ghoul', '2008-10-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('ashenmoor liege', '2008-05-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('ashes of the fallen', '2005-06-01', 'If Spirit is the chosen creature type, the soulshift ability will be able to target any creature card in your graveyard of the appropriate converted mana cost. Suppose two creatures, including one with soulshift, are put into your graveyard from the battlefield at the same time. The Spirit creature type is added as the cards enter the graveyard, just before you choose the target for the soulshift ability.').

card_ruling('ashes to ashes', '2009-10-01', 'If one of the targeted creatures is an illegal target by the time Ashes to Ashes resolves, it still exiles the other one and deals 5 damage to you.').
card_ruling('ashes to ashes', '2009-10-01', 'If both of the targeted creatures are illegal targets by the time Ashes to Ashes resolves, it\'s countered. It doesn\'t deal 5 damage to you.').

card_ruling('ashiok\'s adept', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('ashiok\'s adept', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('ashiok\'s adept', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('ashiok, nightmare weaver', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('ashiok, nightmare weaver', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').
card_ruling('ashiok, nightmare weaver', '2013-09-15', 'Ashiok’s second ability refers to any creature card exiled because of Ashiok’s first or third ability.').
card_ruling('ashiok, nightmare weaver', '2013-09-15', 'Ashiok’s second ability doesn’t target any creature card. You choose which creature card to return when that ability resolves, but you must choose one with converted mana cost equal to the value you chose for X when activating the ability.').
card_ruling('ashiok, nightmare weaver', '2013-09-15', 'If Ashiok leaves the battlefield, and later another Ashiok enters the battlefield, it is a new object (even if the two were represented by the same card). Creature cards exiled by the original Ashiok can’t be put onto the battlefield by the second ability of the new Ashiok.').
card_ruling('ashiok, nightmare weaver', '2013-09-15', 'If you put a creature card with bestow onto the battlefield, it will be a creature on the battlefield, not an Aura.').
card_ruling('ashiok, nightmare weaver', '2013-09-15', 'If you put a creature card with {X} in its mana cost onto the battlefield, the value of that X is 0.').

card_ruling('ashling the pilgrim', '2007-10-01', '\"That much damage\" refers to the number of +1/+1 counters removed from Ashling the Pilgrim. Since the counters are removed, it\'s likely that the damage will kill Ashling.').
card_ruling('ashling the pilgrim', '2007-10-01', 'Counts resolutions, not activations. Any such abilities that are still on the stack won\'t count toward the total.').
card_ruling('ashling the pilgrim', '2007-10-01', 'When the ability resolves, it counts the number of times that same ability from that this creature has already resolved that turn. It doesn\'t matter who controlled the creature or the previous abilities when they resolved. A copy of this ability (created by Rings of Brighthearth, for example) will count toward the total. Abilities from other creatures with the same name don\'t count towards the total. Neither does an ability that\'s been countered.').
card_ruling('ashling the pilgrim', '2007-10-01', 'You get the bonus only the third time the ability resolves. You won\'t get the bonus the fourth, fifth, sixth, or any subsequent times.').

card_ruling('ashling\'s prerogative', '2007-10-01', 'The \"chosen value\" is either \"odd\" or \"even.\" So either creatures with odd converted mana costs enter the battlefield tapped and creatures with even converted mana costs have haste, or vice versa.').
card_ruling('ashling\'s prerogative', '2007-10-01', 'If a creature has X in its mana cost, that X is treated as 0 for the purposes of these effects. It doesn\'t matter what the value of X was while the creature was on the stack.').

card_ruling('ashling, the extinguisher', '2013-07-01', 'The target of the ability is chosen when Ashling\'s ability is put on the stack. When the ability resolves, the player dealt combat damage by Ashling sacrifices that creature if he or she still controls it. Neither regeneration nor indestructibile can save a creature from being sacrificed.').

card_ruling('ashmouth hound', '2011-09-22', 'Ashmouth Hound\'s ability triggers once for each creature it blocks or becomes blocked by.').

card_ruling('ashnod\'s cylix', '2004-10-04', 'If the player has less than 3 cards in his or her library, look at as many as he or she has, put one back, and exile the others.').

card_ruling('ashnod\'s transmogrant', '2007-09-16', 'It\'s the ability, not the counter that makes the creature an artifact. The creature remains an artifact even if the +1/+1 counter is removed.').

card_ruling('asmira, holy avenger', '2004-10-04', 'The ability counts creatures that went to the graveyard before this card entered the battlefield.').

card_ruling('aspect of hydra', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('aspect of hydra', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('aspect of hydra', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('aspect of hydra', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('aspect of hydra', '2014-02-01', 'The value of X is calculated as Aspect of Hydra resolves. The bonus won’t change later in the turn, even if your devotion to green does.').

card_ruling('aspect of wolf', '2009-10-01', 'The effect is continuously updated based on the number of Forests you control at any given time.').

card_ruling('assassin\'s strike', '2012-10-01', 'You must be able to target a creature to cast Assassin\'s Strike.').
card_ruling('assassin\'s strike', '2012-10-01', 'If the creature is an illegal target when Assassin\'s Strike tries to resolve, it will be countered and none of its effects will happen. Its controller won\'t discard a card.').
card_ruling('assassin\'s strike', '2013-07-01', 'If Assassin\'s Strike resolves but the creature isn\'t destroyed (perhaps because it regenerated or has indestructible), its controller will discard a card.').

card_ruling('assault formation', '2015-02-25', 'For example, a 2/3 creature will assign 3 combat damage rather than 2.').
card_ruling('assault formation', '2015-02-25', 'The first ability doesn’t actually change any creature’s power. It changes only the value of the combat damage it assigns. All other rules and effects that check power or toughness use the real values.').

card_ruling('assault suit', '2014-11-07', 'The equipped creature can’t be sacrificed for any reason. If an effect instructs you to sacrifice it, you can’t and it remains on the battlefield. You also can’t sacrifice it to pay a cost that requires you to sacrifice a creature.').
card_ruling('assault suit', '2014-11-07', 'If an effect instructs you to sacrifice a creature, and you control any creatures other than the creature equipped with Assault Suit, you must sacrifice one of them. You can’t try to sacrifice the equipped creature, fail, and therefore ignore the effect.').

card_ruling('assemble the legion', '2013-01-24', 'If Assemble the Legion isn\'t on the battlefield when its ability resolves, use the number of muster counters it had when it was last on the battlefield to determine how many Soldier tokens to put onto the battlefield.').

card_ruling('assembly hall', '2004-10-04', 'You do not have to find a card if you do not want to.').

card_ruling('assembly-worker', '2006-09-25', 'The ability looks for creatures with the Assembly-Worker subtype, not permanents named Assembly-Worker.').

card_ruling('astral arena', '2012-06-01', 'Astral Arena\'s abilities stop more than one creature from being declared as an attacker or as a blocker each combat. However, it doesn\'t affect creatures that are put onto the battlefield attacking or blocking.').
card_ruling('astral arena', '2012-06-01', 'If a turn has multiple combat phases, either a different creature or the same one can attack or block in each of them.').

card_ruling('astral cornucopia', '2014-02-01', 'If you choose 1 for the value of X, Astral Cornucopia will cost {3} to cast and enter the battlefield with one charge counter. If you choose 2 for the value of X, it will cost {6} to cast and enter the battlefield with two charge counters, and so on.').
card_ruling('astral cornucopia', '2014-02-01', 'The last ability is a mana ability. It doesn’t use the stack and can’t be responded to.').

card_ruling('astral slide', '2004-10-04', 'If a player cycles a card during the end step, the creature that is exiled won\'t come back until the end of the next turn.').
card_ruling('astral slide', '2008-08-01', 'If a face-down card is exiled, it turns face-up and remains face-up when it returns to the battlefield. This does not trigger any abilities that would trigger on it being turned face-up').

card_ruling('astral steel', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('astral steel', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('astral steel', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('astral steel', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('astral steel', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('atalya, samite master', '2004-10-04', 'Cost modifiers which add a cost are not restricted to only white mana.').
card_ruling('atalya, samite master', '2004-10-04', 'Spending only white mana applies to the ability regardless of which mode you choose.').

card_ruling('atarka beastbreaker', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('atarka beastbreaker', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('atarka beastbreaker', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('atarka efreet', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('atarka efreet', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('atarka efreet', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('atarka monument', '2015-02-25', 'A Monument can’t attack on the turn it enters the battlefield.').
card_ruling('atarka monument', '2015-02-25', 'Each Monument is colorless, although the last ability will make each of them two colors until end of turn.').
card_ruling('atarka monument', '2015-02-25', 'If a Monument has any +1/+1 counters on it, those counters will remain on the permanent after it stops being a creature. Those counters will have no effect as long as the Monument isn’t a creature, but they will apply again if the Monument later becomes a creature.').
card_ruling('atarka monument', '2015-02-25', 'Activating the last ability of a Monument while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

card_ruling('atarka pummeler', '2015-02-25', 'Activating Atarka Pummeler’s ability more than once conveys no additional benefit to creatures you control.').
card_ruling('atarka pummeler', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('atarka pummeler', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('atarka pummeler', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('atarka\'s command', '2015-02-25', 'You choose the two modes as you cast the spell. You must choose two different modes. Once modes are chosen, they can’t be changed.').
card_ruling('atarka\'s command', '2015-02-25', 'You can choose a mode only if you can choose legal targets for that mode. Ignore the targeting requirements for modes that aren’t chosen. For example, you can cast Ojutai’s Command without targeting a creature spell provided you don’t choose the third mode.').
card_ruling('atarka\'s command', '2015-02-25', 'As the spell resolves, follow the instructions of the modes you chose in the order they are printed on the card. For example, if you chose the second and fourth modes of Ojutai’s Command, you would gain 4 life and then draw a card. (The order won’t matter in most cases.)').
card_ruling('atarka\'s command', '2015-02-25', 'If a Command is copied, the effect that creates the copy will usually allow you to choose new targets for the copy, but you can’t choose new modes.').
card_ruling('atarka\'s command', '2015-02-25', 'If all targets for the chosen modes become illegal before the Command resolves, the spell will be countered and none of its effects will happen. If at least one target is still legal, the spell will resolve but will have no effect on any illegal targets.').

card_ruling('athreos, god of passage', '2014-04-26', 'You are a creature’s owner if the card representing it began the game in your deck, or if it’s a token that entered the battlefield under your control.').
card_ruling('athreos, god of passage', '2014-04-26', 'Athreos’s last ability will trigger if a token creature you own dies. The target opponent has the option to pay 3 life, although the token can’t return to your hand.').
card_ruling('athreos, god of passage', '2014-04-26', 'It doesn’t matter who controlled the creature when it died. Athreos’s last ability will trigger if you owned that creature.').
card_ruling('athreos, god of passage', '2014-04-26', 'The target opponent chooses whether to pay 3 life when Athreos’s last ability resolves. You won’t return the card to your hand if that player pays 3 life or if the card leaves the graveyard before the ability resolves.').
card_ruling('athreos, god of passage', '2014-04-26', 'If there are no legal targets for Athreos’s last ability (perhaps because each of your opponents has hexproof), it will be removed from the stack with no effect. No one may pay 3 life and you won’t return the creature card to your hand.').
card_ruling('athreos, god of passage', '2014-04-26', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('athreos, god of passage', '2014-04-26', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('athreos, god of passage', '2014-04-26', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('athreos, god of passage', '2014-04-26', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('athreos, god of passage', '2014-04-26', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('athreos, god of passage', '2014-04-26', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('athreos, god of passage', '2014-04-26', 'Your devotion to two colors is equal to the number of mana symbols that are the first color, the second color, or both colors among the mana costs of permanents you control. Specifically, a hybrid mana symbol counts only once toward your devotion to its two colors. For example, if the only nonland permanents you control are Pharika, God of Affliction and Golgari Guildmage (whose mana cost is {B/G}{B/G}), your devotion to black and green is four.').
card_ruling('athreos, god of passage', '2014-04-26', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('athreos, god of passage', '2014-04-26', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('athreos, god of passage', '2014-04-26', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('attunement', '2004-10-04', 'If you have less than 4 cards, discard all you have.').

card_ruling('augur of bolas', '2012-07-01', 'You don\'t reveal the other two cards, nor do you reveal their order on the bottom of the library.').
card_ruling('augur of bolas', '2012-07-01', 'If there are fewer that three cards in your library when Augur of Bolas\'s ability resolves, you\'ll look at the remaining cards. This won\'t cause you to lose the game as you haven\'t drawn from an empty library.').

card_ruling('auntie\'s snitch', '2008-04-01', 'The last ability will trigger only if Auntie\'s Snitch is in your graveyard at the moment a Goblin or Rogue deals combat damage to a player. The ability will check again when it tries to resolve. If Auntie\'s Snitch hasn\'t left the graveyard by this time, you may return it to your hand.').
card_ruling('auntie\'s snitch', '2008-04-01', 'If a Goblin or Rogue you control deals combat damage to a player while Auntie\'s Snitch is in your graveyard, you can return Auntie\'s Snitch to your hand, then you can cast it for its prowl cost that turn.').

card_ruling('aura barbs', '2005-02-01', 'The damage is dealt by the enchantments, not by Aura Barbs.').

card_ruling('aura blast', '2004-10-04', 'You don\'t draw a card if this spell is countered. This can happen if the target is illegal on resolution.').
card_ruling('aura blast', '2004-10-04', 'You still draw a card if the enchantment regenerates.').

card_ruling('aura extraction', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('aura finesse', '2010-06-15', 'You may target any Aura you control, and you may target any creature (including one you don\'t control). Whether the Aura could legally enchant the creature is irrelevant as you\'re choosing the targets.').
card_ruling('aura finesse', '2010-06-15', 'If you target an Aura that\'s attached to a creature, you may also target the creature it\'s attached to. Aura Finesse won\'t cause the Aura to move, but you\'ll still draw a card when it resolves.').
card_ruling('aura finesse', '2010-06-15', 'As Aura Finesse resolves, if the targeted Aura can\'t legally enchant the targeted creature (for example, because the Aura is red and the creature has protection from red, or because the Aura has \"enchant creature you control\" and an opponent controls the creature), the spell resolves but the Aura doesn\'t move. You still draw a card.').
card_ruling('aura finesse', '2010-06-15', 'As Aura Finesse resolves, if either target is illegal, the spell resolves but the Aura doesn\'t move. You still draw a card. If both targets are illegal, Aura Finesse is countered and you don\'t draw a card.').
card_ruling('aura finesse', '2010-06-15', 'You still control the Aura, even if you attach it to a creature you don\'t control.').

card_ruling('aura flux', '2004-10-04', 'If two or more Aura Fluxes are on the battlefield, they will give the text to each other. And they will cause each other enchantment to have two triggered abilities for which the player would have to pay.').
card_ruling('aura flux', '2004-10-04', 'This card adds a triggered ability to all other enchantments.').
card_ruling('aura flux', '2004-10-04', 'You choose whether to pay {2} or not on resolution. If not, then you sacrifice the enchantment.').

card_ruling('aura gnarlid', '2010-06-15', 'The power of each potential blocking creature is checked against Aura Gnarlid\'s power only as blockers are declared. Once a creature blocks Aura Gnarlid, increasing the Gnarlid\'s power or decreasing the blocking creature\'s power won\'t undo that block.').
card_ruling('aura gnarlid', '2010-06-15', 'Aura Gnarlid\'s second ability counts each Aura regardless of who controls it.').

card_ruling('aura graft', '2004-10-04', 'If there is no legal place to move the enchantment, then it doesn\'t move but you still control it.').
card_ruling('aura graft', '2007-07-15', 'Aura Graft\'s effect has no duration. You\'ll retain control of the Aura until the game ends, the Aura leaves the battlefield, or an effect causes someone else to gain control of the Aura.').
card_ruling('aura graft', '2007-07-15', 'You can target an Aura you already control just to move that Aura to a new permanent.').

card_ruling('aura mutation', '2004-10-04', 'This spell is countered and you get no Saprolings if the enchantment is an illegal target on resolution.').

card_ruling('aura of silence', '2004-10-04', 'Aura of Silence affects all opponents in a multiplayer game.').

card_ruling('aura thief', '2004-10-04', 'Gaining control of an enchantment often isn\'t very interesting since it probably won\'t change what the enchantment is doing. It only matters if the enchantment does something specifically to \"you\" or \"an opponent\" or if the enchantment has an activated ability (which only the controller can use).').

card_ruling('auramancer\'s guise', '2007-02-01', 'Auramancer\'s Guise counts itself when determining the power and toughness bonus.').

card_ruling('auratouched mage', '2005-10-01', 'Any Aura card you find must be able to enchant Auratouched Mage as it currently exists, or as it most recently existed on the battlefield if it\'s no longer on the battlefield. If an effect has made the Mage an artifact, for example, you could search for an Aura with \"enchant artifact.\"').

card_ruling('aurelia\'s fury', '2013-01-24', 'You announce the value of X and how the damage will be divided as part of casting Aurelia\'s Fury. Each chosen target must receive at least 1 damage.').
card_ruling('aurelia\'s fury', '2013-01-24', 'Aurelia\'s Fury can\'t deal damage to both a planeswalker and that planeswalker\'s controller. If damage dealt by Aurelia\'s Fury is redirected from a player to a planeswalker he or she controls, that player will be able to cast noncreature spells that turn. If you want to stop a player from casting noncreature spells this turn, you can\'t choose to redirect the damage to a planeswalker he or she controls.').
card_ruling('aurelia\'s fury', '2013-01-24', 'If Aurelia\'s Fury has multiple targets, and some but not all of them are illegal targets when Aurelia\'s Fury resolves, Aurelia\'s Fury will still deal damage to the remaining legal targets according to the original damage division.').
card_ruling('aurelia\'s fury', '2013-01-24', 'If all of the targets are illegal when Aurelia\'s Fury tries to resolve, it will be countered and none of its effects will happen. No creature or player will be dealt damage.').

card_ruling('aurelia, the warleader', '2013-01-24', 'You untap all creatures you control, including ones that aren\'t attacking.').
card_ruling('aurelia, the warleader', '2013-01-24', 'You don\'t have to attack with any creatures during the additional combat phase.').
card_ruling('aurelia, the warleader', '2013-01-24', 'If Aurelia is put onto the battlefield attacking, the triggered ability won\'t trigger.').

card_ruling('aurification', '2008-10-01', 'Aurification gives the creature type \"Wall\" to creatures with gold counters on them in addition to granting them defender.').

card_ruling('auriok champion', '2004-12-01', 'Gaining life when a creature enters the battlefield is optional.').

card_ruling('auriok edgewright', '2011-01-01', 'If Auriok Edgewright has double strike as the combat damage step begins, there will be a second combat damage step. As that second combat damage step begins, if Auriok Edgewright no longer has double strike (perhaps because an artifact creature you controlled was destroyed), Auriok Edgewright will not assign combat damage a second time.').

card_ruling('auriok replica', '2011-01-01', 'You choose a source of damage as the ability resolves. Since the ability doesn\'t target the source, you could choose a source with protection from artifacts, for example.').
card_ruling('auriok replica', '2011-01-01', 'If the chosen source would deal damage to you and to other creatures, players, and/or planeswalkers, only the damage that would be dealt to you is prevented.').

card_ruling('auriok salvagers', '2013-06-07', 'If a card in a graveyard has X in its mana cost, X is considered to be 0.').

card_ruling('auriok siege sled', '2004-12-01', 'Remember that an effect that says you \"can\'t\" do something overrides an effect that says you can or must do something. If you activate both of Auriok Siege Sled\'s abilities on the same creature, that creature can\'t block Auriok Siege Sled this turn.').

card_ruling('auriok sunchaser', '2011-01-01', 'For the purposes of blocking (either because Auriok Sunchaser is attacking or because a creature with flying is attacking Auriok Sunchaser\'s controller), whether Auriok Sunchaser has flying is checked only as the declare blockers step begins.').
card_ruling('auriok sunchaser', '2011-01-01', 'If Auriok Sunchaser blocks a creature with flying, causing it to lose flying (by destroying an artifact its controller controls, for example) won\'t change that.').

card_ruling('auriok survivors', '2011-06-01', 'If Auriok Survivors is no longer on the battlefield when its enters-the-battlefield ability resolves, the Equipment will return to the battlefield and remain unattached.').

card_ruling('aurochs', '2004-10-04', 'The bonus is not applied until after you check for attack legality, so it is a 2/3 creature when declaring attackers.').
card_ruling('aurochs', '2009-10-01', 'The ability counts the number of attacking creatures with the creature type Aurochs, not just those named Aurochs.').

card_ruling('austere command', '2007-10-01', 'If the first and third modes are chosen, an artifact creature with converted mana cost 3 or less would be destroyed twice by this spell. It would have to be regenerated twice to survive. (A similar thing happens with most of the other combinations.)').

card_ruling('autochthon wurm', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('autochthon wurm', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('autochthon wurm', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('autochthon wurm', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('autochthon wurm', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('autochthon wurm', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('autochthon wurm', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('autumn willow', '2004-10-04', 'This spell can be countered. Shroud does not work until it enters the battlefield.').

card_ruling('autumn\'s veil', '2010-08-15', 'Autumn\'s Veil itself may be countered by blue or black spells. Its effect doesn\'t apply until after it resolves.').
card_ruling('autumn\'s veil', '2010-08-15', 'After Autumn\'s Veil resolves, any spell you control that turn can still be targeted by spells that try to counter it (such as Cancel), regardless of their color. If those spells are blue or black at the time they would resolve, they will resolve, but the part of their effect that would counter the spell you control simply won\'t do anything. Any other effects those spells have will work as normal.').
card_ruling('autumn\'s veil', '2010-08-15', 'If a creature you control is being targeted by a spell when Autumn\'s Veil resolves, nothing happens right away. When that spell would resolve, its color is checked. If it\'s blue or black, that creature will be an illegal target for that spell, and will be unable to be affected by it. If all that spell\'s targets have become illegal by the time it would resolve, it\'s countered.').
card_ruling('autumn\'s veil', '2010-08-15', 'After Autumn\'s Veil resolves, no new blue or black spell may be cast that turn targeting a creature you control.').
card_ruling('autumn\'s veil', '2010-08-15', 'Keep in mind that an Aura spell targets the permanent it will enchant (but an Aura on the battlefield doesn\'t target the permanent it\'s attached to).').
card_ruling('autumn\'s veil', '2010-08-15', 'Autumn\'s Veil affects any spells and creatures you happen to control at any point during the rest of the turn, not just spells and creatures you control as it resolves. That\'s because it doesn\'t grant an ability to those spells or creatures; rather, it affects the game rules and states something that\'s now true about those spells and creatures.').
card_ruling('autumn\'s veil', '2010-08-15', 'Autumn\'s Veil has no effect on abilities. After it resolves, spells you control may be countered by abilities from blue or black sources, and creatures you control may be targeted by abilities from blue or black sources.').

card_ruling('avacyn\'s collar', '2011-01-22', 'The triggered ability checks whether the equipped creature as it last existed on the battlefield was a Human. (It doesn\'t matter what its creature types are in the graveyard.)').

card_ruling('avacyn, angel of hope', '2013-07-01', 'Creatures with indestructible still have damage marked on them, even though that damage won\'t destroy them. If Avacyn leaves the battlefield, creatures that lose indestructible and have lethal damage marked on them will be destroyed.').
card_ruling('avacyn, angel of hope', '2013-07-01', 'A permanent with indestructible can\'t be destroyed, but it can still be sacrificed, exiled, put into a graveyard, and so on.').

card_ruling('avacyn, guardian angel', '2014-07-18', 'The possible color choices are white, blue, black, red, and green. You can’t choose artifact, colorless, or any other word.').
card_ruling('avacyn, guardian angel', '2014-07-18', 'For each activated ability, you choose the color as the ability resolves. However, the color of a source that would deal damage to the creature or player is checked only when it would deal that damage, not as Avacyn’s ability resolves. For example, if you choose blue, damage a blue creature would deal is prevented, even if that creature wasn’t blue (or wasn’t on the battlefield) when Avacyn’s ability resolved.').
card_ruling('avacyn, guardian angel', '2014-07-18', 'Whether the target creature or player is still a legal target is no longer checked after the appropriate ability resolves. For example, if a creature targeted by the first ability gains protection from white after the ability resolves but before it prevents damage, the effect will still prevent damage.').
card_ruling('avacyn, guardian angel', '2014-07-18', 'The second activated ability doesn’t prevent damage that would be dealt to planeswalkers the target player controls. If that player is you, it can still prevent noncombat damage that your opponent would want to redirect from you to one of your planeswalkers. Just apply the ability’s prevention effect to that damage first, and there won’t be any damage to redirect.').
card_ruling('avacyn, guardian angel', '2014-07-18', 'Both activated abilities prevent all damage of the chosen color, not just combat damage.').
card_ruling('avacyn, guardian angel', '2014-07-18', 'Neither activated ability has any effect on damage that’s already been dealt.').

card_ruling('avalanche tusker', '2014-09-20', 'If the target creature can’t block Avalanche Tusker, but could block another attacking creature, it’s free to block that creature or block no creatures at all.').
card_ruling('avalanche tusker', '2014-09-20', 'If, during the declare blockers step, the target creature is tapped or is affected by a spell or ability that says it can’t block, then it doesn’t block. If there’s a cost associated with having that creature block, its controller isn’t forced to pay that cost. If the player doesn’t, the creature doesn’t block in that case either.').

card_ruling('avarice totem', '2004-12-01', 'It is possible to activate this ability in response to itself and generate some odd combinations. For example, if you control this card and another permanent, you can use this card\'s ability and target the permanent you control. You can then use this card\'s ability again and target a permanent your opponent controls. The second usage resolves first and you get your opponent\'s permanent in exchange for this one. The first usage then resolves and swaps your other permanent for the Totem so you get it back. The net effect is that you can swap any non-land permanent you have for any of theirs if you can activate this ability twice. Note that your opponent does get the chance to use the Totem in between the resolutions of your two usages if they have the mana.').

card_ruling('avatar of fury', '2004-10-04', 'The converted mana cost of this card is still 8, even if you only pay {R}{R} to cast it.').

card_ruling('avatar of hope', '2004-10-04', 'The converted mana cost of this card is still 8, even if you only pay {W}{W} to cast it.').

card_ruling('avatar of might', '2004-10-04', 'The converted mana cost of this card is still 8, even if you only pay {G}{G} to cast it.').

card_ruling('avatar of slaughter', '2011-09-22', 'If, during a player\'s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('avatar of slaughter', '2011-09-22', 'If there are multiple combat phases in a turn, creatures must attack only in the first one in which they\'re able to.').

card_ruling('avatar of the resolute', '2015-02-25', 'When determining how many +1/+1 counters Avatar of the Resolute enters the battlefield with, don’t count any creatures entering the battlefield at the same time, even if those creatures enter the battlefield with +1/+1 counters on them.').

card_ruling('avatar of will', '2004-10-04', 'The converted mana cost of this card is still 8, even if you only pay {U}{U} to cast it.').

card_ruling('avatar of woe', '2004-10-04', 'The converted mana cost of this card is still 8, even if you only pay {B}{B} to cast it.').

card_ruling('aven brigadier', '2004-10-04', 'It gives +2/+2 to creatures that are both Birds and Soldiers.').

card_ruling('aven farseer', '2007-05-01', 'Triggers when any permanent is turned face up, not just a creature.').

card_ruling('aven mimeomancer', '2009-05-01', 'If the second ability affects a creature, it doesn\'t cause that creature to lose any of its abilities. It just gives that creature flying as well.').
card_ruling('aven mimeomancer', '2009-05-01', 'The affected creature remains 3/1 and has flying as long as it has a feather counter on it. The effect continues even if Aven Mimeomancer leaves the battlefield.').
card_ruling('aven mimeomancer', '2009-05-01', 'You may put a feather counter on a creature that already has a feather counter on it. Doing so has no visible effect unless some other effect has changed the creature\'s power or toughness, or caused it to lose flying, since the last feather counter was put on it.').
card_ruling('aven mimeomancer', '2009-05-01', 'If all feather counters on a creature are moved to a different creature, the ability doesn\'t follow them. The first creature stops being affected by the ability because it no longer has a feather counter on it. The second creature is not affected by the ability because Aven Mimeomancer didn\'t target it.').
card_ruling('aven mimeomancer', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('aven mindcensor', '2007-05-01', 'This checks which player is searching a library, not which library is being searched.').
card_ruling('aven mindcensor', '2007-05-01', 'The search works exactly the same as normal except that the player doing the searching sees only the top four cards of that library. The player can\'t look at the other cards in that library at all. Cards not in the top four cards of the library can\'t be found in the search, even if they\'re identifiable in some manner.').
card_ruling('aven mindcensor', '2007-05-01', 'After the search is complete, if the effect has the player shuffle that library, the entire library is shuffled.').
card_ruling('aven mindcensor', '2007-05-01', 'If the player is searching the top four cards of his or her library, the player may cast a Panglacial Wurm located there.').
card_ruling('aven mindcensor', '2007-05-01', 'Shadow of Doubt prevents libraries from being searched at all, so its effect trumps this one.').

card_ruling('aven sunstriker', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('aven sunstriker', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('aven sunstriker', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('aven surveyor', '2014-11-24', 'You choose which mode you’re using as you put the ability on the stack, after the creature has entered the battlefield. Once you’ve chosen a mode, you can’t change that mode even if the creature leaves the battlefield in response to that ability.').
card_ruling('aven surveyor', '2014-11-24', 'If a mode requires a target and there are no legal targets available, you must choose the mode that adds a +1/+1 counter.').

card_ruling('aven tactician', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('aven tactician', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('aven trailblazer', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('aven trailblazer', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('aven trailblazer', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').
card_ruling('aven trailblazer', '2009-02-01', 'Aven Trailblazer\'s second ability is a \"characteristic-defining ability.\" It applies at all times in all zones.').

card_ruling('avenger of zendikar', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('avenger of zendikar', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('avenging angel', '2008-04-01', 'This has been restored as a triggered ability that triggers when it is put into a graveyard from the battlefield, rather than a replacement effect. The Angel does go to the graveyard, and will trigger any other abilities that look for this event.').

card_ruling('avenging arrow', '2012-10-01', 'It doesn\'t matter what the creature dealt damage to. If the creature dealt damage to another creature or a planeswalker, it doesn\'t matter whether that creature or planeswalker is still on the battlefield.').
card_ruling('avenging arrow', '2012-10-01', 'Avenging Arrow doesn\'t affect the damage the creature dealt. It isn\'t retroactively prevented.').

card_ruling('avenging druid', '2004-10-04', 'The ability does not work if all damage is prevented.').
card_ruling('avenging druid', '2004-10-04', 'Putting a land onto the battlefield does not count as playing a land.').

card_ruling('avizoa', '2004-10-04', 'You skip the next untap step that you are not skipping for any other reason. In other words, the skips save up until you skip as many as required.').

card_ruling('avoid fate', '2011-01-22', 'Avoid Fate must target an instant spell or an Aura spell as it\'s cast, and it must be targeting an instant or an Aura spell at the time it resolves. It will still resolve if it starts out targeting a spell of one type but the target changes to a spell of the other type (thanks to Shunt, for instance) before it resolves.').

card_ruling('awaken the ancient', '2013-07-01', 'Awaken the Ancient can enchant any land with the subtype Mountain, not just one named Mountain.').
card_ruling('awaken the ancient', '2013-07-01', 'The enchanted Mountain retains any other types or subtypes it may have. You can still tap it to add {R} to your mana pool. It can be affected by anything that affects either a land or a creature.').
card_ruling('awaken the ancient', '2013-07-01', 'This doesn’t count as a creature entering the battlefield. The Mountain was already on the battlefield; it only changed types.').
card_ruling('awaken the ancient', '2013-07-01', 'If the enchanted Mountain stops being a Mountain for any reason, Awaken the Ancient will be put into its owner’s graveyard as a state-based action.').
card_ruling('awaken the ancient', '2013-07-01', 'Copies of the enchanted Mountain won’t be 7/7 red Giant creatures with haste. For example, if Awaken the Ancient is enchanting a basic Mountain, a copy of that creature would just be a basic Mountain.').

card_ruling('awakener druid', '2009-10-01', 'Awakener Druid\'s ability affects a land with the land type Forest, not necessarily a land with the name Forest.').
card_ruling('awakener druid', '2009-10-01', 'Awakener Druid\'s ability is mandatory. When it enters the battlefield, you must target a Forest (if there is one), even if you don\'t control that Forest.').
card_ruling('awakener druid', '2009-10-01', 'If Awakener Druid leaves the battlefield before its enters-the-battlefield ability resolves, nothing happens to the targeted Forest when that ability resolves. It won\'t become a creature.').
card_ruling('awakener druid', '2009-10-01', 'If the targeted Forest entered the battlefield this turn, it will be affected by \"summoning sickness\" once it becomes a Treefolk. You won\'t be able to attack with it or use its activated abilities that have {T} in the cost (including its mana ability).').
card_ruling('awakener druid', '2009-10-01', 'If Awakener Druid and the Treefolk are dealt lethal damage at the same time, both will be destroyed. However, if Awakener Druid leaves the battlefield before the Treefolk is dealt damage, it will immediately revert to being just a land and thus can\'t be dealt damage.').

card_ruling('awakening', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('away', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('away', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('away', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('away', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('away', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('away', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('away', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('away', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('away', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('away', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('away', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('away', '2013-04-15', 'Away targets only a player. It doesn\'t target any creatures. The target player chooses which creature to sacrifice as the spell resolves.').

card_ruling('awe for the guilds', '2013-04-15', 'A monocolored creature is exactly one color. (Colorless creatures aren\'t monocolored.)').
card_ruling('awe for the guilds', '2013-04-15', 'No monocolored creature will be able to block that turn, including monocolored creatures that enter the battlefield after Awe for the Guilds resolves. A creature that\'s monocolored when Awe for the Guilds resolves but somehow becomes multicolored before blockers are declared will be able to block.').

card_ruling('awesome presence', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').

card_ruling('axebane guardian', '2012-10-01', 'Axebane Guardian\'s activated ability is a mana ability. It doesn\'t use the stack and can\'t be responded to. The number of creatures with defender you control is counted, and the color(s) of mana produced chosen, immediately after the ability is activated when the ability resolves.').

card_ruling('axelrod gunnarson', '2009-10-01', 'Each time a creature is put into a graveyard from play, check whether Axelrod Gunnarson had dealt any damage to it at any time during that turn. (This includes combat damage.) If so, Axelrod Gunnarson\'s second ability will trigger. It doesn\'t matter who controlled the creature or whose graveyard it was put into.').
card_ruling('axelrod gunnarson', '2009-10-01', 'If Axelrod Gunnarson and a creature it dealt damage to are both put into a graveyard at the same time, Axelrod Gunnarson\'s second ability will trigger.').
card_ruling('axelrod gunnarson', '2009-10-01', 'The player you target with Axelrod Gunnarson\'s ability doesn\'t have to be the player that controlled the creature that was put into a graveyard.').

card_ruling('ayesha tanaka', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('ayesha tanaka', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('ayesha tanaka', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('ayesha tanaka', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').
card_ruling('ayesha tanaka', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('aysen bureaucrats', '2004-10-04', 'This checks the power of the creature on activation and on resolution.').

card_ruling('aysen crusader', '2008-10-01', 'Aysen Crusader\'s ability counts the number of permanents you control that are Soldiers and/or Warriors. A permanent with both creature types is counted only once.').

card_ruling('ayumi, the last visitor', '2005-06-01', 'Nonland legendary permanents don\'t affect whether Ayumi can be blocked or not, only legendary lands.').

card_ruling('azor\'s elocutors', '2012-10-01', 'You\'ll only win the game if Azor\'s Elocutors has five or more filibuster counters on it when the first ability resolves. For example, if Azor\'s Elocutors has four filibuster counters on it and you somehow add another during your main phase, you won\'t win the game immediately.').
card_ruling('azor\'s elocutors', '2012-10-01', 'The second ability of Azor\'s Elocutors removes one filibuster counter per source, no matter how much damage that source dealt. For example, if two attacking creatures deal damage to you at the same time, two filibuster counters will be removed.').

card_ruling('azorius arrester', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('azorius arrester', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('azorius arrester', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('azorius arrester', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('azorius arrester', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('azorius arrester', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('azorius chancery', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('azorius charm', '2012-10-01', 'Multiple instances of lifelink on the same creature are redundant.').

card_ruling('azorius first-wing', '2006-05-01', '\"Protection from enchantments\" means that Azorius First-Wing can\'t be enchanted, it can\'t be targeted by Aura spells or by abilities of enchantments, all damage that would be dealt to it by enchantments is prevented, and it can\'t be blocked by creatures that are also enchantments.').
card_ruling('azorius first-wing', '2006-05-01', 'If an effect tries to move an Aura onto this creature, the effect will fail and the Aura will stay where it was.').
card_ruling('azorius first-wing', '2006-05-01', 'Azorius First-Wing doesn\'t have protection from creatures that are enchanted.').

card_ruling('azorius guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('azorius guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('azorius guildmage', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('azorius herald', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('azorius justiciar', '2012-10-01', 'The two creatures can be controlled by the same opponent or by different opponents.').
card_ruling('azorius justiciar', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('azorius justiciar', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('azorius justiciar', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('azorius justiciar', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('azorius justiciar', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('azorius justiciar', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('azorius keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('azorius keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('azorius ploy', '2006-05-01', 'The two targets may be the same creature or they may be different creatures.').

card_ruling('azorius æthermage', '2006-05-01', 'This ability will trigger whenever any permanent, including a token creature or Azorius AEthermage itself, is returned to your hand. It doesn\'t matter who controlled the permanent or if it had ever actually been in your hand before.').

