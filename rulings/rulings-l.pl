% Rulings

card_ruling('laboratory maniac', '2011-09-22', 'If for some reason you can\'t win the game (because your opponent controls Platinum Angel, for example), you won\'t lose for having tried to draw a card from a library with no cards in it, because the draw was still replaced.').

card_ruling('labyrinth champion', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('labyrinth champion', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('labyrinth champion', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('labyrinth minotaur', '2009-10-01', 'If the creature Labyrinth Minotaur blocked is untapped at the time its controller\'s next untap step begins, this ability has no effect. It won\'t apply at some later time when that creature is tapped.').
card_ruling('labyrinth minotaur', '2009-10-01', 'Labyrinth Minotaur doesn\'t track the creature\'s controller. If the affected creature changes controllers before its old controller\'s next untap step, this ability will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('laccolith grunt', '2004-10-04', 'The ability triggers even if a spell or ability makes it blocked, instead of being blocked by a creature.').
card_ruling('laccolith grunt', '2004-10-04', 'It can target any creature on the battlefield, not just one of the ones blocking it.').
card_ruling('laccolith grunt', '2004-10-04', 'The ability allows you to deal damage during the declare blockers step of combat, which is well before even first strike creatures deal damage.').
card_ruling('laccolith grunt', '2004-10-04', 'The ability is optional. You can decide to allow it to deal combat damage as normal.').

card_ruling('laccolith rig', '2004-10-04', 'If placed on a Laccolith creature, both this card\'s ability and the Laccolith\'s ability will trigger and have their effect.').
card_ruling('laccolith rig', '2004-10-04', 'If this is on an opponent\'s creature, the controller of this card (not the creature\'s controller) decides whether to use this card\'s ability or not, and decides what the target will be.').
card_ruling('laccolith rig', '2004-10-04', 'The ability triggers even if a spell or ability makes it blocked, instead of being blocked by a creature.').
card_ruling('laccolith rig', '2004-10-04', 'It can target any creature on the battlefield, not just one of the ones blocking it.').
card_ruling('laccolith rig', '2004-10-04', 'The ability allows you to deal damage during the declare blockers step of combat, which is well before even first strike creatures deal damage.').
card_ruling('laccolith rig', '2004-10-04', 'The ability is optional. You can decide to allow it to deal combat damage as normal.').
card_ruling('laccolith rig', '2006-10-15', 'Moving the enchantment after the ability triggers will not affect which creatures are affected.').

card_ruling('laccolith titan', '2004-10-04', 'The ability triggers even if a spell or ability makes it blocked, instead of being blocked by a creature.').
card_ruling('laccolith titan', '2004-10-04', 'It can target any creature on the battlefield, not just one of the ones blocking it.').
card_ruling('laccolith titan', '2004-10-04', 'The ability allows you to deal damage during the declare blockers step of combat, which is well before even first strike creatures deal damage.').
card_ruling('laccolith titan', '2004-10-04', 'The ability is optional. You can decide to allow it to deal combat damage as normal.').

card_ruling('laccolith warrior', '2004-10-04', 'The ability triggers even if a spell or ability makes it blocked, instead of being blocked by a creature.').
card_ruling('laccolith warrior', '2004-10-04', 'It can target any creature on the battlefield, not just one of the ones blocking it.').
card_ruling('laccolith warrior', '2004-10-04', 'The ability allows you to deal damage during the declare blockers step of combat, which is well before even first strike creatures deal damage.').
card_ruling('laccolith warrior', '2004-10-04', 'The ability is optional. You can decide to allow it to deal combat damage as normal.').

card_ruling('laccolith whelp', '2004-10-04', 'The ability triggers even if a spell or ability makes it blocked, instead of being blocked by a creature.').
card_ruling('laccolith whelp', '2004-10-04', 'It can target any creature on the battlefield, not just one of the ones blocking it.').
card_ruling('laccolith whelp', '2004-10-04', 'The ability allows you to deal damage during the declare blockers step of combat, which is well before even first strike creatures deal damage.').
card_ruling('laccolith whelp', '2004-10-04', 'The ability is optional. You can decide to allow it to deal combat damage as normal.').

card_ruling('lace with moonglove', '2007-10-01', 'Deathtouch applies when any damage is dealt, not just when combat damage is dealt.').

card_ruling('lady sun', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('lady zhurong, warrior queen', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('lagonna-band elder', '2013-09-15', 'If you don’t control an enchantment when Lagonna-Band Elder enters the battlefield, its ability won’t trigger. If it does trigger, but you don’t control an enchantment when the ability tries to resolve, you won’t gain life.').

card_ruling('lagonna-band trailblazer', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('lagonna-band trailblazer', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('lagonna-band trailblazer', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('lair of the ashen idol', '2012-06-01', 'If you control a creature when the first ability resolves, you must sacrifice it.').
card_ruling('lair of the ashen idol', '2012-06-01', 'You can choose to target zero players with the chaos ability.').

card_ruling('lairwatch giant', '2007-10-01', 'Lairwatch Giant\'s second ability triggers if it blocks at least two creatures when blockers are declared. It will also trigger if it blocks fewer than two creatures when blockers are declared and effects then cause it to block more creatures. However, if Lairwatch Giant is already blocking two or more creatures, and effects then cause it to block more creatures, the ability won\'t trigger again.').

card_ruling('lake of the dead', '2004-10-04', 'The tap and sacrifice counts as tapping the land for mana.').
card_ruling('lake of the dead', '2004-10-04', 'You have to sacrifice a swamp before this card is put onto the battlefield. And you have to do this no matter how it is put onto the battlefield.').

card_ruling('lancers en-kor', '2004-10-04', 'The ability of this card does not do anything to stop Trample damage from being assigned to the defending player.').
card_ruling('lancers en-kor', '2004-10-04', 'It can redirect damage to itself.').
card_ruling('lancers en-kor', '2004-10-04', 'It is possible to redirect more damage to a creature than that creature\'s toughness.').
card_ruling('lancers en-kor', '2004-10-04', 'You can use this ability as much as you want prior to damage being dealt.').
card_ruling('lancers en-kor', '2013-07-01', 'When you redirect combat damage it is still combat damage.').

card_ruling('land equilibrium', '2009-10-01', 'This effect applies no matter how the land would enter the battlefield: because an opponent plays it, or because a spell or ability allows that opponent to put it onto the battlefield. Note that it doesn\'t matter whose control the land enters the battlefield under. If the opponent would put the land onto the battlefield under someone else\'s control (as a result of Yavimaya Dryad\'s ability, for example), that opponent will still have to sacrifice a land.').
card_ruling('land equilibrium', '2009-10-01', 'If an opponent puts a land onto the battlefield under his or her own control, he or she may sacrifice that same land. The player won\'t be able to tap that land for mana before sacrificing it.').
card_ruling('land equilibrium', '2009-10-01', 'If a land that would enter the battlefield has its own \"If [this land] would enter the battlefield\" replacement effect, the player who will control that land chooses which replacement effect to apply. After applying one, if the other one would still be applicable, it\'s then applied.').

card_ruling('land grant', '2004-10-04', 'You can pay the \"reveal your hand\" cost even if your hand is already revealed due to another effect.').
card_ruling('land grant', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('land tax', '2006-10-15', 'You can fetch any of the \"Snow-Covered\" lands, since they all also have the supertype basic.').
card_ruling('land tax', '2009-10-01', 'This ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless any one of your opponents controls more lands than you, and (2) the ability will do nothing if you control at least as many lands as each of your opponents by the time it resolves.').
card_ruling('land tax', '2009-10-01', 'You shuffle your library if you search, even if you don\'t put any basic land cards into your hand.').

card_ruling('land\'s edge', '2004-10-04', 'The damage done when you discard a land only applies to lands which are discarded by choice using the Land\'s Edge ability. It does not work on forced discards of any type.').
card_ruling('land\'s edge', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('landslide', '2004-10-04', 'You can sacrifice zero mountains to deal zero damage.').
card_ruling('landslide', '2004-10-04', 'You sacrifice the mountains during resolution.').

card_ruling('lantern of insight', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('lantern of insight', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('lantern of insight', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('lantern scout', '2015-08-25', 'Multiple instances of lifelink are redundant. There usually isn’t much benefit in having the rally ability resolve more than once in a turn, other than that new creatures will be affected by the ability resolving an additional time.').
card_ruling('lantern scout', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('lantern scout', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('lantern spirit', '2011-09-22', 'Only Lantern Spirit\'s controller may activate its ability.').

card_ruling('laquatus\'s disdain', '2004-10-04', 'Can be used to counter a spell cast using the Flashback ability, or cast from the graveyard using any other effect.').

card_ruling('larceny', '2004-10-04', 'Triggers once per creature, not once per point of damage.').

card_ruling('last breath', '2013-09-15', 'If the creature is an illegal target when Last Breath tries to resolve, it will be countered and none of its effects will happen. Its controller won’t gain 4 life.').

card_ruling('last kiss', '2010-06-15', 'You gain 2 life even if the damage is prevented.').
card_ruling('last kiss', '2010-06-15', 'If the targeted creature is an illegal target by the time Last Kiss resolves, the spell is countered. You won\'t gain life.').

card_ruling('last thoughts', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('last thoughts', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('last thoughts', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('last thoughts', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('last thoughts', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('last thoughts', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('last thoughts', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('last thoughts', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('last thoughts', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('last thoughts', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('last thoughts', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('last-ditch effort', '2004-10-04', 'The creatures are chosen and sacrificed on resolution.').
card_ruling('last-ditch effort', '2004-10-04', 'It deals one damage for each creature sacrificed.').

card_ruling('lat-nam\'s legacy', '2008-10-01', 'Shuffling a card from your hand into your library is mandatory. The effect says \"if you do\" because performing that action might have been impossible. If you fail to shuffle a card into your library because your hand was empty, you won\'t draw any cards.').

card_ruling('launch party', '2012-10-01', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that creature is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').
card_ruling('launch party', '2012-10-01', 'If the target creature is an illegal target when Launch Party tries to resolve, it will be countered and none of its effects will happen. No player will lose 2 life.').
card_ruling('launch party', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('launch party', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('launch the fleet', '2014-04-26', 'You declare which player or planeswalker each token is attacking as you put it onto the battlefield. It doesn’t have to be the same player or planeswalker the original creature is attacking.').
card_ruling('launch the fleet', '2014-04-26', 'Although the tokens are attacking, they were never declared as attacking creatures (for purposes of abilities that trigger whenever a creature attacks, for example).').
card_ruling('launch the fleet', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('launch the fleet', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('launch the fleet', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('launch the fleet', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('launch the fleet', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('lava burst', '2008-10-01', 'Prevention and redirection effects will have no effect on damage dealt by Lava Burst to a creature. However, they will work on damage that Lava Burst would deal to a player.').
card_ruling('lava burst', '2008-10-01', 'A creature dealt damage by Lava Burst may be regenerated.').

card_ruling('lava storm', '2008-04-01', 'Lava Storm may be cast at any time, but it will have no effect unless there are attacking or blocking creatures on the battlefield (which exist only during certain steps of the combat phase).').

card_ruling('lava zombie', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('lavaball trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('lavaball trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('lavaball trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('lavaball trap', '2009-10-01', 'You may target any two lands, not just ones that entered the battlefield this turn.').
card_ruling('lavaball trap', '2009-10-01', 'All creatures are dealt damage, not just ones controlled by the targeted lands\' controller(s).').
card_ruling('lavaball trap', '2009-10-01', 'If both targeted lands are illegal targets by the time Lavaball Trap resolves, the entire spell is countered. No creatures are dealt damage.').

card_ruling('lavaborn muse', '2004-10-04', 'The number of cards in hand is checked both at the beginning of upkeep and when the triggered ability resolves.').

card_ruling('lavaclaw reaches', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('lavaclaw reaches', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').

card_ruling('lavacore elemental', '2007-02-01', 'If multiple creatures you control deal combat damage to a player, Lavacore Elemental\'s ability triggers that many times.').
card_ruling('lavacore elemental', '2007-02-01', 'If Lavacore Elemental deals combat damage to a player, it triggers its own ability.').

card_ruling('lavafume invoker', '2010-06-15', 'Only creatures you control as the activated ability resolves are affected.').

card_ruling('lavalanche', '2009-05-01', 'If the targeted player has become an illegal target by the time Lavalanche resolves, Lavalanche is countered. Nothing will be dealt damage.').
card_ruling('lavalanche', '2009-05-01', 'Because Lavalanche doesn\'t target any creatures, it will deal damage to creatures with shroud. Any damage it would deal to creatures with appropriate protection abilities will still be prevented, however.').

card_ruling('lavamancer\'s skill', '2004-10-04', 'If this card enchants a Wizard, the Wizard has both abilities and its controller can use either one.').

card_ruling('lavinia of the tenth', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('lavinia of the tenth', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('lavinia of the tenth', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('lavinia of the tenth', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('lavinia of the tenth', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('lavinia of the tenth', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').
card_ruling('lavinia of the tenth', '2013-04-15', 'The converted mana cost of a creature token is 0 unless that token is a copy of another creature, in which case it copies that creature\'s mana cost.').
card_ruling('lavinia of the tenth', '2013-04-15', 'Lavinia\'s ability doesn\'t affect permanents an opponent gains control of after its enters-the-battlefield ability has resolved. Similarly, if you gain control of a detained permanent after that ability resolves, it will continue to be detained until your next turn.').

card_ruling('lay bare', '2010-06-15', 'If the targeted spell is an illegal target by the time Lay Bare resolves, Lay Bare is countered. You won\'t look at its controller\'s hand.').

card_ruling('lay of the land', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('lay waste', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('lazav, dimir mastermind', '2013-01-24', 'Lazav, Dimir Mastermind becomes a copy of the creature card in the graveyard. If that card is no longer in the graveyard when Lazav\'s ability resolves, use the characteristics of that card as it last existed in the graveyard. Notably, copy effects that applied to the creature card when it was on the battlefield won\'t be copied by Lazav. For example, if a Clone is put into an opponent\'s graveyard from the battlefield and you use Lazav\'s ability, it will become 0/0. It won\'t become a copy of whatever Clone was copying.').
card_ruling('lazav, dimir mastermind', '2013-01-24', 'If multiple creature cards are put into an opponent\'s graveyard at the same time, you choose the order that Lazav\'s triggered abilities go on the stack.').
card_ruling('lazav, dimir mastermind', '2013-01-24', 'Enters-the-battlefield abilities of the creature card Lazav is copying won\'t trigger as Lazav is already on the battlefield when it becomes a copy of that creature card.').
card_ruling('lazav, dimir mastermind', '2013-01-24', 'Token creatures dying won\'t cause Lazav\'s triggered ability to trigger.').

card_ruling('leaf-crowned elder', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('leaf-crowned elder', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('leaf-crowned elder', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('leaf-crowned elder', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('leaf-crowned elder', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('leaf-crowned elder', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').
card_ruling('leaf-crowned elder', '2008-04-01', 'You play the revealed card as part of the resolution of this ability. It\'s played from your library, not your hand. You choose modes, pay additional costs, choose targets, etc. for the card as normal when playing it. Any X in the mana cost will be 0. Alternative costs can\'t be paid.').

card_ruling('leafcrown dryad', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('leafcrown dryad', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('leafcrown dryad', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('leafcrown dryad', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('leafcrown dryad', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('leafcrown dryad', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('leafcrown dryad', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('leaping master', '2014-09-20', 'Activating Leaping Master’s ability after it’s been blocked won’t change or undo the block.').

card_ruling('learn from the past', '2015-02-25', 'If the player’s graveyard is empty, the player will shuffle his or her library, then you’ll draw a card.').

card_ruling('leech bonder', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('leech bonder', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('leech bonder', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('leech bonder', '2008-05-01', 'Leech Bonder\'s activated ability can move any kind of counter, not just a -1/-1 counter. It can target any two creatures, whether they have counters on them or not.').
card_ruling('leech bonder', '2008-05-01', 'This effect may result in a useless counter being placed on a creature. For example, if an age counter is moved from a creature with cumulative upkeep to a creature without cumulative upkeep, it will have no effect on the new creature.').
card_ruling('leech bonder', '2008-05-01', 'If either one of the target creatures becomes an illegal target (because it left the battlefield or for any other reason), the counter doesn\'t move. If both targets become illegal, the ability is countered.').

card_ruling('leeching bite', '2011-06-01', 'You need to be able to choose two different target creatures in order to cast Leeching Bite.').
card_ruling('leeching bite', '2011-06-01', 'If only one of the targeted creatures is still a legal target when Leeching Bite tries to resolve, that creature will still be affected, but the illegal target won\'t be.').

card_ruling('leeching licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('leeching licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('leeching sliver', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('leeching sliver', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('leeching sliver', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('leering emblem', '2008-08-01', 'The bonus is given to the creature Leering Emblem is attached to as the ability resolves.').

card_ruling('legacy\'s allure', '2004-10-04', 'The \"power less than or equal to the number of treasure counters on this card\" restriction is a targeting restriction and will be checked on resolution in addition to on announcement. If the creature\'s power changes to be greater than this before the effect resolves, then the Legacy\'s Allure triggered ability is countered, but the card stays in the graveyard.').

card_ruling('legerdemain', '2008-04-01', 'You can\'t cast Legerdemain unless both targets share a type at that time. It will also check to make sure they share a type as it resolves. If they no longer do so, the exchange will not occur.').

card_ruling('legion loyalist', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('legion loyalist', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('legion loyalist', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('legion\'s initiative', '2013-04-15', 'The beginning of the next combat will happen even if the player whose turn it is doesn\'t intend to attack with any creatures.').
card_ruling('legion\'s initiative', '2013-04-15', 'A creature that\'s both red and white will get +1/+1 from Legion\'s Initiative.').

card_ruling('lens of clarity', '2014-09-20', 'Lens of Clarity lets you look at the top card of your library and at face-down creatures you don’t control whenever you want, even if you don’t have priority. This action doesn’t use the stack. Knowing what those cards are becomes part of the information you have access to, just like you can look at the cards in your hand.').
card_ruling('lens of clarity', '2014-09-20', 'Lens of Clarity doesn’t stop your opponents from looking at face-down creatures they control.').
card_ruling('lens of clarity', '2014-09-20', 'Lens of Clarity doesn’t let you look at face-down spells you don’t control on the stack.').

card_ruling('leonin arbiter', '2011-01-01', 'If an effect says \"You may search your library . . . If you do, shuffle your library,\" and you haven\'t paid {2}, you can\'t choose to search, so you won\'t shuffle.').
card_ruling('leonin arbiter', '2011-01-01', 'If an effect says \"Search your library . . . Then shuffle your library,\" and you haven\'t paid {2}, the search effect fails, but you will still have to shuffle.').
card_ruling('leonin arbiter', '2011-01-01', 'Since players who haven\'t paid {2} can\'t search, they won\'t be able to find any cards in a library. The effect applies to all players and all libraries. If a spell or ability\'s effect has other parts that don\'t depend on searching for or finding cards, they will still work normally.').
card_ruling('leonin arbiter', '2011-01-01', 'Effects that tell a player to reveal cards from a library or look at cards from the top of a library will still work. Only effects that use the word \"search\" will fail unless a player pays {2}.').
card_ruling('leonin arbiter', '2011-01-01', 'Paying {2} to ignore Leonin Arbiter\'s effect is a special action. Any player may take this special action any time he or she has priority. It doesn\'t use the stack and can\'t be responded to.').
card_ruling('leonin arbiter', '2011-01-01', 'Paying {2} doesn\'t let a player pick up a library and search it -- it just allows him or her to ignore Leonin Arbiter\'s effect that turn. That player can search a library only if another spell or ability instructs him or her to do so.').
card_ruling('leonin arbiter', '2011-01-01', 'If a player pays {2}, that enables only him or her to ignore Leonin Arbiter\'s effect that turn. Each other player is still affected by it.').
card_ruling('leonin arbiter', '2011-01-01', 'A player who has paid {2} may search any library when instructed to do so that turn, not just his or her own library.').
card_ruling('leonin arbiter', '2011-01-01', 'Once a player has paid {2}, he or she may search libraries that turn as many times as he or she is instructed to do so.').
card_ruling('leonin arbiter', '2011-01-01', 'If there are multiple Leonin Arbiters on the battlefield, a player must pay {2} for each one before being able to search libraries that turn.').

card_ruling('leonin armorguard', '2009-05-01', 'Leonin Armorguard gets this bonus too.').

card_ruling('leonin bola', '2004-12-01', 'Leonin Bola grants an ability to the equipped creature. The ability can target a creature with protection from artifacts (unless the equipped creature is an artifact creature).').
card_ruling('leonin bola', '2004-12-01', 'The equipped creature\'s controller chooses whether or not to activate the activated ability. It doesn\'t matter who controls Leonin Bola.').
card_ruling('leonin bola', '2004-12-01', '\"Unattach Leonin Bola\" means just that -- Leonin Bola moves off the creature it was equipping and remains on the battlefield.').

card_ruling('leonin iconoclast', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('leonin iconoclast', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('leonin iconoclast', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('leonin relic-warder', '2011-06-01', 'If Leonin Relic-Warder leaves the battlefield before its first ability has resolved, its second ability will trigger and when it resolves, do nothing. Then its first ability will resolve and exile the targeted artifact or enchantment with no way to return it to the battlefield.').

card_ruling('leonin shikari', '2004-12-01', 'An equip ability uses the keyword ability \"equip.\" Normally, equip abilities can be activated only any time you could cast a sorcery.').
card_ruling('leonin shikari', '2004-12-01', 'Leonin Shikari allows you to move your Equipment around during the combat phase or in response to a spell or ability. You\'re still subject to any other restrictions on activating equip abilities.').

card_ruling('lesser werewolf', '2009-10-01', 'You may activate Lesser Werewolf\'s ability no matter what its power is. However, if Lesser Werewolf\'s power is 0 or less at the time the ability resolves, the ability won\'t do anything.').

card_ruling('lethargy trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('lethargy trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('lethargy trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').

card_ruling('lethe lake', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('lethe lake', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('lethe lake', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('lethe lake', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('lethe lake', '2009-10-01', 'If you don\'t have ten cards in your library, you\'ll put all the cards in your library into your graveyard. The same is true if the targeted player doesn\'t have ten cards in his or her library.').

card_ruling('leviathan', '2004-10-04', 'You don\'t have to pay the untap cost if it is untapped outside the upkeep phase or is untapped by an effect.').

card_ruling('ley druid', '2004-10-04', 'The land is untapped as a normal ability that goes on the stack. It is not a mana ability, so it can\'t be used at times that only mana abilities can be used.').

card_ruling('leyline of anticipation', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of anticipation', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of anticipation', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of anticipation', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').

card_ruling('leyline of lifeforce', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of lifeforce', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of lifeforce', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of lifeforce', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').

card_ruling('leyline of lightning', '2006-02-01', 'You may pay the {1} only once per spell.').
card_ruling('leyline of lightning', '2006-02-01', 'If you cast a creature with bloodthirst while Leyline of Lightning is on the battlefield, you may use the Leyline\'s ability to deal damage to an opponent. If you do, that damage is dealt before the creature spell resolves, and the creature will enter the battlefield with counters from its bloodthirst ability.').
card_ruling('leyline of lightning', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of lightning', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of lightning', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of lightning', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').

card_ruling('leyline of punishment', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of punishment', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of punishment', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of punishment', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').
card_ruling('leyline of punishment', '2010-08-15', 'Spells and abilities that would normally cause a player to gain life still resolve, but the life-gain part simply has no effect.').
card_ruling('leyline of punishment', '2010-08-15', 'If a cost includes life gain (like Invigorate\'s alternative cost does), that cost can\'t be paid.').
card_ruling('leyline of punishment', '2010-08-15', 'Effects that would replace gaining life with some other effect won\'t be able to do anything because it\'s impossible for players to gain life.').
card_ruling('leyline of punishment', '2010-08-15', 'Effects that replace an event with gaining life (like Words of Worship\'s effect does) will end up replacing the event with nothing.').
card_ruling('leyline of punishment', '2010-08-15', 'If an effect says to set a player\'s life total to a certain number, and that number is higher than the player\'s current life total, that part of the effect won\'t do anything. (If the number is lower than the player\'s current life total, the effect will work as normal.)').
card_ruling('leyline of punishment', '2010-08-15', 'Damage prevention shields don\'t have any effect. If a prevention effect has an additional effect, the additional effect will still work (if possible). Spells that create prevention effects can still be cast, and abilities that create prevention effects can still be activated.').
card_ruling('leyline of punishment', '2010-08-15', 'Static abilities that prevent damage (including protection abilities) don\'t do so. If they have additional effects that don\'t depend on the amount of damage prevented, those additional effects will still work. Such effects are applied only once per source of damage.').
card_ruling('leyline of punishment', '2010-08-15', 'Effects that replace or redirect damage without using the word \"prevent\" are not affected by Leyline of Punishment.').
card_ruling('leyline of punishment', '2010-08-15', 'If a creature is dealt lethal damage, it can still regenerate. If it does, the damage marked on it will be removed from it.').

card_ruling('leyline of sanctity', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of sanctity', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of sanctity', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of sanctity', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').

card_ruling('leyline of singularity', '2006-02-01', 'Adding a supertype doesn\'t overwrite other supertypes. Each nonland permanent will be legendary in addition to its other supertypes.').
card_ruling('leyline of singularity', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of singularity', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of singularity', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of singularity', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').
card_ruling('leyline of singularity', '2013-07-01', 'The \"legend rule\" says that if a player controls two or more legendary permanents with the same name, that player chooses one of them and the rest are put into their owners\' graveyards. Notably, if you control Leyline of Singularity and two or more creature tokens with the same name, only one of the tokens will be allowed to remain on the battlefield.').
card_ruling('leyline of singularity', '2013-07-01', 'If the game starts with more than one Leyline of Singularity under a single player\'s control, all but one of them will be put into their owners\' graveyards right after the upkeep of the first turn starts. If the game started with multiple copies of any other Leylines under a single player\'s control, that player will also put all but one of them into his or her graveyard at that time.').

card_ruling('leyline of the meek', '2006-02-01', 'Leyline of the Meek gives a bonus to creatures only if they\'re tokens. Cards such as Clone that are copying tokens aren\'t tokens.').
card_ruling('leyline of the meek', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of the meek', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of the meek', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of the meek', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').

card_ruling('leyline of the void', '2006-02-01', 'Leyline of the Void affects cards that would be put into opponents\' graveyards from anywhere, not just from the battlefield.').
card_ruling('leyline of the void', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of the void', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of the void', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of the void', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').
card_ruling('leyline of the void', '2010-08-15', 'Leyline of the Void\'s second ability prevents cards from ever reaching your opponents\' graveyards. Abilities that would trigger when those cards are put into an opponent\'s graveyard (such as Hoarding Dragon\'s last ability) won\'t trigger.').
card_ruling('leyline of the void', '2010-08-15', 'Leyline of the Void\'s second ability doesn\'t affect token permanents that would be put into an opponent\'s graveyard from the battlefield. They\'ll be put into that graveyard as normal (causing any applicable triggered abilities to trigger), then they\'ll cease to exist.').
card_ruling('leyline of the void', '2010-08-15', 'If your opponent discards a card while you control Leyline of the Void, abilities that function when that card is discarded (such as Liliana\'s Caress\'s ability, or a madness ability of the discarded card) still work, even though that card never reaches that player\'s graveyard. In addition, spells or abilities that check the characteristics of the discarded card (such as Chandra Ablaze\'s first ability) can find that card in exile.').

card_ruling('leyline of vitality', '2010-08-15', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('leyline of vitality', '2010-08-15', 'After all players have decided not to take any more mulligans, if the starting player has any Leylines in his or her hand, he or she may put any or all of them onto the battlefield. Then each other player in turn order may do the same.').
card_ruling('leyline of vitality', '2010-08-15', 'Leylines that start the game on the battlefield aren\'t cast as spells. They can\'t be countered.').
card_ruling('leyline of vitality', '2010-08-15', 'After each player has exercised his or her option to put Leylines onto the battlefield, the first turn of the game begins.').
card_ruling('leyline of vitality', '2010-08-15', 'If Leyline of Vitality and a creature enter the battlefield under your control at the same time, Leyline of Vitality\'s third ability will trigger.').

card_ruling('lhurgoyf', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('liar\'s pendulum', '2004-12-01', 'First you name a card. Then the targeted opponent guesses whether the card is in your hand.').
card_ruling('liar\'s pendulum', '2004-12-01', 'After the opponent has guessed, you may reveal your hand. If your opponent was wrong, you draw a card. If your opponent was right, you don\'t draw a card.').
card_ruling('liar\'s pendulum', '2004-12-01', 'You reveal your hand only if you choose to, regardless of whether your opponent guessed right or wrong.').
card_ruling('liar\'s pendulum', '2004-12-01', 'You can\'t draw a card if you don\'t reveal your hand.').

card_ruling('liberate', '2004-10-04', 'The creature returns after \"at the beginning of the end step\" abilities trigger, so any such abilities on the creature will not trigger this turn.').

card_ruling('library of alexandria', '2004-10-04', 'You may tap multiples of these in response to each other because the requirement for 7 cards is checked only at the time the ability is announced and not again when it resolves.').

card_ruling('library of leng', '2004-10-04', 'This effect has no effect on the cards being put into the graveyard from a library, because they are not \"discarded\".').
card_ruling('library of leng', '2004-10-04', 'You can\'t use the Library of Leng ability to place a discarded card on top of your library when you discard a card as a cost, because costs aren\'t effects.').
card_ruling('library of leng', '2004-10-04', 'The discard triggers anything else that triggers on discards.').
card_ruling('library of leng', '2004-10-04', 'You can look at a randomly discarded card before deciding where it goes.').
card_ruling('library of leng', '2004-10-04', 'The ability applies any time a spell or ability has you discard as part of its effect. It does not matter if you or your opponent control the spell or ability. The discard is forced because it is an effect.').
card_ruling('library of leng', '2004-10-04', 'If more than one card is discarded due to a single effect, the Library allows you to decide whether or not to use it on each of the cards. You get to decide the order the cards are placed on the library if more than one goes there.').
card_ruling('library of leng', '2004-10-04', 'Since the card goes directly to the library, the card is not revealed unless the spell or ability requiring the discard specifically says it is.').
card_ruling('library of leng', '2004-10-04', 'The ability replaces the normal discard action with a discard action that puts the card on the library instead of the graveyard.').
card_ruling('library of leng', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Library of Leng onto the battlefield, you\'ll have no maximum hand size. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('lich', '2004-10-04', 'If you have multiple Lich cards on the battlefield, you must sacrifice a permanent for each damage done to you for each Lich. This is because the sacrifice is a triggered ability. But you only draw one card for each life gained regardless of how many Liches you have. This is because the draw is a replacement effect and not a triggered one. You lose if any one of the Liches leaves the battlefield.').
card_ruling('lich', '2004-10-04', 'If you take more than one damage at a time, sacrifice the permanents for that damage simultaneously. This allows you to sacrifice both a creature and any Aura that is on it all at once.').
card_ruling('lich', '2004-10-04', 'You can\'t pay life, just like any player at less than one life can\'t pay life. You can pay zero life if you want.').
card_ruling('lich', '2004-10-04', 'If an opponent steals control of Lich and no other effect prevents you from losing with a life total of zero, you will lose the game due to a zero life total as a State-Based Action before you can take any actions. The last sentence doesn\'t apply in this case since the Lich didn\'t leave the battlefield.').
card_ruling('lich', '2004-10-04', 'If an opponent steals control of Lich, their life total does not change. The life total changes for a player only when it enters the battlefield under that player\'s control.').
card_ruling('lich', '2004-10-04', 'You can lose life and take damage, and thereby have a negative life total, while Lich is on the battlefield.').
card_ruling('lich', '2010-08-15', 'The last ability will cause you to lose the game even if you somehow manage to have a life-total greater than 0 at the time the Lich leaves the battlefield or if some other effect would prevent you from losing for having 0 life. If, on the other hand, some effect such as that from Platinum Angel says that you can\'t lose the game then even the last ability of the Lich cannot cause you to do so. The ability will just resolve and the game will continue as normal.').
card_ruling('lich', '2010-08-15', 'Note that usually you will have 0 or less life at the time Lich leaves the battlefield causing you to lose as a State-Based Action before the last ability can even go on the stack.').
card_ruling('lich', '2011-01-01', 'The last ability will cause you to lose the game even if you somehow manage to have a life-total greater than 0 at the time the Lich is put into the graveyard or if some other effect would prevent you from losing for having 0 life. If, on the other hand, some effect such as that from Platinum Angel says that you can\'t lose the game then even the last ability of the Lich cannot cause you to do so. The ability will just resolve and the game will continue as normal.').

card_ruling('lich lord of unx', '2009-05-01', 'The second ability counts all Zombies you control, not just the tokens put onto the battlefield with the first ability. It will count Lich Lord of Unx itself if it\'s still under your control and still a Zombie when the ability resolves.').

card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror replaces the game-loss event if you would lose the game in the following ways: -- As a state-based action for having 0 or less life. -- As a state-based action for having tried to draw a card from an empty library since the last time state-based actions were checked. -- As a state-based action for having ten or more poison counters (though this isn\'t that helpful; see below). -- Because an ability (such as the one from Immortal Coil) states that you do so.').
card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror has no effect if a spell or ability (such as the one from Helix Pinnacle) states that a player \"wins the game.\" If a player wins the game, the game ends immediately.').
card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror has no effect if you concede the game. If you concede, you\'ll lose.').
card_ruling('lich\'s mirror', '2008-10-01', 'If you can\'t lose the game (for example, you control a Platinum Angel), Lich\'s Mirror won\'t do anything.').
card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror shuffles permanents you own into your library, regardless of who controls them.').
card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror shuffles tokens you own into your library, too. The tokens you own will leave play. However, there\'s no point to physically shuffling tokens into your library because you can\'t draw them as part of Lich\'s Mirror\'s effect and they\'ll cease to exist immediately afterwards.').
card_ruling('lich\'s mirror', '2008-10-01', 'Any abilities that trigger when the permanents leave the battlefield will be put on the stack after Lich\'s Mirror\'s entire effect has been applied.').
card_ruling('lich\'s mirror', '2008-10-01', 'Lich\'s Mirror doesn\'t affect spells on the stack, cards that have been exiled, or permanents you control but don\'t own. They\'ll stay where they are. Spells on the stack will then resolve as normal.').
card_ruling('lich\'s mirror', '2008-10-01', 'Although Lich\'s Mirror has you draw a hand of seven cards and sets your life total to 20, this isn\'t a game restart. You can\'t take a mulligan if you don\'t like your new hand of cards.').
card_ruling('lich\'s mirror', '2008-10-01', 'For your life total to become 20, you actually gain or lose the necessary amount of life. Keep in mind that you may have a negative life total when this happens. For example, if your life total is -4 when you would lose the game, Lich\'s Mirror\'s effect will cause you to gain 24 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('lich\'s mirror', '2008-10-01', 'As part of Lich\'s Mirror\'s effect, it typically shuffles itself into your library. If it does, that means that if you\'d lose the game *again* immediately after its effect is finished, it can\'t help you a second time. This can occur in a few different ways. For example: -- You have ten or more poison counters. Lich\'s Mirror doesn\'t remove poison counters. If you\'d lose the game this way, you\'ll do what Lich\'s Mirror says, then you\'ll lose the game the next time state-based actions are checked. -- Your life total is 0 or less and an effect says that you can\'t gain life. Since your life total can\'t be raised, it stays at whatever it is rather than becoming 20, and you\'ll lose the game the next time state-based actions are checked. -- The number of nontoken permanents you own plus the number of cards in your hand, graveyard, and library is less than seven. When you try to draw seven cards as part of Lich\'s Mirror\'s effect, you\'ll be unable to complete at least one of those draws and you\'ll lose the game the next time state-based actions are checked. -- You control *but don\'t own* a permanent such as Immortal Coil with a triggered ability that causes you to lose the game when a certain game state happens (also known as a \"state trigger\"), and the condition that causes the \"lose the game\" ability to trigger hasn\'t changed. If you owned the permanent, Lich\'s Mirror would shuffle it into your library. In this case, however, it remains on the battlefield and its ability will trigger again.').
card_ruling('lich\'s mirror', '2008-10-01', 'If you control *but don\'t own* Lich\'s Mirror, Lich\'s Mirror itself will still be on the battlefield after its effect is finished. If you would lose the game again for any of the reasons above, Lich\'s Mirror has its effect again . . . and again . . . and again. An involuntary infinite loop will be created, and the game will end in a draw. (In the case of the triggered ability example given last in the list above, it\'s possible that a player could cause the loop to end while the ability is on the stack. None of the loops caused by state-based actions can be stopped at all.)').
card_ruling('lich\'s mirror', '2008-10-01', 'If all the players remaining in a game would lose simultaneously but one of them controls Lich\'s Mirror, that player does what Lich\'s Mirror says instead of losing, and everyone else loses. As a result, the controller of Lich\'s Mirror wins the game because all of his or her opponents have lost. (If Lich\'s Mirror weren\'t in the picture, then the game would be a draw.)').
card_ruling('lich\'s mirror', '2008-10-01', 'If a spell causes you to lose the game the next time state-based actions are checked (by dealing damage to you greater than your life total, for example), that spell will already be in the graveyard by the time Lich\'s Mirror\'s effect happens. If it\'s in your graveyard, it will be shuffled into your library.').
card_ruling('lich\'s mirror', '2008-10-01', 'If, during a check of state-based actions, you\'d lose the game at the same time a creature you own would be put into your graveyard (due to an Earthquake for 10 or combat damage dealt to both you and the creature, for example), that creature\'s controller has a choice to make. The state-based actions rule is trying to simultaneously (a) shuffle that creature card into your library (due to Lich\'s Mirror\'s replacement effect) and (b) put it into your graveyard. Only one of those things can happen. The creature\'s controller chooses which one. If the creature is put into your graveyard, it isn\'t shuffled into your library. Abilities that trigger when that creature is put into a graveyard will trigger only if that option is chosen.').
card_ruling('lich\'s mirror', '2008-10-01', 'If, during a check of state-based actions, you\'d lose the game for multiple reasons (for example, if you were at 1 life and had one card in your library, then Night\'s Whisper caused you to draw two cards and lose 2 life), a single Lich\'s Mirror will replace all of them. You\'ll do what Lich\'s Mirror says just once.').
card_ruling('lich\'s mirror', '2009-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').
card_ruling('lich\'s mirror', '2010-06-15', 'In a Two-Headed Giant game, if your team would lose the game and you control Lich\'s Mirror, your team won\'t lose. Instead, you\'ll do what Lich\'s Mirror says and your teammate won\'t do anything. This is true even if the reason your team would lose is because your teammate tried to draw a card with an empty library or was affected by an ability that said he or she lost the game. Your life total (which is the same as your team\'s life total) becomes 20. Your team\'s life total is adjusted by the amount of life you gain or lose as a result of this, which basically means your team\'s life total becomes 20.').

card_ruling('lichenthrope', '2004-10-04', 'This card\'s effect replaces damage with placement of -1/-1 counters, so side-effects of damage to it will not trigger.').

card_ruling('liege of the axe', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('liege of the hollows', '2008-04-01', 'The player whose turn it is pays any amount of mana he or she wants, then each other player in turn order does the same, then all the Squirrel tokens are put onto the battlefield at the same time.').

card_ruling('liege of the pit', '2006-09-25', 'You can\'t sacrifice Liege of the Pit to its own ability. You can sacrifice one Liege of the Pit to another Liege of the Pit.').
card_ruling('liege of the pit', '2006-09-25', 'If you have another creature on the battlefield when the ability resolves, you must sacrifice it. You can\'t choose to be dealt 7 damage instead.').

card_ruling('liege of the tangle', '2011-01-01', 'You may choose to target zero lands.').
card_ruling('liege of the tangle', '2011-01-01', 'Whether a targeted land remains an 8/8 green Elemental creature depends only on whether it has an awakening counter on it, not on whether Liege of the Tangle is still on the battlefield.').
card_ruling('liege of the tangle', '2011-01-01', 'If all awakening counters on a land are moved to a land without an awakening counter on it (perhaps by using Fate Transfer), the animation effect doesn\'t follow them. The first land is no longer an 8/8 green Elemental creature because it no longer has an awakening counter on it. The second land isn\'t an 8/8 green Elemental creature because Liege of the Tangle didn\'t target it.').

card_ruling('life', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('life and limb', '2007-02-01', 'Each Forest and Saproling will have the card types creature and land, and the subtypes Forest and Saproling. Each subtype maintains its correlation to the proper type: Forest is not a creature type and Saproling is not a land type.').
card_ruling('life and limb', '2007-02-01', 'When Life and Limb leaves the battlefield, the effect ends and the permanents go back to what they were (unless another effect grants these card types and/or subtypes to the affected permanents).').
card_ruling('life and limb', '2007-02-01', 'Forests that enter the battlefield while Life and Limb is on the battlefield will enter the battlefield as creatures. They\'ll have summoning sickness.').
card_ruling('life and limb', '2007-02-01', 'Saprolings that are on the battlefield while Life and Limb is on the battlefield have the ability \"{T}: Add {G} to your mana pool.\" Although they\'re Forests, they are not basic lands.').
card_ruling('life and limb', '2007-02-01', 'With Life and Limb on the battlefield, Mistform Ultimus will be a legendary 1/1 green land creature that has land type Forest and has all creature types.').

card_ruling('life from the loam', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('life from the loam', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('life from the loam', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('life matrix', '2004-10-04', 'Once the counter is placed on the creature, the ability to remove it can only be activated by the player who controls the creature.').

card_ruling('life\'s legacy', '2014-07-18', 'You must sacrifice exactly one creature to cast this spell; you can’t cast it without sacrificing a creature, and you can’t sacrifice additional creatures.').
card_ruling('life\'s legacy', '2014-07-18', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('lifeblood', '2004-10-04', 'In a multi-player game, it affects all opponents.').

card_ruling('lifeblood hydra', '2014-11-07', 'Use Lifeblood Hydra’s power when it died (including any +1/+1 counters it had) to determine how much life to gain and how many cards to draw.').

card_ruling('lifeline', '2004-10-04', 'It fails to bring the creature back if the creature is not still in the graveyard at the end of turn.').
card_ruling('lifeline', '2004-10-04', 'It works for all players and has errata to remove the \"your graveyard\" text.').
card_ruling('lifeline', '2004-10-04', 'The creature comes back even if Lifeline leaves the battlefield after triggering, but before it resolves.').
card_ruling('lifeline', '2004-10-04', 'If multiple creatures are coming back, they come back one at a time, not all at once. This is because Lifeline triggered once for each creature and set up a separate \"at end of turn\" effect for each.').
card_ruling('lifeline', '2010-03-01', 'If more than one creature is on the battlefield and all the creatures on the battlefield go to the graveyard at once, then none of them are returned at end of turn. This is because Lifeline\'s ability has an intervening-if clause, which means that there must be at least one creature on the battlefield at the time the ability resolves.').

card_ruling('lifelink', '2009-10-01', 'The controller of the enchanted creature, not the controller of Lifelink, gains the life (in case they\'re different players).').

card_ruling('lifespark spellbomb', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('lifetap', '2004-10-04', 'Gives one life for each and every Forest tapped.').
card_ruling('lifetap', '2004-10-04', 'In multi-player games it affects all opponents.').

card_ruling('lifted by clouds', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('lifted by clouds', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('lifted by clouds', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('lifted by clouds', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('light from within', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('light from within', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').
card_ruling('light from within', '2008-08-01', 'Light from Within affects each creature individually. For example, if you control a creature with mana cost {1}{W}{W} and a creature with mana cost {W/B}{W/B}{W/B}{W/B}{W/B}, the first creature gets +2/+2 and the second creature gets +5/+5.').

card_ruling('lightform', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('lightform', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('lightform', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('lightform', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('lightform', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('lightform', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('lightform', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('lightform', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('lightform', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('lightform', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('lightform', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('lightform', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('lightform', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').
card_ruling('lightform', '2014-11-24', 'You’ll still manifest the top card of your library even if the “Form” isn’t on the battlefield as its enters-the-battlefield ability resolves.').
card_ruling('lightform', '2014-11-24', 'If you have no cards in your library as the ability resolves, the “Form” will be put into its owner’s graveyard as a state-based action.').
card_ruling('lightform', '2014-11-24', 'If the enchanted creature is turned face up, the “Form” will continue to enchant it.').

card_ruling('lighthouse chronologist', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('lighthouse chronologist', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('lighthouse chronologist', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('lighthouse chronologist', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('lighthouse chronologist', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('lighthouse chronologist', '2010-06-15', 'In a two-player game, if you have a level 7 Lighthouse Chronologist and there are no other extra-turn effects, the game will proceed like this (with extra turns in brackets): your turn, your opponent\'s turn, [your turn], your turn, your opponent\'s turn, [your turn], your turn, your opponent\'s turn, and so on.').
card_ruling('lighthouse chronologist', '2010-06-15', 'In a four-player game, if you have a level 7 Lighthouse Chronologist and there are no other extra-turn effects, the game will proceed like this (with extra turns in brackets): your turn, opponent A\'s turn, [your turn], opponent B\'s turn, [your turn], opponent C\'s turn, [your turn], your turn, opponent A\'s turn, and so on. The same principle applies to a multiplayer game with a different number of players.').
card_ruling('lighthouse chronologist', '2010-06-15', 'The effects of multiple level 7 Lighthouse Chronologists are cumulative. If you control two of them in a two-player game, you\'ll take three turns for each turn your opponent takes.').
card_ruling('lighthouse chronologist', '2010-06-15', 'In a two-player game, if you and your opponent each control a level 7 Lighthouse Chronologist, they\'ll essentially cancel each other out and you\'ll alternate turns. Since all the turns you\'d be taking are extra turns created by these abilities, make sure you know whose normal turn is supposed to be next in case one or both of the Lighthouse Chronologists leaves the battlefield.').
card_ruling('lighthouse chronologist', '2010-06-15', 'In a multiplayer game, if multiple players each control a level 7 Lighthouse Chronologist, extra turns may sometimes be created faster than they can be taken. Keep track of them carefully. If multiple Chronologist\'s abilities trigger during the same turn, the player whose turn would show up sooner in the natural turn order will get the first extra turn. For example, say Players B and C each control a level 7 Lighthouse Chronologist. Player A takes a turn. The game will proceed like this (with extra turns in brackets): Player A\'s current turn, [Player B\'s turn], [Player C\'s turn], [Player B\'s turn], [Player C\'s turn], and so on. Because Players B and C will each add another extra turn after the other player\'s extra turn, Player A will not take another turn as long as both level 7 Lighthouse Chronologists remain on the battlefield.').

card_ruling('lightmine field', '2010-06-15', 'Lightmine Field\'s ability triggers when any creatures attack, including your own.').
card_ruling('lightmine field', '2010-06-15', 'Which creatures are dealt damage is based on which creatures were declared as attackers during the declare attackers step.').
card_ruling('lightmine field', '2010-06-15', 'How much damage each of those creatures is dealt is based how many creatures are attacking at the time the ability resolves.').
card_ruling('lightmine field', '2010-06-15', 'For example: If a creature attacks but is removed from combat before Lightmine Field\'s ability resolves (because it would be destroyed and regenerates, perhaps), that creature is dealt damage by Lightmine Field but isn\'t counted when determining the amount of damage. On the other hand, if a creature is put onto the battlefield attacking (due to Preeminent Captain\'s ability, perhaps), it isn\'t dealt damage by Lightmine Field, but it is counted when determining the amount of damage.').

card_ruling('lightning axe', '2006-09-25', 'This has an additional cost with an option. Essentially, the card\'s cost to cast is either {5}{R} or {R} and a discard. But its mana cost is always {R} and its converted mana cost is always 1.').

card_ruling('lightning berserker', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('lightning berserker', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('lightning berserker', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('lightning berserker', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('lightning blow', '2009-10-01', 'You may target any creature, not just one that doesn\'t have first strike. If the targeted creature already has first strike as Lightning Blow resolves, Lightning Blow won\'t affect it (since multiple instances of first strike are redundant), but you\'ll still draw a card at the beginning of the next turn\'s upkeep.').
card_ruling('lightning blow', '2009-10-01', 'If the targeted creature doesn\'t have first strike, granting it first strike after combat damage has been dealt in the first combat damage step won\'t prevent it from dealing combat damage. It will still assign and deal its combat damage in the second combat damage step.').

card_ruling('lightning cloud', '2004-10-04', 'Triggers when the spell is cast.').
card_ruling('lightning cloud', '2004-10-04', 'You pick the target when putting the triggered ability on the stack, but you don\'t choose whether or not to pay {R} until the triggered ability resolves.').

card_ruling('lightning diadem', '2014-04-26', 'If Lightning Diadem’s target is an illegal target when it tries to resolve, it will be countered and won’t enter the battlefield. Its triggered ability won’t trigger.').

card_ruling('lightning greaves', '2013-04-15', 'You are not allowed to \"unequip\" equipment from a creature. If Lightning Greaves is attached to the only creature you control, you won\'t be able to attach other equipment to it (or target it with anything else) until you have another creature onto which you can move Lightning Greaves.').

card_ruling('lightning helix', '2013-06-07', 'If the creature or player is an illegal target as Lightning Helix tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 3 life.').

card_ruling('lightning mauler', '2012-05-01', 'A creature that loses haste after it\'s been declared as an attacking creature doesn\'t stop attacking.').

card_ruling('lightning reflexes', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('lightning rift', '2004-10-04', 'You choose the target when this ability is put on the stack, but you do not decide whether or not to pay the mana until the ability resolves.').

card_ruling('lightning serpent', '2006-07-15', 'If Lightning Serpent enters the battlefield without X being paid because an effect puts it onto the battlefield or an effect lets you cast it without paying its mana cost, then X is 0.').

card_ruling('lightning shrieker', '2014-11-24', 'Lightning Shrieker’s owner shuffles it into his or her library only if it’s on the battlefield as its ability resolves.').

card_ruling('lightning storm', '2006-07-15', 'Lightning Storm has an activated ability that can be activated only while the spell is on the stack. Any player may activate that ability. When the ability is activated, it goes on the stack on top of Lightning Storm. When the ability resolves, the Lightning Storm spell is affected.').
card_ruling('lightning storm', '2006-07-15', 'When you cast Lightning Storm, you immediately have a chance to activate its ability since you\'ll have priority. If you don\'t, and all other players pass, the spell will resolve. You won\'t receive priority again -- and thus won\'t get another chance to activate its ability -- unless someone else activates Lightning Storm\'s activated ability, casts some other spell, or activates some other ability.').
card_ruling('lightning storm', '2006-07-15', 'After an instance of Lightning Storm\'s activated ability resolves, the active player (not Lightning Storm\'s controller) receives priority.').
card_ruling('lightning storm', '2006-07-15', 'When Lightning Storm\'s activated ability resolves, if there are no other legal targets to choose for the Lightning Storm spell, you must leave the target the same, even if it\'s now an illegal target.').
card_ruling('lightning storm', '2006-07-15', 'When Lightning Storm leaves the stack, its charge counters go away.').

card_ruling('lightning surge', '2004-10-04', 'It either deals 4 damage or 6 damage. It does not deal 10 damage.').
card_ruling('lightning surge', '2013-04-15', 'At the time this spell is resolving, it is on the stack and not in the graveyard. This means that you should not count this card when determining whether the Threshold effect occurs.').

card_ruling('lightwielder paladin', '2009-10-01', 'Lands are colorless (even if their frames have some colored elements to them). You can\'t target a Swamp, a Mountain, or any other land with Lightwielder Paladin\'s ability (unless some other effect has turned that land black or red).').

card_ruling('lignify', '2007-10-01', 'Lignify overwrites characteristic-defining abilities that specify the enchanted creature\'s power or toughness (such as the one Dauntless Dourbark has) or that specify its creature types (such as changeling does).').
card_ruling('lignify', '2007-10-01', 'Lignify will overwrite static abilities that come from the creature itself (such as the one on Verduran Enchantress) because Lignify causes the creature to lose that ability before it gets a chance to apply.').
card_ruling('lignify', '2007-10-01', 'Lignify overwrites previous effects that changed the enchanted creature\'s creature types. Any such effects that apply after Lignify enters the battlefield will work normally.').
card_ruling('lignify', '2007-10-01', 'Lignify causes the enchanted creature to lose all abilities it had at the time it became enchanted. Any abilities granted to the creature after Lignify entered the battlefield will work normally.').
card_ruling('lignify', '2007-10-01', 'If a previous effect switched the creature\'s power and toughness, that effect still applies (though it will apply to the creature\'s new power and toughness).').
card_ruling('lignify', '2007-10-01', 'If the enchanted creature has a characteristic-defining ability that specifies its color, that ability will still have its effect because it\'s applied before the creature loses the ability.').
card_ruling('lignify', '2007-10-01', 'If Lignify leaves the battlefield, the creature goes back to being what it was before. Any effects that applied while the Lignify was on the battlefield, such as Giant Growth or Lace with Moonglove, will continue to apply.').
card_ruling('lignify', '2009-10-01', 'Lignify will overwrite any previous effects that set the enchanted creature\'s power or toughness (such as Serendib Sorcerer does). Any such effects that apply after Lignify entered the battlefield will work normally.').
card_ruling('lignify', '2009-10-01', 'Lignify will not overwrite effects such as those from Glorious Anthem, counters, or Giant Growth that change the enchanted creature\'s power or toughness without setting it to a specific value. Apply such effects after applying Lignify.').

card_ruling('liliana of the dark realms', '2012-07-01', 'You can activate Liliana\'s first ability even if your library contains no Swamp cards (or you don\'t want to find one). You\'ll still search your library and shuffle it.').
card_ruling('liliana of the dark realms', '2012-07-01', 'When you activate Liliana\'s second ability, you choose only the target creature. You choose whether that creature gets +X/+X or -X/-X when the ability resolves. The value of X is determined by the number of Swamps you control when the ability resolves.').
card_ruling('liliana of the dark realms', '2012-07-01', 'Liliana\'s emblem doesn\'t remove any other abilities of Swamps you control.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Each of Liliana’s abilities refers to lands (or land cards) with the subtype Swamp, not just ones named Swamp.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('liliana of the dark realms', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('liliana of the dark realms', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('liliana of the dark realms', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('liliana of the dark realms', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('liliana of the dark realms', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('liliana of the dark realms', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('liliana of the dark realms', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('liliana of the veil', '2011-09-22', 'When the first ability resolves, first you choose a card to discard, then each other player in turn order chooses a card to discard, then all those cards are discarded simultaneously. No one sees what the other players are discarding before deciding which card to discard.').
card_ruling('liliana of the veil', '2011-09-22', 'The player targeted by the second ability chooses which creature to sacrifice when the ability resolves. This ability doesn\'t target any creature.').
card_ruling('liliana of the veil', '2011-09-22', 'When the third ability resolves, you put each permanent the player controls into one of the two piles. For example, you could put a creature into one pile and an Aura enchanting that creature into the other pile.').
card_ruling('liliana of the veil', '2011-09-22', 'A pile can be empty. If the player chooses an empty pile, no permanents will be sacrificed.').
card_ruling('liliana of the veil', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('liliana of the veil', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('liliana of the veil', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('liliana of the veil', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('liliana of the veil', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('liliana of the veil', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('liliana of the veil', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('liliana of the veil', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('liliana of the veil', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('liliana of the veil', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('liliana of the veil', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('liliana vess', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('liliana vess', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('liliana vess', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('liliana vess', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('liliana vess', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('liliana vess', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('liliana vess', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('liliana vess', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('liliana vess', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('liliana vess', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('liliana vess', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('liliana vess', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('liliana\'s caress', '2010-08-15', 'If an opponent discards multiple cards (due to a spell, an ability, or the action of discarding down to the maximum hand size during his or her cleanup step), Liliana\'s Caress\'s ability triggers that many times.').

card_ruling('liliana, defiant necromancer', '2015-06-22', 'When Liliana, Defiant Necromancer’s first ability resolves, first you choose a card to discard, then each other player in turn order chooses a card to discard, then all those cards are discarded simultaneously. No one sees what the other players are discarding before deciding which card to discard.').

card_ruling('liliana, heretical healer', '2015-06-22', 'If multiple nontoken creatures you control die, then Liliana, Heretical Healer’s ability will trigger that many times. However, since Liliana can be exiled and returned to the battlefield only once, only the first ability to resolve will create a Zombie token. The other abilities will resolve but won’t do anything.').
card_ruling('liliana, heretical healer', '2015-06-22', 'Each face of a double-faced card has its own set of characteristics: name, types, subtypes, power and toughness, loyalty, abilities, and so on. While a double-faced card is on the battlefield, consider only the characteristics of the face that’s currently up. The other set of characteristics is ignored. While a double-faced card isn’t on the battlefield, consider only the characteristics of its front face.').
card_ruling('liliana, heretical healer', '2015-06-22', 'The converted mana cost of a double-faced card not on the battlefield is the converted mana cost of its front face.').
card_ruling('liliana, heretical healer', '2015-06-22', 'The back face of a double-faced card doesn’t have a mana cost. A double-faced permanent with its back face up has a converted mana cost of 0. Each back face has a color indicator that defines its color.').
card_ruling('liliana, heretical healer', '2015-06-22', 'The back face of a double-faced card (in the case of Magic Origins, the planeswalker face) can’t be cast.').
card_ruling('liliana, heretical healer', '2015-06-22', 'Although the two rules are similar, the “legend rule” and the “planeswalker uniqueness rule” affect different kinds of permanents. You can control two of this permanent, one front face-up and the other back-face up at the same time. However, if the former is exiled and enters the battlefield transformed, you’ll then control two planeswalkers with the same subtype. You’ll choose one to remain on the battlefield, and the other will be put into its owner’s graveyard.').
card_ruling('liliana, heretical healer', '2015-06-22', 'A double-faced card enters the battlefield with its front face up by default, unless a spell or ability instructs you to put it onto the battlefield transformed, in which case it enters with its back face up.').
card_ruling('liliana, heretical healer', '2015-06-22', 'A Magic Origins planeswalker that enters the battlefield because of the ability of its front face will enter with loyalty counters as normal.').
card_ruling('liliana, heretical healer', '2015-06-22', 'In some rare cases, a spell or ability may cause one of these five cards to transform while it’s a creature (front face up) on the battlefield. If this happens, the resulting planeswalker won’t have any loyalty counters on it and will subsequently be put into its owner’s graveyard.').
card_ruling('liliana, heretical healer', '2015-06-22', 'You can activate one of the planeswalker’s loyalty abilities the turn it enters the battlefield. However, you may do so only during one of your main phases when the stack is empty. For example, if the planeswalker enters the battlefield during combat, there will be an opportunity for your opponent to remove it before you can activate one of its abilities.').
card_ruling('liliana, heretical healer', '2015-06-22', 'If a double-faced card is manifested, it will be put onto the battlefield face down (this is also true if it’s put onto the battlefield face down some other way). Note that “face down” is not synonymous with “with its back face up.” A manifested double-faced card is a 2/2 creature with no name, mana cost, creature types, or abilities. While face down, it can’t transform. If the front face of a manifested double-faced card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced card on the battlefield can’t be turned face down.').

card_ruling('lilting refrain', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('lim-dûl\'s paladin', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('lim-dûl\'s vault', '2007-09-16', 'In other words: -- Step 1: Look at the top five cards of your library. -- Step 2: If you like them, proceed to step 3a. If you don\'t like them, proceed to step 3b. -- Step 3a: Shuffle the rest of your library, then put those five cards back on top of your library in the order you want. The spell has finished resolving. -- Step 3b: Put those five cards on the bottom of your library in the order you want. Pay 1 life. Return to step 1.').
card_ruling('lim-dûl\'s vault', '2007-09-16', 'If you pay no life, you shuffle the rest of your library and then put the top five cards back on top in any order.').
card_ruling('lim-dûl\'s vault', '2007-09-16', 'If you have five or fewer cards in your library, you can pay 1 life as many times as you like, but you\'ll keep seeing the same cards.').

card_ruling('limited resources', '2004-10-04', 'The sacrificed lands are chosen during resolution. The current player chooses first.').
card_ruling('limited resources', '2004-10-04', 'If a player has less than 5 lands, they get to pick and keep all of their lands.').
card_ruling('limited resources', '2004-10-04', 'You can put lands onto the battlefield using effects, you just can\'t play lands from your hand as an action.').

card_ruling('lin sivvi, defiant hero', '2004-10-04', 'You do not have to find a Rebel card if you do not want to.').

card_ruling('linessa, zephyr mage', '2007-05-01', 'Linessa\'s grandeur ability targets only a player. That player chooses which permanents to return as the ability resolves. The permanents are returned one at a time, in a specific sequence. The player can\'t return an artifact creature and have that count as both the artifact and the creature, for example.').

card_ruling('lingering mirage', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('lingering tormentor', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('lingering tormentor', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('lingering tormentor', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('lingering tormentor', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('lingering tormentor', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('lingering tormentor', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('lingering tormentor', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('linvala, keeper of silence', '2010-06-15', 'Linvala\'s ability affects only creatures on the battlefield. Activated abilities that work in other zones (such as cycling or unearth) can still be activated.').
card_ruling('linvala, keeper of silence', '2010-06-15', 'No abilities of creatures your opponents control can be activated, including mana abilities.').
card_ruling('linvala, keeper of silence', '2010-06-15', 'Triggered abilities and static abilities of creatures your opponents control work normally.').
card_ruling('linvala, keeper of silence', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('lion\'s eye diamond', '2004-10-04', 'The ability is a mana ability, so it is activated and resolves as a mana ability, but it can only be activated at times when you can cast an instant. Yes, this is a bit weird.').

card_ruling('liquid fire', '2009-05-01', 'You choose the number as you cast the spell. If you choose 0, the creature\'s controller will be dealt 5 damage. If you choose 5, that player will not be dealt any damage.').

card_ruling('liquimetal coating', '2011-01-01', 'You may target any permanent with the ability, including an artifact.').
card_ruling('liquimetal coating', '2011-01-01', 'Becoming an artifact doesn\'t change what color(s) a permanent is.').

card_ruling('lithomancer\'s focus', '2015-08-25', 'Lithomancer’s Focus will prevent damage to the creature from any colorless source that turn, even if that source didn’t exist or wasn’t colorless as Lithomancer’s Focus resolved.').

card_ruling('liturgy of blood', '2013-07-01', 'Liturgy of Blood isn’t a mana ability and uses the stack. Players can respond to it by casting spells and activating abilities.').
card_ruling('liturgy of blood', '2013-07-01', 'If the target creature is an illegal target when Liturgy of Blood tries to resolve, it will be countered and none of its effects will happen. You won’t add mana to your mana pool.').

card_ruling('liu bei, lord of shu', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('livewire lash', '2011-01-01', 'When the equipped creature becomes the target of a spell, the ability granted to it by Livewire Lash will trigger and go on the stack on top of it. It will resolve before the spell does.').
card_ruling('livewire lash', '2011-01-01', 'The equipped creature is the source of both the triggered ability and the resultant damage, not Livewire Lash. For example, you can target and deal damage to a creature with protection from artifacts (assuming the equipped creature isn\'t an artifact itself, of course).').

card_ruling('living artifact', '2004-10-04', 'You can cast it targeting your opponent\'s artifacts. The controller of the Aura (not the controller of the artifact) controls the Living Artifact ability.').
card_ruling('living artifact', '2004-10-04', 'Does not trigger on loss of life, just on damage.').

card_ruling('living death', '2004-10-04', 'The creatures which are sacrificed can\'t be regenerated.').
card_ruling('living death', '2004-10-04', 'Both players exile their creature cards at once. Then both players sacrifice all creatures they control at once. Then both players put all creatures they exiled onto the battlefield. If there are any choices involved for a given step, the current player makes their choices first, then the other player, and finally you do the action all at once.').
card_ruling('living death', '2013-09-20', '\"All cards he or she exiled this way\" refers only to the cards exiled in the first part of the effect. If a replacement effect (such as Leyline of the Void) exiles any of the sacrificed creatures instead of putting them into the graveyard, those cards aren\'t returned to the battlefield.').

card_ruling('living end', '2006-09-25', '\"All cards he or she exiled this way\" refers only to the cards exiled in the first part of the effect. If a replacement effect (such as Leyline of the Void) exiles any of the sacrificed creatures instead of putting them into the graveyard, those cards aren\'t returned to the battlefield.').
card_ruling('living end', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('living end', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('living end', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('living end', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('living end', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('living end', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('living end', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('living end', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('living end', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('living end', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('living end', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('living end', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('living end', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('living inferno', '2006-02-01', 'You divide the damage among the target creatures as you activate Living Inferno\'s ability. Each target must be assigned at least 1 damage.').
card_ruling('living inferno', '2006-02-01', 'The amount of damage Living Inferno will deal is locked in as you activate its ability, since you\'re dividing damage. If Living Inferno\'s power changes after its ability is activated but before it resolves (via Giant Growth, for example), the amount of damage Living Inferno deals won\'t change. The same is *not* true for the amount of damage dealt to Living Inferno by the other creatures, which is determined when the ability resolves.').

card_ruling('living lands', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('living lore', '2015-02-25', 'If there are no instant or sorcery cards in your graveyard as Living Lore enters the battlefield, it will enter the battlefield as a 0/0 creature and be put into its owner’s graveyard due to having 0 toughness.').
card_ruling('living lore', '2015-02-25', 'If Living Lore leaves the battlefield, the ability that defines its power and toughness will lose track of the exiled card. Its power and toughness are 0 in other zones.').
card_ruling('living lore', '2015-02-25', 'Similarly, if the exiled card somehow leaves exile while Living Lore is on the battlefield, it will become a 0/0 creature and be put into its owner’s graveyard (unless something else is raising its toughness).').
card_ruling('living lore', '2015-02-25', 'Living Lore’s last ability will trigger whenever it deals combat damage, no matter if that damage is dealt to an opponent, an opposing planeswalker, or a creature it’s blocking or being blocked by. You choose whether to sacrifice Living Lore as that ability resolves.').
card_ruling('living lore', '2015-02-25', 'When casting a card with Living Lore’s last ability, ignore timing restrictions based on the card’s type. Other timing restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('living lore', '2015-02-25', 'If you cast a card “without paying its mana cost,” you can’t pay alternative costs. You can pay additional costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('living lore', '2015-02-25', 'If you choose to cast the exiled card and it has {X} in its mana cost, you must choose 0 as its value.').

card_ruling('living plane', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('living plane', '2009-10-01', 'A noncreature permanent that turns into a creature is subject to the \"summoning sickness\" rule: It can only attack, and its {T} abilities can only be activated, if its controller has continuously controlled that permanent since the beginning of his or her most recent turn.').

card_ruling('living terrain', '2004-10-04', 'It does give the land a color and creature type, which is unlike most other ways to animate a land.').
card_ruling('living terrain', '2004-10-04', 'This can be placed on any land, not just Forests.').
card_ruling('living terrain', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('living totem', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('living totem', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('living totem', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('living totem', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('living totem', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('living totem', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('living totem', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('living wish', '2004-10-04', 'Can\'t acquire the Ante cards. They are considered still \"in the game\" as are cards in the library and the graveyard.').
card_ruling('living wish', '2007-05-01', 'The choice is optional.').
card_ruling('living wish', '2007-05-01', 'If you fail to find something, you still exile this.').
card_ruling('living wish', '2007-09-16', 'Can\'t acquire cards that are phased out.').
card_ruling('living wish', '2009-10-01', 'You can\'t acquire exiled cards because those cards are still in one of the game\'s zones.').
card_ruling('living wish', '2009-10-01', 'In a sanctioned event, a card that\'s \"outside the game\" is one that\'s in your sideboard. In an unsanctioned event, you may choose any card from your collection.').

card_ruling('livonya silone', '2009-10-01', '\"Legendary landwalk\" means \"This creature is can\'t be blocked as long as defending player controls a legendary land.\"').

card_ruling('llanowar', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('llanowar', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('llanowar', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('llanowar', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').

card_ruling('llanowar behemoth', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap a creature (including Llanowar Behemoth itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('llanowar druid', '2008-04-01', 'Llanowar Druid\'s ability untaps all Forests on the battlefield, not just the ones you control.').

card_ruling('llanowar reborn', '2007-05-01', 'The +1/+1 counter won\'t affect Llanowar Reborn in any way unless another effect turns it into a creature.').

card_ruling('llanowar sentinel', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('llanowar sentinel', '2007-07-15', 'Llanowar Sentinels can \"chain\": If you put a second Llanowar Sentinel onto the battlefield as a result of this ability, that creature\'s enters-the-battlefield ability triggers and allows you to search for yet another Llanowar Sentinel.').

card_ruling('llanowar wastes', '2015-06-22', 'The damage dealt to you is part of the second mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('llanowar wastes', '2015-06-22', 'Like most lands, each land in this cycle is colorless. The damage dealt to you is dealt by a colorless source.').

card_ruling('loafing giant', '2004-10-04', 'If it somehow blocks multiple creatures in a single combat, the triggered ability only triggers once.').

card_ruling('loam lion', '2010-03-01', 'Loam Lion either gets +1/+2 or it doesn\'t. It doesn\'t get the bonus for each Forest you control.').

card_ruling('loaming shaman', '2006-05-01', 'Loaming Shaman\'s controller chooses all the targets when the ability is put on the stack.').

card_ruling('loathsome catoblepas', '2013-09-15', 'If Loathsome Catoblepas is attacking and its activated ability has resolved, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block it.').
card_ruling('loathsome catoblepas', '2013-09-15', 'The activated ability doesn’t target any creature or make any specific creature have to block. Activating it more than once in a turn provides no additional benefit.').

card_ruling('lobber crew', '2012-10-01', 'The hybrid spells in the Return to Ravnica set are multicolored, even if you cast them with one color of mana.').
card_ruling('lobber crew', '2012-10-01', 'You\'ll untap Lobber Crew before that spell resolves.').

card_ruling('lobotomy', '2004-10-04', 'You only get to pick one card from the player\'s hand.').
card_ruling('lobotomy', '2004-10-04', 'If the player has no cards in their hand, you still get to look through their deck, but you do not get to exile any cards.').
card_ruling('lobotomy', '2004-10-04', 'It exiles the chosen card plus all others in the hand, library, and graveyard. Some people thought the chosen card was not exiled due to some vague wording.').
card_ruling('lobotomy', '2004-10-04', 'Does not affect cards on the battlefield.').
card_ruling('lobotomy', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').

card_ruling('locket of yesterdays', '2006-09-25', 'Locket of Yesterdays reduces only the amount of mana you need to pay to cast a spell. The spell\'s mana cost and converted mana cost are not affected.').
card_ruling('locket of yesterdays', '2006-09-25', 'Colored costs can\'t be reduced by this effect. For example, a spell that costs {2}{R} can\'t be reduced by more than {2}.').
card_ruling('locket of yesterdays', '2006-09-25', 'The spell being cast doesn\'t reduce its own cost, even if you\'re casting it from a graveyard. At the time the cost is reduced, the spell is on the stack.').

card_ruling('lockjaw snapper', '2008-05-01', 'Each of those creatures will get just one new -1/-1 counter, regardless of how many it already has.').

card_ruling('locust miser', '2009-10-01', 'If multiple effects modify a player\'s hand size, apply them in timestamp order. For example, if you put Cursed Rack (an artifact that sets a chosen player\'s maximum hand size to four) onto the battlefield choosing your opponent and then put Locust Miser onto the battlefield, your opponent will have a maximum hand size of two. However, if those permanents entered the battlefield in the opposite order, your opponent\'s maximum hand size would be four.').

card_ruling('lodestone bauble', '2008-10-01', '\"Up to four\" means zero, one, two, three, or four. If you choose zero targets, you still choose a player. That player will draw a card at the beginning of the next turn\'s upkeep.').

card_ruling('lodestone golem', '2010-03-01', 'The ability affects each spell that\'s not an artifact spell, including your own.').
card_ruling('lodestone golem', '2010-03-01', 'The ability affects the total cost of each nonartifact spell, but it doesn\'t change that spell\'s mana cost or converted mana cost. The extra mana also doesn\'t change how many times you kicked the spell or what the value of X is (if applicable).').
card_ruling('lodestone golem', '2010-03-01', 'When determining a spell\'s total cost, effects that increase the cost are applied before effects that reduce the cost.').

card_ruling('logic knot', '2007-05-01', 'Using delve doesn\'t reduce the value of X. It just reduces the amount of mana Logic Knot\'s controller has to pay.').

card_ruling('lone revenant', '2012-05-01', 'You must put one of the cards into your hand, even if you\'d rather put all four on the bottom of your library.').
card_ruling('lone revenant', '2012-05-01', 'If you control Lone Revenant and any other creature (including another Lone Revenant), its ability won\'t trigger.').
card_ruling('lone revenant', '2012-05-01', 'Check whether Lone Revenant\'s ability triggers after combat damage is dealt but before any creatures that are destroyed due to combat damage leave the battlefield. For example, if you control Lone Revenant and another creature, and that other creature takes lethal damage in the same combat damage step that Lone Revenant deals damage to a player, Lone Revenant\'s ability won\'t trigger.').
card_ruling('lone revenant', '2012-05-01', 'If you control no creatures when Lone Revenant\'s ability resolves, you\'ll still look at the top four cards and put one into your hand.').

card_ruling('lone wolf', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('lone wolf', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('lone wolf', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('lone wolf', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('lonely sandbar', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('long-forgotten gohei', '2004-12-01', 'The cost reduction applies to the total cost of the Arcane spell, including any additional costs from cards spliced onto it. However, the cost reduction applies only once to each Arcane spell. For example, if you cast an Arcane spell with mana cost {R} and splice onto it a card whose splice cost is {1}{U}, then the spell costs {U}{R} to cast. If you cast an Arcane spell with mana cost {4}{U} and splice onto it a card whose splice cost is {1}{R}, the spell costs {4}{U}{R} to cast.').

card_ruling('long-term plans', '2004-10-04', 'If there are fewer than 3 cards in your library, put the card on the bottom of your library.').

card_ruling('longbow archer', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('longshot squad', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('longshot squad', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('lord magnus', '2004-10-04', 'The statement that creatures with Plainswalk or Forestwalk can be blocked applies to all creatures on the battlefield blocking them, and not just to Lord Magnus.').

card_ruling('lord of shatterskull pass', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'The last ability triggers and resolves during the declare attackers step, before blockers are declared.').
card_ruling('lord of shatterskull pass', '2010-06-15', 'In a multiplayer game, the only player affected by the last ability is the one Lord of Shatterskull Pass is attacking (or the one who controls the planeswalker Lord of Shatterskull Pass is attacking).').
card_ruling('lord of shatterskull pass', '2010-06-15', 'If a level 6 Lord of Shatterskull Pass attacks in a Two-Headed Giant game, its controller chooses which one of the defending players is affected by its ability. The choice is made as the ability resolves.').

card_ruling('lord of the pit', '2004-10-04', 'You can select creatures with Protection from Black to be sacrificed.').
card_ruling('lord of the pit', '2004-10-04', 'If you have a creature, you must sacrifice it. The 7 damage can only be taken if no creature can be sacrificed.').
card_ruling('lord of the pit', '2004-10-04', 'If you have two Lords of the Pit, you can sacrifice them to each other.').

card_ruling('lord of the undead', '2005-08-01', 'Lord of the Undead now has the Zombie creature type and its first ability has been reworded to affect *other* Zombies. This means that if two Lord of the Undead are on the battlefield, each gives the other a bonus.').

card_ruling('lord of the void', '2013-01-24', 'Lord of the Void\'s ability isn\'t optional. If there is a creature card among the seven cards you exiled, you must put one onto the battlefield under your control.').

card_ruling('lord of tresserhorn', '2004-10-04', 'When Lord of Tresserhorn enters the battlefield, if you have zero or one other creature on the battlefield, Lord of Tresserhorn itself will be sacrificed as part of its ability\'s effect. Regeneration won\'t save it. The rest of the effect still happens.').
card_ruling('lord of tresserhorn', '2004-10-04', 'Since this is a loss of life and not a payment, your life total can be reduced below zero by this.').

card_ruling('lore seeker', '2014-05-29', 'The booster pack can be from any Magic: The Gathering set, but you must provide it. Consult your co-conspirators for guidance.').
card_ruling('lore seeker', '2014-05-29', 'To add a booster pack to the draft, first pass the booster pack that contained Lore Seeker to the next player. Then open the new booster pack, draft a card, and pass the new booster pack in the same direction. Then you’ll receive the booster pack you would’ve received had you not added a booster pack, and the draft round will continue. The added booster pack will last a few picks longer than the rest of the booster packs opened that draft round.').
card_ruling('lore seeker', '2014-05-29', 'Adding a booster pack to a draft may cause some players to have more cards in their card pools than others. This is normal.').
card_ruling('lore seeker', '2014-05-29', 'If you use Agent of Acquisitions to draft a booster pack containing Lore Seeker, and you wish to add a booster pack to the draft, first draft each remaining card from the Lore Seeker booster pack. Then open the new booster pack. You may look at the cards in that pack, but you can’t draft any cards from it. You’ll pass the new booster pack as normal. That was very generous of you.').

card_ruling('lorescale coatl', '2009-05-01', 'If a spell or ability causes you to draw multiple cards, Lorescale Coatl\'s ability triggers that many times.').
card_ruling('lorescale coatl', '2009-05-01', 'If a spell or ability causes you to put cards in your hand without specifically using the word \"draw,\" Lorescale Coatl\'s ability won\'t trigger.').

card_ruling('loreseeker\'s stone', '2014-11-07', 'Use the number of cards in your hand at the time you activate the ability to determine how much mana to add to the activation cost.').

card_ruling('lorthos, the tidemaker', '2009-10-01', 'You may target any eight (or fewer) permanents. It\'s okay if any of them are already tapped, and it\'s okay if any of them are controlled by someone other than the defending player.').
card_ruling('lorthos, the tidemaker', '2009-10-01', 'If a targeted permanent is untapped at the time its controller\'s next untap step begins, this ability has no effect on it. It won\'t apply at some later time when the targeted permanent is tapped.').
card_ruling('lorthos, the tidemaker', '2009-10-01', 'If an affected permanent changes controllers before its old controller\'s next untap step, this ability will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('lose calm', '2015-02-25', 'Lose Calm can target any creature, including one that’s untapped or one you already control.').
card_ruling('lose calm', '2015-02-25', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('loss', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('loss', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('loss', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('loss', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('loss', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('loss', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('loss', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('loss', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('loss', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('loss', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('loss', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('lost auramancers', '2007-05-01', 'Lost Auramancers doesn\'t have to have been sacrificed as part of the vanishing ability for its last ability to trigger. It will trigger no matter why it was put into a graveyard from the battlefield, as long as it had no time counters on it at the time.').

card_ruling('lost hours', '2007-05-01', 'If the player has zero or one card left in his or her library, the card is put on the bottom of that library.').
card_ruling('lost hours', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('lost in a labyrinth', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('lost in a labyrinth', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('lost in a labyrinth', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('lost in a labyrinth', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('lost in the mist', '2011-09-22', 'Lost in the Mist targets both the spell and the permanent. You can only cast it if you can choose legal targets for both parts.').
card_ruling('lost in the mist', '2011-09-22', 'If one of Lost in the Mist\'s targets is illegal by the time it resolves, Lost in the Mist will still affect the remaining legal target. If both targets are illegal at this time, Lost in the Mist will be countered.').

card_ruling('lost in the woods', '2011-01-22', 'Removing an attacking creature from combat doesn\'t untap that creature.').
card_ruling('lost in the woods', '2011-01-22', 'Because Lost in the Woods will trigger once for each creature that attacks you or a planeswalker you control, it\'s important to specify which trigger is associated with which attacking creature before revealing any cards.').

card_ruling('lost in thought', '2004-10-04', 'You can take the \"exile three cards\" action any time you have priority without using the stack.').
card_ruling('lost in thought', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('lotus bloom', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('lotus bloom', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('lotus bloom', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('lotus bloom', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('lotus bloom', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('lotus bloom', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('lotus bloom', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('lotus bloom', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('lotus bloom', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('lotus bloom', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('lotus bloom', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('lotus bloom', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('lotus blossom', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('lotus cobra', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('lotus cobra', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('lotus path djinn', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('lotus path djinn', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('lotus path djinn', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('lotus path djinn', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('lotus vale', '2008-04-01', 'If you don\'t sacrifice the lands, Lotus Vale never enters the battlefield -- it goes directly to your graveyard.').

card_ruling('lotus-eye mystics', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('lotus-eye mystics', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('lotus-eye mystics', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('lotus-eye mystics', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('lovisa coldeyes', '2006-07-15', 'Each creature with at least one of those subtypes gets +2/+2 from Lovisa Coldeyes\' ability, regardless of how many of those subtypes it has. For example, a 3/3 Mistform Ultimus would become 5/5, not 9/9.').
card_ruling('lovisa coldeyes', '2007-10-01', 'Does not give herself a bonus unless she somehow gains the creature type Warrior or Berserker.').

card_ruling('lowland basilisk', '2004-10-04', 'If a creature leaves the battlefield before the end of combat, then the Lowland Basilisk effect on it will end.').
card_ruling('lowland basilisk', '2004-10-04', 'If it damages a creature outside of combat, its effect will destroy the creature at the end of the next combat.').

card_ruling('loxodon hierarch', '2005-10-01', 'The second ability sets up individual regeneration shields for each creature you control. They are each used up as necessary during the turn and wear off when the turn ends.').

card_ruling('loxodon smiter', '2012-10-01', 'If you discard Loxodon Smiter and put it onto the battlefield, you\'ve still discarded a card. Abilities that trigger when you discard a card (such as the one from Liliana\'s Caress) will still trigger.').

card_ruling('loyal cathar', '2011-01-22', 'When Loyal Cathar dies, it will return to the battlefield with its back face (Unhallowed Cathar) up. It doesn\'t return to the battlefield and then transform.').
card_ruling('loyal cathar', '2011-01-22', 'If a card that isn\'t a double-faced card becomes a copy of Loyal Cathar (due to Cytoshape, for example) and then dies, that card will return to the battlefield. It will no longer be a copy of Loyal Cathar and will simply have its printed characteristics.').
card_ruling('loyal cathar', '2011-01-22', 'Another double-faced card that becomes a copy of Loyal Cathar and then dies will return to the battlefield with its back face up.').
card_ruling('loyal cathar', '2011-01-22', 'If you gain control of a Loyal Cathar owned by another player and it dies, it will be put into to its owner\'s graveyard and then return to the battlefield under your control.').

card_ruling('loyal pegasus', '2014-02-01', 'Loyal Pegasus can be declared as an attacker only if another creature is declared as an attacker at the same time. Similarly, Loyal Pegasus can be declared as a blocker only if another creature is declared as a blocker at the same time.').
card_ruling('loyal pegasus', '2014-02-01', 'If you control more than one Loyal Pegasus, they can both attack or block together, even if no other creatures attack or block.').
card_ruling('loyal pegasus', '2014-02-01', 'Although Loyal Pegasus can’t attack alone, other attacking creatures don’t have to attack the same player or planeswalker. For example, Loyal Pegasus could attack an opponent and another creature could attack a planeswalker that opponent controls.').
card_ruling('loyal pegasus', '2014-02-01', 'In a Two-Headed Giant game (or in another format using the shared team turns option), Loyal Pegasus can attack or block with a creature controlled by your teammate, even if no other creatures you control are attacking or blocking.').

card_ruling('loyal retainers', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('lu bu, master-at-arms', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('lu meng, wu general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('lu su, wu advisor', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('lu xun, scholar general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').
card_ruling('lu xun, scholar general', '2009-10-01', 'Lu Xun\'s second ability triggers whenever it deals damage to an opponent for any reason. It\'s not limited to combat damage.').

card_ruling('lucent liminid', '2007-05-01', 'Lucent Liminid is an enchantment creature, in much the same way that Ornithopter is an artifact creature. It\'s affected by anything that affects creatures and anything that affects enchantments.').

card_ruling('ludevic\'s test subject', '2011-09-22', 'It\'s possible (though not advisable) to activate the ability of Ludevic\'s Test Subject enough times in response to one another that a resolving ability places a fifth hatchling counter on Ludevic\'s *Abomination*. If that happens, it\'ll transform back into Ludevic\'s Test Subject.').

card_ruling('lull', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('lullmage mentor', '2009-10-01', 'A spell or ability counters a spell only if it specifically contains the word \"counter\" in its text. If a spell or ability you control causes all the targets of a spell to become illegal, that spell is countered by the game rules, not the spell or ability you control. For example, if you control Lullmage Mentor and cast Doom Blade to destroy a creature targeted by Giant Growth, you do not get a Merfolk token when the game rules counter Giant Growth.').
card_ruling('lullmage mentor', '2009-10-01', 'Lullmage Mentor\'s second ability will cause its first ability to trigger.').
card_ruling('lullmage mentor', '2009-10-01', 'Since the second ability doesn\'t have a tap symbol in its cost, you can tap creatures (including Lullmage Mentor itself) that haven\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('lumbering falls', '2015-08-25', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature nor when it entered the battlefield.').
card_ruling('lumbering falls', '2015-08-25', 'This land is colorless until the last ability gives it colors.').
card_ruling('lumbering falls', '2015-08-25', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won’t trigger.').
card_ruling('lumbering falls', '2015-08-25', 'An ability that turns a land into a creature also sets that creature’s power and toughness. If the land was already a creature (for example, if it was the target of a spell with awaken), this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness will continue to apply no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness. For example, if Lumbering Falls has been made a 0/0 creature with three +1/+1 counters on it, activating its last ability will turn it into a 6/6 creature that’s still a land.').

card_ruling('lumengrid drake', '2011-01-01', 'The triggered ability is mandatory. If you control three or more artifacts as Lumengrid Drake enters the battlefield, you must target a creature. If there are no other creatures on the battlefield, you must target Lumengrid Drake itself. As the ability resolves, if you control three or more artifacts, you must return the targeted creature to its owner\'s hand.').

card_ruling('luminarch ascension', '2009-10-01', 'Luminarch Ascension\'s first ability has an \"intervening \'if\' clause.\" It checks whether you\'ve lost life during the turn both as it would trigger and as it resolves.').
card_ruling('luminarch ascension', '2009-10-01', 'Life lost during the end step after Luminarch Ascension\'s first ability resolves has no bearing on it.').
card_ruling('luminarch ascension', '2009-10-01', 'Luminarch Ascension\'s first ability checks only whether life was lost. It doesn\'t care whether life was also gained. For example, if you lost 4 life and gained 6 life during the turn, you\'ll have a higher life total than you started the turn with -- but Luminarch Ascension\'s first ability won\'t trigger.').
card_ruling('luminarch ascension', '2009-10-01', 'Luminarch Ascension\'s second ability checks how many quest counters are on it only at the time you activate the ability. Once activated, the ability will resolve as normal even if quest counters are removed from Luminarch Ascension.').
card_ruling('luminarch ascension', '2009-10-01', 'In a Two-Headed Giant game, Luminarch Ascension\'s first ability will trigger twice at the beginning of the opposing team\'s end step if you didn\'t lose life that turn. If both abilities resolve, it will get two quest counters. (It doesn\'t matter whether your teammate lost life or not.)').

card_ruling('luminate primordial', '2013-01-24', 'You can choose a number of targets up to the number of opponents you have, one target per opponent.').
card_ruling('luminate primordial', '2013-01-24', 'To determine how much life each opponent gains, use the power of the creature that opponent controlled as it last existed on the battlefield.').
card_ruling('luminate primordial', '2013-01-24', 'If some, but not all, of the targets become illegal, the controller(s) of the illegal target(s) will still gain life, even though the creature is not exiled.').

card_ruling('luminescent rain', '2008-04-01', 'Note that this spell counts the number of permanents you control of the chosen type, not just the number of creatures you control. It will also take your tribal permanents into account.').

card_ruling('luminous wake', '2010-06-15', 'The controller of Luminous Wake (who is not necessarily the controller of the enchanted creature) controls the triggered ability and gains life when it resolves. In other words, if you enchant your opponent\'s creature with Luminous Wake, you -- not your opponent -- gain life whenever that creature attacks or blocks.').
card_ruling('luminous wake', '2010-06-15', 'When the enchanted creature blocks, Luminous Wake\'s ability triggers just once, even if that creature somehow blocked multiple attacking creatures.').

card_ruling('lunar avenger', '2004-12-01', 'You choose whether Lunar Avenger gains flying, first strike, or haste when the activated ability resolves.').

card_ruling('lunar mystic', '2012-05-01', 'The triggered ability will go on the stack on top of the instant spell and resolve before it.').
card_ruling('lunar mystic', '2012-05-01', 'The decision to pay {1} is made when the triggered ability resolves. It\'s not an additional cost of the instant spell.').

card_ruling('lunk errant', '2008-04-01', 'Lunk Errant \"attacks alone\" if it\'s the only creature declared as an attacker as the declare attackers step begins. The ability won\'t trigger if Lunk Errant and some other creatures are declared as attackers, then those other creatures leave the battlefield or are otherwise removed from combat. The ability also won\'t trigger if Lunk Errant is put onto the battlefield attacking, even if no other creatures are attacking at that time.').
card_ruling('lunk errant', '2008-04-01', 'In a Two-Headed Giant game, Lunk Errant must be the only creature declared as an attacker by the entire attacking team for it to be considered \"attacking alone.\"').

card_ruling('lure', '2004-10-04', 'Lure does not give a creature the ability to block the Lured creature, it just forces those creatures which are already able to block the Lured creature to do so.').
card_ruling('lure', '2004-10-04', 'If a Lured creature is in a band, only blockers able to block the Lured creature are affected by the Lure.').
card_ruling('lure', '2004-10-04', 'The opponent does not have to activate any abilities in an attempt to make his or her creatures block. More specifically, a cost to block of zero mana is an optional cost you can choose not to pay.').

card_ruling('lure of prey', '2004-10-04', 'Does work for artifact creatures cast by your opponent.').
card_ruling('lure of prey', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('lurebound scarecrow', '2008-05-01', 'If you control a Lurebound Scarecrow but no color was chosen for it (perhaps because your Cemetery Puca turned into a Lurebound Scarecrow), the trigger condition will have an undefined value in it. It\'ll never trigger, so you won\'t have to sacrifice the Scarecrow.').

card_ruling('lurking arynx', '2015-02-25', 'You may activate Lurking Arynx’s ability multiple times in a turn to force multiple creatures to block it if able.').
card_ruling('lurking arynx', '2015-02-25', 'Activating Lurking Arynx’s ability doesn’t force you to attack with it that turn. If it doesn’t attack, the creatures affected by its ability can block other attacking creatures or not block at all.').
card_ruling('lurking arynx', '2015-02-25', 'If a creature affected by Lurking Arynx’s ability can’t legally block it (perhaps because Lurking Arynx has gained flying), that creature can block other attacking creatures or not block at all.').
card_ruling('lurking arynx', '2015-02-25', 'If, during the declare blockers step, a creature is tapped or is affected by a spell or ability that says it can’t block, it doesn’t block. If there’s a cost associated with having a creature block, its controller isn’t forced to pay that cost, so it doesn’t have to block in that case either.').
card_ruling('lurking arynx', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('lurking arynx', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('lurking arynx', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('lurking automaton', '2014-05-29', 'For example, if you draft Lurking Automaton as the third card in a draft round and then another one as the sixth card in a draft round, each Lurking Automaton that enters the battlefield under your control does so with six +1/+1 counters on it.').
card_ruling('lurking automaton', '2014-05-29', 'If a Lurking Automaton owned by another player enters the battlefield under your control, the number of counters it enters with will be based on the highest number you noted as you drafted a card named Lurking Automaton, not as its owner did. Unless you also drafted Lurking Automaton, this will be considered 0.').

card_ruling('lurking evil', '2004-10-04', 'If you have zero or negative life, half your life is zero, so it costs nothing to use the ability.').
card_ruling('lurking evil', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('lurking evil', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('lurking jackals', '2004-10-04', 'Once it becomes a creature, it will not change back into an enchantment if your opponent\'s life total goes back above 10.').
card_ruling('lurking jackals', '2004-10-04', 'Once it becomes a creature, it is no longer an enchantment.').
card_ruling('lurking jackals', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('lurking predators', '2009-10-01', 'If an opponent casts a spell, this ability triggers and is put on the stack on top of that spell. This ability will resolve (meaning that a revealed creature card will enter the battlefield) before the spell resolves.').
card_ruling('lurking predators', '2009-10-01', 'You must put the revealed card onto the battlefield if it\'s a creature card. If it\'s not a creature card and you don\'t put it on the bottom of your library, it stops being revealed and simply remains on top of your library.').
card_ruling('lurking predators', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('lurking skirge', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('lush growth', '2008-10-01', 'The enchanted land loses its existing land types and any abilities printed on it. It now has the ability to tap to add {R}, {G}, or {W} to its controller\'s mana pool. Lush Growth doesn\'t change the enchanted land\'s name or whether it\'s legendary or basic.').

card_ruling('lust for war', '2010-06-15', 'Lust for War\'s triggered ability triggers when the enchanted creature becomes tapped for any reason, not just when it attacks.').
card_ruling('lust for war', '2010-06-15', 'If, during its controller\'s declare attackers step, the enchanted creature is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then that creature doesn\'t attack. If there\'s a cost associated with having that creature attack, its controller isn\'t forced to pay that cost, so the creature doesn\'t have to attack in that case either.').
card_ruling('lust for war', '2010-06-15', 'If there are multiple combat phases in a turn, the enchanted creature must attack only in the first one in which it\'s able to.').

card_ruling('lyev decree', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('lyev decree', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('lyev decree', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('lyev decree', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('lyev decree', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('lyev decree', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('lyev skyknight', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('lyev skyknight', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('lyev skyknight', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('lyev skyknight', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('lyev skyknight', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('lyev skyknight', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('lymph sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('lymph sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('lyzolda, the blood witch', '2006-05-01', 'You can sacrifice any creature you control to pay for this ability. If the creature was neither red nor black, the ability has no effect.').
card_ruling('lyzolda, the blood witch', '2006-05-01', 'You must choose a target for the ability, even if the creature you sacrifice isn\'t red. If the target becomes illegal before the ability resolves, the ability will be countered; you won\'t draw a card, even if the creature you sacrificed was black.').

