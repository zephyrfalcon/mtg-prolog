% Rulings

card_ruling('rabid wombat', '2005-08-01', 'If an Aura is removed from the Wombat, its power and toughness change immediately.').

card_ruling('rack and ruin', '2004-10-04', 'Must target two different artifacts.').

card_ruling('radha, heir to keld', '2007-02-01', 'Radha\'s triggered ability is not a mana ability (because it doesn\'t trigger from another mana ability). It goes on the stack and it can be countered.').

card_ruling('radiant essence', '2004-10-04', 'In a multiplayer game, it gets the bonus if at least one opponent controls a black permanent.').

card_ruling('radiant flames', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('radiant flames', '2015-08-25', '').
card_ruling('radiant flames', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('radiant flames', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('radiant flames', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('radiant\'s judgment', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('radjan spirit', '2004-10-04', 'Can be used on a creature without Flying with no effect.').

card_ruling('rafiq of the many', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('rafiq of the many', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('rafiq of the many', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('rafiq of the many', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('rafiq of the many', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('rafiq of the many', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('rage extractor', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('rage extractor', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('rage extractor', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('rage extractor', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('rage extractor', '2011-06-01', 'Rage Extractor\'s ability checks for any spell you cast with any Phyrexian mana symbol in its mana cost, regardless of that symbol\'s color or how the cost was paid.').
card_ruling('rage extractor', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').

card_ruling('rage nimbus', '2010-06-15', 'If, during its controller\'s declare attackers step, a creature affected by Rage Nimbus\'s activated ability is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then that creature doesn\'t attack. If there\'s a cost associated with having that creature attack, its controller isn\'t forced to pay that cost, so the creature doesn\'t have to attack in that case either.').

card_ruling('rage of purphoros', '2013-09-15', 'If Rage of Purphoros resolves but the damage is redirected to a different creature or to a player, the target creature still won’t be able to regenerate that turn. This is also true if the damage is prevented.').
card_ruling('rage of purphoros', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('rage of purphoros', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('rage of purphoros', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('rage of purphoros', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('rage reflection', '2008-05-01', 'If a creature loses double strike after assigning damage in the \"first strike\" combat damage step (due to Rage Reflection leaving the battlefield, for example), that creature won\'t assign damage in the \"normal\" combat damage step.').

card_ruling('rage thrower', '2011-09-22', 'If Rage Thrower dies at the same time as another creature, its ability will trigger.').

card_ruling('rageform', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('rageform', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('rageform', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('rageform', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('rageform', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('rageform', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('rageform', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('rageform', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('rageform', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('rageform', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('rageform', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('rageform', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('rageform', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').
card_ruling('rageform', '2014-11-24', 'You’ll still manifest the top card of your library even if the “Form” isn’t on the battlefield as its enters-the-battlefield ability resolves.').
card_ruling('rageform', '2014-11-24', 'If you have no cards in your library as the ability resolves, the “Form” will be put into its owner’s graveyard as a state-based action.').
card_ruling('rageform', '2014-11-24', 'If the enchanted creature is turned face up, the “Form” will continue to enchant it.').

card_ruling('ragemonger', '2014-02-01', 'If you control more than one Ragemonger, the cost reduction is cumulative.').
card_ruling('ragemonger', '2014-02-01', 'You apply cost reduction effects after other cost modifiers, so Ragemonger can reduce additional costs or alternative costs of Minotaur spells if they include {B} and/or {R}.').
card_ruling('ragemonger', '2014-02-01', 'If a spell has hybrid mana symbols in its mana cost, you choose which half you will be paying before determining the total cost. If you choose to pay such a cost with {B} or {R}, Ragemonger can reduce that part of the cost.').

card_ruling('raging ravine', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('raging ravine', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('raging ravine', '2010-03-01', 'Each time you activate Raging Ravine\'s last ability, it gains an instance of the triggered ability \"Whenever this creature attacks, put a +1/+1 counter on it.\" For example, if you activate the last ability twice and then attack with Raging Ravine, both of the triggered abilities it gained will trigger. It will get a total of two +1/+1 counters.').
card_ruling('raging ravine', '2010-03-01', 'Any +1/+1 counters put on Raging Ravine remain on it even after it stops being a creature. They\'ll have no effect until it becomes a creature again.').

card_ruling('raging river', '2008-05-01', 'If a creature enters the battlefield (or something becomes a creature) after the ability resolves, that creature will not be able to block any creature that was attacking at the time the ability resolved.').
card_ruling('raging river', '2008-05-01', 'If a creature is put onto the battlefield attacking after the ability resolves, it can be blocked by any creature that could normally block it (including others creatures that entered the battlefield after the ability resolved).').

card_ruling('raid bombardment', '2010-06-15', 'The power of the attacking creature is checked only when the ability triggers. Once it triggers, Raid Bombardment will deal 1 damage to the defending player even if the creature\'s power changes before the ability resolves.').
card_ruling('raid bombardment', '2010-06-15', 'If you attack with multiple creatures with power 2 or less, Raid Bombardment\'s ability triggers that many times.').
card_ruling('raid bombardment', '2010-06-15', 'In a multiplayer game, the ability checks who the defending player is for each individual attacking creature with power 2 or less.').
card_ruling('raid bombardment', '2010-06-15', 'If Raid Bombardment\'s ability triggers in a Two-Headed Giant game, the ability\'s controller chooses which one of the defending players is dealt damage. The choice is made as the ability resolves. A different choice may be made for each ability.').

card_ruling('raiders\' spoils', '2014-09-20', 'You decide whether to pay 1 life as the triggered ability resolves. You may do this only once per ability (in other words, once per Warrior that deals combat damage to a player).').

card_ruling('raiding party', '2004-10-04', 'The creatures are tapped during spell resolution.').
card_ruling('raiding party', '2004-10-04', 'The color of creature to be tapped is set when the effect is announced and even if you use Sleight of Mind after that, you can\'t change what color creature needs to be tapped to prevent the destruction.').
card_ruling('raiding party', '2004-10-04', 'A player can choose zero or one Plains if they want.').

card_ruling('rain of filth', '2004-10-04', 'The granted ability can be used while the land is tapped.').
card_ruling('rain of filth', '2004-10-04', 'Only grants the ability to lands you control when this ability resolves.').

card_ruling('rain of gore', '2013-04-15', 'This does not apply to life gain caused by combat damage from a creature with lifelink.').

card_ruling('rain of salt', '2004-10-04', 'You must target two different lands. You can\'t choose to target just one.').

card_ruling('rain of thorns', '2012-05-01', 'You can choose just one mode, any two of the modes, or all three. You make this choice as you cast Rain of Thorns.').

card_ruling('rainbow vale', '2004-10-04', 'Only changes controller at the end of a turn in which it was tapped for mana. It does not change controller if it is not tapped for mana.').
card_ruling('rainbow vale', '2004-10-04', 'Stays in the same tapped/untapped state it is in when it switches controllers.').
card_ruling('rainbow vale', '2007-09-16', 'If control of Rainbow Vale has somehow changed before the \"at end of turn\" ability resolves, the player who activated the mana ability (not the current controller of Rainbow Vale) chooses one of his or her opponents to gain control of it.').
card_ruling('rainbow vale', '2007-09-16', 'If Rainbow Vale\'s ability is activated during an End step, its \"at the beginning of the end step\" trigger won\'t trigger until the End step of the next turn. If its ability is activated again during that turn, its \"at the beginning of the end step\" ability will trigger twice at the End step of that turn.').
card_ruling('rainbow vale', '2007-09-16', 'If more than one player manages to tap it for mana in a given turn, then two control-change abilities will trigger at end of the turn. These abilities resolve using the standard order for such things, so the active player\'s triggers are put on the stack first, then the opponents\'.').
card_ruling('rainbow vale', '2007-09-16', 'The opponent is chosen when the \"at end of turn\" triggered ability resolves.').

card_ruling('raise dead', '2004-10-04', 'You must show the card you bring out of the graveyard to your opponent.').
card_ruling('raise dead', '2004-10-04', 'The creature being brought back is chosen on announcement and not on resolution of the spell because it is targeted.').
card_ruling('raise dead', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('raka sanctuary', '2004-10-04', 'It can deal 1 or 3 damage, but never 4.').

card_ruling('rakalite', '2004-10-04', 'Only returns to your hand if it is still on the battlefield at the end of the turn. If it leaves the battlefield, it does not return.').
card_ruling('rakalite', '2004-10-04', 'Can be used any number of times during the turn before it leaves the battlefield.').
card_ruling('rakalite', '2004-10-04', 'Can choose a different creature or player for each use.').

card_ruling('rakdos cackler', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('rakdos cackler', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('rakdos cackler', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('rakdos cackler', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('rakdos carnarium', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('rakdos charm', '2012-10-01', 'Damage dealt by creatures because of the third mode can\'t be redirected to a planeswalker.').

card_ruling('rakdos drake', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('rakdos drake', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('rakdos drake', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('rakdos drake', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('rakdos guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('rakdos guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('rakdos guildmage', '2006-05-01', 'If the second ability is activated during the End step, the token won\'t be exiled until the following turn\'s End step.').

card_ruling('rakdos keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('rakdos keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('rakdos pit dragon', '2006-05-01', 'If Rakdos Pit Dragon loses double strike after first strike combat damage has been dealt, it won\'t deal damage during the normal combat damage step.').

card_ruling('rakdos the defiler', '2006-05-01', 'To calculate the number of permanents that must be sacrificed, count the number of permanents the affected player controls that don\'t have the subtype Demon, then divide that number in half and round up. For example, a player who controls eleven non-Demon permanents will need to sacrifice six of them.').
card_ruling('rakdos the defiler', '2006-05-01', 'A permanent with the subtype Demon, including Rakdos the Defiler itself, can\'t be sacrificed this way.').

card_ruling('rakdos\'s return', '2012-10-01', 'The target opponent will discard X cards even if some or all of the damage dealt by Rakdos\'s Return is prevented or redirected.').

card_ruling('rakdos, lord of riots', '2012-10-01', 'Remember, damage dealt to a player causes that player to lose that much life (unless the source has infect).').
card_ruling('rakdos, lord of riots', '2012-10-01', 'Rakdos can be put onto the battlefield by another spell or ability even if no opponent has lost life that turn.').
card_ruling('rakdos, lord of riots', '2012-10-01', 'Rakdos\'s last ability cares about the total life lost, not necessarily what an opponent\'s life total is compared to what it was at the beginning of the turn. For example, if an opponent loses 5 life and then gains 10 life in a turn, creature spells you cast will cost {5} less to cast.').
card_ruling('rakdos, lord of riots', '2012-10-01', 'Rakdos\'s last ability can\'t reduce the colored mana requirement of a creature spell you cast.').
card_ruling('rakdos, lord of riots', '2012-10-01', 'If there are additional costs to cast a creature spell, such as a kicker cost or a cost imposed by the rules of a Commander game, apply those increases before applying cost reductions.').
card_ruling('rakdos, lord of riots', '2012-10-01', 'Rakdos\'s last ability won\'t reduce the cost to cast Rakdos itself. It applies only to creature spells you cast once Rakdos is on the battlefield.').

card_ruling('rakeclaw gargantuan', '2008-10-01', 'The ability checks the targeted creature\'s power twice: when the creature becomes the target, and when the ability resolves. Once the ability resolves, it will continue to apply to the affected creature no matter what its power may become later in the turn.').

card_ruling('raksha golden cub', '2004-12-01', 'Raksha Golden Cub is a Cat, so it also gets +2/+2 and double strike if it\'s equipped.').

card_ruling('rakshasa gravecaller', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('rakshasa gravecaller', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('rakshasa gravecaller', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('rakshasa gravecaller', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('rakshasa gravecaller', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('rakshasa vizier', '2014-09-20', 'If a card is exiled “instead” of being put into a graveyard, Rakshasa Vizier’s ability won’t trigger.').
card_ruling('rakshasa vizier', '2014-09-20', 'Exiling cards from your graveyard to pay the cost of a spell with delve is a single action. It will cause Rakshasa Vizier’s ability to trigger once, putting a number of +1/+1 counters on it equal to the number of cards you exiled.').

card_ruling('rakshasa\'s disdain', '2014-11-24', 'Count the number of cards in your graveyard as Rakshasa’s Disdain resolves to determine how much mana the controller of the target spell must pay to avoid the spell being countered. Rakshasa’s Disdain is still on the stack at this time and won’t count toward this number.').

card_ruling('ral zarek', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('ral zarek', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('ral zarek', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('ral zarek', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('ral zarek', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('ral zarek', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('ral zarek', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('ral zarek', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('ral zarek', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('ral zarek', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('ral zarek', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('rally the forces', '2011-06-01', 'Even though Rally the Forces affects only attacking creatures, the bonus will remain for the rest of the turn.').
card_ruling('rally the forces', '2011-06-01', 'If Rally the Forces is cast before attackers are declared or after combat ends, it won\'t do anything.').

card_ruling('rally the peasants', '2011-09-22', 'Only creatures you control when Rally the Peasants resolves will be affected. Creatures that enter the battlefield or that you gain control of later in the turn will not.').

card_ruling('ramosian captain', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('ramosian commander', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('ramosian lieutenant', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('ramosian revivalist', '2007-05-01', 'A \"permanent card\" is an artifact card, a creature card, an enchantment card, a planeswalker card, or a land card. Most \"Rebel permanent cards\" are creature cards.').

card_ruling('ramosian sergeant', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('ramosian sky marshal', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('rampaging baloths', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('rampaging baloths', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('rampant growth', '2004-10-04', 'The land does not count toward your one per turn limit because it was put onto the battlefield by an effect.').
card_ruling('rampant growth', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('ramroller', '2015-06-22', 'If, during its controller’s declare attackers step, Ramroller is tapped or is affected by a spell or ability that says it can’t attack, then Ramroller doesn’t attack. If there’s a cost associated with having it attack, its controller isn’t forced to pay that cost. If he or she doesn’t, Ramroller doesn’t have to attack.').

card_ruling('ramses overdark', '2005-08-01', 'A creature is \"enchanted\" if it has any Auras attached to it.').

card_ruling('rancor', '2012-07-01', 'If the target of Rancor when it\'s cast as a spell is an illegal target when Rancor tries to resolve, Rancor will be countered and put into its owner\'s graveyard. It won\'t return to its owner\'s hand.').

card_ruling('ranger of eos', '2008-10-01', 'You may choose to find zero, one, or two creature cards in your library. Each card you find must have converted mana cost 1 or less.').

card_ruling('rank and file', '2004-10-04', 'The -1/-1 penalty is given to all green creatures on the battlefield when the triggered ability resolves. It will not apply to green creatures that enter the battlefield later in the turn.').

card_ruling('rapid decay', '2004-10-04', 'You pick the cards during announcement. If any are not there on resolution, any others that are there are still affected.').
card_ruling('rapid decay', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rapid fire', '2013-09-20', 'If a turn has multiple combat phases, Rapid Fire can only be cast before the beginning of the Declare Blockers Step of the first combat phase in that turn.').

card_ruling('rapid hybridization', '2013-01-24', 'If the creature is an illegal target when Rapid Hybridization tries to resolve, it will be countered and none of its effects will happen. No Frog Lizard token will be created.').
card_ruling('rapid hybridization', '2013-07-01', 'If Rapid Hybridization resolves and the creature isn\'t destroyed (perhaps because it has indestructible), its controller will still get the Frog Lizard token.').

card_ruling('rashka the slayer', '2004-10-04', 'Gets the bonus only once even if it blocks more than one black creature.').
card_ruling('rashka the slayer', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('rasputin dreamweaver', '2009-10-01', 'The fourth ability has an \"intervening \'if\' clause,\" so it won\'t trigger at all unless Rasputin Dreamweaver started the turn untapped.').
card_ruling('rasputin dreamweaver', '2009-10-01', 'If a number of dream counters would be put on Rasputin Dreamweaver for any reason that would bring the total number of dream counters on it to eight or more, the total number of dream counters on it is brought to seven instead.').
card_ruling('rasputin dreamweaver', '2009-10-01', 'If a permanent with eight or more dream counters on it becomes a copy of Rasputin Dreamweaver (or just gains its last ability), all dream counters except seven are removed from it as a state-based action.').

card_ruling('ratchet bomb', '2011-01-01', 'As Ratchet Bomb\'s last ability resolves, its last existence on the battlefield is checked to determine how many charge counters were on it.').
card_ruling('ratchet bomb', '2011-01-01', 'Ratchet Bomb\'s second ability destroys only those nonland permanents whose converted mana cost is exactly equal to the number of charge counters on Ratchet Bomb. It doesn\'t matter who controls them.').
card_ruling('ratchet bomb', '2011-01-01', 'If Ratchet Bomb\'s second ability is activated while it has no charge counters on it, it will destroy each nonland permanent with converted mana cost 0.').
card_ruling('ratchet bomb', '2011-01-01', 'The converted mana cost of a permanent is determined solely by the mana symbols printed in its upper right corner, unless it\'s copying something else (see below). The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('ratchet bomb', '2011-01-01', 'If a permanent is copying something else, its converted mana cost is the converted mana cost of whatever it\'s copying.').
card_ruling('ratchet bomb', '2011-01-01', 'In all cases, ignore any alternative costs or additional costs (such as kicker) paid when the permanent was cast.').
card_ruling('ratchet bomb', '2011-01-01', 'If the mana cost of a permanent includes {X}, X is considered to be 0.').
card_ruling('ratchet bomb', '2011-01-01', 'If a nonland permanent has no mana symbols in its upper right corner (because it\'s a token that\'s not copying something else, for example), its converted mana cost is 0.').

card_ruling('rathi assassin', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('rathi fiend', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('rathi intimidator', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('rathi trapper', '2007-02-01', 'This is the timeshifted version of Master Decoy.').

card_ruling('rattleblaze scarecrow', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('rattleblaze scarecrow', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('rattleclaw mystic', '2014-09-20', 'Although turning a face-down creature with morph face up doesn’t use the stack, Rattleclaw Mystic’s triggered ability does use the stack, and players can respond to that ability. Notably, you can’t turn Rattleclaw Mystic face up while casting a spell or activating an ability. You’ll need to turn Rattleclaw Mystic face up and have its triggered ability resolve before you can spend that mana.').
card_ruling('rattleclaw mystic', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('rattleclaw mystic', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('rattleclaw mystic', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('rattleclaw mystic', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('rattleclaw mystic', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('rattleclaw mystic', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('rattleclaw mystic', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('rattleclaw mystic', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('rattleclaw mystic', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('ravaging blaze', '2015-06-22', 'Ravaging Blaze targets only the creature. It doesn’t target any player, even if the spell mastery ability applies.').
card_ruling('ravaging blaze', '2015-06-22', 'If the creature becomes an illegal target by the time Ravaging Blaze tries to resolve, Ravaging Blaze will be countered and none of its effects will happen. No damage will be dealt to the creature or its controller.').
card_ruling('ravaging blaze', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('raven familiar', '2004-10-04', 'This is not a draw.').

card_ruling('raven\'s crime', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('raven\'s crime', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('raven\'s crime', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('raven\'s crime', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('raven\'s run', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('raven\'s run', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('raven\'s run', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('raven\'s run', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('raven\'s run', '2009-10-01', 'Wither applies to any damage, not just combat damage.').
card_ruling('raven\'s run', '2009-10-01', 'The -1/-1 counters that result from wither remain on the damaged creature indefinitely. They won\'t be removed if the creature regenerates or the turn ends.').
card_ruling('raven\'s run', '2009-10-01', 'Since damage from a creature with wither is real damage, it follows all the other rules for damage. It can be prevented or redirected. When it\'s dealt, it will cause \"Whenever [this creature] deals damage\" and \"Whenever [this creature] is dealt damage\" abilities to trigger.').
card_ruling('raven\'s run', '2009-10-01', 'If the source of the damage is still on the battlefield at the time that damage is dealt, its characteristics are checked to see if it has wither. If the source has left the battlefield by then, its last existence on the battlefield is checked to see if it had wither.').
card_ruling('raven\'s run', '2009-10-01', 'Damage from creatures with wither affects players and planeswalkers normally.').
card_ruling('raven\'s run', '2009-10-01', 'Multiple instances of wither are redundant.').
card_ruling('raven\'s run', '2009-10-01', 'You must target three different creatures when the {C} ability triggers, even if that means you have to target creatures you control. If you can\'t target three creatures (because there are just two creatures on the battlefield, perhaps), the ability is removed from the stack and does nothing.').

card_ruling('ravenous baboons', '2004-10-04', 'If there is no non-basic land to destroy, the ability does nothing.').
card_ruling('ravenous baboons', '2004-10-04', 'You have to pick one of your own non-basic lands if the opponent does not have any and you do.').

card_ruling('ravenous leucrocota', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('ravenous leucrocota', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('ravenous leucrocota', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('ravenous skirge', '2004-10-04', 'If it attacks more than once per turn, it gets the bonus each time.').

card_ruling('ravenous trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('ravenous trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('ravenous trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').

card_ruling('raving dead', '2014-11-07', 'Raving Dead must attack the chosen player if able, not a planeswalker controlled by that player.').
card_ruling('raving dead', '2014-11-07', 'If, as attackers are declared, Raving Dead is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since your turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having Raving Dead attack the chosen player, you aren’t forced to pay that cost, so it doesn’t have to attack that player in that case either.').
card_ruling('raving dead', '2014-11-07', 'If Raving Dead can’t attack the chosen player for one of the above reasons but it can still attack elsewhere, you may choose to have it attack another player, attack a planeswalker, or not attack at all.').
card_ruling('raving dead', '2014-11-07', 'If your turn has multiple combat phases, Raving Dead’s ability triggers at the beginning of each of them. Ignore any choices made during previous combat phases that turn.').
card_ruling('raving dead', '2014-11-07', 'The last ability triggers and resolves after combat damage has been dealt. For example, if an opponent has 17 life and Raving Dead deals 2 combat damage to him or her, he or she will end up at 8 life.').

card_ruling('ray of command', '2004-10-04', 'You may target an untapped creature with Ray of Command.').
card_ruling('ray of command', '2004-10-04', 'The creature returns to the opponent when the \"until end of turn\" effect wears off during the cleanup step. It taps during the Cleanup step (if it is not already tapped), so any abilities triggered off it tapping happen at that time.').
card_ruling('ray of command', '2008-10-01', 'You tap the creature when you lose control of it for any reason -- because Ray of Command\'s effect ends, or because a spell or ability causes another player to gain control of it.').

card_ruling('rayne, academy chancellor', '2004-10-04', 'If a spell or ability targets multiple permanents you control or targets you and one or more of your permanents, this triggers once for each such target.').
card_ruling('rayne, academy chancellor', '2004-10-04', 'The \"if\" in the card text does not have a special meaning. You check if this card is enchanted when the ability resolves and draw 1 or 2 cards.').
card_ruling('rayne, academy chancellor', '2005-08-01', 'When casting an Aura on this card, the triggered ability will resolve before the Aura enters the battlefield.').

card_ruling('razia\'s purification', '2005-10-01', 'If a player doesn\'t control three permanents, that player chooses all the permanents he or she does control and doesn\'t sacrifice anything.').
card_ruling('razia\'s purification', '2005-10-01', 'Players choose permanents in turn order around the table, then simultaneously sacrifice all permanents not chosen.').

card_ruling('razia, boros archangel', '2005-10-01', 'The 3 damage doesn\'t have to be from the same source, and it doesn\'t have to be all dealt at once. If only 2 damage is redirected, the next 1 damage will also be redirected.').
card_ruling('razia, boros archangel', '2005-10-01', 'If either target creature leaves the battlefield before damage is dealt, that damage won\'t be redirected. It doesn\'t matter if Razia leaves the battlefield.').

card_ruling('razing snidd', '2004-10-04', 'It probably won\'t matter much, but you choose the order in which the two triggered abilities are placed on the stack.').
card_ruling('razing snidd', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('razor boomerang', '2010-03-01', 'The source of the damage is Razor Boomerang, not the equipped creature. However, the equipped creature\'s ability is what targets the creature or player. If Razor Boomerang is equipped to a red creature, for example, the ability couldn\'t target a creature with protection from red. It could target a creature with protection from artifacts, but all the damage would be prevented.').
card_ruling('razor boomerang', '2010-03-01', 'Unattaching Razor Boomerang is a cost to activate the equipped creature\'s ability.').
card_ruling('razor boomerang', '2010-03-01', 'If Razor Boomerang is no longer on the battlefield by time the equipped creature\'s ability resolves, it\'s not returned to its owner\'s hand. The rest of the ability resolves as normal, so Razor Boomerang will still deal damage to the targeted creature.').
card_ruling('razor boomerang', '2010-03-01', 'If the targeted creature is an illegal target by the time the equipped creature\'s ability resolves, the entire ability is countered. Razor Boomerang remains on the battlefield unattached.').

card_ruling('razor hippogriff', '2011-01-01', 'If the artifact card in your graveyard is an illegal target by the time the ability resolves, the ability will be countered. You won\'t gain any life.').
card_ruling('razor hippogriff', '2011-01-01', 'If the mana cost of the targeted card includes {X}, X is considered to be 0.').

card_ruling('razorfin abolisher', '2008-08-01', 'Razorfin Abolisher\'s ability can target a creature with any kind of counter on it, not just a -1/-1 counter.').
card_ruling('razorfin abolisher', '2008-08-01', 'If the targeted creature no longer has any counters on it when the ability resolves, the ability will be countered because its only target is illegal. The creature will stay on the battlefield.').

card_ruling('reach of branches', '2008-04-01', 'The triggered ability will trigger whenever any land with the subtype Forest enters the battlefield under your control, not just one named Forest.').
card_ruling('reach of branches', '2008-04-01', 'If Reach of Branches leaves your graveyard before its triggered ability resolves, the ability will have no effect, even if the card returns to your graveyard before then.').

card_ruling('read the bones', '2013-09-15', 'The loss of life is part of the spell’s effect. It’s not an additional cost. If Read the Bones is countered, you won’t lose life.').
card_ruling('read the bones', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('read the bones', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('read the bones', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('read the bones', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('read the runes', '2004-10-04', 'You are never forced to sacrifice any permanents. You can choose to discard your entire hand if your hand is smaller than the number of cards you are required to discard.').
card_ruling('read the runes', '2004-10-04', 'You draw the cards, then sacrifice any permanents you want to sacrifice, then discard the required number of card from your hand. There is no chance to play any cards in between these actions.').

card_ruling('ready', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('ready', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('ready', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('ready', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('ready', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('ready', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('ready', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('ready', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('ready', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('ready', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('ready', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('ready', '2013-07-01', 'A creature that comes under your control after Ready resolves will not be granted by this effect.').

card_ruling('reality shaping', '2012-06-01', 'First, you may put a permanent card onto the battlefield, then each other player in turn order may do the same. Cards such as Sakashima\'s Student that can enter the battlefield as a copy of another creature could copy a creature put onto the battlefield earlier in the resolution of the ability.').
card_ruling('reality shaping', '2012-06-01', 'Each player will know what permanent cards were put onto the battlefield earlier in the ability\'s resolution.').
card_ruling('reality shaping', '2012-06-01', 'This process is not repeated. Go around the table only once.').
card_ruling('reality shaping', '2012-06-01', 'After all permanents are put onto the battlefield and you planeswalk away from Reality Shaping, any abilities that triggered from those permanents entering the battlefield will be put onto the stack. You\'ll put all of your triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').

card_ruling('reality shift', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('reality shift', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('reality shift', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('reality shift', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('reality shift', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('reality shift', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('reality shift', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('reality shift', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('reality shift', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('reality shift', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('reality shift', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('reality shift', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('reality shift', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('reality spasm', '2010-06-15', 'You choose Reality Spasm\'s mode as you cast it.').
card_ruling('reality spasm', '2010-06-15', 'You may target any permanents with Reality Spasm, regardless of whether they\'re tapped or untapped.').

card_ruling('reality strobe', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('reality strobe', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('reality strobe', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('reality strobe', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('reality strobe', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('reality strobe', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('reality strobe', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('reality strobe', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('reality strobe', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('reality strobe', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('reality twist', '2004-10-04', 'If a player uses a text-changing effect to make a land type be listed as producing two different colors, the player tapping the land for mana can choose to produce mana of either color. But if it produces more than one mana, all mana is of the same color.').
card_ruling('reality twist', '2013-04-15', 'If a land has more than one basic land type, the player tapping the land for mana can choose to produce mana of either color. But if it produces more than one mana, all mana is of the same color.').

card_ruling('realm razer', '2008-10-01', 'If Realm Razer leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and exile all lands forever.').

card_ruling('realm seekers', '2014-05-29', 'If Realm Seekers enters the battlefield directly from a player’s hand without being cast, it counts itself when determining the value of X.').
card_ruling('realm seekers', '2014-05-29', 'The value of X is determined as Realm Seekers enters the battlefield. Players can cast spells and activate abilities while Realm Seekers is on the stack to remove cards from their hands if they wish, and this will result in fewer counters being placed on Realm Seekers as it enters the battlefield.').

card_ruling('realms uncharted', '2010-06-15', 'You may reveal fewer than four land cards if you like. If you reveal one or two cards, those cards are put into your graveyard. If you reveal three cards, an opponent will choose two of those cards and you\'ll put them into your graveyard; the remaining revealed card is put into your hand.').

card_ruling('realmwright', '2013-01-24', 'The basic land types are Plains, Island, Swamp, Mountain, and Forest. Other land types, like Gate, aren\'t basic lands types.').
card_ruling('realmwright', '2013-01-24', 'Lands you control will have the ability to tap for the color of mana corresponding to the chosen type. They\'ll also retain any other abilities they have.').

card_ruling('reanimate', '2004-12-01', 'You lose life equal to the converted mana cost of the card you\'re bringing back, so if you Reanimate Volrath\'s Shapeshifter, you lose three life, regardless of what the next card down is.').

card_ruling('reap', '2008-04-01', 'This card has received errata so that it now targets a specific opponent.').
card_ruling('reap', '2008-04-01', 'The count of the number of black permanents the targeted opponent controls is done during announcement since it only affects target selection. It does not get recounted later.').

card_ruling('reap intellect', '2013-04-15', 'Once Reap Intellect begins to resolve, no player can cast spells or activate abilities until it has completely resolved. For example, the opponent can\'t cast any cards that would be exiled after his or her hand is revealed.').
card_ruling('reap intellect', '2013-04-15', 'You can choose to leave cards with those names in the zone they are in. You don\'t have to exile them.').
card_ruling('reap intellect', '2013-04-15', 'If you don\'t exile any cards from the player\'s hand, you don\'t search that player\'s library.').

card_ruling('reap what is sown', '2014-02-01', 'Reap What Is Sown can’t target the same creature multiple times. It will put only one +1/+1 counter on each of its targets.').

card_ruling('reaper from the abyss', '2011-09-22', 'The morbid ability is mandatory. If you control the only non-Demon creature when the ability triggers, you must choose it as the target.').

card_ruling('reaper king', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('reaper king', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Reaper King is all colors even if you spend ten colorless mana to cast it.').
card_ruling('reaper king', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Reaper King has a converted mana cost of 10, even if you spend {W}{U}{B}{R}{G} to cast it.').
card_ruling('reaper king', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Reaper King by spending one mana of each color, {2} and one mana each of four different colors, {4} and one mana each of three different colors, {6} and one mana each of two different colors, {8} and one mana of any color, or {10}.').

card_ruling('reaper of sheoldred', '2011-06-01', 'The source\'s controller will get one poison counter for each time that source deals damage to Reaper of Sheoldred, not one poison counter for each 1 damage dealt.').

card_ruling('reaper of the wilds', '2013-09-15', 'Reaper of the Wilds’s first ability triggers separately for each creature. For example, if five creatures die at the same time, you’ll scry 1 five times. You won’t scry 5.').
card_ruling('reaper of the wilds', '2013-09-15', 'If Reaper of the Wilds dies at the same time as another creature, its ability will trigger for that other creature.').
card_ruling('reaper of the wilds', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('reaper of the wilds', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('reaper of the wilds', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('reaper of the wilds', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('reaping the graves', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('reaping the graves', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('reaping the graves', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('reaping the graves', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('reaping the graves', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('rebellion of the flamekin', '2013-09-20', 'If you clash because of a spell or ability an opponent controls, the ability will still trigger. Likewise, you can still win the clash even if you weren\'t the player to initiate it.').

card_ruling('reborn hero', '2013-04-15', 'You must already have seven or more cards in your graveyard before Reborn Hero is put into the graveyard in order for its Threshold ability to trigger; you don\'t include Reborn Hero itself in the count.').

card_ruling('rebound', '2008-04-01', 'You can\'t use this on a spell unless the target spell is an Aura or uses the word \"target\" in its text. If it isn\'t an Aura and the word \"target\" is not there, then the spell does not target a player at all—it just affects a player without targeting them.').

card_ruling('rebuff the wicked', '2007-02-01', 'A spell with multiple targets is a legal target for Rebuff the Wicked, as long as at least one of those targets is a permanent you control.').

card_ruling('rebuild', '2004-10-04', 'Remember that cards only affect things which are on the battlefield unless they specifically say otherwise. So this only applies to artifacts on the battlefield.').
card_ruling('rebuild', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rebuking ceremony', '2004-12-01', 'If both cards are owned by the same player, that player decides their order on top of his or her library.').

card_ruling('recall', '2009-10-01', 'You don\'t discard cards until Recall resolves. If you don\'t have X cards in your hand at that time, you discard all the cards in your hand.').
card_ruling('recall', '2009-10-01', 'You don\'t choose which cards in your graveyard you\'ll return to your hand until after you discard cards. You choose a card in your graveyard for each card you discarded, then you put all cards chosen this way into your hand at the same time. You may choose to return some or all of the cards you just discarded.').

card_ruling('recantation', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('reckless abandon', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('reckless abandon', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('reckless brute', '2012-07-01', 'Reckless Brute attacks only if it\'s able to do so as the declare attackers step begins. If, at that time, it\'s tapped, it\'s affected by a spell or ability that says it can\'t attack, then it doesn\'t attack. If there\'s a cost associated with having it attack, you\'re not forced to pay that cost, so it doesn\'t attack in that case either.').
card_ruling('reckless brute', '2012-07-01', 'You choose which player or planeswalker Reckless Brute attacks.').

card_ruling('reckless cohort', '2015-08-25', 'Reckless Cohort can attack if you control another Ally. It just isn’t forced to.').
card_ruling('reckless cohort', '2015-08-25', 'Reckless Cohort checks whether you control another Ally as attacking creatures are declared. At that time, if you don’t, Reckless Cohort attacks if able.').
card_ruling('reckless cohort', '2015-08-25', 'If, during its controller’s declare attackers step, that player doesn’t control another Ally but Reckless Cohort is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under that player’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having Reckless Cohort attack, its controller isn’t forced to pay that cost. If he or she doesn’t, Reckless Cohort doesn’t have to attack.').

card_ruling('reckless imp', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('reckless imp', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('reckless imp', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('reckless imp', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('reckless wurm', '2007-02-01', 'This is the timeshifted version of Arrogant Wurm.').

card_ruling('reclusive artificer', '2015-06-22', 'Count the number of artifacts you control as Reclusive Artificer’s triggered ability resolves to determine how much damage it deals.').
card_ruling('reclusive artificer', '2015-06-22', 'If you are the only player who controls a creature when Reclusive Artificer’s second ability triggers, you must choose one of those creatures as the target, although you can choose to not deal it damage.').

card_ruling('reconnaissance', '2004-10-04', 'Does not undo any effects which triggered on declaration of attackers or blockers.').

card_ruling('recoup', '2004-10-04', 'For split cards, you pay only the cost for the half of the card you are casting. This is true because the cost is not looked at until after the card is on the stack, at which time it only has one of the two costs.').
card_ruling('recoup', '2004-10-04', 'For cards with an X (or Y) in the mana cost, you choose these values at the time you play the spell and your choice is used to set the mana cost that Flashback requires.').
card_ruling('recoup', '2004-10-04', 'If the card already has a Flashback ability, a second Flashback ability is added. The card can be cast using either.').
card_ruling('recoup', '2004-10-04', 'The mana cost for the card is determined at the time you would pay the cost. It is not frozen at an earlier time.').

card_ruling('recross the paths', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('recross the paths', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').
card_ruling('recross the paths', '2008-04-01', 'You put the revealed nonland cards on the bottom of your library before you clash.').
card_ruling('recross the paths', '2008-04-01', 'If you have only nonland cards in your library, you reveal your entire library (one card at a time), rearrange it however you like, and then clash.').

card_ruling('recumbent bliss', '2008-08-01', 'The triggered ability is on the Aura, not the creature. It triggers at the beginning of the upkeep of Recumbent Bliss\'s controller, not the enchanted creature\'s controller, and Recumbent Bliss\'s controller will gain the life.').

card_ruling('recurring insight', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('recurring insight', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('recurring insight', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('recurring insight', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('recurring insight', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('recurring insight', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('recurring insight', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('recurring insight', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').

card_ruling('recurring nightmare', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('recycle', '2007-02-01', 'Your maximum hand size is checked only during the cleanup step of your turn. At any other time, you may have any number of cards in hand.').
card_ruling('recycle', '2007-02-01', 'Countering a spell that has been cast will not prevent you from drawing the card.').
card_ruling('recycle', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Spellbook (an artifact that says you have no maximum hand size) onto the battlefield and then put Recycle onto the battlefield, your maximum hand size will be two. However, if those permanents entered the battlefield in the opposite order, you would have no maximum hand size.').

card_ruling('red mana battery', '2004-10-04', 'Can be tapped even if it has no counters.').

card_ruling('red sun\'s zenith', '2011-06-01', 'If this spell is countered, none of its effects occur. In particular, it will go to the graveyard rather than to its owner\'s library.').

card_ruling('redeem the lost', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('redeem the lost', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').

card_ruling('redirect', '2010-08-15', 'You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('redirect', '2010-08-15', 'If the targeted spell is modal (that is, it says \"Choose one --\" or the like), you can\'t choose a different mode.').
card_ruling('redirect', '2010-08-15', 'If you cast Redirect targeting a spell that targets a spell on the stack (like Cancel does, for example), you can\'t change that spell\'s target to itself. You can, however, change that spell\'s target to Redirect. If you do, that spell will be countered when it tries to resolve because Redirect will have left the stack by then.').
card_ruling('redirect', '2010-08-15', 'Redirect can target any spell, not just an instant or sorcery spell. For example, you could use it to change the target of an Aura spell. However, if the targeted spell has no targets (for example, if it\'s an instant or sorcery spell that doesn\'t specifically use the word \"target,\" or if it\'s a creature spell), Redirect won\'t have any effect on it.').

card_ruling('reduce in stature', '2015-02-25', 'Reduce in Stature overrides all previous effects that set the creature’s power or toughness to specific values. However, effects that set its power or toughness to specific values that start to apply after Reduce in Stature becomes attached to the creature will override this effect.').
card_ruling('reduce in stature', '2015-02-25', 'Effects that modify the power or toughness of the creature, such as the effects of Giant Growth or Honor of the Pure, will apply to it no matter when they started to take effect. The same is true for counters that change the creature’s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('reflecting mirror', '2004-10-04', 'Can\'t be used on abilities of permanents. Those are abilities and not spells.').
card_ruling('reflecting mirror', '2004-10-04', 'This spell only changes the target of a spell, and not the player.').
card_ruling('reflecting mirror', '2004-10-04', 'This can\'t be used on spells which affect \"you\" and which do not say they target you.').

card_ruling('reflecting pool', '2004-10-04', 'Any replacement effects are considered by Reflecting Pool when determining the types of mana a land can produce.').
card_ruling('reflecting pool', '2004-10-04', 'Any change to a land\'s type or splicing of text into a land can affect the types of mana a land can produce.').
card_ruling('reflecting pool', '2008-05-01', 'The types of mana are white, blue, black, red, green, and colorless.').
card_ruling('reflecting pool', '2008-05-01', 'Reflecting Pool checks the effects of all mana-producing abilities of lands you control, but it doesn\'t check their costs. For example, Vivid Crag says \"{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.\" If you control Vivid Crag and Reflecting Pool, you can tap Reflecting Pool for any color of mana. It doesn\'t matter whether Vivid Crag has a charge counter on it, and it doesn\'t matter whether it\'s untapped.').
card_ruling('reflecting pool', '2008-05-01', 'Reflecting Pool doesn\'t care about any restrictions or riders your lands put on the mana they produce, such as Pillar of the Paruns and Hall of the Bandit Lord do. It just cares about types of mana.').
card_ruling('reflecting pool', '2008-05-01', 'Multiple Reflecting Pools won\'t help each other produce mana. If you control a Reflecting Pool, and all other lands you control either lack mana abilities or are other Reflecting Pools, you may still activate Reflecting Pool\'s ability -- it just won\'t produce any mana.').

card_ruling('reflex sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('reflex sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('refraction trap', '2010-03-01', 'Refraction Trap\'s only target is the creature or player it may deal damage to. You choose that target as you cast Refraction Trap, not at the time it prevents damage.').
card_ruling('refraction trap', '2010-03-01', 'Refraction Trap doesn\'t target the source of the damage it prevents. You choose that source as Refraction Trap resolves. If you cast Refraction Trap for {W}, the source you choose doesn\'t have to be the red instant or sorcery spell an opponent cast.').
card_ruling('refraction trap', '2010-03-01', 'If the targeted creature or player is an illegal target by the time Refraction Trap resolves, the entire spell is countered. No damage will be prevented.').
card_ruling('refraction trap', '2010-03-01', 'Refraction Trap can prevent damage that would be dealt to you, one or more creatures you control, and/or one or more planeswalkers you control.').
card_ruling('refraction trap', '2010-03-01', 'If the chosen source would simultaneously deal damage to multiple permanents you control, or would simultaneously deal damage to you and at least one permanent you control, you choose which of that damage to prevent. For example, if the chosen source is Earthquake, you might choose to prevent the next 2 damage it would deal to you and the next 1 damage it would deal to a creature you control. You don\'t decide until the point at which the source would deal its damage.').
card_ruling('refraction trap', '2010-03-01', 'Refraction Trap\'s effect is not a redirection effect. If it prevents damage, Refraction Trap (not the chosen source) deals damage to the targeted creature or player as part of that prevention effect. Refraction Trap is the source of the new damage, so the characteristics of the original source (such as its color, or whether it had lifelink or deathtouch) don\'t affect this damage. The new damage is not combat damage, even if the prevented damage was. Since you control the source of the new damage, if you targeted an opponent with Refraction Trap, you may have Refraction Trap deal its damage to a planeswalker that opponent controls.').
card_ruling('refraction trap', '2010-03-01', 'If the chosen source would deal damage, Refraction Trap prevents 3 of that source\'s damage and the source deals its excess damage (if any) at the same time. Immediately afterward, as part of that same prevention effect, Refraction Trap deals its damage. This happens before state-based actions are checked, and before the spell or ability that caused damage to be dealt resumes its resolution.').
card_ruling('refraction trap', '2010-03-01', 'Whether the targeted creature or player is still a legal target is no longer checked after Refraction Trap resolves. For example, if a creature targeted by Refraction Trap gains shroud after Refraction Trap resolves but before it prevents damage, Refraction Trap will still prevent damage and still deal damage to that creature. If Refraction Trap can\'t deal damage to the targeted creature or player (because the creature has gained protection from white, is no longer on the battlefield, or is no longer a creature, or the player is no longer in the game, for example), it will still prevent damage. It just won\'t deal any damage itself.').
card_ruling('refraction trap', '2010-03-01', 'If Refraction Trap doesn\'t prevent any damage (perhaps because a different prevention effect is applied to the damage the source would deal, or because the damage is unpreventable), Refraction Trap won\'t deal any damage itself.').

card_ruling('reign of the pit', '2014-05-29', 'You choose which creature you are sacrificing first, then each other player in turn order does the same. The creatures are all sacrificed simultaneously.').
card_ruling('reign of the pit', '2014-05-29', 'Use the powers of the creatures as they last existed on the battlefield to determine the value of X, even if they were negative. For example, if the sacrificed creatures were 3/3, 5/5, 0/4, and -2/2, the Demon will be 6/6.').

card_ruling('reincarnation', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('reincarnation', '2009-10-01', 'All Reincarnation does when it resolves is set up a delayed triggered ability. If the targeted creature isn\'t put into a graveyard this turn, that ability never triggers. If the targeted creature is put into a graveyard this turn, that ability triggers and resolves like any other triggered ability.').
card_ruling('reincarnation', '2009-10-01', 'The creature card that will be returned to the battlefield isn\'t chosen until the delayed triggered ability resolves. The player who chooses is the player who controlled Reincarnation. The player under whose control the chosen creature card enters the battlefield is the player who owned the targeted creature (in other words, the player into whose graveyard it went), not necessarily the controller of that creature.').
card_ruling('reincarnation', '2009-10-01', 'The creature that\'s returned to the battlefield may be the same one that was put into the graveyard (assuming it\'s still in the graveyard when the delayed triggered ability resolves). If so, it returns to the battlefield as a brand-new creature.').

card_ruling('reinforcements', '2004-10-04', 'You have to show which creature cards you put on top of your library, but not the order you put them there.').

card_ruling('reins of power', '2004-10-04', 'It is perfectly legal for either or both players to have zero creatures. They give control of all their zero creatures as instructed.').
card_ruling('reins of power', '2007-05-01', 'You only untap creatures controlled by you and the targeted opponent, not all creatures.').

card_ruling('reins of the vinesteed', '2008-04-01', 'Reins of the Vinesteed\'s last ability triggers if it and the creature it\'s enchanting are both put into the graveyard at the same time, or if the creature it\'s enchanting is put into the graveyard but Reins of the Vinesteed isn\'t. (In the second case, Reins of the Vinesteed is then put into the graveyard as a state-based action.)').
card_ruling('reins of the vinesteed', '2008-04-01', 'As the triggered ability resolves, check which creatures on the battlefield have any of the creature types that the previously enchanted creature had as it left the battlefield. If that creature was a Goblin Rogue, for example, Reins of the Vinesteed can be returned to the battlefield attached to either a Goblin creature or a Rogue creature.').
card_ruling('reins of the vinesteed', '2008-04-01', 'This triggered ability doesn\'t target a creature. You may return Reins of the Vinesteed to the battlefield attached to a creature that has shroud, for example. However, the creature must be able to be legally enchanted by Reins of the Vinesteed. You can\'t return Reins of the Vinesteed to the battlefield attached to a creature that has protection from green, for example.').
card_ruling('reins of the vinesteed', '2008-04-01', 'You can choose to return Reins of the Vinesteed to the battlefield attached to a creature another player controls. You still control Reins of the Vinesteed. When that creature is put into a graveyard, the ability of Reins of the Vinesteed will trigger again and you\'ll be able choose another creature for the Aura to enchant.').

card_ruling('reiver demon', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('rejuvenate', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('reki, the history of kamigawa', '2005-06-01', 'Legendary spells are spells of any type that have the supertype legendary.').
card_ruling('reki, the history of kamigawa', '2005-06-01', 'Lands aren\'t spells.').

card_ruling('rekindled flame', '2008-08-01', 'Rekindled Flame\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless one of your opponents already has no cards in hand as your upkeep begins, and (2) the ability will do nothing if each of your opponents manages to get a card in hand by the time it resolves.').
card_ruling('rekindled flame', '2008-08-01', 'You don\'t specify an opponent; the ability will check all of them to see if at least one meets the condition. The opponent that meets the condition when the ability triggers and the one who meets the condition when it resolves can be different players.').

card_ruling('reknit', '2008-05-01', 'Reknit can regenerate an artifact, creature, enchantment, land, or planeswalker. If you regenerate a noncreature permanent, the next time that permanent would be destroyed that turn, instead tap it. If that permanent had any damage on it (because it had been a creature earlier in the turn), that damage is removed.').
card_ruling('reknit', '2008-05-01', 'Regenerating a planeswalker will prevent it from being destroyed as a result of a \"destroy\" effect (like the one from Reaper King). However, it won\'t prevent that planeswalker from being put into its owner\'s graveyard when its loyalty becomes 0.').

card_ruling('release', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('release', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('release', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('release', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('release', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('release', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('release', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('release', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('release', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('release', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('release', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('release', '2013-04-15', 'To resolve Release, the active player chooses a permanent he or she controls for each of the listed types, then each other player in turn order does the same, then all the chosen permanents are sacrificed simultaneously.').
card_ruling('release', '2013-04-15', 'If you cast Catch/Release as a fused split spell, you can sacrifice the permanent you just gained control of.').
card_ruling('release', '2013-04-15', 'If you control an artifact creature, a nonartifact creature, and no other artifacts or creatures, you must sacrifice both permanents. You must choose the artifact creature as the artifact and the nonartifact creature as the creature. The same is true for any combination of types—you sacrifice a number of permanents as close to five as possible.').

card_ruling('release the ants', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('release the ants', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').

card_ruling('relentless assault', '2004-10-04', 'If you manage to cast this during a main phase of your opponent\'s turn, that opponent\'s creatures will untap and that opponent will be able to attack again. It will not allow you to attack during their turn.').
card_ruling('relentless assault', '2004-10-04', 'Abilities that trigger when a creature attacks, blocks, or is blocked will trigger during each combat that turn. Thus, a creature with Rampage will get the bonus added during each attack.').
card_ruling('relentless assault', '2004-10-04', 'A creature that must attack each turn, must only attack once that turn. It is not forced into each attack that turn.').
card_ruling('relentless assault', '2004-10-04', 'It only creates an additional combat and main phase if it resolves during a main phase.').

card_ruling('relentless rats', '2004-12-01', 'Yes, you\'re allowed to have a deck consisting of sixty Relentless Rats and nothing else.').
card_ruling('relentless rats', '2004-12-01', 'Relentless Rats\'s last ability overrides the normal limit of four of an individual card in a Constructed deck.').

card_ruling('relentless skaabs', '2011-01-22', 'When Relentless Skaabs returns to the battlefield because of its undying ability, it\'s not being cast. You won\'t exile a creature card from your graveyard.').

card_ruling('relic barrier', '2004-10-04', 'Can target a tapped artifact.').

card_ruling('relic of progenitus', '2008-10-01', 'If you activate the first ability, the targeted player chooses which card to exile. The choice is made as the ability resolves.').
card_ruling('relic of progenitus', '2008-10-01', 'You can activate the second ability even if no players have any cards in their graveyards. You\'ll still draw a card.').

card_ruling('relic putrescence', '2011-01-01', 'Relic Putrescence may target and may enchant an artifact that\'s already tapped. It won\'t do anything until the enchanted artifact changes from being untapped to being tapped.').
card_ruling('relic putrescence', '2011-01-01', 'When the enchanted artifact becomes tapped, Relic Putrescence\'s ability triggers. The player who gets the poison counter is the player who, at the time the ability resolves, controls the artifact that became tapped. If that artifact is no longer on the battlefield, its last existence on the battlefield is checked to determine its controller. It doesn\'t matter whether Relic Putrescence is still on the battlefield as the ability resolves, what artifact it\'s enchanting at that time, who controlled the artifact at the time it became tapped, or who tapped it.').
card_ruling('relic putrescence', '2011-01-01', 'If the enchanted artifact is tapped as a cost to activate a mana ability, the mana ability resolves immediately, then Relic Putrescence\'s ability goes on the stack.').
card_ruling('relic putrescence', '2011-01-01', 'If the enchanted artifact is tapped as a cost to activate an ability that\'s not a mana ability, Relic Putrescence\'s ability will go on the stack on top of that activated ability and resolve first.').

card_ruling('relic seeker', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('relic seeker', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('relic seeker', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('relic ward', '2005-08-01', 'Does not destroy Auras which are on the artifact, and it does not prevent Auras from being moved onto the artifact.').
card_ruling('relic ward', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('reliquary tower', '2009-02-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Reliquary Tower onto the battlefield, you\'ll have no maximum hand size. However, if those permanents enter the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('remand', '2005-10-01', 'The countered spell goes to its owner\'s hand from the stack. It never hits the graveyard.').
card_ruling('remand', '2007-02-01', 'If you Remand a card with Flashback, the card will still be exiled.').

card_ruling('remedy', '2004-10-04', 'You choose the targets and you choose how to divide up the 5 damage on announcement.').
card_ruling('remedy', '2004-10-04', 'You can\'t choose zero targets. You must choose at least one target.').

card_ruling('remember the fallen', '2011-06-01', 'You choose which mode you\'re using -- or whether to use both modes -- as you\'re casting the spell. Once this choice is made, it can\'t be changed later while the spell is on the stack.').
card_ruling('remember the fallen', '2011-06-01', 'If you choose both modes, and only one of the targeted cards is still a legal target when Remember the Fallen tries to resolve, only that card is returned to your hand.').

card_ruling('remembrance', '2004-10-04', 'If a copy card goes to the graveyard under this effect, you get to look for another copy of the card it was copying. This is because a copy card actually takes on the name and initial characteristics of what it copies.').
card_ruling('remembrance', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('remembrance', '2010-03-01', 'If a card which is a creature only due to an effect goes to the graveyard, you can search for another copy of that card even though the card in your library won\'t be a creature card.').

card_ruling('reminisce', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('remote isle', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('remove soul', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('rendclaw trow', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('rendclaw trow', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('rendclaw trow', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('rendclaw trow', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('rendclaw trow', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('rendclaw trow', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('rendclaw trow', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('render silent', '2013-04-15', 'You must be able to target a spell to cast Render Silent.').
card_ruling('render silent', '2013-04-15', 'Render Silent doesn\'t stop any player from casting spells in response to Render Silent before it resolves.').
card_ruling('render silent', '2013-04-15', 'The affected player can still activate abilities, including abilities of cards in his or her hand (such as bloodrush abilities), and the player can still play lands.').
card_ruling('render silent', '2013-04-15', 'If the spell is an illegal target when Render Silent tries to resolve (because it\'s been countered by another spell, for example), Render Silent will be countered and none of its effects will happen.').
card_ruling('render silent', '2013-04-15', 'A spell that “can\'t be countered” is a legal target for Render Silent. The spell won\'t be countered when Render Silent resolves, but that spell\'s controller won\'t be able to cast spells for the rest of the turn.').

card_ruling('renegade doppelganger', '2010-06-15', 'When a creature enters the battlefield under your control, you can choose not to copy it. In that case, Renegade Doppelganger remains 0/1 and still has its triggered ability.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger becomes a copy of another creature, that doesn\'t count as having a creature enter the battlefield. Renegade Doppelganger was already on the battlefield; it only changed its characteristics. If Renegade Doppelganger gains any enters-the-battlefield triggered abilities, they won\'t do anything. The same is true of any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger becomes a copy of another creature, it copies exactly what was printed on that card and nothing more (unless that card is copying something else or it\'s a token; see below). It doesn\'t copy whether the original creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or whether it\'s been affected by any noncopy effects that changed its power, toughness, types, color, or so on.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger becomes a copy of a creature that is itself copying something else (for example, if the creature that entered the battlefield is a Clone), then Renegade Doppelganger becomes a copy of whatever that creature is copying.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger becomes a copy of a token, Renegade Doppelganger copies the original characteristics of that token as stated by the effect that put it onto the battlefield. Renegade Doppelganger doesn\'t become a token.').
card_ruling('renegade doppelganger', '2010-06-15', 'Noncopy effects that have already applied to Renegade Doppelganger will continue to apply to it after it becomes a copy of something else. For example, if Giant Growth had given it +3/+3 earlier in the turn, then it becomes a copy of Runeclaw Bear (a 2/2 creature), it will be a 5/5 Runeclaw Bear.').
card_ruling('renegade doppelganger', '2010-06-15', 'During the cleanup step, Renegade Doppelganger\'s copy effect wears off. It will go back to being a 0/1 blue creature named Renegade Doppelganger, and will have its original ability again. Note that this happens at the same time that damage marked on the Renegade Doppelganger is removed. If it was dealt damage earlier in the turn, this damage will not cause Renegade Doppelganger to be destroyed when its copy effect wears off.').
card_ruling('renegade doppelganger', '2010-06-15', 'If a creature enters the battlefield under your control and Renegade Doppelganger\'s ability triggers, it can become a copy of that creature when the ability resolves even if that creature has left the battlefield by then. If it has, its last existence on the battlefield is checked to see what it was (specifically, if it was itself or if it was copying something else).').
card_ruling('renegade doppelganger', '2010-06-15', 'Renegade Doppelganger\'s ability isn\'t targeted. It can copy a creature with shroud or protection.').
card_ruling('renegade doppelganger', '2010-06-15', 'If the creature Renegade Doppelganger copies has an ability that triggers at the beginning of the end step, that ability will trigger and resolve before Renegade Doppelganger\'s copy effect wears off.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger becomes a copy of a creature with an ability that causes you to make a choice (such as naming a card or choosing a color) as it enters the battlefield, and another ability that refers to that choice, those abilities of the Doppelganger won\'t do anything. You won\'t have the chance to make a choice since the Doppelganger is already on the battlefield, so the value of that choice is undefined.').
card_ruling('renegade doppelganger', '2010-06-15', 'If Renegade Doppelganger and another creature enter the battlefield under your control at the same time, Renegade Doppelganger\'s ability will trigger. When the ability resolves, Renegade Doppelganger can become a copy of that creature.').
card_ruling('renegade doppelganger', '2010-06-15', 'If multiple creatures enter the battlefield under your control at the same time, Renegade Doppelganger\'s ability will trigger once for each of them. You choose the order that those abilities will resolve. It\'s possible to have Renegade Doppelganger become a copy of something, activate an activated ability of that creature, then have it become a copy of something else under this scenario. The last ability that resolves that you choose to use determines what it ends up a copy of.').
card_ruling('renegade doppelganger', '2013-07-01', 'If Renegade Doppelganger becomes a copy of a legendary creature, you will have to put one of them into the graveyard as a state-based action.').

card_ruling('renegade krasis', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('renegade krasis', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('renegade krasis', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('renegade krasis', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('renegade krasis', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('renegade krasis', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').
card_ruling('renegade krasis', '2013-04-15', 'Renegade Krasis “evolves” when its evolve ability resolves and a +1/+1 counter is put on it. If a replacement effect such as Doubling Season\'s causes the evolve ability to put more than one +1/+1 counter on Renegade Krasis, its last ability triggers only once. If no +1/+1 counter is put on it (perhaps because it left the battlefield in response to its evolve ability triggering), then its last ability doesn\'t trigger.').
card_ruling('renegade krasis', '2013-04-15', 'Renegade Krasis\'s last ability won\'t trigger if a +1/+1 counter is put on it for any reason other than its evolve ability resolving.').

card_ruling('renewal', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('renewed faith', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('renewed faith', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('renewed faith', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('renounce', '2004-10-04', 'You choose what to sacrifice on resolution.').

card_ruling('renounce the guilds', '2013-04-15', 'The active player (the player whose turn it is) chooses which multicolored permanent will be sacrificed, then each other player in turn order does the same. Then all chosen permanents are sacrificed at the same time.').
card_ruling('renounce the guilds', '2013-04-15', 'Renounce the Guilds doesn\'t target any player and may be cast even if a player doesn\'t control a multicolored permanent.').

card_ruling('renowned weaponsmith', '2014-11-24', 'You can use mana generated by the first ability to pay an alternative cost or an additional cost to cast an artifact spell. It’s not limited to paying just that spell’s mana cost.').
card_ruling('renowned weaponsmith', '2014-11-24', 'An activated ability appears in the form “Cost: Effect.” Some keywords, including equip, are activated abilities.').
card_ruling('renowned weaponsmith', '2014-11-24', 'Notably, turning a face-down creature face up isn’t an activated ability. If you manifest an artifact creature card or cast an artifact card face down using the morph ability, you can’t use mana generated by the first ability to turn it face up.').
card_ruling('renowned weaponsmith', '2014-11-24', 'The mana generated by the first ability can’t be spent to activate abilities of artifact sources that aren’t on the battlefield.').
card_ruling('renowned weaponsmith', '2014-11-24', 'Heart-Piercer Bow is a card from the Khans of Tarkir set. Vial of Dragonfire doesn’t appear to have been invented yet. Curious.').

card_ruling('renowned weaver', '2014-04-26', 'If Renowned Weaver is attacking or blocking, and you activate its ability before combat damage is assigned and dealt, it won’t deal combat damage. If it was blocking a creature, that creature will remain blocked. If you don’t activate the ability before the combat damage step, it must survive combat for its ability to be activated later.').

card_ruling('repay in kind', '2010-06-15', 'For a player\'s life total to become a certain number that\'s lower than his or her current life total, what actually happens is that the player loses the appropriate amount of life. For example, if the lowest life total in the game is 5 and another player has 12 life, Repay in Kind will cause that player to lose 7 life. Abilities that interact with life loss will interact with this effect accordingly.').
card_ruling('repay in kind', '2010-06-15', 'In a Two-Headed Giant game, anything that cares about a player\'s life total checks the life total of that player\'s team. In addition, if an effect would cause the life total of each member of a team to become a certain number, that team chooses one of its members. On that team, only that player is affected, then the life total change is applied to the entire team. (These are changes from previous rules.) For example, if one team has 13 life and another team has 21 life, the team with the higher life total chooses one of its members and Repay in Kind causes that player to lose 8 life, thus causing that team\'s life total to also be 13.').

card_ruling('repel intruders', '2008-05-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('repel intruders', '2008-05-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('repel intruders', '2008-05-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('repel intruders', '2008-05-01', 'When you cast the spell, you choose its targets before you pay for it.').
card_ruling('repel intruders', '2008-05-01', 'You choose whether or not to target a creature spell before you decide whether or not to spend {U} on Repel Intruders. You may target a creature spell and then not spend {U} to cast Repel Intruders; if you do, Repel Intruders simply won\'t affect that creature spell. Similarly, if you don\'t target a creature spell but then you spend {U} to cast Repel Intruders, that part of Repel Intruders has no effect.').
card_ruling('repel intruders', '2008-05-01', 'If you choose to target a creature spell, Repel Intruders has a target. If that target becomes illegal by the time Repel Intruders resolves, the entire spell will be countered. You won\'t get any tokens.').

card_ruling('repel the darkness', '2010-06-15', 'You may target zero, one, or two creatures. The creatures don\'t need to be untapped.').
card_ruling('repel the darkness', '2010-06-15', 'If you target zero creatures, Repel the Darkness can\'t be countered for having no legal targets. When it resolves, all that happens is that you draw a card.').
card_ruling('repel the darkness', '2010-06-15', 'If you target one creature and that target is illegal as Repel the Darkness resolves, the spell is countered. You don\'t draw a card.').
card_ruling('repel the darkness', '2010-06-15', 'If you target two creatures and they\'re both illegal as Repel the Darkness resolves, the spell is countered; you don\'t draw a card. If just one is illegal, the spell does resolve; the remaining legal target becomes tapped (if it\'s untapped at that time) and you draw a card.').

card_ruling('repentant vampire', '2004-10-04', 'If Threshold is met, this card is white. It is not white and black.').

card_ruling('replenish', '2004-10-04', 'It determines what enchantments to return when its resolution starts. If more enchantments go to the graveyard during the resolution, those additional enchantments are ignored.').
card_ruling('replenish', '2005-08-01', 'You must return Auras if possible, even if this means enchanting an opponent\'s permanent with a good enchantment.').
card_ruling('replenish', '2005-08-01', 'Auras can only be placed on permanents that were on the battlefield before this effect started to resolve. You can\'t put an enchantment onto the battlefield with Replenish and put an Aura that is also entering the battlefield onto one of those enchantments.').

card_ruling('repopulate', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('repopulate', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('reprocess', '2004-10-04', 'You can sacrifice zero permanents if you want to.').

card_ruling('reroute', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('rescind', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rescue from the underworld', '2013-09-15', 'Once you announce you’re casting Rescue from the Underworld, no player may attempt to stop you from casting the spell by removing the creature you want to sacrifice.').
card_ruling('rescue from the underworld', '2013-09-15', 'If you sacrifice a creature token to cast Rescue from the Underworld, it won’t return to the battlefield, although the target creature card will.').
card_ruling('rescue from the underworld', '2013-09-15', 'If either the sacrificed creature or the target creature card leaves the graveyard before the delayed triggered ability resolves during your next upkeep, it won’t return.').
card_ruling('rescue from the underworld', '2013-09-15', 'However, if the sacrificed creature is put into another public zone instead of the graveyard, perhaps because it’s your commander or because of another replacement effect, it will return to the battlefield from the zone it went to.').
card_ruling('rescue from the underworld', '2013-09-15', 'Rescue from the Underworld is exiled as it resolves, not later as its delayed trigger resolves.').

card_ruling('research', '2009-10-01', 'A card \"from outside the game\" may be a card from your collection or a card from your sideboard. In tournament play, you can\'t choose a card from your collection. The cards you choose don\'t all have to come from the same place.').
card_ruling('research', '2009-10-01', 'The exile zone is a part of the game, so you can\'t get exiled cards.').

card_ruling('research the deep', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('research the deep', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').

card_ruling('resolute archangel', '2014-07-18', 'Your starting life total is the life total you began the game with. For most two-player formats, this is 20. For Two-Headed Giant, it’s the life total your team started with, usually 30. In Commander games, your starting life total is 40.').
card_ruling('resolute archangel', '2014-07-18', 'If your life total is less than your starting life total, you actually gain the appropriate amount of life. Other effects that interact with life gain will interact with Resolute Archangel’s ability accordingly.').

card_ruling('resolute blademaster', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('resolute blademaster', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('resounding roar', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('resounding roar', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('resounding roar', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('resounding roar', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('resounding scream', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('resounding scream', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('resounding scream', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('resounding scream', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('resounding silence', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('resounding silence', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('resounding silence', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('resounding silence', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('resounding thunder', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('resounding thunder', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('resounding thunder', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('resounding thunder', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('resounding wave', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('resounding wave', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('resounding wave', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('resounding wave', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('rest for the weary', '2010-03-01', 'Whether you had a land enter the battlefield under your control this turn is checked as this spell resolves, not as you cast it.').
card_ruling('rest for the weary', '2010-03-01', 'Having more than one land enter the battlefield under your control this turn provides no additional benefit.').
card_ruling('rest for the weary', '2010-03-01', 'The landfall ability checks for an action that has happened in the past. It doesn\'t matter if a land that entered the battlefield under your control previously in the turn is still on the battlefield, is still under your control, or is still a land.').
card_ruling('rest for the weary', '2010-03-01', 'Once the spell resolves, having a land enter the battlefield under your control provides no further benefit.').
card_ruling('rest for the weary', '2010-03-01', 'The effect of this spell’s landfall ability replaces its normal effect. If you had a land enter the battlefield under your control this turn, only the landfall-based effect happens.').

card_ruling('rest in peace', '2012-10-01', 'While Rest in Peace is on the battlefield, abilities that trigger whenever a creature dies won\'t trigger because cards and tokens never reach a player\'s graveyard.').
card_ruling('rest in peace', '2012-10-01', 'If Rest in Peace is destroyed by a spell, Rest in Peace will be exiled and then the spell will be put into its owner\'s graveyard.').
card_ruling('rest in peace', '2012-10-01', 'If a card is discarded while Rest in Peace is on the battlefield, abilities that function when a card is discarded (such as madness) still work, even though that card never reaches a graveyard. In addition, spells or abilities that check the characteristics of a discarded card (such as Chandra Ablaze\'s first ability) can find that card in exile.').

card_ruling('restless apparition', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('restless apparition', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('restless apparition', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('restless apparition', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('restless apparition', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('restless apparition', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('restless apparition', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('restore balance', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('restore balance', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('restore balance', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('restore balance', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('restore balance', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('restore balance', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('restore balance', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('restore balance', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('restore balance', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('restore balance', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('restore balance', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('restore balance', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('restore balance', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('restore the peace', '2013-04-15', 'Restore the Peace has no effect on the damage that was dealt.').
card_ruling('restore the peace', '2013-04-15', 'Only creatures on the battlefield will be returned. If a creature dealt damage and then died, the creature card won\'t be returned from the graveyard to its owner\'s hand.').

card_ruling('resuscitate', '2004-10-04', 'Only grants the ability to creatures you control when this spell resolves. Ones entering the battlefield later in the turn do not get it.').

card_ruling('retaliator griffin', '2009-05-01', 'The source of combat damage is the creature that dealt it.').
card_ruling('retaliator griffin', '2009-05-01', 'If a spell causes damage to be dealt, that spell will always identify the source of the damage. In most cases, the source is the spell itself. For example, Breath of Malfegor says \"Breath of Malfegor deals 5 damage to each opponent.\"').
card_ruling('retaliator griffin', '2009-05-01', 'If an ability causes damage to be dealt, that ability will always identify the source of the damage. The ability itself is never the source. However, the source of the ability is often the source of the damage. For example, Deathbringer Thoctar\'s ability says \"Deathbringer Thoctar deals 1 damage to target creature or player.\"').
card_ruling('retaliator griffin', '2009-05-01', 'If the source of the damage is a permanent, Retaliator Griffin checks whether an opponent controls that permanent at the time that damage is dealt. If the permanent has left the battlefield by then, its last known information is used. If the source of the damage is a spell, whether it\'s controlled by an opponent is obvious. If the source of the damage is a card in some other zone (such as a cycled Jund Sojourners), Retaliator Griffin checks whether the card\'s owner, rather than its controller, is an opponent.').
card_ruling('retaliator griffin', '2009-05-01', 'If Retaliator Griffin is dealt lethal damage at the same time that you\'re dealt damage by a source an opponent controls, Retaliator Griffin will be put into a graveyard before it would receive any counters. Its ability will still trigger, but it will do nothing when it resolves.').

card_ruling('retether', '2007-02-01', 'All the Auras return to the battlefield simultaneously. Whether an Aura can be attached to a creature is checked before any of them are returned and doesn\'t take into account any simultaneously returning Auras. For example, if Tattoo Ward (which gives enchanted creature protection from enchantments) and Holy Strength are in your graveyard and there\'s only one creature on the battlefield, both Auras are returned to the battlefield attached to that creature, then Holy Strength is put into your graveyard the next time state-based actions are checked.').
card_ruling('retether', '2007-02-01', 'Auras don\'t need to say \"enchant creature\" to return to the battlefield. For example, an Aura with \"enchant land\" will return to the battlefield if there\'s an animated land for it to enchant.').

card_ruling('rethink', '2004-10-04', 'The spell\'s controller gets the option to pay when this spell resolves.').

card_ruling('retreat to coralhelm', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('retreat to coralhelm', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('retreat to coralhelm', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('retreat to emeria', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('retreat to emeria', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('retreat to emeria', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('retreat to hagra', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('retreat to hagra', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('retreat to hagra', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('retreat to kazandu', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('retreat to kazandu', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('retreat to kazandu', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('retreat to valakut', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('retreat to valakut', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('retreat to valakut', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('retribution', '2008-08-01', 'The player chooses which one to sacrifice as Retribution resolves. If only one of the targets is still legal at that time, then the player must choose that one to sacrifice.').

card_ruling('retribution of the ancients', '2014-09-20', 'Once you announce that you are activating the ability of Retribution of the Ancients, no player may take any actions before you pay its costs. Notably, opponents can’t try to remove a creature with counters on it to stop you from paying.').

card_ruling('return to dust', '2006-09-25', 'Regardless of when Return to Dust is cast, its controller may choose one target or two targets. It can always be cast even if there\'s only one legal target. If it\'s cast at a time other than its controller\'s main phase and a second target is chosen, nothing will happen to that target.').

card_ruling('return to the ranks', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('return to the ranks', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('return to the ranks', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('return to the ranks', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('return to the ranks', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('return to the ranks', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('return to the ranks', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('revealing wind', '2015-02-25', 'You can cast Revealing Wind even if there are no face-down creatures attacking or blocking. The damage-prevention effect will still apply.').

card_ruling('reveillark', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('reveillark', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('reveillark', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('reveillark', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('reveillark', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('reveillark', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').
card_ruling('reveillark', '2008-04-01', 'Reveillark\'s ability may target zero, one, or two creature cards in your graveyard. Each target must have power 2 or less.').

card_ruling('reveka, wizard savant', '2009-10-01', 'Reveka\'s effect causes it not to untap during _your_ next untap step. If another player gains control of it, it will untap during that player\'s next untap step.').

card_ruling('revelation', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('revenant', '2015-06-22', 'The ability that defines Revenant’s power and toughness applies in all zones, not just the battlefield. If Revenant is in your graveyard, its ability will count itself.').

card_ruling('reverberate', '2010-08-15', 'Reverberate can target (and copy) any instant or sorcery spell, not just one with targets. It doesn\'t matter who controls it.').
card_ruling('reverberate', '2010-08-15', 'When Reverberate resolves, it creates a copy of a spell. You control the copy. That copy is created on the stack, so it\'s not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('reverberate', '2010-08-15', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('reverberate', '2010-08-15', 'If the spell Reverberate copies is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('reverberate', '2010-08-15', 'If the spell Reverberate copies has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('reverberate', '2010-08-15', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if a player sacrifices a 3/3 creature to cast Fling, and you copy it with Reverberate, the copy of Fling will also deal 3 damage to its target.').
card_ruling('reverberate', '2010-08-15', 'If the copy says that it affects \"you,\" it affects the controller of the copy, not the controller of the original spell. Similarly, if the copy says that it affects an \"opponent,\" it affects an opponent of the copy\'s controller, not an opponent of the original spell\'s controller.').

card_ruling('reverberation', '2004-10-04', 'If used on a sorcery which does damage to multiple creatures and/or players, it will cause that player to take damage equal to the sum of all damage inflicted by the spell.').

card_ruling('revered dead', '2007-02-01', 'This is the timeshifted version of Drudge Skeletons.').

card_ruling('revered unicorn', '2008-04-01', 'Revered Unicorn\'s leaves-the-battlefield ability triggers no matter how it leaves the battlefield, not just when you don\'t pay its cumulative upkeep.').
card_ruling('revered unicorn', '2008-04-01', 'If you choose not to pay Revered Unicorn\'s cumulative upkeep, it will still get an age counter before being sacrificed, which impacts the amount of life you gain.').

card_ruling('reverence', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('reverent hunter', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('reverent hunter', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('reverent hunter', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('reverent hunter', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('reversal of fortune', '2004-12-01', 'Reversal of Fortune creates a copy of the card in the other player\'s hand, then allows you to cast the copy from that player\'s hand without paying its mana cost.').
card_ruling('reversal of fortune', '2004-12-01', 'You cast the copy while this ability is resolving and still on the stack. Normally, you\'re not allowed to cast spells or activate abilities at this time. Reversal of Fortune provides an exception.').
card_ruling('reversal of fortune', '2004-12-01', 'You don\'t pay the spell\'s mana cost. If the spell has X in its mana cost, X is 0. You do pay any additional costs for that spell. You can\'t use any alternative costs.').
card_ruling('reversal of fortune', '2004-12-01', 'If the copied card is a split card, you may choose to cast either side of the split card but not both.').
card_ruling('reversal of fortune', '2004-12-01', 'You can\'t cast the copy if an effect prevents you from casting instants or sorceries, or from casting that particular instant or sorcery. It doesn\'t matter whether your opponent can cast the card at this time; the only thing that matters is whether you can cast it.').
card_ruling('reversal of fortune', '2004-12-01', 'You can\'t cast the copy unless all of its targets, if any, can be chosen.').
card_ruling('reversal of fortune', '2004-12-01', 'If you don\'t want to cast the copy, you can choose not to; the copy ceases to exist the next time state-based actions are checked.').
card_ruling('reversal of fortune', '2004-12-01', 'The spell is cast from your opponent\'s hand, not yours, which can be important for some cards.').
card_ruling('reversal of fortune', '2005-03-01', 'The creation of the copy and then the casting of the copy are both optional.').

card_ruling('reverse damage', '2004-10-04', 'It only affects damage dealt by the source one time. If the source damages you a second time this turn, the damage will not be reversed.').

card_ruling('reverse the sands', '2004-12-01', 'You choose which player gets which life total when the spell resolves.').
card_ruling('reverse the sands', '2004-12-01', 'You can\'t split up a life total when you redistribute it. For example, suppose that in a two-player game your life total is 5 and your opponent\'s life total is 15 when Reverse the Sands starts to resolve. You can choose to (a) leave the life totals as they are or (b) make your life total 15 and your opponent\'s 5. You can\'t choose to make your life total 20 and your opponent\'s 0.').

card_ruling('revive the fallen', '2008-04-01', 'If you win the clash, the spell moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t win the clash, the spell is put into the graveyard from the stack as normal.').
card_ruling('revive the fallen', '2008-04-01', 'If the spell is countered for any reason (for example, if all its targets become illegal), none of its effects happen. There is no clash, and the spell card won\'t be returned to your hand.').

card_ruling('reviving melody', '2014-04-26', 'You choose which mode you’re using—or whether to use both modes—as you’re casting the spell. Once this choice is made, it can’t be changed later while the spell is on the stack.').
card_ruling('reviving melody', '2014-04-26', 'If you choose both modes, and only one of the cards is still a legal target when Reviving Melody tries to resolve, only that card is returned to your hand.').

card_ruling('reweave', '2005-04-01', 'When Reweave resolves it uses the type(s) that the permanent had right before it was sacrificed').
card_ruling('reweave', '2005-11-01', 'The player reveals cards only if the targeted permanent gets sacrificed.').
card_ruling('reweave', '2008-04-01', 'With the introduction of the Tribal type it became possible for an instant or sorcery card to share a type with the sacrificed permanent (ie, a Tribal instant card shares a type with a Tribal Enchantment card). Reweave has received errata so that the effect looks for a *permanent* card that shares a type with the sacrificed permanent. A permanent card is a card that could be put onto the battlefield, which is any card that isn\'t an instant or sorcery.').
card_ruling('reweave', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('reweave', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('reweave', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('reweave', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('rewind', '2012-07-01', 'Rewind targets only a spell. It doesn\'t target any lands. The lands are chosen on resolution. It doesn\'t matter who controls the lands.').
card_ruling('rewind', '2012-07-01', 'When Rewind resolves, you choose up to four lands and those lands untap once. You can\'t choose one land and have it untap four times, for example.').
card_ruling('rewind', '2012-07-01', 'If the spell is an illegal target when Rewind tries to resolve, perhaps because it was countered by another spell or ability, Rewind will be countered and none of its effects will happen. You won\'t untap any lands.').
card_ruling('rewind', '2012-07-01', 'If Rewind resolves but the target spell can\'t be countered, you\'ll still untap lands.').

card_ruling('rhox', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('rhox', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('rhox', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('rhox', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('rhox charger', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('rhox charger', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('rhox charger', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('rhox charger', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('rhox charger', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('rhox charger', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('rhox faithmender', '2012-07-01', 'If you control two Rhox Faithmenders, life you gain will be multiplied by four. Three Rhox Faithmenders will multiply any life gain by eight, and so on.').
card_ruling('rhox faithmender', '2012-07-01', 'If an effect sets your life total to a specific number, and that number is higher than your current life total, the effect will cause you to gain life equal to the difference. Rhox Faithmender will then double that number. For example, if you have 3 life and an effect says that your life total \"becomes 10,\" your life total will actually become 17.').
card_ruling('rhox faithmender', '2012-07-01', 'In a Two-Headed Giant game, only Rhox Faithmender\'s controller is affected by it. If that player\'s teammate gains life, Rhox Faithmender will have no effect, even when that life gain is applied to the shared team life total.').

card_ruling('rhox maulers', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('rhox maulers', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('rhox maulers', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('rhox meditant', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').

card_ruling('rhys the exiled', '2008-04-01', 'You may sacrifice Rhys the Exiled to pay for its own regeneration ability. However, since Rhys is no longer on the battlefield, the ability will have no effect.').

card_ruling('rhystic cave', '2004-10-04', 'If a player pays {1}, Rhystic Cave does not produce any mana, but is still considered to have been tapped for mana. Things which trigger of the land being tapped for mana will trigger, but things which try to replace mana production will have nothing to replace.').
card_ruling('rhystic cave', '2004-10-04', 'Each player (starting with the current player and going in turn order) gets the option to pay when this ability resolves.').
card_ruling('rhystic cave', '2004-10-04', 'The card\'s ability has errata so you can\'t activate the ability during casting of a spell or activating of an ability. This prevents you from getting into a position where someone paying {1} could stop you from having enough mana to pay for the spell. If you want to use it pay for a spell or ability, you need to use this card before you start the casting or activating.').

card_ruling('rhystic circle', '2004-10-04', 'Can\'t be used to prevent damage to your creatures, just to you.').

card_ruling('rhystic deluge', '2004-10-04', 'The creature\'s controller gets the option to pay when this ability resolves.').

card_ruling('rhystic lightning', '2004-10-04', 'The player gets the option to pay when this spell resolves.').

card_ruling('rhystic scrying', '2004-10-04', 'Each player (starting with the current player and going in turn order) gets the option to pay when this spell resolves.').

card_ruling('rhystic shield', '2004-10-04', 'Only affects creatures you control when it resolves.').
card_ruling('rhystic shield', '2004-10-04', 'Each player (starting with the current player and going in turn order) gets the option to pay when this spell resolves.').

card_ruling('rhystic study', '2004-10-04', 'The player gets the option to pay when this triggered ability resolves. This will be after the spell is announced, but before it resolves.').
card_ruling('rhystic study', '2004-10-04', 'You don\'t have to decide whether or not you are drawing until after the player decides whether or not to pay.').

card_ruling('ricochet trap', '2010-03-01', 'If you cast Ricochet Trap for {R}, you don\'t have to target the blue spell an opponent cast.').
card_ruling('ricochet trap', '2010-03-01', 'Ricochet Trap targets only the spell whose target will be changed. It doesn\'t directly affect the original target of that spell or the new target of that spell.').
card_ruling('ricochet trap', '2010-03-01', 'You don\'t choose the new target for the spell until Ricochet Trap resolves. You must change the target if possible. However, you can\'t change the target to an illegal target. If there are no legal targets to choose from, the target isn\'t changed. It doesn\'t matter if the original target of that spell has somehow become illegal itself.').
card_ruling('ricochet trap', '2010-03-01', 'If you cast Ricochet Trap targeting a spell that targets a spell on the stack (like Cancel does, for example), you can\'t change that spell\'s target to itself. You can, however, change that spell\'s target to Ricochet Trap. If you do, that spell will be countered when it tries to resolve because Ricochet Trap will have left the stack by then.').
card_ruling('ricochet trap', '2010-03-01', 'If a spell targets multiple things, you can\'t target it with Ricochet Trap, even if all but one of those targets have become illegal.').
card_ruling('ricochet trap', '2010-03-01', 'If a spell targets the same player or object multiple times, you can\'t target it with Ricochet Trap.').

card_ruling('riddlekeeper', '2011-09-22', 'Riddlekeeper doesn\'t impose a cost to attack. Creatures can attack you or a planeswalker you control even if their controller has no cards left in his or her library.').

card_ruling('ride down', '2014-09-20', 'The attacking creatures that the destroyed creature was blocking remain blocked (even if no other creatures were blocking them). An attacking creature with trample that has no creature blocking it can deal its combat damage to the defending player or planeswalker.').
card_ruling('ride down', '2014-09-20', 'In some rare cases, the blocking creature wasn’t declared as a blocking creature that combat (for example, if it entered the battlefield blocking). In that case, the attacking creatures it was blocking won’t gain trample even though the blocking creature is destroyed.').

card_ruling('riders of gavony', '2012-05-01', 'You must choose an existing _Magic_ creature type, such as Zombie or Warrior. Card types such as artifact can\'t be chosen.').

card_ruling('ridge rannet', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('riding the dilu horse', '2009-10-01', 'The spell\'s effect has no duration. The targeted creature gets +2/+2 until the game ends or it leaves the battlefield. It has horsemanship until the game ends, it leaves the battlefield, or some other effect causes it to lose horsemanship.').

card_ruling('rift bolt', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('rift bolt', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('rift bolt', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('rift bolt', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('rift bolt', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('rift bolt', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('rift bolt', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('rift bolt', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('rift bolt', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('rift bolt', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('rift elemental', '2007-05-01', 'The counter is removed as a cost, so it can\'t be responded to.').
card_ruling('rift elemental', '2007-05-01', 'If an suspended card\'s last time counter is removed this way, the suspend triggered ability is put on the stack on top of the Rift Elemental ability. The same is true for the vanishing ability of a permanent that loses its last time counter.').

card_ruling('riftmarked knight', '2007-02-01', 'If Riftmarked Knight is suspended, then when the last time counter is removed from it, both its own triggered ability and the \"cast this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('riftmarked knight', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('riftmarked knight', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('riftmarked knight', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('riftmarked knight', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('riftmarked knight', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('riftmarked knight', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('riftmarked knight', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('riftmarked knight', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('riftmarked knight', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('riftmarked knight', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('riftsweeper', '2007-05-01', 'It doesn\'t matter why the card was exiled or who owns it.').
card_ruling('riftsweeper', '2007-05-01', 'If Riftsweeper affects a suspended card, the card loses its time counters and is no longer suspended.').
card_ruling('riftsweeper', '2007-05-01', 'If Riftsweeper affects a card that\'s haunting a creature, the haunt effect ends.').

card_ruling('riftwing cloudskate', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('riftwing cloudskate', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('riftwing cloudskate', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('riftwing cloudskate', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('riftwing cloudskate', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('riftwing cloudskate', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('riftwing cloudskate', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('riftwing cloudskate', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('riftwing cloudskate', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('riftwing cloudskate', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('righteous authority', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('righteous charge', '2013-01-24', 'Only creatures you control when Righteous Charge resolves will get +2/+2. Creatures that come under your control later in the turn will not.').

card_ruling('righteousness', '2009-10-01', 'A \"blocking creature\" is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures it was blocking are no longer on the battlefield or have otherwise left combat.').

card_ruling('riku of two reflections', '2011-09-22', 'Each time the first ability triggers, you can pay {U}{R} only one time to get one copy of the spell. Each time the second ability triggers, you can pay {G}{U} only one time to get one token.').
card_ruling('riku of two reflections', '2011-09-22', 'If the spell that caused Riku\'s first ability to trigger has left the stack by the time the ability resolves, you can still pay {U}{R}. If you do, you\'ll copy the spell as it last existed on the stack.').
card_ruling('riku of two reflections', '2011-09-22', 'Riku\'s first ability triggers whenever you cast any instant or sorcery spell, regardless of whether that spell has targets.').
card_ruling('riku of two reflections', '2011-09-22', 'If the copied spell has {X} in its mana cost, the value of X is copied.').
card_ruling('riku of two reflections', '2011-09-22', 'If the copied spell is modal (that is, it says \"Choose one --\" or the like), the copy has the same mode as the original spell. You can\'t choose a different one.').
card_ruling('riku of two reflections', '2011-09-22', 'You can\'t pay any additional costs for the copy. However, effects based on any additional costs paid for the original spell are copied as though those same costs were paid for the copy. Notably, if the original spell was kicked (or kicked a certain number of times), the copy will also be kicked (or kicked that many times).').
card_ruling('riku of two reflections', '2011-09-22', 'If the creature that caused Riku\'s second ability to trigger has already left the battlefield by the time the ability resolves, you can still pay {G}{U}. If you do, you\'ll still put a token onto the battlefield. That token has the copiable values of the characteristics of that creature as it last existed on the battlefield.').
card_ruling('riku of two reflections', '2011-09-22', 'As the token is created, it checks the printed values of the creature it\'s copying, as well as any copy effects that have been applied to it.').
card_ruling('riku of two reflections', '2011-09-22', 'The copiable values of the token\'s characteristics are the same as the copiable values of the characteristics of the creature it\'s copying.').
card_ruling('riku of two reflections', '2011-09-22', 'If the creature had {X} in its mana cost, X must be 0. Note this is different from an {X} found in the mana cost of a copied spell.').

card_ruling('rime dryad', '2004-10-04', 'Can\'t landwalk through a non-Snow Forest.').

card_ruling('rimefeather owl', '2006-07-15', 'Rimefeather Owl\'s last ability affects all permanents with ice counters on them, whether or not it put the ice counters on them.').

card_ruling('rimehorn aurochs', '2006-07-15', 'If the first creature targeted by Rimehorn Aurochs can\'t block the second targeted creature (for example, because the second creature has flying and the first doesn\'t, or because both creatures are controlled by the same player), the ability does nothing and the first creature is free to block whichever creature its controller chooses, or block no creatures at all.').
card_ruling('rimehorn aurochs', '2006-07-15', 'Abilities of Aurochs care about other creatures that have creature type Aurochs, which includes (but is not limited to) the Ice Age card namedAurochs.').

card_ruling('rimescale dragon', '2006-07-15', 'Rimescale Dragon\'s last ability affects all permanents with ice counters on them, whether or not it put the ice counters on them.').

card_ruling('rimewind cryomancer', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('ring of ma\'rûf', '2007-09-16', 'Ring of Ma\'rûf works a little differently than the Wishes from others sets. Rather than letting you simply put a card into your hand from outside the game, this ability replaces your next draw. If you wouldn\'t draw a card during the rest of the turn, the ability won\'t have any effect.').

card_ruling('rings of brighthearth', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').

card_ruling('ringskipper', '2007-10-01', 'If Ringskipper is removed from the graveyard before the ability resolves, you still clash, but nothing will happen if you win.').

card_ruling('ringwarden owl', '2015-06-22', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('ringwarden owl', '2015-06-22', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('ringwarden owl', '2015-06-22', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('riot control', '2013-04-15', 'Count the number of creatures your opponents control as Riot Control resolves to determine how much life you gain.').
card_ruling('riot control', '2013-04-15', 'If a source an opponent controls would deal noncombat damage to you, you may choose to have Riot Control prevent that damage before that opponent may choose to redirect that damage to a planeswalker you control.').
card_ruling('riot control', '2013-04-15', 'Riot Control doesn\'t prevent combat damage that would be dealt to planeswalkers you control by creatures attacking them. It also doesn\'t prevent damage that would be dealt to creatures you control.').

card_ruling('riptide chimera', '2014-04-26', 'If Riptide Chimera is the only enchantment you control when its ability resolves, you must return it to its owner’s hand.').
card_ruling('riptide chimera', '2014-04-26', 'If you control no enchantments when Riptide Chimera’s ability resolves (perhaps because Riptide Chimera stops being an enchantment for some reason), then nothing happens.').

card_ruling('riptide mangler', '2015-02-25', 'The activated ability overwrites all previous effects that set Riptide Mangler’s base power to specific a value (such as previous activations of the ability). Other effects that set its base power that start to apply after Riptide Mangler’s ability resolves will overwrite this effect.').
card_ruling('riptide mangler', '2015-02-25', 'Effects that modify Riptide Mangler’s power, such as the effect of Giant Growth or Glorious Anthem, will apply to Riptide Mangler no matter when they started applying. The same is true for counters that affect Riptide Mangler’s power and effects that switch its power and toughness.').

card_ruling('riptide pilferer', '2007-02-01', 'This is the timeshifted version of Headhunter.').

card_ruling('riptide survivor', '2004-10-04', 'If you have less than 2 cards in your hand, discard all the cards you have. If you have no cards in hand, you can discard zero and draw 3.').

card_ruling('rise from the grave', '2009-10-01', 'Rise from the Grave doesn\'t overwrite any previous colors or types. Rather, it adds another color and another subtype.').
card_ruling('rise from the grave', '2009-10-01', 'If the targeted creature is normally colorless, it will simply become black. It won\'t be both black and colorless.').
card_ruling('rise from the grave', '2009-10-01', 'A later effect that changes the affected creature\'s colors will overwrite that part of Rise from the Grave effect; the creature will be just the new color. The same is true about an effect that changes the affected creature\'s types or subtypes.').
card_ruling('rise from the grave', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('rise of the hobgoblins', '2008-08-01', 'When the enters-the-battlefield ability resolves, you choose the value of X, pay for it, and put the tokens onto the battlefield. You can activate mana abilities to pay for {X}, but no one can cast spells or activate abilities in response to this choice.').

card_ruling('risen executioner', '2015-02-25', 'Risen Executioner’s last ability doesn’t change when you can cast it.').
card_ruling('risen executioner', '2015-02-25', 'Once you start to cast Risen Executioner from your graveyard, you’ll finish casting it, including paying its cost, before any player receives priority. No one can respond in time to affect the cost you’ll pay (perhaps by destroying one of your other creatures).').

card_ruling('rising miasma', '2015-08-25', 'If you cast Rising Miasma for its awaken cost, creatures will get -2/-2 before the land you targeted is turned into a creature. Unless it was already a land creature before Rising Miasma started resolving, it won’t get -2/-2.').
card_ruling('rising miasma', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('rising miasma', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('rising miasma', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('rising miasma', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('rising miasma', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('rising miasma', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('risky move', '2004-10-04', 'The change of control of creatures does not end at end of turn or even when this card leaves the battlefield.').
card_ruling('risky move', '2004-10-04', 'This card does not target any creatures or players.').

card_ruling('rite of consumption', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('rite of consumption', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('rite of passage', '2004-12-01', 'If the creature is dealt damage by more than one source at the same time, it gets only one counter.').
card_ruling('rite of passage', '2004-12-01', 'The creature doesn\'t get a counter if all the damage that would be dealt to it is prevented.').

card_ruling('rite of replication', '2009-10-01', 'If the targeted creature is an illegal target by the time Rite of Replication resolves, the entire spell is countered. You won\'t get any tokens.').
card_ruling('rite of replication', '2009-10-01', 'As the token or tokens are created, they check the printed values of the creature they\'re copying -- or, if that creature is itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, or so on.').
card_ruling('rite of replication', '2009-10-01', 'The tokens see each other enter the battlefield. If they have a triggered ability that triggers when a creature enters the battlefield, they\'ll all trigger for one another.').

card_ruling('rite of ruin', '2012-05-01', 'First you choose the order for the card types. Then each player in turn order (starting with the active player) chooses one permanent he or she controls of the first card type. Then those permanents are sacrificed at the same time. Then each player in turn order chooses two permanents he or she controls of the second card type. Then those permanents are sacrificed at the same time. Finally, each player in turn order chooses three permanents of the third card type, then those permanents are sacrificed at the same time.').
card_ruling('rite of ruin', '2012-05-01', 'Permanents sacrificed for one card type won\'t be on the battlefield to be chosen for subsequent card types.').
card_ruling('rite of ruin', '2012-05-01', 'If any abilities trigger while Rite of Ruin is resolving, those abilities will wait until Rite of Ruin finishes resolving before going on the stack. Starting with the active player, each player puts his or her abilities on the stack in any order.').

card_ruling('rite of the serpent', '2014-09-20', 'You get the Snake token no matter who controlled the target creature.').
card_ruling('rite of the serpent', '2014-09-20', 'Rite of the Serpent won’t create more than one Snake token, even if the target creature had more than one +1/+1 counter on it.').
card_ruling('rite of the serpent', '2014-09-20', 'If the creature is an illegal target as Rite of the Serpent tries to resolve, Rite of the Serpent will be countered and none of its effects will occur. You won’t get a Snake token.').
card_ruling('rite of the serpent', '2014-09-20', 'If Rite of the Serpent resolves but the creature isn’t destroyed (perhaps because it has indestructible or it regenerated), you’ll get a Snake token if the creature has a +1/+1 counter on it.').

card_ruling('rite of undoing', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('rite of undoing', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('rite of undoing', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('rite of undoing', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('rite of undoing', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('rites of flourishing', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('rites of spring', '2004-10-04', 'You can choose to find fewer basic land cards if you want to.').

card_ruling('rith\'s grove', '2004-10-04', 'If you don\'t want to unsummon a land, you can play this card then tap it for mana before the enters the battlefield ability resolves. You may then choose to sacrifice it instead of unsummoning a land.').
card_ruling('rith\'s grove', '2005-08-01', 'This land is of type \"Lair\" only; other subtypes have been removed. It is not a basic land.').

card_ruling('rith, the awakener', '2004-10-04', 'You choose the color during resolution. This means your opponent does not get to react after knowing the color you chose.').

card_ruling('ritual of subdual', '2008-10-01', 'This effect changes the type of mana produced, but not the amount. For example, if a land that\'s tapped for mana would add {W}{W} to its controller\'s mana pool, it adds {2} to that player\'s mana pool instead.').

card_ruling('ritual of the machine', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('ritual of the machine', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('ritual of the returned', '2014-04-26', 'If the creature card leaves the graveyard in response to Ritual of the Returned, Ritual of the Returned will be countered and none of its effects will happen. You won’t get a Zombie token.').
card_ruling('ritual of the returned', '2014-04-26', 'Use the creature card’s power and toughness as it last existed in the graveyard to determine the power and toughness of the Zombie token.').

card_ruling('rivalry', '2004-10-04', 'This ability will only trigger if the player controls more lands at the beginning of upkeep. This condition is checked again at the start of resolution, and if not true, then nothing happens.').

card_ruling('rivals\' duel', '2008-04-01', 'The two creatures may be controlled by the same player.').
card_ruling('rivals\' duel', '2008-04-01', 'If either one of the creatures leaves the battlefield before Rivals\' Duel resolves, no damage is dealt to or by the remaining creature. If both creatures leave the battlefield before Rivals\' Duel resolves, the spell is countered for having no legal targets.').
card_ruling('rivals\' duel', '2008-04-01', 'If, by the time Rivals\' Duel resolves, an effect has caused the two target creatures to share a creature type, Rivals\' Duel is countered for having no legal targets.').

card_ruling('river kelpie', '2008-05-01', 'River Kelpie doesn\'t give you the ability to cast spells from graveyards. Its second ability merely triggers whenever a spell is cast this way (by using Memory Plunder, for example).').
card_ruling('river kelpie', '2008-05-01', 'If River Kelpie and another permanent are each put onto the battlefield from a graveyard at the same time, River Kelpie\'s first ability will trigger twice. (It will see the other permanent entering the battlefield.)').
card_ruling('river kelpie', '2008-05-01', 'If you cast another artifact, creature, enchantment, or planeswalker spell from a graveyard, only the second ability triggers. That\'s because the card is put onto the stack, not onto the battlefield.').
card_ruling('river kelpie', '2008-05-01', 'If you cast River Kelpie itself from your graveyard (by using Yawgmoth\'s Will, for example), neither ability triggers. The first ability doesn\'t trigger because River Kelpie is put onto the stack; the second ability doesn\'t trigger because it works only while River Kelpie is already on the battlefield.').
card_ruling('river kelpie', '2008-05-01', 'If you play a land card from a graveyard (by using Crucible of Worlds, for example), only the first ability triggers. That\'s because the land (which isn\'t a spell) is put directly onto the battlefield from the graveyard.').
card_ruling('river kelpie', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('river kelpie', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('river kelpie', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('river kelpie', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('river kelpie', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('river kelpie', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('river kelpie', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('river of tears', '2007-05-01', 'The turn you play River of Tears, it will produce {B} when tapped for mana.').
card_ruling('river of tears', '2007-05-01', 'The land produces {B} only after you\'ve played a land, not after you\'ve put a land directly onto the battlefield (such as with Rampant Growth).').

card_ruling('river\'s grasp', '2008-05-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('river\'s grasp', '2008-05-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('river\'s grasp', '2008-05-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('river\'s grasp', '2008-05-01', 'When you cast the spell, you choose its targets before you pay for it.').

card_ruling('riverfall mimic', '2008-08-01', 'The ability triggers whenever you cast a spell that\'s both of its listed colors. It doesn\'t matter whether that spell also happens to be any other colors.').
card_ruling('riverfall mimic', '2008-08-01', 'If you cast a spell that\'s the two appropriate colors for the second time in a turn, the ability triggers again. The Mimic will once again become the power and toughness stated in its ability, which could overwrite power- and toughness-setting effects that have been applied to it in the meantime.').
card_ruling('riverfall mimic', '2008-08-01', 'Any other abilities the Mimic may have gained are not affected.').
card_ruling('riverfall mimic', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('riverwheel aerialists', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('riverwheel aerialists', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('riverwheel aerialists', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('rix maadi guildmage', '2012-10-01', 'A blocking creature is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures it was blocking are no longer on the battlefield or have otherwise left combat.').

card_ruling('rix maadi, dungeon palace', '2006-05-01', 'When the second ability resolves, first the active player chooses a card to discard, then the nonactive player chooses, then all chosen cards are discarded at the same time. No one sees what the other players are discarding before deciding which cards to discard.').

card_ruling('roar of challenge', '2014-09-20', 'Roar of Challenge doesn’t give any creatures the ability to block the target creature. It just forces those creatures that are already able to block the creature to do so.').
card_ruling('roar of challenge', '2014-09-20', 'If, during the declare blockers step, a creature is tapped or is affected by a spell or ability that says it can’t block, then it doesn’t block. If there’s a cost associated with having that creature block, its controller isn’t forced to pay that cost. If the player doesn’t, the creature doesn’t block in that case either.').
card_ruling('roar of challenge', '2014-09-20', 'If there are multiple combat phases, creatures that blocked the creature targeted by Roar of Challenge in the first combat aren’t required to block it again in subsequent combats.').
card_ruling('roar of challenge', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('roar of challenge', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('roar of jukai', '2005-02-01', 'Roar of Jukai can be cast even if you don\'t control a Forest or there are no blocked creatures. If you control no Forests or if there are no blocked creatures, the spell does nothing.').
card_ruling('roar of jukai', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('roar of jukai', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('roar of jukai', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('roar of jukai', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('roar of the crowd', '2008-04-01', 'Note that this spell counts the number of permanents you control of the chosen type, not just the number of creatures you control. It will also take your tribal permanents into account.').
card_ruling('roar of the crowd', '2008-04-01', 'You choose a target for this spell as you cast it, but you don\'t choose a creature type until the spell resolves.').
card_ruling('roar of the crowd', '2008-04-01', 'Spells and abilities that prevent damage or regenerate the targeted creature must be cast or activated before the creature type is chosen.').

card_ruling('roaring primadox', '2012-07-01', 'Roaring Primadox\'s ability is mandatory. When it resolves, if Roaring Primadox is the only creature you control, you must return it to its owner\'s hand.').
card_ruling('roaring primadox', '2014-07-18', 'The ability doesn’t target any creature. You choose which one to return when the ability resolves. No player can respond to this choice once the ability starts resolving.').

card_ruling('robber fly', '2004-10-04', 'It triggers only once even if blocked by more than one creature.').

card_ruling('robe of mirrors', '2010-08-15', 'Having shroud will prevent the creature from being targeted by Aura spells, but it does not remove any Auras that are already attached to the creature or stop Auras from being moved to the creature by some other method.').

card_ruling('rock hydra', '2004-10-04', 'If put onto the battlefield without casting it from your hand, X will be zero.').
card_ruling('rock hydra', '2004-10-04', 'Once on the battlefield, the X is considered to be zero when calculating its mana cost.').
card_ruling('rock hydra', '2008-08-01', 'You can activate the activated ability that prevents damage any time, even if no source seems likely to deal damage to you in the near future. It will prevent the next 1 damage that would be dealt this turn, even if that turns out to be much later in the turn.').
card_ruling('rock hydra', '2008-08-01', 'If damage that would be dealt to Rock Hydra can\'t be prevented, you still remove a +1/+1 counter from it for each 1 damage dealt.').

card_ruling('rock jockey', '2004-10-04', 'The first ability applies only while this card is in your hand (or someplace else you can cast it from).').
card_ruling('rock jockey', '2004-10-04', 'The second ability applies only while this card is on the battlefield.').

card_ruling('rock slide', '2004-10-04', 'If X is greater than 0, you can\'t choose zero targets. You must choose between one and X targets. If X is 0, you can’t choose any targets.').

card_ruling('rocket launcher', '2004-10-04', 'You can choose different targets for each use.').
card_ruling('rocket launcher', '2004-10-04', 'Because it does not have tapping as part of its activation cost, you can activate the ability many times in one turn, but it is destroyed at the end of any turn in which you use it.').
card_ruling('rocket launcher', '2004-10-04', 'It is only destroyed at end of turn if the ability resolved. If the ability is countered, it is not sacrificed.').

card_ruling('rocky tar pit', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('rofellos\'s gift', '2004-10-04', 'You reveal and you choose cards during the resolution.').

card_ruling('rogue elephant', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('rogue elephant', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('rogue skycaptain', '2008-10-01', 'No choices are made when this ability triggers. After the wage counter is put on Rogue Skycaptain, then you choose whether to pay mana and, if you don\'t, which opponent gains control of it.').
card_ruling('rogue skycaptain', '2008-10-01', 'If you can\'t pay the entire payment, none of it is paid.').

card_ruling('rogue\'s passage', '2012-10-01', 'Activating the second ability of Rogue\'s Passage after a creature has become blocked won\'t cause it to become unblocked.').

card_ruling('rohgahh of kher keep', '2009-10-01', 'Rohgahh\'s first ability isn\'t targeted. You don\'t choose the opponent that will gain control of the creatures until the ability resolves.').
card_ruling('rohgahh of kher keep', '2009-10-01', 'If you don\'t pay {R}{R}{R} as the first ability resolves, all creatures named Kobolds of Kher Keep become tapped (even the ones you don\'t control), as does Rohgahh. The chosen opponent gains control of Rohgahh and all creatures named Kobolds of Kher Keep regardless of whether you control them or whether they were tapped by the ability.').

card_ruling('roil elemental', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('roil elemental', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('roil elemental', '2009-10-01', 'If Roil Elemental leaves the battlefield, you no longer control it, so all of its control-change effects end.').
card_ruling('roil elemental', '2009-10-01', 'If Roil Elemental ceases to be under your control before its ability resolves, you won\'t gain control of the targeted creature at all.').
card_ruling('roil elemental', '2009-10-01', 'You may target a creature you already control with Roil Elemental\'s ability. This will usually have no visible effect, but it will overwrite any previous control-change effects. For example, if you gain control of Roil Elemental until the end of the turn (with Mark of Mutiny, perhaps), then its landfall ability triggers and you target Roil Elemental itself, you\'ll gain control of it indefinitely (since its control-change effect will last for as long as you control it).').

card_ruling('roil spout', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('roil spout', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('roil spout', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('roil spout', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('roil spout', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('roil spout', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('roil\'s retribution', '2015-08-25', 'You choose how many targets Roil’s Retribution has and how the damage is divided as you cast the spell. Each target must receive at least 1 damage.').
card_ruling('roil\'s retribution', '2015-08-25', 'If some, but not all, of the targets become illegal by the time Roil’s Retribution tries to resolve, the original division of damage still applies, but no damage is dealt to illegal targets. If all the targets are illegal, the spell will be countered.').

card_ruling('roiling horror', '2007-02-01', 'If your life total is less than or equal to the life total of the opponent with the most life, Roiling Horror\'s toughness is 0 or less. It will be put into its owner\'s graveyard as a state-based action.').
card_ruling('roiling horror', '2007-02-01', 'You never choose an opponent, and the opponent that Roiling Horror looks at is never \"locked in.\" Roiling Horror continuously looks at the life totals of all opponents and uses whichever value is the largest.').
card_ruling('roiling horror', '2007-02-01', 'Both instances of X in the suspend ability are the same. You determine the value of X as you suspend the card from your hand. The value you choose must be at least 1.').
card_ruling('roiling horror', '2007-02-01', 'If this is suspended, then when the last time counter is removed from it, both its triggered ability and the \"play this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('roiling horror', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('roiling horror', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('roiling horror', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('roiling horror', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('roiling horror', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('roiling horror', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('roiling horror', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('roiling horror', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('roiling horror', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('roiling horror', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('roiling terrain', '2010-03-01', 'If the targeted land is an illegal target by the time Roiling Terrain resolves, the entire spell is countered. No damage will be dealt.').
card_ruling('roiling terrain', '2010-03-01', 'The effects of the spell happen in sequence. By the time Roiling Terrain checks how many land cards are in the affected player\'s graveyard, the land Roiling Terrain destroyed is (most likely) already in that graveyard.').
card_ruling('roiling terrain', '2013-07-01', 'If the targeted land regenerates or has indestructible, Roiling Terrain still deals damage to the targeted land\'s controller.').

card_ruling('roilmage\'s trick', '2015-08-25', 'You can cast Roilmage’s Trick even if your opponents control no creatures. You’ll simply draw a card.').
card_ruling('roilmage\'s trick', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('roilmage\'s trick', '2015-08-25', '').
card_ruling('roilmage\'s trick', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('roilmage\'s trick', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('roilmage\'s trick', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('rolling thunder', '2004-10-04', 'If X is greater than 0, you can\'t choose zero targets. You must choose between one and X targets. If X is 0, you can’t choose any targets.').
card_ruling('rolling thunder', '2015-08-25', 'You choose how many targets Rolling Thunder has and how the damage is divided as you cast the spell. Each target must receive at least 1 damage.').
card_ruling('rolling thunder', '2015-08-25', 'If some, but not all, of the targets become illegal by the time Rolling Thunder tries to resolve, the original division of damage still applies, but no damage is dealt to illegal targets. If all the targets are illegal, the spell will be countered.').
card_ruling('rolling thunder', '2015-08-25', 'If Rolling Thunder is copied, the copy will retain the original damage divsion. The spell or ability will likely allow you to choose new targets, but you can’t change the way damage is divided.').

card_ruling('ronin warclub', '2005-02-01', 'The triggered ability can attach Ronin Warclub to an untargetable creature because the ability doesn\'t target.').
card_ruling('ronin warclub', '2005-02-01', 'The triggered ability won\'t attach Ronin Warclub to a creature that can\'t be equipped (such as Goblin Brawler).').

card_ruling('ronom hulk', '2006-07-15', 'Ronom Hulk can\'t be targeted by snow spells (though there are no snow instants or sorceries) or abilities of snow sources, it can\'t be enchanted by snow Auras, it can\'t be equipped by snow Equipment (though there are none), it can\'t be blocked by snow creatures, and all damage that would be dealt to it by snow sources is prevented.').
card_ruling('ronom hulk', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('rooftop storm', '2011-09-22', 'You must still pay any mandatory additional costs, such as exiling a creature card from your graveyard for Makeshift Mauler.').
card_ruling('rooftop storm', '2011-09-22', 'The mana cost and converted mana cost of the spell are unchanged. Rooftop Storm only changes what you pay.').

card_ruling('roon of the hidden realm', '2013-10-17', 'If Roon of the Hidden Realm’s ability targets a commander, that card’s owner may choose to have it go to the command zone rather than being exiled. The delayed triggered ability will return it to the battlefield no matter which of the two zones it went to, as long as it’s still in that zone when that triggered ability resolves.').

card_ruling('root sliver', '2004-10-04', 'The first ability applies when this card is not on the battlefield. The second ability applies when this card is on the battlefield.').
card_ruling('root sliver', '2008-04-01', 'The addition of \"by spells or abilities\" to the second ability is so that tribal instant or sorcery spells that happen to be Slivers (such as Crib Swap) can be countered by the rules if all of their targets become illegal.').
card_ruling('root sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('root sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('root-kin ally', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('root-kin ally', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('root-kin ally', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('root-kin ally', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('root-kin ally', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('root-kin ally', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('root-kin ally', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('rootborn defenses', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('rootborn defenses', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('rootborn defenses', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('rootborn defenses', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('rootborn defenses', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('rootborn defenses', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').
card_ruling('rootborn defenses', '2013-07-01', 'The creature token you put onto the battlefield will also have indestructible.').
card_ruling('rootborn defenses', '2013-07-01', 'Creatures that enter the battlefield after Rootborn Defenses resolves will not gain indestructible.').

card_ruling('rootbound crag', '2009-10-01', 'This checks for lands you control with the land type Mountain or Forest, not for lands named Mountain or Forest. The lands it checks for don\'t have to be basic lands. For example, if you control Temple Garden (a nonbasic land with the land types Forest and Plains), Rootbound Crag will enter the battlefield untapped.').
card_ruling('rootbound crag', '2009-10-01', 'As this is entering the battlefield, it checks for lands that are already on the battlefield. It won\'t see lands that are entering the battlefield at the same time (due to Warp World, for example).').

card_ruling('roots of life', '2004-10-04', 'It affects all opponents in a multiplayer game.').

card_ruling('rootwalla', '2006-02-01', 'If this card\'s ability is activated by one player, then another player takes control of it on the same turn, the second player can\'t activate its ability that turn.').

card_ruling('rootwater matriarch', '2004-10-04', 'Can target an unenchanted creature but has no effect on that creature.').
card_ruling('rootwater matriarch', '2005-08-01', 'A creature is \"enchanted\" if it has any Auras attached to it.').
card_ruling('rootwater matriarch', '2007-07-15', 'Unlike many similar creatures, Rootwater Matriarch untaps as normal during your untap step. Whether it\'s tapped or untapped has no bearing on the control-change effect.').
card_ruling('rootwater matriarch', '2009-02-01', 'The control effect applies so long as at least one Aura is attached to the creature, and will continue to apply even if Rootwater Matriarch has left the battlefield. The effect ends if at any time the creature has no Auras on it.').

card_ruling('rosheen meanderer', '2008-05-01', 'A \"cost that contains {X}\" may be a spell\'s total cost, an activated ability\'s cost, a suspend cost, or a cost you\'re asked to pay as part of the resolution of a spell or ability (such as Broken Ambitions). A spell\'s total cost includes either its mana cost (printed in the upper right corner) or its alternative cost (such as suspend), as well as any additional costs (such as kicker). Basically, if it\'s something you can spend mana on, it\'s a cost. If that cost includes the {X} mana symbol in it, you can spend mana generated by Rosheen on that cost.').
card_ruling('rosheen meanderer', '2008-05-01', 'You can spend mana generated by Rosheen on any part of a cost that contains {X}. You\'re not limited to spending it only on the {X} part.').
card_ruling('rosheen meanderer', '2008-05-01', 'You can spend mana generated by Rosheen on a cost that includes {X} even if you\'ve chosen an X of 0, or the card specifies that you can spend only colored mana on X. (You\'ll have to spend Rosheen\'s mana on a different part of that cost, of course.)').
card_ruling('rosheen meanderer', '2008-05-01', 'You don\'t have to spend all four mana on the same cost.').

card_ruling('rot farm skeleton', '2013-04-15', 'You can activate the last ability only if Rot Farm Skeleton is in your graveyard.').
card_ruling('rot farm skeleton', '2013-04-15', 'Putting four cards from your library into your graveyard is part of the activation cost of Rot Farm Skeleton\'s ability. The four cards will be in the graveyard before any player can respond.').
card_ruling('rot farm skeleton', '2013-04-15', 'If you have three or fewer cards in your library, you can\'t activate Rot Farm Skeleton\'s last ability.').

card_ruling('rot wolf', '2011-06-01', 'If Rot Wolf and a creature dealt damage by Rot Wolf go to the graveyard at the same time, Rot Wolf\'s ability will trigger.').

card_ruling('rotfeaster maggot', '2014-07-18', 'Rotfeaster Maggot’s ability is mandatory. If you are the only player with creature cards in your graveyard, you must target one of them.').

card_ruling('rotted ones, lay siege', '2010-06-15', 'For each Zombie token you put onto the battlefield this way, make sure it\'s clear who its designated player is. A token\'s designated player won\'t change for the rest of the game.').
card_ruling('rotted ones, lay siege', '2010-06-15', 'Each of these Zombies must attack its designated player, not a planeswalker that player controls.').
card_ruling('rotted ones, lay siege', '2010-06-15', 'If, during your declare attackers step, one of these Zombies is tapped or is affected by a spell or ability that says it can\'t attack, then it doesn\'t attack. If there\'s a cost associated with having that creature attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('rotted ones, lay siege', '2010-06-15', 'If one of these Zombies can\'t attack its designated player during any given turn (because that player has left the game, due to a spell or ability such as Chronomantic Escape, or because a player on the opposing team has gained control of it, for example), it may attack another player, attack a planeswalker an opponent controls, or not attack at all. If there\'s a cost with having that creature attack its designated player, you aren\'t forced to pay that cost, so it may attack another player, attack a planeswalker an opponent controls, or not attack at all.').
card_ruling('rotted ones, lay siege', '2010-06-15', 'If there are multiple combat phases in a turn, each of these Zombies must attack its designated player in each of them that it\'s able to.').

card_ruling('rotting rats', '2009-02-01', 'When Rotting Rats\'s enters-the-battlefield ability resolves, first the player whose turn it is chooses a card to discard, then each other player in turn order does the same, then all cards are discarded at the same time. No one sees what the other players are discarding before deciding which card to discard.').

card_ruling('rouse the mob', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('rouse the mob', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('rouse the mob', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('rouse the mob', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('rouse the mob', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('rousing of souls', '2014-05-29', 'Except in some very rare cases, the card each player draws will be the card revealed from the top of his or her library.').

card_ruling('rowen', '2004-10-04', 'The first sentence is a continuous ability. You reveal the card when you draw it, during the resolution of any draw effect. The second sentence is a triggered ability, and thus will wait until the entire draw effect finishes resolving before you do it.').

card_ruling('royal assassin', '2004-10-04', 'Can\'t target itself because you choose the target before you tap him. At that time he is not yet a legal target.').
card_ruling('royal assassin', '2004-10-04', 'If the creature is no longer tapped when the Assassin\'s ability resolves, then the ability is countered.').

card_ruling('royal decree', '2008-10-01', 'Royal Decree\'s ability will trigger a maximum of once for each permanent that becomes tapped, even if that permanent meets multiple criteria. (For example, Badlands is both a Swamp and a Mountain.)').

card_ruling('rubblebelt raiders', '2013-01-24', 'Count the number of attacking creatures you control when Rubblebelt Raiders\'s ability resolves, including Rubblebelt Raiders itself, to determine how many +1/+1 counters to put on it.').

card_ruling('rubblehulk', '2013-01-24', 'The ability that defines Rubblehulk\'s power and toughness works in all zones, not just the battlefield.').
card_ruling('rubblehulk', '2013-01-24', 'If you activate Rubblehulk\'s bloodrush ability, the value of X is the number of lands you control when that ability resolves.').

card_ruling('rubinia soulsinger', '2009-10-01', 'If Rubinia Soulsinger leaves the battlefield, you no longer control it, so the duration of its control-change effect ends.').
card_ruling('rubinia soulsinger', '2009-10-01', 'If Rubinia Soulsinger stops being tapped before its ability resolves -- even if it becomes tapped again right away -- you won\'t gain control of the targeted creature at all. The same is true if you lose control of Rubinia Soulsinger before the ability resolves.').
card_ruling('rubinia soulsinger', '2009-10-01', 'Once the ability resolves, it doesn\'t care whether the targeted permanent remains a legal target for the ability, only that it\'s on the battlefield. The effect will continue to apply to it even if it gains shroud, stops being a creature, or would no longer be a legal target for any other reason.').

card_ruling('ruby medallion', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('ruby medallion', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('ruby medallion', '2004-10-04', 'The effect is cumulative.').
card_ruling('ruby medallion', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('ruby medallion', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('ruby medallion', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('rude awakening', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('rude awakening', '2013-06-07', 'Lands that enter the battlefield or otehrwise come under your control after Rude Awakening resolves won\'t be affected.').

card_ruling('ruhan of the fomori', '2011-09-22', 'Ruhan must attack the chosen player if able, not a planeswalker controlled by the chosen player.').
card_ruling('ruhan of the fomori', '2011-09-22', 'If, during your declare attackers step, Ruhan is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under your control continuously since your turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having Ruhan attack the chosen player, you aren\'t forced to pay that cost, so it doesn\'t have to attack that player in that case either.').
card_ruling('ruhan of the fomori', '2011-09-22', 'If Ruhan isn\'t forced to attack the chosen player for one of the above reasons but can still attack, you may choose to have Ruhan attack another player, attack a planeswalker, or not attack at all.').
card_ruling('ruhan of the fomori', '2011-09-22', 'If your turn has multiple combat phases, Ruhan\'s ability triggers at the beginning of each of them. Ignore any choices made during previous combat phases.').

card_ruling('ruin ghost', '2010-03-01', 'As Ruin Ghost\'s ability resolves, the targeted land is exiled, then immediately returned to the battlefield. The land that enters the battlefield is a different permanent from the one that left. Any counters, Auras, and so on that were affecting the old land won\'t affect the new one. Any spells or abilities that were targeting the old land don\'t target the new one. Whether the old land was tapped or untapped has no bearing on the new one.').
card_ruling('ruin ghost', '2010-03-01', 'The returned land behaves like any other land that\'s put onto the battlefield. If it has an ability that says it enters the battlefield tapped, it does so. (Otherwise it enters the battlefield untapped.) If it has an enters-the-battlefield triggered ability, that ability triggers.').
card_ruling('ruin ghost', '2010-03-01', 'The land returns under your control, regardless of who owns it.').

card_ruling('ruin processor', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('ruin processor', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('ruin processor', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('ruin processor', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('ruination guide', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('ruination guide', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('ruination guide', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('ruination guide', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('ruination guide', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('ruination guide', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('ruination guide', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('ruinous minotaur', '2009-10-01', 'Ruinous Minotaur\'s ability triggers when it deals any kind of damage to an opponent, not just combat damage.').
card_ruling('ruinous minotaur', '2009-10-01', 'You sacrifice a land, not the opponent who was dealt damage.').

card_ruling('ruinous path', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('ruinous path', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('ruinous path', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('ruinous path', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('ruinous path', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('ruinous path', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('rukh egg', '2004-10-04', 'If the Rukh Egg card is removed from the graveyard in the same turn it is put there, a Bird will still be put onto the battlefield.').
card_ruling('rukh egg', '2005-10-01', 'If the Egg is exiled instead of being put into the graveyard, no Bird is put onto the battlefield.').
card_ruling('rukh egg', '2005-10-01', 'Text-changing effects can be used to change the color of the Bird that will be put onto the battlefield.').
card_ruling('rukh egg', '2005-10-01', 'If the Egg is destroyed while under the control of another player, the controller of the Egg gets the Bird.').

card_ruling('rumbling aftershocks', '2010-03-01', 'If you cast a kicked spell, Rumbling Aftershocks\'s ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').
card_ruling('rumbling aftershocks', '2010-03-01', 'The ability triggers just once per kicked spell, even if that spell was kicked multiple times.').

card_ruling('rumbling crescendo', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('rummaging goblin', '2012-07-01', 'Discarding a card is part of the cost to activate Rummaging Goblin\'s ability. If you don\'t have a card in your hand, you can\'t pay this part of the cost and you can\'t activate the ability.').

card_ruling('rune of protection: artifacts', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: artifacts', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: artifacts', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune of protection: black', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: black', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: black', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune of protection: blue', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: blue', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: blue', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune of protection: green', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: green', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: green', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune of protection: lands', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: lands', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('rune of protection: red', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: red', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: red', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune of protection: white', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('rune of protection: white', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').
card_ruling('rune of protection: white', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('rune-tail, kitsune ascendant', '2005-06-01', 'Each Ascendant is legendary in both its unflipped and flipped forms. This means that effects that look for legendary creature cards, such as Time of Need and Captain Sisay, can find an Ascendant.').

card_ruling('runeboggle', '2006-02-01', 'You draw a card even if the player pays {1}.').

card_ruling('runechanter\'s pike', '2011-09-22', 'The value of X is constantly updated as instant cards and sorcery cards are put into or removed from your graveyard.').

card_ruling('runed halo', '2008-05-01', 'Runed Halo gives you protection from each object with the chosen name, whether it\'s a card, a token, or a copy of a spell. It doesn\'t matter what game zone that object is in.').
card_ruling('runed halo', '2008-05-01', 'You can still be attacked by creatures with the chosen name.').
card_ruling('runed halo', '2008-05-01', 'You\'ll have protection from the name, not from the word. For example, if you choose the name Forest, you\'ll have protection from anything named \"Forest\" -- but you won\'t have protection from Forests. An animated Sapseep Forest, for example, could deal damage to you even though its subtype is Forest.').
card_ruling('runed halo', '2008-05-01', 'You can name either half of a split card, but not both. You\'ll have protection from the half you named (and from a fused split spell with that name), but not the other half.').
card_ruling('runed halo', '2008-05-01', 'You can\'t choose [nothing] as a name. Face-down creatures have no name, so Runed Halo can\'t give you protection from them.').
card_ruling('runed halo', '2008-05-01', 'You must choose the name of a card, not the name of a token. For example, you can\'t choose \"Saproling\" or \"Voja.\" However, if a token happens to have the same name as a card (such as \"Shapeshifter\" or \"Goldmeadow Harrier\"), you can choose it.').
card_ruling('runed halo', '2008-05-01', 'You may choose either one of a flip card\'s names. You\'ll have protection only from the appropriate version. For example, if you choose Nighteyes the Desecrator, you won\'t have protection from Nezumi Graverobber.').

card_ruling('runeflare trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('runeflare trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('runeflare trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('runeflare trap', '2009-10-01', 'The player you target with Runeflare Trap doesn\'t have to be the opponent that drew three or more cards this turn.').

card_ruling('runes of the deus', '2008-05-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('runic repetition', '2011-09-22', 'The card could have been exiled for any reason, not just because it was cast using flashback.').
card_ruling('runic repetition', '2011-09-22', 'An effect that gives flashback to an instant or sorcery card in your graveyard stops applying once that card has left the stack. The card won\'t have flashback while exiled and can\'t be the target of Runic Repetition (unless it naturally has flashback).').
card_ruling('runic repetition', '2011-09-22', 'A card that\'s exiled face down doesn\'t have any characteristics or abilities, so it can\'t be the target of Runic Repetition.').

card_ruling('runner\'s bane', '2013-04-15', 'If the enchanted creature\'s power increases to 4 or greater, Runner\'s Bane will be put into its owner\'s graveyard as a state-based action.').
card_ruling('runner\'s bane', '2013-04-15', 'The enchanted creature can still be untapped in other ways, such as by Aurelia, the Warleader.').

card_ruling('ruric thar, the unbowed', '2013-04-15', 'Ruric Thar\'s ability triggers whenever any player casts a noncreature spell, including you.').
card_ruling('ruric thar, the unbowed', '2013-04-15', 'Ruric Thar will deal 6 damage to the player before the noncreature spell resolves.').

card_ruling('rush of battle', '2014-09-20', 'Warrior creatures you control as Rush of Battle resolves gain lifelink in addition to getting +2/+1.').

card_ruling('rush of blood', '2012-05-01', 'The value of X is determined when Rush of Blood resolves. The bonus won\'t change later in the turn if the creature\'s power changes.').
card_ruling('rush of blood', '2012-05-01', 'If the creature has power less than 0, the \"bonus\" will be negative. For example, a creature that\'s somehow become -1/3 would get -1/+0 and become -2/3.').

card_ruling('rush of ice', '2015-08-25', 'Rush of Ice can target a creature that’s already tapped. It still won’t untap during its controller’s next untap step.').
card_ruling('rush of ice', '2015-08-25', 'Rush of Ice tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('rush of ice', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('rush of ice', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('rush of ice', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('rush of ice', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('rush of ice', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('rush of ice', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('rushing river', '2004-10-04', 'You can sacrifice a tapped land.').
card_ruling('rushing river', '2004-10-04', 'You choose a second target only if you choose to pay the Kicker cost.').

card_ruling('rushing-tide zubera', '2005-06-01', 'Rushing-Tide Zubera\'s ability counts all damage dealt to it this turn, from all sources.').

card_ruling('rust', '2004-10-04', 'Can\'t be used on mana abilities because they don\'t use the stack. They\'ve already resolved by the time you get priority.').
card_ruling('rust', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('rust elemental', '2004-12-01', 'Sacrificing an artifact isn\'t optional. If you control an artifact other than Rust Elemental, you must sacrifice it. Otherwise, you must tap Rust Elemental and lose 4 life.').

card_ruling('rust scarab', '2013-01-24', 'Rust Scarab\'s ability normally triggers only once per combat, no matter how many creatures are assigned to block it.').
card_ruling('rust scarab', '2013-01-24', 'The artifact or enchantment will be destroyed before combat damage is dealt but after blockers are chosen.').

card_ruling('rust tick', '2011-01-01', 'You may target a tapped artifact with Rust Tick\'s activated ability.').
card_ruling('rust tick', '2011-01-01', 'If the affected artifact is untapped by some other spell or ability, Rust Tick\'s effect will not end. If you keep Rust Tick tapped, and that artifact becomes tapped again, Rust Tick will continue to prevent it from being untapped during its controller\'s untap step.').
card_ruling('rust tick', '2011-01-01', 'Rust Tick doesn\'t track the artifact\'s controller. If the affected artifact changes controllers, Rust Tick will prevent it from being untapped during its new controller\'s untap step.').
card_ruling('rust tick', '2011-01-01', 'If Rust Tick untaps or leaves the battlefield, its effect will end. This has no immediate visible effect on the affected artifact. (It doesn\'t untap immediately, for example.) The artifact will just untap as normal during its controller\'s next untap step.').
card_ruling('rust tick', '2011-01-01', 'If you control both Rust Tick and the affected artifact, that artifact won\'t untap during the untap step in which you choose to untap Rust Tick. (It will untap during your next one.)').

card_ruling('rusted relic', '2011-01-01', 'If Rusted Relic stops being a creature during combat (because you lose control of one or more artifacts, perhaps), it is removed from combat and will not deal or be dealt combat damage. Creatures it was blocking remain blocked. It won\'t re-enter that combat, even if you wind up controlling three or more artifacts again during that combat phase.').
card_ruling('rusted relic', '2011-01-01', 'Whether Rusted Relic has \"summoning sickness\" is based on how long you have continuously controlled it, not on how long it has continuously been a creature.').

card_ruling('rusted slasher', '2011-06-01', 'You may sacrifice Rusted Slasher to pay for its own regeneration ability. If you do, however, it won\'t regenerate. It\'ll just end up in its owner\'s graveyard as a result of the sacrifice.').

card_ruling('ruthless deathfang', '2015-02-25', 'Ruthless Deathfang’s triggered ability will trigger whenever you sacrifice any creature, including Ruthless Deathfang itself. It will trigger whenever you sacrifice a creature for any reason, including to pay a cost or as the result of a spell or ability (such as an opposing Ruthless Deathfang’s ability).').
card_ruling('ruthless deathfang', '2015-02-25', 'Notably, the “legend rule” does not cause any permanents to be sacrificed. They are simply put into their owners’ graveyards.').

card_ruling('ruthless instincts', '2014-11-24', 'If you choose the first mode, the target creature must not be attacking. However, it can attack later in the turn.').

card_ruling('ruthless invasion', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('ruthless invasion', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('ruthless invasion', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('ruthless invasion', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('ruthless invasion', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('ruthless ripper', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('ruthless ripper', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('ruthless ripper', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('ruthless ripper', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('ruthless ripper', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ruthless ripper', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ruthless ripper', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('ruthless ripper', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('ruthless ripper', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('rysorian badger', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

