% Rulings

card_ruling('back from the brink', '2011-09-22', 'Although you\'re paying the card\'s mana cost, you aren\'t casting that card. Abilities that reduce the cost to cast a creature spell won\'t apply, and additional costs to cast that creature can\'t be paid. If the card has an ability that allows another cost to be paid \"rather than its mana cost,\" that alternative cost can be paid. Other alternative costs that affect what it costs to cast a creature spell, such as evoke, can\'t.').
card_ruling('back from the brink', '2011-09-22', 'Any enters-the-battlefield abilities of the creature will trigger when the token enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the creature will also work.').
card_ruling('back from the brink', '2011-09-22', 'If you exile a double-faced creature card this way, you\'ll pay the mana cost of the front face. The token will be a copy of the front face and it won\'t be able to transform.').
card_ruling('back from the brink', '2011-09-22', 'If the exiled creature card has {X} in its mana cost, X is considered to be zero.').

card_ruling('backlash', '2004-10-04', 'Being untapped is a targeting restriction. If the target is tapped before this spell resolves, then this spell is countered.').

card_ruling('backslide', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('backup plan', '2014-05-29', 'The effect of Backup Plan is cumulative. If you have two in your command zone to start the game, you’ll draw three hands of seven cards and shuffle all but one of them into your library.').
card_ruling('backup plan', '2014-05-29', 'If, through a combination of Backup Plan and/or other conspiracies, you have fewer cards in your deck than you must draw as the game begins, you’ll lose the game when state-based actions are checked during the upkeep of the first turn.').
card_ruling('backup plan', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('backup plan', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('backup plan', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('backup plan', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('backup plan', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('backup plan', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('backup plan', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('bad river', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('badlands', '2004-10-04', 'Land type changing effects that change a dual land\'s land type will remove the old land types completely. Text-changing effects that just change one of the two land types will leave the other type unaffected.').
card_ruling('badlands', '2004-10-04', 'This card is a Mountain and a Swamp even while in the graveyard, library, or any other zone.').
card_ruling('badlands', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('badlands', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('bala ged scorpion', '2010-06-15', 'The power of the targeted creature is checked both as you target it and as the ability resolves.').

card_ruling('bala ged thief', '2009-10-01', 'The ability counts the number of Allies you control as it resolves.').
card_ruling('bala ged thief', '2009-10-01', 'If the number of Allies you control as the ability resolves is greater than the number of cards in the targeted player\'s hand, that player reveals his or her entire hand. You choose one of the cards revealed this way and that player discards it.').

card_ruling('balance', '2004-10-04', 'This spell is not targeted, so cards which can\'t be targeted, such as a creature with protection from white, are both counted and valid choices for being sacrificed.').
card_ruling('balance', '2004-10-04', 'All lands sacrificed go to the graveyard simultaneously, then all cards discarded, and finally all creatures sacrificed. Within each group, you pick the order they end up in the graveyard.').
card_ruling('balance', '2004-10-04', 'Cards are not counted until the appropriate step. So, a land creature sacrificed to the first part of the spell would not count for the last part.').

card_ruling('balance of power', '2004-10-04', 'The number of cards is counted during resolution.').

card_ruling('balduvian conjurer', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('balduvian conjurer', '2008-10-01', 'If an affected land later loses the supertype snow, Balduvian Conjurer\'s effect doesn\'t end. The land will remain a creature until end of turn.').

card_ruling('balduvian dead', '2008-10-01', 'If the ability is activated during a turn\'s Ending phase (after any \"at the beginning of the End step\" triggers would have triggered that turn), the Graveborn token will remain on the battlefield until the following turn\'s End step.').

card_ruling('balduvian fallen', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('balduvian frostwaker', '2006-07-15', 'Balduvian Frostwaker\'s effect has no duration. The snow land is a creature as long as it\'s on the battlefield.').
card_ruling('balduvian frostwaker', '2006-07-15', 'If an affected land later loses the supertype snow, Balduvian Frostwaker\'s effect doesn\'t end. The land will remain a creature.').
card_ruling('balduvian frostwaker', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('balduvian horde', '2004-10-04', 'You choose whether to discard or not on resolution. If not, then you sacrifice this card. You can choose to not discard even if you no longer control this card on resolution.').

card_ruling('balduvian shaman', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('balduvian shaman', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('balduvian shaman', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('balduvian shaman', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('balduvian shaman', '2004-10-04', 'You choose the words to change on resolution.').

card_ruling('balduvian trading post', '2004-10-04', 'You sacrifice the Mountain before this card is put onto the battlefield, and do so no matter how it is put onto the battlefield.').
card_ruling('balduvian trading post', '2004-10-04', 'This card produces two different types of mana. If an effect that adds one mana to your mana pool of the same type as the mana produced by this card, you choose which of the two types it produces.').

card_ruling('balduvian warlord', '2006-07-15', 'Balduvian Warlord\'s ability may be activated during any player\'s turn.').
card_ruling('balduvian warlord', '2006-07-15', 'The targeted creature that\'s removed from combat may then block the attacking creature that it was originally blocking. If so, that creature will no longer be unblocked.').
card_ruling('balduvian warlord', '2006-07-15', 'If an attacking creature becomes unblocked this way and remains unblocked, it will deal its combat damage to the defending player. Abilities that say \"When this creature attacks and isn\'t blocked\" won\'t trigger.').
card_ruling('balduvian warlord', '2006-07-15', 'If the targeted creature has a \"When this creature blocks\" ability, it will trigger when Balduvian Warlord\'s ability resolves and the creature blocks again.').
card_ruling('balduvian warlord', '2006-07-15', 'If an attacking creature has an ability that triggers \"When this creature becomes blocked,\" it triggers when a creature blocks it due to the Warlord\'s ability only if it was unblocked at that point.').
card_ruling('balduvian warlord', '2006-07-15', 'If an attacking creature has a \"When this creature becomes blocked by [a creature]\" ability, it triggers when a creature blocks it due to the Warlord\'s ability.').
card_ruling('balduvian warlord', '2006-07-15', 'The attacking creature will become unblocked only if the targeted creature was the only one that blocked it this combat, even if all the other creatures that were blocking it are no longer on the battlefield or are no longer blocking it (even those removed with other Balduvian Warlords\' abilities).').

card_ruling('balefire dragon', '2011-09-22', 'The damage dealt by Balefire Dragon\'s triggered ability isn\'t combat damage.').

card_ruling('balefire liege', '2008-08-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('baleful eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('baleful eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('baleful eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('baleful eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('baleful eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('baleful eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('baleful eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('ball lightning', '2007-02-01', 'The creature is sacrificed at the end of every turn in which it is on the battlefield. There is no choice about what turn to sacrifice it.').

card_ruling('ballyrush banneret', '2008-04-01', 'A spell you cast that\'s both creature types costs {1} less to cast, not {2} less.').
card_ruling('ballyrush banneret', '2008-04-01', 'The effect reduces the total cost of the spell, regardless of whether you chose to pay additional or alternative costs. For example, if you cast a Rogue spell by paying its prowl cost, Frogtosser Banneret causes that spell to cost {1} less.').

card_ruling('baloth cage trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('baloth cage trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('baloth cage trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').

card_ruling('baloth woodcrasher', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('baloth woodcrasher', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('balthor the defiled', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').
card_ruling('balthor the defiled', '2013-07-01', 'you can only activate this ability if Balthor the Defiled is on the battlefield.').

card_ruling('balthor the stout', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').

card_ruling('balustrade spy', '2013-01-24', 'If the target player has no land cards in his or her library, all cards from that library will be revealed and put into his or her graveyard.').

card_ruling('bandage', '2007-07-15', 'You draw the card when Bandage resolves, not when the damage is actually prevented.').

card_ruling('bane alley broker', '2013-01-24', 'If Bane Alley Broker\'s first ability resolves when you have no cards in your hand, you\'ll draw a card and then exile it. You won\'t have the opportunity to cast that card (or do anything else with it) before exiling it.').
card_ruling('bane alley broker', '2013-01-24', 'Once you are allowed to look at a face-down card in exile, you are allowed to look at that card as long as it\'s exiled. If you no longer control Bane Alley Broker when its last ability resolves, you can continue to look at the relevant cards in exile to choose one to return.').
card_ruling('bane alley broker', '2013-01-24', 'Bane Alley Broker\'s second and third abilities apply to cards exiled with that specific Bane Alley Broker, not any other creature named Bane Alley Broker. You should keep cards exiled by different Bane Alley Brokers separate.').
card_ruling('bane alley broker', '2013-01-24', 'If Bane Alley Broker leaves the battlefield, the cards exiled with it will be exiled indefinitely. If it later returns to the battlefield, it will be a new object with no connection to the cards exiled with it in its previous existence. You won\'t be able to use the “new” Bane Alley Broker to return cards exiled with the “old” one.').
card_ruling('bane alley broker', '2013-01-24', 'Even if not all players can look at the exiled cards, each card\'s owner is still known. It is advisable to keep cards owned by different players in distinct piles in case another player gains control of Bane Alley Broker and exiles one or more cards with it.').

card_ruling('bane of bala ged', '2015-08-25', 'If Bane of Bala Ged attacks a planeswalker, the defending player may exile that planeswalker. Bane of Bala Ged will still be attacking and may be blocked as normal. If it’s not blocked, it won’t deal combat damage.').
card_ruling('bane of bala ged', '2015-08-25', 'In a Two-Headed Giant game, you choose which of your opponents will exile permanents. You can’t have each of them exile one permanent.').
card_ruling('bane of bala ged', '2015-08-25', 'The permanents are exiled before blockers are chosen.').
card_ruling('bane of bala ged', '2015-08-25', 'If the defending player controls only one permanent, that permanent will be exiled.').

card_ruling('bane of progress', '2013-10-17', 'Bane of Progress’s ability destroys all artifacts and enchantments, including those you control.').
card_ruling('bane of progress', '2013-10-17', 'If an artifact or enchantment isn’t destroyed (perhaps because it has indestructible or it regenerated), it won’t count toward the number of +1/+1 counters put on Bane of Progress. However, if an artifact or enchantment is destroyed but doesn’t go to its owner’s graveyard due to a replacement effect (like the one Rest in Peace creates), it will count.').

card_ruling('bane of the living', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').
card_ruling('bane of the living', '2004-10-04', 'The X in the ability has the same value as the X paid in the Morph ability. This is pretty easy to derive since there is no other source of X.').

card_ruling('banefire', '2009-02-01', 'Banefire checks the number you chose for X, not the amount of mana you actually spent.').
card_ruling('banefire', '2009-02-01', 'Banefire can be targeted by spells that try to counter it (such as Countersquall) regardless of what the value of X is. If X is 5 or more, those spells will still resolve, but the part of their effect that would counter Banefire won\'t do anything. Any other effects those spells have will work as normal.').
card_ruling('banefire', '2009-02-01', 'Banefire\'s ability won\'t prevent it from being countered by the game rules if its target has become an illegal target.').

card_ruling('baneslayer angel', '2009-10-01', '\"Protection from Demons and from Dragons\" means the following: -- Baneslayer Angel can\'t be blocked by creatures with the creature type Demon or the creature type Dragon. -- Baneslayer Angel can\'t be targeted abilities from sources with the creature type Demon or the creature type Dragon, or by spells with either of those types. -- All damage that would be dealt to Baneslayer Angel by sources with the creature type Demon or the creature type Dragon is prevented. -- Baneslayer Angel can\'t be enchanted by Auras or equipped by artifacts that have somehow gotten the creature type Demon or the creature type Dragon.').

card_ruling('banewasp affliction', '2008-10-01', 'The player who loses life is the player who controlled the creature when it was put into a graveyard. This may not be the player whose graveyard it was put into. That player loses life equal to the creature\'s toughness as it last existed on the battlefield.').

card_ruling('banisher priest', '2013-07-01', 'Banisher Priest’s ability causes a zone change with a duration, a new style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Banisher Priest have a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Banisher Priest leaves the battlefield.').
card_ruling('banisher priest', '2013-07-01', 'If Banisher Priest leaves the battlefield before its enters-the-battlefield ability resolves, the target creature won’t be exiled.').
card_ruling('banisher priest', '2013-07-01', 'Auras attached to the exiled creature will be put into their owners’ graveyards. Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('banisher priest', '2013-07-01', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('banisher priest', '2013-07-01', 'The exiled card returns to the battlefield immediately after Banisher Priest leaves the battlefield. Nothing happens between the two events, including state-based actions. The two creatures aren’t on the battlefield at the same time. For example, if the returning creature is a Clone, it can’t enter the battlefield as a copy of Banisher Priest.').
card_ruling('banisher priest', '2013-07-01', 'In a multiplayer game, if Banisher Priest’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('banishing knack', '2008-08-01', 'The targeted creature\'s controller, not Banishing Knack\'s controller, is the one who can activate the creature\'s new ability.').
card_ruling('banishing knack', '2008-08-01', '\"Summoning sickness\" applies. The new ability can\'t be activated unless the targeted creature has been under its controller\'s control since the beginning of that player\'s most recent turn or it has haste.').

card_ruling('banishing light', '2014-04-26', 'Banishing Light’s ability causes a zone change with a duration, a new style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Banishing Light have a single ability that creates two one-shot effects: one that exiles the permanent when the ability resolves, and another that returns the exiled card to the battlefield immediately after Banishing Light leaves the battlefield.').
card_ruling('banishing light', '2014-04-26', 'If Banishing Light leaves the battlefield before its enters-the-battlefield ability resolves, the target nonland permanent won’t be exiled.').
card_ruling('banishing light', '2014-04-26', 'Auras attached to the exiled permanent will be put into their owners’ graveyards. Equipment attached to the exiled permanent will become unattached and remain on the battlefield. Any counters on the exiled permanent will cease to exist.').
card_ruling('banishing light', '2014-04-26', 'If a token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('banishing light', '2014-04-26', 'The exiled card returns to the battlefield immediately after Banishing Light leaves the battlefield. Nothing happens between the two events, including state-based actions. Banishing Light and the returned card aren’t on the battlefield at the same time.').
card_ruling('banishing light', '2014-04-26', 'In a multiplayer game, if Banishing Light’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('banners raised', '2012-05-01', 'Only creatures you control when Banners Raised resolves will get the bonus. Creatures that enter the battlefield or that you gain control of later in the turn will be unaffected.').

card_ruling('banshee', '2009-10-01', 'If the targeted creature or player is an illegal target by the time Banshee\'s ability resolves, the ability is countered. You won\'t be dealt damage.').

card_ruling('banshee\'s blade', '2004-12-01', 'The counters stay on Banshee\'s Blade even if it becomes unattached, or moves from one creature to another.').

card_ruling('bant', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('bant', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('bant', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('bant', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('bant', '2009-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. Note that if a creature has multiple instances of exalted, each one triggers separately. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('bant', '2009-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('bant', '2009-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('bant', '2009-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('bant', '2009-10-01', 'Exalted bonuses last until the turn ends. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('bant', '2009-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').
card_ruling('bant', '2013-07-01', 'A creature with a divinity counter put on it due to the {C} ability retains indestructible even after Bant stops being the face-up plane card.').
card_ruling('bant', '2013-07-01', 'If all divinity counters on a creature are moved to a different creature, the indestructible ability doesn\'t move along with them. The first creature no longer has indestructible because it no longer has a divinity counter on it. The second creature doesn\'t have indestructible because the {C} ability didn\'t target it.').

card_ruling('bant charm', '2008-10-01', 'You can choose a mode only if you can choose legal targets for that mode. If you can\'t choose legal targets for any of the modes, you can\'t cast the spell.').
card_ruling('bant charm', '2008-10-01', 'While the spell is on the stack, treat it as though its only text is the chosen mode. The other two modes are treated as though they don\'t exist. You don\'t choose targets for those modes.').
card_ruling('bant charm', '2008-10-01', 'If this spell is copied, the copy will have the same mode as the original.').

card_ruling('bant sojourners', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('bant sojourners', '2009-05-01', 'The triggered ability acts as a cycle-triggered ability or as a leaves-the-battlefield ability, as appropriate. The player who controls the triggered ability is the player who cycled the Sojourner, or the player who last controlled the Sojourner on the battlefield.').
card_ruling('bant sojourners', '2009-05-01', 'If you cycle this card, the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('bant sojourners', '2009-05-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('barbarian general', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('barbarian guides', '2014-02-01', 'You choose a land subtype, and if the defending player controls any snow lands with that subtype, the targeted creature can\'t be blocked.').

card_ruling('barbed field', '2004-10-04', 'Land are normally colorless, so effects that care about the color of the damage\'s source will not find a color.').

card_ruling('barbed shocker', '2006-09-25', 'The number of cards the player draws is equal to the number of cards he or she discarded, not the amount of damage dealt by Barbed Shocker.').

card_ruling('barbed sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('barbed sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('bargaining table', '2004-10-04', 'You choose an opponent on announcement. This is not targeted, but a choice is still made. This choice is made before determining the value for X that is used in the cost.').

card_ruling('barishi', '2008-04-01', 'The player who controlled Barishi when it left the battlefield is the player who controls the ability. It doesn\'t matter whose graveyard Barishi is put into.').
card_ruling('barishi', '2008-04-01', 'If Barishi isn\'t in your graveyard by the time the ability resolves, you still shuffle all creature cards from your graveyard into your library.').

card_ruling('barkhide mauler', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('barl\'s cage', '2004-10-04', 'Does not prevent a creature from untapping at other times. It just stops the \"normal\" untap during the untap step.').
card_ruling('barl\'s cage', '2009-10-01', 'You may target any creature with this ability. It doesn\'t have to be tapped.').
card_ruling('barl\'s cage', '2009-10-01', 'If the targeted creature is untapped at the time its controller\'s next untap step begins, this ability has no effect. It won\'t apply at some later time when the targeted creature is tapped.').
card_ruling('barl\'s cage', '2009-10-01', 'Barl\'s Cage doesn\'t track the creature\'s controller. If the affected creature changes controllers before its old controller\'s next untap step, this ability will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('baron sengir', '2004-10-04', 'The Baron\'s ability will only regenerate creatures with type \"Vampire,\" regardless of whether the creature has \"Vampire\" in its name.').
card_ruling('baron sengir', '2007-09-16', 'Although Baron Sengir is now a Vampire, it still can\'t regenerate itself.').

card_ruling('barrage of boulders', '2014-09-20', 'Barrage of Boulders deals damage only to creatures you don’t control as it resolves. However, the ferocious ability affects all creatures, including ones you control and ones that weren’t on the battlefield as Barrage of Boulders resolved.').
card_ruling('barrage of boulders', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('barrage of boulders', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('barrage of expendables', '2013-07-01', 'Barrage of Expendables deals the damage, not the sacrificed creature. Notably, that creature having deathtouch or lifelink provides no additional benefit.').
card_ruling('barrage of expendables', '2013-07-01', 'Because targets are chosen before costs are paid, it is possible to choose a creature as the target of the ability and then sacrifice that creature. The ability will be countered when it tries to resolve and no damage will be dealt.').

card_ruling('barrage tyrant', '2015-08-25', 'Barrage Tyrant deals the damage, not the sacrificed creature. For example, if the sacrificed creature had lifelink, you won’t gain life.').
card_ruling('barrage tyrant', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('barrage tyrant', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('barrage tyrant', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('barrage tyrant', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('barrage tyrant', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('barrel down sokenzan', '2005-06-01', 'The lands are returned on resolution; this is not a cost of activating the ability.').

card_ruling('barreling attack', '2004-10-04', 'Both the +1/+1 and trample last only until end of turn.').
card_ruling('barreling attack', '2012-05-01', 'Upon resolution, this gives the targeted creature trample and creates a delayed-triggered ability that will trigger when the creature becomes blocked. The creature will get +1/+1 for each creature blocking it as that ability resolves. This means that if Barreling Attack is cast after blockers are declared, the targeted creature will gain trample but won\'t be able to get +1/+1 because it\'s too late for the ability to trigger (unless there is another combat phase in the turn).').

card_ruling('barren glory', '2007-05-01', 'Both conditions need to be true for Barren Glory\'s ability to trigger, and they both need to be true when the ability resolves or it will do nothing. In between, you can control permanents and/or have cards in hand.').

card_ruling('barren moor', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('barrenton medic', '2008-05-01', 'You put the -1/-1 counter on Barrenton Medic as a cost. That means it happens when you activate the ability, not when it resolves. If paying the cost causes the creature to have 0 toughness, it\'s put into the graveyard before you can untap it and before you can even pay the cost again.').

card_ruling('barrin\'s codex', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('barrin\'s spite', '2008-05-01', 'The player chooses which one to sacrifice as the spell resolves. If only one of the targets is still legal at that time, then the player must choose that one to sacrifice.').

card_ruling('barrow ghoul', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('barrow ghoul', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('barrow ghoul', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('barrow ghoul', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('barrow ghoul', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').
card_ruling('barrow ghoul', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('barrow ghoul', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('bartel runeaxe', '2004-10-04', 'Bartel Runeaxe can be targeted by the abilities of enchantments, including Auras.').
card_ruling('bartel runeaxe', '2004-10-04', 'This card can be enchanted by moving Auras onto it. The \"can\'t be the target\" text is different from \"can\'t be enchanted\" in that it only applies to Aura spells when they are being cast.').

card_ruling('barter in blood', '2012-05-01', 'The active player chooses which creatures will be sacrificed first, then each other player in turn order does the same. Then all creatures are sacrificed simultaneously.').
card_ruling('barter in blood', '2012-05-01', 'If a player controls only one creature, that creature is sacrificed.').
card_ruling('barter in blood', '2012-05-01', 'Barter in Blood doesn\'t target any creatures and may be cast even if a player controls fewer than two creatures.').

card_ruling('baru, fist of krosa', '2007-05-01', 'The first ability triggers when a Forest enters the battlefield under any player\'s control, but only your creatures get the bonus.').
card_ruling('baru, fist of krosa', '2007-05-01', 'The size of the Wurm token is determined when the activated ability resolves. It won\'t change as the number of lands you control changes.').

card_ruling('basal sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('basal sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('basalt golem', '2005-11-01', 'The token creature is put onto the battlefield at the end of combat only if the blocking creature is sacrificed during the resolution of the triggered ability.').

card_ruling('basalt monolith', '2004-10-04', 'It can be untapped as often as you can pay for it.').

card_ruling('basandra, battle seraph', '2011-09-22', 'While Basandra, Battle Seraph is on the battlefield, players can\'t cast any spells during the combat phase, including permanent spells with flash.').
card_ruling('basandra, battle seraph', '2011-09-22', 'Players may still activate abilities during combat.').
card_ruling('basandra, battle seraph', '2011-09-22', 'If, during a player\'s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('basilica guards', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('basilica guards', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('basilica guards', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('basilica screecher', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('basilica screecher', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('basilica screecher', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('basking rootwalla', '2006-02-01', 'If this card\'s ability is activated by one player, then another player takes control of it on the same turn, the second player can\'t activate its ability that turn.').

card_ruling('bathe in light', '2005-10-01', 'The color you choose as the spell resolves has nothing to do with the color or colors shared by the affected creatures.').
card_ruling('bathe in light', '2005-10-01', 'All creatures that share a color are affected, even your own.').
card_ruling('bathe in light', '2005-10-01', 'A creature \"shares a color\" with any creature that is at least one of its colors. For example, a green-white creature shares a color with creatures that are green, white, green-white, red-white, black-green, and so on.').
card_ruling('bathe in light', '2005-10-01', 'If it targets a colorless creature, it doesn\'t affect any other creatures. A colorless creature shares a color with nothing, not even other colorless creatures.').
card_ruling('bathe in light', '2005-10-01', 'You check which creatures share a color with the target when the spell resolves.').
card_ruling('bathe in light', '2005-10-01', 'Only one creature is targeted. If that creature leaves the battlefield or otherwise becomes an illegal target, the entire spell is countered. No other creatures are affected.').

card_ruling('baton of morale', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('baton of morale', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('baton of morale', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('baton of morale', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('battering krasis', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('battering krasis', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('battering krasis', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('battering krasis', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('battering krasis', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('battering krasis', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('battering ram', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('battering ram', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('battering ram', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('battering ram', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('battering sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('battering sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('battering wurm', '2006-02-01', 'Battering Wurm compares each blocker\'s power against its own as blockers are being declared.').

card_ruling('batterskull', '2011-06-01', 'The ability to return Batterskull to its owner\'s hand can only be activated if Batterskull is on the battlefield. If Batterskull is no longer on the battlefield when the ability resolves, it will have no effect.').

card_ruling('battle brawler', '2014-11-24', 'If Battle Brawler assigns combat damage during the first combat damage step, and then it loses first strike before the second combat damage step (perhaps because you no longer control a red or white permanent), it won’t assign combat damage again in the second combat damage step.').

card_ruling('battle hymn', '2012-05-01', 'Battle Hymn uses the stack and can be responded to. The amount of mana you add to your mana pool is based on the number of creatures you control when Battle Hymn resolves.').

card_ruling('battle mastery', '2014-07-18', 'If a creature loses double strike after dealing combat damage in the first combat damage step, it won’t assign combat damage in the second one.').

card_ruling('battle of wits', '2012-07-01', 'Battle of Wits has an \"intervening \'if\' clause.\" This means that if you don\'t have 200 or more cards in your library at the beginning of your upkeep, the ability won\'t trigger. If the ability does trigger, but you have fewer than 200 cards in your library when the ability resolves, the ability will do nothing.').
card_ruling('battle of wits', '2012-07-01', 'In tournaments, you must be able to shuffle your entire deck within a reasonable amount of time.').

card_ruling('battle sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('battle sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('battle-rattle shaman', '2010-06-15', 'This ability triggers when the beginning of combat step starts during each of your turns. It resolves before you declare attackers.').
card_ruling('battle-rattle shaman', '2010-06-15', 'If a spell or ability (such as World at War) causes your turn to have multiple combat phases, this ability triggers during each of them.').

card_ruling('battlefield forge', '2015-06-22', 'The damage dealt to you is part of the second mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('battlefield forge', '2015-06-22', 'Like most lands, each land in this cycle is colorless. The damage dealt to you is dealt by a colorless source.').

card_ruling('battlefield medic', '2004-10-04', 'The value of X is determined when this resolves, not later when damage would be dealt.').

card_ruling('battlefield scrounger', '2004-10-04', 'Check the number of cards in your graveyard before activating the ability. If activating the ability causes that number to fall below seven, the ability still resolves as normal.').

card_ruling('battlefield thaumaturge', '2014-04-26', 'Battlefield Thaumaturge’s first ability doesn’t change that spell’s mana cost or converted mana cost. It changes only the amount you actually pay (the “total cost” of the spell).').
card_ruling('battlefield thaumaturge', '2014-04-26', 'To calculate the total cost of an instant or sorcery spell you’re casting, start with the mana cost (or alternative cost, if applicable), add all cost increases (including additional costs such as those imposed by strive abilities), then subtract all cost reductions (such as the one created by Battlefield Thaumaturge’s first ability).').
card_ruling('battlefield thaumaturge', '2014-04-26', 'The cost reduction created by Battlefield Thaumaturge’s first ability can reduce only the generic portion of the spell’s total cost. Colored mana requirements aren’t affected.').
card_ruling('battlefield thaumaturge', '2014-04-26', 'The cost reduction is {1} per target creature, even if the spell targets a creature more than once.').
card_ruling('battlefield thaumaturge', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('battlefield thaumaturge', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('battlefield thaumaturge', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('battleflight eagle', '2012-07-01', 'Battleflight Eagle\'s ability is mandatory, although you can choose Battleflight Eagle as the target.').

card_ruling('battlefront krushok', '2014-11-24', 'The second ability applies only as blockers are declared. If a creature you control with no +1/+1 counters on it becomes blocked by more than one creature, putting a +1/+1 counter on it won’t change or undo the block.').

card_ruling('battlegate mimic', '2008-08-01', 'The ability triggers whenever you cast a spell that\'s both of its listed colors. It doesn\'t matter whether that spell also happens to be any other colors.').
card_ruling('battlegate mimic', '2008-08-01', 'If you cast a spell that\'s the two appropriate colors for the second time in a turn, the ability triggers again. The Mimic will once again become the power and toughness stated in its ability, which could overwrite power- and toughness-setting effects that have been applied to it in the meantime.').
card_ruling('battlegate mimic', '2008-08-01', 'Any other abilities the Mimic may have gained are not affected.').
card_ruling('battlegate mimic', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('battlegrace angel', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('battlegrace angel', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('battlegrace angel', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('battlegrace angel', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('battlegrace angel', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('battlegrace angel', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').
card_ruling('battlegrace angel', '2009-10-01', 'Battlegrace Angel could cause a creature to have multiple instances of lifelink. For example, a creature you control that already has lifelink could attack alone while you control Battlegrace Angel, or a creature you control could attack alone while you control more than one Battlegrace Angel. If a creature has multiple instances of lifelink, they are redundant. You\'ll still only gain life equal to the damage dealt.').

card_ruling('battletide alchemist', '2008-04-01', 'You count the number of Clerics you control at the time the damage would be dealt.').
card_ruling('battletide alchemist', '2008-04-01', 'If the same source would deal damage to a player at different times, you may prevent X of that damage each time.').
card_ruling('battletide alchemist', '2008-04-01', 'If a source would deal damage to multiple players at the same time, you may prevent X damage that source would deal to each of those players. The choice for each player is made separately.').
card_ruling('battletide alchemist', '2008-04-01', 'The player who would be dealt damage chooses the order in which to apply prevention and replacement effects that would affect that damage. This includes effects that involve choices, like the prevention effect from Battletide Alchemist\'s ability and the redirection effect that\'s part of the planeswalker rules. Those choices (such as whether you\'ll actually use Battletide Alchemist\'s ability to prevent that damage) are made as the relevant effects are applied.').

card_ruling('battlewise hoplite', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('battlewise hoplite', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('battlewise hoplite', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('battlewise hoplite', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('battlewise hoplite', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('battlewise hoplite', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('battlewise hoplite', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('battlewise valor', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('battlewise valor', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('battlewise valor', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('battlewise valor', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('batwing brume', '2008-08-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('batwing brume', '2008-08-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('batwing brume', '2008-08-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').

card_ruling('bayou', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('bayou', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('bazaar krovod', '2012-10-01', 'Bazaar Krovod\'s ability can target an attacking creature that\'s untapped (such as one with vigilance).').
card_ruling('bazaar krovod', '2012-10-01', 'Untapping an attacking creature doesn\'t cause it to stop attacking or remove it from combat.').

card_ruling('bazaar of baghdad', '2004-10-04', 'You can\'t cast spells or activate mana abilities before discarding the extra cards.').

card_ruling('bazaar of wonders', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('bazaar trader', '2010-03-01', 'This effect has no duration. The targeted player retains control of the targeted permanent until the game ends, the permanent leaves the battlefield, or an effect causes someone else to gain control of it.').
card_ruling('bazaar trader', '2010-03-01', 'If either the targeted player or the targeted permanent is an illegal target by the time the ability resolves, the ability does nothing. The player won\'t gain control of the permanent.').
card_ruling('bazaar trader', '2010-03-01', 'You may target yourself with Bazaar Trader\'s ability. Normally, this won\'t have any visible effect. However, the ability would override an effect with a limited duration that gave you control of a permanent. For example, if you temporarily gained control of a creature with Act of Treason, targeting yourself and that creature with Bazaar Trader\'s ability would then cause you to gain control of the creature indefinitely.').

card_ruling('beacon of creation', '2007-07-15', 'If a Beacon is countered, it\'s put into its owner\'s graveyard, not shuffled into the library.').

card_ruling('beacon of destruction', '2007-07-15', 'If a Beacon is countered, it\'s put into its owner\'s graveyard, not shuffled into the library.').

card_ruling('beacon of immortality', '2007-02-01', 'If you double a negative life total, you do the real math. A life total of -10 becomes -20.').
card_ruling('beacon of immortality', '2007-07-15', 'Beacon of Immortality\'s effect counts as life gain (or life loss, if the life total was negative) for effects that trigger on or replace life gain (or life loss).').
card_ruling('beacon of immortality', '2007-07-15', 'If a Beacon is countered, it\'s put into its owner\'s graveyard, not shuffled into the library.').

card_ruling('beacon of tomorrows', '2007-07-15', 'If a Beacon is countered, it\'s put into its owner\'s graveyard, not shuffled into the library.').

card_ruling('beacon of unrest', '2007-07-15', 'If a Beacon is countered, it\'s put into its owner\'s graveyard, not shuffled into the library.').

card_ruling('bear umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('bear umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('bear umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('bear umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('bear umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('bear umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('bear umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('bear umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('bear umbra', '2010-06-15', 'When the enchanted creature attacks, all lands controlled by that creature\'s controller (who is not necessarily the Aura\'s controller) untap.').
card_ruling('bear umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('bear umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('bear umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('bearer of the heavens', '2014-04-26', 'The effect of the ability will destroy all permanents on the battlefield as it resolves. It doesn’t matter if those permanents were on the battlefield when Bearer of the Heavens died.').
card_ruling('bearer of the heavens', '2014-04-26', 'If Bearer of the Heavens dies during an end step, the delayed triggered ability will trigger at the beginning of the next turn’s end step.').

card_ruling('beast hunt', '2009-10-01', 'If there are three or fewer cards in your library, you\'ll reveal all of them.').

card_ruling('beast walkers', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('beast walkers', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('beast walkers', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('beast walkers', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('beast within', '2011-06-01', 'If the permanent is an illegal target when Beast Within tries to resolve, the spell will be countered and none of its effects will happen. The permanent\'s controller won\'t get a Beast token.').
card_ruling('beast within', '2013-07-01', 'If the permanent is still a legal target but is not destroyed (perhaps because it regenerated or has indestructible), its controller still gets the Beast token.').

card_ruling('beastbreaker of bala ged', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('beastbreaker of bala ged', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('beastbreaker of bala ged', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('beastbreaker of bala ged', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('beastbreaker of bala ged', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('beastcaller savant', '2015-08-25', 'Mana produced by Beastcaller Savant can be spent on any part of a creature spell’s total cost, including additional costs (such as kicker costs) and alternative costs (such as dash costs). It can’t be spent to pay the costs of abilities of creatures you control.').
card_ruling('beastcaller savant', '2015-08-25', 'Mana produced by Beastcaller Savant can’t be used to activate an ability or cast an instant or sorcery spell that creates creature tokens.').

card_ruling('beastmaster ascension', '2009-10-01', 'If you attack with multiple creatures, Beastmaster Ascension\'s first ability triggers multiple times.').

card_ruling('beck', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('beck', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('beck', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('beck', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('beck', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('beck', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('beck', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('beck', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('beck', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('beck', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('beck', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('beck', '2013-04-15', 'Beck triggers when any creature enters the battlefield, no matter who controls it.').
card_ruling('beck', '2013-04-15', 'If you cast Beck/Call as a fused split spell, the triggered ability it creates will be in effect when the Bird creature tokens enter the battlefield. You\'ll draw up to four cards.').

card_ruling('beckon apparition', '2008-08-01', 'The controller of Beckon Apparition gets the token.').
card_ruling('beckon apparition', '2013-01-24', 'Beckon Apparition will be on the stack when you choose its target. It can\'t target itself.').
card_ruling('beckon apparition', '2013-01-24', 'If the target card is an illegal target when Beckon Apparition tries to resolve (if it\'s no longer in the graveyard, for instance), the spell will be countered and none of its effects will happen. You won\'t get a Spirit token.').

card_ruling('beetleform mage', '2013-04-15', 'If Beetleform Mage\'s ability is activated by one player, then another player gains control of it during the same turn, the second player can\'t activate its ability that turn.').
card_ruling('beetleform mage', '2013-04-15', 'Activating Beetleform Mage\'s ability after it\'s been blocked by a creature without flying or reach won\'t cause it to become unblocked.').

card_ruling('befoul', '2004-10-04', 'This is not modal. You do not have to choose the target type. The target has to either be a land or non-black creature when announced and on resolution.').

card_ruling('beguiler of wills', '2011-01-22', 'In order to activate the ability, you must be able to target a creature with power less than or equal to the number of creatures you control. The comparison is made again when the ability resolves. If the power is greater than the number of creatures you control at that time, the target is illegal and the ability is countered.').
card_ruling('beguiler of wills', '2011-01-22', 'The control-changing effect lasts indefinitely, even if the power of the creature becomes greater than the number of creatures you control after the ability resolves.').

card_ruling('behemoth sledge', '2009-10-01', 'Multiple instances of lifelink are redundant. If a creature with multiple instances of lifelink deals damage, its controller still only gains life equal to the damage dealt.').

card_ruling('behemoth\'s herald', '2008-10-01', 'To activate the ability, you must sacrifice three different creatures. You can\'t sacrifice just one three-colored creature, for example.').
card_ruling('behemoth\'s herald', '2008-10-01', 'You may sacrifice multicolored creatures as part of the cost of the search ability. If you sacrifice a multicolored creature this way, specify which part of the cost that creature is satisfying.').
card_ruling('behemoth\'s herald', '2008-10-01', 'You may sacrifice the Herald itself to help pay for the cost of its ability.').

card_ruling('belbe\'s portal', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('belligerent hatchling', '2008-08-01', 'If you cast a spell that\'s both of the listed colors, both abilities will trigger. You\'ll remove a total of two -1/-1 counters from the Hatchling.').
card_ruling('belligerent hatchling', '2008-08-01', 'If there are no -1/-1 counters on it when the triggered ability resolves, the ability does nothing. There is no penalty for not being able to remove a counter.').

card_ruling('belligerent sliver', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('belligerent sliver', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('belligerent sliver', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('belligerent whiptail', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('belligerent whiptail', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('belligerent whiptail', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('bellowing saddlebrute', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('bellowing saddlebrute', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('belltoll dragon', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('belltoll dragon', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('belltoll dragon', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('benalish commander', '2007-02-01', 'Both instances of X in the suspend ability are the same. You determine the value of X as you suspend the card from your hand. The value you choose must be at least 1.').
card_ruling('benalish commander', '2007-02-01', 'If this is suspended, then when the last time counter is removed from it, both its triggered ability and the \"play this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('benalish commander', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('benalish commander', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('benalish commander', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('benalish commander', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('benalish commander', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('benalish commander', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('benalish commander', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('benalish commander', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('benalish commander', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('benalish commander', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('benalish hero', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('benalish hero', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('benalish hero', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('benalish hero', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('benalish infantry', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('benalish infantry', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('benalish infantry', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('benalish infantry', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('benalish missionary', '2004-10-04', 'Works even if the creature is \"blocked\" by an effect rather than actual creatures.').

card_ruling('benediction of moons', '2006-02-01', '\"For each player\" means for each player currently in the game, which is normally two. A Two-Headed Giant game has four players. In a Free-for-All multiplayer game, players who have been eliminated or who are outside your range of influence don\'t count toward the total.').

card_ruling('benevolent offering', '2014-11-07', 'In some unusual cases, the Spirit tokens will have a toughness of 0 or less as they enter the battlefield, perhaps because an opponent controls an enchantment that gives your creatures -1/-1. Because state-based actions aren’t performed in the middle of a spell’s resolution, you’ll gain 2 life for each of those Spirit tokens. After Benevolent Offering has finished resolving, the Spirit tokens will die and subsequently cease to exist.').
card_ruling('benevolent offering', '2014-11-07', 'You may choose the same opponent for each of the effects, or you may choose different opponents. None of the affected players are targets of the spell.').
card_ruling('benevolent offering', '2014-11-07', 'You choose the opponents for each effect as the spell resolves.').

card_ruling('benthic explorers', '2004-10-04', 'This ability is not targeted any more.').
card_ruling('benthic explorers', '2009-10-01', 'Untapping a tapped land an opponent controls is part of the cost of Benthic Explorers\'s ability. If no opponent controls a tapped land, the ability can\'t be activated.').
card_ruling('benthic explorers', '2009-10-01', 'The types of mana are white, blue, black, red, green, and colorless.').
card_ruling('benthic explorers', '2009-10-01', 'Benthic Explorers checks the effects of all mana-producing abilities of the land untapped to pay its cost, but it doesn\'t check the costs of those abilities. For example, Vivid Crag has the ability \"{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.\" If you untap a tapped Vivid Crag an opponent controls while paying for Benthic Explorers\'s ability, you can add one mana of any color to your mana pool. It doesn\'t matter whether that Vivid Crag has a charge counter on it.').
card_ruling('benthic explorers', '2009-10-01', 'When determining what types of mana an opponent\'s land could produce, take into account any applicable replacement effects that would apply to that land\'s mana abilities (such as Contamination\'s effect, for example). If there is more than one, consider them in any possible order.').
card_ruling('benthic explorers', '2009-10-01', 'Benthic Explorers doesn\'t care about any restrictions or riders an opponent\'s land (such as Ancient Ziggurat or Hall of the Bandit Lord) puts on the mana it produces. It just cares about types of mana.').

card_ruling('benthic infiltrator', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('benthic infiltrator', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('benthic infiltrator', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('benthic infiltrator', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('benthic infiltrator', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('benthic infiltrator', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('benthic infiltrator', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('bereavement', '2004-10-04', 'If the player has no cards in hand, this has no effect.').

card_ruling('berserk', '2004-10-04', 'If the permanent stops being a creature before the end of the turn, it is still destroyed.').

card_ruling('berserkers of blood ridge', '2010-08-15', 'If, during your declare attackers step, Berserkers of Blood Ridge is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then it doesn\'t attack. If there\'s a cost associated with having Berserkers of Blood Ridge attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('berserkers of blood ridge', '2010-08-15', 'If there are multiple combat phases in a turn, Berserkers of Blood Ridge must attack only in the first one in which it\'s able to.').

card_ruling('beseech the queen', '2008-05-01', 'If an effect reduces the cost to cast a spell by an amount of generic mana, it applies to a monocolored hybrid spell only if you\'ve chosen a method of paying for it that includes generic mana.').
card_ruling('beseech the queen', '2008-05-01', 'A card with a monocolored hybrid mana symbol in its mana cost is each of the colors that appears in its mana cost, regardless of what mana was spent to cast it. Thus, Beseech the Queen is black even if you spend six red mana to cast it.').
card_ruling('beseech the queen', '2008-05-01', 'A card with monocolored hybrid mana symbols in its mana cost has a converted mana cost equal to the highest possible cost it could be cast for. Its converted mana cost never changes. Thus, Beseech the Queen has a converted mana cost of 6, even if you spend {B}{B}{B} to cast it.').
card_ruling('beseech the queen', '2008-05-01', 'If a cost includes more than one monocolored hybrid mana symbol, you can choose a different way to pay for each symbol. For example, you can pay for Beseech the Queen by spending {B}{B}{B}, {2}{B}{B}, {4}{B}, or {6}.').

card_ruling('betrayal', '2004-10-04', 'If it ever enchants a creature that isn\'t controlled by an opponent of Betrayal\'s controller, then it\'s put into the graveyard as a State-Based Action.').

card_ruling('betrothed of fire', '2008-04-01', 'If you control Betrothed of Fire, but it\'s attached to a creature you don\'t control, no one can activate the last ability. The creature\'s controller can\'t activate it because that player doesn\'t control the Aura. You can\'t activate it because you can\'t pay the cost -- you can\'t sacrifice a creature you don\'t control.').

card_ruling('bident of thassa', '2013-09-15', 'The controller of each attacking creature still chooses which player or planeswalker that creature attacks.').
card_ruling('bident of thassa', '2013-09-15', 'If, during a player’s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under that player’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, the player isn’t forced to pay that cost, so it doesn’t have to attack in that case either.').

card_ruling('bifurcate', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('bile blight', '2014-02-01', 'Bile Blight has only one target. Other creatures with that name are not targeted. For example, a creature with hexproof will still get -3/-3 if it has the same name as the target creature.').
card_ruling('bile blight', '2014-02-01', 'If the target creature is an illegal target as Bile Blight tries to resolve, it will be countered and none of its effects will happen. No creature will get -3/-3.').
card_ruling('bile blight', '2014-02-01', 'The name of a creature token is the same as its creature types, unless the token is a copy of another creature or the effect that created the token specifically gives it a different name. For example, a 1/1 Cat Soldier creature token is named “Cat Soldier.”').

card_ruling('bind', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('biomantic mastery', '2006-05-01', 'You choose both targets before you draw any cards. The targets must be different. One of them may be you.').

card_ruling('biomass mutation', '2013-01-24', 'Choosing 0 as the value for X will likely cause creatures you control to become 0/0 and be put into the graveyard.').
card_ruling('biomass mutation', '2013-01-24', 'Biomass Mutation overwrites any effects that set the power and/or toughness of a creature you control to a specific value. Effects that modify power and/or toughness but don\'t set them to a specific value (like the one created by Giant Growth), power/toughness changes from counters, and effects that switch a creature\'s power and toughness will continue to apply. For example, say you control a 1/1 creature that\'s getting +4/+2 from an Aura, has a +1/+1 counter on it, and is affected by an effect that switches its power and toughness. This creature is 4/6. If you cast Biomass Mutation with X equal to 4, the creature would become 7/9 (4/4, +4/+2 from the Aura, +1/+1 from the counter, then switch power and toughness).').

card_ruling('bioplasm', '2014-02-01', 'The reminder text printed on Bioplasm is no longer correct. Abilities that define a creature\'s power (and toughness) apply in every zone. The value of * is defined by such an ability.').

card_ruling('biorhythm', '2004-10-04', 'If a player controls no creatures when this resolves, their life total will become zero and he or she will lose the game.').
card_ruling('biorhythm', '2004-10-04', 'This spell causes the player to lose or gain enough life to change to the new life total.').

card_ruling('bioshift', '2013-01-24', 'To move a counter from one creature to another, the counter is removed from the first creature and placed on the second. Any abilities that care about a counter being removed or placed on a creature will apply.').
card_ruling('bioshift', '2013-01-24', 'You decide how many counters to move when Bioshift resolves.').
card_ruling('bioshift', '2013-01-24', 'If one of the two creatures is an illegal target when Bioshift tries to resolve, or if the creatures are controlled by different players at that time, no counters will move.').

card_ruling('biovisionary', '2013-01-24', 'Token creatures that are a copy of Biovisionary will count.').
card_ruling('biovisionary', '2013-01-24', 'If you don\'t control four or more creatures named Biovisionary at the beginning of the end step, the ability won\'t trigger.').
card_ruling('biovisionary', '2013-01-24', 'If the ability does trigger, but you don\'t control four or more creatures named Biovisionary when the ability tries to resolve, the ability will do nothing.').

card_ruling('birchlore rangers', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('birchlore rangers', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('birthing pod', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('birthing pod', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('birthing pod', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('birthing pod', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('birthing pod', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('birthing pod', '2011-06-01', 'A creature\'s converted mana cost is determined solely by the mana symbols printed in its upper right corner (unless that creature is copying something else; see below). If its mana cost includes {X}, X is considered to be 0. If it has no mana symbols in its upper right corner (because it\'s an animated land, for example), its converted mana cost is 0. Ignore any alternative costs or additional costs (such as kicker) paid when the creature was cast.').
card_ruling('birthing pod', '2011-06-01', 'A token has a converted mana cost of 0, unless it is copying something else.').
card_ruling('birthing pod', '2011-06-01', 'If a creature is copying something else, its converted mana cost is the converted mana cost of whatever it\'s copying.').

card_ruling('bite of the black rose', '2014-05-29', 'The effect will be the same for all your opponents, no matter how they voted.').
card_ruling('bite of the black rose', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('bite of the black rose', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('bite of the black rose', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('bite of the black rose', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('bite of the black rose', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('bitter feud', '2014-11-07', 'Bitter Feud applies to damage dealt by any source controlled by one of the chosen players, not just combat damage.').
card_ruling('bitter feud', '2014-11-07', 'The source of the damage doesn’t change. A spell that deals damage will specify the source of the damage. This is usually the spell itself. An ability that deals damage will specify the source of the damage, although it will never be the ability itself. Usually the source of the ability is also the source of the damage.').
card_ruling('bitter feud', '2014-11-07', 'If there are two Bitter Feuds on the battlefield, and the same two players were chosen for each, damage dealt will be doubled for each. So, two Bitter Feuds will end up multiplying the damage by four, three will multiply the damage by eight, and four by sixteen.').
card_ruling('bitter feud', '2014-11-07', 'If multiple effects modify how damage is dealt, the player being dealt damage, or the controller of the permanent being dealt damage, chooses the order in which to apply the effects. For example, the ability of Decorated Griffin says “{1}{W}: Prevent the next 1 combat damage that would be dealt to you this turn.” Suppose you control a Decorated Griffin, and you and an opponent are the chosen players for a Bitter Feud. If a creature that player controls would deal 3 combat damage to you, and Decorated Griffin’s ability has resolved once, you can choose to either (a) apply the effect from Decorated Griffin first and prevent 1 damage, and then let Bitter Feud’s effect double the remaining 2 damage, for a result 4 damage being dealt to you, or (b) let Bitter Feud’s effect apply first and double the damage to 6, and then apply the effect from Decorated Griffin to prevent 1 damage, for a result of 5 damage being dealt to you.').

card_ruling('bitterblossom', '2008-04-01', 'The effect is mandatory. You\'ll lose 1 life even if you have only 1 life left.').
card_ruling('bitterblossom', '2008-04-01', 'The life loss isn\'t a payment. You\'ll get a token even if you had 0 life (and other effect is stopping you from losing the game).').

card_ruling('bitterheart witch', '2011-09-22', 'The Curse must be legally able to enchant the player. For example, if the player has protection from red, you couldn\'t put a red Curse onto the battlefield this way.').

card_ruling('bituminous blast', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('bituminous blast', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('bituminous blast', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('bituminous blast', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('bituminous blast', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('bituminous blast', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('bituminous blast', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('black carriage', '2008-05-01', 'The ability that untaps it during your upkeep has been returned to an activated ability. There is no restriction on how many times it can be untapped during your upkeep with this ability.').

card_ruling('black mana battery', '2004-10-04', 'Can be tapped even if it has no counters.').

card_ruling('black market', '2004-10-04', 'Getting the mana is not optional.').

card_ruling('black oak of odunos', '2014-02-01', 'You can tap a creature that hasn’t been under your control since your most recent turn began to activate the last ability.').

card_ruling('black sun\'s zenith', '2011-06-01', 'If this spell is countered, none of its effects occur. In particular, it will go to the graveyard rather than to its owner\'s library.').

card_ruling('black vise', '2004-10-04', 'You choose one opposing player as it enters the battlefield and it only affects that one player. This choice is not changed even if Black Vise changes controllers. It becomes useless but stays on the battlefield if that player leaves the game.').
card_ruling('black vise', '2009-10-01', 'If the chosen player has four or fewer cards in his or her hand as Black Vise\'s ability resolves, the ability just won\'t do anything that turn.').

card_ruling('blackcleave goblin', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('blackcleave goblin', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('blackcleave goblin', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('blackcleave goblin', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('blackcleave goblin', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('blackcleave goblin', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('blackmail', '2004-10-04', 'If the player has less than 3 cards, all of them are revealed.').

card_ruling('blade of the bloodchief', '2009-10-01', 'Blade of the Bloodchief\'s ability triggers regardless of who controlled the creature and whose graveyard it was put into.').
card_ruling('blade of the bloodchief', '2009-10-01', 'The +1/+1 counters are put on the equipped creature, not Blade of the Bloodchief. If Blade of the Bloodchief is moved to a different creature, the +1/+1 counters will stay where they are.').
card_ruling('blade of the bloodchief', '2009-10-01', 'The creature that gets the counters is the one that\'s equipped by Blade of the Bloodchief at the time the ability resolves. It doesn\'t matter what creature Blade of the Bloodchief was attached to (if any) when it triggered.').
card_ruling('blade of the bloodchief', '2009-10-01', 'If Blade of the Bloodchief leaves the battlefield before its triggered ability resolves, the counters are put on the creature it was attached to at the time it left the battlefield. If it wasn\'t attached to a creature at that time, nothing gets the counters.').
card_ruling('blade of the bloodchief', '2009-10-01', 'If the equipped creature itself is put into a graveyard, Blade of the Bloodchief\'s ability will trigger. However, it won\'t do anything when it resolves unless a spell or ability has caused it to become attached to another creature by that time. (Remember that you may activate an equip ability only as a sorcery.)').
card_ruling('blade of the bloodchief', '2009-10-01', 'If the equipped creature and another creature are dealt lethal damage at the same time, the equipped creature will be put into a graveyard before it would receive any counters.').

card_ruling('blade sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('blade sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('blade-tribe berserkers', '2011-01-01', 'Once the metalcraft ability resolves, Blade-Tribe Berserkers retains its bonuses for the rest of the turn, even if you cease to control three or more artifacts for some reason.').

card_ruling('blades of velis vel', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('bladetusk boar', '2009-10-01', 'Intimidate looks at the current colors of a creature that has it. Normally, Bladetusk Boar can\'t be blocked except by artifact creatures and/or red creatures. If it\'s turned blue, then it can\'t be blocked except by artifact creatures and/or blue creatures.').
card_ruling('bladetusk boar', '2009-10-01', 'If an attacking creature has intimidate, what colors it is matters only as the defending player declares blockers. Once it\'s blocked, changing its colors won\'t change that.').

card_ruling('blanket of night', '2004-10-04', 'The land can now be tapped for black mana in addition to any other abilities it already has. This works just as if the text \"{Tap}: Add {B} to your mana pool\" was added to each mana-producing land. In other words, the land can be tapped for its own ability _or_ it can be tapped for black mana. Not both.').
card_ruling('blanket of night', '2004-10-04', 'Whether the land is a basic land or not is unchanged by this effect.').
card_ruling('blanket of night', '2004-10-04', 'More than one Blanket of Night has no meaningful additional effect.').
card_ruling('blanket of night', '2004-10-04', 'Anything which counts the number of Swamps on the battlefield will not double count an actual Swamp card while Blanket of Night is on the battlefield.').
card_ruling('blanket of night', '2004-10-04', 'It can affect lands which do not produce mana.').
card_ruling('blanket of night', '2004-10-04', 'The lands do not get the name \"Swamp\" in addition to their current name. This is a reversal of older rulings.').
card_ruling('blanket of night', '2004-10-04', 'The ability is a land type change effect so it is ordered along with other land type change effects in the order they entered the battlefield.').
card_ruling('blanket of night', '2006-10-15', 'If this effect is applied to a snow land, the land is now a snow Swamp and not just a regular Swamp.').

card_ruling('blasphemous act', '2011-09-22', 'The total cost to cast Blasphemous Act is locked in before you pay that cost. For example, if there are three creatures on the battlefield, including one you can sacrifice to add {1} to your mana pool, the total cost of Blasphemous Act is {5}{R}. Then you can sacrifice the creature when you activate mana abilities just before paying the cost.').
card_ruling('blasphemous act', '2011-09-22', 'Blasphemous Act\'s ability can\'t reduce the total cost to cast the spell below {R}.').
card_ruling('blasphemous act', '2011-09-22', 'Although players may respond to Blasphemous Act once it\'s been cast, once it\'s announced, they can\'t respond before the cost is calculated and paid.').

card_ruling('blast of genius', '2013-04-15', 'You choose the target creature or player as part of casting the spell. If that creature or player is an illegal target when Blast of Genius tries to resolve, it will be countered and none of its effects will happen. You won\'t draw or discard any cards.').
card_ruling('blast of genius', '2013-04-15', 'If you discard a split card, Blast of Genius will deal damage equal the sum of its two converted mana costs as a single damage-dealing event.').
card_ruling('blast of genius', '2013-04-15', 'If you discard a card with {X} in its mana cost, the value of X is zero.').

card_ruling('blasted landscape', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('blastfire bolt', '2014-07-18', 'Blastfire Bolt can target any creature, not just one with Equipment attached to it. The damage will be dealt to the creature even if it doesn’t have any Equipment attached to it.').
card_ruling('blastfire bolt', '2014-07-18', 'If the target creature is an illegal target when Blastfire Bolt tries to resolve, it will be countered and none of its effects will happen. No Equipment will be destroyed.').

card_ruling('blatant thievery', '2004-10-04', 'If a permanent changes controller after being targeted but before this spell resolves, you won\'t gain control of that permanent.').
card_ruling('blatant thievery', '2004-10-04', 'You gain control of only one permanent from each player.').

card_ruling('blaze commando', '2013-04-15', 'Blaze Commando\'s ability triggers each time an instant or sorcery spell you control deals damage (or, put another way, the number of times the word “deals” appears in its instructions), no matter how much damage is dealt or how many players or permanents are dealt damage. For example, if you cast Punish the Enemy and it “deals 3 damage to target player and 3 damage to target creature,” Blaze Commando\'s ability will trigger once and you\'ll get two Soldier tokens.').
card_ruling('blaze commando', '2013-04-15', 'Some instant or sorcery spells cause other objects to deal damage, rather than deal damage themselves. Such spells don\'t cause Blaze Commando to trigger.').

card_ruling('blaze of glory', '2004-10-04', 'Does not allow a tapped creature to block, or allow a creature to block any creatures it would not normally be able to block.').
card_ruling('blaze of glory', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s before the beginning of that phase\'s Declare Blockers Step.').

card_ruling('blazing archon', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('blazing effigy', '2004-10-04', 'If it dies and there are no creatures to target, then the ability does nothing.').

card_ruling('blazing torch', '2009-10-01', 'If a Blazing Torch controlled by one player somehow winds up equipping a creature a different player controls, the damage ability can\'t be activated by either player. Only the creature\'s controller may activate the ability -- but since that player can\'t sacrifice Blazing Torch (a permanent he or she doesn\'t control), the ability\'s cost can\'t be paid.').
card_ruling('blazing torch', '2009-10-01', 'The source of the damage is Blazing Torch, not the equipped creature. However, the equipped creature\'s ability is what targets the creature or player. If Blazing Torch is equipped to a red creature, for example, the ability couldn\'t target a creature with protection from red. It could target a creature with protection from artifacts, but all the damage would be prevented.').

card_ruling('blessed breath', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('blessed breath', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('blessed breath', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('blessed breath', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('blessed reincarnation', '2015-02-25', 'If there are no creature cards in the library, it will be revealed and shuffled. The target creature will remain exiled.').
card_ruling('blessed reincarnation', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('blessed reincarnation', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('blessed reincarnation', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('blessed reincarnation', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('blessed reincarnation', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('blessed reincarnation', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('blessed reincarnation', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('blessed reincarnation', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('blessed reversal', '2004-10-04', 'The creatures are counted on resolution.').

card_ruling('blessed spirits', '2015-06-22', 'Blessed Spirits’s ability will resolve before the enchantment spell that caused it to trigger.').

card_ruling('blessed wind', '2004-10-04', 'This counts as gaining or losing life as appropriate.').

card_ruling('blessings of nature', '2012-05-01', 'You may choose up to four target creatures. You declare how many counters will be placed on each target creature as you cast Blessings of Nature. Each target must receive at least one counter.').

card_ruling('blight herder', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('blight herder', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('blight herder', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('blight herder', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').
card_ruling('blight herder', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('blight herder', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('blight herder', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('blight herder', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('blight mamba', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('blight mamba', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('blight mamba', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('blight mamba', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('blight mamba', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('blight mamba', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('blightcaster', '2013-07-01', 'If you are the only player who controls a creature when Blightcaster’s ability triggers, you must choose one as the target, although you can choose not to give it -2/-2.').
card_ruling('blightcaster', '2015-06-22', 'Blightcaster’s ability will resolve before the enchantment spell that caused it to trigger.').
card_ruling('blightcaster', '2015-06-22', 'If you are the only player who controls a creature when Blightcaster’s ability triggers, you must choose one of those creatures as the target, although you can choose to not give it -2/-2.').

card_ruling('blighted steppe', '2015-08-25', 'Count the number of creatures you control as the last ability resolves to determine how much life to gain.').

card_ruling('blightning', '2008-10-01', 'The targeted player will discard two cards even if some or all of Blightning\'s damage is prevented or redirected.').

card_ruling('blightsteel colossus', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed or if its toughness is 0 or less. (In these cases, of course, Blightsteel Colossus would be shuffled into its owner\'s library instead of being put into its owner\'s graveyard.)').

card_ruling('blind creeper', '2004-12-01', 'This ability triggers off anyone casting a spell, including you, not just Blind Creeper\'s controller or an opponent.').

card_ruling('blind obedience', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('blind obedience', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('blind obedience', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('blind seer', '2004-10-04', 'If you change the color of a spell which is to become a permanent, the permanent will retain the color change until the end of the turn.').

card_ruling('blind zealot', '2011-06-01', 'You\'ll choose the target creature when the ability goes on the stack. You\'ll choose whether or not to sacrifice Blind Zealot when the ability resolves.').

card_ruling('blind-spot giant', '2013-06-07', 'Blind-Spot Giant checks if you control another Giant only as you declare it as an attacking or blocking creature. Once you do, if you lose control of your other Giants, Blind-Spot Giant won\'t stop attacking or blocking.').

card_ruling('blinding angel', '2004-10-04', 'If you are damaged by more than one, the effects are cumulative and you skip multiple combat phases.').

card_ruling('blinding beam', '2013-06-07', 'The second mode affects all creatures during the player\'s next untap step, including creatures controlled by other players (who may have untapped because of Seedborn Muse, for example) and creatures that weren\'t on the battlefield when Blinding Beam resolved.').

card_ruling('blinding flare', '2014-04-26', 'Blinding Flare can target any creature, including those you control.').
card_ruling('blinding flare', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('blinding flare', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('blinding flare', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('blinding flare', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('blinding flare', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('blinding light', '2004-10-04', 'Will not tap a creature that is both white and another color.').

card_ruling('blinding spray', '2014-09-20', 'Blinding Spray doesn’t affect creatures that enter the battlefield or come under the control of one of your opponents after it resolves.').
card_ruling('blinding spray', '2014-09-20', 'You can cast Blinding Spray even if your opponents control no creatures just to draw a card.').

card_ruling('blinkmoth infusion', '2004-12-01', 'If you control no artifacts, Blinkmoth Infusion costs twelve generic mana and two blue mana to cast.').
card_ruling('blinkmoth infusion', '2004-12-01', 'Blinkmoth Infusion untaps all artifacts on the battlefield, not just artifacts you control.').

card_ruling('blinkmoth nexus', '2004-12-01', 'Blinkmoth Nexus gains the creature type \"Blinkmoth\" when it becomes a creature. This means that it ends up with three types (land, artifact, and creature), plus one creature subtype (Blinkmoth).').
card_ruling('blinkmoth nexus', '2004-12-01', 'The \"target Blinkmoth gets +1/+1\" ability can target any creature with the creature type Blinkmoth.').
card_ruling('blinkmoth nexus', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('blinkmoth nexus', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('blinkmoth urn', '2004-10-04', 'The precombat main phase is the first main phase of the turn. All others are postcombat main phases, even if they technically occur before combat.').
card_ruling('blinkmoth urn', '2004-12-01', 'The \"precombat main phase\" is the first main phase of the turn. All other main phases are \"postcombat main phases.\"').

card_ruling('blister beetle', '2008-10-01', 'If there are no other creatures on the battlefield when Blister Beetle enters the battlefield, it must target itself.').

card_ruling('blistercoil weird', '2012-10-01', 'Blistercoil Weird will get +1/+1 and untap before that instant or sorcery spell resolves.').
card_ruling('blistercoil weird', '2012-10-01', 'Blistercoil Weird will get +1/+1 even if the instant or sorcery spell is countered or if Blistercoil Weird is untapped when its ability resolves.').
card_ruling('blistercoil weird', '2012-10-01', 'If you copy an instant or sorcery spell on the stack (using Nivix Guildmage\'s second ability, for example), the copy isn\'t cast. Blistercoil Weird\'s ability won\'t trigger.').

card_ruling('blisterpod', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('blisterpod', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('blisterpod', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('blisterpod', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('blisterpod', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('blisterpod', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('blisterpod', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('blisterpod', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('blisterpod', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('blitz hellion', '2009-05-01', 'Blitz Hellion\'s last ability triggers at the end of each turn in which it\'s on the battlefield. If it\'s not on the battlefield by the time this ability resolves, nothing happens. Blitz Hellion stays wherever it is, and its owner doesn\'t shuffle his or her library.').
card_ruling('blitz hellion', '2009-05-01', 'A token that\'s copying Blitz Hellion will be shuffled into its owner\'s library when this ability resolves, then will cease to exist. For practical purposes, the token should not actually be put into the library, though the library is still shuffled.').

card_ruling('blizzard specter', '2006-07-15', 'You choose the mode when you put Blizzard Specter\'s triggered ability on the stack.').
card_ruling('blizzard specter', '2006-07-15', 'If Blizzard Specter\'s first mode is chosen, the defending player chooses the permanent to return to hand when the ability resolves.').
card_ruling('blizzard specter', '2006-07-15', 'Blizzard Specter\'s second mode may be chosen even if the defending player\'s hand is empty.').

card_ruling('bloated toad', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('blood', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('blood', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('blood', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('blood', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('blood', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('blood', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('blood', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('blood', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('blood', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('blood', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('blood', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('blood', '2013-04-15', 'As Blood is resolving, if only one of the targets is legal, Blood will still resolve but will have no effect: If the first target is illegal, it can\'t deal damage to anything. If the second target creature or player is illegal, it can\'t be dealt damage.').
card_ruling('blood', '2013-04-15', 'The amount of damage dealt is based on the first target creature\'s power as Blood resolves.').
card_ruling('blood', '2013-04-15', 'If you cast Flesh // Blood as a fused split card, the +1/+1 counters will have been placed before the amount of damage is determined.').

card_ruling('blood artist', '2012-05-01', 'If Blood Artist and one or more other creatures die at the same time, its ability will trigger for each of those creatures.').

card_ruling('blood baron of vizkopa', '2013-04-15', 'In a Two-Headed Giant game, Blood Baron of Vizkopa gets +6/+6 and has flying as long as your team has 30 or more life and an opposing team has 10 or less life.').

card_ruling('blood clock', '2005-06-01', 'A player who controls no permanents doesn\'t need to pay 2 life (although that player can choose to do so).').
card_ruling('blood clock', '2005-06-01', 'A player with less than 2 life can\'t pay 2 life and must return a permanent.').
card_ruling('blood clock', '2005-06-01', 'The choice of which permanent to return or whether to pay life is made as the triggered ability resolves.').

card_ruling('blood crypt', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('blood crypt', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('blood crypt', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('blood cultist', '2008-10-01', 'Each time a creature is put into a graveyard from the battlefield, check whether Blood Cultist had dealt any damage to it at any time during that turn. (This includes combat damage.) If so, Blood Cultist\'s second ability will trigger. It doesn\'t matter who controlled the creature or whose graveyard it was put into.').
card_ruling('blood cultist', '2008-10-01', 'If Blood Cultist and a creature it dealt damage to are both put into a graveyard at the same time, Blood Cultist\'s ability will trigger, but it will do nothing when it resolves.').

card_ruling('blood feud', '2011-01-22', 'Blood Feud can target two creatures with the same controller.').

card_ruling('blood host', '2014-07-18', 'If Blood Host isn’t on the battlefield when its ability resolves, you won’t put a +1/+1 counter on it but you’ll still gain 2 life.').

card_ruling('blood knight', '2007-02-01', 'This is the timeshifted version of Black Knight.').

card_ruling('blood lust', '2005-11-01', 'Has changed slightly in functionality, with the old continuous nature of the toughness reduction gone away; the toughness change remains static. For example, if you have three Swamps and Nightmare on the battlefield and then cast Blood Lust on it, it will become 7/1. If one of your Swamps is subsequently destroyed, your Nightmare will become 6/0 and be put in the graveyard.').

card_ruling('blood moon', '2004-12-01', 'Blood Moon\'s effect doesn\'t affect supertypes and won\'t turn any land into a basic land.').
card_ruling('blood moon', '2013-06-07', 'Nonbasic lands will lose any other land types and abilities they had. They will become Mountains and gain the ability \"{T}: Add {R} to your mana pool.\"').
card_ruling('blood moon', '2013-06-07', 'If a nonbasic land has an ability that causes it to enter the battlefield tapped, that ability will still function. For example, if Blood Crypt is entering the battlefield, its controller may pay 2 life to have it enter untapped. Regardless of this choice, it will be only a Mountain and not a Swamp.').

card_ruling('blood pet', '2009-10-01', 'Because combat damage resolves immediately after it is assigned, you cannot sacrifice Blood Pet in between assigning and dealing combat damage. The last chance you have to sacrifice it before combat damage is dealt is during the Declare Blockers step.').

card_ruling('blood reckoning', '2012-07-01', 'Blood Reckoning will trigger once for each creature that attacks you or a planeswalker you control.').
card_ruling('blood reckoning', '2012-07-01', 'Creatures that are put onto the battlefield attacking were never declared as attackers and won\'t cause Blood Reckoning to trigger.').

card_ruling('blood scrivener', '2013-04-15', 'Any time you are instructed to draw more than one card, you draw them one at a time. For example, if you control Blood Scrivener and have no cards in hand and you\'re instructed to “draw two cards,” your first card draw is replaced by drawing two cards and losing 1 life, then you\'ll draw the second card from the original instruction. In total, you\'ll draw three cards and lose 1 life.').
card_ruling('blood scrivener', '2013-04-15', 'Each additional Blood Scrivener you control will effectively add one card and 1 life lost. Say you control two Blood Scriveners and would draw a card while you have no cards in hand. The effect of one Blood Scrivener will replace the event “draw a card” with “draw two cards and lose 1 life.” The effect of the other Blood Scrivener will replace the drawing of the first of those two cards with “draw two cards and lose 1 life.” You\'ll draw two cards and lose 1 life, then draw another card and lose another 1 life.').

card_ruling('blood seeker', '2011-09-22', 'Life loss is not the same as damage. Blood Seeker\'s ability will not cause creatures with bloodthirst to enter the battlefield with +1/+1 counters.').

card_ruling('blood speaker', '2004-12-01', 'The return-to-hand triggered ability triggers while Blood Speaker is in your graveyard, no matter how it got there.').

card_ruling('blood tribute', '2009-10-01', 'You can tap a creature that hasn\'t been under your control since your most recent turn began to pay the kicker cost.').

card_ruling('blood tyrant', '2009-02-01', 'Blood Tyrant\'s second ability causes each player to lose 1 life, including you. Blood Tyrant will count each player\'s life loss, including yours, when determining how many +1/+1 counters it gets.').
card_ruling('blood tyrant', '2009-02-01', 'Blood Tyrant\'s third ability will trigger no matter how a player loses the game: due to a state-based action (as a result of having a life total of 0 or less, trying to draw a card from an empty library, or having ten poison counters), a spell or ability that says that player loses the game, a concession, or a game loss awarded by a judge.').
card_ruling('blood tyrant', '2009-02-01', 'In a multiplayer game using the limited range of influence option (such as a Grand Melee game), if a spell or ability says that you win the game, it instead causes all of your opponents within your range of influence to lose the game. This is another way by which Blood Tyrant\'s third ability can trigger.').

card_ruling('blood-chin fanatic', '2015-02-25', 'Use the creature’s power as it last existed on the battlefield, including any +1/+1 counters it had, when determining the value of X.').

card_ruling('blood-cursed knight', '2015-06-22', 'If you cast an Aura spell targeting a creature controlled by an opponent, you still control that Aura. It will count for Blood-Cursed Knight’s ability.').

card_ruling('bloodbond vampire', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('bloodbond vampire', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('bloodbond vampire', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('bloodbraid elf', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('bloodbraid elf', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('bloodbraid elf', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('bloodbraid elf', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('bloodbraid elf', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('bloodbraid elf', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('bloodbraid elf', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('bloodchief ascension', '2009-10-01', 'Bloodchief Ascension\'s first ability has an \"intervening \'if\' clause.\" It won\'t trigger at all unless an opponent has already lost 2 or more life by the time the end step begins.').
card_ruling('bloodchief ascension', '2009-10-01', 'Bloodchief Ascension\'s first ability checks how much life was lost by each opponent over the course of the entire turn, even if Bloodchief Ascension wasn\'t on the battlefield the whole time.').
card_ruling('bloodchief ascension', '2009-10-01', 'For the first ability to trigger, a single opponent must have lost 2 life. Two opponents each losing 1 life won\'t cause it to trigger. It will trigger a maximum of once per turn, no matter how many opponents have lost 2 or more life.').
card_ruling('bloodchief ascension', '2009-10-01', 'Bloodchief Ascension\'s first ability checks only whether life was lost. It doesn\'t care whether life was also gained. For example, if an opponent lost 4 life and gained 6 life during the turn, that player will have a higher life total than he or she started the turn with -- but Bloodchief Ascension\'s first ability will still trigger.').
card_ruling('bloodchief ascension', '2009-10-01', 'Bloodchief Ascension\'s second ability doesn\'t behave like a leaves-the-battlefield triggered ability, since the card put into an opponent\'s graveyard may come from anywhere. If a Bloodchief Ascension with three quest counters on it and a permanent an opponent owns are destroyed at the same time, for example, the game will not \"look back in time\" at the game state, and Bloodchief Ascension\'s second ability won\'t trigger.').

card_ruling('bloodcrazed goblin', '2010-08-15', 'For Bloodcrazed Goblin to be able to attack, an opponent must have been dealt damage that turn before the declare attackers step begins. The damage must have been noncombat damage of some sort, or it was combat damage and an additional combat phase has been created.').
card_ruling('bloodcrazed goblin', '2010-08-15', 'It doesn\'t matter how an opponent was dealt damage, or even who controlled the source of the damage.').
card_ruling('bloodcrazed goblin', '2010-08-15', 'If damage would have been dealt to an opponent but it was all prevented, redirected, or otherwise replaced, Bloodcrazed Goblin won\'t be able to attack.').
card_ruling('bloodcrazed goblin', '2010-08-15', 'Bloodcrazed Goblin\'s ability checks only whether damage was dealt to an opponent. It doesn\'t care whether that opponent also gained life. For example, if an opponent was dealt 4 damage and gained 6 life during the turn, that player will have a higher life total than he or she started the turn with -- but Goblin Berserker will still be able to attack.').
card_ruling('bloodcrazed goblin', '2010-08-15', 'If Bloodcrazed Goblin attacks, it doesn\'t have to attack the opponent who was dealt damage. It can attack a planeswalker. In a multiplayer game, it can attack a different opponent.').

card_ruling('bloodcrazed hoplite', '2014-04-26', 'Bloodcrazed Hoplite’s last ability will trigger whenever any +1/+1 counter is placed on it, not just ones due to its heroic ability.').
card_ruling('bloodcrazed hoplite', '2014-04-26', 'If multiple +1/+1 counters are placed on Bloodcrazed Hoplite simultaneously, it last ability will trigger once for each of those counters.').
card_ruling('bloodcrazed hoplite', '2014-04-26', 'If Bloodcrazed Hoplite enters the battlefield with +1/+1 counters on it, its last ability will trigger once for each of those counters.').
card_ruling('bloodcrazed hoplite', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('bloodcrazed hoplite', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('bloodcrazed hoplite', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('bloodfire expert', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('bloodfire expert', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('bloodfire expert', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('bloodfire infusion', '2004-10-04', 'If this card is ever on a creature you don\'t control, it is put into the graveyard as a State-Based Action.').

card_ruling('bloodfray giant', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('bloodfray giant', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('bloodfray giant', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('bloodfray giant', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('bloodghast', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('bloodghast', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('bloodghast', '2009-10-01', 'Bloodghast\'s landfall ability triggers only if it\'s already in your graveyard at the time a land enters the battlefield under your control.').

card_ruling('bloodhall ooze', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').
card_ruling('bloodhall ooze', '2009-02-01', 'A permanent that\'s both black and green will allow both abilities to trigger (as will separate black permanents and green permanents, of course).').

card_ruling('bloodhill bastion', '2012-06-01', 'If a creature you\'ve gained control of temporarily (because of Act of Treason, for example) is exiled and returns to the battlefield under your control, you will control that creature indefinitely.').

card_ruling('bloodletter quill', '2005-10-01', 'You may activate the second ability in response to activating the first one. If you do, you\'ll remove the blood counter that was just added before you lose the life.').

card_ruling('bloodline keeper', '2011-09-22', 'Activating Bloodline Keeper\'s last ability multiple times will cause it to transform into Lord of Lineage, then back again, and so on.').

card_ruling('bloodlord of vaasgoth', '2011-09-22', 'Multiple instances of bloodthirst are cumulative. For example, if you control Bloodlord of Vaasgoth and cast a Vampire creature spell that already has bloodthirst 1, that Vampire will enter the battlefield with four +1/+1 counters on it if an opponent has been dealt damage that turn.').
card_ruling('bloodlord of vaasgoth', '2011-09-22', 'A creature spell like Phantasmal Image that may enter the battlefield as a copy of a creature is not a Vampire creature spell, even if you intend on copying a Vampire creature when that creature enters the battlefield.').
card_ruling('bloodlord of vaasgoth', '2011-09-22', 'Vampire creatures you put directly onto the battlefield without casting them and Vampire creature tokens you put onto the battlefield will not gain bloodthirst 3 from Bloodlord of Vaasgoth.').

card_ruling('bloodpyre elemental', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('bloodrock cyclops', '2004-10-04', 'There is no penalty if it can\'t attack.').

card_ruling('bloodshot trainee', '2011-01-01', 'Once Bloodshot Trainee\'s ability is activated, it will resolve as normal even if Bloodshot Trainee\'s power is less than 4 by the time the ability resolves.').

card_ruling('bloodsoaked champion', '2014-09-20', 'The activated ability can be activated only if Bloodsoaked Champion is in your graveyard. Notably, if it attacks and then dies later in the turn, you can use its ability to return it to the battlefield, as its attack satisfies its own activation instruction.').
card_ruling('bloodsoaked champion', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('bloodsoaked champion', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('bloodstoke howler', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('bloodthorn taunter', '2008-10-01', 'The ability checks the targeted creature\'s power twice: when the creature becomes the target, and when the ability resolves. Once the ability resolves, it will continue to apply to the affected creature no matter what its power may become later in the turn.').

card_ruling('bloodthrone vampire', '2010-06-15', 'You can sacrifice Bloodthrone Vampire to activate its own ability, but it won\'t be on the battlefield to get the bonus.').
card_ruling('bloodthrone vampire', '2010-06-15', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but the creature you wish to sacrifice is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('bloom tender', '2008-08-01', 'For each color (white, blue, black, red, and green), check to see if you control a permanent of that color. You can count the same permanent for multiple colors. For example, if you control a green enchantment and a white-black creature, Bloom Tender\'s ability produces {W}{B}{G}.').
card_ruling('bloom tender', '2008-08-01', 'Bloom Tender won\'t add more than one mana of any particular color to your mana pool. At most, it will produce {W}{U}{B}{R}{G}.').
card_ruling('bloom tender', '2008-08-01', 'Bloom Tender can\'t add colorless mana to your mana pool, even if you control a colorless permanent.').

card_ruling('blossoming wreath', '2004-10-04', 'Creatures are counted on resolution.').

card_ruling('blowfly infestation', '2008-05-01', 'When Blowfly Infestation\'s ability resolves, it will put a single -1/-1 counter on the targeted creature. It doesn\'t matter how many -1/-1 counters were on the creature that went to a graveyard as long as it had at least one on it.').
card_ruling('blowfly infestation', '2008-05-01', 'This ability is mandatory. If you\'re the only player who controls any creatures when this ability triggers, you must target one of them.').

card_ruling('bludgeon brawl', '2011-06-01', 'The affected artifacts retain their other types and abilities.').
card_ruling('bludgeon brawl', '2011-06-01', 'An Equipment that\'s also a creature can\'t be equipped to anything.').
card_ruling('bludgeon brawl', '2011-06-01', 'If an artifact affected by Bludgeon Brawl later becomes a creature, Bludgeon Brawl\'s effect no longer applies to it. That artifact will stop being an Equipment and become unattached from any creature it was attached to; it will remain on the battlefield. The same thing if Bludgeon Brawl leaves the battlefield.').
card_ruling('bludgeon brawl', '2011-06-01', 'While Bludgeon Brawl is on the battlefield, an Aura enchantment that somehow becomes an artifact in addition to its other types will also become an Equipment. Any of its abilities that refer to either \"enchanted creature\" or \"equipped creature\" refer to the creature it\'s currently attached to. It can be attached to other creatures using its equip ability. If you try to attach the Aura Equipment to a creature it can\'t legally be attached to, it remains where it is. If the creature it\'s attached to becomes an illegal permanent for it to enchant, the Aura Equipment will be put into its owner\'s graveyard as a state-based action.').

card_ruling('blue elemental blast', '2004-10-04', 'The decision to counter a spell or destroy a permanent is a decision made on announcement before the target is selected. If the spell is redirected, this mode can\'t be changed, so only targets of the selected type are valid.').

card_ruling('blue mana battery', '2004-10-04', 'Can be tapped even if it has no counters.').

card_ruling('blue sun\'s zenith', '2011-06-01', 'If this spell is countered, none of its effects occur. In particular, it will go to the graveyard rather than to its owner\'s library.').
card_ruling('blue sun\'s zenith', '2011-06-01', 'You follow the instructions in order. You won\'t be able to draw the same Blue Sun\'s Zenith that you cast.').

card_ruling('blunt the assault', '2011-01-01', 'You gain 1 life for each creature on the battlefield, not just for each attacking and blocking creature.').

card_ruling('blur sliver', '2013-07-01', 'If Blur Sliver leaves the battlefield during combat, any attacking Sliver creatures that came under your control this turn will continue to attack, even though they will no longer have haste.').
card_ruling('blur sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('blur sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('blurred mongoose', '2004-10-04', 'It can be targeted while on the stack. Shroud only works while it is on the battlefield.').
card_ruling('blurred mongoose', '2004-10-04', 'Counterspells can be cast that target it, but when they resolve they simply don\'t counter it since it can\'t be countered.').

card_ruling('blustersquall', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('blustersquall', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('blustersquall', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('blustersquall', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('blustersquall', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('blustersquall', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('blustersquall', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('boar umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('boar umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('boar umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('boar umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('boar umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('boar umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('boar umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('boar umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('boar umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('boar umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('boar umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('boartusk liege', '2008-05-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('body double', '2007-02-01', 'Treat Body Double as though it were the chosen card entering the battlefield. Any \"As this card enters the battlefield,\" \"This card enters the battlefield with,\" and \"When this card enters the battlefield\" abilities of the chosen card will work.').
card_ruling('body double', '2007-02-01', 'You don\'t have to choose a card to copy. If you don\'t, Body Double enters the battlefield as a 0/0 creature and is probably put into your graveyard immediately, unless something is increasing its toughness to keep it alive.').
card_ruling('body double', '2007-02-01', 'If Body Double copies a Vesuvan Shapeshifter which then copies another creature and then is somehow turned face down, the Body Double is now a face-down Vesuvan Shapeshifter and may be turned face up via its Morph ability.').

card_ruling('body snatcher', '2004-10-04', 'You choose whether or not to discard on resolution.').
card_ruling('body snatcher', '2004-10-04', 'You can choose to target itself to return to the battlefield, but the exiling happens first and so the return will fail.').

card_ruling('bog down', '2004-10-04', 'The player discards 2 cards or 3 cards, not 5 cards.').

card_ruling('bog glider', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('bog serpent', '2007-02-01', 'This is the timeshifted version of Sea Serpent.').

card_ruling('bog witch', '2004-10-04', 'The ability is a mana ability.').

card_ruling('bogardan firefiend', '2004-10-04', 'You must pick a target creature, even if you are the only player with creatures on the battlefield.').

card_ruling('bogardan hellkite', '2009-10-01', 'The number of targets chosen for the enters-the-battlefield ability must be at least 1 and at most 5. You divide the damage as you put the ability on the stack, not as it resolves. Each target must be assigned at least 1 damage.').

card_ruling('bogardan phoenix', '2006-02-01', 'Errata streamlined the trigger and got rid of the replacement effect. In both cases, it will go to the graveyard. In one it will then come back onto the battlefield; in the other it will then be exiled.').

card_ruling('bogbrew witch', '2013-07-01', 'You don’t have to declare which card you’re searching for when you activate the ability.').
card_ruling('bogbrew witch', '2013-07-01', 'You put only one card onto the battlefield as the activated ability resolves.').

card_ruling('boggart brute', '2015-06-22', 'Once an attacking creature with menace is legally blocked by two or more creatures, removing one or more of those blockers from combat won’t change or undo that block.').

card_ruling('boiling earth', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('boiling earth', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('boiling earth', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('boiling earth', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('boiling earth', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('boiling earth', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('bold defense', '2009-10-01', 'Only creatures you control as Bold Defense resolves are affected.').
card_ruling('bold defense', '2009-10-01', 'If a creature doesn\'t have first strike, granting it first strike after combat damage has been dealt in the first combat damage step won\'t prevent it from dealing combat damage. It will still assign and deal its combat damage in the second combat damage step.').

card_ruling('boldwyr heavyweights', '2008-04-01', 'A player who doesn\'t want to shuffle his or her library may choose not to search for a creature card.').

card_ruling('boldwyr intimidator', '2007-05-01', 'Coward is a new creature type.').
card_ruling('boldwyr intimidator', '2007-05-01', 'Each of the activated abilities replaces all creature types the affected creature may have had.').
card_ruling('boldwyr intimidator', '2008-04-01', 'Note that the exact wording of this card has changed since it was printed in the _Future Sight_(TM) set, but its functionality is the same.').

card_ruling('bolt of keranos', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('bolt of keranos', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('bolt of keranos', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('bolt of keranos', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('boltwing marauder', '2015-02-25', 'Any creature on the battlefield can be the target of Boltwing Marauder’s triggered ability, including the one that entered the battlefield and caused the ability to trigger.').

card_ruling('bomb squad', '2004-10-04', 'If the creature regenerates, the fuse counters are still removed and the four damage is still dealt.').
card_ruling('bomb squad', '2004-10-04', 'If there are two Bomb Squads on the battlefield when a creature ends up with 4 or more fuse counters, both Bomb Squad abilities will trigger causing 4 damage each even though the first to resolve will destroy the creature.').

card_ruling('bomber corps', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('bomber corps', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('bomber corps', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('bond beetle', '2012-07-01', 'You can choose Bond Beetle as the target of its own ability.').

card_ruling('bond of agony', '2006-05-01', 'You can\'t choose an X that\'s greater than your life total.').
card_ruling('bond of agony', '2006-05-01', 'You can choose an X that\'s equal to your life total. If you do, you\'ll pay that much life when the spell is cast and will lose the game before it resolves.').
card_ruling('bond of agony', '2013-04-15', 'You must pay both X life and {X}{B}. For example, if you want each other player to lose 2 life when Bond of Agony resolves, you must pay {2}{B} and 2 life.').

card_ruling('bonded construct', '2015-06-22', 'Bonded Construct can be declared as an attacker only if another creature is declared as an attacker at the same time.').
card_ruling('bonded construct', '2015-06-22', 'If you control more than one creature that can’t attack alone, they can attack together, even if no other creatures attack.').
card_ruling('bonded construct', '2015-06-22', 'Although Bonded Construct can’t attack alone, other attacking creatures don’t have to attack the same player or planeswalker. For example, Bonded Construct could attack an opponent and another creature could attack a planeswalker that opponent controls.').
card_ruling('bonded construct', '2015-06-22', 'If a creature that can’t attack alone also must attack if able, its controller must attack with it and another creature if able.').
card_ruling('bonded construct', '2015-06-22', 'In a Two-Headed Giant game (or in another format using the shared team turns option), Bonded Construct can attack with a creature controlled by your teammate, even if no other creatures you control attack.').

card_ruling('bonds of faith', '2011-09-22', 'Once the enchanted creature has been declared as an attacking or blocking creature, causing it to stop being a Human won\'t remove it from combat. It will lose the +2/+2 bonus, however.').

card_ruling('bonds of quicksilver', '2011-01-01', 'Bonds of Quicksilver may target and may enchant an untapped creature.').

card_ruling('bone dancer', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('bone dancer', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('bone dancer', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('bone dancer', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('bone dancer', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').
card_ruling('bone dancer', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('bone flute', '2009-10-01', 'The ability affects only creatures on the battlefield at the time it resolves. A creature that enters the battlefield later in the turn won\'t get -1/-0.').

card_ruling('bone harvest', '2004-10-04', 'You have to show which creature cards you put on top of your library, but not the order you put them there.').

card_ruling('bone mask', '2004-10-04', 'If you do not have enough cards in your library, the damage is still prevented and all the cards in your library are exiled.').

card_ruling('bone splinters', '2008-10-01', 'You could choose to target the creature you\'re going to sacrifice to pay for Bone Splinters. First you choose the target (at which time it\'s still on the battlefield), then you pay the costs (at which time you sacrifice it). The spell will be countered.').
card_ruling('bone splinters', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('bone splinters', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('bone to ash', '2011-01-22', 'If the creature spell is an illegal target (because it\'s left the stack, for example) when Bone to Ash tries to resolve, Bone to Ash will be countered and none of its effects will happen. You won\'t draw a card.').
card_ruling('bone to ash', '2011-01-22', 'A creature spell that can\'t be countered by spells and abilities is a legal target for Bone to Ash. The creature spell won\'t be countered when Bone to Ash resolves, but you\'ll still draw a card.').

card_ruling('bonehoard', '2011-06-01', 'The value of X is calculated continuously as the number of creature cards in graveyards changes.').
card_ruling('bonehoard', '2011-06-01', 'If there are no creature cards in any graveyard when Bonehoard\'s living weapon ability resolves, the Germ will be 0/0 and put into its owner\'s graveyard.').
card_ruling('bonehoard', '2011-06-01', 'Although creature tokens go to the graveyard before ceasing to exist, they never count as creature cards and won\'t increase the bonus granted by Bonehoard, however briefly.').
card_ruling('bonehoard', '2011-06-01', 'If lethal damage is dealt simultaneously to the equipped creature and another creature, both creatures are put into the graveyard at the same time. The equipped creature won\'t get an additional bonus from the other creature.').

card_ruling('boneknitter', '2004-10-04', 'It can regenerate itself.').

card_ruling('bonescythe sliver', '2013-07-01', 'If Bonescythe Sliver leaves the battlefield after other Sliver creatures you control have dealt first-strike damage but before regular combat damage, those Slivers won’t deal regular combat damage (unless they still have double strike for some other reason).').
card_ruling('bonescythe sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('bonescythe sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('bonesplitter sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('bonesplitter sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('bonethorn valesk', '2007-05-01', 'Triggers when any permanent is turned face up, not just a creature.').

card_ruling('boneyard wurm', '2011-09-22', 'The ability that defines Boneyard Wurm\'s power and toughness works in all zones, not just the battlefield. If Boneyard Wurm is in your graveyard, it will count itself.').

card_ruling('bonfire of the damned', '2012-05-01', 'Bonfire of the Damned targets only the player. Creatures with hexproof that player controls will be dealt damage by Bonfire of the Damned if it resolves.').
card_ruling('bonfire of the damned', '2012-05-01', 'If the player is an illegal target when Bonfire of the Damned tries to resolve, it will be countered and none of its effects will happen. No damage will be dealt to any creature or player.').
card_ruling('bonfire of the damned', '2012-05-01', 'Preventing some or all of the damage dealt to the player won\'t affect the amount of damage dealt to each creature that player controls. The same is true for any effect that increases or decreases the amount of damage dealt to the player.').

card_ruling('booby trap', '2004-10-04', 'The \"if you do\" part is only done if you successfully sacrifice this card. In other words, it has to be on the battlefield when the trigger resolves or it does not deal damage.').
card_ruling('booby trap', '2005-08-01', 'The choice of which card to name is part of the resolution of the spell or ability which puts Booby Trap onto the battlefield. It can\'t be countered.').
card_ruling('booby trap', '2005-08-01', 'If an effect causes the player to draw multiple cards, each card is revealed as it\'s drawn. If Booby Trap\'s ability triggers, it will go on the stack once the card-drawing effect has concluded.').
card_ruling('booby trap', '2005-08-01', 'If a single Booby Trap\'s ability triggers multiple times, then only the first ability to resolve causes Booby Trap to be sacrificed and damage to be dealt.').
card_ruling('booby trap', '2005-08-01', 'If Booby Trap\'s last ability is countered, Booby Trap remains on the battlefield (and no damage is dealt).').

card_ruling('book of rass', '2004-10-04', 'You can\'t spend yourself to below zero life. You can\'t spend life you don\'t have.').

card_ruling('boon of erebos', '2013-09-15', 'The loss of life is part of the spell’s effect. It’s not an additional cost. If Boon of Erebos is countered, you won’t lose life.').

card_ruling('boon reflection', '2008-05-01', 'Boon Reflection doesn\'t cause you to gain life. Rather, it causes life-gaining effects to have you gain more life.').
card_ruling('boon reflection', '2008-05-01', 'The effects of multiple Boon Reflections are cumulative. For example, if you have three Boon Reflections on the battlefield, you\'ll gain eight times the original amount of life.').
card_ruling('boon reflection', '2008-05-01', 'If an effect says to set your life total to a certain number, and that number is higher than your current life total, that effect will cause you to gain life equal to the difference. Boon Reflection will double that number. For example, if you have 13 life and Blessed Wind causes your life total to become 20, your life total will actually become 27.').
card_ruling('boon reflection', '2008-05-01', 'In a Two-Headed Giant game, only Boon Reflection\'s controller is affected by it. If that player\'s teammate gains life, Boon Reflection will have no effect, even when that life gain is applied to the shared team life total.').

card_ruling('boon satyr', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('boon satyr', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('boon satyr', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('boon satyr', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('boon satyr', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('boon satyr', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('boon satyr', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('boonweaver giant', '2014-07-18', 'The Aura card must be able to legally enchant Boonweaver Giant. For example, you can’t put an Aura with enchant land onto the battlefield this way.').
card_ruling('boonweaver giant', '2014-07-18', 'If Boonweaver Giant isn’t on the battlefield when its ability resolves, you can’t put any Aura onto the battlefield. You may still search the appropriate zones and shuffle your library, if applicable.').

card_ruling('borborygmos', '2006-02-01', 'Borborygmos\'s ability won\'t save your creatures that were dealt lethal damage in combat. Those creatures will be sent to your graveyard before they would get a counter.').

card_ruling('boros battleshaper', '2013-04-15', 'You may choose zero targets for either or both instances of “up to one target creature.”').
card_ruling('boros battleshaper', '2013-04-15', 'If you choose two targets for Boros Battleshaper\'s ability, and only one of those targets is legal when the ability tries to resolve, only the legal target will be affected. If all targets are illegal when the ability tries to resolve, it will be countered and none of its effects will happen.').
card_ruling('boros battleshaper', '2013-04-15', 'The controller of each creature chooses which player or planeswalker it attacks or which attacking creature it blocks, as appropriate.').
card_ruling('boros battleshaper', '2013-04-15', 'A creature forced to attack does so only if it\'s able to do so as attackers are declared. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having the creature attack, the creature\'s controller isn\'t forced to pay that cost, so it doesn\'t attack in that case either.').
card_ruling('boros battleshaper', '2013-04-15', 'A creature forced to block does so only if it\'s able to do so as blockers are declared. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t block, or no creatures are attacking its controller or a planeswalker controlled by that player, then it doesn\'t block. If there\'s a cost associated with having the creature block, its controller isn\'t forced to pay that cost, so it doesn\'t block in that case either.').
card_ruling('boros battleshaper', '2013-04-15', 'If another player gains control of the creature chosen to attack or block if able after it attacks, that creature isn\'t forced to then block during that same combat.').

card_ruling('boros charm', '2013-07-01', 'If you choose the second mode, permanents that come under your control later in the turn will not have indestructible.').
card_ruling('boros charm', '2013-07-01', 'Planeswalkers with indestructible will still have loyalty counters removed from them as they are dealt damage. If planeswalker with indestructible has no loyalty counters, it will still be put into its owner\'s graveyard, as the rule that does this doesn\'t destroy the planeswalker.').

card_ruling('boros elite', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('boros elite', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('boros elite', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('boros fury-shield', '2005-10-01', 'If red mana was spent, Boros Fury-Shield, not the target creature, deals the damage to the creature\'s controller.').

card_ruling('boros garrison', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('boros guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('boros guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('boros keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('boros keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('boros mastiff', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('boros mastiff', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('boros mastiff', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('boros reckoner', '2013-01-24', 'Boros Reckoner\'s first ability will trigger even if it is dealt lethal damage. For example, if it blocks a 7/7 creature, its ability will trigger and Boros Reckoner will deal 7 damage to the target creature or player.').
card_ruling('boros reckoner', '2013-01-24', 'Damage dealt by Boros Reckoner due to its first ability isn\'t combat damage, even if it was combat damage that caused that ability to trigger.').

card_ruling('boseiju, who shelters all', '2004-12-01', 'The spell can\'t be countered if the mana produced by Boseiju is spent to cover any cost of the spell, even an additional cost such as a splice cost. This is true even if you pay an additional cost while casting a spell \"without paying its mana cost.\"').
card_ruling('boseiju, who shelters all', '2004-12-01', 'Suppose mana produced by Boseiju is spent on a spell and an effect creates a copy of that spell. The copy can be countered, even though the original can\'t.').

card_ruling('bosk banneret', '2008-04-01', 'A spell you cast that\'s both creature types costs {1} less to cast, not {2} less.').
card_ruling('bosk banneret', '2008-04-01', 'The effect reduces the total cost of the spell, regardless of whether you chose to pay additional or alternative costs. For example, if you cast a Rogue spell by paying its prowl cost, Frogtosser Banneret causes that spell to cost {1} less.').

card_ruling('bottle of suleiman', '2004-10-04', 'The coin is flipped on resolution and not on declaration of the ability.').

card_ruling('bottled cloister', '2006-02-01', 'Errata\'d to make it clear that only the cards you own go into your hand, so if you somehow gain control of the Cloister, your opponent\'s cards won\'t go to your hand.').

card_ruling('bottomless pit', '2004-10-04', 'The ability is controlled by the player who controls Bottomless Pit. This means that Bottomless Pit can trigger abilities which trigger off an opponent forcing you to discard.').

card_ruling('bottomless vault', '2004-10-04', 'It is considered \"tapped for mana\" if you activate its mana ability, even if you choose to take zero mana from it.').
card_ruling('bottomless vault', '2004-10-04', 'This enters the battlefield tapped even if a continuous effect immediately changes it to something else.').
card_ruling('bottomless vault', '2004-10-04', 'If the land is tapped by some external effect, no counters are removed from it.').
card_ruling('bottomless vault', '2004-10-04', 'Counters are not lost if the land is changed to another land type. They wait around for the land to change back.').
card_ruling('bottomless vault', '2004-10-04', 'Whether or not it is tapped is checked at the beginning of upkeep. If it is not tapped, the ability does not trigger. It also checks during resolution and you only get a counter if it is still tapped then.').

card_ruling('boulderfall', '2013-09-15', 'The number of targets chosen for Boulderfall must be at least one and at most five. You divide the damage as you cast the spell, not as it resolves. Each target must be assigned at least 1 damage.').
card_ruling('boulderfall', '2013-09-15', 'If some but not all of Boulderfall’s targets become illegal, you can’t change the division of damage. Damage that would have been dealt to illegal targets simply isn’t dealt.').
card_ruling('boulderfall', '2013-09-15', 'You can’t deal damage to both a player and a planeswalker that player controls using Boulderfall. You also can’t deal damage to more than one planeswalker controlled by the same player. If you choose to redirect the damage being dealt to a player to a planeswalker, you must redirect all the damage to a single planeswalker.').
card_ruling('boulderfall', '2013-09-15', 'If an effect creates a copy of Boulderfall, the division of damage and number of targets can’t be changed. The effect that creates the copy may allow you to change targets, however.').

card_ruling('bound', '2006-05-01', 'The creature is sacrificed when Bound resolves. It\'s not an additional cost. It\'s not optional. If you can\'t sacrifice a creature or a colorless creature is sacrificed, no cards are returned.').
card_ruling('bound', '2006-05-01', 'The cards in the graveyard that you return as part of Bound\'s effect aren\'t targeted. They\'re chosen when the spell resolves. The creature you sacrificed may be one of them.').

card_ruling('bound in silence', '2007-05-01', 'If you put Bound in Silence onto the battlefield without casting it, perhaps using the \"recruiting\" ability of a card like Amrou Scout, it will enter attached to a creature. You choose that creature as you\'re putting it onto the battlefield. If there\'s no creature on the battlefield it can be attached to, it stays in whatever zone it was in.').
card_ruling('bound in silence', '2007-05-01', 'Bound in Silence isn\'t a creature and isn\'t affected by any effect that affects creatures or Rebel creatures.').

card_ruling('boundless realms', '2012-07-01', 'The number of basic land cards you put onto the battlefield is equal to the number of lands you control when Boundless Realms begins resolving.').

card_ruling('bounteous kirin', '2005-06-01', 'If the Spirit or Arcane spell has {X} in the mana cost, then you use the value of {X} on the stack. For example, Shining Shoal costs {X}{W}{W}. If you choose X = 2, then Shining Shoal\'s converted mana cost is 4. Shining Shoal also has an ability that says \"You may exile a white card with converted mana cost X from your hand rather than pay Shining Shoal\'s mana cost\"; if you choose to pay the alternative cost and exile a card with converted mana cost 2, then X is 2 while Shining Shoal is on the stack and its converted mana cost is 4.').

card_ruling('bow of nylea', '2013-09-15', 'You choose which mode you’re using as you activate the ability.').

card_ruling('bower passage', '2012-05-01', 'Bower Passage won\'t affect a blocking creature that gains flying after blockers are declared.').
card_ruling('bower passage', '2012-05-01', 'Creatures with reach can still block creatures you control.').

card_ruling('brago\'s favor', '2014-05-29', 'The second ability can’t reduce the colored mana requirement of a spell.').
card_ruling('brago\'s favor', '2014-05-29', 'The second ability can reduce alternative costs such as overload costs.').
card_ruling('brago\'s favor', '2014-05-29', 'If there are additional costs to cast the spell, such as kicker costs, apply those increases before applying cost reductions.').
card_ruling('brago\'s favor', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('brago\'s favor', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('brago\'s favor', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('brago\'s favor', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('brago\'s favor', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('brago\'s favor', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('brago\'s favor', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('brago\'s favor', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('brago\'s favor', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('brago\'s favor', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('brago\'s favor', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('brago\'s favor', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('brago\'s favor', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('brago\'s representative', '2014-05-29', 'The ability is cumulative. For example, if you control two Brago’s Representatives, you’ll vote three times per vote.').
card_ruling('brago\'s representative', '2014-05-29', 'You make all your votes at the same time. Players who vote after you will know all of your votes when making their own.').
card_ruling('brago\'s representative', '2014-05-29', 'The ability only affects spells and abilities that use the word “vote.” Other cards that involve choices, such as Archangel of Strife, are unaffected.').

card_ruling('brago, king eternal', '2014-05-29', 'The ability exiles and returns all the targets during the combat damage step, after combat damage is dealt. You can’t target any creature that didn’t survive combat.').
card_ruling('brago, king eternal', '2014-05-29', 'You may exile Brago using its own ability.').
card_ruling('brago, king eternal', '2014-05-29', 'If you exile an Aura this way, the player who will control it when it enters the battlefield chooses what it will enchant at that time. That player may choose any player or permanent that Aura could enchant. However, the Aura can’t enter the battlefield enchanting a permanent that enters the battlefield at the same time.').

card_ruling('braid of fire', '2009-10-01', 'Braid of Fire is a very unusual card. \"Add {R} to your mana pool\" is a cost. This enchantment does nothing but add increasing amounts of mana to your mana pool during your upkeep.').
card_ruling('braid of fire', '2009-10-01', 'Any of this mana that isn\'t spent during your upkeep step will be removed before your draw step begins. You cannot save it to be spent on the card you will draw this turn.').

card_ruling('braidwood sextant', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('brain freeze', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('brain freeze', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('brain freeze', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('brain freeze', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('brain freeze', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('brain gorgers', '2007-02-01', 'When this spell is cast, its \"when you cast\" ability triggers and goes on the stack on top of it.').
card_ruling('brain gorgers', '2007-02-01', 'As the triggered ability resolves, the active player gets the option to perform the action. If that player declines, the next player in turn order gets the option. As soon as any player performs the action, the spell is countered, but the remaining players still get the option. If all players decline, the spell remains on the stack.').
card_ruling('brain gorgers', '2013-07-01', 'A player can\'t choose to sacrifice a creature unless he or she actually controls a creature that can be sacrificed. For example, if the action were \"discard three cards,\" a player with two or fewer cards in hand couldn\'t take the option to counter the spell.').

card_ruling('brain maggot', '2014-04-26', 'Brain Maggot’s ability causes a zone change with a duration, a new style of ability that’s somewhat reminiscent of older cards like Tidehollow Sculler. However, unlike Tidehollow Sculler, Brain Maggot has a single ability that creates two one-shot effects: one that exiles the nonland card when the ability resolves, and another that returns the exiled card to its owner’s hand immediately after Brain Maggot leaves the battlefield.').
card_ruling('brain maggot', '2014-04-26', 'If Brain Maggot leaves the battlefield before its enters-the-battlefield ability resolves, the opponent will reveal his or her hand, but no card will be exiled.').
card_ruling('brain maggot', '2014-04-26', 'The exiled card returns to its owner’s hand immediately after Brain Maggot leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('brain maggot', '2014-04-26', 'In a multiplayer game, if Brain Maggot’s owner leaves the game, the exiled card will return to its owner’s hand. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('brain pry', '2006-05-01', 'You name the card during Brain Pry\'s resolution, then the player reveals his or her hand and discards if appropriate. There is no way for the player to do anything between you naming the card and the player discarding.').
card_ruling('brain pry', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('brain weevil', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('brainbite', '2009-05-01', 'You may target an opponent who has no cards in hand.').
card_ruling('brainbite', '2009-05-01', 'When Brainbite resolves, you\'ll draw a card even if the targeted opponent has no cards in hand.').

card_ruling('brainstorm', '2004-10-04', 'This is all one effect. You draw 3 and return 2 cards all in one resolution. Nothing may happen between the two. If something triggers on drawing cards, it will go on the stack after Brainstorm finishes resolving.').

card_ruling('brainwash', '2004-10-04', 'This ability is paid for during the declare attackers step of the combat phase.').
card_ruling('brainwash', '2004-10-04', 'If there are multiple combat phases during the turn, the attack cost must be paid each time if you want to attack with the creature.').
card_ruling('brainwash', '2004-10-04', 'Paying this cost is not an instant or any other kind of ability, it is an additional cost on the declaration of the attacker.').

card_ruling('bramblesnap', '2010-06-15', 'Since Bramblesnap\'s activated ability doesn\'t have a tap symbol in its cost, you can tap a creature (including Bramblesnap itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('bramblewood paragon', '2008-04-01', 'If a Warrior would normally enter the battlefield with a certain number of +1/+1 counters on it, it enters the battlefield with that many +1/+1 counters plus one on it instead. If a Warrior would normally enter the battlefield with no +1/+1 counters on it, it enters the battlefield with one +1/+1 counter on it instead.').
card_ruling('bramblewood paragon', '2008-04-01', 'The creature gets the counter if it would enter the battlefield under your control. It doesn\'t matter who owns the creature or what zone it enters the battlefield from (such as your opponent\'s graveyard, for example).').
card_ruling('bramblewood paragon', '2008-04-01', 'The creature gets the counter if its copiable characteristics as it would exist on the battlefield include the specified creature type. For example, say you control Conspiracy, and the chosen creature type is Warrior. If you put a creature onto the battlefield that isn\'t normally a Warrior, it won\'t get a counter from Bramblewood Paragon.').
card_ruling('bramblewood paragon', '2008-04-01', 'The effects from more than one Bramblewood Paragon are cumulative. That is, if you have more than one Bramblewood Paragon on the battlefield, Warrior creatures you control enter the battlefield with that many additional +1/+1 counters on them.').
card_ruling('bramblewood paragon', '2008-04-01', 'If Bramblewood Paragon enters the battlefield at the same time as another Warrior (due to Living End, for example), that creature doesn\'t get a +1/+1 counter.').

card_ruling('branching bolt', '2008-10-01', 'You may choose just the first mode (targeting a creature with flying), just the second mode (targeting a creature without flying), or both modes (targeting a creature with flying and a creature without flying). You can\'t choose a mode unless there\'s a legal target for it.').

card_ruling('brand', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('brand', '2009-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').

card_ruling('brand of ill omen', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('brass herald', '2004-10-04', 'You must choose a creature type that already exists in Magic.').

card_ruling('brass man', '2006-05-01', 'Reworded so that it triggers only once at beginning of upkeep, instead of being an activated ability usable any time during your upkeep. You can no longer make infinite combos with this ability.').

card_ruling('brass squire', '2011-06-01', 'If one of the targets is illegal when the activated ability resolves, nothing will happen and the Equipment won\'t move. Notably, if an opponent gains control of either target in response, the Equipment won\'t move.').
card_ruling('brass squire', '2011-06-01', 'Unlike the equip ability, Brass Squire\'s ability can be activated whenever you could cast an instant.').

card_ruling('bravado', '2004-10-04', 'The bonus is continuously calculated.').

card_ruling('brave the elements', '2009-10-01', 'You choose a color as Brave the Elements resolves. Once the color is chosen, it\'s too late for players to respond.').
card_ruling('brave the elements', '2009-10-01', 'Only white creatures you control at the time Brave the Elements resolves gain the protection ability. Creatures that come under your control later in the turn, or that turn white later in the turn, won\'t have it.').

card_ruling('brave the sands', '2014-09-20', 'The last ability is cumulative. If you control an additional Brave the Sands, your creatures will each be able to block three creatures, and so on.').

card_ruling('breaching leviathan', '2014-11-07', 'The triggered ability affects all nonblue creatures, including those you control.').
card_ruling('breaching leviathan', '2014-11-07', 'The triggered ability tracks the creatures, but not their controllers. If any of the creatures changes controllers before its original controller’s next untap step has come around, that creature won’t untap during its new controller’s next untap step.').

card_ruling('break asunder', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('break of day', '2013-07-01', 'Because Break of Day affects only creatures you control when it resolves. It won\'t affect creatures that come under your control later in the turn.').
card_ruling('break of day', '2013-07-01', 'Break of Day checks your life total only at the time it resolves. Any creatures that gain indestructible will continue to be indestructible that turn even if you gain life.').

card_ruling('break open', '2004-10-04', 'The player does not have to pay the Morph cost (if any) of the creature.').

card_ruling('break through the line', '2014-11-24', 'If the creature’s power is greater than 2 as the activated ability tries to resolve, the ability will be countered and none of its effects will happen. However, if the creature’s power is raised above 2 after the ability resolves, it will still have haste and can’t be blocked that turn.').

card_ruling('breaker of armies', '2015-08-25', 'If, during its controller’s declare blockers step, a creature the defending player controls is tapped or is affected by a spell or ability that says it can’t block, then it doesn’t block. If there’s a cost associated with having it block, its controller isn’t forced to pay that cost. If he or she doesn’t, the creature doesn’t have to block.').
card_ruling('breaker of armies', '2015-08-25', 'If a creature can’t legally block Breaker of Armies but could block another attacking creature, it may do so. Likewise, if a creature the defending player controls can’t block Breaker of Armies unless its controller pays a cost, its controller may decline to pay that cost and block a different attacking creature.').
card_ruling('breaker of armies', '2015-08-25', 'If more than one Breaker of Armies is attacking, the controller of each creature that could block them chooses which one that creature blocks. In this case, creatures that can block multiple creatures must block as many attacking Breaker of Armies as possible.').

card_ruling('breaking', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('breaking', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('breaking', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('breaking', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('breaking', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('breaking', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('breaking', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('breaking', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('breaking', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('breaking', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('breaking', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('breath of dreams', '2013-04-15', 'If a permanent has multiple instances of cumulative upkeep, each triggers separately. However, the age counters are not connected to any particular ability; each cumulative upkeep ability will count the total number of age counters on the permanent at the time that ability resolves.').

card_ruling('breath of fury', '2005-10-01', 'If there isn\'t a legal creature to attach Breath of Fury to after the enchanted creature is sacrificed, you don\'t untap your creatures or get an additional combat phase, and Breath of Fury is put into its owner\'s graveyard as a state-based action.').

card_ruling('bred for the hunt', '2013-04-15', 'A creature that deals combat damage to a player must have a +1/+1 counter on it at the time damage is dealt in order for Bred for the Hunt\'s ability to trigger.').

card_ruling('breeding pit', '2004-10-04', 'Breeding Pit must actually be on the battlefield at the beginning of the endstep in order for you to get a Thrull. The getting of the Thrull is not part of paying the upkeep, it is part of having the card on the battlefield.').

card_ruling('breeding pool', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('breeding pool', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('breeding pool', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('briar shield', '2008-04-01', 'When you cast Briar Shield\'s activated ability, Briar Shield is put into the graveyard as a cost. The +1/+1 effect immediately ends, but the +3/+3 effect won\'t begin until the ability resolves. The enchanted creature gets no bonus while Briar Shield\'s ability is on the stack. If its toughness is 0 during this time, it\'ll be put into the graveyard.').

card_ruling('briarhorn', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('briber\'s purse', '2014-09-20', 'Activating the ability targeting a creature that’s already attacking or blocking won’t remove it from combat or affect that attack or block.').
card_ruling('briber\'s purse', '2014-09-20', 'If Briber’s Purse has no gem counters on it, it remains on the battlefield, although you can’t activate its last ability.').

card_ruling('bribery', '2004-10-04', 'You put the creature onto the battlefield, so you control it and any \"enters the battlefield\" abilities it has.').
card_ruling('bribery', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('bridge from below', '2007-05-01', 'While Bridge from Below is on the battlefield, it has no effect. It does something only if it\'s in a graveyard.').
card_ruling('bridge from below', '2007-05-01', 'Neither ability cares who controlled the creature (or nontoken creature), only which graveyard it was put into.').

card_ruling('brightflame', '2005-10-01', 'You gain life equal to the total damage dealt by Brightflame to all creatures. You don\'t gain life for any damage that was prevented.').
card_ruling('brightflame', '2005-10-01', 'All creatures that share a color are affected, even your own.').
card_ruling('brightflame', '2005-10-01', 'A creature \"shares a color\" with any creature that is at least one of its colors. For example, a green-white creature shares a color with creatures that are green, white, green-white, red-white, black-green, and so on.').
card_ruling('brightflame', '2005-10-01', 'If it targets a colorless creature, it doesn\'t affect any other creatures. A colorless creature shares a color with nothing, not even other colorless creatures.').
card_ruling('brightflame', '2005-10-01', 'You check which creatures share a color with the target when the spell resolves.').
card_ruling('brightflame', '2005-10-01', 'Only one creature is targeted. If that creature leaves the battlefield or otherwise becomes an illegal target, the entire spell is countered. No other creatures are affected.').

card_ruling('brighthearth banneret', '2008-04-01', 'A spell you cast that\'s both creature types costs {1} less to cast, not {2} less.').
card_ruling('brighthearth banneret', '2008-04-01', 'The effect reduces the total cost of the spell, regardless of whether you chose to pay additional or alternative costs. For example, if you cast a Rogue spell by paying its prowl cost, Frogtosser Banneret causes that spell to cost {1} less.').
card_ruling('brighthearth banneret', '2008-04-01', 'When you cast an Elemental spell by paying its evoke cost, this effect reduces the cost to cast that spell by {1}.').

card_ruling('brilliant spectrum', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('brilliant spectrum', '2015-08-25', '').
card_ruling('brilliant spectrum', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('brilliant spectrum', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('brilliant spectrum', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('brilliant ultimatum', '2008-10-01', 'The cards are exiled face up.').
card_ruling('brilliant ultimatum', '2008-10-01', 'One of the piles may have zero cards in it if the opponent wishes.').
card_ruling('brilliant ultimatum', '2008-10-01', 'You play cards from the chosen pile as part of the resolution of Brilliant Ultimatum. You may play them in any order. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other play restrictions are not (such as \"Cast [this card] only during combat\"). You play all of the cards you like, putting land onto the battlefield and spells on the stack, then Brilliant Ultimatum finishes resolving and is put into your graveyard. The spells you cast this way will then resolve as normal, one at a time, in the opposite order that they were put on the stack.').
card_ruling('brilliant ultimatum', '2008-10-01', 'You can play a land card from the chosen pile only if it\'s your turn (which it probably is, since Brilliant Ultimatum is a sorcery) and you haven\'t yet played a land this turn. That means that if there are two lands in the chosen pile, you\'ll be able to play a maximum of one of them.').
card_ruling('brilliant ultimatum', '2008-10-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as conspire costs and kicker costs.').
card_ruling('brilliant ultimatum', '2008-10-01', 'The cards in the pile that wasn\'t chosen remain exiled. Likewise, any cards in the chosen pile that you can\'t play or you choose not to play remain exiled.').

card_ruling('brimaz, king of oreskos', '2014-02-01', 'For the first triggered ability, you declare which player or planeswalker the token is attacking as you put it onto the battlefield. It doesn’t have to be the same player or planeswalker Brimaz is attacking.').
card_ruling('brimaz, king of oreskos', '2014-02-01', 'If Brimaz somehow blocks two or more creatures (note it can’t naturally do this), its last ability will trigger that many times.').
card_ruling('brimaz, king of oreskos', '2014-02-01', 'Although the tokens enter the battlefield attacking or blocking, they were never declared as attacking or blocking creatures. Abilities that trigger whenever a creature attacks or blocks won’t trigger.').

card_ruling('brimstone mage', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('brimstone mage', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('brimstone mage', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('brimstone mage', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('brimstone mage', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('brindle boar', '2010-08-15', 'If you sacrifice an attacking or blocking Brindle Boar during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but Brindle Boar is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('brine elemental', '2006-09-25', 'Skipping the \"next\" step is cumulative. If a player turns two Brine Elementals face up on the same turn, that player\'s opponents will each skip their next two untap steps.').

card_ruling('brine seer', '2004-10-04', 'You can reveal zero cards, and the controller of the spell can pay {0} to stop this or not pay {0} and let it be countered.').

card_ruling('brine shaman', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('bring to light', '2015-08-25', 'If you cast the exiled card, you do so as part of the resolution of Bring to Light. You can’t wait to cast it later in the turn. Timing restrictions based on the card’s type are ignored, but other restrictions (such as “Cast [this card] only during combat”) are not.').
card_ruling('bring to light', '2015-08-25', 'If you cast a card “without paying its mana cost,” you can’t pay any alternative costs, such as awaken costs. You can, however, pay additional costs, such as kicker costs. If the card has any mandatory additional costs, you must pay those to cast the card.').
card_ruling('bring to light', '2015-08-25', 'If the card has {X} in its mana cost, you must choose 0 as the value of X.').
card_ruling('bring to light', '2015-08-25', 'If you cast an instant or sorcery card this way, it goes to your graveyard as normal. It doesn’t return to exile.').
card_ruling('bring to light', '2015-08-25', 'If you don’t cast the card, it remains exiled.').
card_ruling('bring to light', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('bring to light', '2015-08-25', '').
card_ruling('bring to light', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('bring to light', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('bring to light', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('brink of disaster', '2010-03-01', 'Brink of Disaster may target and may enchant a permanent that\'s already tapped. It won\'t do anything until the enchanted permanent changes from being untapped to being tapped.').
card_ruling('brink of disaster', '2010-03-01', 'When the enchanted permanent becomes tapped, Brink of Disaster\'s ability triggers. That permanent will be destroyed when the ability resolves, even if Brink of Disaster has left the battlefield or is somehow enchanting a different permanent by then.').
card_ruling('brink of disaster', '2010-03-01', 'If the enchanted permanent is tapped as a cost to activate a mana ability, the mana ability resolves immediately, then Brink of Disaster\'s ability goes on the stack.').
card_ruling('brink of disaster', '2010-03-01', 'If the enchanted permanent is tapped as a cost to activate an ability that\'s not a mana ability, Brink of Disaster\'s ability will go on the stack on top of that activated ability. Brink of Disaster\'s ability resolves first (destroying that permanent), then the permanent\'s activated ability resolves.').

card_ruling('brink of madness', '2010-06-15', 'The ability only triggers if you have no cards in your hand as your upkeep step begins.').
card_ruling('brink of madness', '2010-06-15', 'You must sacrifice Brink of Madness if possible, but the opponent discards his or her hand even if you can\'t sacrifice it for some reason (for example, if it were returned to its owner\'s hand before the ability resolved).').

card_ruling('brittle effigy', '2010-08-15', 'Brittle Effigy is exiled as a cost to activate its ability, not as part of its effect. If the targeted creature is an illegal target by the time the ability would resolve, the ability is countered, but Brittle Effigy remains exiled.').

card_ruling('broken ambitions', '2007-10-01', 'The opponent you clash with doesn\'t have to be the controller of the targeted spell.').
card_ruling('broken ambitions', '2007-10-01', 'The clash (and the result of the clash if you win) happens regardless of whether the targeted spell was countered or {X} was paid.').

card_ruling('broken visage', '2004-10-04', 'The token creature enters the battlefield under your control regardless of who controlled the targeted creature.').
card_ruling('broken visage', '2008-10-01', 'The Spirit token has the power and toughness of the attacking creature as it last existed on the battlefield.').

card_ruling('bronze bombshell', '2006-05-01', 'This ability is a state trigger. Once it triggers, it won\'t trigger again while the ability is on the stack. If the triggered ability leaves the stack and the creature is still on the battlefield under the control of a player other than its owner, the ability will immediately trigger again.').
card_ruling('bronze bombshell', '2006-05-01', 'Bronze Bombshell deals 7 damage only if it was sacrificed as a result of its ability. If it leaves the battlefield or changes controllers before the ability resolves, it can\'t be sacrificed and won\'t deal damage.').

card_ruling('bronze tablet', '2004-10-04', 'If using a copy of the Tablet, you must trade the copy card to your opponent just like you would have traded the Tablet.').
card_ruling('bronze tablet', '2004-10-04', 'You can\'t choose to pay 10 life if you have less than 10 life, but you may choose to give up the game immediately. This has roughly the same effect.').
card_ruling('bronze tablet', '2004-10-04', 'You can take control of your opponent\'s Tablet and in the trade you only have to give them back their Tablet.').
card_ruling('bronze tablet', '2004-10-04', 'If the card being targeted by the Bronze Tablet is removed before Tablet takes effect, then the Tablet ability is countered and it remains tapped.').
card_ruling('bronze tablet', '2008-08-01', 'If the tablet is not still on the battlefield when the ability resolves, it is not exiled. The other player still has the choice to pay 10 life, and you still become the owner of their card if they choose not to do so.').

card_ruling('brood birthing', '2010-06-15', 'Whether you control an Eldrazi Spawn is checked as Brood Birthing resolves.').
card_ruling('brood birthing', '2010-06-15', 'If you put a single token onto the battlefield this way, it\'s exactly like the ones specified earlier: it\'s a 0/1 colorless Eldrazi Spawn creature token that has \"Sacrifice this creature: Add {1} to your mana pool.\"').

card_ruling('brood butcher', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('brood butcher', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('brood butcher', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('brood butcher', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('brood butcher', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('brood butcher', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('brood butcher', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('brood butcher', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('brood butcher', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('brood keeper', '2014-07-18', 'Brood Keeper’s ability triggers both whenever an Aura enters the battlefield attached to Brood Keeper and whenever an Aura on the battlefield attached to a different object becomes attached to Brood Keeper.').
card_ruling('brood keeper', '2014-07-18', 'If an Aura becomes attached to Brood Keeper that causes another player to gain control of it, that player will control the triggered ability that creates a Dragon.').

card_ruling('brood monitor', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('brood monitor', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('brood monitor', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('brood monitor', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('brood monitor', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('brood monitor', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('brood monitor', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('brood monitor', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('brood monitor', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('brood of cockroaches', '2004-10-04', 'The loss of life is not optional.').
card_ruling('brood of cockroaches', '2004-10-04', 'The life loss and the return to hand both happen at the beginning of the end step as a single effect. It is not two separate effects.').

card_ruling('brood sliver', '2004-10-04', 'This card\'s controller controls the triggered ability, but the controller of the Sliver that deals damage to a player decides whether or not to put the token onto the battlefield under their control.').
card_ruling('brood sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('brood sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('broodhatch nantuko', '2004-10-04', 'You can choose to put zero tokens or all the tokens onto the battlefield. You can\'t choose a number in between.').

card_ruling('brooding saurian', '2006-07-15', 'If a permanent is enchanted with an Aura such as Confiscate when Brooding Saurian\'s ability resolves, the enchanted permanent\'s owner will regain control of it. The Aura will not be removed. The Aura will still be controlled by its owner, but its effect will be overridden.').

card_ruling('brothers yamazaki', '2004-12-01', 'The two Brothers could be controlled by different players.').
card_ruling('brothers yamazaki', '2004-12-01', 'There are two versions of this card, each with different art. The cards are numbered 160a/306 and 160b/306. The art has no effect on game play.').
card_ruling('brothers yamazaki', '2013-07-01', 'If a third Brothers Yamazaki enters the battlefield, the legend rule will apply. If one player controls more than one Brothers Yamazaki at that time, that player will have choose one and put the rest into his or her graveyard.').

card_ruling('brown ouphe', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('bruna, light of alabaster', '2012-05-01', 'Only Auras that could legally enchant Bruna may be attached to it, whether they\'re being moved from other permanents or being put onto the battlefield. An Aura with \"enchant land,\" for example, could not be attached to Bruna this way.').
card_ruling('bruna, light of alabaster', '2012-05-01', 'All of these Auras will become attached to Bruna before combat damage is dealt. If Bruna is attacking, these Auras become attached before blockers are declared.').

card_ruling('brutal expulsion', '2015-08-25', 'If a spell is returned to its owner’s hand, it’s removed from the stack. The spell isn’t countered, but it won’t resolve. If a copy of a spell is returned to a hand this way, the copy will cease to exist the next time state-based actions are performed.').
card_ruling('brutal expulsion', '2015-08-25', 'The second mode will exile the target creature or planeswalker if it would be put into the graveyard this turn for any reason, not just due to lethal damage. The exile effect applies to that permanent even if Brutal Expulsion deals no damage to it (due to a prevention effect) or Brutal Expulsion deals damage to a different creature or planeswalker (due to a redirection effect).').
card_ruling('brutal expulsion', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('brutal expulsion', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('brutal expulsion', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('brutal expulsion', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('brutal expulsion', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('brutal hordechief', '2014-11-24', 'All blocking declarations must still be legal.').
card_ruling('brutal hordechief', '2014-11-24', 'If there’s a cost associated with having a creature block and you choose for that creature to block, its controller can choose to pay that cost or not. If that player decides to not pay that cost, you must propose a new set of blocking creatures.').
card_ruling('brutal hordechief', '2014-11-24', 'You’ll choose how each creature controlled by an opponent blocks, even if that creature wasn’t on the battlefield or wasn’t controlled by an opponent as the activated ability resolved.').
card_ruling('brutal hordechief', '2014-11-24', 'In a multiplayer game, if more than one player activates Brutal Hordechief’s activated ability on the same turn, the controller of the last ability to resolve will choose how any creatures controlled by players who didn’t resolve this ability will block.').
card_ruling('brutal hordechief', '2014-11-24', 'The defending player affected by Brutal Hordechief’s triggered ability is determined relative to the creature that attacked. For example, if Brutal Hordechief attacked one opponent and two other creatures you control attacked another opponent, the first opponent would lose 1 life and the second opponent would lose 2 life. You’d gain a total of 3 life.').

card_ruling('brutal suppression', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('brute force', '2007-02-01', 'This is the timeshifted version of Giant Growth.').

card_ruling('bubbling muck', '2004-10-04', 'Affects lands with type Swamp, not lands that are named \"Swamp.\"').
card_ruling('bubbling muck', '2004-10-04', 'Affects lands tapped for rest of turn, not just swamps on the battlefield at the time it resolves. This is because it affects players and not the lands themselves.').

card_ruling('budoka gardener', '2004-12-01', 'Budoka Gardener flips if you control ten or more lands when its ability resolves, even if you don\'t use its ability to put a land onto the battlefield.').
card_ruling('budoka gardener', '2013-07-01', 'Budoka Gardener flips even if you put a second copy of a legendary land onto the battlefield. You\'ll have ten lands on the battlefield while the ability is resolving, so the Gardener flips. Then you will choose one of the legendary lands to keep and put the other into its owner\'s graveyard as a state-based action.').

card_ruling('bull aurochs', '2006-07-15', 'Abilities of Aurochs care about other creatures that have creature type Aurochs, which includes (but is not limited to) the Ice Age card namedAurochs.').

card_ruling('bullwhip', '2004-10-04', 'If the creature can attack, it must. If it cannot, then nothing happens.').

card_ruling('bulwark', '2004-10-04', 'Counts cards during resolution.').

card_ruling('burden of guilt', '2011-01-22', 'Only Burden of Guilt\'s controller can activate its ability.').

card_ruling('burgeoning', '2004-10-04', 'Playing a land will trigger it, but putting a land onto the battlefield as part of an effect will not.').

card_ruling('buried alive', '2004-10-04', 'You can look in your library and then choose to find any number from zero to three creatures after you look at it.').

card_ruling('burn', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('burn', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('burn', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('burn', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('burn', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('burn', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('burn', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('burn', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('burn', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('burn', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('burn', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('burn at the stake', '2012-05-01', 'Players may not respond to the paying of costs. Once Burn at the Stake has been announced and put on the stack, no one can try to respond by tapping any creatures.').
card_ruling('burn at the stake', '2012-05-01', 'The amount of damage Burn at the Stake deals won\'t change if any creatures tapped to cast it become untapped or leave the battlefield before it resolves.').
card_ruling('burn at the stake', '2012-05-01', 'All creatures tapped this way become tapped at the same time.').

card_ruling('burn away', '2014-09-20', 'If the creature doesn’t die that turn, the delayed triggered ability will simply cease to exist. It won’t trigger on a future turn.').

card_ruling('burn the impure', '2011-06-01', 'If the creature is an illegal target when Burn the Impure resolves, the spell will be countered and none of its effects will happen, even if that creature has infect.').

card_ruling('burning anger', '2014-07-18', 'Use the power of the enchanted creature as the activated ability resolves to determine how much damage is dealt. If the creature isn’t on the battlefield at that time, use its power as it last existed on the battlefield. Note that this works differently than the fight keyword, which requires both creatures be on the battlefield for any damage to be dealt.').
card_ruling('burning anger', '2014-07-18', 'The enchanted creature is the source of the activated ability, not Burning Anger. For example, if the enchanted creature is green, you could activate the ability choosing a creature with protection from red as the target.').

card_ruling('burning earth', '2013-07-01', 'To tap a land for mana is to activate one of that land’s mana abilities that has {T} in its activation cost.').
card_ruling('burning earth', '2013-07-01', 'The ability will trigger each time a nonbasic land is tapped for mana. Each of these abilities goes on the stack and resolves separately.').
card_ruling('burning earth', '2013-07-01', 'If any nonbasic lands are tapped for mana while a player is casting a spell or activating an ability, Burning Earth’s ability will trigger that many times and wait. When the player finishes casting that spell or activating that ability, Burning Earth’s triggered abilities are put on the stack on top of it. Burning Earth’s abilities will resolve first.').
card_ruling('burning earth', '2013-07-01', 'On the other hand, a player can tap nonbasic lands for mana, put the Burning Earth triggered abilities on the stack, and then respond to those abilities by casting an instant spell or activating an ability using that mana. In that case, the instant spell or activated ability will resolve first.').

card_ruling('burning inquiry', '2009-10-01', 'First the player whose turn it is draws three cards, then each other player in turn order does so. Then the active player randomly chooses the three cards he or she will discard, then each other player in turn order does so. Then all the cards are discarded at the same time.').
card_ruling('burning inquiry', '2009-10-01', 'Each player must discard three cards, even a player that (for some reason) didn\'t actually draw three cards.').

card_ruling('burning of xinye', '2009-10-01', 'Burning of Xinye targets only an opponent. If that player is an illegal target by the time Burning of Xinye resolves, the entire spell is countered.').
card_ruling('burning of xinye', '2009-10-01', 'The lands that will be destroyed aren\'t chosen until Burning of Xinye resolves.').
card_ruling('burning of xinye', '2009-10-01', 'A land with a regeneration shield on it will regenerate from Burning of Xinye\'s destruction effect. However, the spell or ability that creates that shield must be cast or activated before Burning of Xinye starts to resolve.').
card_ruling('burning of xinye', '2009-10-01', 'In a multiplayer game, only lands controlled by two players (you and the targeted opponent) are destroyed. However, Burning of Xinye will deal 4 damage to each creature, even those controlled by other players.').
card_ruling('burning of xinye', '2013-07-01', 'As Burning of Xinye resolves, first you destroy four lands you control (all at the same time), then the targeted opponent destroys four lands he or she controls (all at the same time). That player chooses which of his or her lands to destroy. Neither of you can choose to destroy a land with indestructible.').

card_ruling('burning vengeance', '2011-09-22', 'Although flashback is the most common way to cast a spell from your graveyard, Burning Vengeance will trigger whenever you cast a spell from your graveyard through any means.').
card_ruling('burning vengeance', '2011-09-22', 'Burning Vengeance won\'t trigger when you activate an ability of a card in your graveyard such as unearth or the ability of Reassembling Skeleton.').
card_ruling('burning vengeance', '2011-09-22', 'Burning Vengeance\'s triggered ability will resolve before the spell you cast from your graveyard.').

card_ruling('burning wish', '2004-10-04', 'Can\'t acquire the Ante cards. They are considered still \"in the game\" as are cards in the library and the graveyard.').
card_ruling('burning wish', '2007-05-01', 'The choice is optional.').
card_ruling('burning wish', '2007-05-01', 'If you fail to find something, you still exile this.').
card_ruling('burning wish', '2007-09-16', 'Can\'t acquire cards that are phased out.').
card_ruling('burning wish', '2009-10-01', 'You can\'t acquire exiled cards because those cards are still in one of the game\'s zones.').
card_ruling('burning wish', '2009-10-01', 'In a sanctioned event, a card that\'s \"outside the game\" is one that\'s in your sideboard. In an unsanctioned event, you may choose any card from your collection.').

card_ruling('burning-eye zubera', '2005-06-01', 'Burning-Eye Zubera\'s ability counts all damage dealt to it this turn, from all sources.').

card_ruling('burning-tree bloodscale', '2006-02-01', 'If both abilities are used on the same creature, it can\'t block Burning-Tree Bloodscale.').

card_ruling('burning-tree emissary', '2013-01-24', 'Burning-Tree Emissary\'s enters-the-battlefield ability isn\'t a mana ability. It uses the stack and can be responded to.').

card_ruling('burning-tree shaman', '2006-02-01', 'A mana ability is an ability that adds mana to your mana pool, not one that costs mana to activate.').
card_ruling('burning-tree shaman', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').

card_ruling('burnout', '2008-10-01', 'You may target any instant spell with Burnout. If it\'s blue at the time Burnout resolves, that spell will be countered and you\'ll draw a card at the beginning of the next turn\'s upkeep. If it\'s not blue at the time Burnout resolves, that spell won\'t be countered, but you\'ll still draw a card at the beginning of the next turn\'s upkeep.').

card_ruling('burnt offering', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('burnt offering', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('burrenton forge-tender', '2007-10-01', 'If the red source you chose changes colors before the damage is dealt, the damage-prevention effect doesn\'t apply.').

card_ruling('burst of speed', '2009-10-01', 'Burst of Speed affects only creatures you control at the time it resolves. It won\'t affect creatures that come under your control later in the turn.').

card_ruling('burst of strength', '2013-01-24', 'Burst of Strength can target and put a +1/+1 counter on a creature that\'s already untapped.').

card_ruling('butcher of malakir', '2010-03-01', 'When the triggered ability resolves, first the player whose turn it is (if that player is an opponent) chooses which creature he or she will sacrifice, then each other opponent in turn order does the same, then all chosen creatures are sacrificed at the same time.').
card_ruling('butcher of malakir', '2010-03-01', 'If multiple creatures you control (possibly including Butcher of Malakir itself) are put into their owners\' graveyards at the same time, Butcher of Malakir\'s triggered ability will trigger that many times.').
card_ruling('butcher of malakir', '2010-03-01', 'If you control more than one Butcher of Malakir and a creature you control is put into a graveyard, each of those Butchers\' abilities will trigger. Each opponent will sacrifice a creature each time one of those abilities resolves.').
card_ruling('butcher of malakir', '2010-03-01', 'If you and an opponent each control a Butcher of Malakir and a creature is put into a graveyard, a chain reaction happens. First the ability of one player\'s Butcher will trigger, causing each opponent to sacrifice a creature. That sacrifice causes the ability of the other player\'s Butcher to trigger, and so on.').

card_ruling('butcher of the horde', '2014-09-20', 'You choose which ability Butcher of the Horde gains when the activated ability resolves.').

card_ruling('butcher orgg', '2004-10-04', 'If it is blocked but then all of its blockers are removed before combat damage is assigned, then it won\'t be able to deal combat damage and you won\'t be able to use its ability.').
card_ruling('butcher orgg', '2004-10-04', 'You can use the ability to divide damage even if it is not blocked.').
card_ruling('butcher orgg', '2004-10-04', 'If you choose to use the ability to divide up the damage (and there is at least one point of damage to be assigned), you must choose at least one creature or the defending player, and can\'t choose more total creatures and players than the amount of damage being assigned (minimum 1 point per player or creature).').

