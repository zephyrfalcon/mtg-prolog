% Rulings

card_ruling('cabal archon', '2004-10-04', 'It can sacrifice itself.').

card_ruling('cabal therapy', '2004-10-04', 'You name the card during resolution, then your opponent reveals their hand and discards if appropriate. There is no way for your opponent to do anything between you naming the card and them discarding.').

card_ruling('cache raiders', '2008-08-01', 'This ability isn\'t targeted. You choose a permanent to return when the ability resolves. No one will be able to respond to the choice.').

card_ruling('cached defenses', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('cached defenses', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('cackling counterpart', '2011-09-22', 'The token copies exactly what was printed on the original creature and nothing else (unless that creature is copying something else or is a token; see below). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('cackling counterpart', '2011-09-22', 'If you copy a double-faced creature, the token will be a copy of the face that\'s up when the token is created. Because the token is not a double-faced card, it won\'t be able to transform.').
card_ruling('cackling counterpart', '2011-09-22', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('cackling counterpart', '2011-09-22', 'If the copied creature is copying something else (for example, if the copied creature is an Evil Twin), then the token enters the battlefield as whatever that creature copied.').
card_ruling('cackling counterpart', '2011-09-22', 'If the copied creature is a token, the token that\'s created copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('cackling counterpart', '2011-09-22', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').

card_ruling('cackling fiend', '2004-10-04', 'If the opponent has no cards in hand, this has no effect.').

card_ruling('caged sun', '2011-06-01', 'Caged Sun\'s triggered ability is a mana ability, which means the ability doesn\'t use the stack and can\'t be responded to.').

card_ruling('cairn wanderer', '2007-10-01', 'Cairn Wanderer\'s ability looks at all cards in all graveyards. It gains any landwalk abilities and any protection abilities.').

card_ruling('calciderm', '2007-02-01', 'This is the timeshifted version of Blastoderm.').

card_ruling('calcite snapper', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('calcite snapper', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('calcite snapper', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('calcite snapper', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('calculated dismissal', '2015-06-22', 'If the spell mastery ability applies, you’ll scry 2 even if the controller of the spell pays {3}.').
card_ruling('calculated dismissal', '2015-06-22', 'In one unusual situation, you can cast Calculated Dismissal targeting an instant or sorcery spell you control while there is one instant or sorcery card in your graveyard. In this situation, if you decline to pay {3}, the spell will be countered and put into your graveyard. The spell mastery ability will then apply and you’ll scry 2.').
card_ruling('calculated dismissal', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('caldera hellion', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('caldera hellion', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('caldera hellion', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('caldera hellion', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').
card_ruling('caldera hellion', '2008-10-01', 'Caldera Hellion will deal 3 damage to itself (as well as to each other creature) when its enters-the-battlefield ability resolves. This damage will be lethal if Caldera Hellion hasn\'t devoured any creatures and its toughness hasn\'t been increased by any other means.').

card_ruling('caldera kavu', '2004-10-04', 'You choose a color on resolution.').

card_ruling('call', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('call', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('call', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('call', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('call', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('call', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('call', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('call', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('call', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('call', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('call', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('call', '2013-04-15', 'If you cast Beck/Call as a fused split spell, the triggered ability it creates will be in effect when the Bird creature tokens enter the battlefield. You\'ll draw up to four cards.').

card_ruling('call for blood', '2005-02-01', 'Uses last known information for the sacrificed creature\'s power.').
card_ruling('call for blood', '2005-02-01', 'You may sacrifice the creature chosen as the target for Call for Blood (if you control it) when you pay the additional cost for the spell. If you do, Call for Blood is countered on resolution.').
card_ruling('call for blood', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('call for blood', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('call of the full moon', '2015-06-22', 'Call of the Full Moon’s last ability will look at the entire previous turn, even if Call of the Moon wasn’t on the battlefield for some or all of that turn. For example, if you cast Call of the Full Moon and another spell on the same turn, you’ll have to sacrifice Call of the Full Moon at the beginning of the upkeep of the following turn.').
card_ruling('call of the full moon', '2015-06-22', 'A single player must have cast two or more spells during the previous turn for Call of the Full Moon’s last ability to trigger. If multiple players each cast just one spell during that turn, the ability won’t trigger.').

card_ruling('call of the nightwing', '2013-01-24', 'You can exile Call of the Nightwing encoded on the Horror creature token it just created.').
card_ruling('call of the nightwing', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('call of the nightwing', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('call of the nightwing', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('call of the nightwing', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('call of the nightwing', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('call of the nightwing', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('call of the nightwing', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('call of the nightwing', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('call of the nightwing', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('call of the nightwing', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('call of the nightwing', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('call the scions', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('call the scions', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('call the scions', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('call the scions', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('call the scions', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('call the scions', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('call the scions', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('call the scions', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('call the scions', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('call the skybreaker', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('call the skybreaker', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('call the skybreaker', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('call the skybreaker', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('call to arms', '2004-10-04', 'You can\'t choose \"colorless\" as a color.').
card_ruling('call to arms', '2009-10-01', 'Call to Arms affects all white creatures, not just white creatures you control.').
card_ruling('call to arms', '2009-10-01', 'If Call to Arms changes controllers, it will continue to check the nontoken permanents controlled by the player chosen as it entered the battlefield, even if that player isn\'t an opponent of its current controller.').
card_ruling('call to arms', '2009-10-01', 'The game continually counts the number of nontoken permanents of each color the chosen player controls. Multicolored ones are counted for each of their colors; colorless ones are ignored. The moment the number of nontoken permanents of the chosen color that player controls is less than or equal to the number of permanents of one of the other colors that player controls, Call to Arms\'s second ability stops giving white creatures +1/+1 and its third ability triggers. Call to Arms will be sacrificed when the third ability resolves, even if those numbers have changed by then.').
card_ruling('call to arms', '2009-10-01', 'Call to Arms\'s third ability is a \"state trigger.\" Once a state trigger triggers, it won\'t trigger again as long as the ability is on the stack. If the ability is countered and the trigger condition is still true, it will immediately trigger again.').

card_ruling('call to heel', '2008-10-01', 'The player who draws a card is the player who controls the creature when Call to Heel resolves. This may not be the player whose hand the creature is put into.').
card_ruling('call to heel', '2008-10-01', 'If the targeted creature becomes an illegal target before Call to Heel resolves, Call to Heel is countered. No one draws a card.').

card_ruling('call to serve', '2012-05-01', 'If the enchanted creature becomes black, Call to Serve will be put into its owner\'s graveyard the next time state-based actions are performed.').

card_ruling('call to the grave', '2011-09-22', 'If a player controls no creatures, or each creature he or she controls is a Zombie, that player does nothing when the first ability resolves.').
card_ruling('call to the grave', '2011-09-22', 'If there is at least one creature on the battlefield at the beginning of the end step, the last ability won\'t trigger. If it does trigger, but a creature enters the battlefield before it resolves, the ability won\'t do anything when it resolves. Call to the Grave\'s controller won\'t sacrifice it.').
card_ruling('call to the grave', '2011-09-22', 'In a Two-Headed Giant game, Call to the Grave will trigger twice during each team\'s upkeep, once for each player. Each of the two players will sacrifice a non-Zombie creature when the ability referring to that player resolves.').

card_ruling('call to the kindred', '2011-01-22', 'The creature need only share one creature type with enchanted creature, not necessarily all of them. For example, if the enchanted creature was a Human Soldier, you could put a Human Wizard creature card, a Goblin Soldier creature card, or a Human Soldier Ally creature card onto the battlefield.').

card_ruling('calming licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('calming licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('calming verse', '2004-10-04', 'It checks if you control an untapped land during resolution.').

card_ruling('camel', '2004-10-04', 'It does prevent damage from animated Deserts in combat.').
card_ruling('camel', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('camel', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('camel', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('camel', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('canal dredger', '2014-05-29', 'If you draft a card named Canal Dredger, you pass the last card from each booster pack to yourself. In other words, when you are passed two cards, you draft both of them.').
card_ruling('canal dredger', '2014-05-29', 'If multiple players have drafted a card named Canal Dredger, each player chooses which of those players to pass the last card to.').
card_ruling('canal dredger', '2014-05-29', 'You draft all cards passed to you this way one at a time. If you also drafted a Cogwork Grinder, you may remove any of those cards from the draft.').

card_ruling('candelabra of tawnos', '2004-10-04', 'You may untap your opponent\'s lands if desired.').
card_ruling('candelabra of tawnos', '2004-10-04', 'Can be used on an untapped land.').
card_ruling('candelabra of tawnos', '2004-10-04', 'This is not mana ability. It is a normal ability and it will resolve along with other spells and abilities on the stack. The lands untap during resolution.').

card_ruling('candles of leng', '2006-09-25', 'Unless something weird happens, the card that\'s drawn will be the card that was revealed.').

card_ruling('candles\' glow', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('candles\' glow', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('candles\' glow', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('candles\' glow', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('cankerous thirst', '2008-08-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('cankerous thirst', '2008-08-01', 'When you cast the spell, you choose its targets before you pay for it.').
card_ruling('cankerous thirst', '2008-08-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('cankerous thirst', '2008-08-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('cankerous thirst', '2008-08-01', 'As you cast Cankerous Thirst, you must target a creature for the first effect and you must target a creature for the second effect. Both targets may be the same. As Cankerous Thirst resolves, you decide whether to apply either effect.').

card_ruling('cannibalize', '2008-05-01', 'You choose the two target creatures during announcement. You choose which gets the counters and which is to get exiled on resolution.').

card_ruling('canopy cover', '2013-04-15', 'This is not the same as hexproof. If, for example, you target one of your opponent\'s creatures, your opponents won\'t be able to target their own creature with spells or abilities.').

card_ruling('canopy crawler', '2004-10-04', 'The number of +1/+1 counters for the ability is checked when the ability resolves.').

card_ruling('canopy spider', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('canopy vista', '2015-08-25', 'Even though these lands have basic land types, they are not basic lands because “basic” doesn’t appear on their type line. Notably, controlling two or more of them won’t allow others to enter the battlefield untapped.').
card_ruling('canopy vista', '2015-08-25', 'However, because these cards have basic land types, effects that specify a basic land type without also specifying that the land be basic can affect them. For example, a spell or ability that reads “Destroy target Forest” can target Canopy Vista, while one that reads “Destroy target basic Forest” cannot.').
card_ruling('canopy vista', '2015-08-25', 'If one of these lands enters the battlefield at the same time as any number of basic lands, those other lands are not counted when determining if this land enters the battlefield tapped or untapped.').

card_ruling('canyon lurkers', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('canyon lurkers', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('canyon lurkers', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('canyon lurkers', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('canyon lurkers', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('canyon lurkers', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('canyon lurkers', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('canyon lurkers', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('canyon lurkers', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('cao cao, lord of wei', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('cao ren, wei commander', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('capricious efreet', '2009-10-01', 'If Capricious Efreet is the only nonland permanent you control when its ability triggers, you\'ll have to target it.').
card_ruling('capricious efreet', '2009-10-01', 'You may target zero, one, or two nonland permanents you don\'t control.').
card_ruling('capricious efreet', '2009-10-01', 'You target between one and three permanents as you put the ability on the stack. You don\'t randomly choose which one will be destroyed until the ability resolves. If one of those permanents has become an illegal target by then, you randomly choose between the remaining ones.').
card_ruling('capricious efreet', '2009-10-01', 'As the ability resolves, there is no time to react between the time a permanent is chosen at random and the time it\'s destroyed. If you want to put a regeneration shield on one of those permanents, or sacrifice it for some effect, or anything else, you must do so before the ability resolves (and before you know which one of the permanents will be chosen at random).').
card_ruling('capricious efreet', '2013-07-01', 'If one of the targets has indestructible, it can be chosen at random but nothing will happen to it if it is.').

card_ruling('capricious sorcerer', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('captain of the watch', '2009-10-01', 'Captain of the Watch grants +1/+1 and vigilance to all other Soldier creatures you control, not just the ones its ability puts onto the battlefield.').

card_ruling('captain sisay', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('captain\'s maneuver', '2004-10-04', 'If the second target is no longer on the battlefield, or if the second target is no longer a creature or player when the damage would be redirected, the damage is dealt to the first target as if this spell was not cast.').
card_ruling('captain\'s maneuver', '2004-10-04', 'If the first target is no longer on the battlefield, then the damage will not be dealt at all, so it can\'t be redirected by this ability.').

card_ruling('captivating glance', '2007-10-01', 'The ability triggers at the end of Captivating Glance\'s controller\'s turn. \"You\" refers to the controller of Captivating Glance, not the controller of the enchanted creature.').
card_ruling('captivating glance', '2007-10-01', 'The opponent you clash with doesn\'t have to be the controller of the enchanted creature.').
card_ruling('captivating glance', '2007-10-01', 'If you win the clash, you gain control of the enchanted creature (even if the other player won the clash too; this can happen if a split card is revealed). If you don\'t win the clash, the other player gains control of the enchanted creature (even if that player didn\'t win the clash either).').
card_ruling('captivating glance', '2007-10-01', 'Captivating Glance\'s controller never changes as a result of this card.').
card_ruling('captivating glance', '2007-10-01', 'Captivating Glance may cause a player to gain control of a creature he or she already controls. This layers a new control effect on that creature, but doing so rarely has any visible effect.').
card_ruling('captivating glance', '2007-10-01', 'Captivating Glance\'s control-change effect continues to apply even if Captivating Glance leaves the battlefield.').

card_ruling('captivating vampire', '2010-08-15', 'Since Captivating Vampire\'s activated ability doesn\'t have a tap symbol in its cost, you can tap a Vampire (including Captivating Vampire itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').
card_ruling('captivating vampire', '2010-08-15', 'The effect of Captivating Vampire\'s activated ability has no duration. You retain control of the affected creature until the game ends, the creature leaves the battlefield, or a later effect causes someone else to gain control of it. It doesn\'t matter whether Captivating Vampire remains on the battlefield. Similarly, the affected creature remains a Vampire in addition to its other types until the game ends, the creature leaves the battlefield, or a later effect changes its types or subtypes.').
card_ruling('captivating vampire', '2010-08-15', 'Gaining control of a creature doesn\'t cause you gain control of any Auras or Equipment attached to it.').

card_ruling('captured sunlight', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('captured sunlight', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('captured sunlight', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('captured sunlight', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('captured sunlight', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('captured sunlight', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('captured sunlight', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('carapace', '2004-10-04', 'This leaves the battlefield when you activate its activated ability, but the enchanted creature won\'t be regenerated until the ability resolves. In the intervening time, the creature will no longer have the bonus that this had been giving it. This may cause the creature to be destroyed before the regeneration shield is created.').

card_ruling('caravan escort', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('caravan escort', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('caravan escort', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('caravan escort', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('caravan escort', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('caravan vigil', '2011-09-22', 'You can choose to put the basic land card into your hand even if a creature died the turn you cast Caravan Vigil.').

card_ruling('caribou range', '2007-10-01', 'You can only sacrifice a Caribou token to this, not any creature with the type Caribou.').

card_ruling('carnage gladiator', '2013-04-15', 'Carnage Gladiator\'s ability triggers whenever any creature blocks, regardless of who controls that creature or which creature it blocked.').
card_ruling('carnage gladiator', '2013-04-15', 'If a creature somehow blocks multiple creatures (because it has the ability to block an additional creature, for example), Carnage Gladiator\'s ability triggers only once.').

card_ruling('carnifex demon', '2011-01-01', 'As Carnifex Demon\'s last ability resolves, you\'ll put a -1/-1 counter on each creature on the battlefield -- including creatures you control -- except for that Carnifex Demon.').

card_ruling('carnival hellsteed', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('carnival hellsteed', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('carnival hellsteed', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('carnival hellsteed', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('carnival of souls', '2004-10-04', 'This is not optional.').

card_ruling('carom', '2006-05-01', 'If either target has left the battlefield before the damage would be dealt, that damage isn\'t redirected.').
card_ruling('carom', '2006-05-01', 'Carom\'s controller draws a card when Carom resolves, not when the damage is redirected.').

card_ruling('carpet of flowers', '2004-10-04', 'You can add less than X mana if you want, even zero.').
card_ruling('carpet of flowers', '2004-10-04', 'You can target a different opponent each turn.').
card_ruling('carpet of flowers', '2004-10-04', 'The ability is not a mana ability.').

card_ruling('carrier thrall', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('carrier thrall', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('carrier thrall', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('carrier thrall', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('carrion', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('carrion', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('carrion beetles', '2004-10-04', 'If any of the targets are not there on resolution, the others are still affected.').
card_ruling('carrion beetles', '2004-10-04', 'You pick the 0, 1, 2, or 3 target cards on announcement.').

card_ruling('carrion call', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('carrion call', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('carrion call', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('carrion call', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('carrion call', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('carrion call', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('carrion thrash', '2008-10-01', 'You choose a target when the ability triggers. You decide whether to pay {2} when the ability resolves.').
card_ruling('carrion thrash', '2008-10-01', 'If Carrion Thrash and another creature are put into a graveyard from the battlefield at the same time, you may target the other creature with Carrion Thrash\'s ability.').

card_ruling('carrionette', '2004-10-04', 'Since it is exiled as part of the effect and not part of the cost, you can activate its ability more than once.').
card_ruling('carrionette', '2004-10-04', 'A creature with Protection from Creatures can\'t be targeted by this card\'s ability.').

card_ruling('carry away', '2004-12-01', 'Normally, gaining control of an Equipment doesn\'t change what it\'s attached to. However, Carry Away has a triggered ability that unattaches the Equipment from whatever it\'s attached to. The Equipment\'s new controller can activate its equip ability to move it onto a creature he or she controls.').

card_ruling('cartel aristocrat', '2013-01-24', 'You choose the color when the ability resolves.').

card_ruling('cast through time', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('cast through time', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('cast through time', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('cast through time', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('cast through time', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('cast through time', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('cast through time', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('cast through time', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('cast through time', '2010-06-15', 'Multiple instances of rebound on the same spell are redundant.').
card_ruling('cast through time', '2010-06-15', 'The rebound effect is not optional. Each instant and sorcery spell you cast from your hand is exiled instead of being put into your graveyard as it resolves, whether you want it to be or not. Casting the spell during your next upkeep is optional, however.').
card_ruling('cast through time', '2010-06-15', 'If a spell moves itself into another zone as part of its resolution (as Arc Blade, All Suns\' Dawn, and Beacon of Unrest do), rebound won\'t get a chance to apply.').
card_ruling('cast through time', '2010-06-15', 'If a spell you cast from your hand has both rebound and buyback (and the buyback cost was paid), you choose which effect to apply as it resolves.').
card_ruling('cast through time', '2010-06-15', 'You\'ll be able to cast a spell with flashback three times this way. First you can cast it from your hand. It will be exiled due to rebound as it resolves. Then you can cast it from exile due to rebound\'s delayed triggered ability. It will be put into your graveyard as it resolves. Then you can cast it from your graveyard due to flashback. It will be exiled due to flashback as it resolves.').
card_ruling('cast through time', '2010-06-15', 'For the rebound effect to happen, Cast Through Time needs to be on the battlefield as the spell _finishes_ resolving. For example, if you cast Warp World from your hand, and as part of its resolution it puts Cast Through Time onto the battlefield, Warp World will rebound. Conversely, if Warp World shuffles your Cast Through Time into your library as part of its resolution, and doesn\'t put another one onto the battlefield, it will not rebound.').
card_ruling('cast through time', '2010-06-15', 'If you cast an instant or sorcery spell from your hand and it\'s exiled due to rebound, the delayed triggered ability will allow you to cast it during your next upkeep even if Cast Through Time has left the battlefield by then.').
card_ruling('cast through time', '2010-06-15', 'If you cast a card from exile \"without paying its mana cost,\" you can\'t pay any alternative costs. Any X in the mana cost will be 0. On the other hand, if the card has optional additional costs (such as kicker or multikicker), you may pay those when you cast the card. If the card has mandatory additional costs (such as Momentous Fall does), you must pay those if you choose to cast the card.').
card_ruling('cast through time', '2010-06-15', 'If a spell has restrictions on when it can be cast (for example, \"Cast [this spell] only during the declare blockers step\"), those restrictions may prevent you from casting it from exile during your upkeep.').
card_ruling('cast through time', '2010-06-15', 'If you cast a spell using the madness or suspend abilities, you\'re casting it from exile, not from your hand. Although those spells will have rebound, the ability won\'t have any effect.').
card_ruling('cast through time', '2010-06-15', 'Similarly, if you gain control of an instant or sorcery spell with Commandeer, it will have rebound, but the ability won\'t do anything because that spell wasn\'t cast from your hand.').

card_ruling('cataclysm', '2004-10-04', 'If you control a permanent with more than one type, you can choose that same permanent for more than one of the choices if you want to. This makes it possible to select an artifact creature as both your artifact and creature, and then select a land and thereby keep only two cards.').
card_ruling('cataclysm', '2004-10-04', 'If you control a permanent with more than one type, you can choose that permanent to be the representative of either type. This makes it possible to have more than one creature (or other permanent type) on the battlefield. For example, if you select a creature for your creature and an artifact creature for your artifact, you get to keep both of these creatures.').
card_ruling('cataclysm', '2004-10-04', 'The permanents to save are chosen on resolution. You do not choose them when casting the spell.').
card_ruling('cataclysm', '2004-10-04', 'The current player makes their choices before the other player. After both players make choices, all the sacrifices are done simultaneously.').
card_ruling('cataclysm', '2014-02-01', 'Each permanent not chosen by a player will be sacrificed. If a permanent has none of the listed types (for example, a Planeswalker with no other types), it can\'t be chosen.').

card_ruling('catacomb sifter', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('catacomb sifter', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('catacomb sifter', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('catacomb sifter', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('catacomb sifter', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('catacomb sifter', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('catacomb sifter', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('catacomb sifter', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('catacomb sifter', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('catapult master', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('catapult master', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('catapult squad', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('catapult squad', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('catastrophe', '2004-10-04', 'You choose land or creatures when the spell resolves.').

card_ruling('catch', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('catch', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('catch', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('catch', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('catch', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('catch', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('catch', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('catch', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('catch', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('catch', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('catch', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('catch', '2013-04-15', 'Catch can target any permanent, including one that\'s untapped or one you already control.').
card_ruling('catch', '2013-04-15', 'If you cast Catch/Release as a fused split spell, you can sacrifice the permanent you just gained control of.').

card_ruling('cateran brute', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran enforcer', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran kidnappers', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran overlord', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran persuader', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran slaver', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('cateran summons', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('caterwauling boggart', '2007-10-01', 'Each creature you control that\'s both a Goblin and an Elemental can\'t be blocked except by two or more creatures (as opposed to four or more creatures).').

card_ruling('cathars\' crusade', '2012-05-01', 'The creature that entered the battlefield and caused the ability to trigger will also get a +1/+1 counter, provided it\'s still on the battlefield when the ability resolves.').

card_ruling('cathedral membrane', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('cathedral membrane', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('cathedral membrane', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('cathedral membrane', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('cathedral membrane', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('cathedral membrane', '2011-06-01', 'Cathedral Membrane\'s ability will trigger no matter why it\'s put into a graveyard during the combat phase, not just because of combat damage.').

card_ruling('cathodion', '2004-10-04', 'The ability is never a mana ability.').
card_ruling('cathodion', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('cauldron dance', '2004-10-04', 'If the targeted creature card in the graveyard is removed before resolution, this spell will be countered since its only target will be invalid.').
card_ruling('cauldron dance', '2004-10-04', 'You can cast this spell when you have no creature card in your hand. If this happens, simply ignore that part of the spell. You still put the graveyard creature onto the battlefield and still return it to your hand at the beginning of the end step.').
card_ruling('cauldron dance', '2004-10-04', 'You must have a creature in your graveyard to cast this spell since it requires a valid target.').

card_ruling('cauldron haze', '2008-08-01', 'If a nontoken creature that gains persist this way is put into a graveyard, that card will be returned to the battlefield with a -1/-1 counter on it. However, because it\'s a new object with no relation to its previous existence, that permanent will not have persist.').
card_ruling('cauldron haze', '2008-08-01', 'If a creature has multiple instances of persist, the result is largely the same as having just one instance of persist. When the creature is put into a graveyard, each persist ability will trigger. The first one to resolve will return the creature to the battlefield with a -1/-1 counter on it. The rest will do nothing.').
card_ruling('cauldron haze', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('cauldron haze', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('cauldron haze', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('cauldron haze', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('cauldron haze', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('cauldron haze', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('cauldron haze', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('cauldron of souls', '2008-05-01', 'If a nontoken creature that gains persist this way is put into a graveyard, that card will be returned to the battlefield with a -1/-1 counter on it. However, because it\'s a new object with no relation to its previous existence, the returned creature will not have persist.').
card_ruling('cauldron of souls', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('cauldron of souls', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('cauldron of souls', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('cauldron of souls', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('cauldron of souls', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('cauldron of souls', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('cauldron of souls', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('caustic crawler', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('caustic crawler', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('cautery sliver', '2007-02-01', 'Note that the first ability can affect any kind of creature, but the second ability can affect only Slivers. Both abilities can affect players.').
card_ruling('cautery sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('cautery sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('cavalry master', '2006-09-25', 'Cavalry Master adds only one instance of flanking per creature, even if that creature already has multiple instances of flanking.').
card_ruling('cavalry master', '2006-09-25', 'If a creature without flanking is given flanking by another effect, then this ability will grant a second instance. Similarly, if a creature loses flanking, then it won\'t get the Calvary Master instance either.').

card_ruling('cavalry pegasus', '2013-09-15', 'Once the triggered ability resolves, the attacking Humans will continue to have flying even if Cavalry Pegasus leaves the battlefield.').
card_ruling('cavalry pegasus', '2013-09-15', 'The attacking Humans gain flying before blockers are declared.').

card_ruling('cavern harpy', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('cavern lampad', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('cavern lampad', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('cavern lampad', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('cavern lampad', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('cavern lampad', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('cavern lampad', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('cavern lampad', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('cavern of souls', '2012-05-01', 'You must choose an existing _Magic_ creature type, such as Zombie or Warrior. Card types such as artifact can\'t be chosen.').
card_ruling('cavern of souls', '2012-05-01', 'The spell can\'t be countered if the mana produced by Cavern of Souls is spent to cover any cost of the spell, even an additional cost such as a kicker cost. This is true even if you use the mana to pay an additional cost while casting a spell \"without paying its mana cost.\"').

card_ruling('caverns of despair', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('caves of koilos', '2015-06-22', 'The damage dealt to you is part of the second mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('caves of koilos', '2015-06-22', 'Like most lands, each land in this cycle is colorless. The damage dealt to you is dealt by a colorless source.').

card_ruling('cease-fire', '2004-10-04', 'This spell will not counter any creature spells on the stack. It only prevents new creature spells from being cast after it resolves.').

card_ruling('ceaseless searblades', '2007-10-01', 'This triggers whenever you activate an activated ability of an Elemental permanent, but not when you activate an activated ability of an Elemental source that\'s not on the battlefield.').

card_ruling('celestial archon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('celestial archon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('celestial archon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('celestial archon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('celestial archon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('celestial archon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('celestial archon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('celestial colonnade', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('celestial colonnade', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').

card_ruling('celestial crusader', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('celestial crusader', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('celestial crusader', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('celestial crusader', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('celestial crusader', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('celestial crusader', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('celestial dawn', '2004-10-04', 'The effect to turn all your non-land cards (including artifacts) white is the effect of a static ability. Thus, a color change on a permanent prior to Celestial Dawn entering the battlefield will be overridden by Celestial Dawn\'s effect.').
card_ruling('celestial dawn', '2004-10-04', 'Celestial Dawn does not change the type of lands which are not on the battlefield. Lands not on the battlefield are what they say they are.').
card_ruling('celestial dawn', '2004-10-04', 'Will not add or remove Snow Supertype from a land.').
card_ruling('celestial dawn', '2004-10-04', 'You may use a different color mana than the color required for spells and abilities that require a specific color. If you do, and the spell or ability checks the actual color of the mana, it can tell the difference.').
card_ruling('celestial dawn', '2004-10-04', 'If you pay white mana to activate an ability that can only be activated by spending another color of mana, then any limitations on activating the ability (such as the number of times it can be activated each turn) still apply as if you spent the proper color.').
card_ruling('celestial dawn', '2004-10-04', 'Celestial Dawn no longer changes mana symbols on cards.').
card_ruling('celestial dawn', '2006-09-25', 'Mana produced by spells you control and nonland permanents you control isn\'t affected by Celestial Dawn. Llanowar Elves will produce {G}, for example. However, any colored mana may be spent only on the colorless part of costs.').
card_ruling('celestial dawn', '2006-09-25', 'Land cards you own that aren\'t on the battlefield aren\'t changed to Plains.').
card_ruling('celestial dawn', '2006-09-25', 'If you have nonwhite mana in your mana pool when Celestial Dawn enters the battlefield, it remains there but you can spend it only on the colorless part of costs.').
card_ruling('celestial dawn', '2006-09-25', '\"Enters the battlefield\" triggered abilities of lands you play won\'t trigger since the lands will enter the battlefield as Plains. Effects that modify how those lands enter the battlefield, however, will still work. For example, if you play a Dimir Aqueduct, it will enter the battlefield tapped as a Plains, but you won\'t return a land to your hand.').
card_ruling('celestial dawn', '2006-09-25', 'If you use a text-changing effect such as Mind Bend on Celestial Dawn to change the word \"Plains\" to a different land type, your lands are all of that new land type, and they will produce mana of the appropriate color for that type.').
card_ruling('celestial dawn', '2006-09-25', 'If a permanent enters the battlefield while Celestial Dawn is on the battlefield, and then Celestial Dawn leaves the battlefield, the permanent\'s color will revert to the colors in its mana cost.').

card_ruling('celestial flare', '2013-07-01', 'Celestial Flare targets only a player, not any creature. A creature with hexproof or protection from white can be sacrificed this way.').
card_ruling('celestial flare', '2013-07-01', 'If the player sacrifices a blocking creature, any attacking creature it was blocking remains blocked. Unless that attacking creature has trample or is being blocked by another creature, it won’t assign or deal combat damage.').
card_ruling('celestial flare', '2013-07-01', 'Creatures continue to be attacking or blocking creatures through the end of combat step. It is possible to cast Celestial Flare within combat but after combat damage is dealt (specifically, during the combat damage step or the end of combat step). Only attacking and blocking creatures that survived combat damage can be sacrificed at this time.').

card_ruling('celestial force', '2011-09-22', 'In a Two-Headed Giant game, this ability triggers once at the beginning of each team\'s upkeep.').

card_ruling('celestial gatekeeper', '2004-10-04', 'You can choose to target this card as one of the two Bird and/or Cleric cards. If you do, it gets exiled and does not get returned to the battlefield.').

card_ruling('celestial kirin', '2005-06-01', 'If the Spirit or Arcane spell has {X} in the mana cost, then you use the value of {X} on the stack. For example, Shining Shoal costs {X}{W}{W}. If you choose X = 2, then Shining Shoal\'s converted mana cost is 4. Shining Shoal also has an ability that says \"You may exile a white card with converted mana cost X from your hand rather than pay Shining Shoal\'s mana cost\"; if you choose to pay the alternative cost and exile a card with converted mana cost 2, then X is 2 while Shining Shoal is on the stack and its converted mana cost is 4.').

card_ruling('celestial mantle', '2009-10-01', 'At the time the ability triggers, determine which creature Celestial Mantle is enchanting. As the ability resolves, determine who currently controls that creature (or, if it\'s no longer on the battlefield, determine who controlled it when it left). That\'s the player whose life total is doubled. It doesn\'t matter who controls Celestial Mantle, who controlled the creature at the time the ability triggered, or what creature Celestial Mantle enchants at the time the ability resolves.').
card_ruling('celestial mantle', '2009-10-01', 'If a creature dealing combat damage at the same time as the enchanted creature has lifelink, the life gained due to lifelink happens before Celestial Mantle\'s triggered ability resolves.').
card_ruling('celestial mantle', '2009-10-01', 'If the enchanted creature\'s controller has a life total below 0 (which is possible if that player controls Platinum Angel, for example), Celestial Mantle\'s triggered ability multiplies that number by two. For example, if the player had -4 life, that player\'s life total becomes -8.').
card_ruling('celestial mantle', '2009-10-01', 'If a player\'s life total is doubled, that player actually gains or loses the necessary amount of life. For example, if the life total of the enchanted creature\'s controller is 14 when Celestial Mantle\'s triggered ability resolves, the ability causes that player to gain 14 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('celestial mantle', '2010-06-15', 'In a Two-Headed Giant game, Celestial Mantle\'s essentially doubles the team\'s life-total. Specifically, the triggered ability will affect one player\'s life total, and then the team\'s life total is adjusted by the amount of life the player gains as a result of this ability. Suppose a creature enchanted by Celestial Mantle deals combat damage to an opponent, and that creature\'s controller\'s team has 11 life. That player then has 11 life, so it\'s doubled to 22, for a net gain of 11 life. The team\'s life total becomes 22 (11 + 11).').

card_ruling('celestial purge', '2009-10-01', 'Lands are colorless (even if their frames have some colored elements to them). You can\'t target a Swamp, a Mountain, or any other land with Celestial Purge (unless some other effect has turned that land black or red).').

card_ruling('cemetery puca', '2008-05-01', 'This effect has no duration. If you use the ability, Cemetery Puca will remain a copy of the creature until it leaves the battlefield or you use the ability again. If it becomes a copy of a different creature card, the new copy will overwrite the old copy.').
card_ruling('cemetery puca', '2008-05-01', 'Cemetery Puca copies the printed values of the creature, plus any copy effects that have been applied to it. It won\'t copy other effects that have changed the creature\'s power, toughness, types, color, or so on. It also won\'t copy counters on the creature (it\'ll just retain the counters it already has).').
card_ruling('cemetery puca', '2008-05-01', 'If Cemetery Puca becomes a copy of a token, it copies the original characteristics of that token as stated by the effect that put it onto the battlefield. It does not become a token.').
card_ruling('cemetery puca', '2008-05-01', 'Cemetery Puca\'s ability isn\'t targeted. It can copy a creature with shroud or protection.').
card_ruling('cemetery puca', '2008-05-01', 'If multiple creatures are put into their owners\' graveyards at the same time, Cemetery Puca\'s ability will trigger once for each of those creatures. You choose the order that those abilities will resolve. It\'s possible to pay {1}, have Cemetery Puca become a copy of something, activate an activated ability of that creature, then pay {1} again, for example. It will end up a copy of the last creature card paid for.').

card_ruling('cemetery reaper', '2009-10-01', 'Cemetery Reaper grants +1/+1 to all other Zombie creatures you control, not just the ones its ability puts onto the battlefield.').
card_ruling('cemetery reaper', '2009-10-01', 'The creature card in a graveyard is exiled as part of the activated ability\'s effect, not as a cost. If that card has left the graveyard by the time the ability would resolve, the ability is countered and you don\'t get a Zombie token.').

card_ruling('cenn\'s enlistment', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('cenn\'s enlistment', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('cenn\'s enlistment', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('cenn\'s enlistment', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('cenn\'s tactician', '2008-04-01', 'The second ability is cumulative. That is, each creature you control with any +1/+1 counters on it can block an additional creature for each Cenn\'s Tactician you control. Cenn\'s Tactician doesn\'t care whether the creature has any +1/+1 counters on it beyond the first.').

card_ruling('centaur battlemaster', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('centaur battlemaster', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('centaur battlemaster', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('centaur\'s herald', '2012-10-01', 'Once you activate the ability of Centaur\'s Herald, it\'s too late for a player to respond by trying to destroy it or otherwise stop you from sacrificing it.').
card_ruling('centaur\'s herald', '2012-10-01', 'You can only sacrifice Centaur\'s Herald once, so it\'s not possible to activate its ability more than once.').

card_ruling('center soul', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('center soul', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('center soul', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('center soul', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('center soul', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('center soul', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('center soul', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('center soul', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('cerebral eruption', '2011-01-01', 'If the targeted opponent is an illegal target by the time Cerebral Eruption resolves, the spell is countered. No cards will be revealed and no damage will be dealt.').
card_ruling('cerebral eruption', '2011-01-01', 'Once the card is revealed, it\'s too late for players to respond. The targeted opponent can\'t, for example, see how much damage will be dealt, then cast a damage prevention spell or activate a regeneration ability. All such actions must be done before Cerebral Eruption starts to resolve.').
card_ruling('cerebral eruption', '2011-01-01', 'The converted mana cost of the revealed card is determined solely by the mana symbols printed in its upper right corner. The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('cerebral eruption', '2011-01-01', 'If the mana cost of the revealed card includes {X}, X is considered to be 0.').
card_ruling('cerebral eruption', '2011-01-01', 'If the revealed card has no mana symbols in its upper right corner (because it\'s a land card, for example), its converted mana cost is 0.').
card_ruling('cerebral eruption', '2011-01-01', 'If a land card is revealed this way, Cerebral Eruption is returned to its owner\'s hand from the stack. It finishes resolving, but it isn\'t put into the graveyard.').

card_ruling('cerulean wisps', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('cerulean wisps', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('cerulean wisps', '2008-05-01', 'Colorless is not a color.').

card_ruling('ceta sanctuary', '2004-10-04', 'The ability has you draw one or two cards, but never three.').

card_ruling('chain lightning', '2004-10-04', 'The copy is a spell, so it can be targeted by other spells and abilities that target a spell.').
card_ruling('chain lightning', '2004-10-04', 'The player putting the copy spell on the stack controls that spell.').
card_ruling('chain lightning', '2004-10-04', 'The spell copy has all the text and can result in itself being copied.').
card_ruling('chain lightning', '2009-10-01', 'As Chain Lightning resolves, the targeted player, or the controller of the targeted creature, may copy it. The copy has the same text, target, and color of the resolving spell, though the player who\'s copying it may choose a new target for it. Once that copy is created (or not), the resolving Chain Lightning has finished resolving and leaves the stack.').
card_ruling('chain lightning', '2009-10-01', 'If the targeted creature or player is an illegal target by the time Chain Lightning resolves, the spell is countered. It won\'t be copied.').

card_ruling('chain reaction', '2010-03-01', 'The value of X is determined as Chain Reaction resolves.').

card_ruling('chain stasis', '2004-10-04', 'The decision to tap or untap is made on resolution of the spell (and of each copy).').
card_ruling('chain stasis', '2004-10-04', 'Paying to make Chain Stasis continue is done as part of the resolution of the spell.').
card_ruling('chain stasis', '2004-10-04', 'The copy is a spell, so it can be targeted by other spells and abilities that target a spell.').
card_ruling('chain stasis', '2004-10-04', 'The player putting the copy spell on the stack controls that spell.').
card_ruling('chain stasis', '2004-10-04', 'The spell copy has all the text and can result in itself being copied.').

card_ruling('chained throatseeker', '2011-06-01', 'A player is poisoned if that player has one or more poison counters.').

card_ruling('chained to the rocks', '2013-09-15', 'If the land Chained to the Rocks is enchanting stops being a Mountain or another player gains control of it, Chained to the Rocks will be put into its owner’s graveyard when state-based actions are performed.').
card_ruling('chained to the rocks', '2013-09-15', 'Chained to the Rocks’s ability causes a zone change with a duration, a style of ability introduced in Magic 2014 that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Chained to the Rocks have a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Chained to the Rocks leaves the battlefield.').
card_ruling('chained to the rocks', '2013-09-15', 'If Chained to the Rocks leaves the battlefield before its triggered ability resolves, the target creature won’t be exiled.').
card_ruling('chained to the rocks', '2013-09-15', 'Auras attached to the exiled creature will be put into their owners’ graveyards (unless they have bestow). Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('chained to the rocks', '2013-09-15', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('chained to the rocks', '2013-09-15', 'The exiled card returns to the battlefield immediately after Chained to the Rocks leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('chained to the rocks', '2013-09-15', 'In a multiplayer game, if Chained to the Rocks’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('chains of mephistopheles', '2004-10-04', 'The effect is cumulative. If there are two of these on the battlefield, each of them will modify each draw (after the first one if during the draw step), and will cause the player to discard or to \"mill\" a card from their library. As they resolve in order, the player must discard if possible. Once the player fails to discard and instead \"mills\" a card, all further effects of additional Chains of Mephistopheles will not do anything. This is because the \"mill\" also replaces the draw effect and the player is no longer drawing a card. You handle them in order. Each one makes you discard first and then continue or else mill a card and lose the draw.').
card_ruling('chains of mephistopheles', '2004-10-04', 'Cards which are drawn as a cost are affected by this card because replacement effects can alter the payment of costs.').
card_ruling('chains of mephistopheles', '2007-09-16', 'If a spell or ability would cause a player to draw multiple cards, this is treated as a number of individual \"draw one card\" actions. Apply the effect of Chains of Mephistopheles to each one.').
card_ruling('chains of mephistopheles', '2007-09-16', 'A player\'s normal card draw on his or her turn is exempt from this effect. All other draws will be affected.').
card_ruling('chains of mephistopheles', '2007-09-16', 'Here\'s what happens when Chains of Mephistopheles replaces a player\'s draw: -- If that player has at least one card in his or her hand, he or she discards a card and then draws a card. -- If that player\'s hand is empty, he or she puts the top card of his or her library into his or her graveyard. The player doesn\'t draw a card at all.').

card_ruling('chalice of life', '2011-01-22', 'Your starting life total is the life total you began the game with. For most two-player formats, this is 20. For Two-Headed Giant, it\'s the life total your team started with, usually 30. In Commander games, the starting life total is 40.').
card_ruling('chalice of life', '2011-01-22', 'You check your life total to see if Chalice of Life transforms only when its ability resolves.').

card_ruling('chalice of the void', '2004-12-01', 'A mana cost of {X}{X} means that you pay twice X. If you want X to be 3, you pay 6 mana to cast Chalice of the Void.').
card_ruling('chalice of the void', '2004-12-01', 'The number of counters on Chalice of the Void matters only at the time the spell is cast. Changing the number of charge counters on Chalice of the Void after a spell has been cast won\'t change whether the ability counters the spell. If the Chalice had the correct number of counters when the spell was cast, its ability will trigger. If the Chalice had too many or too few counters when the spell was cast, the Chalice\'s ability won\'t trigger.').
card_ruling('chalice of the void', '2004-12-01', 'If there are zero charge counters on Chalice of the Void, it counters each spell with a converted mana cost of 0.').
card_ruling('chalice of the void', '2004-12-01', 'Chalice of the Void has to be on the battlefield at the end of casting a spell for the ability to trigger. If you sacrifice Chalice of the Void as a cost to cast a spell, its ability can\'t trigger.').

card_ruling('chamber of manipulation', '2004-10-04', 'Remember that the creature does not gain Haste, so you can\'t attack with that creature or use any ability with {T} in the activation cost during the turn you take control of it.').
card_ruling('chamber of manipulation', '2004-10-04', 'The creature does not untap if it is tapped.').

card_ruling('champion of lambholt', '2012-05-01', 'Creatures with power less than Champion of Lambholt can\'t block any creature you control, not just Champion of Lambholt.').
card_ruling('champion of lambholt', '2012-05-01', 'Champion of Lambholt\'s ability works even if it isn\'t attacking.').
card_ruling('champion of lambholt', '2012-05-01', 'The comparison of power is only done when blockers are declared. Increasing the power of a blocking creature (or decreasing the power of Champion of Lambholt) after this point won\'t cause any creature to stop blocking or become unblocked.').

card_ruling('champion of stray souls', '2014-02-01', 'You choose the targets of the first ability as you activate that ability, before you pay any costs. You can’t target any of the creatures you sacrifice.').
card_ruling('champion of stray souls', '2014-02-01', 'The last ability can be activated only if Champion of Stray Souls is in your graveyard.').

card_ruling('champion\'s helm', '2011-09-22', 'It\'s possible for a Champion\'s Helm you control to be attached to a creature an opponent controls (if an opponent gains control of your equipped creature, for example). In this case, the creature couldn\'t be the target of spells or abilities you control, as you are an opponent of the controller of the creature with hexproof.').

card_ruling('chance encounter', '2009-02-01', 'You can only win a coin flip if you are the player flipping the coin. Your opponent losing a flip does not count as you winning one. This is a reversal of previous rulings.').

card_ruling('chancellor of the annex', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('chancellor of the annex', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').
card_ruling('chancellor of the annex', '2011-06-01', 'If you reveal more than one Chancellor of the Annex from your opening hand, each will trigger when an opponent casts his or her first spell of the game. That player will have to pay {1} that many times or the spell will be countered.').
card_ruling('chancellor of the annex', '2011-06-01', 'In a multiplayer game, if you reveal Chancellor of the Annex from your opening hand, it will trigger each time an opponent casts his or her first spell of the game.').

card_ruling('chancellor of the dross', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('chancellor of the dross', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').

card_ruling('chancellor of the forge', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('chancellor of the forge', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').
card_ruling('chancellor of the forge', '2011-06-01', 'The number of creature tokens you put onto the battlefield with the last ability is determined when the ability resolves. If Chancellor of the Forge is still on the battlefield at that time, it will be counted.').

card_ruling('chancellor of the spires', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('chancellor of the spires', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').
card_ruling('chancellor of the spires', '2011-06-01', 'If you reveal more than one Chancellor of the Spires from your opening hand, each opponent will put the top seven cards of his or her library into his or her graveyard that many times (for a total of fourteen, twenty-one, or twenty-eight cards).').
card_ruling('chancellor of the spires', '2011-06-01', 'The instant or sorcery card you target with the enters-the-battlefield triggered ability is cast as part of the resolution of that ability. Timing restrictions based on the card\'s type are ignored. Other restrictions, such as \"Cast [this card] only during combat,\" are not.').
card_ruling('chancellor of the spires', '2011-06-01', 'If you are unable to cast the card, perhaps because there are no legal targets available, the card remains in its owner\'s graveyard.').
card_ruling('chancellor of the spires', '2011-06-01', 'You may pay additional costs, such as kicker costs, of the instant or sorcery card.').
card_ruling('chancellor of the spires', '2011-06-01', 'If the card has any mandatory additional costs, then you must pay them in order to cast the spell.').

card_ruling('chancellor of the tangle', '2011-06-01', 'Your \"opening hand\" is the hand of cards you decide to start the game with after taking any mulligans.').
card_ruling('chancellor of the tangle', '2011-06-01', 'After all players have decided not to take any more mulligans, if the starting player has any cards that allow actions to be taken with them from a player\'s opening hand, that player may take the actions of any of those cards. The starting player repeats this, taking all actions he or she wishes to, one at a time. Then each other player in turn order may do the same. After each player has exercised his or her option to take actions from his or her opening hand, the first turn of the game begins.').

card_ruling('chandra ablaze', '2009-10-01', 'If you activate Chandra Ablaze\'s first ability, you don\'t discard a card until the ability resolves. You may activate the ability even if your hand is empty. You choose a target as you activate the ability even if you have no red cards in hand at that time.').
card_ruling('chandra ablaze', '2009-10-01', 'As the first ability resolves, nothing happens if your hand is empty. But if you have any cards in hand, you must discard one. If you discard a nonred card, Chandra doesn\'t deal any damage.').
card_ruling('chandra ablaze', '2009-10-01', 'If the creature targeted by the first ability is an illegal target by the time it resolves, the entire ability is countered. You won\'t discard a card.').
card_ruling('chandra ablaze', '2009-10-01', 'You may activate Chandra\'s second ability even if your hand is empty. As it resolves, a player whose hand is empty simply draws three cards.').
card_ruling('chandra ablaze', '2009-10-01', 'You cast red instant cards and red sorcery cards from your graveyard as part of the resolution of Chandra Ablaze\'s third ability. You don\'t choose which ones to cast until you\'re actually doing so as the ability resolves. You cast only the ones you want to, and you may cast them in any order. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as \"Cast [this card] only during combat\"). Each card you cast this way is put on the stack, then the ability finishes resolving. Those spells will then resolve as normal, one at a time, in the opposite order that they were put on the stack. They\'ll go back to the graveyard as they resolve.').
card_ruling('chandra ablaze', '2009-10-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as kicker costs.').
card_ruling('chandra ablaze', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('chandra ablaze', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('chandra ablaze', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('chandra ablaze', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('chandra ablaze', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('chandra ablaze', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('chandra ablaze', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('chandra ablaze', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('chandra ablaze', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('chandra ablaze', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('chandra ablaze', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('chandra nalaar', '2007-10-01', 'To activate the second ability, you choose a value of X equal to or less than the number of loyalty counters on Chandra Nalaar. You may choose 0. You can\'t choose a negative number.').
card_ruling('chandra nalaar', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('chandra nalaar', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('chandra nalaar', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('chandra nalaar', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('chandra nalaar', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('chandra nalaar', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('chandra nalaar', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('chandra nalaar', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('chandra nalaar', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('chandra nalaar', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('chandra nalaar', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('chandra\'s fury', '2012-07-01', 'Chandra\'s Fury targets only the player, not any creature. Chandra\'s Fury will deal 1 damage to a creature with hexproof, for example.').

card_ruling('chandra\'s ignition', '2015-06-22', 'The creature is the source of the damage, not Chandra’s Ignition. For example, Chandra’s Ignition can have a white creature deal damage to a creature with protection from red.').
card_ruling('chandra\'s ignition', '2015-06-22', 'Use the power of the target creature as Chandra’s Ignition resolves to determine how much damage it deals to each other creature and each opponent.').
card_ruling('chandra\'s ignition', '2015-06-22', 'If the creature becomes an illegal target by the time Chandra’s Ignition tries to resolve (perhaps because another player controls it or it’s left the battlefield), Chandra’s Ignition will be countered and none of its effects will happen. No damage will be dealt.').

card_ruling('chandra\'s outrage', '2010-08-15', 'The player who\'s dealt damage is the player who controls the targeted creature at the time Chandra\'s Outrage resolves.').
card_ruling('chandra\'s outrage', '2010-08-15', 'If the targeted creature is an illegal target by the time Chandra\'s Outrage resolves, the entire spell is countered. No player is dealt damage.').

card_ruling('chandra\'s spitfire', '2010-08-15', 'Combat damage is the damage that\'s dealt automatically by attacking and blocking creatures, even if it\'s redirected (by Harm\'s Way, perhaps). Noncombat damage is any other damage (generally, it\'s damage dealt as the result of a spell or ability).').
card_ruling('chandra\'s spitfire', '2010-08-15', 'The ability triggers just once for each event in which an opponent is dealt noncombat damage, regardless of how much damage that player is dealt. For example, if Lava Axe deals 5 damage to an opponent, Chandra\'s Spitfire gets just +3/+0.').
card_ruling('chandra\'s spitfire', '2010-08-15', 'In a multiplayer game, if a source such as Earthquake deals damage to multiple opponents at the same time, the ability will trigger that many times.').

card_ruling('chandra, fire of kaladesh', '2015-06-22', 'Chandra, Fire of Kaladesh’s activated ability will count any damage Chandra has dealt during the turn to any permanent or player, including combat damage.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'The last sentence of Chandra, Fire of Kaladesh’s activated ability isn’t a separate ability. The check happens only as that activated ability resolves. You must activate the ability in order to exile Chandra and return her to the battlefield transformed, even if Chandra has already dealt 3 or more damage during the turn.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'Each face of a double-faced card has its own set of characteristics: name, types, subtypes, power and toughness, loyalty, abilities, and so on. While a double-faced card is on the battlefield, consider only the characteristics of the face that’s currently up. The other set of characteristics is ignored. While a double-faced card isn’t on the battlefield, consider only the characteristics of its front face.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'The converted mana cost of a double-faced card not on the battlefield is the converted mana cost of its front face.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'The back face of a double-faced card doesn’t have a mana cost. A double-faced permanent with its back face up has a converted mana cost of 0. Each back face has a color indicator that defines its color.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'The back face of a double-faced card (in the case of Magic Origins, the planeswalker face) can’t be cast.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'Although the two rules are similar, the “legend rule” and the “planeswalker uniqueness rule” affect different kinds of permanents. You can control two of this permanent, one front face-up and the other back-face up at the same time. However, if the former is exiled and enters the battlefield transformed, you’ll then control two planeswalkers with the same subtype. You’ll choose one to remain on the battlefield, and the other will be put into its owner’s graveyard.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'A double-faced card enters the battlefield with its front face up by default, unless a spell or ability instructs you to put it onto the battlefield transformed, in which case it enters with its back face up.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'A Magic Origins planeswalker that enters the battlefield because of the ability of its front face will enter with loyalty counters as normal.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'In some rare cases, a spell or ability may cause one of these five cards to transform while it’s a creature (front face up) on the battlefield. If this happens, the resulting planeswalker won’t have any loyalty counters on it and will subsequently be put into its owner’s graveyard.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'You can activate one of the planeswalker’s loyalty abilities the turn it enters the battlefield. However, you may do so only during one of your main phases when the stack is empty. For example, if the planeswalker enters the battlefield during combat, there will be an opportunity for your opponent to remove it before you can activate one of its abilities.').
card_ruling('chandra, fire of kaladesh', '2015-06-22', 'If a double-faced card is manifested, it will be put onto the battlefield face down (this is also true if it’s put onto the battlefield face down some other way). Note that “face down” is not synonymous with “with its back face up.” A manifested double-faced card is a 2/2 creature with no name, mana cost, creature types, or abilities. While face down, it can’t transform. If the front face of a manifested double-faced card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced card on the battlefield can’t be turned face down.').

card_ruling('chandra, pyromaster', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('chandra, pyromaster', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('chandra, pyromaster', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('chandra, pyromaster', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('chandra, pyromaster', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('chandra, pyromaster', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('chandra, pyromaster', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('chandra, pyromaster', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('chandra, pyromaster', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('chandra, pyromaster', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('chandra, pyromaster', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').
card_ruling('chandra, pyromaster', '2014-07-18', 'To activate the first ability, you must target a player but you can choose to not target any creatures.').
card_ruling('chandra, pyromaster', '2014-07-18', 'If the first ability resolves but the damage is prevented or redirected, the target creature still won’t be able to block that turn.').
card_ruling('chandra, pyromaster', '2014-07-18', 'The card exiled by the second ability is exiled face up. Playing it follows the normal rules for playing that card. You must pay its costs, and you must follow all applicable timing rules. For example, if it’s a creature card, you can cast it only during your main phase while the stack is empty.').
card_ruling('chandra, pyromaster', '2014-07-18', 'If you exile a land card using the second ability, you may play that land only if you have any available land plays. Normally, this means you can play the land only if you haven’t played a land yet that turn.').
card_ruling('chandra, pyromaster', '2014-07-18', 'In the third ability, “exiled this way” means the cards you just exiled because of that ability resolving. Cards exiled with the second ability or with previous activations of the third ability don’t count.').
card_ruling('chandra, pyromaster', '2014-07-18', 'While resolving the third ability, you create the copies of the card in exile and cast them from exile. You can cast zero, one, two, or all three copies. The card itself isn’t cast. It remains exiled.').
card_ruling('chandra, pyromaster', '2014-07-18', 'While casting the copies created by the third ability, timing restrictions based on the card’s type (such as creature or sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” are not.').
card_ruling('chandra, pyromaster', '2014-07-18', 'Because you’re casting the copies “without paying their mana costs,” you can’t pay any alternative costs for the copies. You can pay additional costs, such as kicker costs. If the copies have any mandatory additional costs, you must pay those.').
card_ruling('chandra, pyromaster', '2014-07-18', 'If the copied card has {X} in its mana cost, you must choose 0 as its value when casting the copies.').

card_ruling('chandra, roaring flame', '2015-06-22', 'The emblem created by Chandra, Roaring Flame is colorless. The damage it deals is from a colorless source.').
card_ruling('chandra, roaring flame', '2015-06-22', 'Only players actually dealt damage by the third ability of Chandra, Roaring Flame will get an emblem. If all of that damage to a player is prevented, that player won’t get an emblem. If any of that damage is redirected to Chandra’s controller, that player will get an emblem.').
card_ruling('chandra, roaring flame', '2015-06-22', 'Each player who gets Chandra’s emblem is the owner of that emblem. In multiplayer games, that emblem will remain in the game as long as its owner does, even if Chandra’s owner leaves the game.').

card_ruling('chandra, the firebrand', '2011-09-22', 'Chandra\'s second ability creates a delayed triggered ability that will copy the next instant or sorcery spell you cast that turn, regardless of whether that spell has targets.').
card_ruling('chandra, the firebrand', '2011-09-22', 'When the delayed trigger resolves, it creates a copy of a spell. You control the copy. That copy is created on the stack, so it\'s not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('chandra, the firebrand', '2011-09-22', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('chandra, the firebrand', '2011-09-22', 'If the spell Chandra copies is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('chandra, the firebrand', '2011-09-22', 'If the spell Chandra copies has an X whose value was determined as it was cast (like Consume Spirit does), the copy has the same value of X.').
card_ruling('chandra, the firebrand', '2011-09-22', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if you sacrifice a 3/3 creature to cast Fling after activating Chandra\'s second ability, the copy of Fling will also deal 3 damage.').
card_ruling('chandra, the firebrand', '2011-09-22', 'When you activate Chandra\'s last ability, you choose up to six target creatures and/or players. No damage is divided, and Chandra can\'t deal more than 6 damage to any one target. Chandra deals 6 damage to each target that is still legal when the ability resolves.').
card_ruling('chandra, the firebrand', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('chandra, the firebrand', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('chandra, the firebrand', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('chandra, the firebrand', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('chandra, the firebrand', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('chandra, the firebrand', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('chandra, the firebrand', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('chandra, the firebrand', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('chandra, the firebrand', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('chandra, the firebrand', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('chandra, the firebrand', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('change of heart', '2004-10-04', 'Change of Heart will not remove an already attacking creature from combat. It must be cast before attackers are declared or it won\'t have any noticeable effect on the game.').

card_ruling('channel', '2004-10-04', 'You can\'t pay life you don\'t have. In other words, you can\'t Channel yourself below zero life.').

card_ruling('channel harm', '2014-11-24', 'Channel Harm’s only target is the creature it may deal damage to. You choose that target as you cast Channel Harm, not at the time it prevents damage.').
card_ruling('channel harm', '2014-11-24', 'If the target creature is an illegal target as Channel Harm tries to resolve, the entire spell is countered. No damage will be prevented.').
card_ruling('channel harm', '2014-11-24', 'Whether the target creature is still a legal target is not checked after Channel Harm resolves. Damage will still be prevented, even if Channel Harm can’t deal damage to that creature.').
card_ruling('channel harm', '2014-11-24', 'Channel Harm’s effect is not a redirection effect. If it prevents damage, you may have Channel Harm (not the original source) deal damage to the creature as part of that prevention effect. Channel Harm is the source of the new damage, so the characteristics of the original source (such as its color, or whether it had lifelink or deathtouch) don’t affect the new damage. The new damage is not combat damage, even if the prevented damage was.').
card_ruling('channel harm', '2014-11-24', 'You can choose a creature you control as the target. If you do, the damage Channel Harm deals to that creature won’t be prevented.').
card_ruling('channel harm', '2014-11-24', 'If two players have each cast Channel Harm targeting a creature the other controls, and a source one player controls would deal damage to the second player or a permanent that player controls, one Channel Harm will prevent that damage and try to deal damage to the creature controlled by the first player. That player’s Channel Harm will then prevent that damage and try to deal damage to the creature controlled by the second player. This forms a loop that either player can break by choosing to not have Channel Harm try to deal damage. It doesn’t matter which player breaks the loop, as no damage will be dealt in either case.').

card_ruling('chant of the skifsang', '2011-01-22', 'A creature with power 0 or less assigns no combat damage. (It doesn\'t assign a negative amount of combat damage.)').

card_ruling('chant of vitu-ghazi', '2005-10-01', 'The damage prevention shield lasts for the rest of the turn, and you gain life each time that shield prevents 1 or more damage.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('chant of vitu-ghazi', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('chaos imps', '2012-10-01', 'Chaos Imps will have trample if it has any +1/+1 counter on it, not just the one put on it by the unleash ability.').
card_ruling('chaos imps', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('chaos imps', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('chaos imps', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('chaos imps', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('chaos lord', '2004-10-04', 'This creature\'s changing of controllers is a new effect each upkeep so it will take precedence over any other control effects. This means that using another control-changing effect won\'t guarantee that you keep it.').

card_ruling('chaos moon', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('chaos orb', '2004-10-04', 'Chaos Orb can only affect permanents. Cards that are in the game but not on the battlefield, such as those in the Library and Graveyard, can\'t be affected.').
card_ruling('chaos orb', '2004-10-04', 'You can arrange your cards any time before the Orb is put onto the battlefield, but not after. In general, you should not stack cards or put them in places where your opponent can\'t read the names of all of them or count them. This is recommended good gaming practice.').
card_ruling('chaos orb', '2004-10-04', 'It must flip 360 degrees (that\'s what \"flip\" means). And this flip must be in the air and not in your hand.').
card_ruling('chaos orb', '2004-10-04', 'This is a not a targeted ability.').
card_ruling('chaos orb', '2004-10-04', 'If you have sleeves on cards, they count as the cards.').
card_ruling('chaos orb', '2004-10-04', 'You can\'t interfere in any physical way with the casting of this card.').

card_ruling('chaos warp', '2011-09-22', 'The owner of a token is the player under whose control the token was put onto the battlefield. If a token is shuffled into a player\'s library this way, that player shuffles before revealing the top card of that library.').
card_ruling('chaos warp', '2011-09-22', 'If the permanent is an illegal target by the time Chaos Warp tries to resolve, it will be countered and none of its effects will occur. No library will be shuffled and no card will be revealed.').
card_ruling('chaos warp', '2011-09-22', 'A permanent card is a card with one or more of the following card types: artifact, creature, enchantment, land, or planeswalker.').
card_ruling('chaos warp', '2011-09-22', 'If the revealed card is a permanent card but can\'t enter the battlefield (perhaps because it\'s an Aura with nothing to enchant), it remains on top of that library.').
card_ruling('chaos warp', '2011-09-22', 'If the revealed card is not a permanent card, it remains on top of that library.').

card_ruling('chaosphere', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('chaotic backlash', '2008-08-01', 'Each permanent is counted only once. For example, if the targeted player controls a white creature, a blue enchantment, and a white-blue creature, Chaotic Backlash will deal 6 damage to that player.').

card_ruling('chaotic strike', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s after the beginning of that phase\'s Declare Blockers Step.').

card_ruling('chaotic æther', '2012-06-01', 'While Chaotic AEther\'s effect applies, rolling any blank face of the planar die will cause chaos abilities to trigger.').
card_ruling('chaotic æther', '2012-06-01', 'Planeswalking away from a phenomenon (as you do when resolving Chaotic AEther\'s ability) doesn\'t cause Chaotic AEther\'s effect to expire.').

card_ruling('char-rumbler', '2007-05-01', 'Yes, Char-Rumbler\'s printed power is -1. While its power is -1 or 0, it simply deals no combat damage.').
card_ruling('char-rumbler', '2007-05-01', 'A first strike damage step will be created if Char-Rumbler is in combat, even if its power is 0 or less. The normal combat damage step will follow.').
card_ruling('char-rumbler', '2007-05-01', 'If Char-Rumbler\'s power changes between the assignment of first-strike combat damage and the assignment of normal combat damage, Char-Rumbler will deal a different amount of damage in each combat damage step.').

card_ruling('chariot of the sun', '2004-10-04', 'The entire effect lasts until end of turn, not just the toughness reduction.').
card_ruling('chariot of the sun', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('charmbreaker devils', '2011-09-22', 'The instant or sorcery card returned to your hand is chosen at random as the triggered ability resolves. If any player responds to the ability, that player won\'t yet know what card will be returned.').
card_ruling('charmbreaker devils', '2011-09-22', 'Because the first ability doesn\'t target the instant or sorcery card, any instants or sorceries put into your graveyard in response to that ability may be returned to your hand.').

card_ruling('charmed pendant', '2004-10-04', 'The ability is still a mana ability (and doesn\'t use the stack) even though you can only activate it when you could cast an instant.').
card_ruling('charmed pendant', '2005-11-01', 'If you flip over a hybrid card, you get one of either color mana, not both, for each mana symbol.').

card_ruling('charnelhoard wurm', '2009-02-01', 'If Charnelhoard Wurm is dealt lethal damage at the same time it deals damage to an opponent, Charnelhoard Wurm will be in your graveyard by the time you put its ability on the stack. In that case, you can target it with its own ability.').

card_ruling('chartooth cougar', '2009-02-01', 'Unlike the normal cycling ability, Mountaincycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Mountain card. After you find a Mountain card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('chartooth cougar', '2009-02-01', 'Mountaincycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Mountaincycling this card. Any ability that stops a cycling ability from being activated also stops Mountaincycling from being activated.').
card_ruling('chartooth cougar', '2009-02-01', 'Mountaincycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Mountaincycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('chartooth cougar', '2009-02-01', 'You can choose to find any card with the Mountain land type, including nonbasic lands. You can also choose not to find a card, even if there is a Mountain card in your library.').

card_ruling('chasm guide', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('chasm guide', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('chasm skulker', '2014-07-18', 'If you draw multiple cards, the first ability will trigger that many times. Each of these abilities will cause a +1/+1 counter to be put on Chasm Skulker.').
card_ruling('chasm skulker', '2014-07-18', 'If enough -1/-1 counters are put on Chasm Skulker at the same time to make its toughness 0 or less, the number of +1/+1 counters on it before it got any -1/-1 counters will be used to determine how many Squid tokens you get. For example, if there are two +1/+1 counters on Chasm Skulker and it gets three -1/-1 counters, you’ll get two Squid tokens.').

card_ruling('chemister\'s trick', '2012-10-01', 'If you cast Chemister\'s Trick with overload, only creatures you don\'t control that are on the battlefield when Chemister\'s Trick resolves are affected. Creatures that come under another player\'s control later in the turn are not.').
card_ruling('chemister\'s trick', '2012-10-01', 'The controller of a creature that attacks if able still chooses which player or planeswalker it attacks.').
card_ruling('chemister\'s trick', '2012-10-01', 'If, during a player\'s declare attacker\'s step, a creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('chemister\'s trick', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('chemister\'s trick', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('chemister\'s trick', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('chemister\'s trick', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('chemister\'s trick', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('chemister\'s trick', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('chemister\'s trick', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('chief engineer', '2014-07-18', 'Multiple instances of convoke on a single spell are redundant.').
card_ruling('chief engineer', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('chief engineer', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('chief engineer', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('chief engineer', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('chief engineer', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('chief engineer', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('chief engineer', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('chieftain en-dal', '2004-10-04', 'Chieftain en-Dal does give itself first strike when attacking.').

card_ruling('child of gaea', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice Child of Gaea. You can choose to not pay even if you no longer control Child of Gaea on resolution.').
card_ruling('child of gaea', '2009-10-01', 'If you do choose not to pay, but no longer control Child of Gaea, you cannot sacrifice. It will remain on the battlefield under its current controller\'s control.').

card_ruling('children of korlis', '2006-09-25', 'If your life total drops to 0 or less, it\'s too late to use this ability before losing the game.').
card_ruling('children of korlis', '2006-09-25', 'The life you gain is based on the total of all changes where your life total went down during the turn, not the net downward change. So if you lose 5 life, gain 3 life, and then lose 2 more life before activating this ability, the ability causes you gain 7 life, not 4.').

card_ruling('chimeric coils', '2004-12-01', 'If X is 0, Chimeric Coils becomes a 0/0 creature and is put into its owner\'s graveyard.').
card_ruling('chimeric coils', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('chimeric egg', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('chimeric idol', '2004-10-04', 'Your lands are tapped as part of the effect, not part of the cost.').
card_ruling('chimeric idol', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('chimeric mass', '2011-01-01', 'If the number of charge counters on Chimeric Mass changes while it is a creature, its power and toughness will change accordingly.').
card_ruling('chimeric mass', '2011-01-01', 'If you activate Chimeric Mass\'s last ability while it has no charge counters on it, it will become a 0/0 creature and be put into its owner\'s graveyard as a state-based action.').
card_ruling('chimeric mass', '2011-01-01', 'Activating the last ability while Chimeric Mass is a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').
card_ruling('chimeric mass', '2011-01-01', 'For example, say you activate the last ability of a Chimeric Mass with three charge counters on it. After it resolves, you cast Giant Growth targeting it. It\'s now 6/6. Then Diminish (\"Target creature becomes 1/1 until end of turn\") is cast targeting it. Once Diminish resolves, Chimeric Mass would be 4/4. Activating Chimeric Mass\'s last ability a second time would make it 6/6 again until end of turn.').

card_ruling('chimeric sphere', '2008-04-01', 'If the first ability is activated and resolves, then the second ability is activated and resolves, Chimeric Sphere will be a 3/2 creature. It won\'t have flying.').
card_ruling('chimeric sphere', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('chimeric staff', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('chimeric staff', '2009-10-01', 'If Chimeric Staff is already an artifact creature when you activate its ability, Chimeric Staff stays an artifact creature but the ability resets its creature types. You always apply the effect from this card before counters or other effects that modify power and toughness. For example, if you pay {3} to make it a 3/3, then someone gives it -1/-1, it will be a 2/2. If you then activate the ability with {4}, you will apply the \"4/4\" effect before the -1/-1 effect, so the end result will be a 3/3. Damage that was dealt to the creature stays on it.').

card_ruling('choice of damnations', '2005-06-01', 'After the opponent chooses a number, Choice of Damnations\' controller may choose to make that opponent either lose that much life or make that opponent sacrifice permanents.').
card_ruling('choice of damnations', '2005-06-01', 'If the opponent must sacrifice all but a number of permanents, that opponent chooses that many permanents and then sacrifices the rest. If the number chosen is greater than the number of permanents the opponent controls, the player sacrifices nothing.').

card_ruling('choking tethers', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('choking tethers', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('choking tethers', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('choking tethers', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('choking vines', '2004-10-04', 'Choking Vines will make any creatures it targets become blocked, even if the creature had blocking restrictions on it. For example, you can make a creature with Islandwalk become blocked even if the defending player has Islands. This is because evasion abilities only prevent creatures from being assigned to block them.').
card_ruling('choking vines', '2004-10-04', 'You can declare zero targets, with X=0.').
card_ruling('choking vines', '2008-04-01', 'Choking Vines isn\'t particularly effective against creatures with trample. Since the creature with trample has no blocker to assign damage to, all the damage will be assigned to the defending player.').
card_ruling('choking vines', '2008-04-01', 'You may target a blocked creature with Choking Vines. Choking Vines will deal 1 damage to it, and have no other effect on it.').
card_ruling('choking vines', '2008-04-01', 'If a previously unblocked creature becomes blocked as a result of Choking Vines, any “When this creature becomes blocked” abilities that creature has will trigger. Any “When this creature becomes blocked by a creature” abilities that creature has won\'t trigger.').

card_ruling('choose your champion', '2010-06-15', 'The effect doesn\'t wear off until just before your next untap step (even if an effect will cause that untap step to be skipped).').
card_ruling('choose your champion', '2010-06-15', 'The player is chosen as the ability resolves. Once a player is chosen, it\'s too late for other players to respond by casting spells.').
card_ruling('choose your champion', '2010-06-15', 'The targeted opponent may choose himself or herself.').
card_ruling('choose your champion', '2010-06-15', 'The targeted opponent may choose you. In that case, only you can cast spells and attack with creatures before your next turn begins.').
card_ruling('choose your champion', '2010-06-15', 'Players other than you and the chosen player may still perform game actions besides casting spells and attacking with creatures: They may block with creatures, activate abilities, perform special actions, and so on.').

card_ruling('chord of calling', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('chord of calling', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('chord of calling', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('chord of calling', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('chord of calling', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('chord of calling', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('chord of calling', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('chorus of might', '2012-10-01', 'The number of creatures you control is counted only when Chorus of Might resolves.').

card_ruling('chorus of the conclave', '2005-10-01', 'Chorus of the Conclave\'s ability works only while it\'s on the battlefield, so you can\'t use it to put +1/+1 counters on itself.').
card_ruling('chorus of the conclave', '2005-10-01', 'Chorus of the Conclave\'s ability applies to creature spells only as they\'re being cast. You can\'t pay mana to put counters on creatures being put onto the battlefield by an effect.').
card_ruling('chorus of the conclave', '2005-10-01', 'Chorus of the Conclave\'s ability combines well with the convoke mechanic, effectively letting you tap creatures to put +1/+1 counters on the creature with convoke that you\'re casting, if you choose to do so.').

card_ruling('chorus of the tides', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('chorus of the tides', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('chorus of the tides', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('chorus of the tides', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('chorus of the tides', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('chorus of the tides', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('chorus of the tides', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('chosen by heliod', '2013-09-15', 'If the target of an Aura is illegal when it tries to resolve, the Aura will be countered. The Aura doesn’t enter the battlefield, so you won’t get to draw a card.').

card_ruling('chosen of markov', '2011-01-22', 'You can tap any untapped Vampire you control, including one you haven\'t controlled continuously since the beginning of your most recent turn, to pay the cost of Chosen of Markov\'s activated ability. You must have controlled Chosen of Markov continuously since the beginning of your most recent turn, however.').

card_ruling('chromanticore', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('chromanticore', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('chromanticore', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('chromanticore', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('chromanticore', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('chromanticore', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('chromanticore', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('chromatic lantern', '2012-10-01', 'Lands you control won\'t lose any other abilities they had. They also won\'t gain or lose any land types.').

card_ruling('chromatic sphere', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('chrome mox', '2004-12-01', 'If no card is imprinted on Chrome Mox, the Mox can\'t add mana to your mana pool. It can\'t add colorless mana to your mana pool.').
card_ruling('chrome mox', '2004-12-01', 'If you imprinted a multicolored card, you choose one of that card\'s colors each time the Mox\'s ability resolves.').

card_ruling('chromeshell crab', '2004-10-04', 'You choose the two target creatures when you activate the ability. You do not have to choose Chromeshell Crab. You choose whether or not to do the exchange on resolution.').
card_ruling('chromeshell crab', '2004-10-04', 'The exchange fails if either target is not still on the battlefield at that time.').
card_ruling('chromeshell crab', '2004-10-04', 'The exchange fails if either you no longer control your creature or the other creature is now controlled by you.').
card_ruling('chromeshell crab', '2004-10-04', 'The creatures can\'t attack until you start a turn with them under your control. So you can\'t swap and then attack.').
card_ruling('chromeshell crab', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('chronatog', '2004-10-04', 'You can only activate the ability once each turn for each Chronatog.').

card_ruling('chronatog totem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('chronic flooding', '2012-10-01', 'Chronic Flooding\'s ability will trigger whenever the enchanted land becomes tapped for any reason, not just because its controller taps it for mana.').
card_ruling('chronic flooding', '2012-10-01', 'Chronic Flooding becoming attached to a tapped land won\'t cause its ability to immediately trigger. That land must go from untapped to tapped.').

card_ruling('chronicler of heroes', '2013-09-15', 'Whether you control a creature with a +1/+1 counter on it is checked only when the ability resolves. Notably, if Chronicler of Heroes entering the battlefield causes an evolve ability to trigger, you can have the evolve ability resolve first and then draw a card.').

card_ruling('chronomantic escape', '2013-04-15', 'Chronomantic Escape can affect creatures that aren\'t on the battlefield at the time it resolves, because it modifies the announcement of an attack, not the creatures on the battlefield. For example, if Chronomantic Escape resolves on your turn, then on your opponent\'s turn he or she casts a creature with haste, that creature can\'t attack that turn.').
card_ruling('chronomantic escape', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('chronomantic escape', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('chronomantic escape', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('chronomantic escape', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('chronomantic escape', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('chronomantic escape', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('chronomantic escape', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('chronomantic escape', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('chronomantic escape', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('chronomantic escape', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('chronomantic escape', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('chronosavant', '2006-09-25', 'If Chronosavant is in your graveyard, you can activate this ability multiple times in response to itself. The first one that resolves will return Chronosavant to the battlefield and make you skip your next turn. The others will just make you skip turns.').

card_ruling('chronozoa', '2007-02-01', 'The ability checks to see if Chronozoa had no time counters on it at the time it was put into a graveyard from the battlefield. This doesn\'t necessarily mean it must have been sacrificed due to vanishing; it could have been put into the graveyard some other way (say, while the sacrifice ability of vanishing is on the stack).').
card_ruling('chronozoa', '2007-02-01', 'Chronozoa\'s last ability creates two Chronozoa tokens that each enter the battlefield with three time counters and have all of Chronozoa\'s abilities.').
card_ruling('chronozoa', '2007-02-01', 'If Chronozoa becomes a copy of another creature and is put into the graveyard from the battlefield, no copies will be created. If another creature becomes a copy of Chronozoa and is put into a graveyard from the battlefield (with no time counters on it), two copies of Chronozoa will be created.').

card_ruling('cinder glade', '2015-08-25', 'Even though these lands have basic land types, they are not basic lands because “basic” doesn’t appear on their type line. Notably, controlling two or more of them won’t allow others to enter the battlefield untapped.').
card_ruling('cinder glade', '2015-08-25', 'However, because these cards have basic land types, effects that specify a basic land type without also specifying that the land be basic can affect them. For example, a spell or ability that reads “Destroy target Forest” can target Canopy Vista, while one that reads “Destroy target basic Forest” cannot.').
card_ruling('cinder glade', '2015-08-25', 'If one of these lands enters the battlefield at the same time as any number of basic lands, those other lands are not counted when determining if this land enters the battlefield tapped or untapped.').

card_ruling('cinder seer', '2004-10-04', 'You can reveal zero cards and deal zero damage.').

card_ruling('cinderhaze wretch', '2008-05-01', 'You put the -1/-1 counter on Cinderhaze Wretch as a cost. That means it happens when you activate the ability, not when it resolves. If paying the cost causes the creature to have 0 toughness, it\'s put into the graveyard before you can untap it and before you can even pay the cost again.').

card_ruling('circle of affliction', '2007-02-01', 'The player you target doesn\'t have to be the controller of the source that dealt damage to you.').
card_ruling('circle of affliction', '2007-02-01', 'If the damage reduces you to 0 or less life, you lose the game before Circle of Affliction\'s ability has a chance to give you life.').
card_ruling('circle of affliction', '2007-02-01', 'Each time a source deals damage to you, you may pay {1}. You can\'t pay more than {1}, even if more than 1 damage was dealt.').
card_ruling('circle of affliction', '2007-02-01', 'If several sources deal damage to you simultaneously (for example, several attacking creatures), this ability triggers once for each source.').

card_ruling('circle of elders', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('circle of elders', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('circle of elders', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('circle of flame', '2014-07-18', 'Circle of Flame’s ability triggers only if a creature without flying is declared as an attacker during the declare attackers step. If a creature with flying attacks you or a planeswalker you control and then loses flying, or if a creature without flying is put onto the battlefield attacking you or a planeswalker you control, the ability won’t trigger.').

card_ruling('circle of protection: black', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('circle of protection: black', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('circle of protection: blue', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('circle of protection: blue', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('circle of protection: green', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('circle of protection: green', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('circle of protection: red', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('circle of protection: red', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('circle of protection: shadow', '2004-10-04', 'It can be used on non-combat damage which happens to come from creatures with the Shadow ability.').

card_ruling('circle of protection: white', '2004-10-04', 'A source of damage is a permanent, a spell on the stack (including one that creates a permanent), or any object referred to by an object on the stack. A source doesn\'t need to be capable of dealing damage to be a legal choice.').
card_ruling('circle of protection: white', '2004-10-04', 'Can be used even when there is no damage to prevent. It prevents the next damage (if any) from the source this turn.').

card_ruling('circling vultures', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('circling vultures', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('circling vultures', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('circling vultures', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('circling vultures', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').
card_ruling('circling vultures', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('circling vultures', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').
card_ruling('circling vultures', '2008-04-01', 'Circling Vultures\'s discard ability is a static ability, not an activated ability. Although it acts like an activated ability that says “Discard Circling Vultures: Nothing happens,” it is not one and cards like Pithing Needle or Abeyance won\'t prevent you from being able to discard it.').

card_ruling('circu, dimir lobotomist', '2005-10-01', 'The first two abilities target libraries, not players.').
card_ruling('circu, dimir lobotomist', '2005-10-01', 'If you cast a blue and black spell, both of Circu\'s triggered abilities trigger. You can target two different libraries or target the same library twice.').
card_ruling('circu, dimir lobotomist', '2005-10-01', 'Circu\'s last ability applies to all of its controller\'s opponents, not just the owner of the exiled card.').
card_ruling('circu, dimir lobotomist', '2005-10-01', 'Circu\'s last ability doesn\'t prevent copies of the exiled cards from being cast because copies aren\'t cards.').
card_ruling('circu, dimir lobotomist', '2005-10-01', 'If a split card is exiled this way, opponents can\'t cast either half of the split card.').

card_ruling('citadel castellan', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('citadel castellan', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('citadel castellan', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('citadel of pain', '2004-10-04', 'It counts the untapped lands on resolution.').

card_ruling('citadel siege', '2014-11-24', 'Each Siege will have one of the two listed abilities, depending on your choice as it enters the battlefield.').
card_ruling('citadel siege', '2014-11-24', 'The words “Khans” and “Dragons” are anchor words, connecting your choice to the appropriate ability. Anchor words are a new rules concept. “[Anchor word] — [Ability]” means “As long as you chose [anchor word] as this permanent entered the battlefield, this permanent has [ability].” Notably, the anchor word “Dragons” has no connection to the creature type Dragon.').
card_ruling('citadel siege', '2014-11-24', 'Each of the last two abilities is linked to the first ability. They each refer only to the choice made as a result of the first ability. If a permanent enters the battlefield as a copy of one of the Sieges, its controller will make a new choice for that Siege. Which ability the copy has won’t depend on the choice made for the original permanent.').

card_ruling('citanul druid', '2004-10-04', 'In multiplayer games it affects all players.').

card_ruling('citanul flute', '2004-10-04', 'X can be zero if you want.').
card_ruling('citanul flute', '2004-10-04', 'You look in your library but you do not have to choose a creature even if you have one that meets the criteria.').

card_ruling('city in a bottle', '2004-10-04', 'Token creatures and counters created by Arabian Nights cards are not removed.').
card_ruling('city in a bottle', '2014-02-01', 'Any time a player receives priority to cast spells or activate abilities, check to see whether any permanents on the battlefield were originally printed in the Arabian Nights expansion (even if the physical card representing that permanent is a reprint with a different expansion symbol). If there are any such permanents, the ability will trigger and those permanents will be sacrificed.').

card_ruling('city of brass', '2004-10-04', 'The first ability triggers no matter how the land becomes tapped.').
card_ruling('city of brass', '2004-10-04', 'If you tap City of Brass while you are casting a spell or activating an ability, its ability will trigger and wait. When you finish casting that spell or activating that ability, City of Brass\'s triggered ability is put on the stack on top of it. City of Brass\'s ability will resolve first.').
card_ruling('city of brass', '2004-10-04', 'On the other hand, you can tap City of Brass, put its triggered ability on the stack, and then respond to that ability by casting an instant or activating an ability using that mana. In that case, the instant spell or activated ability will resolve first.').

card_ruling('city of shadows', '2004-10-04', 'The storage counters aren\'t removed when you activate the mana ability.').
card_ruling('city of shadows', '2009-10-01', 'You can activate City of Shadows\'s second ability while it has no storage counters on it, though it won\'t add any mana to your mana pool in this case. You\'ve still tapped it for mana, though, in case any abilities (such as the ability of a Fertile Ground enchanting it) care about that.').

card_ruling('city of solitude', '2004-10-04', 'City of Solitude can affect abilities of cards that are not on the battlefield because it prevents players from activating those abilities.').
card_ruling('city of solitude', '2004-10-04', 'City of Solitude does not stop triggered abilities from being put on the stack. They are never \"activated\".').
card_ruling('city of solitude', '2009-10-01', 'This stops players from activating mana abilities.').

card_ruling('city of traitors', '2004-10-04', 'City of Traitors does not trigger on lands that are put onto the battlefield without playing them.').
card_ruling('city of traitors', '2004-10-04', 'City of Traitors does not trigger on itself being played.').

card_ruling('civic saber', '2012-10-01', 'Civic Saber\'s bonus can range from +0/+0 (for a colorless creature) to +5/+0 (for a creature that\'s all five colors).').
card_ruling('civic saber', '2012-10-01', 'If the equipped creature becomes a different number of colors, the bonus will change accordingly.').

card_ruling('civilized scholar', '2011-09-22', 'You don\'t have priority between untapping Civilized Scholar and transforming it. You can\'t activate the draw-and-discard ability again, for example.').
card_ruling('civilized scholar', '2011-09-22', 'If Civilized Scholar attacks, and later in the turn (but before the beginning of your end step), it transforms, Homicidal Brute\'s last ability won\'t trigger. This is because the creature attacked that turn, even if had its other face up at the time.').

card_ruling('clan defiance', '2013-01-24', 'You can choose just one mode, any two of the modes, or all three. You make this choice as you cast Clan Defiance.').
card_ruling('clan defiance', '2013-01-24', 'The value of X is the same for each mode you choose.').

card_ruling('clarion ultimatum', '2008-10-01', 'This spell has no targets. You don\'t choose five permanents you control until Clarion Ultimatum resolves. If you control fewer than five permanents at that time, choose each permanent you do control.').
card_ruling('clarion ultimatum', '2008-10-01', 'The five permanents you choose must all be different. However, some of them may have the same name as one another. For example, you may choose two Empyrial Archangels and three Plains. If you do, you search your library for up to two more Empyrial Archangels and up to three more Plains and put them onto the battlefield tapped.').
card_ruling('clarion ultimatum', '2008-10-01', 'If you choose a permanent whose name has been changed by an effect (for example, a Clone that\'s copying another creature), you\'ll search for a card with the new name, not one with the original name. Note that changing a land\'s subtype doesn\'t change its name.').
card_ruling('clarion ultimatum', '2008-10-01', 'If you choose a permanent with no name, such as a face-down creature, no card in your library can be found with that name.').
card_ruling('clarion ultimatum', '2008-10-01', 'You make just one search. (This could matter for cards like Aven Mindcensor, for example.)').
card_ruling('clarion ultimatum', '2008-10-01', 'The cards all enter the battlefield at the same time. If one of the cards that\'s entering the battlefield is an Aura, it must enter the battlefield attached to a permanent already on the battlefield. It can\'t enter the battlefield attached to another permanent entering the battlefield via Clarion Ultimatum. If an Aura can\'t enter the battlefield this way, it remains in your library.').
card_ruling('clarion ultimatum', '2008-10-01', 'If you find a card that isn\'t a permanent card while searching (for example, you chose an Illusion token and find the split card Illusion/Reality), that card remains in your library.').

card_ruling('clash of realities', '2005-02-01', 'Clash of Realities\'s abilities apply as each creature enters the battlefield and gives them a enters-the-battlefield triggered ability.').
card_ruling('clash of realities', '2005-02-01', 'Creatures with no creature type are non-Spirit creatures.').

card_ruling('claustrophobia', '2015-06-22', 'Claustrophobia can target and enchant a tapped or untapped creature.').
card_ruling('claustrophobia', '2015-06-22', 'The enchanted creature can still be untapped in other ways. Claustrophobia will remain attached, and the creature will continue to not untap during its controller’s untap step.').

card_ruling('claws of wirewood', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('cleansing beam', '2005-10-01', 'All creatures that share a color are affected, even your own.').
card_ruling('cleansing beam', '2005-10-01', 'A creature \"shares a color\" with any creature that is at least one of its colors. For example, a green-white creature shares a color with creatures that are green, white, green-white, red-white, black-green, and so on.').
card_ruling('cleansing beam', '2005-10-01', 'If it targets a colorless creature, it doesn\'t affect any other creatures. A colorless creature shares a color with nothing, not even other colorless creatures.').
card_ruling('cleansing beam', '2005-10-01', 'You check which creatures share a color with the target when the spell resolves.').
card_ruling('cleansing beam', '2005-10-01', 'Only one creature is targeted. If that creature leaves the battlefield or otherwise becomes an illegal target, the entire spell is countered. No other creatures are affected.').

card_ruling('cleansing meditation', '2004-10-04', 'Auras returning to the battlefield are placed on any legal permanent of your choice. Doing this does not target the permanent. If there is no legal permanent for an Aura, it remains in the graveyard.').
card_ruling('cleansing meditation', '2005-08-01', 'Auras returning to the battlefield can only enchant something that was already on the battlefield. An Enchant Enchantment returning to the battlefield can\'t be placed on an enchantment that is also in the process of returning to the battlefield.').

card_ruling('clear', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('cleaver riot', '2012-07-01', 'Cleaver Riot affects only creatures you control at the time it resolves. It won\'t affect creatures that come under your control later in the turn.').

card_ruling('cleric of the forward order', '2015-06-22', 'Count the number of creatures named Cleric of the Forward Order you control as the ability resolves to determine how much life to gain. If the Cleric of the Forward Order with the ability that triggered is still on the battlefield, it will count itself.').

card_ruling('clever impersonator', '2014-09-20', 'You choose which nonland permanent Clever Impersonator will copy, if any, as it enters the battlefield. This doesn’t target that nonland permanent.').
card_ruling('clever impersonator', '2014-09-20', 'Remember that if you control more than one legendary permanent with the same name, or more than one planeswalker with the same type, you’ll choose one to remain on the battlefield and put the rest into their owner’s graveyard.').
card_ruling('clever impersonator', '2014-09-20', 'If Clever Impersonator enters the battlefield as a copy of a planeswalker, it will enter the battlefield with a number of loyalty counters on it equal to the loyalty printed in the lower right corner of the planeswalker card. It won’t copy the number of loyalty counters on the original planeswalker.').
card_ruling('clever impersonator', '2014-09-20', 'Clever Impersonator copies exactly what is printed on the chosen permanent (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that permanent is tapped or untapped, whether that permanent has any counters on it or any Auras and/or Equipment attached to it, and it doesn’t copy any non-copy effects that have changed that permanent’s power, toughness, types, color, abilities, or so on.').
card_ruling('clever impersonator', '2014-09-20', 'If the chosen permanent has {X} in its mana cost, X is considered to be 0.').
card_ruling('clever impersonator', '2014-09-20', 'If the chosen permanent is a copy of something else (for example, if the chosen permanent is another Clever Impersonator), then your Clever Impersonator enters the battlefield as whatever the chosen permanent copied.').
card_ruling('clever impersonator', '2014-09-20', 'If the chosen permanent is a token, Clever Impersonator copies the original characteristics of that token as defined by the effect that put that token onto the battlefield. Clever Impersonator is not a token.').
card_ruling('clever impersonator', '2014-09-20', 'Any enters-the-battlefield abilities of the chosen permanent will trigger when Clever Impersonator enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the chosen permanent will also work.').
card_ruling('clever impersonator', '2014-09-20', 'If Clever Impersonator enters the battlefield at the same time as another permanent, it can’t become a copy of that permanent. You may only choose a nonland permanent that’s already on the battlefield.').
card_ruling('clever impersonator', '2014-09-20', 'You can choose not to copy anything. In that case, Clever Impersonator enters the battlefield as a 0/0 creature, and it’s put into the graveyard immediately (unless something is raising its toughness above 0).').

card_ruling('cliffrunner behemoth', '2009-02-01', 'As long as at least one permanent you control is the specified color, the ability will \"work\" and grant this creature the bonus. Otherwise, it won\'t have the bonus.').
card_ruling('cliffrunner behemoth', '2009-02-01', 'Whether Cliffrunner Behemoth has haste matters only when attackers are declared. Once it\'s been declared as an attacker, it doesn\'t matter if it loses haste.').
card_ruling('cliffrunner behemoth', '2009-02-01', 'Whether Cliffrunner Behemoth has lifelink matters only when it deals damage (that is, at the point when its combat damage resolves, or at the point when a spell or ability that causes it to deal damage resolves). For example, if your only white permanent is a creature that receives lethal damage at the same time Cliffrunner Behemoth deals damage, the lifelink ability will still apply and you\'ll gain life.').

card_ruling('cliffside market', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('cliffside market', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('cliffside market', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('cliffside market', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('cliffside market', '2009-10-01', 'For two players to exchange life totals, what actually happens is that each player gains or loses the amount of life necessary to equal the other player\'s previous life total. For example, if Player A has 5 life and Player B has 3 life before the exchange, Player A will lose 2 life and Player B will gain 2 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('cliffside market', '2009-10-01', 'Neither permanent you target with the {C} ability needs to be controlled by you.').
card_ruling('cliffside market', '2009-10-01', 'The control-change effect has no duration. For each permanent, it will last until the game ends or the permanent leaves the battlefield. It will override all previous control-change effects for those permanents, and may be overridden by a later control-change effect.').
card_ruling('cliffside market', '2009-10-01', 'Both permanents targeted by the {C} ability may be controlled by the same player. If they are, the effect nothing.').
card_ruling('cliffside market', '2009-10-01', 'If either permanent targeted by the {C} ability is an illegal target by the time the ability resolves, the exchange doesn\'t happen. If both targets become illegal, the ability is countered.').

card_ruling('clinging anemones', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('clinging anemones', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('clinging anemones', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('clinging anemones', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('clinging anemones', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('clinging anemones', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('clinging mists', '2011-01-22', 'In most cases, attacking creatures will already be tapped. They still won\'t untap during their controller\'s next untap step.').
card_ruling('clinging mists', '2011-01-22', 'Clinging Mists doesn\'t track the controller of the attacking creatures. If one of them changes controller before its previous controller\'s next untap step, it won\'t untap during its new controller\'s next untap step.').

card_ruling('cloak and dagger', '2008-04-01', 'Each of these Equipment has two subtypes listed on its type line. The first one is a creature type, which in this case is also a subtype of tribal. The second one is Equipment, which is a subtype of artifact.').
card_ruling('cloak and dagger', '2008-04-01', 'Each of these Equipment has a triggered ability that says \"Whenever a [creature type] creature enters the battlefield, you may attach [this Equipment] to it.\" This triggers whenever any creature of the specified creature type enters the battlefield, no matter who controls it. You may attach your Equipment to another player\'s creature this way, even though you can\'t do so with the equip ability.').
card_ruling('cloak and dagger', '2008-04-01', 'If you attach an Equipment you control to another player\'s creature, you retain control of the Equipment, but you don\'t control the creature. Only you can activate the Equipment\'s equip ability, and if the Equipment\'s ability triggers again, you choose whether to move the Equipment. Only the creature\'s controller can activate any activated abilities the Equipment grants to the creature, and \"you\" in any abilities granted to the creature refers to that player.').

card_ruling('cloak of confusion', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('clockspinning', '2006-09-25', 'Clockspinning can affect any kind of counter, not just a time counter. The type of counter isn\'t chosen until resolution. Whether to add or remove a counter isn\'t chosen until resolution.').
card_ruling('clockspinning', '2006-09-25', 'If the target is a permanent and it has no counter on it when Clockspinning resolves, nothing happens.').
card_ruling('clockspinning', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('clockspinning', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('clockspinning', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('clockspinning', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('clockspinning', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('clockspinning', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('clockspinning', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('clockspinning', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('clockspinning', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('clockspinning', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('clockwork avian', '2004-10-04', 'Loses a counter even if it is affected by a Fog-like effect which prevents it from dealing damage.').
card_ruling('clockwork avian', '2004-10-04', 'Can attack or block even if it has no counters.').
card_ruling('clockwork avian', '2004-10-04', 'If the ability to add counters resolves when there are already the maximum number of counters on it, any counters over the maximum are simply not added.').

card_ruling('clockwork beast', '2004-10-04', 'Loses a counter even if it is affected by a Fog-like effect which prevents it from dealing damage.').
card_ruling('clockwork beast', '2004-10-04', 'Can attack or block even if it has no counters.').
card_ruling('clockwork beast', '2007-09-16', 'This is a change from the most recent wording. Now, if some other spell or ability causes +1/+0 counters to be put on Clockwork Beast, it can wind up with more than seven such counters on it.').
card_ruling('clockwork beast', '2007-09-16', 'If Clockwork Beast has seven or fewer +1/+0 counters on it when its last ability resolves, it can wind up a maximum of seven such counters on it. If it has seven or more +1/+0 counters on it, the ability will have no effect.').
card_ruling('clockwork beast', '2007-09-16', 'Clockwork Beast\'s last ability resolves, you can choose to put fewer than X +1/+0 counters on it.').

card_ruling('clockwork steed', '2004-10-04', 'Loses a counter even if it is affected by a Fog-like effect which prevents it from dealing damage.').
card_ruling('clockwork steed', '2004-10-04', 'Can attack or block even if it has no counters.').
card_ruling('clockwork steed', '2008-10-01', 'When Clockwork Steed\'s last ability resolves, you can choose to put fewer than X +1/+0 counters on it.').
card_ruling('clockwork steed', '2008-10-01', 'If Clockwork Steed has four or fewer +1/+0 counters on it when its last ability resolves, it can wind up with a maximum of four such counters on it. If it has four or more +1/+0 counters on it, the ability will have no effect.').

card_ruling('clockwork swarm', '2004-10-04', 'Loses a counter even if it is affected by a Fog-like effect which prevents it from dealing damage.').
card_ruling('clockwork swarm', '2004-10-04', 'Can attack or block even if it has no counters.').
card_ruling('clockwork swarm', '2004-10-04', 'If the ability to add counters resolves when there are already the maximum number of counters on it, any counters over the maximum are simply not added.').

card_ruling('clone', '2007-07-15', 'If the chosen creature is copying something else (for example, if the chosen creature is another Clone), then your Clone enters the battlefield as whatever the chosen creature copied.').
card_ruling('clone', '2007-07-15', 'If the chosen creature is a token, Clone copies the original characteristics of that token as stated by the effect that put it onto the battlefield. Clone is not a token.').
card_ruling('clone', '2007-07-15', 'Any enters-the-battlefield abilities of the copied creature will trigger when Clone enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').
card_ruling('clone', '2009-10-01', 'If the chosen creature has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('clone', '2012-07-01', 'Clone copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below). It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('clone', '2012-07-01', 'If Clone somehow enters the battlefield at the same time as another creature, Clone can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('clone', '2012-07-01', 'You can choose not to copy anything. In that case, Clone enters the battlefield as a 0/0 Shapeshifter creature, and is probably put into the graveyard immediately.').
card_ruling('clone', '2013-07-01', 'Clone\'s ability doesn’t target the chosen creature.').

card_ruling('clone legion', '2015-02-25', 'The token creatures all enter the battlefield at the same time.').
card_ruling('clone legion', '2015-02-25', 'A token that enters the battlefield as a copy of a face-down creature is a face-up colorless 2/2 creature with no name, abilities, or creature types.').
card_ruling('clone legion', '2015-02-25', 'Each token copies exactly what was printed on the original creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('clone legion', '2015-02-25', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('clone legion', '2015-02-25', 'If the copied creature is copying something else, then the token enters the battlefield as whatever that creature copied.').
card_ruling('clone legion', '2015-02-25', 'If the copied creature is a token, the token created by Clone Legion copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('clone legion', '2015-02-25', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').
card_ruling('clone legion', '2015-02-25', 'Remember that if you control more than one legendary permanent with the same name, you’ll choose one to remain on the battlefield and put the rest into their owners’ graveyards.').

card_ruling('clone shell', '2011-01-01', 'If you have fewer than four cards in your library as Clone Shell\'s first ability resolves, you\'ll look at all of them.').
card_ruling('clone shell', '2011-01-01', 'As Clone Shell\'s first ability resolves, you must exile one of the cards you look at, even if none of them is a creature card.').
card_ruling('clone shell', '2011-01-01', 'As Clone Shell\'s second ability resolves, if the exiled card is not a creature card, it simply remains in exile face up.').
card_ruling('clone shell', '2011-01-01', 'If you gain control of another player\'s Clone Shell, you won\'t be able to look at the face-down exiled card. However, if Clone Shell is put into a graveyard, you will turn that card face up as the ability resolves and, if it\'s a creature card, you will put it onto the battlefield under your control (even if you don\'t want to).').

card_ruling('clot sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('clot sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('cloud of faeries', '2004-10-04', 'You can untap another player\'s lands.').
card_ruling('cloud of faeries', '2004-10-04', 'This does not target the lands.').
card_ruling('cloud of faeries', '2004-10-04', 'You can untap 0, 1, or 2 lands.').
card_ruling('cloud of faeries', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('cloud spirit', '2004-10-04', 'If it loses Flying, it will be unable to block any creatures.').

card_ruling('cloudchaser eagle', '2004-10-04', 'It destroys one of your enchantments if you are the only player with any enchantments.').
card_ruling('cloudchaser eagle', '2004-10-04', 'Nothing happens if there are no legal enchantments to target when it enters the battlefield.').

card_ruling('cloudfin raptor', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('cloudfin raptor', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('cloudfin raptor', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('cloudfin raptor', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('cloudfin raptor', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('cloudfin raptor', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('cloudform', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('cloudform', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('cloudform', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('cloudform', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('cloudform', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('cloudform', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('cloudform', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('cloudform', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('cloudform', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('cloudform', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('cloudform', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('cloudform', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('cloudform', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').
card_ruling('cloudform', '2014-11-24', 'You’ll still manifest the top card of your library even if the “Form” isn’t on the battlefield as its enters-the-battlefield ability resolves.').
card_ruling('cloudform', '2014-11-24', 'If you have no cards in your library as the ability resolves, the “Form” will be put into its owner’s graveyard as a state-based action.').
card_ruling('cloudform', '2014-11-24', 'If the enchanted creature is turned face up, the “Form” will continue to enchant it.').

card_ruling('cloudheath drake', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('cloudhoof kirin', '2005-06-01', 'If the Spirit or Arcane spell has {X} in the mana cost, then you use the value of {X} on the stack. For example, Shining Shoal costs {X}{W}{W}. If you choose X = 2, then Shining Shoal\'s converted mana cost is 4. Shining Shoal also has an ability that says \"You may exile a white card with converted mana cost X from your hand rather than pay Shining Shoal\'s mana cost\"; if you choose to pay the alternative cost and exile a card with converted mana cost 2, then X is 2 while Shining Shoal is on the stack and its converted mana cost is 4.').

card_ruling('cloudthresher', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('clout of the dominus', '2008-08-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('cloven casting', '2009-05-01', 'This ability triggers only once for each multicolored instant or sorcery spell you cast. You can\'t use a single Cloven Casting to copy the same spell twice.').
card_ruling('cloven casting', '2009-05-01', 'You can copy the spell that caused Cloven Casting\'s ability to trigger even if it\'s been countered by the time that ability resolves.').
card_ruling('cloven casting', '2009-05-01', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. The new targets must be legal.').
card_ruling('cloven casting', '2009-05-01', 'If a modal spell is copied, the copy will have the same mode as the original.').
card_ruling('cloven casting', '2009-05-01', 'Abilities that trigger when a spell is cast (such as this ability or cascade) won\'t trigger when you create a copy this way.').

card_ruling('clutch of currents', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('clutch of currents', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('clutch of currents', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('clutch of currents', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('clutch of currents', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('clutch of currents', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('coal stoker', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').
card_ruling('coal stoker', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('coalition flag', '2004-10-04', 'If this card is ever on a creature you don\'t control, it is put into the graveyard as a State-Based Action.').

card_ruling('coalition relic', '2007-05-01', 'If you remove multiple charge counters from Coalition Relic at once, you may add a different color of mana to your mana pool for each one.').
card_ruling('coalition relic', '2007-05-01', 'Only the first main phase each turn is considered a precombat main phase, even if other combat phases are created (with Relentless Assault, for example).').

card_ruling('coalition victory', '2006-09-25', 'When Coalition Victory resolves, it checks for the five basic land types (Plains, Island, Swamp, Mountain, Forest) and the five colors (white, blue, black, red, green). If a single land has multiple types and/or a single creature is multiple colors, it will count all those types and/or colors.').

card_ruling('coastal discovery', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('coastal discovery', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('coastal discovery', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('coastal discovery', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('coastal discovery', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('coastal discovery', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('coastal piracy', '2004-10-04', 'You draw one card per creature, not one per point of damage.').

card_ruling('coastal wizard', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('coastline chimera', '2013-09-15', 'Coastline Chimera’s activated ability is cumulative. Activating it a second time will allow it to block three creatures, and so on.').

card_ruling('coat of arms', '2004-10-04', 'If a creature has more than one creature type, and one of those types matches the creature you are calculating for, then count that creature. Only one type needs to match in order to get counted.').
card_ruling('coat of arms', '2004-10-04', 'If you have a creature with more than one creature type, count all creatures which have either creature type.').
card_ruling('coat of arms', '2009-10-01', 'Sharing multiple creature types doesn\'t give an additional bonus. Coat of Arms counts creatures, not creature types.').

card_ruling('cobra trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('cobra trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('cobra trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('cobra trap', '2009-10-01', 'A spell or ability destroys a permanent only if that spell or ability specifically contains the word \"destroy\" in its text. If a spell or ability an opponent controls exiles a noncreature permanent you control, causes you to sacrifice a noncreature permanent, removes all loyalty counters from a planeswalker you control, or causes an ability you control to trigger (and then that ability destroys a permanent you control), Cobra Trap\'s alternative cost condition hasn\'t been met.').

card_ruling('cockatrice', '2004-10-04', 'The ability destroys the creature at the end of the combat, which is after all first strike and normal damage dealing is done. This means that a creature may have to regenerate twice to survive the combat, once from damage and once again at end of combat.').

card_ruling('cocoon', '2004-10-04', 'The creature can still be untapped using spells and abilities.').
card_ruling('cocoon', '2004-10-04', 'Cocoon can be used on a tapped creature.').
card_ruling('cocoon', '2005-08-01', 'If the Aura is moved to a different creature, the number of counters is unchanged.').

card_ruling('codex shredder', '2012-10-01', 'You choose the target of Codex Shredder\'s last ability before paying its costs, so you can\'t return Codex Shredder to your hand this way.').

card_ruling('coercive portal', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('coercive portal', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('coercive portal', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('coercive portal', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('coercive portal', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('coffin puppets', '2004-10-04', 'The ability works even if you sacrifice your last swamp to activate it. It only checks for a swamp on activation.').

card_ruling('coffin queen', '2004-10-04', 'If the creature put onto the battlefield leaves the battlefield, the tracking effect ends and the creature will no longer be exiled if something happens to the Coffin Queen.').
card_ruling('coffin queen', '2004-10-04', 'If the creature put onto the battlefield phases out, the tracking effect continues when it phases back in if the Coffin Queen is still on the battlefield. If the Queen left the battlefield while the creature was phased out, the creature phases in as normal.').
card_ruling('coffin queen', '2014-02-01', 'If Coffin Queen leaves the battlefield before its ability resolves, the delayed trigger condition can never be fulfilled, so the creature will not be exiled. If Coffin Queen is untapped when its ability resolves, the creature will not be exiled until the next time Coffin Queen becomes untapped.').

card_ruling('cogwork grinder', '2014-05-29', 'You still draft the card that you remove. This may matter if an ability refers to “the next card you draft” or similar. If that card instructs you to draft it face up, do so, then turn it face down as you remove it. If that card instructs you to reveal it as you draft it and then perform additional actions, perform those actions before turning the card face down and removing it from the draft.').
card_ruling('cogwork grinder', '2014-05-29', 'A card that is removed from the draft isn’t in any player’s card pool. That card can’t be played in any games associated with that draft.').
card_ruling('cogwork grinder', '2014-05-29', 'You can’t remove cards from the draft that you’ve already drafted. Only cards drafted after you draft Cogwork Grinder may be removed this way.').
card_ruling('cogwork grinder', '2014-05-29', 'If you draft multiple Cogwork Grinders, the value for X will be the same for each of them. It doesn’t matter that some of the removed cards may have been removed before you drafted the second Cogwork Grinder, for example.').
card_ruling('cogwork grinder', '2014-05-29', 'No other player may look at the cards you remove from the draft (unless Cogwork Spy allows a player to).').
card_ruling('cogwork grinder', '2014-05-29', 'If a Cogwork Grinder owned by another player enters the battlefield under your control, the number of counters it enters with will be based on the number of cards you’ve removed from the draft with Cogwork Grinders, not the number its owner removed. Unless you also drafted Cogwork Grinder, this will be 0.').

card_ruling('cogwork librarian', '2014-05-29', 'Cogwork Librarian essentially lets you draft two cards from a booster pack in exchange for putting Cogwork Librarian back into that booster pack.').
card_ruling('cogwork librarian', '2014-05-29', 'The two cards you draft are drafted one at a time, in case one of the cards cares about when it was drafted or what the next card you draft is.').
card_ruling('cogwork librarian', '2014-05-29', 'You may use the ability of more than one Cogwork Librarian while drafting from one booster pack. For example you can draft three cards from one booster pack and put two Cogwork Librarians into that booster pack.').

card_ruling('cogwork spy', '2014-05-29', 'If you are the next person to draft from the booster pack (perhaps because of Cogwork Librarian), the first ability doesn’t do anything.').
card_ruling('cogwork spy', '2014-05-29', 'You may look at the card even if it’s removed from the draft face down by Cogwork Grinder.').

card_ruling('cogwork tracker', '2014-05-29', 'If your draft is breaking up into multiple games, you and the noted player(s) aren’t required to be in the same game.').
card_ruling('cogwork tracker', '2014-05-29', 'If the noted player isn’t in the game, Cogwork Tracker’s last ability won’t do anything, although it must still attack each turn if able. This is also true if there is no noted player because Cogwork Tracker was the first card drafted from a booster pack.').
card_ruling('cogwork tracker', '2014-05-29', 'If the last ability doesn’t apply, you choose which player or planeswalker Cogwork Tracker attacks.').
card_ruling('cogwork tracker', '2014-05-29', 'If there are multiple noted players, perhaps because the players on both your left and right passed a Cogwork Tracker to you, you choose which of those players each Cogwork Tracker attacks. If you control more than one, they can each attack a different noted player.').
card_ruling('cogwork tracker', '2014-05-29', 'If, during your declare attackers step, Cogwork Tracker is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').

card_ruling('cold storage', '2004-10-04', 'If this card leaves the battlefield, the cards it exiled remain exiled.').
card_ruling('cold storage', '2004-10-04', 'If it exiles a token creature, the token ceases to exist and will not return.').
card_ruling('cold storage', '2008-04-01', 'For a time, the second ability returned all cards exiled by it to the battlefield, regardless of what types they had while exiled. It has been restored to its original functionality, so only cards that actually have the type Creature will get returned.').

card_ruling('colfenor\'s plans', '2007-10-01', 'The turn you cast Colfenor\'s Plans, you have cast (at least) one spell that turn. After it enters the battlefield and its last ability goes into effect, you can\'t cast any more spells that turn.').
card_ruling('colfenor\'s plans', '2007-10-01', 'Playing a card exiled with Colfenor\'s Plans follows all the normal rules for playing that card. You must pay its costs, and you must follow all timing restrictions, for example.').
card_ruling('colfenor\'s plans', '2007-10-01', 'If an exiled card has morph, you may cast it face down.').
card_ruling('colfenor\'s plans', '2013-04-15', 'If Colfenor\'s Plans leaves the battlefield, you can continue to look at cards exiled by it, but you can no longer play them.').

card_ruling('colfenor\'s urn', '2007-10-01', 'The second ability checks how many creatures have been exiled with Colfenor\'s Urn over the course of the entire game.').
card_ruling('colfenor\'s urn', '2007-10-01', 'The second ability forces you to sacrifice Colfenor\'s Urn. If the ability triggers and the Urn isn\'t sacrificed (because it left the battlefield or changed controllers, for example), the exiled cards won\'t be returned to the battlefield.').
card_ruling('colfenor\'s urn', '2007-10-01', 'The first ability triggers when a token creature with toughness 4 or greater is put into your graveyard. However, that token will cease to exist before Colfenor\'s Plans can exile it.').

card_ruling('collapsing borders', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('collapsing borders', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('collapsing borders', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('collateral damage', '2014-11-24', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won’t deal combat damage. If you wait until the combat damage step, but that creature is dealt lethal damage, it’ll be destroyed before you get a chance to sacrifice it.').
card_ruling('collateral damage', '2014-11-24', 'You must sacrifice exactly one creature to cast this spell; you can’t cast it without sacrificing a creature, and you can’t sacrifice additional creatures.').
card_ruling('collateral damage', '2014-11-24', 'Players can respond to Collateral Damage only after it’s been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to stop you from casting this spell.').

card_ruling('collected company', '2015-02-25', 'Each of the creature cards can have converted mana cost 3 or less. It’s not the total converted mana cost of the two cards.').

card_ruling('collective restraint', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').
card_ruling('collective restraint', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('collective restraint', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('collective restraint', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').
card_ruling('collective restraint', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('collective voyage', '2011-09-22', 'You may find any number of basic lands up to X, including zero. However, searching and shuffling your library is not optional.').

card_ruling('colossal heroics', '2014-04-26', 'Colossal Heroics can target any creatures, not just ones that are tapped.').
card_ruling('colossal heroics', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('colossal heroics', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('colossal heroics', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('colossal heroics', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('colossal heroics', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').
card_ruling('colossal heroics', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('colossal heroics', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('colossal heroics', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('colossal whale', '2013-07-01', 'Colossal Whale’s triggered ability triggers and resolves during the declare attackers step, before blocking creatures are declared.').
card_ruling('colossal whale', '2013-07-01', 'Colossal Whale’s ability causes a zone change with a duration, a new style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Colossal Whale have a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Colossal Whale leaves the battlefield.').
card_ruling('colossal whale', '2013-07-01', 'If Colossal Whale leaves the battlefield before its triggered ability resolves, the target creature won’t be exiled.').
card_ruling('colossal whale', '2013-07-01', 'Auras attached to the exiled creature will be put into their owners’ graveyards. Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('colossal whale', '2013-07-01', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('colossal whale', '2013-07-01', 'The exiled card returns to the battlefield immediately after Colossal Whale leaves the battlefield. Nothing happens between the two events, including state-based actions. The two creatures aren’t on the battlefield at the same time. For example, if the returning creature is a Clone, it can’t enter the battlefield as a copy of Colossal Whale.').
card_ruling('colossal whale', '2013-07-01', 'In a multiplayer game, if Colossal Whale’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('colossus of akros', '2013-09-15', 'Colossus of Akros doesn’t lose defender when it’s monstrous. It’s just able to attack.').
card_ruling('colossus of akros', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('colossus of akros', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('colossus of akros', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('colossus of sardia', '2008-05-01', 'The ability that untaps it during your upkeep has been returned to an activated ability. There is no restriction on how many times it can be untapped during your upkeep with this ability.').

card_ruling('coma veil', '2008-10-01', 'Coma Veil may target and may enchant an untapped artifact or creature.').

card_ruling('combust', '2010-08-15', 'Combust can be targeted by spells and abilities that try to counter it (such as Cancel). Those spells and abilities will resolve, but the part of their effect that would counter Combust won\'t do anything. Any other effects those spells and abilities have will work as normal.').
card_ruling('combust', '2010-08-15', 'If the targeted creature is an illegal target by the time Combust would resolve, Combust will be countered by the game rules.').
card_ruling('combust', '2010-08-15', 'Spells that create prevention effects affecting the targeted creature can still be cast, and abilities that create prevention effects affecting the targeted creature can still be activated. However, damage prevention shields (including those created before Combust was cast) don\'t have any effect on the damage dealt by Combust. If such a prevention effect has an additional effect, the additional effect will still work (if possible).').
card_ruling('combust', '2010-08-15', 'If a static ability would prevent damage from being dealt to the targeted creature, it fails to prevent the damage dealt by Combust. If that ability has an additional effect that doesn\'t depend on the amount of damage prevented, that additional effect will still work. It\'s applied just once as Combust resolves.').
card_ruling('combust', '2010-08-15', 'Effects that replace or redirect damage without using the word \"prevent\" aren\'t affected by Combust; they\'ll work as normal.').
card_ruling('combust', '2010-08-15', 'If a creature is dealt lethal damage by Combust, it can still regenerate. If it does, the damage marked on it will be removed from it.').

card_ruling('comet storm', '2010-03-01', 'The number of targets you choose for Comet Storm is one more than the number of times it\'s kicked. First you declare how many times you\'re going to kick the spell (at the same time you declare the value of X), then you choose the targets accordingly, then you pay the costs. No player can respond between the time you declare how many times you\'ll kick the spell and the time you choose the targets.').
card_ruling('comet storm', '2010-03-01', 'Each target you choose must be different.').
card_ruling('comet storm', '2010-03-01', 'For example, if you want Comet Storm to deal 4 damage to each of three different targets, that means X is 4 and you\'re kicking the spell twice. You\'ll pay a mana cost of {4}{R}{R}, plus a kicker cost of {1}, plus another kicker cost of {1}, for a total of {6}{R}{R}.').
card_ruling('comet storm', '2010-03-01', 'As long as any of its targets are legal at the time Comet Storm resolves, Comet Storm will deal X damage to each of those legal targets.').

card_ruling('comeuppance', '2014-11-07', 'Comeuppance’s effect is not a redirection effect. If it prevents damage, Comeuppance (not the original source) deals damage to the creature or player as part of that prevention effect. Comeuppance is the source of the new damage, so the characteristics of the original source (such as its color, or whether it had lifelink or deathtouch) don’t affect the new damage. The new damage is not combat damage, even if the prevented damage was.').
card_ruling('comeuppance', '2014-11-07', 'If Comeuppance would deal damage to an opponent, you may redirect that damage to a planeswalker that player controls.').

card_ruling('command tower', '2011-09-22', 'The color identity of your commander is set before the game begins and doesn\'t change during the game, even your commander is in a hidden zone (like the hand or library) or an effect changes your commander\'s color.').
card_ruling('command tower', '2011-09-22', 'If your commander is a card like Kozilek, Butcher of Truth that has no colors in its color identity, Command Tower\'s ability produces no mana.').
card_ruling('command tower', '2011-09-22', 'In formats other than Commander, Command Tower\'s ability produces no mana.').

card_ruling('commandeer', '2006-07-15', 'If you gain control of an instant or sorcery spell with Commandeer, it will still be put into its owner\'s graveyard when it resolves.').
card_ruling('commandeer', '2006-07-15', 'After Commandeer resolves, you control the targeted spell. Any instance of \"you\" in that spell\'s text now refers to you, \"an opponent\" refers to one of your opponents, and so on. The change of control happens before new targets are chosen, so any targeting restrictions such as \"target opponent\" or \"target creature you control\" are now made in reference to you, not the spell\'s original controller. You may either change those targets to be legal in reference to you, or, if those are the spell\'s only targets, the spell will be countered on resolution for having illegal targets. When the spell resolves, any illegal targets are unaffected by it and you make all decisions the spell\'s effect calls for.').
card_ruling('commandeer', '2006-07-15', 'You may change any of the targeted spell\'s targets. If you change a target, you must choose a legal target for the spell. If you can\'t, you must leave the target the same (even if that target is now illegal).').
card_ruling('commandeer', '2006-07-15', 'If the targeted spell has a triggered ability that copies it (for example, replicate or storm), the copies will be controlled by the player who cast that spell.').
card_ruling('commandeer', '2006-07-15', 'You may pay the alternative cost rather than the card\'s mana cost. Any additional costs are paid as normal.').
card_ruling('commandeer', '2006-07-15', 'If you don\'t have two cards of the right color in your hand, you can\'t choose to cast the spell using the alternative cost.').
card_ruling('commandeer', '2006-07-15', 'You can\'t exile a card from your hand to pay for itself. At the time you would pay costs, that card is on the stack, not in your hand.').
card_ruling('commandeer', '2007-02-01', 'If you Commandeer a spell for which Buyback has been paid, the card returns to its owner\'s hand.').

card_ruling('commander\'s authority', '2012-05-01', 'The controller of the enchanted creature gets the token.').

card_ruling('commander\'s sphere', '2014-11-07', 'The color identity of your commander is set before the game begins and doesn’t change during the game, even if your commander is in a hidden zone (such as your hand or your library) or if an effect changes your commander’s color.').
card_ruling('commander\'s sphere', '2014-11-07', 'If your commander has no colors in its color identity, Commander’s Sphere produces no mana.').
card_ruling('commander\'s sphere', '2014-11-07', 'In formats other than Commander, Commander’s Sphere produces no mana.').

card_ruling('common bond', '2012-10-01', 'You may choose the same creature for both targets. You may also choose two different creatures. This is because the word “target” is used twice.').

card_ruling('common cause', '2004-10-04', 'It really does mean \"all creatures\", including your opponent\'s.').
card_ruling('common cause', '2004-10-04', 'The ability gives +2/+2 to all creatures which are not artifact creatures, as long as every one of those creatures is of the same color. They can have additional colors beyond the one they share, but they all have to share one color. If any creature is not of the shared color, the bonus is lost.').

card_ruling('commune with lava', '2015-02-25', 'The cards are exiled face up.').
card_ruling('commune with lava', '2015-02-25', 'Playing a card this way follows the normal rules for playing the card. You must pay its costs, and you must follow all applicable timing rules. For example, if one of the cards is a creature card, you can cast that card only during your main phase while the stack is empty.').
card_ruling('commune with lava', '2015-02-25', 'Under normal circumstances, you can play a land card exiled with Commune with Lava only if you haven’t played a land yet that turn.').
card_ruling('commune with lava', '2015-02-25', 'Any cards you don’t play will remain exiled.').

card_ruling('commune with nature', '2004-12-01', 'If you don\'t reveal a creature card, put all the revealed cards on the bottom of your library in any order.').

card_ruling('complete disregard', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('complete disregard', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('complete disregard', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('complete disregard', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('complete disregard', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('complicate', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('complicate', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('complicate', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('complicate', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('compost', '2004-10-04', 'The ability triggers on a black card being discarded, milled, or countered, as well as a black card going to the graveyard from the battlefield.').

card_ruling('compulsive research', '2005-10-01', 'A player can discard either one land card or two cards which may or may not be lands. (The player can discard two land cards if he or she chooses).').

card_ruling('concerted effort', '2005-10-01', 'For example, a player controls three creatures when Concerted Effort\'s ability resolves: one creature with flying and protection from red, one with islandwalk and protection from green; and one with vigilance. All three creatures will have flying, islandwalk, vigilance, protection from red, and protection from green until the end of the turn.').
card_ruling('concerted effort', '2005-10-01', 'Creatures keep all abilities granted this way until the end of the turn, even if Concerted Effort or the original creature with that ability leaves the battlefield.').
card_ruling('concerted effort', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('conch horn', '2004-10-04', 'You draw two cards, then put one back as part of the same effect. You can\'t do anything in between. Anything that triggers off of card drawing will wait until you finish resolving this ability before going on the stack.').

card_ruling('conclave equenaut', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('conclave equenaut', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('conclave equenaut', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('conclave equenaut', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('conclave equenaut', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('conclave equenaut', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('conclave equenaut', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('conclave phalanx', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('conclave phalanx', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('conclave phalanx', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('conclave phalanx', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('conclave phalanx', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('conclave phalanx', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('conclave phalanx', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('conclave\'s blessing', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('conclave\'s blessing', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('conclave\'s blessing', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('conclave\'s blessing', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('conclave\'s blessing', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('conclave\'s blessing', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('conclave\'s blessing', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('concordant crossroads', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').
card_ruling('concordant crossroads', '2009-10-01', 'This ability affects all creatures, not just the ones you control.').

card_ruling('concussive bolt', '2011-06-01', 'The number of artifacts you control is checked only once: when Concussive Bolt resolves. If you don\'t control three or more artifacts at that time, creatures will be able to block that turn, even if you gain control of more artifacts later in the turn.').
card_ruling('concussive bolt', '2011-06-01', 'If the player is an illegal target when Concussive Bolt resolves, the spell will be countered. None of its effects will happen, even if you control three or more artifacts.').
card_ruling('concussive bolt', '2011-06-01', 'If you controlled three or more artifacts as Concussive Bolt resolved, the targeted player won\'t be able to block with any creatures this turn. That is, the set of affected creatures isn\'t locked in as the spell resolves.').

card_ruling('condemn', '2010-08-15', 'The affected creature\'s last known existence on the battlefield is checked to determine its toughness.').

card_ruling('conduit of ruin', '2015-08-25', 'Conduit of Ruin’s last ability doesn’t change the mana cost or converted mana cost of any spell. It changes only the total cost you actually pay.').
card_ruling('conduit of ruin', '2015-08-25', 'Conduit of Ruin’s last ability will look at the entire turn, even if Conduit of Ruin wasn’t on the battlefield for some of it. Notably, if you cast Conduit of Ruin in a turn, then no other creature spell you cast that turn can be your first.').
card_ruling('conduit of ruin', '2015-08-25', 'Conduit of Ruin’s last ability can’t reduce the amount of colored mana you pay for a spell. It reduces only the generic component of that mana cost.').
card_ruling('conduit of ruin', '2015-08-25', 'If you control more than one Conduit of Ruin, the last ability of each of them applies only to the first creature spell you cast each turn, not to different spells; the first creature spell you cast each turn will cost {4} less.').
card_ruling('conduit of ruin', '2015-08-25', 'The first creature spell you cast each turn doesn’t necessarily have to be the first spell you cast. You could cast a sorcery spell and then cast a creature spell that would get the discount.').
card_ruling('conduit of ruin', '2015-08-25', 'If there are additional costs to cast a spell, or if the cost to cast a spell is increased by an effect, apply those increases before applying cost reductions.').
card_ruling('conduit of ruin', '2015-08-25', 'The cost reduction can apply to alternative costs (such as dash costs).').
card_ruling('conduit of ruin', '2015-08-25', 'If the first creature spell you cast in a turn has {X} in its mana cost, you choose the value of X before calculating the spell’s total cost. For example, if the first creature spell you cast in a turn has a mana cost of {X}{G}, you could choose 2 as the value of X and pay {G} to cast the spell.').
card_ruling('conduit of ruin', '2015-08-25', 'If the first creature spell you cast in a turn has converge, you can’t ignore the cost reduction of Conduit of Ruin’s last ability in order to spend more colors of mana.').

card_ruling('cone of flame', '2014-07-18', 'Each of the three targets must be different. If there aren’t three different legal targets available, you can\'t cast the spell.').
card_ruling('cone of flame', '2014-07-18', 'If one or two of Cone of Flame’s targets are illegal when it resolves, you can’t change how much damage will be dealt to the remaining legal targets.').

card_ruling('confiscate', '2004-10-04', 'If you take control of an Aura, you do not get to move it.').

card_ruling('conflagrate', '2006-09-25', 'If X is greater than 0, the number of targets must be at least 1 and at most X. If X is 0, the number of targets must also be 0.').
card_ruling('conflagrate', '2006-09-25', 'While Conflagrate is on the stack, its mana cost will reflect the value chosen for X, even if it was cast with flashback and no mana was spent on X.').

card_ruling('conflux', '2009-02-01', 'You don\'t have to find all five cards.').
card_ruling('conflux', '2009-02-01', 'You may find multicolored cards with Conflux. The white card you find, for example, can be any number of colors, as long as white is one of them. What colors it is won\'t impact what other cards you can find. For example, you may find a white-blue card as the white card and another white-blue card as the blue card.').

card_ruling('confound', '2004-10-04', 'Confound is countered on resolution if the targeted spell has been countered or does not still target one or more permanents that are still creatures and are still on the battlefield. Confound is different from other such spells in that it actually specifies that the spell must target a creature, not just be able to target a creature or that the spell has a certain number of targets.').

card_ruling('confusion in the ranks', '2004-10-04', 'The exchange isn\'t optional. If there is a legal choice, you must make one.').
card_ruling('confusion in the ranks', '2004-12-01', 'The permanents have to share a permanent type both when the target is chosen and when the ability resolves.').
card_ruling('confusion in the ranks', '2004-12-01', 'Even though Confusion in the Ranks triggers only when an artifact, creature, or enchantment enters the battlefield, the ability can target a land if the permanent entering the battlefield is an artifact land.').
card_ruling('confusion in the ranks', '2004-12-01', 'The permanents are exchanged only if they\'re both on the battlefield when the ability resolves.').
card_ruling('confusion in the ranks', '2004-12-01', 'Confusion in the Ranks triggers on itself entering the battlefield. If an opponent controls an enchantment, exchange Confusion in the Ranks for that enchantment.').

card_ruling('congregate', '2004-10-04', 'Creatures are counted during resolution.').

card_ruling('congregation at dawn', '2005-10-01', 'The order in which the cards are placed on top of the library isn\'t revealed.').

card_ruling('conjured currency', '2012-10-01', 'The only legal target for Conjured Currency\'s ability is a permanent both owned and controlled by another player, although it doesn\'t have to be the same player. For example, a permanent owned by an opponent but controlled by your teammate (or another opponent) could be chosen as the target.').
card_ruling('conjured currency', '2012-10-01', 'The exchange occurs only if Conjured Currency and the target permanent are both on the battlefield and controlled by different players when the ability resolves. In addition, the target permanent must be one you neither own nor control. If any of these things aren\'t true when the ability tries to resolve, the exchange won\'t happen and neither permanent will change controllers.').
card_ruling('conjured currency', '2012-10-01', 'You don\'t necessarily have to control Conjured Currency when its ability resolves in order for the exchange to happen. If another player has gained control of Conjured Currency while the ability is on the stack, and a third player controls the target (and you don\'t own it), you may choose to have those two players exchange control of the relevant permanents.').

card_ruling('conjurer\'s ban', '2006-02-01', 'Players may cast spells while Conjurer\'s Ban is on the stack. Only after all players pass priority does Conjurer\'s Ban resolve, and that\'s when you name a card. Once you name a card, that card can\'t be played in response.').
card_ruling('conjurer\'s ban', '2006-02-01', 'You may name a land card to prevent it from being played.').
card_ruling('conjurer\'s ban', '2006-02-01', 'Conjurer\'s Ban only affects cards. A copy of a card (for example, one created by Eye of the Storm isn\'t a card, so it can be played.').
card_ruling('conjurer\'s ban', '2008-08-01', 'This can\'t be used as a counterspell. It will have no effect on spells which were on the stack when it was cast, nor on those cast in response to it.').

card_ruling('conquering manticore', '2010-06-15', 'You may target a creature that\'s already untapped.').
card_ruling('conquering manticore', '2010-06-15', 'You retain control of the affected creature until end of turn, even if you lose control of Conquering Manticore before then.').

card_ruling('consecrate land', '2005-04-01', 'The land can be targeted by land-destroying spells and the spell will resolve, but the land will simply not be destroyed.').
card_ruling('consecrate land', '2006-09-25', 'If Consecrate Land enters the battlefield attached to a land that\'s enchanted by other Auras, those Auras are put into their owners\' graveyards.').
card_ruling('consecrate land', '2013-07-01', 'A permanent with indestructible can\'t be destroyed, but it can still be sacrificed, exiled, put into a graveyard, and so on.').

card_ruling('consecrated by blood', '2015-06-22', 'If the regeneration ability is activated before combat damage is dealt, the two creatures you sacrifice won’t deal combat damage. However, you must regenerate a creature before it would be destroyed, so if you wait for combat damage to be dealt, the enchanted creature may be destroyed by that damage before it has the chance to regenerate.').

card_ruling('consecrated sphinx', '2011-06-01', 'You may either draw two cards or not draw at all. You can\'t choose to draw only one card.').
card_ruling('consecrated sphinx', '2011-06-01', 'The ability triggers once for each card an opponent draws. You choose whether to draw two cards as each of those abilities resolves.').
card_ruling('consecrated sphinx', '2011-06-01', 'If each player controls a Consecrated Sphinx, their abilities will cause each other to trigger until one player chooses not to draw cards.').

card_ruling('consign to dust', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('consign to dust', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('consign to dust', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('consign to dust', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('consign to dust', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('conspiracy', '2004-10-04', 'It does not replace any use of creature types in card text.').
card_ruling('conspiracy', '2004-10-04', 'This can grant a creature type to animated lands and artifacts that would otherwise have no creature type.').
card_ruling('conspiracy', '2005-08-01', '\"Legend\" is no longer a creature type, and may not be chosen.').
card_ruling('conspiracy', '2005-08-01', 'If you choose Wall, then your creatures can still attack because creature types don\'t confer abilities such as defender (any more than, say, choosing creature type Bird would confer flying).').
card_ruling('conspiracy', '2013-09-20', 'Affected spells, cards, and creatures lose all of their other creature types.').

card_ruling('constricting sliver', '2014-07-18', 'Constricting Sliver gives Slivers you control an ability that causes a zone change with a duration, a newer style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring’s abilities, this ability creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after the Sliver leaves the battlefield.').
card_ruling('constricting sliver', '2014-07-18', 'When Constricting Sliver enters the battlefield, the ability it gives itself will trigger.').
card_ruling('constricting sliver', '2014-07-18', 'The Sliver that enters the battlefield is the source of the ability that exiles the creature. For example, if a blue Sliver enters the battlefield under your control, the ability can target and exile a creature with protection from white.').
card_ruling('constricting sliver', '2014-07-18', 'Auras attached to the exiled creature will be put into their owners’ graveyards (unless they’re Theros block cards with the bestow ability). Equipment attached to the exiled creature will become unattached and remain on the battlefield. Any counters on the exiled creature will cease to exist.').
card_ruling('constricting sliver', '2014-07-18', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('constricting sliver', '2014-07-18', 'If a Sliver leaves the battlefield before its enters-the-battlefield ability resolves, the target creature won’t be exiled.').
card_ruling('constricting sliver', '2014-07-18', 'If a Sliver you control leaves the battlefield, only the card that particular Sliver exiled will return to the battlefield. Each card exiled by another Sliver will remain exiled until that Sliver leaves the battlefield.').
card_ruling('constricting sliver', '2014-07-18', 'The exiled card returns to the battlefield immediately after the Sliver leaves the battlefield. Nothing happens between the two events, including state-based actions. The two creatures aren’t on the battlefield at the same time. For example, if the returning creature is a Mercurial Pretender, it can’t enter the battlefield as a copy of the Sliver.').
card_ruling('constricting sliver', '2014-07-18', 'The exiled card will return to the battlefield under its owner’s control.').
card_ruling('constricting sliver', '2014-07-18', 'The exiled card will return even if the Sliver whose ability exiled it doesn’t have that ability at the time (perhaps because Constricting Sliver is no longer on the battlefield).').
card_ruling('constricting sliver', '2014-07-18', 'In a multiplayer game, if the Sliver’s owner leaves the game, the exiled card will return to the battlefield. Because the one-shot effect that returns the card isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').
card_ruling('constricting sliver', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('constricting sliver', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('constricting sliver', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('constricting tendrils', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('consul\'s lieutenant', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('consul\'s lieutenant', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('consul\'s lieutenant', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('consume spirit', '2009-10-01', 'The amount of life you gain is equal to the number chosen for X, not the amount of damage Consume Spirit deals (in case some of it is prevented).').
card_ruling('consume spirit', '2009-10-01', 'If the targeted creature or player is an illegal target by the time Consume Spirit would resolve, the entire spell is countered. You won\'t gain any life.').

card_ruling('consuming aberration', '2013-01-24', 'If any opponent has no land cards in his or her library, all the cards from that library will be revealed and put into his or her graveyard.').
card_ruling('consuming aberration', '2013-01-24', 'The cards will be put into graveyards before the spell you cast to cause the ability to trigger resolves. However, none of those cards will be in a graveyard as you cast that spell, so they can\'t be chosen as a target of that spell.').

card_ruling('consuming vapors', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('consuming vapors', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('consuming vapors', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('consuming vapors', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('consuming vapors', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('consuming vapors', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('consuming vapors', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('consuming vapors', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('consuming vapors', '2010-06-15', 'The sacrificed creature\'s last known existence on the battlefield is checked to determine its toughness.').
card_ruling('consuming vapors', '2010-06-15', 'If the targeted player doesn\'t sacrifice a creature (because the player didn\'t control any creatures or due to Tajuru Preserver, perhaps), you gain no life.').

card_ruling('consuming vortex', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('consuming vortex', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('consuming vortex', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('consuming vortex', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('consumptive goo', '2004-10-04', 'If it targets itself, it gets both -1/-1 and the +1/+1 counter during resolution. Since state-based actions (like being at zero toughness) are not checked until resolution is done, it does not die.').

card_ruling('contagion clasp', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('contagion clasp', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('contagion clasp', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('contagion clasp', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('contagion clasp', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').
card_ruling('contagion clasp', '2011-01-01', 'Contagion Clasp\'s first ability is mandatory. If you\'re the only player who controls a creature, you must target one of them.').

card_ruling('contagion engine', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('contagion engine', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('contagion engine', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('contagion engine', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('contagion engine', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').
card_ruling('contagion engine', '2011-01-01', 'As Contagion Engine\'s activated ability resolves, you\'ll complete an entire proliferate action, then you\'ll complete a second proliferate action. You may choose different players and/or permanents, or different counters on those permanents, when you proliferate the second time.').
card_ruling('contagion engine', '2011-01-01', 'Players can\'t respond between the first and the second proliferate actions.').

card_ruling('contagious nim', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('contagious nim', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('contagious nim', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('contagious nim', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('contagious nim', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('contagious nim', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('containment priest', '2014-11-07', 'Containment Priest’s last ability won’t affect any nontoken creatures that were cast, including ones cast from unusual zones such as your graveyard.').
card_ruling('containment priest', '2014-11-07', 'Containment Priest’s last ability doesn’t stop creature tokens from entering the battlefield. It also doesn’t affect creatures that were already on the battlefield.').
card_ruling('containment priest', '2014-11-07', 'If Containment Priest enters the battlefield without being cast, it won’t exile itself.').
card_ruling('containment priest', '2014-11-07', 'If Containment Priest enters the battlefield at the same time as other creatures, its ability won’t affect those creatures.').

card_ruling('contaminated ground', '2010-06-15', 'The enchanted land loses its existing land types and any abilities printed on it. It now has the land type Swamp and has the ability to tap to add {B} to its controller\'s mana pool. Contaminated Ground doesn\'t change the enchanted land\'s name or whether it\'s legendary, basic, or snow.').
card_ruling('contaminated ground', '2010-06-15', 'Contaminated Ground\'s last ability triggers whenever the enchanted land becomes tapped for any reason, not just when it\'s tapped for mana.').
card_ruling('contaminated ground', '2010-06-15', 'If, while casting a spell or activating an ability, the enchanted land\'s controller taps the land for mana to pay for it, Contaminated Ground\'s ability triggers and goes on the stack on top of that spell or ability. Contaminated Ground\'s ability will resolve first.').
card_ruling('contaminated ground', '2010-06-15', 'On the other hand, the enchanted land\'s controller may tap the land for mana, let Contaminated Ground\'s ability trigger and go on the stack, then spend that mana to cast an instant or activate an ability in response. In that case, that instant or ability will resolve first.').

card_ruling('contamination', '2004-10-04', 'The second ability of this card is a replacement effect, not a triggered ability.').
card_ruling('contamination', '2004-10-04', 'External effects that generate more mana still generate their additional mana as directed without being affected by this card. Effects which generate more mana based on what type of mana the land produces will be affected in color but not in amount.').
card_ruling('contamination', '2004-10-04', 'You choose whether to sacrifice a creature or not on resolution. If not, then you sacrifice Contamination. You can choose to not sacrifice even if you no longer control Contamination on resolution.').

card_ruling('contested cliffs', '2004-10-04', 'If either target is illegal when the ability resolves, it will do nothing.').

card_ruling('contested war zone', '2011-06-01', 'The last activated ability affects all attacking creatures, not just those you control.').
card_ruling('contested war zone', '2011-06-01', 'The word \"you\" in a permanent\'s trigger condition always refers to the permanent\'s current controller.').
card_ruling('contested war zone', '2011-06-01', 'Gaining control of Contested War Zone doesn\'t cause it to tap or untap.').
card_ruling('contested war zone', '2011-06-01', 'In some multiplayer formats, creatures controlled by different players may deal combat damage to you simultaneously. If this happens, you control each triggered ability and can put them on the stack in any order. The last ability to resolve will determine who ends up controlling Contested War Zone.').

card_ruling('contradict', '2015-02-25', 'If the spell becomes an illegal target (because it’s left the stack, for example) when Contradict tries to resolve, Contradict will be countered and none of its effects will happen. You won’t draw a card.').
card_ruling('contradict', '2015-02-25', 'A spell that can’t be countered by spells and abilities is a legal target for Contradict. The spell won’t be countered when Contradict resolves, but you’ll draw a card.').

card_ruling('control magic', '2004-10-04', 'You take control of a creature, but Auras enchanting and Equipment attached to the creature do not change controller.').

card_ruling('controlled instincts', '2009-02-01', 'Controlled Instincts may target and may enchant an untapped creature.').
card_ruling('controlled instincts', '2009-02-01', 'Controlled Instincts can enchant only a creature that\'s red or green. If at any time the enchanted creature is neither red nor green, Controlled Instincts is put into its owner\'s graveyard as a state-based action.').

card_ruling('conundrum sphinx', '2010-08-15', 'First the player whose turn it is names a card, then each other player in turn order does the same. Then each player reveals the top card of his or her library at the same time.').
card_ruling('conundrum sphinx', '2010-08-15', 'If a card a player reveals this way is the card that particular player named, he or she must put it into his or her hand. Otherwise, he or she must put it on the bottom of his or her library, even if it\'s the card a different player named. In no cases can a player leave the revealed card on top of his or her library.').

card_ruling('convalescence', '2004-10-04', 'The life total is checked when it triggers and the ability will be put on the stack only if you have 10 life or less. It checks again on resolution and does nothing if you have more life by then.').

card_ruling('conversion', '2004-10-04', 'The Conversion effect is a continuous effect. There is no chance to tap a just-played mountain for red mana before it becomes a plains.').
card_ruling('conversion', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').

card_ruling('conversion chamber', '2011-06-01', 'If the targeted artifact card is no longer in the graveyard when the first ability tries to resolve, it will be countered and none of its effects will happen. You won\'t put a charge counter on Conversion Chamber.').

card_ruling('convincing mirage', '2009-10-01', 'The affected land loses its existing land types and any abilities printed on it. It becomes the chosen basic land type and now has the ability to tap to add one mana of the appropriate color to your mana pool. Convincing Mirage\'s ability doesn\'t change the affected land\'s name or whether it\'s legendary or basic.').

card_ruling('convulsing licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('convulsing licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('cooperation', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('cooperation', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('cooperation', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('cooperation', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('coordinated barrage', '2008-04-01', 'Note that this spell counts the number of permanents you control of the chosen type, not just the number of creatures you control. It will also take your tribal permanents into account.').
card_ruling('coordinated barrage', '2008-04-01', 'You choose a target for this spell as you cast it, but you don\'t choose a creature type until the spell resolves.').
card_ruling('coordinated barrage', '2008-04-01', 'Spells and abilities that prevent damage or regenerate the targeted creature must be cast or activated before the creature type is chosen.').

card_ruling('copper gnomes', '2004-10-04', 'You choose the artifact card on resolution.').
card_ruling('copper gnomes', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('copperhorn scout', '2011-01-01', 'As Copperhorn Scout\'s ability resolves, you\'ll untap each other creature you control regardless of whether that creature was attacking.').
card_ruling('copperhorn scout', '2011-01-01', 'Untapping an attacking creature doesn\'t cause it to stop attacking.').

card_ruling('copy artifact', '2004-10-04', 'The copy is both an artifact and an enchantment, so it is an artifact-enchantment (perhaps even an artifact-creature-enchantment). It can be affected by anything which affects either type of permanent.').
card_ruling('copy artifact', '2004-10-04', 'The copy of the artifact is not still blue. It copies the color of the thing it is copying.').
card_ruling('copy artifact', '2004-10-04', 'The artifact to copy is chosen at the time this card enters the battlefield. If there is no valid artifact to choose, then this card enters the battlefield as an enchantment that has no effect.').

card_ruling('copy enchantment', '2005-10-01', 'If you choose an Aura, you also choose a legal permanent for the Copy Enchantment copy of it to enchant. Copy Enchantment doesn\'t target that permanent, however, so it can enter the battlefield attached to an untargetable creature.').
card_ruling('copy enchantment', '2005-10-01', 'If you choose an Aura and there isn\'t a legal permanent for it to enchant, you put Copy Enchantment into your graveyard. It never enters the battlefield.').
card_ruling('copy enchantment', '2005-10-01', 'If you don\'t choose an enchantment, Copy Enchantment enters the battlefield without copying anything, and it sits there with a useless ability.').
card_ruling('copy enchantment', '2006-04-01', 'If this is copying a Genju, such as Genju of the Fields, and the land this enchants goes to the graveyard, you will return the Copy Enchantment to your hand.').
card_ruling('copy enchantment', '2006-07-01', 'Copies all copiable values, to include mana cost, so if you copy Dream Leash, for example, the converted mana cost of Copy Enchantment will be 5.').

card_ruling('coral atoll', '2006-02-01', 'This has a triggered ability when it enters the battlefield, not a replacement effect, as previously worded.').

card_ruling('coral fighters', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('coralhelm commander', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('coralhelm commander', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('coralhelm commander', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('coralhelm commander', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('coralhelm commander', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('coralhelm guide', '2015-08-25', 'Once an attacking creature has been legally blocked, making it so it can’t be blocked won’t change or undo that block.').

card_ruling('cornered market', '2004-10-04', 'Cornered Market does not prevent effects which put a card onto the battlefield. It only stops people from casting.').
card_ruling('cornered market', '2004-10-04', 'This card only looks at permanents, not spells on the stack. So it is possible to cast two instances of a spell onto the stack.').

card_ruling('corpse connoisseur', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('corpse connoisseur', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('corpse connoisseur', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('corpse connoisseur', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('corpse connoisseur', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('corpse cur', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('corpse cur', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('corpse cur', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('corpse cur', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('corpse cur', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('corpse cur', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('corpse dance', '2004-10-04', 'Corpse Dance only exiles the card if the card is still on the battlefield at the beginning of the end step.').
card_ruling('corpse dance', '2008-04-01', 'The effect only grants Haste until the end of the current turn. If the creature somehow manages to stick around until a later turn, it will no longer have Haste.').

card_ruling('corpse harvester', '2004-10-04', 'You do not have to find a Zombie card or swamp card if you do not want to, even if you have them in your library.').

card_ruling('corpse traders', '2012-05-01', 'You can sacrifice Corpse Traders to pay the cost of its ability.').
card_ruling('corpse traders', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('corpsejack menace', '2012-10-01', 'If a creature you control would enter the battlefield with a number of +1/+1 counters on it, it enters with twice that many instead.').
card_ruling('corpsejack menace', '2012-10-01', 'If you control two Corpsejack Menaces, the number of +1/+1 counters placed is four times the original number. Three Corpsejack Menaces multiplies the original number by eight, and so on.').

card_ruling('corpseweft', '2015-02-25', 'Because exiling the creature cards from your graveyard is part of the activation cost, no player can respond to stop you from activating the ability by moving creature cards out of your graveyard.').
card_ruling('corpseweft', '2015-02-25', '“Exiled this way” refers to that activation of the ability, not any creature cards that have been previously exiled.').

card_ruling('corpulent corpse', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('corpulent corpse', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('corpulent corpse', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('corpulent corpse', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('corpulent corpse', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('corpulent corpse', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('corpulent corpse', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('corpulent corpse', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('corpulent corpse', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('corpulent corpse', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('corrosion', '2004-10-04', 'It does count rust counters put on artifacts by other Corrosion cards. This means that having more than one of these can result in fast destruction of artifacts.').
card_ruling('corrosion', '2004-10-04', 'In multiplayer games, you can choose a new target player each upkeep.').
card_ruling('corrosion', '2006-02-01', 'Only puts rust counters on artifacts your opponents control, but destroys all artifacts with rust counters on them, even your own. When it leaves the battlefield, it removes all rust counters, not just those that happen to be on artifacts.').

card_ruling('corrosive gale', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('corrosive gale', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('corrosive gale', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('corrosive gale', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('corrosive gale', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('corrupt', '2004-10-04', 'The swamps are counted on resolution.').
card_ruling('corrupt', '2010-08-15', 'The life you gain is equal to the damage dealt by Corrupt, not necessarily to the number of Swamps you control (if some or all of the damage is prevented, for example).').
card_ruling('corrupt', '2010-08-15', 'The amount of damage dealt to a creature is not bounded by its toughness, and the amount of damage dealt to a player is not bounded by that player\'s life total. For example, if Corrupt deals 6 damage to a 2/2 creature, you\'ll gain 6 life.').
card_ruling('corrupt', '2013-07-01', 'Corrupt counts any land you control with the subtype Swamp, not just ones named Swamp.').

card_ruling('corrupt eunuchs', '2009-10-01', 'If there are no other creatures on the battlefield when Corrupt Eunuchs enters the battlefield, it will have to target itself.').

card_ruling('corrupt official', '2004-10-04', 'It triggers only once even if blocked by more than one creature.').

card_ruling('corrupted conscience', '2011-06-01', 'Multiple instances of infect on the same creature are redundant.').

card_ruling('corrupted harvester', '2011-01-01', 'You may sacrifice Corrupted Harvester to pay for its own regeneration ability. If you do, however, it won\'t regenerate. It\'ll just end up in its owner\'s graveyard as a result of the sacrifice.').

card_ruling('corrupted resolve', '2011-06-01', 'A player is poisoned if that player has one or more poison counters.').

card_ruling('corrupted roots', '2009-02-01', 'Corrupted Roots can enchant only a Forest or a Plains. It checks the enchanted land\'s subtypes, not its name. If at any time the enchanted land is neither a Forest nor a Plains (due to Unstable Frontier\'s ability, perhaps), Corrupted Roots is put into its owner\'s graveyard as a state-based action.').
card_ruling('corrupted roots', '2009-02-01', 'Corrupted Roots doesn\'t care why the enchanted land becomes tapped. If that land changes from being untapped to being tapped for any reason, the ability will trigger.').

card_ruling('corrupted zendikon', '2010-03-01', 'The enchanted permanent will be both a land and a creature and can be affected by anything that affects either a land or a creature.').
card_ruling('corrupted zendikon', '2010-03-01', 'When a land becomes a creature, that doesn\'t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('corrupted zendikon', '2010-03-01', 'An attacking or blocking creature that stops being a creature is removed from combat. This can happen if a Zendikon enchanting an attacking or blocking creature leaves the battlefield, for example. The permanent that was removed from combat neither deals nor is dealt combat damage. Any attacking creature that the land creature was blocking remains blocked, however.').
card_ruling('corrupted zendikon', '2010-03-01', 'An ability that turns a land into a creature also sets that creature\'s power and toughness. If the land was already a creature, this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness, such as the effects of Disfigure or Glorious Anthem, will continue to apply, no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('corrupted zendikon', '2010-03-01', 'If a Zendikon and the land it\'s enchanting are destroyed at the same time (due to Akroma\'s Vengeance, for example), the Zendikon\'s last ability will still trigger.').
card_ruling('corrupted zendikon', '2010-03-01', 'If a Zendikon\'s last ability triggers, but the land card it refers to leaves the graveyard before it resolves, it will resolve but do nothing.').

card_ruling('corrupting licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('corrupting licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('cosi\'s ravager', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('cosi\'s ravager', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('cosi\'s trickster', '2009-10-01', 'Cosi\'s Trickster\'s ability triggers when an opponent shuffles his or her library because that player was instructed to do so by a spell or ability that specifically contains the word \"shuffle\" in its text or (in the case of a keyword ability) in its rules.').
card_ruling('cosi\'s trickster', '2009-10-01', 'The cascade ability doesn\'t cause a player to shuffle his or her library, even if that player revealed his or her entire library. The revealed cards are put on the bottom of their owner\'s library in a random order, but they\'re not \"shuffled.\"').
card_ruling('cosi\'s trickster', '2009-10-01', 'If an opponent\'s library is empty or has just a single card in it when a spell or ability instructs that player to shuffle his or her library, Cosi\'s Trickster\'s ability will still trigger.').

card_ruling('cosmic horror', '2013-07-01', 'If you choose not to pay Cosmic Horror\'s upkeep cost, it deals 7 damage to you only if it\'s actually destroyed. If it regenerates or an effect has given it indestructible, it deals no damage.').

card_ruling('council guardian', '2014-05-29', 'The vote happens as the ability resolves. Until this happens, Council Guardian doesn’t have protection from any color. Spells and abilities may target Council Guardian as normal in response to its enters-the-battlefield ability, but not in response to the vote. Once players have started voting, it’s too late to respond.').
card_ruling('council guardian', '2014-05-29', 'If Council Guardian is not on the battlefield when its ability resolves, the vote will still take place; it simply won’t have any effect. However, cards that care about voting (for example, Grudge Keeper) will still see the vote.').
card_ruling('council guardian', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('council guardian', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('council guardian', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('council guardian', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('council guardian', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('council of the absolute', '2013-04-15', 'The last ability can\'t reduce the colored mana requirement of a spell.').
card_ruling('council of the absolute', '2013-04-15', 'If there are additional costs to cast the spell, such as kicker costs, apply those increases before applying cost reductions.').
card_ruling('council of the absolute', '2013-04-15', 'The last ability can reduce alternative costs such as overload costs.').
card_ruling('council of the absolute', '2013-04-15', 'If a spell with the chosen name includes one generic mana in its cost to cast, that cost will be reduced by {1}.').
card_ruling('council of the absolute', '2013-04-15', 'You can name either half of a split card, but not both. If you name half of a split card, your opponents can cast the other half. If that split card has fuse, your opponents can\'t cast that card as a fused split card.').
card_ruling('council of the absolute', '2013-04-15', 'If you name half of a split card with fuse, and then you cast both halves, the cost reduction will apply to the total cost of the spell. This is true even if the half with the chosen name requires only colored mana to cast.').

card_ruling('council\'s judgment', '2014-05-29', 'None of the affected permanents are targeted. Players may vote for a permanent with protection from white, for example.').
card_ruling('council\'s judgment', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('council\'s judgment', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('council\'s judgment', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('council\'s judgment', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('council\'s judgment', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('counterbalance', '2006-07-15', 'If an opponent casts a spell with X in its mana cost, the converted mana cost of that spell takes the value of X into account. If you reveal a card with X in its mana cost, X is 0. For example, if your opponent casts Blaze with X=1, Counterbalance will counter that spell if you reveal a card with converted mana cost 2, but it won\'t counter that spell if you reveal a Blaze of your own.').
card_ruling('counterbalance', '2006-07-15', 'If an opponent casts half of a split card (for example, Hit), and you reveal a split card where half of it has the same converted mana cost (for example, Hit/Run or Stand/Deliver), Counterbalance will counter the spell.').

card_ruling('counterbore', '2008-05-01', 'Counterbore has no effect on cards on the battlefield that have the same name as the targeted spell.').
card_ruling('counterbore', '2008-05-01', 'The cards you\'re searching for must be found if they\'re in the graveyard, because that\'s a zone everyone can see. Finding those cards in the hand and library is optional, though.').
card_ruling('counterbore', '2008-05-01', 'In most cases, Counterbore will exile the targeted spell. Its first sentence counters the spell, which puts it into its owner\'s graveyard. Its second sentence then removes it from the graveyard. However, there are some exceptions to this, as listed below.').
card_ruling('counterbore', '2008-05-01', 'If the spell\'s controller isn\'t the same as its owner (due to Commandeer, for example), it won\'t be exiled. The spell is countered and put into its owner\'s graveyard. Then the spell\'s controller\'s graveyard, hand, and library are searched.').
card_ruling('counterbore', '2008-05-01', 'If the targeted spell can\'t be countered (it\'s Vexing Shusher, for example), that spell will remain on the stack. Counterbore will continue to resolve. You still get to search for and exile all other cards with that name.').

card_ruling('counterflux', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('counterflux', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('counterflux', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('counterflux', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('counterflux', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('counterflux', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('counterflux', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('counterlash', '2011-01-22', 'You cast the card in your hand as part of the resolution of Counterlash. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other restrictions are not (such as \"Cast [this card] only during combat\").').
card_ruling('counterlash', '2011-01-22', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('counterlash', '2011-01-22', 'If the card has {X} in its mana cost, you must choose 0 as its value.').

card_ruling('countermand', '2014-04-26', 'If the target spell is an illegal target when Countermand tries to resolve, Countermand will be countered and none of its effects will happen. The target’s controller won’t put any cards from the top of his or her library into his or her graveyard.').
card_ruling('countermand', '2014-04-26', 'You may target a spell that can’t be countered with Countermand. If you do, when Countermand resolves, the target spell won’t be countered, but its controller will still put the top four cards of his or her library into his or her graveyard.').

card_ruling('countryside crusher', '2008-04-01', 'The first ability will keep putting cards from the top of your library into your graveyard until you reveal a nonland card. That nonland card remains on top of your library.').
card_ruling('countryside crusher', '2008-04-01', 'Each time you put a land card into your graveyard with the first ability, the second ability will trigger. Those triggers wait to be put onto the stack until the first ability has completely finished resolving.').

card_ruling('courier\'s capsule', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('courser of kruphix', '2014-02-01', 'Courser of Kruphix doesn’t change when you can play lands. You can do so only during your main phase when you have priority and the stack is empty.').
card_ruling('courser of kruphix', '2014-02-01', 'Playing a land with the second ability counts as your land play for the turn. If you play a land from your hand during your turn, you won’t be able to play an additional land from the top of your library unless another effect allows you to.').
card_ruling('courser of kruphix', '2014-02-01', 'The last ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad’s ability, for example).').
card_ruling('courser of kruphix', '2014-02-01', 'While playing with the top card of your library revealed, if you draw multiple cards, reveal each one before you draw it.').

card_ruling('coursers\' accord', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('coursers\' accord', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('coursers\' accord', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('coursers\' accord', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('coursers\' accord', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('coursers\' accord', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('court archers', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('court archers', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('court archers', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('court archers', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('court archers', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('court archers', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('court hussar', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('courtly provocateur', '2012-07-01', 'After the first ability resolves, the creature attacks only if it\'s able to do so as the declare attackers step begins. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having the creature attack, the player isn\'t forced to pay that cost, so it doesn\'t attack in that case either.').
card_ruling('courtly provocateur', '2012-07-01', 'The controller of the creature targeted by the first ability chooses which player or planeswalker that creature attacks.').
card_ruling('courtly provocateur', '2012-07-01', 'After the second ability resolves, the creature blocks only if it\'s able to do so as the declare blockers step begins. If, at that time, the creature is tapped, is affected by a spell or ability that says it can\'t block, or no creatures are attacking that player or a planeswalker controlled by that player, then it doesn\'t block. If there\'s a cost associated with having the creature block, the player isn\'t forced to pay that cost, so it doesn\'t block in that case either.').
card_ruling('courtly provocateur', '2012-07-01', 'The controller of the creature targeted by the second ability chooses which attacking creature that creature blocks.').
card_ruling('courtly provocateur', '2012-07-01', 'If there are multiple combat phases in a turn, the creature must attack or block only in the first one in which it\'s able to.').

card_ruling('covenant of blood', '2014-07-18', 'If the target creature or player is an illegal target when Covenant of Blood tries to resolve, it will be countered and none of its effects will happen. You won’t gain any life. (Any creatures you tapped to cast Covenant of Blood remain tapped.)').
card_ruling('covenant of blood', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('covenant of blood', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('covenant of blood', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('covenant of blood', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('covenant of blood', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('covenant of blood', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('covenant of blood', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('covenant of minds', '2008-10-01', 'The targeted opponent has two options: Let you have the three revealed cards or let you have five unknown cards. Note that if the opponent goes with the first option, you put cards into your hand rather than draw them (in case something like Hoofprints of the Stag cares about that), but if the opponent goes with the second option, you actually draw cards.').

card_ruling('cover of winter', '2006-07-15', 'Cover of Winter\'s replacement effect prevents combat damage creatures would deal to you and to attacking or blocking creatures you control.').
card_ruling('cover of winter', '2006-07-15', 'If multiple creatures would deal combat damage to you and your creatures at the same time, this prevents X of the damage that would be dealt by each of them. This prevention is applied creature by creature. For example, if Cover of Winter has two age counters and you\'re being attacked by an unblocked 1/1 and an unblocked 4/4, this will prevent 2 of the combat damage that would be dealt by each one. All damage from the 1/1 is prevented and 2 damage from the 4/4 is prevented, meaning you\'re dealt 2 damage.').
card_ruling('cover of winter', '2006-07-15', 'If a creature has double strike, Cover of Winter prevents X of the combat damage it would deal during each combat damage step.').
card_ruling('cover of winter', '2006-07-15', 'If a creature\'s combat damage is assigned to multiple recipients (for example, if it has trample and is blocked by two creatures, and damage is assigned to each of those creatures and to you), you decide how to divide the prevention effect. You may prevent damage that would be dealt to the first creature, the second creature, and/or you, as long as the total amount of that creature\'s damage that would be prevented is X.').

card_ruling('covetous dragon', '2004-10-04', 'The ability will trigger if you don\'t control an artifact, even for a brief moment during the resolution of another spell or ability.').
card_ruling('covetous dragon', '2004-10-04', 'The ability only checks if you control no artifacts at the time it triggers. It does not check again on resolution. So gaining control of an artifact before then will not save the Dragon.').

card_ruling('cower in fear', '2012-07-01', 'Cower in Fear affects only creatures your opponents control at the time it resolves. It won\'t affect creatures that come under their control later in the turn.').

card_ruling('crab umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('crab umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('crab umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('crab umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('crab umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('crab umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('crab umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('crab umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('crab umbra', '2010-06-15', 'Only Crab Umbra\'s controller (who is not necessarily the enchanted creature\'s controller) can activate its activated ability.').
card_ruling('crab umbra', '2010-06-15', 'When Crab Umbra\'s activated ability resolves, it will untap the creature Crab Umbra is enchanting at that time (regardless of what creature Crab Umbra was enchanting when the ability was activated). If Crab Umbra has left the battlefield by then, the ability will untap the creature it was enchanting at the time it left the battlefield.').
card_ruling('crab umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('crab umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('crab umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('crackleburr', '2008-08-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('crackleburr', '2008-08-01', 'The \"summoning sickness\" rule applies to {Q}. If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability. Ignore this rule if the creature also has haste.').
card_ruling('crackleburr', '2008-08-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('crackleburr', '2008-08-01', 'To activate either ability, you\'ll need Crackleburr plus two other creatures. Crackleburr must have been under your control since your most recent turn began (or have haste), but the other two creatures don\'t.').

card_ruling('crackling doom', '2014-09-20', 'If a player controls two or more creatures tied for the greatest power among creatures he or she controls, that player chooses one of them to sacrifice.').
card_ruling('crackling doom', '2014-09-20', 'The sacrifice is not dependent on the damage being dealt. It doesn’t matter if that damage is prevented or redirected.').
card_ruling('crackling doom', '2014-09-20', 'Crackling Doom doesn’t target any player or creature. For example, a creature with protection from black could be sacrificed.').
card_ruling('crackling doom', '2014-09-20', 'In a multiplayer game, each opponent, starting with the active player and proceeding in turn order, chooses which creature he or she will sacrifice and then all creatures are sacrificed at the same time. Each successive player will know what the players before him or her chose to sacrifice.').

card_ruling('crackling perimeter', '2013-01-24', 'You can\'t tap an untapped Gate you control for mana and tap it to activate Crackling Perimeter\'s ability at the same time. You must choose one or the other.').

card_ruling('cradle of vitality', '2008-10-01', 'You choose a target when the ability triggers. You don\'t choose whether to pay {1}{W} until the ability resolves.').
card_ruling('cradle of vitality', '2008-10-01', 'Cradle of Vitality triggers just once for each life-gaining event, whether it\'s 1 life from Deathgreeter or 8 life from Soul\'s Grace. You pay the cost just once, but the targeted creature will get a number of counters equal to the amount of life you gained.').
card_ruling('cradle of vitality', '2014-02-01', 'If multiple creatures you control with lifelink deal damage simultaneously, each instance is a single life-gain event and the ability will trigger separately for each of them.').

card_ruling('crafty pathmage', '2004-10-04', 'If you increase the power of the targeted creature after the ability resolves, it still can\'t be blocked that turn.').

card_ruling('crag puca', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('crag puca', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('cragganwick cremator', '2008-05-01', 'You target the player when the ability triggers. You won\'t know which card is discarded until the ability resolves.').
card_ruling('cragganwick cremator', '2008-05-01', 'Cragganwick Cremator deals damage equal to the discarded creature\'s power as it exists in the graveyard. (This can matter for certain creatures with variable power, such as Maro or Tarmogoyf.)').
card_ruling('cragganwick cremator', '2008-05-01', 'If you have no cards in hand when Cragganwick Cremator\'s ability resolves, you don\'t discard anything.').

card_ruling('cranial archive', '2014-09-20', 'If the player’s graveyard is empty, the player will shuffle his or her library, then you’ll draw a card.').

card_ruling('crash landing', '2006-02-01', 'Crash Landing can\'t target a creature without flying just to deal damage to it.').

card_ruling('crashing boars', '2004-10-04', 'If the chosen creature is not still untapped at the time blockers are declared, it does not have to block.').
card_ruling('crashing boars', '2004-10-04', 'The ability is not optional. If there are no untapped creatures to choose, then the ability does nothing.').

card_ruling('crater elemental', '2015-02-25', 'Crater Elemental’s last ability overrides all previous effects that set its power to a specific value. Other effects that set its power to a specific value that start to apply after the ability resolves will override this effect.').
card_ruling('crater elemental', '2015-02-25', 'Effects that modify Crater Elemental’s power, such as the effects of Giant Growth or Hall of Triumph, will apply to it no matter when they started to take effect. The same is true for counters that change its power (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('crater elemental', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('crater elemental', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('crater elemental', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('crater hellion', '2004-10-04', 'The ability does 4 damage to every creature on the battlefield other than Crater Hellion.').

card_ruling('crater\'s claws', '2014-09-20', 'If you control a creature with power 4 or greater, Crater’s Claws with X equals 0 will deal 2 damage, with X equals 1 will deal 3 damage, and so on.').
card_ruling('crater\'s claws', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('crater\'s claws', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('craterhoof behemoth', '2012-05-01', 'The bonus is calculated based on the number of creatures you control when Craterhoof Behemoth\'s ability resolves, potentially including itself. If that number increases or decreases later in the turn, the bonus won\'t change.').
card_ruling('craterhoof behemoth', '2012-05-01', 'Only creatures you control when Craterhoof Behemoth\'s ability resolves will get the bonus. Creatures that enter the battlefield or that you gain control of later in the turn will be unaffected.').

card_ruling('crawlspace', '2014-02-01', 'Your opponents can still attack planeswalkers you control with any number of creatures each combat.').

card_ruling('creakwood ghoul', '2008-08-01', 'If the targeted card is removed from the graveyard before the ability resolves, the entire ability is countered. You won\'t gain 1 life.').

card_ruling('creakwood liege', '2008-08-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').
card_ruling('creakwood liege', '2008-08-01', 'Since the token is black and green, it will get +2/+2 from Creakwood Liege as long as the Liege is on the battlefield.').

card_ruling('cream of the crop', '2008-04-01', 'The creature\'s power is checked as this ability resolves. If the creature has left the battlefield, use its last known information.').
card_ruling('cream of the crop', '2008-04-01', 'If the creature has 0 power, this ability has no effect.').
card_ruling('cream of the crop', '2008-04-01', 'If the creature has 1 power, this ability lets you look at the top card of your library, then you\'ll have to leave it there.').

card_ruling('creeperhulk', '2014-11-07', 'The activated ability overwrites all previous effects that set the target creature’s base power and/or toughness to specific values. Other effects that set its base power and/or toughness that start to apply after Creeperhulk’s ability resolves will overwrite Creeperhulk’s effect.').
card_ruling('creeperhulk', '2014-11-07', 'Effects that modify the target creature’s power and/or toughness, such as the effect of Giant Growth or Glorious Anthem, will apply to the creature no matter when they started applying. The same is true for counters that affect the target creature’s power and toughness and effects that switch its power and toughness.').

card_ruling('creeping renaissance', '2011-09-22', 'The permanent types are artifact, creature, enchantment, land, and planeswalker.').

card_ruling('creeping tar pit', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('creeping tar pit', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').

card_ruling('creepy doll', '2011-09-22', 'You don\'t flip the coin until the ability resolves. If you want to respond to the ability, perhaps by regenerating the damaged creature, you\'ll have to do so before you know the outcome of the flip.').
card_ruling('creepy doll', '2011-09-22', 'If the combat damage Creepy Doll deals to a creature is lethal, you\'ll still flip a coin. If the creature is still on the battlefield (perhaps because it regenerated), it could be destroyed a second time, depending on the coin flip.').

card_ruling('cremate', '2012-10-01', 'You must be able to target a card in a graveyard to cast Cremate.').
card_ruling('cremate', '2012-10-01', 'If the target card is no longer in the graveyard when Cremate tries to resolve, it will be countered and none of its effects will happen. You won\'t draw a card.').

card_ruling('crescendo of war', '2011-09-22', 'In a Two-Headed Giant game, the first ability triggers once during each team\'s upkeep.').

card_ruling('crimson kobolds', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('crimson wisps', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('crimson wisps', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('crimson wisps', '2008-05-01', 'Colorless is not a color.').

card_ruling('crippling chill', '2012-05-01', 'Crippling Chill can target a creature that\'s already tapped. It still won\'t untap during its controller\'s next untap step.').
card_ruling('crippling chill', '2012-05-01', 'Crippling Chill tracks the creature, but not its controller. If the creature changes controllers before its first controller\'s next untap step has come around, then it won\'t untap during its new controller\'s next untap step.').
card_ruling('crippling chill', '2014-09-20', 'Crippling Chill can target a creature that’s already tapped. It still won’t untap during its controller’s next untap step.').
card_ruling('crippling chill', '2014-09-20', 'Crippling Chill tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').

card_ruling('crocanura', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('crocanura', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('crocanura', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('crocanura', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('crocanura', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('crocanura', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('crookclaw transmuter', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('crookclaw transmuter', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('crooked scales', '2004-10-04', 'If you pay the {3}, you do not choose a new pair of targets when it repeats. This means that if you lose the flip, you have the choice of having your creature destroyed or paying {3} to try again to win the flip. If you lose again, you get the same choice, and so on. You can keep trying to destroy your opponent\'s creature as long as you have mana to pay.').
card_ruling('crooked scales', '2004-10-04', 'You choose both targets on activation, so you can only activate this when you can legally choose both targets.').

card_ruling('crookshank kobolds', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('crosis\'s catacombs', '2004-10-04', 'You can return a land that is tapped or untapped.').
card_ruling('crosis\'s catacombs', '2004-10-04', 'If you don\'t want to unsummon a land, you can play this card then tap it for mana before the enters the battlefield ability resolves. You may then choose to sacrifice it instead of unsummoning a land.').
card_ruling('crosis\'s catacombs', '2005-08-01', 'This land is of type \"Lair\" only; other subtypes have been removed. It is not a basic land.').

card_ruling('crosis, the purger', '2004-10-04', 'You choose the color during resolution. This means your opponent does not get to react after knowing the color you chose.').

card_ruling('crossbow ambush', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('crovax the cursed', '2004-10-04', 'Crovax can sacrifice itself to its upkeep triggered ability.').
card_ruling('crovax the cursed', '2004-10-04', 'You can only sacrifice one creature to it each turn.').
card_ruling('crovax the cursed', '2004-10-04', 'If you choose not to sacrifice a creature and he has no +1/+1 counters, then nothing bad happens. Note that some other effect must be keeping him alive if he has no counters.').

card_ruling('crovax, ascendant hero', '2007-02-01', 'If Crovax, Ascendant Hero stops being white, it gives itself -1/-1.').

card_ruling('crowd\'s favor', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('crowd\'s favor', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('crowd\'s favor', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('crowd\'s favor', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('crowd\'s favor', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('crowd\'s favor', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('crowd\'s favor', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('crown of convergence', '2005-10-01', 'A colorless creature on top of your library never gives a bonus, and a colorless creature on the battlefield never gets a bonus.').
card_ruling('crown of convergence', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('crown of convergence', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('crown of convergence', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('crown of doom', '2014-11-07', 'You are Crown of Doom’s owner if it started the game in your deck. Therefore, once you hand off the Crown, your opponents cannot give it back to you by activating its ability (although other effects, such as the one Zedruu the Greathearted’s ability creates, may do this).').

card_ruling('crown of empires', '2011-09-22', 'Whether or not you control the correct artifacts is determined when the ability resolves.').
card_ruling('crown of empires', '2011-09-22', 'If any of the named cards stops being an artifact, it won\'t be considered by these abilities.').
card_ruling('crown of empires', '2011-09-22', 'If you control artifacts named Scepter of Empires and Throne of Empires, the targeted creature won\'t be tapped. You\'ll just gain control of it.').

card_ruling('crown of the ages', '2004-10-04', 'This only targets the Aura and not either creature. This means it can move Auras onto a creature which can\'t normally be targeted by spells and abilities if the Aura is legal on that creature.').

card_ruling('crucible of the spirit dragon', '2014-11-24', 'You can use mana generated by the last ability to pay an alternative cost (such as a dash cost) or an additional cost to cast a Dragon spell. It’s not limited to paying just that spell’s mana cost.').
card_ruling('crucible of the spirit dragon', '2014-11-24', 'An activated ability appears in the form “Cost: Effect.”').
card_ruling('crucible of the spirit dragon', '2014-11-24', 'Notably, turning a face-down creature face up isn’t an activated ability. If you manifest a Dragon creature card or cast a Dragon creature card face down using the morph ability, you can’t use mana generated by the last ability to turn that card face up.').
card_ruling('crucible of the spirit dragon', '2014-11-24', 'The mana generated by the last ability can’t be spent to activate abilities of Dragon sources that aren’t on the battlefield.').

card_ruling('crucible of worlds', '2004-12-01', 'Crucible of Worlds allows you to play land cards from your graveyard as well as from your hand.').
card_ruling('crucible of worlds', '2004-12-01', 'Crucible of Worlds doesn\'t change the times when you can play those land cards. You can still play only one land per turn, and only during your main phase when you have priority and the stack is empty.').
card_ruling('crucible of worlds', '2004-12-01', 'Crucible of Worlds doesn\'t allow you to activate activated abilities (such as cycling) of land cards in your graveyard.').

card_ruling('crude rampart', '2004-10-04', 'If it attacks while face down and is turned face up during combat, this will not remove it from combat.').

card_ruling('cruel feeding', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('cruel feeding', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('cruel feeding', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('cruel feeding', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('cruel feeding', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('cruel sadist', '2014-07-18', 'You can remove any +1/+1 counters from Cruel Sadist to activate its second ability, not just those put on Cruel Sadist by its first ability.').

card_ruling('cruel ultimatum', '2008-10-01', 'Cruel Ultimatum\'s only target is an opponent. You don\'t choose which creature card in your graveyard you\'ll return to your hand until Cruel Ultimatum resolves.').
card_ruling('cruel ultimatum', '2008-10-01', 'All of these actions are performed sequentially, in the order listed. Earlier actions may affect how you perform later actions. (For example, if the opponent sacrifices a creature that he or she controls but you own, it will end up in your graveyard. When Cruel Ultimatum lets you return a creature card from your graveyard to your hand, you can choose that one.)').
card_ruling('cruel ultimatum', '2008-10-01', 'If, as Cruel Ultimatum begins to resolve, your opponent\'s life total is 5 or less and you have two or fewer cards in your library, the game will result in a draw. Your opponent\'s life total will drop to 0 or less, but Cruel Ultimatum must finish resolving completely before state-based actions are checked. You\'ll then be forced to draw three cards and fail to draw one. When state-based actions are finally checked, you and your opponent will both lose the game at the same time, which means the game is a draw.').

card_ruling('crumble', '2004-10-04', 'If the target artifact becomes illegal before resolution, the player does not gain any life.').

card_ruling('crumble to dust', '2015-08-25', 'You may leave any cards with that name in the player’s graveyard, hand, or library. You don’t have to exile them.').
card_ruling('crumble to dust', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('crumble to dust', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('crumble to dust', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('crumble to dust', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('crumble to dust', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('crumbling ashes', '2008-08-01', 'This ability is mandatory. If you\'re the only player who controls a creature with a -1/-1 counter on it as your upkeep begins, you must target one of your own creatures.').
card_ruling('crumbling ashes', '2008-08-01', 'You can\'t target a creature unless it already has a -1/-1 counter on it as your upkeep begins. Putting a -1/-1 counter on a creature during your upkeep won\'t let Crumbling Ashes destroy it that turn.').
card_ruling('crumbling ashes', '2008-08-01', 'If the targeted creature loses all its -1/-1 counters by the time the ability resolves, the ability will be countered because its only target is illegal. The creature won\'t be destroyed.').

card_ruling('crumbling colossus', '2011-09-22', 'If another player controls Crumbling Colossus when its ability tries to resolve, you\'ll be unable to sacrifice it. It will simply remain on the battlefield.').

card_ruling('crumbling sanctuary', '2004-10-04', 'If you have no cards in your library, then damage done to you will have no effect since it will get changed into a removal that can\'t happen.').
card_ruling('crumbling sanctuary', '2004-10-04', 'The ability affects all players.').

card_ruling('crusader of odric', '2012-07-01', 'The ability that defines Crusader of Odric\'s power and toughness works in all zones, not just the battlefield.').
card_ruling('crusader of odric', '2012-07-01', 'As long as Crusader of Odric is on the battlefield, its ability will count itself.').

card_ruling('crusading knight', '2004-10-04', 'It counts all opponents\' swamps.').

card_ruling('crush', '2011-06-01', 'If the targeted artifact is also a creature when Crush tries to resolve, it will be countered.').

card_ruling('crush underfoot', '2013-06-07', 'Crush Underfoot doesn\'t target the Giant creature you control. You choose that Giant as Crush Underfoot resolves. If you don\'t control a Giant creature at that time, the spell will finish resolving with no effect.').

card_ruling('crusher zendikon', '2010-03-01', 'The enchanted permanent will be both a land and a creature and can be affected by anything that affects either a land or a creature.').
card_ruling('crusher zendikon', '2010-03-01', 'When a land becomes a creature, that doesn\'t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('crusher zendikon', '2010-03-01', 'An attacking or blocking creature that stops being a creature is removed from combat. This can happen if a Zendikon enchanting an attacking or blocking creature leaves the battlefield, for example. The permanent that was removed from combat neither deals nor is dealt combat damage. Any attacking creature that the land creature was blocking remains blocked, however.').
card_ruling('crusher zendikon', '2010-03-01', 'An ability that turns a land into a creature also sets that creature\'s power and toughness. If the land was already a creature, this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness, such as the effects of Disfigure or Glorious Anthem, will continue to apply, no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('crusher zendikon', '2010-03-01', 'If a Zendikon and the land it\'s enchanting are destroyed at the same time (due to Akroma\'s Vengeance, for example), the Zendikon\'s last ability will still trigger.').
card_ruling('crusher zendikon', '2010-03-01', 'If a Zendikon\'s last ability triggers, but the land card it refers to leaves the graveyard before it resolves, it will resolve but do nothing.').

card_ruling('crypsis', '2014-02-01', 'Abilities of creature cards not on the battlefield owned by an opponent can’t target that creature, and damage those cards would deal to the creature is prevented.').
card_ruling('crypsis', '2014-02-01', 'If another player gains control of the creature after Crypsis resolves, the creature will have protection from creatures controlled by opponents of its new controller.').

card_ruling('crypt champion', '2006-05-01', 'The first triggered ability isn\'t targeted, so the creature cards aren\'t chosen until the ability resolves. Each player chooses which creature card from his or her own graveyard to return. If a player doesn\'t have an applicable creature card, that player does nothing. If a player does have any applicable cards, that player must return one to the battlefield.').
card_ruling('crypt champion', '2006-05-01', 'If this enters the battlefield in a way other than announcing it as a spell, then the appropriate mana can\'t have been paid, and you\'ll have to sacrifice it.').

card_ruling('crypt cobra', '2004-10-04', 'The player gets a poison counter as a triggered ability on not blocking the Cobra. Killing the Cobra after declaring blockers or preventing its damage will not prevent the poison counter.').
card_ruling('crypt cobra', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('crypt creeper', '2012-05-01', 'You must be able to target a card in a graveyard to activate Crypt Creeper\'s ability. It can\'t target itself.').

card_ruling('crypt ghast', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('crypt ghast', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('crypt ghast', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('crypt sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('crypt sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('cryptborn horror', '2012-10-01', 'Remember, damage dealt to a player causes that player to lose that much life (unless the source has infect).').
card_ruling('cryptborn horror', '2012-10-01', 'If no opponent has lost life on the turn Cryptborn Horror enters the battlefield, it will enter with no +1/+1 counters on it and be put into its owner\'s graveyard (unless something else is raising its toughness).').
card_ruling('cryptborn horror', '2012-10-01', 'Cryptborn Horror cares about the total life lost, not necessarily what an opponent\'s life total is compared to what it was at the beginning of the turn. For example, if an opponent loses 5 life and then gains 10 life on the turn Cryptborn Horror enters the battlefield, it will enter with five +1/+1 counters on it.').
card_ruling('cryptborn horror', '2012-10-01', 'In multiplayer games, life lost the turn Cryptborn Horror enters the battlefield by opponents who left the game later that turn will count toward the value of X.').

card_ruling('cryptic annelid', '2007-05-01', 'You do the three different scry actions in sequence, in the order they\'re printed on the card.').

card_ruling('cryptic command', '2013-06-07', 'You choose both modes as you cast Cryptic Command. You must choose two different modes.').
card_ruling('cryptic command', '2013-06-07', 'Look at both chosen modes to determine how many targets Cryptic Command has, if any. If it has at least one target, and all its targets are illegal when it tries to resolve, then it will be countered and none of its effects will happen. For example, if you choose the second and fourth modes, and the permanent is an illegal target when Cryptic Command tries to resolve, you won\'t draw a card.').

card_ruling('cryptic cruiser', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('cryptic cruiser', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('cryptic cruiser', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('cryptic cruiser', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('cryptic cruiser', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('cryptic cruiser', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('cryptic cruiser', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('cryptic cruiser', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('cryptic cruiser', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('cryptic gateway', '2004-10-04', 'For this to be useful, either both creatures have to have a common creature type that the one in your hand also has, or the card in your hand needs to have at least two creature types so each tapped creature can match a different one.').

card_ruling('cryptoplasm', '2011-06-01', 'You choose the target for the triggered ability when the ability is put onto the stack. You choose whether or not Cryptoplasm becomes a copy of that creature when the ability resolves.').
card_ruling('cryptoplasm', '2011-06-01', 'The copy effect lasts indefinitely. Often, it will last until it is overwritten by another copy effect (if it copies another creature on a future turn, perhaps.)').
card_ruling('cryptoplasm', '2011-06-01', 'If the creature is an illegal target when the ability tries to resolve, it will be countered. Cryptoplasm won\'t become a copy of that creature; it remains whatever it was before.').
card_ruling('cryptoplasm', '2011-06-01', 'If another creature becomes a copy of Cryptoplasm, it will become a copy of whatever Cryptoplasm is currently copying (if anything), plus it will have Cryptoplasm\'s triggered ability.').

card_ruling('crystal shard', '2004-10-04', 'You can pay either of the two costs (but not both at the same time) to activate the ability.').

card_ruling('crystal spray', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('crystal spray', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('crystal spray', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('crystal spray', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('crystal spray', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('crystal spray', '2004-10-04', 'You can\'t change proper nouns (i.e. card names) such as \"Island Fish Jasconius\".').
card_ruling('crystal spray', '2004-10-04', 'Changing the text of a spell will not allow you to change the targets of the spell because the targets were chosen when the spell was cast. The text change will (probably) cause it to be countered since the targets will be illegal.').
card_ruling('crystal spray', '2004-10-04', 'If you change the text of a spell which is to become a permanent, the permanent will retain the text change until the effect wears off.').
card_ruling('crystal spray', '2004-10-04', 'It can be used to change a land\'s type from one basic land type to another. For example, Forest can be changed to Island so it produces blue mana. It doesn\'t change the name of any permanent.').

card_ruling('crystalline nautilus', '2014-04-26', 'Crystalline Nautilus’s middle ability functions as long as Crystalline Nautilus is on the battlefield, whether it’s a creature or an Aura.').
card_ruling('crystalline nautilus', '2014-04-26', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('crystalline nautilus', '2014-04-26', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('crystalline nautilus', '2014-04-26', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('crystalline nautilus', '2014-04-26', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('crystalline nautilus', '2014-04-26', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('crystalline nautilus', '2014-04-26', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('crystalline nautilus', '2014-04-26', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('crystalline nautilus', '2014-04-26', 'You still control the Aura, even if it’s enchanting a creature controlled by another player.').
card_ruling('crystalline nautilus', '2014-04-26', 'If the enchanted creature leaves the battlefield, the Aura stops being an Aura and remains on the battlefield. Control of that permanent doesn’t change; you’ll control the resulting enchantment creature.').
card_ruling('crystalline nautilus', '2014-04-26', 'Similarly, if you cast an Aura spell with bestow targeting a creature controlled by another player, and that creature is an illegal target when the spell tries to resolve, it will finish resolving as an enchantment creature spell. It will enter the battlefield under your control.').

card_ruling('crystalline sliver', '2004-10-04', 'The ability only applies while this card is on the battlefield.').
card_ruling('crystalline sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('crystalline sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('crystallization', '2009-05-01', 'When the enchanted creature becomes the target of a spell or ability, Crystallization\'s last ability will trigger. When that ability resolves, that creature will be exiled, even if Crystallization has left the battlefield or is enchanting a different creature by that time. If Crystallization is still enchanting the same creature, Crystallization will then be put into its owner\'s graveyard as a state-based action.').

card_ruling('cudgel troll', '2010-08-15', 'Activating Cudgel Troll\'s ability causes a \"regeneration shield\" to be created for it. The next time Cudgel Troll would be destroyed that turn, the regeneration shield is used up instead. This works only if Cudgel Troll is dealt lethal damage, dealt damage from a source with deathtouch, or affected by a spell or ability that says to \"destroy\" it. Other effects that cause Cudgel Troll to be put into the graveyard (such as reducing its toughness to 0 or sacrificing it) don\'t destroy it, so regeneration won\'t save it. If it hasn\'t been used, the regeneration shield goes away as the turn ends.').
card_ruling('cudgel troll', '2010-08-15', 'To work, the regeneration shield must be created before Cudgel Troll is destroyed. This usually means activating its ability during the declare blockers step, or in response to a spell or ability that would destroy it.').
card_ruling('cudgel troll', '2010-08-15', 'If Cudgel Troll is dealt lethal damage and is dealt damage by a source with deathtouch during the same combat damage step, a single regeneration shield will save it.').

card_ruling('culling dais', '2011-01-01', 'As Culling Dais\'s last ability resolves, its last existence on the battlefield is checked to determine how many charge counters were on it.').

card_ruling('culling drone', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('culling drone', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('culling drone', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('culling drone', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('culling drone', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('culling drone', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('culling drone', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('culling mark', '2014-02-01', 'The creature blocks only if it’s able to do so as the declare blockers step begins. If, at that time, the creature is tapped, it’s affected by a spell or ability that says it can’t block, or no creatures are attacking its controller or a planeswalker controlled by that player, then it doesn’t block. If there’s a cost associated with having the creature block, the player isn’t forced to pay that cost. If that cost isn’t paid, the creature won’t block.').
card_ruling('culling mark', '2014-02-01', 'The controller of the creature chooses which attacking creature that creature blocks.').
card_ruling('culling mark', '2014-02-01', 'If there are multiple combat phases in a turn, the creature must block only in the first one in which it’s able to.').

card_ruling('culling scales', '2004-12-01', 'You choose the target. If there\'s more than one nonland permanent tied for lowest converted mana cost, you choose which one to target.').
card_ruling('culling scales', '2004-12-01', 'If the targeted permanent doesn\'t have the lowest converted mana cost when the ability resolves, the ability is countered and the permanent isn\'t destroyed.').
card_ruling('culling scales', '2004-12-01', 'Most tokens have a converted mana cost of 0. A token that\'s a copy of another permanent or card has a converted mana cost equal to that permanent or card\'s converted mana cost.').
card_ruling('culling scales', '2004-12-01', 'If the lowest converted mana cost is 3, Culling Scales can destroy itself.').

card_ruling('culling the weak', '2004-10-04', 'You sacrifice a creature when casting the spell and you can\'t sacrifice more than one creature to get extra mana.').
card_ruling('culling the weak', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('culling the weak', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('cultbrand cinder', '2008-05-01', 'If there are no other creatures on the battlefield when Cultbrand Cinder enters the battlefield, it must target itself.').

card_ruling('cultivate', '2010-08-15', 'If you choose to find only one basic land card, you put it onto the battlefield tapped.').

card_ruling('cunning advisor', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('cunning lethemancer', '2008-10-01', 'First you choose a card to discard, then each other player in turn order chooses a card to discard, then all those cards are discarded simultaneously. No one sees what the other players are discarding before deciding which cards to discard.').

card_ruling('cunning strike', '2014-11-24', 'You can’t cast Cunning Strike unless you target both a creature and a player. If one target (but not both) is illegal as Cunning Strike resolves, it deals damage to the remaining legal target and you draw a card. If both targets are illegal, the spell will be countered and none of its effects will happen. You won’t draw a card in that case.').

card_ruling('cunning wish', '2004-10-04', 'Can\'t acquire the Ante cards. They are considered still \"in the game\" as are cards in the library and the graveyard.').
card_ruling('cunning wish', '2007-05-01', 'The choice is optional.').
card_ruling('cunning wish', '2007-05-01', 'If you fail to find something, you still exile this.').
card_ruling('cunning wish', '2007-09-16', 'Can\'t acquire cards that are phased out.').
card_ruling('cunning wish', '2009-10-01', 'You can\'t acquire exiled cards because those cards are still in one of the game\'s zones.').
card_ruling('cunning wish', '2009-10-01', 'In a sanctioned event, a card that\'s \"outside the game\" is one that\'s in your sideboard. In an unsanctioned event, you may choose any card from your collection.').

card_ruling('cuombajj witches', '2004-10-04', 'In multiplayer games you can choose a different opposing player each time it is used. You also don\'t have to choose the same player that you targeted with the effect (or whose creature you targeted).').
card_ruling('cuombajj witches', '2004-10-04', 'If either target becomes invalid, the other one is still affected.').
card_ruling('cuombajj witches', '2004-10-04', 'Both targets are chosen on activation, but you choose your target before the opponent chooses.').
card_ruling('cuombajj witches', '2005-10-01', 'The two targets can be the same.').

card_ruling('curiosity', '2007-02-01', 'You draw one card each time the enchanted creature damages the opponent. This is not one card per point of damage.').
card_ruling('curiosity', '2007-02-01', 'If put on your opponent\'s creature, you do not draw a card when that creature damages you. The creature has to damage your opponent in order to have this work.').
card_ruling('curiosity', '2007-02-01', 'Drawing a card is optional. If you forget, you can\'t go back later and do it, even if it is something you normally do.').
card_ruling('curiosity', '2011-09-22', '\"You\" refers to the controller of Curiosity, which may be different from the controller of the enchanted creature.\"An opponent\" refers to an opponent of Curiosity\'s controller.').
card_ruling('curiosity', '2011-09-22', 'Any damage dealt by the enchanted creature to an opponent will cause Curiosity to trigger, not just combat damage.').
card_ruling('curiosity', '2011-09-22', 'Curiosity doesn\'t trigger if the enchanted creature deals damage to a planeswalker controlled by an opponent.').

card_ruling('curse of bloodletting', '2011-01-22', 'Curse of Bloodletting works with any damage, not just combat damage. It also doesn\'t matter who controls the source of the damage that\'s being dealt.').
card_ruling('curse of bloodletting', '2011-01-22', 'The source of the damage doesn\'t change. A spell that deals damage will specify the source of the damage, often the spell itself. An ability that deals damage will also specify the source of the damage, although the ability itself will never be that source. Often the source of the ability is also the source of the damage.').
card_ruling('curse of bloodletting', '2011-01-22', 'If more than one Curse of Bloodletting enchants the same player, damage dealt to that player will double for each one (two of them will end up multiplying the damage by four, three of them by eight, and four of them by sixteen).').
card_ruling('curse of bloodletting', '2011-01-22', 'If multiple effects modify how damage will be dealt to the enchanted player, that player chooses the order to apply the effects. For example, Mending Hands says, \"Prevent the next 4 damage that would be dealt to target creature or player this turn.\" Suppose a spell would deal 5 damage to enchanted player and that player has cast Mending Hands targeting him or herself. The enchanted player can either (a) prevent 4 damage first and then let Curse of Bloodletting\'s effect double the remaining 1 damage, taking 2 damage, or (b) double the damage to 10 and then prevent 4 damage, taking 6 damage.').
card_ruling('curse of bloodletting', '2011-01-22', 'If the enchanted player controls a planeswalker and noncombat damage is being dealt to the player from a source controlled by an opponent, the enchanted player will choose whether to apply Curse of Bloodletting or the planeswalker redirection effect first. If the player chooses to apply the planeswalker redirection effect first, and the opponent chooses to redirect the damage to the planeswalker, then Curse of Bloodletting won\'t double the damage.').

card_ruling('curse of chaos', '2013-10-17', 'A Curse spell targets the player it will enchant like any other Aura spell, and a Curse stays on the battlefield like any other Aura. If the enchanted player gains protection from the Curse’s color (or any other characteristic the Curse has), the Curse will be put into its owner’s graveyard.').
card_ruling('curse of chaos', '2013-10-17', 'Each of the Curses can be attached to any player, including the player who cast the Curse.').
card_ruling('curse of chaos', '2013-10-17', 'Curse is an enchantment type, not a creature type (or any other kind of subtype).').
card_ruling('curse of chaos', '2013-10-17', 'The ability won’t trigger when a creature attacks a planeswalker controlled by the enchanted player.').
card_ruling('curse of chaos', '2013-10-17', 'The Curse subtype has no inherent rules meaning, but two cards in the Innistrad set (Bitterheart Witch and Witchbane Orb) refer to Curses.').
card_ruling('curse of chaos', '2013-10-17', 'Curse of Chaos’s ability will trigger only once per attacking player per combat phase, no matter how many creatures that player attacks with.').

card_ruling('curse of echoes', '2011-01-22', 'When the triggered ability resolves, it creates a copy of that spell for each other player. The active player creates his or her copy and may choose new targets first (if that player isn\'t the enchanted player), then each other player (except the enchanted player) creates his or her copy and may choose new targets in turn order. The last player who creates his or her copy controls the copy that will resolve first.').
card_ruling('curse of echoes', '2011-01-22', 'All the copies are created on the stack, so none of them are cast. Abilities that trigger when a player casts a spell won\'t trigger. The copies will then resolve as normal, after players get a chance to cast spells and activate abilities.').
card_ruling('curse of echoes', '2011-01-22', 'Each copy will have the same targets as the spell it\'s copying unless its controller chooses new ones. That player may change any number of the targets, including all of them or none of them. If, for one of the targets, that player can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('curse of echoes', '2011-01-22', 'If the original spell is modal (that is, it says \"Choose one --\" or the like), each copy will have the same mode(s). You can\'t choose different ones.').
card_ruling('curse of echoes', '2011-01-22', 'If the original spell has an X whose value was determined as it was cast, each copy has the same value of X.').
card_ruling('curse of echoes', '2011-01-22', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if the enchanted player sacrifices a 3/3 creature to cast Fling, each copy of Fling will also deal 3 damage.').

card_ruling('curse of exhaustion', '2011-01-22', 'Curse of Exhaustion will look at the entire turn to see if the enchanted player has cast a spell yet that turn, even if Curse of Exhaustion wasn\'t on the battlefield when that spell was cast.').

card_ruling('curse of inertia', '2013-10-17', 'A Curse spell targets the player it will enchant like any other Aura spell, and a Curse stays on the battlefield like any other Aura. If the enchanted player gains protection from the Curse’s color (or any other characteristic the Curse has), the Curse will be put into its owner’s graveyard.').
card_ruling('curse of inertia', '2013-10-17', 'Each of the Curses can be attached to any player, including the player who cast the Curse.').
card_ruling('curse of inertia', '2013-10-17', 'Curse is an enchantment type, not a creature type (or any other kind of subtype).').
card_ruling('curse of inertia', '2013-10-17', 'The ability won’t trigger when a creature attacks a planeswalker controlled by the enchanted player.').
card_ruling('curse of inertia', '2013-10-17', 'The Curse subtype has no inherent rules meaning, but two cards in the Innistrad set (Bitterheart Witch and Witchbane Orb) refer to Curses.').
card_ruling('curse of inertia', '2013-10-17', 'Curse of Inertia’s ability will trigger only once per attacking player per combat phase, no matter how many creatures that player attacks with.').
card_ruling('curse of inertia', '2013-10-17', 'Even though the attacking player chooses the target of the ability, the ability is still controlled by the player who controls Curse of Inertia. Notably, a creature with hexproof controlled by the player who controls Curse of Inertia could be chosen, even though another player may be choosing the target.').

card_ruling('curse of misfortunes', '2011-01-22', 'The Curse card you put onto the battlefield must have a different name than each other Curse enchanting the enchanted player when the triggered ability resolves. If multiple of these abilities trigger, the Curse card you put onto the battlefield when the first resolves may impact your choices for each subsequent one.').

card_ruling('curse of oblivion', '2011-09-22', 'If the enchanted player has only one card in his or her graveyard, he or she exiles that card.').

card_ruling('curse of predation', '2013-10-17', 'A Curse spell targets the player it will enchant like any other Aura spell, and a Curse stays on the battlefield like any other Aura. If the enchanted player gains protection from the Curse’s color (or any other characteristic the Curse has), the Curse will be put into its owner’s graveyard.').
card_ruling('curse of predation', '2013-10-17', 'Each of the Curses can be attached to any player, including the player who cast the Curse.').
card_ruling('curse of predation', '2013-10-17', 'Curse is an enchantment type, not a creature type (or any other kind of subtype).').
card_ruling('curse of predation', '2013-10-17', 'The ability won’t trigger when a creature attacks a planeswalker controlled by the enchanted player.').
card_ruling('curse of predation', '2013-10-17', 'The Curse subtype has no inherent rules meaning, but two cards in the Innistrad set (Bitterheart Witch and Witchbane Orb) refer to Curses.').

card_ruling('curse of shallow graves', '2013-10-17', 'A Curse spell targets the player it will enchant like any other Aura spell, and a Curse stays on the battlefield like any other Aura. If the enchanted player gains protection from the Curse’s color (or any other characteristic the Curse has), the Curse will be put into its owner’s graveyard.').
card_ruling('curse of shallow graves', '2013-10-17', 'Each of the Curses can be attached to any player, including the player who cast the Curse.').
card_ruling('curse of shallow graves', '2013-10-17', 'Curse is an enchantment type, not a creature type (or any other kind of subtype).').
card_ruling('curse of shallow graves', '2013-10-17', 'The ability won’t trigger when a creature attacks a planeswalker controlled by the enchanted player.').
card_ruling('curse of shallow graves', '2013-10-17', 'The Curse subtype has no inherent rules meaning, but two cards in the Innistrad set (Bitterheart Witch and Witchbane Orb) refer to Curses.').

card_ruling('curse of stalked prey', '2011-09-22', 'The ability will trigger when any creature deals combat damage to the enchanted player, including one controlled by another opponent or even by the enchanted player (if combat damage gets redirected somehow).').

card_ruling('curse of the bloody tome', '2011-09-22', 'If the enchanted player has only one card in his or her library, he or she puts that card into his or her graveyard.').

card_ruling('curse of the cabal', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('curse of the cabal', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('curse of the cabal', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('curse of the cabal', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('curse of the cabal', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('curse of the cabal', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('curse of the cabal', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('curse of the cabal', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('curse of the cabal', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('curse of the cabal', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('curse of the forsaken', '2013-10-17', 'A Curse spell targets the player it will enchant like any other Aura spell, and a Curse stays on the battlefield like any other Aura. If the enchanted player gains protection from the Curse’s color (or any other characteristic the Curse has), the Curse will be put into its owner’s graveyard.').
card_ruling('curse of the forsaken', '2013-10-17', 'Each of the Curses can be attached to any player, including the player who cast the Curse.').
card_ruling('curse of the forsaken', '2013-10-17', 'Curse is an enchantment type, not a creature type (or any other kind of subtype).').
card_ruling('curse of the forsaken', '2013-10-17', 'The ability won’t trigger when a creature attacks a planeswalker controlled by the enchanted player.').
card_ruling('curse of the forsaken', '2013-10-17', 'The Curse subtype has no inherent rules meaning, but two cards in the Innistrad set (Bitterheart Witch and Witchbane Orb) refer to Curses.').

card_ruling('curse of the nightly hunt', '2011-09-22', 'The enchanted player still chooses which player or planeswalker each creature he or she controls attacks.').
card_ruling('curse of the nightly hunt', '2011-09-22', 'If there are multiple combat phases in a turn, each creature must attack only in the first one in which it\'s able to.').
card_ruling('curse of the nightly hunt', '2011-09-22', 'If, during the enchanted player\'s declare attackers step, a creature he or she controls is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having the creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('curse of the swine', '2013-09-15', 'In a Commander game, if a commander is put into the command zone instead of being exiled by Curse of the Swine, its controller will still get a Boar token.').

card_ruling('curse of thirst', '2011-01-22', 'The number of Curses attached to the player is counted when the ability resolves.').

card_ruling('curse of wizardry', '2010-06-15', 'Curse of Wizardry affects all players, including you.').
card_ruling('curse of wizardry', '2010-06-15', 'If a player casts a spell of the chosen color, Curse of Wizardry\'s second ability triggers and goes on the stack on top of it. The ability will resolve before the spell does.').

card_ruling('cursebreak', '2012-05-01', 'If the enchantment is an illegal target when Cursebreak tries to resolve, it\'ll be countered and none of its effects will happen. You won\'t gain 2 life.').

card_ruling('cursed rack', '2004-10-04', 'The effect of lowering the maximum hand size makes it so the player discards down to 4 cards (instead of the usual 7) during the cleanup step.').
card_ruling('cursed rack', '2004-10-04', 'You choose one opposing player just as this card is entering the battlefield and it only affects that one player. This choice is not changed even if this card changes controllers. It becomes useless but stays on the battlefield if the chosen player leaves the game.').
card_ruling('cursed rack', '2009-10-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then a player puts Cursed Rack onto the battlefield choosing you, your maximum hand size will be four. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('cursed scroll', '2008-08-01', 'If you have no cards in hand, you still have to name a card. Then you fail to reveal one (since there aren\'t any). Since the named card wasn\'t revealed, no damage is dealt.').

card_ruling('cursed totem', '2004-10-04', 'Cursed Totem does not prevent creature cards which are not on the battlefield from having their abilities activated.').
card_ruling('cursed totem', '2004-10-04', 'Cursed Totem can prevent mana abilities from being activated.').
card_ruling('cursed totem', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('curtain of light', '2005-06-01', 'An attacking unblocked creature exists from the time the defending player chooses not to block that creature until the combat phase ends. Curtain of Light doesn\'t have any legal targets outside of the combat phase.').
card_ruling('curtain of light', '2005-06-01', 'Curtain of Light does trigger bushido abilities.').
card_ruling('curtain of light', '2005-06-01', 'Curtain of Light isn\'t particularly effective against creatures with trample. Since the creature with trample has no blocker to assign damage to, all the damage will be assigned to the defending player.').
card_ruling('curtain of light', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s after the beginning of that phase\'s Declare Blockers Step.').

card_ruling('custodi squire', '2014-05-29', 'If Custodi Squire is not on the battlefield when its ability resolves, the vote will still take place and the card(s) that had the most votes will still be returned to your hand.').
card_ruling('custodi squire', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('custodi squire', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('custodi squire', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('custodi squire', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('custodi squire', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('cycle of life', '2004-10-04', 'This does not work on creatures which are put onto the battlefield by an effect instead of by being cast.').

card_ruling('cyclical evolution', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('cyclical evolution', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('cyclical evolution', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('cyclical evolution', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('cyclical evolution', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('cyclical evolution', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('cyclical evolution', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('cyclical evolution', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('cyclical evolution', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('cyclical evolution', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('cyclonic rift', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('cyclonic rift', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('cyclonic rift', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('cyclonic rift', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('cyclonic rift', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('cyclonic rift', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('cyclonic rift', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('cyclopean giant', '2006-09-25', 'If the triggered ability is countered (the targeted land becomes an illegal target, for example), Cyclopean Giant remains in the graveyard.').
card_ruling('cyclopean giant', '2006-09-25', 'If a Vesuvan Shapeshifter that\'s copying Cyclopean Giant is put into a graveyard, the Shapeshifter will be exiled when the ability resolves even though it\'s not a Cyclopean Giant anymore.').

card_ruling('cyclopean mummy', '2004-10-04', 'The Mummy does go to the graveyard and trigger abilities which watch for something going to the graveyard. It then gets exiled as a triggered ability.').
card_ruling('cyclopean mummy', '2004-10-04', 'It does not get exiled if it goes to the graveyard from someplace other than the battlefield. This includes discarding from your hand or if the spell is countered.').

card_ruling('cyclopean tomb', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').
card_ruling('cyclopean tomb', '2008-08-01', 'After leaving the battlefield, the ability triggers during each of your upkeeps for the rest of the game. As it resolves, you must remove a mire counter from a land that had a mire counter put on it by that instance of Cyclopean Tomb, but it doesn\'t matter where the mire counter you remove came from. For instance, you could remove mire counters that were put on the land by Gilder Bairn.').
card_ruling('cyclopean tomb', '2008-08-01', 'The land remains a Swamp as long as it has a mire counter on it. This effect is not tied to the Tomb remaining on the battlefield.').

card_ruling('cyclops gladiator', '2010-08-15', 'Cyclops Gladiator\'s ability triggers and resolves during the declare attackers step, before blockers are declared. If it survives, it continues to attack (presumably with some damage marked on it) and may be blocked.').
card_ruling('cyclops gladiator', '2010-08-15', 'If the targeted creature leaves the battlefield (or otherwise becomes an illegal target) before the ability resolves, the ability is countered. Cyclops Gladiator isn\'t dealt damage.').
card_ruling('cyclops gladiator', '2010-08-15', 'On the other hand, if Cyclops Gladiator leaves the battlefield before the ability resolves, the ability continues to resolve. Cyclops Gladiator deals damage to the targeted creature equal to the power it had as it last existed on the battlefield.').

card_ruling('cyclops of eternal fury', '2014-04-26', 'Cyclops of Eternal Fury’s ability gives itself haste.').

card_ruling('cyclops tyrant', '2013-07-01', 'If Cyclops Tyrant blocks a creature with power 3 or greater, and then that creature’s power becomes 2 or less, Cyclops Tyrant continues to block that creature.').

card_ruling('cylian sunsinger', '2009-02-01', 'When the ability resolves, it gives the +3/+3 bonus to the source of the ability and each other creature that shares a name with that source, even if that source isn\'t actually named Cylian Sunsinger. This could happen if Skill Borrower gains this ability, or if Cylian Sunsinger\'s name is changed (due to Mirrorweave, perhaps) before the ability resolves.').
card_ruling('cylian sunsinger', '2009-02-01', 'If Cylian Sunsinger has left the battlefield by the time its ability resolves, its ability will affect each creature whose name is the same as the name Cylian Sunsinger had as it last existed on the battlefield.').

card_ruling('cystbearer', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('cystbearer', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('cystbearer', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('cystbearer', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('cystbearer', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('cystbearer', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('cytoplast manipulator', '2006-05-01', 'The control change lasts as long as Cytoplast Manipulator is on the battlefield. It doesn\'t end if the creature loses all its +1/+1 counters, it stops being a creature, or if Cytoplast Manipulator becomes untapped.').
card_ruling('cytoplast manipulator', '2006-05-01', 'If the ability is activated and Cytoplast Manipulator leaves the battlefield before it resolves, the ability does nothing. The target creature will remain under its current controller\'s control.').

card_ruling('cytoshape', '2006-05-01', 'Only the creature that\'s becoming a copy is targeted. The creature that it will copy isn\'t chosen until Cytoshape resolves.').
card_ruling('cytoshape', '2006-05-01', 'The creature copies the printed values of the chosen creature, plus any copy effects that have been applied to it. It won\'t copy counters on that creature. It won\'t copy effects that have changed the creature\'s power, toughness, types, color, or so on.').
card_ruling('cytoshape', '2006-05-01', 'If the creature copies a creature that\'s copying a creature, it will become whatever the chosen creature is copying.').
card_ruling('cytoshape', '2006-05-01', 'If the creature becomes a copy of a face-down creature, it will become a 2/2 creature with no name, creature type, abilities, mana cost, or color. It will not become face-down and thus can\'t be turned face up.').
card_ruling('cytoshape', '2006-05-01', 'This effect can cause the target to stop being a creature. For example, if it becomes a copy of an animated Blinkmoth Nexus, the printed wording will be copied and it will become an unanimated Blinkmoth Nexus.').
card_ruling('cytoshape', '2006-05-01', 'Effects that have already applied to the target creature will continue to apply to it. For example, if Giant Growth had given it +3/+3 earlier in the turn, then Cytoshape makes it a copy of Grizzly Bears, it will be a 5/5 Grizzly Bears.').
card_ruling('cytoshape', '2006-05-01', 'A creature may become a copy of itself this way. This generally won\'t have any visible effect.').
card_ruling('cytoshape', '2006-05-01', 'At the end of the turn, the creature reverts to what it was before. If two Cytoshapes affect the same creature on the same turn, they\'ll both wear off at the same time.').

