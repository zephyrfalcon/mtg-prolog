% Rulings

card_ruling('i bask in your silent awe', '2010-06-15', 'Players can\'t respond to the action of setting a scheme in motion. That means that if this scheme is turned face up, your opponents can\'t cast spells before the static ability takes effect.').
card_ruling('i bask in your silent awe', '2010-06-15', 'If any of your opponents cast a spell during your upkeep or draw step, that player can\'t cast another spell during the turn this scheme is set in motion.').
card_ruling('i bask in your silent awe', '2010-06-15', 'The scheme is abandoned if all your opponents refrain from casting spells during their entire turn, and during your subsequent upkeep before this scheme\'s last ability resolves. They may still activate abilities and perform special actions.').

card_ruling('i call on the ancient magics', '2010-06-15', 'Your opponents each find a card in whatever order they like. They may consult with each other during this process. You\'ll know what cards they\'ve found before you search.').
card_ruling('i call on the ancient magics', '2010-06-15', 'You don\'t reveal the cards for which you search.').

card_ruling('i delight in your convulsions', '2010-06-15', 'The amount of life a player can lose is not bounded by his or her life total. For example, if your opponents have 1, 2, and 10 life, respectively, when this scheme\'s ability resolves, each of them will lose 3 life. This brings their life totals to -2, -1, and 7 life, respectively. You gain 9 life. Then the first two opponents lose the game as a state-based action. (If the first two opponents concede before the triggered ability resolves, however, you\'ll gain only 3 life.)').

card_ruling('i know all, i see all', '2010-06-15', 'As an opponent\'s untap step begins, if you control this face-up scheme card, all your permanents untap during that untap step. You have no choice about what untaps. Those permanents untap at the same time as the active players\' permanents.').
card_ruling('i know all, i see all', '2010-06-15', 'Since your opponents take a shared team turn, they all have the same untap step. Your permanents untap just once during that step, no matter how many opponents you have.').
card_ruling('i know all, i see all', '2010-06-15', 'During an opponent\'s untap step, effects that would otherwise cause your permanents to stay tapped don\'t apply because they apply only during *your* untap step. For example, if you control a Deep-Slumber Titan (a creature that says \"Deep-Slumber Titan doesn\'t untap during your untap step\"), you untap Deep-Slumber Titan during each opponent\'s untap step.').
card_ruling('i know all, i see all', '2010-06-15', 'Controlling more than one face-up I Know All, I See All card is redundant. You can\'t untap your permanents more than once in a single untap step.').
card_ruling('i know all, i see all', '2010-06-15', 'The last ability won\'t trigger at all unless, as an end step starts, three or more cards have already been put into your graveyard that turn. Those cards don\'t still need to be in your graveyard at that time.').
card_ruling('i know all, i see all', '2010-06-15', 'The last ability of this scheme counts the number of cards put into your graveyard over the course of the entire turn, even if it wasn\'t face up the whole time. Specifically, during the turn you set this scheme in motion, its last ability will count cards that were put into your graveyard during your upkeep or draw step.').
card_ruling('i know all, i see all', '2010-06-15', 'The last ability doesn\'t count tokens that were put into your graveyard from the battlefield, because they\'re not cards. The same is true for copies of spells that were put into your graveyard when they resolved or were countered.').

card_ruling('ib halfheart, goblin tactician', '2006-09-25', 'If you don\'t actually sacrifice the Goblin (because it was removed from the battlefield before the ability resolved, for example), no damage is dealt.').

card_ruling('icatian infantry', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('icatian infantry', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('icatian infantry', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('icatian infantry', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('icatian phalanx', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('icatian phalanx', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('icatian phalanx', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('icatian phalanx', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('icatian skirmishers', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('icatian skirmishers', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('icatian skirmishers', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('icatian skirmishers', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('icatian store', '2004-10-04', 'It is considered \"tapped for mana\" if you activate its mana ability, even if you choose to take zero mana from it.').
card_ruling('icatian store', '2004-10-04', 'This enters the battlefield tapped even if a continuous effect immediately changes it to something else.').
card_ruling('icatian store', '2004-10-04', 'If the land is tapped by some external effect, no counters are removed from it.').
card_ruling('icatian store', '2004-10-04', 'Counters are not lost if the land is changed to another land type. They wait around for the land to change back.').
card_ruling('icatian store', '2004-10-04', 'Whether or not it is tapped is checked at the beginning of upkeep. If it is not tapped, the ability does not trigger. It also checks during resolution and you only get a counter if it is still tapped then.').

card_ruling('ice cage', '2009-10-01', 'If the enchanted creature becomes the target of a spell or ability, Ice Cage\'s ability triggers and is put on the stack on top of that spell or ability. Ice Cage\'s ability will resolve (causing Ice Cage to be destroyed) first.').
card_ruling('ice cage', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('ice cauldron', '2004-10-04', 'The Cauldron counts only mana spent on it. It doesn\'t see the value of X, so isn\'t affected by cost reductions. Thus, if you spent {2}{R}{R} on X, you get {2}{R}{R} later even if X was 6.').
card_ruling('ice cauldron', '2004-10-04', 'Cards which are not actually in your hand can\'t be exiled by the Ice Cauldron.').
card_ruling('ice cauldron', '2004-10-04', 'If multiple cards are exiled by the Cauldron, the mana can only be used for the card that was exiled by the Cauldron when the most recent charge counter was put there.').
card_ruling('ice cauldron', '2004-10-04', 'The mana put in the Cauldron can only be used to cast the given spell, but you can add additional mana to a spell. This means you can pay part of the cost on one turn and the rest of it on the next turn.').
card_ruling('ice cauldron', '2004-10-04', 'You do not have to use any mana from the Cauldron when casting the spell if you don\'t want to. You don\'t even have to tap the Cauldron and draw the mana, you can just cast the spell using mana from somewhere else.').
card_ruling('ice cauldron', '2004-10-04', 'If multiple cards are exiled by the Cauldron, any one of them can be cast.').
card_ruling('ice cauldron', '2004-10-04', 'If the ability to exile a card with the Cauldron is countered, you do not lose the card since it is exiled during resolution.').
card_ruling('ice cauldron', '2004-10-04', 'It is possible to have more than one card exiled by the Cauldron. You can tap the Cauldron to remove the charge counter and whatever mana is on it but leave the card there. Later, you can tap it and put in mana and a charge counter to add another card.').
card_ruling('ice cauldron', '2004-10-04', 'X can be zero. This places a zero mana counter on the Cauldron.').
card_ruling('ice cauldron', '2004-10-04', 'You can only cast the spell when you could legally cast it normally. So no casting a sorcery on your opponent\'s turn.').
card_ruling('ice cauldron', '2004-10-04', 'The mana can be used to pay for additional costs to cast the spell.').
card_ruling('ice cauldron', '2004-10-04', 'If the Cauldron leaves the battlefield, you can still cast any cards it exiled as though they were in your hand. You just no longer have access to the mana you charged the Cauldron with.').
card_ruling('ice cauldron', '2004-10-04', 'Tapping the Cauldron for the mana is a mana ability.').
card_ruling('ice cauldron', '2004-10-04', 'Produces mana of the last type used to put a counter on itself, not via any other charge counters.').

card_ruling('ice cave', '2004-10-04', 'The mana cost does not include cost increases or reductions.').

card_ruling('ice floe', '2008-10-01', 'You may target a tapped creature with Ice Floe\'s activated ability.').
card_ruling('ice floe', '2008-10-01', 'The targeted creature is not removed from combat. Its attack will continue as normal.').
card_ruling('ice floe', '2008-10-01', 'If the ability resolves, then the creature gains flying later, Ice Floe\'s effect will not end. (The ability checks whether the creature has flying only at the time you activate it and the time it resolves.)').
card_ruling('ice floe', '2008-10-01', 'If the affected creature is untapped by some other spell or ability, Ice Floe\'s effect will not end. If you keep Ice Floe tapped, and that creature becomes tapped again, Ice Floe will continue to prevent it from being untapped during its controller\'s untap step.').
card_ruling('ice floe', '2008-10-01', 'Ice Floe doesn\'t track the creature\'s controller. If the affected creature changes controllers, Ice Floe will prevent it from being untapped during its new controller\'s untap step.').
card_ruling('ice floe', '2008-10-01', 'If Ice Floe untaps or leaves the battlefield, its effect will end. This has no immediate visible affect on the creature. (It doesn\'t untap immediately, for example.) The creature will just untap as normal during its controller\'s next untap step.').

card_ruling('icefall regent', '2015-02-25', 'Icefall Regent’s triggered ability can target any creature controlled by an opponent, including one that is already tapped. In that case, the creature stays tapped and won’t untap during its controller’s untap step.').
card_ruling('icefall regent', '2015-02-25', 'If another player gains control of Icefall Regent, the creature will no longer be affected by the ability stopping it from untapping, even if you later regain control of Icefall Regent.').
card_ruling('icefall regent', '2015-02-25', 'The ability stopping the creature from untapping will continue to apply to it even if the creature changes controllers.').
card_ruling('icefall regent', '2015-02-25', 'Icefall Regent’s last ability affects all spells cast by your opponents that target it, including Aura spells and spells with multiple targets. It doesn’t affect abilities.').

card_ruling('icefeather aven', '2014-09-20', 'If you control the only other creatures when Icefeather Aven is turned face up, you must target one of them. You choose whether that creature is returned to its owner’s hand as the ability resolves.').
card_ruling('icefeather aven', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('icefeather aven', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('icefeather aven', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('icefeather aven', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('icefeather aven', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('icefeather aven', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('icefeather aven', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('icefeather aven', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('icefeather aven', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('icequake', '2006-10-15', 'Checks only on resolution to see if the land has the supertype snow; it\'s not part of the targeting condition.').

card_ruling('ichneumon druid', '2004-10-04', 'It tracks the instant count for each player separately.').

card_ruling('ichor explosion', '2011-06-01', 'X is the power of the sacrificed creature as it last existed on the battlefield.').
card_ruling('ichor explosion', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('ichor explosion', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('ichor rats', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('ichor rats', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('ichor rats', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('ichor rats', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('ichor rats', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('ichor rats', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('ichor rats', '2011-01-01', 'If Ichor Rats\'s last ability causes each player to have ten poison counters, the game is a draw.').

card_ruling('ichor slick', '2007-05-01', 'If you cycle Ichor Slick, you may cast it for its madness cost. You choose whether or not to cast it with madness before you draw a card from the cycling effect.').
card_ruling('ichor slick', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('ichor wellspring', '2011-06-01', 'The ability will trigger when Ichor Wellspring is put into a graveyard from the battlefield, even if the ability that triggered when it entered the battlefield hasn\'t resolved yet.').

card_ruling('ichorclaw myr', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('ichorclaw myr', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('ichorclaw myr', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('ichorclaw myr', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('ichorclaw myr', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('ichorclaw myr', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('ichorclaw myr', '2011-01-01', 'Whenever Ichorclaw Myr becomes blocked, its last ability triggers just once, no matter how many creatures it became blocked by. It will get just +2/+2.').

card_ruling('icy blast', '2014-09-20', 'You check if you control a creature with power 4 or greater as Icy Blast resolves. It doesn’t matter if you control such a creature during any of the relevant untap steps.').
card_ruling('icy blast', '2014-09-20', 'Icy Blast can target a creature that’s already tapped. It still won’t untap during its controller’s next untap step.').
card_ruling('icy blast', '2014-09-20', 'Icy Blast tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('icy blast', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('icy blast', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('icy manipulator', '2004-10-04', 'Tapping a card with an effect like this will never pay the cost of an ability. For example, tapping a land with this card will not put mana into a player\'s pool.').
card_ruling('icy manipulator', '2004-10-04', 'Icy Manipulator can\'t be used to stop someone from using an ability of the permanent you plan to tap. It can be used to make a player use the ability now or to not use it.').
card_ruling('icy manipulator', '2004-10-04', 'The ability can target a tapped card, but tapping a tapped card does nothing useful. And it will not trigger \"if the card becomes tapped\" effects.').
card_ruling('icy manipulator', '2004-10-04', 'If you want to stop someone from attacking with a creature by using this card, you must do so before attackers are declared. You can\'t wait until after attackers are declared and then try to use it to make a creature stop attacking. Note that your opponent can\'t start declaring attackers without letting you use the Icy Manipulator.').
card_ruling('icy manipulator', '2004-10-04', 'Tapping an attacking creature which did not tap to attack does not remove the creature from the attack or cause the creature to stop dealing damage.').

card_ruling('icy prison', '2008-10-01', 'If Icy Prison leaves the battlefield before its first ability has resolved, its third ability will trigger and do nothing. Then its first ability will resolve and exile the targeted creature forever.').

card_ruling('ideas unbound', '2005-06-01', 'If you don\'t have three or more cards in hand at the end of turn, you discard what cards you have.').

card_ruling('idle thoughts', '2008-08-01', 'You can activate the ability whether or not you have any cards in your hand. However, you\'ll draw a card only if your hand is empty when the ability resolves. (This card works differently than the _Tempest_(TM) card Fool\'s Tome.)').

card_ruling('igneous pouncer', '2009-05-01', 'Unlike the normal cycling ability, landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a land card of the specified land type, reveal it, put it into your hand, then shuffle your library.').
card_ruling('igneous pouncer', '2009-05-01', 'Landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being landcycled. Any ability that stops a cycling ability from being activated also stops a landcycling ability from being activated.').
card_ruling('igneous pouncer', '2009-05-01', 'You can choose to find any card with the appropriate land type, including nonbasic lands. You can also choose not to find a card, even if there is a land card with the appropriate type in your library.').
card_ruling('igneous pouncer', '2009-05-01', 'A landcycling ability lets you search for any card in your library with the stated land type. It doesn\'t have to be a basic land.').
card_ruling('igneous pouncer', '2009-05-01', 'You may only activate one landcycling ability at a time. You must specify which landcycling ability you are activating as you cycle this card, not as the ability resolves.').

card_ruling('ignite disorder', '2009-02-01', 'The number of targets must be at least 1 and at most 3.').
card_ruling('ignite disorder', '2009-02-01', 'You divide the damage as you cast Ignite Disorder, not as it resolves. Each target must be assigned at least 1 damage.').
card_ruling('ignite disorder', '2009-10-01', 'The number of targets chosen for Ignite Disorder must be at least 1 and at most 3. You divide the damage as you cast the spell, not as it resolves. Each target must be assigned at least 1 damage.').

card_ruling('ignite memories', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('ignite memories', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('ignite memories', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('ignite memories', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('ignite memories', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('ignite the cloneforge!', '2010-06-15', 'You can target any permanent an opponent controls, not just a creature.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'The token that\'s put onto the battlefield copies exactly what\'s printed on the targeted permanent (unless that permanent is copying something else or it\'s a token; see below). It doesn\'t copy whether that permanent is tapped or untapped, whether it has any counters on it or Auras attached to it, or whether it\'s been affected by any noncopy effects that changed its power, toughness, types, color, or so on.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'If the targeted permanent is copying something else when the ability resolves (for example, if it\'s a Clone), then the token enters the battlefield as a copy of whatever that permanent is copying.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'If the targeted permanent is a token, the new token copies the characteristics of the original token as stated by the effect that put it onto the battlefield.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'If the targeted permanent has {X} in its mana cost (such as Protean Hydra), X is considered to be zero.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'Any enters-the-battlefield abilities of the targeted permanent will trigger when the token is put onto the battlefield. Any \"as [this permanent] enters the battlefield\" or \"[this permanent] enters the battlefield with\" abilities of that permanent will also work.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'If the targeted permanent leaves the battlefield before the ability resolves, the ability is countered. You don\'t get a token.').
card_ruling('ignite the cloneforge!', '2010-06-15', 'If the targeted permanent is an Aura, you choose a legal permanent, player, or other card for the token to enchant as it enters the battlefield. A permanent you choose must be able to be enchanted by the token. (For example, if the Aura it\'s copying is red and has \"enchant creature,\" the permanent must be a creature, and it can\'t have protection from red.) Since the token Aura wasn\'t cast as a spell, it doesn\'t target what it will enchant; you may choose something that has shroud, for example. As the ability resolves, no player can respond between the time you choose what the token will enchant and the time it enters the battlefield. If you can\'t choose something for the token to enchant, it enters the battlefield unattached, then is put into your graveyard as a state-based action.').

card_ruling('ignition team', '2014-05-29', 'If the target land hasn’t been under its controller’s control continuously since the beginning of his or her most recent turn, that land won’t be able to attack and its {T} abilities can’t be activated. In most cases, that means it will no longer be able to be tapped for mana that turn.').
card_ruling('ignition team', '2014-05-29', 'This doesn’t count as a creature entering the battlefield. The land was already on the battlefield; it only changed types.').
card_ruling('ignition team', '2014-05-29', 'Ignition Team’s ability doesn’t affect the land’s name or any other types, subtypes, or supertypes (such as basic or legendary) the land may have. The land will also keep any abilities it had.').
card_ruling('ignition team', '2014-05-29', 'Effects that modify power and/or toughness without setting them to a specific value (like the one created by Giant Growth), power and toughness changes from counters, and effects that switch a creature’s power and toughness will continue to apply. This may happen if the land was already a creature when the ability resolved.').
card_ruling('ignition team', '2014-05-29', 'Copies of the affected land won’t be 4/4 red Elemental creatures. For example, if the target land is a basic Mountain, a copy of that creature would just be a basic Mountain.').

card_ruling('ignorant bliss', '2006-05-01', 'No player may look at the cards while they\'re face down.').
card_ruling('ignorant bliss', '2006-05-01', 'If you cast this during the End step, you won\'t return the cards to your hand or draw a card until the next turn\'s End step.').
card_ruling('ignorant bliss', '2006-05-01', 'If the delayed triggered ability is countered (for example, with Voidslime), the cards remain exiled forever.').

card_ruling('ikiral outrider', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('ikiral outrider', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('ikiral outrider', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('ikiral outrider', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('ikiral outrider', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('ill-gotten gains', '2004-10-04', 'Players can get fewer than 3 cards if they want.').

card_ruling('ill-tempered cyclops', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('ill-tempered cyclops', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('ill-tempered cyclops', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('illicit auction', '2004-10-04', 'This is a life loss and not a life payment so you can bid more life than you have.').

card_ruling('illness in the ranks', '2013-01-24', 'If an effect creates a creature token that normally has toughness 1, it will enter the battlefield with toughness 0, be put into its owner\'s graveyard as a state-based action, and then cease to exist. Any abilities that trigger when a creature enters the battlefield or dies will trigger.').

card_ruling('illuminated folio', '2008-05-01', 'The two revealed cards stay revealed until the ability resolves. If one of them is played, is discarded, or otherwise leaves your hand, the ability will still resolve as normal.').
card_ruling('illuminated folio', '2008-05-01', 'Colorless is not a color. You can\'t reveal two colorless cards from your hand to activate this ability because those cards don\'t share a color with one another.').

card_ruling('illusionary armor', '2013-07-01', 'Only Illusionary Armor is sacrificed because of its triggered ability; the enchanted creature is not.').

card_ruling('illusionary forces', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('illusionary mask', '2004-10-04', 'The creature enters the battlefield face down, so none of its \"enters the battlefield\" abilities will trigger or have any effect. Also none of its \"As this enters the battlefield\" abilities apply.').
card_ruling('illusionary mask', '2004-10-04', 'The creature\'s \"enters the battlefield\" abilities (and any other abilities relating to the creature entering the battlefield) do not trigger when it turns face up.').
card_ruling('illusionary mask', '2004-10-04', 'Only the controller of the face down creature can look at it.').
card_ruling('illusionary mask', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('illusionary mask', '2009-10-01', 'If you use the ability to cast a creature card face down, you must keep track of the amount and types of mana you spent on {X}. If that creature spell is moved from the stack to anywhere other than the battlefield, the resulting creature leaves the battlefield, or the game ends, the face-down card is revealed. If its mana cost couldn\'t be paid by some amount of, or all of, the mana you spent on {X}, all applicable penalties for casting a card illegally are assessed.').
card_ruling('illusionary mask', '2009-10-01', 'You actually cast the card face-down, much as when playing a spell with Morph. It can be responded to and countered.').
card_ruling('illusionary mask', '2009-10-01', 'The effect that turns it face-up is a replacement effect. It doesn\'t use the stack and can\'t be responded to.').
card_ruling('illusionary mask', '2009-10-01', 'You may not turn a face-down spell face up. You may not turn a face-down permanent face up unless it would have morph while face up or an effect specifically allows you to turn it face up. Illusionary Mask\'s ability has you turn a face-down creature face up if it would assign damage, deal damage, be dealt damage, or become tapped, but not for any other reason. For example, if you use Illusionary Mask\'s ability to cast a black creature face down, you can\'t turn it face up just because it\'s being targeted by Terror.').
card_ruling('illusionary mask', '2009-10-01', 'Both the amount and types of mana you spend on {X} are taken into account while you\'re choosing a creature card from your hand. For example, if you spent {U}{U} on {X}, you can choose a creature card with mana cost {U}{U}, {1}{U}, {2}, or {W/U}{W/U}, among other possibilities, but not one that costs {2}{U} or one that costs {G}.').
card_ruling('illusionary mask', '2009-10-01', 'While the creature card is face down, it\'s a 2/2 creature with no name, mana cost, color, creature type, abilities, or expansion symbol. Since it has no mana cost, its converted mana cost is 0.').
card_ruling('illusionary mask', '2009-10-01', 'You may look at a face-down spell or permanent you control at any time.').
card_ruling('illusionary mask', '2009-10-01', 'You can turn a face-down permanent face up if it would have morph while face up. This applies to creatures you cast face down as a result of Illusionary Mask\'s effect. The rest of Illusionary Mask\'s effect applies to it as well.').
card_ruling('illusionary mask', '2009-10-01', 'Illusionary Mask\'s ability will continue to apply to creatures cast face down with it, even if Illusionary Mask has left the battlefield.').

card_ruling('illusionary presence', '2005-11-01', 'Can only give landwalk of land sub-types that exist (so you can no longer give \"Island of Wak-Wak walk\").').
card_ruling('illusionary presence', '2005-11-01', 'Can\'t give Snow landwalk, because Snow is supertype, not a subtype.').
card_ruling('illusionary presence', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('illusionary servant', '2009-10-01', 'If Illusionary Servant becomes the target of a spell or ability, Illusionary Servant\'s ability triggers and is put on the stack on top of that spell or ability. Illusionary Servant\'s ability will resolve (causing it to be sacrificed) first. Unless the spell or ability has another target, it will then be countered when it tries to resolve for having no legal targets.').

card_ruling('illusionary terrain', '2006-10-15', 'Will not add or remove Snow Supertype to or from a land.').

card_ruling('illusionist\'s bracers', '2013-01-24', 'An activated ability is written in the form “Cost: Effect.”').
card_ruling('illusionist\'s bracers', '2013-01-24', 'The copy will have the same targets as the ability it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('illusionist\'s bracers', '2013-01-24', 'If the ability is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('illusionist\'s bracers', '2013-01-24', 'If the ability has {X} in its cost, the value of X is copied.').
card_ruling('illusionist\'s bracers', '2013-01-24', 'If the cost of an activated ability requires Illusionist\'s Bracers or the equipped creature to be sacrificed, the ability won\'t be copied. At the time the ability is considered activated (after all costs are paid), Illusionist\'s Bracers is no longer equipped to that creature.').
card_ruling('illusionist\'s bracers', '2013-01-24', 'If Illusionist\'s Bracers somehow becomes equipped to a creature an opponent controls, and an activated ability (that isn\'t a mana ability) of that creature is activated, you will control the copy of that ability.').
card_ruling('illusionist\'s bracers', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').

card_ruling('illusions of grandeur', '2004-10-04', 'If a player takes control of this card away from you, you do not lose 20 life because the loss of 20 life is a separate ability. That player is now subject to the loss.').

card_ruling('illusory angel', '2012-06-01', 'Notably, if you exile Illusory Angel because of a cascade ability of a spell with converted mana cost 4 or greater, you\'ll be able to cast Illusory Angel.').
card_ruling('illusory angel', '2014-07-18', 'It doesn’t matter whether the other spell resolved. It could have been countered or, if you’ve somehow cast Illusory Angel as though it had flash, it could still be on the stack.').

card_ruling('illusory demon', '2009-05-01', 'If you cast a spell, the spell goes on the stack, then Illusory Demon\'s triggered ability goes on the stack on top of it. The triggered ability will resolve (and you\'ll sacrifice Illusory Demon) before the spell resolves.').

card_ruling('illusory gains', '2015-02-25', 'Illusory Gains’s last ability is mandatory. You can’t choose to have Illusory Gains not move.').
card_ruling('illusory gains', '2015-02-25', 'If Illusory Gains can’t legally enchant the creature that enters the battlefield (perhaps because it has protection from blue), it remains where it is. You retain control of the creature it’s currently enchanting.').
card_ruling('illusory gains', '2015-02-25', 'The last ability of Illusory Gains doesn’t target the new creature. It will become attached to that creature even if that creature has hexproof or shroud.').
card_ruling('illusory gains', '2015-02-25', 'If multiple creatures enter the battlefield under an opponent’s control at the same time, Illusory Gains will trigger for each of them. You may put these abilities on the stack in any order. As each resolves, you’ll briefly gain control of the associated creature. The last one to resolve determines which one you’ll ultimately control.').

card_ruling('imaginary pet', '2005-08-01', 'Imaginary Pet\'s ability triggers only if you have cards in your hand as your upkeep begins, and the ability checks again as it resolves. If your hand is empty at both those times, Imaginary Pet stays on the battlefield.').

card_ruling('immediate action', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('immediate action', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('immediate action', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('immediate action', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('immediate action', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('immediate action', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('immediate action', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('immediate action', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('immediate action', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('immediate action', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('immediate action', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('immediate action', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('immediate action', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('immersturm', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('immersturm', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('immersturm', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('immersturm', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('immersturm', '2009-10-01', 'If a creature controlled by a player other than the planar controller enters the battlefield, the planar controller (not the creature\'s controller) controls Immersturm\'s triggered ability. That means that if multiple creatures enter the battlefield at the same time, that player controls all of Immersturm\'s triggered abilities and may put them on the stack in whichever order he or she likes. The last one put on the stack is the first one to resolve.').
card_ruling('immersturm', '2009-10-01', 'When the {C} ability resolves, the targeted creature is exiled, then immediately returned to the battlefield as a new permanent with no relation to its previous existence.').
card_ruling('immersturm', '2009-10-01', 'Any \"as [this permanent] enters the battlefield\" choices for the card returned by the {C} ability are made by that card\'s owner, not its old controller or the controller of the {C} ability.').
card_ruling('immersturm', '2009-10-01', 'A token that has left the battlefield can\'t come back onto the battlefield. If the {C} ability exiles a token, that token remains in exile until state-based actions are checked, then it ceases to exist.').

card_ruling('immerwolf', '2011-01-22', 'A creature that\'s both a Wolf and a Werewolf gets the bonus only once.').
card_ruling('immerwolf', '2011-01-22', 'Immerwolf\'s bonus never applies to itself, even if it somehow becomes a Werewolf creature.').
card_ruling('immerwolf', '2011-01-22', 'A \"non-Human Werewolf\" is a creature that is a Werewolf but is not a Human.').

card_ruling('immortal coil', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('immortal coil', '2008-10-01', 'The first ability exiles two cards in your graveyard as a cost. You can\'t activate this ability unless you have at least two cards in your graveyard. Exiling those two cards can\'t be responded to.').
card_ruling('immortal coil', '2008-10-01', 'The second ability prevents all damage that would be dealt to you, regardless of how many cards are in your graveyard. For example, if you have three cards in your graveyard and would be dealt 5 damage, Immortal Coil prevents all 5 damage and exiles all the cards in your graveyard.').
card_ruling('immortal coil', '2008-10-01', 'The third ability checks whether your graveyard is empty only at the time it triggers. Putting a card into your graveyard after that doesn\'t help. You\'ll lose the game when the ability resolves.').
card_ruling('immortal coil', '2008-10-01', 'If the third ability is countered, but your graveyard is still empty, the ability will immediately trigger again.').
card_ruling('immortal coil', '2008-10-01', 'Similarly, if the third ability resolves but you don\'t lose the game for some reason (because you control Platinum Angel, perhaps), it will immediately trigger again if your graveyard is still empty. Immortal Coil + Platinum Angel + an empty graveyard is an involuntary infinite loop. Unless a player disrupts it, the game will end in a draw.').
card_ruling('immortal coil', '2008-10-01', 'Suppose you control Immortal Coil and a planeswalker, and a source controlled by an opponent would deal noncombat damage to you. Both Immortal Coil and the planeswalker have effects that could affect that damage. You choose one to apply. -- If you apply Immortal Coil\'s effect, the damage is prevented. The planeswalker replacement effect is no longer applicable. -- If you apply the planeswalker replacement effect, your opponent then chooses whether he or she wants to redirect the damage to your planeswalker. If your opponent does, then that many loyalty counters are removed from the planeswalker, and Immortal Coil\'s effect is no longer applicable. If your opponent doesn\'t, then Immortal Coil\'s effect applies and the damage is prevented.').

card_ruling('immortal servitude', '2013-01-24', 'If a creature card in a graveyard has X in its mana cost, that X is considered to be 0.').

card_ruling('imp\'s mischief', '2004-10-04', 'Once the spell resolves, the new target is considered to be targeted by the deflected spell. This will trigger any effects which trigger on being targeted.').
card_ruling('imp\'s mischief', '2004-10-04', 'This only targets the spell being changed, not the original or new target of the spell it is affecting.').
card_ruling('imp\'s mischief', '2004-10-04', 'The target of a spell that targets another spell on the stack can be changed to any other spell on the stack, even if the new target will resolve before the spell does.').
card_ruling('imp\'s mischief', '2004-10-04', 'You can\'t make a spell which is on the stack target itself.').
card_ruling('imp\'s mischief', '2004-10-04', 'You can choose to make a spell on the stack target this spell (if such a target choice would be legal had the spell been cast while this spell was on the stack). The new target for the deflected spell is not chosen until this spell resolves. This spell is still on the stack when new targets are selected for the spell.').
card_ruling('imp\'s mischief', '2004-10-04', 'If there is no other legal target for the spell, this does not change the target.').
card_ruling('imp\'s mischief', '2004-10-04', 'This does not check if the current target is legal. It just checks if the spell has a single target.').
card_ruling('imp\'s mischief', '2004-10-04', 'You choose the spell to target on announcement, but you pick the new target for that spell on resolution.').
card_ruling('imp\'s mischief', '2007-02-01', 'If there is no other legal target for the spell, the spell\'s target isn\'t changed. You still lose the life.').
card_ruling('imp\'s mischief', '2007-02-01', 'If a spell targets multiple things, you can\'t target it with Imp\'s Mischief, even if all but one of the targets has become illegal.').
card_ruling('imp\'s mischief', '2007-02-01', 'If a spell has multiple targets but is targeting the same thing with all of them (such as Seeds of Strength targeting the same creature three times), you can\'t target that spell with Imp\'s Mischief.').

card_ruling('impact resonance', '2014-11-07', 'You determine the value for X and announce how damage will be divided as you cast Impact Resonance. Each chosen target must receive at least 1 damage. Once the value of X is determined, it won’t change, even if a source deals a greater amount of damage before Impact Resonance resolves. (Most of the time, the value of X in an effect would be determined only as the spell resolves. However, the rules state that if damage needs to be “divided,” that information is checked as the spell is put on the stack.)').
card_ruling('impact resonance', '2014-11-07', 'Impact Resonance looks at the entire turn to see the greatest amount of damage dealt from a single source to a single permanent or player. For example, if the only damage dealt during the turn was Earthquake, which dealt 5 damage to each of four creatures and two players, then X is 5.').
card_ruling('impact resonance', '2014-11-07', 'If Impact Resonance has multiple targets, and some, but not all, of them become illegal before Impact Resonance resolves, Impact Resonance will still deal damage to the remaining legal targets according to the original damage division.').

card_ruling('impact tremors', '2015-02-25', 'If multiple creatures enter the battlefield under your control at the same time, Impact Tremors will trigger that many times. Each of these separate abilities will deal 1 damage to each opponent.').

card_ruling('impatience', '2004-10-04', 'If a spell is countered, it still counts as having been cast.').

card_ruling('impelled giant', '2008-08-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap creatures that haven\'t been under your control since your most recent turn began to pay the cost.').
card_ruling('impelled giant', '2008-08-01', 'The power of the creature you tap is checked as the ability resolves, not as the creature is tapped, and not at any later time should the power of that creature change. If that creature is no longer on the battlefield as the ability resolves, use the power it had when it was last on the battlefield.').

card_ruling('impending disaster', '2004-10-04', 'This ability does not trigger at all if there are not seven lands on the battlefield at the beginning of upkeep. It checks this number again when it is going to resolve and does not do anything if the land count is below 7.').
card_ruling('impending disaster', '2004-10-04', 'The ability is not optional.').
card_ruling('impending disaster', '2010-06-15', 'If this card is not on the battlefield at the times its ability resolves, then you can\'t sacrifice it. However, you still destroy all lands.').

card_ruling('imperial hellkite', '2004-10-04', 'You do not have to find a Dragon card if you do not want to, even if you have one in your library.').
card_ruling('imperial hellkite', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('imperial mask', '2007-05-01', 'This card creates tokens that enter the battlefield as enchantments, not creatures.').
card_ruling('imperial mask', '2007-05-01', 'Only teammates within the range of influence of Imperial Mask\'s controller will get a token. Imperial Mask\'s controller doesn\'t get a token; that player just gets the Imperial Mask.').
card_ruling('imperial mask', '2007-05-01', 'Each Imperial Mask permanent is independent and affects only its controller and the opponents of its controller.').
card_ruling('imperial mask', '2007-05-01', 'If Imperial Mask has left the battlefield by the time the triggered ability resolves, the tokens will still enter the battlefield as copies of Imperial Mask.').
card_ruling('imperial mask', '2007-05-01', 'If Imperial Mask becomes a copy of something else after the ability triggers but before it resolves, the tokens will enter the battlefield as copies of whatever the ex-Imperial Mask is now. If Imperial Mask became a copy of something else and then left the battlefield, the tokens will enter the battlefield as copies of whatever it was copying when it last existed on the battlefield.').

card_ruling('imperial recruiter', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('imperial recruiter', '2008-10-01', 'If a creature card has \"*\" in its power, the ability that defines its power works in all zones.').

card_ruling('imperiosaur', '2007-05-01', 'Mana from any source other than a land with the supertype \"basic\" can\'t be spent to cast Imperiosaur.').
card_ruling('imperiosaur', '2007-05-01', 'Many mana-producing effects (such as Heartbeat of Spring or Utopia Sprawl, for example) add mana to a player\'s mana pool whenever a land becomes tapped. This mana is produced by the enchantment, not the land, so it can\'t be used to pay for Imperiosaur.').
card_ruling('imperiosaur', '2007-05-01', 'A small number of effects (Pulse of Llanowar and Hall of Gemstone, for example) state that a land \"produces\" certain mana instead of what it would normally produce. If the land is basic, then mana produced this way can be spent to cast Imperiosaur.').
card_ruling('imperiosaur', '2007-05-01', 'If a basic land gains a mana ability (from Riftstone Portal, Joiner Adept, or Rain of Filth, for example), the mana produced this way can be used to cast for Imperiosaur.').
card_ruling('imperiosaur', '2007-05-01', 'Imperiosaur\'s restriction applies to the total cost to cast it. If there are any additional costs (such as from Feroz\'s Ban, for example), those costs would have to be paid with mana produced by basic lands as well.').

card_ruling('impetuous sunchaser', '2014-02-01', 'Impetuous Sunchaser attacks only if it’s able to do so as the declare attackers step begins. If, at that time, it’s tapped or affected by a spell or ability that says it can’t attack, then it doesn’t attack. If there’s a cost associated with having it attack, you’re not forced to pay that cost. If that cost isn’t paid, Impetuous Sunchaser won’t attack.').
card_ruling('impetuous sunchaser', '2014-02-01', 'You choose which player or planeswalker Impetuous Sunchaser attacks.').

card_ruling('imprison', '2004-10-04', 'Both abilities are triggered abilities. The first one triggers on the activating of a creature ability, and the second triggers on declaration of attacking or blocking.').
card_ruling('imprison', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').

card_ruling('impromptu raid', '2008-05-01', 'If you put a creature onto the battlefield and a different player takes control of it before the end of the turn, you can\'t sacrifice it at end of turn. It will remain on the battlefield. It will have haste as long as it\'s on the battlefield.').
card_ruling('impromptu raid', '2008-05-01', 'If the ability is activated after the current turn\'s End step has begun, the creature won\'t be sacrificed until the next turn\'s End step.').

card_ruling('improvised armor', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('impulse', '2004-10-04', 'This is not a draw.').
card_ruling('impulse', '2004-10-04', 'Due to errata, you no longer shuffle your library.').

card_ruling('impulsive maneuvers', '2004-10-04', 'You flip a coin for each creature that attacks, not once per attack.').

card_ruling('in the eye of chaos', '2004-10-04', 'This ability triggers when the spell is cast.').
card_ruling('in the eye of chaos', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('in the web of war', '2005-02-01', 'In the Web of War doesn\'t affect creatures you take control of which are already on the battlefield.').

card_ruling('inaction injunction', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('inaction injunction', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('inaction injunction', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('inaction injunction', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('inaction injunction', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('inaction injunction', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('iname as one', '2005-06-01', 'Effects that put Iname onto the battlefield don\'t cause its enters-the-battlefield ability to trigger.').
card_ruling('iname as one', '2005-06-01', 'Iname can target itself with the graveyard-triggered ability, but it will be exiled and the ability won\'t be able to return Iname.').
card_ruling('iname as one', '2005-06-01', 'You aren\'t required to exile Iname. If you don\'t, the target Spirit card isn\'t returned to the battlefield.').
card_ruling('iname as one', '2013-04-15', 'In a Commander game, you may send Iname to the Command Zone instead of exiling it during the resolution of its ability. If you do, its ability still works. Iname\'s ability only requires that you attempted to exile it, not that it actually gets to the exile zone. This is similar to how destroying a creature (with, for example, Rest in Peace) doesn\'t necessarily ensure that creature will end up in the graveyard; it just so happens that the action of exiling something and the exile zone both use the same word: \"exile\".').
card_ruling('iname as one', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').
card_ruling('iname as one', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('iname, life aspect', '2004-12-01', 'The targets of Iname, Life Aspect\'s ability are chosen when the ability is put onto the stack, but you don\'t choose whether to exile Iname, Life Aspect until the ability resolves.').
card_ruling('iname, life aspect', '2004-12-01', 'You may target Iname, Life Aspect. However, this doesn\'t do anything since the card will no longer be in your graveyard when the creature cards are returned to your hand.').
card_ruling('iname, life aspect', '2004-12-01', 'You can exile the card even if you choose no targets.').
card_ruling('iname, life aspect', '2013-04-15', 'In a Commander game, you may send Iname to the Command Zone instead of exiling it during the resolution of its ability. If you do, its ability still works. Iname\'s ability only requires that you attempted to exile it, not that it actually gets to the exile zone. This is similar to how destroying a creature (with, for example, Rest in Peace) doesn\'t necessarily ensure that creature will end up in the graveyard; it just so happens that the action of exiling something and the exile zone both use the same word: \"exile\".').

card_ruling('incendiary', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('incinerate', '2007-07-15', 'Regeneration abilities that would affect that creature can still be activated; they just won\'t do anything. The creature can\'t be regenerated for any reason, even if an effect other than Incinerate would deal lethal damage to it or otherwise destroy it.').

card_ruling('incite', '2010-08-15', 'Incite overwrites all of the targeted creature\'s old colors, leaving it just red. It doesn\'t matter what colors it used to be (even if, for example, it used to be red and green).').
card_ruling('incite', '2010-08-15', 'You may target any creature with Incite. It\'s okay if that creature is already red. It\'s also okay if it would be unable to attack that turn.').
card_ruling('incite', '2010-08-15', 'If, during its controller\'s declare attackers step, a creature affected by Incite is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then that creature doesn\'t attack. If there\'s a cost associated with having that creature attack, its controller isn\'t forced to pay that cost, so the creature doesn\'t have to attack in that case either.').
card_ruling('incite', '2010-08-15', 'If there are multiple combat phases in a turn, a creature affected by Incite must attack only in the first one in which it\'s able to.').

card_ruling('incite hysteria', '2005-10-01', 'All creatures that share a color are affected, even your own.').
card_ruling('incite hysteria', '2005-10-01', 'A creature \"shares a color\" with any creature that is at least one of its colors. For example, a green-white creature shares a color with creatures that are green, white, green-white, red-white, black-green, and so on.').
card_ruling('incite hysteria', '2005-10-01', 'If it targets a colorless creature, it doesn\'t affect any other creatures. A colorless creature shares a color with nothing, not even other colorless creatures.').
card_ruling('incite hysteria', '2005-10-01', 'You check which creatures share a color with the target when the spell resolves.').
card_ruling('incite hysteria', '2005-10-01', 'Only one creature is targeted. If that creature leaves the battlefield or otherwise becomes an illegal target, the entire spell is countered. No other creatures are affected.').
card_ruling('incite hysteria', '2005-11-01', 'The targeted creature and the creatures that share a color with it at the time Incite Hysteria resolves gain an ability that says they can\'t block this turn. It doesn\'t matter if those creatures change colors before blockers are declared.').

card_ruling('incite rebellion', '2014-11-07', 'The damage is dealt simultaneously. If this causes all players to have 0 or less life, the game is a draw.').

card_ruling('increasing confusion', '2011-01-22', 'Regardless of which zone Increasing Confusion is cast from, it puts cards from the top of target player\'s library into that player\'s graveyard.').

card_ruling('increasing vengeance', '2011-01-22', 'Increasing Vengeance can target (and copy) any instant or sorcery spell you control, not just one with targets.').
card_ruling('increasing vengeance', '2011-01-22', 'When Increasing Vengeance resolves, it creates one or two copies of a spell. You control each of the copies. Those copies are created on the stack, so they\'re not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. The copies will then resolve like normal spells, after players get a chance to cast spells and activate abilities.').
card_ruling('increasing vengeance', '2011-01-22', 'Each of the copies will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal). If there are two copies, you may change the targets of each of them to different legal targets.').
card_ruling('increasing vengeance', '2011-01-22', 'If the spell Increasing Vengeance copies is modal (that is, it says \"Choose one --\" or the like), the copies will have the same mode(s). You can\'t choose different ones.').
card_ruling('increasing vengeance', '2011-01-22', 'If the spell Increasing Vengeance copies has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('increasing vengeance', '2011-01-22', 'You can\'t choose to pay any additional costs for the copies. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copies too. For example, if you sacrifice a 3/3 creature to cast Fling and then copy it with Increasing Vengeance, the copies of Fling will also deal 3 damage to its target.').

card_ruling('incremental blight', '2008-05-01', 'You must target three different creatures. If you can\'t, you can\'t cast Incremental Blight.').

card_ruling('incremental growth', '2014-09-20', 'You must choose three different targets in order to cast Incremental Growth. You decide how many +1/+1 counters each creature will get as part of casting the spell.').
card_ruling('incremental growth', '2014-09-20', 'If some of the creatures are illegal targets as Incremental Growth tries to resolve, the remaining legal targets still get the appropriate number of +1/+1 counters. If all targets are illegal, Incremental Growth is countered.').

card_ruling('incubator drone', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('incubator drone', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('incubator drone', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('incubator drone', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('incubator drone', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('incubator drone', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('incubator drone', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('incubator drone', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('incubator drone', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('incursion specialist', '2013-01-24', 'Incursion Specialist\'s ability can trigger only once each turn. The ability will resolve before the second spell resolves. It doesn\'t matter if the first spell you cast that turn has resolved, was countered, or is still on the stack.').

card_ruling('indestructibility', '2009-10-01', 'If an effect would simultaneously destroy Indestructibility and the creature it\'s enchanting, only Indestructibility is destroyed.').
card_ruling('indestructibility', '2013-07-01', 'Lethal damage, damage from a source with deathtouch, and effects that say \"destroy\" won\'t cause a permanent with indestructible to be put into the graveyard. However, a permanent with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, if it\'s a planeswalker and another planeswalker with the same subtype is controlled by the same player, if it\'s a creature with toughness 0 or less, or if it\'s an Aura that\'s either unattached or attached to something illegal.').
card_ruling('indestructibility', '2013-07-01', 'If a creature enchanted by Indestructibility is dealt lethal damage, the creature isn\'t destroyed, but the damage remains marked on the creature. If Indestructibility stops enchanting that creature later in the turn, the creature will no longer have indestructible and will be destroyed.').

card_ruling('indestructible aura', '2004-10-04', 'Despite the name, this card only prevents damage and not destroy effects. It\'s also not an Aura.').

card_ruling('indigo faerie', '2008-08-01', 'This ability doesn\'t overwrite any previous colors. Rather, it adds another color.').
card_ruling('indigo faerie', '2008-08-01', 'If something affected by Indigo Faerie\'s ability is normally colorless, it will simply be blue. It won\'t be both blue and colorless.').
card_ruling('indigo faerie', '2008-08-01', 'After Indigo Faerie\'s ability resolves, an effect that changes the affected permanent\'s colors will overwrite Indigo Faerie\'s effect. For example, if Indigo Faerie\'s ability is activated on Llanowar Elves, the Elves will be blue and green; if Aphotic Wisps is then cast on the same creature, it\'ll be just black.').

card_ruling('indrik umbra', '2012-06-01', 'Indrik Umbra does not give any creature the ability to block the enchanted creature. It just forces those creatures which are already able to block that creature to do so.').
card_ruling('indrik umbra', '2012-06-01', 'If, during a player\'s declare blockers step, a creature is tapped or it\'s affected by a spell or ability that says it can\'t block, then it doesn\'t block. If there\'s a cost associated with having a creature block, its controller isn\'t forced to pay that cost. The creature doesn\'t have to block.').

card_ruling('indulgent tormentor', '2014-07-18', 'The opponent chooses whether he or she sacrifices a creature, pays 3 life, or allows you to draw a card. That player can’t make an impossible choice, such as sacrificing a creature while he or she controls no creatures.').
card_ruling('indulgent tormentor', '2014-07-18', 'If there are no legal targets to choose, perhaps because all your opponents have hexproof, the ability is removed from the stack with no effect. You won’t draw a card. This is also true if the target opponent becomes an illegal target in response to the ability.').

card_ruling('inescapable brute', '2008-05-01', 'If Inescapable Brute is attacking, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block Inescapable Brute.').

card_ruling('inexorable tide', '2011-01-01', 'You can choose any player that has a counter, including yourself.').
card_ruling('inexorable tide', '2011-01-01', 'You can choose any permanent that has a counter, including ones controlled by opponents. You can\'t choose cards in any zone other than the battlefield, even if they have counters on them, such as suspended cards or a Lightning Storm on the stack.').
card_ruling('inexorable tide', '2011-01-01', 'You don\'t have to choose every permanent or player that has a counter, only the ones you want to add another counter to. Since \"any number\" includes zero, you don\'t have to choose any permanents at all, and you don\'t have to choose any players at all.').
card_ruling('inexorable tide', '2011-01-01', 'If a permanent chosen this way has multiple kinds of counters on it, only a single new counter is put on that permanent.').
card_ruling('inexorable tide', '2011-01-01', 'Players can respond to the spell or ability whose effect includes proliferating. Once that spell or ability starts to resolve, however, and its controller chooses which permanents and players will get new counters, it\'s too late for anyone to respond.').
card_ruling('inexorable tide', '2011-01-01', 'Whenever you cast a spell, Inexorable Tide\'s ability triggers and goes on the stack on top of it. It will resolve (and you\'ll proliferate) before the spell resolves.').

card_ruling('infantry veteran', '2010-08-15', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').

card_ruling('infectious bloodlust', '2015-06-22', 'If, during its controller’s declare attackers step, the enchanted creature is tapped or is affected by a spell or ability that says it can’t attack, then that creature doesn’t attack. If there’s a cost associated with having that creature attack, its controller isn’t forced to pay that cost. If he or she doesn’t, the creature doesn’t have to attack.').

card_ruling('infectious rage', '2004-10-04', 'When this card returns to the battlefield, it is put onto the battlefield on the creature without targeting it, so it can be placed on a creature with Shroud.').
card_ruling('infectious rage', '2004-10-04', 'You can\'t return this card to the battlefield onto something it can\'t enchant. For example, you can\'t place this card on a permanent with protection from red.').

card_ruling('infernal caretaker', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('infernal contract', '2007-02-01', 'If you attempt to halve a negative life total, you halve 0. This means that the life total stays the same. A life total of -10 would remain -10.').

card_ruling('infernal darkness', '2008-10-01', 'This effect changes the type of mana produced, but not the amount. For example, if a land that\'s tapped for mana would add {W}{W} to its controller\'s mana pool, it adds {B}{B} to that player\'s mana pool instead.').

card_ruling('infernal denizen', '2004-10-04', 'You must sacrifice the Swamps if you can. You can\'t choose not to pay if you have them.').
card_ruling('infernal denizen', '2004-10-04', 'You should always have one creature for your opponent to take (i.e. the Denizen). If by chance you have no creature for your opponent to take, then they don\'t get one. Remember that taking a creature is optional so your opponent is not forced to take the Denizen.').

card_ruling('infernal genesis', '2008-08-01', 'If the card has no mana cost (for instance, if it\'s a land), then its converted mana cost is 0. This means no tokens will be created.').

card_ruling('infernal harvest', '2004-10-04', 'The return of X of your Swamps to your hand is part of the cost and is paid on casting. You do not have a choice to pay this cost zero times or more than one time in order to multiply the effect.').
card_ruling('infernal harvest', '2004-10-04', 'You can use X as being zero.').
card_ruling('infernal harvest', '2004-10-04', 'If X is greater than 0, you can\'t choose zero targets. You must choose between one and X targets. If X is 0, you can’t choose any targets.').

card_ruling('infernal kirin', '2005-06-01', 'If the Spirit or Arcane spell has {X} in the mana cost, then you use the value of {X} on the stack. For example, Shining Shoal costs {X}{W}{W}. If you choose X = 2, then Shining Shoal\'s converted mana cost is 4. Shining Shoal also has an ability that says \"You may exile a white card with converted mana cost X from your hand rather than pay Shining Shoal\'s mana cost\"; if you choose to pay the alternative cost and exile a card with converted mana cost 2, then X is 2 while Shining Shoal is on the stack and its converted mana cost is 4.').

card_ruling('infernal offering', '2014-11-07', 'The sacrifice isn’t optional. If you control at least one creature, you must sacrifice one. If you control no creatures, you won’t sacrifice one and you won’t draw two cards. The same is true for the chosen opponent.').
card_ruling('infernal offering', '2014-11-07', 'You choose which creature card you’re returning to the battlefield, then the chosen opponent chooses which creature card he or she is returning. These choices aren’t made until Infernal Offering resolves. Neither card is a target of Infernal Offering.').
card_ruling('infernal offering', '2014-11-07', 'You may choose the same opponent for each of the effects, or you may choose different opponents. None of the affected players are targets of the spell.').
card_ruling('infernal offering', '2014-11-07', 'You choose the opponents for each effect as the spell resolves.').

card_ruling('infernal plunge', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('infernal plunge', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('infernal scarring', '2015-06-22', 'The player who controlled the creature when it died is the one who will draw a card.').

card_ruling('inferno elemental', '2009-10-01', 'Inferno Elemental\'s ability will trigger for each creature it blocks or becomes blocked by.').
card_ruling('inferno elemental', '2009-10-01', 'Inferno Elemental\'s ability resolves during the declare blockers step, before even first strike damage is assigned. If the damage dealt to another creature this way is lethal damage, that creature will be destroyed before it can deal its combat damage.').
card_ruling('inferno elemental', '2009-10-01', 'The 3 damage that Inferno Elemental deals as a result of its ability is not combat damage. Inferno Elemental will still deal its combat damage as normal (unless all the creatures that blocked or were blocked by it have left the battlefield by the time the combat damage step begins).').

card_ruling('inferno fist', '2014-07-18', 'As soon as Inferno Fist leaves the battlefield, its power bonus stops applying. This means that if you sacrifice Inferno Fist before combat damage is dealt, perhaps to destroy a potential blocker, the creature that was enchanted won’t have the bonus when it deals combat damage.').
card_ruling('inferno fist', '2014-07-18', 'If another player gains control of the enchanted creature, or if Inferno Fist becomes attached to a creature another player controls, Inferno Fist will be put into its owner’s graveyard as a state-based action.').

card_ruling('inferno titan', '2010-08-15', 'You divide the damage as you put Inferno Titan\'s triggered ability on the stack, not as it resolves. Each target must be assigned at least 1 damage. (In other words, as you put the ability on the stack, you choose whether to have it deal 3 damage to a single target, 2 damage to one target and 1 damage to another target, or 1 damage to each of three targets.)').

card_ruling('inferno trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('inferno trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('inferno trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('inferno trap', '2009-10-01', 'Inferno Trap\'s alternative cost condition checks for any damage, not just combat damage.').
card_ruling('inferno trap', '2009-10-01', 'The alternative cost condition doesn\'t check who controlled the creatures that dealt damage to you. Damage dealt to you by your own creatures counts.').
card_ruling('inferno trap', '2009-10-01', 'The alternative cost condition is satisfied only if two different creatures dealt damage to you, not if one creature dealt damage to you twice. The damage may have been dealt to you simultaneously (as with combat damage), or at separate times.').
card_ruling('inferno trap', '2009-10-01', 'You may target any creature, not just one that dealt damage to you this turn.').

card_ruling('infiltration lens', '2011-01-01', 'If the equipped creature becomes blocked by multiple creatures, Infiltration Lens\'s ability triggers that many times.').

card_ruling('infiltrator il-kor', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('infiltrator il-kor', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('infiltrator il-kor', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('infiltrator il-kor', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('infiltrator il-kor', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('infiltrator il-kor', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('infiltrator il-kor', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('infiltrator il-kor', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('infiltrator il-kor', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('infiltrator il-kor', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('infinite authority', '2004-10-04', 'The enchanted creature gets a +1/+1 counter for each creature that triggers the ability.').
card_ruling('infinite authority', '2006-10-15', 'Moving the enchantment after the ability triggers will not affect which creatures are affected.').

card_ruling('infinite obliteration', '2015-06-22', '').

card_ruling('infinite reflection', '2012-05-01', 'Infinite Reflection can enchant a creature controlled by another player.').
card_ruling('infinite reflection', '2012-05-01', 'If Infinite Reflection enters the battlefield attached to a creature and then becomes attached to a different creature before its enters-the-battlefield trigger resolves, each other nontoken creature you control will become a copy of the first creature.').
card_ruling('infinite reflection', '2012-05-01', 'If Infinite Reflection leaves the battlefield or becomes attached to a different creature, none of the copy effects end. Your creatures will still be copies of whatever creature each was a copy of.').
card_ruling('infinite reflection', '2012-05-01', 'If Infinite Reflection enters the battlefield attached to a token creature, the nontoken creatures that become copies of that creature are not themselves tokens.').
card_ruling('infinite reflection', '2012-05-01', 'Creatures you control don\'t copy whether the enchanted creature is tapped or untapped, whether it has any counters on it, whether it has any Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('infinite reflection', '2012-05-01', 'Because creatures you control enter the battlefield as a copy of the enchanted creature, any enters-the-battlefield triggered abilities printed on such creatures won\'t trigger. However, if the enchanted creature has any enters-the-battlefield triggered abilities, those will trigger.').
card_ruling('infinite reflection', '2012-05-01', 'Replacement effects that modify how a permanent enters the battlefield are now applied in the following order: control-changing effects (such as Gather Specimens), then copy effects (such as the abilities of Infinite Reflection and Clone), then all other effects.').
card_ruling('infinite reflection', '2012-05-01', 'If a creature such as Clone is entering the battlefield under your control, there will be two copy effects to apply: the creature\'s own and Infinite Reflection\'s. No matter what order these effects are applied, the creature will be a copy of the enchanted creature when it enters the battlefield.').
card_ruling('infinite reflection', '2012-05-01', 'Other enters-the-battlefield replacement abilities printed on the creature entering the battlefield won\'t be applied because the creature will already be a copy of the enchanted creature at that point (and therefore it won\'t have those abilities). For example, if the enchanted creature is Serra Angel, a creature that normally enters the battlefield tapped will enter the battlefield as an untapped copy of Serra Angel, and a creature that would normally enter the battlefield with counters on it will enter the battlefield as a copy of Serra Angel with no counters.').
card_ruling('infinite reflection', '2012-05-01', 'External abilities may still affect how a creature enters the battlefield. For example, if your opponent controls Urabrask the Hidden, which reads, in part, \"Creatures your opponents control enter the battlefield tapped,\" a creature entering the battlefield under your control will be a tapped copy of the enchanted creature.').
card_ruling('infinite reflection', '2012-05-01', 'If you control more than one Infinite Reflection, you can apply those copy effects in any order. Creatures you control will enter the battlefield as a copy of the one whose copy effect you apply last.').

card_ruling('infuse', '2009-10-01', 'You may target any artifact, creature, or land, not just a tapped one. If the targeted permanent is already untapped as Infuse resolves, Infuse won\'t affect it, but you\'ll still draw a card at the beginning of the next turn\'s upkeep.').

card_ruling('infuse with the elements', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('infuse with the elements', '2015-08-25', '').
card_ruling('infuse with the elements', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('infuse with the elements', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('infuse with the elements', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('ingot chewer', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('inheritance', '2008-10-01', 'If Inheritance and a creature are each put into a graveyard from the battlefield at the same time, Inheritance\'s ability will trigger.').

card_ruling('initiates of the ebon hand', '2004-10-04', 'Mana which \"changes color\" as it goes through the Hand forgets its original source because the old mana gets used up and new mana gets generated.').

card_ruling('ink dissolver', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('ink dissolver', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('ink dissolver', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('ink dissolver', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('ink dissolver', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('ink dissolver', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('ink-eyes, servant of oni', '2005-02-01', 'If, during the combat damage step, a creature is destroyed during combat and Ink-Eyes also deals combat damage to a player, the destroyed creature can be a legal target for Ink-Eyes\'s ability.').

card_ruling('ink-treader nephilim', '2006-02-01', 'Ink-Treader Nephilim\'s ability is mandatory and will create a copy targeting each possible creature whether you want it to or not. You control all the copies of the spell (even if you didn\'t control the original spell), so you choose the order to put them on the stack.').
card_ruling('ink-treader nephilim', '2006-02-01', 'Even though you control the copies, the ability cares about who cast the original spell when determining which other creatures that spell could target. If the original spell has a targeting restriction that refers to \"an opponent,\" the copies will see that from the perspective of the original spell\'s controller. For example, if your opponent casts a spell that says \"Destroy target creature an opponent controls\" targeting your Ink-Treader Nephilim, its ability will create a copy for each other creature the original spell could target: the rest of your creatures. However, since you control the copies, all the copies will be countered for having illegal targets.').
card_ruling('ink-treader nephilim', '2006-02-01', 'Ink-Treader Nephilim\'s ability will trigger off a spell with multiple targets if all of those targets are Ink-Treader Nephilim. For example, Seeds of Strength cast with Ink-Treader Nephilim as each of the three targets will cause the ability to trigger.').

card_ruling('inkfathom witch', '2008-05-01', 'An \"unblocked creature\" is a creature that attacked and wasn\'t blocked. Creatures aren\'t \"blocked\" or \"unblocked\" until the declare blockers step, so activating this ability before then (or after combat ends) will have no effect.').
card_ruling('inkfathom witch', '2008-05-01', 'Creatures stop being unblocked as the combat phase ends. However, they\'ll stay 4/1 until turn ends.').
card_ruling('inkfathom witch', '2008-05-01', 'Inkfathom Witch doesn\'t cause creatures to lose their abilities.').
card_ruling('inkfathom witch', '2008-05-01', 'If Inkfathom Witch is attacking and unblocked, it can make itself 4/1.').
card_ruling('inkfathom witch', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('inkmoth nexus', '2011-06-01', 'If Inkmoth Nexus entered the battlefield this turn and it\'s not a creature, it can be tapped for mana. If it becomes a creature that turn, it can\'t attack or be tapped for mana.').
card_ruling('inkmoth nexus', '2011-06-01', 'If you\'ve controlled Inkmoth Nexus continuously since your most recent turn began and it becomes a creature, it can attack or be tapped for mana.').

card_ruling('inner-flame acolyte', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('inner-flame igniter', '2007-10-01', 'Counts resolutions, not activations. Any such abilities that are still on the stack won\'t count toward the total.').
card_ruling('inner-flame igniter', '2007-10-01', 'When the ability resolves, it counts the number of times that same ability from that this creature has already resolved that turn. It doesn\'t matter who controlled the creature or the previous abilities when they resolved. A copy of this ability (created by Rings of Brighthearth, for example) will count toward the total. Abilities from other creatures with the same name don\'t count towards the total. Neither does an ability that\'s been countered.').
card_ruling('inner-flame igniter', '2007-10-01', 'You get the bonus only the third time the ability resolves. You won\'t get the bonus the fourth, fifth, sixth, or any subsequent times.').

card_ruling('inquisition', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('inquisition of kozilek', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('inquisitor\'s flail', '2011-09-22', 'If a creature is equipped with a second Inquisitor\'s Flail, combat damage dealt by and dealt to that creature will be multiplied by four. A third Inquisitor\'s Flail would multiply the combat damage by eight, and so on.').
card_ruling('inquisitor\'s flail', '2011-09-22', 'If you divide the combat damage dealt by the equipped creature, perhaps because the creature has trample or is dealing combat damage to multiple creatures, you\'ll divide the original amount and then double the results. For example, if a 5/5 creature with trample is blocked by a 2/2 creature, you can assign 2 damage to the blocker and 3 damage to the defending player. These amounts are then doubled to 4 and 6 damage, respectively. You can\'t double the damage to 10 first and then assign 2 to the creature and 8 to the player.').

card_ruling('inquisitor\'s snare', '2008-05-01', 'The prevention effect applies even after the creature leaves the battlefield.').

card_ruling('inside out', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('inside out', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('insolence', '2004-10-04', 'The damaging ability triggers no matter how the creature becomes tapped.').
card_ruling('insolence', '2004-10-04', 'It does not trigger immediately when cast targeting an already tapped creature.').

card_ruling('inspired charge', '2010-08-15', 'Only creatures you control as Inspired Charge resolves are affected.').

card_ruling('inspiring call', '2015-02-25', 'Once a creature gains indestructible, it will have it for the turn, even if it loses all its +1/+1 counters.').
card_ruling('inspiring call', '2015-02-25', 'Creatures you control that have +1/+1 counters put on them after Inspiring Call resolves won’t gain indestructible.').

card_ruling('instigator', '2004-10-04', 'There is no penalty if the creatures are unable to attack.').

card_ruling('instill energy', '2004-10-04', 'If attached to an opponent\'s creature, you can untap their creature during your turn.').
card_ruling('instill energy', '2004-10-04', 'Instill Energy\'s untap ability will not untap the creature until it resolves. This means other spells and abilities can be used before it actually becomes untapped.').
card_ruling('instill energy', '2005-08-01', 'Any Auras (or other effects) which are on the creature that would cause it to not be untapped (or have a cost to be untapped) during untap step do not in any way hinder or imply a cost to use this card\'s ability to untap once during the turn.').

card_ruling('instill furor', '2005-10-01', 'Instill Furor grants a triggered ability that triggers at the end of the creature\'s controller\'s turn.').
card_ruling('instill furor', '2005-10-01', 'Once the ability triggers, nothing that happens to Instill Furor can prevent the ability from resolving.').

card_ruling('insurrection', '2004-10-04', 'You untap all creatures, control all creatures, and give all creatures Haste.').

card_ruling('intellectual offering', '2014-11-07', 'You may choose the same opponent for each of the effects, or you may choose different opponents. None of the affected players are targets of the spell.').
card_ruling('intellectual offering', '2014-11-07', 'You choose the opponents for each effect as the spell resolves.').

card_ruling('interdict', '2004-10-04', 'This card targets the ability, and not the permanent itself.').
card_ruling('interdict', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('interplanar tunnel', '2012-06-01', 'The plane card you put on top of your planar deck is the one you\'ll planeswalk to after planeswalking away from Interplanar Tunnel.').

card_ruling('interpret the signs', '2014-04-26', 'If you reveal a card with converted mana cost 0, such as a land card, you won’t draw any cards. Otherwise, the card you revealed will be the first card you draw.').

card_ruling('intervene', '2004-10-04', 'If a spell is modal, use the target(s) of the current mode only for this check. Targets for the other modes are never selected.').
card_ruling('intervene', '2004-10-04', 'Intervene can only target a spell that is currently targeting a creature.').
card_ruling('intervene', '2004-10-04', 'It means \"targeting at least one creature\" not \"exactly one creature\".').
card_ruling('intervene', '2004-10-04', 'If the spell\'s target leaves the battlefield before this spell resolves, Intervene will be countered since it no longer targets a spell that targets a creature.').

card_ruling('intervention pact', '2007-05-01', 'The delayed triggered ability will trigger regardless of whether any damage is actually prevented.').
card_ruling('intervention pact', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('intet, the dreamer', '2007-02-01', 'After Intet leaves the battlefield, if a card exiled this way is still exiled, you may still look at it. You won\'t be able to play it, though.').
card_ruling('intet, the dreamer', '2007-02-01', 'Once you play a card this way, it leaves the Exile zone and goes to the stack. You won\'t be able to play it again.').

card_ruling('intimidation bolt', '2009-05-01', 'If the targeted creature has become an illegal target by the time Intimidation Bolt resolves, Intimidation Bolt is countered. Creatures will still be able to attack that turn.').
card_ruling('intimidation bolt', '2009-05-01', 'If the targeted creature survives the damage, it can attack this turn, but no other creatures can. If the targeted creature is destroyed, no creatures can attack this turn, including creatures that enter the battlefield later on.').
card_ruling('intimidation bolt', '2009-05-01', 'For the second part of Intimidation Bolt\'s effect to do anything, it must be cast before attackers are declared.').

card_ruling('into the core', '2011-06-01', 'You must be able to target two different artifacts in order to cast Into the Core.').

card_ruling('into the earthen maw', '2010-06-15', 'You may choose zero, one, two, or three targets.').

card_ruling('into the fray', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('into the fray', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('into the fray', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('into the fray', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('into the maw of hell', '2011-09-22', 'Into the Maw of Hell targets both the land and the creature. You can only cast it if you can choose a legal target for both.').
card_ruling('into the maw of hell', '2011-09-22', 'If one of Into the Maw of Hell\'s targets is illegal by the time it resolves, Into the Maw of Hell will still affect the remaining legal target. If both targets are illegal at this time, Into the Maw of Hell will be countered.').

card_ruling('into the roil', '2009-10-01', 'If the targeted permanent is an illegal target by the time Into the Roil resolves, the entire spell is countered. You don\'t draw a card.').

card_ruling('into the wilds', '2013-07-01', 'Putting a land onto the battlefield this way doesn’t count as playing a land. You’ll still be able to play a land during one of your main phases.').
card_ruling('into the wilds', '2013-07-01', 'If the card is not a land card, or you choose not to put it onto the battlefield, it will stay on top of your library. In most cases, you’ll then draw it during your draw step.').

card_ruling('intrepid hero', '2004-10-04', 'The power of the creature is checked on activation and on resolution. If the targeted creature\'s power is not 4 or greater on resolution, the ability is countered.').

card_ruling('introductions are in order', '2010-06-15', 'You choose this ability\'s mode as you put it on the stack.').

card_ruling('intruder alarm', '2004-10-04', 'If multiple creatures enter the battlefield at one time, this ability triggers once for each creature. This doesn\'t matter much, but it can in some cases.').

card_ruling('intuition', '2006-07-15', 'Targets an opponent. You choose an opponent during announcement. If you can\'t target an opponent, you can\'t cast this card.').

card_ruling('inundate', '2008-08-01', 'A hybrid creature that\'s blue and another color is *not* a nonblue creature. Since it\'s blue, it can\'t be nonblue!').

card_ruling('invasive species', '2014-07-18', 'The ability doesn’t target any permanent. You choose which one to return when the ability resolves. No player can respond to this choice once the ability starts resolving.').

card_ruling('invert the skies', '2008-08-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('invert the skies', '2008-08-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('invert the skies', '2008-08-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('invert the skies', '2008-08-01', 'Invert the Skies affects only creatures on the battlefield at the time it resolves. If a creature enters the battlefield later that turn, it won\'t be affected.').
card_ruling('invert the skies', '2008-08-01', 'If a creature changes controllers later in the turn, that won\'t change how Invert the Skies affected it.').

card_ruling('invincible hymn', '2008-10-01', 'Invincible Hymn checks the number of cards in your library just once: when it resolves. It doesn\'t continuously modify your life total as your library size changes.').
card_ruling('invincible hymn', '2008-10-01', 'For your life total to become the appropriate number, you actually gain or lose the necessary amount of life. For example, if your life total is 20 and your library has thirty-two cards in it when Invincible Hymn resolves, Invincible Hymn will cause you to gain 12 life. Other cards that interact with life gain or life loss will interact with Invincible Hymn accordingly.').
card_ruling('invincible hymn', '2010-06-15', 'In a Two-Headed Giant game, Invincible Hymn will essentially set your team\'s life total to the number of cards in your library. For example, suppose your team has 8 life and you have thirty cards in your library. Your life total (being the same as your team\'s life total) changes from 8 life to 30 life, for a net gain of 22 life. Your team\'s life total becomes 30 (22 + 8).').

card_ruling('invoke prejudice', '2004-10-04', 'In a multi-player game, it affects all opposing players.').
card_ruling('invoke prejudice', '2004-10-04', 'This ability triggers when the spell is cast.').

card_ruling('invoke the firemind', '2006-02-01', 'As you cast Invoke the Firemind, first you choose the mode (draw cards or deal damage), then you choose the value of X. You then choose a target, but only if you chose the second mode.').

card_ruling('iona, shield of emeria', '2009-10-01', 'Iona\'s third ability includes permanent spells (artifacts, creatures, enchantments, and planeswalkers), not just instant and sorcery spells. It does not include lands or abilities (such as cycling or unearth).').
card_ruling('iona, shield of emeria', '2009-10-01', 'Once the color is chosen, it\'s too late for opponents to respond by casting spells of that color. Iona is not yet in the battlefield at the time the color is chosen, so, for example, there\'s no way for an opponent to destroy it by casting Doom Blade if the chosen color is black.').

card_ruling('ior ruin expedition', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('ior ruin expedition', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('ire shaman', '2015-02-25', 'If a face-down Ire Shaman attacks and is blocked by one creature, turning it face up won’t cause it to become unblocked.').
card_ruling('ire shaman', '2015-02-25', 'The card exiled with Ire Shaman’s last ability is exiled face up.').
card_ruling('ire shaman', '2015-02-25', 'Playing the card exiled with Ire Shaman’s last ability follows the normal rules for playing the card. You must pay its costs, and you must follow all applicable timing rules. For example, if the card is a creature card, you can cast that card only during your main phase while the stack is empty.').
card_ruling('ire shaman', '2015-02-25', 'Under normal circumstances, you can play a land card exiled with Ire Shaman only if you haven’t played a land yet that turn.').
card_ruling('ire shaman', '2015-02-25', 'If you don’t play the card, it will remain exiled.').
card_ruling('ire shaman', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('ire shaman', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('ire shaman', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('iridescent angel', '2004-10-04', 'This card has Protection from Black, from Blue, from Red, from White, and from Green. This is the definition of \"All Colors\". This means it is possible to use an effect that removes one color of protection, such as just black, on this card.').

card_ruling('iridescent drake', '2004-10-04', 'If you pick an Aura that can\'t legally enchant this card, then the enchantment stays in the graveyard.').
card_ruling('iridescent drake', '2004-10-04', 'You must pick an Aura even if there are only undesirable ones in the graveyard.').
card_ruling('iridescent drake', '2008-04-01', 'This card used to have errata that kept it from triggering if it wasn\'t cast from your hand. This errata has since been removed, so the ability will trigger regardless of where the Drake was before entering the battlefield.').

card_ruling('iroas, god of victory', '2014-04-26', 'Iroas’s last ability prevents all damage dealt to attacking creatures you control, not just combat damage.').
card_ruling('iroas, god of victory', '2014-04-26', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('iroas, god of victory', '2014-04-26', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('iroas, god of victory', '2014-04-26', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('iroas, god of victory', '2014-04-26', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('iroas, god of victory', '2014-04-26', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('iroas, god of victory', '2014-04-26', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('iroas, god of victory', '2014-04-26', 'Your devotion to two colors is equal to the number of mana symbols that are the first color, the second color, or both colors among the mana costs of permanents you control. Specifically, a hybrid mana symbol counts only once toward your devotion to its two colors. For example, if the only nonland permanents you control are Pharika, God of Affliction and Golgari Guildmage (whose mana cost is {B/G}{B/G}), your devotion to black and green is four.').
card_ruling('iroas, god of victory', '2014-04-26', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('iroas, god of victory', '2014-04-26', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('iroas, god of victory', '2014-04-26', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('iron will', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('ironclaw buzzardiers', '2006-09-25', '\"Can\'t block\" abilities apply only as blockers are declared. After Ironclaw Buzzardiers blocks, increasing the blocked creature\'s power won\'t make it become unblocked.').

card_ruling('irresistible prey', '2010-06-15', 'You can target a creature you don\'t control or that otherwise couldn\'t attack that turn.').
card_ruling('irresistible prey', '2010-06-15', 'If the targeted creature is an illegal target by the time Irresistible Prey resolves, the spell is countered. You won\'t draw a card.').
card_ruling('irresistible prey', '2010-06-15', 'The creature affected by Irresistible Prey doesn\'t have to attack that turn.').
card_ruling('irresistible prey', '2010-06-15', 'If the affected creature attacks, the defending player must assign at least one blocker to it during the declare blockers step, unless that player controls no creatures that could block it.').
card_ruling('irresistible prey', '2010-06-15', 'If there are multiple combat phases in a turn, the affected creature must be blocked only in the first one in which it\'s able to be blocked.').

card_ruling('island fish jasconius', '2006-05-01', 'Reworded so that it triggers only once at beginning of upkeep, instead of being an activated ability usable any time during your upkeep. You can no longer make infinite combos with this ability.').

card_ruling('island of wak-wak', '2004-10-04', 'This is not an Island.').
card_ruling('island of wak-wak', '2005-11-01', 'Sets power to 0 instead of giving the creature -X/-0.').
card_ruling('island of wak-wak', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('island sanctuary', '2004-10-04', 'If you get multiple draws or you use a spell or ability during the draw step to draw extra cards, you can have the replacement effect apply to any one or all of those. You need only have it apply once to get the effect. If you skip more than one, there is no additional effect.').
card_ruling('island sanctuary', '2004-10-04', 'If the replacement effect is applied, the effect will continue until your next turn even if this card leaves the battlefield.').
card_ruling('island sanctuary', '2004-10-04', 'Since the draw is replaced, you can\'t use the same draw to do other things.').

card_ruling('isle of vesuva', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('isle of vesuva', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('isle of vesuva', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('isle of vesuva', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('isle of vesuva', '2009-10-01', 'As a token is created by Isle of Vesuva\'s first ability, it checks the printed values of the creature it\'s copying, as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, and so on.').
card_ruling('isle of vesuva', '2009-10-01', 'If the creature that caused Isle of Vesuva\'s first ability to trigger has already left the battlefield by the time the ability resolves, the ability still creates a token. It will have the copiable values of the characteristics of that nontoken creature as it last existed on the battlefield.').
card_ruling('isle of vesuva', '2009-10-01', 'The {C} ability will destroy all creatures with the same name as the target creature, not just tokens created by Isle of Vesuva\'s first ability. It doesn\'t matter who controls them.').
card_ruling('isle of vesuva', '2009-10-01', 'If the permanent targeted by the {C} ability has become an illegal target by the time the ability resolves, the ability is countered. Nothing will be destroyed.').
card_ruling('isle of vesuva', '2009-10-01', 'A token\'s name is the same as the creature types listed by the effect that created it, unless the token copies a permanent or that effect specifically gives that token a name. For example, the effect of The Fourth Sphere\'s {C} ability creates a token with the name \"Zombie.\"').
card_ruling('isle of vesuva', '2009-10-01', 'A face-down creature has no name, so it doesn\'t have the same name as anything else.').
card_ruling('isle of vesuva', '2013-07-01', 'If the permanent targeted by the {C} ability isn\'t destroyed (because it regenerates, or because it has indestructible), all other permanents with the same name as it will still be destroyed.').

card_ruling('isleback spawn', '2008-05-01', 'It doesn\'t matter which library has twenty or fewer cards in it, and you don\'t have to specify a library. If multiple libraries have twenty or fewer cards in them, Isleback Spawn will still get its bonus only once.').

card_ruling('isochron scepter', '2004-10-04', 'If the imprinted card leaves the Exile zone while the ability to make a copy is on the stack, then no copy will be made.').
card_ruling('isochron scepter', '2004-10-04', 'If this card leaves the battlefield while the ability to make a copy is on the stack, the ability will still make a copy using the last-known-information rule.').
card_ruling('isochron scepter', '2004-12-01', 'Isochron Scepter\'s second ability creates a copy of the imprinted card in the Exile zone (that\'s where the imprinted instant card is), then allows you to cast it without paying its mana cost.').
card_ruling('isochron scepter', '2004-12-01', 'You cast the copy while this ability is resolving, and still on the stack. Normally, you\'re not allowed to cast spells or activate abilities at this time. Isochron Scepter\'s ability provides an exception.').
card_ruling('isochron scepter', '2004-12-01', 'You don\'t pay the spell\'s mana cost. If the spell has X in its mana cost, X is 0. You do pay any additional costs for that spell. You can\'t use any alternative costs.').
card_ruling('isochron scepter', '2004-12-01', 'If the copied card is a split card, you may choose to cast either side of the split card, but not both. The split cards Fire/Ice, Illusion/Reality, Night/Day, Stand/Deliver, and Wax/Wane all have at least one side with converted mana cost 2 or less.)').
card_ruling('isochron scepter', '2004-12-01', 'You can\'t cast the copy if an effect prevents you from casting instants or from casting that particular instant.').
card_ruling('isochron scepter', '2004-12-01', 'You can\'t cast the copy unless all of its targets can be chosen.').
card_ruling('isochron scepter', '2004-12-01', 'If you don\'t want to cast the copy, you can choose not to; the copy ceases to exist the next time state-based actions are checked.').
card_ruling('isochron scepter', '2005-03-01', 'The creation of the copy and then the casting of the copy are both optional.').

card_ruling('isperia the inscrutable', '2006-05-01', 'The card is named when the ability resolves. The named card can\'t be played after it\'s named but before the player\'s hand is revealed.').

card_ruling('isperia\'s skywatch', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('isperia\'s skywatch', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('isperia\'s skywatch', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('isperia\'s skywatch', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('isperia\'s skywatch', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('isperia\'s skywatch', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('isperia, supreme judge', '2012-10-01', 'In a Two-Headed Giant game, Isperia\'s ability will trigger whenever a creature attacks your team or a planeswalker you control. It won\'t trigger when a creature attacks a planeswalker controlled by your teammate.').

card_ruling('it that betrays', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('it that betrays', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('it that betrays', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('it that betrays', '2010-06-15', 'The second ability triggers whenever an opponent sacrifices a nontoken permanent for any reason, not just due to the annihilator ability.').
card_ruling('it that betrays', '2010-06-15', 'It doesn\'t matter whose graveyard the permanent is put into, only that it was last controlled by, and sacrificed by, an opponent.').
card_ruling('it that betrays', '2010-06-15', 'If an opponent sacrifices a nontoken permanent as part of paying the cost of a spell or ability, the second ability triggers and goes on the stack on top of it. This ability will resolve, causing you to return the card to the battlefield before the other spell or ability resolves.').
card_ruling('it that betrays', '2010-06-15', 'When the second ability resolves, you must return the card to the battlefield, even if you don\'t want to.').
card_ruling('it that betrays', '2010-06-15', 'If an opponent sacrifices an Aura, you\'ll choose what it enchants as you return it to the battlefield. No player can respond to the choice. Since an Aura doesn\'t target anything if it isn\'t cast as a spell, you can enchant a permanent with shroud this way.').
card_ruling('it that betrays', '2010-06-15', 'If the sacrificed permanent that caused the second ability to trigger somehow leaves the graveyard before the ability resolves (possibly because it was returned to the battlefield by the ability of another It That Betrays), the ability simply won\'t do anything when it resolves.').

card_ruling('iterative analysis', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('iterative analysis', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('iterative analysis', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('iterative analysis', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('iterative analysis', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('iterative analysis', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('iterative analysis', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('iterative analysis', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('iterative analysis', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('iterative analysis', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('iterative analysis', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('iterative analysis', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('iterative analysis', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('ith, high arcanist', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('ith, high arcanist', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('ith, high arcanist', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('ith, high arcanist', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('ith, high arcanist', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('ith, high arcanist', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ith, high arcanist', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('ith, high arcanist', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ith, high arcanist', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('ith, high arcanist', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('ith, high arcanist', '2013-07-01', 'In a Commander game where this card is your commander, you cannot suspend it from the Command zone.').

card_ruling('ivory gargoyle', '2008-10-01', 'The player who controlled Ivory Gargoyle when it was put into a graveyard will skip his or her next draw step even if Ivory Gargoyle is returned to the battlefield under a different player\'s control (because it\'s owned by someone else) or isn\'t returned to the battlefield at all (because it left the graveyard before the end of the turn, for example).').
card_ruling('ivory gargoyle', '2008-10-01', 'The second ability can be activated only if Ivory Gargoyle is on the battlefield.').

card_ruling('ivory giant', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('ivory giant', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('ivory giant', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('ivory giant', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('ivory giant', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('ivory giant', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ivory giant', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('ivory giant', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('ivory giant', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('ivory giant', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('ivory guardians', '2009-10-01', 'The ability of an Ivory Guardians will affect itself as well as all other creatures named Ivory Guardians (not just the ones you control).').
card_ruling('ivory guardians', '2009-10-01', 'Each ability applies a maximum of once, even if an opponent controls multiple nontoken red permanents or multiple opponents each control a nontoken red permanent.').
card_ruling('ivory guardians', '2009-10-01', 'The abilities from multiple Ivory Guardians are cumulative. For example, if you control two different Ivory Guardians and an opponent controls a nontoken red permanent, each of your Ivory Guardians will be 5/5.').

card_ruling('ivory tower', '2004-10-04', 'If you have four or fewer cards in your hand when Ivory Tower\'s ability resolves, the ability has no effect.').

card_ruling('ivorytusk fortress', '2014-09-20', 'Each creature you control with a +1/+1 counter on it untaps at the same time as the active player’s permanents. You can’t choose to not untap them at that time.').
card_ruling('ivorytusk fortress', '2014-09-20', 'Effects that state a creature you control doesn’t untap during your untap step won’t apply during another player’s untap step.').
card_ruling('ivorytusk fortress', '2014-09-20', 'Controlling more than one Ivorytusk Fortress doesn’t allow you to untap any creature more than once during a single untap step.').

card_ruling('ivy lane denizen', '2013-01-24', 'The green creature that entered the battlefield can be chosen as the target of Ivy Lane Denizen\'s ability.').
card_ruling('ivy lane denizen', '2013-01-24', 'Ivy Lane Denizen can be chosen as the target of its own ability.').

card_ruling('ivy seer', '2004-10-04', 'You can reveal zero cards and give +0/+0.').

card_ruling('ixidor, reality sculptor', '2004-10-04', 'When you turn a card face up, its controller does not have to pay its Morph cost.').

card_ruling('ixidron', '2006-09-25', 'Creatures turned face down by Ixidron are 2/2 creatures with no text, no name, no subtypes, no expansion symbol, and no mana cost.').
card_ruling('ixidron', '2006-09-25', 'If Ixidron and another creature are entering the battlefield at the same time, the other creature enters the battlefield face up.').
card_ruling('ixidron', '2006-09-25', 'The controller of a face-down creature can look at it at any time, even if it doesn\'t have morph. Other players can\'t, but the rules for face-down permanents state that \"you must ensure at all times that your face-down spells and permanents can be easily differentiated from each other.\" As a result, all players must be able to figure out what each of the creatures Ixidron turned face down is.').
card_ruling('ixidron', '2006-09-25', 'You turn the creatures face-down *as* Ixidron enters the battlefield. There is never a moment when Ixidron is on the battlefield and the creatures are face-up. If a creature on the battlefield has a \"whenever another creature enters the battlefield\" ability, it won\'t trigger because that creature will be face down before Ixidron enters the battlefield.').
card_ruling('ixidron', '2006-09-25', 'Turning a face-down creature face-down typically has no effect; the creature\'s status is unchanged.').

card_ruling('izzet boilerworks', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('izzet guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('izzet guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('izzet keyrune', '2012-10-01', 'If Izzet Keyrune becomes a creature in a way other than its own activated ability, its last ability will still work.').
card_ruling('izzet keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('izzet keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('izzet staticaster', '2012-10-01', 'Izzet Staticaster\'s activated ability has only one target. Other creatures with that name are not targeted. For example, a creature with hexproof will be dealt damage if it has the same name as the target creature.').
card_ruling('izzet staticaster', '2012-10-01', 'The name of a creature token is the same as its creature types unless the token is a copy of another creature or the effect that created the token specifically gives it a different name. For example, a 3/3 Centaur creature token is named “Centaur.”').

card_ruling('izzet steam maze', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('izzet steam maze', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('izzet steam maze', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('izzet steam maze', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('izzet steam maze', '2009-10-01', 'The effect of Izzet Steam Maze\'s first ability is mandatory. You must copy the spell whether you want to or not.').
card_ruling('izzet steam maze', '2009-10-01', 'Izzet Steam Maze\'s first ability will copy any instant or sorcery spell, not just one with targets.').
card_ruling('izzet steam maze', '2009-10-01', 'The controller of the spell that caused Izzet Steam Maze\'s first ability to trigger also controls the copy. The copy resolves before the original spell.').
card_ruling('izzet steam maze', '2009-10-01', 'A copy is created even if the spell that caused Izzet Steam Maze\'s first ability to trigger has been countered by the time that ability resolves.').
card_ruling('izzet steam maze', '2009-10-01', 'The copy will have the same targets as the spell it\'s copying unless its controller chooses new ones. That player may change any number of the targets, including all of them or none of them. The new targets must be legal.').
card_ruling('izzet steam maze', '2009-10-01', 'If the spell that\'s copied is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. Its controller can\'t choose a different one.').
card_ruling('izzet steam maze', '2009-10-01', 'If the spell that\'s copied has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('izzet steam maze', '2009-10-01', 'The controller of the copy can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too.').
card_ruling('izzet steam maze', '2009-10-01', 'The copy that Izzet Steam Maze\'s first ability creates is created on the stack, so it\'s not \"cast.\" Abilities that trigger when a player casts a spell (like Izzet Steam Maze\'s first ability itself) won\'t trigger.').

