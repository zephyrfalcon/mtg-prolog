% Rulings

card_ruling('ma chao, western warrior', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('macetail hystrodon', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('mad auntie', '2007-10-01', 'The second ability can target any Goblin permanent except the Mad Auntie whose ability is being activated. It can target a different Mad Auntie.').

card_ruling('mad prophet', '2012-05-01', 'Discarding a card is part of the ability\'s activation cost. You can\'t activate the ability if you have no cards in hand.').

card_ruling('maddening imp', '2004-10-04', 'If a creature can\'t attack because of some restriction, or because it is tapped, it is destroyed at the end of the turn.').

card_ruling('madrush cyclops', '2009-05-01', 'Madrush Cyclops gives itself haste.').

card_ruling('maelstrom archangel', '2009-02-01', 'If you want to cast a card this way, you cast it as part of the resolution of Maelstrom Angel\'s triggered ability. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other casting restrictions are not (such as \"Cast [this card] only before attackers are declared\").').
card_ruling('maelstrom archangel', '2009-02-01', 'If you cast a card this way, you\'re casting it as a spell. It can be countered.').
card_ruling('maelstrom archangel', '2009-02-01', 'If you cast a card with X in its cost this way, X must be 0.').
card_ruling('maelstrom archangel', '2009-02-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs, such as evoke or the alternative cost provided by the morph ability. However, you can pay optional additional costs, such as conspire, and you must still pay mandatory additional costs, such as the one on Goldmeadow Stalwart.').

card_ruling('maelstrom djinn', '2007-05-01', 'If Maelstrom Djinn turns face up and then is somehow turned face down, it will still have vanishing and time counters. The vanishing ability will continue to work as normal. If it\'s turned face up again, it will get more time counters on it, but it will also get another copy of vanishing. Both abilities would trigger during its controller\'s upkeep.').

card_ruling('maelstrom nexus', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('maelstrom nexus', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('maelstrom nexus', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('maelstrom nexus', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('maelstrom nexus', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('maelstrom nexus', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('maelstrom nexus', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').
card_ruling('maelstrom nexus', '2009-05-01', 'Maelstrom Nexus affects the first spell you cast each turn, not just the first spell you cast on your own turn.').
card_ruling('maelstrom nexus', '2009-05-01', 'If the first spell you cast in a turn already has cascade, both cascade abilities will trigger separately. Deal with them one at a time: First one cascade ability will resolve, and you\'ll cast the applicable card if you want. That new spell will resolve. Then the other cascade ability will resolve in the same way. Finally, the original spell will resolve.').
card_ruling('maelstrom nexus', '2009-05-01', 'This effect takes into account spells that were cast earlier in the turn that Maelstrom Nexus entered the battlefield, including any spells still on the stack. If you\'ve already cast any spells that turn (including Maelstrom Nexus itself), this ability won\'t give any of your spells cascade that turn.').
card_ruling('maelstrom nexus', '2009-05-01', 'A spell\'s converted mana cost is determined solely by the mana symbols printed in its upper right corner. If its mana cost includes {X}, take the chosen value of X into account. Ignore any alternative costs, additional costs, cost increases, or cost reductions.').

card_ruling('maelstrom pulse', '2009-05-01', 'Maelstrom Pulse has only one target. If that permanent is an illegal target as Maelstrom Pulse tries to resolve, it will be countered and none of its effects will happen. Nothing will be destroyed.').
card_ruling('maelstrom pulse', '2009-05-01', 'The name of a creature token is the same as its creature types unless the token is a copy of another creature or the effect that created the token specifically gives it a different name. For example, a 1/1 Soldier creature token is named “Soldier.”').
card_ruling('maelstrom pulse', '2009-05-01', 'A face-down creature has no name, so it doesn\'t have the same name as anything else.').
card_ruling('maelstrom pulse', '2013-07-01', 'If Maelstrom Pulse resolves but the targeted permanent isn\'t destroyed (perhaps because it regenerates or has indestructible), all other permanents with the same name as it will still be destroyed (unless they also have indestructible.').

card_ruling('maelstrom wanderer', '2012-06-01', 'Maelstrom Wanderer gives itself haste.').
card_ruling('maelstrom wanderer', '2012-06-01', 'Each instance of cascade triggers and resolves separately. The spell you cast due to the first cascade ability will go on the stack on top of the second cascade ability. That spell will resolve before you exile cards for the second cascade ability. Casting that spell may cause additional cascade abilities to trigger.').
card_ruling('maelstrom wanderer', '2012-06-01', 'No matter what spell you cast with the first cascade trigger (or any cascade triggers that result from casting that spell), the second cascade trigger will look for a spell with converted mana cost less than Maelstrom Wanderer\'s converted mana cost of 8.').

card_ruling('mage il-vec', '2004-10-04', 'You must discard a card as part of the activation cost. You can\'t announce the ability unless you have a card in hand.').

card_ruling('mage slayer', '2009-05-01', 'This ability triggers and resolves in the declare attackers step. The creature will still deal its combat damage as normal later on during the combat phase.').
card_ruling('mage slayer', '2009-05-01', 'When the equipped creature attacks, the first ability will trigger. When that ability resolves, that creature will deal damage equal to its power to defending player, even if Mage Slayer has left the battlefield or is equipping a different creature by that time.').

card_ruling('mage\'s guile', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('mage-ring bully', '2015-06-22', 'If, during its controller’s declare attackers step, Mage-Ring Bully is tapped or is affected by a spell or ability that says it can’t attack, then Mage-Ring Bully doesn’t attack. If there’s a cost associated with having it attack, its controller isn’t forced to pay that cost. If he or she doesn’t, Mage-Ring Bully doesn’t have to attack.').
card_ruling('mage-ring bully', '2015-06-22', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('mage-ring bully', '2015-06-22', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('mage-ring bully', '2015-06-22', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('mage-ring responder', '2015-06-22', 'The last ability triggers and resolves before blockers are declared. If the target creature is destroyed by the damage, it won’t be on the battlefield to block.').

card_ruling('magebane armor', '2009-10-01', 'Magebane Armor can equip a creature that doesn\'t have flying. The \"lose flying\" effect just won\'t do anything in that case.').
card_ruling('magebane armor', '2009-10-01', 'Magebane Armor causes the equipped creature to lose flying at the time it became equipped. Any ability that grants that creature flying that starts to work later (such as a Jump that resolves later, or a Levitation that comes under your control later) will work normally.').
card_ruling('magebane armor', '2009-10-01', 'Combat damage is the damage that\'s dealt automatically by attacking and blocking creatures, even if it\'s redirected (by Harm\'s Way, perhaps). Noncombat damage is any other damage (generally, it\'s damage dealt as the result of a spell or ability).').

card_ruling('magewright\'s stone', '2006-05-01', 'If an activated ability with {T} in its cost is granted to a creature by an effect (such as Hypervolt Grasp\'s effect, for instance), Magewright\'s Stone can untap that creature.').
card_ruling('magewright\'s stone', '2006-05-01', 'This looks for the {T} symbol. The phrase \"tap an untapped creature you control\" isn\'t the same and doesn\'t count.').

card_ruling('magical hack', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('magical hack', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('magical hack', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('magical hack', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('magical hack', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('magical hack', '2004-10-04', 'You can\'t change proper nouns (i.e. card names) such as \"Island Fish Jasconius\".').
card_ruling('magical hack', '2004-10-04', 'Changing the text of a spell will not allow you to change the targets of the spell because the targets were chosen when the spell was cast. The text change will (probably) cause it to be countered since the targets will be illegal.').
card_ruling('magical hack', '2004-10-04', 'If you change the text of a spell which is to become a permanent, the permanent will retain the text change until the effect wears off.').
card_ruling('magical hack', '2004-10-04', 'It can be used to change a land\'s type from one basic land type to another. For example, Forest can be changed to Island so it produces blue mana. It doesn\'t change the name of any permanent.').

card_ruling('magister of worth', '2014-05-29', 'Because the votes are cast in turn order, each player will know the votes of players who voted beforehand.').
card_ruling('magister of worth', '2014-05-29', 'You must vote for one of the available options. You can’t abstain.').
card_ruling('magister of worth', '2014-05-29', 'No player votes until the spell or ability resolves. Any responses to that spell or ability must be made without knowing the outcome of the vote.').
card_ruling('magister of worth', '2014-05-29', 'Players can’t do anything after they finishing voting but before the spell or ability that included the vote finishes resolving.').
card_ruling('magister of worth', '2014-05-29', 'The phrase “the vote is tied” refers only to when there is more than one choice that received the most votes. For example, if a 5-player vote from among three different choices ends 3 votes to 1 vote to 1 vote, the vote isn’t tied.').

card_ruling('magister sphinx', '2009-02-01', 'This ability is not optional.').
card_ruling('magister sphinx', '2009-02-01', 'For a player\'s life total to become 10, what actually happens is that the player gains or loses the appropriate amount of life. For example, if the targeted player\'s life total is 4 when this ability resolves, it will cause that player to gain 6 life; alternately, if the targeted player\'s life total is 17 when this ability resolves, it will cause that player to lose 7 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('magister sphinx', '2010-06-15', 'In a Two-Headed Giant game, this ability basically causes the team\'s life total to become 10, but only the targeted player is considered to have actually gained or lost life.').

card_ruling('magistrate\'s scepter', '2004-10-04', 'If multiple \"extra turn\" effects resolve in the same turn, take them in the reverse of the order that the effects resolved.').

card_ruling('magma burst', '2004-10-04', 'The second target must be different from the first one.').
card_ruling('magma burst', '2004-10-04', 'You choose a second target only if you choose to pay the Kicker cost.').

card_ruling('magma jet', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('magma jet', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('magma jet', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('magma jet', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('magma sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('magma sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('magma spray', '2008-10-01', 'The second sentence will exile the targeted creature if it would be put into a graveyard this turn for any reason, not just due to lethal damage. It applies to the targeted creature even if Magma Spray deals no damage to it (due to a prevention effect) or Magma Spray deals damage to a different creature (due to a redirection effect).').
card_ruling('magma spray', '2014-04-26', 'The second sentence will exile the target creature if it would die this turn for any reason, not just due to lethal damage. It applies to the target creature even if Magma Spray deals no damage to it (due to a prevention effect) or Magma Spray deals damage to a different creature (due to a redirection effect).').

card_ruling('magmaquake', '2012-07-01', 'Magmaquake doesn\'t target any creature or planeswalker. For example, it will deal damage to a creature without flying that has hexproof.').

card_ruling('magmatic chasm', '2015-02-25', 'No creature without flying will be able to block that turn, including creatures that lose flying after Magmatic Chasm resolves and creatures without flying that enter the battlefield after Magmatic Chasm resolves.').

card_ruling('magmatic core', '2006-07-15', 'The damage is divided when Magmatic Core\'s ability triggers. Increasing or decreasing the number of age counters on Magmatic Core after the ability triggers but before it resolves has no effect on the amount of damage that\'s dealt.').
card_ruling('magmatic core', '2006-07-15', 'If Magmatic Core has no age counters on it, the only number of targets the ability can have is zero.').
card_ruling('magmatic core', '2006-07-15', 'If Magmatic Core has at least one age counter on it, the number of targets must be at least one and at most X. Each target must be assigned at least 1 damage.').
card_ruling('magmatic core', '2006-07-15', 'Magmatic Core\'s effect is mandatory. At the end of your turn, if the only creatures on the battlefield are yours, you must have Magmatic Core deal damage to at least one of them.').

card_ruling('magmatic force', '2011-09-22', 'The controller of Magmatic Force when its ability triggers always chooses the target, even when the ability triggers during another player\'s upkeep.').
card_ruling('magmatic force', '2011-09-22', 'In a Two-Headed Giant game, this ability triggers once at the beginning of each team\'s upkeep.').

card_ruling('magmatic insight', '2015-06-22', 'You must discard exactly one land card to cast Magmatic Insight. You can’t cast it without discarding a land card, and you can’t discard additional cards.').

card_ruling('magmaw', '2010-06-15', 'You may sacrifice Magmaw to pay its own activation cost. If you do, Magmaw will still deal 1 damage to the targeted creature or player (unless, for some reason, you targeted Magmaw itself, in which case the ability will be countered for having no legal targets).').

card_ruling('magnetic mine', '2011-06-01', 'Magnetic Mine will not trigger when it is put into a graveyard from the battlefield itself, although it will trigger if another artifact is put into the graveyard from the battlefield at the same time.').

card_ruling('magnetic theft', '2004-12-01', 'Magnetic Theft can cause an Equipment one player controls to be attached to a creature another player controls. The controller of the Equipment can pay the equip cost to move the Equipment to a creature he or she controls (but only any time that player could cast a sorcery).').

card_ruling('magnigoth treefolk', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('magnigoth treefolk', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('magnigoth treefolk', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('magosi, the waterveil', '2009-10-01', 'As Magosi\'s third ability resolves, you\'ll skip your next turn even if you don\'t put an eon counter on Magosi (because it\'s left the battlefield by then, perhaps).').
card_ruling('magosi, the waterveil', '2009-10-01', 'You can activate Magosi\'s fourth ability only if it has an eon counter on it.').
card_ruling('magosi, the waterveil', '2009-10-01', 'You return Magosi to its owner\'s hand as part of the cost to activate its fourth ability. If Magosi has more than one eon counter on it, you can\'t activate the ability, untap it, then activate it again.').
card_ruling('magosi, the waterveil', '2009-10-01', 'If you somehow activate Magosi\'s third and fourth abilities during the same turn, the effects will cancel each other out. The turn you skip is the next one you would take, which is the turn created by Magosi\'s fourth ability. It doesn\'t matter in which order the abilities resolved.').
card_ruling('magosi, the waterveil', '2009-10-01', 'If you take an extra turn in a Two-Headed Giant game, you entire team takes the extra turn.').

card_ruling('magus of the abyss', '2007-05-01', 'The player who controls Magus of the Abyss when the ability triggers controls the ability, but the player whose upkeep it is chooses the target.').

card_ruling('magus of the arena', '2007-02-01', 'If either target leaves the battlefield before the ability resolves, the ability taps the remaining target, but no damage is dealt. If both targets leave the battlefield, the ability is countered.').
card_ruling('magus of the arena', '2007-02-01', 'Magus of the Arena\'s controller always chooses his or her creature first.').
card_ruling('magus of the arena', '2007-02-01', 'If an effect changes the targets of this ability, the second target can be changed to any creature the chosen opponent controls. The choice of opponent can\'t be changed.').
card_ruling('magus of the arena', '2007-02-01', 'Tapped creatures can be targeted. Tapping them again will do nothing, but they\'ll still deal damage.').

card_ruling('magus of the disk', '2013-07-01', 'If an effect gives Magus of the Disk indestructible or regenerates it, it won\'t be destroyed by its ability.').

card_ruling('magus of the future', '2007-05-01', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('magus of the future', '2007-05-01', 'If the top card of your library is a land, you can play it if you could play a land.').
card_ruling('magus of the future', '2007-05-01', 'If the top card of your library has morph, you can cast that card face down for {3}. However, all players will have seen which card it is before the spell is cast.').
card_ruling('magus of the future', '2007-05-01', 'As soon as you finish playing the card on top of your library, reveal the next card in your library.').
card_ruling('magus of the future', '2007-05-01', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').

card_ruling('magus of the library', '2004-10-04', 'You may tap multiples of these in response to each other because the requirement for 7 cards is checked only at the time the ability is announced and not again when it resolves.').

card_ruling('magus of the mirror', '2006-09-25', 'When the life totals are exchanged, each player gains or loses the amount of life necessary to equal the other player\'s previous life total. For example, if player A has 5 life and player B has 3 life before the exchange, player A will lose 2 life and player B will gain 2 life. Replacement effects may modify these gains and losses, and triggered abilities may trigger on them.').
card_ruling('magus of the mirror', '2011-01-01', 'If an effect says that a player can\'t lose life, that player can\'t exchange life totals with a player who has a lower life total; in that case, the exchange won\'t happen.').

card_ruling('magus of the moon', '2008-04-01', 'Nonbasic lands that are turned into Mountains are still nonbasic. Magus of the Moon does nothing to change the land\'s supertype.').
card_ruling('magus of the moon', '2008-04-01', 'Lands that are turned into Mountains will have \"{T}: Add {R} to your mana pool.').

card_ruling('magus of the unseen', '2004-10-04', 'You may target an untapped artifact with this ability.').
card_ruling('magus of the unseen', '2004-10-04', 'The artifact taps after returning to the opponent (if it is not already tapped), so any abilities triggered off it tapping happen at that time.').
card_ruling('magus of the unseen', '2008-10-01', 'You tap the artifact when you lose control of it for any reason -- because Magus of the Unseen\'s effect ends, or because a spell or ability causes another player to gain control of it.').
card_ruling('magus of the unseen', '2008-10-01', 'The artifact will gain haste whether or not it\'s a creature. Of course, if it\'s not a creature, haste will have no particular effect.').

card_ruling('magus of the vineyard', '2007-05-01', 'This ability is not optional.').

card_ruling('make a wish', '2011-09-22', 'Make a Wish isn\'t put into its owner\'s graveyard until it is finished resolving, so it can\'t be returned by its own effect.').
card_ruling('make a wish', '2011-09-22', 'The cards aren\'t randomly chosen until Make a Wish resolves.').
card_ruling('make a wish', '2011-09-22', 'If you only have one card in your graveyard when Make a Wish resolves, that card will be returned to your hand.').

card_ruling('makindi patrol', '2015-08-25', 'Vigilance matters only as attackers are declared. Giving a creature vigilance after that point won’t untap it, even if it’s attacking.').
card_ruling('makindi patrol', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('makindi patrol', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('makindi shieldmate', '2009-10-01', 'This ability triggers on this creature entering the battlefield, so it will enter the battlefield with its unmodified power and toughness, and then receive a +1/+1 counter a short time later. It does not enter the battlefield with the +1/+1 counter already on it.').

card_ruling('makindi sliderunner', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('makindi sliderunner', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('makindi sliderunner', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('malach of the dawn', '2007-02-01', 'This is the timeshifted version of Ghost Ship.').

card_ruling('malakir familiar', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('malakir familiar', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('malakir familiar', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('malfegor', '2009-02-01', 'This ability is not optional. You must discard your entire hand no matter how many creatures your opponents control.').
card_ruling('malfegor', '2009-02-01', 'After you discard your hand and the number of creatures each opponent must sacrifice has been determined, each opponent in turn order, starting with the player whose turn it is if that player is your opponent, chooses which creatures he or she will sacrifice, then all the chosen creatures are all sacrificed at the same time.').

card_ruling('malicious affliction', '2014-11-07', 'The copy of Malicious Affliction is created on the stack. It’s not cast, so the copy won’t cause the morbid ability to trigger again.').
card_ruling('malicious affliction', '2014-11-07', 'By default, the copy has the same target as the original spell. You don’t have to change this target if you don’t want to or can’t (perhaps if there are no other nonblack creatures on the battlefield). If you do change the target, the new target must be a legal target for Malicious Affliction.').

card_ruling('malicious intent', '2012-05-01', 'Once a creature has been declared as a blocking creature, making it unable to block will have no effect.').

card_ruling('malignant growth', '2004-10-04', 'In multiplayer games it affects all opponents.').
card_ruling('malignant growth', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('malignus', '2012-05-01', 'If Malignus becomes blocked by a creature with protection from red, the damage Malignus deals to that creature won\'t be prevented.').

card_ruling('mammoth umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('mammoth umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('mammoth umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('mammoth umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('mammoth umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('mammoth umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('mammoth umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('mammoth umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('mammoth umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('mammoth umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('mammoth umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('man-o\'-war', '2004-10-04', 'It can choose itself. It has to if there are no other creatures.').

card_ruling('mana bloom', '2012-10-01', 'Mana Bloom\'s last ability will trigger only if it has no charge counters on it at the beginning of your upkeep. If it has one charge counter on it as your turn begins, you won\'t have priority to remove the counter until after this check.').

card_ruling('mana breach', '2004-10-04', 'The ability itself is controlled by Mana Breach\'s controller.').
card_ruling('mana breach', '2004-10-04', 'The person who cast the spell chooses which of their lands is affected.').

card_ruling('mana chains', '2013-04-15', 'If a permanent has multiple instances of cumulative upkeep, each triggers separately. However, the age counters are not connected to any particular ability; each cumulative upkeep ability will count the total number of age counters on the permanent at the time that ability resolves.').

card_ruling('mana clash', '2004-10-04', 'Always affects the player and targets one of his or her opponents.').
card_ruling('mana clash', '2004-10-04', 'Each point of damage is dealt separately, so if you are damaged twice by this spell, each time is considered a separate time this source deals damage.').
card_ruling('mana clash', '2004-10-04', 'This card\'s coin flip has no winner or loser.').

card_ruling('mana drain', '2009-10-01', 'If the targeted spell \"can\'t be countered,\" it remains on the stack. Mana Drain continues to resolve. At the beginning of your next main phase, the delayed triggered ability will still add the mana to your mana pool.').
card_ruling('mana drain', '2009-10-01', 'If the targeted spell is an illegal target by the time Mana Drain resolves, Mana Drain is countered. You won\'t get any mana later.').
card_ruling('mana drain', '2009-10-01', 'Mana Drain\'s delayed triggered ability will usually trigger at the beginning of your precombat main phase. However, if you cast Mana Drain during your precombat main phase or during your combat phase, its delayed triggered ability will trigger at the beginning of that turn\'s postcombat main phase.').

card_ruling('mana echoes', '2004-10-04', 'You either get zero mana or all the mana. You can\'t choose an amount in between.').
card_ruling('mana echoes', '2004-10-04', 'You do get to count the creature itself, you even get one mana if it is the only creature you have on the battlefield (assuming it has at least one creature type).').

card_ruling('mana flare', '2004-10-04', 'If the land produces more than one color of mana at a single time, its controller chooses which of those types is produced by Mana Flare\'s ability.').
card_ruling('mana flare', '2004-10-04', 'The types of mana are white, blue, black, red, green, and colorless.').
card_ruling('mana flare', '2004-10-04', 'When used with lands that can produce multiple colors, Mana Flare produces one additional mana of the same color that was produced. It can\'t give a mana of a color that wasn\'t produced.').
card_ruling('mana flare', '2004-10-04', 'Only produces extra mana when land is tapped for mana, not when tapped by some other effect.').
card_ruling('mana flare', '2004-10-04', 'Mana Flare adds one of whatever color the land produces after applying any land type or color changing effects.').
card_ruling('mana flare', '2004-10-04', 'This is a triggered mana ability.').
card_ruling('mana flare', '2007-09-16', 'Does not copy any restrictions on the mana, such as with Mishra\'s Workshop or Pillar of the Paruns.').
card_ruling('mana flare', '2007-09-16', 'Produces only one additional mana, regardless of how much was produced by tapping the land.').

card_ruling('mana matrix', '2004-10-04', 'Multiple Matrices are cumulative. Two will reduce the cost by up to {4}, and so on.').
card_ruling('mana matrix', '2004-10-04', 'Only affects its controller, not all players.').
card_ruling('mana matrix', '2004-10-04', 'Only works on instants and enchantments. It does not work on sorceries.').
card_ruling('mana matrix', '2004-10-04', 'Only reduces the generic mana portion of a spell\'s cost. If the cost does not include generic mana or includes less than {2}, you get a reduced or null effect from this card.').

card_ruling('mana maze', '2004-10-04', 'Does not affect the first spell cast each turn.').
card_ruling('mana maze', '2004-10-04', 'If a spell\'s color was changed while on the stack, use the color at the time it was when you started announcement, not as it currently is on the stack or as it was at the time it resolved.').

card_ruling('mana reflection', '2008-05-01', 'You\'re \"tapping a permanent for mana\" only if you\'re activating an activated ability of that permanent that includes the {T} symbol in its cost and produces mana as part of its effect.').
card_ruling('mana reflection', '2008-05-01', 'Mana Reflection affects any permanent you tap for mana, not just lands you tap for mana.').
card_ruling('mana reflection', '2008-05-01', 'Mana Reflection doesn\'t produce any mana itself. Rather, it causes permanents you tap for mana to produce more mana. (This is different than seemingly similar cards like Heartbeat of Spring and Gauntlet of Power.) If the permanent puts any restrictions or riders on the mana it produces, as Pillar of the Paruns and Hall of the Bandit Lord do, that will apply to all the mana it produces this way.').
card_ruling('mana reflection', '2008-05-01', 'Mana Reflection doesn\'t cause permanents you tap for mana to produce one extra mana -- rather, it doubles the type and amount of mana that permanent would produce. For example, if you tap Mystic Gate for {W}{U}, it will produce {W}{W}{U}{U} instead.').
card_ruling('mana reflection', '2008-05-01', 'The effects of multiple Mana Reflections are cumulative. For example, if you have three Mana Reflections on the battlefield, you\'ll get eight times the original amount and type of mana.').
card_ruling('mana reflection', '2008-05-01', 'If you have Mana Reflection on the battlefield and tap a permanent for mana, but another replacement effect would change the type and/or amount of mana it would produce (such as Contamination, which says \"If a land is tapped for mana, it produces {B} instead of any other type and amount\"), you decide what order to apply those effects in. In this example, applying Mana Reflection first and Contamination second would result in {B}; applying Contamination first and Mana Reflection second would result in {B}{B}.').

card_ruling('mana short', '2004-10-04', 'Since this is an instant, your opponent may use instants in response to the casting of this spell. Note that such spells and abilities will be resolved before the mana pool is emptied by the Mana Short. This makes it useless as a countermeasure for spells. To use it effectively, you need to use it during Upkeep and even then it will not prevent the use of instant spells and abilities.').
card_ruling('mana short', '2004-10-04', 'If you play Mana Short in response to a spell, it will have no effect on that spell since the mana has already been paid.').
card_ruling('mana short', '2004-10-04', 'It even taps lands that do not produce mana.').

card_ruling('mana tithe', '2004-10-04', 'The payment is optional.').
card_ruling('mana tithe', '2007-02-01', 'This is the timeshifted version of Force Spike.').

card_ruling('mana vapors', '2008-08-01', 'No land controlled by the player will untap during his or her next untap step. This includes lands which entered the battlefield after Mana Vapors resolved.').

card_ruling('mana vortex', '2009-10-01', 'When you cast Mana Vortex, its first ability triggers and goes on the stack on top of the Mana Vortex spell. The triggered ability resolves first. When it resolves, you either sacrifice a land or you counter Mana Vortex.').
card_ruling('mana vortex', '2009-10-01', 'Mana Vortex\'s third ability is a \"state trigger.\" Once a state trigger triggers, it won\'t trigger again as long as the ability is on the stack. If the ability is countered and the trigger condition is still true, it will immediately trigger again.').

card_ruling('mana web', '2008-04-01', 'When a land an opponent controls is tapped for mana, this card checks which types of mana (white, blue, black, red, green, or colorless) that land could produce using any of its mana abilities, not merely the one the player just used. Then it checks which lands that player controls have abilities that could produce any of those types of mana. Then it taps those lands.').
card_ruling('mana web', '2008-04-01', 'The lands that are tapped by Mana Web\'s ability don\'t produce mana as a result.').
card_ruling('mana web', '2008-04-01', 'The opponent can tap his or her lands for mana in response to Mana Web\'s ability triggering.').

card_ruling('manabarbs', '2004-10-04', 'This card has a triggered ability that causes a separate effect for 1 point of damage each time a land is tapped for mana.').
card_ruling('manabarbs', '2009-10-01', 'This ability is not a mana ability. It goes on the stack and can be responded to.').
card_ruling('manabarbs', '2009-10-01', 'The ability will trigger each time a land is tapped for mana. Each ability is separate.').
card_ruling('manabarbs', '2009-10-01', 'If any lands are tapped for mana while a player is casting a spell or activating an ability, Manabarbs\'s ability will trigger that many times and wait. When the player finishes casting that spell or activating that ability, it\'s put on the stack, then Manabarbs\'s triggered abilities are put on the stack on top of it. The Manabarbs abilities will resolve first.').
card_ruling('manabarbs', '2009-10-01', 'On the other hand, a player can tap lands for mana, let the Manabarbs triggered abilities be put on the stack, and then respond to those abilities by casting an instant or activating an ability using that mana. In that case, the spell or ability will resolve first.').

card_ruling('manabond', '2004-10-04', 'The lands are put onto the battlefield during the resolution of the ability.').
card_ruling('manabond', '2004-10-04', 'You discard all the cards still in your hand after you put the lands onto the battlefield.').
card_ruling('manabond', '2004-10-04', 'Putting a land onto the battlefield does not count as playing a land.').

card_ruling('manaforce mace', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('manaforce mace', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('manaforce mace', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('managorger hydra', '2015-06-22', 'Managorger Hydra’s last ability will resolve before the spell that caused it to trigger.').

card_ruling('manaplasm', '2008-10-01', 'Converted mana cost takes into account only the mana symbols printed in the upper right corner of the card. Manaplasm doesn\'t care about additional costs, alternative costs, cost-reduction effects, or what you actually paid to cast the spell.').
card_ruling('manaplasm', '2008-10-01', 'While a spell with {X} in its cost is on the stack, its converted mana cost takes the chosen value of X into account. For example, if you cast Blaze (a spell with the mana cost {X}{R}) and choose a value of 5 for that X, then your Manaplasm will get +6/+6 until end of turn.').

card_ruling('manaweft sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('manaweft sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('mangara of corondor', '2006-09-25', 'If Mangara of Corondor leaves the battlefield before the ability resolves, the targeted permanent will still be exiled.').
card_ruling('mangara of corondor', '2006-09-25', 'If the target permanent becomes an illegal target, the ability will be countered and Mangara will remain on the battlefield.').

card_ruling('mangara\'s blessing', '2004-10-04', 'The 2 life from having it discarded is gained when the triggered ability resolves, not at end of turn. Only the return to hand is done at end of turn.').
card_ruling('mangara\'s blessing', '2004-10-04', 'The Mangara\'s Blessing card is only returned if it is still in the graveyard at end of turn and never left the graveyard in the meantime.').

card_ruling('mangara\'s tome', '2004-10-04', 'The ability is a replacement effect that replaces a draw.').
card_ruling('mangara\'s tome', '2004-10-04', 'If there are no cards left, you can still use the ability to turn a draw into a \"do nothing\".').

card_ruling('manic vandal', '2010-08-15', 'Manic Vandal\'s ability is mandatory. If you\'re the only player who controls an artifact, you must target one of them.').

card_ruling('mannichi, the fevered dream', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('mannichi, the fevered dream', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('manor gargoyle', '2013-07-01', 'Lethal damage dealt to Manor Gargoyle while it has indestructible will stay marked on it that turn. If Manor Gargoyle loses indestructible after having been dealt lethal damage earlier in the turn, it will be destroyed.').

card_ruling('map the wastes', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('map the wastes', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('maralen of the mornsong', '2008-04-01', 'Maralen prevents players from drawing cards for any reason, including as a cost, as an effect, and as a result of the \"draw a card\" turn-based action during the draw step.').
card_ruling('maralen of the mornsong', '2008-04-01', 'While Maralen is on the battlefield, no player can lose the game due to being instructed to draw a card with an empty library.').
card_ruling('maralen of the mornsong', '2008-04-01', 'While Maralen is on the battlefield, replacement effects that instruct a player to do something instead of drawing a card won\'t work.').
card_ruling('maralen of the mornsong', '2008-04-01', 'While Maralen is on the battlefield, a player can\'t choose to draw cards (or choose to have another player draw cards) as a spell or ability resolves. For example, if you cast the Development side of Research // Development (\"Put a 3/1 red Elemental creature token onto the battlefield unless any opponent lets you draw a card. Repeat this process two more times.\"), you\'ll put three Elemental tokens onto the battlefield because your opponents can\'t \"let you draw a card.\"').
card_ruling('maralen of the mornsong', '2008-04-01', 'The effect of the triggered ability is mandatory. The life loss isn\'t a payment; you\'ll lose 3 life even if you have 3 or less life left. Unless your library is empty, you must put a card from your library into your hand.').
card_ruling('maralen of the mornsong', '2008-04-01', 'In a Two-Headed Giant game, Maralen\'s triggered ability will trigger twice per draw step (once for each player on the active team). Maralen\'s controller chooses the order to put the abilities on the stack.').

card_ruling('marang river prowler', '2014-11-24', 'Marang River Prowler’s ability doesn’t change when you can cast it. You must still pay the spell’s costs.').
card_ruling('marang river prowler', '2014-11-24', 'Marang River Prowler checks if you control a black or green permanent only as you begin to cast it. Once you begin to cast Marang River Prowler from your graveyard, it doesn’t matter what happens to the black or green permanents you control.').

card_ruling('marang river skeleton', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('marang river skeleton', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('marang river skeleton', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('marath, will of the wild', '2013-10-17', 'Marath, Will of the Wild has received minor errata. The text “X can’t be 0” was inadvertently omitted from the card. The correct Oracle wording appears above.').
card_ruling('marath, will of the wild', '2013-10-17', 'You announce the value of X as you activate the ability, and all instances of X in the activation cost are equal to the announced value. For example, if you choose 2 as the value of X, then you pay {2} and remove two +1/+1 counters to pay the cost.').
card_ruling('marath, will of the wild', '2014-02-01', 'The amount of mana you spent to cast this creature is usually equal to its converted mana cost. However, you also include any additional costs you pay, including the cost imposed for casting your commander from the command zone.').
card_ruling('marath, will of the wild', '2014-02-01', 'You can’t choose to pay extra mana to cast a creature spell unless something instructs you to.').
card_ruling('marath, will of the wild', '2014-02-01', 'If Marath enters the battlefield without being cast, then no mana was spent to cast it. It will therefore enter the battlefield without any +1/+1 counters. If no other effects are increasing its toughness at that time, it will subsequently be put into its owner\'s graveyard as a state-based action.').

card_ruling('marauding knight', '2004-10-04', 'Counts plains controlled by all opponents.').

card_ruling('marauding maulhorn', '2013-07-01', 'Marauding Maulhorn can attack if you control a creature named Advocate of the Beast. It just isn’t forced to.').
card_ruling('marauding maulhorn', '2013-07-01', 'Marauding Maulhorn checks whether you control a creature named Advocate of the Beast as attacking creatures are declared. At that time, if you don’t, Marauding Maulhorn attacks if able.').
card_ruling('marauding maulhorn', '2013-07-01', 'If, during your declare attackers step, Marauding Maulhorn is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having Marauding Maulhorn attack, you aren’t forced to pay that cost, so it doesn’t have to attack in that case either.').

card_ruling('maraxus of keld', '2008-04-01', 'A permanent that\'s more than one of these types (such as an artifact creature) counts only once.').

card_ruling('marble chalice', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('march of the machines', '2004-12-01', 'If a noncreature artifact is also another card type, such as enchantment or land, it will retain those types in addition to being an artifact creature.').
card_ruling('march of the machines', '2004-12-01', 'If an Equipment becomes a creature, it can no longer equip a creature. If it\'s currently attached to a creature, it becomes unattached (but remains on the battlefield). You can activate the Equipment\'s equip ability, but it won\'t do anything.').
card_ruling('march of the machines', '2004-12-01', 'Each artifact land has a converted mana cost of 0. March of the Machines makes them 0/0 creatures, which are put into the graveyard as a state-based action.').
card_ruling('march of the machines', '2007-07-15', 'If a noncreature artifact becomes an artifact creature this way and then another effect animates it, the new effect overrides March of the Machines\'s effect. For example, Chimeric Staff is a 4/4 creature while March of the Machines is on the battlefield. If you activate Chimeric Staff\'s ability and choose X = 5, Chimeric Staff will be a 5/5 artifact creature for the rest of the turn.').
card_ruling('march of the machines', '2009-10-01', 'A noncreature permanent that turns into a creature is subject to the \"summoning sickness\" rule: It can only attack, and its {T} abilities can only be activated, if its controller has continuously controlled that permanent since the beginning of his or her most recent turn.').

card_ruling('marchesa\'s emissary', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('marchesa\'s emissary', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('marchesa\'s emissary', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('marchesa\'s emissary', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('marchesa\'s infiltrator', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('marchesa\'s infiltrator', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('marchesa\'s infiltrator', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('marchesa\'s infiltrator', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('marchesa\'s smuggler', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('marchesa\'s smuggler', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('marchesa\'s smuggler', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('marchesa\'s smuggler', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('marchesa, the black rose', '2014-05-29', 'If a creature has multiple instances of dethrone, each triggers separately.').
card_ruling('marchesa, the black rose', '2014-05-29', 'If the creature card leaves the graveyard before the delayed triggered ability resolves, that card won’t return to the battlefield, even if it’s back in the graveyard when the delayed triggered ability resolves.').
card_ruling('marchesa, the black rose', '2014-05-29', 'If Marchesa has a +1/+1 counter on it when it dies, it will return to the battlefield under your control because of its own ability.').
card_ruling('marchesa, the black rose', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('marchesa, the black rose', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('marchesa, the black rose', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('marchesa, the black rose', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('mardu ascendancy', '2014-09-20', 'As the Goblin token enters the battlefield, you choose which opponent or opposing planeswalker it’s attacking. It doesn’t have to attack the same opponent or opposing planeswalker that the original creature is attacking.').
card_ruling('mardu ascendancy', '2014-09-20', 'Although the tokens are attacking, they were never declared as attacking creatures (for purposes of abilities that trigger whenever a creature attacks, for example).').

card_ruling('mardu heart-piercer', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('mardu heart-piercer', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('mardu hordechief', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('mardu hordechief', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('mardu runemark', '2014-11-24', 'If the enchanted creature assigns combat damage during the first combat damage step, and then it loses first strike before the second combat damage step, it won’t assign combat damage again in the second combat damage step.').

card_ruling('mardu scout', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('mardu scout', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('mardu scout', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('mardu scout', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('mardu shadowspear', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('mardu shadowspear', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('mardu shadowspear', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('mardu shadowspear', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('mardu skullhunter', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('mardu skullhunter', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('mardu strike leader', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('mardu strike leader', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('mardu strike leader', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('mardu strike leader', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('mardu warshrieker', '2014-09-20', 'Mardu Warshrieker’s raid ability isn’t a mana ability even though it adds mana to your mana pool. It uses the stack and can be responded to.').
card_ruling('mardu warshrieker', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('mardu warshrieker', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('marjhan', '2008-05-01', 'The ability that untaps it during your upkeep has been returned to an activated ability. There is no restriction on how many times it can be untapped during your upkeep with this ability.').

card_ruling('mark for death', '2013-01-24', 'That creature\'s controller still decides which attacking creature it blocks.').
card_ruling('mark for death', '2013-01-24', 'If the creature can\'t block (perhaps because it has become tapped or it doesn\'t have flying and all attacking creatures do), then the requirement to block does nothing.').
card_ruling('mark for death', '2013-01-24', 'If there are any costs required for that creature to block (such as the one imposed by War Cadence), the creature\'s controller isn\'t required to pay those costs. In that case, the requirement to block would do nothing.').

card_ruling('mark of asylum', '2009-02-01', '\"Noncombat damage\" is damage dealt as the result of a spell or ability. (In unusual cases involving a card like Words of War, noncombat damage can also be dealt as the result of a turn-based action.)').

card_ruling('mark of fury', '2004-10-04', 'It only returns if it is still on the battlefield at end of turn.').

card_ruling('mark of mutiny', '2009-10-01', 'You may target a creature you already control. You may target a creature that\'s already untapped.').
card_ruling('mark of mutiny', '2009-10-01', 'The +1/+1 counter remains on the creature after the control-changing effect ends.').

card_ruling('mark of the vampire', '2012-07-01', 'Multiple instances of lifelink are redundant. If Mark of the Vampire enchants a creature that already has lifelink, damage that creature deals won\'t cause you to gain additional life.').

card_ruling('markov blademaster', '2011-01-22', 'The +1/+1 counter isn\'t put on Markov Blademaster in time to increase the amount of combat damage dealt during that combat step. However, the +1/+1 counter put on Markov Blademaster when first-strike damage is dealt will increase the amount of damage dealt during the regular combat damage step.').
card_ruling('markov blademaster', '2011-01-22', 'If this creature deals combat damage to multiple players simultaneously, perhaps because some combat damage was redirected, its ability will trigger for each of those players.').

card_ruling('markov warlord', '2011-01-22', 'You may target zero, one, or two creatures with Markov Warlord\'s enters-the-battlefield triggered ability.').
card_ruling('markov warlord', '2011-01-22', 'The creatures can\'t block any creatures that turn, not just Markov Warlord.').

card_ruling('maro', '2004-10-04', 'Creatures are only put into a graveyard for having 0 toughness when state-based actions are checked. If Maro\'s toughness goes to 0 during the announcement or resolution of a spell or ability and then goes back up above 0 before the end of that announcement or resolution, it won\'t be put into your graveyard when state-based actions are checked.').

card_ruling('marrow shards', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('marrow shards', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('marrow shards', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('marrow shards', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('marrow shards', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('marrow-gnawer', '2004-12-01', 'Marrow-Gnawer\'s ability counts the number of Rats on the battlefield when it resolves, so you don\'t count the sacrificed Rat.').

card_ruling('marsh crocodile', '2004-10-04', 'Both triggered abilities trigger at the same time so you can decide which order they go on the stack.').
card_ruling('marsh crocodile', '2004-10-04', 'This card can return itself to your hand.').
card_ruling('marsh crocodile', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('marsh flitter', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('marsh hulk', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('marsh hulk', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('marsh hulk', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('marshaling cry', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('marshmist titan', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('marshmist titan', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('marshmist titan', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('marshmist titan', '2014-02-01', 'Your devotion to black is calculated when you determine Marshmist Titan’s total cost, and that cost is locked in before any costs are paid. For example, if you control a creature with black mana symbols in its mana cost that can be sacrificed for mana, those mana symbols will count toward your devotion to black. You can then sacrifice that creature for mana to pay the reduced total cost.').
card_ruling('marshmist titan', '2014-02-01', 'If your devotion to black is greater than six, Marshmist Titan will cost {B} to cast. The colored mana requirement isn’t reduced.').

card_ruling('martial coup', '2009-02-01', 'Martial Coup checks the number you chose for X, not the amount of mana you actually spent.').
card_ruling('martial coup', '2009-02-01', 'You do what the spell says in order. If X is 5 or more, you\'ll put the Soldier tokens onto the battlefield, then you\'ll destroy all other creatures.').
card_ruling('martial coup', '2009-02-01', 'No one can cast spells or activate abilities between the time the Soldier tokens are put onto the battlefield and the time all other creatures are destroyed. For example, you can\'t sacrifice one of those Soldier tokens to regenerate a Skeletal Kathari.').

card_ruling('martial glory', '2013-01-24', 'You may choose the same creature for both targets since the card says “target creature” multiple times. You may also choose two different creatures.').

card_ruling('martial law', '2013-04-15', 'Activated abilities include a colon and are written in the form “[cost]: [effect].” No one can activate any activated abilities, including mana abilities, of a detained permanent.').
card_ruling('martial law', '2013-04-15', 'The static abilities of a detained permanent still apply. The triggered abilities of a detained permanent can still trigger.').
card_ruling('martial law', '2013-04-15', 'If a creature is already attacking or blocking when it\'s detained, it won\'t be removed from combat. It will continue to attack or block.').
card_ruling('martial law', '2013-04-15', 'If a permanent\'s activated ability is on the stack when that permanent is detained, the ability will be unaffected.').
card_ruling('martial law', '2013-04-15', 'If a noncreature permanent is detained and later turns into a creature, it won\'t be able to attack or block.').
card_ruling('martial law', '2013-04-15', 'When a player leaves a multiplayer game, any continuous effects with durations that last until that player\'s next turn or until a specific point in that turn will last until that turn would have begun. They neither expire immediately nor last indefinitely.').

card_ruling('martyr of ashes', '2006-07-15', 'Each card that\'s revealed to pay the cost remains revealed until the ability leaves the stack.').
card_ruling('martyr of ashes', '2006-07-15', 'A card that\'s already revealed for another cost or because of an effect can be revealed to pay this cost.').
card_ruling('martyr of ashes', '2006-07-15', 'If one of the cards that\'s revealed to pay the cost leaves its owner\'s hand before the ability resolves, it stops being revealed, but the value of X is not affected.').

card_ruling('martyr of bones', '2006-07-15', 'Each card that\'s revealed to pay the cost remains revealed until the ability leaves the stack.').
card_ruling('martyr of bones', '2006-07-15', 'A card that\'s already revealed for another cost or because of an effect can be revealed to pay this cost.').
card_ruling('martyr of bones', '2006-07-15', 'If one of the cards that\'s revealed to pay the cost leaves its owner\'s hand before the ability resolves, it stops being revealed, but the value of X is not affected.').

card_ruling('martyr of frost', '2006-07-15', 'Each card that\'s revealed to pay the cost remains revealed until the ability leaves the stack.').
card_ruling('martyr of frost', '2006-07-15', 'A card that\'s already revealed for another cost or because of an effect can be revealed to pay this cost.').
card_ruling('martyr of frost', '2006-07-15', 'If one of the cards that\'s revealed to pay the cost leaves its owner\'s hand before the ability resolves, it stops being revealed, but the value of X is not affected.').

card_ruling('martyr of sands', '2006-07-15', 'Each card that\'s revealed to pay the cost remains revealed until the ability leaves the stack.').
card_ruling('martyr of sands', '2006-07-15', 'A card that\'s already revealed for another cost or because of an effect can be revealed to pay this cost.').
card_ruling('martyr of sands', '2006-07-15', 'If one of the cards that\'s revealed to pay the cost leaves its owner\'s hand before the ability resolves, it stops being revealed, but the value of X is not affected.').

card_ruling('martyr of spores', '2006-07-15', 'Each card that\'s revealed to pay the cost remains revealed until the ability leaves the stack.').
card_ruling('martyr of spores', '2006-07-15', 'A card that\'s already revealed for another cost or because of an effect can be revealed to pay this cost.').
card_ruling('martyr of spores', '2006-07-15', 'If one of the cards that\'s revealed to pay the cost leaves its owner\'s hand before the ability resolves, it stops being revealed, but the value of X is not affected.').

card_ruling('martyr\'s bond', '2011-09-22', 'If Martyr\'s Bond and other nonland permanents are put into the graveyard from the battlefield at the same time, Martyr\'s Bond will trigger for itself and each of those other nonland permanents.').
card_ruling('martyr\'s bond', '2011-09-22', 'Each opponent must sacrifice only one permanent for each trigger, even if the nonland permanent you controlled that went to the graveyard had multiple types. For example, if an artifact creature you control is put into a graveyard from the battlefield, each opponent sacrifices exactly one artifact or creature, and that permanent doesn\'t need to be an artifact creature.').

card_ruling('martyr\'s cry', '2004-10-04', 'Only affects creatures on the battlefield, not ones in hands or graveyards.').

card_ruling('martyrdom', '2004-10-04', 'If the target stops being a creature, you can\'t redirect damage to it until/unless it becomes a creature again.').
card_ruling('martyrdom', '2004-10-04', 'If the target creature is not still on the battlefield when the damage is to be redirected, the damage stays where it is.').
card_ruling('martyrdom', '2004-10-04', 'Can be used to redirect damage from that creature to itself. This has no particularly useful effect.').

card_ruling('martyrs of korlis', '2004-10-04', 'If you have multiple Martyrs of Korlis, you can decide which one receives the redirected damage each time artifact damage would be dealt to you.').

card_ruling('masako the humorless', '2004-12-01', 'Masako allows tapped creatures to block only if they could otherwise block. It doesn\'t allow creatures that \"can\'t block\" to block, nonflying creatures to block fliers, etc.').

card_ruling('mask of avacyn', '2011-09-22', 'If Mask of Avacyn somehow becomes attached to a creature an opponent controls, that creature can\'t be the target of spells or abilities you control.').

card_ruling('mask of law and grace', '2004-10-04', 'If a text-changing effect is used to change one of the color words so it grants Protection from white, then it will cause itself to be put into the graveyard.').

card_ruling('mask of riddles', '2009-05-01', 'If a Mask of Riddles you control somehow winds up equipping a creature you don\'t control, Mask of Riddles\'s triggered ability will still trigger whenever that creature deals combat damage to a player (even if that player is you). You will control that ability, so you may draw a card.').

card_ruling('mask of the mimic', '2004-10-04', 'You look for a card of the same name, the current type of the card does not matter.').
card_ruling('mask of the mimic', '2004-10-04', 'You do not have to find a card if you do not want to.').
card_ruling('mask of the mimic', '2004-10-04', 'If a copy card is targeted by this effect, you get to look for another copy of the card it is copying. This is because a copy card actually takes on the name and initial characteristics of what it copies.').
card_ruling('mask of the mimic', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('mask of the mimic', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('masked admirers', '2013-06-07', 'The second triggered ability triggers only if Masked Admirers is in your graveyard when you cast a creature spell.').
card_ruling('masked admirers', '2013-06-07', 'Masked Admirers will return to your hand before the creature spell you cast resolves.').

card_ruling('mass appeal', '2012-05-01', 'The number of Humans you control is counted only when Mass Appeal resolves.').

card_ruling('mass mutiny', '2012-06-01', 'The phrase \"up to one\" was inadvertently omitted from Mass Mutiny\'s rules text. The card has received errata to correct this.').
card_ruling('mass mutiny', '2012-06-01', 'You can cast Mass Mutiny even if an opponent doesn\'t control any creatures. You simply won\'t choose a target for that opponent.').
card_ruling('mass mutiny', '2012-06-01', 'Mass Mutiny can target untapped creatures.').
card_ruling('mass mutiny', '2012-06-01', 'Gaining control of a creature doesn\'t cause you to gain control of any Auras or Equipment attached to it. However, if you gain control of a creature enchanted by an Aura with totem armor and that creature would be destroyed that turn, instead the Aura will be destroyed and the creature will survive.').

card_ruling('mass polymorph', '2010-08-15', 'The creatures you exile with Mass Polymorph remain exiled for the rest of the game.').
card_ruling('mass polymorph', '2010-08-15', 'If the number of creatures you exile is greater than the number of creature cards remaining in your library, you\'ll wind up revealing your entire library, putting all creature cards revealed that way onto the battlefield, then shuffling your library.').
card_ruling('mass polymorph', '2010-08-15', 'All creatures you put onto the battlefield with Mass Polymorph enter the battlefield at the same time.').
card_ruling('mass polymorph', '2010-08-15', 'Any abilities that trigger during the resolution of Mass Polymorph (such as a creature\'s enters-the-battlefield ability) will wait to be put onto the stack until Mass Polymorph finishes resolving. The player whose turn it is will put all of his or her triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').

card_ruling('massacre wurm', '2011-06-01', 'If an opponent controls a creature you own, that opponent will lose 2 life when that creature is put into your graveyard from the battlefield.').
card_ruling('massacre wurm', '2011-06-01', 'If a creature an opponent controls is put into the graveyard from the battlefield at the same time as Massacre Wurm, its last ability will trigger.').

card_ruling('massive raid', '2013-01-24', 'Count the number of creatures you control when Massive Raid resolves to determine how much damage is dealt.').

card_ruling('master biomancer', '2013-01-24', 'To determine how many additional +1/+1 counters a creature enters the battlefield with, use Master Biomancer\'s power as that creature is entering the battlefield.').

card_ruling('master of arms', '2011-06-01', 'Tapping the blocker does not remove it from combat and does not stop it from dealing or receiving damage.').

card_ruling('master of cruelties', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').
card_ruling('master of cruelties', '2013-04-15', 'Master of Cruelties isn\'t forced to attack, but if it does, it must do so alone. If you control another creature with an ability that says it must attack if able, that creature must attack and Master of Cruelties won\'t be able to.').
card_ruling('master of cruelties', '2013-04-15', 'If Master of Cruelties attacks alone, another creature entering the battlefield attacking will have no effect on it. Master of Cruelties continues to be an attacking creature. The other attacking creature will assign combat damage normally, regardless of whether Master of Cruelties\'s last ability triggers.').
card_ruling('master of cruelties', '2013-04-15', 'Master of Cruelties\'s last ability won\'t trigger if it attacks a planeswalker.').
card_ruling('master of cruelties', '2013-04-15', 'For a player\'s life total to become 1, what actually happens is that the player loses (or in some rare cases, gains) the appropriate amount of life. For example, if the player\'s life total is 4 when the last ability resolves, it will cause that player to lose 3 life. Other effects that interact with life loss (or gain) will interact with this effect accordingly.').
card_ruling('master of cruelties', '2013-04-15', 'Assigning no combat damage isn\'t the same as preventing that damage. Effects that make damage unpreventable will have no effect if no combat damage can be assigned.').

card_ruling('master of diversion', '2013-07-01', 'The ability will resolve and the creature will become tapped before blocking creatures are declared.').
card_ruling('master of diversion', '2013-07-01', 'In a Two-Headed Giant game, any creature controlled by a member of the defending team can be chosen as the target.').
card_ruling('master of diversion', '2013-07-01', 'In other multiplayer formats, including Commander, only a creature controlled by the defending player Master of Diversion is attacking can be chosen as the target, even if your other attacking creatures are attacking different players.').

card_ruling('master of etherium', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('master of etherium', '2008-10-01', 'The first ability works in all zones.').
card_ruling('master of etherium', '2008-10-01', 'Since Master of Etherium is an artifact itself, while it\'s on the battlefield you will usually control at least one artifact.').

card_ruling('master of pearls', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('master of pearls', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('master of pearls', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('master of pearls', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('master of pearls', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('master of pearls', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('master of pearls', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('master of pearls', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('master of pearls', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('master of predicaments', '2014-07-18', 'To choose a card in your hand, clearly separate it from the rest of your hand without revealing it.').
card_ruling('master of predicaments', '2014-07-18', 'Although you may choose a land card in your hand, you won’t be able to play it even if the player guessed wrong.').
card_ruling('master of predicaments', '2014-07-18', 'If you don’t cast the card, either because the player guessed right or because you choose not to cast it, you don’t have to reveal it. Your opponent won’t know the reason you didn’t cast the card.').
card_ruling('master of predicaments', '2014-07-18', 'If the card has {X} in its mana cost, X is 0.').
card_ruling('master of predicaments', '2014-07-18', 'You cast the card in your hand as part of the resolution of the triggered ability. Timing restrictions based on the card’s type (such as creature or sorcery) are ignored. Other restrictions, such as “Cast [this card] only during combat,” are not.').
card_ruling('master of predicaments', '2014-07-18', 'If you cast a card “without paying its mana cost,” you can’t pay any alternative costs. Notably, you can’t cast a card with bestow as an Aura this way. You can, however, pay additional costs, such as kicker costs. If the card has any mandatory additional costs, you must pay those.').
card_ruling('master of predicaments', '2014-07-18', 'If the card is a split card, it has two converted mana costs, one for each half. If both converted mana costs are greater than 4 or if both are not, the opponent must guess appropriately to be correct. If one converted mana cost is greater than 4 and one is not, guessing that the card’s converted mana cost is greater than 4 is correct, and guessing that the card’s converted mana cost isn’t greater than 4 is wrong. If the player guesses wrong and you cast a split card this way, you may cast either half (or both halves if the card has fuse), no matter what its converted mana cost is.').

card_ruling('master of the hunt', '2004-10-04', 'Since tapping is not part of the cost, you can use this more than once a turn.').

card_ruling('master of the veil', '2004-10-04', 'It can target itself and turn itself face down.').
card_ruling('master of the veil', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('master of the wild hunt', '2009-10-01', 'The Wolves are tapped as part of the activated ability\'s effect, not as a cost.').
card_ruling('master of the wild hunt', '2009-10-01', 'If the targeted creature is an illegal target by the time the activated ability would resolve, the entire ability is countered. Nothing else happens. (For example, the Wolves aren\'t tapped.)').
card_ruling('master of the wild hunt', '2009-10-01', 'The Wolves deal their damage first. This may be important if the damage causes the targeted creature\'s power to be reduced (if it\'s Protean Hydra, for example). However, creatures aren\'t destroyed for having been dealt lethal damage until a spell or ability has finished resolving. If the targeted creature is dealt lethal damage by the tapped Wolves, it will still get to deal damage to those Wolves before being destroyed.').
card_ruling('master of the wild hunt', '2009-10-01', 'Only a Wolf creature tapped as part of the ability\'s effect can be dealt damage by the targeted creature.').
card_ruling('master of the wild hunt', '2009-10-01', 'The targeted creature\'s controller doesn\'t divide that creature\'s damage as the ability is activated (since the Wolves that will receive that damage aren\'t targeted), so that player does so as the ability resolves. Each Wolf chosen by that player must be dealt at least 1 damage. There is no time to react between the time a Wolf is chosen, the time damage is dealt to it, and the time it\'s destroyed for having been dealt lethal damage. If you want to put a regeneration shield on one of those Wolves, or target it with a damage-prevention spell, or anything else, you must do so before the ability resolves (and before you know which Wolves will be chosen and how much damage will be dealt to them).').

card_ruling('master of waves', '2013-09-15', 'Use your devotion to blue as the triggered ability resolves to determine how many tokens are put onto the battlefield. If Master of Waves is still on the battlefield at that time, its mana cost will count toward your devotion to blue.').
card_ruling('master of waves', '2013-09-15', 'Barring other effects, the Elemental tokens will enter the battlefield as 2/1 creatures.').
card_ruling('master of waves', '2013-09-15', 'If Master of Waves leaves the battlefield, and nothing else is raising the toughness of the Elemental tokens above 0, the Elemental tokens will die and subsequently cease to exist.').
card_ruling('master of waves', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('master of waves', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('master of waves', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('master of waves', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('master the way', '2014-09-20', 'Use the number of cards in your hand after you draw the card to determine how much damage Master the Way deals to the target creature or player. (Remember that Master the Way is not in your hand; it’s on the stack.)').
card_ruling('master the way', '2014-09-20', 'If the creature or player is an illegal target as Master the Way tries to resolve, Master the Way will be countered and none of its effects will happen. You won’t draw a card.').

card_ruling('master thief', '2011-09-22', 'If Master Thief leaves the battlefield, you no longer control it, and its control-change effect ends.').
card_ruling('master thief', '2011-09-22', 'If Master Thief ceases to be under your control before its ability resolves, you won\'t gain control of the targeted artifact at all.').
card_ruling('master thief', '2011-09-22', 'If another player gains control of Master Thief, its control-change effect ends. Regaining control of Master Thief won\'t cause you to regain control of the artifact.').

card_ruling('master transmuter', '2009-02-01', 'Returning an artifact you control to its owner\'s hand is part of the cost of Master Transmuter\'s activated ability. Paying a cost can\'t be responded to (with Naturalize, for example).').
card_ruling('master transmuter', '2009-02-01', 'Since Master Transmuter is an artifact, you may return Master Transmuter itself to your hand. If you do, the rest of the ability will still work normally.').
card_ruling('master transmuter', '2009-02-01', 'The artifact card you put onto the battlefield when the ability resolves may be the same card that you returned to your hand when you paid the cost. If so, it returns to the battlefield as a new object with no relation to its previous existence.').

card_ruling('master warcraft', '2005-10-01', 'You choose attackers and make blocking assignments regardless of whether it\'s your turn and regardless of whether the creatures are attacking you. Your choices must be legal within the normal rules for attacking and blocking.').
card_ruling('master warcraft', '2006-01-01', 'You can decide that a creature won\'t block.').
card_ruling('master warcraft', '2008-04-01', 'If the defending player controls a planeswalker, the person who cast Master Warcraft first chooses the complete group of creatures that are going to attack. Then, for each of those creatures, the active player chooses who or what it\'s going to attack.').
card_ruling('master warcraft', '2013-09-20', 'If a turn has multiple combat phases, this spell can only be cast before the beginning of the Declare Attackers Step of the first combat phase in that turn.').

card_ruling('masterwork of ingenuity', '2014-11-07', 'Although Masterwork of Ingenuity doesn’t have an equip ability, it will have the equip ability of the Equipment it copies.').
card_ruling('masterwork of ingenuity', '2014-11-07', 'Any enters-the-battlefield abilities of the chosen Equipment will trigger. Any “as [this permanent] enters the battlefield or “[this permanent] enters the battlefield with” abilities of the chosen Equipment will also work.').

card_ruling('mastery of the unseen', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('mastery of the unseen', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('mastery of the unseen', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('mastery of the unseen', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('mastery of the unseen', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('mastery of the unseen', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('mastery of the unseen', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('mastery of the unseen', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('mastery of the unseen', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('mastery of the unseen', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('mastery of the unseen', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('mastery of the unseen', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('mastery of the unseen', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('matca rioters', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('matca rioters', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('matca rioters', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').
card_ruling('matca rioters', '2009-02-01', 'Matca Rioters\' ability is a \"characteristic-defining ability.\" It applies at all times in all zones.').

card_ruling('maw of the mire', '2011-09-22', 'If the targeted land is an illegal target by the time Maw of the Mire resolves, it will be countered and none of its effects will occur. You won\'t gain 4 life.').

card_ruling('may civilization collapse', '2010-06-15', 'The targeted player may choose \"self\" even if he or she can\'t perform the resulting action. For example, a player targeted with Feed the Machine may choose \"self\" even if he or she controls no creatures.').
card_ruling('may civilization collapse', '2010-06-15', 'The targeted player may choose \"others\" even if there are no others (because all of his or her teammates have lost the game, for example), or the archenemy\'s other opponents can\'t perform the resulting action.').
card_ruling('may civilization collapse', '2010-06-15', 'In a Supervillain Rumble game, the targeted player may still choose \"others.\" Each player who isn\'t the active player or the targeted player will thus be affected.').

card_ruling('mayael\'s aria', '2009-05-01', 'This ability triggers at the beginning of each of your upkeeps. Whether you control any applicable creatures is checked only when the ability resolves.').
card_ruling('mayael\'s aria', '2009-05-01', 'The three parts of the ability happen sequentially. For example, if you control a creature with 9 power when the ability starts to resolve, you\'ll put a +1/+1 counter on each creature you control -- and now, since you control a creature with 10 power, you\'ll gain 10 life.').

card_ruling('maze of ith', '2004-10-04', 'Can target an untapped attacking creature. Both parts of the effect will try to happen even if one part does nothing useful. So if it tries to untap an already untapped card, it will still make the creature neither deal nor receive damage.').
card_ruling('maze of ith', '2004-10-04', 'The creature is still part of the combat. It just has its damage prevented. Don\'t infer something from the fact that it becomes untapped.').

card_ruling('maze\'s end', '2013-04-15', 'Returning Maze\'s End to its owner\'s hand is part of the cost to activate its last ability. Once that ability is announced, players can\'t respond to it until after you\'ve paid its activation cost and returned Maze\'s End to hand.').
card_ruling('maze\'s end', '2013-04-15', 'When the last ability of Maze\'s End resolves, you\'ll search for a Gate and put it onto the battlefield before checking to see if you win the game. This check will happen even if you don\'t put a Gate onto the battlefield this way. This check will happen only as the ability resolves, not at other times.').
card_ruling('maze\'s end', '2013-04-15', 'Putting a Gate onto the battlefield with Maze\'s End doesn\'t count as the one land you can play during your turn. If it\'s your turn, you can play Maze\'s End or a different land card from your hand after its ability has resolved.').
card_ruling('maze\'s end', '2013-04-15', 'Controlling multiple Gates with the same name has no effect on your ability to win the game with Maze\'s End. The excess Gates are simply ignored.').

card_ruling('meadowboon', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('meadowboon', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('meadowboon', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('meadowboon', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('meadowboon', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('meadowboon', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').

card_ruling('meandering towershell', '2014-09-20', 'As Meandering Towershell returns to the battlefield because of the delayed triggered ability, you choose which opponent or opposing planeswalker it’s attacking. It doesn’t have to attack the same opponent or opposing planeswalker that it was when it was exiled.').
card_ruling('meandering towershell', '2014-09-20', 'If Meandering Towershell enters the battlefield attacking, it wasn’t declared as an attacking creature that turn. Abilities that trigger when a creature attacks, including its own triggered ability, won’t trigger.').
card_ruling('meandering towershell', '2014-09-20', 'On the turn Meandering Towershell attacks and is exiled, raid abilities will see it as a creature that attacked. Conversely, on the turn Meandering Towershell enters the battlefield attacking, raid abilities will not.').
card_ruling('meandering towershell', '2014-09-20', 'If you attack with a Meandering Towershell that you don’t own, you’ll control it when it returns to the battlefield.').

card_ruling('measure of wickedness', '2005-06-01', 'Once the at-end-of-turn ability triggers, it affects the player who controlled Measure of Wickedness at that time. If Measure of Wickedness is under a different player\'s control as its at-end-of-turn ability resolves, however, Measure of Wickedness stays on the battlefield.').

card_ruling('meddle', '2004-10-04', 'Only works on spells with a single target, where that target is a creature. Will not work on spells that target a single creature and a player, for example.').

card_ruling('meddling mage', '2009-05-01', 'No one can cast spells or activate abilities between the time a card is named and the time that Meddling Mage\'s ability starts to work.').
card_ruling('meddling mage', '2009-05-01', 'Spells with the chosen name that somehow happen to already be on the stack when Meddling Mage enters the battlefield are not affected by Meddling Mage\'s ability.').
card_ruling('meddling mage', '2009-05-01', 'Although the named card can\'t be cast, it can still be put onto the battlefield by a spell or ability (if it\'s a permanent card).').
card_ruling('meddling mage', '2009-05-01', 'You can name either half of a split card, but not both. If you do so, that half (and both halves, if the split card has fuse) can\'t be cast. The other half is unaffected.').
card_ruling('meddling mage', '2009-05-01', 'If a card with morph is named, that card may still be cast face down. Face-down cards have no names.').
card_ruling('meddling mage', '2009-05-01', 'Some cards (such as Isochron Scepter, for example) let you cast a copy of a card. A copy of a card isn\'t actually a card, so Meddling Mage can\'t stop this.').
card_ruling('meddling mage', '2009-05-01', 'Once Meddling Mage leaves the battlefield, the named card can be cast again.').

card_ruling('medicine runner', '2008-05-01', 'This can remove any kind of counter, not just a -1/-1 counter.').
card_ruling('medicine runner', '2008-05-01', 'You may target a permanent that doesn\'t have a counter on it.').

card_ruling('meditate', '2004-10-04', 'You skip one turn as part of the effect.').

card_ruling('meditation puzzle', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('meditation puzzle', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('meditation puzzle', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('meditation puzzle', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('meditation puzzle', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('meditation puzzle', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('meditation puzzle', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('medomai the ageless', '2013-09-15', 'An “extra turn” is any turn created by a spell or ability. Notably, it doesn’t include additional turns taken in tournaments after time expires for a round.').
card_ruling('medomai the ageless', '2013-09-15', 'It doesn’t matter whose turn it is. In very rare cases, an opponent may be taking an extra turn and gain control of Medomai. Medomai can’t attack during that turn.').

card_ruling('megantic sliver', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('megantic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('meglonoth', '2009-02-01', 'You don\'t check Meglonoth\'s power until its triggered ability resolves.').
card_ruling('meglonoth', '2009-02-01', 'If Meglonoth leaves the battlefield after its ability triggers but before it resolves, the amount of damage it deals is equal to its power as it last existed on the battlefield.').
card_ruling('meglonoth', '2009-02-01', 'If Meglonoth somehow blocks more than one creature, its ability will trigger once for each creature it blocks.').

card_ruling('melancholy', '2007-02-01', 'This is the timeshifted version of Thirst.').

card_ruling('melee', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').
card_ruling('melee', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s before the beginning of that phase\'s Declare Blockers Step.').

card_ruling('melek, izzet paragon', '2013-04-15', 'Melek doesn\'t affect the timing rules associated with when you can cast the card. If it\'s a sorcery card, you can cast it only during your main phase when the stack is empty.').
card_ruling('melek, izzet paragon', '2013-04-15', 'You still pay all costs for that spell, including additional costs. You may also pay alternative costs, such as overload costs. Although you can\'t pay additional costs for the copy that\'s created, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if a player sacrifices a 3/3 creature to cast Fling, and you copy it, the copy of Fling will also deal 3 damage to its target.').
card_ruling('melek, izzet paragon', '2013-04-15', 'Melek\'s last ability will copy any instant or sorcery spell you cast from your library, not just one with targets.').
card_ruling('melek, izzet paragon', '2013-04-15', 'When the last ability resolves, it creates a copy of the spell. You control the copy. That copy is created on the stack, so it\'s not “cast.” Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('melek, izzet paragon', '2013-04-15', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('melek, izzet paragon', '2013-04-15', 'If the spell being copied is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can\'t choose a different one.').
card_ruling('melek, izzet paragon', '2013-04-15', 'If the spell being copied has an X whose value was determined as it was cast (like Debt to the Deathless has), the copy has the same value of X.').
card_ruling('melek, izzet paragon', '2013-04-15', 'Instant and sorcery cards with miracle allow a player to cast a card immediately upon drawing it. If you cast a spell this way, you\'re casting it from your hand, not your library. Melek\'s ability won\'t trigger.').
card_ruling('melek, izzet paragon', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('melek, izzet paragon', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('melek, izzet paragon', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('meletis astronomer', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('meletis astronomer', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('meletis astronomer', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('meletis charlatan', '2013-09-15', 'Meletis Charlatan’s ability can target any instant or sorcery spell, not just one with targets and not just one that you control.').
card_ruling('meletis charlatan', '2013-09-15', 'When the ability resolves, it creates a copy of the spell. The controller of the original spell controls the copy. That copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('meletis charlatan', '2013-09-15', 'The copy will have the same targets as the spell it’s copying unless its controller chooses new ones. That player may change any number of the targets, including all of them or none of them. If, for one of the targets, that player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('meletis charlatan', '2013-09-15', 'If the spell being copied is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. Its controller can’t choose a different one.').
card_ruling('meletis charlatan', '2013-09-15', 'If the spell being copied has an X whose value was determined as it was cast (like Curse of the Swine has), the copy has the same value of X.').

card_ruling('melira\'s keepers', '2011-06-01', 'Melira\'s Keepers can\'t have any kind of counters placed on it, not just -1/-1 counters.').
card_ruling('melira\'s keepers', '2011-06-01', 'Damage from a source with infect has no effect on Melira\'s Keepers. No -1/-1 counters are placed on it, and no damage is marked on it. The damage is still dealt for purposes of effects that care about damage, such as lifelink.').
card_ruling('melira\'s keepers', '2011-06-01', 'If Melira\'s Keepers would enter the battlefield with a counter on it, it simply enters the battlefield without that counter.').
card_ruling('melira\'s keepers', '2011-06-01', 'If a creature with a counter on it becomes a copy of Melira\'s Keepers, those counters will remain.').

card_ruling('melira, sylvok outcast', '2011-06-01', 'Melira doesn\'t remove any poison counters you already have or any -1/-1 counters already on creatures you control.').
card_ruling('melira, sylvok outcast', '2011-06-01', 'Damage dealt to planeswalkers you control causes loyalty counters to be removed from them, regardless of whether Melira is on the battlefield.').
card_ruling('melira, sylvok outcast', '2011-06-01', 'If a creature with infect enters the battlefield under an opponent\'s control after Melira enters the battlefield, that creature loses infect.').
card_ruling('melira, sylvok outcast', '2011-06-01', 'It\'s possible for a creature an opponent controls to gain infect after Melira enters the battlefield. For example, your opponent could cast Glistening Oil on a creature, and then Melira\'s ability and Glistening Oil\'s ability would be applied in timestamp order, with the most recent effect winning. The damage dealt to you by such a creature won\'t result in you losing life (because the creature has infect) or in you getting poison counters (because Melira stops them). Similarly, damage dealt to a creature you control by such a creature won\'t result in damage being marked on the creature or in -1/-1 counters. However, abilities that check whether damage was dealt (such as lifelink or Whispering Specter\'s triggered ability) will still see that damage.').

card_ruling('melt terrain', '2011-01-01', 'If the targeted land is an illegal target by the time Melt Terrain resolves, the spell will be countered. No damage will be dealt.').

card_ruling('memnarch', '2004-12-01', 'The effects of Memnarch\'s abilities don\'t end at end of turn, and they don\'t end when Memnarch leaves the battlefield. Both effects last until the affected permanent leaves the battlefield.').
card_ruling('memnarch', '2004-12-01', 'You can use the first ability on a nonartifact permanent, wait for the ability to resolve, and then use the second ability on that permanent.').

card_ruling('memoricide', '2011-01-01', 'Because you\'re searching for \"any number\" of cards with the chosen name, you can opt to find all of them, none of them, or any number in between. That means you may leave any cards with that name in that target\'s graveyard, hand, and/or library.').

card_ruling('memory crystal', '2004-10-04', 'Can\'t reduce the cost below zero.').
card_ruling('memory crystal', '2004-10-04', 'Does not apply to other parts of the cost.').
card_ruling('memory crystal', '2004-10-04', 'Only affects generic mana portions of Buyback costs.').
card_ruling('memory crystal', '2004-10-04', 'It applies to all players.').
card_ruling('memory crystal', '2004-10-04', 'The ability is not optional.').

card_ruling('memory jar', '2004-10-04', 'You can\'t look at the cards you exiled until they return to your hand.').

card_ruling('memory lapse', '2004-10-04', 'The card does not go to the graveyard before being put on the library.').
card_ruling('memory lapse', '2004-10-04', 'This card has a self replacement. This means that it replaces going to the graveyard before any other effect can replace that event. If Flashback is applied, however, Flashback will change the destination.').
card_ruling('memory lapse', '2004-10-04', 'If the spell is not countered (because the spell it targets can\'t be countered), then it does not go to its owner\'s library.').

card_ruling('memory plunder', '2008-05-01', 'If you want to cast the card, you cast it as part of the resolution of Memory Plunder. Timing restrictions based on the card\'s type are ignored if it\'s a sorcery. Other casting restrictions are not (such as \"Cast [this card] only during combat\").').
card_ruling('memory plunder', '2008-05-01', 'If you are unable to cast the card (there are no legal targets for the spell, for example), nothing happens when Memory Plunder resolves, and the card remains in its owner\'s graveyard.').
card_ruling('memory plunder', '2008-05-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. On the other hand, if the card has additional costs (such as conspire), you may pay those.').

card_ruling('memory\'s journey', '2011-09-22', 'You don\'t have to target any cards when you cast Memory\'s Journey, but you must target a player.').
card_ruling('memory\'s journey', '2011-09-22', 'If the player is an illegal target by the time Memory\'s Journey resolves, the spell will have no effect, even if the cards are still legal targets. This is because a spell can\'t make an illegal target (the player) perform any actions (such as shuffling his or her library).').
card_ruling('memory\'s journey', '2011-09-22', 'Any of the targeted cards that are illegal targets by the time Memory\'s Journey resolves aren\'t shuffled into their owner\'s library.').
card_ruling('memory\'s journey', '2011-09-22', 'If no cards were targeted by Memory\'s Journey or if all the targeted cards are illegal targets by the time Memory\'s Journey resolves, the targeted player will still shuffle his or her library.').
card_ruling('memory\'s journey', '2011-09-22', 'If you cast Memory\'s Journey with flashback, it won\'t be in the graveyard when you choose targets. It can\'t target itself.').

card_ruling('menacing ogre', '2004-10-04', 'You have to choose zero or a positive number. It must be an integer number.').

card_ruling('mental agony', '2012-05-01', 'If the targeted player has fewer than two cards in his or her hand, that player will still lose 2 life (and discard one card, if applicable).').

card_ruling('mental misstep', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('mental misstep', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('mental misstep', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('mental misstep', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('mental misstep', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').
card_ruling('mental misstep', '2011-06-01', 'If a spell has {X} in its mana cost, X is considered to be the value chosen for it while that spell is on the stack.').
card_ruling('mental misstep', '2011-06-01', 'Each Phyrexian mana symbol in a spell\'s mana cost contributes 1 to that spell\'s converted mana cost. For example, Mental Misstep\'s converted mana cost is 1, regardless of how its cost was paid.').

card_ruling('mental vapors', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('mental vapors', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('mental vapors', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('mental vapors', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('mental vapors', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('mental vapors', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('mental vapors', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('mental vapors', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('mental vapors', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('mental vapors', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('mental vapors', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('mentor of the meek', '2011-09-22', 'You only check the power of the other creature when it enters the battlefield. --If it\'s 2 or less, the ability will trigger. Once the ability triggers, raising that creature\'s power above 2 won\'t affect that ability. --Similarly, reducing the creature\'s power to 2 or less after it enters the battlefield won\'t cause the ability to trigger.').
card_ruling('mentor of the meek', '2011-09-22', 'Apply power bonuses from counters the creature enters the battlefield with and continuous effects like Mayor of Avabruck\'s when checking to see if Mentor of the Meek\'s ability will trigger.').
card_ruling('mentor of the meek', '2011-09-22', 'You choose whether or not to pay {1} when the triggered ability resolves.').

card_ruling('mephidross vampire', '2004-12-01', 'The triggered ability Mephidross Vampire grants to each creature you control triggers on any damage dealt to any creature, not just combat damage.').

card_ruling('mer-ek nightblade', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('mer-ek nightblade', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('mercadia\'s downfall', '2004-10-04', 'The creatures that are attacking is determined when this resolves and is not changed later, even if you have another combat phase.').

card_ruling('mercadian atlas', '2004-10-04', 'It only checks if you played a land. Putting a land onto the battlefield by using an effect of another card does not count as \"playing a land\".').

card_ruling('mercadian lift', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('merchant scroll', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('merchant ship', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('merciless eviction', '2013-01-24', 'You choose the mode as you cast the spell.').

card_ruling('merciless executioner', '2014-11-24', 'If you control no other creatures as Merciless Executioner’s ability resolves, you’ll have to sacrifice Merciless Executioner.').

card_ruling('mercurial chemister', '2012-10-01', 'If the discarded card has {X} in its mana cost, X is 0.').
card_ruling('mercurial chemister', '2012-10-01', 'If the discarded card has no mana symbols in its upper right corner (because it\'s a land card, for example), its converted mana cost is 0.').

card_ruling('mercurial pretender', '2014-07-18', 'Mercurial Pretender’s ability doesn’t target the chosen creature.').
card_ruling('mercurial pretender', '2014-07-18', 'The activated ability Mercurial Pretender gives itself becomes part of its copiable values. Unless the ability is overwritten by another copy effect, a creature that’s a copy of Mercurial Pretender will have that ability.').
card_ruling('mercurial pretender', '2014-07-18', 'Mercurial Pretender copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, etc. For example, if Mercurial Pretender enters the battlefield as a copy of a Profane Memento with Ensoul Artifact attached to it, Mercurial Pretender will be a noncreature Profane Memento with Mercurial Pretender’s added activated ability.').
card_ruling('mercurial pretender', '2014-07-18', 'If the chosen creature has {X} in its mana cost (such as Genesis Hydra), X is 0.').
card_ruling('mercurial pretender', '2014-07-18', 'If the chosen creature is copying something else (for example, if the chosen creature is another Mercurial Pretender), then Mercurial Pretender enters the battlefield as whatever the chosen creature is copying (plus any abilities it gained as part of the copying process, if applicable).').
card_ruling('mercurial pretender', '2014-07-18', 'If the chosen creature is a token, Mercurial Pretender copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Mercurial Pretender is not a token, even when copying one.').
card_ruling('mercurial pretender', '2014-07-18', 'Any enters-the-battlefield abilities of the copied creature will trigger when Mercurial Pretender enters the battlefield. Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the chosen creature will also work.').
card_ruling('mercurial pretender', '2014-07-18', 'If Mercurial Pretender somehow enters the battlefield at the same time as another creature, Mercurial Pretender can’t become a copy of that creature. You may only choose a creature that’s already on the battlefield.').
card_ruling('mercurial pretender', '2014-07-18', 'You can choose not to copy anything. In that case, Mercurial Pretender enters the battlefield as a 0/0 Shapeshifter creature, and is probably put into the graveyard immediately.').

card_ruling('mercy killing', '2013-07-01', 'You target the creature that will be sacrificed when you cast Mercy Killing, then its controller (who may or may not be you) sacrifices it when Mercy Killing resolves. Because this is a sacrifice, neither regeneration nor indestructibile will save the creature.').

card_ruling('merfolk assassin', '2006-10-15', 'Can target a creature with snow islandwalk.').

card_ruling('merfolk skyscout', '2010-06-15', 'When Merfolk Skyscout blocks, its second ability triggers just once, even if it somehow blocked multiple attacking creatures.').
card_ruling('merfolk skyscout', '2010-06-15', 'You may target any permanent with the ability, including Merfolk Skyscout itself or a permanent that\'s already untapped.').

card_ruling('merfolk sovereign', '2009-10-01', 'To have any effect, Merfolk Sovereign\'s activated ability must be activated before the declare blockers step begins. Once a Merfolk has become blocked, activating Merfolk Sovereign\'s ability won\'t change that.').

card_ruling('merfolk spy', '2010-08-15', 'Islandwalk cares about lands with the land type Island, not necessarily lands named Island.').
card_ruling('merfolk spy', '2010-08-15', 'The second ability triggers just once each time Merfolk Spy deals combat damage to a player, regardless of how much damage it deals.').
card_ruling('merfolk spy', '2010-08-15', 'After the second ability resolves, the revealed card stops being revealed in its owner\'s hand.').
card_ruling('merfolk spy', '2010-08-15', 'The next time Merfolk Spy deals combat damage to the same player, the card that\'s revealed at random may be the same card or may be a different card.').

card_ruling('merfolk thaumaturgist', '2007-02-01', 'This is the timeshifted version of Dwarven Thaumaturgist.').
card_ruling('merfolk thaumaturgist', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('merfolk thaumaturgist', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('merfolk wayfinder', '2009-10-01', 'If there are three or fewer cards in your library, you\'ll reveal all of them.').

card_ruling('merieke ri berit', '2006-09-25', 'If Merieke becomes untapped, its \"destroy\" ability will trigger, but you won\'t lose control of the creature you took before it\'s destroyed.').
card_ruling('merieke ri berit', '2006-09-25', 'If another player takes control of Merieke, you will immediately lose control of the creature you took, but Merieke\'s \"destroy\" ability won\'t trigger. The affected creature will go back under the control of the appropriate player.').
card_ruling('merieke ri berit', '2006-09-25', 'If Merieke leaves the battlefield, you will immediately lose control of the creature you took and Merieke\'s \"destroy\" ability will trigger. The affected creature will go back under the control of the appropriate player, then it will be destroyed.').

card_ruling('merrow bonegnawer', '2008-08-01', 'The targeted player chooses which card to exile.').

card_ruling('merrow grimeblotter', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('merrow grimeblotter', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('merrow grimeblotter', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('merrow wavebreakers', '2008-05-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('merrow wavebreakers', '2008-05-01', 'If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability, unless the creature has haste.').
card_ruling('merrow wavebreakers', '2008-05-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').

card_ruling('merseine', '2004-10-04', 'Can remove as many counters per turn as you want to and this can be done at any time instants are legal.').
card_ruling('merseine', '2004-10-04', 'The cost that needs to be paid includes colored mana requirements.').

card_ruling('mesa enchantress', '2005-08-01', 'This does not trigger on the moving of an Aura from one permanent to another.').
card_ruling('mesa enchantress', '2007-02-01', 'This is the timeshifted version of Verduran Enchantress.').
card_ruling('mesa enchantress', '2007-02-01', 'If an effect changes the targets of this ability, the second target can be changed to any creature the chosen opponent controls. The choice of opponent can\'t be changed.').
card_ruling('mesa enchantress', '2007-02-01', 'Tapped creatures can be targeted. Tapping them again will do nothing, but they\'ll still deal damage.').
card_ruling('mesa enchantress', '2009-10-01', 'If you cast an enchantment spell, Mesa Enchantress\'s ability triggers and is put on the stack on top of that spell. Mesa Enchantress\'s ability will resolve before the spell does.').

card_ruling('mesa pegasus', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('mesa pegasus', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('mesa pegasus', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('mesa pegasus', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('mesmeric orb', '2004-12-01', 'It triggers for each permanent that untaps, so if ten permanents you control untap, Mesmeric Orb\'s effect puts the top ten cards of your library into your graveyard.').
card_ruling('mesmeric orb', '2004-12-01', 'Mesmeric Orb triggers once for each permanent that untaps, so the cards are put into your graveyard one at a time.').
card_ruling('mesmeric orb', '2013-04-15', 'If permanents become untapped during the untap step, Mesmeric Orb\'s ability will trigger that many times. However, since no player gets priority during the untap step, those abilities wait to be put on the stack until the upkeep starts. At that time, all the \"beginning of upkeep\" triggers will also trigger. Those abililties and Mesmeric Orb\'s triggers can be put on the stack in any order.').

card_ruling('mesmeric sliver', '2007-05-01', 'This ability triggers when Mesmeric Sliver itself enters the battlefield.').
card_ruling('mesmeric sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('mesmeric sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('metallic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('metallic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('metallurgeon', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('metalworker', '2004-10-04', 'This is a mana ability. It does not go on the stack and can\'t be responded to.').
card_ruling('metalworker', '2004-10-04', 'You can reveal zero cards to gain zero mana.').

card_ruling('metamorphosis', '2004-10-04', 'This mana may be used on additional costs to cast the spell, such as Kicker.').
card_ruling('metamorphosis', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('metamorphosis', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('metathran aerostat', '2004-10-04', 'You can activate the ability in response to itself and get more than one creature card onto the battlefield before this returns to its owner\'s hand.').

card_ruling('metathran elite', '2005-08-01', 'Being \"enchanted\" means there is an Aura on it.').

card_ruling('meteor crater', '2004-10-04', 'You can\'t choose \"colorless\". You have to choose one of the five colors.').

card_ruling('meteor shower', '2004-10-04', 'You can\'t choose to do fractional damage to a target.').
card_ruling('meteor shower', '2008-10-01', 'A tip on how this math works: To deal a total of 1 damage, X is 0. The cost is {0} + {0} + {R} = {R}. To deal a total of 2 damage, X is 1. The cost is {1} + {1} + {R} = {2}{R}. To deal a total of 3 damage, X is 2. The cost is {2} + {2} + {R} = {4}{R}. And so on.').
card_ruling('meteor shower', '2008-10-01', 'The number of targets must be at least 1 and at most X + 1. You divide the damage as you cast Meteor Shower, not as it resolves. Each target must be assigned at least 1 damage.').

card_ruling('michiko konda, truth seeker', '2005-06-01', 'One permanent is sacrificed each time an opponent\'s source deals damage to Michiko Konda\'s controller. The amount of damage doesn\'t matter.').
card_ruling('michiko konda, truth seeker', '2005-06-01', 'Sources that deal damage multiple times (such as creatures with double strike) will trigger Michiko Konda multiple times.').
card_ruling('michiko konda, truth seeker', '2005-06-01', 'Spells that deal damage repeatedly trigger Michiko Konda\'s ability each time they deal damage. For example, a Glacial Ray spliced onto another Glacial Ray triggers the ability twice (once for each \"2 damage\" event from the Glacial Ray).').

card_ruling('midnight recovery', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('midnight recovery', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('midnight recovery', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('midnight recovery', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('midnight recovery', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('midnight recovery', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('midnight recovery', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('midnight recovery', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('midnight recovery', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('midnight recovery', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('midnight recovery', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('midsummer revel', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('might makes right', '2014-07-18', '“Each creature on the battlefield with the greatest power” may be a single creature if each other creature has less power.').
card_ruling('might makes right', '2014-07-18', 'The ability will trigger only if you control each creature on the battlefield with the greatest power as the beginning of combat step begins. If another player controls a creature with the greatest power or tied for the greatest power at that time, the ability won’t trigger at all.').
card_ruling('might makes right', '2014-07-18', 'The ability will check its condition again as it tries to resolve. If, at that time, another player controls a creature with the greatest power or tied for the greatest power, the ability won’t do anything.').
card_ruling('might makes right', '2014-07-18', 'The ability can target any creature an opponent controls, not just one that’s tapped.').
card_ruling('might makes right', '2014-07-18', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('might of alara', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('might of alara', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('might of alara', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('might of the masses', '2015-06-22', 'The bonus is determined as Might of the Masses resolves. It won\'t change if the number of creatures you control changes later in the turn.').
card_ruling('might of the masses', '2015-06-22', 'If you target a creature you control with Might of the Masses, remember to count that one when determining the amount of the bonus.').

card_ruling('might sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('might sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('mightstone', '2004-10-04', 'The +1/+0 applies to attacking creatures from all players.').

card_ruling('mighty emergence', '2008-10-01', 'The ability checks that creature\'s power only once: when that creature enters the battlefield. The trigger checks a creature\'s initial power upon being put on the battlefield, so it will take into account counters that it enters the battlefield with and static abilities that may give it a continuous power boost once it\'s on the battlefield (such as the one on Glorious Anthem). After the creature is already on the battlefield, boosting its power with a spell (such as Giant Growth), activated ability, or triggered ability won\'t allow this ability to trigger; it\'s too late by then. Once the ability triggers, it will resolve no matter what the creature\'s power may become while the ability is on the stack.').

card_ruling('mighty leap', '2015-06-22', 'Giving a creature flying after it’s already been blocked won’t change or undo that block. If you want the flying to affect what can block the creature, you must cast Mighty Leap during the declare attackers step at the latest.').

card_ruling('mijae djinn', '2004-10-04', 'The coin flip is done as a triggered ability on declaring the Djinn as an attacker.').
card_ruling('mijae djinn', '2004-10-04', 'If the Djinn attacks, but you lose the coin toss, then it is still considered to have attacked, and any other abilities that trigger on it attacking will resolve normally.').
card_ruling('mijae djinn', '2004-10-04', 'Any abilities which have already resolved before the coin flip are not undone. Any abilities that trigger when it attacks which have not already resolved will still resolve.').

card_ruling('mikaeus, the unhallowed', '2011-01-22', 'Noncombat damage dealt to you by a Human and damage dealt to you by a Human you control will also cause Mikaeus\'s triggered ability to trigger.').
card_ruling('mikaeus, the unhallowed', '2011-01-22', 'The +1/+1 bonus that Mikaeus gives to other non-Human creatures is not a counter and won\'t prevent the undying ability from working.').
card_ruling('mikaeus, the unhallowed', '2011-01-22', 'Mikaeus, the Unhallowed and the Innistrad set\'s Mikaeus, the Lunarch can be on the battlefield at the same time. The \"legend rule\" applies only to legendary permanents that have exactly the same name.').

card_ruling('military intelligence', '2014-07-18', 'The creatures don’t have to be attacking the same player or planeswalker.').
card_ruling('military intelligence', '2014-07-18', 'Once the ability triggers, it doesn’t matter how many creatures are still attacking when the ability resolves.').

card_ruling('militia\'s pride', '2007-10-01', 'The token enters the battlefield attacking. It doesn\'t trigger \"when a creature attacks\" triggers and is not subject to restrictions on declaring attackers.').
card_ruling('militia\'s pride', '2007-10-01', 'You choose which player or planeswalker the token is attacking. It doesn\'t have to attack the same player or planeswalker as the creature that caused the ability to trigger.').

card_ruling('millstone', '2004-10-04', 'Can be used on a player with less than 2 cards in their library. It will remove 0 or 1 cards if that is all that is available.').
card_ruling('millstone', '2004-10-04', 'It is not a draw effect so it will not cause a player with less than 2 cards in their library to lose.').
card_ruling('millstone', '2004-10-04', 'Since you are putting more than one card in the graveyard at one time, the affected player can choose the order the 2 cards go in.').

card_ruling('mimic vat', '2011-01-01', 'The imprint ability will trigger whenever a nontoken creature is put into any graveyard from the battlefield, not just your graveyard.').
card_ruling('mimic vat', '2011-01-01', 'Exiling the card as the first ability resolves is optional. If you choose not to exile it, or you can\'t exile it because the card has somehow left the graveyard before the ability resolves, the ability simply doesn\'t do anything as it resolves. Any card currently exiled by Mimic Vat remains exiled.').
card_ruling('mimic vat', '2011-01-01', 'If multiple nontoken creatures are put into their owners\' graveyards from the battlefield at the same time, the imprint ability will trigger that many times. You put the triggered abilities on the stack in any order, so you\'ll determine in which order they resolve. However, since exiling those cards is optional, and choosing to exile a card this way causes the previously exiled cards to return to their owners\' graveyards, the order generally doesn\'t matter: You\'ll wind up with at most one of those cards exiled, and the rest will be in the appropriate graveyards.').
card_ruling('mimic vat', '2011-01-01', 'You may exile a noncreature card with Mimic Vat\'s first ability. For example, if a nontoken artifact that\'s become a creature is put into a graveyard from the battlefield, Mimic Vat\'s first ability triggers and you may exile that card.').
card_ruling('mimic vat', '2011-01-01', 'The token created by the second ability will be a copy of whatever card is exiled with Mimic Vat at the time the ability resolves. This might not be the same card that was exiled with Mimic Vat at the time the ability was activated. It also might not be a creature card.').
card_ruling('mimic vat', '2011-01-01', 'If the token is a copy of a noncreature card, it will still have haste, though that won\'t matter unless that token somehow becomes a creature.').
card_ruling('mimic vat', '2011-01-01', 'You may activate the second ability even if no card has been exiled with Mimic Vat. If no card has been exiled with Mimic Vat by the time the ability resolves, no token will be created.').
card_ruling('mimic vat', '2011-01-01', 'If the exiled card has {X} in its mana cost (such as Protean Hydra), X is considered to be 0.').
card_ruling('mimic vat', '2011-01-01', 'Any enters-the-battlefield abilities of the exiled card will trigger when the token is put onto the battlefield. Any \"as [this permanent] enters the battlefield\" or \"[this permanent] enters the battlefield with\" abilities of the exiled card will also work.').
card_ruling('mimic vat', '2011-01-01', 'The token is exiled at the beginning of the next end step regardless of who controls it at that time, whether the exiled card is still exiled at that time, or whether Mimic Vat is still on the battlefield at that time.').
card_ruling('mimic vat', '2011-01-01', 'If Mimic Vat\'s second ability is activated during a turn\'s end step, the token will be exiled at the beginning of the following turn\'s end step.').
card_ruling('mimic vat', '2011-01-01', 'If the token isn\'t exiled when the delayed triggered ability resolves (due to Stifle, perhaps), it remains on the battlefield indefinitely. It continues to have haste.').

card_ruling('miming slime', '2013-01-24', 'The value of X is the greatest power among creatures you control when Miming Slime resolves. If you control no creatures at that time, X will be 0, creating a 0/0 Ooze token that will be put into your graveyard as a state-based action (unless something else is raising its toughness).').
card_ruling('miming slime', '2013-01-24', 'The power and toughness of the Ooze token is determined when Miming Slime resolves. It won\'t change as the greatest power among creatures you control changes.').

card_ruling('minamo', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('minamo', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('minamo', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('minamo', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('minamo', '2009-10-01', 'Minamo\'s first ability resolves before the spell that caused it to trigger.').
card_ruling('minamo', '2009-10-01', 'The {C} ability doesn\'t target the cards it will return, so they\'re not chosen until resolution. As the ability resolves, first the active player may choose a blue card in his or her graveyard, then each other player in turn order does the same, then all cards chosen this way are returned to their owners\' hands. Once a card is chosen, it\'s too late for players to respond.').

card_ruling('minamo scrollkeeper', '2013-04-15', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Minamo Scrollkeeper onto the battlefield, your maximum hand size will be three. However, if those permanents entered the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('mind bend', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('mind bend', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('mind bend', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('mind bend', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('mind bend', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('mind bend', '2004-10-04', 'You can\'t change proper nouns (i.e. card names) such as \"Island Fish Jasconius\".').
card_ruling('mind bend', '2004-10-04', 'It can be used to change a land\'s type from one basic land type to another. For example, Forest can be changed to Island so it produces blue mana. It doesn\'t change the name of any permanent.').

card_ruling('mind control', '2010-08-15', 'Gaining control of a creature doesn\'t cause you gain control of any Auras or Equipment attached to it.').

card_ruling('mind extraction', '2004-10-04', 'If you sacrifice a colorless creature, you get to see their hand but they discard no cards.').
card_ruling('mind extraction', '2004-10-04', 'If you sacrifice a multicolored creature, all cards that share at least one color with the creature are discarded.').
card_ruling('mind extraction', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('mind extraction', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('mind funeral', '2009-05-01', 'The four land cards, as well as all the other cards revealed during this process, are put into the graveyard. If there aren\'t four land cards left in the targeted opponent\'s library, that player\'s entire library is put into the graveyard.').

card_ruling('mind grind', '2013-01-24', 'If an opponent\'s library contains fewer than X land cards, all cards from that library are revealed and put into his or her graveyard.').
card_ruling('mind grind', '2013-01-24', 'If another spell or ability instructs you to cast Mind Grind “without paying its mana cost,” you won\'t be able to. You must pick 0 as the value of X in the mana cost of a spell being cast “without paying its mana cost,” but the X in Mind Grind\'s mana cost can\'t be 0.').

card_ruling('mind maggots', '2004-10-04', 'You can choose to discard zero creature cards.').

card_ruling('mind raker', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('mind raker', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('mind raker', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('mind raker', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('mind raker', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('mind raker', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('mind raker', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('mind raker', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('mind raker', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('mind ravel', '2004-10-04', 'You still draw a card if opponent had no cards in hand.').

card_ruling('mind sculpt', '2012-07-01', 'You can cast Mind Sculpt no matter how many cards are in the opponent\'s library. If there are fewer than seven, the opponent will put all of them into his or her graveyard.').

card_ruling('mind swords', '2004-10-04', 'The active player chooses two cards, then the other player chooses two cards, then all the chosen cards are exiled simultaneously.').

card_ruling('mind unbound', '2011-09-22', 'The ability is mandatory. You can\'t choose to draw fewer cards than the number of lore counters on Mind Unbound.').

card_ruling('mind warp', '2004-10-04', 'The target player decides what order to put the cards in the graveyard. You decide which cards, but they do the discarding.').

card_ruling('mind\'s desire', '2004-10-04', 'If the card is not played by end of turn, it remains exiled until end of game.').
card_ruling('mind\'s desire', '2004-10-04', 'The card is face-up when exiled.').
card_ruling('mind\'s desire', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('mind\'s desire', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('mind\'s desire', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('mind\'s desire', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('mindbreak trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('mindbreak trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('mindbreak trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('mindbreak trap', '2009-10-01', 'Mindbreak Trap\'s alternative cost condition checks whether an opponent cast three or more spells this turn, not whether those spells have resolved.').
card_ruling('mindbreak trap', '2009-10-01', 'If a spell is exiled, it\'s removed from the stack and thus will not resolve. The spell isn\'t countered; it just no longer exists. This works on spells that can\'t be countered, such as Terra Stomper.').

card_ruling('mindclaw shaman', '2012-07-01', 'Any player can respond to Mindclaw Shaman\'s triggered ability. However, once that ability starts resolving and the target opponent reveals his or her hand, players can\'t respond until after the ability finishes resolving. For example, the opponent can\'t cast the instant or sorcery card you choose to cast in order to stop you from casting it.').
card_ruling('mindclaw shaman', '2012-07-01', 'You cast the card from the opponent\'s hand immediately. Ignore timing restrictions based on the card\'s type. Other timing restrictions, such as \"Cast [this card] only during combat,\" must be followed.').
card_ruling('mindclaw shaman', '2012-07-01', 'The card goes to its owner\'s graveyard after it resolves (or otherwise leaves the stack), not yours.').
card_ruling('mindclaw shaman', '2012-07-01', 'If you can\'t cast any instant or sorcery card (perhaps because there are no legal targets available) or if you choose not to cast one, then Mindclaw Shaman\'s ability finishes resolving and the game moves on.').
card_ruling('mindclaw shaman', '2012-07-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('mindclaw shaman', '2012-07-01', 'If the card has X in its mana cost, you must choose 0 as its value.').

card_ruling('mindculling', '2011-06-01', 'You must be able to target an opponent to cast Mindculling. If that opponent is an illegal target when Mindculling tries to resolve, it\'ll be countered and will have no effect. You won\'t draw two cards.').
card_ruling('mindculling', '2011-06-01', 'If the opponent has one card in hand, that player will discard that card and you will draw two cards. If the opponent has no cards in hand, you will draw two cards.').

card_ruling('mindlash sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('mindlash sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('mindleech mass', '2005-10-01', 'The spell is cast during the resolution of Mindleech Mass\'s triggered ability. You may cast any nonland card in the player\'s hand, not just an instant card. This doesn\'t get around other prohibitions, such as \"Cast this card only before attackers are declared.\"').
card_ruling('mindleech mass', '2005-10-01', 'You control the spell and, if it\'s an artifact, creature, or enchantment spell, you control the resulting permanent.').
card_ruling('mindleech mass', '2005-10-01', 'The card is cast from your opponent\'s hand, not yours.').
card_ruling('mindleech mass', '2005-10-01', 'You choose modes, pay additional costs, choose targets, etc. for the spell as normal when casting it. Any X in the mana cost will be 0. Alternative costs can\'t be paid.').

card_ruling('mindless null', '2009-10-01', 'Whether you control a Vampire matters only at the time you declare blockers. Once Mindless Null blocks a creature, that won\'t change, even if all the Vampires you control leave the battlefield.').

card_ruling('mindlock orb', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('mindlock orb', '2008-10-01', 'If an effect says \"You may search your library . . . If you do, shuffle your library,\" you can\'t choose to search, so you won\'t shuffle.').
card_ruling('mindlock orb', '2008-10-01', 'If an effect says \"Search your library . . . Then shuffle your library,\" the search effect fails, but you will have to shuffle.').
card_ruling('mindlock orb', '2008-10-01', 'Since players can\'t search, players won\'t be able to find any cards in a library. The effect applies to all players and all libraries. If a spell or ability\'s effect has other parts that don\'t depend on searching for or finding cards, they will still work normally.').
card_ruling('mindlock orb', '2008-10-01', 'Effects that tell a player to reveal cards from a library or look at cards from the top of a library will still work. Only effects that use the word \"search\" will fail.').

card_ruling('mindmoil', '2005-10-01', 'Whenever you cast a spell, Mindmoil\'s ability triggers. The ability will resolve before the spell does.').
card_ruling('mindmoil', '2005-10-01', 'After Mindmoil\'s ability triggers, players may cast instants and activate activated abilities. Each time you cast an instant spell \"in response,\" Mindmoil\'s ability triggers again.').
card_ruling('mindmoil', '2005-10-01', 'After Mindmoil\'s ability resolves, but before the spell that triggered it does, you can cast instants from your new hand of cards. Each time you do, Mindmoil\'s ability triggers again.').

card_ruling('mindreaver', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('mindreaver', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('mindreaver', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('mindreaver', '2014-02-01', 'The cards are exiled face up. All players can look at them.').
card_ruling('mindreaver', '2014-02-01', 'If Mindreaver leaves the battlefield, and later another Mindreaver enters the battlefield, it is a new object (even if the two were represented by the same card). The last ability of the second Mindreaver doesn’t refer to any cards exiled with the first one.').

card_ruling('minds aglow', '2011-09-22', 'The card-drawing part of the effect is not optional. A player can\'t choose to draw fewer than X cards.').

card_ruling('mindslaver', '2004-10-04', 'Only lets you make decisions that the player would actually make. If another effect allows another player to make decisions that would normally be made by that player, such as having another player decide how combat damage is dealt, then the other effect takes precedence.').
card_ruling('mindslaver', '2004-12-01', 'You control them for the entire turn, from the untap step to the cleanup step.').
card_ruling('mindslaver', '2004-12-01', 'You could gain control of yourself using Mindslaver, but gaining control of yourself doesn\'t really do anything.').
card_ruling('mindslaver', '2004-12-01', 'You don\'t control any of the other player\'s permanents, spells, or abilities.').
card_ruling('mindslaver', '2004-12-01', 'You can\'t make the other player concede. A player can choose to concede at any time.').
card_ruling('mindslaver', '2004-12-01', 'You get to make every decision the other player would have made during that turn. You can\'t make any illegal decisions or illegal choices -- you can\'t do anything that player couldn\'t do. You can spend mana in the player\'s mana pool only on that player\'s spells and abilities. The mana in your mana pool can be spent only on your spells and abilities.').
card_ruling('mindslaver', '2004-12-01', 'You choose which spells the other player casts, and make all decisions as those spells are cast and when they resolve. For example, you choose the target for that player\'s Shock, and what card that player gets with Diabolic Tutor.').
card_ruling('mindslaver', '2004-12-01', 'You choose which activated abilities the other player activates, and make all decisions as those abilities are activated and when they resolve. For example, you can have your opponent sacrifice his or her creatures to his or her Nantuko Husk or have your opponent\'s Timberwatch Elf give your blocking creature +X/+X.').
card_ruling('mindslaver', '2004-12-01', 'You make all decisions for the other player\'s triggered abilities, including what they target and any decisions made when they resolve.').
card_ruling('mindslaver', '2004-12-01', 'You choose which creatures attack and how those attacking creatures assign their combat damage.').
card_ruling('mindslaver', '2004-12-01', 'You also make choices for your own permanents, spells, and abilities as usual.').
card_ruling('mindslaver', '2004-12-01', 'You can\'t make any decisions that aren\'t called for or allowed by the game rules, or by any cards, permanents, spells, abilities, and so on.').
card_ruling('mindslaver', '2004-12-01', 'If you make another player cast Shahrazad, you don\'t control that player in the subgame, but you continue to control them once the subgame is completed.').
card_ruling('mindslaver', '2012-07-01', 'You can see everything that player can see but you normally could not. This includes that player\'s hand, face-down creatures, his or her sideboard, and any cards in his or her library that he or she looks at.').

card_ruling('mindsparker', '2013-07-01', 'Mindsparker’s ability will trigger whenever an opponent casts an instant or sorcery spell that’s white, blue, or both. If the spell is both white and blue, Mindsparker’s ability will trigger only once.').
card_ruling('mindsparker', '2013-07-01', 'If an opponent casts a copy of a white or blue instant or sorcery card (as opposed to putting a copy of such a spell directly on the stack), the ability will trigger.').

card_ruling('mindstab', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('mindstab', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('mindstab', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('mindstab', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('mindstab', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('mindstab', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('mindstab', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('mindstab', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('mindstab', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('mindstab', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('mindstab thrull', '2004-10-04', 'The defending player gets to choose which cards they discard.').
card_ruling('mindstab thrull', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('mindstorm crown', '2004-12-01', 'Mindstorm Crown\'s ability doesn\'t have an \"intervening \'if\' clause.\" It always goes onto the stack at the start of your upkeep.').
card_ruling('mindstorm crown', '2004-12-01', 'Mindstorm Crown cares about the number of cards in your hand at the time the turn started, not as the ability resolves. So if you have more than one Crown on the battlefield and no cards in your hand at the start of the turn, you draw a card for each Crown.').

card_ruling('mindswipe', '2014-09-20', 'If the controller of the target spell pays {X}, the spell won’t be countered. Mindswipe will still deal damage to that player.').
card_ruling('mindswipe', '2014-09-20', 'If the target spell is an illegal target as Mindswipe tries to resolve (perhaps because it was countered by another spell or ability), Mindswipe will be countered and none of its effects will happen. No damage will be dealt.').

card_ruling('mindwhip sliver', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').
card_ruling('mindwhip sliver', '2013-04-15', 'If you cast a Sliver as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').
card_ruling('mindwhip sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('mindwhip sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('mindwrack liege', '2008-08-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('minion of leshrac', '2008-10-01', 'If you don\'t sacrifice a creature at the beginning of your upkeep (either by choice or because you can\'t), but you somehow prevent all the damage Minion of Leshrac would deal to you, then you don\'t tap Minion of Leshrac.').
card_ruling('minion of leshrac', '2008-10-01', 'Minion of Leshrac can\'t target itself with its activated ability because it has protection from black.').

card_ruling('minion of the wastes', '2011-01-25', 'The life is paid as Minion of the Wastes enters the battlefield. If Minion of the Wastes is countered, you don\'t get the opportunity to pay any life.').

card_ruling('minion reflector', '2008-10-01', 'As the token is created, it checks the printed values of the creature it\'s copying, as well as any copy effects that have been applied to it. It won\'t copy counters on the creature, nor will it copy other effects that have changed the creature\'s power, toughness, types, color, and so on.').
card_ruling('minion reflector', '2008-10-01', 'The copiable values of the token\'s characteristics are the same as the copiable values of the characteristics of the creature it\'s copying, plus haste and \"At end of turn, sacrifice this permanent.\" That means if something becomes a copy of the token, it will also have haste and it will also have to be sacrificed at the end of the turn.').
card_ruling('minion reflector', '2008-10-01', 'If the creature that caused Minion Reflector\'s ability to trigger has already left the battlefield by the time the ability resolves, you can still pay {2}. If you do, you\'ll still put a token onto the battlefield. That token has the copiable values of the characteristics of that nontoken creature as it last existed on the battlefield.').

card_ruling('minister of pain', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('minister of pain', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('minister of pain', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('minister of pain', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('minister of pain', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('mirari', '2004-10-04', 'Everything about the original spell is copied, including any decisions made on announcement, such as whether it was kicked or its Buyback cost was paid. Effects on the spell, such as Sleight of Mind or Flashback, are not copied.').
card_ruling('mirari', '2004-10-04', 'If the copied spell requires a target and there are no legal targets, then the copy has the same target (which should be illegal) as the original.').
card_ruling('mirari', '2004-10-04', 'If the spell is countered before the triggered ability resolves, then the ability will make a copy of how the spell looked right before it was countered.').
card_ruling('mirari', '2004-10-04', 'It triggers once per spell, so you can\'t get multiple copies of a spell using this card. And since this card is legendary, you can\'t control more than one.').
card_ruling('mirari', '2004-10-04', 'The copy is not \"cast\" so it will not trigger anything that triggers on a spell being cast.').
card_ruling('mirari', '2004-10-04', 'You choose whether or not to pay when the triggered ability resolves.').
card_ruling('mirari', '2004-10-04', 'You are not required to choose new targets. If you do choose, then the targets must be legal. If you don\'t change them, the spell is placed on the stack whether or not the targets are legal. The legality of the targets is always checked on resolution of the spell, so the spell may be countered at that time if all of its targets are illegal.').

card_ruling('mire blight', '2009-10-01', 'Mire Blight\'s ability triggers when the enchanted creature is dealt any kind of damage, not just combat damage.').
card_ruling('mire blight', '2009-10-01', 'If Mire Blight\'s ability triggers, the creature it was enchanting at that time is the one that will be destroyed when the ability resolves. It doesn\'t matter if Mire Blight leaves the battlefield or somehow becomes attached to another creature by that time.').

card_ruling('mire\'s malice', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('mire\'s malice', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('mire\'s malice', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('mire\'s malice', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('mire\'s malice', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('mire\'s malice', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('mire\'s toll', '2010-03-01', 'If the number of Swamps you control exceeds the number of cards in the targeted player\'s hand, that player reveals all the cards in his or her hand.').

card_ruling('mirko vosk, mind drinker', '2013-04-15', 'If the player has fewer than four land cards in his or her library, all cards from that library will be revealed and put into that player\'s graveyard.').

card_ruling('mirozel', '2004-10-04', 'Triggers on a spell or ability being changed to target this card.').
card_ruling('mirozel', '2004-10-04', 'Triggers on any spell or ability being announced which targets this card. This means it returns to owner\'s hand before that spell resolves.').

card_ruling('mirri the cursed', '2007-02-01', 'If Mirri the Cursed is in combat with a creature that doesn\'t have first strike or double strike, Mirri will deal combat damage to that creature, then get a +1/+1 counter, then (if the other creature is still on the battlefield) Mirri will be dealt combat damage.').

card_ruling('mirror entity', '2007-10-01', 'Activating the ability with X = 0 will cause all creatures you control to become 0/0 and be put into the graveyard.').
card_ruling('mirror entity', '2007-10-01', 'Activating the ability for a second time in the same turn will overwrite the first activation. For example, if you activate the ability with X = 4, then activate it with X = 1, each of your creatures will be 1/1.').
card_ruling('mirror entity', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').
card_ruling('mirror entity', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('mirror gallery', '2005-02-01', 'Mirror Gallery removes the \"Legend rule\" while it\'s on the battlefield. If all Mirror Galleries leave the battlefield, the \"Legend rule\" will apply the next time state-based actions are performed.').

card_ruling('mirror mockery', '2015-02-25', 'Notably, the token won’t be attacking.').
card_ruling('mirror mockery', '2015-02-25', 'If Mirror Mockery enchants a creature you don’t control, and you are the defending player, you can block with the token.').
card_ruling('mirror mockery', '2015-02-25', 'The ability that exiles the token will trigger at end of combat no matter what happens to the original creature or to Mirror Mockery.').
card_ruling('mirror mockery', '2015-02-25', 'If Mirror Mockery is no longer on the battlefield as its triggered ability resolves, its ability makes a copy of the creature it was enchanting when it left the battlefield. If it left the battlefield because it was no longer enchanting a legal creature, it makes a copy of the creature it was most recently enchanting before it left the battlefield.').
card_ruling('mirror mockery', '2015-02-25', 'If Mirror Mockery’s ability creates multiple tokens (due to an effect such as the one Doubling Season creates), all such tokens will be exiled by the delayed triggered ability.').
card_ruling('mirror mockery', '2015-02-25', 'A token that enters the battlefield as a copy of a face-down creature is a face-up colorless 2/2 creature with no name, abilities, or creature types.').
card_ruling('mirror mockery', '2015-02-25', 'The token copies exactly what was printed on the original creature and nothing else (unless that permanent is copying something else or is a token; see below). It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('mirror mockery', '2015-02-25', 'If the copied creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('mirror mockery', '2015-02-25', 'If the copied creature is copying something else, then the token enters the battlefield as whatever that creature copied.').
card_ruling('mirror mockery', '2015-02-25', 'If the copied creature is a token, the token created by Mirror Mockery copies the original characteristics of that token as stated by the effect that put the token onto the battlefield.').
card_ruling('mirror mockery', '2015-02-25', 'Any enters-the-battlefield abilities of the copied creature will trigger when the token enters the battlefield. Any “as [this permanent] enters the battlefield” or “[this permanent] enters the battlefield with” abilities of the copied creature will also work.').

card_ruling('mirror of fate', '2009-10-01', 'You don\'t choose the exiled cards until Mirror of Fate\'s ability resolves. It doesn\'t matter how they wound up in the exile zone.').
card_ruling('mirror of fate', '2009-10-01', 'Exiled cards are face up by default; they\'re face down only if the effect that exiled them said so. For example, the cards from your library that Mirror of Fate exiles are exiled face up.').
card_ruling('mirror of fate', '2009-10-01', 'You choose the order that the chosen cards are put on top of your library. You don\'t have to show anyone else that order.').

card_ruling('mirror sheen', '2008-08-01', 'The targeted spell can have any number of targets, as long as you are one of them.').
card_ruling('mirror sheen', '2008-08-01', 'It doesn\'t matter who controls the spell.').
card_ruling('mirror sheen', '2008-08-01', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them.').

card_ruling('mirror universe', '2007-09-16', 'When the life totals are exchanged, each player gains or loses the amount of life necessary to equal the other player\'s previous life total. For example, if player A has 5 life and player B has 3 life before the exchange, player A will lose 2 life and player B will gain 2 life. Replacement effects may modify these gains and losses, and triggered abilities may trigger on them.').
card_ruling('mirror universe', '2011-01-01', 'If an effect says that a player can\'t lose life, that player can\'t exchange life totals with a player who has a lower life total; in that case, the exchange won\'t happen.').

card_ruling('mirror-mad phantasm', '2011-09-22', 'You can only activate the ability if you control Mirror-Mad Phantasm, even if you don\'t own it.').
card_ruling('mirror-mad phantasm', '2011-09-22', 'If no card named Mirror-Mad Phantasm is revealed (possibly because it was a card copying Mirror-Mad Phantasm or it was a token), all cards from that library will be put into their owner\'s graveyard.').
card_ruling('mirror-mad phantasm', '2011-09-22', 'If another creature (such as Necrotic Ooze) gains Mirror-Mad Phantasm\'s activated ability, its owner reveals cards until he or she reveals a card named Mirror-Mad Phantasm.').

card_ruling('mirror-sigil sergeant', '2009-02-01', 'The \"intervening \'if\' clause\" means that (1) the ability won\'t trigger at all unless you control a permanent of the specified color, and (2) the ability will do nothing unless you control a permanent of the specified color at the time it resolves.').
card_ruling('mirror-sigil sergeant', '2009-02-01', 'Normally, when a token is created by this ability, it will simply be a Mirror-Sigil Sergeant, so it\'ll also have the token-creating ability. (See the other ruling for weird exceptions.) At the beginning of your next upkeep, if you still control the original Sergeant, the token copy, and a blue permanent, you\'ll get two more token copies; the turn after that you\'ll get four; then eight; and so on.').
card_ruling('mirror-sigil sergeant', '2009-02-01', 'Here\'s the detailed version of what happens. As the token is created, it checks the printed values of the Mirror-Sigil Sergeant it\'s copying -- or, if the Mirror-Sigil Sergeant whose ability triggered was itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the Mirror-Sigil Sergeant, nor will it copy other effects that have changed Mirror-Sigil Sergeant\'s power, toughness, types, color, or so on.').
card_ruling('mirror-sigil sergeant', '2009-02-01', 'If Mirror-Sigil Sergeant has left the battlefield by the time its triggered ability resolves, you\'ll still put a token onto the battlefield. That token has the copiable values of the characteristics of Mirror-Sigil Sergeant as it last existed on the battlefield.').
card_ruling('mirror-sigil sergeant', '2009-02-01', 'Here are the weird exceptions. If any copy effects have affected the Mirror-Sigil Sergeant whose ability triggered, they\'re taken into account when the token is created. For example: -- If Mirror-Sigil Sergeant\'s ability triggers, then Mirror-Sigil Sergeant temporarily becomes a copy of another creature before its ability resolves (due to Mirrorweave, perhaps), the token will be a copy of whatever creature the Mirror-Sigil Sergeant is currently a copy of. After the turn ends, the Mirrorweaved Mirror-Sigil Sergeant reverts back to what it was, but the token will stay as it is. -- If the copy ability of a creature (such as Cemetery Puca, perhaps) makes it become a copy of Mirror-Sigil Sergeant and gain another ability, the token created by this creature at the beginning of your upkeep will be a Mirror-Sigil Sergeant with that additional ability.').

card_ruling('mirrorweave', '2008-05-01', 'Each other creature copies the printed values of the targeted creature, plus any copy effects that have been applied to that creature. They won\'t copy other effects that have changed the targeted creature\'s power, toughness, types, color, or so on. They also won\'t copy counters on the targeted creature (they\'ll each just retain the counters they already have).').
card_ruling('mirrorweave', '2008-05-01', 'If the targeted creature is itself copying a creature, each other creature will become whatever it\'s copying, as modified by that copy effect. For example, if you target a Cemetery Puca that\'s copying a Grizzly Bears (a 2/2 creature with no abilities), each other creature will become a Grizzly Bears with the Cemetery Puca ability.').
card_ruling('mirrorweave', '2008-05-01', 'This effect can cause each other creature to stop being a creature. For example, if you target an animated Mutavault (a land with an activated ability that turns it into a creature), only the printed wording will be copied -- the \"becomes a creature\" effect won\'t. Each other creature will become an unanimated Mutavault.').
card_ruling('mirrorweave', '2008-05-01', 'Noncopy effects that have already applied to the other creatures will continue to apply to them. For example, if Giant Growth had given one of them +3/+3 earlier in the turn, then Mirrorweave made it a copy of Grizzly Bears, it will be a 5/5 Grizzly Bears.').
card_ruling('mirrorweave', '2008-05-01', 'If the targeted creature is a token, each other creature copies the original characteristics of that token as stated by the effect that put it onto the battlefield. Those creatures don\'t become tokens.').
card_ruling('mirrorweave', '2008-05-01', 'As the turn ends, the other creatures revert to what they were before. If two Mirrorweaves are cast on the same turn, they\'ll both wear off at the same time.').

card_ruling('mirrorwood treefolk', '2004-10-04', 'If the target creature is not on the battlefield (or is not a creature) at the time the damage would be redirected, then the damage goes on this card.').
card_ruling('mirrorwood treefolk', '2004-10-04', 'The ability applies to the next time damage is dealt. Normally this means damage from one source at one time, but during combat it is possible for multiple sources to damage the Treefolk at one time, in which case damage from all of those sources is redirected.').

card_ruling('mirrorworks', '2011-06-01', 'Each time the ability triggers, you can pay {2} only one time to get one token.').
card_ruling('mirrorworks', '2011-06-01', 'As the token is created, it checks the printed values of the artifact it\'s copying, as well as any copy effects that have been applied to it.').
card_ruling('mirrorworks', '2011-06-01', 'The copiable values of the token\'s characteristics are the same as the copiable values of the characteristics of the artifact it\'s copying.').
card_ruling('mirrorworks', '2011-06-01', 'If the artifact that caused Mirrorworks\'s ability to trigger has already left the battlefield by the time the ability resolves, you can still pay {2}. If you do, you\'ll still put a token onto the battlefield. That token has the copiable values of the characteristics of that nontoken artifact as it last existed on the battlefield.').

card_ruling('miscalculation', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('mischievous quanar', '2013-04-15', 'If a spell with split second is on the stack, you can still respond by turning this creature face up and targeting that spell with the trigger. This is because split second only stops players from casting spells or activating abilities, while turning a creature face up is a special action.').

card_ruling('misdirection', '2004-10-04', 'Once the spell resolves, the new target is considered to be targeted by the deflected spell. This will trigger any effects which trigger on being targeted.').
card_ruling('misdirection', '2004-10-04', 'This only targets the spell being changed, not the original or new target of the spell it is affecting.').
card_ruling('misdirection', '2004-10-04', 'The target of a spell that targets another spell on the stack can be changed to any other spell on the stack, even if the new target will resolve before the spell does.').
card_ruling('misdirection', '2004-10-04', 'You can\'t make a spell which is on the stack target itself.').
card_ruling('misdirection', '2004-10-04', 'You can choose to make a spell on the stack target this spell (if such a target choice would be legal had the spell been cast while this spell was on the stack). The new target for the deflected spell is not chosen until this spell resolves. This spell is still on the stack when new targets are selected for the spell.').
card_ruling('misdirection', '2004-10-04', 'If there is no other legal target for the spell, this does not change the target.').
card_ruling('misdirection', '2004-10-04', 'This does not check if the current target is legal. It just checks if the spell has a single target.').
card_ruling('misdirection', '2004-10-04', 'You choose the spell to target on announcement, but you pick the new target for that spell on resolution.').

card_ruling('misers\' cage', '2004-10-04', 'It affects all opponents.').

card_ruling('misfortune\'s gain', '2008-04-01', 'The creature\'s *owner* gains the life, regardless of who happens to control it.').
card_ruling('misfortune\'s gain', '2009-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').

card_ruling('mishra\'s factory', '2004-10-04', 'When it is an Assembly-Worker, it is an artifact and can therefore be affected by spells and abilities that affect artifacts.').
card_ruling('mishra\'s factory', '2004-10-04', 'When it is an Assembly-Worker, it is still a land and retains all of its other abilities.').
card_ruling('mishra\'s factory', '2004-10-04', 'Any counters on the Assembly-Worker remain even if the counters stop being meaningful when it de-animates.').
card_ruling('mishra\'s factory', '2004-10-04', 'If an Assembly-Worker is changed to another land type, it will stay a 2/2 artifact creature and keep the creature type \"Assembly-Worker\" (until end of turn), but it will change its land type as directed.').
card_ruling('mishra\'s factory', '2004-10-04', 'Tapping a land for something other than mana is not a mana ability.').
card_ruling('mishra\'s factory', '2004-10-04', 'When animated into an Assembly-Worker, it has creature type Assembly-Worker.').
card_ruling('mishra\'s factory', '2004-10-04', 'The card name does not change. It remains \"Mishra\'s Factory\".').
card_ruling('mishra\'s factory', '2005-08-01', 'If another player takes control of the Factory while it is animated, that player keeps control until the control effect ends. They do not lose control just because the card stops being an Assembly-Worker. Note that Auras which steal cards will go to the graveyard if the permanent they enchant is no longer valid.').
card_ruling('mishra\'s factory', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('mishra\'s factory', '2009-10-01', 'Activating the ability that turns it into a creature while it\'s already a creature will override any effects that set its power and/or toughness to a specific number. However, any effect that raises or lowers power and/or toughness (such as the effect created by Giant Growth, Glorious Anthem, or a +1/+1 counter) will continue to apply.').

card_ruling('mishra\'s groundbreaker', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('mishra\'s groundbreaker', '2008-10-01', 'The ability\'s effect has no duration. The affected land will remain an artifact, creature, and land until the game ends, it leaves the battlefield, or some other effect changes its types.').

card_ruling('mishra\'s helix', '2004-10-04', 'You can target already tapped lands.').

card_ruling('mishra\'s war machine', '2004-10-04', 'You may choose to take damage or to discard. You can\'t avoid taking damage if you have no cards to discard.').
card_ruling('mishra\'s war machine', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('mishra\'s war machine', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('mishra\'s war machine', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('mishra\'s war machine', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('mishra\'s workshop', '2004-10-04', 'You can spend the mana on costs on the spell\'s text.').
card_ruling('mishra\'s workshop', '2004-10-04', 'The mana can\'t be used to pay Echo costs.').
card_ruling('mishra\'s workshop', '2004-10-04', 'This mana may not be used to pay costs imposed after the spell is initially cast.').
card_ruling('mishra\'s workshop', '2004-10-04', 'This mana may be used on additional costs to cast the spell, such as Kicker.').

card_ruling('misinformation', '2008-10-01', 'You target the cards, but not the opponent.').
card_ruling('misinformation', '2008-10-01', 'You don\'t need to show the opponent what order you put the cards on top of his or her library.').

card_ruling('misstep', '2004-10-04', 'The creatures are only prevented from untapping during the targeted player\'s next untap step. They can still can untap during other player\'s untap steps.').
card_ruling('misstep', '2008-08-01', 'No creature controlled by the player will untap during his or her next untap step. This includes creatures which entered the battlefield after Misstep resolved.').

card_ruling('mist intruder', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('mist intruder', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('mist intruder', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('mist intruder', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('mist intruder', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('mist intruder', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('mist intruder', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('mist raven', '2012-05-01', 'Mist Raven\'s ability is mandatory. If Mist Raven is the only creature on the battlefield when its ability triggers, you must choose it as the target.').

card_ruling('mistcutter hydra', '2013-09-15', 'Spells that can’t be countered can still be chosen as the target of spells or abilities that try to counter them. Although the spell won’t be countered, the spell or ability that tries to counter it will resolve, and any other effects of that spell or ability will happen.').

card_ruling('mistfire adept', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('mistfire adept', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('mistfire adept', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('mistfire adept', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('mistfire weaver', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('mistfire weaver', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('mistfire weaver', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('mistfire weaver', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('mistfire weaver', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('mistfire weaver', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('mistfire weaver', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('mistfire weaver', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('mistfire weaver', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('mistform sliver', '2004-10-04', 'Unlike most type changing effects, this one adds a creature type instead of replacing existing creature types.').
card_ruling('mistform sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('mistform sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('mistform ultimus', '2006-09-25', 'The type \"Illusion\" is printed on the card\'s type line purely for flavor. The Ultimus has every other creature type as well.').
card_ruling('mistform ultimus', '2006-09-25', 'Like any creature, if Mistform Ultimus becomes another type as the result of a spell or ability, it will lose all its other creature types.').
card_ruling('mistform ultimus', '2006-09-25', 'Mistform Ultimus has all creature types even if an effect removes its ability. Type-changing static abilities will apply in layer 4 before effects such as Sudden Spoiling\'s remove them in layer 6.').

card_ruling('misthollow griffin', '2012-05-01', 'Misthollow Griffin can also be cast from a player\'s hand.').
card_ruling('misthollow griffin', '2012-05-01', 'Misthollow Griffin doesn\'t have an ability that exiles itself. Some other effect will be needed to get the card into exile.').
card_ruling('misthollow griffin', '2012-05-01', 'You must still pay Misthollow Griffin\'s mana cost when you cast it from exile.').
card_ruling('misthollow griffin', '2012-05-01', 'Misthollow Griffin\'s ability doesn\'t change when you can cast creature spells: when you have priority during your main phase when the stack is empty.').

card_ruling('misthoof kirin', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('misthoof kirin', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('misthoof kirin', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('mistmeadow skulk', '2007-05-01', 'While a spell with X in its cost is on the stack, its converted mana cost takes the chosen value of X into account. Mistmeadow Skulk can be chosen as a target for a Blaze (which has mana cost {X}{R}) if X is 0 or 1, for example, but it can\'t be chosen as a target for a Blaze if X is 2 or more.').
card_ruling('mistmeadow skulk', '2007-05-01', 'A face-down creature has a converted mana cost of 0, so Mistmeadow Skulk doesn\'t have protection from it .').
card_ruling('mistmeadow skulk', '2008-05-01', 'The protection ability means the following: - Mistmeadow Skulk can\'t be blocked by creatures with converted mana cost 3 or greater. - Mistmeadow Skulk can\'t be enchanted by Auras with converted mana cost 3 or greater. It also can\'t be equipped by Equipment with converted mana cost 3 or greater. - Mistmeadow Skulk can\'t be targeted by spells with converted mana cost 3 or greater. It also can\'t be targeted by abilities from sources with converted mana cost 3 or greater. - All damage that would be dealt to Mistmeadow Skulk by sources with converted mana cost 3 or greater is prevented.').

card_ruling('mistmoon griffin', '2004-10-04', 'You still put the top creature card from your graveyard onto the battlefield even if this card is not in your graveyard when the triggered ability resolves.').
card_ruling('mistmoon griffin', '2008-04-01', 'The “top” card of your graveyard is the card that was put there most recently.').
card_ruling('mistmoon griffin', '2008-04-01', 'Players may not rearrange the cards in their graveyards. This is a little-known rule because new cards that care about graveyard order haven\'t been printed in years.').
card_ruling('mistmoon griffin', '2008-04-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('mistmoon griffin', '2008-04-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. --Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('mistmoon griffin', '2008-04-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').
card_ruling('mistmoon griffin', '2008-04-01', 'If you control Mistmoon Griffin when it goes to the graveyard, you exile the Griffin and return the top creature card from your graveyard to the battlefield. It doesn\'t matter whose graveyard the Griffin goes to.').

card_ruling('mistvein borderpost', '2009-05-01', 'Casting this card by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast an artifact spell. It also doesn\'t change the spell\'s mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('mistvein borderpost', '2009-05-01', 'Effects that increase or reduce the cost to cast this card will apply to whichever cost you chose to pay.').
card_ruling('mistvein borderpost', '2009-05-01', 'To satisfy the alternative cost, you may return any basic land you control to its owner\'s hand, regardless of that land\'s subtype or whether it\'s tapped.').
card_ruling('mistvein borderpost', '2009-05-01', 'As you cast a spell, you get a chance to activate mana abilities before you pay that spell\'s costs. Therefore, you may tap a basic land for mana, then both spend that mana and return that land to your hand to pay this card\'s alternative cost. (Of course, you can return a different basic land instead.)').

card_ruling('mitotic manipulation', '2011-06-01', '\"The same name as a permanent\" means the same name as a permanent currently on the battlefield as Mitotic Manipulation resolves.').
card_ruling('mitotic manipulation', '2011-06-01', 'No matter how many of the seven cards have the same name as a permanent, you can put only one of them onto the battlefield.').
card_ruling('mitotic manipulation', '2011-06-01', 'You don\'t reveal any of the other cards or the order in which they go to the bottom of your library.').

card_ruling('mizzium meddler', '2015-06-22', 'If changing one target of a spell or ability to Mizzium Meddler would make other targets of that spell or ability illegal, the targets remain unchanged.').
card_ruling('mizzium meddler', '2015-06-22', 'If the spell or ability has multiple instances of the word “target,” you choose which target you’re changing to Mizzium Meddler as Mizium Meddler’s ability resolves.').
card_ruling('mizzium meddler', '2015-06-22', 'The target of the spell or ability won’t change unless Mizzium Meddler fulfills all the targeting criteria. If a spell or ability has multiple targets, such as Send to Sleep, you can change only one of the targets to Mizzium Meddler.').
card_ruling('mizzium meddler', '2015-06-22', 'If a spell or ability has a variable number of targets, you can’t change the number of targets.').
card_ruling('mizzium meddler', '2015-06-22', 'If Mizzium Meddler leaves the battlefield before its ability resolves, the targets remain unchanged.').
card_ruling('mizzium meddler', '2015-06-22', 'Mizzium Meddler’s triggered ability can target a spell or ability even if Mizzium Meddler wouldn’t be a legal target for that spell or ability. However, the target of that spell or ability will remain unchanged.').
card_ruling('mizzium meddler', '2015-06-22', 'If Mizzium Meddler’s triggered ability targets a spell or ability with no targets, nothing happens.').

card_ruling('mizzium mortars', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('mizzium mortars', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('mizzium mortars', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('mizzium mortars', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('mizzium mortars', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('mizzium mortars', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('mizzium mortars', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('mizzium skin', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('mizzium skin', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('mizzium skin', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('mizzium skin', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('mizzium skin', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('mizzium skin', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('mizzium skin', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('mizzium transreliquat', '2006-02-01', 'If you use the first ability, Mizzium Transreliquat loses both of its copy abilities until the effect wears off (unless it copied itself or another Mizzium Transreliquat). When it wears off, Mizzium Transreliquat loses all abilities it gained this way and goes back to being what it was before.').
card_ruling('mizzium transreliquat', '2006-02-01', 'The second ability doesn\'t wear off. If you use it, Mizzium Transreliquat becomes a copy of target artifact permanently and gains the {1}{U}{R} ability. It no longer has the {3} ability (unless it copied itself or another Mizzium Transreliquat).').
card_ruling('mizzium transreliquat', '2006-02-01', 'An artifact\'s \"copiable values\" are those printed on it, as modified by other copy effects, plus any values set by \"enters the battlefield as\" abilities. Counters and other effects aren\'t copied. For example, if you copy a Gruul War Plow that\'s been turned into an artifact creature, the result will be just an artifact.').
card_ruling('mizzium transreliquat', '2006-02-01', 'The results of all copy effects are copied. If Mizzium Transreliquat is copied, the copy will be a Mizzium Transreliquat after applying all copy effects currently affecting the original. For example, Copy Artifact copying a Transreliquat that\'s using its first ability to copy an Izzet Signet will be an Izzet Signet. The effect won\'t wear off at the end of the turn; rather, the Copy Artifact will remain an Izzet Signet for the rest of the game.').
card_ruling('mizzium transreliquat', '2006-02-01', 'If you use Mizzium Transreliquat\'s abilities multiple times in a turn in response to one another, then each time one of those abilities resolves, it will overwrite whatever the permanent was copying. The Transreliquat will wind up as a copy of the artifact targeted by the last ability to resolve. When the turn ends, all instances of its first ability will wear off at the same time. If one of those was the last copy ability to resolve, the Transreliquat will become what it was before those abilities resolved. This will likely be either the original Mizzium Transreliquat or whatever it copied with the last instance of its second ability to resolve.').

card_ruling('mnemonic nexus', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('mnemonic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('mnemonic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('mob justice', '2004-10-04', 'Creatures are counted on resolution.').

card_ruling('mob rule', '2014-11-24', 'You choose which mode you’re using as you cast Mob Rule. Once it’s cast, you can’t change its mode, even if creatures’ powers change in response.').
card_ruling('mob rule', '2014-11-24', 'Once you gain control of a creature, it doesn’t matter what happens to its power.').
card_ruling('mob rule', '2014-11-24', 'Mob Rule can affect creatures you already control or ones that are already untapped. It will untap them if applicable and give them haste.').
card_ruling('mob rule', '2014-11-24', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('mogg assassin', '2004-10-04', 'You and your opponent both pick the target creatures on announcing, which is before the coin is flipped. You pick first.').

card_ruling('mogg bombers', '2004-10-04', 'The Mogg Bombers do trigger on any creatures that enter the battlefield at the same time they do.').
card_ruling('mogg bombers', '2011-01-25', 'If multiple creatures enter the battlefield at the same time, the ability triggers once for each of them. You may choose a different target player for each time it triggers.').
card_ruling('mogg bombers', '2011-01-25', 'The damage is dealt whether or not you sacrifice the Mogg Bombers. For instance, if Mogg Bombers is destroyed or sacrificed in between the time when the ability triggers and when it resolves, then the targeted player will still be dealt 3 damage.').

card_ruling('mogg conscripts', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('mogg flunkies', '2012-07-01', 'Mogg Flunkies can be declared as an attacker only if another creature is declared as an attacker at the same time. Similarly, Mogg Flunkies can be declared as a blocker only if another creature is declared as a blocker at the same time.').
card_ruling('mogg flunkies', '2012-07-01', 'Two or more Mogg Flunkies can attack or block together.').
card_ruling('mogg flunkies', '2012-07-01', 'Although Mogg Flunkies can\'t attack alone, the other attacking creature(s) doesn\'t have to attack the same player or planeswalker. For example, Mogg Flunkies could attack an opponent and another creature could attack a planeswalker that opponent controls.').
card_ruling('mogg flunkies', '2012-07-01', 'In a Two-Headed Giant game (or in another format using the shared team turns option), Mogg Flunkies can attack or block with a creature controlled by your teammate, even if no other creatures you control are attacking or blocking.').

card_ruling('mogg maniac', '2004-10-04', 'If it blocks a trample creature, it only gets dealt damage equal to what is assigned to it. This means it will likely only get to deal 1 damage to the opponent.').

card_ruling('moggcatcher', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('mogis\'s marauder', '2013-09-15', 'Unlike most abilities that use devotion to determine the magnitude of an effect, Mogis’s Marauder uses devotion to determine the number of targets its ability has. Use your devotion to black as you put the ability on the stack. Mogis’s Marauder’s mana cost will always count toward your devotion to black. Once targets are chosen, it doesn’t matter if your devotion to black changes while the ability is on the stack. The number of targets is locked in.').
card_ruling('mogis\'s marauder', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('mogis\'s marauder', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('mogis\'s marauder', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').

card_ruling('mogis\'s warhound', '2014-04-26', 'The controller of Mogis’s Warhound or the creature it’s enchanting chooses which player or planeswalker to attack.').
card_ruling('mogis\'s warhound', '2014-04-26', 'If, during your declare attackers step, Mogis’s Warhound (or the creature it enchants) is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under its controller’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, its controller isn’t forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('mogis\'s warhound', '2014-04-26', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('mogis\'s warhound', '2014-04-26', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('mogis\'s warhound', '2014-04-26', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('mogis\'s warhound', '2014-04-26', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('mogis\'s warhound', '2014-04-26', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('mogis\'s warhound', '2014-04-26', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('mogis\'s warhound', '2014-04-26', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('mogis\'s warhound', '2014-04-26', 'You still control the Aura, even if it’s enchanting a creature controlled by another player.').
card_ruling('mogis\'s warhound', '2014-04-26', 'If the enchanted creature leaves the battlefield, the Aura stops being an Aura and remains on the battlefield. Control of that permanent doesn’t change; you’ll control the resulting enchantment creature.').
card_ruling('mogis\'s warhound', '2014-04-26', 'Similarly, if you cast an Aura spell with bestow targeting a creature controlled by another player, and that creature is an illegal target when the spell tries to resolve, it will finish resolving as an enchantment creature spell. It will enter the battlefield under your control.').

card_ruling('mogis, god of slaughter', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('mogis, god of slaughter', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('mogis, god of slaughter', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('mogis, god of slaughter', '2014-02-01', 'If the player can’t sacrifice a creature (usually because he or she doesn’t control one), Mogis will deal 2 damage to him or her.').

card_ruling('mold adder', '2009-10-01', 'If an opponent casts a blue or black spell, Mold Adder\'s ability triggers and is put on the stack on top of that spell. Mold Adder\'s ability will resolve before the spell does.').
card_ruling('mold adder', '2009-10-01', 'If an opponent casts a spell that\'s both blue and black, Mold Adder\'s ability triggers only once.').

card_ruling('mold demon', '2004-10-04', 'The sacrifice is done as a triggered ability just after it enters the battlefield.').

card_ruling('molder beast', '2011-01-01', 'If Molder Beast and an artifact creature are both involved in combat during the same combat damage step, and that artifact creature is destroyed due to lethal combat damage, Molder Beast\'s triggered ability will trigger -- but it will get the bonus too late to affect that combat.').

card_ruling('moldervine cloak', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('moldervine cloak', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('moldervine cloak', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('moldgraf monstrosity', '2011-09-22', 'If Moldgraf Monstrosity\'s ability can\'t exile it (perhaps because it\'s not still in the graveyard when the ability resolves), the two creature cards are still returned to the battlefield.').
card_ruling('moldgraf monstrosity', '2011-09-22', 'If two Moldgraf Monstrosities die simultaneously, the first ability to resolve could return the other Moldgraf Monstrosity to the battlefield. If it does, the second Moldgraf Monstrosity\'s ability won\'t exile it but it will return two more creature cards to the battlefield.').

card_ruling('molten birth', '2013-07-01', 'If you win the flip, Molten Birth goes directly from the stack to its owner’s hand. It never goes to a graveyard.').

card_ruling('molten disaster', '2007-05-01', 'If Molten Disaster was kicked, Molten Disaster has split second as long as it\'s on the stack.').
card_ruling('molten disaster', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('molten disaster', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('molten disaster', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('molten disaster', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('molten disaster', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('molten disaster', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('molten firebird', '2007-02-01', 'The player who controlled Molten Firebird when it was put into a graveyard will skip his or her next draw step even if Molten Firebird is returned to the battlefield under a different player\'s control (because it\'s owned by someone else) or isn\'t returned to the battlefield at all (because it left the graveyard before the end of the turn, for example).').
card_ruling('molten firebird', '2007-02-01', 'The second ability can be activated only if Molten Firebird is on the battlefield.').
card_ruling('molten firebird', '2007-02-01', 'This is the timeshifted version of Ivory Gargoyle.').

card_ruling('molten frame', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('molten frame', '2009-02-01', 'Any permanent that\'s both an artifact and a creature is a legal target for Molten Frame.').

card_ruling('molten nursery', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('molten nursery', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('molten nursery', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('molten nursery', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('molten nursery', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('molten primordial', '2013-01-24', 'You can choose a number of targets up to the number of opponents you have, one target per opponent.').
card_ruling('molten primordial', '2013-01-24', 'Molten Primordial\'s triggered ability can target a creature that\'s already untapped.').

card_ruling('molten psyche', '2011-01-01', 'If a player doesn\'t have any cards in his or her hand, the player will still shuffle his or her library.').
card_ruling('molten psyche', '2011-01-01', 'The metalcraft effect counts all cards each opponent drew for any reason during that turn, not just the cards those opponents drew due to Molten Psyche\'s first effect.').

card_ruling('molten sentry', '2005-10-01', 'If a creature enters the battlefield copying Molten Sentry, that creature\'s controller flips a coin to see what the copy will be.').
card_ruling('molten sentry', '2005-10-01', 'If a creature that\'s already on the battlefield copies Molten Sentry, it becomes a copy of whatever Molten Sentry already is (either a 5/2 creature with haste or a 2/5 creature with defender).').

card_ruling('moltensteel dragon', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('moltensteel dragon', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('moltensteel dragon', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('moltensteel dragon', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('moltensteel dragon', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('molting snakeskin', '2014-09-20', 'The regeneration ability is granted to the enchanted creature. Only the creature’s controller can activate the regeneration ability.').

card_ruling('moment of heroism', '2011-09-22', 'Multiple instances of lifelink on the same creature are redundant.').

card_ruling('moment of silence', '2004-10-04', 'The player skips their next combat phase this turn (if any). If they manage to have two combat phases, then only their next one combat phase is skipped.').
card_ruling('moment of silence', '2004-10-04', 'It must be used before the combat phase starts or it has no effect.').
card_ruling('moment of silence', '2004-10-04', 'If cast on a player when it is not their turn, it has no effect.').

card_ruling('momentary blink', '2006-09-25', 'When Momentary Blink resolves, the creature is exiled, then immediately returned to the battlefield. The game sees the returning card as a different permanent from the one that left the battlefield. Any counters, Auras, and so on are removed. Any spells or abilities targeting the creature no longer target it.').
card_ruling('momentary blink', '2006-09-25', 'Any \"as this enters the battlefield\" choices for the affected creature are made by its owner, not its old controller.').
card_ruling('momentary blink', '2007-02-01', 'If Momentary Blink is cast on a token creature, the token will not return to the battlefield, because of the rule that says a token that leaves the battlefield can\'t come back.').

card_ruling('momentous fall', '2010-06-15', 'The sacrificed creature\'s last known existence on the battlefield is checked to determine its power and its toughness.').
card_ruling('momentous fall', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('momentous fall', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('momentum', '2004-10-04', 'Putting on a counter is optional. If you forget, you can\'t go back later even if it is something you usually do.').

card_ruling('momir vig, simic visionary', '2006-05-01', 'If you cast a green and blue creature spell, both abilities will trigger. The abilities may be stacked so that when they resolve, you search your library for a creature card, put it on top of your library, and then put that card into your hand. The abilities may also be stacked in the other order, of course.').

card_ruling('monastery flock', '2014-09-20', 'If an attacking face-down Monastery Flock is turned face up, it will continue to be attacking even though it will have defender. If it’s turned face up before blockers are declared, then creatures without flying or reach won’t be able to block it.').
card_ruling('monastery flock', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('monastery flock', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('monastery flock', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('monastery flock', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('monastery flock', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('monastery flock', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('monastery flock', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('monastery flock', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('monastery flock', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('monastery loremaster', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('monastery loremaster', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('monastery loremaster', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('monastery mentor', '2014-11-24', 'Casting a noncreature spell will cause both prowess and Monastery Mentor’s other ability to trigger. You can put these abilities on the stack in either order. Whichever ability is put on the stack last will resolve first.').
card_ruling('monastery mentor', '2014-11-24', 'The spell that causes Monastery Mentor’s second ability to trigger will not cause the prowess ability of the Monk token that’s created to trigger.').
card_ruling('monastery mentor', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('monastery mentor', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('monastery mentor', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('monastery mentor', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('monastery siege', '2014-11-24', 'The “Khans” ability happens after you draw the card you normally draw during your draw step. That card and the additional card will be in your hand when you have to discard a card.').
card_ruling('monastery siege', '2014-11-24', 'The “Dragons” ability adds {2} per spell, not per target. It won’t cause a spell to cost more than {2} more to cast, even if that spell targets you and a permanent you control or more than one permanent you control.').
card_ruling('monastery siege', '2014-11-24', 'Each Siege will have one of the two listed abilities, depending on your choice as it enters the battlefield.').
card_ruling('monastery siege', '2014-11-24', 'The words “Khans” and “Dragons” are anchor words, connecting your choice to the appropriate ability. Anchor words are a new rules concept. “[Anchor word] — [Ability]” means “As long as you chose [anchor word] as this permanent entered the battlefield, this permanent has [ability].” Notably, the anchor word “Dragons” has no connection to the creature type Dragon.').
card_ruling('monastery siege', '2014-11-24', 'Each of the last two abilities is linked to the first ability. They each refer only to the choice made as a result of the first ability. If a permanent enters the battlefield as a copy of one of the Sieges, its controller will make a new choice for that Siege. Which ability the copy has won’t depend on the choice made for the original permanent.').

card_ruling('monastery swiftspear', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('monastery swiftspear', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('monastery swiftspear', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('mondronen shaman', '2011-01-22', 'Tovolar\'s Magehunter\'s first triggered ability will resolve before the spell that caused it to trigger. If this causes the player who cast that spell to lose the game, that spell won\'t resolve.').

card_ruling('mongrel pack', '2004-10-04', 'The Hound tokens are put onto the battlefield if it dies during the combat phase, even if it was not an attacker or blocker.').

card_ruling('monk idealist', '2004-10-04', 'You can cast this if there are no enchantments in your graveyard. The enters the battlefield ability just doesn\'t do anything.').

card_ruling('monomania', '2011-09-22', 'If there are one or zero cards in the player\'s hand, he or she will discard no cards.').

card_ruling('monstrify', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('monstrify', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('monstrify', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('monstrify', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').

card_ruling('monstrous carabid', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('moonbow illusionist', '2005-06-01', 'Only the targeted land is affected.').
card_ruling('moonbow illusionist', '2005-06-01', 'The land\'s name as well as any supertype it might have (such as legendary) remains unchanged.').

card_ruling('moonhold', '2008-08-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('moonhold', '2008-08-01', 'When you cast the spell, you choose its targets before you pay for it.').
card_ruling('moonhold', '2008-08-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('moonhold', '2008-08-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('moonhold', '2008-08-01', 'Moonhold is not a counterspell. It will have no effect on creature spells the targeted player cast before Moonhold resolved.').
card_ruling('moonhold', '2008-08-01', 'Moonhold doesn\'t stop effects that \"put a card onto the battlefield.\"').

card_ruling('moonlace', '2006-09-25', 'Moonlace\'s effect doesn\'t wear off.').
card_ruling('moonlace', '2006-09-25', 'If Moonlace is cast on a spell that becomes a permanent (a creature spell, an artifact spell, or an enchantment spell), the permanent will enter the battlefield colorless and will remain colorless until it leaves the battlefield (or until another effect changes its color).').

card_ruling('moonmist', '2011-09-22', 'Moonmist causes any double-faced Human to transform, not just Werewolves.').
card_ruling('moonmist', '2011-09-22', 'Whether or not a creature is a Werewolf or a Wolf is checked only as combat damage is dealt. If the creature isn\'t a Werewolf or a Wolf at that time, its combat damage will be prevented.').
card_ruling('moonmist', '2011-09-22', 'Moonmist will prevent combat damage dealt by a creature that isn\'t a Werewolf or a Wolf even if that creature wasn\'t on the battlefield (or was a Werewolf or a Wolf) when Moonmist resolved.').

card_ruling('moonring mirror', '2004-12-01', 'Note that Moonring Mirror\'s first ability is not a replacement effect.').
card_ruling('moonring mirror', '2004-12-01', 'If you choose to use Moonring Mirror\'s second ability, you return all the cards you own exiled by both of its abilities, but not any of the cards you just exiled from your hand.').

card_ruling('morality shift', '2004-10-04', 'This spell does work if either your graveyard or library is empty.').
card_ruling('morality shift', '2004-10-04', 'You can choose the order of the cards being placed into the graveyard.').

card_ruling('morbid bloom', '2009-05-01', 'If the targeted card is removed from the graveyard before Morbid Bloom resolves, the spell is countered. You won\'t get any Saproling tokens.').
card_ruling('morbid bloom', '2009-05-01', 'If a creature card has \"*\" in its toughness, the ability that defines \"*\" works in all zones. The value of X is equal to the toughness of the targeted creature card as it last existed in the graveyard.').

card_ruling('morgue burst', '2013-04-15', 'Use the power of the card as it last existed in your graveyard to determine how much damage will be dealt.').
card_ruling('morgue burst', '2013-04-15', 'If the creature card is an illegal target when Morgue Burst tried to resolve (most likely because it was exiled in response), it won\'t be returned to your hand and no damage will be dealt to the target creature or player.').

card_ruling('morinfen', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('morkrut banshee', '2011-09-22', 'The morbid ability is mandatory. If there are no other creatures on the battlefield when the ability triggers (and a creature died this turn), the ability must target Morkrut Banshee itself.').

card_ruling('morphic tide', '2012-06-01', 'This ability is very similar to that of Warp World, but note that planeswalkers are put onto the battlefield.').
card_ruling('morphic tide', '2012-06-01', 'Taking it slowly, here\'s what happens when Morphic Tide\'s ability resolves: 1) Each player counts the number of permanents he or she owns. 2) Each player shuffles those permanents into his or her library. 3) Each player reveals cards from the top of his or her library equal to the number that player counted. 4) Each player puts all artifact, creature, land, and planeswalker cards revealed this way onto the battlefield. All of these cards enter the battlefield at the same time. 5) Each player puts all enchantment cards revealed this way onto the battlefield. An Aura put onto the battlefield this way can enchant an artifact, creature, land, or planeswalker that was already put onto the battlefield, but can\'t enchant an enchantment that\'s being put onto the battlefield at the same time as it. If multiple players have Auras to put onto the battlefield, the player whose turn it is announces what his or her Auras will enchant, then each other player in turn order does the same, then all enchantments (both Auras and non-Auras) enter the battlefield at the same time. 6) Each player puts all of his or her other revealed cards (instants, sorceries, and Auras that can\'t enchant anything) on the bottom of his or her library in any order.').
card_ruling('morphic tide', '2012-06-01', 'Cards such as Sakashima\'s Student that can enter the battlefield as a copy of another permanent won\'t be able to enter the battlefield as a copy of a permanent that\'s entering the battlefield at the same time.').
card_ruling('morphic tide', '2012-06-01', 'After all permanents are put onto the battlefield and you planeswalk away from Morphic Tide, any abilities that triggered from those permanents entering the battlefield, from the previous generation of permanents leaving the battlefield, and from the new plane or phenomenon will be put onto the stack. You\'ll put all of your triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').
card_ruling('morphic tide', '2012-06-01', 'The permanents will enter the battlefield while Morphic Tide is face up. This means the game won\'t be on any plane when determining if any abilities trigger because those permanents entered the battlefield, for example. However, you\'ll planeswalk away from Morphic Tide before any such abilities are put on the stack. After you turn the next card of the planar deck face up, any abilities that triggered during the resolution of Morphic Tide (including any abilities that triggered because you encountered another phenomenon or planeswalked to the next plane) will be put on the stack. The planar controller will put his or her abilities on the stack in any order, followed by each other player in turn order. The last ability put on the stack will be the first one to resolve.').
card_ruling('morphic tide', '2012-06-01', 'Token permanents a player owns will count toward the number of cards that player reveals. They\'ll be shuffled into that player\'s library and subsequently cease to exist. A token\'s owner is the player under whose control it first entered the battlefield.').

card_ruling('mortal combat', '2007-07-15', 'If you don\'t have twenty or more creature cards in your graveyard by the time your upkeep starts, the ability won\'t trigger that turn.').
card_ruling('mortal combat', '2007-07-15', 'Mortal Combat\'s ability has an \"intervening \'if\' clause,\" so the condition is checked again when the triggered ability resolves. If you don\'t still have twenty creature cards in your graveyard, the ability does nothing.').

card_ruling('mortal flesh is weak', '2010-06-15', 'For a player\'s life total to become a certain number that\'s lower than his or her current life total, what actually happens is that the player loses the appropriate amount of life. For example, if the lowest life total among your opponents is 5 and another opponent has 12 life, this scheme\'s ability will cause that player to lose 7 life. Abilities that interact with life loss will interact with this effect accordingly.').

card_ruling('mortal obstinacy', '2014-04-26', 'You decide whether to sacrifice Mortal Obstinacy as its triggered ability resolves. If it’s not on the battlefield at that time, you can’t sacrifice it and destroy the target enchantment.').
card_ruling('mortal obstinacy', '2014-04-26', 'If another player gains control of the enchanted creature, Mortal Obstinacy will be put into its owner’s graveyard.').

card_ruling('mortarpod', '2011-06-01', 'If you sacrifice the equipped creature to deal 1 damage to a target creature or player, the damage is dealt by the creature as it last existed on the battlefield.').

card_ruling('mortician beetle', '2010-06-15', 'Mortician Beetle\'s ability triggers whenever any player, including you, sacrifices a creature because some other spell, ability, or cost instructed the player to do so. Mortician Beetle itself doesn\'t allow you to sacrifice any creatures.').

card_ruling('mortify', '2006-02-01', 'This spell isn\'t Modal. When it resolves, it will destroy the target if it\'s a creature or an enchantment, even if it changed from one to the other between targeting and resolution.').

card_ruling('mortis dogs', '2011-06-01', 'The loss of life is equal to Mortis Dogs\'s power as it last existed on the battlefield.').

card_ruling('mortuary', '2004-10-04', 'This is not an optional ability.').

card_ruling('mortus strider', '2013-01-24', 'Mortis Strider will return to its owner\'s hand only if it is still in the graveyard when its ability resolves.').

card_ruling('mossbridge troll', '2008-05-01', 'The first ability essentially means that Mossbridge Troll always has a regeneration shield. It retains that shield even if it\'s used.').
card_ruling('mossbridge troll', '2008-05-01', 'The total power of the creatures you tap is checked only when you pay the cost. It doesn\'t matter if their power decreases, or if any of them have left the battlefield, by the time the ability resolves. Note that you may tap a Mossbridge Troll to help pay the cost of a different Mossbridge Troll\'s ability.').

card_ruling('mossfire egg', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('mosstodon', '2008-10-01', 'The ability checks the targeted creature\'s power twice: when the creature becomes the target, and when the ability resolves. Once the ability resolves, it will continue to apply to the affected creature no matter what its power may become later in the turn.').

card_ruling('mothdust changeling', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap a creature (including Mothdust Changeling itself) that hasn\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('mount keralia', '2012-06-01', 'If you roll {C}, Mount Keralia\'s damage-prevention effect will last for the rest of the game. It will apply to damage dealt by any plane named Mount Keralia, regardless of who owns that plane.').

card_ruling('mountain valley', '2004-10-04', 'You do not have to find a mountain or forest card if you do not want to.').

card_ruling('mounted archers', '2004-10-04', 'You can use the ability multiple times so it can block more than two creatures.').
card_ruling('mounted archers', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').

card_ruling('mourning', '2004-10-04', 'If attached to an opponent\'s creature, only you can activate the ability to return it.').

card_ruling('mournwhelk', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('mox diamond', '2008-05-01', 'If you don\'t discard a land card, Mox Diamond never enters the battlefield. It won\'t trigger abilities that look for something entering the battlefield, and you won\'t get the opportunity to tap it for mana.').

card_ruling('mtenda griffin', '2004-10-04', 'Must have a Griffin in the graveyard to use this ability because it requires a target.').

card_ruling('muck drubb', '2007-02-01', 'The spell\'s target is changed to Muck Drubb only if Muck Drubb is a legal target for that spell.').
card_ruling('muck drubb', '2007-02-01', 'If a spell targets multiple things, you can\'t target it with Muck Drubb\'s ability, even if only one of those targets is a creature or all but one of those targets has become illegal.').
card_ruling('muck drubb', '2007-02-01', 'If a spell has multiple targets, but it\'s targeting the same thing with all of them (such as Seeds of Strength targeting the same creature three times), you can target that spell with Muck Drubb\'s ability. In that case, you change all of those targets to Muck Drubb.').

card_ruling('mudbutton clanger', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('mudbutton clanger', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('mudbutton clanger', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('mudbutton clanger', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('mudbutton clanger', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('mudbutton clanger', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('mudslide', '2008-10-01', 'First the player chooses all the tapped creatures he or she would like to untap this way. Then the entire payment is made at once, then all of those creatures untap at the same time. If the entire payment can\'t be paid (because the player chose too many creatures, for example), none of it is paid and none of those creatures untap.').

card_ruling('mul daya channelers', '2010-06-15', 'If the top card of your library is both a creature card and a land card (as Dryad Arbor is), Mul Daya Channelers gets both bonuses.').
card_ruling('mul daya channelers', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('mul daya channelers', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('mul daya channelers', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('multani\'s presence', '2004-10-04', 'Will trigger when a spell is countered due to all of its targets being illegal.').

card_ruling('munda, ambush leader', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('munda, ambush leader', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('muraganda petroglyphs', '2007-05-01', 'Muraganda Petroglyphs gives a bonus only to creatures that have no rules text at all. This includes true vanilla creatures (such as Grizzly Bears), face-down creatures, many tokens, and creatures that have lost their abilities (due to Ovinize, for example). Any ability of any kind, whether or not the ability functions in the on the battlefield zone, including things like \"Cycling {2}\" means the creature doesn\'t get the bonus.').
card_ruling('muraganda petroglyphs', '2007-05-01', 'Animated basic lands have mana abilities, so they won\'t get the bonus.').
card_ruling('muraganda petroglyphs', '2007-05-01', 'Some Auras and Equipment grant abilities to creatures, meaning the affected creature would no longer get the +2/+2 bonus. For example, Flight grants flying to the enchanted creature. Other Auras and Equipment do not, meaning the affected creature would continue to get the +2/+2 bonus. For example, Dehydration states something now true about the enchanted creature, but doesn\'t give it any abilities. Auras and Equipment that grant abilities will use the words \"gains\" or \"has,\" and they\'ll list a keyword ability or an ability in quotation marks.').
card_ruling('muraganda petroglyphs', '2013-07-01', 'Cipher grants an ability to creatures, meaning the affected creatures would no longer get the +2/+2 bonus.').

card_ruling('murasa', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('murasa', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('murasa', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('murasa', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('murasa', '2009-10-01', 'The effect of the {C} ability has no duration. The affected land will remain a creature until the end of the game, it leaves the battlefield, or some other effect changes its card types, whichever comes first. It doesn\'t matter whether Murasa remains the face-up plane card.').

card_ruling('murasa pyromancer', '2009-10-01', 'The ability counts the number of Allies you control as it resolves.').
card_ruling('murasa pyromancer', '2009-10-01', 'You target a creature when the ability triggers. You decide whether to have Murasa Pyromancer deal damage to that creature as the ability resolves.').

card_ruling('murasa ranger', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('murasa ranger', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('murasa ranger', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('murder investigation', '2013-01-24', 'To determine how many Soldier tokens are created, use the power of the enchanted creature as it last existed on the battlefield.').
card_ruling('murder investigation', '2013-01-24', 'If another player gains control of either Murder Investigation or the enchanted creature (but not both), Murder Investigation will be enchanting an illegal permanent. The Aura will be put into its owner\'s graveyard as a state-based action.').

card_ruling('murder of crows', '2011-09-22', 'If you choose to draw a card, and that draw is replaced by another action, you\'ll still discard a card.').
card_ruling('murder of crows', '2011-09-22', 'You can\'t do anything in between drawing a card and discarding a card, including casting the card you drew.').

card_ruling('murderous cut', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('murderous cut', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('murderous cut', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('murderous cut', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('murderous cut', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('murderous redcap', '2008-05-01', 'Murderous Redcap\'s power is checked at the time the ability resolves. If it\'s left the battlefield by then, its last known information is used.').
card_ruling('murderous redcap', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('murderous redcap', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('murderous redcap', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('murderous redcap', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('murderous redcap', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('murderous redcap', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('murderous redcap', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('murk dwellers', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('murk strider', '2015-08-25', 'If the target creature becomes illegal before the triggered ability resolves, it will be countered and none of its effects will happen. You won’t “process” a card in exile.').
card_ruling('murk strider', '2015-08-25', 'Players can respond to the triggered ability, but once it starts resolving and you decide whether to put a card from exile into the player’s graveyard, it’s too late for anyone to respond.').
card_ruling('murk strider', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('murk strider', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('murk strider', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('murk strider', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('murk strider', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('murk strider', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('murk strider', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('murk strider', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('murk strider', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('murkfiend liege', '2008-08-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').
card_ruling('murkfiend liege', '2008-08-01', 'All your green and/or blue creatures untap during each other player\'s untap step. You have no choice about what untaps. Those creatures untap at the same time as the active player\'s permanents.').
card_ruling('murkfiend liege', '2008-08-01', 'During each other player\'s untap step, effects that would otherwise cause your green and/or blue creatures to stay tapped don\'t apply because they only apply during *your* untap step. For example, if you control Nettle Sentinel (a green creature that says \"Nettle Sentinel doesn\'t untap during your untap step\"), you untap it during each other player\'s untap step.').
card_ruling('murkfiend liege', '2008-08-01', 'Multiple Murkfiend Lieges are redundant when it comes to the untap effect. You can\'t untap your permanents more than once in a single untap step.').

card_ruling('murmuring bosk', '2008-04-01', 'Murmuring Bosk is a Forest, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do. For example, you can\'t find Murmuring Bosk with Fertilid\'s ability (\"searches his or her library for a basic land card\"), but you can find Murmuring Bosk with Everbark Shaman\'s ability (\"search your library for two Forest cards\").').

card_ruling('muscle sliver', '2004-10-04', 'Yes, it does give the +1/+1 bonus to itself.').
card_ruling('muscle sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('muscle sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('muse vessel', '2006-05-01', 'Only cards exiled as a result of the first ability can be played as a result of the second ability.').

card_ruling('musician', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').
card_ruling('musician', '2008-10-01', 'This ability has no duration. An affected creature will retain the triggered ability until the game ends, it leaves the battlefield, or some other effect causes it to lose its abilities. It doesn\'t matter if Musician is still on the battlefield or not.').
card_ruling('musician', '2008-10-01', 'Although a creature can gain the triggered ability only once, it can be targeted by multiple Musicians to increase its number of music counters.').
card_ruling('musician', '2008-10-01', 'If a music counter is put on a creature by some other means (Fate Transfer, for example), but that creature has never been affected by Musician\'s ability, that creature won\'t have the triggered ability and its controller won\'t have to pay mana. The music counters will stay on that creature but won\'t do anything.').

card_ruling('mutagenic growth', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('mutagenic growth', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('mutagenic growth', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('mutagenic growth', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('mutagenic growth', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('mutant\'s prey', '2013-04-15', 'Only a creature with a +1/+1 counter on it can be chosen as the first target of Mutant\'s Prey.').
card_ruling('mutant\'s prey', '2013-04-15', 'If one of the targets is illegal when Mutant\'s Prey tries to resolve (for example, if the first target creature no longer has a +1/+1 counter on it), neither creature will deal or be dealt damage.').

card_ruling('mutavault', '2008-04-01', 'Although Mutavault has all creature types while it\'s animated, it does not have the changeling keyword ability.').
card_ruling('mutavault', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('mutavault', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('mutilate', '2012-07-01', 'The effect is based on the number of Swamps you control when Mutilate resolves. Only creatures on the battlefield at that time will be affected. The effect won\'t change later in the turn, even if the number of Swamps you control changes.').

card_ruling('muzzio\'s preparations', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('muzzio\'s preparations', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('muzzio\'s preparations', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('muzzio\'s preparations', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('muzzio\'s preparations', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('muzzio\'s preparations', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('muzzio\'s preparations', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('muzzio, visionary architect', '2014-05-29', 'The value of X is determined as the ability resolves. If, at that time, you control no artifacts, you won’t look at any cards.').

card_ruling('mwonvuli beast tracker', '2012-07-01', 'The creature card must have one of the listed abilities, not an ability that grants one of the listed abilities to the creature. For example, you couldn\'t put Prized Elephant, which has \"{G}: Prized Elephant gains trample until end of turn,\" on top of your library this way.').

card_ruling('mwonvuli ooze', '2004-10-04', 'Its power/toughness changes when the cumulative upkeep resolves, not when it triggers.').
card_ruling('mwonvuli ooze', '2008-04-01', 'If you choose not to pay Mwonvuli Ooze\'s cumulative upkeep, it will still get an age counter before being sacrificed. Its power and toughness will momentarily increase before being put into the graveyard, which might matter to certain other cards (such as Proper Burial).').

card_ruling('my crushing masterstroke', '2010-06-15', 'Each of those permanents attacks its owner if able, not necessarily the player you gained control of it from.').
card_ruling('my crushing masterstroke', '2010-06-15', 'Any of those permanents that are creatures at the time your declare attackers step begins must attack its owner if able. This includes permanents that became creatures after you gained control of them, such as an animated Chimeric Staff. Any of those permanents that aren\'t creatures at that time can\'t attack.').
card_ruling('my crushing masterstroke', '2010-06-15', 'If, during your declare attackers step, one of the creatures you gained control of this way is tapped or is affected by a spell or ability that says it can\'t attack, then it doesn\'t attack. If there\'s a cost associated with having that creature attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('my crushing masterstroke', '2010-06-15', 'If one of those creatures can\'t attack its owner that turn due to a spell or ability (such as Chronomantic Escape), you may have it attack another player, attack a planeswalker an opponent controls, or not attack at all. If there\'s a cost with having that creature attack its owner, you aren\'t forced to pay that cost, so you may have it attack another player, attack a planeswalker an opponent controls, or not attack at all.').
card_ruling('my crushing masterstroke', '2010-06-15', 'If there are multiple combat phases that turn, each of those permanents must attack its owner only in the first one in which it\'s able to.').

card_ruling('my genius knows no bounds', '2010-06-15', 'As the ability resolves, you choose a value for X and decide whether to pay {X}. If you do decide to pay {X}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('my undead horde awakens', '2010-06-15', 'You put a creature card on the battlefield this way during the end step of each of your turns. As soon as any one of those creatures is put back into a graveyard for any reason, this scheme is abandoned.').
card_ruling('my undead horde awakens', '2010-06-15', 'When this scheme is abandoned, nothing happens to the rest of the creatures that were put onto the battlefield with it. They simply remain on the battlefield.').

card_ruling('my wish is your command', '2010-06-15', 'You choose a total of one card, and that card can\'t be a creature or a land.').
card_ruling('my wish is your command', '2010-06-15', 'If you choose a card, you cast it as part of the resolution of this ability. Timing restrictions based on the card\'s type are ignored. Other restrictions are not (such as \"Cast [this card] only during your end step\").').
card_ruling('my wish is your command', '2010-06-15', 'If you choose a card you can\'t cast (because there are no legal targets for the spell, for example), nothing happens to it. It remains in its owner\'s hand.').
card_ruling('my wish is your command', '2010-06-15', 'If you cast a card \"without paying its mana cost,\" the value of any X in the card\'s mana cost must be 0. You can\'t pay any alternative costs for that card. On the other hand, if the card has optional additional costs (such as kicker or replicate), you may pay those when you cast the card. If the card has mandatory additional costs (such as Fling does), you must pay those when you cast the card.').

card_ruling('mycoid shepherd', '2009-05-01', 'Mycoid Shepherd\'s ability will trigger when it\'s put into a graveyard from the battlefield no matter what its power is at that time.').
card_ruling('mycoid shepherd', '2009-05-01', 'If another creature you control is put into a graveyard from the battlefield, Mycoid Shepherd\'s ability checks that creature\'s power as it last existed on the battlefield. If it was 5 or greater, the ability will trigger.').
card_ruling('mycoid shepherd', '2009-05-01', 'If Mycoid Shepherd and another creature you control with power 5 or greater are put into a graveyard from the battlefield at the same time, Mycoid Shepherd\'s ability will trigger twice.').

card_ruling('mycologist', '2007-02-01', 'This is the timeshifted version of Elvish Farmer.').

card_ruling('mycoloth', '2008-10-01', 'You may choose not to sacrifice any creatures for the Devour ability.').
card_ruling('mycoloth', '2008-10-01', 'If you cast this as a spell, you choose how many and which creatures to devour as part of the resolution of that spell. (It can\'t be countered at this point.) The same is true of a spell or ability that lets you put a creature with devour onto the battlefield.').
card_ruling('mycoloth', '2008-10-01', 'You may sacrifice only creatures that are already on the battlefield. If a creature with devour and another creature are entering the battlefield under your control at the same time, the creature with devour can\'t devour that other creature. The creature with devour also can\'t devour itself.').
card_ruling('mycoloth', '2008-10-01', 'If multiple creatures with devour are entering the battlefield under your control at the same time, you may use each one\'s devour ability. A creature you already control can be devoured by only one of them, however. (In other words, you can\'t sacrifice the same creature to satisfy multiple devour abilities.) All creatures devoured this way are sacrificed at the same time.').
card_ruling('mycoloth', '2008-10-01', 'The number of Saproling tokens created by the triggered ability is based on the number of +1/+1 counters on Mycoloth, not on the number of creatures Mycoloth devoured. It doesn\'t matter where the +1/+1 counters came from.').

card_ruling('mycosynth golem', '2004-12-01', 'The spells gain affinity for artifacts as they\'re put onto the stack. They don\'t have the ability while they\'re cards in your hand.').
card_ruling('mycosynth golem', '2004-12-01', 'If Mycosynth Golem is sacrificed as part of the cost to cast a spell, the cost reduction will already be locked in, and the cost won\'t be increased.').
card_ruling('mycosynth golem', '2004-12-01', 'Two or more instances of the affinity ability are cumulative, even if they\'re both affinity for the same thing.').

card_ruling('mycosynth lattice', '2004-12-01', 'Mycosynth Lattice turns all permanents on the battlefield into artifacts. Spells on the stack and cards in other zones aren\'t permanents, so those spells and cards don\'t become artifacts.').
card_ruling('mycosynth lattice', '2004-12-01', 'Mycosynth Lattice\'s second ability makes everything, in every zone of the game, colorless.').
card_ruling('mycosynth lattice', '2004-12-01', 'The Lattice\'s third ability lets players spend even colorless mana as though it had a color. However, it doesn\'t remove restrictions on the mana. For example, Mycosynth Lattice doesn\'t allow mana from Vedalken Engineer to be used to cast a nonartifact spell.').
card_ruling('mycosynth lattice', '2009-10-01', 'The Lattice\'s first ability causes Auras to become artifacts. Combined with March of the Machines from the Mirrodin set, this can then make those Auras become creatures. An Aura that\'s also a creature can\'t enchant anything. It\'s unattached the next time state-based actions are checked, and then immediately put into its owner\'s graveyard as a second state-based action.').

card_ruling('mycosynth wellspring', '2011-06-01', 'The ability will trigger when Mycosynth Wellspring is put into a graveyard from the battlefield, even if the ability that triggered when it entered the battlefield hasn\'t resolved yet.').

card_ruling('myojin of cleansing fire', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('myojin of infinite rage', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('myojin of life\'s web', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('myojin of night\'s reach', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('myojin of seeing winds', '2013-07-01', 'In a Commander game where this card is your commander, casting it from the Command zone does not count as casting it from your hand.').

card_ruling('myr battlesphere', '2011-01-01', 'You choose the value for X as the last ability resolves. You can\'t choose a value for X that\'s greater than the number of untapped Myr you control.').
card_ruling('myr battlesphere', '2011-01-01', 'You can tap any untapped Myr you control as the last ability resolves, not just the Myr tokens you put onto the battlefield with the first ability. This includes Myr that haven\'t been under your control since your most recent turn began.').
card_ruling('myr battlesphere', '2011-01-01', 'The defending player referred to by the last ability is the player attacked by Myr Battlesphere or the controller of the planeswalker attacked by Myr Battlesphere. For the purposes of this ability, that player remains the defending player even if Myr Battlesphere or that planeswalker is removed from combat before the ability resolves.').
card_ruling('myr battlesphere', '2011-01-01', 'As the last ability resolves, you can tap untapped Myr you control even if Myr Battlesphere is no longer on the battlefield by then. If that has happened, Myr Battlesphere won\'t be able to get the +X/+0 bonus, but it will still deal X damage to the defending player.').

card_ruling('myr landshaper', '2004-12-01', 'The land keeps all its other types, subtypes, and supertypes: If it was a basic land, it\'s still basic. If it was a Forest, it\'s still a Forest.').

card_ruling('myr moonvessel', '2011-01-25', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('myr propagator', '2011-01-01', 'Normally, when a token is created by this ability, it will simply be a Myr Propagator, so it\'ll also have the token-creating ability. (See below for weird exceptions.)').
card_ruling('myr propagator', '2011-01-01', 'Here\'s the detailed version of what happens. As the token is created, it checks the printed values of the Myr Propagator it\'s copying -- or, if the Myr Propagator whose ability was activated was itself a token, the original characteristics of that token as stated by the effect that put it onto the battlefield -- as well as any copy effects that have been applied to it. It won\'t copy counters on the Myr Propagator, nor will it copy other effects that have changed Myr Propagator\'s power, toughness, types, color, or so on.').
card_ruling('myr propagator', '2011-01-01', 'If Myr Propagator has left the battlefield by the time its ability resolves, you\'ll still put a token onto the battlefield. That token has the copiable values of the characteristics of Myr Propagator as it last existed on the battlefield.').
card_ruling('myr propagator', '2011-01-01', 'Here are the weird exceptions promised above. If any copy effects have affected the Myr Propagator whose ability was activated, they\'re taken into account when the token is created. For example: If Myr Propagator\'s ability is activated, then Myr Propagator temporarily becomes a copy of another creature before its ability resolves (due to Cytoshape, perhaps), the token will be a copy of whatever creature the Myr Propagator is currently a copy of. After the turn ends, the Cytoshaped Myr Propagator reverts back to what it was, but the token will stay as it is. Also, if the copy ability of a creature (such as Cemetery Puca, perhaps) makes it become a copy of Myr Propagator and gain another ability, the token created by this creature\'s ability will be a Myr Propagator with that additional ability.').

card_ruling('myr quadropod', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('myr quadropod', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('myr reservoir', '2011-01-01', 'You can use the mana produced by Myr Reservoir\'s first ability to pay an alternative cost or additional cost incurred while casting a Myr spell. It\'s not limited to just that spell\'s mana cost.').
card_ruling('myr reservoir', '2011-01-01', 'The mana can\'t be spent to activate activated abilities of Myr sources that aren\'t on the battlefield.').
card_ruling('myr reservoir', '2011-01-01', 'A card, spell, or permanent is a Myr only if it has the subtype Myr, regardless of its name. For example, Myr Reservoir itself is not a Myr.').

card_ruling('myr servitor', '2004-12-01', 'The Myr Servitor that\'s the source of the ability must be on the battlefield when the ability resolves. If it is, all other Myr Servitor cards are returned from all graveyards to the battlefield under their owner\'s control. If it\'s not, no Myr Servitors are returned to the battlefield.').

card_ruling('myr superion', '2011-06-01', 'Myr Superion can be cast only with mana produced by the abilities of one or more creatures on the battlefield (such as Alloy Myr or Priest of Urabrask in this set).').
card_ruling('myr superion', '2011-06-01', 'Mana produced by abilities of creature cards not on the battlefield, such as Chancellor of the Tangle or Elvish Spirit Guide, can\'t be used to cast Myr Superion.').

card_ruling('myr turbine', '2011-06-01', 'You can tap a Myr that hasn\'t been under your control since your most recent turn began to pay for the cost of the second activated ability.').

card_ruling('myr welder', '2011-06-01', 'Myr Welder has only the activated abilities of cards it exiles. It doesn\'t gain triggered abilities or static abilities.').
card_ruling('myr welder', '2011-06-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities; they have colons in their reminder text.').
card_ruling('myr welder', '2011-06-01', 'If an activated ability of a card exiled with Myr Welder references the card it\'s printed on by name, treat Myr Welder\'s version of that ability as though it referenced Myr Welder by name instead. For example, if Lux Cannon (which says, in part, \"{T}: Put a charge counter on Lux Cannon.\") has been exiled by Myr Welder, Myr Welder has the ability \"{T}: Put a charge counter on Myr Welder.\"').

card_ruling('myriad landscape', '2014-11-07', 'You can choose to find one basic land card with the second activated ability and put it onto the battlefield tapped.').

card_ruling('mysteries of the deep', '2010-03-01', 'Whether you had a land enter the battlefield under your control this turn is checked as this spell resolves, not as you cast it.').
card_ruling('mysteries of the deep', '2010-03-01', 'Having more than one land enter the battlefield under your control this turn provides no additional benefit.').
card_ruling('mysteries of the deep', '2010-03-01', 'The landfall ability checks for an action that has happened in the past. It doesn\'t matter if a land that entered the battlefield under your control previously in the turn is still on the battlefield, is still under your control, or is still a land.').
card_ruling('mysteries of the deep', '2010-03-01', 'Once the spell resolves, having a land enter the battlefield under your control provides no further benefit.').
card_ruling('mysteries of the deep', '2010-03-01', 'The effect of this spell’s landfall ability replaces its normal effect. If you had a land enter the battlefield under your control this turn, only the landfall-based effect happens.').

card_ruling('mystic barrier', '2013-10-17', 'Mystic Barrier affects only what players and planeswalkers each player may attack with creatures he or she controls. It doesn’t affect what players may be targeted by spells or abilities or other interactions.').
card_ruling('mystic barrier', '2013-10-17', 'Mystic Barrier doesn’t affect creatures that enter the battlefield attacking.').
card_ruling('mystic barrier', '2013-10-17', 'In some formats, you may need to disregard teammates sitting between you and the opponent seated nearest you in the chosen direction. In formats like Emperor, other rules may prohibit you from attacking that player.').
card_ruling('mystic barrier', '2013-10-17', 'If two Mystic Barriers are on the battlefield with two different chosen directions, you may not attack a player or planeswalker unless that player or the controller of that planeswalker is the opponent seated nearest you in both directions. (This happens most often in two-player games.)').

card_ruling('mystic decree', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('mystic genesis', '2013-01-24', 'If the target spell is an illegal target when Mystic Genesis tries to resolve, Mystic Genesis will be countered and none of its effects will happen. You won\'t get an Ooze token.').
card_ruling('mystic genesis', '2013-01-24', 'You may target a spell that can\'t be countered. When Mystic Genesis resolves, the target spell will be unaffected, but you\'ll still get an Ooze token.').

card_ruling('mystic meditation', '2015-02-25', 'You may choose to discard a creature card and another card if you want to.').

card_ruling('mystic of the hidden way', '2014-09-20', 'If a face-down Mystic of the Hidden Way is blocked and then turned face up, it stays blocked.').
card_ruling('mystic of the hidden way', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('mystic of the hidden way', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('mystic of the hidden way', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('mystic of the hidden way', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('mystic of the hidden way', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('mystic of the hidden way', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('mystic of the hidden way', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('mystic of the hidden way', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('mystic of the hidden way', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('mystic remora', '2007-09-16', 'First the opponent chooses whether or not to pay {4}. Then, if he or she doesn\'t pay, you choose whether or not to draw a card.').
card_ruling('mystic remora', '2008-04-01', 'A \"noncreature spell\" is any spell that doesn\'t have the type Creature. Artifact Creatures, Enchantment Creatures, and older cards of type Summon are all creature spells.').

card_ruling('mystic retrieval', '2011-01-22', 'Mystic Retrieval can\'t target itself. It\'s on the stack when you choose its target.').

card_ruling('mystic veil', '2005-08-01', 'Does not destroy Auras which are already on the creature.').
card_ruling('mystic veil', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('mystical tutor', '2004-10-04', 'You do not have to find an instant or sorcery card if you do not want to.').

card_ruling('mystifying maze', '2010-08-15', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('mystifying maze', '2010-08-15', 'The targeted creature doesn\'t have to be attacking you. It can be attacking a planeswalker, or (in a multiplayer game) it can be attacking another player.').
card_ruling('mystifying maze', '2010-08-15', 'At the beginning of the next end step, the affected creature is returned to the battlefield even if Mystifying Maze is no longer on the battlefield by then.').

card_ruling('myth realized', '2015-02-25', 'Myth Realized can’t attack the turn it enters the battlefield.').
card_ruling('myth realized', '2015-02-25', 'Activating the ability that turns Myth Realized into a creature while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

