% Rulings

card_ruling('dack fayden', '2014-05-29', 'The ability of Dack’s emblem will resolve before the spell that caused it to trigger.').
card_ruling('dack fayden', '2014-05-29', 'The effect of Dack’s second ability and the effect of the emblem’s ability last indefinitely. You won’t lose control of the permanents if Dack leaves the battlefield.').
card_ruling('dack fayden', '2014-05-29', 'If you gain control of a permanent and you leave the game, the control-changing effect will end. Unless there’s another control-changing effect affecting that permanent, it will return to its owner’s control.').
card_ruling('dack fayden', '2014-05-29', 'If you gain control of a permanent another player owns, and that player leaves the game, the permanent will also leave the game.').

card_ruling('dack\'s duplicate', '2014-05-29', 'Haste and dethrone are part of the copiable values of Dack’s Duplicate. If another creature enters the battlefield as or becomes a copy of Dack’s Duplicate, it will copy whatever Dack’s Duplicate is copying and have haste and dethrone.').
card_ruling('dack\'s duplicate', '2014-05-29', 'The ability of Dack’s Duplicate doesn’t target the creature.').
card_ruling('dack\'s duplicate', '2014-05-29', 'Dack’s Duplicate copies exactly what was printed on the original creature and nothing more (unless that creature is copying something else or is a token; see below), except it will have haste and dethrone. It doesn’t copy whether that creature is tapped or untapped, whether it has any counters on it or Auras attached to it, or any non-copy effects that have changed its power, toughness, types, color, and so on.').
card_ruling('dack\'s duplicate', '2014-05-29', 'If the copied creature has {X} in its mana cost (such as Grenzo, Dungeon Warden), X is considered to be 0.').
card_ruling('dack\'s duplicate', '2014-05-29', 'If the chosen creature is copying something else (for example, if the chosen creature is another Dack’s Duplicate), then Dack’s Duplicate enters the battlefield as whatever the chosen creature is copying. It will have haste and dethrone.').
card_ruling('dack\'s duplicate', '2014-05-29', 'If the chosen creature is a token, Dack’s Duplicate copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Dack’s Duplicate is not a token, even when copying one.').
card_ruling('dack\'s duplicate', '2014-05-29', 'Any enters-the-battlefield abilities of the copied creature will trigger when Dack’s Duplicate enters the battlefield. Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the chosen creature will also work.').
card_ruling('dack\'s duplicate', '2014-05-29', 'If Dack’s Duplicate somehow enters the battlefield at the same time as another creature, Dack’s Duplicate can’t become a copy of that creature. You may only choose a creature that’s already on the battlefield.').
card_ruling('dack\'s duplicate', '2014-05-29', 'You can choose to not copy anything. In that case, Dack’s Duplicate enters the battlefield as a 0/0 Shapeshifter creature, and will probably die almost immediately, when state-based actions are next performed.').
card_ruling('dack\'s duplicate', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('dack\'s duplicate', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('dack\'s duplicate', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('dack\'s duplicate', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('daghatar the adamant', '2014-11-24', 'The two targets of the last ability must be different creatures. Either one may be Daghatar the Adamant.').
card_ruling('daghatar the adamant', '2014-11-24', 'The +1/+1 counter is moved only if both targets are still legal as the ability resolves.').
card_ruling('daghatar the adamant', '2014-11-24', 'To move a counter from one creature to another, the counter is removed from the first creature and placed on the second. Any abilities that care about a counter being placed on the second creature will apply.').

card_ruling('daily regimen', '2008-04-01', 'The +1/+1 counters that are put on the enchanted creature are independent of Daily Regimen. If Daily Regimen leaves the battlefield or is moved to another creature, the counters will stay put.').

card_ruling('dakmor salvage', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('dakmor salvage', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('dakmor salvage', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('dakra mystic', '2014-04-26', 'You make the decision after all the cards are revealed. If you decide not to put the cards into their owners’ graveyards, the card each player draws will be the revealed card.').

card_ruling('damia, sage of stone', '2011-09-22', 'The number of cards in your hand is checked at the beginning of your upkeep before you have the opportunity to cast spells and activate abilities. If you have seven or more cards in your hand at that time, Damia\'s last ability won\'t trigger.').
card_ruling('damia, sage of stone', '2011-09-22', 'After the ability has triggered but before it resolves, you have the opportunity to cast spells and activate abilities.').
card_ruling('damia, sage of stone', '2011-09-22', 'When Damia\'s triggered ability tries to resolve, if you have seven or more cards in your hand, it will have no effect. If you have fewer than seven cards in your hand, you\'ll determine the difference and draw that many cards.').
card_ruling('damia, sage of stone', '2011-09-22', 'Effects can modify the number of cards you\'ll draw. For example, if you have four cards in your hand and control Thought Reflection (\"If you would draw a card, draw two cards instead.\"), you\'ll draw six cards.').

card_ruling('damnation', '2007-02-01', 'This is the timeshifted version of Wrath of God.').

card_ruling('dampen thought', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('dampen thought', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('dampen thought', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('dampen thought', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('damping engine', '2008-08-01', 'A player affected by this card can sacrifice a permanent any time he or she could cast an instant. This is a special action that doesn\'t use the stack and can\'t be responded to.').
card_ruling('damping engine', '2008-08-01', 'A player can only sacrifice a permanent to end this effect if he or she has more permanents than any other player, and he or she hasn\'t already sacrificed a permanent to end the effect this turn. The ability to sacrifice a creature is granted by the same effect that is being ended.').

card_ruling('damping field', '2004-10-04', 'Artifact creatures are artifacts. They are affected so only one may untap.').
card_ruling('damping field', '2004-10-04', 'Damping Field does not prevent artifacts being untapped by spells or abilities at other times.').

card_ruling('damping matrix', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('dance of many', '2004-10-04', 'Destroying the creature that was copied will not cause the token creature to be destroyed as well.').
card_ruling('dance of many', '2004-10-04', 'Each Dance of Many is associated only with its token creature. If one Dance leaves the battlefield, only the corresponding token is affected, not all tokens from all instances of Dance of Many.').
card_ruling('dance of many', '2004-10-04', 'The ability is targeted and checks the validity of the target when put on the stack and when resolving. If the creature is not still there when the copy ability resolves, the ability is countered and no token is put onto the battlefield. This card remains on the battlefield as an enchantment with no token.').
card_ruling('dance of many', '2004-10-04', 'The mana cost is copied.').
card_ruling('dance of many', '2004-10-04', 'Treat the token as entering the battlefield as a copy of the chosen creature. If that creature normally gets counters when entering the battlefield, the token creature gets counters.').
card_ruling('dance of many', '2009-10-01', 'If Dance of Many leaves the battlefield before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve and put a token onto the battlefield. That token won\'t have any connection to a Dance of Many permanent, so it won\'t be exiled when a Dance of Many leaves the battlefield.').

card_ruling('dance of the dead', '2004-10-04', 'If more than one Dance of the Dead ends up on a creature, each contributes a +1/+1. But you only have to pay the untap cost once. You may pay for each one, however, and untap the card more than once during upkeep.').
card_ruling('dance of the dead', '2008-04-01', 'This is a new wording. Dance of the Dead is now an Aura. You target a creature card in a graveyard when you cast it. It enters the battlefield attached to that card. Then it returns that card to the battlefield, and attaches itself to that card again (since the card is treated as a new object on the battlefield).').
card_ruling('dance of the dead', '2008-04-01', 'Once the creature is returned to the battlefield, Dance of the Dead can\'t be attached to anything other than it (unless Dance of the Dead somehow manages to put a different creature onto the battlefield). Attempting to move Dance of the Dead to another creature won\'t work.').
card_ruling('dance of the dead', '2008-04-01', 'If the creature card put onto the battlefield has Protection from Black (or anything that prevents this from legally being attached), this won\'t be able to attach to it. Then this will go to the graveyard as a State-Based Action, causing the creature to be sacrificed.').
card_ruling('dance of the dead', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('dance of the skywise', '2015-02-25', 'The target creature will lose all other colors and creature types and be blue, a Dragon, and an Illusion. It will retain any other types it may have had, such as artifact.').
card_ruling('dance of the skywise', '2015-02-25', 'The target creature will lose any abilities it may have gained prior to Dance of the Skywise resolving. Notably, if the creature is a face-down creature with morph or megamorph, you can’t turn it face up, as it wouldn’t have a morph or megamorph cost when face up (because it’s lost all abilities other than flying).').
card_ruling('dance of the skywise', '2015-02-25', 'After Dance of the Skywise resolves, the creature can gain abilities as normal.').
card_ruling('dance of the skywise', '2015-02-25', 'Dance of the Skywise overrides all previous effects that set the creature’s power or toughness to specific values. However, effects that set its power or toughness to specific values that start to apply after Dance of the Skywise resolves will override this effect.').
card_ruling('dance of the skywise', '2015-02-25', 'Effects that modify the power or toughness of the creature, such as the effects of Giant Growth or Hall of Triumph, will apply to it no matter when they started to take effect. The same is true for counters that change the creature’s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('dance, pathetic marionette', '2010-06-15', 'You\'ll put just one creature card onto the battlefield this way. The other revealed creature cards, and all the revealed noncreature cards, will wind up in their owners\' graveyards.').
card_ruling('dance, pathetic marionette', '2010-06-15', 'If an opponent has no creature cards left in his or her library, that player will wind up revealing his or her entire library and putting it into his or her graveyard. You\'ll still get to choose a creature card revealed by another player.').

card_ruling('dangerous', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('dangerous', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('dangerous', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('dangerous', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('dangerous', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('dangerous', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('dangerous', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('dangerous', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('dangerous', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('dangerous', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('dangerous', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('dangerous', '2013-04-15', 'Dangerous doesn\'t give any creatures the ability to block the target creature. It just forces those creatures that are already able to block the creature to do so.').
card_ruling('dangerous', '2013-04-15', 'As blockers are declared, any creature that\'s tapped or affected by a spell or ability that says it can\'t block doesn\'t block. If there\'s a cost associated with having the creature block, no player is forced to pay that cost, so it doesn\'t block if that cost isn\'t paid.').
card_ruling('dangerous', '2013-04-15', 'If there are multiple combat phases, creatures that blocked the creature targeted by Dangerous in the first combat aren\'t required to block it again in subsequent combats.').

card_ruling('dangerous wager', '2012-05-01', 'If you have no cards in hand when Dangerous Wager resolves, you\'ll simply draw two cards.').

card_ruling('daretti, scrap savant', '2014-11-07', 'You may choose to discard zero cards as the first ability resolves. In that case, you won’t draw any cards.').
card_ruling('daretti, scrap savant', '2014-11-07', 'You choose which artifact to sacrifice as the second ability resolves. You must sacrifice an artifact if you control at least one at that time. For example, if you control two artifacts when you activate the second ability, but the artifact you intended to sacrifice is destroyed in response, you must sacrifice the other artifact as the ability resolves.').
card_ruling('daretti, scrap savant', '2014-11-07', 'The ability of Daretti’s emblem will return the artifact card only if it’s still in your graveyard when the delayed triggered ability resolves at the beginning of the next end step. If the artifact card left your graveyard before that point (even if it was put back into your graveyard), the ability won’t return it to the battlefield.').

card_ruling('darigaaz\'s caldera', '2004-10-04', 'If you don\'t want to unsummon a land, you can play this card then tap it for mana before the enters the battlefield ability resolves. You may then choose to sacrifice it instead of unsummoning a land.').
card_ruling('darigaaz\'s caldera', '2005-08-01', 'This land is of type \"Lair\" only; other subtypes have been removed. It is not a basic land.').

card_ruling('darigaaz, the igniter', '2004-10-04', 'You choose the color during resolution. This means your opponent does not get to react after knowing the color you chose.').

card_ruling('daring leap', '2004-10-04', 'If used after a blocker is assigned your creature is still blocked.').

card_ruling('daring skyjek', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('daring skyjek', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('daring skyjek', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('daring thief', '2014-04-26', 'The exchange of control lasts indefinitely. It doesn’t expire when Daring Thief leaves the battlefield.').
card_ruling('daring thief', '2014-04-26', 'Gaining control of an Aura or Equipment doesn’t cause it to move, though gaining control of an Equipment will allow you to activate its equip ability to attach it to a creature you control later.').
card_ruling('daring thief', '2014-04-26', 'The relevant card types (that is, the ones a permanent can have) are artifact, creature, enchantment, planeswalker, and tribal.').
card_ruling('daring thief', '2014-04-26', 'If the full exchange can’t happen, perhaps because one of the targets is illegal as the inspired ability tries to resolve, then nothing happens. No permanents change controllers.').
card_ruling('daring thief', '2014-04-26', 'If another spell or ability allows you to change the targets of the ability (or perhaps copy it and choose new targets for the copy), you can change the targets only such that the final set of targets is still legal. For example, if the ability targets a creature you control and a creature an opponent controls, you couldn’t change just the second target to a noncreature permanent controlled by that player. You could, however, change just the second target to a creature controlled by a different opponent. The two new targets can share a different card type than the two original targets did.').
card_ruling('daring thief', '2014-04-26', 'Although the permanent you control can’t be a land, the other target can, provided it shares a card type with the first target. For example, you could target an artifact you control and an artifact land controlled by an opponent.').
card_ruling('daring thief', '2014-04-26', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('daring thief', '2014-04-26', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('daring thief', '2014-04-26', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('daring thief', '2014-04-26', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('dark dabbling', '2015-06-22', 'You draw a card as Dark Dabbling resolves, not as the creature actually regenerates.').
card_ruling('dark dabbling', '2015-06-22', 'Whether the spell mastery applies or not, Dark Dabbling targets only one creature. If that creature becomes an illegal target by the time Dark Dabbling tries to resolve, Dark Dabbling will be countered and none of its effects will happen. No creature will regenerate and you won’t draw a card.').
card_ruling('dark dabbling', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('dark deal', '2014-11-24', 'If a player had one card or no cards in his or her hand, that player won’t draw any cards.').

card_ruling('dark depths', '2006-07-15', 'Dark Depths is a land with no mana ability.').
card_ruling('dark depths', '2006-07-15', 'Dark Depths\'s last ability is a state trigger. It will not trigger again while the ability is on the stack, but if the ability is countered and Dark Depths is still on the battlefield with no ice counters on it, it will trigger again immediately.').

card_ruling('dark favor', '2011-09-22', 'If the creature targeted by Dark Favor is an illegal target when Dark Favor tries to resolve, Dark Favor will be countered. It won\'t enter the battlefield and its enters-the-battlefield ability won\'t trigger.').

card_ruling('dark impostor', '2012-05-01', 'Dark Impostor gains only activated abilities. It doesn\'t gain triggered abilities or static abilities. Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities; they have colons in their reminder text.').
card_ruling('dark impostor', '2012-05-01', 'If an activated ability of a card in exile references the card it\'s printed on by name, treat Dark Impostor\'s version of that ability as though it referenced Dark Impostor by name instead. For example, if Marrow Bats (which says \"Pay 4 life: Regenerate Marrow Bats\") is exiled with Dark Impostor, Dark Impostor has the ability \"Pay 4 life: Regenerate Dark Impostor.\"').
card_ruling('dark impostor', '2012-05-01', 'Dark Impostor can target and exile a permanent that\'s only a creature temporarily, like an animated land. However, because that card isn\'t a creature card, Dark Impostor won\'t have any of that card\'s activated abilities.').
card_ruling('dark impostor', '2012-05-01', 'If you control more than one Dark Impostor, each will have only the activated abilities of creature cards exiled with that specific Dark Impostor.').
card_ruling('dark impostor', '2012-05-01', 'Once Dark Impostor leaves the battlefield, it will no longer have the activated abilities of the creature cards exiled with it. If it returns to the battlefield, it will be a new Dark Impostor with no connection to those exiled cards.').

card_ruling('dark petition', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('dark revenant', '2012-10-01', 'Dark Revenant\'s ability isn\'t optional. However, if Dark Revenant leaves the graveyard before its ability resolves, it won\'t be put on top of its owner\'s library.').

card_ruling('dark sphere', '2013-04-15', 'If two or more of these effects would apply, you apply them sequentially. So if the source would deal 5 damage after two of these abilities have resolved, the first one prevents 2 damage, reducing it to 3 damage, then the second one prevents a further 1 damage, reducing the total damage dealt to 2.').

card_ruling('dark supplicant', '2004-10-04', 'You can choose which and how many of the three zones (graveyard, hand, and library) you want to search.').
card_ruling('dark supplicant', '2004-10-04', 'You only shuffle your library if you choose to search it.').

card_ruling('dark temper', '2009-02-01', 'This checks the colors of permanents you control as it resolves.').
card_ruling('dark temper', '2009-02-01', 'Dark Temper has a self-replacement effect, which applies before other prevention or replacement effects would. If you control a black permanent as Dark Temper resolves, it doesn\'t deal damage at all (so damage prevention or redirection effects would have no effect); it simply destroys the targeted creature.').

card_ruling('dark tutelage', '2010-08-15', 'The converted mana cost of the revealed card is determined solely by the mana symbols printed in its upper right corner. The converted mana cost is the total amount of mana in that cost, regardless of color. For example, a card with mana cost {3}{U}{U} has converted mana cost 5.').
card_ruling('dark tutelage', '2010-08-15', 'If the mana cost of the revealed card includes {X}, X is considered to be 0.').
card_ruling('dark tutelage', '2010-08-15', 'If the revealed card has no mana symbols in its upper right corner (because it\'s a land card, for example), its converted mana cost is 0.').

card_ruling('darkblast', '2013-06-07', 'Dredge lets you replace any card draw, not just the one during your draw step.').
card_ruling('darkblast', '2013-06-07', 'Once you decide to replace a draw using a card\'s dredge ability, that card can\'t be removed from your graveyard \"in response.\" (Replacement effects don\'t use the stack.)').
card_ruling('darkblast', '2013-06-07', 'You can\'t use dredge unless you\'re going to draw a card and the card with dredge is already in your graveyard.').

card_ruling('darkest hour', '2013-04-15', 'Affected creatures lose all other colors and are mono-black.').

card_ruling('darkheart sliver', '2007-02-01', 'The player who controlled the Sliver that\'s sacrificed gains the life, not the controller of Darkheart Sliver.').
card_ruling('darkheart sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('darkheart sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('darksteel axe', '2013-07-01', 'Darksteel Axe itself has indestructible, not the creature it\'s equipping.').
card_ruling('darksteel axe', '2013-07-01', 'Although Darksteel Axe has indestructible, it can still be put into the graveyard for other reasons. The most likely reason is if it\'s sacrificed.').

card_ruling('darksteel brute', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('darksteel citadel', '2014-07-18', 'Although Darksteel Citadel is an artifact, it can’t be cast as a spell. Playing it follows the normal rules for playing a land.').

card_ruling('darksteel colossus', '2013-07-01', 'Lethal damage, damage from a source with deathtouch, and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed or if its toughness is 0 or less. (In these cases, of course, Darksteel Colossus would be shuffled into its owner\'s library instead of being put into its owner\'s graveyard.)').

card_ruling('darksteel garrison', '2007-05-01', 'Fortification is to lands what Equipment is to creatures. Except for the difference regarding what kind of permanent it can affect, Fortifications and Equipment follow the same rules.').
card_ruling('darksteel garrison', '2007-05-01', 'If Darksteel Garrison and the fortified land would be destroyed at the same time, only Darksteel Garrison is destroyed.').
card_ruling('darksteel garrison', '2007-05-01', 'The second ability triggers whenever the fortified land becomes tapped, not just when it\'s tapped for mana.').
card_ruling('darksteel garrison', '2007-05-01', 'The triggered ability is mandatory. If you don\'t have a creature to target, you must target another player\'s creature, if possible.').

card_ruling('darksteel juggernaut', '2011-01-01', 'Since Darksteel Juggernaut is itself an artifact, its power and toughness will always be at least 1 while it\'s on the battlefield (unless a spell or ability somehow changes its card type).').
card_ruling('darksteel juggernaut', '2011-01-01', 'Lethal damage and effects that say \"destroy\" won\'t cause Darksteel Juggernaut to be put into the graveyard. However, it can be put into the graveyard for a number of other reasons. The most likely reasons are if its toughness is 0 or less or it\'s sacrificed.').
card_ruling('darksteel juggernaut', '2011-01-01', 'If, during your declare attackers step, Darksteel Juggernaut is tapped, is affected by a spell or ability that says it can\'t attack, or is affected by \"summoning sickness,\" then it doesn\'t attack. If there\'s a cost associated with having Darksteel Juggernaut attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('darksteel juggernaut', '2011-01-01', 'If there are multiple combat phases in a turn, Darksteel Juggernaut must attack only in the first one in which it\'s able to.').
card_ruling('darksteel juggernaut', '2011-01-01', 'The first ability works in all zones.').

card_ruling('darksteel mutation', '2013-10-17', 'Darksteel Mutation has received minor errata to clarify its functionality. The updated Oracle wording appears above.').
card_ruling('darksteel mutation', '2013-10-17', 'Darksteel Mutation overwrites the printed power and toughness of the enchanted creature, as well as any characteristic-defining abilities that define power and/or toughness.').
card_ruling('darksteel mutation', '2013-10-17', 'Darksteel Mutation overwrites any previous effects that set the enchanted creature’s power or toughness to a specific value. Any such effects that start to apply after Darksteel Mutation entered the battlefield will work normally.').
card_ruling('darksteel mutation', '2013-10-17', 'However, Darksteel Mutation does not overwrite effects that change the enchanted creature’s power or toughness without setting it to a specific value (such as the ones created by Giant Growth or Glorious Anthem). It also won’t overwrite the effect of counters.').
card_ruling('darksteel mutation', '2013-10-17', 'The enchanted creature will be only an artifact and a creature, not any other card types. It will be only an Insect, not any other creature types.').
card_ruling('darksteel mutation', '2013-10-17', 'In some rare cases, the creature may have subtypes other than creature types before becoming enchanted with Darksteel Mutation. If it had any other artifact subtypes (such as Equipment), it will retain those. If it had any subtypes other than artifact types and creature types (such as Shrine), it won’t retain those.').
card_ruling('darksteel mutation', '2013-10-17', 'The creature will keep any supertypes it previously had. Notably, if Darksteel Mutation is enchanting a legendary creature, that creature will continue to be legendary. Also, if it’s enchanting a commander, that creature will continue to be a commander.').
card_ruling('darksteel mutation', '2013-10-17', 'Darksteel Mutation doesn’t affect the enchanted creature’s colors, if any. It will continue to be whatever color or colors it was before Darksteel Mutation entered the battlefield.').
card_ruling('darksteel mutation', '2013-10-17', 'Darksteel Mutation causes the enchanted creature to lose all abilities except indestructible at the time it becomes enchanted. Any abilities the creature gains after that point will work normally.').

card_ruling('darkwatch elves', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('darkwater egg', '2008-08-01', 'This is a mana ability, which means it can be activated as part of the process of casting a spell or activating another ability. If that happens you get the mana right away, but you don\'t get to look at the drawn card until you have finished casting that spell or activating that ability.').

card_ruling('daru mender', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('daru sanctifier', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('dash hopes', '2007-02-01', 'When this spell is cast, its \"when you cast\" ability triggers and goes on the stack on top of it.').
card_ruling('dash hopes', '2007-02-01', 'As the triggered ability resolves, the active player gets the option to perform the action. If that player declines, the next player in turn order gets the option. As soon as any player performs the action, the spell is countered, but the remaining players still get the option. If all players decline, the spell remains on the stack.').
card_ruling('dash hopes', '2013-07-01', 'A player can\'t choose to pay 5 life unless he or she actually has 5 or more life to pay.').

card_ruling('dauntless dourbark', '2007-10-01', 'Dauntless Dourbark counts itself when determining its power and toughness.').
card_ruling('dauntless dourbark', '2007-10-01', 'A permanent that\'s both a Forest and a Treefolk will be counted twice when determining Dauntless Dourbark\'s power and toughness.').

card_ruling('dauntless escort', '2013-07-01', 'Once Dauntless Escort\'s ability resolves, the set of creatures it affects is locked in. It won\'t affect creatures that enter the battlefield after the ability has finished resolving.').
card_ruling('dauntless escort', '2013-07-01', 'If any creature leaves your control, it will retain indestructible.').
card_ruling('dauntless escort', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('dauthi jackal', '2004-10-04', 'This creature does not have to be an attacker or blocker to use its ability.').

card_ruling('dauthi mindripper', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('dauthi warlord', '2004-10-04', 'Dauthi Warlord counts creatures with the Shadow ability controlled by all players.').

card_ruling('dawn charm', '2007-02-01', 'Dawn Charm\'s third mode can target a spell that has multiple targets, as long as at least one of those targets is you.').

card_ruling('dawn of the dead', '2004-10-04', 'The creature is only exiled if it is still on the battlefield at the beginning of the end step.').
card_ruling('dawn of the dead', '2008-08-01', 'If the creature leaves the battlefield, but returns to the battlefield before the triggered ability resolves, you won\'t exile it. This is because it is treated as a different object than the one to which the ability refers.').

card_ruling('dawn to dusk', '2014-02-01', 'You choose which mode you’re using—or that you’re using both modes—as you’re casting the spell. Once this choice is made, it can’t be changed later while the spell is on the stack.').
card_ruling('dawn to dusk', '2014-02-01', 'Dawn to Dusk won’t affect any target that is illegal when it tries to resolve. If you chose to use both modes and both targets are illegal at that time, Dawn to Dusk will be countered.').

card_ruling('dawn\'s reflection', '2004-12-01', 'The controller of the land gets the additional mana, not the controller of the enchantment.').
card_ruling('dawn\'s reflection', '2004-12-01', 'The two mana can be of two different colors. The controller of the land chooses the colors.').

card_ruling('dawnbringer charioteers', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('dawnbringer charioteers', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('dawnbringer charioteers', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('dawnfluke', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('dawnglow infusion', '2008-05-01', 'The spell cares about what mana was spent to pay its total cost, not just what mana was spent to pay the hybrid part of its cost.').
card_ruling('dawnglow infusion', '2008-05-01', 'The spell checks on resolution to see if any mana of the stated colors was spent to pay its cost. If so, it doesn\'t matter how much mana of that color was spent.').
card_ruling('dawnglow infusion', '2008-05-01', 'If the spell is copied, the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').
card_ruling('dawnglow infusion', '2008-05-01', 'If you spent both {G} and {W} to cast Dawnglow Infusion, you\'ll gain (X + X) life. An ability that triggers whenever you gain life will trigger just once.').
card_ruling('dawnglow infusion', '2008-05-01', 'Whether you choose to spend both {G} and {W} to cast Dawnglow Infusion has no bearing on the value of X. For example, say you decide that X is 4 and you\'ll spend {G} on {G/W}. If you end up spending {W}{W}{R}{R}{G} to pay the cost, X is still 4 and you\'ve spent both {G} and {W}. You\'ll gain a total of 8 life.').

card_ruling('dawnray archer', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('dawnray archer', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('dawnray archer', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('dawnray archer', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('dawnray archer', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('dawnray archer', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('daxos of meletis', '2013-09-15', 'If Daxos is blocked by a creature, raising that creature’s power to 3 or greater won’t change or undo the block.').
card_ruling('daxos of meletis', '2013-09-15', 'If the exiled card is a land card, you won’t gain any life and you won’t be able to play the land.').
card_ruling('daxos of meletis', '2013-09-15', 'You must pay all costs to cast the exiled card. You may pay alternative or additional costs. If the card has any mandatory additional costs, you must pay those.').
card_ruling('daxos of meletis', '2013-09-15', 'Daxos doesn’t change when you can cast the exiled card. For example, if you exile a creature card without flash, you can cast it only during your main phase when the stack is empty.').

card_ruling('day of the dragons', '2004-10-04', 'The exiled cards are face up while exiled.').
card_ruling('day of the dragons', '2004-10-04', 'The exiled cards return to the battlefield face up.').

card_ruling('day\'s undoing', '2015-06-22', 'Ending the turn this way means the following things happen in order: 1) All spells and abilities on the stack are exiled. This includes spells and abilities that can’t be countered. 2) If there are any attacking and blocking creatures, they’re removed from combat. 3) State-based actions are checked. No player gets priority, and no triggered abilities are put onto the stack. 4) The current phase and/or step ends. The game skips straight to the cleanup step. 5) The cleanup step happens in its entirety.').
card_ruling('day\'s undoing', '2015-06-22', 'If any triggered abilities do trigger during this process, they’re put onto the stack during the cleanup step. If this happens, players will have a chance to cast spells and activate abilities, then there will be another cleanup step before the turn ends.').
card_ruling('day\'s undoing', '2015-06-22', 'Though other spells and abilities that are exiled won’t get a chance to resolve, they don’t count as being countered.').
card_ruling('day\'s undoing', '2015-06-22', 'Any “at the beginning of the next end step” triggered abilities won’t get the chance to trigger that turn because the end step is skipped. Those abilities will trigger at the beginning of the end step of the next turn. The same is true of abilities that trigger at the beginning of other phases or steps.').
card_ruling('day\'s undoing', '2015-06-22', 'If both your hand and graveyard are empty as Day’s Undoing starts resolving, you’ll still shuffle your library before drawing seven cards.').

card_ruling('daybreak coronet', '2007-05-01', 'If all other Auras attached to the enchanted creature stop enchanting it, Daybreak Coronet will be attached to an illegal permanent and will be put into its owner\'s graveyard.').
card_ruling('daybreak coronet', '2007-05-01', 'Because Retether returns all Auras to the battlefield at the same time, it won\'t let you attach Daybreak Coronet to a creature unless that creature is already enchanted.').

card_ruling('dazzling beauty', '2004-10-04', 'Is not very useful against Trample creatures since Trample damage comes through as if a zero toughness creature blocked the Trampler.').
card_ruling('dazzling beauty', '2004-10-04', 'Works on creatures that can\'t be blocked and on creatures that have special blocking requirements.').

card_ruling('dead drop', '2014-09-20', 'The target player chooses which creatures he or she will sacrifice as Dead Drop resolves. If the player controls only one creature at that time, he or she will sacrifice it.').
card_ruling('dead drop', '2014-09-20', 'Dead Drop doesn’t target any creature. The target player could sacrifice a creature with hexproof, for example.').
card_ruling('dead drop', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('dead drop', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('dead drop', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('dead drop', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('dead drop', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('dead reckoning', '2010-03-01', 'You must choose two targets as you cast Dead Reckoning: a creature card in your graveyard and a creature on the battlefield. If you can\'t, then you can\'t cast the spell.').
card_ruling('dead reckoning', '2010-03-01', 'If the targeted creature card is an illegal target as Dead Reckoning resolves (because it\'s no longer in your graveyard, perhaps), you can\'t choose to put it on top of your library. No damage will be dealt.').
card_ruling('dead reckoning', '2010-03-01', 'If the targeted creature is an illegal target as Dead Reckoning resolves (because it\'s no longer on the battlefield, perhaps), you may still put the targeted creature card from your graveyard on top of your library. No damage will be dealt.').
card_ruling('dead reckoning', '2010-03-01', 'Dead Reckoning deals damage equal to the power the creature card had while it was in your graveyard. This could matter if that card is something like Lord of Extinction or Tarmogoyf whose power is determined by a characteristic-defining ability that checks the contents of your graveyard.').

card_ruling('dead reveler', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('dead reveler', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('dead reveler', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('dead reveler', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('dead ringers', '2004-10-04', 'Both of the target creatures must be exactly the same color or combination of colors. Both being colorless is also okay. If they differ in any way, you can still cast the spell, but it does not do anything on resolution.').
card_ruling('dead ringers', '2004-10-04', 'Colors are checked on resolution.').
card_ruling('dead ringers', '2004-10-04', 'If one of the targets left the battlefield before resolution, use the colors it had at the time it left the battlefield for the comparison.').
card_ruling('dead ringers', '2004-10-04', 'If one of the targets became an illegal target before resolution, this spell will still check the color of that target even though it will not destroy that target.').

card_ruling('dead-iron sledge', '2006-10-15', 'Moving Dead-Iron Sledge after the ability triggers will not affect which creatures are destroyed.').

card_ruling('deadbridge chant', '2013-04-15', 'The card is chosen at random as the ability resolves. If any player responds to the ability, that player won\'t yet know what card will be chosen.').
card_ruling('deadbridge chant', '2013-04-15', 'Because the last ability doesn\'t target any card in your graveyard, any card put into your graveyard in response to the ability may be chosen.').
card_ruling('deadbridge chant', '2013-04-15', 'Each card in your graveyard must have an equal chance of being chosen. Assigning them each a number and rolling a die is an easy way to do this. In formats that allow you to rearrange your graveyard—including Standard, Modern, and Return to Ravnica block Limited formats, you may also shuffle your graveyard face down and choose a card at random.').

card_ruling('deadbridge goliath', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('deadeye navigator', '2012-05-01', 'Once Deadeye Navigator or the creature it\'s paired with is exiled, the other creature will no longer have the activated ability. However, you can activate the ability of one creature in response to activating the ability of the other creature.').

card_ruling('deadly allure', '2011-01-22', 'If the target creature is attacking, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block it.').

card_ruling('deadly grub', '2007-02-01', 'The ability checks to see if Deadly Grub had no time counters on it at the time it was put into a graveyard from the battlefield. This doesn\'t necessarily mean it must have been sacrificed due to vanishing; it could have been put into the graveyard some other way (say, while the sacrifice ability of vanishing is on the stack).').

card_ruling('deadshot', '2004-10-04', 'Deadshot can target a creature which is already tapped as its first target, and it will still damage the second target. This is because it taps that creature as an effect, not as a cost.').

card_ruling('deadshot minotaur', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('deadshot minotaur', '2009-05-01', 'The enters-the-battlefield ability is mandatory. If you\'re the only player who controls a creature with flying, you must target one of those creatures.').

card_ruling('deal broker', '2014-05-29', 'You may reveal Deal Broker itself for its second ability. If you do, and you accept another player’s offer, that player won’t be able to use the second ability to offer another exchange.').
card_ruling('deal broker', '2014-05-29', 'When you use Deal Broker’s second ability, after you reveal your card, each other player chooses up to one card from his or her card pool, then all those chosen cards are revealed simultaneously.').
card_ruling('deal broker', '2014-05-29', 'If you accept a player’s offer, the card you revealed becomes part of that player’s card pool and the other offered card becomes part of your card pool.').
card_ruling('deal broker', '2014-05-29', 'The cards exchanged this way aren’t drafted. They can’t be removed from the draft by Cogwork Grinder, for example.').
card_ruling('deal broker', '2014-05-29', 'If either card involved in the exchange has any information noted for it, that information stays with the exchanged card, not the player who initially drafted it. Any abilities that refer to actions taken as you drafted the card (or similar) now refer to the card’s new owner, even though that player wasn’t the person who actually drafted the card.').
card_ruling('deal broker', '2014-05-29', 'If a player exchanges a card that has multiple pieces of information noted for it, all of the information is shared during the exchange. For example, if you receive a Cogwork Tracker from a player who drafted multiple Cogwork Trackers, your Cogwork Trackers may attack any player noted by either you or the other player for Cogwork Tracker. The other player will not remove a noted player from his or her remaining Cogwork Tracker.').
card_ruling('deal broker', '2014-05-29', 'If more than one player drafts a Deal Broker, players who wish to use the second ability to exchange cards do so in a random order.').
card_ruling('deal broker', '2014-05-29', 'If your draft is breaking up into multiple games, the exchange happens before it’s known who will be playing in each game.').

card_ruling('dearly departed', '2011-09-22', 'The effect is cumulative. Human creatures you control will enter the battlefield with a +1/+1 counter for each Dearly Departed in your graveyard.').
card_ruling('dearly departed', '2011-09-22', 'In most cases, when determining whether a creature entering the battlefield under your control should get a +1/+1 counter, you\'ll simply look at what the creature will look like on the battlefield. You\'ll consider any effects affecting a creature entering the battlefield under your control that fall under one of these categories: --Replacement effects that have already modified how the permanent enters the battlefield, like the one created by Clone. --Continuous effects that changed the permanent\'s creature type on the stack, like the one created by Artificial Evolution. --Effects from the permanent\'s own static abilities, as long as they affect only that permanent, like Adaptive Automaton\'s.').
card_ruling('dearly departed', '2011-09-22', 'However, you\'ll ignore the continuous effects of static abilities that come from other sources. For example, if you control Xenograft with Human as the chosen creature type, a Vampire creature would not enter the battlefield with a +1/+1 counter.').

card_ruling('death baron', '2008-10-01', 'A creature that\'s both a Skeleton and a Zombie will get the bonus only once.').
card_ruling('death baron', '2008-10-01', 'Death Baron doesn\'t normally affect itself. If you manage to turn it into a Skeleton, however, then it will give itself +1/+1 and deathtouch.').

card_ruling('death bomb', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('death bomb', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('death by dragons', '2011-09-22', 'If the player is an illegal target when Death by Dragons tries to resolve, it will be countered and none of its effects will happen. No player will put a Dragon token onto the battlefield.').
card_ruling('death by dragons', '2011-09-22', 'If Death by Dragons resolves, the player who\'s targeted is the only one who won\'t put a Dragon token onto the battlefield.').

card_ruling('death cloud', '2013-06-07', 'If you control a permanent that\'s a creature and a land, and you choose to sacrifice it as you sacrifice creatures, it won\'t be on the battlefield when you sacrifice lands. You can\'t sacrifice it again.').

card_ruling('death frenzy', '2014-09-20', 'You gain 1 life for each creature that Death Frenzy causes to have a toughness of 0 or less. The delayed triggered ability is created before state-based actions cause any creatures with 0 or less toughness to die.').
card_ruling('death frenzy', '2014-09-20', 'Only creatures on the battlefield when Death Frenzy resolves will get -2/-2. However, if a creature enters the battlefield and then dies later that turn, you’ll gain 1 life for it.').

card_ruling('death mutation', '2008-08-01', 'If the target is illegal on resolution, the entire spell is countered. As such, you will not put any tokens onto the battlefield.').

card_ruling('death pulse', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('death pulse', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('death pulse', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('death pulse', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('death rattle', '2013-06-07', 'Delve reduces only the generic mana requirement of the spell.').
card_ruling('death rattle', '2013-06-07', 'If you sacrifice permanents or discard cards while casting the spell (for example, activating the mana ability of Blood Pet), those cards can be exiled using delve to reduce the spell\'s cost.').

card_ruling('death spark', '2008-10-01', 'A card is \"directly above\" another card in your graveyard if it was put into that graveyard later and there are no cards in between the two.').
card_ruling('death spark', '2008-10-01', 'layers may not rearrange the cards in their graveyards.').
card_ruling('death spark', '2008-10-01', 'If an effect or rule puts two or more cards into the same graveyard at the same time, the owner of those cards may arrange them in any order.').
card_ruling('death spark', '2008-10-01', 'The last thing that happens to a resolving instant or sorcery spell is that it\'s put into its owner\'s graveyard. Example: You cast Wrath of God. All creatures on the battlefield are destroyed. You arrange all the cards put into your graveyard this way in any order you want. The other players in the game do the same to the cards that are put into their graveyards. Then you put Wrath of God into your graveyard, on top of the other cards.').
card_ruling('death spark', '2008-10-01', 'Say you\'re the owner of both a permanent and an Aura that\'s attached to it. If both the permanent and the Aura are destroyed at the same time (by Akroma\'s Vengeance, for example), you decide the order they\'re put into your graveyard. If just the enchanted permanent is destroyed, it\'s put into your graveyard first. Then, after state-based actions are checked, the Aura (which is no longer attached to anything) is put into your graveyard on top of it.').

card_ruling('death wish', '2004-10-04', 'Can\'t acquire the Ante cards. They are considered still \"in the game\" as are cards in the library and the graveyard.').
card_ruling('death wish', '2007-05-01', 'The choice is optional.').
card_ruling('death wish', '2007-05-01', 'If you fail to find something, you still exile this.').
card_ruling('death wish', '2007-05-01', 'If you fail to find something, you still lose half your life, rounded up.').
card_ruling('death wish', '2007-09-16', 'Can\'t acquire cards that are phased out.').
card_ruling('death wish', '2009-10-01', 'You can\'t acquire exiled cards because those cards are still in one of the game\'s zones.').
card_ruling('death wish', '2009-10-01', 'In a sanctioned event, a card that\'s \"outside the game\" is one that\'s in your sideboard. In an unsanctioned event, you may choose any card from your collection.').

card_ruling('death\'s approach', '2013-01-24', 'The value of X will change as the number of creature cards in the enchanted creature\'s controller\'s graveyard changes. Any time state-based actions are performed, if that creature\'s toughness is 0 or less, or damage marked on it is equal to or greater than its toughness, the creature will be put into its owner\'s graveyard.').

card_ruling('death\'s caress', '2011-01-22', 'Check the creature as it last existed on the battlefield to determine if it was a Human. If it was, use its toughness as it last existed on the battlefield to determine how much life is gained.').
card_ruling('death\'s caress', '2011-01-22', 'If the target creature isn\'t on the battlefield or is otherwise an illegal target when Death\'s Caress tries to resolve, Death\'s Caress is countered and none of its effects will happen. You won\'t gain any life.').
card_ruling('death\'s caress', '2013-07-01', 'If Death\'s Caress resolves but the target creature isn\'t destroyed, perhaps because it has indestructible or it regenerated, you still check if it\'s a Human. If so, you\'ll gain life equal to its toughness.').

card_ruling('death\'s presence', '2012-10-01', 'X is the power of that creature as it last existed on the battlefield.').

card_ruling('death\'s shadow', '2010-03-01', 'Death\'s Shadow\'s ability applies only while Death\'s Shadow is on the battlefield. In all other zones, its power and toughness are 13.').
card_ruling('death\'s shadow', '2010-03-01', 'The value of X changes as you gain and lose life. It\'s not locked in as Death\'s Shadow enters the battlefield.').
card_ruling('death\'s shadow', '2010-03-01', 'If your life total is 13 or greater (and nothing else is boosting Death\'s Shadow\'s toughness), Death\'s Shadow is put into its owner\'s graveyard as a state-based action.').
card_ruling('death\'s shadow', '2010-03-01', 'If your life total is less than 0 and an effect (such as the one from an opponent\'s Abyssal Persecutor) is keeping you from losing the game, Death\'s Shadow\'s ability will actually increase its power and toughness. For example, if your life total is -2, Death\'s Shadow gets +2/+2.').
card_ruling('death\'s shadow', '2010-06-15', 'In a Two-Headed Giant game, your life total is your team\'s life-total').

card_ruling('death-mask duplicant', '2004-12-01', 'Death-Mask Duplicant has an activated imprint ability. This ability can be activated any number of times, so multiple creature cards can become imprinted on this creature.').
card_ruling('death-mask duplicant', '2004-12-01', 'Death-Mask Duplicant gains all landwalk and protection abilities of the imprinted creature cards. If Death-Mask Duplicant imprints a card with protection from red and a card with protection from creatures, it has protection from red and from creatures.').

card_ruling('deathbellow raider', '2013-09-15', 'You still choose which player or planeswalker Deathbellow Raider attacks.').
card_ruling('deathbellow raider', '2013-09-15', 'If, during your declare attackers step, Deathbellow Raider is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under your control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').

card_ruling('deathbringer liege', '2008-08-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').
card_ruling('deathbringer liege', '2008-08-01', 'If you cast a spell that\'s both white and black, the last two abilities will both trigger. You may use them together to destroy any creature. To do so, put the \"destroy\" ability on the stack first, then put the \"tap\" ability on the stack, with each one targeting the same creature. When the abilities resolve, they\'ll tap the creature, then destroy it.').
card_ruling('deathbringer liege', '2010-06-15', 'The last ability can target any creature, not just a tapped creature (it doesn\'t say \"target tapped creature\"). When the ability resolves, check whether the creature is tapped or not. If it\'s tapped, you may destroy it. If it\'s not tapped, the ability does nothing.').

card_ruling('deathbringer regent', '2015-02-25', 'If you cast a creature spell that enters the battlefield as a copy of Deathbringer Regent, the enters-the-battlefield ability will trigger (assuming the “five or more other creatures” requirement is also met).').
card_ruling('deathbringer regent', '2015-02-25', 'Deathbringer Regent’s last ability destroys all creatures except Deathbringer Regent, including the five other creatures required for the ability to have an effect.').

card_ruling('deathbringer thoctar', '2009-05-01', 'If Deathbringer Thoctar and another creature are dealt lethal damage at the same time, Deathbringer Thoctar will be put into a graveyard before it would receive any counters. Its ability will still trigger, but it will do nothing when it resolves.').
card_ruling('deathbringer thoctar', '2009-05-01', 'You may remove any +1/+1 counter from Deathbringer Thoctar to activate its second ability, not just a +1/+1 counter put on it with its first ability.').

card_ruling('deathcoil wurm', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('deathcoil wurm', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('deathcoil wurm', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('deathcoil wurm', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('deathcult rogue', '2013-01-24', 'If a creature already assigned to block Deathcult Rogue stops being a Rogue, it will continue to block.').

card_ruling('deathless angel', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('deathless behemoth', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('deathless behemoth', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('deathless behemoth', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('deathless behemoth', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('deathmist raptor', '2015-02-25', 'Deathmist Raptor’s second ability triggers only if Deathmist Raptor is in the graveyard when a permanent you control is turned face up.').
card_ruling('deathmist raptor', '2015-02-25', 'You choose whether Deathmist Raptor will enter the battlefield face up or face down as the ability resolves. You may also choose to leave it in the graveyard.').
card_ruling('deathmist raptor', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('deathmist raptor', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('deathmist raptor', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('deathpact angel', '2013-01-24', 'The card named Deathpact Angel that you return to the battlefield doesn\'t necessarily have to be the same one that created the token you sacrificed.').
card_ruling('deathpact angel', '2013-01-24', 'The activated ability is part of the Cleric token\'s copiable values. Any copy of that creature token will also have that ability.').
card_ruling('deathpact angel', '2013-01-24', 'The activated ability of the token doesn\'t target any card in your graveyard. You choose the card named Deathpact Angel, if any, when the ability resolves.').

card_ruling('deathreap ritual', '2014-05-29', 'Deathreap Ritual’s ability has an intervening “if” clause, so a creature must have died before the end step begins in order for the ability to trigger. Notably, this means that the trigger won’t be on the stack for you to respond to by destroying a creature.').
card_ruling('deathreap ritual', '2014-05-29', 'You draw one card when the ability resolves, not one card per creature that died during the turn.').

card_ruling('deathrender', '2007-10-01', 'If the creature you put onto the battlefield can\'t be equipped by Deathrender (due to protection from artifacts, for example), the creature enters the battlefield but Deathrender remains unattached.').

card_ruling('deathrite shaman', '2012-10-01', 'Because the first ability requires a target, it is not a mana ability. It uses the stack and can be responded to.').
card_ruling('deathrite shaman', '2012-10-01', 'If the target of any of Deathrite Shaman\'s three abilities is an illegal target when that ability tries to resolve, it will be countered and none of its effects will happen. You won\'t add mana to your mana pool, no opponent will lose life, or you won\'t gain life, as appropriate.').

card_ruling('debt of loyalty', '2008-04-01', 'Casting Debt of Loyalty puts a regeneration shield on a creature. If, later in the turn, that creature would be destroyed, instead it regenerates and you gain control of it. Note that it will be tapped when you gain control of it. The control change has no duration; you\'ll retain control of that creature until the game ends or until some other effect causes it to change control.').
card_ruling('debt of loyalty', '2008-04-01', 'If you cast Debt of Loyalty but nothing happens to the targeted creature for the rest of the turn that would cause it to be destroyed, Debt of Loyalty has no visible effect. The regeneration shield wears off at the end of the turn. If the creature regenerates during a later turn, you won\'t gain control of it.').
card_ruling('debt of loyalty', '2008-04-01', 'If a creature has two regeneration shields on it -- one from a Debt of Loyalty you cast and one from another effect -- and it would be destroyed, its controller chooses which regeneration shield to use. If that player chooses to use the Debt of Loyalty shield, you\'ll gain control of the creature. If the player chooses to use the other shield, you won\'t gain control of the creature, but the Debt of Loyalty shield will remain on that creature for the rest of the turn (in case it would be destroyed again).').
card_ruling('debt of loyalty', '2008-04-01', 'You may cast Debt of Loyalty targeting a creature you already control. If you do, and that creature would be destroyed later in the turn, it will regenerate and you\'ll gain control of it (though gaining control of a creature you already control usually has no visible effect).').
card_ruling('debt of loyalty', '2013-07-01', 'A creature with indestructible never needs to regenerate, so Debt of Loyalty will have no effect on it.').

card_ruling('decaying soil', '2004-10-04', 'Multiple creatures going to the graveyard at the same time all end up there at once, so either all of them or none of them will cause the trigger to go off, depending on whether or not Threshold was met prior to the creatures going to the graveyard.').
card_ruling('decaying soil', '2004-10-04', 'The ability in Threshold will trigger only if Threshold was met before the creature goes to the graveyard. So the one that causes Threshold to be met will not trigger the ability.').

card_ruling('decimate', '2004-12-01', 'You no longer need four distinct targets for Decimate. If the spell or ability uses the word target in multiple places, the same object or player can be chosen once for each instance of the word \"target.\" For example, you could choose Tree of Tales for both the artifact and the land. You still need a legal target enchantment and creature as well.').

card_ruling('declaration of naught', '2008-04-01', 'You can name either half of a split card, but not both. If a player casts the half with the chosen name, it can be targeted by the second ability. In addition, if a player casts a fused split spell with the chosen name, it can be targeted by the second ability.').

card_ruling('decomposition', '2013-04-15', 'If a permanent has multiple instances of cumulative upkeep, each triggers separately. However, the age counters are not connected to any particular ability; each cumulative upkeep ability will count the total number of age counters on the permanent at the time that ability resolves.').

card_ruling('decree of annihilation', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('decree of annihilation', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('decree of annihilation', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('decree of justice', '2004-10-04', 'The two X\'s in the mana cost mean that you pay 2 mana (above the {2}{W}{W} base cost) for each token you want to create.').
card_ruling('decree of justice', '2004-10-04', 'When you cycle this card, the last ability triggers. When that ability resolves, you choose a value of X and pay it during resolution.').
card_ruling('decree of justice', '2004-10-04', 'You can cast this with X=0.').
card_ruling('decree of justice', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('decree of justice', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('decree of justice', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('decree of pain', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('decree of pain', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('decree of pain', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').

card_ruling('decree of savagery', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('decree of savagery', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('decree of savagery', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('decree of savagery', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('decree of silence', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('decree of silence', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('decree of silence', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('decree of silence', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('deep water', '2004-10-04', 'Deep Waters affects lands you control when it resolves and any lands you gain control of this turn.').
card_ruling('deep water', '2008-08-01', 'The amount of mana produced is unchanged, but it will all be {U}.').

card_ruling('deep-sea kraken', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('deep-sea kraken', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('deep-sea kraken', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('deep-sea kraken', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('deep-sea kraken', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('deep-sea kraken', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('deep-sea kraken', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('deep-sea kraken', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('deep-sea kraken', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('deep-sea kraken', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('deep-sea terror', '2015-06-22', 'Deep-Sea Terror’s ability only checks as attackers are declared. After Deep-Sea Terror legally attacks, reducing the number of cards in your graveyard won’t cause it to stop attacking.').

card_ruling('deepcavern imp', '2007-05-01', 'If you have no cards in hand, you can\'t pay the echo cost and Deepcavern Imp will be sacrificed.').

card_ruling('deepfire elemental', '2006-07-15', 'For example, for Deepfire Elemental to destroy a creature with converted mana cost 4 would cost {4}+{4}+{1} = {9}.').

card_ruling('deepwater hypnotist', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('deepwater hypnotist', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('deepwater hypnotist', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('deepwater hypnotist', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('defender of chaos', '2004-10-04', 'This card is always a creature spell and is never an instant spell, no matter when you cast it.').

card_ruling('defender of law', '2004-10-04', 'This card is always a creature spell and is never an instant spell, no matter when you cast it.').

card_ruling('defender of the order', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('defense grid', '2004-10-04', 'The additional cost can be reduced by effects that reduce costs, such as Sapphire Medallion.').
card_ruling('defense grid', '2006-05-01', 'In Two-Headed Giant, spells don\'t cost extra on your own turn.').

card_ruling('defense of the heart', '2004-10-04', 'In a multiplayer game, it triggers if at least one opponent controls three or more creatures.').
card_ruling('defense of the heart', '2004-10-04', 'You choose how many cards to put onto the battlefield during the resolution after you look through your library. You do not choose as the ability goes on the stack.').
card_ruling('defense of the heart', '2004-10-04', 'This triggered ability only triggers if your opponent controls 3 or more creatures at the beginning of upkeep. This condition is checked again on resolution.').
card_ruling('defense of the heart', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('defense of the heart', '2010-06-15', 'If this card is not on the battlefield at the time it resolves, then you can\'t sacrifice it. You still perform the search and put any creature cards you find onto the battlefield.').

card_ruling('defensive formation', '2004-10-04', 'Damage assignment choices from Trample and other things which allow alternative damage assignments for attacking creatures are all made by you.').
card_ruling('defensive formation', '2010-03-01', 'With the exception of ignoring the damage assignment order, you follow all the normal rules for damage assignment.').

card_ruling('defiant bloodlord', '2015-08-25', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Drana’s Emissary or 7 life from Nissa’s Renewal.').
card_ruling('defiant bloodlord', '2015-08-25', 'A creature with lifelink dealing combat damage is a single life-gaining event. For example, if two creatures you control with lifelink deal combat damage at the same time, the ability will trigger twice. However, if a single creature with lifelink deals combat damage to multiple creatures, players, and/or planeswalkers at the same time (perhaps because it has trample or was blocked by more than one creature), the ability will trigger only once.').
card_ruling('defiant bloodlord', '2015-08-25', 'In a Two-Headed Giant game, life gained by your teammate won’t cause the ability to trigger, even though it causes your team’s life total to increase.').

card_ruling('defiant falcon', '2004-10-04', 'This card is both a Rebel and a Bird.').
card_ruling('defiant falcon', '2004-10-04', 'You do not have to find a Rebel card if you do not want to.').

card_ruling('defiant ogre', '2014-11-24', 'You choose which mode you’re using as you put the ability on the stack, after the creature has entered the battlefield. Once you’ve chosen a mode, you can’t change that mode even if the creature leaves the battlefield in response to that ability.').
card_ruling('defiant ogre', '2014-11-24', 'If a mode requires a target and there are no legal targets available, you must choose the mode that adds a +1/+1 counter.').

card_ruling('defiant vanguard', '2004-10-04', 'You do not have to find a Rebel card if you do not want to.').

card_ruling('deflecting palm', '2014-09-20', 'Deflecting Palm doesn’t target any creature or player. You choose a source of damage as Deflecting Palm resolves.').
card_ruling('deflecting palm', '2014-09-20', 'If multiple prevention and/or replacement effects are trying to apply to the same damage, the player who would be dealt damage chooses the order in which to apply them. Notably, if noncombat damage would be dealt to you by a source controlled by an opponent, you choose the order in which to apply Deflecting Palm and the planeswalker redirection effect. If you apply Deflecting Palm first, the damage will be prevented (and damage will be dealt to the source’s controller) and the planeswalker redirection effect won’t apply.').

card_ruling('deflection', '2004-10-04', 'Once the spell resolves, the new target is considered to be targeted by the deflected spell. This will trigger any effects which trigger on being targeted.').
card_ruling('deflection', '2004-10-04', 'This only targets the spell being changed, not the original or new target of the spell it is affecting.').
card_ruling('deflection', '2004-10-04', 'The target of a spell that targets another spell on the stack can be changed to any other spell on the stack, even if the new target will resolve before the spell does.').
card_ruling('deflection', '2004-10-04', 'You can\'t make a spell which is on the stack target itself.').
card_ruling('deflection', '2004-10-04', 'You can choose to make a spell on the stack target this spell (if such a target choice would be legal had the spell been cast while this spell was on the stack). The new target for the deflected spell is not chosen until this spell resolves. This spell is still on the stack when new targets are selected for the spell.').
card_ruling('deflection', '2004-10-04', 'If there is no other legal target for the spell, this does not change the target.').
card_ruling('deflection', '2004-10-04', 'This does not check if the current target is legal. It just checks if the spell has a single target.').
card_ruling('deflection', '2004-10-04', 'You choose the spell to target on announcement, but you pick the new target for that spell on resolution.').
card_ruling('deflection', '2009-02-01', 'If a spell targets multiple things, you can\'t target it with Deflection, even if all but one of those targets has become illegal.').
card_ruling('deflection', '2009-02-01', 'If a spell targets the same player or object multiple times, you can\'t target it with Deflection.').

card_ruling('defy death', '2012-05-01', 'No player can cast spells or activate abilities between returning the creature card to the battlefield and checking whether it\'s an Angel.').
card_ruling('defy death', '2012-05-01', 'If the creature is an Angel only on the battlefield (perhaps because it\'s a non-Angel creature card and Xenograft is on the battlefield), it will get the +1/+1 counters.').

card_ruling('dega sanctuary', '2004-10-04', 'You can gain 2 or 4 life, but never 6.').

card_ruling('deicide', '2014-04-26', 'Deicide looks at the card in exile, not the permanent that was exiled, to determine if it is a God. For each of the Gods in the Theros block, it won’t matter what your devotion to its color(s) was. The card is a God card when not on the battlefield.').
card_ruling('deicide', '2014-04-26', 'When you search the target’s controller’s graveyard, hand, and library, you may leave any cards with that name in the zones they’re in. You don’t have to exile them.').

card_ruling('deity of scars', '2008-08-01', 'You can pay {B/G} and remove a -1/-1 counter to activate the ability any time you want. Deity of Scars doesn\'t need to be in danger of being destroyed.').
card_ruling('deity of scars', '2008-08-01', 'Removing a -1/-1 counter from Deity of Scars is a cost of activating its activated ability. You can\'t activate the ability if there are no -1/-1 counters on Deity of Scars.').
card_ruling('deity of scars', '2008-08-01', 'Deity of Scars\'s ability causes it to have more toughness when you activate the ability (as a result of losing a -1/-1 counter) and then puts a regeneration shield on it when the ability resolves. If you activate the ability in advance of Deity of Scars being dealt 5 damage, for example, Deity of Scars will be 6/6 before the damage is dealt to it. It won\'t use the regeneration shield unless it\'s dealt more damage that turn.').

card_ruling('delay', '2007-05-01', 'When the last time counter is removed from the exiled card, it\'s cast as a completely new spell. Modes and targets are chosen again. If the spell has any additional costs, they must be paid again. If they can\'t be, the spell can\'t be cast and stays exiled.').
card_ruling('delay', '2007-05-01', 'If the spell targeted by Delay was cast with flashback, Delay\'s effect will exile it, not the flashback effect. The card will get time counters and gain suspend (if it didn\'t already have suspend).').

card_ruling('delaying shield', '2004-10-04', 'Note that the damage is replaced and is not \"prevented\". This means it can replace damage that \"can\'t be prevented\".').

card_ruling('delif\'s cone', '2004-10-04', 'If you use the ability after blockers are declared, it won\'t trigger at all. So you want to use it before blockers are declared.').
card_ruling('delif\'s cone', '2004-10-04', 'This card has an activated ability that creates a triggered ability which watches the target creature.').
card_ruling('delif\'s cone', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('delif\'s cube', '2004-10-04', 'If you use the ability after blockers are declared, it won\'t trigger at all. So you want to use it before blockers are declared.').
card_ruling('delif\'s cube', '2004-10-04', 'This card has an activated ability that creates a triggered ability which watches the target creature.').
card_ruling('delif\'s cube', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('delirium', '2004-10-04', 'Tapping the creature is part of the effect and not the cost, therefore you can cast this targeting a tapped creature.').

card_ruling('delirium skeins', '2006-05-01', 'First the active player chooses which cards to discard, then the nonactive player chooses, then all cards are discarded at the same time. No one sees what the other players are discarding before deciding which cards to discard.').

card_ruling('delver of secrets', '2011-09-22', 'You may reveal the card even if it\'s not an instant or sorcery. Whether or not you reveal it, the card stays on top of your library.').

card_ruling('dementia bat', '2011-06-01', 'Unlike most abilities that force a player to discard cards, this ability may be activated whenever you could cast an instant, including during your opponent\'s draw step after he or she has drawn a card.').
card_ruling('dementia bat', '2011-06-01', 'You can\'t force an opponent to discard a card he or she casts by activating Dementia Bat\'s ability in response. The spell is on the stack by then.').

card_ruling('dementia sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('dementia sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('demigod of revenge', '2008-05-01', 'This ability triggers when you cast Demigod of Revenge as a spell. It won\'t trigger if Demigod of Revenge is put directly onto the battlefield.').
card_ruling('demigod of revenge', '2008-05-01', 'The triggered ability will resolve before Demigod of Revenge will.').
card_ruling('demigod of revenge', '2008-05-01', 'If Demigod of Revenge is countered *before* its triggered ability resolves, the ability will still resolve. It will return that Demigod of Revenge from your graveyard to the battlefield, as well as any others. If you\'re casting a spell to counter Demigod of Revenge, it\'s important to clarify you\'re casting it *after* the triggered ability resolves if you don\'t want that Demigod of Revenge to be put onto the battlefield.').

card_ruling('demon of death\'s gate', '2010-08-15', 'Casting Demon of Death\'s Gate by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast a creature spell.').
card_ruling('demon of death\'s gate', '2010-08-15', 'Casting Demon of Death\'s Gate by paying its alternative cost doesn\'t change its mana cost or converted mana cost.').
card_ruling('demon of death\'s gate', '2010-08-15', 'Effects that increase or reduce the cost to cast Demon of Death\'s Gate will apply to it even if you choose to pay its alternative cost.').

card_ruling('demon of wailing agonies', '2014-11-07', 'If Demon of Wailing Agonies deals combat damage to a player at the same time your commander is dealt lethal damage, the triggered ability will trigger and the player will sacrifice a creature.').
card_ruling('demon of wailing agonies', '2014-11-07', 'Lieutenant abilities apply only if your commander is on the battlefield and under your control.').
card_ruling('demon of wailing agonies', '2014-11-07', 'Lieutenant abilities refer only to whether you control your commander, not any other player’s commander.').
card_ruling('demon of wailing agonies', '2014-11-07', 'If you gain control of a creature with a lieutenant ability owned by another player, that ability will check to see if you control your commander and will apply if you do. It won’t check whether its owner controls his or her commander.').
card_ruling('demon of wailing agonies', '2014-11-07', 'If you lose control of your commander, lieutenant abilities of creatures you control will immediately stop applying. If this causes a creature’s toughness to become less than or equal to the amount of damage marked on it, the creature will be destroyed.').
card_ruling('demon of wailing agonies', '2014-11-07', 'If a triggered ability granted by a lieutenant ability triggers, and in response to that trigger you lose control of your commander (causing the lieutenant to lose that ability), that triggered ability will still resolve.').

card_ruling('demon\'s herald', '2008-10-01', 'To activate the ability, you must sacrifice three different creatures. You can\'t sacrifice just one three-colored creature, for example.').
card_ruling('demon\'s herald', '2008-10-01', 'You may sacrifice multicolored creatures as part of the cost of the search ability. If you sacrifice a multicolored creature this way, specify which part of the cost that creature is satisfying.').
card_ruling('demon\'s herald', '2008-10-01', 'You may sacrifice the Herald itself to help pay for the cost of its ability.').

card_ruling('demon\'s horn', '2009-10-01', 'The ability triggers whenever any player, not just you, casts a black spell.').
card_ruling('demon\'s horn', '2009-10-01', 'If a player casts a black spell, Demon\'s Horn\'s ability triggers and is put on the stack on top of that spell. Demon\'s Horn\'s ability will resolve (causing you to gain 1 life) before the spell does.').

card_ruling('demonic appetite', '2010-06-15', 'When Demonic Appetite\'s triggered ability resolves, you may sacrifice the creature enchanted by Demonic Appetite. If you control no other creatures, you\'ll have to sacrifice that one.').
card_ruling('demonic appetite', '2010-06-15', 'If another player gains control of either Demonic Appetite or the enchanted creature (but not both), Demonic Appetite will be enchanting an illegal permanent. The Aura will be put into its owner\'s graveyard as a state-based action.').

card_ruling('demonic consultation', '2004-10-04', 'There is no way to make this card affect your opponent. It affects \"you\", and \"you\" means the controller of the spell. It has no targets.').
card_ruling('demonic consultation', '2004-10-04', 'You must name a card that actually exists in the game of Magic.').
card_ruling('demonic consultation', '2008-10-01', 'You don\'t name a card until Demonic Consultation resolves.').
card_ruling('demonic consultation', '2008-10-01', 'If you don\'t reveal the named card (perhaps because it was in the top six cards of your library), you\'ll end up exiling your entire library. You don\'t lose the game at that point, but will lose the next time you\'re instructed to draw a card.').

card_ruling('demonic dread', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('demonic dread', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('demonic dread', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('demonic dread', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('demonic dread', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('demonic dread', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('demonic dread', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('demonic pact', '2015-06-22', 'You choose the mode as the triggered ability goes on the stack. You can choose a mode that requires targets only if there are legal targets available.').
card_ruling('demonic pact', '2015-06-22', 'If the ability is countered (either for having its target become illegal or because a spell or ability counters it), the mode chosen for that instance of the ability still counts as being chosen.').
card_ruling('demonic pact', '2015-06-22', 'The phrase “that hasn’t been chosen” refers only to that specific Demonic Pact. If you control one and cast another one, you can choose any mode for the second one the first time its ability triggers.').
card_ruling('demonic pact', '2015-06-22', 'It doesn’t matter who has chosen any particular mode. For example, say you control Demonic Pact and have chosen the first two modes. If an opponent gains control of Demonic Pact, that player can choose only the third or fourth mode.').
card_ruling('demonic pact', '2015-06-22', 'In some very unusual situations, you may not be able to choose a mode, either because all modes have previously been chosen or the only remaining modes require targets and there are no legal targets available. In this case, the ability is simply removed from the stack with no effect.').
card_ruling('demonic pact', '2015-06-22', 'Yes, if the fourth mode is the only one remaining, you must choose it. You read the whole contract, right?').

card_ruling('demonic rising', '2012-05-01', 'If you control zero creatures or more than one creature at the beginning of your end step, the ability won\'t trigger. If you control zero creatures or more than one creature when the ability resolves, it will do nothing.').
card_ruling('demonic rising', '2012-05-01', 'Although you must control exactly one creature both when the ability would trigger and when it would resolve in order for the ability to create the Demon token, that creature doesn\'t have to be the same one both times. For example, if the ability triggers, then you lose control of your one creature before it resolves, you can cast a creature with flash to make sure you control exactly one creature when the ability resolves.').

card_ruling('demonic taskmaster', '2012-05-01', 'If you control no other creatures, you won\'t sacrifice anything.').
card_ruling('demonic taskmaster', '2012-05-01', 'If Demonic Taskmaster leaves the battlefield after its ability triggers but before it resolves, you\'ll still have to sacrifice a creature. Notably, if you control two Demonic Taskmasters and no other creatures, each one will force you to sacrifice the other.').

card_ruling('demonic tutor', '2004-10-04', 'You pick a card on resolution.').
card_ruling('demonic tutor', '2004-10-04', 'You don\'t reveal the card to your opponent.').
card_ruling('demonic tutor', '2004-10-04', 'This card is put directly into your hand. It is not drawn.').

card_ruling('demonlord of ashmouth', '2012-05-01', 'Demonlord of Ashmouth\'s enters-the-battlefield ability will trigger no matter how it entered the battlefield, including because of its undying ability.').
card_ruling('demonlord of ashmouth', '2012-05-01', 'You don\'t have to sacrifice another creature, even if you control one. You may choose to exile Demonlord of Ashmouth.').

card_ruling('demonmail hauberk', '2011-09-22', 'You can sacrifice the creature Demonmail Hauberk is equipping in order to equip it to another creature.').

card_ruling('demonspine whip', '2009-05-01', 'When the first ability resolves, the +X/+0 bonus is given to the creature that Demonspine Whip is attached to at that time, regardless of what creature (if any) it was attached to when the ability was activated. If Demonspine Whip is on the battlefield but not attached to a creature at this time, no creature gets the bonus.').
card_ruling('demonspine whip', '2009-05-01', 'Once Demonspine Whip\'s ability resolves and gives the +X/+0 bonus to a creature, that bonus stays with that creature for the rest of the turn even if Demonspine Whip moves to another creature or leaves the battlefield.').
card_ruling('demonspine whip', '2009-05-01', 'If Demonspine Whip is no longer on the battlefield by the time its first ability resolves, the game checks whether it was attached to a creature at the time it left the battlefield. If it was, that creature gets the +X/+0 bonus. If it wasn\'t, no creature gets the bonus.').

card_ruling('den protector', '2015-02-25', 'You compare Den Protector’s power to the power of any creature trying to block it only as blockers are assigned. Once Den Protector has been legally blocked by a creature, changing the power of either creature won’t change or undo the block.').
card_ruling('den protector', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('den protector', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('den protector', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('dense canopy', '2008-05-01', 'A creature with reach can still block creatures with flying as well as creatures without flying.').

card_ruling('deny reality', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('deny reality', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('deny reality', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('deny reality', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('deny reality', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('deny reality', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('deny reality', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('deranged hermit', '2004-10-04', 'The Squirrel tokens get the bonus, so they are effectively 2/2 creatures until the Hermit leaves the battlefield. And they get bigger if multiple Hermits are on the battlefield.').

card_ruling('deranged outcast', '2011-01-22', 'You can sacrifice any Human you control, including Deranged Outcast itself.').
card_ruling('deranged outcast', '2011-01-22', 'Because targets are chosen before costs are paid, you can activate Deranged Outcast\'s ability targeting itself and then sacrifice Deranged Outcast to pay the cost.').

card_ruling('derevi, empyrial tactician', '2013-10-17', 'You can activate Derevi’s last ability only when it is in the command zone.').
card_ruling('derevi, empyrial tactician', '2013-10-17', 'When you activate Derevi’s last ability, you’re not casting Derevi as a spell. The ability can’t be countered by something that counters only spells. The ability isn’t subject to the additional cost of casting commanders from the command zone.').

card_ruling('dermoplasm', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('descendant of kiyomaro', '2005-06-01', 'If any opponent has more cards or as many cards in hand as Descendant of Kiyomaro\'s controller, Descendant of Kiyomaro doesn\'t get any bonus.').
card_ruling('descendant of kiyomaro', '2005-06-01', 'Changing the number of cards in any player\'s hand can cause Descendant of Kiyomaro to lose the life-gain triggered ability. Once an ability triggers, however, it exists independently of its source.').

card_ruling('descendant of masumaro', '2005-06-01', 'Descendant of Masumaro\'s printed power and toughness is 1/1, so removing all the +1/+1 counters usually doesn\'t reduce the creature\'s toughness to 0.').
card_ruling('descendant of masumaro', '2005-06-01', 'The counters are added and then removed without a pause for priority or state-based actions being checked in between.').
card_ruling('descendant of masumaro', '2005-06-01', 'All +1/+1 counters on Descendant of Masumaro are removed, including those put on it by other effects.').

card_ruling('descendants\' path', '2012-05-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('descendants\' path', '2012-05-01', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('descendants\' path', '2012-05-01', 'If the revealed card is a creature card that shares a creature type with a creature you control, but you choose not to cast it, it stays on top of your library.').

card_ruling('descent into madness', '2012-05-01', 'For each despair counter on Descent into Madness, you\'ll exile a permanent you control or exile a card from your hand, not both.').
card_ruling('descent into madness', '2012-05-01', 'First you choose the permanents and/or cards from your hand that will be exiled. Then each other player in turn order does the same. Then all the chosen permanents and cards are exiled simultaneously. Players who choose after you will know what permanents you\'ll be exiling when they choose. They\'ll know how many cards you\'ll be exiling from your hand, but they won\'t see those cards.').
card_ruling('descent into madness', '2012-05-01', 'If there are more counters on Descent into Madness than the total number of permanents you control plus the number of cards in your hand, you\'ll exile all permanents you control (including Descent into Madness) and all cards from your hand.').
card_ruling('descent into madness', '2012-05-01', 'If Descent into Madness isn\'t on the battlefield when its ability resolves, use the number of counters on it when it left the battlefield to determine how many permanents and/or cards from hands to exile.').

card_ruling('descent of the dragons', '2015-02-25', 'If a creature is targeted but not destroyed (for example, if it regenerates or has indestructible), it won’t count toward the number of Dragon tokens put onto the battlefield.').

card_ruling('desecrated earth', '2009-10-01', 'If the targeted land is an illegal target by the time Desecrated Earth resolves, the entire spell is countered. No one discards a card.').

card_ruling('desecration demon', '2012-10-01', 'Players won\'t know which player or planeswalker Desecration Demon will attack, if any, when deciding whether to sacrifice a creature.').
card_ruling('desecration demon', '2012-10-01', 'Each opponent in turn order may choose to sacrifice a creature, even if an opponent already chose to sacrifice a creature that combat. Desecration Demon will have a maximum of one +1/+1 counter put on it each combat, no matter how many creatures were sacrificed.').

card_ruling('desecration elemental', '2004-12-01', 'This ability triggers off anyone casting a spell, including you, not just Desecration Elemental\'s controller or an opponent.').
card_ruling('desecration elemental', '2004-12-01', 'If Desecration Elemental is the only creature you control when its triggered ability resolves, you\'ll have to sacrifice it.').

card_ruling('desecrator hag', '2008-08-01', 'This ability doesn\'t target a card. It will determine which card is returned to your hand as it resolves. If there\'s a tie, you choose which of the tied cards to return to your hand at that time.').
card_ruling('desecrator hag', '2008-08-01', 'If the creature card has \"*\" in its power or toughness, the ability that defines \"*\" works in all zones.').

card_ruling('desert', '2004-10-04', 'The ability can be used on any player\'s attacking creatures. This includes your own and creatures in an attack you are not involved in (for multiplayer games).').

card_ruling('deserter\'s quarters', '2014-04-26', 'The activated ability can target any creature, not just one that’s untapped.').
card_ruling('deserter\'s quarters', '2014-04-26', 'As long as Deserter’s Quarters remains tapped, the effect stopping the creature from untapping during its controller’s untap step remains even if that creature untaps some other way (for example, with Colossal Heroics).').

card_ruling('desertion', '2004-10-04', 'The card is put onto the battlefield, but any effects that check if the original card was \"cast from your hand\" will not trigger or otherwise consider the card to have been cast from your hand. The card was put onto the battlefield by the effect of Desertion instead.').
card_ruling('desertion', '2004-10-04', 'This spell includes a replacement effect. If the target is an artifact or creature, it never goes to the graveyard.').
card_ruling('desertion', '2004-10-04', 'If the spell is not countered (because the spell it targets can\'t be countered), then this card\'s ability does not put the card onto the battlefield. The spell continues to resolve as normal.').

card_ruling('desolation', '2004-10-04', 'It works even if it was not on the battlefield when the land was tapped for mana.').
card_ruling('desolation', '2004-10-04', 'Each Desolation only causes up to one land per turn to be sacrificed. If you have more than one, each will do this.').

card_ruling('desperate charge', '2009-10-01', 'Desperate Charge affects only creatures you control at the time it resolves. A creature that enters the battlefield later in the turn won\'t get +2/+0.').

card_ruling('desperate gambit', '2004-10-04', 'The coin is flipped during resolution of this spell, so you know in advance what will happen when the source deals damage the next time.').
card_ruling('desperate gambit', '2008-04-01', 'The chosen source may be a permanent, a spell on the stack, or anything referred to by something on the stack (such as a Prodigal Pyromancer that\'s left the battlefield while its ability is still on the stack)').

card_ruling('desperate ritual', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('desperate ritual', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('desperate ritual', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('desperate ritual', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').
card_ruling('desperate ritual', '2013-06-07', 'Desperate Ritual isn\'t a mana ability. When cast, it (or the spell it\'s spliced onto) goes on the stack like any spell and can be responded to.').

card_ruling('desperate stand', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('desperate stand', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('desperate stand', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('desperate stand', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('desperate stand', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('despotic scepter', '2008-10-01', 'You own a nontoken permanent if it started the game in your deck. If it started the game in no one\'s deck (for example, it was brought into the game by Living Wish), you own it if you brought it into the game.').
card_ruling('despotic scepter', '2012-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').

card_ruling('destroy the evidence', '2012-10-01', 'If the controller of the target land has no land cards in his or her library, all cards from that library will be revealed and put into his or her graveyard.').

card_ruling('destructive force', '2010-08-15', 'First the player whose turn it is chooses five lands to sacrifice, then each other player in turn order does the same, then all chosen lands are sacrificed at the same time. After that, Destructive Force deals 5 damage to each creature.').

card_ruling('destructive revelry', '2013-09-15', 'You must target an artifact or enchantment to cast Destructive Revelry. If that artifact or enchantment is an illegal target when Destructive Revelry tries to resolve, it will be countered and none of its effects will happen. No damage will be dealt. However, if Destructive Revelry resolves and the artifact or enchantment isn’t destroyed (perhaps because it has indestructible), damage will be dealt.').

card_ruling('destructive urge', '2004-10-04', 'The ability has no effect if the player has no lands.').

card_ruling('detainment spell', '2006-09-25', 'Detainment Spell prevents mana abilities of the enchanted creature from being activated.').
card_ruling('detainment spell', '2006-09-25', 'Morph isn\'t an activated ability, so Detainment Spell doesn\'t prevent face-down creatures from being able to turn face up.').
card_ruling('detainment spell', '2006-09-25', 'Responding to a creature\'s activated ability being activated by moving Detainment Spell onto that creature won\'t affect the ability that was just activated.').

card_ruling('detention sphere', '2012-10-01', 'Although the target of the enters-the-battlefield ability must not be a land, lands with the same name as that permanent will be exiled.').
card_ruling('detention sphere', '2012-10-01', 'The enters-the-battlefield ability has only one target. The other permanents with that name aren\'t targeted. For example, a permanent with protection from white will be exiled if it has the same name as the target nonland permanent.').
card_ruling('detention sphere', '2012-10-01', 'If the target nonland permanent is an illegal target when the enters-the-battlefield ability tries to resolve, it will be countered and none of its effects will happen. No permanents will be exiled, including those with the same name as the target.').
card_ruling('detention sphere', '2012-10-01', 'If Detention Sphere leaves the battlefield before its enters-the-battlefield ability has resolved, its leaves-the-battlefield ability will trigger and do nothing. Then the enters-the-battlefield ability will resolve and exile the targeted nonland permanent and other permanents with that name indefinitely.').

card_ruling('determined', '2006-05-01', 'After Determined resolves, any spells or abilities that would counter a spell you control that turn will still resolve. They just won\'t counter that spell.').
card_ruling('determined', '2006-05-01', 'Spells you control may be countered by spells or abilities while Determined is on the stack, waiting to resolve.').

card_ruling('detonate', '2004-10-04', 'If the artifact becomes an illegal target before resolution, then no damage is done.').

card_ruling('detritivore', '2007-02-01', 'The triggered ability is mandatory. If no other player has any nonbasic lands on the battlefield, you must target one of your own.').
card_ruling('detritivore', '2007-02-01', 'Both instances of X in the suspend ability are the same. You determine the value of X as you suspend the card from your hand. The value you choose must be at least 1.').
card_ruling('detritivore', '2007-02-01', 'If this is suspended, then when the last time counter is removed from it, both its triggered ability and the \"play this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('detritivore', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('detritivore', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('detritivore', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('detritivore', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('detritivore', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('detritivore', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('detritivore', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('detritivore', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('detritivore', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('detritivore', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('deus of calamity', '2008-05-01', 'This ability will trigger when Deus of Calamity deals 6 damage to an opponent at one time. It won\'t keep track of the accumulated damage that it deals at different times.').
card_ruling('deus of calamity', '2008-05-01', 'This ability triggers from any damage, not just combat damage, that Deus of Calamity deals to an opponent in increments of 6 or more.').

card_ruling('devastating summons', '2010-06-15', 'You may sacrifice zero lands as you cast Devastating Summons. If you do, you\'ll put two 0/0 creature tokens onto the battlefield as the spell resolves. Unless some effect (such as the one from Glorious Anthem) is boosting their toughness, they\'ll be put into the graveyard as a state-based action. Abilities that trigger when a creature enters the battlefield will trigger, as will those that trigger when a creature leaves the battlefield.').

card_ruling('development', '2006-05-01', 'Development\'s effect is repeated three times no matter which option the opponent chooses. The opponent may choose a different option each time. You\'ll wind up with three Elemental tokens, two tokens and a card, one token and two cards, or three cards, whichever your opponent chooses.').
card_ruling('development', '2006-05-01', 'In a multiplayer game, each opponent, in turn, has the option to let you draw a card. If no opponent does, you put a token onto the battlefield. Then the process repeats two more times. A different opponent may let you draw a card each time.').

card_ruling('deviant glee', '2012-10-01', 'Only the controller of the enchanted creature can activate the ability that gives it trample.').

card_ruling('devoted caretaker', '2004-10-04', 'The permanent affected by this ability can\'t be targeted by instant and sorcery spells and (if it\'s a creature) all damage from instant and sorcery spells will be prevented.').

card_ruling('devoted druid', '2008-05-01', 'You put the -1/-1 counter on Devoted Druid as a cost. That means it happens when you activate the ability, not when it resolves. If paying the cost causes the creature to have 0 toughness, it\'s put into the graveyard before you can untap it and before you can even pay the cost again.').

card_ruling('devour flesh', '2013-01-24', 'Devour Flesh targets only a player. It doesn\'t target any creatures. The target player chooses which creature to sacrifice when the spell resolves. That player can sacrifice only a creature he or she controls.').
card_ruling('devour flesh', '2013-01-24', 'The amount of life gained is equal to the creature\'s toughness as it last existed on the battlefield.').

card_ruling('devouring light', '2014-07-18', 'You can tap untapped attacking or blocking creatures you control to help cast Devouring Light. This doesn’t remove those creatures from combat.').
card_ruling('devouring light', '2014-07-18', 'The declare blockers step is the last chance to cast Devouring Light before creatures deal their combat damage. However, a creature remains an attacking or blocking creature during the combat damage step and end of combat step. Devouring Light may be cast targeting such a creature during those steps, after the creature has dealt combat damage.').
card_ruling('devouring light', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('devouring light', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('devouring light', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('devouring light', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('devouring light', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('devouring light', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('devouring light', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('devout chaplain', '2012-05-01', 'You can tap any two untapped Humans you control, including ones you haven\'t controlled continuously since the beginning of your most recent turn, to pay the cost of Devout Chaplain\'s activated ability. You must have controlled Devout Chaplain continuously since the beginning of your most recent turn, however.').

card_ruling('devout harpist', '2005-08-01', 'It can only target \"Enchantment - Aura\" cards, which includes cards changed into Auras, such as Licids.').

card_ruling('devout invocation', '2013-07-01', 'You choose the creatures to tap as Devout Invocation resolves. Once it starts resolving, no player can respond to you tapping the creatures.').

card_ruling('devout lightcaster', '2009-10-01', 'Devout Lightcaster\'s triggered ability isn\'t optional. When it enters the battlefield, if you\'re the only player that controls a black permanent, you\'ll have to target a black permanent you control.').

card_ruling('diabolic intent', '2004-10-04', 'You must choose a card if there are any in your library.').
card_ruling('diabolic intent', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('diabolic intent', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('diabolic servitude', '2004-10-04', 'If the creature is on the battlefield and is returned to its owner\'s hand or is exiled, this card loses track of the creature and has no further effects in the future.').

card_ruling('diaochan, artful beauty', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('dichotomancy', '2007-02-01', 'The cards all enter the battlefield simultaneously. If one of the cards that\'s entering the battlefield is an Aura, it must enter the battlefield attached to a permanent already on the battlefield. It can\'t enter the battlefield attached to another permanent entering the battlefield via Dichotomancy. If an Aura can\'t enter the battlefield this way, it remains in the opponent\'s library.').
card_ruling('dichotomancy', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('dichotomancy', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('dichotomancy', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('dichotomancy', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('dichotomancy', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('dichotomancy', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('dichotomancy', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('dichotomancy', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('dichotomancy', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('dichotomancy', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('dictate of erebos', '2014-04-26', 'When the triggered ability resolves, first the player whose turn it is (if that player is an opponent) chooses which creature he or she will sacrifice, then each other opponent in turn order does the same, then all chosen creatures are sacrificed at the same time.').
card_ruling('dictate of erebos', '2014-04-26', 'None of the opponents are targeted. An opponent with hexproof would still sacrifice a creature.').
card_ruling('dictate of erebos', '2014-04-26', 'If multiple creatures you control die at the same time, Dictate of Erebos’s triggered ability will trigger that many times.').
card_ruling('dictate of erebos', '2014-04-26', 'If you control more than one Dictate of Erebos and a creature you control dies, each of the triggered abilities will trigger. Each opponent will sacrifice a creature each time one of those abilities resolves.').
card_ruling('dictate of erebos', '2014-04-26', 'If you and an opponent each control a Dictate of Erebos and a creature dies, a chain reaction happens. First the ability of one player’s Dictate of Erebos will trigger, causing each opponent to sacrifice a creature. That sacrifice causes the ability of the other player’s Dictate of Erebos to trigger, and so on.').

card_ruling('dictate of the twin gods', '2014-04-26', 'Dictate of the Twin Gods applies to any damage, not just combat damage. It also doesn’t matter who controls the source of the damage that’s being dealt.').
card_ruling('dictate of the twin gods', '2014-04-26', 'The source of the damage doesn’t change. A spell that deals damage will specify the source of the damage, often the spell itself. An ability that deals damage will also specify the source of the damage, although the ability itself will never be that source. Often the source of the ability is also the source of the damage.').
card_ruling('dictate of the twin gods', '2014-04-26', 'If more than one Dictate of the Twin Gods is on the battlefield, damage dealt will double for each one (two of them will end up multiplying the damage by four, three of them by eight, and four of them by sixteen).').
card_ruling('dictate of the twin gods', '2014-04-26', 'If multiple effects modify how damage will be dealt, the player being dealt damage or the controller of the permanent being dealt damage chooses the order to apply the effects. For example, the ability of Decorated Griffin says “Prevent the next 1 combat damage that would be dealt to you this turn.” Suppose you would be dealt 3 combat damage and you activate the ability of Decorated Griffin. You can either (a) prevent 1 damage first and then let Dictate of the Twin Gods’s effect double the remaining 2 damage, for a result of being dealt 4 damage, or (b) double the damage to 6 and then prevent 1 damage, for a result of being dealt 5 damage.').

card_ruling('didgeridoo', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('diffusion sliver', '2014-07-18', 'The granted ability applies to any spell (including Aura spells), activated ability, or triggered ability that’s controlled by an opponent that targets a Sliver you control.').
card_ruling('diffusion sliver', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('diffusion sliver', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('diffusion sliver', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('dig through time', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('dig through time', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('dig through time', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('dig through time', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('dig through time', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('diluvian primordial', '2013-01-24', 'You can choose a number of targets up to the number of opponents you have, one target per opponent.').
card_ruling('diluvian primordial', '2013-01-24', 'You cast the cards one at a time, choosing modes, targets and so on. The last card you cast will be the first one to resolve.').
card_ruling('diluvian primordial', '2013-01-24', 'When casting an instant or sorcery card this way, ignore timing restrictions based on the card\'s type. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('diluvian primordial', '2013-01-24', 'If you can\'t cast one of the target instant or sorcery cards, perhaps because there are no legal targets available, or if you choose not to cast one, it will remain in its owner\'s graveyard.').
card_ruling('diluvian primordial', '2013-01-24', 'If you cast a card “without paying its mana cost,” you can\'t pay alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('diluvian primordial', '2013-01-24', 'If a card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('diluvian primordial', '2013-01-24', 'If an instant or sorcery card you cast this way is countered, it will still be exiled.').
card_ruling('diluvian primordial', '2013-01-24', 'If you cast an instant or sorcery spell with cipher this way, you may exile the card encoded on a creature you control. If you can\'t, or if you choose not to, the card will end up exiled but not encoded on a creature.').
card_ruling('diluvian primordial', '2013-01-24', 'If an instant or sorcery card you cast this way goes to a zone other than exile or a graveyard, perhaps because one of its abilities says to put it into its owner\'s hand, it won\'t be exiled. This is true even if the card would be put into a graveyard later that turn.').

card_ruling('dimensional breach', '2004-10-04', 'Each player chooses which one of their exiled cards to return during each of their upkeeps.').
card_ruling('dimensional breach', '2004-10-04', 'Remember that the returned creature just came under your control, so it can\'t attack until you start a turn with it under your control.').
card_ruling('dimensional breach', '2004-10-04', 'You can\'t pick a card that can\'t legally be returned to the battlefield. If there are no cards that can be legally returned, then you can\'t pick one.').

card_ruling('diminish', '2010-08-15', 'Diminish overwrites all previous effects that set the targeted creature\'s power and toughness to specific values. Other effects that set its power or toughness to specific values that start to apply after Diminish resolves will overwrite this effect.').
card_ruling('diminish', '2010-08-15', 'Effects that modify the targeted creature\'s power or toughness, such as the effects of Giant Growth or Honor of the Pure, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('diminishing returns', '2007-09-16', 'Each player may choose to draw any number of cards from zero to seven.').
card_ruling('diminishing returns', '2007-09-16', 'First the player whose turn it is chooses how many cards to draw, then each other player in turn order chooses. Then the cards are drawn, first by the player whose turn it is, then by each other player in turn order.').
card_ruling('diminishing returns', '2013-07-01', 'This card won\'t be put into your graveyard until after it\'s finished resolving, which means it won\'t be shuffled into your library as part of its own effect.').

card_ruling('dimir aqueduct', '2013-04-15', 'If this land enters the battlefield and you control no other lands, its ability will force you to return it to your hand.').

card_ruling('dimir doppelganger', '2005-10-01', 'This creature becomes an exact copy of a copied card, except that it also has Dimir Doppelganger\'s activated ability. If it becomes a copy of a different creature card, the new copy will overwrite the old copy.').
card_ruling('dimir doppelganger', '2005-10-01', 'A permanent\'s ability that refers to cards the creature exiled (such as Sisters of Stone Death\'s third ability) only affects cards exiled by other abilities intrinsic to that permanent (such as Sisters of Stone Death\'s second ability). Suppose that (a) Dimir Doppelganger copies Arc-Slogger, (b) its \"deal 2 damage\" ability is activated, and then (c) it copies Sisters of Stone Death. Creatures exiled by Arc-Slogger\'s ability can\'t be returned with Sisters of Stone Death\'s ability.').

card_ruling('dimir guildgate', '2013-04-15', 'The subtype Gate has no special rules significance, but other spells and abilities may refer to it.').
card_ruling('dimir guildgate', '2013-04-15', 'Gate is not a basic land type.').

card_ruling('dimir keyrune', '2013-04-15', 'Until the ability that turns the Keyrune into a creature resolves, the Keyrune is colorless.').
card_ruling('dimir keyrune', '2013-04-15', 'Activating the ability that turns the Keyrune into a creature while it\'s already a creature will override any effects that set its power and/or toughness to another number, but effects that modify power and/or toughness without directly setting them will still apply.').

card_ruling('din of the fireherd', '2008-05-01', 'The parts of the spell happen in order. When determining how many black creatures and how many red creatures you control, keep in mind that you just put a creature onto the battlefield that\'s black and red.').
card_ruling('din of the fireherd', '2008-05-01', 'If the targeted opponent becomes an illegal target, the entire spell is countered. You won\'t get a token.').

card_ruling('dinrova horror', '2013-01-24', 'If the target permanent is an illegal target when Dinrova Horror\'s ability tries to resolve, the ability will be countered and none of its effects will happen. No player will discard a card.').
card_ruling('dinrova horror', '2013-01-24', 'If a player has no cards in his or her hand and Dinrova\'s Horror returns a card to that player\'s hand, the player must discard that card. He or she won\'t have the opportunity to cast that card (or do anything else with it) before discarding it.').
card_ruling('dinrova horror', '2013-01-24', 'A token permanent returned to a player\'s hand isn\'t a card and can\'t be discarded. It will cease to exist when state-based actions are performed after the ability finishes resolving.').

card_ruling('diplomacy of the wastes', '2014-11-24', 'If you control a Warrior as Diplomacy of the Wastes resolves, the target opponent will lose 2 life even if that player didn’t discard a card (perhaps because he or she had no cards in hand).').

card_ruling('diplomatic immunity', '2004-10-04', 'Shroud does not apply until after it is on the battlefield, so it can be the target of a spell while it is on the stack.').

card_ruling('dire undercurrents', '2008-05-01', 'An object that\'s both of the listed colors will cause both abilities to trigger. You can put them on the stack in either order.').

card_ruling('dire wolves', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('dire wolves', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('dire wolves', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('dire wolves', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('diregraf escort', '2012-05-01', 'Diregraf Escort can be paired with a Zombie. The protection will have no effect on the pairing.').

card_ruling('dirge of dread', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('dirge of dread', '2008-10-01', 'When you cycle this card, first the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('dirge of dread', '2008-10-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('dirge of dread', '2008-10-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('dirgur nemesis', '2015-02-25', 'If an attacking face-down Dirgur Nemesis is turned face up, it will continue to be attacking even though it will have defender.').
card_ruling('dirgur nemesis', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('dirgur nemesis', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('dirgur nemesis', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('dirtcowl wurm', '2004-10-04', 'A spell or ability that tells a player to \"put a land onto the battlefield\" does not count as playing a land.').

card_ruling('dirty', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('dirty', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('dirty', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('dirty', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('dirty', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('dirty', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('dirty', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('dirty', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('dirty', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('dirty', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('dirty', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('disarm', '2004-12-01', 'The Equipment remains on the battlefield under its controller\'s control, but is no longer attached to that creature.').

card_ruling('disciple of bolas', '2012-07-01', 'You can\'t sacrifice Disciple of Bolas to its own ability, because it says \"another.\"').
card_ruling('disciple of bolas', '2012-07-01', 'You can\'t sacrifice more than one creature this way.').
card_ruling('disciple of bolas', '2012-07-01', 'The ability is mandatory. If you control at least one other creature when the ability resolves, you must sacrifice one. If you don\'t control any other creatures at that time, the ability won\'t do anything.').
card_ruling('disciple of bolas', '2012-07-01', 'The creature\'s power when it was last on the battlefield is the amount of life you gain and the number of cards you draw.').

card_ruling('disciple of deceit', '2014-04-26', 'If there’s an {X} in the mana cost of the card you discarded or the card you wish to search for, X is 0.').
card_ruling('disciple of deceit', '2014-04-26', 'If you discard a split card this way, you may find a card with the same converted mana cost as either half (but not their sum). You may find a split card if either half has the same converted mana cost as the card you discarded.').
card_ruling('disciple of deceit', '2014-04-26', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('disciple of deceit', '2014-04-26', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('disciple of deceit', '2014-04-26', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('disciple of deceit', '2014-04-26', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('disciple of grace', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('disciple of griselbrand', '2011-09-22', 'The amount of life you gain is equal to the toughness of the creature as it last existed on the battlefield, not its toughness in the graveyard.').

card_ruling('disciple of law', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('disciple of malice', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('disciple of phenax', '2013-09-15', 'Use your devotion to black as the triggered ability resolves to determine how many cards will be revealed. If Disciple of Phenax is still on the battlefield at that time, its mana cost will count toward your devotion to black.').
card_ruling('disciple of phenax', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('disciple of phenax', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('disciple of phenax', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('disciple of phenax', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('discordant dirge', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('discordant spirit', '2004-10-04', 'This will not count damage that was prevented.').
card_ruling('discordant spirit', '2006-05-01', 'In Two-Headed Giant, triggers only once per end of turn, not once for each player.').

card_ruling('disdainful stroke', '2014-09-20', 'A face-down spell has converted mana cost 0 and can’t be targeted by Disdainful Stroke.').

card_ruling('disenchant', '2004-10-04', 'This is not modal. If the target changes from an artifact to an enchantment or vice versa, this still destroys it.').

card_ruling('disentomb', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('disharmony', '2004-10-04', 'Disharmony works even if the attacker was not tapped to attack.').
card_ruling('disharmony', '2004-10-04', 'The creature untaps (if it was tapped) before you get control of it.').
card_ruling('disharmony', '2008-10-01', 'The current wording explicitly removes the creature from combat. It is no longer an attacking or blocking creature.').
card_ruling('disharmony', '2008-10-01', 'Disharmony can target a creature you control.').
card_ruling('disharmony', '2009-10-01', 'If you cast Disharmony during an opponent\'s turn, you may then block with the creature you gain control of (assuming there\'s an attacking creature it can block).').
card_ruling('disharmony', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s before the beginning of that phase\'s Declare Blockers Step.').

card_ruling('disintegrate', '2004-10-04', 'The \"can\'t regenerate\" is an effect of Disintegrate and not an effect of the damage. It works even if the damage is prevented or redirected. If redirected, the damage does not take this effect with it.').
card_ruling('disintegrate', '2004-10-04', 'Disintegrated creatures do not go to the graveyard at all before being exiled. They do not trigger abilities which trigger due to a creature going to the graveyard.').

card_ruling('dismantle', '2004-12-01', 'Dismantle targets only the artifact that will be destroyed. When Dismantle resolves, you choose which type of counters you want and choose an artifact you control to put them on.').
card_ruling('dismantle', '2004-12-01', 'It doesn\'t matter what kind of counters the destroyed artifact had on it, only how many. If an artifact had five bomb counters and two trap counters on it, that converts to seven +1/+1 counters or seven charge counters.').
card_ruling('dismantle', '2004-12-01', 'You can put +1/+1 counters on a noncreature artifact. They won\'t do anything unless the artifact becomes a creature, at which time they\'ll add to the creature\'s power and toughness.').
card_ruling('dismantle', '2013-07-01', 'If the artifact regenerates or has indestructible, it won\'t be destroyed, but you still put the appropriate number of counters onto an artifact you control.').

card_ruling('dismantling blow', '2004-10-04', 'If the target is not legal on resolution, the spell is countered and you do not get to draw the two cards.').

card_ruling('dismember', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('dismember', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('dismember', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('dismember', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('dismember', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('disorder', '2004-10-04', 'Players take 0 or 2 damage. Disorder does not do 2 damage per creature to the player.').

card_ruling('disorient', '2009-10-01', 'A creature with power 0 or less assigns no combat damage. (It doesn\'t assign a negative amount of combat damage.)').

card_ruling('disowned ancestor', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('disowned ancestor', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('dispatch', '2011-06-01', 'If you control three or more artifacts when Dispatch resolves, you\'ll tap the creature, then exile it.').

card_ruling('dispeller\'s capsule', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('dispense justice', '2011-01-01', 'The targeted player chooses which attacking creature(s) to sacrifice as Dispense Justice resolves.').

card_ruling('displacement wave', '2015-06-22', 'If a permanent has an {X} in its mana cost, that X is 0.').
card_ruling('displacement wave', '2015-06-22', 'A token has converted mana cost 0 unless it’s a copy of something else, in which case it has the mana cost of whatever it’s copying.').

card_ruling('display of dominance', '2015-02-25', 'If you choose the second mode, and if a permanent you control is being targeted by a spell when Display of Dominance resolves, nothing happens right away. When that spell would resolve, its color is checked. If it’s blue or black, that permanent will be an illegal target for that spell and won’t be affected by it. If all that spell’s targets have become illegal by the time it would resolve, it’s countered.').
card_ruling('display of dominance', '2015-02-25', 'If you choose the second mode, no new blue or black spell may be cast by an opponent that turn targeting a permanent you control after Display of Dominance resolves.').
card_ruling('display of dominance', '2015-02-25', 'If you choose the second mode, Display of Dominance will affect any permanent you happen to control at any point during the rest of the turn, not just permanents you control as it resolves. That’s because it doesn’t grant an ability to those permanents; rather, it affects the game rules and states something that’s now true about those permanents.').
card_ruling('display of dominance', '2015-02-25', 'Keep in mind that an Aura spell targets the permanent it will enchant (but an Aura on the battlefield doesn’t target the permanent it’s attached to).').
card_ruling('display of dominance', '2015-02-25', 'Permanents you control may be the targets of abilities from blue or black sources controlled by your opponents.').

card_ruling('disrupt', '2008-04-01', 'You draw a card regardless of whether the targeted spell was countered or {1} was paid.').

card_ruling('disrupting scepter', '2004-10-04', 'You can use it on yourself.').

card_ruling('disrupting shoal', '2005-02-01', 'Disrupting Shoal can target any spell, but does nothing unless that spell\'s converted mana cost is X.').

card_ruling('disruption aura', '2004-12-01', 'Mana cost includes any requirements for colored mana, although most artifacts don\'t have any color requirements.').
card_ruling('disruption aura', '2004-12-01', 'If the enchanted artifact has X in its mana cost, X is 0.').

card_ruling('dissipate', '2004-10-04', 'The card does not go to the graveyard before being exiled.').
card_ruling('dissipate', '2004-10-04', 'If the spell is not countered (because the spell it targets can\'t be countered), then it does not get exiled.').

card_ruling('dissipation field', '2011-01-01', 'The ability triggers whenever you\'re dealt any damage (not just combat damage) by any permanent (not just creatures). This includes permanents you control.').

card_ruling('dissolve', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('dissolve', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('dissolve', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('dissolve', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('distant melody', '2008-04-01', 'Note that this spell counts the number of permanents you control of the chosen type, not just the number of creatures you control. It will also take your tribal permanents into account.').

card_ruling('distant memories', '2011-06-01', 'The card is exiled face up. Each opponent will know what the card is when deciding whether to have you put it in your hand.').
card_ruling('distant memories', '2011-06-01', 'If no opponent has you put the card into your hand, the card remains exiled.').
card_ruling('distant memories', '2011-06-01', 'In multiplayer formats, your opponents each make the decision in turn order.').

card_ruling('distortion strike', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('distortion strike', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('distortion strike', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('distortion strike', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('distortion strike', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('distortion strike', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('distortion strike', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('distortion strike', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('distortion strike', '2010-06-15', 'Distortion Strike doesn\'t grant an ability to the targeted creature. Rather, it affects the game rules and states something that\'s now true about that creature. The creature can\'t be blocked even if it loses all abilities.').

card_ruling('distress', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('divine congregation', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('divine congregation', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('divine congregation', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('divine congregation', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('divine congregation', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('divine congregation', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('divine congregation', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('divine congregation', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('divine congregation', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('divine congregation', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('divine deflection', '2012-05-01', 'Divine Deflection\'s only target is the creature or player it may deal damage to. You choose that target as you cast Divine Deflection, not at the time it prevents damage.').
card_ruling('divine deflection', '2012-05-01', 'If the targeted creature or player is an illegal target by the time Divine Deflection resolves, the entire spell is countered. No damage will be prevented.').
card_ruling('divine deflection', '2012-05-01', 'Divine Deflection can prevent damage that would be dealt to you, one or more creatures you control, and/or one or more planeswalkers you control.').
card_ruling('divine deflection', '2012-05-01', 'Divine Deflection can prevent damage from multiple sources dealing damage simultaneously (such as during combat). If the chosen value for X won\'t prevent all the damage, you choose which sources to prevent damage from.').
card_ruling('divine deflection', '2012-05-01', 'If damage is dealt to multiple permanents you control, or is dealt to you and at least one permanent you control, you choose which of that damage to prevent if the chosen value for X won\'t prevent all the damage. For example, if 3 damage would be dealt to you and to each of two creatures you control, and Divine Deflection will prevent the next 3 damage, you might choose to prevent the next 2 damage it would deal to you and the next 1 damage it would deal to one of the creatures, among other choices. You don\'t decide until the point at which the damage would be dealt.').
card_ruling('divine deflection', '2012-05-01', 'Divine Deflection\'s effect is not a redirection effect. If it prevents damage, Divine Deflection (not the original source) deals damage to the targeted creature or player as part of that prevention effect. Divine Deflection is the source of the new damage, so the characteristics of the original source (such as its color, or whether it had lifelink or deathtouch) don\'t affect this damage. The new damage is not combat damage, even if the prevented damage was. Since you control the source of the new damage, if you targeted an opponent with Divine Deflection, you may have Divine Deflection deal its damage to a planeswalker that opponent controls.').
card_ruling('divine deflection', '2012-05-01', 'If Divine Deflection prevents damage, excess damage (if any) dealt by that source is dealt at the same time. Immediately afterward, as part of that same prevention effect, Divine Deflection deals its damage. This happens before state-based actions are checked, and before the spell or ability that caused damage to be dealt resumes its resolution.').
card_ruling('divine deflection', '2012-05-01', 'Whether the targeted creature or player is still a legal target is not checked after Divine Deflection resolves. For example, if a creature targeted by Divine Deflection gains shroud after Divine Deflection resolves, Divine Deflection can still deal damage to that creature.').
card_ruling('divine deflection', '2012-05-01', 'If Divine Deflection can\'t deal damage to the targeted creature or player (because the creature has gained protection from white, is no longer on the battlefield, or is no longer a creature, or the player is no longer in the game, for example), it will still prevent damage. It just won\'t deal any damage itself.').

card_ruling('divine favor', '2014-07-18', 'If the creature targeted by Divine Favor is an illegal target when Divine Favor tries to resolve, Divine Favor will be countered. It won\'t enter the battlefield and its enters-the-battlefield ability won\'t trigger.').

card_ruling('divine intervention', '2009-10-01', 'When Divine Intervention\'s third ability resolves, the game ends immediately. The game is a draw, meaning neither player wins and neither player loses.').
card_ruling('divine intervention', '2009-10-01', 'Divine Intervention\'s third ability triggers only if its controller removes the last intervention counter from it. It doesn\'t matter how that happens. For example, if you control Divine Intervention and the last intervention counter is removed as a result of a Clockspinning you control, the ability will trigger. On the other hand, if the last intervention counter is removed as a result of a Clockspinning another player controls, the ability won\'t trigger (and won\'t ever be able to).').
card_ruling('divine intervention', '2009-10-01', 'In a multiplayer game played with the limited range of influence option, Divine Intervention won\'t necessarily end the entire game when its third ability resolves. All players within range of Divine Intervention will leave the game. They\'ll neither win nor lose; as far as they\'re concerned, the result of the game is a draw. All other players will continue playing.').

card_ruling('divine offering', '2004-10-04', 'If the target artifact becomes illegal before resolution, you do not gain any life.').
card_ruling('divine offering', '2013-07-01', 'If Divine Offering resolves, but the artifact regenerates or has indestructible, you\'ll still gain the life.').

card_ruling('divine presence', '2004-10-04', 'This is not preventing damage since it does not use the word \"prevent\". It is a replacement.').

card_ruling('divine reckoning', '2011-09-22', 'Starting with the player whose turn it is, each player chooses a creature in turn order. Players will know the choice of each player who chose before them.').

card_ruling('divine verdict', '2009-10-01', 'An \"attacking creature\" is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat.').
card_ruling('divine verdict', '2009-10-01', 'A \"blocking creature\" is one that has been declared as a blocker this combat, or one that was put onto the battlefield blocking this combat. Unless that creature leaves combat, it continues to be a blocking creature through the end of combat step, even if the creature or creatures it was blocking are no longer on the battlefield or have otherwise left combat.').
card_ruling('divine verdict', '2012-07-01', 'Destroying a blocking creature won\'t cause any of the creatures it was blocking to become unblocked. They won\'t deal combat damage to the defending player or planeswalker (unless they have trample).').

card_ruling('diviner spirit', '2013-10-17', 'If Diviner Spirit deals combat damage to its controller (perhaps because damage was redirected), that player will draw twice that number of cards.').

card_ruling('diviner\'s wand', '2008-04-01', 'Each of these Equipment has two subtypes listed on its type line. The first one is a creature type, which in this case is also a subtype of tribal. The second one is Equipment, which is a subtype of artifact.').
card_ruling('diviner\'s wand', '2008-04-01', 'Each of these Equipment has a triggered ability that says \"Whenever a [creature type] creature enters the battlefield, you may attach [this Equipment] to it.\" This triggers whenever any creature of the specified creature type enters the battlefield, no matter who controls it. You may attach your Equipment to another player\'s creature this way, even though you can\'t do so with the equip ability.').
card_ruling('diviner\'s wand', '2008-04-01', 'If you attach an Equipment you control to another player\'s creature, you retain control of the Equipment, but you don\'t control the creature. Only you can activate the Equipment\'s equip ability, and if the Equipment\'s ability triggers again, you choose whether to move the Equipment. Only the creature\'s controller can activate any activated abilities the Equipment grants to the creature, and \"you\" in any abilities granted to the creature refers to that player.').

card_ruling('divining witch', '2004-10-04', 'You name a card during resolution of the ability.').

card_ruling('dizzying gaze', '2004-10-04', 'Dizzying Gaze can\'t be cast targeting an opponent\'s creature. And this card is put into the graveyard if ever on a creature you do not control.').

card_ruling('djinn illuminatus', '2006-02-01', 'If you cast a spell that already has replicate while you have this creature on the battlefield, the spell will have two replicate abilities: one with a replicate cost as printed on it, and one with a replicate cost equal to its mana cost. (In this set, both costs are always the same.) You may use either ability, or both abilities, to replicate the spell.').
card_ruling('djinn illuminatus', '2006-02-01', 'If you cast an instant or sorcery spell that has {X} in its mana cost, the value of X in the spell\'s replicate cost will be the same as the value of X you chose for its mana cost.').

card_ruling('djinn of infinite deceits', '2013-10-17', 'You don’t have to control either target.').
card_ruling('djinn of infinite deceits', '2013-10-17', 'If the same player controls both creatures when the ability resolves, nothing happens.').
card_ruling('djinn of infinite deceits', '2013-10-17', 'If one of the creatures is an illegal target when the ability resolves, the exchange won’t happen. If both creatures are illegal targets, the ability will be countered.').

card_ruling('djinn of wishes', '2009-10-01', 'You play the revealed card as part of the resolution of Djinn of Wishes\'s ability. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other play restrictions are not (such as \"Cast [this card] only during combat\").').
card_ruling('djinn of wishes', '2009-10-01', 'If the revealed card is a spell, you may cast it only if you can choose legal targets for it. If you cast it, it\'s put on the stack, then Djinn of Wishes\'s ability finishes resolving. The spell will then resolve as normal, after players get a chance to cast spells and activate abilities.').
card_ruling('djinn of wishes', '2009-10-01', 'If the revealed card is a land, you can play it only if it\'s your turn and you haven\'t yet played a land this turn.').
card_ruling('djinn of wishes', '2009-10-01', 'If you play a card \"without paying its mana cost,\" you can\'t pay any alternative costs. You can pay additional costs, such as conspire costs and kicker costs.').

card_ruling('domestication', '2010-06-15', 'Domestication\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability triggers only if the enchanted creature\'s power is 4 or greater as your end step begins, and (2) the ability does nothing if the enchanted creature\'s power is 3 or less by the time it resolves.').
card_ruling('domestication', '2010-06-15', 'Domestication can target, and can enchant, a creature with power 4 or greater. The enchanted creature\'s power is checked only when the triggered ability triggers and resolves.').
card_ruling('domestication', '2010-06-15', 'If an effect changes the enchanted creature\'s power until end of turn, it will still have the modified power during your end step.').

card_ruling('dominating licid', '2004-10-04', 'It becomes an Enchantment - Aura and stops being all other permanent types.').
card_ruling('dominating licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('dominating licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('dominator drone', '2015-08-25', 'Dominator Drone’s last ability checks to see if you control another colorless creature at the moment it enters the battlefield. If you don’t, the ability won’t trigger at all. If you do, the ability will check again as it tries to resolve. If you no longer control another colorless creature, the ability will be removed from the stack and have no effect.').
card_ruling('dominator drone', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('dominator drone', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('dominator drone', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('dominator drone', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('dominator drone', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('dominator drone', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('dominator drone', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('domineer', '2004-12-01', 'The enchanted permanent must be both an artifact and a creature. Domineer can be cast only on an artifact creature. Domineer \"falls off\" and is put into the graveyard if the artifact creature it\'s enchanting stops being an artifact or stops being a creature.').

card_ruling('domineering will', '2014-11-07', 'The controller of the creatures still chooses which attacking creature each of the three target creatures blocks.').
card_ruling('domineering will', '2014-11-07', 'Each of those creatures blocks only if it’s able to do so as the declare blockers step begins. If, at that time, one of the creatures is tapped, is affected by a spell or ability that says it can’t block, or no creatures are attacking that creature’s controller or a planeswalker controlled by that player, then it doesn’t block. If there’s a cost associated with having the creature block, the player isn’t forced to pay that cost, so it doesn’t block in that case either.').
card_ruling('domineering will', '2014-11-07', 'Notably, Domineering Will doesn’t give any of the target creatures haste. Unless they have haste due to some other means, they won’t be able to attack that turn.').

card_ruling('dominus of fealty', '2008-08-01', 'If you already control the targeted permanent when the ability resolves, you still need to choose whether or not to apply the effect. If you choose to apply it, the permanent untaps and gains haste. If you don\'t, it doesn\'t.').

card_ruling('domri rade', '2013-01-24', 'When resolving Domri\'s first ability, if the card you look at isn\'t a creature card, or it it\'s a creature card you don\'t want to put into your hand, you simply put it back on top of your library.').
card_ruling('domri rade', '2013-01-24', 'The second target of the second ability can be another creature you control, but it can\'t be the same creature as the first target.').
card_ruling('domri rade', '2013-01-24', 'If either target of the second ability is an illegal target when the ability tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('domri rade', '2013-01-24', 'Domri\'s emblem doesn\'t remove any other abilities of creatures you control.').
card_ruling('domri rade', '2013-04-15', 'If a creature has both double strike and trample, the combat damage it assigned during the first combat damage step will be considered when determining how much damage can trample through in the second combat damage step. If the blocker is destroyed during the first combat damage step, then all of the damage must be assigned to the defending player during the second combat damage step.').
card_ruling('domri rade', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('domri rade', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('domri rade', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('domri rade', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('domri rade', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('domri rade', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('domri rade', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('domri rade', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('domri rade', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('domri rade', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('domri rade', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('donate', '2004-10-04', 'If you Donate an Aura, it stays where it is, but the targeted player now controls it.').

card_ruling('doomgape', '2008-08-01', 'If you have no other creatures to sacrifice, you\'ll have to sacrifice Doomgape itself. You\'ll gain life equal to its toughness.').

card_ruling('doomsday', '2004-10-04', 'You lose the life during resolution.').
card_ruling('doomsday', '2008-04-01', 'If your graveyard and library combined contain five or more cards, you must choose five cards from among them. You can\'t choose to find fewer than that.').
card_ruling('doomsday', '2008-04-01', 'If your graveyard and library combined contain fewer than five cards, all of those cards will wind up in your library.').

card_ruling('doomsday specter', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('doomwake giant', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('doomwake giant', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('doomwake giant', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('door of destinies', '2008-04-01', 'If you cast a creature spell of the chosen type, Door of Destinies will get a charge counter before the creature enters the battlefield. The creature will enter the battlefield with the additional boost to its power and toughness.').
card_ruling('door of destinies', '2013-07-01', 'Creature types, such as Human or Sliver, appear after the dash on the type line of creatures. Notably, artifact is not a creature type. A creature may have more than one creature type, such as Goblin Warrior. Such a creature would benefit from Door of Destinies if the chosen creature type was Goblin or Warrior.').

card_ruling('doorkeeper', '2012-10-01', 'The number of creatures with defender you control is counted when the ability resolves.').

card_ruling('doran, the siege tower', '2007-10-01', 'Doran\'s ability means, for example, that a 2/3 creature will assign 3 damage in combat instead of 2.').
card_ruling('doran, the siege tower', '2007-10-01', 'Doran\'s ability doesn\'t actually change creatures\' power; it changes only the value of the combat damage they assign. All other rules and effects that check power or toughness use the real values.').
card_ruling('doran, the siege tower', '2007-10-01', 'This effect is mandatory and affects all creatures.').

card_ruling('dormant gomazoa', '2010-06-15', 'Dormant Gomazoa\'s last ability triggers when you become the target of any spell, including one you control. It doesn\'t matter who else or what else the spell targets.').
card_ruling('dormant gomazoa', '2010-06-15', 'A spell can target you only if it specifically says the word \"target.\" One that says \"You draw two cards,\" for example, doesn\'t target you.').
card_ruling('dormant gomazoa', '2010-06-15', 'You become the target of a spell when a spell is cast targeting you, when a spell has one of its targets changed to you due to a spell or ability like Deflection, when a spell that\'s targeting you is copied and that target isn\'t changed, or when a spell is copied and one of its targets is changed to you.').
card_ruling('dormant gomazoa', '2010-06-15', 'Dormant Gomazoa\'s last ability won\'t trigger when you become the target of an ability.').
card_ruling('dormant gomazoa', '2010-06-15', 'If you become the target of a spell, Dormant Gomazoa\'s last ability triggers and is put on the stack on top of that spell. Dormant Gomazoa\'s last ability will resolve (allowing you to untap it) first.').

card_ruling('dormant sliver', '2007-02-01', 'The controller of the Sliver that enters the battlefield, not the controller of Dormant Sliver, draws a card.').
card_ruling('dormant sliver', '2007-02-01', 'The ability triggers when Dormant Sliver enters the battlefield.').
card_ruling('dormant sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('dormant sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('dormant volcano', '2006-02-01', 'This has a triggered ability when it enters the battlefield, not a replacement effect, as previously worded.').

card_ruling('dosan the falling leaf', '2004-12-01', 'Dosan\'s ability only stops players from casting spells. It doesn\'t stop activated or triggered abilities.').

card_ruling('double negative', '2009-05-01', 'You may cast Double Negative with no targets, one target, or two targets.').
card_ruling('double negative', '2009-05-01', 'In order to counter two target spells, both of those spells must be on the stack at the time you cast Double Negative. Two spells can be on the stack at the same time as the result of an ability (such as cascade or replicate), as the result of a copy effect (such as the one from Cloven Casting or Twincast), or because a spell has been cast in response to another spell.').
card_ruling('double negative', '2009-05-01', 'It\'s unusual for a player to cast a spell in response to his or her own spell, and that will be specifically called out when it happens. Under normal circumstances, if a player casts a spell, it is assumed that the player passes priority afterward. If that player casts another spell, it is assumed that each other player has also passed priority, which caused the first spell to resolve and the first player to receive priority again. Unless the first player says otherwise, only one spell will be on the stack at a time in that scenario.').

card_ruling('double stroke', '2014-05-29', 'You must turn Double Stroke face up before you cast the spell with the chosen name to have Double Stroke’s second ability trigger.').
card_ruling('double stroke', '2014-05-29', 'When the ability resolves, it creates a copy of the spell. You control the copy. That copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('double stroke', '2014-05-29', 'The copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('double stroke', '2014-05-29', 'If the spell being copied is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('double stroke', '2014-05-29', 'If the spell being copied has an X whose value was determined as it was cast (like Skeletal Scrying has), the copy will have the same value of X.').
card_ruling('double stroke', '2014-05-29', 'Effects based on any additional costs that were paid for the original spell will be copied as though those same costs were paid for the copy. For example, if a player exiles three cards from his or her graveyard to cast Skeletal Scrying, and you copy it, the copy will also cause you to draw three cards and lose 3 life (but you won’t exile any cards from your graveyard).').
card_ruling('double stroke', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('double stroke', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('double stroke', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('double stroke', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('double stroke', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('double stroke', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('double stroke', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').
card_ruling('double stroke', '2014-05-29', 'You name the card as the game begins, as you put the conspiracy into the command zone, not as you turn the face-down conspiracy face up.').
card_ruling('double stroke', '2014-05-29', 'There are several ways to secretly name a card, including writing the name on a piece of paper that’s kept with the face-down conspiracy. If you have multiple face-down conspiracies, you may name a different card for each one. It’s important that each named card is clearly associated with only one of the conspiracies.').
card_ruling('double stroke', '2014-05-29', 'You must name a Magic card. Notably, you can’t name a token (except in the unusual case that a token’s name matches the name of a card, such as Illusion).').
card_ruling('double stroke', '2014-05-29', 'If you play multiple games after the draft, you can name a different card in each new game.').
card_ruling('double stroke', '2014-05-29', 'As a special action, you may turn a face-down conspiracy face up. You may do so any time you have priority. This action doesn’t use the stack and can’t be responded to. Once face up, the named card is revealed and the conspiracy’s abilities will affect the game.').
card_ruling('double stroke', '2014-05-29', 'At the end of the game, you must reveal any face-down conspiracies you own in the command zone to all players.').

card_ruling('doubling chant', '2011-09-22', 'The cards you find in your library must be creature cards. For example, you can\'t put a land card onto the battlefield even if it has the same name as a land you control that\'s also a creature (perhaps because of an ability of that land).').
card_ruling('doubling chant', '2011-09-22', 'If you control a creature whose name has been changed by an effect (for example, a Clone that\'s copying another creature), you\'ll search for a card with the new name, not one with the original name.').
card_ruling('doubling chant', '2011-09-22', 'If you control no creatures when Doubling Chant resolves, you may still search your library and you must shuffle your library.').

card_ruling('doubling cube', '2004-12-01', 'Doubling Cube\'s ability is a mana ability.').
card_ruling('doubling cube', '2004-12-01', 'The \"type\" of mana is its color, or lack thereof (if the mana is colorless).').
card_ruling('doubling cube', '2004-12-01', 'Any restrictions on the mana in your mana pool aren\'t copied. For example, if you have {W}{W}{B}{5} with no restrictions on it in your mana pool and {U}{U}{U} that can be used only to cast artifact spells, you\'ll end up with {W}{W}{W}{W}{B}{B}{10}, {U}{U}{U} that can be used only to cast artifact spells, and {U}{U}{U} that can be used for anything.').

card_ruling('doubling season', '2005-10-01', 'The tokens and counters that Doubling Season creates are exact copies of the kind of tokens and counters that were being created in the first place.').
card_ruling('doubling season', '2005-10-01', 'Doubling Season affects cards that \"enter the battlefield with\" a certain number of counters. Triskelion, for example, would enter the battlefield with six +1/+1 counters on it rather than three.').
card_ruling('doubling season', '2005-10-01', 'If there are two Doubling Seasons on the battlefield, then the number of tokens or counters is four times the original number. If there are three on the battlefield, then the number of tokens or counters is eight times the original number, and so on.').
card_ruling('doubling season', '2005-10-01', 'Planeswalkers will enter the battlefield with double the normal amount of loyalty counters. However, if you activate an ability whose cost has you put loyalty counters on a planeswalker, the number you put on isn\'t doubled. This is because those counters are put on as a cost, not as an effect.').

card_ruling('dovescape', '2006-05-01', 'The Bird tokens are created even if the spell isn\'t countered by Dovescape\'s ability. This may happen because the spell can\'t be countered, because it\'s already been countered, or because it\'s otherwise been removed from the stack by the time Dovescape\'s ability resolves.').
card_ruling('dovescape', '2006-05-01', 'If a split card is cast, Dovescape makes tokens equal to the converted mana cost of the half that was cast.').

card_ruling('down', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('down', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('down', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('down', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('down', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('down', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('down', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('down', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('down', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('down', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('down', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('downpour', '2012-07-01', 'If Downpour targets more than one creature, and some but not all of the creatures are illegal targets by the time Downpour resolves, the remaining legal targets will be tapped.').

card_ruling('downsize', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('downsize', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('downsize', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('downsize', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('downsize', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('downsize', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('downsize', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('draco', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('draco', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('draco', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('draconian cylix', '2004-10-04', 'The card is discarded from your hand during announcement and as a cost.').

card_ruling('draconic roar', '2015-02-25', 'Draconic Roar targets only the creature, not its controller. If that creature becomes an illegal target before Draconic Roar resolves, the spell is countered and none of its effects will occur. No damage is dealt.').
card_ruling('draconic roar', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('draconic roar', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('draconic roar', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('drag down', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('drag down', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('drag down', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('dragon appeasement', '2009-05-01', 'Dragon Appeasement itself doesn\'t allow you to sacrifice any creatures. Its last ability triggers whenever you sacrifice a creature because some other spell, ability, or cost instructed you to.').

card_ruling('dragon arch', '2004-10-04', 'A multicolored card has more than one color in its mana cost.').
card_ruling('dragon arch', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('dragon bell monk', '2014-11-24', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('dragon bell monk', '2014-11-24', 'Prowess triggers only once for any spell, even if that spell has multiple types.').
card_ruling('dragon bell monk', '2014-11-24', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('dragon bell monk', '2014-11-24', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('dragon broodmother', '2009-05-01', 'Putting a token creature with devour onto the battlefield works the same as putting any other creature with devour onto the battlefield. The token may devour Dragon Broodmother itself!').
card_ruling('dragon broodmother', '2009-05-01', 'In a Two-Headed Giant game, Dragon Broodmother\'s second ability triggers only once at the beginning of each team\'s upkeep, not once for each player. You\'ll get one Dragon token at the beginning of your team\'s upkeep and one Dragon token at the beginning of the opposing team\'s upkeep.').

card_ruling('dragon grip', '2014-09-20', 'If you’re using Dragon Grip’s ferocious ability to cast it as though it had flash, you must control a creature with power 4 or greater only as you begin the casting process and put Dragon Grip on the stack. Losing control of that creature while casting Dragon Grip (perhaps because you sacrificed it to activate a mana ability), or in response to Dragon Grip won’t affect Dragon Grip on the stack.').
card_ruling('dragon grip', '2014-09-20', 'You must cast Dragon Grip at the latest during the declare blockers step after blockers are declared if you want first strike to matter during that combat.').
card_ruling('dragon grip', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('dragon grip', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('dragon mage', '2004-10-04', 'All players discard, not just the player that was damaged.').

card_ruling('dragon mantle', '2013-09-15', 'If the target of an Aura is illegal when it tries to resolve, the Aura will be countered. The Aura doesn’t enter the battlefield, so you won’t get to draw a card.').

card_ruling('dragon tempest', '2015-02-25', 'Use the number of Dragons you control as the last ability resolves to determine the value of X.').
card_ruling('dragon tempest', '2015-02-25', 'The two abilities aren’t mutually exclusive. If a Dragon with flying (which is most of them) enters the battlefield under your control, both abilities will trigger.').

card_ruling('dragon throne of tarkir', '2014-09-20', 'The value of X is determined as the activated ability resolves. If the equipped creature isn’t on the battlefield at that time, use its power the last time it was on the battlefield to determine the value of X. If that power was negative, other creatures you control will lose power and toughness. For example, if the equipped creature was 2/2 and a spell gave it -5/-5 in response to Dragon Throne of Tarkir’s activated ability, other creatures you control would get -3/-3 until end of turn.').

card_ruling('dragon whelp', '2009-10-01', 'The first three times Dragon Whelp\'s ability is activated in a turn will do nothing other than give it +1/+0 until end of turn. Any subsequent activations will also cause its delayed triggered ability to trigger. That means if you activate Dragon Whelp\'s ability five times, for example, two different delayed triggered abilities will trigger at the beginning of the next end step and cause you to sacrifice Dragon Whelp.').
card_ruling('dragon whelp', '2009-10-01', 'If the fourth (or more) time Dragon Whelp\'s ability is activated during the same turn happens to be during that turn\'s end step, the delayed triggered ability won\'t trigger until the beginning of the next turn\'s end step. You\'ll have to sacrifice Dragon Whelp at that time.').

card_ruling('dragon whisperer', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('dragon whisperer', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('dragon whisperer', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('dragon wings', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('dragon\'s claw', '2009-10-01', 'The ability triggers whenever any player, not just you, casts a red spell.').
card_ruling('dragon\'s claw', '2009-10-01', 'If a player casts a red spell, Dragon\'s Claw\'s ability triggers and is put on the stack on top of that spell. Dragon\'s Claw\'s ability will resolve (causing you to gain 1 life) before the spell does.').

card_ruling('dragon\'s eye savants', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('dragon\'s eye savants', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('dragon\'s herald', '2008-10-01', 'To activate the ability, you must sacrifice three different creatures. You can\'t sacrifice just one three-colored creature, for example.').
card_ruling('dragon\'s herald', '2008-10-01', 'You may sacrifice multicolored creatures as part of the cost of the search ability. If you sacrifice a multicolored creature this way, specify which part of the cost that creature is satisfying.').
card_ruling('dragon\'s herald', '2008-10-01', 'You may sacrifice the Herald itself to help pay for the cost of its ability.').

card_ruling('dragon-scarred bear', '2015-02-25', 'If you control a creature with power less than 0, use its actual power when calculating the total power of creatures you control. For example, if you control three creatures with powers 4, 5, and -2, the total power of creatures you control is 7.').
card_ruling('dragon-scarred bear', '2015-02-25', 'Some formidable abilities are activated abilities that require creatures you control to have total power 8 or greater. Once you activate these abilities, it doesn’t matter what happens to the total power of creatures you control.').
card_ruling('dragon-scarred bear', '2015-02-25', 'Other formidable abilities are triggered abilities with an “intervening ‘if’” clause. Such abilities check the total power of creatures you control twice: once at the appropriate time to see if the ability will trigger, and again as the ability tries to resolve. If, at that time, the total power of creatures you control is no longer 8 or greater, the ability will have no effect.').

card_ruling('dragon-style twins', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('dragon-style twins', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('dragon-style twins', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('dragonlair spider', '2012-06-01', 'The triggered ability will resolve and the Insect will be created before the spell that caused it to trigger resolves.').
card_ruling('dragonlair spider', '2012-06-01', 'The triggered ability won\'t be affected if the spell that caused it to trigger is countered.').

card_ruling('dragonlord atarka', '2015-02-25', 'You choose how many targets the ability has and how the damage is divided as you put the ability on the stack. Each target must receive at least 1 damage.').
card_ruling('dragonlord atarka', '2015-02-25', 'If some of the targets are illegal targets as the ability tries to resolve, the original division of damage still applies but no damage is dealt to the illegal targets. If all targets are illegal, the ability is countered.').

card_ruling('dragonlord kolaghan', '2015-02-25', 'Casting a spell face down won’t cause Dragonlord Kolaghan’s last ability to trigger. A spell with no name can’t have the same name as any other card.').
card_ruling('dragonlord kolaghan', '2015-02-25', 'If an opponent casts a creature spell with delve and exiles all cards from his or her graveyard with the same name to help pay its cost, Dragonlord Kolaghan’s ability won’t trigger.').

card_ruling('dragonlord silumgar', '2015-02-25', 'If Dragonlord Silumgar leaves the battlefield, you no longer control it, and its control-change effect ends.').
card_ruling('dragonlord silumgar', '2015-02-25', 'If Dragonlord Silumgar ceases to be under your control before its ability resolves, you won’t gain control of the creature or planeswalker at all.').
card_ruling('dragonlord silumgar', '2015-02-25', 'If another player gains control of Dragonlord Silumgar, its control-change effect ends. Regaining control of Dragonlord Silumgar won’t cause you to regain control of the creature or planeswalker.').

card_ruling('dragonlord\'s prerogative', '2015-02-25', 'If one of these spells is copied, the controller of the copy will get the “Dragon bonus” only if a Dragon card was revealed as an additional cost. The copy wasn’t cast, so whether you controlled a Dragon won’t matter.').
card_ruling('dragonlord\'s prerogative', '2015-02-25', 'You can’t reveal more than one Dragon card to multiply the bonus. There is also no additional benefit for both revealing a Dragon card as an additional cost and controlling a Dragon as you cast the spell.').
card_ruling('dragonlord\'s prerogative', '2015-02-25', 'If you don’t reveal a Dragon card from your hand, you must control a Dragon as you are finished casting the spell to get the bonus. For example, if you lose control of your only Dragon while casting the spell (because, for example, you sacrificed it to activate a mana ability), you won’t get the bonus.').

card_ruling('dragonrage', '2014-11-24', 'Even though it can generate mana, Dragonrage isn’t a mana ability. It uses the stack and can be responded to.').
card_ruling('dragonrage', '2014-11-24', 'Only creatures that are attacking as Dragonrage resolves will count toward how much mana is generated, and only those creatures will gain the “firebreathing” activated ability. In other words, casting Dragonrage before you’ve declared attackers usually won’t do anything.').
card_ruling('dragonrage', '2014-11-24', 'Remember that unused mana empties from players’ mana pools at the end of each step and phase. For example, if you cast Dragonrage during your declare attackers step, except in very rare cases, the mana Dragonrage generates won’t last until the declare blockers step or the postcombat main phase.').

card_ruling('dragonscale boon', '2014-09-20', 'Dragonscale Boon can target any creature, including one that’s already untapped.').

card_ruling('dragonscale general', '2014-11-24', 'Count the number of tapped creatures you control as the ability resolves to determine the value of X.').
card_ruling('dragonscale general', '2014-11-24', 'The creature you put the counters on doesn’t have to be one of the tapped creatures.').
card_ruling('dragonscale general', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('dragonscale general', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('dragonshift', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('dragonshift', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('dragonshift', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('dragonshift', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('dragonshift', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('dragonshift', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('dragonshift', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').
card_ruling('dragonshift', '2013-04-15', 'If you cast Dragonshift using overload, only creatures you control when it resolves will be affected. Creatures that come under your control later in the turn will not.').
card_ruling('dragonshift', '2013-04-15', 'Each affected creature will lose all other colors and creature types and be only red, blue, and a Dragon. Each will retain any other types it may have had, such as artifact.').
card_ruling('dragonshift', '2013-04-15', 'Each affected creature will lose any abilities it may have gained prior to Dragonshift resolving. Notably, this includes the ability to cast the copies of a card with cipher that\'s encoded on the creature, although that ability will return after the turn ends.').
card_ruling('dragonshift', '2013-04-15', 'If any of the affected creatures gains an ability after Dragonshift resolves, it will keep that ability.').
card_ruling('dragonshift', '2013-04-15', 'Dragonshift overwrites all previous effects that set a creature\'s power or toughness to specific values. However, effects that set a creature\'s power or toughness to specific values that start to apply after Dragonshift resolves will overwrite this effect.').
card_ruling('dragonshift', '2013-04-15', 'Effects that modify the power or toughness of an affected creature, such as the effects of Phytoburst or Legion\'s Initiative, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').

card_ruling('dragonskull summit', '2009-10-01', 'This checks for lands you control with the land type Swamp or Mountain, not for lands named Swamp or Mountain. The lands it checks for don\'t have to be basic lands. For example, if you control Stomping Ground (a nonbasic land with the land types Mountain and Forest), Dragonskull Summit will enter the battlefield untapped.').
card_ruling('dragonskull summit', '2009-10-01', 'As this is entering the battlefield, it checks for lands that are already on the battlefield. It won\'t see lands that are entering the battlefield at the same time (due to Warp World, for example).').

card_ruling('dragonsoul knight', '2009-02-01', 'When Dragonsoul Knight\'s activated ability resolves, Dragonsoul Knight will gain flying and trample in addition to its other abilities. However, becoming a Dragon will make it lose all other creature types. It will no longer be a Human or a Knight.').
card_ruling('dragonsoul knight', '2009-02-01', 'If Dragonsoul Knight gains flying after blockers have been declared, it won\'t cause those blocks to change.').
card_ruling('dragonsoul knight', '2009-02-01', 'You can activate Dragonsoul Knight\'s ability more than once during a turn. The second time it resolves, it will probably have no visible effect other than giving Dragonsoul Knight another +5/+3.').

card_ruling('dragonstorm', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('dragonstorm', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('dragonstorm', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('dragonstorm', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('drain life', '2004-10-04', 'Cost reducers can be used to reduce the X part of the mana cost.').
card_ruling('drain life', '2004-10-04', 'You may gain up to the total toughness of the creature even if it was already damaged.').

card_ruling('drain power', '2004-10-04', 'This card forces the target player to draw mana from lands if they are untapped, but that player can choose how to draw the mana if a land he or she controls has multiple mana abilities, or mana abilities with choices.').
card_ruling('drain power', '2004-10-04', 'Since this is a sorcery, your opponent may use instants and abilities of permanents in response to this spell before you get the mana from their mana pool and lands.').
card_ruling('drain power', '2004-10-04', 'If a land can draw a variable amount of mana, the target player (not the player of this spell) chooses how much to draw.').
card_ruling('drain power', '2013-04-15', 'A mana ability is an ability that (1) isn\'t a loyalty ability, (2) doesn\'t target, and (3) could put mana into a player\'s mana pool when it resolves.').

card_ruling('draining whelk', '2006-09-25', 'If the triggered ability is countered (the targeted spell becomes an illegal target, for example), Draining Whelk doesn\'t get any +1/+1 counters.').
card_ruling('draining whelk', '2006-10-15', 'If the triggered ability doesn\'t counter the spell (if it is, for example, uncounterable), Draining Whelk still gets the counters.').

card_ruling('drainpipe vermin', '2012-10-01', 'You may pay {B} only once. The target player will discard a maximum of one card because of Drainpipe Vermin\'s ability.').

card_ruling('drake familiar', '2005-10-01', 'The ability lets you return any enchantment on the battlefield, including an opponent\'s enchantment. The ability isn\'t targeted, so you can return an untargetable enchantment. If there are no enchantments on the battlefield, or you choose not to return one, you must sacrifice Drake Familiar.').

card_ruling('drake umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('drake umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('drake umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('drake umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('drake umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('drake umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('drake umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('drake umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('drake umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('drake umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('drake umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('drakestown forgotten', '2014-05-29', 'If Drakestown Forgotten enters the battlefield from a graveyard, it counts itself when determining the value of X.').

card_ruling('dralnu\'s crusade', '2004-10-04', 'The Goblins are both Goblins and Zombies and they are black (they lose their previous color(s)).').
card_ruling('dralnu\'s crusade', '2004-10-04', 'This only affects Goblins which are on the battlefield. Ones in your graveyard, library, etc. are not changed to be black or to be Zombies.').

card_ruling('dralnu\'s pet', '2009-10-01', 'If the Dralnu\'s Pet is kicked, Dralnu\'s Pet will have flying from the moment it enters the battlefield. This will trigger abilities that look for a creature with flying entering the battlefield.').

card_ruling('dralnu, lich lord', '2006-09-25', 'All of the permanents are sacrificed at the same time, when the damage would have been dealt. So if Dralnu is one of the permanents you sacrifice due to its first ability, you must still sacrifice enough other permanents to equal the amount of damage that would have been dealt to it.').
card_ruling('dralnu, lich lord', '2006-09-25', 'If you don\'t control enough permanents, sacrifice all permanents you control.').

card_ruling('dramatic rescue', '2012-10-01', 'You must be able to target a creature to cast Dramatic Rescue.').
card_ruling('dramatic rescue', '2012-10-01', 'If the target creature is an illegal target when Dramatic Rescue tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 2 life.').

card_ruling('drana, kalastria bloodchief', '2010-06-15', 'The value of X may exceed the targeted creature\'s toughness. Drana\'s bonus is based on the value of X, regardless of what the targeted creature\'s toughness was.').
card_ruling('drana, kalastria bloodchief', '2010-06-15', 'If the targeted creature is an illegal target by the time the ability resolves, the ability is countered. Drana won\'t get the bonus.').
card_ruling('drana, kalastria bloodchief', '2010-06-15', 'You may target Drana itself with its ability. For example, if you do and X is 3, Drana will become 7/1 until end of turn.').

card_ruling('drana, liberator of malakir', '2015-08-25', 'Drana’s last ability triggers after combat damage has been dealt, so only attacking creatures that survive the combat damage step in which Drana deals combat damage will have +1/+1 counters put on them.').
card_ruling('drana, liberator of malakir', '2015-08-25', 'The +1/+1 counter won’t change how much damage Drana or any other attacking creature with first strike or double strike deals during that combat damage step. However, the +1/+1 counters that are put on attacking creatures without first strike will affect the damage those creatures deal during the regular combat damage step.').

card_ruling('drastic revelation', '2009-05-01', 'You discard your hand as part of Drastic Revelation\'s effect, not as an additional cost. If you have no cards in hand at this point, you simply don\'t discard any; the rest of the spell will still have its effect.').
card_ruling('drastic revelation', '2009-05-01', 'You\'ll discard three cards at random even if some other effect replaces your seven draws.').
card_ruling('drastic revelation', '2009-05-01', 'If there are fewer than seven cards left in your library, you\'ll draw all the cards in your library, then you\'ll discard three cards at random. Then you\'ll lose the game as a state-based action.').

card_ruling('dread', '2007-10-01', 'The last ability triggers when the Incarnation is put into its owner\'s graveyard from any zone, not just from on the battlefield.').
card_ruling('dread', '2007-10-01', 'Although this ability triggers when the Incarnation is put into a graveyard from the battlefield, it doesn\'t *specifically* trigger on leaving the battlefield, so it doesn\'t behave like other leaves-the-battlefield abilities. The ability will trigger from the graveyard.').
card_ruling('dread', '2007-10-01', 'If the Incarnation had lost this ability while on the battlefield (due to Lignify, for example) and then was destroyed, the ability would still trigger and it would get shuffled into its owner\'s library. However, if the Incarnation lost this ability when it was put into the graveyard (due to Yixlid Jailer, for example), the ability wouldn\'t trigger and the Incarnation would remain in the graveyard.').
card_ruling('dread', '2007-10-01', 'If the Incarnation is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('dread cacodemon', '2011-09-22', 'If you put Dread Cacodemon directly onto the battlefield from your hand, perhaps because of Kaalia of the Vast\'s ability, its ability won\'t trigger.').
card_ruling('dread cacodemon', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('dread slaver', '2012-05-01', 'Each time a creature dies, check whether Dread Slaver had dealt any damage to it at any time during that turn. If so, Dread Slaver\'s ability will trigger. It doesn\'t matter who controlled the creature or whose graveyard it was put into.').
card_ruling('dread slaver', '2012-05-01', 'If Dread Slaver and a creature it was blocking or blocked by both die in combat, Dread Slaver\'s ability will trigger.').
card_ruling('dread slaver', '2012-05-01', 'Dread Slaver will return if it somehow dealt damage to itself and then dies.').
card_ruling('dread slaver', '2012-05-01', 'The card will return to the battlefield under your control only if it\'s still in the graveyard when the ability resolves. If it\'s not (perhaps because an ability like undying has already returned it to the battlefield), nothing happens.').

card_ruling('dread statuary', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('dread statuary', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').

card_ruling('dreadbringer lampads', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('dreadbringer lampads', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('dreadbringer lampads', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('dreadwaters', '2012-05-01', 'The number of cards put into the graveyard is based on the number of lands you control when Dreadwaters resolves.').

card_ruling('dream coat', '2004-10-04', 'You can choose any single color or combination of colors. The valid colors are Black, Blue, Green, Red, and White.').
card_ruling('dream coat', '2004-10-04', 'The color change does not wear off at end of turn.').

card_ruling('dream fighter', '2004-10-04', 'Dream Fighter affects all creatures that end up blocking it, including ones that are members of bands.').
card_ruling('dream fighter', '2004-10-04', 'If Dream Fighter is assigned to block a creature with Flanking, the Flanking ability and the Dream Fighter ability both trigger. The active player\'s Flanking effect is put on the stack first and resolves after the Dream Fighter\'s phasing out effect happen, so the Flanking does nothing.').

card_ruling('dream halls', '2004-10-04', 'This only replaces the mana cost (the mana in the upper right hand corner of the card). It will not pay additional costs from the card text (such as Buyback) or from other effects. It does not prevent you from paying those additional costs if you want or need to.').

card_ruling('dream leash', '2005-10-01', 'The restriction that the permanent must be tapped is checked only if you cast Dream Leash as a spell. Once Dream Leash is on the battlefield, it doesn\'t matter if the enchanted permanent becomes untapped.').
card_ruling('dream leash', '2005-10-01', 'If an effect puts Dream Leash onto the battlefield or moves it to another permanent, the permanent it becomes attached to need not be tapped. If you cast Copy Enchantment and copy Dream Leash as it enters the battlefield, the permanent it becomes attached to need not be tapped.').

card_ruling('dream thief', '2008-08-01', 'Dream Thief doesn\'t check whether you\'ve cast another blue spell until its enters-the-battlefield ability resolves. If Dream Thief\'s enters-the-battlefield ability triggers, then you cast a blue instant spell in response, you\'ll get the bonus.').
card_ruling('dream thief', '2008-08-01', 'Although Dream Thief says it looks for \"another blue spell,\" there\'s no requirement that Dream Thief actually be cast as a spell (or be blue) for this part of its ability to work. For example, if a spell such as Zombify puts Dream Thief directly onto the battlefield, its ability will still check whether you\'ve cast a blue spell this turn, even though you didn\'t cast Dream Thief itself as a spell.').

card_ruling('dream tides', '2006-02-01', 'You can only untap nongreen creatures once, since this has a beginning of upkeep triggered ability.').

card_ruling('dreampod druid', '2012-06-01', 'Dreampod Druid\'s ability checks to see if it\'s enchanted at the beginning of each upkeep. If it\'s not, the ability won\'t trigger. If the ability triggers but Dreampod Druid isn\'t enchanted when that ability resolves, the ability will do nothing.').
card_ruling('dreampod druid', '2012-06-01', 'Notably, if Dreampod Druid leaves the battlefield while its ability is on the stack, that ability will use Dreampod Druid\'s last known information to determine whether or not it was enchanted. If it was enchanted when it left the battlefield, the ability will resolve and create a Saproling.').

card_ruling('dreams of the dead', '2004-10-04', 'This is a replacement effect.').
card_ruling('dreams of the dead', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('dreams of the dead', '2008-10-01', 'If a creature returned to the battlefield with Dreams of the Dead would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence.').
card_ruling('dreams of the dead', '2013-04-15', 'If a permanent has multiple instances of cumulative upkeep, each triggers separately. However, the age counters are not connected to any particular ability; each cumulative upkeep ability will count the total number of age counters on the permanent at the time that ability resolves.').

card_ruling('dreg mangler', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('dregs of sorrow', '2004-10-04', 'X can be zero.').

card_ruling('dregscape zombie', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('dregscape zombie', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('dregscape zombie', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('dregscape zombie', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('dregscape zombie', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('drifting djinn', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice Drifting Djinn. You can choose to not pay if you no longer control this card on resolution.').
card_ruling('drifting djinn', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('drifting meadow', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('drinker of sorrow', '2004-10-04', 'You only sacrifice one permanent, no matter how many things it deals damage to.').

card_ruling('driver of the dead', '2012-05-01', 'If Driver of the Dead dies at the same time as a creature with converted mana cost 2 or less, you can target that card and return it to the battlefield.').

card_ruling('drogskol reaver', '2011-01-22', 'If Drogskol Reaver deals enough first-strike damage to destroy each creature it\'s blocking or was blocked by, it won\'t deal any damage during the regular combat damage step (unless it\'s attacking and somehow gains trample). Its ability won\'t trigger a second time that combat.').
card_ruling('drogskol reaver', '2011-01-22', 'The ability triggers just once for each life-gaining event, no matter how much life was gained.').
card_ruling('drogskol reaver', '2011-01-22', 'If multiple creatures with lifelink you control deal combat damage at the same time, the damage dealt by each of those creatures is a separate life-gaining event and Drogskol Reaver\'s ability will trigger that many times.').
card_ruling('drogskol reaver', '2011-01-22', 'If a creature with lifelink you control deals damage to multiple creatures, players, and/or planeswalkers at the same time, the ability will trigger just once. The damage dealt by that creature is a single life-gaining event.').

card_ruling('dromad purebred', '2005-10-01', 'Each time Dromad Purebred is dealt damage, including from multiple sources at once, you gain 1 life. You don\'t gain life equal to the damage dealt.').

card_ruling('dromar\'s cavern', '2004-10-04', 'If you don\'t want to unsummon a land, you can play this card then tap it for mana before the enters the battlefield ability resolves. You may then choose to sacrifice it instead of unsummoning a land.').
card_ruling('dromar\'s cavern', '2005-08-01', 'This land is of type \"Lair\" only; other subtypes have been removed. It is not a basic land.').

card_ruling('dromar, the banisher', '2004-10-04', 'You choose the color during resolution. This means your opponent does not get to react after knowing the color you chose.').

card_ruling('dromoka captain', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('dromoka captain', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('dromoka monument', '2015-02-25', 'A Monument can’t attack on the turn it enters the battlefield.').
card_ruling('dromoka monument', '2015-02-25', 'Each Monument is colorless, although the last ability will make each of them two colors until end of turn.').
card_ruling('dromoka monument', '2015-02-25', 'If a Monument has any +1/+1 counters on it, those counters will remain on the permanent after it stops being a creature. Those counters will have no effect as long as the Monument isn’t a creature, but they will apply again if the Monument later becomes a creature.').
card_ruling('dromoka monument', '2015-02-25', 'Activating the last ability of a Monument while it’s already a creature will override any effects that set its power or toughness to a specific value. Effects that modify power or toughness without directly setting them to a specific value will continue to apply.').

card_ruling('dromoka\'s command', '2015-02-25', 'You choose the two modes as you cast the spell. You must choose two different modes. Once modes are chosen, they can’t be changed.').
card_ruling('dromoka\'s command', '2015-02-25', 'You can choose a mode only if you can choose legal targets for that mode. Ignore the targeting requirements for modes that aren’t chosen. For example, you can cast Ojutai’s Command without targeting a creature spell provided you don’t choose the third mode.').
card_ruling('dromoka\'s command', '2015-02-25', 'As the spell resolves, follow the instructions of the modes you chose in the order they are printed on the card. For example, if you chose the second and fourth modes of Ojutai’s Command, you would gain 4 life and then draw a card. (The order won’t matter in most cases.)').
card_ruling('dromoka\'s command', '2015-02-25', 'If a Command is copied, the effect that creates the copy will usually allow you to choose new targets for the copy, but you can’t choose new modes.').
card_ruling('dromoka\'s command', '2015-02-25', 'If all targets for the chosen modes become illegal before the Command resolves, the spell will be countered and none of its effects will happen. If at least one target is still legal, the spell will resolve but will have no effect on any illegal targets.').

card_ruling('dromoka\'s gift', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('dromoka\'s gift', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('dromoka, the eternal', '2014-11-24', 'Each Dragon you attack with will cause Dromoka’s triggered ability to trigger. For each instance of the ability, you’ll determine which creature to put +1/+1 counters on as it resolves. Specifically, the creature you control with the lowest toughness as the first such ability resolves may not have the lowest toughness as the second such ability resolves.').
card_ruling('dromoka, the eternal', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('dromoka, the eternal', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('dromosaur', '2004-10-04', 'If it manages to block or become blocked more than once in a turn, it can get the bonus twice.').
card_ruling('dromosaur', '2004-10-04', 'Switching blockers will not re-trigger this since it only triggers on becoming blocked and switching does not unblock and reblock it.').

card_ruling('droning bureaucrats', '2006-02-01', 'Droning Bureaucrats\'s ability affects all creatures with converted mana cost equal to exactly X, regardless of whether they were on the battlefield at the time the ability resolved. For example, if the ability resolves with an X of 4, then Giant Solifuge is put onto the battlefield, the Solifuge can\'t attack or block that turn.').
card_ruling('droning bureaucrats', '2006-02-01', 'If you use Droning Bureaucrats\'s ability after you have declared attackers but before your opponent declares blockers, the restriction will apply to only your opponent\'s creatures. Creatures that are already attacking will be unaffected.').
card_ruling('droning bureaucrats', '2006-02-01', 'Creature lands and most token creatures have converted mana cost 0. If a creature on the battlefield has {X} in its mana cost, treat X as 0.').

card_ruling('drop of honey', '2006-07-15', 'This card\'s ability is not targeted, so even untargetable creatures or those with Protection can be chosen.').
card_ruling('drop of honey', '2013-07-01', 'If the creature with the least power has indestructible, the ability does nothing.').
card_ruling('drop of honey', '2013-07-01', 'If there are multiple creatures tied for least power and some but not all of them have indestructible, the ones with indestructible can\'t be chosen.').

card_ruling('dross hopper', '2011-01-01', 'You may sacrifice Dross Hopper to pay for its own ability. If you do, however, it won\'t gain flying because it won\'t be on the battlefield by the time the ability resolves.').

card_ruling('drought', '2004-10-04', 'You have to sacrifice a Swamp for each black mana in the activation cost. If you use Pestilence with {B}{B}{B}{B}, that\'s 4 activations with {B} each so you sacrifice 4 Swamps.').
card_ruling('drought', '2008-08-01', 'A hybrid symbol that is both black and another type is a black mana symbol, regardless of what cost is paid for it.').
card_ruling('drought', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('drown in filth', '2013-04-15', 'You choose the target of Drown in Filth as you cast the spell. You won\'t necessarily know what will happen to that creature\'s power and toughness at that time.').

card_ruling('drown in sorrow', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('drown in sorrow', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('drown in sorrow', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('drown in sorrow', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').
card_ruling('drown in sorrow', '2014-02-01', 'Only creatures on the battlefield when Drown in Sorrow resolves will get -2/-2. Creatures that enter the battlefield later in the turn will not.').

card_ruling('drowned catacomb', '2009-10-01', 'This checks for lands you control with the land type Island or Swamp, not for lands named Island or Swamp. The lands it checks for don\'t have to be basic lands. For example, if you control Blood Crypt (a nonbasic land with the land types Swamp and Mountain), Drowned Catacomb will enter the battlefield untapped.').
card_ruling('drowned catacomb', '2009-10-01', 'As this is entering the battlefield, it checks for lands that are already on the battlefield. It won\'t see lands that are entering the battlefield at the same time (due to Warp World, for example).').

card_ruling('drowned rusalka', '2006-02-01', 'Unlike many similar cards, Drowned Rusalka\'s ability causes you to discard a card first and then draw a card. If your hand is empty, you will just draw a card.').

card_ruling('drowner of hope', '2015-08-25', 'You can sacrifice any Eldrazi Scion to activate Drowner of Hope’s last ability, not just one created by Drowner of Hope.').
card_ruling('drowner of hope', '2015-08-25', 'You can’t sacrifice the same permanent to pay two different costs. Notably, if you sacrifice an Eldrazi Scion to activate the last ability of Drowner of Hope, you won’t add {1} to your mana pool.').
card_ruling('drowner of hope', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('drowner of hope', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('drowner of hope', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('drowner of hope', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('drowner of hope', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('drowner of hope', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('drowner of hope', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('drowner of hope', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('drowner of hope', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('drowner of secrets', '2008-10-01', 'You can tap any permanent with the Merfolk creature type to pay for the ability, including noncreature tribal permanents such as Merrow Commerce.').

card_ruling('drudge beetle', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').

card_ruling('drudge spell', '2004-10-04', 'This destroys all Skeleton tokens from all sources if it leaves the battlefield. It does not just destroy ones generated by this Drudge Spell or just ones you control.').
card_ruling('drudge spell', '2004-10-04', 'The fact that Skeleton tokens are destroyed is an aspect of the enchantment and not of the tokens. Thus, only Skeleton tokens which are on the battlefield when it leaves the battlefield are destroyed. Ones which are going to appear due to use of the ability but which have not yet appeared are safe.').
card_ruling('drudge spell', '2004-10-04', 'The creatures in the graveyard are exiled during activation as a cost.').

card_ruling('druid\'s deliverance', '2012-10-01', 'You can cast Druid\'s Deliverance even if you don\'t control any creature tokens.').
card_ruling('druid\'s deliverance', '2012-10-01', 'Combat damage dealt to creatures or planeswalkers you control and combat damage dealt to your teammates is not prevented.').
card_ruling('druid\'s deliverance', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('druid\'s deliverance', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('druid\'s deliverance', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('druid\'s deliverance', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('druid\'s deliverance', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('druid\'s deliverance', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('druid\'s familiar', '2012-05-01', 'If Druid\'s Familiar becomes unpaired, it will immediately lose the +2/+2 bonus. If this causes it to have damage marked on it equal to it or greater than its new toughness, it will be destroyed. The same is true for the creature it was paired with.').

card_ruling('druidic satchel', '2011-09-22', 'If the revealed card is both a creature and a land (such as Dryad Arbor), you\'ll put a Saproling creature token and that card onto the battlefield.').
card_ruling('druidic satchel', '2011-09-22', 'If the card is not a land card, it will remain on top of your library.').

card_ruling('druids\' repository', '2012-05-01', 'The first ability of Druids\' Repository will trigger once for each creature you control that attacks.').
card_ruling('druids\' repository', '2012-05-01', 'You can activate the second ability of Druids\' Repository multiple times in the same turn. It\'s a mana ability, doesn\'t use the stack, and can\'t be responded to.').

card_ruling('drumhunter', '2008-10-01', 'The first ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control a creature with power 5 or greater as your end step begins, and (2) the ability will do nothing if you don\'t control a creature with power 5 or greater by the time it resolves. (It doesn\'t have to be the same creature as the one that allowed the ability to trigger.) Power-boosting effects that last \"until end of turn\" will still be in effect when this kind of ability triggers and resolves. An ability like this will trigger a maximum of once per turn, no matter how many applicable creatures you control.').

card_ruling('dryad arbor', '2007-05-01', 'Forest is a land type. Dryad is a creature type.').
card_ruling('dryad arbor', '2007-05-01', 'Dryad Arbor is played as a land. It doesn\'t use the stack, it\'s not a spell, it can\'t be responded to, it has no mana cost, and it counts as your land play for the turn.').
card_ruling('dryad arbor', '2007-05-01', 'If a Dryad Arbor gains flash, or you have the ability to play Dryad Arbor as though it had flash (due to Teferi, Mage of Zhalfir or Scout\'s Warning, for example), you can ignore the normal timing rules for playing a land, but not any other restrictions. You can\'t play Dryad Arbor during another player\'s turn, and you can\'t play Dryad Arbor if it\'s your turn and you\'ve already played a land.').
card_ruling('dryad arbor', '2011-09-22', 'If Dryad Arbor is changed into another basic land type (such as by Sea\'s Claim), it continues to be a creature and a Dryad.').
card_ruling('dryad arbor', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('dryad militant', '2012-10-01', 'If an instant or sorcery spell destroys Dryad Militant directly (like Murder does), that instant or sorcery card will be put into its owner\'s graveyard. However, if an instant or sorcery card deals lethal damage to Dryad Militant, Dryad Militant will remain on the battlefield until the next time state-based actions are checked, which is after the instant or sorcery finishes resolving. The instant or sorcery will be exiled.').
card_ruling('dryad militant', '2012-10-01', 'If an instant or sorcery card is discarded while Dryad Militant is on the battlefield, abilities that function when a card is discarded (such as madness) still work, even though that card never reaches a graveyard. In addition, spells or abilities that check the characteristics of a discarded card (such as Chandra Ablaze\'s first ability) can find that card in exile.').

card_ruling('dryad sophisticate', '2006-02-01', '\"Nonbasic landwalk\" means \"This creature can\'t be blocked as long as defending player controls a nonbasic land.\" A nonbasic land is any land that lacks the basic supertype, whether or not it has a basic land type (such as Island).').

card_ruling('dryad\'s caress', '2005-10-01', 'The spell checks on resolution to see if any mana of the stated color was spent to pay its cost. It doesn\'t matter how much mana of that color was spent.').
card_ruling('dryad\'s caress', '2005-10-01', 'If the spell is copied (such as with Twincast), the copy will never have had mana of the stated color paid for it, no matter what colors were spent on the original spell.').

card_ruling('dryad\'s favor', '2010-08-15', 'Forestwalk cares about lands with the land type Forest, not necessarily lands named Forest.').

card_ruling('dual casting', '2012-05-01', 'The ability granted by Dual Casting can target (and copy) any instant or sorcery spell you control, not just one with targets.').
card_ruling('dual casting', '2012-05-01', 'The copy is created on the stack, so it\'s not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities.').
card_ruling('dual casting', '2012-05-01', 'The copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('dual casting', '2012-05-01', 'If the spell being copied is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode(s). You can\'t choose different ones.').
card_ruling('dual casting', '2012-05-01', 'If the spell being copied has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('dual casting', '2012-05-01', 'You can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if you sacrifice a 3/3 creature to cast Fling and then copy it with the ability granted by Dual Casting, the copy of Fling will also deal 3 damage to its target.').

card_ruling('dual nature', '2004-10-04', 'If Dual Nature leaves the battlefield between when it triggers and when a token is put onto the battlefield, that token remains on the battlefield indefinitely. It missed the separate ability which would have exiled the token.').
card_ruling('dual nature', '2004-10-04', 'If the original creature has any \"enters the battlefield\" abilities, the copy will also have those.').
card_ruling('dual nature', '2009-10-01', 'The token is not considered to have been kicked, even if the creature it\'s a copy of was kicked.').
card_ruling('dual nature', '2013-07-01', 'If a legendary creature enters the battlefield, the token is put onto the battlefield and then the \"legend rule\" forces its controller to put either the original or the token into its owner\'s graveyard before any player can take any actions.').

card_ruling('dualcaster mage', '2014-11-07', 'You may change any number of the targets for the copy, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('dualcaster mage', '2014-11-07', 'If the targeted spell is modal (that is, it says “Choose one —” or similar), you can’t choose a different mode.').
card_ruling('dualcaster mage', '2014-11-07', 'If the copied spell targets a spell on the stack (like Cancel does, for example), you can’t change that spell’s target to itself.').

card_ruling('due respect', '2011-06-01', 'Due Respect won\'t tap any permanents that have entered the battlefield before it resolved.').

card_ruling('dueling grounds', '2004-10-04', 'The ability affects all players.').

card_ruling('duergar hedge-mage', '2008-08-01', 'Each of the triggered abilities has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control two or more lands of the appropriate land type when the Hedge-Mage enters the battlefield, and (2) the ability will do nothing if you don\'t control two or more lands of the appropriate land type by the time it resolves.').
card_ruling('duergar hedge-mage', '2008-08-01', 'Both abilities trigger at the same time. You can put them on the stack in any order.').
card_ruling('duergar hedge-mage', '2008-08-01', 'Each of the triggered abilities look at your lands individually. This means that if you only control two dual-lands of the appropriate types, both of the abilities will trigger').
card_ruling('duergar hedge-mage', '2008-08-01', 'Both abilities are optional; you choose whether to use them when they resolve. If an ability has a target, you must choose a target even if you don\'t plan to use the ability.').

card_ruling('duergar mine-captain', '2008-08-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('duergar mine-captain', '2008-08-01', 'The \"summoning sickness\" rule applies to {Q}. If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability. Ignore this rule if the creature also has haste.').
card_ruling('duergar mine-captain', '2008-08-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('duergar mine-captain', '2008-08-01', 'This ability affects only creatures that are attacking at the time it resolves. It won\'t affect creatures that attack later in the turn.').

card_ruling('dulcet sirens', '2014-11-07', '“Target opponent” means one of your opponents. In other words, Dulcet Sirens’s ability can’t make a creature attack you.').
card_ruling('dulcet sirens', '2014-11-07', 'The target creature must attack the target opponent if able, not a planeswalker controlled by that player.').
card_ruling('dulcet sirens', '2014-11-07', 'If, as attackers are declared, the target creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn’t been under its controller’s control continuously since his or her last turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having the creature attack the opponent, its controller isn’t forced to pay that cost, so it doesn’t have to attack that player in that case either.').
card_ruling('dulcet sirens', '2014-11-07', 'If the creature can’t attack the player for one of the above reasons but it can still attack elsewhere, its controller may choose to have it attack another player, attack a planeswalker, or not attack at all.').
card_ruling('dulcet sirens', '2014-11-07', 'If either the target creature or the target player is an illegal target when the activated ability tries to resolve, the ability will have no effect. The creature won’t be forced to attack that opponent, but it still can if able.').

card_ruling('duneblast', '2014-09-20', 'You decide which creature to spare as Duneblast resolves. This choice doesn’t target the creature. If you don’t choose a creature, then all creatures will be destroyed.').

card_ruling('dunerider outlaw', '2004-10-04', 'If it damages the opponent multiple times in a turn, it gets only one +1/+1 counter at end of turn.').
card_ruling('dunerider outlaw', '2007-02-01', 'Dunerider Outlaw gets a maximum of one +1/+1 counter at end of turn, no matter how many times it may have dealt damage to an opponent.').
card_ruling('dunerider outlaw', '2007-02-01', 'Dunerider Outlaw\'s triggered ability checks to see if it dealt damage during the turn to an opponent of its current controller. If it dealt damage to player A, then player A takes control of it, the ability doesn\'t trigger.').
card_ruling('dunerider outlaw', '2007-02-01', 'If Dunerider Outlaw dealt damage to an opponent of its current controller during the turn and that opponent is no longer in the game at end of turn, the ability will still trigger and the Outlaw will still get a +1/+1 counter.').
card_ruling('dunerider outlaw', '2007-02-01', 'This is the timeshifted version of Whirling Dervish.').

card_ruling('dungeon geists', '2011-01-22', 'If another player gains control of Dungeon Geists, the creature will no longer be affected by the ability preventing it from untapping, even if you later regain control of Dungeon Geists.').
card_ruling('dungeon geists', '2011-01-22', 'If you gain control of the creature that\'s being prevented from untapping, that creature won\'t untap during your untap step for as long as you control Dungeon Geists.').
card_ruling('dungeon geists', '2013-04-15', 'The ability can target a tapped creature. If the targeted creature is already tapped when it resolves, that creature just remains tapped and doesn\'t untap during its controller\'s next untap step.').

card_ruling('dungrove elder', '2011-09-22', 'Dungrove Elder\'s power and toughness are each equal to the number of lands you control with the land type Forest, not necessarily lands named Forest.').
card_ruling('dungrove elder', '2011-09-22', 'Dungrove Elder\'s power and toughness will change as the number of Forests you control changes.').
card_ruling('dungrove elder', '2011-09-22', 'Dungrove Elder\'s ability sets its power and toughness in all zones, not just the battlefield.').

card_ruling('duplicant', '2004-12-01', 'If no creature card is imprinted on Duplicant, it has its normal power, toughness, and creature types.').
card_ruling('duplicant', '2004-12-01', 'Duplicant\'s power and toughness change to the imprinted card\'s power and toughness. Counters and other effects that change Duplicant\'s power and toughness still apply.').
card_ruling('duplicant', '2004-12-01', 'Duplicant keeps all creature types it had when the card was imprinted, including any types that Duplicant had gained.').

card_ruling('duplicity', '2004-10-04', 'The cards are face down all the time. You can\'t look at them. This applies even to cards from your hand that got swapped out. You have to remember what they were if you care.').
card_ruling('duplicity', '2008-04-01', 'For a time the last ability triggered when Duplicity left the battlefield. It has been restored to triggering when you lose control of Duplicity.').
card_ruling('duplicity', '2008-04-01', 'Taking control of another player\'s Duplicity will trigger the ability that causes the cards they exiled to be put into their owner\'s graveyard.').

card_ruling('durkwood baloth', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('durkwood baloth', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('durkwood baloth', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('durkwood baloth', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('durkwood baloth', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('durkwood baloth', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('durkwood baloth', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('durkwood baloth', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('durkwood baloth', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('durkwood baloth', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('durkwood tracker', '2006-09-25', 'If Durkwood Tracker has left the battlefield before the ability resolves, the ability doesn\'t do anything. If the targeted creature has left the battlefield before the ability resolves, the ability is countered.').

card_ruling('dusk urchins', '2008-05-01', 'The -1/-1 counter is put on Dusk Urchins during the declare attackers or declare blockers step, whichever is appropriate, so it will be smaller by the time combat damage is dealt. (Note that this is different from how the _Shadowmoor_ card Wicker Warcrawler works.)').
card_ruling('dusk urchins', '2008-05-01', 'If Dusk Urchins has just 1 toughness when it blocks, it will get a -1/-1 counter and be put into the graveyard. The creature that it blocked remains blocked, however.').

card_ruling('duskmantle guildmage', '2013-01-24', 'The first ability considers each of your opponents. You don\'t choose just one of them.').
card_ruling('duskmantle guildmage', '2013-01-24', 'Each time the first ability resolves, a delayed triggered ability is created. Whenever a card is put into an opponent\'s graveyard that turn, each of those abilities will trigger. For example, if you activate the first ability twice (and let those abilities resolve) and then activate the second ability targeting an opponent, that player will lose a total of 4 life. Each card put into that player\'s graveyard will cause two abilities to trigger, each causing that player to lose 1 life.').
card_ruling('duskmantle guildmage', '2013-01-24', 'The delayed triggered ability created won\'t trigger if a token permanent is put into an opponent\'s graveyard.').

card_ruling('duskmantle seer', '2013-01-24', 'The loss of life is simultaneous. If this causes both players to end up at 0 or less life, the game will be a draw. In a multiplayer game, players with 0 or less life will lose the game.').
card_ruling('duskmantle seer', '2013-01-24', 'The cards put into hands this way are not “drawn.” For example, you couldn\'t reveal a card with miracle put into your hand this way (but it also won\'t count as the first card you\'ve drawn this turn either).').

card_ruling('duskrider peregrine', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('duskrider peregrine', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('duskrider peregrine', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('duskrider peregrine', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('duskrider peregrine', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('duskrider peregrine', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('duskrider peregrine', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('duskrider peregrine', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('duskrider peregrine', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('duskrider peregrine', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('duskworker', '2004-12-01', 'Setting up the regeneration shield doesn\'t remove Duskworker from combat. However, Duskworker is removed from combat if it would be destroyed and then regenerates.').

card_ruling('dust corona', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) can block this because they don\'t actually have Flying.').

card_ruling('dust elemental', '2007-02-01', 'You may return this creature itself to its owner\'s hand. If you control no other creatures, you must return it.').
card_ruling('dust elemental', '2007-02-01', 'The ability doesn\'t target what you return. You don\'t choose what to return until the ability resolves. No one can respond to the choice.').
card_ruling('dust elemental', '2007-02-01', 'If you are instructed to return more creatures than you control, you must return all the creatures you control to their owner\'s hand.').

card_ruling('dust stalker', '2015-08-25', 'Dust Stalker’s last ability checks to see if you control another colorless creature as the end step begins. If you do, the ability won’t trigger. If you don’t, the ability will check again as it tries to resolve. If you somehow control another colorless creature at that time, the ability won’t do anything. You won’t return Dust Stalker to its owner’s hand.').
card_ruling('dust stalker', '2015-08-25', 'Dust Stalker’s last ability doesn’t depend on Dust Stalker being colorless. If it gains one or more colors, the ability will still look for another creature you control that’s colorless.').
card_ruling('dust stalker', '2015-08-25', 'Like most abilities, Dust Stalker’s last ability functions only while Dust Stalker is on the battlefield.').
card_ruling('dust stalker', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('dust stalker', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('dust stalker', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('dust stalker', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('dust stalker', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('dust to dust', '2004-10-04', 'If one target is removed or becomes invalid after declaration, the other target is still affected.').

card_ruling('dwarven armory', '2004-10-04', 'The ability can be activated during your opponent\'s upkeep as well as your own. The upkeep restriction is there to avoid its use during combat.').

card_ruling('dwarven bloodboiler', '2004-10-04', 'The ability can be activated the turn it enters the battlefield because the ability does not use the {Tap} symbol.').
card_ruling('dwarven bloodboiler', '2004-10-04', 'Yes, it can tap itself.').

card_ruling('dwarven catapult', '2004-10-04', 'Count the number of creatures on resolution of the spell and divide X by that number at that time. If the number of creatures changes before resolution, the damage done may be different than what you expected.').
card_ruling('dwarven catapult', '2007-09-16', 'For example, if X is 5 and the targeted opponent controls three creatures, Dwarven Catapult deals 1 damage to each of those creatures.').
card_ruling('dwarven catapult', '2014-02-01', 'If the targeted opponent controls more than X creatures, no damage will be dealt to any of them.').

card_ruling('dwarven hold', '2004-10-04', 'It is considered \"tapped for mana\" if you activate its mana ability, even if you choose to take zero mana from it.').
card_ruling('dwarven hold', '2004-10-04', 'This enters the battlefield tapped even if a continuous effect immediately changes it to something else.').
card_ruling('dwarven hold', '2004-10-04', 'If the land is tapped by some external effect, no counters are removed from it.').
card_ruling('dwarven hold', '2004-10-04', 'Counters are not lost if the land is changed to another land type. They wait around for the land to change back.').
card_ruling('dwarven hold', '2004-10-04', 'Whether or not it is tapped is checked at the beginning of upkeep. If it is not tapped, the ability does not trigger. It also checks during resolution and you only get a counter if it is still tapped then.').

card_ruling('dwarven patrol', '2004-10-04', 'Remember that lands are not spells.').

card_ruling('dwarven sea clan', '2008-08-01', 'You can\'t target a creature if its controller doesn\'t control an Island as you are activating the ability, and the ability will be countered if the creature\'s controller doesn\'t control an Island as the ability resolves.').
card_ruling('dwarven sea clan', '2008-08-01', 'If control of the targeted creature changes between the activating and resolution of the ability (ie, while it\'s on the stack), then the player who must control an Island as the ability resolves is the creature\'s new controller.').

card_ruling('dwarven song', '2004-10-04', 'You can choose to target zero creatures.').

card_ruling('dwarven thaumaturgist', '2005-11-01', 'Note that the wording \"Effects that alter the creature\'s power alter its toughness instead, and vice versa, this turn\" has been removed.').
card_ruling('dwarven thaumaturgist', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('dwarven thaumaturgist', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('dwarven vigilantes', '2013-04-15', 'An ability that triggers when something \"attacks and isn\'t blocked\" triggers in the declare blockers step after blockers are declared if (1) that creature is attacking and (2) no creatures are declared to block it. It will trigger even if that creature was put onto the battlefield attacking rather than having been declared as an attacker in the declare attackers step.').

card_ruling('dwarven warriors', '2004-10-04', 'The ability can be activated after a creature is blocked, but it has no effect. Once a creature is blocked, it can\'t be unblocked.').
card_ruling('dwarven warriors', '2004-10-04', 'If you increase the power of the targeted creature after the ability resolves, it still can\'t be blocked that turn.').

card_ruling('dwynen, gilt-leaf daen', '2015-06-22', 'Count the number of attacking Elves you control as Dwynen’s last ability resolves to determine how much life to gain.').

card_ruling('dying wail', '2004-10-04', 'If they have less than 2 cards, they discard all the cards they have.').
card_ruling('dying wail', '2004-10-04', 'It only triggers on a creature going to the graveyard from the battlefield.').

card_ruling('dying wish', '2013-01-24', 'Use the creature\'s power the last time it was on the battlefield to determine how much life the target player loses and how much life you gain.').
card_ruling('dying wish', '2013-01-24', 'If another player gains control of either Dying Wish or the enchanted creature (but not both), Dying Wish will be enchanting an illegal permanent. The Aura will be put into its owner\'s graveyard as a state-based action.').

card_ruling('dynacharge', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('dynacharge', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('dynacharge', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('dynacharge', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('dynacharge', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('dynacharge', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('dynacharge', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('dystopia', '2004-10-04', 'The permanent is chosen and sacrificed during resolution.').

