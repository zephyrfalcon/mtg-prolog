% Rulings

card_ruling('wake of destruction', '2004-10-04', 'Only looks at the card name, so it will not destroy lands that just share the destroyed land\'s type.').

card_ruling('wake the dead', '2014-11-07', 'The delayed triggered ability that makes you sacrifice the creatures will trigger only once. If you don’t sacrifice one or more of those creatures at that time (perhaps because another player gained control of them), the ability won’t try to make you sacrifice them on future turns.').

card_ruling('wake the reflections', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('wake the reflections', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('wake the reflections', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('wake the reflections', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('wake the reflections', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('wake the reflections', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('wake thrasher', '2008-08-01', 'If permanents you control become untapped during your untap step, Wake Thresher\'s ability will trigger that many times. However, since no player gets priority during the untap step, those abilities wait to be put on the stack until your upkeep starts. At that time, all your \"beginning of upkeep\" triggers will also trigger. You can put them and Wake Thresher\'s abilities on the stack in any order.').

card_ruling('wakestone gargoyle', '2006-05-01', 'Wakestone Gargoyle\'s ability allows itself to attack.').
card_ruling('wakestone gargoyle', '2006-05-01', 'Wakestone Gargoyle\'s ability will affect creatures with defender that come under your control after the ability resolves but before you declare attackers (though those creatures still can\'t attack unless they have haste).').

card_ruling('walker of the grove', '2008-04-01', 'Evoke doesn\'t change the timing of when you can cast the creature that has it. If you could cast that creature spell only when you could cast a sorcery, the same is true for cast it with evoke.').
card_ruling('walker of the grove', '2008-04-01', 'If a creature spell cast with evoke changes controllers before it enters the battlefield, it will still be sacrificed when it enters the battlefield. Similarly, if a creature cast with evoke changes controllers after it enters the battlefield but before its sacrifice ability resolves, it will still be sacrificed. In both cases, the controller of the creature at the time it left the battlefield will control its leaves-the-battlefield ability.').
card_ruling('walker of the grove', '2008-04-01', 'When you cast a spell by paying its evoke cost, its mana cost doesn\'t change. You just pay the evoke cost instead.').
card_ruling('walker of the grove', '2008-04-01', 'Effects that cause you to pay more or less to cast a spell will cause you to pay that much more or less while casting it for its evoke cost, too. That\'s because they affect the total cost of the spell, not its mana cost.').
card_ruling('walker of the grove', '2008-04-01', 'Whether evoke\'s sacrifice ability triggers when the creature enters the battlefield depends on whether the spell\'s controller chose to pay the evoke cost, not whether he or she actually paid it (if it was reduced or otherwise altered by another ability, for example).').
card_ruling('walker of the grove', '2008-04-01', 'If you\'re casting a spell \"without paying its mana cost,\" you can\'t use its evoke ability.').

card_ruling('walking atlas', '2010-03-01', 'The word \"artifact\" was inadvertently omitted from Walking Atlas\'s type line. The card has received errata to correct this omission; it is an artifact creature.').
card_ruling('walking atlas', '2010-03-01', 'Putting a land onto the battlefield as a result of Walking Atlas\'s ability isn\'t the same as playing a land. You may do put a land onto the battlefield even if it\'s an opponent\'s turn or you\'ve played a land this turn. Similarly, putting a land onto the battlefield during your turn doesn\'t preclude you from playing a land later in that turn.').

card_ruling('walking sponge', '2004-10-04', 'The target loses just one of the listed abilities.').

card_ruling('wall of caltrops', '2004-10-04', 'Banding is gained when the trigger resolves in the declare blockers step of the combat phase if the criterion is met.').

card_ruling('wall of essence', '2014-07-18', 'Wall of Essence’s triggered ability will trigger even if it’s dealt lethal damage. For example, if it’s dealt 7 combat damage, its ability will trigger and you’ll gain 7 life.').

card_ruling('wall of frost', '2014-07-18', 'Wall of Frost’s ability tracks the creature, not the creature’s controller. That is, if the creature changes controllers before its first controller’s next untap step, then it won’t untap during its new controller’s next untap step.').
card_ruling('wall of frost', '2014-07-18', 'If the creature isn’t tapped during its controller’s next untap step (perhaps because it was untapped by a spell), Wall of Frost’s ability has no effect at that time. It won’t try to keep the creature tapped on subsequent turns.').

card_ruling('wall of junk', '2004-10-04', 'It only returns to owner\'s hand if it is still on the battlefield at end of combat.').

card_ruling('wall of limbs', '2014-07-18', 'The ability triggers just once for each life-gaining event, whether it’s 1 life from Soulmender or 8 life from Meditation Puzzle.').
card_ruling('wall of limbs', '2014-07-18', 'Use Wall of Limbs’s power as it last existed on the battlefield, including any +1/+1 counters it had, to determine the value of X.').
card_ruling('wall of limbs', '2014-07-18', 'If you gain life at the same time Wall of Limbs is dealt lethal damage (probably because you control a blocking creature with lifelink), Wall of Limbs’s triggered ability will trigger, but Wall of Limbs will be destroyed before the ability resolves.').

card_ruling('wall of mulch', '2004-10-04', 'It can be sacrificed for its own ability.').

card_ruling('wall of nets', '2004-10-04', 'It only exiles creatures if this card is still on the battlefield at the end of combat.').

card_ruling('wall of putrid flesh', '2005-08-01', 'The term \"enchanted creatures\" means \"creatures with an Aura on them\".').

card_ruling('wall of resistance', '2004-10-04', 'It gets only one counter a turn, not one per point of damage.').

card_ruling('wall of reverence', '2009-02-01', 'This ability triggers and resolves before \"until end of turn\" effects have worn off.').

card_ruling('wall of shadows', '2004-10-04', 'Can be targeted by a modal spell that can target a non-Wall in one of its modes, even if it has a mode that targets only Walls. That spell can, in theory, target a non-Wall, so it is not a \"spell that can target only Walls\".').

card_ruling('wall of shards', '2006-07-15', 'When paying Wall of Shards\' cumulative upkeep, you may choose a different opponent for each age counter, or you can choose the same opponent multiple times.').
card_ruling('wall of shards', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('wall of souls', '2004-10-04', 'Redirected damage is still combat damage, so it will trigger on combat damage that is redirected to it.').

card_ruling('wall of tombstones', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').
card_ruling('wall of tombstones', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('wall of wonder', '2004-10-04', 'Paying to make the Wall capable of attacking does not override the normal rule that a creature may not attack unless it began your turn on the battlefield.').

card_ruling('wand of ith', '2004-10-04', 'Any X in the mana cost of a card is zero for purposes of the Wand.').

card_ruling('wandering goblins', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('wandering goblins', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('wandering goblins', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('wandering graybeard', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('wandering graybeard', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('wandering graybeard', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('wandering graybeard', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('wandering graybeard', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('wandering graybeard', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('wandering mage', '2009-10-01', 'The second ability checks that the targeted creature is a Cleric or Wizard only at the time the ability is activated and at the time it resolves. It doesn\'t check at the time the damage is actually prevented.').
card_ruling('wandering mage', '2009-10-01', 'If you activate Wandering Mage\'s third ability, you put the -1/-1 counter on a creature you control as a cost. That means it happens when you activate the ability, not when it resolves. If paying the cost causes the only creature you control to have 0 toughness, it\'s put into the graveyard before you can pay the cost again.').

card_ruling('wandering stream', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('wandering stream', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('wandering stream', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('wandering wolf', '2012-05-01', 'The comparison of power is only done when blockers are declared. Increasing the power of a blocking creature (or decreasing the power of Wandering Wolf) after this point won\'t cause any creature to stop blocking or become unblocked.').

card_ruling('war behemoth', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('war behemoth', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('war behemoth', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('war behemoth', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('war behemoth', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('war behemoth', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('war behemoth', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('war behemoth', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('war behemoth', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('war cadence', '2004-10-04', 'You can choose X=0. This doesn\'t have an effect most of the time, but it does mean that creatures can\'t block unless the controller chooses to pay the cost (a cost of zero is not automatically paid). You can use this to make your creatures unable to block an attacker.').
card_ruling('war cadence', '2004-10-04', 'The ability only applies to blocks declared after it resolves. It will not add costs to any blockers already announced.').

card_ruling('war dance', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('war elephant', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('war elephant', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('war elephant', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('war elephant', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('war falcon', '2012-07-01', 'To attack with War Falcon, you must control a Knight or a Soldier when you declare attackers. Once War Falcon is attacking, it won\'t be removed from combat if at any point you no longer control a Knight or Soldier.').
card_ruling('war falcon', '2012-07-01', 'If an effect makes War Falcon a Knight or a Soldier, it will be able to attack even if it\'s the only creature you control.').

card_ruling('war oracle', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('war oracle', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('war oracle', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('war priest of thune', '2010-08-15', 'Unlike Manic Vandal\'s ability, War Priest of Thune\'s ability is not mandatory. If you\'re the only player who controls an enchantment, you must target one of them, but you don\'t have to destroy it.').

card_ruling('war report', '2011-06-01', 'Each artifact creature on the battlefield is counted twice.').

card_ruling('war tax', '2004-10-04', 'You can choose X=0. This doesn\'t have an effect most of the time, but it does mean that creatures can\'t attack unless the controller chooses to pay the cost (a cost of zero is not automatically paid, so the controller can choose to not pay).').
card_ruling('war tax', '2004-10-04', 'The ability only applies to attackers declared after it resolves. It will not add costs to any attackers already announced.').
card_ruling('war tax', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').

card_ruling('war\'s toll', '2006-05-01', 'Your opponent can tap lands for mana in response to the triggered ability. The effect won\'t prevent your opponent from casting spells or activating abilities, but it will make it difficult for that player to cast other spells or activate abilities later in the turn because players\' mana pools empty at the end of each step.').

card_ruling('war-name aspirant', '2014-09-20', 'Once a creature legally blocks War-Name Aspirant, changing that creature’s power to 1 or less won’t change or undo that block.').
card_ruling('war-name aspirant', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('war-name aspirant', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('war-wing siren', '2014-04-26', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('war-wing siren', '2014-04-26', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('war-wing siren', '2014-04-26', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('warbreak trumpeter', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').
card_ruling('warbreak trumpeter', '2004-10-04', 'The X in the ability has the same value as the X paid in the Morph ability. This is pretty easy to derive since there is no other source of X.').

card_ruling('warbringer', '2015-02-25', 'Warbringer’s first ability doesn’t affect Warbringer itself.').
card_ruling('warbringer', '2015-02-25', 'Warbringer’s first ability can’t affect the colored mana requirement of a dash cost. If a card’s dash cost includes one generic mana, that cost will be reduced by {1}.').
card_ruling('warbringer', '2015-02-25', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('warbringer', '2015-02-25', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('warbringer', '2015-02-25', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('warbringer', '2015-02-25', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('warchanter of mogis', '2014-02-01', 'Inspired abilities trigger no matter how the creature becomes untapped: by the turn-based action at the beginning of the untap step or by a spell or ability.').
card_ruling('warchanter of mogis', '2014-02-01', 'If an inspired ability triggers during your untap step, the ability will be put on the stack at the beginning of your upkeep. If the ability creates one or more token creatures, those creatures won’t be able to attack that turn (unless they gain haste).').
card_ruling('warchanter of mogis', '2014-02-01', 'Inspired abilities don’t trigger when the creature enters the battlefield.').
card_ruling('warchanter of mogis', '2014-02-01', 'If the inspired ability includes an optional cost, you decide whether to pay that cost as the ability resolves. You can do this even if the creature leaves the battlefield in response to the ability.').

card_ruling('ward of bones', '2008-08-01', 'Spelling this out completely: -- Each opponent who controls more creatures than you can\'t play creature cards. -- Each opponent who controls more artifacts than you can\'t play artifact cards. -- Each opponent who controls more enchantments than you can\'t play enchantment cards. -- Each opponent who controls more lands than you can\'t play land cards.').

card_ruling('ward of lights', '2009-10-01', 'The sacrifice occurs only if you cast it using its own ability. If you cast it using some other effect (for instance, if it gained flash from Vedalken Orrery), then it won\'t be sacrificed.').

card_ruling('ward of piety', '2005-02-01', 'Redirected damage retains its information. For example, your opponent attacks you with Slith Firewalker (which reads, in part, \"Whenever Slith Firewalker deals combat damage to a player, put a +1/+1 counter on it.\") and you block with a creature enchanted with Ward of Piety. If you activate Ward of Piety, targeting your opponent, the Slith\'s ability triggers during the combat damage step and it gets a +1/+1 counter. It\'s still combat damage.').

card_ruling('ward sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('ward sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('warden of evos isle', '2013-07-01', 'A creature spell that doesn’t have flying won’t cost less even if the creature it will become would have flying once on the battlefield. For example, if you control Galerider Sliver, other Sliver spells won’t cost {1} less even though those Slivers will have flying once they’re on the battlefield.').
card_ruling('warden of evos isle', '2013-07-01', 'The ability can’t reduce the colored mana requirement of a creature spell with flying.').

card_ruling('warden of the beyond', '2014-07-18', 'Warden of the Beyond can’t get more than +2/+2 from its last ability, no matter how many opponents own a card in exile or how many exiled cards they own.').

card_ruling('warden of the first tree', '2014-11-24', 'Neither the first nor the second ability has a duration. If one of them resolves, it will remain in effect until Warden of the First Tree leaves the battlefield or some subsequent effect changes its characteristics.').
card_ruling('warden of the first tree', '2014-11-24', 'The first and second activated abilities cause Warden of the First Tree to lose any other creature types it has. It retains any card types or supertypes it may have.').
card_ruling('warden of the first tree', '2014-11-24', 'The first ability overwrites any previous effects that set the creature’s base power and toughness to specific values. Any power- or toughness-setting effects that start to apply after the first ability resolves will overwrite this effect.').
card_ruling('warden of the first tree', '2014-11-24', 'Effects that modify Warden of the First Tree’s power or toughness, such as the one created by Titanic Growth, will apply to Warden of the First Tree no matter when they started to take effect. The same is true for any +1/+1 counters on Warden of the First Tree and effects that switch its power and toughness.').

card_ruling('warden of the wall', '2011-01-22', 'Warden of the Wall still has the ability to tap for mana when it\'s not your turn.').

card_ruling('wardscale dragon', '2014-11-24', '“Defending player” refers to the player Wardscale Dragon is attacking or the controller of the planeswalker Wardscale Dragon is attacking. In multiplayer formats that allow attacking multiple players, your other opponents can still cast spells, even if you control other creatures attacking those players or planeswalkers those players control.').
card_ruling('wardscale dragon', '2014-11-24', 'The defending player can still activate abilities or take special actions, such as turning a face-down creature face up.').

card_ruling('wargate', '2009-05-01', 'A permanent card is an artifact, creature, enchantment, land, or planeswalker card.').
card_ruling('wargate', '2009-05-01', 'You may always find a permanent card with converted mana cost 0. (If X is 0, that\'s all you can find.) Cards with no mana cost (such as land cards), cards with mana cost {0}, and cards with mana cost {X} all have converted mana cost 0.').

card_ruling('warleader\'s helix', '2013-04-15', 'If the target creature or player is an illegal target when Warleader\'s Helix tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 4 life.').

card_ruling('warmind infantry', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('warmind infantry', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('warmind infantry', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('warmonger hellkite', '2014-11-07', 'Each creature’s controller still chooses which player or planeswalker the creature attacks.').
card_ruling('warmonger hellkite', '2014-11-07', 'If, during a player’s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can’t attack, or hasn\'t been under that player’s control continuously since the turn began (and doesn’t have haste), then it doesn’t attack. If there’s a cost associated with having a creature attack, the player isn’t forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('warmonger hellkite', '2014-11-07', 'Only creatures that are attacking when the last ability resolves will get +1/+0. That is, activating that ability before attackers have been declared won’t give the bonus to any creatures, including ones that attack later in the turn.').
card_ruling('warmonger hellkite', '2014-11-07', 'If there are multiple combat phases in a turn, creatures must attack during each combat phase in which they’re able to.').

card_ruling('warmonger\'s chariot', '2010-06-15', 'The second ability doesn\'t cause the equipped creature to lose defender. It just lets it attack.').
card_ruling('warmonger\'s chariot', '2010-06-15', 'If the equipped creature doesn\'t have defender, the second ability simply doesn\'t affect it.').

card_ruling('warmth', '2004-10-04', 'It triggers on all opposing players.').

card_ruling('warp world', '2006-10-15', 'Anything that triggers during the resolution of this will wait to be put on the stack until everything is put onto the battlefield and resolution is complete. The player whose turn it is will put all of his or her triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').
card_ruling('warp world', '2009-10-01', 'Taking it slowly, here\'s what happens when Warp World resolves: 1) Each player counts the number of permanents he or she owns. 2) Each player shuffles those permanents into his or her library. 3) Each player reveals cards from the top of his or her library equal to the number that player counted. 4) Each player puts all artifact, land, and creature cards revealed this way onto the battlefield. All of these cards enter the battlefield at the same time. 5) Each player puts all enchantment cards revealed this way onto the battlefield. An Aura put onto the battlefield this way can enchant an artifact, land, or creature that was already put onto the battlefield, but can\'t enchant an enchantment that\'s being put onto the battlefield at the same time as it. If multiple players have Auras to put onto the battlefield, the player whose turn it is announces what his or her Auras will enchant, then each other player in turn order does the same, then all enchantments (both Auras and non-Auras) enter the battlefield at the same time. 6) Each player puts all of his or her other revealed cards (instants, sorceries, planeswalkers, and Auras that can\'t enchant anything) on the bottom of his or her library in any order.').
card_ruling('warp world', '2009-10-01', 'Tokens are permanents but not cards. They\'ll count toward the number of permanents shuffled into your library, so you\'ll get a card back for each token you owned. But the tokens themselves should be ignored while you\'re revealing *cards* from your library. In practice, you shouldn\'t actually shuffle them into your library since they\'ll cease to exist as soon as Warp World finishes resolving. Note that a token\'s owner is the player under whose control it first entered the battlefield; this is a change from previous rules.').

card_ruling('warped devotion', '2004-10-04', 'This card can trigger on itself being returned to a player\'s hand.').

card_ruling('warped physique', '2013-04-15', 'Use the number of cards in your hand as Warped Physique resolves to determine the value of X. Warped Physique won\'t be in your hand at that time.').
card_ruling('warped physique', '2013-04-15', 'Once Warped Physique resolves, the effect won\'t change if the number of cards in your hand changes.').

card_ruling('warped researcher', '2004-10-04', 'On your turn, an opponent can cycle a card that has a cycling triggered ability and target this card with that ability. This works on your turn because your triggered abilities go on the stack first, so your opponent\'s ability will resolve before yours. This situation is reversed on your opponent\'s turn.').

card_ruling('warren instigator', '2009-10-01', 'Warren Instigator\'s ability triggers when it deals any kind of damage to an opponent, not just combat damage.').
card_ruling('warren instigator', '2009-10-01', 'If Warren Instigator attacks an opponent and isn\'t blocked, its double strike ability will cause it to deal combat damage to that opponent twice, once during each combat damage step. Its triggered ability will thus trigger twice: Warren Instigator deals first-strike combat damage, its ability triggers and resolves, it deals regular combat damage, and its ability triggers and resolves again.').

card_ruling('warren pilferers', '2007-10-01', 'The ability checks whether the targeted card is a Goblin when the ability resolves.').
card_ruling('warren pilferers', '2007-10-01', 'If the card is an illegla target as the ability resolves, the ability will be countered and none of its effects will happen. Warren Pilferers won\'t gain haste.').

card_ruling('warrior en-kor', '2004-10-04', 'The ability of this card does not do anything to stop Trample damage from being assigned to the defending player.').
card_ruling('warrior en-kor', '2004-10-04', 'It can redirect damage to itself.').
card_ruling('warrior en-kor', '2004-10-04', 'It is possible to redirect more damage to a creature than that creature\'s toughness.').
card_ruling('warrior en-kor', '2004-10-04', 'You can use this ability as much as you want prior to damage being dealt.').
card_ruling('warrior en-kor', '2013-07-01', 'When you redirect combat damage it is still combat damage.').

card_ruling('warrior\'s honor', '2007-07-15', 'Warrior\'s Honor affects creatures you control that are on the battlefield at the time it resolves. If you put a creature onto the battlefield later in the turn, that creature won\'t get the bonus.').

card_ruling('warstorm surge', '2011-09-22', 'The creature that entered the battlefield deals damage equal to its current power to the targeted creature or player. If it\'s no longer on the battlefield, its last known existence on the battlefield is checked to determine its power.').
card_ruling('warstorm surge', '2011-09-22', 'Warstorm Surge is the source of the ability, but the creature is the source of the damage. The ability couldn\'t target a creature with protection from red, for example. It could target a creature with protection from creatures, but all the damage would be prevented. Since damage is dealt by the creature, abilities like lifelink, deathtouch and infect are taken into account, even if the creature has left the battlefield by the time it deals damage.').

card_ruling('wasteland strangler', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('wasteland strangler', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('wasteland strangler', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('wasteland strangler', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('wasteland strangler', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('wasteland strangler', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('wasteland strangler', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('wasteland strangler', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('wasteland strangler', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').

card_ruling('watchdog', '2008-04-01', 'The second ability is no longer triggered. It continuously checks the tapped/untapped status of the Watchdog, and gives them -1/-0 as appropriate.').

card_ruling('watcher of the roost', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('watcher of the roost', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('watcher of the roost', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('watcher of the roost', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('watcher of the roost', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('watcher of the roost', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('watcher of the roost', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('watcher of the roost', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('watcher of the roost', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('watcher sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('watcher sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('water servant', '2010-08-15', 'You can activate Water Servant\'s second ability even if doing so would cause Water Servant\'s power to be less than 0.').

card_ruling('water wurm', '2004-10-04', 'Only gets the bonus once even if more than one opponent has an Island on the battlefield.').

card_ruling('waterspout djinn', '2004-10-04', 'You choose a land to return and return it during resolution of the upkeep triggered ability. This means if you have two Waterspout Djinns, they must each return different Islands.').

card_ruling('waterspout elemental', '2004-10-04', 'It affects your creatures as well as your opponent\'s.').

card_ruling('waterspout weavers', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('waterspout weavers', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('waterspout weavers', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('waterspout weavers', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('waterspout weavers', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('waterspout weavers', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('watery grave', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('watery grave', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('watery grave', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('wave of terror', '2005-06-01', 'Will not generally kill zero cost creatures because the age counters go on before the ability triggers. If would be possible if something like Power Conduit were removing counters.').
card_ruling('wave of terror', '2008-04-01', 'This destroys creatures with converted mana cost exactly equal to the appropriate number, not equal to or less than that number.').
card_ruling('wave of terror', '2008-04-01', 'If you don\'t pay the cumulative upkeep, you\'ll sacrifice Wave of Terror before your draw step begins. It won\'t destroy any creatures that turn.').

card_ruling('wave-wing elemental', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('wave-wing elemental', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('wave-wing elemental', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('wavecrash triton', '2013-09-15', 'Wavecrash Triton’s ability can target a creature that’s already tapped. That creature still won’t untap during its controller’s next untap step.').
card_ruling('wavecrash triton', '2013-09-15', 'Wavecrash Triton’s ability tracks the creature, but not its controller. If the creature changes controllers before its first controller’s next untap step has come around, then it won’t untap during its new controller’s next untap step.').
card_ruling('wavecrash triton', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('wavecrash triton', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('wavecrash triton', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('waves of aggression', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('waves of aggression', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('waves of aggression', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('waves of aggression', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').
card_ruling('waves of aggression', '2008-08-01', 'Waves of Aggression untaps all creatures that attacked this turn, not just those that attacked during the most recent combat phase.').
card_ruling('waves of aggression', '2008-08-01', 'If it\'s somehow not a main phase when Waves of Aggression resolves, all it does is untap all creatures that attacked that turn. No new phases are created.').

card_ruling('waveskimmer aven', '2008-10-01', 'If you declare exactly one creature as an attacker, each exalted ability on each permanent you control (including, perhaps, the attacking creature itself) will trigger. The bonuses are given to the attacking creature, not to the permanent with exalted. Ultimately, the attacking creature will wind up with +1/+1 for each of your exalted abilities.').
card_ruling('waveskimmer aven', '2008-10-01', 'If you attack with multiple creatures, but then all but one are removed from combat, your exalted abilities won\'t trigger.').
card_ruling('waveskimmer aven', '2008-10-01', 'Some effects put creatures onto the battlefield attacking. Since those creatures were never declared as attackers, they\'re ignored by exalted abilities. They won\'t cause exalted abilities to trigger. If any exalted abilities have already triggered (because exactly one creature was declared as an attacker), those abilities will resolve as normal even though there may now be multiple attackers.').
card_ruling('waveskimmer aven', '2008-10-01', 'Exalted abilities will resolve before blockers are declared.').
card_ruling('waveskimmer aven', '2008-10-01', 'Exalted bonuses last until end of turn. If an effect creates an additional combat phase during your turn, a creature that attacked alone during the first combat phase will still have its exalted bonuses in that new phase. If a creature attacks alone during the second combat phase, all your exalted abilities will trigger again.').
card_ruling('waveskimmer aven', '2008-10-01', 'In a Two-Headed Giant game, a creature \"attacks alone\" if it\'s the only creature declared as an attacker by your entire team. If you control that attacking creature, your exalted abilities will trigger but your teammate\'s exalted abilities won\'t.').

card_ruling('wayfaring giant', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('wayfaring giant', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('wayfaring giant', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('wayfaring temple', '2012-10-01', 'The ability that defines Wayfaring Temple\'s power and toughness works in all zones, not just the battlefield.').
card_ruling('wayfaring temple', '2012-10-01', 'As long as Wayfaring Temple is on the battlefield, its first ability will count itself.').
card_ruling('wayfaring temple', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('wayfaring temple', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('wayfaring temple', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('wayfaring temple', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('wayfaring temple', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('wayfaring temple', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('wayward angel', '2004-10-04', 'When you have seven or more cards in your graveyard, this card is black. It is not white and black.').

card_ruling('weakstone', '2004-10-04', 'The -1/-0 applies to attacking creatures from all players.').

card_ruling('weapon surge', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('weapon surge', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('weapon surge', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('weapon surge', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('weapon surge', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('weapon surge', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('weapon surge', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('wear', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('wear', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('wear', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('wear', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('wear', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('wear', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('wear', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('wear', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('wear', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('wear', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('wear', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').

card_ruling('wear away', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('wear away', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('wear away', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('wear away', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('weathered bodyguards', '2006-09-25', 'Weathered Bodyguards affects only combat damage from unblocked creatures. It won\'t affect noncombat damage, and it won\'t affect trample damage.').

card_ruling('weatherseed totem', '2006-09-25', 'Weatherseed Totem will be returned to its owner\'s hand if it was a creature at the time it was put into a graveyard from the battlefield.').
card_ruling('weatherseed totem', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('weaver of lies', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('web of inertia', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('weed-pruner poplar', '2008-04-01', 'The ability is mandatory. If you\'re the only player who controls any other creatures, you must target one of your own creatures with the ability.').

card_ruling('wei elite companions', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('wei night raiders', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').
card_ruling('wei night raiders', '2009-10-01', 'Wei Night Raiders\'s second ability triggers whenever it deals damage to an opponent for any reason. It\'s not limited to combat damage.').

card_ruling('wei scout', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('wei strike force', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('weight of conscience', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap creatures that haven\'t been under your control since your most recent turn began to pay the cost.').
card_ruling('weight of conscience', '2008-04-01', 'The ability exile the enchanted creature, but Weight of Conscience remains on the battlefield. The Aura will then be put into your graveyard as a state-based action.').

card_ruling('welkin hawk', '2004-10-04', 'You do not have to find a Welkin Hawk card if you do not want to.').

card_ruling('well', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('well', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('well', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('well', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('well', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('well', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('well', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('well', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('well', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('well', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('well', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('well', '2013-04-15', 'Count the number of creatures you control when Well resolves to determine how much life you gain.').
card_ruling('well', '2013-04-15', 'If you cast Alive // Well as a fused split spell, the Centaur creature token will count toward the amount of life you gain.').

card_ruling('well of knowledge', '2004-10-04', 'Each player may use this ability as many times as they choose during their turn\'s draw step.').

card_ruling('wellgabber apothecary', '2007-10-01', 'The targeted creature must be tapped when the ability is activated and when it resolves. It doesn\'t need to remain tapped in between.').
card_ruling('wellgabber apothecary', '2007-10-01', 'Once the ability has resolved, all damage that would be dealt to that creature for the rest of the turn will be prevented, regardless of whether it\'s tapped or untapped at the time that damage would be dealt.').

card_ruling('wellspring', '2004-10-04', 'If Wellspring leaves the battlefield, the control effect still lasts until end of turn.').

card_ruling('wheel and deal', '2004-10-04', 'You can choose to target zero opponents.').
card_ruling('wheel and deal', '2004-10-04', 'You can\'t target yourself.').

card_ruling('wheel of fate', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('wheel of fate', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('wheel of fate', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('wheel of fate', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('wheel of fate', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('wheel of fate', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('wheel of fate', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('wheel of fate', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('wheel of fate', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('wheel of fate', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('wheel of fate', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('wheel of fate', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('wheel of fate', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('wheel of sun and moon', '2008-05-01', 'Wheel of Sun and Moon\'s replacement effect will apply to cards that would be put into the enchanted player\'s graveyard from any game zone. This includes from the battlefield (if a nontoken permanent is destroyed or would otherwise be put into the graveyard), from the stack (if a spell is countered, or if an instant or sorcery spell resolves), from the player\'s hand or library, and so on.').
card_ruling('wheel of sun and moon', '2008-05-01', 'Wheel of Sun and Moon won\'t affect tokens that are put into the graveyard from the battlefield or copies of spells that resolve or are countered. They\'re not cards, so they\'ll go to the graveyard as normal, then cease to exist.').
card_ruling('wheel of sun and moon', '2008-05-01', 'If multiple cards would be put into the enchanted player\'s graveyard at the same time (due to Millstone, for example), they are instead all revealed and put on the bottom of the enchanted player\'s library at the same time. That player chooses what order to put them in. The order is not revealed to the other players.').

card_ruling('wheel of torture', '2004-10-04', 'The number of cards is counted during resolution.').

card_ruling('where ancients tread', '2008-10-01', 'The ability checks that creature\'s power only once: when that creature enters the battlefield. The trigger checks a creature\'s initial power upon being put on the battlefield, so it will take into account counters that it enters the battlefield with and static abilities that may give it a continuous power boost once it\'s on the battlefield (such as the one on Glorious Anthem). After the creature is already on the battlefield, boosting its power with a spell (such as Giant Growth), activated ability, or triggered ability won\'t allow this ability to trigger; it\'s too late by then. Once the ability triggers, it will resolve no matter what the creature\'s power may become while the ability is on the stack.').

card_ruling('whetstone', '2004-10-04', 'Can be used if a player has less than 2 cards in their library. It will remove 0 or 1 cards if that is all that is available.').
card_ruling('whetstone', '2004-10-04', 'It is not a draw effect so it will not cause a player with less than 2 cards in their library to lose.').
card_ruling('whetstone', '2004-10-04', 'Since you are putting more than one card in the graveyard at one time, each affected player can choose the order the 2 cards go in.').

card_ruling('which of you burns brightest?', '2010-06-15', 'You choose the target opponent when the ability triggers. As the ability resolves, you choose a value for X and decide whether to pay {X}. If you do decide to pay {X}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('whim of volrath', '2004-10-04', 'Alters all occurrences of the chosen word in the text box and the type line of the given card.').
card_ruling('whim of volrath', '2004-10-04', 'Can target a card with no appropriate words on it, or even one with no words at all.').
card_ruling('whim of volrath', '2004-10-04', 'It can\'t change a word to the same word. It must be a different word.').
card_ruling('whim of volrath', '2004-10-04', 'It only changes what is printed on the card (or set on a token when it was created or set by a copy effect). It will not change any effects that are on the permanent.').
card_ruling('whim of volrath', '2004-10-04', 'You choose the words to change on resolution.').
card_ruling('whim of volrath', '2004-10-04', 'You can\'t change proper nouns (i.e. card names) such as \"Island Fish Jasconius\".').
card_ruling('whim of volrath', '2004-10-04', 'It can be used to change a land\'s type from one basic land type to another. For example, Forest can be changed to Island so it produces blue mana. It doesn\'t change the name of any permanent.').

card_ruling('whims of the fates', '2014-02-01', 'To choose a pile at random, each pile must have an equal chance of being chosen. There are many ways to do this, including assigning each pile a number and rolling a die.').
card_ruling('whims of the fates', '2014-02-01', 'You may put Auras or Equipment into one pile and the creatures they were attached to into a different pile.').
card_ruling('whims of the fates', '2014-02-01', 'If you put all permanents you control into one pile, you’ll have a two-thirds chance of not having to sacrifice any permanents, but a one-third chance of having to sacrifice all of them.').

card_ruling('whip of erebos', '2013-09-15', 'At the beginning of the next end step, the creature returned to the battlefield with Whip of Erebos is exiled. This is a delayed triggered ability. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won’t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('whip of erebos', '2013-09-15', 'Whip of Erebos grants haste to the creature that’s returned to the battlefield. However, neither of the “exile” abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('whip of erebos', '2013-09-15', 'If a creature returned to the battlefield with Whip of Erebos would leave the battlefield for any reason, it’s exiled instead. However, if that creature is already being exiled, then the replacement effect won’t apply. If the spell or ability that exiles it later returns it to the battlefield (as Chained to the Rocks might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The effects from Whip of Erebos will no longer apply to it.').
card_ruling('whip of erebos', '2013-09-15', 'The exiled creature is never put into the graveyard. Any abilities the creature has that trigger when it dies won’t trigger.').

card_ruling('whipgrass entangler', '2004-10-04', 'If you use this ability on the same creature more than once, the cost is cumulative.').

card_ruling('whipkeeper', '2004-10-04', 'If the creature regenerated during the turn, and thereby had all damage on it removed, the damage before it regenerated was still dealt that turn and will be counted.').
card_ruling('whipkeeper', '2004-10-04', 'This counts all damage that was dealt during the turn. This does not include any damage that was prevented.').

card_ruling('whiplash trap', '2009-10-01', 'You may ignore a Trap’s alternative cost condition and simply cast it for its normal mana cost. This is true even if its alternative cost condition has been met.').
card_ruling('whiplash trap', '2009-10-01', 'Casting a Trap by paying its alternative cost doesn\'t change its mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('whiplash trap', '2009-10-01', 'Effects that increase or reduce the cost to cast a Trap will apply to whichever cost you chose to pay.').
card_ruling('whiplash trap', '2009-10-01', 'You must target two creatures as you cast Whiplash Trap. If you can\'t (because just one creature is on the battlefield, perhaps), you can\'t cast the spell.').
card_ruling('whiplash trap', '2009-10-01', 'You can target any two creatures, not just ones that entered the battlefield this turn.').

card_ruling('whirler rogue', '2015-06-22', 'You may tap any two untapped artifacts you control, including artifact creatures that haven’t been under your control continuously since the beginning of your most recent turn.').
card_ruling('whirler rogue', '2015-06-22', 'Activating the second ability of Whirler Rogue after a creature has become blocked won’t cause it to become unblocked.').

card_ruling('whirling catapult', '2008-10-01', 'If your library has fewer than two cards in it, you can\'t activate the ability.').

card_ruling('whirling dervish', '2004-10-04', 'If it damages the opponent multiple times in a turn, it gets only one +1/+1 counter at end of turn.').

card_ruling('whirlpool whelm', '2007-10-01', 'You choose the target when you play Whirlpool Whelm, not when you clash.').
card_ruling('whirlpool whelm', '2007-10-01', 'If you don\'t win the clash, the targeted creature always goes to its owner\'s hand. If you win the clash, you choose whether the creature goes to its owner\'s hand or the top of its owner\'s library.').
card_ruling('whirlpool whelm', '2007-10-01', 'If you clash with the owner of the targeted creature and win, the owner of the creature decides where to put to card revealed during the clash before you decide whether to put the creature on top of that library. You know the player\'s decision before you make your decision.').

card_ruling('whirlwind adept', '2014-09-20', 'Any spell you cast that doesn’t have the type creature will cause prowess to trigger. If a spell has multiple types, and one of those types is creature (such as an artifact creature), casting it won’t cause prowess to trigger. Playing a land also won’t cause prowess to trigger.').
card_ruling('whirlwind adept', '2014-09-20', 'Prowess goes on the stack on top of the spell that caused it to trigger. It will resolve before that spell.').
card_ruling('whirlwind adept', '2014-09-20', 'Once it triggers, prowess isn’t connected to the spell that caused it to trigger. If that spell is countered, prowess will still resolve.').

card_ruling('whispergear sneak', '2014-05-29', 'If you look at a booster pack not being drafted during the current draft round, open that booster pack, look at the cards, and return them in a face-down pile.').
card_ruling('whispergear sneak', '2014-05-29', 'If you use Whispergear Sneak during the first draft round to look at an unopened booster pack, the owner of that booster pack doesn’t have to “open” that booster pack for the next draft round, although he or she may do so.').
card_ruling('whispergear sneak', '2014-05-29', 'If you look at a booster pack that is being drafted during the current draft round, you may do so after a player has drafted a card from that booster pack and before the next player has been passed the cards. As a courtesy, you may inform the player currently looking at the booster pack that you’ll be looking at it next, before it’s passed to the next player.').

card_ruling('whispering madness', '2013-01-24', 'Each player draws cards simultaneously. If this causes a player to draw more cards than are left in his or her library, that player will lose the game. If this causes all players to do so, the game is a draw. (In multiplayer games, multiple players drawing from an empty library will cause those players to lose although the game may continue.)').
card_ruling('whispering madness', '2013-01-24', 'You\'ll draw cards before deciding which creature (if any) Whispering Madness will be encoded on.').
card_ruling('whispering madness', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('whispering madness', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('whispering madness', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('whispering madness', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('whispering madness', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('whispering madness', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('whispering madness', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('whispering madness', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('whispering madness', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('whispering madness', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('whispering madness', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('whispering specter', '2011-06-01', 'You choose whether or not to sacrifice Whispering Specter, and the number of poison counters the player has is counted, when the triggered ability resolves.').

card_ruling('whisperwood elemental', '2014-11-24', 'The last ability grants that ability to face-up nontoken creatures you control as that ability resolves. Creatures that come under your control later in the turn won’t have that ability.').
card_ruling('whisperwood elemental', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('whisperwood elemental', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('whisperwood elemental', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('whisperwood elemental', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('whisperwood elemental', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('whisperwood elemental', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('whisperwood elemental', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('whisperwood elemental', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('whisperwood elemental', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('whisperwood elemental', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('whisperwood elemental', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('whisperwood elemental', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('whisperwood elemental', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('white mana battery', '2004-10-04', 'Can be tapped even if it has no counters.').

card_ruling('white sun\'s zenith', '2011-06-01', 'If this spell is countered, none of its effects occur. In particular, it will go to the graveyard rather than to its owner\'s library.').

card_ruling('whitemane lion', '2007-02-01', 'You may return this creature itself to its owner\'s hand. If you control no other creatures, you must return it.').
card_ruling('whitemane lion', '2007-02-01', 'The ability doesn\'t target what you return. You don\'t choose what to return until the ability resolves. No one can respond to the choice.').

card_ruling('whitewater naiads', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('whitewater naiads', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('whitewater naiads', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('wicked reward', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('wicked reward', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('wicker warcrawler', '2008-05-01', 'The -1/-1 counter is put on Wicker Warcrawler at end of combat, after combat damage has already been dealt. (Note that this is different from how Dusk Urchins works.)').

card_ruling('widespread panic', '2013-10-17', 'Widespread Panic’s ability is put onto the stack after the library is shuffled.').

card_ruling('wight of precinct six', '2013-01-24', 'Wight of Precinct Six\'s ability applies only if Wight of Precinct Six is on the battlefield.').

card_ruling('wiitigo', '2004-10-04', 'Any +1/+1 counter from any source can be used with his ability.').

card_ruling('wild beastmaster', '2012-10-01', 'The value of X is determined when the triggered ability resolves. If Wild Beastmaster is no longer on the battlefield at that time, use its last known power to determine the value of X. This could be bad for you if Wild Beastmaster\'s power was negative. For example, if Wild Beastmaster\'s power is -4, each other creature you control will get -4/-4 until end of turn.').

card_ruling('wild cantor', '2006-02-01', 'While casting a spell with convoke, if you sacrifice Wild Cantor to add mana to your mana pool, Wild Cantor won’t be on the battlefield when you pay that spell’s costs. It can\'t be tapped to help cast that spell using convoke.').

card_ruling('wild defiance', '2012-05-01', 'Wild Defiance\'s ability will resolve before that instant or sorcery spell.').
card_ruling('wild defiance', '2012-05-01', 'If an instant or sorcery spell targets the same creature you control more than once, Wild Defiance will trigger only once.').

card_ruling('wild dogs', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('wild evocation', '2010-08-15', 'If a player reveals a nonland card, that player must cast that card if it\'s possible to do so, even if he or she doesn\'t want to. The player casts it as part of the resolution of Wild Evocation\'s ability. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other play restrictions are not (such as \"Cast [this card] only during combat\").').
card_ruling('wild evocation', '2010-08-15', 'If a player reveals a nonland card, but can\'t cast it due to a lack of legal targets or a play restriction, it simply remains in his or her hand.').
card_ruling('wild evocation', '2010-08-15', 'If, as the result of Wild Evocation\'s ability, a player casts a card \"without paying its mana cost,\" any X in the mana cost will be 0, and that player can\'t pay any alternative costs (such as Demon of Death\'s Gate\'s alternative cost). On the other hand, if the card has optional additional costs (such as kicker or multikicker), the player may pay those.').
card_ruling('wild evocation', '2010-08-15', 'If casting the revealed card involves paying a mandatory additional cost (such as the one Fling has), the player casting that card must pay that cost if able. If he or she can\'t, the card remains uncast in his or her hand. If the mandatory additional cost includes a mana payment, the situation is more complex. If the player has enough mana in his or her mana pool to pay the cost, that player must do so. If the player can\'t possibly pay the cost, the card remains uncast in his or her hand. However, if the player has the means to produce enough mana to pay the cost, then he or she has a choice: The player may cast the card, produce mana, and pay the cost. Or the player may choose to activate no mana abilities, thus making the card impossible to cast because the additional mana can\'t be paid.').
card_ruling('wild evocation', '2010-08-15', 'If a player casts a nonland card revealed with Wild Evocation\'s ability, it\'s put on the stack as a spell, then Wild Evocation\'s ability finishes resolving. The spell will then resolve as normal, after players get a chance to cast spells and activate abilities.').

card_ruling('wild growth', '2004-10-04', 'The additional mana is not an ability of the land and is not something the land can produce.').

card_ruling('wild guess', '2012-07-01', 'Because discarding a card is an additional cost, you can\'t cast Wild Guess if you have no other cards in hand.').

card_ruling('wild instincts', '2015-06-22', 'You must target both a creature you control and a creature an opponent controls to cast Wild Instincts.').
card_ruling('wild instincts', '2015-06-22', 'If the creature an opponent controls is an illegal target as Wild Instincts tries to resolve, but the creature you control is still a legal target, the creature you control will get +2/+2, but the creatures won’t fight. Neither creature will deal or be dealt damage during the resolution of Wild Instincts.').

card_ruling('wild might', '2004-10-04', 'Each player gets the option to pay when the spell resolves.').

card_ruling('wild nacatl', '2008-10-01', 'Wild Nacatl\'s abilities check for lands with the subtypes Mountain and Plains. Those lands don\'t need to be named Mountain and Plains. Wild Nacatl will get both bonuses if those two subtypes are contained among the lands you control, even if they\'re from the same land (such as Plateau or Sacred Foundry).').

card_ruling('wild pair', '2007-02-01', 'If you cast a 2/2 Grizzly Bears, for example, you may then search for any creature with total power and toughness 4. It can be 0/4, 1/3, 2/2, or 3/1.').
card_ruling('wild pair', '2007-02-01', 'The game checks the power and toughness of the creature that just entered the battlefield as it exists on the battlefield. That might mean it\'s impossible to get another copy of that same card. For example, a Triskelion on the battlefield is 4/4, but a Triskelion in your library is 1/1. The power and toughness might also have been affected while the triggered ability was on the stack (such as with Giant Growth, Sudden Death, or Ovinize).').
card_ruling('wild pair', '2007-02-01', 'If the creature that entered the battlefield has left the battlefield by the time the ability resolves, use the power and toughness of that creature as it last appeared before leaving the battlefield.').
card_ruling('wild pair', '2007-02-01', 'If the total power and toughness of the creature that entered the battlefield is less than 0 (due to Bewilder or Sudden Death, for example), it is treated as 0 by Wild Pair. For example, if a creature was -2/1, you could search for a creature with total power and toughness of 0. If either the power or toughness is less than 0, but the total is greater than 0, use the actual total. For example, if the creature is -1/3, you could search for a creature with total power and toughness of 2.').

card_ruling('wild research', '2004-10-04', 'You do not have to find an enchantment or instant card if you do not want to.').

card_ruling('wild ricochet', '2013-07-01', 'Wild Ricochet can target (and copy) any instant or sorcery spell, not just one with targets. It doesn’t matter who controls it.').
card_ruling('wild ricochet', '2013-07-01', 'When Wild Ricochet resolves, it creates a copy of a spell. You control the copy. The controller of the original spell retains control of that spell. The copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities. The copy resolves before the original spell.').
card_ruling('wild ricochet', '2013-07-01', 'The copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('wild ricochet', '2013-07-01', 'If the spell Wild Ricochet copies is modal (that is, it says “Choose one —” or the like), the copy will have the same mode. You can’t choose a different one.').
card_ruling('wild ricochet', '2013-07-01', 'If the spell Wild Ricochet copies has an X whose value was determined as it was cast (like Volcanic Geyser does), the copy has the same value of X.').
card_ruling('wild ricochet', '2013-07-01', 'You can’t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too. For example, if a player sacrifices a 3/3 creature to cast Fling, and you copy it with Wild Ricochet, the copy of Fling will also deal 3 damage to its target.').
card_ruling('wild ricochet', '2013-07-01', 'If the copy says that it affects “you,” it affects the controller of the copy, not the controller of the original spell. Similarly, if the copy says that it affects an “opponent,” it affects an opponent of the copy’s controller, not an opponent of the original spell’s controller.').

card_ruling('wild slash', '2014-11-24', 'Wild Slash’s ferocious ability applies to all damage that would be dealt that turn, including the damage Wild Slash deals.').
card_ruling('wild slash', '2014-11-24', 'If you control a creature with power 4 or greater as Wild Slash resolves, the ferocious ability will apply. Damage dealt that turn can’t be prevented, even if you no longer control a creature with power 4 or greater as that damage would be dealt.').
card_ruling('wild slash', '2014-11-24', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('wild swing', '2008-05-01', 'You target three permanents as you cast Wild Swing. You don\'t randomly choose which one will be destroyed until Wild Swing resolves. If one of those permanents has become an illegal target by then, you randomly choose between the other two. If two of those permanents have become illegal targets by then, there is no random choice -- the remaining permanent is destroyed.').
card_ruling('wild swing', '2008-05-01', 'As Wild Swing resolves, there is no time to react between the time a permanent is chosen at random and the time it\'s destroyed. If you want to put a regeneration shield on one of those permanents, or sacrifice it for some effect, or anything else, you must do so before Wild Swing resolves (and before you know which one of the permanents will be chosen at random).').
card_ruling('wild swing', '2013-07-01', 'If one of the targets has indestructible, it can be chosen at random but nothing will happen to it if it is.').

card_ruling('wildcall', '2014-11-24', 'If you choose 0 for X, you’ll just manifest the top card of your library.').
card_ruling('wildcall', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('wildcall', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('wildcall', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('wildcall', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('wildcall', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('wildcall', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('wildcall', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('wildcall', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('wildcall', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('wildcall', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('wildcall', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('wildcall', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('wildcall', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('wildfield borderpost', '2009-05-01', 'Casting this card by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast an artifact spell. It also doesn\'t change the spell\'s mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('wildfield borderpost', '2009-05-01', 'Effects that increase or reduce the cost to cast this card will apply to whichever cost you chose to pay.').
card_ruling('wildfield borderpost', '2009-05-01', 'To satisfy the alternative cost, you may return any basic land you control to its owner\'s hand, regardless of that land\'s subtype or whether it\'s tapped.').
card_ruling('wildfield borderpost', '2009-05-01', 'As you cast a spell, you get a chance to activate mana abilities before you pay that spell\'s costs. Therefore, you may tap a basic land for mana, then both spend that mana and return that land to your hand to pay this card\'s alternative cost. (Of course, you can return a different basic land instead.)').

card_ruling('wildfire cerberus', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('wildfire cerberus', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('wildfire cerberus', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('will of the naga', '2014-11-24', 'Will of the Naga can target creatures that are already tapped. Those creatures won’t untap during their controller’s next untap step.').
card_ruling('will of the naga', '2014-11-24', 'Will of the Naga tracks the creatures, but not their controllers. If any of those creatures changes controllers before its first controller’s next untap step, then it won’t untap during its new controller’s next untap step.').
card_ruling('will of the naga', '2014-11-24', 'If you chose two targets and one is an illegal target as Will of the Naga resolves, that creature won’t become tapped and it won’t be stopped from untapping during its controller’s next untap step. It won’t be affected by Will of the Naga in any way.').
card_ruling('will of the naga', '2014-11-24', 'You exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('will of the naga', '2014-11-24', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, the converted mana cost of Tasigur’s Cruelty (with mana cost {5}{B}) is 6 even if you exile three cards to cast it.').
card_ruling('will of the naga', '2014-11-24', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('will of the naga', '2014-11-24', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than five cards from your graveyard to cast Tasigur’s Cruelty.').
card_ruling('will of the naga', '2014-11-24', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('will-forged golem', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('will-forged golem', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('will-forged golem', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('will-forged golem', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('will-forged golem', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('will-forged golem', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('will-forged golem', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('willbender', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').
card_ruling('willbender', '2013-04-15', 'If a spell with split second is on the stack, you can still respond by turning this creature face up and targeting that spell with the trigger. This is because split second only stops players from casting spells or activating abilities, while turning a creature face up is a special action.').

card_ruling('willbreaker', '2015-06-22', 'If Willbreaker leaves the battlefield, you no longer control it, and its control-change effect ends.').
card_ruling('willbreaker', '2015-06-22', 'If you lose control of Willbreaker before its ability resolves, you won’t gain control of the creature at all.').
card_ruling('willbreaker', '2015-06-22', 'If another player gains control of Willbreaker, its control-change effect ends. Regaining control of Willbreaker won’t cause you to regain control of the creature.').

card_ruling('willing', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('willing', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('willing', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('willing', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('willing', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('willing', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('willing', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('willing', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('willing', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('willing', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('willing', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('willing', '2013-04-15', 'Multiple instances of deathtouch or lifelink are redundant.').
card_ruling('willing', '2013-04-15', 'A creature that comes under your control after Willing resolves won\'t have deathtouch or lifelink, because adding abilities does change a creature\'s characteristics.').

card_ruling('willow priestess', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').

card_ruling('willow satyr', '2009-10-01', 'If Willow Satyr leaves the battlefield, you no longer control it, so the duration of its control-change effect ends.').
card_ruling('willow satyr', '2009-10-01', 'If Willow Satyr stops being tapped before its ability resolves -- even if it becomes tapped again right away -- you won\'t gain control of the targeted creature at all. The same is true if you lose control of Willow Satyr before the ability resolves.').
card_ruling('willow satyr', '2009-10-01', 'Once the ability resolves, it doesn\'t care whether the targeted permanent remains a legal target for the ability, only that it\'s on the battlefield. The effect will continue to apply to it even if it gains shroud, stops being a creature, stops being legendary, or would no longer be a legal target for any other reason.').

card_ruling('wilt-leaf liege', '2008-05-01', 'The abilities are separate and cumulative. If another creature you control is both of the listed colors, it will get a total of +2/+2.').

card_ruling('wind shear', '2004-10-04', 'The -2/-2 and loss of Flying both last until end of turn. The -2/-2 is not permanent.').

card_ruling('wind zendikon', '2010-03-01', 'The enchanted permanent will be both a land and a creature and can be affected by anything that affects either a land or a creature.').
card_ruling('wind zendikon', '2010-03-01', 'When a land becomes a creature, that doesn\'t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('wind zendikon', '2010-03-01', 'An attacking or blocking creature that stops being a creature is removed from combat. This can happen if a Zendikon enchanting an attacking or blocking creature leaves the battlefield, for example. The permanent that was removed from combat neither deals nor is dealt combat damage. Any attacking creature that the land creature was blocking remains blocked, however.').
card_ruling('wind zendikon', '2010-03-01', 'An ability that turns a land into a creature also sets that creature\'s power and toughness. If the land was already a creature, this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness, such as the effects of Disfigure or Glorious Anthem, will continue to apply, no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('wind zendikon', '2010-03-01', 'If a Zendikon and the land it\'s enchanting are destroyed at the same time (due to Akroma\'s Vengeance, for example), the Zendikon\'s last ability will still trigger.').
card_ruling('wind zendikon', '2010-03-01', 'If a Zendikon\'s last ability triggers, but the land card it refers to leaves the graveyard before it resolves, it will resolve but do nothing.').

card_ruling('windborn muse', '2004-10-04', 'If you have more than one of these on the battlefield, the cost is cumulative.').
card_ruling('windborn muse', '2007-02-01', 'In the Two-Headed Giant format, you still only have to pay once per creature.').
card_ruling('windborn muse', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('windborne charge', '2009-10-01', 'You must target two creatures you control as you cast Windborne Charge. If you can\'t (because you control just one creature, perhaps), you can\'t cast the spell.').

card_ruling('windbrisk heights', '2007-10-01', 'At the time the ability resolves, you\'ll get to play the card if you declared three different creatures as attackers at any point in the turn. A creature declared as an attacker in two different attack phases counts only once. A creature that entered the battlefield attacking (such as a token created by Militia\'s Pride) doesn\'t count because you never attacked with it.').

card_ruling('windbrisk raptor', '2008-05-01', 'This is a static ability. Your creatures will have lifelink from the moment they\'re declared as attackers until the moment the combat phase ends, they\'re removed from combat, or Windbrisk Raptor leaves the battlefield, whichever comes first.').

card_ruling('winding canyons', '2008-04-01', 'The ability isn\'t limited to creature cards in your hand. If an effect allows you to cast creature cards from some other zone, then Winding Canyons will allow you to do so as though they had flash.').

card_ruling('windreader sphinx', '2013-07-01', 'It doesn’t matter who controls the creature with flying or what player or planeswalker that creature is attacking.').
card_ruling('windreader sphinx', '2013-07-01', 'Windreader Sphinx’s ability will trigger when it itself attacks.').
card_ruling('windreader sphinx', '2013-07-01', 'The creature must have flying when declared as an attacking creature in order for Windreader Sphinx’s ability to trigger. For example, attacking with Trained Condor (a creature with flying and “Whenever Trained Condor attacks, another target creature you control gains flying until end of turn.”) and a creature without flying will cause the ability to trigger only once.').

card_ruling('windreaver', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('windreaver', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('windriddle palaces', '2012-06-01', 'You can only play the top cards of players\' libraries when you are the planar controller. Generally, this means as long as it\'s your turn. When another player begins his or her turn, that player becomes the planar controller and can play the top card of players\' libraries. You won\'t be able to until you again become the planar controller.').
card_ruling('windriddle palaces', '2012-06-01', 'You may play the top card of any player\'s library, including your own. This effect doesn\'t change when you can play any of these cards (meaning play it if it\'s a land card or cast it if it\'s a nonland card). You can still cast creature spells only on your turn during your main phase and so on.').
card_ruling('windriddle palaces', '2012-06-01', 'If the top card of a player\'s library is a land card, you may play it only if you haven\'t played a land yet that turn.').
card_ruling('windriddle palaces', '2012-06-01', 'As soon as you finish playing the card on top of a player\'s library, the next card in that library becomes revealed.').
card_ruling('windriddle palaces', '2012-06-01', 'If a player draws multiple cards at once, he or she reveals each one before drawing it.').
card_ruling('windriddle palaces', '2012-06-01', 'Other players won\'t be able to play the top card of their libraries unless another effect allows it. If you and another player can play the top card of that player\'s library (because of some other effect), you\'ll be able to in most cases. This is because you have priority first at the beginning of each phase and step and after each spell or ability resolves. Once you play that card, it immediately moves from that library to the appropriate zone (the battlefield if it\'s a land card or the stack if it\'s a nonland card). The other player can\'t respond by playing that card.').

card_ruling('windrider eel', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('windrider eel', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('winds of change', '2007-09-16', 'Each player draws a number of cards equal to the number of cards he or she shuffled into his or her own library.').

card_ruling('winds of qal sisma', '2014-11-24', 'If you control a creature with power 4 or greater as Winds of Qal Sisma resolves, the ferocious ability will apply. Combat damage dealt by creatures your opponents control will be prevented, even if you no longer control a creature with power 4 or greater as that damage would be dealt.').

card_ruling('winds of rath', '2005-08-01', 'A creature is \"enchanted\" if it has any Auras attached to it.').

card_ruling('wing puncture', '2011-01-01', 'If either target has become illegal by the time Wing Puncture resolves, the spell will still resolve but have no effect. If the first target is illegal, it can\'t perform any actions, so it can\'t deal damage. If the second target is illegal, there\'s nothing for the first target to deal damage to. If both targets have become illegal, the spell will be countered.').

card_ruling('wing shards', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('wing shards', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('wing shards', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('wing shards', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('wing shards', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('wingbeat warrior', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('winged sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('winged sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('wingmate roc', '2014-09-20', 'Count the number of attacking creatures when the last ability resolves, including Wingmate Roc itself if it’s still on the battlefield, to determine how much life you gain.').
card_ruling('wingmate roc', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('wingmate roc', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('wingrattle scarecrow', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('wingrattle scarecrow', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('wings of velis vel', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('wingsteed rider', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('wingsteed rider', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('wingsteed rider', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('winnower patrol', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('winnower patrol', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('winnower patrol', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('winnower patrol', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('winnower patrol', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('winnower patrol', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('winter blast', '2007-09-16', 'Winter Blast can target tapped creatures. If they have flying, the second part of the effect will still affect them.').
card_ruling('winter blast', '2007-09-16', 'Checks if the creatures are Flying on resolution and not on announcement. It\'s not a targeting restriction.').

card_ruling('winter orb', '2004-10-04', 'Even lands that are also creatures are affected by this card\'s ability.').

card_ruling('winter\'s chill', '2004-10-04', 'The payments are made when the spell resolves. The three options are: pay {2} to let creature act as normal, pay {1} to have creature neither deal or receive damage, or pay nothing and the creature does deal and receive damage but it will be destroyed at end of combat.').
card_ruling('winter\'s chill', '2013-09-20', 'If a turn has multiple combat phases, this spell can be cast during any of them as long as it\'s before the beginning of that phase\'s Declare Blockers Step.').

card_ruling('winter\'s night', '2008-10-01', 'If a land affected by Winter\'s Night is untapped at the time its controller\'s next untap step begins, the \"doesn\'t untap\" effect doesn\'t do anything. It won\'t apply at some later time when that land is tapped.').
card_ruling('winter\'s night', '2008-10-01', 'Winter\'s Night doesn\'t track the lands\' controllers. If an affected land changes controllers before its old controller\'s next untap step, Winter\'s Night will prevent it from being untapped during its new controller\'s next untap step.').
card_ruling('winter\'s night', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('winterflame', '2014-09-20', 'You choose which mode you’re using—or that you’re using both modes—as you’re casting the spell. Once this choice is made, it can’t be changed later while the spell is on the stack.').
card_ruling('winterflame', '2014-09-20', 'If you choose both modes, they can each target the same creature or they can target different creatures.').
card_ruling('winterflame', '2014-09-20', 'Winterflame won’t affect any target that’s illegal as it tries to resolve. If you choose to use both modes and both targets are illegal at that time, Winterflame will be countered.').

card_ruling('wipe away', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('wipe away', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('wipe away', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('wipe away', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('wipe away', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('wipe away', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('wipe clean', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('wirewood guardian', '2009-02-01', 'Unlike the normal cycling ability, Forestcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Forest card. After you find a Forest card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('wirewood guardian', '2009-02-01', 'Forestcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Forestcycling this card. Any ability that stops a cycling ability from being activated also stops Forestcycling from being activated.').
card_ruling('wirewood guardian', '2009-02-01', 'Forestcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Forestcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('wirewood guardian', '2009-02-01', 'You can choose to find any card with the Forest land type, including nonbasic lands. You can also choose not to find a card, even if there is a Forest card in your library.').

card_ruling('wispmare', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('witch engine', '2008-04-01', 'Because the ability is targeted, it is not a mana ability. It can only be activated when you have priority, and goes on the stack like other non-mana abilities.').

card_ruling('witch hunt', '2013-10-17', 'Spells and abilities that would cause a player to gain life still resolve, but the life-gain part has no effect.').
card_ruling('witch hunt', '2013-10-17', 'Abilities that trigger whenever a player gains life can’t trigger and effects that would replace gaining life with another effect won’t apply because it’s impossible for players to gain life.').
card_ruling('witch hunt', '2013-10-17', 'If an effect sets a player’s life total to a specific number and that number is higher than the player’s current life total, that part of the effect won’t do anything. (If the number is lower than the player’s current life total, the effect will work as normal.)').
card_ruling('witch hunt', '2013-10-17', 'To choose a target at random, all possible legal targets must have an equal chance of being chosen. There are many ways to do this, including assigning each possible legal target a number and rolling a die.').
card_ruling('witch hunt', '2013-10-17', 'The target is chosen at random as you put the last ability on the stack. Players can respond to this ability knowing who the target is.').
card_ruling('witch hunt', '2013-10-17', 'If you control Witch Hunt, you are not a legal target. If there are no legal targets available, the ability is removed from the stack and you retain control of Witch Hunt.').

card_ruling('witch\'s mist', '2007-05-01', 'If all damage that would be dealt to a creature is prevented, no damage is actually dealt and that creature can\'t be targeted by Witch\'s Mist.').
card_ruling('witch\'s mist', '2007-05-01', 'If a creature is lethally damaged and regenerates, all damage is removed from that creature. But since it was actually dealt damage earlier in the turn, it can be targeted by Witch\'s Mist.').

card_ruling('witchstalker', '2013-07-01', 'The counter will be put on Witchstalker before the spell cast by an opponent resolves.').
card_ruling('witchstalker', '2013-07-01', 'If an opponent casts a copy of a card that’s blue or black during your turn (as opposed to putting a copy of such a spell directly on the stack), the ability will also trigger. Check for the word “cast” in the effect that creates the copy.').

card_ruling('withdraw', '2004-10-04', 'The creature\'s controller gets the option to pay when the spell resolves.').
card_ruling('withdraw', '2004-10-04', 'The two creatures may have different controllers.').
card_ruling('withdraw', '2004-10-04', 'Must target two different creatures.').
card_ruling('withdraw', '2004-10-04', 'You choose on announcement which creature is the one that can be paid to prevent the effect. The payment itself is made on resolution (if at all).').

card_ruling('withering boon', '2005-11-01', 'The payment of 3 life is an additional cost of casting the spell.').
card_ruling('withering boon', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('withering wisps', '2008-10-01', 'Withering Wisps\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless there are no creatures on the battlefield when a turn\'s end step begins, and (2) the ability will do nothing if there are any creatures on the battlefield by the time the ability resolves.').
card_ruling('withering wisps', '2008-10-01', 'If all creatures leave the battlefield during a turn\'s End step (after any \"at the beginning of the end step\" triggers would have triggered that turn), Withering Wisps will remain on the battlefield because the trigger condition has already passed.').
card_ruling('withering wisps', '2008-10-01', 'The number of snow Swamps you control and the number of times Withering Wisps\'s ability has been activated so far that turn are checked each time you attempt to activate the ability.').

card_ruling('witherscale wurm', '2008-05-01', 'Witherscale Wurm\'s second ability triggers when it deals any damage to an opponent, not just combat damage.').
card_ruling('witherscale wurm', '2008-05-01', 'Witherscale Wurm\'s first ability does *not* give Witherscale Wurm wither.').

card_ruling('withstand', '2006-02-01', 'You draw a card when Withstand resolves, not when damage is prevented.').

card_ruling('witness of the ages', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('witness of the ages', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('witness of the ages', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('witness of the ages', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('witness of the ages', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('witness of the ages', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('witness of the ages', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('witness of the ages', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('witness of the ages', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('wizard mentor', '2004-10-04', 'Can target itself.').

card_ruling('wizened snitches', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('wizened snitches', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('wizened snitches', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').

card_ruling('wojek halberdiers', '2013-04-15', 'The three attacking creatures don\'t have to be attacking the same player or planeswalker.').
card_ruling('wojek halberdiers', '2013-04-15', 'Once a battalion ability has triggered, it doesn\'t matter how many creatures are still attacking when that ability resolves.').
card_ruling('wojek halberdiers', '2013-04-15', 'The effects of battalion abilities vary from card to card. Read each card carefully.').

card_ruling('wolf pack', '2008-04-01', 'If this creature is attacking a planeswalker, assigning its damage as though it weren\'t blocked means the damage is assigned to the planeswalker, not to the defending player.').
card_ruling('wolf pack', '2008-04-01', 'When assigning combat damage, you choose whether you want to assign all damage to blocking creatures, or if you want to assign all of it to the defending player or planeswalker. You can\'t split the damage assignment between them.').
card_ruling('wolf pack', '2008-04-01', 'You can decide to assign damage to the defending player or planeswalker even if the blocking creature has protection from green or damage preventing effects on it.').
card_ruling('wolf pack', '2008-04-01', 'If blocked by a creature with banding, the defending player decides whether or not the damage is assigned \"as though it weren\'t blocked\".').

card_ruling('wolf-skull shaman', '2008-04-01', 'Kinship is an ability word that indicates a group of similar triggered abilities that appear on _Morningtide_ creatures. It doesn\'t have any special rules associated with it.').
card_ruling('wolf-skull shaman', '2008-04-01', 'The first two sentences of every kinship ability are the same (except for the creature\'s name). Only the last sentence varies from one kinship ability to the next.').
card_ruling('wolf-skull shaman', '2008-04-01', 'You don\'t have to reveal the top card of your library, even if it shares a creature type with the creature that has the kinship ability.').
card_ruling('wolf-skull shaman', '2008-04-01', 'After the kinship ability finishes resolving, the card you looked at remains on top of your library.').
card_ruling('wolf-skull shaman', '2008-04-01', 'If you have multiple creatures with kinship abilities, each triggers and resolves separately. You\'ll look at the same card for each one, unless you have some method of shuffling your library or moving that card to a different zone.').
card_ruling('wolf-skull shaman', '2008-04-01', 'If the top card of your library is already revealed (due to Magus of the Future, for example), you still have the option to reveal it or not as part of a kinship ability\'s effect.').

card_ruling('wolfbitten captive', '2011-01-22', 'You can activate Wolfbitten Captive\'s ability, let it transform into Krallenhorde Killer, then activate Krallenhorde Killer\'s ability, all on the same turn, and vice versa.').
card_ruling('wolfbitten captive', '2011-01-22', 'If you activate one face\'s ability, and some effect causes the card to transform twice that turn, you won\'t be able to activate that face\'s ability again that turn.').

card_ruling('wolfhunter\'s quiver', '2011-01-22', 'Only the equipped creature\'s controller can activate the abilities it gains from Wolfhunter\'s Quiver.').
card_ruling('wolfhunter\'s quiver', '2011-01-22', 'For each of the activated abilities, the equipped creature is the source of the ability and the damage. For example, if the equipped creature is not an artifact, you could activate either ability targeting a creature with protection from artifacts.').
card_ruling('wolfhunter\'s quiver', '2011-01-22', 'You could activate the first ability targeting a Werewolf creature if you wanted to.').

card_ruling('wood elemental', '2004-10-04', 'The Forests are sacrificed to define the power/toughness of this card right before it enters the battlefield. You have to do this sacrifice no matter how this card enters the battlefield.').

card_ruling('wood elves', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('wooden stake', '2011-09-22', 'The Vampire is destroyed before any combat damage is dealt.').

card_ruling('woodfall primus', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('woodfall primus', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('woodfall primus', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('woodfall primus', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('woodfall primus', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('woodfall primus', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('woodfall primus', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('woodland bellower', '2015-06-22', 'If a card has {X} in its mana cost, X is 0.').

card_ruling('woodland sleuth', '2011-09-22', 'The creature card isn\'t chosen at random until the ability resolves.').
card_ruling('woodland sleuth', '2011-09-22', 'Woodland Sleuth could die in response to its own morbid ability. If this happens, the ability could return Woodland Sleuth to its owner\'s hand.').

card_ruling('woodland wanderer', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('woodland wanderer', '2015-08-25', '').
card_ruling('woodland wanderer', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('woodland wanderer', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('woodland wanderer', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('woodlurker mimic', '2008-08-01', 'The ability triggers whenever you cast a spell that\'s both of its listed colors. It doesn\'t matter whether that spell also happens to be any other colors.').
card_ruling('woodlurker mimic', '2008-08-01', 'If you cast a spell that\'s the two appropriate colors for the second time in a turn, the ability triggers again. The Mimic will once again become the power and toughness stated in its ability, which could overwrite power- and toughness-setting effects that have been applied to it in the meantime.').
card_ruling('woodlurker mimic', '2008-08-01', 'Any other abilities the Mimic may have gained are not affected.').
card_ruling('woodlurker mimic', '2009-10-01', 'The effect from the ability overwrites other effects that set power and/or toughness if and only if those effects existed before the ability resolved. It will not overwrite effects that modify power or toughness (whether from a static ability, counters, or a resolved spell or ability), nor will it overwrite effects that set power and toughness which come into existence after the ability resolves. Effects that switch the creature\'s power and toughness are always applied after any other power or toughness changing effects, including this one, regardless of the order in which they are created.').

card_ruling('woodvine elemental', '2014-05-29', 'Except in some very rare cases, the card each player draws will be the card revealed from the top of his or her library.').

card_ruling('woodwraith corrupter', '2005-10-01', 'The effect has no stated duration, so a land turned into a creature this way continues being a creature as long as the land is on the battlefield.').
card_ruling('woodwraith corrupter', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('woolly loxodon', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('woolly loxodon', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('woolly loxodon', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('woolly loxodon', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('woolly loxodon', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('woolly loxodon', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('woolly loxodon', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('woolly loxodon', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('woolly loxodon', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('woolly razorback', '2006-07-15', 'Having an ice counter on Woolly Razorback doesn\'t prevent combat damage that would be dealt to it.').
card_ruling('woolly razorback', '2006-07-15', 'The third time Woolly Razorback blocks, its last ice counter will be removed, meaning the combat damage it deals that combat won\'t be prevented.').

card_ruling('word of command', '2004-10-04', 'To \"play a card\" is to either cast a spell or to put a land onto the battlefield using the main phase special action.').
card_ruling('word of command', '2009-10-01', 'You do get to choose which lands get tapped. You can make them choose an optional mana generating ability of the land that they tap, such as making them tap and sacrifice the land instead of just tap it.').
card_ruling('word of command', '2011-01-01', 'Word of Command can\'t be used to force a player to play a card that isn\'t in his or her hand.').
card_ruling('word of command', '2011-01-01', 'You control the player while this spell is resolving, which means you get to see anything he or she can see. If that player is required to search his or her library as part of playing the card or as part of its resolution if it\'s cast as a spell, then you can see the cards in that player\'s library as well.').
card_ruling('word of command', '2011-01-01', 'Your opponent can\'t counter the Word of Command after letting you look at his or her hand, but they can attempt to counter the spell you force them to cast.').
card_ruling('word of command', '2011-01-01', 'You may Command your opponent to play a land (if they have not already done so this turn).').
card_ruling('word of command', '2011-01-01', 'You must order your opponent to play the chosen card if it is possible to do so.').
card_ruling('word of command', '2011-01-01', 'If there is a non-mana way to cast a spell, as with Pitch Spells, you may require that way to be used if it is possible to do so.').
card_ruling('word of command', '2011-01-01', 'Since this spell is an instant, your opponent gets a chance to respond to it as normal. Once this spell resolves, you look at your opponent\'s hand and choose a card. Note that it is common practice to respond to Word of Command by using up any spells or mana you have prior to letting it resolve.').
card_ruling('word of command', '2011-01-01', 'During the resolution of this spell, that player plays the chosen card.').

card_ruling('word of seizing', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('word of seizing', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('word of seizing', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('word of seizing', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('word of seizing', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('word of seizing', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('words of war', '2004-10-04', 'If multiple Words have been used prior to drawing a card, then you can choose which one to apply (and use up) each time you draw a card.').

card_ruling('words of waste', '2004-10-04', 'If multiple Words have been used prior to drawing a card, then you can choose which one to apply (and use up) each time you draw a card.').

card_ruling('words of wilding', '2004-10-04', 'If multiple Words have been used prior to drawing a card, then you can choose which one to apply (and use up) each time you draw a card.').

card_ruling('words of wind', '2004-10-04', 'If multiple Words have been used prior to drawing a card, then you can choose which one to apply (and use up) each time you draw a card.').

card_ruling('words of worship', '2004-10-04', 'If multiple Words have been used prior to drawing a card, then you can choose which one to apply (and use up) each time you draw a card.').

card_ruling('world at war', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('world at war', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('world at war', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('world at war', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('world at war', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('world at war', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('world at war', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('world at war', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('world at war', '2010-06-15', 'As long as World at War resolves before the first postcombat main phase of a turn ends, it will have its full effect. That will happen in most cases, since it\'s a sorcery (meaning it\'s probably cast during the first or second main phase of a turn), and it has rebound (meaning it\'ll be cast during your upkeep). If, however, it\'s cast later than that, it won\'t create any new phases. For example, if one World at War creates a second combat phase and a third main phase in a turn, then a second World at War is cast during that third main phase, no additional phases are created. The rebound effect still works, though.').
card_ruling('world at war', '2010-06-15', 'Multiple World at War effects are cumulative, as long as they\'re cast early enough. The first one modifies the turn structure to this: beginning phase, precombat main phase, combat phase, postcombat main phase, [new combat phase], [new postcombat main phase], ending phase. Each subsequent one inserts another combat phase and main phase into the turn after the original postcombat main phase and before the newest combat phase.').
card_ruling('world at war', '2010-06-15', 'Unlike other similar cards (such as Relentless Assault), World at War doesn\'t untap the creatures that have attacked this turn when it resolves. Rather, those creatures untap when the new combat phase created by the spell begins. All creatures that attacked this turn untap, regardless of which combat phase they attacked in (if there have been more than one) or who controls them (if you\'re playing a Two-Headed Giant game, for example).').
card_ruling('world at war', '2010-06-15', 'You don\'t have to attack with any creatures during the new combat phase.').

card_ruling('world queller', '2009-10-01', 'You don\'t choose a card type until the ability resolves. Once you choose a type, it\'s too late for players to respond.').
card_ruling('world queller', '2009-10-01', 'First you choose which permanent you\'ll sacrifice (if you control any of the chosen type), then each other player in turn order does the same, then all chosen permanents are sacrificed at the same time.').
card_ruling('world queller', '2013-07-01', 'You may choose any card type. Choosing anything other than artifact, creature, enchantment, land, planeswalker, or tribal will result in no permanents being sacrificed, though, since these are the only types permanents can have.').

card_ruling('worldfire', '2012-07-01', 'These actions are performed in order, but no triggered abilities resolve in between them and no player may cast spells or activate abilities in between them. This means that if a permanent\'s ability triggers on a player losing life, that permanent will be exiled first and won\'t be on the battlefield when your life total becomes 1. Leaves-the-battlefield triggers won\'t resolve until you\'re done resolving Worldfire entirely.').
card_ruling('worldfire', '2012-07-01', 'If your life total is greater than 1 when Worldfire resolves, the change in life total will count as life loss. Other effects that interact with life loss will interact with this effect accordingly.').
card_ruling('worldfire', '2012-07-01', 'Worldfire won\'t be exiled. It will go to your graveyard as the final part of its resolution.').

card_ruling('worldgorger dragon', '2004-10-04', 'It is possible for the Dragon to leave the battlefield before its \"enters the battlefield\" trigger resolves. If this happens, then the \"leaves the battlefield\" trigger will have nothing to return and the \"enters the battlefield\" trigger\'s effects will be irreversible.').
card_ruling('worldgorger dragon', '2005-08-01', 'If an Aura is exiled this way, you can place it on any legal permanent when it returns. It doesn\'t have to go back to the same place. It can\'t be placed on a permanent that is entering the battlefield at the same time.').

card_ruling('worldheart phoenix', '2009-02-01', 'Casting Worldheart Phoenix from your graveyard by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast a creature spell.').
card_ruling('worldheart phoenix', '2009-02-01', 'If you cast Worldheart Phoenix this way, it is a spell and can be countered.').
card_ruling('worldheart phoenix', '2009-02-01', 'As soon as you start to cast Worldheart Phoenix from your graveyard, the card moves onto the stack. At that point, it\'s too late for an opponent to prevent you from casting it by removing it from your graveyard.').

card_ruling('worldknit', '2014-05-29', 'Your card pool includes every card you drafted or received after the draft because of Deal Broker. It doesn’t include any cards removed from the draft with Cogwork Grinder.').
card_ruling('worldknit', '2014-05-29', 'Conspiracies are never put into your deck. Instead, you put any number of conspiracies from your card pool into the command zone as the game begins. These conspiracies are face up unless they have hidden agenda, in which case they begin the game face down.').
card_ruling('worldknit', '2014-05-29', 'A conspiracy doesn’t count as a card in your deck for purposes of meeting minimum deck size requirements. (In most drafts, the minimum deck size is 40 cards.)').
card_ruling('worldknit', '2014-05-29', 'You don’t have to play with any conspiracy you draft. However, you have only one opportunity to put conspiracies into the command zone, as the game begins. You can’t put conspiracies into the command zone after this point.').
card_ruling('worldknit', '2014-05-29', 'You can look at any player’s face-up conspiracies at any time. You’ll also know how many face-down conspiracies a player has in the command zone, although you won’t know what they are.').
card_ruling('worldknit', '2014-05-29', 'A conspiracy’s static and triggered abilities function as long as that conspiracy is face-up in the command zone.').
card_ruling('worldknit', '2014-05-29', 'Conspiracies are colorless, have no mana cost, and can’t be cast as spells.').
card_ruling('worldknit', '2014-05-29', 'Conspiracies aren’t legal for any sanctioned Constructed format, but may be included in other Limited formats, such as Cube Draft.').

card_ruling('worldly counsel', '2004-10-04', 'You choose the order on the bottom of your library.').
card_ruling('worldly counsel', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('worldly counsel', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('worldly counsel', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('worldly tutor', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('worldslayer', '2011-09-22', 'The equipped creature is also destroyed.').

card_ruling('worldspine wurm', '2012-10-01', 'Worldspine Wurm\'s last ability is a triggered ability, not a replacement ability. Players can respond to this ability, for example, by trying to exile Worldspine Wurm from the graveyard before it\'s shuffled into a library.').

card_ruling('worm harvest', '2008-08-01', 'Casting a card by using its retrace ability works just like casting any other spell, with two exceptions: You\'re casting it from your graveyard rather than your hand, and you must discard a land card in addition to any other costs.').
card_ruling('worm harvest', '2008-08-01', 'A retrace card cast from your graveyard follows the normal timing rules for its card type.').
card_ruling('worm harvest', '2008-08-01', 'When a retrace card you cast from your graveyard resolves or is countered, it\'s put back into your graveyard. You may use the retrace ability to cast it again.').
card_ruling('worm harvest', '2008-08-01', 'If the active player casts a spell that has retrace, that player may cast that card again after it resolves, before another player can remove the card from the graveyard. The active player has priority after the spell resolves, so he or she can immediately cast a new spell. Since casting a card with retrace from the graveyard moves that card onto the stack, no one else would have the chance to affect it while it\'s still in the graveyard.').
card_ruling('worm harvest', '2008-08-01', 'If you cast Worm Harvest from your graveyard by using retrace, the land you discard as an additional cost will be counted by Worm Harvest when it resolves.').

card_ruling('worms of the earth', '2004-10-04', 'Land can\'t enter the battlefield, so an effect that \"puts a land onto the battlefield\" won\'t do anything, and the land will stay where it is.').
card_ruling('worms of the earth', '2004-10-04', 'If you cast a spell or activate an ability that lets you put a land onto the battlefield, the effect fails. You\'ll still do everything else the spell or ability tells you to do, such as shuffle the library.').
card_ruling('worms of the earth', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').
card_ruling('worms of the earth', '2008-08-01', 'If a permanent spell tries to enter the battlefield as a land during its resolution (for instance, a Clone entering the battlefield as a copy of a Dryad Arbor or an animated Mutavault), it is put into its owner\'s graveyard instead of entering the battlefield. It never enters the battlefield, so abilities that would have triggered on it entering the battlefield won\'t trigger.').

card_ruling('wormwood treefolk', '2009-10-01', 'Causing Wormwood Treefolk to gain forestwalk or swampwalk is useful only during the declare attackers step or earlier. Once it becomes blocked, giving it a landwalk ability won\'t change that.').

card_ruling('worship', '2004-10-04', 'It reduces your life total to 1, not the damage to 1.').
card_ruling('worship', '2004-10-04', 'Worship does not prevent damage. It causes some damage to be unable to lower your life total. So any damage rendered useless by Worship was still dealt and is counted by effects that track the amount of damage done to a player. In addition, Worship does not prevent loss of life, so loss of life bypasses Worship.').

card_ruling('worst fears', '2014-04-26', 'While controlling another player, you can see all cards that player can see. This includes cards in that player\'s hand, face-down cards that player controls, his or her sideboard, and any cards in his or her library that he or she looks at.').
card_ruling('worst fears', '2014-04-26', 'The player you’re controlling is still the active player during that turn.').
card_ruling('worst fears', '2014-04-26', 'While controlling another player, you also continue to make your own choices and decisions.').
card_ruling('worst fears', '2014-04-26', 'While controlling another player, you make all choices and decisions that player is allowed to make or is told to make during that turn. This includes choices about what spells to cast or what abilities to activate, as well as any decisions called for by triggered abilities or for any other reason.').
card_ruling('worst fears', '2014-04-26', 'You can’t make the affected player concede. That player may choose to concede at any time, even while you’re controlling him or her.').
card_ruling('worst fears', '2014-04-26', 'You can’t make any illegal decisions or illegal choices—you can’t do anything that player couldn’t do. You can’t make choices or decisions for that player that aren’t called for by the game rules or by any cards, permanents, spells, abilities, and so on. If an effect causes another player to make decisions that the affected player would normally make (such as Master Warcraft does), that effect takes precedence. In other words, if the affected player wouldn’t make a decision, you wouldn’t make that decision on his or her behalf.').
card_ruling('worst fears', '2014-04-26', 'You also can’t make any choices or decisions for the player that would be called for by the tournament rules (such as whether to take an intentional draw or whether to call a judge).').
card_ruling('worst fears', '2014-04-26', 'You can use only the affected player’s resources (cards, mana, and so on) to pay costs for that player; you can’t use your own. Similarly, you can use the affected player’s resources only to pay that player’s costs; you can’t spend them on your costs.').
card_ruling('worst fears', '2014-04-26', 'You only control the player. You don’t control any of his or her permanents, spells, or abilities.').
card_ruling('worst fears', '2014-04-26', 'If the target player skips his or her next turn, you’ll control the next turn the affected player actually takes.').
card_ruling('worst fears', '2014-04-26', 'Multiple player-controlling effects that affect the same player overwrite each other. The last one to be created is the one that works.').
card_ruling('worst fears', '2014-04-26', 'You could gain control of yourself using Worst Fears, but unless you do so to overwrite someone else’s player-controlling effect, this doesn’t do anything.').

card_ruling('worthy cause', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('worthy cause', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('wound reflection', '2008-05-01', 'When Wound Reflection\'s ability resolves, it checks how much life each opponent lost over the course of the turn, then it causes that opponent to lose that much life. It doesn\'t matter how the opponent lost life or who caused it. It also doesn\'t matter if Wound Reflection wasn\'t on the battlefield at the time some or all of the life was lost.').
card_ruling('wound reflection', '2008-05-01', 'Wound Reflection\'s ability checks only whether life was lost. It doesn\'t care whether life was also gained. For example, if an opponent lost 4 life and gained 6 life during the turn, that player will have a higher life total than he or she started the turn with -- but Wound Reflection\'s ability will still cause that player to lose 4 life.').
card_ruling('wound reflection', '2008-05-01', 'Wound Reflection\'s ability triggers at the end of every turn. It doesn\'t have to be your turn, and it doesn\'t have to be the turn of an opponent that lost life.').
card_ruling('wound reflection', '2008-05-01', 'If Wound Reflection\'s ability resolves, then an opponent loses life later in the same turn, that life loss is never counted by Wound Reflection.').
card_ruling('wound reflection', '2008-05-01', 'Multiple Wound Reflections are cumulative. They\'ll both trigger at the same time. One will resolve first. When the second one resolves, it will see the same life loss that the first Wound Reflection saw, as well as the life loss caused by the first Wound Reflection. For example, if an opponent lost 3 life during the turn, the first Wound Reflection will cause that opponent to lose 3 more life, then the second Wound Reflection will cause that opponent to lose 6 life.').

card_ruling('wrack with madness', '2011-01-22', 'Creature abilities like deathtouch or lifelink may affect the results of the damage dealt.').

card_ruling('wrap in flames', '2010-06-15', 'You may target zero, one, two, or three creatures.').
card_ruling('wrap in flames', '2010-06-15', 'The \"can\'t block\" effect applies to each of the targeted creatures -- and only those creatures -- even if Wrap in Flames deals no damage to one or more of them (due to a prevention effect, for example) or Wrap in Flames deals damage to a different creature (due to a redirection effect).').
card_ruling('wrap in flames', '2010-06-15', 'If any of the targeted creatures is an illegal target by the time Wrap in Flames resolves, it won\'t be dealt damage and will be able to block. The other targeted creatures will still be affected.').

card_ruling('wrap in vigor', '2007-05-01', 'Wrap in Vigor sets up a separate regeneration shield on each creature you control at the time it resolves.').

card_ruling('wreath of geists', '2011-09-22', 'The value of X is constantly updated as creature cards are put into or removed from your graveyard.').

card_ruling('wren\'s run packmaster', '2007-10-01', 'Wren\'s Run Packmaster gives deathtouch to all Wolf permanents you control, not just the tokens it creates.').
card_ruling('wren\'s run packmaster', '2009-10-01', 'A Wolf\'s deathtouch ability will apply if both Wren\'s Run Packmaster and that Wolf are on the battlefield at the time the Wolf deals damage. The creature dealt damage by the Wolf will be destroyed the next time state-based actions are checked, even if the Wolf or Wren\'s Run Packmaster leaves the battlefield.').

card_ruling('wrench mind', '2004-12-01', 'You can discard either one artifact card or two cards which may or may not be artifacts. If you really want to, you can discard two artifact cards.').

card_ruling('wretched banquet', '2009-02-01', 'Wretched Banquet may target any creature. The creature\'s power is checked only as Wretched Banquet resolves. If another creature has less power than the targeted creature at that time, Wretched Banquet does nothing.').

card_ruling('wrexial, the risen deep', '2010-03-01', 'If you want to cast the targeted card, you cast it as part of the resolution of Wrexial\'s triggered ability. Timing restrictions based on the card\'s type are ignored. Other restrictions are not (such as \"Cast [this card] only during your end step\").').
card_ruling('wrexial, the risen deep', '2010-03-01', 'If you are unable to cast the targeted card (there are no legal targets for the spell, for example), nothing happens when the ability resolves, and the card remains in its owner\'s graveyard.').
card_ruling('wrexial, the risen deep', '2010-03-01', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. On the other hand, if the card has additional costs (such as kicker or multikicker), you may pay those.').
card_ruling('wrexial, the risen deep', '2010-03-01', 'If you cast the targeted card and it would be put into its owner\'s graveyard from the stack for any reason (either because it resolves or because it\'s countered), that card is exiled instead.').

card_ruling('write into being', '2014-11-24', 'Other players won’t know whether the card you manifest is the top card or second card of that library.').
card_ruling('write into being', '2014-11-24', 'If you’re playing with the top card of your library revealed, you’ll manifest one of the cards, then the other one will be revealed, then you can choose to put that card on the bottom of your library or leave it on top.').

card_ruling('wu elite cavalry', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('wu light cavalry', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('wu longbowman', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('wu scout', '2009-10-01', 'Despite the similarities between horsemanship and flying, horsemanship doesn\'t interact with flying or reach.').

card_ruling('wurm\'s tooth', '2009-10-01', 'The ability triggers whenever any player, not just you, casts a green spell.').
card_ruling('wurm\'s tooth', '2009-10-01', 'If a player casts a green spell, Wurm\'s Tooth\'s ability triggers and is put on the stack on top of that spell. Wurm\'s Tooth\'s ability will resolve (causing you to gain 1 life) before the spell does.').

card_ruling('wurmcoil engine', '2011-01-01', 'The two creature tokens enter the battlefield at the same time.').
card_ruling('wurmcoil engine', '2011-01-01', 'It must be clear to all players which token has deathtouch and which token has lifelink.').

card_ruling('wurmweaver coil', '2006-02-01', 'Wurmweaver Coil can enchant only a green creature. It can\'t enter the battlefield attached to a nongreen creature, it can\'t be moved onto a nongreen creature, and if the creature it\'s attached to stops being green, the Aura is put into its owner\'s graveyard.').

