% Rulings

card_ruling('haakon, stromgald scourge', '2006-07-15', 'Haakon can\'t be cast from your hand. It can\'t be cast from any zone other than your graveyard, even if an effect such as that of Spelljack or Muse Vessel would otherwise allow you to.').
card_ruling('haakon, stromgald scourge', '2006-07-15', 'While Haakon is in your graveyard, you may cast it. This follows the normal rules and timing for casting a creature spell; the only difference is what zone Haakon is being cast from. The spell goes on the stack. You have to pay Haakon\'s mana cost and any applicable additional costs. The same applies for Knight cards you cast from your graveyard while Haakon is on the battlefield.').

card_ruling('haazda snare squad', '2013-04-15', 'You choose the target creature when the ability triggers and goes on the stack. You choose whether to pay {W} when that ability resolves. You may pay {W} only once. The creature will be tapped before blockers are chosen.').

card_ruling('hada spy patrol', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('hada spy patrol', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('hada spy patrol', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('hada spy patrol', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('hada spy patrol', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('hag hedge-mage', '2008-08-01', 'Each of the triggered abilities has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control two or more lands of the appropriate land type when the Hedge-Mage enters the battlefield, and (2) the ability will do nothing if you don\'t control two or more lands of the appropriate land type by the time it resolves.').
card_ruling('hag hedge-mage', '2008-08-01', 'Both abilities trigger at the same time. You can put them on the stack in any order.').
card_ruling('hag hedge-mage', '2008-08-01', 'Each of the triggered abilities look at your lands individually. This means that if you only control two dual-lands of the appropriate types, both of the abilities will trigger').
card_ruling('hag hedge-mage', '2008-08-01', 'Both abilities are optional; you choose whether to use them when they resolve. If an ability has a target, you must choose a target even if you don\'t plan to use the ability.').

card_ruling('hagra crocodile', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('hagra crocodile', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('hagra diabolist', '2009-10-01', 'The ability counts the number of Allies you control as it resolves.').

card_ruling('hagra sharpshooter', '2015-08-25', 'An effect that gives a creature -1/-1 is different from putting a -1/-1 counter on that creature. Notably, if the target of Hagra Sharpshooter’s ability has any +1/+1 counters on it, those counters will remain.').

card_ruling('hail of arrows', '2005-06-01', 'You choose how the damage will be divided among the target creatures at the time you cast Hail of Arrows. Each target must be dealt at least 1 damage. If any of those creatures becomes an illegal target before Hail of Arrows resolves, the division of damage among the remaining creatures doesn\'t change.').

card_ruling('hakim, loreweaver', '2004-10-04', 'You only check if he has no Auras when activating the ability. You can use the ability multiple times in response to each other to get multiple Auras on him this way.').
card_ruling('hakim, loreweaver', '2004-10-04', 'If you pick an Aura that can\'t legally enchant this card, then the Aura stays in the graveyard.').

card_ruling('halcyon glaze', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('halfdane', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').
card_ruling('halfdane', '2009-10-01', 'Note that the duration of Halfdane\'s ability is until the end of your next upkeep. If Halfdane\'s ability doesn\'t get a chance to resolve (because there are no legal targets when it triggers, or because it\'s countered), then the effect from the previous turn\'s ability will continue to work through the rest of that upkeep step. Then that effect will wear off, and Halfdane will revert to being 3/3, its printed power and toughness (unless some other effect is modifying what its power and toughness is).').

card_ruling('halimar wavewatch', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('halimar wavewatch', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('halimar wavewatch', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('halimar wavewatch', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('halimar wavewatch', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('hall of gemstone', '2004-10-04', 'Although the color may be altered, any restrictions on the use of the mana are not removed.').
card_ruling('hall of gemstone', '2004-10-04', 'Will not affect lands that do not produce mana.').
card_ruling('hall of gemstone', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('hall of the bandit lord', '2004-12-01', 'The creature has haste if the mana is spent to cover any cost of the spell, even an additional cost.').
card_ruling('hall of the bandit lord', '2004-12-01', 'The effect that grants haste doesn\'t wear off at end of turn.').

card_ruling('hallowed burial', '2008-08-01', 'All creatures leave the battlefield simultaneously. They\'re not destroyed; they\'re just put on the bottom of their owners\' libraries. This includes token creatures, though they\'ll cease to exist as soon as Hallowed Burial finishes resolving.').
card_ruling('hallowed burial', '2008-08-01', 'Each player chooses the relative order of the cards he or she is putting on the bottom of his or her library, regardless of who controlled them while they were on the battlefield. Players don\'t reveal this order to other players.').

card_ruling('hallowed fountain', '2005-10-01', 'Has basic land types, but isn\'t a basic land. Things that affect basic lands don\'t affect it. For example, you can\'t find it with Civic Wayfinder.').
card_ruling('hallowed fountain', '2005-10-01', 'If another effect (such as Loxodon Gatekeeper\'s ability) tells you to put lands onto the battlefield tapped, it enters the battlefield tapped whether you pay 2 life or not.').
card_ruling('hallowed fountain', '2005-10-01', 'If multiple permanents with \"as enters the battlefield\" effects are entering the battlefield at the same time, process those effects one at a time, then put the permanents onto the battlefield all at once. For example, if you\'re at 3 life and an effect puts two of these onto the battlefield, you can pay 2 life for only one of them, not both.').

card_ruling('hallowed moonlight', '2015-06-22', 'After Hallowed Moonlight resolves, if a creature token would be put onto the battlefield, it’s put into exile instead and then ceases to exist. Creature tokens are never cast, even if the spell that created them was.').
card_ruling('hallowed moonlight', '2015-06-22', 'Hallowed Moonlight won’t affect any creature that was cast, no matter which zone it was cast from and whether or not its mana cost was paid.').

card_ruling('hallowed spiritkeeper', '2014-11-07', 'Count the number of creature cards in your graveyard as the triggered ability resolves, including Hallowed Spiritkeeper itself if it’s still there, to determine how many Spirits are created.').

card_ruling('halo hunter', '2009-10-01', 'Intimidate looks at the current colors of a creature that has it. Normally, Halo Hunter can\'t be blocked except by artifact creatures and/or black creatures. If it\'s turned white, then it can\'t be blocked except by artifact creatures and/or white creatures.').
card_ruling('halo hunter', '2009-10-01', 'If an attacking creature has intimidate, what colors it is matters only as the defending player declares blockers. Once it\'s blocked, changing its colors won\'t change that.').
card_ruling('halo hunter', '2009-10-01', 'Halo Hunter\'s triggered ability isn\'t optional. When it enters the battlefield, if you\'re the only player that controls an Angel, you\'ll have to target an Angel you control.').

card_ruling('hamletback goliath', '2012-07-01', 'The value of X is determined as the ability begins resolving. If the other creature is no longer on the battlefield, its last known existence on the battlefield is checked to determine its power.').

card_ruling('hammerheim', '2004-10-04', 'This can be used on a creature without landwalking but has no effect.').
card_ruling('hammerheim', '2009-10-01', 'Causing a creature to lose landwalk abilities is useful only during the declare attackers step or earlier. Once the declare blockers step begins, it\'s too late to block the creature.').

card_ruling('hanabi blast', '2004-12-01', 'If another player casts a Hanabi Blast that you own, it returns to your hand, and then that player discards a card at random.').

card_ruling('hand of emrakul', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('hand of emrakul', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('hand of emrakul', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('hand of emrakul', '2010-06-15', 'Casting Hand of Emrakul by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast a creature spell.').
card_ruling('hand of emrakul', '2010-06-15', 'Casting Hand of Emrakul by paying its alternative cost doesn\'t change its mana cost or converted mana cost.').
card_ruling('hand of emrakul', '2010-06-15', 'Effects that increase or reduce the cost to cast Hand of Emrakul will apply to it even if you choose to pay its alternative cost.').

card_ruling('hand of justice', '2004-10-04', 'It can destroy one of the three creatures it taps.').

card_ruling('hand of the praetors', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('hand of the praetors', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('hand of the praetors', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('hand of the praetors', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('hand of the praetors', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('hand of the praetors', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').
card_ruling('hand of the praetors', '2011-01-01', 'Whenever you cast a creature spell with infect, Hand of the Praetors\'s last ability triggers and goes on the stack on top of it. It will resolve before the creature spell does.').
card_ruling('hand of the praetors', '2011-01-01', 'The last ability triggers only if Hand of the Praetors is already on the battlefield at the time you cast a creature spell with infect. Casting Hand of the Praetors itself will not cause its own last ability to trigger.').

card_ruling('hands of binding', '2013-01-24', 'If the creature that was tapped by Hands of Binding is untapped during its controller\'s next untap step (perhaps because a spell untapped it), Hands of Binding has no effect at that time. It won\'t apply at some later time when the creature is tapped.').
card_ruling('hands of binding', '2013-01-24', 'If a different player gains control of the creature that was tapped by Hands of Binding, Hands of Binding will stop that creature from untapping during its new controller\'s next untap step.').
card_ruling('hands of binding', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('hands of binding', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('hands of binding', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('hands of binding', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('hands of binding', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('hands of binding', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('hands of binding', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('hands of binding', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('hands of binding', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('hands of binding', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('hands of binding', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').
card_ruling('hands of binding', '2013-04-15', 'This spell can target tapped creatures. If a targeted creature is already tapped when the spell resolves, that creature just remains tapped and doesn\'t untap during its controller\'s next untap step.').

card_ruling('hangarback walker', '2015-06-22', 'The value of each X in Hangarback Walker’s mana cost must be equal. For example, if X is 2, you’ll pay {4} to cast Hangarback Walker and it will enter the battlefield with two +1/+1 counters on it.').
card_ruling('hangarback walker', '2015-06-22', 'If enough -1/-1 counters are put on Hangarback Walker at the same time to make its toughness 0 or less, the number of +1/+1 counters on it before it got any -1/-1 counters will be used to determine how many Thopter tokens you get. For example, if there are three +1/+1 counters on Hangarback Walker and it gets four -1/-1 counters, you’ll get three Thopter tokens. That’s because Hangarback Walker’s triggered ability checks the creature’s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('harabaz druid', '2010-03-01', 'Harabaz Druid\'s activated ability is a mana ability, so it doesn\'t use the stack and players can\'t respond to it.').

card_ruling('harbinger of the hunt', '2015-02-25', 'If Harbinger of the Hunt loses flying, its first activated ability will cause it to deal damage to itself.').

card_ruling('harbor bandit', '2012-07-01', 'Activating Harbor Bandit\'s ability after it\'s been blocked won\'t cause it to become unblocked.').

card_ruling('harbor serpent', '2010-08-15', 'Harbor Serpent\'s abilities care about lands with the land type Island, not necessarily lands named Island.').
card_ruling('harbor serpent', '2010-08-15', 'The second ability checks how many Islands are on the battlefield (regardless of who controls them) only as attackers are declared. Once Harbor Serpent is declared as an attacker, it will continue to attack even if the number of Islands on the battlefield falls below five.').

card_ruling('hardened berserker', '2015-02-25', 'Hardened Berserker can’t reduce the colored mana requirement of a spell you cast.').
card_ruling('hardened berserker', '2015-02-25', 'If there are any additional costs to cast a spell, apply those before applying Hardened Berserker and any other cost reductions.').
card_ruling('hardened berserker', '2015-02-25', 'The ability can apply to alternative costs to cast a spell.').

card_ruling('hardened scales', '2014-09-20', '“Placed on a creature you control” includes that creature entering the battlefield with +1/+1 counters on it. If a creature would enter the battlefield with a number of +1/+1 counters on it while you control Hardened Scales, it enters with that many counters plus one.').
card_ruling('hardened scales', '2014-09-20', 'Each additional Hardened Scales you control will increase the number of +1/+1 counters placed on a creature by one.').

card_ruling('harm\'s way', '2009-10-01', 'As you cast Harm\'s Way, you target the creature or player that the redirected damage will be dealt to. As Harm\'s Way resolves, you choose a source of damage. You never choose the original recipient of the damage; Harm\'s Way will apply to whatever the chosen source tries to deal damage to, as long as it\'s you or a permanent you control.').
card_ruling('harm\'s way', '2009-10-01', 'Harm\'s Way can affect damage that would be dealt to a planeswalker you control (as well as damage that would be dealt to you, or to a creature you control).').
card_ruling('harm\'s way', '2009-10-01', 'If the chosen source would deal just 1 damage to you or a permanent you control, Harm\'s Way\'s effect will redirect that damage and still have a \"shield\" left for another 1 damage from that source later in the turn.').
card_ruling('harm\'s way', '2009-10-01', 'If the chosen source would simultaneously deal damage to multiple permanents you control (like Pyroclasm could) or to you and at least one permanent you control (like Earthquake could), Harm\'s Way will redirect just 2 of that damage. It won\'t redirect the next 2 damage that would be dealt to each recipient. You choose which 2 damage is redirected. If you like, you can choose to redirect 1 damage that would be dealt by the chosen source to each of two different recipients.').
card_ruling('harm\'s way', '2009-10-01', 'After Harm\'s Way resolves, it no longer checks to see if the targeted creature or player is a legal target. However, if that creature or player can\'t be dealt damage at the time the chosen source would deal damage (perhaps because the creature is no longer on the battlefield or is no longer a creature, or because the player is no longer in the game), the damage can\'t be redirected. It\'s dealt to the original recipient.').
card_ruling('harm\'s way', '2009-10-01', 'Harm\'s Way has no effect on damage that\'s already been dealt.').

card_ruling('harmless assault', '2010-06-15', 'Combat damage dealt by blocking creatures isn\'t prevented.').

card_ruling('harmonic convergence', '2004-10-04', 'If any player needs to put more than one enchantment on top of their library, they choose the order.').

card_ruling('harmonic sliver', '2006-09-25', 'The controller of the Sliver that enters the battlefield, not the controller of Harmonic Sliver, chooses the target.').
card_ruling('harmonic sliver', '2006-09-25', 'This ability will trigger when Harmonic Sliver enters the battlefield.').
card_ruling('harmonic sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('harmonic sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('harmonize', '2007-02-01', 'This is the timeshifted version of Concentrate.').

card_ruling('harness by force', '2014-04-26', 'Harness by Force can target any creature, even one that’s untapped or one you already control.').
card_ruling('harness by force', '2014-04-26', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').
card_ruling('harness by force', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('harness by force', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('harness by force', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('harness by force', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('harness by force', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('harrier griffin', '2006-02-01', 'If your opponents control no creatures, you must target Harrier Griffin or another of your own creatures.').

card_ruling('harrow', '2004-10-04', 'The sacrifice of a land is part of the cost of casting Harrow. You can\'t pay this cost more than once to get a multiple effect.').
card_ruling('harrow', '2004-10-04', 'The two lands do not count toward your normal one land you can play each turn.').
card_ruling('harrow', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('harvest wurm', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('harvest wurm', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('harvestguard alseids', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('harvestguard alseids', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('harvestguard alseids', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('hasran ogress', '2004-10-04', 'You either take damage or pay the 2 mana during resolution.').

card_ruling('hateflayer', '2008-08-01', 'If the permanent is already untapped, you can\'t activate its {Q} ability. That\'s because you can\'t pay the \"Untap this permanent\" cost.').
card_ruling('hateflayer', '2008-08-01', 'The \"summoning sickness\" rule applies to {Q}. If a creature with an {Q} ability hasn\'t been under your control since your most recent turn began, you can\'t activate that ability. Ignore this rule if the creature also has haste.').
card_ruling('hateflayer', '2008-08-01', 'When you activate an {Q} ability, you untap the creature with that ability as a cost. The untap can\'t be responded to. (The actual ability can be responded to, of course.)').
card_ruling('hateflayer', '2008-08-01', 'Wither applies to any damage Hateflayer deals to a creature, which includes both its combat damage and damage from its activated ability.').

card_ruling('hatred', '2004-10-04', 'The life payment is part of the cost to cast Hatred, so it is lost if this spell is countered.').

card_ruling('haunted fengraf', '2011-01-22', 'The creature card returned to your hand is chosen at random as the ability resolves. If any player responds to the ability, that player won\'t yet know what card will be returned.').
card_ruling('haunted fengraf', '2011-01-22', 'Because the ability doesn\'t target any creature card, any creature card put into the graveyard in response to that ability may be returned to your hand.').

card_ruling('haunted plate mail', '2014-07-18', 'Although you must control no creatures to activate the “animation” ability, having a creature somehow come under your control in response to that ability won’t stop it from resolving.').
card_ruling('haunted plate mail', '2014-07-18', 'It’s possible to activate the first activated ability of Haunted Plate Mail in response to activating the first ability of another Haunted Plate Mail and have them both become creatures until end of turn.').
card_ruling('haunted plate mail', '2014-07-18', 'You can activate the equip ability of Haunted Plate Mail while it isn’t an Equipment, but the ability will have no effect. Haunted Plate Mail won’t become attached to any creature.').

card_ruling('haunting apparition', '2004-10-04', 'You choose one opposing player when it enters the battlefield and it only affects that one player. This choice is not changed even if this card changes controllers. This card becomes useless but stays on the battlefield if the chosen player leaves the game.').

card_ruling('haunting echoes', '2009-10-01', 'While you\'re searching the player\'s library, you don\'t have to find all the cards with the same name as an exiled card if you don\'t want to. You can leave any number of them in that player\'s library').

card_ruling('haven of the spirit dragon', '2015-02-25', 'The mana produced by Haven of the Spirit Dragon’s second ability can be used to pay for any additional or alternative costs (such as dash costs) involved in casting a Dragon creature spell.').

card_ruling('havengul lich', '2011-01-22', 'Havengul Lich\'s activated ability creates a delayed triggered ability that triggers when you cast the creature card. This triggered ability will go on the stack on top of the creature spell and will resolve before the creature spell will.').
card_ruling('havengul lich', '2011-01-22', 'If you don\'t cast the creature card that turn, Havengul Lich won\'t gain any of its abilities.').
card_ruling('havengul lich', '2011-01-22', 'Havengul Lich gains the activated abilities of the card while that card is still a spell on the stack.').
card_ruling('havengul lich', '2013-04-15', 'Havengul Lich\'s ability changes the zone you are permitted to cast the card from, not the times you are permitted to cast it. You can cast a creature card that has been targeted by Havengul Lich from the graveyard, but unless it has flash, you can only do so if it\'s your main phase, the stack is empty, and you have priority.').
card_ruling('havengul lich', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('havengul runebinder', '2011-01-22', 'Although players may respond to the activated ability, they can\'t respond to the paying of its costs. The creature card will already be exiled by the time any player could respond.').
card_ruling('havengul runebinder', '2011-01-22', 'The Zombie creature token you put onto the battlefield will also get a +1/+1 counter.').

card_ruling('havengul skaab', '2012-05-01', 'If you don\'t control any other creatures, the ability won\'t do anything.').

card_ruling('havoc', '2004-10-04', 'In a multiplayer game, it affects all opponents.').

card_ruling('havoc festival', '2012-10-01', 'Spells and abilities that would cause a player to gain life still resolve, but the life-gain part has no effect.').
card_ruling('havoc festival', '2012-10-01', 'Effects that would replace gaining life with another effect won\'t apply because it\'s impossible for players to gain life.').
card_ruling('havoc festival', '2012-10-01', 'If an effect says to set a player\'s life total to a certain number and that number is higher than the player\'s current life total, that part of the effect won\'t do anything. (If the number is lower than the player\'s current life total, the effect will work as normal.)').
card_ruling('havoc festival', '2012-10-01', 'The amount of life lost is calculated when the triggered ability resolves. For example, if a player has 20 life and there are two Havoc Festivals on the battlefield, the first ability will cause that player to lose 10 life and then the second ability will cause that player to lose 5 life.').
card_ruling('havoc festival', '2012-10-01', 'In a Two-Headed Giant game, the last ability triggers for each player. Any life loss is applied to the team\'s life total. For example, if the team has 30 life, the first ability will cause the player (and thus the team) to lose 15 life and then the second ability will cause the other player (and thus the team) to lose 8 life. The team will end up having 7 life.').

card_ruling('haze frog', '2010-06-15', 'Combat damage dealt by Haze Frog itself during the turn it enters the battlefield isn\'t prevented. Combat damage that would be dealt by all other creatures is prevented, including creatures that weren\'t on the battlefield at the time the ability resolved.').
card_ruling('haze frog', '2010-06-15', 'If two Haze Frogs enter the battlefield during the same turn, each one\'s ability will prevent the other one\'s combat damage.').

card_ruling('haze of rage', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('haze of rage', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('haze of rage', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('haze of rage', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('hazezon tamar', '2009-10-01', 'When Hazezon Tamar\'s second ability resolves, all permanents with both the Sand and Warrior creature types are exiled, not just the Sand Warrior creature tokens it created. Permanents that have just one of those types are unaffected.').
card_ruling('hazezon tamar', '2009-10-01', 'The first ability creates a delayed triggered ability that triggers at the beginning of your next upkeep. It doesn\'t matter who controls Hazezon Tamar at that time, or even if it\'s on the battlefield. The player who put Hazezon onto the battlefield gets the Sand Warriors.').
card_ruling('hazezon tamar', '2009-10-01', 'Only Sand Warriors that are actually on the battlefield when Hazezon Tamar leaves the battlefield are affected when its second ability resolves. If Hazezon leaves the battlefield after the delayed triggered ability created by its first ability triggers but before that ability resolves, its second ability triggers and resolves first, then the Sand Warriors are put on the battlefield. Those Sand Warriors will remain on the battlefield indefinitely.').

card_ruling('he who hungers', '2013-04-15', 'If you cast this as normal during your main phase, it will enter the battlefield and you\'ll receive priority. If no abilities trigger because of this, you can activate its ability immediately, before any other player has a chance to remove it from the battlefield.').

card_ruling('heal', '2009-10-01', 'You draw a card at the beginning of the next turn\'s upkeep after Heal resolves, regardless of whether 1 damage was actually prevented.').

card_ruling('heal the scars', '2007-10-01', 'You gain the life when Heal the Scars resolves, not when the creature actually regenerates.').

card_ruling('healer of the pride', '2012-07-01', 'If Healer of the Pride enters the battlefield under your control at the same time as other creatures, its ability will trigger once for each of those creatures.').

card_ruling('healing leaves', '2007-02-01', 'This is the timeshifted version of Healing Salve.').

card_ruling('heart of bogardan', '2008-04-01', 'In other words, if you sacrifice Heart of Bogardan rather than pay its cumulative upkeep, X is equal to the amount of mana last spent to pay its cumulative upkeep.').

card_ruling('heart of ramos', '2004-10-04', 'You can activate the sacrifice ability while this card is tapped.').

card_ruling('heart of yavimaya', '2004-10-04', 'You have to sacrifice a forest before this card is put onto the battlefield, and no matter how it is put onto the battlefield.').

card_ruling('heart sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('heart sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('heart-piercer bow', '2014-09-20', 'The controller of Heart-Piercer Bow chooses the target of the triggered ability. Heart-Piercer Bow is the source of the damage, not the equipped creature.').
card_ruling('heart-piercer bow', '2014-09-20', 'The triggered ability resolves before blockers are declared.').
card_ruling('heart-piercer bow', '2014-09-20', 'In some rare cases, a Heart-Piercer Bow you control may be attached to a creature that’s attacking you or a planeswalker you control. In that case, you choose the target of the triggered ability, but as you are the defending player, you must choose a creature you control as the target.').

card_ruling('heartlash cinder', '2008-08-01', 'Chroma abilities check only mana costs, which are found in a card\'s upper right corner. They don\'t count mana symbols that appear in a card\'s text box.').
card_ruling('heartlash cinder', '2008-08-01', 'Chroma abilities count hybrid mana symbols of the appropriate color. For example, a card with mana cost {3}{U/R}{U/R} has two red mana symbols in its mana cost.').
card_ruling('heartlash cinder', '2008-08-01', 'The effect counts the mana symbols in this cards mana cost as well.').

card_ruling('heartless summoning', '2011-09-22', 'The first ability doesn\'t reduce any colored mana requirements of the creature spell.').

card_ruling('heartmender', '2013-06-07', 'If a creature with persist stops being a creature, persist will still work.').
card_ruling('heartmender', '2013-06-07', 'The persist ability triggers when the permanent is put into a graveyard. Its last known information (that is, how the creature last existed on the battlefield) is used to determine whether it had a -1/-1 counter on it.').
card_ruling('heartmender', '2013-06-07', 'If a permanent has multiple instances of persist, they\'ll each trigger separately, but the redundant instances will have no effect. If one instance returns the card to the battlefield, the next to resolve will do nothing.').
card_ruling('heartmender', '2013-06-07', 'If a token with no -1/-1 counters on it has persist, the ability will trigger when the token is put into the graveyard. However, the token will cease to exist and can\'t return to the battlefield.').
card_ruling('heartmender', '2013-06-07', 'When a permanent with persist returns to the battlefield, it\'s a new object with no memory of or connection to its previous existence.').
card_ruling('heartmender', '2013-06-07', 'If multiple creatures with persist are put into the graveyard at the same time (due to combat damage or a spell that destroys all creatures, for example), the active player (the player whose turn it is) puts all of his or her persist triggers on the stack in any order, then each other player in turn order does the same. The last trigger put on the stack is the first one that resolves. That means that in a two-player game, the nonactive player\'s persist creatures will return to the battlefield first, then the active player\'s persist creatures do the same. The creatures return to the battlefield one at a time.').
card_ruling('heartmender', '2013-06-07', 'If a creature with persist that has +1/+1 counters on it receives enough -1/-1 counters to cause it to be destroyed by lethal damage or put into its owner\'s graveyard for having 0 or less toughness, persist won\'t trigger and the card won\'t return to the battlefield. That\'s because persist checks the creature\'s existence just before it leaves the battlefield, and it still has all those counters on it at that point.').

card_ruling('heartseeker', '2004-12-01', 'Heartseeker grants an ability to the equipped creature. The ability can target a creature with protection from artifacts (unless the equipped creature is an artifact creature).').
card_ruling('heartseeker', '2004-12-01', 'The equipped creature\'s controller chooses whether or not to activate the activated ability. It doesn\'t matter who controls Heartseeker.').
card_ruling('heartseeker', '2004-12-01', '\"Unattach Heartseeker\" means just that -- Heartseeker moves off the creature it was equipping and remains on the battlefield.').

card_ruling('heartstone', '2004-10-04', 'The cost reduction can be applied to additional costs.').
card_ruling('heartstone', '2004-10-04', 'It will not add a {1} to abilities with no generic mana in their activation cost.').
card_ruling('heartstone', '2004-10-04', 'It will never reduce any colored mana portion of an activation cost.').
card_ruling('heartstone', '2006-07-15', 'Can\'t reduce Snow mana costs.').
card_ruling('heartstone', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('heartwood shard', '2004-10-04', 'You can pay either of the two costs (but not both at the same time) to activate the ability.').

card_ruling('heat stroke', '2007-05-01', 'This doesn\'t trigger until the end of combat.').

card_ruling('heaven\'s gate', '2004-10-04', 'It can target zero creatures if you want.').

card_ruling('heavy arbalest', '2011-01-01', 'The controller of the equipped creature may activate the damage ability, not the controller of Heavy Arbalest (if they somehow wind up being different players).').
card_ruling('heavy arbalest', '2011-01-01', 'If the equipped creature\'s ability is activated, that creature is the source of the damage. It doesn\'t matter if Heavy Arbalest leaves the battlefield or somehow becomes attached to another creature by that time.').
card_ruling('heavy arbalest', '2011-01-01', 'The equipped creature is the source of both the damage ability and the resultant damage, not Heavy Arbalest. For example, you can activate the ability to target and deal damage to a creature with protection from artifacts (assuming the equipped creature isn\'t an artifact itself, of course).').
card_ruling('heavy arbalest', '2011-01-01', 'The creature that doesn\'t untap during its controller\'s untap step is the one that Heavy Arbalest is equipped to at that time. If a different creature was tapped to activate the ability granted to it by the Arbalest, but the Arbalest is no longer equipping it, it will untap as normal.').

card_ruling('heavy fog', '2009-10-01', 'If all the attacking creatures attack your planeswalkers, you can\'t cast Heavy Fog. To cast it, a creature needs to have attacked _you_.').
card_ruling('heavy fog', '2009-10-01', 'Heavy Fog prevents only damage that would be dealt to you by attacking creatures. It won\'t prevent damage attacking creatures would deal to creatures or planeswalkers you control.').
card_ruling('heavy fog', '2009-10-01', 'Heavy Fog prevents all damage attacking creatures would deal to you this turn, not just combat damage. Noncombat damage from those creatures is prevented only if they\'re still attacking at the time that damage would be dealt or, if the creature has left the battlefield by then, if it was an attacking creature at the time it left.').

card_ruling('hecatomb', '2004-10-04', 'If you have less than 4 creatures, you do not partially sacrifice. You pay 4 creatures or none.').
card_ruling('hecatomb', '2004-10-04', 'The sacrifice is done as a triggered ability just after it enters the battlefield. It is not done on casting.').
card_ruling('hecatomb', '2004-10-04', 'You choose whether to sacrifice creatures or not on resolution. If not, then you sacrifice Hecatomb. You can choose to not sacrifice creatures if you no longer control this card on resolution.').

card_ruling('heckling fiends', '2011-01-22', 'The creature\'s controller still decides which player or planeswalker the creature attacks.').
card_ruling('heckling fiends', '2011-01-22', 'If, during a player\'s declare attackers step, a creature is tapped, is affected by a spell or ability that says it can\'t attack, or hasn\'t been under that player\'s control continuously since the turn began (and doesn\'t have haste), then it doesn\'t attack. If there\'s a cost associated with having a creature attack, the player isn\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').

card_ruling('hedge troll', '2007-02-01', 'This is the timeshifted version of Sedge Troll.').

card_ruling('hedonist\'s trove', '2015-02-25', 'The cards are exiled face up.').
card_ruling('hedonist\'s trove', '2015-02-25', 'The once-a-turn restriction of the last ability applies only to nonland cards. You can play a land card exiled with Hedonist’s Trove on the same turn you cast a nonland card exiled that way.').
card_ruling('hedonist\'s trove', '2015-02-25', 'Playing a card this way follows the normal rules for playing the card. You must pay its costs, and you must follow all applicable timing rules. For example, if one of the cards is a creature card, you can cast that card only during your main phase while the stack is empty.').
card_ruling('hedonist\'s trove', '2015-02-25', 'Under normal circumstances, you can play a land card exiled with Hedonist’s Trove only if you haven’t played a land yet that turn.').
card_ruling('hedonist\'s trove', '2015-02-25', 'If Hedonist’s Trove leaves the battlefield, any cards it exiled remain exiled. Those cards can no longer be played. Any future instance of Hedonist’s Trove (even one represented by the same card) will be a new object and won’t allow you to play those cards.').

card_ruling('hedron crab', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('hedron crab', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('hedron fields of agadeem', '2012-06-01', 'A creature\'s power is checked when attackers or blockers are declared. Increasing the power of an attacking or blocking creature to 7 or greater won\'t remove that creature from combat.').

card_ruling('hedron rover', '2010-03-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('hedron rover', '2010-03-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('hedron scrabbler', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('hedron scrabbler', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('hedron-field purists', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('hedron-field purists', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('hedron-field purists', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('hedron-field purists', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('hedron-field purists', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('hedron-field purists', '2010-06-15', 'The prevention effects prevent 1 or 2 damage, as applicable, from each source (regardless of who controls it) each time that source would deal damage to you or a creature you control. They prevent damage of any kind, not just combat damage.').
card_ruling('hedron-field purists', '2010-06-15', 'Remember that Hedron-Field Purists never has both of these prevention effects at the same time.').
card_ruling('hedron-field purists', '2010-06-15', 'If a source would deal damage to multiple applicable recipients, the prevention effects apply to each of those recipients separately. For example, if Hedron-Field Purists is level 1 and you cast Earthquake (\"Earthquake deals X damage to each creature without flying and each player\") with X equal to 3, that Earthquake will deal 2 damage to you, 2 damage to each creature without flying you control, 3 damage to each other player, and 3 damage to each creature without flying other players control.').
card_ruling('hedron-field purists', '2010-06-15', 'The prevention effects don\'t affect damage dealt directly to a planeswalker you control (such as combat damage, or damage Sarkhan the Mad deals to himself due to his first ability). They can prevent noncombat damage that\'s redirected from you to a planeswalker you control if you apply these effects first.').
card_ruling('hedron-field purists', '2010-06-15', 'The effects from multiple Hedron-Field Purists are cumulative.').

card_ruling('heir of the wilds', '2014-09-20', 'Heir of the Wilds has a triggered ferocious ability with an intervening “if” clause. That ability will check if you control a creature with power 4 or greater whenever Heir of the Wilds attacks. If you don’t at that time, the ability won’t trigger at all. If it triggers, the ability will also check if you control a creature with power 4 or greater as it resolves. If you don’t at that time, the ability won’t do anything. Note that the creature with power 4 or greater you control as the ability triggers doesn’t necessarily have to be the same one you control as the ability resolves.').
card_ruling('heir of the wilds', '2014-09-20', 'Some ferocious abilities that appear on instants and sorceries use the word “instead.” These spells have an upgraded effect if you control a creature with power 4 or greater as they resolve. For these, you only get the upgraded effect, not both effects.').
card_ruling('heir of the wilds', '2014-09-20', 'Ferocious abilities of instants and sorceries that don’t use the word “instead” will provide an additional effect if you control a creature with power 4 or greater as they resolve.').

card_ruling('heliod\'s emissary', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('heliod\'s emissary', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('heliod\'s emissary', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('heliod\'s emissary', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('heliod\'s emissary', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('heliod\'s emissary', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('heliod\'s emissary', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('heliod\'s pilgrim', '2014-07-18', 'Enchantment creatures with bestow from the Theros block aren’t Aura cards in your library. You can’t find one with Heliod’s Pilgrim.').

card_ruling('heliod, god of the sun', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('heliod, god of the sun', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('heliod, god of the sun', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('heliod, god of the sun', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('heliod, god of the sun', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('heliod, god of the sun', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('heliod, god of the sun', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('heliod, god of the sun', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('heliod, god of the sun', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('heliod, god of the sun', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').

card_ruling('helix pinnacle', '2008-08-01', 'Helix Pinnacle\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless Helix Pinnacle already has 100 or more tower counters on it as your upkeep begins, and (2) the ability will do nothing if Helix Pinnacle has lost enough counters such that it has fewer than 100 counters on it when the ability resolves.').
card_ruling('helix pinnacle', '2008-08-01', 'If Helix Pinnacle leaves the battlefield before its triggered ability resolves, check the number of counters that were on it as it last existed on the battlefield.').

card_ruling('hell\'s caretaker', '2004-10-04', 'Can sacrifice itself.').
card_ruling('hell\'s caretaker', '2005-08-01', 'This is not a beginning of upkeep triggered ability; you can activate it any time during your upkeep.').
card_ruling('hell\'s caretaker', '2005-08-01', 'The ability does not give the returned creature Haste.').
card_ruling('hell\'s caretaker', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('hell\'s thunder', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('hell\'s thunder', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('hell\'s thunder', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('hell\'s thunder', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('hell\'s thunder', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('hellcarver demon', '2010-06-15', 'The triggered ability is mandatory. All the permanents you control, including all lands, will be sacrificed.').
card_ruling('hellcarver demon', '2010-06-15', 'The cards from your library are exiled face up.').
card_ruling('hellcarver demon', '2010-06-15', 'You cast nonland cards from exile as part of the resolution of Hellcarver Demon\'s ability. You may cast those cards in any order. Timing restrictions based on the card\'s type (such as creature or sorcery) are ignored. Other restrictions are not (such as \"Cast [this card] only before the combat damage step\"). You cast all of the cards you like, putting them onto the stack, then Hellcarver Demon\'s ability finishes resolving. The spells you cast this way will then resolve as normal, one at a time, in the opposite order that they were put on the stack.').
card_ruling('hellcarver demon', '2010-06-15', 'If you cast a card \"without paying its mana cost,\" you can\'t pay any alternative costs. On the other hand, if the card has optional additional costs (such as kicker or multikicker), you may pay those when you cast the card. If the card has mandatory additional costs (such as Momentous Fall does), you must pay those when you cast the card.').
card_ruling('hellcarver demon', '2010-06-15', 'Any cards you can\'t cast (because they\'re lands or they have no legal targets, perhaps) or choose not to cast simply remain exiled.').
card_ruling('hellcarver demon', '2010-06-15', 'If you control two Hellcarver Demons that deal combat damage to a player at the same time, each Demon\'s ability triggers. When the first one resolves, you\'ll sacrifice all permanents except that Hellcarver Demon (meaning you\'ll sacrifice the other one). Then you\'ll exile six cards from your library and may cast nonland cards exiled that way. After those spells resolve, the other Demon\'s ability resolves. You\'ll sacrifice all permanents you now control (including the first Demon and any permanents put on the battlefield as a result of the spells you just cast), exile six more cards from your library, and cast any number of them.').

card_ruling('hellfire', '2009-10-01', 'Hellfire checks the number of creatures put into graveyards as the result of its effect, not the number of creatures destroyed as a result of its effect. For example, a replacement effect that says \"If a creature would be put into a graveyard from the battlefield, exile it instead\" won\'t affect whether a creature is destroyed or not, but will affect whether it\'s put into a graveyard or not.').
card_ruling('hellfire', '2009-10-01', 'A creature that regenerates instead of being destroyed doesn\'t leave the battlefield, so it\'s not put into a graveyard.').
card_ruling('hellfire', '2009-10-01', 'A token creature that\'s destroyed is put into its owner\'s graveyard, and will be counted by Hellfire\'s effect. (Immediately after Hellfire finishes resolving, the token will cease to exist.)').

card_ruling('hellfire mongrel', '2009-10-01', 'Hellfire Mongrel\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless the opponent whose turn it is has two or fewer cards in hand as his or her upkeep starts, and (2) the ability will do nothing if that player has three or more cards in hand by the time it resolves.').
card_ruling('hellfire mongrel', '2009-10-01', 'In a Two-Headed Giant game, this ability will potentially trigger twice at the beginning of the opposing team\'s upkeep -- once for each player on that team.').

card_ruling('hellhole flailer', '2012-10-01', 'The damage dealt by the last ability is equal to Hellhole Flailer\'s power when it was last on the battlefield. For example, if it had a +1/+1 counter on it, it will deal 4 damage.').
card_ruling('hellhole flailer', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('hellhole flailer', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('hellhole flailer', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('hellhole flailer', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('hellkite charger', '2009-10-01', 'You decide whether to pay {5}{R}{R} as Hellkite Charger\'s ability resolves.').
card_ruling('hellkite charger', '2009-10-01', 'If you pay {5}{R}{R}, the new combat phase immediately follows the current combat phase. There is no main phase in between.').
card_ruling('hellkite charger', '2009-10-01', 'Hellkite Charger\'s ability may trigger multiple times in the same turn, since its own ability gives it multiple chances to attack. Each time it resolves, you may create an additional combat phase.').
card_ruling('hellkite charger', '2009-10-01', 'If two Hellkite Chargers attack at the same time, both of their abilities trigger. If you pay {5}{R}{R} for each, two new combat phases will be created. However, all attacking creatures untap as those abilities resolve, not as the combat phases start. Any creature that attacks in the second combat phase will remain tapped during the third combat phase.').

card_ruling('hellkite hatchling', '2009-02-01', 'Hellkite Hatchling has flying and trample if at least one creature was sacrificed as a result of the Hatchling\'s devour ability as it entered the battlefield. It retains those abilities even if its +1/+1 counters are somehow removed.').
card_ruling('hellkite hatchling', '2009-02-01', 'If another creature becomes a copy of Hellkite Hatchling (due to Mirrorweave, for example), the second ability checks to see whether that creature -- not the original Hellkite Hatchling -- devoured a creature as it entered the battlefield.').

card_ruling('hellkite igniter', '2011-06-01', 'The number of artifacts you control is counted when the ability resolves.').

card_ruling('hellkite tyrant', '2013-01-24', 'If you don\'t control twenty or more artifacts at the beginning of your upkeep, Hellkite Tyrant\'s last ability won\'t trigger. If it does trigger, it will check again when it tries to resolve. If you don\'t control twenty or more artifacts at that time, the ability will do nothing.').

card_ruling('hellraiser goblin', '2013-01-24', 'Hellraiser Goblin\'s ability also affects itself. If it enters the battlefield before combat, it will have to attack that combat if able.').
card_ruling('hellraiser goblin', '2013-01-24', 'If, during your declare attackers step, a creature you control is tapped or is affected by a spell or ability that says it can\'t attack, then it doesn\'t attack. If there\'s a cost associated with having a creature attack, you aren\'t forced to pay that cost, so it doesn\'t have to attack in that case either.').
card_ruling('hellraiser goblin', '2013-01-24', 'Unlike many other effects that force a creature to attack if able, Hellraiser Goblin\'s ability applies during each combat. If a turn has multiple combat phases, creatures you control must attack in each of them if able.').

card_ruling('hellrider', '2011-01-22', 'This damage is not combat damage and may be redirected to a planeswalker controlled by the defending player.').
card_ruling('hellrider', '2011-01-22', 'If a creature you control attacks a planeswalker, the defending player is the controller of that planeswalker.').
card_ruling('hellrider', '2011-01-22', 'In some multiplayer variants, creatures you control may attack multiple players and/or planeswalkers. For each attacking creature, Hellrider will deal damage to the corresponding defending player.').

card_ruling('helm of awakening', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('helm of awakening', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('helm of awakening', '2004-10-04', 'The effect is cumulative.').
card_ruling('helm of awakening', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('helm of awakening', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('helm of awakening', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('helm of chatzuk', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('helm of chatzuk', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('helm of chatzuk', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('helm of chatzuk', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('helm of kaldra', '2004-12-01', 'The token isn\'t put onto the battlefield unless you control all three Equipment when the ability resolves.').
card_ruling('helm of kaldra', '2004-12-01', 'Helm of Kaldra is the third card in the Kaldra cycle. Sword of Kaldra was in the Mirrodin set. Shield of Kaldra was in the Darksteel set.').
card_ruling('helm of kaldra', '2004-12-01', 'The token is named Kaldra, has the supertype legendary, and has creature type Avatar.').

card_ruling('helm of obedience', '2008-10-01', 'You put the creature card onto the battlefield even if you can\'t sacrifice Helm of Obedience (because it\'s left the battlefield by the time its ability resolves, for example).').
card_ruling('helm of obedience', '2008-10-01', 'If an effect like that of Leyline of the Void prevents cards from being put into your opponent\'s graveyard, the process described in the first sentence of Helm of Obedience\'s effect will never stop. Your opponent\'s entire library will be exiled, even if X is 1.').

card_ruling('helm of the ghastlord', '2008-05-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').
card_ruling('helm of the ghastlord', '2008-05-01', 'With regards to the two triggered abilities that Helm of the Ghastlord grants to the enchanted creature, the point when damage is actually dealt is the only time it matters what color the creature is. If the creature is blue or black at that time, the appropriate ability or abilities will trigger. They\'ll resolve even if the creature changes color or loses its Aura.').
card_ruling('helm of the ghastlord', '2008-05-01', 'The two abilities trigger when the enchanted creature deals damage to an opponent of the creature\'s controller, not when it deals damage to an opponent of the Helm\'s controller. They trigger from any damage, not just combat damage.').

card_ruling('helm of the gods', '2015-06-22', 'If you cast an Aura spell targeting a permanent controlled by an opponent, you still control that Aura. It will count toward the bonus given by Helm of the Gods.').

card_ruling('helvault', '2011-01-22', 'Helvault\'s first and third abilities are linked. Similarly, Helvault\'s second and third abilities are linked. Only cards exiled as a result of Helvault\'s first or second abilities will be returned to the battlefield by its third ability.').

card_ruling('herald of anafenza', '2014-09-20', 'The Warrior token is created before the +1/+1 counter is put on Herald of Anafenza.').
card_ruling('herald of anafenza', '2014-09-20', 'The cost to activate a creature’s outlast ability includes the tap symbol ({T}). A creature’s outlast ability can’t be activated unless that creature has been under your control continuously since the beginning of your turn.').
card_ruling('herald of anafenza', '2014-09-20', 'Several creatures with outlast also grant an ability to creatures you control with +1/+1 counters on them, including themselves. These counters could come from an outlast ability, but any +1/+1 counter on the creature will count.').

card_ruling('herald of dromoka', '2015-02-25', 'If an attacking creature loses vigilance, it remains untapped and attacking.').

card_ruling('herald of kozilek', '2015-08-25', 'Herald of Kozilek’s last ability doesn’t change the mana cost or converted mana cost of any spell. It changes only the total cost you actually pay.').
card_ruling('herald of kozilek', '2015-08-25', 'Herald of Kozilek’s last ability can’t reduce the amount of colored mana you pay for a spell. It reduces only the generic component of that mana cost.').
card_ruling('herald of kozilek', '2015-08-25', 'If there are additional costs to cast a spell, or if the cost to cast a spell is increased by an effect, apply those increases before applying cost reductions.').
card_ruling('herald of kozilek', '2015-08-25', 'The cost reduction can apply to alternative costs.').
card_ruling('herald of kozilek', '2015-08-25', 'If a colorless spell you cast has {X} in its mana cost, you choose the value of X before calculating the spell’s total cost. For example, if that spell’s mana cost is {X}, you could choose 5 as the value of X and pay {4} to cast the spell.').
card_ruling('herald of kozilek', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('herald of kozilek', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('herald of kozilek', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('herald of kozilek', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('herald of kozilek', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('herald of leshrac', '2006-07-15', 'When Herald of Leshrac\'s cumulative upkeep ability resolves, if its age counters outnumber the number of lands on the battlefield that you don\'t control, you can\'t pay its cumulative upkeep cost and must sacrifice it.').
card_ruling('herald of leshrac', '2006-07-15', 'You must choose a different land you don\'t control for each age counter on Herald of Leshrac. Otherwise, you\'d try to gain control of a land you *do* control midway through paying the cost and need to back up.').
card_ruling('herald of leshrac', '2006-07-15', 'Herald of Leshrac\'s leaves-the-battlefield ability affects all lands you control but don\'t own, not just the ones you gained control of with Herald of Leshrac. For example, if you had gained control of an opponent\'s land with Annex, its owner will regain control of that land. Annex will remain attached to it, but its effect will be overridden.').
card_ruling('herald of leshrac', '2013-04-15', 'The cumulative upkeep trigger doesn\'t target what you gain control of, and you don\'t choose lands until the ability resolves. Your opponents can not tap lands in response to the choice, but they could respond to the ability by tapping any number of lands before you\'ve made any choices.').

card_ruling('herald of the pantheon', '2015-06-22', 'Herald of the Pantheon’s first ability can’t reduce the colored mana requirement of an enchantment spell.').
card_ruling('herald of the pantheon', '2015-06-22', 'If there are additional costs to cast an enchantment spell, apply those before applying cost reductions.').
card_ruling('herald of the pantheon', '2015-06-22', 'Herald of the Pantheon can reduce alternative costs such as bestow costs.').

card_ruling('herald of torment', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('herald of torment', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('herald of torment', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('herald of torment', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('herald of torment', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('herald of torment', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('herald of torment', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('herald of torment', '2014-02-01', 'The triggered ability that causes you to lose life applies whether Herald of Torment is a creature or an Aura. The “you” in that ability refers to Herald of Torment’s controller. If you control Herald of Torment as an Aura enchanting a creature another player controls, that ability will trigger at the beginning of your upkeep and cause you to lose 1 life.').

card_ruling('herald of war', '2012-05-01', 'The +1/+1 counter is put on Herald of War before blockers are declared and combat damage is dealt.').
card_ruling('herald of war', '2012-05-01', 'The cost reduction is based on the total number of +1/+1 counters on Herald of War, not just the ones put on it by its own ability.').
card_ruling('herald of war', '2012-05-01', 'If there are additional costs to cast a creature spell, such as a kicker cost, apply those increases before applying cost reductions.').
card_ruling('herald of war', '2012-05-01', 'A creature spell that\'s both an Angel and a Human will cost {1} less to cast for each +1/+1 counter on Herald of War.').

card_ruling('herdchaser dragon', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('herdchaser dragon', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('herdchaser dragon', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('heretic\'s punishment', '2011-09-22', 'If the targeted creature or player is an illegal target by the time the ability resolves, the entire ability will be countered. No cards will be put into your graveyard, and no damage will be dealt.').
card_ruling('heretic\'s punishment', '2011-09-22', 'If you have two or fewer cards in your library when the ability resolves, all of them will be put into your graveyard. Heretic\'s Punishment will still deal damage equal to the highest converted mana cost among those cards.').
card_ruling('heretic\'s punishment', '2011-09-22', 'The converted mana cost of a double-faced card in your graveyard is the converted mana cost of the front face.').
card_ruling('heretic\'s punishment', '2011-09-22', 'If all three cards have a converted mana cost of 0, no damage will be dealt.').
card_ruling('heretic\'s punishment', '2011-09-22', 'If a split card is put into your graveyard this way, consider each cost individually. For example, if Crime/Punishment, a split card with converted mana costs of 2 and 5, is put into your graveyard as well as a land and a card with converted mana cost 6, 6 damage is dealt because 6 is greater than 5 and greater than 2.').

card_ruling('heritage druid', '2008-04-01', 'Since the activated ability doesn\'t have a tap symbol in its cost, you can tap creatures (including Heritage Druid itself) that haven\'t been under your control since your most recent turn began to pay the cost.').

card_ruling('hermit druid', '2004-10-04', 'You can put the cards from your library into your graveyard in any order.').

card_ruling('hero of bladehold', '2011-06-01', 'Whenever Hero of Bladehold attacks, both abilities will trigger. You can put them onto the stack in any order. If the token-creating ability resolves first, the tokens each get +1/+0 until end of turn from the battle cry ability.').
card_ruling('hero of bladehold', '2011-06-01', 'You choose which opponent or planeswalker an opponent controls that each token is attacking when it is put onto the battlefield.').
card_ruling('hero of bladehold', '2011-06-01', 'Although the tokens are attacking, they never were declared as attacking creatures (for purposes of abilities that trigger whenever a creature attacks, for example).').

card_ruling('hero of goma fada', '2015-08-25', 'When an Ally enters the battlefield under your control, each rally ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('hero of goma fada', '2015-08-25', 'If a creature with a rally ability enters the battlefield under your control at the same time as other Allies, that ability will trigger once for each of those creatures and once for the creature with the ability itself.').

card_ruling('hero of iroas', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('hero of iroas', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('hero of iroas', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('hero of iroas', '2014-02-01', 'Hero of Iroas’s first ability will apply if you cast a card for its bestow cost.').

card_ruling('hero of leina tower', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('hero of leina tower', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('hero of leina tower', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').
card_ruling('hero of leina tower', '2014-02-01', 'You decide the value of X and choose whether to pay {X} as the heroic ability resolves.').

card_ruling('hero of oxid ridge', '2011-06-01', 'The power of each creature is checked only when blocking creatures are declared. If a creature has power 1 or less at that time, it can\'t block, even if its power was greater than 1 when the last ability resolved.').
card_ruling('hero of oxid ridge', '2011-06-01', 'Creatures with power 1 or less can\'t block even if they weren\'t on the battlefield when the last ability resolved.').

card_ruling('hero\'s blade', '2014-11-24', 'The triggered ability will trigger when one of the Gods from Theros block enters the battlefield only if your devotion is high enough that it’s a creature when it enters. If Hero’s Blade is attached to a God that stops being a creature (or any creature that stops being a creature), it will become unattached.').

card_ruling('heroes remembered', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('heroes remembered', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('heroes remembered', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('heroes remembered', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('heroes remembered', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('heroes remembered', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('heroes remembered', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('heroes remembered', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('heroes remembered', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('heroes remembered', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('heroes\' bane', '2014-04-26', 'The value of X is determined using the power of Heroes’ Bane when the ability resolves.').
card_ruling('heroes\' bane', '2014-04-26', 'If Heroes’ Bane’s power is less than 0 when its second ability resolves, X is considered to be 0.').

card_ruling('heroes\' podium', '2014-02-01', 'The bonus given by Heroes’ Podium counts only legendary creatures. It won’t count itself unless some other effect causes it to be a creature in addition to being a land (in which case it will also get the bonus).').
card_ruling('heroes\' podium', '2014-02-01', 'Heroes’ Podium will give a bonus to each legendary creature you control, even if gaining control of one causes the “legend rule” to apply. For example, if you control Brimaz, King of Oreskos (a 3/4 legendary creature), and gain control of another one, they’ll each be 4/5 when you put one into its owner’s graveyard. Then the remaining one will return to being 3/4.').

card_ruling('heroism', '2004-10-04', 'The cost to avoid the penalty must be paid when the spell resolves. You can\'t wait until later to do so.').
card_ruling('heroism', '2004-10-04', 'Paying the Heroism cost to allow a creature to deal damage does not prevent other effects from preventing it from dealing damage. Paying the cost only prevents the Heroism effect.').

card_ruling('hesitation', '2004-10-04', 'Playing a land is not casting a spell, so playing a land will not trigger this card.').

card_ruling('hewed stone retainers', '2014-11-24', 'It doesn’t matter whether the other spell resolved. It could have been countered or, if you’ve somehow cast Hewed Stone Retainers as though it had flash, it could still be on the stack.').

card_ruling('hex', '2005-10-01', 'You must target six different creatures. If you can\'t, you can\'t cast Hex. If some of the creatures become illegal targets before the spell resolves, Hex will still destroy the rest of them.').

card_ruling('hex parasite', '2011-06-01', 'You may choose a value for X that\'s greater than the number of counters on the targeted permanent. However, Hex Parasite only gets the bonus for counters actually removed.').
card_ruling('hex parasite', '2011-06-01', 'If the permanent has multiple kinds of counters, you choose how many of each to remove when the ability resolves.').

card_ruling('hibernation sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('hibernation sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('hibernation\'s end', '2006-07-15', 'The converted mana cost of the creature you find must be exactly equal to the number of age counters on Hibernation\'s End.').

card_ruling('hidden ancients', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden ancients', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('hidden ancients', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden dragonslayer', '2015-02-25', 'Turning a face-down creature with megamorph face up and putting a +1/+1 counter on it is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('hidden dragonslayer', '2015-02-25', 'If a face-down creature with megamorph is turned face up some other way (for example, if you manifest a card with megamorph and then pay its mana cost to turn it face up), you won’t put a +1/+1 counter on it.').
card_ruling('hidden dragonslayer', '2015-02-25', 'Megamorph is a variant of the morph ability. You can find more information on morph on cards with morph from the Khans of Tarkir set.').

card_ruling('hidden gibbons', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden gibbons', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('hidden gibbons', '2008-08-01', 'The ability triggers when the spell is cast, so it becomes a creature before the spell resolves.').
card_ruling('hidden gibbons', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden guerrillas', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden guerrillas', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('hidden guerrillas', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden herd', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden herd', '2004-10-04', 'Does not trigger on lands put onto the battlefield by a spell or ability.').
card_ruling('hidden herd', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden horror', '2008-04-01', 'When the ability resolves, you choose whether to sacrifice the creature or perform the other action. If you can\'t perform the other action, then you must sacrifice the creature.').
card_ruling('hidden horror', '2008-04-01', 'If the creature is no longer on the battlefield when the ability resolves, you may still perform the action if you want.').

card_ruling('hidden predators', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden predators', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden retreat', '2004-10-04', 'This can be used on damage to a creature or player.').
card_ruling('hidden retreat', '2004-10-04', 'This can be used on instant or sorcery spells on the stack, but not on abilities.').

card_ruling('hidden spider', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden spider', '2008-04-01', 'This card now uses the Reach keyword ability to enable the blocking of flying creatures. This works because a creature with flying can only be blocked by creatures with flying or reach.').
card_ruling('hidden spider', '2008-05-01', 'The ability only triggers if the creature spell has flying, regardless of whether an effect would cause the creature to have flying once it entered the battlefield.').
card_ruling('hidden spider', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden stag', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('hidden stag', '2004-10-04', 'Does not trigger on lands put onto the battlefield by a spell or ability.').
card_ruling('hidden stag', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('hidden strings', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('hidden strings', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('hidden strings', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('hidden strings', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('hidden strings', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('hidden strings', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('hidden strings', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('hidden strings', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('hidden strings', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('hidden strings', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('hidden strings', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('hideous end', '2009-10-01', 'If the targeted creature is an illegal target by the time Hideous End resolves, the entire spell is countered. No one loses life.').

card_ruling('hideous laughter', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('hideous laughter', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('hideous laughter', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('hideous laughter', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('hideous visage', '2011-09-22', 'If an attacking creature has intimidate, what colors it is matters only as the defending player declares blockers. Once it\'s blocked, changing its colors won\'t change that.').
card_ruling('hideous visage', '2011-09-22', 'A multicolored creature with intimidate can be blocked by any creature that shares a color with it, in addition to artifact creatures. For example, a white-blue creature with intimidate can be blocked by white creatures and/or blue creatures, no matter what other colors they are.').

card_ruling('hidetsugu\'s second rite', '2005-06-01', 'Since the player\'s life total is checked only as the spell resolves, you can cast Hidetsugu\'s Second Rite targeting any player, regardless of life total. If the player isn\'t at 10 life as Hidetsugu\'s Second Rite begins resolving, it does nothing and finishes resolving. Note that since having 10 life is not a targeting restriction, the spell won\'t actually be countered for having an illegal target; it just won\'t do anything.').

card_ruling('high ground', '2007-07-15', 'High Ground\'s effect is cumulative. If you have a creature that can already block an additional creature, now it can block three creatures.').
card_ruling('high ground', '2007-07-15', 'High Ground allows you to make some complicated blocks. For example, if you\'re being attacked by three creatures (call them A, B, and C) and you control three creatures (X, Y, and Z), you can have X block A and B, Y block B and C, and Z block just C, among many other possible options. The defending player chooses how each blocking creature\'s combat damage will be divided among the creatures it\'s blocking.').

card_ruling('high priest of penance', '2013-01-24', 'High Priest of Penance\'s ability triggers once for each instance of damage dealt to it, not once for each 1 damage. The ability will trigger even if High Priest of Penance is dealt lethal damage.').

card_ruling('high sentinels of arashin', '2014-09-20', 'The activated ability can target and put +1/+1 counters on High Sentinels of Arashin itself.').
card_ruling('high sentinels of arashin', '2014-09-20', 'High Sentinels of Arashin gets +1/+1 per creature, not per +1/+1 counter. It doesn’t matter how many +1/+1 counters are on any other creature you control as long as there’s one or more.').

card_ruling('high tide', '2010-03-01', 'The delayed triggered ability triggers and produces {U} any time a land with the land-type Island is tapped for mana, even if that land has other types or the mana for which it was tapped is another color. For example, if you tap a Breeding Pool for {G}, High Tide will give you an extra {U}.').
card_ruling('high tide', '2010-03-01', 'The effect applies even if the Island in question entered the battlefield after High Tide resolved.').

card_ruling('highland berserker', '2009-10-01', 'The ability affects only Allies you control at the time the ability resolves.').

card_ruling('higure, the still wind', '2005-02-01', 'If Higure deals regular combat damage and you use its ability to search for another Ninja, that card won\'t be in your hand in time to activate its ninjutsu ability to put it onto the battlefield and deal damage. However, the card you searched for could still be swapped for an unblocked creature, including Higure, as long as you activate its ninjutsu ability before the end of combat.').
card_ruling('higure, the still wind', '2005-02-01', 'If Higure deals first strike damage (or the first part of double strike damage), the triggered ability would resolve before regular combat damage, and the searched-for Ninja would be able to get swapped with an unblocked creature and still deal its regular damage.').

card_ruling('hikari, twilight guardian', '2004-12-01', 'You may choose not to exile Hikari.').
card_ruling('hikari, twilight guardian', '2004-12-01', 'If you exile Hikari, it leaves the battlefield before the spell that triggered it resolves its ability, so it will be unaffected by that spell.').

card_ruling('hindering light', '2008-10-01', 'Hindering Light can target a spell that has multiple targets, as long as at least one of those targets is you or a permanent you control.').
card_ruling('hindering light', '2008-10-01', 'You may choose to target a spell that \"can\'t be countered.\" If you do, the first part of Hindering Light\'s effect won\'t do anything, but you\'ll still get to draw a card.').

card_ruling('hindering touch', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('hindering touch', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('hindering touch', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('hindering touch', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('hindering touch', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('hindervines', '2013-01-24', 'Hindervines checks whether a creature has a +1/+1 counter on it at the moment it deals damage. It doesn\'t matter whether a creature had a +1/+1 counter, or was even on the battlefield, when Hindervines resolved.').

card_ruling('hint of insanity', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('hinterland hermit', '2011-01-22', 'If Hinterland Scourge is attacking, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block it.').

card_ruling('hired torturer', '2013-04-15', 'The card stops being revealed after Hired Torturer\'s ability resolves.').
card_ruling('hired torturer', '2013-04-15', 'You can choose an opponent with no cards in hand as the target of Hired Torturer\'s ability. If the target opponent has no cards in hand when the ability resolves, that player will just lose 2 life.').
card_ruling('hired torturer', '2013-04-15', 'Each card in the opponent\'s hand has an equal chance of being revealed, even if it was previously revealed because of Hired Torturer\'s ability.').

card_ruling('hive mind', '2009-10-01', 'Hive Mind\'s effect is mandatory. Each other player must copy the spell whether they want to or not.').
card_ruling('hive mind', '2009-10-01', 'Hive Mind will copy any instant or sorcery spell, not just one with targets.').
card_ruling('hive mind', '2009-10-01', 'If a player casts an instant or sorcery spell, Hive Mind\'s ability triggers and is put on the stack on top of that spell. Hive Mind\'s ability will resolve first. When it does, it creates a number of copies of that spell equal to the number of players in the game minus one. First the player whose turn it is (or, if that\'s the player who cast the original spell, the player to that player\'s left) puts his or her copy on the stack, choosing new targets for it if he or she likes. Then each other player in turn order does the same. The last copy put on the stack will be the first one that resolves. (Note that the very last thing to happen is that the original spell resolves.)').
card_ruling('hive mind', '2009-10-01', 'The copies that Hive Mind\'s ability creates are created on the stack, so they\'re not \"cast.\" Abilities that trigger when a player casts a spell (like Hive Mind\'s ability itself) won\'t trigger.').
card_ruling('hive mind', '2009-10-01', 'Each copy will have the same targets as the spell it\'s copying unless its controller chooses new ones. That player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('hive mind', '2009-10-01', 'If the spell that\'s copied is modal (that is, it says \"Choose one --\" or the like), the copy will have the same mode. A player can\'t choose a different one.').
card_ruling('hive mind', '2009-10-01', 'If the spell that\'s copied has an X whose value was determined as it was cast (like Earthquake does), the copy has the same value of X.').
card_ruling('hive mind', '2009-10-01', 'A copy\'s controller can\'t choose to pay any additional costs for the copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for the copy too.').
card_ruling('hive mind', '2009-10-01', 'If a copy says that it affects \"you,\" it affects the controller of the copy, not the controller of the original spell. Similarly, if a copy says that it affects an \"opponent,\" it affects an opponent of the copy\'s controller, not an opponent of the original spell\'s controller.').

card_ruling('hive stirrings', '2013-07-01', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').
card_ruling('hive stirrings', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').

card_ruling('hivestone', '2006-09-25', 'If a creature is already a Sliver, Hivestone has no effect on it.').
card_ruling('hivestone', '2006-10-15', 'Turns only your creatures on the battlefield, not in other zones, into Slivers. It won\'t allow you to have Root Sliver on the battlefield and make your Grizzly Bears uncounterable, for example.').

card_ruling('hixus, prison warden', '2015-06-22', 'Hixus’s ability causes a zone change with a duration, a style of ability that’s somewhat reminiscent of older cards like Oblivion Ring. However, unlike Oblivion Ring, cards like Hixus have a single ability that creates two one-shot effects: one that exiles the creature when the ability resolves, and another that returns the exiled card to the battlefield immediately after Hixus leaves the battlefield.').
card_ruling('hixus, prison warden', '2015-06-22', 'If Hixus leaves the battlefield before its triggered ability resolves, the creature that dealt combat damage to you won’t be exiled.').
card_ruling('hixus, prison warden', '2015-06-22', 'Auras attached to the exiled creatures will be put into their owners’ graveyards. Equipment attached to the exiled creatures will become unattached and remain on the battlefield. Any counters on the exiled creatures will cease to exist.').
card_ruling('hixus, prison warden', '2015-06-22', 'If a creature token is exiled, it ceases to exist. It won’t be returned to the battlefield.').
card_ruling('hixus, prison warden', '2015-06-22', 'The exiled cards return to the battlefield immediately after Hixus leaves the battlefield. Nothing happens between the two events, including state-based actions.').
card_ruling('hixus, prison warden', '2015-06-22', 'In a multiplayer game, if Hixus’s owner leaves the game, the exiled cards will return to the battlefield. Because the one-shot effect that returns the cards isn’t an ability that goes on the stack, it won’t cease to exist along with the leaving player’s spells and abilities on the stack.').

card_ruling('hoard-smelter dragon', '2011-01-01', 'If the targeted artifact is an illegal target by the time the activated ability resolves, the ability is countered. Hoard-Smelter Dragon won\'t get the bonus.').
card_ruling('hoard-smelter dragon', '2011-01-01', 'If the mana cost of a permanent includes {X}, that X is considered to be 0.').
card_ruling('hoard-smelter dragon', '2013-07-01', 'If the activated ability resolves but the targeted artifact isn\'t destroyed (perhaps because it has indestructible or it regenerates), Hoard-Smelter Dragon will still get the bonus.').

card_ruling('hoarder\'s greed', '2007-10-01', 'The effect will automatically repeat itself until its controller doesn\'t win the clash. There\'s no other way to stop it.').

card_ruling('hoarding dragon', '2014-07-18', 'The artifact card you find is exiled face up. All players can see what it is.').
card_ruling('hoarding dragon', '2014-07-18', 'If Hoarding Dragon is put into a graveyard before its first ability has resolved, its second ability will trigger and do nothing. Then its first ability will resolve. If you choose to find an artifact card from your library, it will be exiled for the rest of the game.').
card_ruling('hoarding dragon', '2014-07-18', 'Hoarding Dragon’s two abilities are linked. The second ability refers only to the card exiled by that Hoarding Dragon’s first ability. In other words, each Dragon has its own hoard.').
card_ruling('hoarding dragon', '2014-07-18', 'If Hoarding Dragon dies before its first ability resolves, its second ability will trigger and do nothing. Then its first ability will resolve. If you choose to exile an artifact card from your library, it will be exiled indefinitely.').

card_ruling('hokori, dust drinker', '2005-02-01', 'If there are two Hokori on the battlefield (for example, if Mirror Gallery is on the battlefield), each player untaps two lands he or she controls at the beginning of his or her upkeep.').

card_ruling('hold the gates', '2013-01-24', 'Creatures you control will have vigilance even if you control no Gates.').

card_ruling('hollow dogs', '2004-10-04', 'If it attacks more than once per turn, it gets the bonus each time.').

card_ruling('hollow specter', '2004-10-04', 'You decide on the value of X and pay {X} during resolution.').
card_ruling('hollow specter', '2004-10-04', 'X can be zero, but then that player discards nothing.').

card_ruling('hollow trees', '2004-10-04', 'It is considered \"tapped for mana\" if you activate its mana ability, even if you choose to take zero mana from it.').
card_ruling('hollow trees', '2004-10-04', 'This enters the battlefield tapped even if a continuous effect immediately changes it to something else.').
card_ruling('hollow trees', '2004-10-04', 'If the land is tapped by some external effect, no counters are removed from it.').
card_ruling('hollow trees', '2004-10-04', 'Counters are not lost if the land is changed to another land type. They wait around for the land to change back.').
card_ruling('hollow trees', '2004-10-04', 'Whether or not it is tapped is checked at the beginning of upkeep. If it is not tapped, the ability does not trigger. It also checks during resolution and you only get a counter if it is still tapped then.').

card_ruling('hollow warrior', '2004-10-04', 'This card can\'t tap itself to allow itself to attack or block. You have to tap a different creature which is not being declared as an attacker or blocker.').

card_ruling('hollowborn barghest', '2008-05-01', 'The first ability checks whether you have no cards in hand both when it triggers and when it resolves. If you have any cards in your hand at the beginning of your upkeep, it won\'t trigger at all. If you have any cards in your hand when the ability resolves, it does nothing.').
card_ruling('hollowborn barghest', '2008-05-01', 'The second ability behaves the same way, except that it\'s checking your opponent instead of you.').

card_ruling('hollowhenge spirit', '2011-01-22', 'Removing an attacking creature from combat doesn\'t untap that creature.').
card_ruling('hollowhenge spirit', '2011-01-22', 'Removing a blocking creature from combat doesn\'t cause the creature it was blocking to become unblocked. If no other creatures are blocking that attacking creature, it won\'t assign combat damage (unless it has trample).').
card_ruling('hollowhenge spirit', '2011-01-22', 'If there are multiple combat phases in a turn, a creature that\'s been removed from combat can still attack or block in future combat steps that turn.').

card_ruling('hollowsage', '2008-05-01', 'If Hollowsage becomes untapped during your untap step, the ability will trigger. However, since no player gets priority during the untap step, it waits to be put on the stack until your upkeep starts. At that time, all your \"beginning of upkeep\" triggers will also trigger. You can put them and Hollowsage\'s trigger on the stack in any order.').

card_ruling('holy justiciar', '2012-05-01', 'If the creature is an untapped Zombie, it will become tapped, then exiled.').

card_ruling('holy mantle', '2013-01-24', 'The enchanted creature can\'t be blocked, it can\'t be targeted by abilities of creatures or creature cards (like, notably, the scavenge ability), and all damage dealt to it by creatures or creature cards is prevented.').

card_ruling('homarid warrior', '2004-10-04', 'It taps when the ability resolves if it was not already tapped. It may be used even if it is already tapped.').
card_ruling('homarid warrior', '2005-08-01', 'Does not cause Auras on it to be removed when the effect is activated. An Aura on the battlefield is neither a spell nor an ability.').

card_ruling('homeward path', '2011-09-22', 'The owner of a card is the player who started the game with that card in his or her deck.').
card_ruling('homeward path', '2011-09-22', 'The owner of a token is the player under whose control the token was put onto the battlefield.').

card_ruling('homicidal brute', '2011-09-22', 'You\'ll tap and transform Homicidal Brute even if it couldn\'t attack.').

card_ruling('homing lightning', '2013-01-24', 'Homing Lightning has only one target. Other creatures with that name are not targeted. For example, a creature with hexproof will be dealt damage if it has the same name as the target creature.').
card_ruling('homing lightning', '2013-01-24', 'The name of a creature token is the same as its creature types unless the token is a copy of another creature or the effect that created the token specifically gives it a different name. For example, a 1/1 Soldier creature token is named “Soldier.”').

card_ruling('homing sliver', '2009-02-01', 'Unlike the normal cycling ability, Slivercycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Sliver card. After you find a Sliver card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('homing sliver', '2009-02-01', 'Slivercycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Slivercycling this card. Any ability that stops a cycling ability from being activated also stops Plainscycling from being activated.').
card_ruling('homing sliver', '2009-02-01', 'Slivercycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Slivercycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('homing sliver', '2009-02-01', 'You can choose to find any card with the Sliver creature type, even if it isn\'t a creature card. This includes, for example, Tribal cards with the Changeling ability. You can also choose not to find a card, even if there is a Sliver card in your graveyard.').
card_ruling('homing sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('homing sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('homura, human ascendant', '2005-06-01', 'Each Ascendant is legendary in both its unflipped and flipped forms. This means that effects that look for legendary creature cards, such as Time of Need and Captain Sisay, can find an Ascendant.').

card_ruling('honor\'s reward', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('honor\'s reward', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('honorable scout', '2004-10-04', 'If a creature is both black and red, you gain 2 life, not 4.').

card_ruling('honored hierarch', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('honored hierarch', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('honored hierarch', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('hooded assassin', '2014-11-24', 'If a creature was dealt damage but regenerated (which removes all damage from it), it will still be a legal target for the second mode of the triggered ability.').
card_ruling('hooded assassin', '2014-11-24', 'You choose which mode you’re using as you put the ability on the stack, after the creature has entered the battlefield. Once you’ve chosen a mode, you can’t change that mode even if the creature leaves the battlefield in response to that ability.').
card_ruling('hooded assassin', '2014-11-24', 'If a mode requires a target and there are no legal targets available, you must choose the mode that adds a +1/+1 counter.').

card_ruling('hooded horror', '2013-10-17', 'The number of creatures each player controls is evaluated only as blocking creatures are declared. If, at that time, the defending player doesn’t control the most creatures or isn’t tied for the most, Hooded Horror can be blocked.').
card_ruling('hooded horror', '2013-10-17', 'Once Hooded Horror is blocked, it doesn’t matter if the defending player controls the most creatures. The block won’t be undone.').

card_ruling('hooded hydra', '2014-09-20', 'Use the number of +1/+1 counters that were on Hooded Hydra the last time it was on the battlefield to determine how many Snake tokens to create.').
card_ruling('hooded hydra', '2014-09-20', 'Hooded Hydra’s last ability isn’t a triggered ability. It’s a replacement ability that modifies how Hooded Hydra is turned face up. Players can’t respond to Hooded Hydra being turned face up and having five +1/+1 counters put on it.').
card_ruling('hooded hydra', '2014-09-20', 'If Hooded Hydra is face down and it’s turned face up some other way than by having its morph cost paid, the last ability will still apply.').
card_ruling('hooded hydra', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('hooded hydra', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('hooded hydra', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('hooded hydra', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('hooded hydra', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('hooded hydra', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('hooded hydra', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('hooded hydra', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('hooded hydra', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('hoofprints of the stag', '2007-10-01', 'If a spell or ability has you draw multiple cards, Hoofprints of the Stag\'s ability triggers that many times.').

card_ruling('hooting mandrills', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('hooting mandrills', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('hooting mandrills', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('hooting mandrills', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('hooting mandrills', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('hope and glory', '2004-10-04', 'Must target two different creatures.').

card_ruling('hopeful eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('hopeful eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('hopeful eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('hopeful eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('hopeful eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('hopeful eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('hopeful eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('horde ambusher', '2014-09-20', 'Turning Horde Ambusher face up after blockers have been declared won’t remove any creature from combat or change how any creature is blocking.').
card_ruling('horde ambusher', '2014-09-20', 'If a face-down Horde Ambusher blocks and is then turned face up, its first ability won’t trigger because it didn’t have that ability when it blocked.').
card_ruling('horde ambusher', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('horde ambusher', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('horde ambusher', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('horde ambusher', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('horde ambusher', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('horde ambusher', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('horde ambusher', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('horde ambusher', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('horde ambusher', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('horde of notions', '2007-10-01', 'This ability does not allow you to pay the evoke cost of the targeted Elemental card.').

card_ruling('horizon chimera', '2013-09-15', 'If you draw multiple cards, Horizon Chimera’s ability will trigger that many times. Each of these abilities will cause a separate life-gaining event.').

card_ruling('horizon drake', '2010-03-01', 'Protection from lands works like any other protection ability. Horizon Drake can\'t be blocked by land creatures, all damage that would be dealt to it by lands (including combat damage from land creatures) is prevented, and it can\'t be the target of activated or triggered abilities from lands (including your own Teetering Peaks, for example).').

card_ruling('horizon scholar', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('horizon scholar', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('horizon scholar', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('horizon scholar', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('horizon spellbomb', '2013-04-15', 'When you sacrifice Horizon Spellbomb, it\'s search ability goes on the stack, and then the triggered draw ability goes on the stack on top of it. This means you first choose whether to pay {G} and draw a card, then you search; you do not search first.').

card_ruling('horn of greed', '2004-10-04', 'Playing a land will trigger it, but putting a land onto the battlefield as part of an effect will not.').

card_ruling('horn of ramos', '2004-10-04', 'You can activate the sacrifice ability while this card is tapped.').

card_ruling('horncaller\'s chant', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('horncaller\'s chant', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('horncaller\'s chant', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('horncaller\'s chant', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('horncaller\'s chant', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('horncaller\'s chant', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('horned kavu', '2004-10-04', 'You choose the creature to return on resolution of the triggered ability. This is not targeted.').

card_ruling('horned sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('horned sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('hornet harasser', '2007-10-01', 'Hornet Harasser\'s ability is mandatory. If you\'re the only player who controls any creatures, you must give one of your own creatures -2/-2.').

card_ruling('hornet nest', '2014-07-18', 'Hornet Nest’s triggered ability will trigger even if it’s dealt lethal damage. For example, if it’s dealt 7 damage, its ability will trigger and you’ll put seven Insect creature tokens onto the battlefield.').

card_ruling('horobi\'s whisper', '2005-02-01', 'Horobi\'s Whisper can target a creature even if you don\'t control a Swamp, but the spell does nothing.').
card_ruling('horobi\'s whisper', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('horobi\'s whisper', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('horobi\'s whisper', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('horobi\'s whisper', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('horribly awry', '2015-08-25', 'The converted mana cost of a spell doesn’t change if its controller paid an alternative cost (such as an evoke cost) to cast it.').
card_ruling('horribly awry', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('horribly awry', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('horribly awry', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('horribly awry', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('horribly awry', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('horrifying revelation', '2011-06-01', 'If the player has no cards in his or her hand or no cards in his or her library, the other effect still happens.').

card_ruling('hostility', '2007-10-01', 'If a spell you control would deal damage to an opponent and that opponent controls a planeswalker, that opponent chooses which of your effects to apply first. -- If that player chooses to apply Hostility\'s effect first, the damage is prevented, you put some tokens onto the battlefield, and the planeswalker redirection effect is moot because there\'s no damage to redirect. -- If that player chooses to apply the planeswalker redirection effect first, you have a choice. You can redirect the damage to the planeswalker (in which case Hostility\'s prevention effect is moot because nothing\'s dealing damage to the player anymore) or you can have your spell continue to deal damage to the opponent. If you choose the latter, Hostility\'s effect then applies, the damage is prevented, and you get the tokens.').
card_ruling('hostility', '2007-10-01', 'The last ability triggers when the Incarnation is put into its owner\'s graveyard from any zone, not just from on the battlefield.').
card_ruling('hostility', '2007-10-01', 'Although this ability triggers when the Incarnation is put into a graveyard from the battlefield, it doesn\'t *specifically* trigger on leaving the battlefield, so it doesn\'t behave like other leaves-the-battlefield abilities. The ability will trigger from the graveyard.').
card_ruling('hostility', '2007-10-01', 'If the Incarnation had lost this ability while on the battlefield (due to Lignify, for example) and then was destroyed, the ability would still trigger and it would get shuffled into its owner\'s library. However, if the Incarnation lost this ability when it was put into the graveyard (due to Yixlid Jailer, for example), the ability wouldn\'t trigger and the Incarnation would remain in the graveyard.').
card_ruling('hostility', '2007-10-01', 'If the Incarnation is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('hotheaded giant', '2008-08-01', 'Hotheaded Giant doesn\'t check whether you\'ve cast another red spell until Hotheaded Giant enters the battlefield.').
card_ruling('hotheaded giant', '2008-08-01', 'Although Hotheaded Giant says it looks for \"another red spell,\" there\'s no requirement that Hotheaded Giant actually be cast as a spell (or be red) for this part of its ability to work. For example, if a spell such as Zombify puts Hotheaded Giant directly onto the battlefield, its ability will still check whether you\'ve cast a red spell this turn even though you didn\'t cast Hotheaded Giant itself as a spell.').

card_ruling('hour of need', '2014-04-26', 'In a Commander game, if a commander is put into the command zone instead of being exiled by Hour of Need, its controller will still get a Sphinx token.').
card_ruling('hour of need', '2014-04-26', 'You choose how many targets each spell with a strive ability has and what those targets are as you cast it. It’s legal to cast such a spell with no targets, although this is rarely a good idea. You can’t choose the same target more than once for a single strive spell.').
card_ruling('hour of need', '2014-04-26', 'The mana cost and converted mana cost of strive spells don’t change no matter how many targets they have. Strive abilities affect only what you pay.').
card_ruling('hour of need', '2014-04-26', 'If all of the spell’s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen. If one or more of its targets are legal when it tries to resolve, the spell will resolve and affect only those legal targets. It will have no effect on any illegal targets.').
card_ruling('hour of need', '2014-04-26', 'If such a spell is copied, and the effect that copies the spell allows a player to choose new targets for the copy, the number of targets can’t be changed. The player may change any number of the targets, including all of them or none of them. If, for one of the targets, the player can’t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('hour of need', '2014-04-26', 'If a spell or ability allows you to cast a strive spell without paying its mana cost, you must pay the additional costs for any targets beyond the first.').

card_ruling('hour of reckoning', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('hour of reckoning', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('hour of reckoning', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('hour of reckoning', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('hour of reckoning', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('hour of reckoning', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('hour of reckoning', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('howl of the horde', '2014-09-20', 'Howl of the Horde creates a delayed triggered ability that triggers when you cast your next instant or sorcery spell that turn. The raid ability creates a second, identical delayed triggered ability. Each of these will create a single copy of the next instant or sorcery spell you cast during that turn. If you don’t cast another instant or sorcery spell during that turn, both delayed triggered abilities will cease to exist.').
card_ruling('howl of the horde', '2014-09-20', 'The delayed triggered abilities will copy any instant or sorcery spell, not just one with targets.').
card_ruling('howl of the horde', '2014-09-20', 'When either ability resolves, it creates a copy of the instant or sorcery spell. You control each copy. Each copy is created on the stack, so it’s not “cast.” Abilities that trigger when a player casts a spell won’t trigger. The copy or copies will then resolve like normal spells, after players get a chance to cast spells and activate abilities.').
card_ruling('howl of the horde', '2014-09-20', 'Each copy will have the same targets as the spell it’s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can’t choose a new legal target, then it remains unchanged (even if the current target is illegal). You can choose different targets for each copy.').
card_ruling('howl of the horde', '2014-09-20', 'If the spell being copied is modal (that is, it says “Choose one —” or the like), each copy will have the same mode. You can’t choose a different one.').
card_ruling('howl of the horde', '2014-09-20', 'If the spell being copied has an X whose value was determined as it was cast (like Crater’s Claws has), each copy will have the same value of X.').
card_ruling('howl of the horde', '2014-09-20', 'Raid abilities care only that you attacked with a creature. It doesn’t matter how many creatures you attacked with, or which opponent or planeswalker controlled by an opponent those creatures attacked.').
card_ruling('howl of the horde', '2014-09-20', 'Raid abilities evaluate the entire turn to see if you attacked with a creature. That creature doesn’t have to still be on the battlefield. Similarly, the player or planeswalker it attacked doesn’t have to still be in the game or on the battlefield, respectively.').

card_ruling('howl of the night pack', '2013-07-01', 'The number of Wolves you put onto the battlefield is based on the number of Forests you control when Howl of the Night Pack resolves.').
card_ruling('howl of the night pack', '2013-07-01', 'Howl of the Night Pack counts any land you control with the subtype Forest, not just ones named Forest.').

card_ruling('howlgeist', '2012-05-01', 'The comparison of power is performed only when blockers are declared. Increasing the power of a blocking creature (or decreasing the power of Howlgeist) after this point won\'t cause any creature to stop blocking or become unblocked.').

card_ruling('howling mine', '2004-10-04', 'If Howling Mine leaves the battlefield before it resolves, then the last known tap or untap state of the card is used for resolution.').
card_ruling('howling mine', '2004-10-04', 'It does not trigger at all if this is tapped at the start of the draw step, and it checks this again on resolution.').
card_ruling('howling mine', '2004-10-04', 'The additional draw is separate from any other draw during your draw step. It happens when the triggered ability resolves.').
card_ruling('howling mine', '2013-04-15', 'The triggered ability is put onto the stack after you have already drawn your card for the turn.').

card_ruling('howlpack alpha', '2011-09-22', 'A creature that is both a Werewolf and a Wolf will only get +1/+1 from Howlpack Alpha.').

card_ruling('hua tuo, honored physician', '2013-09-20', 'If a turn has multiple combat phases, the ability can only be activated before the beginning of the declare attackers step of the first combat phase in that turn.').

card_ruling('humble', '2007-02-01', 'Removes all creature abilities. This includes mana abilities. Animated lands will also lose the ability to tap for mana.').

card_ruling('humble defector', '2014-11-24', 'Humble Defector’s ability can be activated any time during your turn, including in response to a spell or ability.').
card_ruling('humble defector', '2014-11-24', 'If Humble Defector isn’t on the battlefield as its ability resolves, but the target player is still a legal target, the ability will resolve. You’ll draw two cards, even though the player won’t gain control of Humble Defector.').
card_ruling('humble defector', '2014-11-24', 'If Humble Defector is controlled by a player other than its owner, and its controller leaves the game, the effect giving that player control of Humble Defector ends. Humble Defector will return to the control of the player still in the game who most recently controlled it.').

card_ruling('humbler of mortals', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('humbler of mortals', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('humbler of mortals', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('humility', '2006-02-01', 'With a Humility and two Opalescences on the battlefield, if Humility has the latest timestamp, then all creatures are 1/1 with no abilities. If the timestamp order is Opalescence, Humility, Opalescence, the second Opalescence is 1/1, and the Humility and first Opalescence are 4/4. If Humility has the earliest timestamp, then everything is 4/4.').
card_ruling('humility', '2007-02-01', 'Removes all creature abilities. This includes mana abilities. Animated lands will also lose the ability to tap for mana.').
card_ruling('humility', '2009-10-01', 'This is the current interaction between Humility and Opalescence: The type-changing effect applies at layer 4, but the rest happens in the applicable layers. The rest of it will apply even if the permanent loses its ability before it\'s finished applying. So if Opalescence, Humility, and Worship are on the battlefield and Opalescence entered the battlefield before Humility, the following is true: Layer 4: Humility and Worship each become creatures that are still enchantments. (Opalescence). Layer 6: Humility and Worship each lose their abilities. (Humility) Layer 7b: Humility becomes 4/4 and Worship becomes 4/4. (Opalescence). Humility becomes 1/1 and Worship becomes 1/1 (Humility). But if Humility entered the battlefield before Opalescence, the following is true: Layer 4: Humility and Worship each become creatures that are still enchantments (Opalescence). Layer 6: Humility and Worship each lose their abilities (Humility). Layer 7b: Humility becomes 1/1 and Worship becomes 1/1 (Humility). Humility becomes 4/4 and Worship becomes 4/4 (Opalescence).').
card_ruling('humility', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('hundred-handed one', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('hundred-handed one', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('hundred-handed one', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('hundred-talon strike', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('hundred-talon strike', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('hundred-talon strike', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('hundred-talon strike', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('hundroog', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('hungering yeti', '2014-11-24', 'Hungering Yeti checks if you control a green or blue permanent only as you begin to cast it. Once you begin to cast Hungering Yeti as though it had flash, it doesn’t matter what happens to the green or blue permanents you control.').

card_ruling('hunt down', '2007-10-01', 'If the first creature targeted by Hunt Down can\'t block the second targeted creature (for example, because the second creature has flying and the first doesn\'t, or because both creatures are controlled by the same player), the ability does nothing and the first creature is free to block whichever creature its controller chooses, or block no creatures at all.').

card_ruling('hunt the hunter', '2013-09-15', 'You must target both a green creature you control and a green creature an opponent controls to cast Hunt the Hunter.').
card_ruling('hunt the hunter', '2013-09-15', 'If the green creature an opponent controls is an illegal target when Hunt the Hunter tries to resolve, but the green creature you control is still a legal target, the creature you control will get +2/+2, but the creatures won’t fight. Neither creature will deal or be dealt damage.').

card_ruling('hunt the weak', '2014-07-18', 'If either target is an illegal target when Hunt the Weak tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('hunt the weak', '2014-07-18', 'If the creature you control is an illegal target when Hunt the Weak tries to resolve, you won’t put a +1/+1 counter on it. If that creature is a legal target but the creature you don’t control isn’t, you’ll still put the counter on the creature you control.').
card_ruling('hunt the weak', '2014-11-24', 'You can’t cast Hunt the Weak unless you choose both a creature you control and a creature you don’t control as targets.').
card_ruling('hunt the weak', '2014-11-24', 'If either target is an illegal target as Hunt the Weak tries to resolve, neither creature will deal or be dealt damage.').
card_ruling('hunt the weak', '2014-11-24', 'If the creature you control is an illegal target as Hunt the Weak tries to resolve, you won’t put a +1/+1 counter on it. If that creature is a legal target but the creature you don’t control isn’t, you’ll still put the counter on the creature you control.').

card_ruling('hunted dragon', '2005-10-01', 'The tokens are the color white and have name and creature type Knight. They are *not* functionally identical to the card White Knight.').

card_ruling('hunter of eyeblights', '2007-10-01', 'The activated ability can target a creature with any kind of counter on it, not just a +1/+1 counter.').

card_ruling('hunter sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('hunter sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('hunter\'s ambush', '2014-07-18', 'The prevention effect will apply to any combat damage that would be dealt this turn by a nongreen creature, even if that creature was green or wasn’t on the battlefield when Hunter’s Ambush resolved.').

card_ruling('hunting cheetah', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').
card_ruling('hunting cheetah', '2009-10-01', 'Hunting Cheetah\'s ability allows you to search your library for any card with the land type Forest, not just a card with the name Forest.').

card_ruling('hunting drake', '2004-10-04', 'If the target changes color from red to green or green to red between triggering and resolution, the ability still works.').

card_ruling('hunting kavu', '2004-10-04', 'Being \"without flying\" is a targeting restriction. The ability will be countered if the target gains flying before the ability resolves.').

card_ruling('hunting moa', '2004-10-04', 'You get a +1/+1 counter both when it enters the battlefield and when it goes to a graveyard from the battlefield.').

card_ruling('hunting pack', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('hunting pack', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('hunting pack', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('hunting pack', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('hunting wilds', '2007-02-01', 'If the Hunting Wilds is kicked, the Forests become creatures with haste permanently.').
card_ruling('hunting wilds', '2009-10-01', 'A noncreature permanent that turns into a creature is subject to the \"summoning sickness\" rule: It can only attack, and its {T} abilities can only be activated, if its controller has continuously controlled that permanent since the beginning of his or her most recent turn.').

card_ruling('hurkyl\'s recall', '2004-10-04', 'Retrieves all artifacts owned by the target player regardless of who controls them. Ignores artifacts owned by other players even if target player has control of them.').
card_ruling('hurkyl\'s recall', '2004-10-04', 'It only returns artifacts which are on the battlefield.').

card_ruling('hurricane', '2004-10-04', 'Whether or not a creature has flying is only checked on resolution.').

card_ruling('hush', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('hushwing gryff', '2014-07-18', 'Triggered abilities use the word “when,” “whenever,” or “at.” They’re often written as “[Trigger condition], [effect].”').
card_ruling('hushwing gryff', '2014-07-18', 'Abilities that create replacement effects, such as a permanent entering the battlefield tapped or with counters on it, are unaffected.').
card_ruling('hushwing gryff', '2014-07-18', 'Abilities that apply “as [this creature] enters the battlefield,” such as choosing a creature to copy with Mercurial Pretender, are also unaffected.').
card_ruling('hushwing gryff', '2014-07-18', 'Hushwing Gryff’s ability stops a creature’s own enters-the-battlefield triggered abilities as well as other triggered abilities that would trigger when a creature enters the battlefield.').
card_ruling('hushwing gryff', '2014-07-18', 'The trigger event doesn’t have to specify “creatures” entering the battlefield. For example, Amulet of Vigor says “Whenever a permanent enters the battlefield tapped and under your control, untap it.” If a creature enters the battlefield tapped and under your control, Amulet of Vigor’s ability would not trigger. If a land (that isn’t also a creature) enters the battlefield tapped and under your control, Amulet of Vigor’s ability would trigger.').
card_ruling('hushwing gryff', '2014-07-18', 'Look at the permanent as it exists on the battlefield, taking into account continuous effects, to determine whether any triggered abilities will trigger. For example, if you control March of the Machines, which says, in part, “Each noncreature artifact is an artifact creature,” each artifact will be a creature at the time it enters the battlefield and will not cause triggered abilities to trigger.').
card_ruling('hushwing gryff', '2014-07-18', 'If Hushwing Gryff and another creature enter the battlefield at the same time, neither creature entering the battlefield will cause triggered abilities to trigger.').

card_ruling('hydra broodmaster', '2014-04-26', 'The value of each X in Hydra Broodmaster’s last ability is equal to the value chosen for X when its monstrosity ability was activated.').
card_ruling('hydra broodmaster', '2014-04-26', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('hydra broodmaster', '2014-04-26', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('hydra broodmaster', '2014-04-26', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('hydra omnivore', '2011-09-22', 'The damage dealt by Hydra Omnivore as a result of its triggered ability is not combat damage (and doesn\'t cause the ability to trigger again).').

card_ruling('hydroblast', '2004-10-04', 'The decision to counter a spell or destroy a permanent is a decision made on announcement before the target is selected. If the spell is redirected, this mode can\'t be changed, so only targets of the selected type are valid.').
card_ruling('hydroblast', '2007-09-16', 'Note that Hydroblast can target any spell or permanent, not just a red one. It checks the color of the target only on resolution.').
card_ruling('hydroblast', '2007-09-16', 'If there are no legal targets for either of the modes, you can\'t choose that mode.').

card_ruling('hydroform', '2013-01-24', 'If the target land hasn\'t been under its controller\'s control continuously since the beginning of his or her most recent turn, that land won\'t be able to attack and its {T} abilities won\'t be able to be activated. In most cases, that means it will no longer be able to be tapped for mana that turn.').
card_ruling('hydroform', '2013-01-24', 'Hydroform doesn\'t affect the land\'s name or any other types, subtypes, or supertypes (such as basic or legendary) the land may have. The land will also keep any abilities it had.').
card_ruling('hydroform', '2013-01-24', 'Effects that modify power and/or toughness but don\'t set them to a specific value (like the one created by Giant Growth), power/toughness changes from counters, and effects that switch a creature\'s power and toughness will continue to apply. This may happen if the land was already a creature when Hydroform resolved.').

card_ruling('hydrolash', '2015-06-22', 'You may cast Hydrolash even if there aren’t any attacking creatures simply to draw a card.').

card_ruling('hyena umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('hyena umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('hyena umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('hyena umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('hyena umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('hyena umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('hyena umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('hyena umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('hyena umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('hyena umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('hyena umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('hymn of rebirth', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('hypergenesis', '2006-09-25', 'In a game of N players, the process ends when all N players in sequence (starting with you) choose not to put a card onto the battlefield. It doesn\'t end the first time a player chooses not to put a card onto the battlefield. If a player chooses not to put a card onto the battlefield but the process continues, that player may put a card onto the battlefield the next time the process gets around to him or her.').
card_ruling('hypergenesis', '2006-10-15', 'This has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Fist of Suns or Mind\'s Desire.').
card_ruling('hypergenesis', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('hypergenesis', '2006-10-15', 'Anything that triggers during the resolution of this will wait to be put on the stack until everything is put onto the battlefield and resolution is complete. The player whose turn it is will put all of his or her triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').
card_ruling('hypergenesis', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('hypergenesis', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('hypergenesis', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('hypergenesis', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('hypergenesis', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('hypergenesis', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('hypergenesis', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('hypergenesis', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('hypergenesis', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('hypergenesis', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').
card_ruling('hypergenesis', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('hyperion blacksmith', '2004-10-04', 'You decide whether you are tapping or untapping the artifact on resolution.').

card_ruling('hypersonic dragon', '2012-10-01', 'The last ability applies to sorcery cards in any zone, provided something is allowing you to cast them. For example, you could cast a sorcery with flashback as though it had flash.').
card_ruling('hypersonic dragon', '2012-10-01', 'The last ability has no effect on abilities that you can activate “any time you could cast a sorcery.”').

card_ruling('hypnotic siren', '2013-10-17', 'If you Hypnotic Siren for its bestow cost, and you gain control of a creature with a constellation ability, that ability will trigger when Hypnotic Siren enters the battlefield. You\'ll controll that triggered ability.').
card_ruling('hypnotic siren', '2014-04-26', 'Gaining control of a creature doesn’t cause you to gain control of any Auras or Equipment attached to it.').
card_ruling('hypnotic siren', '2014-04-26', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('hypnotic siren', '2014-04-26', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('hypnotic siren', '2014-04-26', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('hypnotic siren', '2014-04-26', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('hypnotic siren', '2014-04-26', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('hypnotic siren', '2014-04-26', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('hypnotic siren', '2014-04-26', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').

card_ruling('hypnotic specter', '2008-08-01', 'The ability triggers even if the Specter\'s damage is being redirected to an opponent. It does not trigger if damage that would have been dealt to the opponent is redirected to a nonopponent player or a creature, or if the damage is prevented.').

card_ruling('hypnox', '2013-09-20', 'If a creature (such as Clone) enters the battlefield as a copy of this creature, the copy\'s \"enters-the-battlefield\" ability will still trigger as long as you cast that creature spell from your hand.').

card_ruling('hysterical blindness', '2011-09-22', 'Only creatures controlled by your opponent when Hysterical Blindness resolves will get -4/-0. Creatures that enter the battlefield or that an opponent gains control of later in the turn will be unaffected.').
card_ruling('hysterical blindness', '2011-09-22', 'Hysterical Blindness\'s effect will continue to apply to a creature even if you (or a teammate) gains control of that creature later in the turn.').

card_ruling('hythonia the cruel', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('hythonia the cruel', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('hythonia the cruel', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

