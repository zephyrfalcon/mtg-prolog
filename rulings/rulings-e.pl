% Rulings

card_ruling('earnest fellowship', '2004-10-04', 'A creature with no color does not gain any Protection ability.').

card_ruling('earth servant', '2010-08-15', 'The ability cares about lands with the land type Mountain, not necessarily lands named Mountain.').

card_ruling('earth surge', '2006-02-01', 'Earth Surge gives the bonus to each permanent that has both the \"land\" and \"creature\" types. It doesn\'t turn lands into creatures.').

card_ruling('earthbind', '2004-10-04', 'If the enchanted creature gains flying after Earthbind is put onto it, it will have flying. The two effects are simply applied in timestamp order.').

card_ruling('earthen arms', '2015-08-25', 'Any permanent can have a +1/+1 counter put on it. Those counters won’t have any effect unless that permanent is or becomes a creature.').
card_ruling('earthen arms', '2015-08-25', 'If you cast Earthen Arms for its awaken cost, a land you control can be both targets.').
card_ruling('earthen arms', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('earthen arms', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('earthen arms', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('earthen arms', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('earthen arms', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('earthen arms', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('earthen goo', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('earthquake', '2004-10-04', 'Whether or not a creature is without flying is only checked on resolution.').

card_ruling('earwig squad', '2013-06-07', 'The cards are exiled face up. All players will know what they are.').

card_ruling('eater of the dead', '2004-10-04', 'It untaps during the resolution of its ability.').
card_ruling('eater of the dead', '2007-09-16', 'You can activate the ability whether or not Eater of the Dead is tapped. However, if Eater of the Dead is untapped when the ability resolves, the ability won\'t do anything.').
card_ruling('eater of the dead', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('ebon drake', '2004-12-01', 'This ability triggers off anyone casting a spell, including you, not just Ebon Drake\'s controller or an opponent.').

card_ruling('ebonblade reaper', '2007-02-01', 'If you attempt to halve a negative life total, you halve 0. This means that the life total stays the same. A life total of -10 would remain -10.').

card_ruling('ebony charm', '2004-10-04', 'It targets the cards in the graveyard. These targets are selected when casting the spell.').

card_ruling('echo chamber', '2004-10-04', 'The token enters the battlefield as the creature it is copying. This triggers its own and any other appropriate enters the battlefield abilities.').
card_ruling('echo chamber', '2008-04-01', 'The effect only grants Haste until the end of the current turn. If the creature somehow manages to stick around until a later turn, it will no longer have Haste.').

card_ruling('echo circlet', '2011-01-01', 'Destroying or unequipping the Echo Circlet after blockers have been declared will not undo any blocks made by the equipped creature.').
card_ruling('echo circlet', '2011-01-01', 'Echo Circlet\'s effect is cumulative. If it equips a creature that can already block an additional creature, now it can block three creatures. The same is true if two Echo Circlets equip the same creature, for example.').

card_ruling('echo mage', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('echo mage', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('echo mage', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('echo mage', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('echo mage', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').
card_ruling('echo mage', '2010-06-15', 'Echo Mage\'s last two activated abilities can target (and copy) any instant or sorcery spell, not just one with targets. It doesn\'t matter who controls it.').
card_ruling('echo mage', '2010-06-15', 'When either ability resolves, it creates a copy (or two) of a spell. The copies are created on the stack, so they\'re not \"cast.\" Abilities that trigger when a player casts a spell won\'t trigger. Each copy will then resolve like a normal spell, after players get a chance to cast spells and activate abilities. Each copy resolves before the original spell does.').
card_ruling('echo mage', '2010-06-15', 'Each copy will have the same targets as the spell it\'s copying unless you choose new ones. You may change any number of the targets, including all of them or none of them. If, for one of the targets, you can\'t choose a new legal target, then it remains unchanged (even if the current target is illegal).').
card_ruling('echo mage', '2010-06-15', 'If you activate Echo Mage\'s last ability to copy a spell twice, you may choose new targets for each copy independently. They don\'t have to have the same targets as one another. Be sure to specify which of the copies will resolve first.').
card_ruling('echo mage', '2010-06-15', 'If the spell the ability copies is modal (that is, it says \"Choose one --\" or the like), each copy will have the same mode as the original spell. You can\'t choose a different one.').
card_ruling('echo mage', '2010-06-15', 'If the spell the ability copies has an X whose value was determined as it was cast (like Earthquake does), each copy has the same value of X as the original spell.').
card_ruling('echo mage', '2010-06-15', 'You can\'t choose to pay any additional costs for a copy. However, effects based on any additional costs that were paid for the original spell are copied as though those same costs were paid for each copy too.').
card_ruling('echo mage', '2010-06-15', 'If a copy says that it affects \"you,\" it affects the controller of that copy, not the controller of the original spell. Similarly, if a copy says that it affects an \"opponent,\" it affects an opponent of that copy\'s controller, not an opponent of the original spell\'s controller.').

card_ruling('echo tracer', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').

card_ruling('echoes of the kin tree', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('echoes of the kin tree', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('edge of autumn', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('edge of malacol', '2012-06-01', 'While Edge of Malacol is face up, the normal turn-based action of untapping your creatures is replaced. Those creatures won\'t untap. Other tapped permanents you control will untap as normal.').
card_ruling('edge of malacol', '2012-06-01', 'Other spells and abilities can untap creatures as normal. In that case, they\'ll just untap. You won\'t put any +1/+1 counters on them.').
card_ruling('edge of malacol', '2012-06-01', 'If you untap a creature you control during another player\'s untap step (perhaps due to Seedborn Muse\'s ability), that creature will untap normally. You won\'t put any +1/+1 counters on it.').

card_ruling('edge of the divinity', '2008-08-01', 'If the enchanted creature is both of the listed colors, it will get both bonuses.').

card_ruling('edgewalker', '2004-10-04', 'If you have more than one of these on the battlefield, the cost reduction is cumulative.').
card_ruling('edgewalker', '2008-08-01', 'You apply cost reduction effects after other cost modifiers, so it can reduce additional costs or alternative costs if they include {W} and/or {B}.').
card_ruling('edgewalker', '2008-08-01', 'If a spell has hybrid mana symbols in its mana cost, you choose which half you will be paying before determining the total cost. If you choose to pay such a cost with {W} or {B}, Edgewalker can reduce that part of the cost.').

card_ruling('edric, spymaster of trest', '2011-09-22', 'A creature controlled by an opponent that deals combat damage to another opponent will cause Edric\'s ability to trigger. The creature\'s controller chooses whether to draw a card.').
card_ruling('edric, spymaster of trest', '2011-09-22', 'When a creature you control deals combat damage to one of your opponents, you may draw a card.').

card_ruling('eel umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('eel umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('eel umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('eel umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('eel umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('eel umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('eel umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('eel umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('eel umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('eel umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('eel umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('efreet weaponmaster', '2014-09-20', 'Morph lets you cast a card face down by paying {3}, and lets you turn the face-down permanent face up any time you have priority by paying its morph cost.').
card_ruling('efreet weaponmaster', '2014-09-20', 'The face-down spell has no mana cost and has a converted mana cost of 0. When you cast a face-down spell, put it on the stack face down so no other player knows what it is, and pay {3}. This is an alternative cost.').
card_ruling('efreet weaponmaster', '2014-09-20', 'When the spell resolves, it enters the battlefield as a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the creature can still grant it any of these characteristics.').
card_ruling('efreet weaponmaster', '2014-09-20', 'Any time you have priority, you may turn the face-down creature face up by revealing what its morph cost is and paying that cost. This is a special action. It doesn’t use the stack and can’t be responded to. Only a face-down permanent can be turned face up this way; a face-down spell cannot.').
card_ruling('efreet weaponmaster', '2014-09-20', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('efreet weaponmaster', '2014-09-20', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('efreet weaponmaster', '2014-09-20', 'At any time, you can look at a face-down spell or permanent you control. You can’t look at face-down spells or permanents you don’t control unless an effect instructs you to do so.').
card_ruling('efreet weaponmaster', '2014-09-20', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for doing this include using markers or dice, or simply placing them in order on the battlefield.').
card_ruling('efreet weaponmaster', '2014-09-20', 'If a face-down permanent leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').

card_ruling('ego erasure', '2007-10-01', 'If a creature loses all creature types but then gains a new creature type later in the turn, it will be that new creature type.').
card_ruling('ego erasure', '2013-07-01', 'Examples of creature types include Sliver, Goblin, and Soldier. Creature types appear after the dash on the type line of creatures.').

card_ruling('eidolon of blossoms', '2014-04-26', 'A constellation ability triggers whenever an enchantment enters the battlefield under your control for any reason. Enchantments with other card types, such as enchantment creatures, will also cause constellation abilities to trigger.').
card_ruling('eidolon of blossoms', '2014-04-26', 'An Aura spell without bestow that has an illegal target when it tries to resolve will be countered and put into its owner’s graveyard. It won’t enter the battlefield and constellation abilities won’t trigger. An Aura spell with bestow won’t be countered this way. It will revert to being an enchantment creature and resolve, entering the battlefield and triggering constellation abilities.').
card_ruling('eidolon of blossoms', '2014-04-26', 'When an enchantment enters the battlefield under your control, each constellation ability of permanents you control will trigger. You can put these abilities on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('eidolon of countless battles', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('eidolon of countless battles', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('eidolon of countless battles', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('eidolon of countless battles', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('eidolon of countless battles', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('eidolon of countless battles', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('eidolon of countless battles', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('eidolon of countless battles', '2014-02-01', 'Eidolon of Countless Battles’s last ability functions only on the battlefield. In other zones, it is a 0/0 enchantment creature card.').
card_ruling('eidolon of countless battles', '2014-02-01', 'A permanent with bestow is either a creature or an Aura, not both (although it’s an enchantment either way). It will contribute just +1/+1 toward the bonus given by Eidolon of Countless Battles.').

card_ruling('eidolon of rhetoric', '2014-04-26', 'Eidolon of Rhetoric will look at the entire turn to see if a player has cast a spell yet that turn, even if Eidolon of Rhetoric wasn’t on the battlefield when that spell was cast. Specifically, you can’t cast Eidolon of Rhetoric and then cast another spell that turn.').

card_ruling('eidolon of the great revel', '2014-04-26', 'Casting Eidolon of the Great Revel doesn’t cause its own ability to trigger. It must be on the battlefield when the spell is cast in order for its ability to trigger.').
card_ruling('eidolon of the great revel', '2014-04-26', 'Casting a spell for an alternative cost, such as a bestow cost, doesn’t change its converted mana cost. Similarly, casting a spell with an additional cost, such as a spell with a strive ability, doesn’t change its converted mana cost. For example, casting Gnarled Scarhide (a card with mana cost {B} and bestow cost {3}{B}) for its bestow cost will cause Eidolon of the Great Revel’s ability to trigger.').

card_ruling('eightfold maze', '2009-10-01', 'If all the attacking creatures attack your planeswalkers, you can\'t cast Eightfold Maze. To cast it, a creature needs to have attacked _you_.').
card_ruling('eightfold maze', '2009-10-01', 'If you\'re able to cast Eightfold Maze, it can target any attacking creature (including one attacking a planeswalker).').

card_ruling('eladamri\'s call', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('eladamri\'s vineyard', '2004-10-04', 'It will not trigger a second time in a turn, even if you cast a spell that gives you a third main phase.').
card_ruling('eladamri\'s vineyard', '2004-10-04', 'Only the first main phase each turn is considered a precombat main phase, even if other combat phases are created (with Relentless Assault, for example).').
card_ruling('eladamri\'s vineyard', '2007-05-01', 'This ability is not optional.').

card_ruling('eladamri, lord of leaves', '2008-04-01', 'This card only gives forestwalk to other Elf creatures. It gives shroud to all other Elf permanents, including non-creatures like Prowess of the Fair.').

card_ruling('eland umbra', '2010-06-15', 'Totem armor\'s effect is mandatory. If the enchanted permanent would be destroyed, you must remove all damage from it and destroy the Aura that has totem armor instead.').
card_ruling('eland umbra', '2010-06-15', 'Totem armor\'s effect is applied no matter why the enchanted permanent would be destroyed: because it\'s been dealt lethal damage, or because it\'s being affected by an effect that says to \"destroy\" it (such as Doom Blade). In either case, all damage is removed from the permanent and the Aura is destroyed instead.').
card_ruling('eland umbra', '2010-06-15', 'If a permanent you control is enchanted with multiple Auras that have totem armor, and the enchanted permanent would be destroyed, one of those Auras is destroyed instead -- but only one of them. You choose which one because you control the enchanted permanent.').
card_ruling('eland umbra', '2010-06-15', 'If a creature enchanted with an Aura that has totem armor would be destroyed by multiple state-based actions at the same time the totem armor\'s effect will replace all of them and save the creature.').
card_ruling('eland umbra', '2010-06-15', 'If a spell or ability (such as Planar Cleansing) would destroy both an Aura with totem armor and the permanent it\'s enchanting at the same time, totem armor\'s effect will save the enchanted permanent from being destroyed. Instead, the spell or ability will destroy the Aura in two different ways at the same time, but the result is the same as destroying it once.').
card_ruling('eland umbra', '2010-06-15', 'Totem armor\'s effect is not regeneration. Specifically, if totem armor\'s effect is applied, the enchanted permanent does not become tapped and is not removed from combat as a result. Effects that say the enchanted permanent can\'t be regenerated (as Vendetta does) won\'t prevent totem armor\'s effect from being applied.').
card_ruling('eland umbra', '2010-06-15', 'Say you control a permanent enchanted with an Aura that has totem armor, and the enchanted permanent has gained a regeneration shield. The next time it would be destroyed, you choose whether to apply the regeneration effect or the totem armor effect. The other effect is unused and remains, in case the permanent would be destroyed again.').
card_ruling('eland umbra', '2010-06-15', 'If a spell or ability says that it would \"destroy\" a permanent enchanted with an Aura that has totem armor, that spell or ability causes the Aura to be destroyed instead. (This matters for cards such as Karmic Justice.) Totem armor doesn\'t destroy the Aura; rather, it changes the effects of the spell or ability. On the other hand, if a spell or ability deals lethal damage to a creature enchanted with an Aura that has totem armor, the game rules regarding lethal damage cause the Aura to be destroyed, not that spell or ability.').
card_ruling('eland umbra', '2013-07-01', 'Totem armor has no effect if the enchanted permanent is put into a graveyard for any other reason, such as if it\'s sacrificed, if it\'s legendary and another legendary permanent with the same name is controlled by the same player, or if its toughness is 0 or less.').
card_ruling('eland umbra', '2013-07-01', 'If a creature enchanted with an Aura that has totem armor has indestructible, lethal damage and effects that try to destroy it simply have no effect. Totem armor won\'t do anything because it won\'t have to.').
card_ruling('eland umbra', '2013-07-01', 'Say you control a permanent enchanted with an Aura that has totem armor, and that Aura has gained a regeneration shield. The next time the enchanted permanent would be destroyed, the Aura would be destroyed instead -- but it regenerates, so nothing is destroyed at all. Alternately, if that Aura somehow gains indestructible, the enchanted permanent is effectively indestructible as well.').

card_ruling('elbrus, the binding blade', '2011-01-22', 'The \"legend rule\" checks only for permanents with exactly the same name. Elbrus, the Binding Blade and Withengar Unbound can be on the battlefield at the same time without either going to its owner\'s graveyard.').
card_ruling('elbrus, the binding blade', '2011-01-22', 'Elbrus will transform even if you are unable to unattach it (most likely because the equipped creature died).').
card_ruling('elbrus, the binding blade', '2011-01-22', 'If the equipped creature deals combat damage to more than one player at the same time, perhaps because some of its combat damage was redirected, Elbrus\'s ability will trigger once for each player it deals combat damage to. This may cause it to become unattached, transform, then transform again. If it transforms back into Elbrus, it will remain unattached.').
card_ruling('elbrus, the binding blade', '2011-01-22', 'Withengar\'s triggered ability will trigger no matter how a player loses the game: due to a state-based action (as a result of having a life total of 0 or less, trying to draw a card from an empty library, or having ten poison counters), a spell or ability that says that player loses the game, a concession, or a game loss awarded by a judge.').
card_ruling('elbrus, the binding blade', '2011-01-22', 'In a multiplayer game using the limited range of influence option (such as a Grand Melee game), if a spell or ability says that you win the game, it instead causes all of your opponents within your range of influence to lose the game. This is another way by which Withengar\'s ability can trigger.').

card_ruling('elder druid', '2004-10-04', 'The decision of whether to tap or untap is made on resolution.').

card_ruling('elder land wurm', '2004-10-04', 'Another effect can give Elder Land Wurm defender after it has lost it. Blocking again will cause it to lose that instance of defender too.').

card_ruling('elder mastery', '2009-02-01', 'The last ability triggers from any damage dealt by the enchanted creature, not just combat damage.').

card_ruling('elder of laurels', '2011-09-22', 'The number of creatures you control is counted as the ability resolves.').
card_ruling('elder of laurels', '2011-09-22', 'Once the ability has resolved, the bonus won\'t change if the number of creatures you control changes later in the turn.').

card_ruling('elderscale wurm', '2012-07-01', 'Elderscale Wurm\'s last ability doesn\'t prevent damage. It only changes the result of damage dealt to you. For example, a creature will lifelink that deals damage to you will still cause its controller to gain life, even if that damage would reduce your life total to less than 7.').
card_ruling('elderscale wurm', '2012-07-01', 'Elderscale Wurm\'s last ability doesn\'t affect the loss or payment of life.').
card_ruling('elderscale wurm', '2012-07-01', 'If you have less than 7 life, damage dealt to you reduces your life total as normal.').
card_ruling('elderscale wurm', '2012-07-01', 'If damage is dealt to you and Elderscale Wurm at the same time, and that damage is enough to destroy Elderscale Wurm and reduce your life total to 7 or less, Elderscale Wurm\'s ability will still apply. For example, if you have 10 life and a spell deals 10 damage to you and 10 damage to Elderscale Wurm, you\'ll end up at 7 life and Elderscale Wurm will be destroyed.').
card_ruling('elderscale wurm', '2012-07-01', 'In a Commander game, combat damage you\'re dealt by a commander is still tracked, even if it doesn\'t change your life total.').

card_ruling('elderwood scion', '2012-06-01', 'None of Elderwood Scion\'s abilities affect abilities that target it.').
card_ruling('elderwood scion', '2012-06-01', 'Aura spells require a target when they are cast. Elderwood Scion\'s cost-reduction ability will affect Aura spells cast targeting it.').
card_ruling('elderwood scion', '2012-06-01', 'Spells you cast that target Elderwood Scion will cost {2} less to cast even if those spells have additional targets. The same is true for spells your opponents cast costing {2} more.').
card_ruling('elderwood scion', '2012-06-01', 'Elderwood Scion\'s cost-reduction ability can\'t reduce the colored mana requirement of a spell.').
card_ruling('elderwood scion', '2012-06-01', 'Elderwood Scion\'s cost-reduction ability can apply to any cost of a spell, including additional or alternative costs.').

card_ruling('eldrazi conscription', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('eldrazi conscription', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('eldrazi conscription', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('eldrazi conscription', '2010-06-15', 'Eldrazi Conscription is an Eldrazi. However, it doesn\'t turn the enchanted creature into an Eldrazi.').

card_ruling('eldrazi monument', '2009-10-01', 'If you control any creatures as the triggered ability resolves, you must sacrifice one. You can\'t choose to sacrifice Eldrazi Monument instead. You sacrifice the Monument only if you had no creatures to sacrifice.').
card_ruling('eldrazi monument', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a creature with indestructible to be put into the graveyard. However, a creature with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s legendary and another legendary creature with the same name is controlled by the same player, or if its toughness is 0 or less.').

card_ruling('eldrazi skyspawner', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('eldrazi skyspawner', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('electrickery', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('electrickery', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('electrickery', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('electrickery', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('electrickery', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('electrickery', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('electrickery', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('electrolyze', '2006-02-01', 'If all chosen targets are illegal as Electrolyze tries to resolve, it will be countered and none of its effects will happen. No damage will be dealt and you won\'t draw a card.').

card_ruling('electropotence', '2009-10-01', 'You target a creature or player when the ability triggers. You decide whether or not to pay {2}{R} as the ability resolves.').
card_ruling('electropotence', '2009-10-01', 'Once you decide to pay {2}{R}, it\'s too late for anyone to respond (such as by activating a regeneration ability the targeted creature has).').
card_ruling('electropotence', '2009-10-01', 'If you pay {2}{R}, the creature that entered the battlefield deals damage equal to its current power to the targeted creature or player. If it\'s no longer on the battlefield, its last known existence on the battlefield is checked to determine its power.').
card_ruling('electropotence', '2009-10-01', 'Electropotence is the source of the ability, but the creature is the source of the damage. The ability couldn\'t target a creature with protection from red, for example. It could target a creature with protection from creatures, but all the damage would be prevented. Since damage is dealt by the creature, lifelink, deathtouch, and wither are taken into account, even if the creature has left the battlefield by the time it deals damage.').

card_ruling('elemental appeal', '2009-10-01', 'If Elemental Appeal was kicked, it creates a 7/1 token with trample and haste that gets +7/+0 until end of turn. It doesn\'t create a 14/1 token. This matters in case something copies that token (the copy will be just 7/1), or in case Elemental Appeal is somehow cast during an end step (in which case the +7/+0 lasts just for that turn).').

card_ruling('elemental bond', '2015-06-22', 'The creature must have power 3 or greater as it enters the battlefield, or Elemental Bond’s ability won’t trigger. Static abilities that raise (or lower) a creature’s power are taken into account. However, you can’t have a creature with power 2 or less enter the battlefield and try to raise its power with a spell, an activated ability, or a triggered ability.').

card_ruling('elemental mastery', '2008-05-01', 'The enchanted creature\'s power is checked at the time the ability resolves. If the enchanted creature has left the battlefield by then, its last known information is used.').
card_ruling('elemental mastery', '2008-05-01', 'The tokens will be exiled in the End step even if Elemental Mastery or the enchanted creature has left the battlefield by then.').
card_ruling('elemental mastery', '2008-05-01', 'If the ability is activated after the current turn\'s End step has begun, the tokens won\'t be exiled until the next turn\'s End step.').

card_ruling('elemental resonance', '2006-05-01', 'If the enchanted permanent\'s mana cost is {2}{W}{U}, this Aura\'s controller adds {2}{W}{U} to his or her mana pool. If its mana cost is {X}{G}, this Aura\'s controller adds {G} to his or her mana pool. If its mana cost is (W/U)(W/U), this Aura\'s controller chooses whether to add {W}{W}, {W}{U}, or {U}{U} to his or her mana pool. The choice may be different each time the ability resolves.').
card_ruling('elemental resonance', '2006-05-01', 'This ability isn\'t a mana ability. It uses the stack.').
card_ruling('elemental resonance', '2006-05-01', 'Only the first main phase in a turn is the \"precombat main phase,\" even if additional main phases are generated by some effect.').
card_ruling('elemental resonance', '2009-10-01', 'You get the mana whether you want it or not. If you don\'t spend it, it will disappear at the end of the current step (or phase).').

card_ruling('elephant grass', '2014-02-01', 'Unless some effect explicitly says otherwise, a creature that can\'t attack you can still attack a planeswalker you control.').

card_ruling('elephant guide', '2004-10-04', 'If cast targeting an opponent\'s creature, you get the Elephant when that creature goes to the graveyard.').

card_ruling('elephant resurgence', '2004-10-04', 'The token creatures\' power and toughness continuously adjust. They are not \"locked in\" when the spell resolves. This is because the tokens get an ability, rather than simply having their power and toughness set.').

card_ruling('elesh norn, grand cenobite', '2013-07-01', 'If more than one Elesh Norn, Grand Cenobite is under your control, all of their static abilities will briefly apply. Any creatures that have 0 or less toughness and any creatures that now have lethal damage marked on them will be put into graveyards at the same time that the \"legend rule\" causes all but one of the Elesh Norns to be put into their owners\' graveyards.').

card_ruling('elfhame sanctuary', '2004-10-04', 'If you have more than one of these on the battlefield, you get to use them all and you don\'t skip multiple draw steps.').
card_ruling('elfhame sanctuary', '2004-10-04', 'The \"if you do\" means \"if you search your library\". You get to skip your draw step and shuffle your deck even if you don\'t find (or choose not to find) a basic land.').
card_ruling('elfhame sanctuary', '2004-10-04', 'Because the \"search\" requires you to find a card with certain characteristics, you don\'t have to find the card if you don\'t want to.').

card_ruling('elite arcanist', '2013-07-01', 'You don’t choose the value of {X} in the activation cost of Elite Arcanist’s ability. It’s defined by the converted mana cost of the exiled instant card.').
card_ruling('elite arcanist', '2013-07-01', 'The copy is created in and cast from exile. You must still follow any timing restrictions, such as “Cast [this spell] only during combat.”').
card_ruling('elite arcanist', '2013-07-01', 'You can’t pay any alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the spell has any mandatory additional costs, you must pay those.').
card_ruling('elite arcanist', '2013-07-01', 'If the copied card has {X} in its mana cost, you must choose 0 as its value when casting the copy. Don’t confuse that {X} with the {X} in the activation cost of Elite Arcanist’s ability.').
card_ruling('elite arcanist', '2013-07-01', 'If the exiled card is a split card, then {X} is the sum of the converted mana costs of each half. However, when you cast the copy, you choose only one half to cast. You may cast a different half each time you create and cast a copy.').
card_ruling('elite arcanist', '2013-07-01', 'If Elite Arcanist leaves the battlefield, you will no longer be able to copy the exiled card. If it returns to the battlefield, you’ll be able to exile another instant card from your hand, but Elite Arcanist will be a different permanent with no connection to the first exiled card.').

card_ruling('elite scaleguard', '2014-11-24', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Abzan Skycaptain’s bolster ability.').
card_ruling('elite scaleguard', '2014-11-24', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves.').

card_ruling('elite skirmisher', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('elite skirmisher', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('elite skirmisher', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('elixir of immortality', '2010-08-15', 'Paying the activation cost of Elixir of Immortality\'s ability doesn\'t cause it to leave the battlefield. If you have a way to untap it, you can activate the ability multiple times in response to itself.').
card_ruling('elixir of immortality', '2011-09-22', 'As the ability resolves, you\'ll shuffle Elixir of Immortality into its owner\'s library directly from the battlefield, if it\'s still there.').
card_ruling('elixir of immortality', '2011-09-22', 'If Elixir of Immortality is in your graveyard at the time the ability resolves, you\'ll still wind up shuffling it into your library because you shuffle your entire graveyard into your library. If it\'s anywhere else by that time, including in another player\'s graveyard, it remains where it is and you shuffle just your graveyard into your library.').
card_ruling('elixir of immortality', '2011-09-22', 'If you gain control of another player\'s Elixir of Immortality and activate it, the Elixir of Immortality will be shuffled into its owner\'s library and the cards in your graveyard will be shuffled into your library. You are considered to have shuffled each affected library (even if, as a shortcut, each player physically shuffles his or her own library).').

card_ruling('elkin bottle', '2004-10-04', 'This is not considered to be drawing a card.').
card_ruling('elkin bottle', '2004-10-04', 'If the Bottle leaves the battlefield or your control, the cards remain waiting until played or until the beginning of your next upkeep. The card is in the \"Exile\" zone.').
card_ruling('elkin bottle', '2004-10-04', 'The card is not part of your hand in any way. You can\'t be forced to discard it due to a discard from hand effect, and you can\'t discard it to pay a cost.').
card_ruling('elkin bottle', '2004-10-04', 'To \"play a card\" is to either cast a spell or to put a land onto the battlefield using the main phase special action.').
card_ruling('elkin bottle', '2008-08-01', 'If you do not play the card before your next upkeep, it remains exiled; you just lose the ability to play it.').
card_ruling('elkin bottle', '2008-10-01', 'The exiled card is played using the normal timing rules for its card type, as well as any other applicable restrictions such as \"Cast [this card] only during combat.\" For example, you can\'t play the card during an opponent\'s turn unless it\'s an instant or has flash. Similarly, if the exiled card is a land, you can\'t play it if you\'ve already played a land that turn. If it\'s a nonland card, you\'ll have to pay its mana cost. The only thing that\'s different is you\'re playing it from the Exile zone.').

card_ruling('elkin lair', '2006-02-01', 'You can play the card this turn, not just during the resolution of the ability.').
card_ruling('elkin lair', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('eloren wilds', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('eloren wilds', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('eloren wilds', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('eloren wilds', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('eloren wilds', '2009-10-01', 'You\'re \"tapping a permanent for mana\" only if you\'re activating an activated ability of that permanent that includes the {T} symbol in its cost and produces mana as part of its effect.').
card_ruling('eloren wilds', '2009-10-01', 'Eloren Wilds\'s first ability affects any permanent players tap for mana, not just lands they tap for mana.').
card_ruling('eloren wilds', '2009-10-01', 'The additional mana is produced by Eloren Wilds, not the permanent that was tapped for mana.').
card_ruling('eloren wilds', '2009-10-01', 'The {C} ability prevents the affected player from casting permanent spells (artifacts, creatures, enchantments, and planeswalkers), not just instant and sorcery spells. It doesn\'t stop the player from playing lands or activating abilities (such as cycling or unearth).').

card_ruling('elsewhere flask', '2008-05-01', 'All your lands will have the chosen land type and no other land type.').
card_ruling('elsewhere flask', '2008-05-01', 'Changing a land\'s type doesn\'t change its name or whether it\'s legendary or basic.').
card_ruling('elsewhere flask', '2008-05-01', 'Lands you control will have the mana ability of the basic land type you choose (for example, Forests can tap to produce green mana) and will lose all other innate abilities they had.').

card_ruling('elspeth tirel', '2011-01-01', 'As Elspeth Tirel\'s last ability resolves, each permanent that isn\'t a token, a land, or Elspeth Tirel herself is destroyed.').
card_ruling('elspeth tirel', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('elspeth tirel', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('elspeth tirel', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('elspeth tirel', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('elspeth tirel', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('elspeth tirel', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('elspeth tirel', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('elspeth tirel', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('elspeth tirel', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('elspeth tirel', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('elspeth tirel', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('elspeth, knight-errant', '2011-01-01', 'Emblems behave similarly to enchantments: They have an ability that, in a general sense, continually affects the game. The primary difference between them is that emblems aren\'t permanents and don\'t exist on the battlefield. Nothing in the game can remove an emblem, simply because no other spell or ability references them. Once you get an emblem, you keep it for the rest of the game. Emblems have no color, name, card type, or other characteristics beyond the listed ability').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Lethal damage and effects that say \"destroy\" won\'t cause a permanent with indestructible to be put into the graveyard. However, a permanent with indestructible can be put into the graveyard for a number of reasons. The most likely reasons are if it\'s sacrificed, if it\'s a creature whose toughness is 0 or less, or if it\'s an Aura that\'s either unattached or attached to something illegal.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('elspeth, knight-errant', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('elspeth, knight-errant', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('elspeth, sun\'s champion', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('elspeth, sun\'s champion', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('elusive krasis', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('elusive krasis', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('elusive krasis', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('elusive krasis', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('elusive krasis', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('elusive krasis', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('elusive spellfist', '2015-02-25', 'Once Elusive Spellfist has been blocked, causing its ability to trigger won’t change or undo that block.').

card_ruling('elven riders', '2007-05-01', 'Creatures with the Reach ability (such as Giant Spider) don\'t actually have Flying, so they can\'t block this.').

card_ruling('elvish aberration', '2009-02-01', 'Unlike the normal cycling ability, Forestcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Forest card. After you find a Forest card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('elvish aberration', '2009-02-01', 'Forestcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Forestcycling this card. Any ability that stops a cycling ability from being activated also stops Forestcycling from being activated.').
card_ruling('elvish aberration', '2009-02-01', 'Forestcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Forestcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('elvish aberration', '2009-02-01', 'You can choose to find any card with the Forest land type, including nonbasic lands. You can also choose not to find a card, even if there is a Forest card in your library.').

card_ruling('elvish archdruid', '2009-10-01', 'Elvish Archdruid\'s first ability affects only other Elves you control. However, Elvish Archdruid\'s second ability counts all Elves you control -- including itself.').
card_ruling('elvish archdruid', '2011-09-22', 'Elvish Archdruid\'s activated ability is a mana ability. It doesn\'t use the stack and players can\'t respond to it.').

card_ruling('elvish branchbender', '2007-10-01', 'The value of X is determined when the ability resolves. It won\'t change later, even if the number of Elves you control changes.').
card_ruling('elvish branchbender', '2007-10-01', 'If you control no Elves when the ability resolves, the Forest will become a 0/0 creature and be put into the graveyard. However, since Elvish Branchbender is itself an Elf, the Forest will usually be at least a 1/1.').
card_ruling('elvish branchbender', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('elvish champion', '2004-10-04', 'It affects Elves controlled by all players, not just yours.').
card_ruling('elvish champion', '2005-08-01', 'This card is now an Elf but has been reworded so that it does not give itself the bonus.').

card_ruling('elvish eulogist', '2007-10-01', 'By the time this ability resolves, Elvish Eulogist will most likely be in your graveyard, so it will be counted by its own ability.').

card_ruling('elvish healer', '2004-10-04', 'It checks the color of the creature on resolution of the ability.').

card_ruling('elvish hunter', '2008-10-01', 'You may target any creature with this ability. It doesn\'t have to be tapped.').
card_ruling('elvish hunter', '2008-10-01', 'If the targeted creature is untapped at the time its controller\'s next untap step begins, this ability has no effect. It won\'t apply at some later time when the targeted creature is tapped.').
card_ruling('elvish hunter', '2008-10-01', 'Elvish Hunter doesn\'t track the creature\'s controller. If the affected creature changes controllers before its old controller\'s next untap step, Elvish Hunter will prevent it from being untapped during its new controller\'s next untap step.').

card_ruling('elvish piper', '2007-05-01', 'Putting the card onto the battlefield is optional. When the ability resolves, you can choose not to.').
card_ruling('elvish piper', '2009-10-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('elvish spirit guide', '2007-02-01', 'This ability is a mana ability.').

card_ruling('elvish vanguard', '2004-10-04', 'If this card enters the battlefield at the same time as another Elf, the ability will trigger due to that Elf.').

card_ruling('emancipation angel', '2012-05-01', 'The triggered ability doesn\'t target a permanent. You choose which one to return to its owner\'s hand as the ability resolves. No one can respond to the choice.').
card_ruling('emancipation angel', '2012-05-01', 'If Emancipation Angel is still on the battlefield as its triggered ability resolves, you may return Emancipation Angel itself.').

card_ruling('ember beast', '2013-01-24', 'Ember Beast can\'t attack or block unless another creature is also assigned to attack or block at the same time. Notably, two Ember Beasts can attack or block together.').
card_ruling('ember beast', '2013-01-24', 'Once Ember Beast has been declared as an attacker or blocker, it doesn\'t matter what happens to the other creature(s).').
card_ruling('ember beast', '2013-01-24', 'Other creatures assigned to attack alongside Ember Beast don\'t have to attack the same player or planeswalker. Other creatures assigned to block alongside Ember Beast don\'t have to block the same creature as Ember Beast.').

card_ruling('ember gale', '2008-05-01', 'Ember Gale will deal damage only to white and/or blue creatures the targeted player controls at the time it resolves. However, it will prevent all creatures that player controls from blocking that turn, even if those creatures enter the battlefield later.').

card_ruling('ember swallower', '2013-09-15', 'When the last ability resolves, the active player will choose three lands he or she controls, then each other player in turn order will do the same. Then all of those lands are sacrificed at the same time.').
card_ruling('ember swallower', '2013-09-15', 'Once a creature becomes monstrous, it can’t become monstrous again. If the creature is already monstrous when the monstrosity ability resolves, nothing happens.').
card_ruling('ember swallower', '2013-09-15', 'Monstrous isn’t an ability that a creature has. It’s just something true about that creature. If the creature stops being a creature or loses its abilities, it will continue to be monstrous.').
card_ruling('ember swallower', '2013-09-15', 'An ability that triggers when a creature becomes monstrous won’t trigger if that creature isn’t on the battlefield when its monstrosity ability resolves.').

card_ruling('ember weaver', '2009-02-01', 'As long as at least one permanent you control is the specified color, the ability will \"work\" and grant this creature the bonus. Otherwise, it won\'t have the bonus.').
card_ruling('ember weaver', '2009-02-01', 'Whether Ember Weaver has first strike matters only at the point that the normal combat damage step would begin, and only if there hasn\'t already been a first-strike combat damage step that turn. If any of your creatures has first strike at that point, a first-strike combat damage step is created. -- If Ember Weaver has first strike at that point, it will assign and deal combat damage during that step (even if it loses first strike before that damage is dealt) and it won\'t deal combat damage during the normal combat damage step. -- If Ember Weaver doesn\'t have first strike at that point, it will assign and deal its combat damage during the normal combat damage step (even if it gains first strike before then).').

card_ruling('embermaw hellion', '2015-06-22', 'Embermaw Hellion’s last ability doesn’t cause Embermaw Hellion to deal damage; it affects the amount of damage dealt by the original red source.').
card_ruling('embermaw hellion', '2015-06-22', 'If the player or permanent being dealt damage is also affected by a damage prevention effect, that player or the controller of that permanent can apply that effect and Embermaw Hellion’s effect in any order. If all of the damage is prevented, Embermaw Hellion’s effect can’t apply to it.').
card_ruling('embermaw hellion', '2015-06-22', 'Multiple Embermaw Hellions are cumulative. If you control two of them, you’ll add 2 to the damage dealt by another red source you control. In this case, you would also add 1 to the damage dealt by either Embermaw Hellion.').

card_ruling('emberstrike duo', '2008-05-01', 'An object that\'s both of the listed colors will cause both abilities to trigger. You can put them on the stack in either order.').

card_ruling('emblem of the warmind', '2007-05-01', 'Although this is an Aura, Emblem of the Warmind has no ability that specifically references the enchanted creature.').

card_ruling('embolden', '2004-10-04', 'You can\'t choose zero targets. You must choose at least one target.').

card_ruling('emerald medallion', '2004-10-04', 'The generic X cost is still considered generic even if there is a requirement that a specific color be used for it. For example, \"only black mana can be spent this way\". This distinction is important for effects which reduce the generic portion of a spell\'s cost.').
card_ruling('emerald medallion', '2004-10-04', 'This can lower the cost to zero, but not below zero.').
card_ruling('emerald medallion', '2004-10-04', 'The effect is cumulative.').
card_ruling('emerald medallion', '2004-10-04', 'The lower cost is not optional like with some other cost reducers.').
card_ruling('emerald medallion', '2004-10-04', 'Can never affect the colored part of the cost.').
card_ruling('emerald medallion', '2004-10-04', 'If this card is sacrificed to pay part of a spell\'s cost, the cost reduction still applies.').

card_ruling('emerge unscathed', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('emerge unscathed', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('emerge unscathed', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('emerge unscathed', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('emerge unscathed', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('emerge unscathed', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('emerge unscathed', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('emerge unscathed', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').
card_ruling('emerge unscathed', '2010-06-15', 'You choose the color as Emerge Unscathed resolves. Once you choose a color, it\'s too late for players to respond.').

card_ruling('emeria angel', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('emeria angel', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').

card_ruling('emeria shepherd', '2015-08-25', 'A “nonland permanent card” is an artifact, creature, enchantment, or planeswalker card.').
card_ruling('emeria shepherd', '2015-08-25', 'If you return an Aura to the battlefield this way, you choose what it will enchant as it enters the battlefield. This doesn’t target any player or permanent, so you could attach an Aura to a permanent with hexproof controlled by an opponent, for example. If there’s nothing the Aura can legally enchant, it stays in the graveyard.').
card_ruling('emeria shepherd', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('emeria shepherd', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('emeria shepherd', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('emeria, the sky ruin', '2009-10-01', 'Emeria, the Sky Ruin\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control seven or more Plains as your upkeep starts, and (2) the ability will do nothing if you control fewer than seven Plains by the time it resolves.').

card_ruling('emmara tandris', '2013-04-15', 'Emmara Tandris\'s ability prevents all damage that would be dealt to creature tokens you control, not just combat damage.').
card_ruling('emmara tandris', '2013-04-15', 'If Emmara Tandris is dealt lethal damage at the same creature tokens you control would be dealt damage, the damage that would be dealt to those tokens is prevented.').

card_ruling('emperor crocodile', '2004-10-04', 'The ability will trigger if you don\'t control another creature, even for a brief moment during the resolution of another spell or ability.').
card_ruling('emperor crocodile', '2004-10-04', 'Only checks if you control no other creatures at the time it triggers. It does not check again on resolution, so gaining control of a creature before then will not save the Crocodile.').

card_ruling('empty the pits', '2014-09-20', 'The rules for delve have changed slightly since it was last in an expansion. Previously, delve reduced the cost to cast a spell. Under the current rules, you exile cards from your graveyard at the same time you pay the spell’s cost. Exiling a card this way is simply another way to pay that cost.').
card_ruling('empty the pits', '2014-09-20', 'Delve doesn’t change a spell’s mana cost or converted mana cost. For example, Dead Drop’s converted mana cost is 10 even if you exiled three cards to cast it.').
card_ruling('empty the pits', '2014-09-20', 'You can’t exile cards to pay for the colored mana requirements of a spell with delve.').
card_ruling('empty the pits', '2014-09-20', 'You can’t exile more cards than the generic mana requirement of a spell with delve. For example, you can’t exile more than nine cards from your graveyard to cast Dead Drop.').
card_ruling('empty the pits', '2014-09-20', 'Because delve isn’t an alternative cost, it can be used in conjunction with alternative costs.').

card_ruling('empty the warrens', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('empty the warrens', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('empty the warrens', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('empty the warrens', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').

card_ruling('empyrial archangel', '2008-10-01', 'Suppose you control Empyrial Archangel and a planeswalker, and a source controlled by an opponent would deal noncombat damage to you. Both Empyrial Archangel and the planeswalker have effects that change (or could change) what that damage would be dealt to. You choose one to apply. -- If you apply Empyrial Archangel\'s effect, the damage is redirected to it. The planeswalker replacement effect is no longer applicable. -- If you apply the planeswalker replacement effect, your opponent then chooses whether he or she wants to redirect the damage to your planeswalker. If your opponent does, that many loyalty counters are removed from the it. If your opponent doesn\'t, then Empyrial Archangel\'s effect applies and the damage is redirected to it.').

card_ruling('empyrial armor', '2004-10-04', 'The value of X is recalculated constantly, so this card\'s bonus varies as the number of cards in your hand varies.').

card_ruling('emrakul, the aeons torn', '2010-06-15', 'Annihilator abilities trigger and resolve during the declare attackers step. The defending player chooses and sacrifices the required number of permanents before he or she declares blockers. Any creatures sacrificed this way won\'t be able to block.').
card_ruling('emrakul, the aeons torn', '2010-06-15', 'If a creature with annihilator is attacking a planeswalker, and the defending player chooses to sacrifice that planeswalker, the attacking creature continues to attack. It may be blocked. If it isn\'t blocked, it simply won\'t deal combat damage to anything.').
card_ruling('emrakul, the aeons torn', '2010-06-15', 'In a Two-Headed Giant game, the controller of an attacking creature with annihilator chooses which of the defending players is affected by the ability. Only that player sacrifices permanents. The choice is made as the ability resolves; once a player is chosen, it\'s too late for anyone to respond.').
card_ruling('emrakul, the aeons torn', '2010-06-15', 'Emrakul can be targeted by spells that try to counter it (such as Lay Bare). Those spells will resolve, but the part of their effect that would counter Emrakul won\'t do anything. Any other effects those spells have will work as normal.').
card_ruling('emrakul, the aeons torn', '2010-06-15', '\"Protection from colored spells\" means that Emrakul can\'t be the target of colored spells (including colored Aura spells) or of abilities whose sources are colored spells (such as the \"when you cast\" ability of an Ulamog, the Infinite Gyre that\'s been turned red by Painter\'s Servant). It also means that all damage that would be dealt to Emrakul by colored spells is prevented. Like every protection ability, it works only while Emrakul is on the battlefield.').
card_ruling('emrakul, the aeons torn', '2010-06-15', '\"Colored spells\" is not synonymous with \"colored instants and sorceries.\" For example, if a player cycles Choking Tethers, its triggered ability may target Emrakul because Choking Tethers wasn\'t cast as a spell.').
card_ruling('emrakul, the aeons torn', '2010-06-15', 'Emrakul may be affected by colored spells that don\'t target it or deal damage to it, including those that cause it to become blocked. Abilities of colored permanents (such as Journey to Nowhere) may target it. Auras may be moved onto it by abilities or by colored spells that don\'t target it (such as Aura Graft).').

card_ruling('encase in ice', '2015-02-25', 'If the enchanted creature stops being red or green, or if it stops being a creature, Encase in Ice will be put into its owner’s graveyard.').
card_ruling('encase in ice', '2015-02-25', 'The creature can still be untapped by other spells and abilities.').

card_ruling('enchanted evening', '2008-05-01', 'This has no effect on those permanents other than letting them interact with things that care about enchantments.').

card_ruling('enchantment alteration', '2005-08-01', 'If you change this card\'s target, you can change which Aura is targeted, but you can\'t choose where that Aura will be moved.').
card_ruling('enchantment alteration', '2005-08-01', 'Only targets the Aura and not either of the things the Aura may be on.').

card_ruling('enchantress\'s presence', '2004-10-04', 'It does not trigger for itself being cast.').

card_ruling('encircling fissure', '2015-08-25', 'Encircling Fissure prevents all combat damage that would be dealt by any creature controlled by the target opponent, even if that creature wasn’t on the battlefield or wasn’t a creature as Encircling Fissure resolved.').
card_ruling('encircling fissure', '2015-08-25', 'You can cast a spell with awaken for its mana cost and get only its first effect. If you cast a spell for its awaken cost, you’ll get both effects.').
card_ruling('encircling fissure', '2015-08-25', 'If the non-awaken part of the spell requires a target, you must choose a legal target. You can’t cast the spell if you can’t choose a legal target for each instance of the word “target” (though you only need a legal target for the awaken ability if you’re casting the spell for its awaken cost).').
card_ruling('encircling fissure', '2015-08-25', 'If a spell with awaken has multiple targets (including the land you control), and some but not all of those targets become illegal by the time the spell tries to resolve, the spell won’t affect the illegal targets in any way.').
card_ruling('encircling fissure', '2015-08-25', 'If the non-awaken part of the spell doesn’t require a target and you cast the spell for its awaken cost, then the spell will be countered if the target land you control becomes illegal before the spell resolves (such as due to being destroyed in response to the spell being cast).').
card_ruling('encircling fissure', '2015-08-25', 'The land will retain any other types, subtypes, or supertypes it previously had. It will also retain any mana abilities it had as a result of those subtypes. For example, a Forest that’s turned into a creature this way can still be tapped for {G}.').
card_ruling('encircling fissure', '2015-08-25', 'Awaken doesn’t give the land you control a color. As most lands are colorless, in most cases the resulting land creature will also be colorless.').

card_ruling('enclave cryptologist', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('enclave cryptologist', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('enclave cryptologist', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('enclave cryptologist', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('enclave cryptologist', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('encroach', '2004-10-04', 'You choose a land card that is not a basic land.').
card_ruling('encroach', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('encrust', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').
card_ruling('encrust', '2014-07-18', 'Static and triggered abilities are unaffected by Encrust.').
card_ruling('encrust', '2014-07-18', 'The permanent can be untapped by other spells and abilities.').

card_ruling('end hostilities', '2014-09-20', 'Permanents attached to creatures may include Auras and Equipment. All the affected permanents are destroyed at the same time.').

card_ruling('endemic plague', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('endemic plague', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('endless horizons', '2008-08-01', 'If Endless Horizons leaves the battlefield, the remaining Plains cards stay exiled. If Endless Horizons returns to the battlefield, it\'s a new object with no relation to its previous existence. The previously exiled Plains cards will still stay exiled.').
card_ruling('endless horizons', '2008-08-01', 'If a different player gains control of Endless Horizons, its ability will trigger at the beginning of that player\'s upkeep, but it won\'t do anything when it resolves.').

card_ruling('endless obedience', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('endless obedience', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('endless obedience', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('endless obedience', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('endless obedience', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('endless obedience', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('endless obedience', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('endless ranks of the dead', '2011-09-22', 'If you control fewer than two Zombies, you won\'t get any tokens.').
card_ruling('endless ranks of the dead', '2011-09-22', 'The number of Zombies you control is counted when the ability resolves. If you control multiple Endless Ranks of the Dead, the tokens you get when the first ability resolves will count for the subsequent abilities (if the tokens are still under your control at that time).').

card_ruling('endless whispers', '2004-12-01', 'The creatures have the triggered ability, so each creature\'s controller chooses one of his or her opponents to target.').

card_ruling('endless wurm', '2004-10-04', 'You choose whether to sacrifice an enchantment or not on resolution. If not, then you sacrifice this card. You can choose to not sacrifice an enchantment if you no longer control this card on resolution.').

card_ruling('endrek sahr, master breeder', '2006-09-25', 'The second ability is a state trigger. Once it triggers, it won\'t trigger again as long as the ability is on the stack. If the ability is countered and the trigger condition is still true, it will immediately trigger again.').
card_ruling('endrek sahr, master breeder', '2006-09-25', 'When the second ability resolves, Endrek Sahr will be sacrificed regardless of how many Thrulls you control at that time, even if it\'s less than seven.').
card_ruling('endrek sahr, master breeder', '2006-09-25', 'Face-down spells are creature spells, but they have converted mana cost 0.').

card_ruling('enduring renewal', '2004-10-04', 'Enduring Renewal only affects creature cards that are \"drawn\". It doesn\'t affect cards that are put into your hand from your library, or from anywhere else.').
card_ruling('enduring renewal', '2004-10-04', 'If the creature is only a creature due to an effect, it still goes to your hand. This includes any way to animate a card.').
card_ruling('enduring renewal', '2004-10-04', 'Token creatures cease to exist if they leave the battlefield, so this effect will not let you get them in your hand.').
card_ruling('enduring renewal', '2006-07-15', 'The last ability is now a triggered ability, not a replacement effect. That means your opponent has the opportunity to remove the card from your graveyard while the triggered ability is still on the stack.').
card_ruling('enduring renewal', '2006-09-25', 'Unless something weird happens, the card you draw as a result of the second ability will be the card you revealed.').
card_ruling('enduring renewal', '2006-09-25', 'If, after the last ability triggers, the creature card is removed from your graveyard in response, it won\'t be returned to your hand.').
card_ruling('enduring renewal', '2008-04-01', 'A \"creature card\" is any card with the type Creature, even if it has other types such as Artifact, Enchantment, or Land. Older cards of type Summon are also Creature cards.').

card_ruling('enduring scalelord', '2015-02-25', 'A creature entering the battlefield with one or more +1/+1 counters on it will cause Enduring Scalelord’s ability to trigger.').
card_ruling('enduring scalelord', '2015-02-25', 'If +1/+1 counters are put on multiple creatures you control (other than Enduring Scalelord) at the same time, Enduring Scalelord’s ability will trigger once for each of those creatures.').
card_ruling('enduring scalelord', '2015-02-25', 'If you control two Enduring Scalelords, putting a +1/+1 counter on one of them will cause the ability of the other one to trigger. When this ability resolves, you’ll put a +1/+1 counter on the other Scalelord. This will cause the ability of the first one to trigger. This loop will repeat until you choose not to put a +1/+1 counter on one of the Enduring Scalelords.').

card_ruling('enduring victory', '2015-02-25', 'Bolster itself doesn’t target any creature, though some spells and abilities that bolster may have other effects that target creatures. For example, you could put counters on a creature with protection from white with Aven Tactician’s bolster ability.').
card_ruling('enduring victory', '2015-02-25', 'You determine which creature to put counters on as the spell or ability that instructs you to bolster resolves. That could be the creature with the bolster ability, if it’s still under your control and has the least toughness.').

card_ruling('energy arc', '2007-09-16', 'Energy Arc can target untapped creatures. The second part of the effect will still affect them.').
card_ruling('energy arc', '2007-09-16', 'The number of target creatures may be zero.').

card_ruling('energy field', '2004-10-04', 'Buyback causes a spell to go to your hand instead of your graveyard, so using Buyback will avoid triggering this card to be sacrificed.').
card_ruling('energy field', '2004-10-04', 'Discarding does trigger this.').

card_ruling('energy vortex', '2004-10-04', 'Does a flat 3 damage, not 3 damage per counter.').

card_ruling('engineered explosives', '2004-12-01', 'If Engineered Explosives had no charge counters on it when you sacrificed it, it won\'t destroy an animated land, because that permanent is a land in addition to being a creature.').

card_ruling('engulfing slagwurm', '2011-01-01', 'Engulfing Slagwurm\'s ability triggers and resolves during the declare blockers step. Creatures destroyed this way will not deal combat damage.').
card_ruling('engulfing slagwurm', '2011-01-01', 'If your Engulfing Slagwurm blocks or becomes blocked by multiple creatures, its ability triggers that many times. Each trigger will be associated with a specific creature. You choose which order to have the abilities resolve.').
card_ruling('engulfing slagwurm', '2011-01-01', 'If Engulfing Slagwurm\'s ability resolves and the other creature is not destroyed (perhaps because it has already left the battlefield or it regenerates), you\'ll still gain life equal to that creature\'s toughness.').
card_ruling('engulfing slagwurm', '2011-01-01', 'As each ability resolves, the amount of life you gain is equal to the appropriate creature\'s current toughness (if it\'s somehow still on the battlefield), or its toughness as it last existed on the battlefield (in all other cases).').
card_ruling('engulfing slagwurm', '2011-01-01', 'Even if the ability of an attacking Engulfing Slagwurm destroys each creature blocking it, Engulfing Slagwurm won\'t deal combat damage to the player or planeswalker it\'s attacking unless it somehow gains trample.').

card_ruling('enigma sphinx', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('enigma sphinx', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('enigma sphinx', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('enigma sphinx', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('enigma sphinx', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('enigma sphinx', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('enigma sphinx', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').
card_ruling('enigma sphinx', '2009-05-01', 'After Enigma Sphinx\'s middle ability triggers, if Enigma Sphinx is removed from your graveyard in response, it won\'t be put into your library.').
card_ruling('enigma sphinx', '2009-05-01', 'If you have zero or one card left in your library when Enigma Sphinx\'s middle ability resolves, Enigma Sphinx is put on the bottom of your library.').
card_ruling('enigma sphinx', '2009-05-01', 'If you control an Enigma Sphinx that\'s owned by another player, it\'s put into that player\'s graveyard from the battlefield, so Enigma Sphinx\'s middle ability won\'t trigger.').

card_ruling('enlarge', '2013-07-01', 'If the creature attacks, the defending player must assign at least one blocker to it during the declare blockers step if that player controls any creatures that could block it.').

card_ruling('enlightened tutor', '2004-10-04', 'The \"shuffle and put the card on top\" is a single action. If an effect is making the top card of the library face up, the second card down on the library is not revealed.').
card_ruling('enlightened tutor', '2004-10-04', 'You do not have to find an artifact or enchantment card if you do not want to.').

card_ruling('enlisted wurm', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('enlisted wurm', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('enlisted wurm', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('enlisted wurm', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('enlisted wurm', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('enlisted wurm', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('enlisted wurm', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('enraged revolutionary', '2014-05-29', 'Dethrone doesn’t trigger if the creature attacks a planeswalker, even if its controller has the most life.').
card_ruling('enraged revolutionary', '2014-05-29', 'Once dethrone triggers, it doesn’t matter what happens to the players’ life totals before the ability resolves. You’ll put a +1/+1 counter on the creature even if the defending player doesn’t have the most life as the ability resolves.').
card_ruling('enraged revolutionary', '2014-05-29', 'The +1/+1 counter is put on the creature before blockers are declared.').
card_ruling('enraged revolutionary', '2014-05-29', 'In a Two-Headed Giant game, dethrone will trigger if the creature attacks the team with the most life or tied for the most life.').

card_ruling('enraging licid', '2013-04-15', 'Paying the cost to end the effect is a special action and can\'t be responded to.').
card_ruling('enraging licid', '2013-04-15', 'If a spell that can target enchantments but not creatures (such as Clear) is cast targeting the Licid after it has become an enchantment, but the Licid\'s controller pays the cost, the Licid will no longer be an enchantment. This will result in the spell being countered on resolution for having no legal targets.').

card_ruling('enslave', '2007-02-01', 'The owner of a card is the player who started the game with that card in his or her deck. If the card didn\'t start the game in a deck, its owner is the player who brought the card into the game, via Living Wish or a similar effect.').
card_ruling('enslave', '2009-10-01', 'A token\'s owner is the player under whose control it entered the battlefield.').

card_ruling('ensnaring bridge', '2004-10-04', 'All creatures on the battlefield (including yours) are subject to the restriction based on the number of cards in your hand.').
card_ruling('ensnaring bridge', '2004-10-04', 'It only checks the cards in your hand when declaring attackers. Once they are declared, changes to your hand size or to the power of the creatures will not remove creatures from the attack.').

card_ruling('ensoul artifact', '2014-07-18', 'The artifact retains any types, subtypes, or supertypes it has. Notably, if an Equipment becomes an artifact creature, it can’t be attached to another creature. If it was attached to a creature, it becomes unattached.').
card_ruling('ensoul artifact', '2014-07-18', 'If the artifact was already a creature, its base power and toughness will each become 5. This overwrites any previous effects that set the creature’s base power and toughness to specific values. Any power- or toughness-setting effects that start to apply after Ensoul Artifact becomes attached to the artifact will overwrite this effect.').
card_ruling('ensoul artifact', '2014-07-18', 'Effects that modify a creature’s power and/or toughness, such as the ones created by Titanic Growth or a +/1+1 counter, will apply to the creature no matter when they started to take effect. The same is true for any counters that change its power and/or toughness and effects that switch power and toughness.').
card_ruling('ensoul artifact', '2014-07-18', 'The resulting artifact creature will be able to attack on your turn if it’s been under your control continuously since the turn began. That is, it doesn’t matter how long it’s been a creature, just how long it’s been on the battlefield.').

card_ruling('ensouled scimitar', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('entangling trap', '2013-09-20', 'If you clash because of a spell or ability an opponent controls, the ability will still trigger. Likewise, you can still win the clash even if you weren\'t the player to initiate it.').

card_ruling('entangling vines', '2009-10-01', 'Entangling Vines can enchant only a creature that\'s tapped. If at any time the enchanted creature is untapped, Entangling Vines is put into its owner\'s graveyard as a state-based action.').
card_ruling('entangling vines', '2009-10-01', 'If Entangling Vines is cast as a spell, it can target only a tapped creature. If the creature has become untapped by the time Entangling Vines would resolve, it\'s countered and put into its owner\'s graveyard from the stack.').

card_ruling('enter the infinite', '2013-01-24', 'You won\'t have to discard any card during the cleanup step of the turn you cast Enter the Infinite. You will have to discard down to your maximum hand size during the cleanup step of your next turn.').
card_ruling('enter the infinite', '2013-01-24', 'If you are playing with the top card of your library revealed (due to Garruk\'s Horde, for example), you\'ll reveal each card before you draw it.').

card_ruling('entering', '2013-04-15', 'If you\'re casting a split card with fuse from any zone other than your hand, you can\'t cast both halves. You\'ll only be able to cast one half or the other.').
card_ruling('entering', '2013-04-15', 'To cast a fused split spell, pay both of its mana costs. While the spell is on the stack, its converted mana cost is the total amount of mana in both costs.').
card_ruling('entering', '2013-04-15', 'You can choose the same object as the target of each half of a fused split spell, if appropriate.').
card_ruling('entering', '2013-04-15', 'When resolving a fused split spell with multiple targets, treat it as you would any spell with multiple targets. If all targets are illegal when the spell tries to resolve, the spell is countered and none of its effects happen. If at least one target is still legal at that time, the spell resolves, but an illegal target can\'t perform any actions or have any actions performed on it.').
card_ruling('entering', '2013-04-15', 'When a fused split spell resolves, follow the instructions of the left half first, then the instructions on the right half.').
card_ruling('entering', '2013-04-15', 'In every zone except the stack, split cards have two sets of characteristics and two converted mana costs. If anything needs information about a split card not on the stack, it will get two values.').
card_ruling('entering', '2013-04-15', 'On the stack, a split spell that hasn\'t been fused has only that half\'s characteristics and converted mana cost. The other half is treated as though it didn\'t exist.').
card_ruling('entering', '2013-04-15', 'Some split cards with fuse have two monocolor halves of different colors. If such a card is cast as a fused split spell, the resulting spell is multicolored. If only one half is cast, the spell is the color of that half. While not on the stack, such a card is multicolored.').
card_ruling('entering', '2013-04-15', 'Some split cards with fuse have two halves that are both multicolored. That card is multicolored no matter which half is cast, or if both halves are cast. It\'s also multicolored while not on the stack.').
card_ruling('entering', '2013-04-15', 'If a player names a card, the player may name either half of a split card, but not both. A split card has the chosen name if one of its two names matches the chosen name.').
card_ruling('entering', '2013-04-15', 'If you cast a split card with fuse from your hand without paying its mana cost, you can choose to use its fuse ability and cast both halves without paying their mana costs.').
card_ruling('entering', '2013-04-15', 'Entering doesn\'t target any creature card. You choose which card you\'re putting onto the battlefield as Entering resolves. You can choose any creature card in a graveyard at time, including one put into a graveyard because of Breaking if you cast Breaking // Entering as a fused split spell.').

card_ruling('enthralling victor', '2015-06-22', 'Once Enthralling Victor’s ability resolves and you gain control of a creature, raising its power above 2 won’t cause you to lose control of it. Similarly, it doesn’t matter if Enthralling Victor leaves the battlefield or you lose control of Enthralling Victor. You’ll keep control of the creature that was the target of the ability until the end of the turn.').
card_ruling('enthralling victor', '2015-06-22', 'Enthralling Victor’s ability can target a creature that’s already untapped. You’ll still gain control of it and it will gain haste.').

card_ruling('entropic specter', '2004-10-04', 'The opponent does not discard a card if all the damage is prevented.').
card_ruling('entropic specter', '2004-10-04', 'You choose one opposing player during resolution and it only affects that one player. This choice is not changed even if this card changes controllers. It becomes 0/0 if the chosen player leaves the battlefield.').

card_ruling('eon hub', '2004-12-01', 'The upkeep step is skipped entirely. The turn proceeds from untap step to draw step.').
card_ruling('eon hub', '2004-12-01', 'Upkeep-triggered abilities don\'t trigger, and \"activate this ability only during your upkeep\" abilities can\'t be activated.').
card_ruling('eon hub', '2004-12-01', 'Any triggered abilities that triggered during the untap step will go onto the stack at the start of the draw step.').

card_ruling('ephara\'s enlightenment', '2014-02-01', 'The last ability of Ephara’s Enlightenment will trigger only if it’s on the battlefield when the creature enters the battlefield under your control. You may return it to its owner’s hand only if it’s still on the battlefield when that ability resolves.').
card_ruling('ephara\'s enlightenment', '2014-02-01', 'If Ephara’s Enlightenment isn’t on the battlefield when its enters-the-battlefield ability resolves, put a +1/+1 counter on the creature it was enchanting when it left the battlefield.').

card_ruling('ephara, god of the polis', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('ephara, god of the polis', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('ephara, god of the polis', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('ephara, god of the polis', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('ephara, god of the polis', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('ephara, god of the polis', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('ephara, god of the polis', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('ephara, god of the polis', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('ephara, god of the polis', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('ephara, god of the polis', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').
card_ruling('ephara, god of the polis', '2014-02-01', 'Ephara’s last ability checks at the beginning of each upkeep whether another creature entered the battlefield under your control last turn. If one did, it will trigger; otherwise, it won’t. The ability will trigger only once no matter how many creatures entered the battlefield under your control that turn, as long as at least one did.').
card_ruling('ephara, god of the polis', '2014-02-01', 'The last ability will trigger regardless of what has happened to the creature that entered the battlefield on the previous turn. It doesn’t matter whether it’s still under your control or whether it’s still on the battlefield.').
card_ruling('ephara, god of the polis', '2014-02-01', 'If a noncreature permanent you control (such as an Aura with bestow) becomes a creature, it will not cause Ephara’s last ability to trigger the following turn. This is true even if that noncreature permanent became a creature the same turn it entered the battlefield.').

card_ruling('ephemeral shields', '2014-07-18', 'The rules for convoke have changed slightly since it last appeared in an expansion. Previously, convoke reduced the cost to cast a spell. Under current rules, you tap creatures at the same time you pay the spell’s costs. Tapping a creature this way is simply another way to pay.').
card_ruling('ephemeral shields', '2014-07-18', 'Convoke doesn’t change a spell’s mana cost or converted mana cost.').
card_ruling('ephemeral shields', '2014-07-18', 'When calculating a spell’s total cost, include any alternative costs, additional costs, or anything else that increases or reduces the cost to cast the spell. Convoke applies after the total cost is calculated.').
card_ruling('ephemeral shields', '2014-07-18', 'Because convoke isn’t an alternative cost, it can be used in conjunction with alternative costs.').
card_ruling('ephemeral shields', '2014-07-18', 'Tapping a multicolored creature using convoke will pay for {1} or one mana of your choice of any of that creature’s colors.').
card_ruling('ephemeral shields', '2014-07-18', 'When using convoke to cast a spell with {X} in its mana cost, first choose the value for X. That choice, plus any cost increases or decreases, will determine the spell’s total cost. Then you can tap creatures you control to help pay that cost. For example, if you cast Chord of Calling (a spell with convoke and mana cost {X}{G}{G}{G}) and choose X to be 3, the total cost is {3}{G}{G}{G}. If you tap two green creatures and two red creatures, you’ll have to pay {1}{G}.').
card_ruling('ephemeral shields', '2014-07-18', 'If a creature you control has a mana ability with {T} in the cost, activating that ability while casting a spell with convoke will result in the creature being tapped when you pay the spell’s costs. You won’t be able to tap it again for convoke. Similarly, if you sacrifice a creature to activate a mana ability while casting a spell with convoke, that creature won’t be on the battlefield when you pay the spell’s costs, so you won’t be able to tap it for convoke.').

card_ruling('epic confrontation', '2015-02-25', 'If the creature you control is an illegal target as Epic Confrontation tries to resolve, the creature you control won\'t get +1/+2. If that creature is a legal target but the creature you don’t control isn’t, your creature will still get +1/+2. In both cases, neither creature will deal or be dealt damage.').

card_ruling('epic experiment', '2012-10-01', 'You cast the cards one at a time, choosing modes, targets, and so on. The last card you cast will be the first one to resolve.').
card_ruling('epic experiment', '2012-10-01', 'When casting an instant or sorcery card this way, ignore timing restrictions based on the card\'s type. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('epic experiment', '2012-10-01', 'If you can\'t cast an instant or sorcery card, perhaps because there are no legal targets available, or if you choose not to cast one, it will be put into your graveyard.').
card_ruling('epic experiment', '2012-10-01', 'If you cast a card “without paying its mana cost,” you can\'t pay alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('epic experiment', '2012-10-01', 'If a card has {X} in its mana cost, you must choose 0 as its value.').

card_ruling('epic struggle', '2004-10-04', 'To win, you must control 20 or more creatures both at the beginning of upkeep and when it resolves.').

card_ruling('epochrasite', '2007-05-01', 'Epochrasite will enter the battlefield with three +1/+1 counters on it if you cast it from a zone other than your hand, or if an effect let you put it directly onto the battlefield.').
card_ruling('epochrasite', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('epochrasite', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('epochrasite', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('epochrasite', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('epochrasite', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('epochrasite', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('epochrasite', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('epochrasite', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('epochrasite', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('epochrasite', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('equilibrium', '2008-04-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').
card_ruling('equilibrium', '2008-08-01', 'The ability is put on the stack immediately after you cast a creature spell, which means the creature that spell will become won\'t be a legal target.').
card_ruling('equilibrium', '2008-08-01', 'The ability will still resolve even if the spell that triggered it gets countered.').

card_ruling('equinox', '2004-10-04', 'Equinox will not counter a spell which requires sacrificing land when it enters the battlefield, or one that requires sacrificing land as part of the cost to cast it.').
card_ruling('equinox', '2004-10-04', 'Equinox will not counter a spell that has a random chance of destroying a land.').
card_ruling('equinox', '2004-10-04', 'Equinox will not counter a spell which would indirectly cause destruction of one of your lands.').
card_ruling('equinox', '2004-10-04', 'The ability can target any spell, even one that would not destroy a land.').
card_ruling('equinox', '2004-10-04', 'Equinox will not counter a spell that deals damage to an animated land, even if it would deal more damage than the land\'s toughness. This is because the spell itself does not destroy the land directly. The land is destroyed by a game rule.').
card_ruling('equinox', '2008-10-01', 'Will not counter a spell which would destroy a land only if a choice is made.').
card_ruling('equinox', '2013-09-20', 'When the activated ability resolves, determine whether the targeted spell would destroy a land if it resolved right then. If it would, then counter that spell. Otherwise, it is not countered, even if the spell could, under other circumstances, destroy a land.').

card_ruling('equipoise', '2004-10-04', 'In multiplayer games, you can choose a different target player each turn. You can even choose yourself.').
card_ruling('equipoise', '2009-10-01', 'You perform the selection and phasing out on lands first, then on artifacts, then on creatures. This means that if you choose an artifact creature during the second pass, your opponent will control one fewer creature when it comes time to perform the third pass.').

card_ruling('eradicate', '2004-10-04', 'Use the name of the card as it leaves the battlefield.').
card_ruling('eradicate', '2004-10-04', 'If a copy card is targeted by this effect, you get to look for another copy of the card it is copying. This is because a copy card actually takes on the name and initial characteristics of what it copies.').
card_ruling('eradicate', '2004-10-04', 'Does not exile other cards of the same name that are on the battlefield. Just from the graveyard, hand, and library.').
card_ruling('eradicate', '2005-02-01', 'If you manage to turn a land (such as a Forest or Stalking Stones) into a creature, you can indeed use this effect on that basic land type. Eradicate\'s exile effect only looks for cards by name, not type.').
card_ruling('eradicate', '2005-02-01', 'The copies must be found if they are in publicly viewable zones. Finding copies while searching private zones is optional.').
card_ruling('eradicate', '2012-05-01', 'A face-down creature has no name, so no card can possibly share a name with it (not even other cards with no name). If you Eradicate a face-down creature you will still search its controller\'s library, but you won\'t be able to exile any cards from that library.').

card_ruling('erase', '2004-10-04', 'The card does not go to the graveyard first.').

card_ruling('erayo, soratami ascendant', '2005-06-01', 'Erayo\'s Essence triggers only for the first spell an opponent casts in a given turn. It doesn\'t trigger off any other spell that player casts after the first.').
card_ruling('erayo, soratami ascendant', '2005-06-01', 'If Erayo, Soratami Ascendant flips while an opponent\'s first spell for the turn is still on the stack, the ability of Erayo\'s Essence doesn\'t trigger because the opponent\'s spell has already been cast. The spell isn\'t countered.').
card_ruling('erayo, soratami ascendant', '2005-06-01', 'If you have multiple opponents, Erayo\'s Essence can trigger once for each opponent.').
card_ruling('erayo, soratami ascendant', '2005-06-01', 'Each Ascendant is legendary in both its unflipped and flipped forms. This means that effects that look for legendary creature cards, such as Time of Need and Captain Sisay, can find an Ascendant.').

card_ruling('erdwal ripper', '2011-01-22', 'The +1/+1 counter isn\'t put on the creature in time to increase the amount of combat damage dealt during that combat step.').
card_ruling('erdwal ripper', '2011-01-22', 'If this creature deals combat damage to multiple players simultaneously, perhaps because some combat damage was redirected, its ability will trigger for each of those players.').

card_ruling('erebos\'s emissary', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('erebos\'s emissary', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('erebos\'s emissary', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('erebos\'s emissary', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('erebos\'s emissary', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('erebos\'s emissary', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('erebos\'s emissary', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('erebos\'s emissary', '2014-02-01', 'Only the controller of the Erebos\'s Emissary may activate the ability, even if Erebos\'s Emissary is attached to a creature another player controls.').

card_ruling('erebos\'s titan', '2015-06-22', 'Damage dealt to a creature with indestructible remains marked on that creature. If Erebos’s Titan has lethal damage marked on it and it loses indestructible (perhaps because your opponent controlled no creatures and then gained control of one), Erebos’s Titan will be destroyed.').
card_ruling('erebos\'s titan', '2015-06-22', 'The last ability triggers only if Erebos’s Titan is in your graveyard.').
card_ruling('erebos\'s titan', '2015-06-22', 'In a multiplayer game, the last ability won’t trigger when a player with a creature card in his or her graveyard leaves the game.').

card_ruling('erebos, god of the dead', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('erebos, god of the dead', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('erebos, god of the dead', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('erebos, god of the dead', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').
card_ruling('erebos, god of the dead', '2013-09-15', 'The type-changing ability that can make the God not be a creature functions only on the battlefield. It’s always a creature card in other zones, regardless of your devotion to its color.').
card_ruling('erebos, god of the dead', '2013-09-15', 'If a God enters the battlefield, your devotion to its color (including the mana symbols in the mana cost of the God itself) will determine if a creature entered the battlefield or not, for abilities that trigger whenever a creature enters the battlefield.').
card_ruling('erebos, god of the dead', '2013-09-15', 'If a God stops being a creature, it loses the type creature and all creature subtypes. It continues to be a legendary enchantment.').
card_ruling('erebos, god of the dead', '2013-09-15', 'The abilities of Gods function as long as they’re on the battlefield, regardless of whether they’re creatures.').
card_ruling('erebos, god of the dead', '2013-09-15', 'If a God is attacking or blocking and it stops being a creature, it will be removed from combat.').
card_ruling('erebos, god of the dead', '2013-09-15', 'If a God is dealt damage, then stops being a creature, then becomes a creature again later in the same turn, the damage will still be marked on it. This is also true for any effects that were affecting the God when it was originally a creature. (Note that in most cases, the damage marked on the God won’t matter because it has indestructible.)').

card_ruling('erhnam djinn', '2004-10-04', 'In multiplayer games you can choose a different player\'s creature each upkeep. You are forced to pick a creature that some opponent controls if there is at least one creature on the battlefield that is a legal target.').
card_ruling('erhnam djinn', '2004-10-04', 'If you have more than one Djinn, you can have all of them target the same creature with their ability.').
card_ruling('erhnam djinn', '2004-10-04', 'If there are no creatures your opponent controls to target at the beginning of your upkeep, ignore this ability.').

card_ruling('erithizon', '2004-10-04', 'Erithizon\'s controller controls the triggered ability, but their opponent chooses the target.').

card_ruling('errand of duty', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('errand of duty', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('errand of duty', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('errand of duty', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('errant ephemeron', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('errant ephemeron', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('errant ephemeron', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('errant ephemeron', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('errant ephemeron', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('errant ephemeron', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('errant ephemeron', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('errant ephemeron', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('errant ephemeron', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('errant ephemeron', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('errantry', '2008-10-01', '\"Can only attack alone\" means the enchanted creature can\'t be declared as an attacker during the declare attackers step unless no other creatures are declared as attackers at that time (either by you or your Two-Headed Giant teammate).').
card_ruling('errantry', '2008-10-01', 'If the enchanted creature attacks alone, then a spell or ability causes another creature to be put onto the battlefield attacking, that\'s fine.').

card_ruling('erratic explosion', '2004-10-04', 'If all your cards are lands, no damage is dealt.').

card_ruling('erratic mutation', '2007-02-01', 'The target creature is chosen as the spell is cast. You don\'t reveal cards until the spell resolves.').
card_ruling('erratic mutation', '2007-02-01', 'If a split card is revealed, two +X/-X effects are created, one for each half of the split card.').

card_ruling('ersatz gnomes', '2013-04-15', 'If Ersatz Gnomes targets a spell that becomes a permanent, the permanent will enter the battlefield colorless and will remain colorless until it leaves the battlefield (or until another effect changes its color).').

card_ruling('ertai\'s familiar', '2008-04-01', 'Activating the activated ability will prevent Ertai\'s Familiar from phasing out for any reason, not just due to its own phasing ability.').

card_ruling('ertai\'s meddling', '2004-10-04', 'Note that a delayed spell that targets another spell will be countered when it resolves since it will find that its target is no longer on the stack.').
card_ruling('ertai\'s meddling', '2004-10-04', 'A targeted spell which is delayed will still succeed even if its target has phased out and back in again.').
card_ruling('ertai\'s meddling', '2004-10-04', 'Ertai\'s Meddling can\'t be cast through any way that doesn\'t pay its mana cost. This is because the X in the Meddling\'s mana cost can\'t be 0, but effects that allow spells to be cast without paying their mana costs set X to 0.').
card_ruling('ertai\'s meddling', '2004-10-04', 'If a copy of a spell (one that has no card representing it) is affected by Ertai\'s Meddling, the spell ceases to exist when exiled. It will not gain counters and will not be put back on the stack.').
card_ruling('ertai\'s meddling', '2004-10-04', 'Once it is put back on the stack, it is a \"new\" spell again and can be countered or even targeted by another Ertai\'s Meddling.').
card_ruling('ertai\'s meddling', '2004-10-04', 'If Ertai\'s Meddling is used to copy a spell being cast face down due to Morph ability, the spell will create a face up, 2/2, colorless, nameless creature with no text. This may be a little counter-intuitive, because you might expect the card to enter the battlefield face down like it would have when originally cast, but Ertai\'s Meddling copies only the original spell and not the entire card the spell represented.').
card_ruling('ertai\'s meddling', '2008-04-01', 'This now exiles the spell as part of Ertai\'s Meddling\'s resolution, instead of waiting for the targeted spell to start resolving.').
card_ruling('ertai\'s meddling', '2011-01-22', 'If the spell was cast using flashback, Ertai\'s Meddling will still exile it with delay counters on it. When the card is returned to the stack, it still \"remembers\" the flashback cost was originally paid. It\'ll be exiled when it resolves or otherwise leaves the stack.').
card_ruling('ertai\'s meddling', '2013-04-15', 'If Ertai\'s Meddling is used to copy an arcane spell that had effects spliced onto it, it will create a spell with all of those effects. That spell\'s controller will not be able to splice additional effects onto the spell, since they did not re-cast the spell and instead simply put it back onto the stack.').
card_ruling('ertai\'s meddling', '2013-04-15', 'Ertai\'s Meddling has the spell\'s controller put the spell back onto the stack; it does not have its controller cast the spell again. Anything that triggers off of casting spells, such as Contemplation , won\'t trigger. Simillarly, effects that count spells that are cast (like Rule of Law) or prevent spells from being cast (like Iona, Shield of Emeria) won\'t count or affect the copy that is put onto the stack since the copy wasn\'t cast.').

card_ruling('ertai\'s trickery', '2009-10-01', 'You can target any spell, but that spell will be countered only if it was kicked.').

card_ruling('ertai, the corrupted', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').

card_ruling('ertai, wizard adept', '2004-10-04', 'The Legend rule only applies to two legendary permanents with identical names. If the names are different, even if they depict the same character, the rule does not apply.').

card_ruling('escaped null', '2010-06-15', 'When Escaped Null blocks or becomes blocked, its last ability triggers just once, no matter how many creatures Escaped Null blocked or became blocked by.').

card_ruling('escaped shapeshifter', '2004-10-04', 'In a multiplayer game, it works if any opponent controls creatures with the appropriate abilities.').

card_ruling('esper battlemage', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('esper charm', '2008-10-01', 'You can choose a mode only if you can choose legal targets for that mode. If you can\'t choose legal targets for any of the modes, you can\'t cast the spell.').
card_ruling('esper charm', '2008-10-01', 'While the spell is on the stack, treat it as though its only text is the chosen mode. The other two modes are treated as though they don\'t exist. You don\'t choose targets for those modes.').
card_ruling('esper charm', '2008-10-01', 'If this spell is copied, the copy will have the same mode as the original.').

card_ruling('esper sojourners', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('esper sojourners', '2009-05-01', 'The triggered ability acts as a cycle-triggered ability or as a leaves-the-battlefield ability, as appropriate. The player who controls the triggered ability is the player who cycled the Sojourner, or the player who last controlled the Sojourner on the battlefield.').
card_ruling('esper sojourners', '2009-05-01', 'If you cycle this card, the cycling ability goes on the stack, then the triggered ability goes on the stack on top of it. The triggered ability will resolve before you draw a card from the cycling ability.').
card_ruling('esper sojourners', '2009-05-01', 'The cycling ability and the triggered ability are separate. If the triggered ability is countered (with Stifle, for example, or if all its targets have become illegal), the cycling ability will still resolve and you\'ll draw a card.').
card_ruling('esper sojourners', '2009-05-01', 'You can cycle this card even if there are no targets for the triggered ability. That\'s because the cycling ability itself has no targets.').

card_ruling('esperzoa', '2009-02-01', 'When Esperzoa\'s ability resolves, if you control no other artifacts, you\'ll have to return Esperzoa itself.').

card_ruling('essence backlash', '2012-10-01', 'Essence Backlash can target a creature spell that can\'t be countered. If Essence Backlash resolves, it won\'t counter the spell but it will still deal damage.').
card_ruling('essence backlash', '2012-10-01', 'If the creature\'s power is * and it has an ability defining its power, use that ability to determine its power.').
card_ruling('essence backlash', '2012-10-01', 'Abilities that change the power of a creature (such as the one of Collective Blessing) affect only creatures on the battlefield. They won\'t change a creature spell\'s power on the stack.').

card_ruling('essence drain', '2012-07-01', 'If the target creature or player is an illegal target when Essence Drain tries to resolve, it will be countered and none of its effects will happen. You won\'t gain 3 life.').

card_ruling('essence flare', '2008-10-01', 'The -0/-1 counters stay on the creature even if Essence Flare is removed.').

card_ruling('essence fracture', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('essence harvest', '2012-05-01', 'The value of X is determined when Essence Harvest resolves. If you control no creatures that time, no life will be lost or gained.').
card_ruling('essence harvest', '2012-05-01', 'Essence Harvest targets only the player. It doesn\'t target any creatures. If that player is an illegal target when Essence Harvest tries to resolve, it will be countered and none of its effects will happen. No life will be lost or gained.').

card_ruling('essence leak', '2004-10-04', 'It can enchant a permanent that is not red or green, but it doesn\'t do anything in that case.').

card_ruling('essence of the wild', '2011-09-22', 'Creatures you control don\'t copy whether Essence of the Wild is tapped or untapped, whether it has any counters on it or any Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('essence of the wild', '2011-09-22', 'Because creatures you control enter the battlefield as a copy of Essence of the Wild, any enters-the-battlefield triggered abilities printed on such creatures won\'t trigger.').
card_ruling('essence of the wild', '2011-09-22', 'Replacement effects that modify how a creature enters the battlefield are applied in the following order: first control-changing effects (such as Gather Specimens), then copy effects (such as the abilities of Essence of the Wild and Clone), then all other effects. This is a minor rules change to make Essence of the Wild and similar cards in the future work intuitively.').
card_ruling('essence of the wild', '2011-09-22', 'If a creature such as Clone is entering the battlefield under your control, there will be two copy effects to apply: the creature\'s own and Essence of the Wild\'s. No matter what order these effects are applied, the creature will be a copy of Essence of the Wild when it enters the battlefield.').
card_ruling('essence of the wild', '2011-09-22', 'Other enters-the-battlefield replacement abilities printed on the creature entering the battlefield won\'t be applied because the creature will already be Essence of the Wild at that point (and therefore it won\'t have those abilities). For example, a creature that normally enters the battlefield tapped will enter the battlefield as an untapped Essence of the Wild, and a creature that would normally enter the battlefield with counters on it will enter the battlefield as an Essence of the Wild with no counters.').
card_ruling('essence of the wild', '2011-09-22', 'External abilities may still affect how a creature enters the battlefield. For example, if your opponent controls Urabrask the Hidden, which reads, in part, \"Creatures your opponents control enter the battlefield tapped,\" a creature entering the battlefield under your control will be a tapped Essence of the Wild.').
card_ruling('essence of the wild', '2011-09-22', 'If a creature such as Cryptoplasm that\'s already on the battlefield copies Essence of the Wild and modifies its own copy effect, that modification is also copied by creatures entering the battlefield under your control.').
card_ruling('essence of the wild', '2011-09-22', 'If you control more than one Essence of the Wild, creatures you control will enter the battlefield as a copy of the one whose copy effect you apply last.').

card_ruling('essence scatter', '2009-10-01', 'A \"creature spell\" is any spell with the type Creature, even if it has other types such as Artifact or Enchantment. Older cards of type Summon are also Creature spells.').

card_ruling('essence sliver', '2004-10-04', 'You only gain the life when the triggered ability resolves. If you are reduced to zero life before the ability resolves, you will lose before gaining the life.').
card_ruling('essence sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('essence sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('essence warden', '2004-10-04', 'Does not trigger on a card on the battlefield being changed into a creature.').
card_ruling('essence warden', '2004-10-04', 'The ability will not trigger on itself entering the battlefield, but it will trigger on any other creature that is put onto the battlefield at the same time.').
card_ruling('essence warden', '2007-02-01', 'This is the timeshifted version of Soul Warden.').

card_ruling('etched champion', '2011-01-01', '\"Protection from all colors\" means protection from white, from blue, from black, from red, and from green. (In other words, it doesn\'t just mean \"protection from objects that have all five colors.\")').

card_ruling('eternal dragon', '2009-02-01', 'Unlike the normal cycling ability, Plainscycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Plains card. After you find a Plains card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('eternal dragon', '2009-02-01', 'Plainscycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Plainscycling this card. Any ability that stops a cycling ability from being activated also stops Plainscycling from being activated.').
card_ruling('eternal dragon', '2009-02-01', 'Plainscycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Plainscycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('eternal dragon', '2009-02-01', 'You can choose to find any card with the Plains land type, including nonbasic lands. You can also choose not to find a card, even if there is a Plains card in your library.').

card_ruling('eternity vessel', '2009-10-01', 'The landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability (such as Rampant Growth) causes you to put a land onto the battlefield under your control. It will even trigger when a spell or ability causes another player to put a land onto the battlefield under your control (as can happen with Yavimaya Dryad\'s ability, for example).').
card_ruling('eternity vessel', '2009-10-01', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one that resolves.').
card_ruling('eternity vessel', '2009-10-01', 'Eternity Vessel\'s landfall ability cares only about how many charge counters are on it, not what its controller\'s life total was as it entered the battlefield. The number of charge counters may be changed by other spells and abilities.').
card_ruling('eternity vessel', '2009-10-01', 'For your life total to become the number of charge counters on Eternity Vessel, you actually gain or lose the necessary amount of life. For example, if your life total is 4 when Eternity Vessel\'s landfall ability resolves and Eternity Vessel has 12 charge counters on it, the ability will cause you to gain 8 life. Other cards that interact with life gain or life loss will interact with this effect accordingly.').
card_ruling('eternity vessel', '2010-06-15', 'In a Two-Headed Giant game, your life total is the same as your team\'s life total, so Eternity Vessel enters the battlefield with a number of charge counters on it equal to your team\'s life total. When its landfall ability resolves, it causes its controller to lose or gain the appropriate amount of life, and the team\'s life total is adjusted by the amount of life the player gains or loses as a result of this ability. Suppose Eternity Vessel has 15 charge counters on it when its landfall ability resolves, and its controller\'s team has 11 life. That player\'s life total is 11, which becomes 15, for a net gain of 4 life. The team\'s life total becomes 15.').

card_ruling('ether well', '2004-10-04', 'The choice of where to put the creature is made on resolution.').

card_ruling('ethereal ambush', '2014-11-24', 'The cards are manifested one at a time. It must remain clear which face-down creature was the top card of your library and which one was the second card of your library.').
card_ruling('ethereal ambush', '2014-11-24', 'If you’re playing with the top card of your library revealed as Ethereal Ambush resolves (perhaps because you control a card such as Courser of Kruphix), you’ll manifest the top card, reveal the next card (now the top card), and then manifest that card.').
card_ruling('ethereal ambush', '2014-11-24', 'The face-down permanent is a 2/2 creature with no name, mana cost, creature types, or abilities. It’s colorless and has a converted mana cost of 0. Other effects that apply to the permanent can still grant or change any of these characteristics.').
card_ruling('ethereal ambush', '2014-11-24', 'Any time you have priority, you may turn a manifested creature face up by revealing that it’s a creature card (ignoring any copy effects or type-changing effects that might be applying to it) and paying its mana cost. This is a special action. It doesn’t use the stack and can’t be responded to.').
card_ruling('ethereal ambush', '2014-11-24', 'If a manifested creature would have morph if it were face up, you may also turn it face up by paying its morph cost.').
card_ruling('ethereal ambush', '2014-11-24', 'Unlike a face-down creature that was cast using the morph ability, a manifested creature may still be turned face up after it loses its abilities if it’s a creature card.').
card_ruling('ethereal ambush', '2014-11-24', 'Because the permanent is on the battlefield both before and after it’s turned face up, turning a permanent face up doesn’t cause any enters-the-battlefield abilities to trigger.').
card_ruling('ethereal ambush', '2014-11-24', 'Because face-down creatures don’t have names, they can’t have the same name as any other creature, even another face-down creature.').
card_ruling('ethereal ambush', '2014-11-24', 'A permanent that turns face up or face down changes characteristics but is otherwise the same permanent. Spells and abilities that were targeting that permanent, as well as Auras and Equipment that were attached to the permanent, aren’t affected.').
card_ruling('ethereal ambush', '2014-11-24', 'Turning a permanent face up or face down doesn’t change whether that permanent is tapped or untapped.').
card_ruling('ethereal ambush', '2014-11-24', 'At any time, you can look at a face-down permanent you control. You can’t look at face-down permanents you don’t control unless an effect allows you to or instructs you to.').
card_ruling('ethereal ambush', '2014-11-24', 'If a face-down permanent you control leaves the battlefield, you must reveal it. You must also reveal all face-down spells and permanents you control if you leave the game or if the game ends.').
card_ruling('ethereal ambush', '2014-11-24', 'You must ensure that your face-down spells and permanents can easily be differentiated from each other. You’re not allowed to mix up the cards that represent them on the battlefield in order to confuse other players. The order they entered the battlefield should remain clear. Common methods for indicating this include using markers or dice, or simply placing them in order on the battlefield. You must also track how each became face down (manifested, cast face down using the morph ability, and so on).').
card_ruling('ethereal ambush', '2014-11-24', 'There are no cards in the Fate Reforged set that would turn a face-down instant or sorcery card on the battlefield face up, but some older cards can try to do this. If something tries to turn a face-down instant or sorcery card on the battlefield face up, reveal that card to show all players it’s an instant or sorcery card. The permanent remains on the battlefield face down. Abilities that trigger when a permanent turns face up won’t trigger, because even though you revealed the card, it never turned face up.').
card_ruling('ethereal ambush', '2014-11-24', 'Some older Magic sets feature double-faced cards, which have a Magic card face on each side rather than a Magic card face on one side and a Magic card back on the other. The rules for double-faced cards are changing slightly to account for the possibility that they are manifested. If a double-faced card is manifested, it will be put onto the battlefield face down. While face down, it can’t transform. If the front face of the card is a creature card, you can turn it face up by paying its mana cost. If you do, its front face will be up. A double-faced permanent on the battlefield still can’t be turned face down.').

card_ruling('ethereal armor', '2012-10-01', 'Ethereal Armor counts each enchantment you control, including itself and any Auras you control attached to an opponent or to permanents controlled by an opponent.').

card_ruling('etherium astrolabe', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('etherium sculptor', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('etherium sculptor', '2008-10-01', 'This effect doesn\'t change the mana cost or converted mana cost of an artifact spell. Rather, it reduces the total cost of the spell, which is the amount you actually pay while casting it. The total cost takes into account additional or alternative costs.').
card_ruling('etherium sculptor', '2008-10-01', 'This effect can reduce only the generic portion of the artifact spell\'s total cost.').

card_ruling('etherium-horn sorcerer', '2012-06-01', 'Etherium-Horn Sorcerer\'s first ability can only be activated when it is on the battlefield. If Etherium-Horn Sorcerer isn\'t on the battlefield when the ability resolves, the ability won\'t do anything.').

card_ruling('ethersworn canonist', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('ethersworn canonist', '2008-10-01', 'In other words: Each turn, each player can cast any number of artifact spells plus a maximum of one nonartifact spell.').
card_ruling('ethersworn canonist', '2008-10-01', 'This effect counts all nonartifact spells that are cast, even those that are countered.').
card_ruling('ethersworn canonist', '2008-10-01', 'This effect takes into account spells that were cast earlier in the turn that Ethersworn Canonist entered the battlefield, including any spells still on the stack. However, any spells on the stack as Ethersworn Canonist enters the battlefield have already been cast by that point, so they\'re not affected by it.').

card_ruling('ethersworn shieldmage', '2009-05-01', 'When Ethersworn Shieldmage\'s triggered ability resolves, it doesn\'t lock in which permanents will be affected. For the rest of the turn, any time damage would be dealt to a permanent, check whether it\'s an artifact creature at that time. If it is, that damage is prevented.').

card_ruling('etherwrought page', '2009-05-01', 'You choose a mode when the ability triggers.').
card_ruling('etherwrought page', '2009-05-01', 'You may choose a different mode each time it triggers.').

card_ruling('eureka', '2004-10-04', 'The cards are put onto the battlefield and will not trigger effects which trigger on such cards being \"cast\" or \"played\".').
card_ruling('eureka', '2004-10-04', 'In a game of N players, the process ends when all N players in sequence (starting with you) choose not to put a card onto the battlefield. It doesn\'t end the first time a player chooses not to put a card onto the battlefield. If a player chooses not to put a card onto the battlefield but the process continues, that player may put a card onto the battlefield the next time the process gets around to him or her.').
card_ruling('eureka', '2004-10-04', 'If the card has an X in its mana cost, treat X as zero.').
card_ruling('eureka', '2006-10-15', 'Anything that triggers during the resolution of this will wait to be put on the stack until everything is put onto the battlefield and resolution is complete. The player whose turn it is will put all of his or her triggered abilities on the stack in any order, then each other player in turn order will do the same. (The last ability put on the stack will be the first one that resolves.)').
card_ruling('eureka', '2007-09-16', 'A \"permanent card\" is a card that would be a permanent once it\'s on the battlefield. Specifically, it\'s an artifact, creature, enchantment, land, or planeswalker card.').

card_ruling('evangel of heliod', '2013-09-15', 'Numeric mana symbols ({0}, {1}, and so on) in mana costs of permanents you control don’t count toward your devotion to any color.').
card_ruling('evangel of heliod', '2013-09-15', 'Mana symbols in the text boxes of permanents you control don’t count toward your devotion to any color.').
card_ruling('evangel of heliod', '2013-09-15', 'Hybrid mana symbols, monocolored hybrid mana symbols, and Phyrexian mana symbols do count toward your devotion to their color(s).').
card_ruling('evangel of heliod', '2013-09-15', 'If an activated ability or triggered ability has an effect that depends on your devotion to a color, you count the number of mana symbols of that color among the mana costs of permanents you control as the ability resolves. The permanent with that ability will be counted if it’s still on the battlefield at that time.').

card_ruling('evangelize', '2006-09-25', 'When you put the spell on the stack, you choose an opponent, then that opponent chooses the target. This is not a choice made on resolution.').
card_ruling('evangelize', '2006-09-25', 'The target of the spell is a creature the chosen opponent controls. As Evangelize resolves, if the target isn\'t a creature controlled by the chosen opponent, Evangelize will be countered. If Evangelize\'s target is changed (via Shunt, for example), the new target must be a creature controlled by the chosen opponent.').

card_ruling('evaporate', '2004-10-04', 'A creature which is both blue and white only takes one damage.').

card_ruling('evasive action', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('evasive action', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('evasive action', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('even the odds', '2007-05-01', 'It doesn\'t matter how many creatures you control as Even the Odds resolves.').

card_ruling('everflame eidolon', '2013-09-15', 'You don’t choose whether the spell is going to be an Aura spell or not until the spell is already on the stack. Abilities that affect when you can cast a spell, such as flash, will apply to the creature card in whatever zone you’re casting it from. For example, an effect that said you can cast creature spells as though they have flash will allow you to cast a creature card with bestow as an Aura spell anytime you could cast an instant.').
card_ruling('everflame eidolon', '2013-09-15', 'On the stack, a spell with bestow is either a creature spell or an Aura spell. It’s never both, although it’s an enchantment spell in either case.').
card_ruling('everflame eidolon', '2013-09-15', 'Unlike other Aura spells, an Aura spell with bestow isn’t countered if its target is illegal as it begins to resolve. Rather, the effect making it an Aura spell ends, it loses enchant creature, it returns to being an enchantment creature spell, and it resolves and enters the battlefield as an enchantment creature.').
card_ruling('everflame eidolon', '2013-09-15', 'Unlike other Auras, an Aura with bestow isn’t put into its owner’s graveyard if it becomes unattached. Rather, the effect making it an Aura ends, it loses enchant creature, and it remains on the battlefield as an enchantment creature. It can attack (and its {T} abilities can be activated, if it has any) on the turn it becomes unattached if it’s been under your control continuously, even as an Aura, since your most recent turn began.').
card_ruling('everflame eidolon', '2013-09-15', 'If a permanent with bestow enters the battlefield by any method other than being cast, it will be an enchantment creature. You can’t choose to pay the bestow cost and have it become an Aura.').
card_ruling('everflame eidolon', '2013-09-15', 'Auras attached to a creature don’t become tapped when the creature becomes tapped. Except in some rare cases, an Aura with bestow remains untapped when it becomes unattached and becomes a creature.').
card_ruling('everflame eidolon', '2013-09-15', 'An Aura that becomes a creature is no longer put into its owner’s graveyard as a state-based action. Rather, it becomes unattached and remains on the battlefield as long as it’s a creature. While it’s a creature, it can’t be attached to another permanent or player. An Aura that’s not attached to a legal permanent or player as defined by its enchant ability and also isn’t a creature will be put into its owner’s graveyard as a state-based action.').
card_ruling('everflame eidolon', '2014-02-01', 'As Everflame Eidolon’s ability resolves, the bonus is applied to either Everflame Eidolon (if it’s a creature) or to the enchanted creature (if it’s an Aura). If the bonus is applied to the enchanted creature, that bonus won’t apply to Everflame Eidolon, even if it becomes a creature later that turn.').

card_ruling('everflowing chalice', '2010-03-01', 'If Everflowing Chalice has no charge counters on it, you can still activate its last ability. That ability is still a mana ability, even though it produces no mana as it resolves.').

card_ruling('everglades', '2006-02-01', 'This has a triggered ability when it enters the battlefield, not a replacement effect, as previously worded.').

card_ruling('everglove courier', '2004-10-04', 'It checks the creature type when the ability is announced and resolved, but once the effect is placed on the creature, if its creature type changes the effect still continues.').

card_ruling('everlasting torment', '2008-05-01', 'Spells and abilities that would normally cause a player to gain life still resolve, but the life-gain part simply has no effect.').
card_ruling('everlasting torment', '2008-05-01', 'If a cost includes life gain (like Invigorate\'s alternative cost does), that cost can\'t be paid.').
card_ruling('everlasting torment', '2008-05-01', 'Effects that would replace gaining life with some other effect won\'t be able to do anything because it\'s impossible for players to gain life.').
card_ruling('everlasting torment', '2008-05-01', 'Effects that replace an event with gaining life (like Words of Worship\'s effect does) will end up replacing the event with nothing.').
card_ruling('everlasting torment', '2008-05-01', 'If an effect says to set your life total to a certain number, and that number is higher than your current life total, that effect will normally cause you to gain life equal to the difference. With Everlasting Torment on the battlefield, that part of the effect won\'t do anything. (If the number is lower than your current life total, the effect will work as normal.)').
card_ruling('everlasting torment', '2008-05-01', 'The \"damage can\'t be prevented\" statement overrides all forms of preventing damage, including protection abilities. Damage prevention spells and abilities can still be cast and played; they just don\'t do anything.').
card_ruling('everlasting torment', '2008-05-01', 'Spells and abilities that replace or redirect damage aren\'t affected by Everlasting Torment\'s second ability. They\'ll work as normal.').
card_ruling('everlasting torment', '2008-05-01', 'The last ability affects all damage, whether it\'s dealt by creatures, other permanents, spells, or cards that aren\'t on the battlefield. Wither works everywhere.').

card_ruling('evermind', '2005-06-01', 'Evermind is blue in all zones.').
card_ruling('evermind', '2005-06-01', 'Evermind is a blue Arcane instant card. Cards that check whether a card has any of those attributes will affect Evermind. For example, you can exile it (as a blue card) to pay the alternative costs of spells like Force of Will and Disrupting Shoal (X = 0). You could search for it with Mystical Tutor because it\'s an instant or Eerie Procession because it\'s an Arcane spell.').
card_ruling('evermind', '2006-10-15', 'If a spell or effect, such as Parallectric Feedback, asks for Evermind\'s converted mana cost, it\'s 0.').
card_ruling('evermind', '2006-10-15', 'Evermind has no mana cost, which means it can\'t normally be cast as a spell. You could, however, cast it via some alternate means, like with Sunforger or Kaho, Minamo Historian.').
card_ruling('evermind', '2006-10-15', 'This has no mana cost, which means it can\'t be cast with the Replicate ability of Djinn Illuminatus or by somehow giving it Flashback.').
card_ruling('evermind', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('evermind', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('evermind', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('evermind', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').
card_ruling('evermind', '2013-06-07', 'Although originally printed with a characteristic-defining ability that defined its color, this card now has a color indicator. This color indicator can\'t be affected by text-changing effects (such as the one created by Crystal Spray), although color-changing effects can still overwrite it.').

card_ruling('evernight shade', '2012-05-01', 'Evernight Shade\'s ability doesn\'t put +1/+1 counters on it; it just gives Evernight Shade +1/+1 until end of turn. If Evernight Shade has no +1/+1 counters on it and dies after its ability has resolved, undying will still return it to the battlefield.').

card_ruling('evershrike', '2008-08-01', 'You can activate Evershrike\'s ability only while it\'s in your graveyard.').
card_ruling('evershrike', '2008-08-01', 'You can\'t put an Aura onto the battlefield this way unless that Aura can legally be attached to Evershrike. For example, you can\'t put an Aura card with \"enchant player\" onto the battlefield this way.').
card_ruling('evershrike', '2008-08-01', 'You can activate Evershrike\'s ability with X = 0. If you do, you\'ll return Evershrike to the battlefield from your graveyard, then exile it. (There are no Aura cards with converted mana cost 0.)').
card_ruling('evershrike', '2008-08-01', 'You can activate Evershrike\'s ability even if you have no Auras in your hand, or if you have an Aura in your hand that you don\'t want to put on it or can\'t put on it. In either case, you\'ll return Evershrike to the battlefield from your graveyard, then exile it. No player has the opportunity to cast spells or activate abilities while Evershrike is on the battlefield.').

card_ruling('every hope shall vanish', '2010-06-15', 'First you choose a nonland card in each opponent\'s hand (if he or she has any). Then all the chosen cards are discarded at the same time.').

card_ruling('every last vestige shall rot', '2010-06-15', 'You choose the target player when the ability triggers. As the ability resolves, you choose a value for X and decide whether to pay {X}. If you do decide to pay {X}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('evil presence', '2009-10-01', 'The enchanted land loses its existing land types and any abilities printed on it. It now has the land type Swamp and has the ability \"{T}: Add {B} to your mana pool.\" Evil Presence doesn\'t change the enchanted land\'s name or whether it\'s legendary, basic, or snow.').

card_ruling('evil twin', '2011-09-22', 'Evil Twin copies exactly what was printed on the original creature (unless that creature is copying something else or is a token; see below) and it gains the activated ability. It doesn\'t copy whether that creature is tapped or untapped, whether it has any counters on it or any Auras and Equipment attached to it, or any non-copy effects that have changed its power, toughness, types, color, or so on.').
card_ruling('evil twin', '2011-09-22', 'If you copy a double-faced creature, Evil Twin will be a copy of the face that\'s up when Evil Twin enters the battlefield. Because Evil Twin is not a double-faced card, it won\'t be able to transform.').
card_ruling('evil twin', '2011-09-22', 'If the chosen creature has {X} in its mana cost, X is considered to be zero.').
card_ruling('evil twin', '2011-09-22', 'If the chosen creature is copying something else (for example, if the chosen creature is another Evil Twin), then your Evil Twin enters the battlefield as whatever the chosen creature copied.').
card_ruling('evil twin', '2011-09-22', 'If the chosen creature is a token, Evil Twin copies the original characteristics of that token as stated by the effect that put the token onto the battlefield. Evil Twin is not a token.').
card_ruling('evil twin', '2011-09-22', 'Any enters-the-battlefield abilities of the copied creature will trigger when Evil Twin enters the battlefield. Any \"as [this creature] enters the battlefield\" or \"[this creature] enters the battlefield with\" abilities of the chosen creature will also work.').
card_ruling('evil twin', '2011-09-22', 'If Evil Twin somehow enters the battlefield at the same time as another creature, Evil Twin can\'t become a copy of that creature. You may only choose a creature that\'s already on the battlefield.').
card_ruling('evil twin', '2011-09-22', 'You can choose not to copy anything. In that case, Evil Twin enters the battlefield as a 0/0 creature, and is probably put into the graveyard immediately.').

card_ruling('eviscerator', '2004-10-04', 'This is loss of life, and not damage. It can\'t be prevented.').

card_ruling('evolutionary leap', '2015-06-22', 'No player will know the order of the cards put on the bottom of your library. Practically speaking, the cards should be shuffled (although this is not the game action of “shuffling”).').
card_ruling('evolutionary leap', '2015-06-22', 'If you don’t reveal a creature card, you’ll reveal all the cards from your library and then put them back in your library in a random order. (This is effectively the same as shuffling your library, although it’s still not technically a shuffle.)').

card_ruling('exalted angel', '2004-10-04', 'You only gain the life when the triggered ability resolves. If you are reduced to zero life before the ability resolves, you will lose before gaining the life.').

card_ruling('exalted dragon', '2004-10-04', 'You have to sacrifice a land at the time you declare this creature as an attacker if you want it to attack. This is additive with any other attack costs.').

card_ruling('exava, rakdos blood witch', '2013-04-15', 'You make the choice to have the creature with unleash enter the battlefield with a +1/+1 counter or not as it\'s entering the battlefield. At that point, it\'s too late for a player to respond to the creature spell by trying to counter it, for example.').
card_ruling('exava, rakdos blood witch', '2013-04-15', 'The unleash ability applies no matter where the creature is entering the battlefield from.').
card_ruling('exava, rakdos blood witch', '2013-04-15', 'A creature with unleash can\'t block if it has any +1/+1 counter on it, not just one put on it by the unleash ability.').
card_ruling('exava, rakdos blood witch', '2013-04-15', 'Putting a +1/+1 counter on a creature with unleash that\'s already blocking won\'t remove it from combat. It will continue to block.').

card_ruling('excavator', '2005-11-01', 'If a land with the supertype \"basic\" somehow has multiple subtypes, then the creature will gain landwalk of all those types.').

card_ruling('excise', '2004-10-04', 'The creature\'s controller gets the option to pay when this spell resolves.').

card_ruling('exclusion ritual', '2011-06-01', 'If the exiled card somehow leaves exile, the last ability will no longer have any effect.').
card_ruling('exclusion ritual', '2011-06-01', 'In a Commander game, a player may replace Exclusion Ritual exiling his or her commander with putting that commander into the command zone. If this happens, Exclusion Ritual\'s last ability will have no effect.').

card_ruling('excruciator', '2005-10-01', 'An effect may redirect Excruciator\'s damage.').
card_ruling('excruciator', '2005-10-01', 'Excruciator can deal damage to creatures with protection from red. (It can\'t block creatures with protection from red, though.)').
card_ruling('excruciator', '2005-10-01', 'Damage prevention shields that would prevent this damage aren\'t used up and they stick around for the next time something would deal damage.').
card_ruling('excruciator', '2005-10-01', 'A creature dealt lethal damage by Excruciator can be regenerated.').
card_ruling('excruciator', '2005-10-01', 'Replacement effects that don\'t use the word \"prevent\" can replace Excruciator\'s damage with something else. See Phytohydra and Szadek, Lord of Secrets, for example.').

card_ruling('executioner\'s capsule', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').

card_ruling('executioner\'s swing', '2013-01-24', 'You can\'t choose a creature that hasn\'t yet dealt damage during the turn as the target of Executioner\'s Swing, so in most cases it\'s not possible for Executioner\'s Swing to reduce the amount of combat damage a creature deals. (If a creature has double strike, you could cast Executioner\'s Swing after it deals first-strike damage but before it deals regular combat damage.)').
card_ruling('executioner\'s swing', '2013-01-24', 'It doesn\'t matter what the creature dealt damage to or if that damage was combat damage.').

card_ruling('exert influence', '2015-08-25', 'Check the creature’s power as Exert Influence resolves to see if it’s less than or equal to the number of colors spent to cast Exert Influence. If it’s greater, Exert Influence will finish resolving with no effect.').
card_ruling('exert influence', '2015-08-25', 'Once you’ve gained control of a creature with Exert Influence, it doesn’t matter what happens to its power. Raising it won’t cause the control-changing effect to end.').
card_ruling('exert influence', '2015-08-25', 'The maximum number of colors of mana you can spend to cast a spell is five. Colorless is not a color. Note that the cost of a spell with converge may limit how many colors of mana you can spend.').
card_ruling('exert influence', '2015-08-25', '').
card_ruling('exert influence', '2015-08-25', 'If there are any alternative or additional costs to cast a spell with a converge ability, the mana spent to pay those costs will count. For example, if an effect makes sorcery spells cost {1} more to cast, you could pay {W}{U}{B}{R} to cast Radiant Flames and deal 4 damage to each creature.').
card_ruling('exert influence', '2015-08-25', 'If you cast a spell with converge without spending any mana to cast it (perhaps because an effect allowed you to cast it without paying its mana cost), then the number of colors spent to cast it will be zero.').
card_ruling('exert influence', '2015-08-25', 'If a spell with a converge ability is copied, no mana was spent to cast the copy, so the number of colors of mana spent to cast the spell will be zero. The number of colors spent to cast the original spell is not copied.').

card_ruling('exhaustion', '2004-10-04', 'The creatures and lands are only prevented from untapping during the targeted player\'s next untap step. They can still can untap during other player\'s untap steps.').
card_ruling('exhaustion', '2009-10-01', 'No creatures or lands controlled by the player will untap during his or her next untap step. This includes those which entered the battlefield after Exhaustion resolved.').

card_ruling('exhumer thrull', '2006-02-01', 'If you own the creature Exhumer Thrull was haunting, that creature will generally be a legal target for the haunt trigger to return to your hand.').

card_ruling('exoskeletal armor', '2004-10-04', 'The value of X changes as the number of creatures cards in graveyards changes.').

card_ruling('exotic curse', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('exotic curse', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('exotic curse', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('exotic disease', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('exotic disease', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('exotic disease', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('exotic orchard', '2009-02-01', 'The colors of mana are white, blue, black, red, and green. Exotic Orchard can\'t be tapped for colorless mana, even if a land an opponent controls could produce colorless mana.').
card_ruling('exotic orchard', '2009-02-01', 'Exotic Orchard checks the effects of all mana-producing abilities of lands your opponents control, but it doesn\'t check their costs. For example, Vivid Crag has the ability \"{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.\" If an opponent controls Vivid Crag and you control Exotic Orchard, you can tap Exotic Orchard for any color of mana. It doesn\'t matter whether Vivid Crag has a charge counter on it, and it doesn\'t matter whether it\'s untapped.').
card_ruling('exotic orchard', '2009-02-01', 'When determining what colors of mana your opponents\' lands could produce, Exotic Orchard takes into account any applicable replacement effects that would apply to those lands\' mana abilities (such as Contamination\'s effect, for example). If there are more than one, consider them in any possible order.').
card_ruling('exotic orchard', '2009-02-01', 'Exotic Orchard doesn\'t care about any restrictions or riders your opponents\' lands (such as Ancient Ziggurat or Hall of the Bandit Lord) put on the mana they produce. It just cares about colors of mana.').
card_ruling('exotic orchard', '2009-02-01', 'Lands that produce mana based only on what other lands \"could produce\" won\'t help each other unless some other land allows one of them to actually produce some type of mana. For example, if you control an Exotic Orchard and your opponent controls an Exotic Orchard and a Reflecting Pool, none of those lands would produce mana if their mana abilities were activated. On the other hand, if you control a Forest and an Exotic Orchard, and your opponent controls an Exotic Orchard and a Reflecting Pool, then each of those lands can be tapped to produce {G}. Your opponent\'s Exotic Orchard can produce {G} because you control a Forest. Your Exotic Orchard and your opponent\'s Reflecting Pool can each produce {G} because your opponent\'s Exotic Orchard can produce {G}.').

card_ruling('expedition map', '2009-10-01', 'You may find any land card, not just a basic land card.').

card_ruling('experiment kraj', '2006-05-01', 'Unlike with cards like Quicksilver Elemental, the costs of the activated abilities Experiment Kraj gains must be paid with the correct colors of mana.').
card_ruling('experiment kraj', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('experiment one', '2013-01-24', 'If removing two +1/+1 counters from Experiment One causes the amount of damage already marked on Experiment One to be equal to or greater than its toughness, it will be put into its owner\'s graveyard as a state-based action before the regeneration shield is created.').
card_ruling('experiment one', '2013-04-15', 'When comparing the stats of the two creatures for evolve, you always compare power to power and toughness to toughness.').
card_ruling('experiment one', '2013-04-15', 'Whenever a creature enters the battlefield under your control, check its power and toughness against the power and toughness of the creature with evolve. If neither stat of the new creature is greater, evolve won\'t trigger at all.').
card_ruling('experiment one', '2013-04-15', 'If evolve triggers, the stat comparison will happen again when the ability tries to resolve. If neither stat of the new creature is greater, the ability will do nothing. If the creature that entered the battlefield leaves the battlefield before evolve tries to resolve, use its last known power and toughness to compare the stats.').
card_ruling('experiment one', '2013-04-15', 'If a creature enters the battlefield with +1/+1 counters on it, consider those counters when determining if evolve will trigger. For example, a 1/1 creature that enters the battlefield with two +1/+1 counters on it will cause the evolve ability of a 2/2 creature to trigger.').
card_ruling('experiment one', '2013-04-15', 'If multiple creatures enter the battlefield at the same time, evolve may trigger multiple times, although the stat comparison will take place each time one of those abilities tries to resolve. For example, if you control a 2/2 creature with evolve and two 3/3 creatures enter the battlefield, evolve will trigger twice. The first ability will resolve and put a +1/+1 counter on the creature with evolve. When the second ability tries to resolve, neither the power nor the toughness of the new creature is greater than that of the creature with evolve, so that ability does nothing.').
card_ruling('experiment one', '2013-04-15', 'When comparing the stats as the evolve ability resolves, it\'s possible that the stat that\'s greater changes from power to toughness or vice versa. If this happens, the ability will still resolve and you\'ll put a +1/+1 counter on the creature with evolve. For example, if you control a 2/2 creature with evolve and a 1/3 creature enters the battlefield under your control, it toughness is greater so evolve will trigger. In response, the 1/3 creature gets +2/-2. When the evolve trigger tries to resolve, its power is greater. You\'ll put a +1/+1 counter on the creature with evolve.').

card_ruling('exploding borders', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('exploding borders', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('exploding borders', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').
card_ruling('exploding borders', '2009-02-01', 'You do what the spell says in order, so you\'ll put a new basic land card onto the battlefield before you determine the value of X.').
card_ruling('exploding borders', '2009-02-01', 'Exploding Borders will still deal damage even if you don\'t put a land card onto the battlefield.').
card_ruling('exploding borders', '2009-02-01', 'Exploding Borders has only one target: the player. If that player becomes an illegal target by the time Exploding Borders would resolve, the entire spell is countered. You won\'t get to search for a basic land card.').

card_ruling('explore', '2010-03-01', 'Explore\'s effect allows you to play an additional land during your main phase. Doing so follows the normal timing rules for playing lands. In particular, you don\'t get to play a land as Explore resolves; Explore fully resolves (and you\'ll draw a card, perhaps a land you\'ll play later) first.').
card_ruling('explore', '2010-03-01', 'The effects of multiple Explores in the same turn are cumulative. They\'re also cumulative with other effects that let you play additional lands, such as the one from Oracle of Mul Daya.').
card_ruling('explore', '2010-03-01', 'If you somehow manage to cast Explore when it\'s not your turn, you\'ll draw a card when it resolves, but you won\'t be able to play a land that turn.').

card_ruling('explosive revelation', '2010-06-15', 'If the targeted creature or player is an illegal target by the time Explosive Revelation resolves, the spell is countered. You won\'t reveal any cards from your library.').
card_ruling('explosive revelation', '2010-06-15', 'Once you start revealing cards from your library, it\'s too late for players to respond. If you target a creature, for example, its controller can\'t wait to see how much damage will be dealt to it before casting a damage prevention spell, activating a regeneration ability, sacrificing it for some benefit, or anything else. Any such actions must be done before Explosive Revelation starts to resolve.').
card_ruling('explosive revelation', '2010-06-15', 'If all the cards in your library are land cards, you\'ll reveal all of them, then arrange them in your library in whatever order you like. No damage is dealt.').

card_ruling('expunge', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('exquisite blood', '2012-05-01', 'Damage dealt to an opponent usually causes that opponent to lose life. An opponent paying life also causes loss of life.').
card_ruling('exquisite blood', '2012-05-01', 'If you and an opponent both lose life simultaneously, and this causes your life total to become 0 or less, you\'ll lose the game before Exquisite Blood\'s triggered ability can resolve.').

card_ruling('exquisite firecraft', '2015-06-22', 'Even if the spell mastery ability applies, Exquisite Firecraft can be targeted by spells or abilities that would counter it. The part of their effect that would counter Exquisite Firecraft won’t do anything, but any other effects those spells or abilities may have will still happen, if applicable.').
card_ruling('exquisite firecraft', '2015-06-22', 'Check to see if there are two or more instant and/or sorcery cards in your graveyard as the spell resolves to determine whether the spell mastery ability applies. The spell itself won’t count because it’s still on the stack as you make this check.').

card_ruling('exsanguinate', '2011-01-01', 'Players can lose more life than they have. For example, say you\'re playing a multiplayer game in which one of your opponents has 3 life and your other opponent has 10 life. If you cast Exsanguinate with X of 4, your opponents will wind up at -1 life and 6 life, respectively. You\'ll gain 8 life.').

card_ruling('extirpate', '2013-06-07', 'Players still get priority while a card with split second is on the stack.').
card_ruling('extirpate', '2013-06-07', 'Split second doesn\'t prevent players from activating mana abilities.').
card_ruling('extirpate', '2013-06-07', 'Split second doesn\'t prevent triggered abilities from triggering. If one does, its controller puts it on the stack and chooses targets for it, if any. Those abilities will resolve as normal.').
card_ruling('extirpate', '2013-06-07', 'Split second doesn\'t prevent players from performing special actions. Notably, players may turn face-down creatures face up while a spell with split second is on the stack.').
card_ruling('extirpate', '2013-06-07', 'Casting a spell with split second won\'t affect spells and abilities that are already on the stack.').
card_ruling('extirpate', '2013-06-07', 'If the resolution of a triggered ability involves casting a spell, that spell can\'t be cast if a spell with split second is on the stack.').

card_ruling('extract from darkness', '2014-05-29', 'Extract from Darkness doesn’t target any creature card. You choose which card you’re putting onto the battlefield as it resolves. You can choose any creature card in a graveyard at that time, including one just put into a graveyard by Extract from Darkness. If there are no creature cards in graveyards at that time, Extract from Darkness simply finishes resolving.').

card_ruling('extractor demon', '2009-02-01', 'If Extractor Demon and another creature leave the battlefield at the same time, Extractor Demon\'s triggered ability will trigger.').
card_ruling('extractor demon', '2009-02-01', 'You can target any player with Extractor Demon\'s triggered ability. The target doesn\'t have to be the controller of the creature that left the battlefield.').

card_ruling('extruder', '2004-10-04', 'It can choose itself as the target, but it will be countered when it tries to resolve since it will no longer be on the battlefield. This is useful only when you want to simply destroy this card.').

card_ruling('exuberant firestoker', '2008-10-01', 'The first ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless you control a creature with power 5 or greater as your end step begins, and (2) the ability will do nothing if you don\'t control a creature with power 5 or greater by the time it resolves. (It doesn\'t have to be the same creature as the one that allowed the ability to trigger.) Power-boosting effects that last \"until end of turn\" will still be in effect when this kind of ability triggers and resolves. An ability like this will trigger a maximum of once per turn, no matter how many applicable creatures you control.').

card_ruling('eye for an eye', '2009-02-01', 'The damage dealt to you is still being dealt by the original source. The damage dealt to the other player is being dealt by Eye for an Eye.').
card_ruling('eye for an eye', '2009-02-01', 'Eye for an Eye must resolve before damage would be dealt to you in order to affect that damage.').

card_ruling('eye gouge', '2014-02-01', 'If the target creature is a Cyclops, it will get -1/-1 before it’s destroyed.').

card_ruling('eye of doom', '2013-10-17', 'The active player chooses a permanent first, followed by each other player in turn order. Each player will know what previous players have chosen when making his or her own choice. Then the doom counters are put on all the chosen permanents simultaneously.').
card_ruling('eye of doom', '2013-10-17', 'None of the permanents are targeted. A player can choose a permanent with protection from artifacts, for example.').
card_ruling('eye of doom', '2013-10-17', 'If a permanent with a doom counter isn’t destroyed by Eye of Doom’s last ability (perhaps because it has indestructible or it regenerated), the doom counter will remain on that permanent.').

card_ruling('eye of ramos', '2004-10-04', 'You can activate the sacrifice ability while this card is tapped.').

card_ruling('eye of singularity', '2004-10-04', 'If a copy card enters the battlefield, the permanent it is copying is destroyed and the copy card stays on the battlefield.').
card_ruling('eye of singularity', '2004-10-04', 'Since phasing in does not cause enters-the-battlefield ability to trigger, it is possible to phase in a card which is already on the battlefield and not trigger this effect.').
card_ruling('eye of singularity', '2008-10-01', 'This has the supertype world. When a world permanent enters the battlefield, any world permanents that were already on the battlefield are put into their owners\' graveyards. This is a state-based action called the \"world rule.\" The new world permanent stays on the battlefield. If two world permanents enter the battlefield at the same time, they\'re both put into their owners\' graveyards.').

card_ruling('eye of the storm', '2005-10-01', 'The ability triggers when a player casts an instant or sorcery *card*. A copy of a card (such as those generated by Eye of the Storm itself) won\'t trigger this ability. The card that was cast is exiled and won\'t resolve, but the player will get a copy of the spell from Eye of the Storm.').
card_ruling('eye of the storm', '2005-10-01', 'When the ability resolves, the player copies all cards exiled with Eye of the Storm, not just the cards he or she owns. That player then chooses which of those copies to cast, if any. All chosen copies are cast in the order the player chooses.').
card_ruling('eye of the storm', '2005-10-01', 'The player chooses modes, pays additional costs, chooses targets, and so on for the copies when casting them. Any X in the mana cost of a copy cast this way will be 0. Alternative costs can\'t be paid.').
card_ruling('eye of the storm', '2006-01-01', 'You must still follow any restrictions on when you can cast the spell, such as \"cast only during combat\" or \"cast only on your own turn.\"').
card_ruling('eye of the storm', '2006-02-01', 'When a spell with Replicate is copied by Eye of the Storm, you have the opportunity to pay additional costs, so you may Replicate as much as you like (assuming you have the mana available).').

card_ruling('eye of ugin', '2010-03-01', 'Eye of Ugin doesn\'t have a mana ability.').
card_ruling('eye of ugin', '2010-03-01', 'Eye of Ugin\'s second ability lets you find any colorless creature card in your deck, such as an artifact creature card that has no colored mana symbols in its mana cost, or a creature card that\'s become colorless due to Mycosynth Lattice.').
card_ruling('eye of ugin', '2013-04-15', 'Mistform Ultimus and creatures with changeling are Eldrazi. If they become colorless, possibly due to Mycosynth Lattice, Eye of Ugin would reduce the cost to cast them by {2}.').

card_ruling('eyeless watcher', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('eyeless watcher', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('eyeless watcher', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('eyeless watcher', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('eyeless watcher', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('eyeless watcher', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('eyeless watcher', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('eyeless watcher', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('eyeless watcher', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('eyes in the skies', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('eyes in the skies', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('eyes in the skies', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('eyes in the skies', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('eyes in the skies', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('eyes in the skies', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('eyes of the watcher', '2004-12-01', 'You do the scrying at the end of the ability\'s resolution (assuming you pay {1}).').

card_ruling('ezuri\'s archers', '2011-01-01', 'If Ezuri\'s Archers somehow blocks multiple creatures with flying (perhaps because it\'s equipped by Echo Circlet), its ability triggers that many times.').

card_ruling('ezuri, renegade leader', '2011-01-01', 'Ezuri\'s first ability can\'t affect itself, but its second ability does.').
card_ruling('ezuri, renegade leader', '2011-01-01', 'Only Elf creatures you control when the last ability resolves will get +3/+3 and gain trample until end of turn. Non-Elf creatures that become Elves or Elf creatures that enter the battlefield under your control later in the turn won\'t get the bonuses.').

