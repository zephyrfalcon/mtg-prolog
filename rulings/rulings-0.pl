% Rulings

card_ruling('ærathi berserker', '2004-10-04', 'There is a typographical error in the title of the card so that the \"AE\" does not appear.').

card_ruling('æther adept', '2010-08-15', 'If there are no other creatures on the battlefield when Æther Adept enters the battlefield, it must target itself.').

card_ruling('æther flash', '2004-10-04', 'This is not a targeted ability, so it deals damage to creatures that can\'t be targeted by spells and abilities.').

card_ruling('æther gale', '2014-11-07', 'You must choose six different legal targets in order to cast Æther Gale. If some, but not all, of those targets become illegal before Æther Gale resolves, the remaining legal targets will be put into their owners’ hands.').

card_ruling('æther membrane', '2007-02-01', 'The blocked creature is returned to its owner\'s hand at end of combat only if the blocked creature is still on the battlefield. It doesn\'t matter whether AEther Membrane is on the battlefield.').

card_ruling('æther mutation', '2004-10-04', 'You only put the tokens onto the battlefield if the target creature is still a legal target on resolution.').

card_ruling('æther rift', '2009-10-01', 'First, the card is discarded. Then, in turn order, each player is given the option to pay 5 life. Then, if no player paid 5 life, the card is put onto the battlefield from the graveyard.').
card_ruling('æther rift', '2009-10-01', 'The card is actually discard, so anything which triggers on a discard will trigger. Such abilities will wait to go on the stack until after this ability has completely resolved.').

card_ruling('æther searcher', '2014-05-29', 'When casting a card this way, ignore timing restrictions based on the card’s type. Other timing restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('æther searcher', '2014-05-29', 'If you cast a card “without paying its mana cost,” you can’t pay alternative costs such as overload costs. You can pay additional costs such as kicker costs. If the card has mandatory additional costs, you must pay those.').
card_ruling('æther searcher', '2014-05-29', 'If the card has {X} in its mana cost, you must choose 0 as its value.').
card_ruling('æther searcher', '2014-05-29', 'If Æther Searcher leaves the battlefield before its ability resolves, you’ll still be able to search for the named card and cast it.').
card_ruling('æther searcher', '2014-05-29', 'If the card you draft after Æther Searcher is a land card, you won’t be able to play that land using Æther Searcher’s ability.').
card_ruling('æther searcher', '2014-05-29', 'If Æther Searcher is the last card you draft in any draft round other than the last one, you’ll reveal the first card you draft in the following draft round and note its name.').
card_ruling('æther searcher', '2014-05-29', 'If Æther Searcher is the last card you draft, there won’t be a noted name. You can still search your hand and/or library when it enters the battlefield, but you won’t cast any card this way unless you also drafted an Æther Searcher and noted a card then.').
card_ruling('æther searcher', '2014-05-29', 'If an Æther Searcher owned by another player enters the battlefield under your control, you can search for a card you’ve named while drafting a card named Æther Searcher, not a card named by its owner. Unless you also drafted a card named Æther Searcher, you won’t cast a card.').
card_ruling('æther searcher', '2014-05-29', 'You cast the card before shuffling your library (if it was searched). In some very rare cases, you may need to take actions involving your library while casting the card. For example, if it has an additional cost and you activate Deranged Assistant’s ability (“{T}, Put the top card of your library into your graveyard: Add {1} to your mana pool.”) to pay for it, you must maintain the order of your library while you search it.').

card_ruling('æther shockwave', '2005-06-01', 'You choose the mode on announcement.').

card_ruling('æther snap', '2004-12-01', 'AEther Snap removes all counters from permanents, no matter what kind they are.').
card_ruling('æther snap', '2004-12-01', 'Counters on objects that aren\'t permanents, such as time counters on a suspended card or poison counters on a player, are unaffected by AEther Snap.').

card_ruling('æther sting', '2009-10-01', 'The ability triggers when an opponent casts a creature spell, and goes on the stack above that spell. As such, the ability will deal 1 damage to that player before the creature enters the battlefield.').

card_ruling('æther storm', '2004-10-04', 'This does not stop a creature card from being put directly onto the battlefield by a spell or ability.').
card_ruling('æther storm', '2008-08-01', 'Affects any spell with the type creature, including those with other types such as artifact or enchantment. This includes older cards with \"summon\" on their type line.').

card_ruling('æther web', '2006-09-25', 'The enchanted creature can block creatures with shadow and creatures without shadow.').
card_ruling('æther web', '2006-09-25', 'If the enchanted creature has shadow, it won\'t be able to block any creatures (not even those with shadow).').

card_ruling('ætherflame wall', '2006-09-25', 'AEtherflame Wall can block creatures with shadow and creatures without shadow.').
card_ruling('ætherflame wall', '2006-09-25', 'If AEtherflame Wall gains shadow, it won\'t be able to block any creatures (not even those with shadow).').

card_ruling('ætherize', '2013-01-24', 'An “attacking creature” is one that has been declared as an attacker this combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left combat. There\'s no such thing as an attacking creature outside of the combat phase.').

card_ruling('ætherling', '2013-04-15', 'Ætherling\'s first ability will return it to the battlefield only if that ability also exiled it. If Ætherling left the battlefield in response to that ability, it won\'t return, even if it was exiled by another spell or ability.').
card_ruling('ætherling', '2013-04-15', 'Ætherling\'s last ability can be activated even if its power is 0 or less. If a creature would assign 0 or less damage in combat, it doesn\'t assign combat damage at all.').

card_ruling('æthermage\'s touch', '2006-05-01', 'If you reveal more than one creature card, you may put up to one of them onto the battlefield, and the rest of the revealed cards go on the bottom of your library. If you don\'t reveal any creature cards, all the revealed cards go on the bottom of your library.').
card_ruling('æthermage\'s touch', '2006-05-01', 'If the creature leaves the battlefield before the end of your turn, it won\'t return to your hand at the end of your turn.').
card_ruling('æthermage\'s touch', '2006-05-01', 'If you put a creature onto the battlefield this way during your end-of-turn step, it won\'t be returned to your hand until the end of your next turn.').
card_ruling('æthermage\'s touch', '2006-05-01', 'If the triggered ability granted to the creature is countered (with Voidslime, for example), the creature will stay on the battlefield. At the end of your next turn, the ability will trigger again.').
card_ruling('æthermage\'s touch', '2006-05-01', 'If a creature such as Clone copies the creature, it will not copy the triggered ability.').

card_ruling('ætherplasm', '2006-02-01', 'AEtherplasm will return to your hand before any combat damage is assigned.').
card_ruling('ætherplasm', '2006-02-01', 'If you want, you can return AEtherplasm to your hand and not put anything onto the battlefield. The attacking creature will still be considered blocked, so it won\'t be able to deal combat damage to you unless it has trample or a similar ability.').
card_ruling('ætherplasm', '2006-02-01', 'The creature you put onto the battlefield may be the AEtherplasm you just returned to your hand. This allows it to dodge abilities like Deathgazer\'s.').
card_ruling('ætherplasm', '2006-02-01', 'The creature you put onto the battlefield from your hand is blocking the attacking creature, even if the block couldn\'t legally be declared (for example, if that creature enters the battlefield tapped, or it can\'t block, or the attacking creature has protection from it).').

card_ruling('æthersnipe', '2013-04-15', 'If you cast this card for its evoke cost, you may put the sacrifice trigger and the regular enters-the-battlefield trigger on the stack in either order. The one put on the stack last will resolve first.').

card_ruling('ætherspouts', '2014-07-18', 'The owner of each attacking creature chooses whether to put it on the top or bottom of his or her library. The active player (the player whose turn it is) makes all of his or her choices first, followed by each other player in turn order.').
card_ruling('ætherspouts', '2014-07-18', 'If an effect puts two or more cards on the top or bottom of a library at the same time, the owner of those cards may arrange them in any order. That library’s owner doesn’t reveal the order in which the cards go into his or her library.').

