% Rulings

card_ruling('vagrant plowbeasts', '2009-02-01', 'This ability checks the targeted creature\'s power only at the time the ability is activated and at the time it resolves. The regeneration shield it creates will work no matter what that creature\'s power is at the time it would be destroyed.').

card_ruling('valakut fireboar', '2013-04-15', 'Effects that switch power and toughness apply after all other effects that change power and/or toughness, regardless of which effect was created first.').
card_ruling('valakut fireboar', '2013-04-15', 'Switching a creature\'s power and toughness twice (or any even number of times) effectively returns the creature to the power and toughness it had before any switches.').

card_ruling('valakut predator', '2015-08-25', 'A landfall ability triggers whenever a land enters the battlefield under your control for any reason. It triggers whenever you play a land, as well as whenever a spell or ability puts a land onto the battlefield under your control.').
card_ruling('valakut predator', '2015-08-25', 'When a land enters the battlefield under your control, each landfall ability of the permanents you control will trigger. You can put them on the stack in any order. The last ability you put on the stack will be the first one to resolve.').
card_ruling('valakut predator', '2015-08-25', 'If the ability has an additional or replacement effect that depends on the land having a certain basic land type, the ability will check that land’s type as the ability resolves. If, at that time, the land that entered the battlefield is no longer on the battlefield, use its types when it left the battlefield to determine what happens.').

card_ruling('valakut, the molten pinnacle', '2009-10-01', 'Valakut, the Molten Pinnacle\'s triggered ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless, at the time a Mountain enters the battlefield under your control, you control five or more Mountains other than that new one, and (2) the ability will do nothing if you control fewer than five Mountains other than that new one by the time it resolves.').
card_ruling('valakut, the molten pinnacle', '2009-10-01', 'If a Mountain you control leaves the battlefield between the time Valakut\'s second ability triggers and the time it resolves, be aware if that was the Mountain that caused Valakut\'s ability to trigger or not. If it was, Valakut\'s count isn\'t affected; if it wasn\'t, Valakut\'s count goes down by one.').
card_ruling('valakut, the molten pinnacle', '2009-10-01', 'If multiple Mountains enter the battlefield under your control at the same time, Valakut\'s second ability could trigger that many times. Each ability takes into consideration the other Mountains that entered the battlefield at the same time as the one that caused it to trigger.').
card_ruling('valakut, the molten pinnacle', '2009-10-01', 'You target a creature or player when the ability triggers. You decide whether to have Valakut deal damage to that creature or player as the ability resolves.').

card_ruling('valeron wardens', '2015-06-22', 'Renown won’t trigger when a creature deals combat damage to a planeswalker or another creature. It also won’t trigger when a creature deals noncombat damage to a player.').
card_ruling('valeron wardens', '2015-06-22', 'If a creature with renown deals combat damage to its controller because that damage was redirected, renown will trigger.').
card_ruling('valeron wardens', '2015-06-22', 'If a renown ability triggers, but the creature leaves the battlefield before that ability resolves, the creature doesn’t become renowned. Any ability that triggers “whenever a creature becomes renowned” won’t trigger.').

card_ruling('valley dasher', '2014-09-20', 'You still choose which player or planeswalker Valley Dasher attacks.').
card_ruling('valley dasher', '2014-09-20', 'If, during your declare attackers step, Valley Dasher is tapped or is affected by a spell or ability that says it can’t attack, then it doesn’t attack. If there’s a cost associated with having Valley Dasher attack, you’re not forced to pay that cost, so it doesn’t have to attack in that case either.').
card_ruling('valley dasher', '2014-09-20', 'If Valley Dasher enters the battlefield before the combat phase, it will attack that turn if able. If it enters the battlefield after combat, it won’t attack that turn and will usually be available to block on the following turn.').

card_ruling('valley rannet', '2009-05-01', 'Unlike the normal cycling ability, landcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a land card of the specified land type, reveal it, put it into your hand, then shuffle your library.').
card_ruling('valley rannet', '2009-05-01', 'Landcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on a card being landcycled. Any ability that stops a cycling ability from being activated also stops a landcycling ability from being activated.').
card_ruling('valley rannet', '2009-05-01', 'You can choose to find any card with the appropriate land type, including nonbasic lands. You can also choose not to find a card, even if there is a land card with the appropriate type in your library.').
card_ruling('valley rannet', '2009-05-01', 'A landcycling ability lets you search for any card in your library with the stated land type. It doesn\'t have to be a basic land.').
card_ruling('valley rannet', '2009-05-01', 'You may only activate one landcycling ability at a time. You must specify which landcycling ability you are activating as you cycle this card, not as the ability resolves.').

card_ruling('valleymaker', '2009-10-01', 'The second ability is a mana ability. It doesn\'t target a player and it doesn\'t use the stack. If the player who adds {G}{G}{G} to his or her mana pool can\'t spend all of it before the phase ends, the remaining mana will leave his or her mana pool at the end of the current step (or phase).').

card_ruling('vampire aristocrat', '2009-10-01', 'You can sacrifice Vampire Aristocrat to activate its own ability, but it won\'t be around to get the bonus.').
card_ruling('vampire aristocrat', '2009-10-01', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that creature has been dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('vampire hexmage', '2009-10-01', 'Any permanent can be targeted by the second ability, not just one with counters on it.').
card_ruling('vampire hexmage', '2009-10-01', 'If all loyalty counters are removed from a planeswalker, that planeswalker is put into its owner\'s graveyard as a state-based action.').

card_ruling('vampire lacerator', '2009-10-01', 'Whether an opponent has 10 or less life is checked only as the ability resolves.').

card_ruling('vampire nocturnus', '2009-10-01', 'To work as an evasion ability, an attacking creature must already have flying when the declare blockers step begins. Once a creature has become blocked, giving it flying won\'t change that.').
card_ruling('vampire nocturnus', '2013-04-15', 'The top card of your library isn\'t in your hand, so you can\'t suspend it, cycle it, discard it, or activate any of its activated abilities.').
card_ruling('vampire nocturnus', '2013-04-15', 'If the top card of your library changes while you\'re casting a spell or activating an ability, the new top card won\'t be revealed until you finish casting that spell or activating that ability.').
card_ruling('vampire nocturnus', '2013-04-15', 'When playing with the top card of your library revealed, if an effect tells you to draw several cards, reveal each one before you draw it.').
card_ruling('vampire nocturnus', '2013-07-01', 'If the top card of your library is black, Vampire Nocturnus will give itself +2/+1 and flying even if it\'s somehow not a Vampire.').

card_ruling('vampire warlord', '2013-07-01', 'Activating Vampire Warlord’s ability causes a “regeneration shield” to be created for it. The next time that creature would be destroyed that turn, the regeneration shield is used up instead. This works only if the creature is dealt lethal damage, dealt damage from a source with deathtouch, or affected by a spell or ability that says to “destroy” it. Other effects that cause the creature to be put into the graveyard (such as reducing its toughness to 0 or sacrificing it) don’t destroy it, so regeneration won’t save it. If the regeneration shield isn’t used, it goes away as the turn ends.').
card_ruling('vampire warlord', '2013-07-01', 'To work, the regeneration shield must be created before Vampire Warlord is destroyed. This usually means activating its ability during the declare blockers step, or in response to a spell or ability that would destroy it.').
card_ruling('vampire warlord', '2013-07-01', 'If Vampire Warlord is dealt lethal damage and is dealt damage by a source with deathtouch during the same combat damage step, a single regeneration shield will save it.').

card_ruling('vampiric fury', '2011-09-22', 'Only Vampires you control when Vampiric Fury resolves will receive the bonus. Creatures that enter the battlefield, that you gain control of later in the turn, or that become Vampires later in the turn won\'t be affected.').

card_ruling('vampiric link', '2004-10-04', 'Damage that is redirected is not dealt to the original creature or player, but it is dealt to the new creature or player.').
card_ruling('vampiric link', '2007-02-01', 'This is the timeshifted version of Spirit Link.').
card_ruling('vampiric link', '2008-04-01', 'You gain life for all damage which is not prevented, regardless of what the creature damages (player or another creature) or the toughness of the blocking creature. If you put this on a 5/5 and it is blocked by a 1/1, you still gain 5 life.').

card_ruling('vampiric sliver', '2006-09-25', 'If a Sliver has this ability when it deals damage, but not when the damaged creature is put into a graveyard, nothing happens.').
card_ruling('vampiric sliver', '2006-09-25', 'If a Sliver doesn\'t have this ability when it deals damage, but does have it when the damaged creature is put into a graveyard, the ability will trigger.').
card_ruling('vampiric sliver', '2006-09-25', 'If Vampiric Sliver is put into a graveyard from the battlefield at the same time as a creature dealt damage by another Sliver, the Vampiric Sliver\'s ability will trigger for the other Sliver. That Sliver will get a +1/+1 counter.').
card_ruling('vampiric sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('vampiric sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('vandalblast', '2013-04-15', 'If you don\'t pay the overload cost of a spell, that spell will have a single target. If you pay the overload cost, the spell won\'t have any targets.').
card_ruling('vandalblast', '2013-04-15', 'Because a spell with overload doesn\'t target when its overload cost is paid, it may affect permanents with hexproof or with protection from the appropriate color.').
card_ruling('vandalblast', '2013-04-15', 'Note that if the spell with overload is dealing damage, protection from that spell\'s color will still prevent that damage.').
card_ruling('vandalblast', '2013-04-15', 'Overload doesn\'t change when you can cast the spell.').
card_ruling('vandalblast', '2013-04-15', 'Casting a spell with overload doesn\'t change that spell\'s mana cost. You just pay the overload cost instead.').
card_ruling('vandalblast', '2013-04-15', 'Effects that cause you to pay more or less for a spell will cause you to pay that much more or less while casting it for its overload cost, too.').
card_ruling('vandalblast', '2013-04-15', 'If you are instructed to cast a spell with overload “without paying its mana cost,” you can\'t choose to pay its overload cost instead.').

card_ruling('vanguard of brimaz', '2013-09-15', 'Heroic abilities will resolve before the spell that caused them to trigger.').
card_ruling('vanguard of brimaz', '2013-09-15', 'Heroic abilities will trigger only once per spell, even if that spell targets the creature with the heroic ability multiple times.').
card_ruling('vanguard of brimaz', '2013-09-15', 'Heroic abilities won’t trigger when a copy of a spell is created on the stack or when a spell’s targets are changed to include a creature with a heroic ability.').

card_ruling('vanguard\'s shield', '2012-05-01', 'Abilities that allow a creature to block an additional creature are cumulative.').

card_ruling('vanish into memory', '2006-07-15', 'Vanish into Memory cares about the creature\'s power just before it left the battlefield and its toughness just after it returns to the battlefield. For example, if you target a Vigean Hydropon with two +1/+1 counters on it, you\'ll draw two cards, then when the Hydropon returns to the battlefield with five +1/+1 counters on it, you\'ll discard five cards.').
card_ruling('vanish into memory', '2006-07-15', 'If the creature exiled with Vanish into Memory never returns to the battlefield (because it was a token creature, for example), you don\'t discard any cards').
card_ruling('vanish into memory', '2006-07-15', 'If the creature exiled with Vanish into Memory isn\'t a creature card (for example, it was an animated Gruul War Plow), that card will still be returned to the battlefield, but not as a creature. Since the card returned to the battlefield has no toughness, you discard no cards.').

card_ruling('vanquish the foul', '2013-09-15', 'When you scry, you may put all the cards you look at back on top of your library, you may put all of those cards on the bottom of your library, or you may put some of those cards on top and the rest of them on the bottom.').
card_ruling('vanquish the foul', '2013-09-15', 'You choose how to order cards returned to your library after scrying no matter where you put them.').
card_ruling('vanquish the foul', '2013-09-15', 'You perform the actions stated on a card in sequence. For some spells and abilities, that means you’ll scry last. For others, that means you’ll scry and then perform other actions.').
card_ruling('vanquish the foul', '2013-09-15', 'Scry appears on some spells and abilities with one or more targets. If all of the spell or ability’s targets are illegal when it tries to resolve, it will be countered and none of its effects will happen. You won’t scry.').

card_ruling('varchild\'s war-riders', '2004-10-04', 'In a multiplayer game, you can put the Survivor tokens onto the battlefield under the control of different players.').
card_ruling('varchild\'s war-riders', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('varolz, the scar-striped', '2013-04-15', 'Exiling the creature card with scavenge is part of the cost of activating the scavenge ability. Once the ability is activated and the cost is paid, it\'s too late to stop the ability from being activated by trying to remove the creature card from the graveyard.').
card_ruling('varolz, the scar-striped', '2013-04-15', 'While Varolz, the Scar-Striped is on the battlefield, the scavenge cost of a creature card in your graveyard is equal to its mana cost, including any colored mana requirements.').
card_ruling('varolz, the scar-striped', '2013-04-15', 'Abilities that reduce the cost to cast a creature spell or alternative costs you can pay rather than a card\'s mana cost don\'t apply to activating a card\'s scavenge ability.').
card_ruling('varolz, the scar-striped', '2013-04-15', 'The number of counters that a card\'s scavenge ability puts on a creature is based on the card\'s power as it last existed in the graveyard.').
card_ruling('varolz, the scar-striped', '2013-04-15', 'If the creature card you scavenge has {X} in its mana cost, X is 0.').
card_ruling('varolz, the scar-striped', '2013-04-15', 'If a creature card has multiple instances of scavenge, you can activate either ability (but not both, as the card will be exiled when you activate one of them). Varolz\'s ability doesn\'t affect the scavenge cost of a creature\'s other scavenge ability.').

card_ruling('vastwood animist', '2010-03-01', 'A land that becomes a creature may be affected by “summoning sickness.” You can’t attack with it or use any of its {T} abilities (including its mana abilities) unless it began your most recent turn on the battlefield under your control. Note that summoning sickness cares about when that permanent came under your control, not when it became a creature.').
card_ruling('vastwood animist', '2010-03-01', 'When a land becomes a creature, that doesn’t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('vastwood animist', '2010-03-01', 'The value of X is determined as Vastwood Animist\'s activated ability resolves. The power and toughness of the Elemental won\'t change if the number of Allies you control changes later in the turn.').
card_ruling('vastwood animist', '2010-03-01', 'If you control no Allies when the ability resolves, the land becomes a 0/0 creature and is put into its owner\'s graveyard as a state-based action. However, since Vastwood Animist is itself an Ally, the land will usually be at least a 1/1.').

card_ruling('vastwood hydra', '2013-07-01', 'The ability that triggers when Vastwood Hydra dies doesn’t target any of the creatures. You can choose to put counters on a creature with protection from green, for example.').
card_ruling('vastwood hydra', '2013-07-01', 'You choose how the counters will be distributed when the ability resolves.').
card_ruling('vastwood hydra', '2013-07-01', 'In some unusual cases, Vastwood Hydra will have enough -1/-1 counters put on it to make its toughness 0 or less. If this happens, the ability will still distribute a number of +1/+1 counters equal to the number of +1/+1 counters on Vastwood Hydra before the -1/-1 counters were put on it. For example, if Vastwood Hydra has two +1/+1 counters on it and three -1/-1 counters are put on it, two +1/+1 counters will be distributed.').

card_ruling('vastwood zendikon', '2010-03-01', 'The enchanted permanent will be both a land and a creature and can be affected by anything that affects either a land or a creature.').
card_ruling('vastwood zendikon', '2010-03-01', 'When a land becomes a creature, that doesn\'t count as having a creature enter the battlefield. The permanent was already on the battlefield; it only changed its types. Abilities that trigger whenever a creature enters the battlefield won\'t trigger.').
card_ruling('vastwood zendikon', '2010-03-01', 'An attacking or blocking creature that stops being a creature is removed from combat. This can happen if a Zendikon enchanting an attacking or blocking creature leaves the battlefield, for example. The permanent that was removed from combat neither deals nor is dealt combat damage. Any attacking creature that the land creature was blocking remains blocked, however.').
card_ruling('vastwood zendikon', '2010-03-01', 'An ability that turns a land into a creature also sets that creature\'s power and toughness. If the land was already a creature, this will overwrite the previous effect that set its power and toughness. Effects that modify its power or toughness, such as the effects of Disfigure or Glorious Anthem, will continue to apply, no matter when they started to take effect. The same is true for counters that change its power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('vastwood zendikon', '2010-03-01', 'If a Zendikon and the land it\'s enchanting are destroyed at the same time (due to Akroma\'s Vengeance, for example), the Zendikon\'s last ability will still trigger.').
card_ruling('vastwood zendikon', '2010-03-01', 'If a Zendikon\'s last ability triggers, but the land card it refers to leaves the graveyard before it resolves, it will resolve but do nothing.').

card_ruling('vault of the archangel', '2011-01-22', 'Vault of the Archangel\'s last ability affects only creatures you control when that ability resolves. It won\'t affect creatures that come under your control later in the turn.').
card_ruling('vault of the archangel', '2011-01-22', 'Multiple instances of deathtouch or lifelink on the same creature are redundant.').

card_ruling('vault skirge', '2011-06-01', 'A card with Phyrexian mana symbols in its mana cost is each color that appears in that mana cost, regardless of how that cost may have been paid.').
card_ruling('vault skirge', '2011-06-01', 'To calculate the converted mana cost of a card with Phyrexian mana symbols in its cost, count each Phyrexian mana symbol as 1.').
card_ruling('vault skirge', '2011-06-01', 'As you cast a spell or activate an activated ability with one or more Phyrexian mana symbols in its cost, you choose how to pay for each Phyrexian mana symbol at the same time you would choose modes or choose a value for X.').
card_ruling('vault skirge', '2011-06-01', 'If you\'re at 1 life or less, you can\'t pay 2 life.').
card_ruling('vault skirge', '2011-06-01', 'Phyrexian mana is not a new color. Players can\'t add Phyrexian mana to their mana pools.').

card_ruling('vault skyward', '2011-01-01', 'Vault Skyward can target any creature, including one that already has flying or is already untapped.').
card_ruling('vault skyward', '2011-01-01', 'For flying to work as an evasion ability, Vault Skyward must be cast before the declare blockers step begins. Once a creature has become blocked, giving it flying won\'t change that.').

card_ruling('vaultbreaker', '2014-11-24', 'If you choose to pay the dash cost rather than the mana cost, you’re still casting the spell. It goes on the stack and can be responded to and countered. You can cast a creature spell for its dash cost only when you otherwise could cast that creature spell. Most of the time, this means during your main phase when the stack is empty.').
card_ruling('vaultbreaker', '2014-11-24', 'If you pay the dash cost to cast a creature spell, that card will be returned to its owner’s hand only if it’s still on the battlefield when its triggered ability resolves. If it dies or goes to another zone before then, it will stay where it is.').
card_ruling('vaultbreaker', '2014-11-24', 'You don’t have to attack with the creature with dash unless another ability says you do.').
card_ruling('vaultbreaker', '2014-11-24', 'If a creature enters the battlefield as a copy of or becomes a copy of a creature whose dash cost was paid, the copy won’t have haste and won’t be returned to its owner’s hand.').

card_ruling('vebulid', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('vectis silencers', '2008-10-01', 'The only difference between a colored artifact and a colorless artifact is, obviously, its color. Unlike most artifacts, a colored artifact requires colored mana to cast. Also unlike most artifacts, a colored artifact has a color in all zones. It will interact with cards that care about color. Other than that, a colored artifact behaves just like any other artifact. It will interact as normal with any card that cares about artifacts, such as Shatter or Arcbound Ravager.').
card_ruling('vectis silencers', '2009-10-01', 'If you activate the ability multiple times during the same turn, Vectis Silencers will gain multiple instances of deathtouch; however, multiple instances of deathtouch are redundant. A creature dealt damage by a source with deathtouch is destroyed as a state-based action the next time they are checked, so a single regeneration effect will save it regardless of how many instances of deathtouch the source had at the time damage was dealt.').

card_ruling('vector asp', '2011-01-01', 'A player who has ten or more poison counters loses the game. This is a state-based action.').
card_ruling('vector asp', '2011-01-01', 'Infect\'s effect applies to any damage, not just combat damage.').
card_ruling('vector asp', '2011-01-01', 'The -1/-1 counters remain on the creature indefinitely. They\'re not removed if the creature regenerates or the turn ends.').
card_ruling('vector asp', '2011-01-01', 'Damage from a source with infect is damage in all respects. If the source with infect also has lifelink, damage dealt by that source also causes its controller to gain that much life. Damage from a source with infect can be prevented or redirected. Abilities that trigger on damage being dealt will trigger if a source with infect deals damage, if appropriate.').
card_ruling('vector asp', '2011-01-01', 'If damage from a source with infect that would be dealt to a player is prevented, that player doesn\'t get poison counters. If damage from a source with infect that would be dealt to a creature is prevented, that creature doesn\'t get -1/-1 counters.').
card_ruling('vector asp', '2011-01-01', 'Damage from a source with infect affects planeswalkers normally.').

card_ruling('vedalken anatomist', '2011-06-01', 'If the creature is an illegal target when the ability tries to resolve, it will be countered. None of its effects happen.').

card_ruling('vedalken dismisser', '2013-06-07', 'If Vedalken Dismisser is the only creature on the battlefield when its ability triggers, you\'ll have to choose it as the target.').

card_ruling('vedalken heretic', '2009-05-01', 'Vedalken Heretic\'s ability triggers when it deals any damage to an opponent, not just combat damage.').

card_ruling('vedalken infuser', '2011-06-01', 'You may target and put a charge counter on any artifact, even if it doesn\'t have an ability that references charge counters.').

card_ruling('vedalken orrery', '2004-12-01', 'The controller of Vedalken Orrery can cast nonland artifact, creature, enchantment, and sorcery cards any time he or she could cast an instant. This includes on other players\' turns and during the upkeep step, draw step, combat phase, and end step.').
card_ruling('vedalken orrery', '2010-06-15', 'This applies only to casting spells. It does not, for example, change when you may activate abilities that can only be activated \"any time you could cast a sorcery\".').

card_ruling('vedalken plotter', '2006-02-01', 'If either land becomes an illegal target (or leaves the battlefield) before the ability resolves, the exchange won\'t happen. There\'s no way for you to gain a land without losing another one in the process.').

card_ruling('vedalken shackles', '2004-12-01', 'The creature\'s power and the number of Islands you control matter only when the ability is activated and when it resolves. After that, only the fact that Vedalken Shackles remains tapped matters. If the creature stops being a creature, you\'ll keep control of it.').
card_ruling('vedalken shackles', '2004-12-01', 'If Vedalken Shackles becomes untapped before its ability resolves, you won\'t gain control of the creature.').
card_ruling('vedalken shackles', '2004-12-01', 'If Vedalken Shackles leaves the battlefield, it\'s no longer tapped, so the control-changing effect ends.').

card_ruling('vedalken æthermage', '2009-02-01', 'Unlike the normal cycling ability, Wizardcycling doesn\'t allow you to draw a card. Instead, it lets you search your library for a Wizard card. After you find a Wizard card in your library, you reveal it, put it into your hand, then shuffle your library.').
card_ruling('vedalken æthermage', '2009-02-01', 'Wizardcycling is a form of cycling. Any ability that triggers on a card being cycled also triggers on Wizardcycling this card. Any ability that stops a cycling ability from being activated also stops Wizardcycling from being activated.').
card_ruling('vedalken æthermage', '2009-02-01', 'Wizardcycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with Wizardcycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').
card_ruling('vedalken æthermage', '2009-02-01', 'You can choose to find any card with the Wizard creature type, even if it isn\'t a creature card. This includes, for example, Tribal cards with the Changeling ability. You can also choose not to find a card, even if there is a Wizard card in your library.').

card_ruling('veil of birds', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('veil of birds', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('veil of birds', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('veil of birds', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('veil of secrecy', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('veil of secrecy', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('veil of secrecy', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('veil of secrecy', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('veilborn ghoul', '2012-07-01', 'Veilborn Ghoul\'s ability triggers only if it\'s in your graveyard at the moment the Swamp enters the battlefield.').

card_ruling('veiled apparition', '2004-10-04', 'You choose whether to pay or not on resolution. If not, then you sacrifice the creature. You can choose to not pay if you do not control the creature on resolution.').
card_ruling('veiled apparition', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('veiled apparition', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('veiled apparition', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('veiled apparition', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('veiled crocodile', '2004-10-04', 'It can trigger even if a player\'s hand is empty momentarily during the middle of the resolution of a spell or ability.').
card_ruling('veiled crocodile', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('veiled crocodile', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('veiled sentry', '2004-10-04', 'If the spell is no longer on the stack when the triggered ability resolves, the converted mana cost of the spell is still remembered.').
card_ruling('veiled sentry', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('veiled sentry', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('veiled sentry', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').

card_ruling('veiled serpent', '2004-10-04', 'When it turns into a creature, it is no longer an enchantment.').
card_ruling('veiled serpent', '2004-10-04', 'It changes into a creature even if the spell is countered.').
card_ruling('veiled serpent', '2004-10-04', 'It triggers when the spell is cast, which means it becomes a creature before that spell resolves.').
card_ruling('veiled serpent', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').
card_ruling('veiled serpent', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('veiling oddity', '2007-02-01', 'If Veiling Oddity is suspended, then when the last time counter is removed from it, both its triggered ability and the \"cast this card\" part of the suspend ability will trigger. They can be put on the stack in either order.').
card_ruling('veiling oddity', '2007-02-01', 'Regardless of which order those abilities resolve in, Veiling Oddity can\'t be blocked that turn. The ability affects all creatures, not just the creatures that were on the battlefield when it resolved.').
card_ruling('veiling oddity', '2007-02-01', 'If Veiling Oddity is suspended and the last time counter is removed during another player\'s turn, that player\'s creatures can\'t be blocked that turn, too.').
card_ruling('veiling oddity', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('veiling oddity', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('veiling oddity', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('veiling oddity', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('veiling oddity', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('veiling oddity', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('veiling oddity', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('veiling oddity', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('veiling oddity', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('veiling oddity', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('vein drinker', '2008-10-01', 'When the first ability resolves, if the targeted creature is no longer on the battlefield, the ability is countered for having no legal targets. No damage is dealt.').
card_ruling('vein drinker', '2008-10-01', 'When the first ability resolves, if the targeted creature is still on the battlefield but Vein Drinker is not, Vein Drinker will still deal damage equal to its power as it last existed on the battlefield to the targeted creature.').
card_ruling('vein drinker', '2008-10-01', 'Each time a creature is put into a graveyard from the battlefield, check whether Vein Drinker had dealt damage to it at any time during that turn. If so, Vein Drinker\'s third ability will trigger. It doesn\'t matter whether the damage was combat damage or not. It also doesn\'t matter who controlled the creature or whose graveyard it was put into.').
card_ruling('vein drinker', '2008-10-01', 'If Vein Drinker\'s first ability causes Vein Drinker and the targeted creature to deal lethal damage to each other, they\'ll be destroyed at the same time. Vein Drinker\'s triggered ability will trigger, but it will have been put into a graveyard before it would receive any counters. The ability will do nothing when it resolves.').

card_ruling('veinfire borderpost', '2009-05-01', 'Casting this card by paying its alternative cost doesn\'t change when you can cast it. You can cast it only at the normal time you could cast an artifact spell. It also doesn\'t change the spell\'s mana cost or converted mana cost. The only difference is the cost you actually pay.').
card_ruling('veinfire borderpost', '2009-05-01', 'Effects that increase or reduce the cost to cast this card will apply to whichever cost you chose to pay.').
card_ruling('veinfire borderpost', '2009-05-01', 'To satisfy the alternative cost, you may return any basic land you control to its owner\'s hand, regardless of that land\'s subtype or whether it\'s tapped.').
card_ruling('veinfire borderpost', '2009-05-01', 'As you cast a spell, you get a chance to activate mana abilities before you pay that spell\'s costs. Therefore, you may tap a basic land for mana, then both spend that mana and return that land to your hand to pay this card\'s alternative cost. (Of course, you can return a different basic land instead.)').

card_ruling('vela the night-clad', '2012-06-01', 'If Vela leaves the battlefield at the same time as other creatures you control, its ability will trigger for each of those creatures.').

card_ruling('velis vel', '2009-10-01', 'A plane card is treated as if its text box included \"When you roll {PW}, put this card on the bottom of its owner\'s planar deck face down, then move the top card of your planar deck off that planar deck and turn it face up.\" This is called the \"planeswalking ability.\"').
card_ruling('velis vel', '2009-10-01', 'A face-up plane card that\'s turned face down becomes a new object with no relation to its previous existence. In particular, it loses all counters it may have had.').
card_ruling('velis vel', '2009-10-01', 'The controller of a face-up plane card is the player designated as the \"planar controller.\" Normally, the planar controller is whoever the active player is. However, if the current planar controller would leave the game, instead the next player in turn order that wouldn\'t leave the game becomes the planar controller, then the old planar controller leaves the game. The new planar controller retains that designation until he or she leaves the game or a different player becomes the active player, whichever comes first.').
card_ruling('velis vel', '2009-10-01', 'If an ability of a plane refers to \"you,\" it\'s referring to whoever the plane\'s controller is at the time, not to the player that started the game with that plane card in his or her deck. Many abilities of plane cards affect all players, while many others affect only the planar controller, so read each ability carefully.').
card_ruling('velis vel', '2009-10-01', 'Sharing multiple creature types doesn\'t give an additional bonus. Velis Vel\'s first ability counts creatures, not creature types.').
card_ruling('velis vel', '2009-10-01', 'The {C} ability doesn\'t give a creature changeling.').

card_ruling('venarian glimmer', '2014-02-01', 'If you target yourself with this spell, you must reveal your entire hand to the other players just as any other player would.').

card_ruling('venarian gold', '2011-06-01', 'The counters are placed on the creature to which it enters the battlefield attached. If you later move Venarian Gold to a different creature, the counters remain on the original creature and no counters are put on the new creature.').

card_ruling('vendetta', '2010-06-15', 'If the targeted creature is an illegal target by the time Vendetta resolves, the spell is countered. You won\'t lose life.').
card_ruling('vendetta', '2010-06-15', 'The destroyed creature\'s last existence on the battlefield is checked to determine its toughness.').
card_ruling('vendetta', '2010-06-15', 'If the targeted creature isn\'t destroyed due to totem armor\'s effect, Vendetta continues to resolve. You lose life equal to the creature\'s current toughness (after the Aura with totem armor has been destroyed).').

card_ruling('venerated teacher', '2010-06-15', 'The abilities a leveler grants to itself don\'t overwrite any other abilities it may have. In particular, they don\'t overwrite the creature\'s level up ability; it always has that.').
card_ruling('venerated teacher', '2010-06-15', 'Effects that set a leveler\'s power or toughness to a specific value, including the effects from a level symbol\'s ability, apply in timestamp order. The timestamp of each level symbol\'s ability is the same as the timestamp of the leveler itself, regardless of when the most recent level counter was put on it.').
card_ruling('venerated teacher', '2010-06-15', 'Effects that modify a leveler\'s power or toughness, such as the effects of Giant Growth or Glorious Anthem, will apply to it no matter when they started to take effect. The same is true for counters that change the creature\'s power or toughness (such as +1/+1 counters) and effects that switch its power and toughness.').
card_ruling('venerated teacher', '2010-06-15', 'If another creature becomes a copy of a leveler, all of the leveler\'s printed abilities -- including those represented by level symbols -- are copied. The current characteristics of the leveler, and the number of level counters on it, are not. The abilities, power, and toughness of the copy will be determined based on how many level counters are on the copy.').
card_ruling('venerated teacher', '2010-06-15', 'A creature\'s level is based on how many level counters it has on it, not how many times its level up ability has been activated or has resolved. If a leveler gets level counters due to some other effect (such as Clockspinning) or loses level counters for some reason (such as Vampire Hexmage), its level is changed accordingly.').

card_ruling('vengeful archon', '2010-08-15', 'The only target of Vengeful Archon\'s ability is the player it may deal damage to. You choose that target as you activate the ability, not at the time it prevents damage.').
card_ruling('vengeful archon', '2010-08-15', 'If the targeted player is an illegal target by the time the ability resolves, the entire ability is countered. No damage will be prevented.').
card_ruling('vengeful archon', '2010-08-15', 'Vengeful Archon\'s ability doesn\'t have you choose a source of damage. It will apply to the next X damage you would be dealt that turn, regardless of where that damage comes from. It doesn\'t matter whether the damage would all be dealt at the same time. For example, if X is 4 and you\'d be dealt 3 damage by Lightning Bolt, that 3 damage is prevented and Vengeful Archon deals 3 damage to the targeted player. The prevention effect will still apply to the next 1 damage you\'d be dealt that turn.').
card_ruling('vengeful archon', '2010-08-15', 'The effect of Vengeful Archon\'s ability is not a redirection effect. If it prevents damage, Vengeful Archon (not the source of that damage) deals damage to the targeted player as part of that prevention effect. Vengeful Archon is the source of the new damage, so the characteristics of the original source (such as its color, or whether it had lifelink or deathtouch) don\'t affect this damage. The new damage is not combat damage, even if the prevented damage was. Since you control the source of the new damage, if you targeted an opponent with Vengeful Archon\'s ability, you may have Vengeful Archon deal its damage to a planeswalker that opponent controls.').
card_ruling('vengeful archon', '2010-08-15', 'Whether the targeted player is still a legal target is no longer checked after Vengeful Archon\'s ability resolves. For example, if a player targeted by Vengeful Archon\'s ability puts Leyline of Sanctity (which says \"You can\'t be the target of spells or abilities your opponents control\") onto the battlefield after the ability resolves but before it prevents damage, the ability will still prevent damage and still deal damage to that player. If Vengeful Archon can\'t deal damage to the targeted player (because the player is no longer in the game in a multiplayer game, for example), it will still prevent damage. It just won\'t deal any damage itself.').
card_ruling('vengeful archon', '2010-08-15', 'If Vengeful Archon\'s ability doesn\'t prevent any damage (perhaps because a different prevention effect is applied to the damage that would be dealt to you, or because the damage is unpreventable), Vengeful Archon won\'t deal any damage itself.').
card_ruling('vengeful archon', '2010-08-15', 'If Vengeful Archon\'s ability prevents damage, Vengeful Archon deals its damage immediately afterward as part of that same prevention effect. This happens before state-based actions are checked, and before any player can cast spells or activate abilities. If a spell or ability would have caused that damage to be dealt, this happens before that spell or ability resumes its resolution.').
card_ruling('vengeful archon', '2010-08-15', 'If you would be dealt combat damage by multiple attacking creatures, you choose which of that damage to prevent. For example, if X is 3 and you\'d be dealt combat damage by a 1/3 Scroll Thief and a 3/5 Siege Mastodon, you might choose to prevent 1 damage from the Scroll Thief and 2 damage from the Siege Mastodon. You don\'t decide until the point at which the creatures would deal their damage.').
card_ruling('vengeful archon', '2010-08-15', 'If the amount of damage that would be dealt to you is in excess of the amount of damage that Vengeful Archon\'s ability would prevent, that source deals its excess damage to you at the same time that the rest of it is prevented. Then Vengeful Archon deals its damage.').
card_ruling('vengeful archon', '2010-08-15', 'If multiple effects modify how damage will be dealt, the player who would be dealt damage chooses the order to apply the effects. For example, say you\'ve activated Vengeful Archon\'s ability with X = 3 targeting Player A, and you\'ve activated Vengeful Archon\'s ability with X = 1 targeting Player B. You\'re dealt 2 damage. You can apply the first prevention effect (dealing 2 damage to Player A), or you can apply the second prevention effect (dealing 1 damage to Player B) followed by the first prevention effect (dealing 1 damage to Player A). The unused portions of the prevention effects remain.').

card_ruling('vengeful pharaoh', '2011-09-22', 'Vengeful Pharaoh must be in your graveyard when combat damage is dealt to you or a planeswalker you control in order for its ability to trigger. That is, it can\'t die and trigger from your graveyard during the same combat damage step.').
card_ruling('vengeful pharaoh', '2011-09-22', 'If Vengeful Pharaoh is no longer in your graveyard when the triggered ability would resolve, the triggered ability won\'t do anything.').
card_ruling('vengeful pharaoh', '2011-09-22', 'If the attacking creature is an illegal target when the triggered ability tries to resolve, it will be countered and none of its effects will happen. Vengeful Pharaoh will remain in your graveyard.').
card_ruling('vengeful pharaoh', '2011-09-22', 'If multiple creatures deal combat damage to you simultaneously, Vengeful Pharaoh will only trigger once.').
card_ruling('vengeful pharaoh', '2011-09-22', 'If multiple creatures deal combat damage to you and to a planeswalker you control simultaneously, Vengeful Pharaoh will trigger twice. The first trigger will cause Vengeful Pharaoh to be put on top of your library. The second trigger will then do nothing, as Vengeful Pharaoh is no longer in your graveyard when it tries to resolve. Note that the second trigger will do nothing even if Vengeful Pharaoh is put back into your graveyard before it tries to resolve, as it\'s a different Vengeful Pharaoh than the one that was there before.').
card_ruling('vengeful pharaoh', '2011-09-22', 'If your own attacking creatures deal combat damage to you or a planeswalker you control (perhaps because combat damage was redirected), Vengeful Pharaoh will still trigger. You must target and destroy one of your own attacking creatures.').

card_ruling('vengeful rebirth', '2009-05-01', 'Vengeful Rebirth has two targets: a card in your graveyard, and a creature or player.').
card_ruling('vengeful rebirth', '2009-05-01', 'You can return a land card to your hand with Vengeful Rebirth. It just won\'t deal any damage to the targeted creature or player if you do.').
card_ruling('vengeful rebirth', '2009-05-01', 'If the targeted creature or player becomes an illegal target but the targeted card in your graveyard doesn\'t, Vengeful Rebirth will still return that card from your graveyard to your hand, but it won\'t deal any damage. Vengeful Rebirth will then be exiled.').
card_ruling('vengeful rebirth', '2009-05-01', 'If the targeted card in your graveyard becomes an illegal target but the targeted creature or player doesn\'t, Vengeful Rebirth won\'t return that card to your hand and it won\'t deal any damage (because you didn\'t return a nonland card to your hand this way). Vengeful Rebirth will still be exiled, though.').
card_ruling('vengeful rebirth', '2009-05-01', 'If both targets are illegal, Vengeful Rebirth is countered. It will go to your graveyard.').

card_ruling('vengevine', '2010-06-15', 'If you cast your second creature spell in a turn and Vengevine is in your graveyard, Vengevine\'s second ability triggers and goes on the stack on top of it. The ability will resolve, allowing you to return Vengevine to the battlefield, before the creature spell does.').
card_ruling('vengevine', '2010-06-15', 'Vengevine\'s triggered ability works only if Vengevine is already in your graveyard as you finish casting your second creature spell in a turn, and it remains there continuously until the ability resolves.').
card_ruling('vengevine', '2010-06-15', 'Vengevine doesn\'t need to be in your graveyard as you start casting your second creature spell in a turn, only as you finish. This matters if you sacrifice a Vengevine on the battlefield while casting that creature spell, perhaps to pay a cost of that spell or a cost of a mana ability.').
card_ruling('vengevine', '2010-06-15', 'Vengevine\'s triggered ability checks the card types of the spells you cast over the course of the entire turn, not just the ones you cast while Vengevine is in your graveyard. For example, if you cast a creature spell, then Vengevine is put into your graveyard, then you cast another creature spell, Vengevine\'s ability triggers.').
card_ruling('vengevine', '2010-06-15', 'Vengevine\'s ability triggers only for the second creature spell you cast in a turn. It won\'t trigger for the third, fourth, or so on. It also doesn\'t matter how many noncreature spells you cast in a turn; the ability counts just the creature spells.').

card_ruling('venom', '2004-10-04', 'If this Aura is moved onto a creature after blockers are declared, creatures blocking the newly-enchanted creature are not affected. This is because the ability triggers at the time blocking occurs.').

card_ruling('venom sliver', '2014-07-18', 'Slivers in this set affect only Sliver creatures you control. They don’t grant bonuses to your opponents’ Slivers. This is the same way Slivers from the Magic 2014 core set worked, while Slivers in earlier sets granted bonuses to all Slivers.').
card_ruling('venom sliver', '2014-07-18', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like indestructible and the ability granted by Belligerent Sliver, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('venom sliver', '2014-07-18', 'If you change the creature type of a Sliver you control so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures you control.').

card_ruling('venomous breath', '2004-10-04', 'It affects creatures that had blocked the target when this spell resolves, even if they are no longer blocking the creature.').

card_ruling('venomous fangs', '2004-10-04', 'If the damaged creature takes both lethal damage and is destroyed by this effect, you have to regenerate the creature twice to keep it alive.').

card_ruling('venser\'s journal', '2011-01-01', 'If multiple effects modify your hand size, apply them in timestamp order. For example, if you put Null Profusion (an enchantment that says your maximum hand size is two) onto the battlefield and then put Venser\'s Journal onto the battlefield, you\'ll have no maximum hand size. However, if those permanents enter the battlefield in the opposite order, your maximum hand size would be two.').

card_ruling('venser, shaper savant', '2007-05-01', 'If a spell is returned to its owner\'s hand, it\'s removed from the stack and thus will not resolve. The spell isn\'t countered; it just no longer exists.').
card_ruling('venser, shaper savant', '2007-05-01', 'If a copy of a spell is returned to its owner\'s hand, it\'s moved there, then it will cease to exist as a state-based action.').
card_ruling('venser, shaper savant', '2007-05-01', 'If Venser\'s enters-the-battlefield ability targets a spell cast with flashback, that spell will be exiled instead of returning to its owner\'s hand.').

card_ruling('venser, the sojourner', '2011-01-01', 'Emblems behave similarly to enchantments: They have an ability that, in a general sense, continually affects the game. The primary difference between them is that emblems aren\'t permanents and don\'t exist on the battlefield. Nothing in the game can remove an emblem, simply because no other spell or ability references them. Once you get an emblem, you keep it for the rest of the game. Emblems have no color, name, card type, or other characteristics beyond the listed ability').
card_ruling('venser, the sojourner', '2011-01-01', 'The first ability can target any permanent you own, including those another player controls.').
card_ruling('venser, the sojourner', '2011-01-01', 'If the first ability exiles a token, that token will cease to exist. It won\'t return to the battlefield.').
card_ruling('venser, the sojourner', '2011-01-01', 'A permanent exiled by the first ability will return to the battlefield under your control at the beginning of the next end step even if you no longer control Venser at that time.').
card_ruling('venser, the sojourner', '2011-01-01', 'Venser\'s second ability doesn\'t lock in what it applies to. That\'s because the effect states a true thing about creatures, but doesn\'t actually change the characteristics of those creatures. As a result, all creatures can\'t be blocked that turn, including creatures you don\'t control, creatures that weren\'t on the battlefield at the time the ability resolved, and creatures that have lost all abilities.').
card_ruling('venser, the sojourner', '2011-01-01', 'Venser\'s last ability creates an emblem with a triggered ability. The emblem is the source of the triggered ability. Because emblems are colorless, you can target permanents with protection from white or from blue, for example, with the triggered ability.').
card_ruling('venser, the sojourner', '2011-01-01', 'Whenever you cast a spell, the emblem\'s ability triggers and goes on the stack on top of it. It will resolve before the spell does.').
card_ruling('venser, the sojourner', '2011-01-01', 'If you control more than one such emblem, each one\'s ability will trigger separately whenever you cast a spell.').
card_ruling('venser, the sojourner', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('venser, the sojourner', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('venser, the sojourner', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('venser, the sojourner', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('venser, the sojourner', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('venser, the sojourner', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('venser, the sojourner', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('venser, the sojourner', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('venser, the sojourner', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('venser, the sojourner', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('venser, the sojourner', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('verdant embrace', '2006-09-25', 'The token is put onto the battlefield under the control of the player who controls the enchanted creature, not the player who controls Verdant Embrace.').

card_ruling('verdant force', '2004-10-04', 'Verdant Force\'s controller gets the tokens. That is the controller at the beginning of upkeep.').
card_ruling('verdant force', '2005-08-01', 'Once created, the tokens are independent of Verdant Force. If Verdant Force changes controllers, the new controller will put tokens onto the battlefield, but existing tokens will stay where they are.').
card_ruling('verdant force', '2006-05-01', 'In Two-Headed Giant, triggers only once per upkeep, not once for each player.').

card_ruling('verdant haven', '2014-07-18', 'If the land targeted by Verdant Haven is an illegal target when Verdan Haven tries to resolve, Verdant Haven will be countered. It won’t enter the battlefield and its enters-the-battlefield ability won’t trigger.').

card_ruling('verdant succession', '2004-10-04', 'You can choose not to find the card.').
card_ruling('verdant succession', '2008-08-01', 'If the creature was only a creature as the result of an animation effect, its controller still gets to search for a card with the same name. For example, if an animated Still Life is put into a graveyard, its controller gets to search his or her library for a card named Still Life.').

card_ruling('verdant touch', '2004-10-04', 'The land is still a land and retains all its abilities, plus any other characteristics.').
card_ruling('verdant touch', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('verdeloth the ancient', '2013-06-07', 'Verdeloth won\'t give himself +1/+1 unless he somehow becomes a Saproling.').

card_ruling('verduran enchantress', '2005-08-01', 'This does not trigger on the moving of an Aura from one permanent to another.').

card_ruling('vernal bloom', '2004-10-04', 'This is a triggered mana ability. It does not go on the stack.').

card_ruling('vertigo spawn', '2006-02-01', 'If the creature was already tapped when Vertigo Spawn blocked it, that creature still won\'t untap during its controller\'s next untap step.').
card_ruling('vertigo spawn', '2006-02-01', 'If two Vertigo Spawns block the same creature, both abilities trigger, but each only affects the creature during its controller\'s next untap step. The creature can untap during its controller\'s untap step following that one.').

card_ruling('vestige of emrakul', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('vestige of emrakul', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('vestige of emrakul', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('vestige of emrakul', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('vestige of emrakul', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').

card_ruling('vesuva', '2006-09-25', 'Vesuva has no intrinsic mana abilities.').
card_ruling('vesuva', '2006-09-25', 'If you don\'t choose a land on the battlefield, Vesuva enters the battlefield untapped as itself.').
card_ruling('vesuva', '2006-10-15', 'Cornered Market won\'t prevent Vesuva from being played (unless there\'s a Vesuva on the battlefield copying nothing), since you play it before it becomes named something else.').

card_ruling('vesuvan doppelganger', '2004-10-04', 'When the Doppelganger switches creatures, the creature it used to be is not considered to have left the battlefield. Such effects will consider the creature to have left the battlefield when the Doppelganger leaves the battlefield.').
card_ruling('vesuvan doppelganger', '2004-10-04', 'Damage is not removed when it changes forms.').
card_ruling('vesuvan doppelganger', '2004-10-04', 'Can switch to the same creature it is currently a copy of.').
card_ruling('vesuvan doppelganger', '2004-10-04', 'When it takes on the characteristics of the other card, it is no longer of creature type Shapeshifter.').
card_ruling('vesuvan doppelganger', '2004-10-04', 'When changing forms, any text changes that exist on the Doppelganger are applied to the new text, if appropriate.').
card_ruling('vesuvan doppelganger', '2004-12-01', 'If a Doppelganger is flipped, and it copies a flip card in any state, it will copy both \"sides\" of that card but use the flipped side. If a Doppelganger is unflipped, and it copies a flip card in any state, it will copy both \"sides\" of that card but use the unflipped side. In the second case, if it ever meets the flip conditions of its new ability, it will flip and used the flipped side of what it is copying.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'When Vesuvan Doppelganger\'s triggered ability triggers, you must choose a target for it. You determine whether to have the Doppelganger become a copy of that target when the ability resolves.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'Vesuvan Doppelganger copies the mana cost of the creature it\'s copying but doesn\'t copy its color.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'If another creature copies Vesuvan Doppelganger, the new creature will become a copy of whatever Vesuvan Doppelganger is copying except for its color, will copy Vesuvan Doppelganger\'s color, and will gain Vesuvan Doppelganger\'s triggered ability.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'Vesuvan Doppelganger\'s triggered ability may cause it to become a copy of itself. A Vesuvan Doppelganger that becomes a copy of a Vesuvan Doppelganger (either itself or a different one) will gain another instance of its triggered ability. Each instance of that ability will trigger during its controller\'s upkeep. They\'ll all be put on the stack and resolve one at a time. The last one to resolve determines the Doppelganger\'s characteristics for the rest of the turn.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'Vesuvan Doppelganger copies a face-down creature, it becomes a face-up 2/2 blue creature with no name and no abilities other than the one it gives itself.').
card_ruling('vesuvan doppelganger', '2007-09-16', 'Although Vesuvan Doppelganger\'s triggered ability is targeted, its \"as enters the battlefield\" ability is not.').

card_ruling('vesuvan shapeshifter', '2006-09-25', 'Essentially, each time Vesuvan Shapeshifter turns face up, it\'s a copy of a new creature. During your upkeep, you may turn it face down in preparation to copy something else.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'If Vesuvan Shapeshifter is cast face up, it behaves like Clone except that it gives itself a triggered ability.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'If Vesuvan Shapeshifter is turned face up, the process of turning face up includes (optionally) choosing another creature on the battlefield. You\'ll pay {1}{U} (not the morph cost, if any, of the new creature), then Vesuvan Shapeshifter will turn face up as a copy of that creature, plus the triggered ability it gives itself. It won\'t turn face up as a Vesuvan Shapeshifter and then change. If the copied creature has a \"when this creature is turned face up\" ability, it will trigger for Vesuvan Shapeshifter.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'When Vesuvan Shapeshifter is turned face down, its copy effect wears off. While it\'s face down, it\'s a face-down Vesuvan Shapeshifter that can be turned face up for a morph cost of {1}{U}.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'If another creature copies Vesuvan Shapeshifter while it\'s face up, the new creature will become a copy of whatever Vesuvan Shapeshifter is copying and gain the \"you may turn this creature face down\" ability. It won\'t gain morph {1}{U}. If that creature is then turned face down, its copy effect will continue and it\'ll be a face-down version of whatever it\'s copying. If the creature it\'s copying has morph, it can be turned face up. If the creature it\'s copying doesn\'t have morph, it\'s stuck face down forever unless some other effect (like Break Open) turns it face up again.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'If Vesuvan Shapeshifter copies a face-down creature, it becomes a face-up 2/2 colorless nameless creature with no abilities other than the one it gives itself.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'When Vesuvan Shapeshifter copies a creature that has echo, the echo ability will trigger at the beginning of your upkeep if Vesuvan Shapeshifter came under your control since the beginning of your last upkeep. If you\'ve controlled Vesuvan Shapeshifter since your last upkeep (regardless of whether it had echo then), the echo ability won\'t trigger.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'Turning Vesuvan Shapeshifter face down during your upkeep doesn\'t prevent you from needing to pay applicable upkeep costs such as echo.').
card_ruling('vesuvan shapeshifter', '2006-09-25', 'If you don\'t or can\'t choose another creature on the battlefield for Vesuvan Shapeshifter to copy, it doesn\'t get the triggered ability that turns it face down. It\'s just a 0/0 creature with morph {1}{U}.').

card_ruling('veteran bodyguard', '2004-10-04', 'If a creature is blocked but Trample damage is still done to a player, this damage can\'t be redirected to the Bodyguard because the Bodyguard only takes damage from unblocked creatures.').
card_ruling('veteran bodyguard', '2004-10-04', 'Damage goes to the Bodyguard as long as he is untapped. This works even if he is blocking.').
card_ruling('veteran bodyguard', '2004-10-04', 'If you have multiple Veteran Bodyguards, you can decide which one receives the redirected damage each time damage would be dealt to you.').

card_ruling('veteran explorer', '2008-04-01', 'The player whose turn it is searches his or her library (or chooses not to), then each other player in turn order does the same. Then the player whose turn it is shuffles his or her library (if that player searched), then each other player in turn order does the same.').

card_ruling('veteran of the depths', '2007-10-01', 'This is a triggered ability, not an activated ability. It doesn\'t allow you to tap the creature whenever you want; rather, you need some other way of tapping it, such as by attacking with the creature.').
card_ruling('veteran of the depths', '2007-10-01', 'For the ability to trigger, the creature has to actually change from untapped to tapped. If an effect attempts to tap the creature, but it was already tapped at the time, this ability won\'t trigger.').

card_ruling('veteran warleader', '2015-08-25', 'The ability that defines Veteran Warleader’s power and toughness works in all zones, not just the battlefield. If Veteran Warleader is on the battlefield, it will count itself.').
card_ruling('veteran warleader', '2015-08-25', 'To activate the last ability, you can tap any other untapped Ally you control, including one that hasn’t been under your control since the beginning of your most recent turn. (Note that ability doesn’t use the tap symbol.)').
card_ruling('veteran warleader', '2015-08-25', 'You choose which ability Veteran Warleader gains as the last ability resolves, not as you activate it.').
card_ruling('veteran warleader', '2015-08-25', 'Vigilance matters only as attackers are declared. Giving a creature vigilance after that point won’t untap it, even if it’s attacking.').

card_ruling('veteran\'s voice', '2004-10-04', 'Is put into its owner\'s graveyard if you lose control of the creature.').

card_ruling('vexing arcanix', '2004-10-04', 'If the player has no cards in their library, the effect does nothing. It does not cause any damage.').
card_ruling('vexing arcanix', '2004-10-04', 'The target player names a card on resolution.').

card_ruling('vexing devil', '2012-05-01', 'If a player chooses to have Vexing Devil deal 4 damage to him or her, but some or all of that damage is prevented or redirected, Vexing Devil will still be sacrificed.').

card_ruling('vexing shusher', '2008-05-01', 'After Vexing Shusher\'s ability resolves, any spells or abilities that would counter the targeted spell will still resolve. They just won\'t counter that spell.').
card_ruling('vexing shusher', '2008-05-01', 'Vexing Shusher\'s ability won\'t prevent the targeted spell from being countered by the game rules (for example, if all its targets are illegal).').

card_ruling('vexing sphinx', '2006-07-15', 'If Vexing Sphinx has more age counters on it than you have cards in your hand, you can\'t pay the upkeep and you must sacrifice it.').
card_ruling('vexing sphinx', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').

card_ruling('vhati il-dal', '2006-09-25', 'You don\'t choose whether to affect the targeted creature\'s power or its toughness until the ability resolves.').
card_ruling('vhati il-dal', '2009-10-01', 'You apply power/toughness changing effects in a series of sublayers in the following order: (a) effects from characteristic-defining abilities; (b) effects that set power and/or toughness to a specific number or value; (c) effects that modify power and/or toughness but don\'t set power and/or toughness to a specific number or value; (d) changes from counters; (e) effects that switch a creature\'s power and toughness. This card\'s effect is always applied in (b), which means that effects applied in sublayer (c), (d), or (e) will not be overwritten; they will be applied to the new value.').

card_ruling('viashino bey', '2004-10-04', 'The ability is not a triggered ability. It is a constraint on declaring an attack.').

card_ruling('viashino cutthroat', '2004-10-04', 'It is returned to its owner\'s hand at the end of turn only if it is on the battlefield.').

card_ruling('viashino sandscout', '2004-10-04', 'It is returned to its owner\'s hand at the end of turn only if it is on the battlefield.').

card_ruling('viashino sandstalker', '2004-10-04', 'It is returned to its owner\'s hand at the end of any turn in which it is on the battlefield.').

card_ruling('vibrating sphere', '2004-10-04', 'Any creature which is lowered below a toughness of 1 will die at the beginning of the upkeep step, since that is the first time State-Based Actions are checked.').

card_ruling('vicious shadows', '2008-10-01', 'The player you target doesn\'t have to be the controller of the creature that was put into a graveyard.').
card_ruling('vicious shadows', '2008-10-01', 'The number of cards in the targeted opponent\'s hand is checked only when the ability resolves.').

card_ruling('victimize', '2004-10-04', 'This card can be used to put both parts of the B.F.M. onto the battlefield at once.').
card_ruling('victimize', '2004-10-04', 'If one of the target creatures is not there on resolution, the other is still affected. If both creatures are gone, then nothing happens.').
card_ruling('victimize', '2010-03-01', 'You can\'t bring back the creature you sacrificed because targets are chosen as the spell is cast, but the creature is not sacrificed until the spell resolves.').

card_ruling('victory\'s herald', '2011-06-01', 'Even though the triggered ability affects only attacking creatures, the bonuses will remain for the rest of the turn.').
card_ruling('victory\'s herald', '2011-06-01', 'Victory\'s Herald also gains flying (which will likely be redundant) and lifelink if it\'s still attacking when the ability resolves.').

card_ruling('victual sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('victual sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('view from above', '2009-02-01', 'This checks the colors of permanents you control as it resolves.').
card_ruling('view from above', '2009-02-01', 'If you control a white permanent, View from Above moves from the stack to your hand as part of its resolution. It never hits the graveyard. If you don\'t control a white permanent, View from Above is put into the graveyard from the stack as normal.').
card_ruling('view from above', '2009-02-01', 'If View from Above is countered for any reason (for example, if its target has become illegal), none of its effects happen. It will be put into your graveyard as normal.').

card_ruling('vigean hydropon', '2006-05-01', 'Even though this creature can\'t attack, it doesn\'t have defender.').

card_ruling('vigil for the lost', '2011-01-01', 'As the ability resolves, you choose a value for X and decide whether to pay {X}. If you do decide to pay {X}, it\'s too late for any player to respond since the ability is already in the midst of resolving.').

card_ruling('vigor', '2007-10-01', 'The last ability triggers when the Incarnation is put into its owner\'s graveyard from any zone, not just from on the battlefield.').
card_ruling('vigor', '2007-10-01', 'Although this ability triggers when the Incarnation is put into a graveyard from the battlefield, it doesn\'t *specifically* trigger on leaving the battlefield, so it doesn\'t behave like other leaves-the-battlefield abilities. The ability will trigger from the graveyard.').
card_ruling('vigor', '2007-10-01', 'If the Incarnation had lost this ability while on the battlefield (due to Lignify, for example) and then was destroyed, the ability would still trigger and it would get shuffled into its owner\'s library. However, if the Incarnation lost this ability when it was put into the graveyard (due to Yixlid Jailer, for example), the ability wouldn\'t trigger and the Incarnation would remain in the graveyard.').
card_ruling('vigor', '2007-10-01', 'If the Incarnation is removed from the graveyard after the ability triggers but before it resolves, it won\'t get shuffled into its owner\'s library. Similarly, if a replacement effect has the Incarnation move to a different zone instead of being put into the graveyard, the ability won\'t trigger at all.').

card_ruling('vigor mortis', '2005-10-01', 'The +1/+1 counter from this ability is in addition to any other counters the creature enters the battlefield with.').

card_ruling('vile aggregate', '2015-08-25', 'The ability that defines Vile Aggregate’s power works in all zones, not just the battlefield. If Vile Aggregate is on the battlefield, it will count itself.').
card_ruling('vile aggregate', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('vile aggregate', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('vile aggregate', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('vile aggregate', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('vile aggregate', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('vile aggregate', '2015-08-25', 'The card exiled by the ingest ability is exiled face up.').
card_ruling('vile aggregate', '2015-08-25', 'If the player has no cards in his or her library when the ingest ability resolves, nothing happens. That player won’t lose the game (until he or she has to draw a card from an empty library).').

card_ruling('vile rebirth', '2012-07-01', 'If the creature card is an illegal target when Vile Rebirth tries to resolve, it will be countered and none of its effects will happen. You won\'t put a Zombie token onto the battlefield.').

card_ruling('vile requiem', '2004-10-04', 'Adding a counter is optional. If you forget to add one during your upkeep, you cannot back up and add one later.').

card_ruling('village bell-ringer', '2011-09-22', 'Untapping an attacking creature doesn\'t remove it from combat.').

card_ruling('village survivors', '2011-01-22', 'Tapping creatures to attack with them happens just before any required costs to attack with those creatures are paid. If paying such a cost causes your life total to fall below 6, your tapped attackers will have vigilance, but the ability will have no benefit that combat.').

card_ruling('villainous wealth', '2014-09-20', 'You cast the cards one at a time as Villainous Wealth is resolving, choosing modes, targets, and so on. The last card you cast will be the first one to resolve. Ignore timing restrictions based on the cards’ types. Other timing restrictions, such as “Cast [this card] only during combat,” must be followed.').
card_ruling('villainous wealth', '2014-09-20', 'Because you’re already casting the cards using an alternative cost (by casting them without paying their mana costs), you can’t pay any other alternative costs for the cards, including casting them face down using the morph ability. You can pay additional costs, such as kicker costs. If the cards have any mandatory additional costs, you must pay those.').
card_ruling('villainous wealth', '2014-09-20', 'If a card has {X} in its mana cost, you must choose 0 as the value for X when casting it.').
card_ruling('villainous wealth', '2014-09-20', 'Any cards you don’t cast this way will remain in exile.').

card_ruling('vine snare', '2015-06-22', 'Check the power of each creature as it would deal combat damage to determine if that damage is prevented. It doesn’t matter what any creature’s power is as Vine Snare resolves.').

card_ruling('vines of vastwood', '2013-04-15', 'This is not the same as hexproof. If, for example, you target one of your opponent\'s creatures, your opponents won\'t be able to target their own creature with spells or abilities.').

card_ruling('vineweft', '2014-07-18', 'You can activate the last ability only if Vineweft is in your graveyard.').

card_ruling('violent eruption', '2004-10-04', 'You can\'t choose zero targets. You must choose at least one target.').

card_ruling('violent outburst', '2009-05-01', 'Cascade triggers when you cast the spell, meaning that it resolves before that spell. If you end up casting the exiled card, it will go on the stack above the spell with Cascade.').
card_ruling('violent outburst', '2009-05-01', 'When the Cascade ability resolves, you must exile cards. The only optional part of the ability is whether or not your cast the last card exiled.').
card_ruling('violent outburst', '2009-05-01', 'If a spell with Cascade is countered, the Cascade ability will still resolve normally.').
card_ruling('violent outburst', '2009-05-01', 'You exile the cards face-up. All players will be able to see them.').
card_ruling('violent outburst', '2009-05-01', 'If you exile a split card with Cascade, check if at least one half of that split card has a converted mana cost that\'s less than the converted mana cost of the spell with cascade. If so, you can cast either half of that split card.').
card_ruling('violent outburst', '2009-05-01', 'If you cast the last exiled card, you\'re casting it as a spell. It can be countered. If that card has cascade, the new spell\'s cascade ability will trigger, and you\'ll repeat the process for the new spell.').
card_ruling('violent outburst', '2009-05-01', 'After you\'re done exiling cards and casting the last one if you chose to do so, the remaining exiled cards are randomly placed on the bottom of your library. Neither you nor any other player is allowed to know the order of those cards.').

card_ruling('violent ultimatum', '2008-10-01', 'You must target three different permanents. If some of the permanents become illegal targets before the spell resolves, Violent Ultimatum will still destroy the rest of them.').

card_ruling('violet pall', '2008-04-01', 'You get the token, not the player who controlled the creature.').

card_ruling('viridescent wisps', '2008-05-01', 'An effect that changes a permanent\'s colors overwrites all its old colors unless it specifically says \"in addition to its other colors.\" For example, after Cerulean Wisps resolves, the affected creature will just be blue. It doesn\'t matter what colors it used to be (even if, for example, it used to be blue and black).').
card_ruling('viridescent wisps', '2008-05-01', 'Changing a permanent\'s color won\'t change its text. If you turn Wilt-Leaf Liege blue, it will still affect green creatures and white creatures.').
card_ruling('viridescent wisps', '2008-05-01', 'Colorless is not a color.').

card_ruling('viridian betrayers', '2011-06-01', 'A player is poisoned if that player has one or more poison counters.').
card_ruling('viridian betrayers', '2011-06-01', 'The player must be poisoned when Viridian Betrayers deals damage in order for Viridian Betrayers to have infect. If the player has no poison counters, damage dealt to that player by Viridian Betrayers will result in loss of life, even if it\'s dealt at the same time as damage from another creature with infect.').

card_ruling('viridian corrupter', '2011-06-01', 'If there are no artifacts on the battlefield when Viridian Corrupter enters the battlefield, its enters-the-battlefield does nothing. Viridian Corrupter will simply enter the battlefield.').
card_ruling('viridian corrupter', '2011-06-01', 'The enters-the-battlefield ability is not optional. You must target an artifact you control if there are no other artifacts on the battlefield.').

card_ruling('viridian harvest', '2011-06-01', 'If the enchanted artifact and Viridian Harvest are put into the graveyard at the same time, you\'ll still gain 6 life.').

card_ruling('viridian revel', '2011-01-01', 'It doesn\'t matter who controlled the artifact while it was on the battlefield, only whose graveyard it was put into.').

card_ruling('virulent plague', '2015-02-25', 'If an effect creates a creature token that normally has toughness 2 or less, it will enter the battlefield with toughness 0 or less, be put into its owner’s graveyard, and then cease to exist. Any abilities that trigger when a creature enters the battlefield or dies will trigger.').

card_ruling('virulent sliver', '2013-07-01', 'Abilities that Slivers grant, as well as power/toughness boosts, are cumulative. However, for some abilities, like flying, having more than one instance of the ability doesn’t provide any additional benefit.').
card_ruling('virulent sliver', '2013-07-01', 'If the creature type of a Sliver changes so it’s no longer a Sliver, it will no longer be affected by its own ability. Its ability will continue to affect other Sliver creatures.').

card_ruling('virulent swipe', '2010-06-15', 'If a spell with rebound that you cast from your hand is countered for any reason (due to a spell like Cancel, or because all of its targets are illegal), the spell doesn\'t resolve and rebound has no effect. The spell is simply put into your graveyard. You won\'t get to cast it again next turn.').
card_ruling('virulent swipe', '2010-06-15', 'If you cast a spell with rebound from anywhere other than your hand (such as from your graveyard due to Sins of the Past, from your library due to cascade, or from your opponent\'s hand due to Sen Triplets), rebound won\'t have any effect. If you do cast it from your hand, rebound will work regardless of whether you paid its mana cost (for example, if you cast it from your hand due to Maelstrom Archangel).').
card_ruling('virulent swipe', '2010-06-15', 'If a replacement effect would cause a spell with rebound that you cast from your hand to be put somewhere else instead of your graveyard (such as Leyline of the Void might), you choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('virulent swipe', '2010-06-15', 'Rebound will have no effect on copies of spells because you don\'t cast them from your hand.').
card_ruling('virulent swipe', '2010-06-15', 'If you cast a spell with rebound from your hand and it resolves, it isn\'t put into your graveyard. Rather, it\'s exiled directly from the stack. Effects that care about cards being put into your graveyard won\'t do anything.').
card_ruling('virulent swipe', '2010-06-15', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card\'s type (if it\'s a sorcery) are ignored. Other restrictions are not (such as the one from Rule of Law).').
card_ruling('virulent swipe', '2010-06-15', 'If you are unable to cast a card from exile this way, or you choose not to, nothing happens when the delayed triggered ability resolves. The card remains exiled for the rest of the game, and you won\'t get another chance to cast the card. The same is true if the ability is countered (due to Stifle, perhaps).').
card_ruling('virulent swipe', '2010-06-15', 'If you cast a card from exile this way, it will go to your graveyard when it resolves or is countered. It won\'t go back to exile.').

card_ruling('virulent wound', '2011-06-01', 'If the creature is not put into a graveyard on the turn Virulent Wound resolves, its controller won\'t get a poison counter.').

card_ruling('viscera dragger', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('viscera dragger', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('viscera dragger', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('viscera dragger', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('viscera dragger', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').
card_ruling('viscera dragger', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('viscera seer', '2010-08-15', 'You can sacrifice Viscera Seer to activate its own ability.').
card_ruling('viscera seer', '2010-08-15', 'If you sacrifice an attacking or blocking creature during the declare blockers step, it won\'t deal combat damage. If you wait until the combat damage step, but that creature is dealt lethal damage, it\'ll be destroyed before you get a chance to sacrifice it.').

card_ruling('viscerid deepwalker', '2013-06-07', 'You can exile a card in your hand using suspend any time you could cast that card. Consider its card type, any effect that affects when you could cast it (such as flash) and any other effects that could stop you from casting it (such as Meddling Mage\'s effect) to determine if and when you can do this. Whether or not you could actually complete all steps in casting the card is irrelevant. For example, you can exile a card with suspend that has no mana cost or requires a target even if no legal targets are available at that time.').
card_ruling('viscerid deepwalker', '2013-06-07', 'Exiling a card with suspend isn\'t casting that card. This action doesn\'t use the stack and can\'t be responded to.').
card_ruling('viscerid deepwalker', '2013-06-07', 'If the spell requires any targets, those targets are chosen when the spell is finally cast, not when it\'s exiled.').
card_ruling('viscerid deepwalker', '2013-06-07', 'If the first triggered ability of suspend (the one that removes time counters) is countered, no time counter is removed. The ability will trigger again during the card\'s owner\'s next upkeep.').
card_ruling('viscerid deepwalker', '2013-06-07', 'When the last time counter is removed, the second triggered ability of suspend will trigger. It doesn\'t matter why the last time counter was removed or what effect removed it.').
card_ruling('viscerid deepwalker', '2013-06-07', 'If the second triggered ability of suspend (the one that lets you cast the card) is countered, the card can\'t be cast. It remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('viscerid deepwalker', '2013-06-07', 'As the second triggered ability resolves, you must cast the card if able. Timing restrictions based on the card\'s type are ignored.').
card_ruling('viscerid deepwalker', '2013-06-07', 'If you can\'t cast the card, perhaps because there are no legal targets available, it remains exiled with no time counters on it, and it\'s no longer suspended.').
card_ruling('viscerid deepwalker', '2013-06-07', 'If the spell has any mandatory additional costs, you must pay those if able. However, if an additional cost includes a mana payment, you are forced to pay that cost only if there\'s enough mana in your mana pool at the time you cast the spell. You aren\'t forced to activate any mana abilities, although you may do so if you wish.').
card_ruling('viscerid deepwalker', '2013-06-07', 'A creature cast using suspend will enter the battlefield with haste. It will have haste until another player gains control of it (or, in some rare cases, gains control of the creature spell itself).').

card_ruling('vish kal, blood arbiter', '2011-09-22', 'Vish Kal\'s last ability doesn\'t deal damage. It doesn\'t cause you to gain any life from its lifelink ability.').

card_ruling('vision charm', '2004-10-04', 'All lands changed are changed to the same type.').

card_ruling('visions', '2004-10-04', 'You can\'t rearrange the cards. You put them back in the same order or have the whole library shuffled.').
card_ruling('visions', '2004-10-04', 'If there are less than 5 cards in the library, you look at whatever ones remain and you still get the option to shuffle.').
card_ruling('visions', '2004-10-04', 'This is not a draw and will not cause a player to lose if there are less than 5 cards in the library.').

card_ruling('visions of beyond', '2011-09-22', 'The number of cards in each player\'s graveyard is counted when Visions of Beyond resolves. Visions of Beyond won\'t count itself as it will still be on the stack at this time.').

card_ruling('vital surge', '2013-06-07', 'You reveal all cards you intend to splice at the same time. Each individual card can be spliced only once onto any one spell.').
card_ruling('vital surge', '2013-06-07', 'A card with a splice ability can\'t be spliced onto itself because the spell is on the stack (and not in your hand) when you reveal the cards you want to splice onto it.').
card_ruling('vital surge', '2013-06-07', 'You choose all targets for the spell after revealing cards you want to splice, including any targets required by the text of any of those cards. You may choose a different target for each instance of the word \"target\" on the resulting spell.').
card_ruling('vital surge', '2013-06-07', 'If all of the spell\'s targets are illegal when the spell tries to resolve, it will be countered and none of its effects will happen.').

card_ruling('vitalizing wind', '2004-10-04', 'Only affects creatures you control when the spell resolves.').

card_ruling('vithian renegades', '2009-05-01', 'The enters-the-battlefield ability is mandatory. If you\'re the only player who controls any artifacts, you must target one of them.').

card_ruling('vithian stinger', '2008-10-01', 'If you activate a card\'s unearth ability but that card is removed from your graveyard before the ability resolves, that unearth ability will resolve and do nothing.').
card_ruling('vithian stinger', '2008-10-01', 'Activating a creature card\'s unearth ability isn\'t the same as casting the creature card. The unearth ability is put on the stack, but the creature card is not. Spells and abilities that interact with activated abilities (such as Stifle) will interact with unearth, but spells and abilities that interact with spells (such as Remove Soul) will not.').
card_ruling('vithian stinger', '2008-10-01', 'At the beginning of the end step, a creature returned to the battlefield with unearth is exiled. This is a delayed triggered ability, and it can be countered by effects such as Stifle or Voidslime that counter triggered abilities. If the ability is countered, the creature will stay on the battlefield and the delayed trigger won\'t trigger again. However, the replacement effect will still exile the creature when it eventually leaves the battlefield.').
card_ruling('vithian stinger', '2008-10-01', 'Unearth grants haste to the creature that\'s returned to the battlefield. However, neither of the \"exile\" abilities is granted to that creature. If that creature loses all its abilities, it will still be exiled at the beginning of the end step, and if it would leave the battlefield, it is still exiled instead.').
card_ruling('vithian stinger', '2008-10-01', 'If a creature returned to the battlefield with unearth would leave the battlefield for any reason, it\'s exiled instead -- unless the spell or ability that\'s causing the creature to leave the battlefield is actually trying to exile it! In that case, it succeeds at exiling it. If it later returns the creature card to the battlefield (as Oblivion Ring or Flickerwisp might, for example), the creature card will return to the battlefield as a new object with no relation to its previous existence. The unearth effect will no longer apply to it.').

card_ruling('vitu-ghazi guildmage', '2013-04-15', 'You can choose any creature token you control for populate. If a spell or ability puts a token onto the battlefield under your control and then instructs you to populate (as Coursers\' Accord does), you may choose to copy the token you just created, or you may choose to copy another creature token you control.').
card_ruling('vitu-ghazi guildmage', '2013-04-15', 'If you choose to copy a creature token that\'s a copy of another creature, the new creature token will copy the characteristics of whatever the original token is copying.').
card_ruling('vitu-ghazi guildmage', '2013-04-15', 'The new creature token copies the characteristics of the original token as stated by the effect that put the original token onto the battlefield.').
card_ruling('vitu-ghazi guildmage', '2013-04-15', 'The new token doesn\'t copy whether the original token is tapped or untapped, whether it has any counters on it or Auras and Equipment attached to it, or any noncopy effects that have changed its power, toughness, color, and so on.').
card_ruling('vitu-ghazi guildmage', '2013-04-15', 'Any “as [this creature] enters the battlefield” or “[this creature] enters the battlefield with” abilities of the new token will work.').
card_ruling('vitu-ghazi guildmage', '2013-04-15', 'If you control no creature tokens when you populate, nothing will happen.').

card_ruling('vivify', '2008-08-01', 'A noncreature permanent that turns into a creature can attack, and its {T} abilities can be activated, only if its controller has continuously controlled that permanent since the beginning of his or her most recent turn. It doesn\'t matter how long the permanent has been a creature.').

card_ruling('vivisection', '2013-04-15', 'You must sacrifice exactly one creature to cast this spell; you cannot cast it without sacrificing a creature, and you cannot sacrifice additional creatures.').
card_ruling('vivisection', '2013-04-15', 'Players can only respond once this spell has been cast and all its costs have been paid. No one can try to destroy the creature you sacrificed to prevent you from casting this spell.').

card_ruling('vizkopa confessor', '2013-01-24', 'You may pay 0 life when Vizkopa Confessor\'s enters-the-battlefield ability resolves. If you do, no cards are revealed or exiled.').
card_ruling('vizkopa confessor', '2013-01-24', 'The amount of life you pay is chosen when Vizkopa Confessor\'s last ability resolves.').
card_ruling('vizkopa confessor', '2013-01-24', 'If the target opponent is an illegal target when Vizkopa Confessor\'s last ability tries to resolve, it will be countered and none of its effects will happen. You won\'t pay life and no player will reveal any cards.').
card_ruling('vizkopa confessor', '2013-04-15', 'You may pay {W/B} a maximum of one time for each extort triggered ability. You decide whether to pay when the ability resolves.').
card_ruling('vizkopa confessor', '2013-04-15', 'The amount of life you gain from extort is based on the total amount of life lost, not necessarily the number of opponents you have. For example, if your opponent\'s life total can\'t change (perhaps because that player controls Platinum Emperion), you won\'t gain any life.').
card_ruling('vizkopa confessor', '2013-04-15', 'The extort ability doesn\'t target any player.').

card_ruling('vizkopa guildmage', '2013-01-24', 'Multiple instances of lifelink are redundant. Giving the same creature lifelink more than once won\'t cause you to gain additional life.').
card_ruling('vizkopa guildmage', '2013-01-24', 'Each time the second ability resolves, a delayed triggered ability is created. Whenever you gain life that turn, each of those abilities will trigger. For example, if you activate the second ability twice (and let those abilities resolve) and then you gain 2 life, each opponent will lose a total of 4 life. Each instance will cause two abilities to trigger, each causing that player to lose 2 life.').

card_ruling('vodalian mystic', '2004-10-04', 'It can target spells of type instant or sorcery, and not spells of other types that say they can be cast \"any time you could cast\" an instant or sorcery.').

card_ruling('vodalian war machine', '2004-10-04', 'The Merfolk are tapped during announcement and as a cost. The ability can\'t be announced if the Merfolk are not in an untapped state.').

card_ruling('voice of resurgence', '2013-04-15', 'If Voice of Resurgence\'s ability triggers because an opponent cast a spell during your turn, the token will be created before that spell resolves.').
card_ruling('voice of resurgence', '2013-04-15', 'The power and toughness of the token change as the number of creatures you control changes.').
card_ruling('voice of resurgence', '2013-04-15', 'Copies of the token will also have the ability that defines its power and toughness.').

card_ruling('voice of the woods', '2004-10-04', 'Since the ability does not have the {Tap} symbol, you can use the ability before this creature begins a turn under your control.').
card_ruling('voice of the woods', '2004-10-04', 'It can tap itself but is not required to do so.').

card_ruling('voices from the void', '2009-02-01', 'To determine the number of basic land types among lands you control, look at the lands you have on the battlefield and ask yourself whether the subtypes Plains, Island, Swamp, Mountain, and Forest appear within that group. The number of times you say yes (topping out at five) tells you how powerful your domain abilities will be.').
card_ruling('voices from the void', '2009-02-01', 'How many lands you control of a particular basic land type is irrelevant to a domain ability, as long as that number is greater than zero. As far as domain is concerned, ten Forests is the same as one Forest.').
card_ruling('voices from the void', '2009-02-01', 'A number of nonbasic lands have basic land types. Domain abilities don\'t count the number of lands you control -- they count the number of basic land types among lands you control, even if that means checking the same land twice. For example, if you control a Tundra, an Overgrown Tomb, and a Madblind Mountain, you\'ll have a Plains, Island, Swamp, Mountain, and Forest among the lands you control. Your domain abilities will be maxed out.').

card_ruling('void', '2004-10-04', 'You can choose zero. Token creatures and artifact lands have converted mana costs of zero, for example.').

card_ruling('void attendant', '2015-08-25', 'Once you announce that you’re activating this ability, players can’t respond until after you have paid its costs and completed activating it. Specifically, no one can try to remove the card from exile to stop you from activating the ability.').
card_ruling('void attendant', '2015-08-25', 'Cards with devoid use frames that are variations of the transparent frame traditionally used for Eldrazi. The top part of the card features some color over a background based on the texture of the hedrons that once imprisoned the Eldrazi. This coloration is intended to aid deckbuilding and game play.').
card_ruling('void attendant', '2015-08-25', 'A card with devoid is just colorless. It’s not colorless and the colors of mana in its mana cost.').
card_ruling('void attendant', '2015-08-25', 'Other cards and abilities can give a card with devoid color. If that happens, it’s just the new color, not that color and colorless.').
card_ruling('void attendant', '2015-08-25', 'Devoid works in all zones, not just on the battlefield.').
card_ruling('void attendant', '2015-08-25', 'If a card loses devoid, it will still be colorless. This is because effects that change an object’s color (like the one created by devoid) are considered before the object loses devoid.').
card_ruling('void attendant', '2015-08-25', 'If a spell or ability requires that you put more than one exiled card into the graveyard, you may choose cards owned by different opponents. Each card chosen will be put into its owner’s graveyard.').
card_ruling('void attendant', '2015-08-25', 'If a replacement effect will cause cards that would be put into a graveyard from anywhere to be exiled instead (such as the one created by Anafenza, the Foremost), you can still put an exiled card into its opponent’s graveyard. The card becomes a new object and remains in exile. In this situation, you can’t use a single exiled card if required to put more than one exiled card into the graveyard. Conversely, you could use the same card in this situation if two separate spells or abilities each required you to put a single exiled card into its owner’s graveyard.').
card_ruling('void attendant', '2015-08-25', 'You can’t look at face-down cards in exile unless an effect allows you to.').
card_ruling('void attendant', '2015-08-25', 'Face-down cards in exile are grouped using two criteria: what caused them to be exiled face down and when they were exiled face down. If you want to put a face-down card in exile into its owner’s graveyard, you must first choose one of these groups and then choose a card from within that group at random. For example, say an artifact causes your opponent to exile his or her hand of three cards face down. Then on a later turn, that artifact causes your opponent to exile another two cards face down. If you use Wasteland Strangler to put one of those cards into his or her graveyard, you would pick the first or second pile and put a card chosen at random from that pile into the graveyard.').
card_ruling('void attendant', '2015-08-25', 'Eldrazi Scions are similar to Eldrazi Spawn, seen in the Zendikar block. Note that Eldrazi Scions are 1/1, not 0/1.').
card_ruling('void attendant', '2015-08-25', 'Eldrazi and Scion are each separate creature types. Anything that affects Eldrazi will affect these tokens, for example.').
card_ruling('void attendant', '2015-08-25', 'Sacrificing an Eldrazi Scion creature token to add {1} to your mana pool is a mana ability. It doesn’t use the stack and can’t be responded to.').
card_ruling('void attendant', '2015-08-25', 'Some instants and sorceries that create Eldrazi Scions require targets. If all targets for such a spell have become illegal by the time that spell tries to resolve, the spell will be countered and none of its effects will happen. You won’t get any Eldrazi Scions.').

card_ruling('void maw', '2006-07-15', 'If Void Maw and another creature would be put into the graveyard from the battlefield at the same time, Void Maw is put into your graveyard and the other creature is exiled.').
card_ruling('void maw', '2011-06-01', 'Cards exiled with Void Maw that would go to the graveyard but have that replaced by an effect such as Leyline of the Void are considered new objects. They can not be used to activate Void Maw\'s ability again.').

card_ruling('void squall', '2015-02-25', 'Casting the card again due to the delayed triggered ability is optional. If you choose not to cast the card, or if you can’t (perhaps because there are no legal targets available), the card will stay exiled. You won’t get another chance to cast it on a future turn.').
card_ruling('void squall', '2015-02-25', 'If a spell with rebound that you cast from your hand is countered for any reason (either because of another spell or ability or because all its targets are illegal as it tries to resolve), that spell won’t resolve and none of its effects will happen, including rebound. The spell will be put into its owner’s graveyard and you won’t get to cast it again on your next turn.').
card_ruling('void squall', '2015-02-25', 'At the beginning of your upkeep, all delayed triggered abilities created by rebound effects trigger. You may handle them in any order. If you want to cast a card this way, you do so as part of the resolution of its delayed triggered ability. Timing restrictions based on the card’s type (if it’s a sorcery) are ignored. Other restrictions, such as “Cast [this spell] only during combat,” must be followed.').
card_ruling('void squall', '2015-02-25', 'As long as you cast a spell with rebound from your hand, rebound will work regardless of whether you paid its mana cost or an alternative cost you were permitted to pay.').
card_ruling('void squall', '2015-02-25', 'If you cast a spell with rebound from any zone other than your hand (including your opponent’s hand), rebound will have no effect.').
card_ruling('void squall', '2015-02-25', 'If a replacement effect (such as the one created by Rest in Peace) would cause a spell with rebound that you cast from your hand to be put somewhere other than into your graveyard as it resolves, you can choose whether to apply the rebound effect or the other effect as the spell resolves.').
card_ruling('void squall', '2015-02-25', 'Rebound will have no effect on copies of spells because you don’t cast them from your hand.').
card_ruling('void squall', '2015-02-25', 'If you cast a card from exile this way, it will go to its owner’s graveyard when it resolves or is countered. It won’t go back to exile.').

card_ruling('void stalker', '2012-07-01', 'You can activate Void Stalker\'s ability targeting a creature you control, including Void Stalker itself. In that case, only you will shuffle your library.').
card_ruling('void stalker', '2012-07-01', 'If the target creature is an illegal target when the ability tries to resolve, it will be countered and none of its effects will happen. No creature will leave the battlefield and no library will be shuffled.').
card_ruling('void stalker', '2012-07-01', 'If Void Stalker leaves the battlefield before its ability resolves, the target creature will be shuffled into its owner\'s library but Void Stalker will remain wherever it is.').

card_ruling('void winnower', '2015-08-25', 'Effects that increase or reduce the cost to cast a spell don’t affect that spell’s converted mana cost.').
card_ruling('void winnower', '2015-08-25', 'For spells with {X} in their mana costs, use the value chosen for X to determine if the spell’s converted mana cost is even or not. For example, your opponent could cast Endless One (with mana cost {X}) with X equal to 5, but not with X equal to 6.').
card_ruling('void winnower', '2015-08-25', 'For creatures on the battlefield with {X} in their mana costs, the value for X is 0. Mistcutter Hydra (a creature with mana cost {X}{G}) could block, but Endless One could not.').
card_ruling('void winnower', '2015-08-25', 'The converted mana cost of a face-down creature spell or a face-down creature is 0. Void Winnower will stop face-down creature spells from being cast and face-down creatures from blocking.').
card_ruling('void winnower', '2015-08-25', 'Yes, your opponent can’t even. We know.').

card_ruling('voidmage apprentice', '2004-10-04', 'The trigger occurs when you use the Morph ability to turn the card face up, or when an effect turns it face up. It will not trigger on being revealed or on leaving the battlefield.').
card_ruling('voidmage apprentice', '2013-04-15', 'If a spell with split second is on the stack, you can still respond by turning this creature face up and targeting that spell with the trigger. This is because split second only stops players from casting spells or activating abilities, while turning a creature face up is a special action.').

card_ruling('voidmage husher', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('voidslime', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('voidstone gargoyle', '2007-02-01', 'Copies of the named card can be cast (such as with Isochron Scepter).').
card_ruling('voidstone gargoyle', '2007-02-01', 'The named card can be cast face down.').
card_ruling('voidstone gargoyle', '2007-02-01', 'You can name either half of a split card, but not both. If you do so, that half (and both halves, if the split card has fuse) can\'t be cast. The other half is unaffected.').
card_ruling('voidstone gargoyle', '2007-02-01', 'Voidstone Gargoyle stops activated abilities that are mana abilities from being activated, unlike Pithing Needle.').
card_ruling('voidstone gargoyle', '2007-07-15', 'Activated abilities of copies of a spell with that name (such as Lightning Storm) cannot be activated.').
card_ruling('voidstone gargoyle', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('voidwalk', '2013-04-15', 'The spell with cipher is encoded on the creature as part of that spell\'s resolution, just after the spell\'s other effects. That card goes directly from the stack to exile. It never goes to the graveyard.').
card_ruling('voidwalk', '2013-04-15', 'You choose the creature as the spell resolves. The cipher ability doesn\'t target that creature, although the spell with cipher may target that creature (or a different creature) because of its other abilities.').
card_ruling('voidwalk', '2013-04-15', 'If the spell with cipher is countered, none of its effects will happen, including cipher. The card will go to its owner\'s graveyard and won\'t be encoded on a creature.').
card_ruling('voidwalk', '2013-04-15', 'If the creature leaves the battlefield, the exiled card will no longer be encoded on any creature. It will stay exiled.').
card_ruling('voidwalk', '2013-04-15', 'If you want to encode the card with cipher onto a noncreature permanent such as a Keyrune that can turn into a creature, that permanent has to be a creature before the spell with cipher starts resolving. You can choose only a creature to encode the card onto.').
card_ruling('voidwalk', '2013-04-15', 'The copy of the card with cipher is created in and cast from exile.').
card_ruling('voidwalk', '2013-04-15', 'You cast the copy of the card with cipher during the resolution of the triggered ability. Ignore timing restrictions based on the card\'s type.').
card_ruling('voidwalk', '2013-04-15', 'If you choose not to cast the copy, or you can\'t cast it (perhaps because there are no legal targets available), the copy will cease to exist the next time state-based actions are performed. You won\'t get a chance to cast the copy at a later time.').
card_ruling('voidwalk', '2013-04-15', 'The exiled card with cipher grants a triggered ability to the creature it\'s encoded on. If that creature loses that ability and subsequently deals combat damage to a player, the triggered ability won\'t trigger. However, the exiled card will continue to be encoded on that creature.').
card_ruling('voidwalk', '2013-04-15', 'If another player gains control of the creature, that player will control the triggered ability. That player will create a copy of the encoded card and may cast it.').
card_ruling('voidwalk', '2013-04-15', 'If a creature with an encoded card deals combat damage to more than one player simultaneously (perhaps because some of the combat damage was redirected), the triggered ability will trigger once for each player it deals combat damage to. Each ability will create a copy of the exiled card and allow you to cast it.').

card_ruling('voidwielder', '2012-10-01', 'If Voidwielder is the only creature on the battlefield, you\'ll have to choose it as the target of its ability, although you may choose to not return it to its owner\'s hand.').

card_ruling('volatile rig', '2012-10-01', 'If Volatile Rig is dealt damage by multiple sources at the same time (for example, multiple blocking creatures), its first triggered ability will trigger only once.').

card_ruling('volcanic awakening', '2013-06-07', 'The copies are put directly onto the stack. They aren\'t cast and won\'t be counted by other spells with storm cast later in the turn.').
card_ruling('volcanic awakening', '2013-06-07', 'Spells cast from zones other than a player\'s hand and spells that were countered are counted by the storm ability.').
card_ruling('volcanic awakening', '2013-06-07', 'A copy of a spell can be countered like any other spell, but it must be countered individually. Countering a spell with storm won\'t affect the copies.').
card_ruling('volcanic awakening', '2013-06-07', 'The triggered ability that creates the copies can itself be countered by anything that can counter a triggered ability. If it is countered, no copies will be put onto the stack.').
card_ruling('volcanic awakening', '2013-06-07', 'You may choose new targets for any of the copies. You can choose differently for each copy.').

card_ruling('volcanic eruption', '2004-10-04', 'Can be used with X equal to zero. This is useful if no Mountains are on the battlefield.').

card_ruling('volcanic fallout', '2009-02-01', 'Volcanic Fallout can be targeted by spells that try to counter it (such as Countersquall). Those spells will still resolve, but the part of their effect that would counter Volcanic Fallout won\'t do anything. Any other effects those spells have will work as normal.').

card_ruling('volcanic island', '2008-10-01', 'This has the mana abilities associated with both of its basic land types.').
card_ruling('volcanic island', '2008-10-01', 'This has basic land types, but it isn\'t a basic land. Things that affect basic lands don\'t affect it. Things that affect basic land types do.').

card_ruling('volcanic offering', '2014-11-07', 'Unlike the other Offerings, you choose the opponents for Volcanic Offering as you cast the spell because those players are involved in choosing the spell’s targets.').
card_ruling('volcanic offering', '2014-11-07', 'The chosen opponent for each effect may choose the same target nonbasic land or creature you don’t control that you did.').
card_ruling('volcanic offering', '2014-11-07', 'You may choose the same opponent for each of the effects, or you may choose different opponents. None of the affected players are targets of the spell.').

card_ruling('volcanic rush', '2015-02-25', 'An “attacking creature” is one that has been declared as an attacker that combat, or one that was put onto the battlefield attacking this combat. Unless that creature leaves combat, it continues to be an attacking creature through the end of combat step, even if the player it was attacking has left the game, or the planeswalker it was attacking has left the battlefield.').
card_ruling('volcanic rush', '2015-02-25', 'Even though Volcanic Rush affects only attacking creatures, the bonus will remain for the rest of the turn.').
card_ruling('volcanic rush', '2015-02-25', 'Only creatures that are attacking as Volcanic Rush resolves will receive the bonus. In other words, casting it before you declare attackers usually won’t do anything.').

card_ruling('volcanic submersion', '2008-10-01', 'Cycling is an activated ability. Effects that interact with activated abilities (such as Stifle or Rings of Brighthearth) will interact with cycling. Effects that interact with spells (such as Remove Soul or Faerie Tauntings) will not.').

card_ruling('volcanic vision', '2015-02-25', 'If the target instant or sorcery card becomes an illegal target before Volcanic Vision resolves, the spell will be countered and none of its effects will happen. No damage will be dealt and Volcanic Vision won’t be exiled.').
card_ruling('volcanic vision', '2015-02-25', 'If the target has {X} in its mana cost, X is 0.').
card_ruling('volcanic vision', '2015-02-25', 'If the target is a split card, damage will be dealt only once to each creature your opponents control. The damage will be equal to the sum of the card’s two converted mana costs.').

card_ruling('volcanic wind', '2004-10-04', 'The value of X is determined at the time you announce this spell, and you choose the targets and the division of damage among those targets as part of the announcement as well.').
card_ruling('volcanic wind', '2004-10-04', 'If there are no creatures on the battlefield when you cast this, you do not pick any targets and it deals no damage.').
card_ruling('volcanic wind', '2004-10-04', 'If there is at least one creature on the battlefield when you are announcing this, you can\'t choose zero targets. You must choose between 1 and X targets.').

card_ruling('volcano hellion', '2007-02-01', 'As Volcano Hellion\'s enters-the-battlefield ability resolves, you choose a number. Volcano Hellion deals that amount of damage to you and to the targeted creature. You may choose 0.').
card_ruling('volcano hellion', '2007-02-01', 'Volcano Hellion\'s echo cost constantly changes; it\'s not locked in when it enters the battlefield. The echo cost you pay is equal your life total as the echo triggered ability resolves.').

card_ruling('volition reins', '2011-01-01', 'Volition Reins may target and may enchant an untapped permanent.').
card_ruling('volition reins', '2011-01-01', 'If the enchanted permanent is untapped as Volition Reins enters the battlefield, the triggered ability won\'t trigger at all.').
card_ruling('volition reins', '2011-01-01', 'Gaining control of a permanent doesn\'t cause you to gain control of any Auras or Equipment attached to it.').

card_ruling('volley of boulders', '2004-10-04', 'You can\'t choose zero targets. You must choose between 1 and 6 targets.').

card_ruling('volrath\'s curse', '2004-10-04', 'If the creature this card is on is sacrificed to this card, this card is put into the graveyard before the ability to unsummon it can be used.').
card_ruling('volrath\'s curse', '2013-07-01', 'Activated abilities contain a colon. They\'re generally written \"[Cost]: [Effect].\" Some keywords are activated abilities and will have colons in their reminder texts.').

card_ruling('volrath\'s laboratory', '2004-10-04', 'The token has the same name as the creature type.').
card_ruling('volrath\'s laboratory', '2004-10-04', 'The token does not have any built-in abilities. For example, choosing creature type Bird does not give the token Flying.').

card_ruling('volrath\'s shapeshifter', '2004-10-04', 'If the top card in the graveyard has any other undefined characteristics, then those characteristics are not acquired and the shapeshifter uses the characteristic from its own card.').
card_ruling('volrath\'s shapeshifter', '2004-10-04', 'If the top card in your graveyard is a creature card, this card uses the listed characteristics, including color and any other types. But, this card keeps the granted ability in addition to those from the card being copied.').
card_ruling('volrath\'s shapeshifter', '2004-10-04', 'When it changes forms, any \"enters the battlefield\" abilities of the card it \"copies\" do not trigger.').
card_ruling('volrath\'s shapeshifter', '2004-10-04', 'A copy of a Volrath\'s Shapeshifter will have the shapeshifting ability.').
card_ruling('volrath\'s shapeshifter', '2004-10-04', 'If the top card in your graveyard isn\'t a creature card (meaning a card with the type Creature, which may or may not have other types such as Artifact or Enchantment), then it\'s just a 0/1 blue creature. Older cards of type Summon are also Creature cards.').
card_ruling('volrath\'s shapeshifter', '2008-08-01', 'The ability that sets its characteristics is a text-changing effect.').

card_ruling('volt charge', '2011-06-01', 'If the creature or player is an illegal target when Volt Charge tries to resolve, it will be countered and none of its effects will happen. You won\'t proliferate.').

card_ruling('volunteer reserves', '2008-10-01', 'If a creature with banding attacks, it can team up with any number of other attacking creatures with banding (and up to one nonbanding creature) and attack as a unit called a \"band.\" The band can be blocked by any creature that could block a single creature in the band. Blocking any creature in a band blocks the entire band. If a creature with banding is blocked, the attacking player chooses how the blockers\' damage is assigned.').
card_ruling('volunteer reserves', '2008-10-01', 'A maximum of one nonbanding creature can join an attacking band no matter how many creatures with banding are in it.').
card_ruling('volunteer reserves', '2008-10-01', 'Creatures in the same band must all attack the same player or planeswalker.').
card_ruling('volunteer reserves', '2008-10-01', 'Paying cumulative upkeep is always optional. If it\'s not paid, the permanent with cumulative upkeep is sacrificed. Partial payments of the total cumulative upkeep cost can\'t be made. For example, if a permanent with \"cumulative upkeep {1}\" has three age counters on it when its cumulative upkeep ability triggers, it gets another age counter and then its controller chooses to either pay {4} or sacrifice the permanent.').
card_ruling('volunteer reserves', '2009-10-01', 'If a creature in combat has banding, its controller assigns damage for creatures blocking or blocked by it. That player can ignore the damage assignment order when making this assignment.').

card_ruling('voodoo doll', '2009-10-01', 'Voodoo Doll\'s second ability has an \"intervening \'if\' clause.\" That means (1) the ability won\'t trigger at all unless Voodoo Doll is untapped, and (2) the ability will do nothing if Voodoo Doll is tapped by the time it resolves. If Voodoo Doll is no longer on the battlefield as the ability resolves, its last existence on the battlefield is checked to see if it was tapped or untapped.').
card_ruling('voodoo doll', '2009-10-01', 'Each X in the cost of Voodoo Doll\'s third ability is the number of pin counters on Voodoo Doll at the time you activate the ability. The X in the ability\'s effect is the number of pin counters on Voodoo Doll at the time it resolves, which may not be the same number (if a spell or ability put counters on it or removed counters from it in the meantime). If Voodoo Doll is no longer on the battlefield as the ability resolves, its last existence on the battlefield is checked to see how many pin counters were on it.').
card_ruling('voodoo doll', '2011-01-01', 'Voodoo Doll\'s second ability causes it to be destroyed regardless of whether it actually deals damage to you or not. Voodoo Doll is destroyed even if all the damage is prevented or redirected.').

card_ruling('voracious dragon', '2009-02-01', 'Voracious Dragon can devour any type of creatures, not just Goblins.').
card_ruling('voracious dragon', '2009-02-01', '\"The number of Goblins it devoured\" means \"The number of Goblins sacrificed as a result of its devour ability as it entered the battlefield.\" For each creature that Voracious Dragon devoured, this ability checks its creature type as it last existed on the battlefield to see if it was a Goblin at that time.').

card_ruling('voracious hatchling', '2008-08-01', 'If you cast a spell that\'s both of the listed colors, both abilities will trigger. You\'ll remove a total of two -1/-1 counters from the Hatchling.').
card_ruling('voracious hatchling', '2008-08-01', 'If there are no -1/-1 counters on it when the triggered ability resolves, the ability does nothing. There is no penalty for not being able to remove a counter.').

card_ruling('voracious wurm', '2013-07-01', 'Voracious Wurm’s ability checks how much life you’ve gained during the turn, not what your life total is compared to what it was when the turn began. For example, if you start the turn at 10 life, gain 6 life during the turn, then lose 6 life later that turn, Voracious Wurm will enter the battlefield with six +1/+1 counters.').
card_ruling('voracious wurm', '2013-07-01', 'In a Two-Headed Giant game, life gained by your teammate isn’t considered, even though it causes your team’s life total to increase.').

card_ruling('vorel of the hull clade', '2013-04-15', 'The effect of Vorel\'s ability will essentially double the counters on the target artifact, creature, or land. For example, if a creature has three +1/+1 counters and a divinity counter on it before the ability resolves, it will have six +1/+1 counters and two divinity counters on it after the ability resolves.').

card_ruling('vortex elemental', '2014-02-01', 'If no creature is blocking or being blocked by Vortex Elemental as its first ability resolves, it alone will be put on top of its owner’s library, and that library will be shuffled. You can activate the first ability outside of combat.').
card_ruling('vortex elemental', '2014-02-01', 'If Vortex Elemental isn’t on the battlefield when its first ability resolves, any creatures blocking or blocked by it when it left the battlefield will be put on top of their owners’ libraries and those libraries will be shuffled.').
card_ruling('vortex elemental', '2014-02-01', 'The creature blocks Vortex Elemental only if it’s able to do so as the declare blockers step begins. If, at that time, the creature is tapped, it’s affected by a spell or ability that says it can’t block, or Vortex Elemental isn’t attacking its controller or a planeswalker controlled by that player, then it doesn’t block. If there’s a cost associated with having the creature block, the player isn’t forced to pay that cost. If that cost isn’t paid, the creature won’t block.').

card_ruling('vraska the unseen', '2012-10-01', 'If an effect creates a copy of one of the Assassin creature tokens, the copy will also have the triggered ability.').
card_ruling('vraska the unseen', '2012-10-01', 'Each Assassin token\'s triggered ability will trigger whenever it deals combat damage to any player, including you.').
card_ruling('vraska the unseen', '2013-07-01', 'Planeswalkers are permanents. You can cast one at the time you could cast a sorcery. When your planeswalker spell resolves, it enters the battlefield under your control.').
card_ruling('vraska the unseen', '2013-07-01', 'Planeswalkers are not creatures. Spells and abilities that affect creatures won’t affect them.').
card_ruling('vraska the unseen', '2013-07-01', 'Planeswalkers have loyalty. A planeswalker enters the battlefield with a number of loyalty counters on it equal to the number printed in its lower right corner. Activating one of its abilities may cause it to gain or lose loyalty counters. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it. If it has no loyalty counters on it, it’s put into its owner’s graveyard as a state-based action.').
card_ruling('vraska the unseen', '2013-07-01', 'Planeswalkers each have a number of activated abilities called “loyalty abilities.” You can activate a loyalty ability of a planeswalker you control only at the time you could cast a sorcery and only if you haven’t activated one of that planeswalker’s loyalty abilities yet that turn.').
card_ruling('vraska the unseen', '2013-07-01', 'The cost to activate a planeswalker’s loyalty ability is represented by a symbol with a number inside. Up-arrows contain positive numbers, such as “+1”; this means “Put one loyalty counter on this planeswalker.” Down-arrows contain negative numbers, such as “-7”; this means “Remove seven loyalty counters from this planeswalker.” A symbol with a “0” means “Put zero loyalty counters on this planeswalker.”').
card_ruling('vraska the unseen', '2013-07-01', 'You can’t activate a planeswalker’s ability with a negative loyalty cost unless the planeswalker has at least that many loyalty counters on it.').
card_ruling('vraska the unseen', '2013-07-01', 'Planeswalkers can’t attack (unless an effect turns the planeswalker into a creature). However, they can be attacked. Each of your attacking creatures can attack your opponent or a planeswalker that player controls. You say which as you declare attackers.').
card_ruling('vraska the unseen', '2013-07-01', 'If your planeswalkers are being attacked, you can block the attackers as normal.').
card_ruling('vraska the unseen', '2013-07-01', 'If a creature that’s attacking a planeswalker isn’t blocked, it’ll deal its combat damage to that planeswalker. Damage dealt to a planeswalker causes that many loyalty counters to be removed from it.').
card_ruling('vraska the unseen', '2013-07-01', 'If a source you control would deal noncombat damage to an opponent, you may have that source deal that damage to a planeswalker that opponent controls instead. For example, although you can’t target a planeswalker with Shock, you can target your opponent with Shock, and then as Shock resolves, choose to have Shock deal its 2 damage to one of your opponent’s planeswalkers. (You can’t split up that damage between different players and/or planeswalkers.) If you have Shock deal its damage to a planeswalker, two loyalty counters are removed from it.').
card_ruling('vraska the unseen', '2013-07-01', 'If a player controls two or more planeswalkers that share a planeswalker type, that player chooses one of them and the rest are put into their owners’ graveyards as a state-based action.').

card_ruling('vryn wingmare', '2015-06-22', 'The ability affects each spell that’s not a creature spell, including your own.').
card_ruling('vryn wingmare', '2015-06-22', 'The ability affects what you pay to cast each noncreature spell (its total cost), but it doesn’t change that spell’s mana cost or converted mana cost.').
card_ruling('vryn wingmare', '2015-06-22', 'When determining a spell’s total cost, effects that increase the cost are applied before effects that reduce the cost.').

card_ruling('vulshok battlemaster', '2004-12-01', 'The enters-the-battlefield effect moves all Equipment onto the Battlemaster, regardless of whether that Equipment was attached to other creatures.').
card_ruling('vulshok battlemaster', '2004-12-01', 'Other players\' Equipment is moved onto the Battlemaster as well as your own. This doesn\'t change who controls the Equipment or who can activate its equip ability to move it onto another creature.').
card_ruling('vulshok battlemaster', '2004-12-01', 'If an Equipment can\'t equip Vulshok Battlemaster, it isn\'t attached to the Battlemaster, and it doesn\'t become unattached (if it\'s attached to a creature).').

card_ruling('vulturous aven', '2015-02-25', 'A creature with exploit “exploits a creature” when the controller of the exploit ability sacrifices a creature as that ability resolves.').
card_ruling('vulturous aven', '2015-02-25', 'You choose whether to sacrifice a creature and which creature to sacrifice as the exploit ability resolves.').
card_ruling('vulturous aven', '2015-02-25', 'You can sacrifice the creature with exploit if it’s still on the battlefield. This will cause its other ability to trigger.').
card_ruling('vulturous aven', '2015-02-25', 'If the creature with exploit isn\'t on the battlefield as the exploit ability resolves, you won’t get any bonus from the creature with exploit, even if you sacrifice a creature. Because the creature with exploit isn’t on the battlefield, its other triggered ability won’t trigger.').
card_ruling('vulturous aven', '2015-02-25', 'You can’t sacrifice more than one creature to any one exploit ability.').

card_ruling('vulturous zombie', '2005-10-01', 'The ability doesn\'t care why a card goes to your opponent\'s graveyard -- only that it does. The ability triggers when a card is put into your opponent\'s graveyard from the stack (a spell resolves or is countered), from the battlefield (a permanent is destroyed or sacrificed), from the player\'s hand (a card is discarded), from the player\'s library (from a Millstone-like effect), or from any other zone.').
card_ruling('vulturous zombie', '2005-10-01', 'This ability triggers only on cards, so it won\'t trigger when a token is put into your opponent\'s graveyard.').

