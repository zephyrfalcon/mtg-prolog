% Mirrodin Besieged

set('MBS').
set_name('MBS', 'Mirrodin Besieged').
set_release_date('MBS', '2011-02-04').
set_border('MBS', 'black').
set_type('MBS', 'expansion').
set_block('MBS', 'Scars of Mirrodin').

card_in_set('accorder paladin', 'MBS').
card_original_type('accorder paladin'/'MBS', 'Creature — Human Knight').
card_original_text('accorder paladin'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_first_print('accorder paladin', 'MBS').
card_image_name('accorder paladin'/'MBS', 'accorder paladin').
card_uid('accorder paladin'/'MBS', 'MBS:Accorder Paladin:accorder paladin').
card_rarity('accorder paladin'/'MBS', 'Uncommon').
card_artist('accorder paladin'/'MBS', 'Kekai Kotaki').
card_number('accorder paladin'/'MBS', '1').
card_flavor_text('accorder paladin'/'MBS', '\"I fight for the suns, the surface, and everything in between.\"').
card_multiverse_id('accorder paladin'/'MBS', '213818').
card_watermark('accorder paladin'/'MBS', 'Mirran').

card_in_set('ardent recruit', 'MBS').
card_original_type('ardent recruit'/'MBS', 'Creature — Human Soldier').
card_original_text('ardent recruit'/'MBS', 'Metalcraft — Ardent Recruit gets +2/+2 as long as you control three or more artifacts.').
card_first_print('ardent recruit', 'MBS').
card_image_name('ardent recruit'/'MBS', 'ardent recruit').
card_uid('ardent recruit'/'MBS', 'MBS:Ardent Recruit:ardent recruit').
card_rarity('ardent recruit'/'MBS', 'Common').
card_artist('ardent recruit'/'MBS', 'Mike Bierek').
card_number('ardent recruit'/'MBS', '2').
card_flavor_text('ardent recruit'/'MBS', '\"We lost our homes and our kin. We won\'t let those rotters take our future as well.\"').
card_multiverse_id('ardent recruit'/'MBS', '213794').
card_watermark('ardent recruit'/'MBS', 'Mirran').

card_in_set('banishment decree', 'MBS').
card_original_type('banishment decree'/'MBS', 'Instant').
card_original_text('banishment decree'/'MBS', 'Put target artifact, creature, or enchantment on top of its owner\'s library.').
card_first_print('banishment decree', 'MBS').
card_image_name('banishment decree'/'MBS', 'banishment decree').
card_uid('banishment decree'/'MBS', 'MBS:Banishment Decree:banishment decree').
card_rarity('banishment decree'/'MBS', 'Common').
card_artist('banishment decree'/'MBS', 'James Ryman').
card_number('banishment decree'/'MBS', '3').
card_flavor_text('banishment decree'/'MBS', '\"Unworthy of consecration.\"\n—Axsh, lesser cenobite').
card_multiverse_id('banishment decree'/'MBS', '213792').
card_watermark('banishment decree'/'MBS', 'Phyrexian').

card_in_set('black sun\'s zenith', 'MBS').
card_original_type('black sun\'s zenith'/'MBS', 'Sorcery').
card_original_text('black sun\'s zenith'/'MBS', 'Put X -1/-1 counters on each creature. Shuffle Black Sun\'s Zenith into its owner\'s library.').
card_image_name('black sun\'s zenith'/'MBS', 'black sun\'s zenith').
card_uid('black sun\'s zenith'/'MBS', 'MBS:Black Sun\'s Zenith:black sun\'s zenith').
card_rarity('black sun\'s zenith'/'MBS', 'Rare').
card_artist('black sun\'s zenith'/'MBS', 'Daniel Ljunggren').
card_number('black sun\'s zenith'/'MBS', '39').
card_flavor_text('black sun\'s zenith'/'MBS', '\"Under the suns, Mirrodin kneels and begs us for perfection.\"\n—Geth, Lord of the Vault').
card_multiverse_id('black sun\'s zenith'/'MBS', '214061').
card_watermark('black sun\'s zenith'/'MBS', 'Phyrexian').

card_in_set('bladed sentinel', 'MBS').
card_original_type('bladed sentinel'/'MBS', 'Artifact Creature — Construct').
card_original_text('bladed sentinel'/'MBS', '{W}: Bladed Sentinel gains vigilance until end of turn.').
card_first_print('bladed sentinel', 'MBS').
card_image_name('bladed sentinel'/'MBS', 'bladed sentinel').
card_uid('bladed sentinel'/'MBS', 'MBS:Bladed Sentinel:bladed sentinel').
card_rarity('bladed sentinel'/'MBS', 'Common').
card_artist('bladed sentinel'/'MBS', 'Tomasz Jedruszek').
card_number('bladed sentinel'/'MBS', '98').
card_flavor_text('bladed sentinel'/'MBS', 'The Mirran partisans created hundreds of patrol sentinels to divert Phyrexian assaults from the Tangle.').
card_multiverse_id('bladed sentinel'/'MBS', '214049').
card_watermark('bladed sentinel'/'MBS', 'Mirran').

card_in_set('blightsteel colossus', 'MBS').
card_original_type('blightsteel colossus'/'MBS', 'Artifact Creature — Golem').
card_original_text('blightsteel colossus'/'MBS', 'Trample, infect\nBlightsteel Colossus is indestructible.\nIf Blightsteel Colossus would be put into a graveyard from anywhere, reveal Blightsteel Colossus and shuffle it into its owner\'s library instead.').
card_first_print('blightsteel colossus', 'MBS').
card_image_name('blightsteel colossus'/'MBS', 'blightsteel colossus').
card_uid('blightsteel colossus'/'MBS', 'MBS:Blightsteel Colossus:blightsteel colossus').
card_rarity('blightsteel colossus'/'MBS', 'Mythic Rare').
card_artist('blightsteel colossus'/'MBS', 'Chris Rahn').
card_number('blightsteel colossus'/'MBS', '99').
card_multiverse_id('blightsteel colossus'/'MBS', '221563').
card_watermark('blightsteel colossus'/'MBS', 'Phyrexian').

card_in_set('blightwidow', 'MBS').
card_original_type('blightwidow'/'MBS', 'Creature — Spider').
card_original_text('blightwidow'/'MBS', 'Reach (This creature can block creatures with flying.)\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('blightwidow', 'MBS').
card_image_name('blightwidow'/'MBS', 'blightwidow').
card_uid('blightwidow'/'MBS', 'MBS:Blightwidow:blightwidow').
card_rarity('blightwidow'/'MBS', 'Common').
card_artist('blightwidow'/'MBS', 'Daniel Ljunggren').
card_number('blightwidow'/'MBS', '77').
card_multiverse_id('blightwidow'/'MBS', '213820').
card_watermark('blightwidow'/'MBS', 'Phyrexian').

card_in_set('blisterstick shaman', 'MBS').
card_original_type('blisterstick shaman'/'MBS', 'Creature — Goblin Shaman').
card_original_text('blisterstick shaman'/'MBS', 'When Blisterstick Shaman enters the battlefield, it deals 1 damage to target creature or player.').
card_first_print('blisterstick shaman', 'MBS').
card_image_name('blisterstick shaman'/'MBS', 'blisterstick shaman').
card_uid('blisterstick shaman'/'MBS', 'MBS:Blisterstick Shaman:blisterstick shaman').
card_rarity('blisterstick shaman'/'MBS', 'Common').
card_artist('blisterstick shaman'/'MBS', 'Svetlin Velinov').
card_number('blisterstick shaman'/'MBS', '58').
card_flavor_text('blisterstick shaman'/'MBS', 'A productive warren requires a good deal of prodding.').
card_multiverse_id('blisterstick shaman'/'MBS', '213793').
card_watermark('blisterstick shaman'/'MBS', 'Mirran').

card_in_set('blue sun\'s zenith', 'MBS').
card_original_type('blue sun\'s zenith'/'MBS', 'Instant').
card_original_text('blue sun\'s zenith'/'MBS', 'Target player draws X cards. Shuffle Blue Sun\'s Zenith into its owner\'s library.').
card_first_print('blue sun\'s zenith', 'MBS').
card_image_name('blue sun\'s zenith'/'MBS', 'blue sun\'s zenith').
card_uid('blue sun\'s zenith'/'MBS', 'MBS:Blue Sun\'s Zenith:blue sun\'s zenith').
card_rarity('blue sun\'s zenith'/'MBS', 'Rare').
card_artist('blue sun\'s zenith'/'MBS', 'Izzy').
card_number('blue sun\'s zenith'/'MBS', '20').
card_flavor_text('blue sun\'s zenith'/'MBS', '\"The Origin Query will wait. We must ensure we survive to return to it.\"\n—Pelyus, vedalken ordinar').
card_multiverse_id('blue sun\'s zenith'/'MBS', '221556').
card_watermark('blue sun\'s zenith'/'MBS', 'Mirran').

card_in_set('bonehoard', 'MBS').
card_original_type('bonehoard'/'MBS', 'Artifact — Equipment').
card_original_text('bonehoard'/'MBS', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +X/+X, where X is the number of creature cards in all graveyards.\nEquip {2}').
card_first_print('bonehoard', 'MBS').
card_image_name('bonehoard'/'MBS', 'bonehoard').
card_uid('bonehoard'/'MBS', 'MBS:Bonehoard:bonehoard').
card_rarity('bonehoard'/'MBS', 'Rare').
card_artist('bonehoard'/'MBS', 'Chippy').
card_number('bonehoard'/'MBS', '100').
card_multiverse_id('bonehoard'/'MBS', '214040').
card_watermark('bonehoard'/'MBS', 'Phyrexian').

card_in_set('brass squire', 'MBS').
card_original_type('brass squire'/'MBS', 'Artifact Creature — Myr').
card_original_text('brass squire'/'MBS', '{T}: Attach target Equipment you control to target creature you control.').
card_first_print('brass squire', 'MBS').
card_image_name('brass squire'/'MBS', 'brass squire').
card_uid('brass squire'/'MBS', 'MBS:Brass Squire:brass squire').
card_rarity('brass squire'/'MBS', 'Uncommon').
card_artist('brass squire'/'MBS', 'Ryan Pancoast').
card_number('brass squire'/'MBS', '101').
card_flavor_text('brass squire'/'MBS', '\"I admire it. Few pull off pluck and subservience at the same time.\"\n—Ezuri, renegade leader').
card_multiverse_id('brass squire'/'MBS', '213800').
card_watermark('brass squire'/'MBS', 'Mirran').

card_in_set('burn the impure', 'MBS').
card_original_type('burn the impure'/'MBS', 'Instant').
card_original_text('burn the impure'/'MBS', 'Burn the Impure deals 3 damage to target creature. If that creature has infect, Burn the Impure deals 3 damage to that creature\'s controller.').
card_first_print('burn the impure', 'MBS').
card_image_name('burn the impure'/'MBS', 'burn the impure').
card_uid('burn the impure'/'MBS', 'MBS:Burn the Impure:burn the impure').
card_rarity('burn the impure'/'MBS', 'Common').
card_artist('burn the impure'/'MBS', 'Nic Klein').
card_number('burn the impure'/'MBS', '59').
card_flavor_text('burn the impure'/'MBS', 'Flame doesn\'t kneel to Phyrexia.').
card_multiverse_id('burn the impure'/'MBS', '213771').
card_watermark('burn the impure'/'MBS', 'Mirran').

card_in_set('caustic hound', 'MBS').
card_original_type('caustic hound'/'MBS', 'Creature — Hound').
card_original_text('caustic hound'/'MBS', 'When Caustic Hound is put into a graveyard from the battlefield, each player loses 4 life.').
card_first_print('caustic hound', 'MBS').
card_image_name('caustic hound'/'MBS', 'caustic hound').
card_uid('caustic hound'/'MBS', 'MBS:Caustic Hound:caustic hound').
card_rarity('caustic hound'/'MBS', 'Common').
card_artist('caustic hound'/'MBS', 'Dave Allsop').
card_number('caustic hound'/'MBS', '40').
card_flavor_text('caustic hound'/'MBS', 'At first, the Mirrans aimed for its exposed gut. The survivors quickly learned to do otherwise.').
card_multiverse_id('caustic hound'/'MBS', '213789').
card_watermark('caustic hound'/'MBS', 'Phyrexian').

card_in_set('choking fumes', 'MBS').
card_original_type('choking fumes'/'MBS', 'Instant').
card_original_text('choking fumes'/'MBS', 'Put a -1/-1 counter on each attacking creature.').
card_first_print('choking fumes', 'MBS').
card_image_name('choking fumes'/'MBS', 'choking fumes').
card_uid('choking fumes'/'MBS', 'MBS:Choking Fumes:choking fumes').
card_rarity('choking fumes'/'MBS', 'Uncommon').
card_artist('choking fumes'/'MBS', 'Scott Chou').
card_number('choking fumes'/'MBS', '4').
card_flavor_text('choking fumes'/'MBS', '\"Fall to your knees and welcome our embrace.\"\n—Qal-Sha, Priest of Norn').
card_multiverse_id('choking fumes'/'MBS', '213808').
card_watermark('choking fumes'/'MBS', 'Phyrexian').

card_in_set('concussive bolt', 'MBS').
card_original_type('concussive bolt'/'MBS', 'Sorcery').
card_original_text('concussive bolt'/'MBS', 'Concussive Bolt deals 4 damage to target player.\nMetalcraft — If you control three or more artifacts, creatures that player controls can\'t block this turn.').
card_first_print('concussive bolt', 'MBS').
card_image_name('concussive bolt'/'MBS', 'concussive bolt').
card_uid('concussive bolt'/'MBS', 'MBS:Concussive Bolt:concussive bolt').
card_rarity('concussive bolt'/'MBS', 'Common').
card_artist('concussive bolt'/'MBS', 'Johann Bodin').
card_number('concussive bolt'/'MBS', '60').
card_flavor_text('concussive bolt'/'MBS', 'Shock one, awe the rest.').
card_multiverse_id('concussive bolt'/'MBS', '213756').
card_watermark('concussive bolt'/'MBS', 'Mirran').

card_in_set('consecrated sphinx', 'MBS').
card_original_type('consecrated sphinx'/'MBS', 'Creature — Sphinx').
card_original_text('consecrated sphinx'/'MBS', 'Flying\nWhenever an opponent draws a card, you may draw two cards.').
card_first_print('consecrated sphinx', 'MBS').
card_image_name('consecrated sphinx'/'MBS', 'consecrated sphinx').
card_uid('consecrated sphinx'/'MBS', 'MBS:Consecrated Sphinx:consecrated sphinx').
card_rarity('consecrated sphinx'/'MBS', 'Mythic Rare').
card_artist('consecrated sphinx'/'MBS', 'Mark Zug').
card_number('consecrated sphinx'/'MBS', '21').
card_flavor_text('consecrated sphinx'/'MBS', 'Blessed by the hands of Jin-Gitaxias.').
card_multiverse_id('consecrated sphinx'/'MBS', '214063').
card_watermark('consecrated sphinx'/'MBS', 'Phyrexian').

card_in_set('contested war zone', 'MBS').
card_original_type('contested war zone'/'MBS', 'Land').
card_original_text('contested war zone'/'MBS', 'Whenever a creature deals combat damage to you, that creature\'s controller gains control of Contested War Zone.\n{T}: Add {1} to your mana pool.\n{1}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_first_print('contested war zone', 'MBS').
card_image_name('contested war zone'/'MBS', 'contested war zone').
card_uid('contested war zone'/'MBS', 'MBS:Contested War Zone:contested war zone').
card_rarity('contested war zone'/'MBS', 'Rare').
card_artist('contested war zone'/'MBS', 'Scott Chou').
card_number('contested war zone'/'MBS', '144').
card_multiverse_id('contested war zone'/'MBS', '213775').
card_watermark('contested war zone'/'MBS', 'Mirran').

card_in_set('copper carapace', 'MBS').
card_original_type('copper carapace'/'MBS', 'Artifact — Equipment').
card_original_text('copper carapace'/'MBS', 'Equipped creature gets +2/+2 and can\'t block.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('copper carapace', 'MBS').
card_image_name('copper carapace'/'MBS', 'copper carapace').
card_uid('copper carapace'/'MBS', 'MBS:Copper Carapace:copper carapace').
card_rarity('copper carapace'/'MBS', 'Common').
card_artist('copper carapace'/'MBS', 'Franz Vohwinkel').
card_number('copper carapace'/'MBS', '102').
card_flavor_text('copper carapace'/'MBS', '\"We will fight as they do: our flesh protected behind metal.\"\n—Tae Aquil, Viridan weaponsmith').
card_multiverse_id('copper carapace'/'MBS', '213805').
card_watermark('copper carapace'/'MBS', 'Mirran').

card_in_set('core prowler', 'MBS').
card_original_type('core prowler'/'MBS', 'Artifact Creature — Horror').
card_original_text('core prowler'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Core Prowler is put into a graveyard from the battlefield, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('core prowler', 'MBS').
card_image_name('core prowler'/'MBS', 'core prowler').
card_uid('core prowler'/'MBS', 'MBS:Core Prowler:core prowler').
card_rarity('core prowler'/'MBS', 'Uncommon').
card_artist('core prowler'/'MBS', 'Dave Allsop').
card_number('core prowler'/'MBS', '103').
card_multiverse_id('core prowler'/'MBS', '213822').
card_watermark('core prowler'/'MBS', 'Phyrexian').

card_in_set('corrupted conscience', 'MBS').
card_original_type('corrupted conscience'/'MBS', 'Enchantment — Aura').
card_original_text('corrupted conscience'/'MBS', 'Enchant creature\nYou control enchanted creature.\nEnchanted creature has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('corrupted conscience', 'MBS').
card_image_name('corrupted conscience'/'MBS', 'corrupted conscience').
card_uid('corrupted conscience'/'MBS', 'MBS:Corrupted Conscience:corrupted conscience').
card_rarity('corrupted conscience'/'MBS', 'Uncommon').
card_artist('corrupted conscience'/'MBS', 'Jason Chan').
card_number('corrupted conscience'/'MBS', '22').
card_flavor_text('corrupted conscience'/'MBS', 'Karn\'s creation is now his master.').
card_multiverse_id('corrupted conscience'/'MBS', '214043').
card_watermark('corrupted conscience'/'MBS', 'Phyrexian').

card_in_set('creeping corrosion', 'MBS').
card_original_type('creeping corrosion'/'MBS', 'Sorcery').
card_original_text('creeping corrosion'/'MBS', 'Destroy all artifacts.').
card_first_print('creeping corrosion', 'MBS').
card_image_name('creeping corrosion'/'MBS', 'creeping corrosion').
card_uid('creeping corrosion'/'MBS', 'MBS:Creeping Corrosion:creeping corrosion').
card_rarity('creeping corrosion'/'MBS', 'Rare').
card_artist('creeping corrosion'/'MBS', 'Ryan Pancoast').
card_number('creeping corrosion'/'MBS', '78').
card_flavor_text('creeping corrosion'/'MBS', '\"We will reveal the futility of their heresy by showing them how fragile their relics are.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('creeping corrosion'/'MBS', '214029').
card_watermark('creeping corrosion'/'MBS', 'Phyrexian').

card_in_set('crush', 'MBS').
card_original_type('crush'/'MBS', 'Instant').
card_original_text('crush'/'MBS', 'Destroy target noncreature artifact.').
card_first_print('crush', 'MBS').
card_image_name('crush'/'MBS', 'crush').
card_uid('crush'/'MBS', 'MBS:Crush:crush').
card_rarity('crush'/'MBS', 'Common').
card_artist('crush'/'MBS', 'Matt Stewart').
card_number('crush'/'MBS', '61').
card_flavor_text('crush'/'MBS', 'A golem\'s hands know no tenderness.').
card_multiverse_id('crush'/'MBS', '213806').
card_watermark('crush'/'MBS', 'Mirran').

card_in_set('cryptoplasm', 'MBS').
card_original_type('cryptoplasm'/'MBS', 'Creature — Shapeshifter').
card_original_text('cryptoplasm'/'MBS', 'At the beginning of your upkeep, you may have Cryptoplasm become a copy of another target creature. If you do, Cryptoplasm gains this ability.').
card_first_print('cryptoplasm', 'MBS').
card_image_name('cryptoplasm'/'MBS', 'cryptoplasm').
card_uid('cryptoplasm'/'MBS', 'MBS:Cryptoplasm:cryptoplasm').
card_rarity('cryptoplasm'/'MBS', 'Rare').
card_artist('cryptoplasm'/'MBS', 'Eric Deschamps').
card_number('cryptoplasm'/'MBS', '23').
card_flavor_text('cryptoplasm'/'MBS', '\"If left in the enemy\'s shape too long, it might be lost to them.\"\n—Vy Covalt, Neurok agent').
card_multiverse_id('cryptoplasm'/'MBS', '214037').
card_watermark('cryptoplasm'/'MBS', 'Mirran').

card_in_set('darksteel plate', 'MBS').
card_original_type('darksteel plate'/'MBS', 'Artifact — Equipment').
card_original_text('darksteel plate'/'MBS', 'Darksteel Plate is indestructible.\nEquipped creature is indestructible.\nEquip {2}').
card_first_print('darksteel plate', 'MBS').
card_image_name('darksteel plate'/'MBS', 'darksteel plate').
card_uid('darksteel plate'/'MBS', 'MBS:Darksteel Plate:darksteel plate').
card_rarity('darksteel plate'/'MBS', 'Rare').
card_artist('darksteel plate'/'MBS', 'Daniel Ljunggren').
card_number('darksteel plate'/'MBS', '104').
card_flavor_text('darksteel plate'/'MBS', '\"If there can be no victory, then I will fight forever.\"\n—Koth of the Hammer').
card_multiverse_id('darksteel plate'/'MBS', '213749').
card_watermark('darksteel plate'/'MBS', 'Mirran').

card_in_set('decimator web', 'MBS').
card_original_type('decimator web'/'MBS', 'Artifact').
card_original_text('decimator web'/'MBS', '{4}, {T}: Target opponent loses 2 life, gets a poison counter, then puts the top six cards of his or her library into his or her graveyard.').
card_first_print('decimator web', 'MBS').
card_image_name('decimator web'/'MBS', 'decimator web').
card_uid('decimator web'/'MBS', 'MBS:Decimator Web:decimator web').
card_rarity('decimator web'/'MBS', 'Rare').
card_artist('decimator web'/'MBS', 'Daniel Ljunggren').
card_number('decimator web'/'MBS', '105').
card_flavor_text('decimator web'/'MBS', 'Mycosynth grew unfettered beneath the black lacuna, metastasizing into a matrix of noxious energy.').
card_multiverse_id('decimator web'/'MBS', '214055').
card_watermark('decimator web'/'MBS', 'Phyrexian').

card_in_set('distant memories', 'MBS').
card_original_type('distant memories'/'MBS', 'Sorcery').
card_original_text('distant memories'/'MBS', 'Search your library for a card, exile it, then shuffle your library. Any opponent may have you put that card into your hand. If no player does, you draw three cards.').
card_first_print('distant memories', 'MBS').
card_image_name('distant memories'/'MBS', 'distant memories').
card_uid('distant memories'/'MBS', 'MBS:Distant Memories:distant memories').
card_rarity('distant memories'/'MBS', 'Rare').
card_artist('distant memories'/'MBS', 'Karl Kopinski').
card_number('distant memories'/'MBS', '24').
card_flavor_text('distant memories'/'MBS', '\"The fleeting shadows of his primitive self have all but vanished.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('distant memories'/'MBS', '213765').
card_watermark('distant memories'/'MBS', 'Phyrexian').

card_in_set('divine offering', 'MBS').
card_original_type('divine offering'/'MBS', 'Instant').
card_original_text('divine offering'/'MBS', 'Destroy target artifact. You gain life equal to its converted mana cost.').
card_image_name('divine offering'/'MBS', 'divine offering').
card_uid('divine offering'/'MBS', 'MBS:Divine Offering:divine offering').
card_rarity('divine offering'/'MBS', 'Common').
card_artist('divine offering'/'MBS', 'Terese Nielsen').
card_number('divine offering'/'MBS', '5').
card_flavor_text('divine offering'/'MBS', '\"The Phyrexians are vulnerable. Do not despair. We will prevail!\"\n—Juryan, rebel leader').
card_multiverse_id('divine offering'/'MBS', '213760').
card_watermark('divine offering'/'MBS', 'Mirran').

card_in_set('dross ripper', 'MBS').
card_original_type('dross ripper'/'MBS', 'Artifact Creature — Hound').
card_original_text('dross ripper'/'MBS', '{2}{B}: Dross Ripper gets +1/+1 until end of turn.').
card_first_print('dross ripper', 'MBS').
card_image_name('dross ripper'/'MBS', 'dross ripper').
card_uid('dross ripper'/'MBS', 'MBS:Dross Ripper:dross ripper').
card_rarity('dross ripper'/'MBS', 'Common').
card_artist('dross ripper'/'MBS', 'David Rapoza').
card_number('dross ripper'/'MBS', '106').
card_flavor_text('dross ripper'/'MBS', '\"Such a creation serves no purpose other than exterminating every one of us.\"\n—Sadra Alic, Neurok strategist').
card_multiverse_id('dross ripper'/'MBS', '214032').
card_watermark('dross ripper'/'MBS', 'Phyrexian').

card_in_set('fangren marauder', 'MBS').
card_original_type('fangren marauder'/'MBS', 'Creature — Beast').
card_original_text('fangren marauder'/'MBS', 'Whenever an artifact is put into a graveyard from the battlefield, you may gain 5 life.').
card_first_print('fangren marauder', 'MBS').
card_image_name('fangren marauder'/'MBS', 'fangren marauder').
card_uid('fangren marauder'/'MBS', 'MBS:Fangren Marauder:fangren marauder').
card_rarity('fangren marauder'/'MBS', 'Common').
card_artist('fangren marauder'/'MBS', 'James Ryman').
card_number('fangren marauder'/'MBS', '79').
card_flavor_text('fangren marauder'/'MBS', '\"The fangren fight without comfort of any kind. We can ask no less of ourselves.\"\n—Tilien, Sylvok partisan').
card_multiverse_id('fangren marauder'/'MBS', '213732').
card_watermark('fangren marauder'/'MBS', 'Mirran').

card_in_set('flayer husk', 'MBS').
card_original_type('flayer husk'/'MBS', 'Artifact — Equipment').
card_original_text('flayer husk'/'MBS', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('flayer husk', 'MBS').
card_image_name('flayer husk'/'MBS', 'flayer husk').
card_uid('flayer husk'/'MBS', 'MBS:Flayer Husk:flayer husk').
card_rarity('flayer husk'/'MBS', 'Common').
card_artist('flayer husk'/'MBS', 'Igor Kieryluk').
card_number('flayer husk'/'MBS', '107').
card_multiverse_id('flayer husk'/'MBS', '221560').
card_watermark('flayer husk'/'MBS', 'Phyrexian').

card_in_set('flensermite', 'MBS').
card_original_type('flensermite'/'MBS', 'Creature — Gremlin').
card_original_text('flensermite'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('flensermite', 'MBS').
card_image_name('flensermite'/'MBS', 'flensermite').
card_uid('flensermite'/'MBS', 'MBS:Flensermite:flensermite').
card_rarity('flensermite'/'MBS', 'Common').
card_artist('flensermite'/'MBS', 'Dave Allsop').
card_number('flensermite'/'MBS', '41').
card_multiverse_id('flensermite'/'MBS', '213817').
card_watermark('flensermite'/'MBS', 'Phyrexian').

card_in_set('flesh-eater imp', 'MBS').
card_original_type('flesh-eater imp'/'MBS', 'Creature — Imp').
card_original_text('flesh-eater imp'/'MBS', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nSacrifice a creature: Flesh-Eater Imp gets +1/+1 until end of turn.').
card_first_print('flesh-eater imp', 'MBS').
card_image_name('flesh-eater imp'/'MBS', 'flesh-eater imp').
card_uid('flesh-eater imp'/'MBS', 'MBS:Flesh-Eater Imp:flesh-eater imp').
card_rarity('flesh-eater imp'/'MBS', 'Uncommon').
card_artist('flesh-eater imp'/'MBS', 'Johann Bodin').
card_number('flesh-eater imp'/'MBS', '42').
card_multiverse_id('flesh-eater imp'/'MBS', '213759').
card_watermark('flesh-eater imp'/'MBS', 'Phyrexian').

card_in_set('forest', 'MBS').
card_original_type('forest'/'MBS', 'Basic Land — Forest').
card_original_text('forest'/'MBS', 'G').
card_image_name('forest'/'MBS', 'forest1').
card_uid('forest'/'MBS', 'MBS:Forest:forest1').
card_rarity('forest'/'MBS', 'Basic Land').
card_artist('forest'/'MBS', 'Mark Tedin').
card_number('forest'/'MBS', '154').
card_multiverse_id('forest'/'MBS', '214031').

card_in_set('forest', 'MBS').
card_original_type('forest'/'MBS', 'Basic Land — Forest').
card_original_text('forest'/'MBS', 'G').
card_image_name('forest'/'MBS', 'forest2').
card_uid('forest'/'MBS', 'MBS:Forest:forest2').
card_rarity('forest'/'MBS', 'Basic Land').
card_artist('forest'/'MBS', 'Mark Tedin').
card_number('forest'/'MBS', '155').
card_multiverse_id('forest'/'MBS', '214075').

card_in_set('frantic salvage', 'MBS').
card_original_type('frantic salvage'/'MBS', 'Instant').
card_original_text('frantic salvage'/'MBS', 'Put any number of target artifact cards from your graveyard on top of your library.\nDraw a card.').
card_first_print('frantic salvage', 'MBS').
card_image_name('frantic salvage'/'MBS', 'frantic salvage').
card_uid('frantic salvage'/'MBS', 'MBS:Frantic Salvage:frantic salvage').
card_rarity('frantic salvage'/'MBS', 'Common').
card_artist('frantic salvage'/'MBS', 'Scott Chou').
card_number('frantic salvage'/'MBS', '6').
card_flavor_text('frantic salvage'/'MBS', '\"We will mourn when there is time. For now, we survive.\"').
card_multiverse_id('frantic salvage'/'MBS', '213791').
card_watermark('frantic salvage'/'MBS', 'Mirran').

card_in_set('fuel for the cause', 'MBS').
card_original_type('fuel for the cause'/'MBS', 'Instant').
card_original_text('fuel for the cause'/'MBS', 'Counter target spell, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('fuel for the cause', 'MBS').
card_image_name('fuel for the cause'/'MBS', 'fuel for the cause').
card_uid('fuel for the cause'/'MBS', 'MBS:Fuel for the Cause:fuel for the cause').
card_rarity('fuel for the cause'/'MBS', 'Common').
card_artist('fuel for the cause'/'MBS', 'Steven Belledin').
card_number('fuel for the cause'/'MBS', '25').
card_flavor_text('fuel for the cause'/'MBS', '\"Your ideas will be discarded and your will repurposed.\"').
card_multiverse_id('fuel for the cause'/'MBS', '213736').
card_watermark('fuel for the cause'/'MBS', 'Phyrexian').

card_in_set('galvanoth', 'MBS').
card_original_type('galvanoth'/'MBS', 'Creature — Beast').
card_original_text('galvanoth'/'MBS', 'At the beginning of your upkeep, you may look at the top card of your library. If it\'s an instant or sorcery card, you may cast it without paying its mana cost.').
card_first_print('galvanoth', 'MBS').
card_image_name('galvanoth'/'MBS', 'galvanoth').
card_uid('galvanoth'/'MBS', 'MBS:Galvanoth:galvanoth').
card_rarity('galvanoth'/'MBS', 'Rare').
card_artist('galvanoth'/'MBS', 'Kev Walker').
card_number('galvanoth'/'MBS', '62').
card_flavor_text('galvanoth'/'MBS', 'It chews open Mirrodin\'s husk and feeds on the outpouring of energy.').
card_multiverse_id('galvanoth'/'MBS', '214053').
card_watermark('galvanoth'/'MBS', 'Mirran').

card_in_set('glissa\'s courier', 'MBS').
card_original_type('glissa\'s courier'/'MBS', 'Creature — Horror').
card_original_text('glissa\'s courier'/'MBS', 'Mountainwalk').
card_first_print('glissa\'s courier', 'MBS').
card_image_name('glissa\'s courier'/'MBS', 'glissa\'s courier').
card_uid('glissa\'s courier'/'MBS', 'MBS:Glissa\'s Courier:glissa\'s courier').
card_rarity('glissa\'s courier'/'MBS', 'Common').
card_artist('glissa\'s courier'/'MBS', 'Dave Kendall').
card_number('glissa\'s courier'/'MBS', '80').
card_flavor_text('glissa\'s courier'/'MBS', '\"So, the survivors from Oxid Ridge are on the move. Let them come and witness predation in its purest form.\"\n—Glissa').
card_multiverse_id('glissa\'s courier'/'MBS', '213783').
card_watermark('glissa\'s courier'/'MBS', 'Phyrexian').

card_in_set('glissa, the traitor', 'MBS').
card_original_type('glissa, the traitor'/'MBS', 'Legendary Creature — Zombie Elf').
card_original_text('glissa, the traitor'/'MBS', 'First strike, deathtouch\nWhenever a creature an opponent controls is put into a graveyard from the battlefield, you may return target artifact card from your graveyard to your hand.').
card_image_name('glissa, the traitor'/'MBS', 'glissa, the traitor').
card_uid('glissa, the traitor'/'MBS', 'MBS:Glissa, the Traitor:glissa, the traitor').
card_rarity('glissa, the traitor'/'MBS', 'Mythic Rare').
card_artist('glissa, the traitor'/'MBS', 'Chris Rahn').
card_number('glissa, the traitor'/'MBS', '96').
card_multiverse_id('glissa, the traitor'/'MBS', '214072').
card_watermark('glissa, the traitor'/'MBS', 'Phyrexian').

card_in_set('gnathosaur', 'MBS').
card_original_type('gnathosaur'/'MBS', 'Creature — Lizard').
card_original_text('gnathosaur'/'MBS', 'Sacrifice an artifact: Gnathosaur gains trample until end of turn.').
card_first_print('gnathosaur', 'MBS').
card_image_name('gnathosaur'/'MBS', 'gnathosaur').
card_uid('gnathosaur'/'MBS', 'MBS:Gnathosaur:gnathosaur').
card_rarity('gnathosaur'/'MBS', 'Common').
card_artist('gnathosaur'/'MBS', 'Jason Chan').
card_number('gnathosaur'/'MBS', '63').
card_flavor_text('gnathosaur'/'MBS', 'Mirran creatures that could withstand the Phyrexian oil found an abundance of crunchy snacks.').
card_multiverse_id('gnathosaur'/'MBS', '213763').
card_watermark('gnathosaur'/'MBS', 'Mirran').

card_in_set('go for the throat', 'MBS').
card_original_type('go for the throat'/'MBS', 'Instant').
card_original_text('go for the throat'/'MBS', 'Destroy target nonartifact creature.').
card_image_name('go for the throat'/'MBS', 'go for the throat').
card_uid('go for the throat'/'MBS', 'MBS:Go for the Throat:go for the throat').
card_rarity('go for the throat'/'MBS', 'Uncommon').
card_artist('go for the throat'/'MBS', 'David Rapoza').
card_number('go for the throat'/'MBS', '43').
card_flavor_text('go for the throat'/'MBS', 'Having flesh is increasingly a liability on Mirrodin.').
card_multiverse_id('go for the throat'/'MBS', '213799').
card_watermark('go for the throat'/'MBS', 'Mirran').

card_in_set('goblin wardriver', 'MBS').
card_original_type('goblin wardriver'/'MBS', 'Creature — Goblin Warrior').
card_original_text('goblin wardriver'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_first_print('goblin wardriver', 'MBS').
card_image_name('goblin wardriver'/'MBS', 'goblin wardriver').
card_uid('goblin wardriver'/'MBS', 'MBS:Goblin Wardriver:goblin wardriver').
card_rarity('goblin wardriver'/'MBS', 'Uncommon').
card_artist('goblin wardriver'/'MBS', 'Chippy').
card_number('goblin wardriver'/'MBS', '64').
card_flavor_text('goblin wardriver'/'MBS', '\"A true warrior fights with whatever\'s handy.\"\n—Qerk of the Secret Warren').
card_multiverse_id('goblin wardriver'/'MBS', '213782').
card_watermark('goblin wardriver'/'MBS', 'Mirran').

card_in_set('gore vassal', 'MBS').
card_original_type('gore vassal'/'MBS', 'Creature — Hound').
card_original_text('gore vassal'/'MBS', 'Sacrifice Gore Vassal: Put a -1/-1 counter on target creature. Then if that creature\'s toughness is 1 or greater, regenerate it.').
card_first_print('gore vassal', 'MBS').
card_image_name('gore vassal'/'MBS', 'gore vassal').
card_uid('gore vassal'/'MBS', 'MBS:Gore Vassal:gore vassal').
card_rarity('gore vassal'/'MBS', 'Uncommon').
card_artist('gore vassal'/'MBS', 'Matt Cavotta').
card_number('gore vassal'/'MBS', '7').
card_flavor_text('gore vassal'/'MBS', '\"Rid them of their unfaithful organs. Bring new hearts to the unbelievers.\"\n—Tome of Machines, verse 1703').
card_multiverse_id('gore vassal'/'MBS', '213798').
card_watermark('gore vassal'/'MBS', 'Phyrexian').

card_in_set('green sun\'s zenith', 'MBS').
card_original_type('green sun\'s zenith'/'MBS', 'Sorcery').
card_original_text('green sun\'s zenith'/'MBS', 'Search your library for a green creature card with converted mana cost X or less, put it onto the battlefield, then shuffle your library. Shuffle Green Sun\'s Zenith into its owner\'s library.').
card_first_print('green sun\'s zenith', 'MBS').
card_image_name('green sun\'s zenith'/'MBS', 'green sun\'s zenith').
card_uid('green sun\'s zenith'/'MBS', 'MBS:Green Sun\'s Zenith:green sun\'s zenith').
card_rarity('green sun\'s zenith'/'MBS', 'Rare').
card_artist('green sun\'s zenith'/'MBS', 'David Rapoza').
card_number('green sun\'s zenith'/'MBS', '81').
card_flavor_text('green sun\'s zenith'/'MBS', 'As the green sun crowned, Phyrexian prophecies glowed on the Tree of Tales.').
card_multiverse_id('green sun\'s zenith'/'MBS', '221559').
card_watermark('green sun\'s zenith'/'MBS', 'Phyrexian').

card_in_set('gruesome encore', 'MBS').
card_original_type('gruesome encore'/'MBS', 'Sorcery').
card_original_text('gruesome encore'/'MBS', 'Put target creature card from an opponent\'s graveyard onto the battlefield under your control. It gains haste. Exile it at the beginning of the next end step. If that creature would leave the battlefield, exile it instead of putting it anywhere else.').
card_first_print('gruesome encore', 'MBS').
card_image_name('gruesome encore'/'MBS', 'gruesome encore').
card_uid('gruesome encore'/'MBS', 'MBS:Gruesome Encore:gruesome encore').
card_rarity('gruesome encore'/'MBS', 'Uncommon').
card_artist('gruesome encore'/'MBS', 'Adrian Smith').
card_number('gruesome encore'/'MBS', '44').
card_multiverse_id('gruesome encore'/'MBS', '214073').
card_watermark('gruesome encore'/'MBS', 'Phyrexian').

card_in_set('gust-skimmer', 'MBS').
card_original_type('gust-skimmer'/'MBS', 'Artifact Creature — Insect').
card_original_text('gust-skimmer'/'MBS', '{U}: Gust-Skimmer gains flying until end of turn.').
card_first_print('gust-skimmer', 'MBS').
card_image_name('gust-skimmer'/'MBS', 'gust-skimmer').
card_uid('gust-skimmer'/'MBS', 'MBS:Gust-Skimmer:gust-skimmer').
card_rarity('gust-skimmer'/'MBS', 'Common').
card_artist('gust-skimmer'/'MBS', 'Dan Scott').
card_number('gust-skimmer'/'MBS', '108').
card_flavor_text('gust-skimmer'/'MBS', 'Phyrexian smog clouds choked the skies, threatening creatures who couldn\'t comprehend the menace below.').
card_multiverse_id('gust-skimmer'/'MBS', '213768').
card_watermark('gust-skimmer'/'MBS', 'Mirran').

card_in_set('hellkite igniter', 'MBS').
card_original_type('hellkite igniter'/'MBS', 'Creature — Dragon').
card_original_text('hellkite igniter'/'MBS', 'Flying, haste\n{1}{R}: Hellkite Igniter gets +X/+0 until end of turn, where X is the number of artifacts you control.').
card_first_print('hellkite igniter', 'MBS').
card_image_name('hellkite igniter'/'MBS', 'hellkite igniter').
card_uid('hellkite igniter'/'MBS', 'MBS:Hellkite Igniter:hellkite igniter').
card_rarity('hellkite igniter'/'MBS', 'Rare').
card_artist('hellkite igniter'/'MBS', 'Jason Chan').
card_number('hellkite igniter'/'MBS', '65').
card_flavor_text('hellkite igniter'/'MBS', 'Its flight sets the sky itself on fire.').
card_multiverse_id('hellkite igniter'/'MBS', '214052').
card_watermark('hellkite igniter'/'MBS', 'Mirran').

card_in_set('hero of bladehold', 'MBS').
card_original_type('hero of bladehold'/'MBS', 'Creature — Human Knight').
card_original_text('hero of bladehold'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nWhenever Hero of Bladehold attacks, put two 1/1 white Soldier creature tokens onto the battlefield tapped and attacking.').
card_image_name('hero of bladehold'/'MBS', 'hero of bladehold').
card_uid('hero of bladehold'/'MBS', 'MBS:Hero of Bladehold:hero of bladehold').
card_rarity('hero of bladehold'/'MBS', 'Mythic Rare').
card_artist('hero of bladehold'/'MBS', 'Austin Hsu').
card_number('hero of bladehold'/'MBS', '8').
card_multiverse_id('hero of bladehold'/'MBS', '214064').
card_watermark('hero of bladehold'/'MBS', 'Mirran').

card_in_set('hero of oxid ridge', 'MBS').
card_original_type('hero of oxid ridge'/'MBS', 'Creature — Human Knight').
card_original_text('hero of oxid ridge'/'MBS', 'Haste\nBattle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nWhenever Hero of Oxid Ridge attacks, creatures with power 1 or less can\'t block this turn.').
card_first_print('hero of oxid ridge', 'MBS').
card_image_name('hero of oxid ridge'/'MBS', 'hero of oxid ridge').
card_uid('hero of oxid ridge'/'MBS', 'MBS:Hero of Oxid Ridge:hero of oxid ridge').
card_rarity('hero of oxid ridge'/'MBS', 'Mythic Rare').
card_artist('hero of oxid ridge'/'MBS', 'Eric Deschamps').
card_number('hero of oxid ridge'/'MBS', '66').
card_multiverse_id('hero of oxid ridge'/'MBS', '214028').
card_watermark('hero of oxid ridge'/'MBS', 'Mirran').

card_in_set('hexplate golem', 'MBS').
card_original_type('hexplate golem'/'MBS', 'Artifact Creature — Golem').
card_original_text('hexplate golem'/'MBS', '').
card_first_print('hexplate golem', 'MBS').
card_image_name('hexplate golem'/'MBS', 'hexplate golem').
card_uid('hexplate golem'/'MBS', 'MBS:Hexplate Golem:hexplate golem').
card_rarity('hexplate golem'/'MBS', 'Common').
card_artist('hexplate golem'/'MBS', 'Matt Cavotta').
card_number('hexplate golem'/'MBS', '109').
card_flavor_text('hexplate golem'/'MBS', '\"Use everything. Iron, rust, scrap . . .  even the ground must join our cause.\"\n—Ezuri, renegade leader').
card_multiverse_id('hexplate golem'/'MBS', '213807').
card_watermark('hexplate golem'/'MBS', 'Mirran').

card_in_set('horrifying revelation', 'MBS').
card_original_type('horrifying revelation'/'MBS', 'Sorcery').
card_original_text('horrifying revelation'/'MBS', 'Target player discards a card, then puts the top card of his or her library into his or her graveyard.').
card_first_print('horrifying revelation', 'MBS').
card_image_name('horrifying revelation'/'MBS', 'horrifying revelation').
card_uid('horrifying revelation'/'MBS', 'MBS:Horrifying Revelation:horrifying revelation').
card_rarity('horrifying revelation'/'MBS', 'Common').
card_artist('horrifying revelation'/'MBS', 'Shelly Wan').
card_number('horrifying revelation'/'MBS', '45').
card_flavor_text('horrifying revelation'/'MBS', '\"Ours is a glorious transmission! Behold a future where all bow to the Father of Machines.\"\n—Isila, Priest of Sheoldred').
card_multiverse_id('horrifying revelation'/'MBS', '213723').
card_watermark('horrifying revelation'/'MBS', 'Phyrexian').

card_in_set('ichor wellspring', 'MBS').
card_original_type('ichor wellspring'/'MBS', 'Artifact').
card_original_text('ichor wellspring'/'MBS', 'When Ichor Wellspring enters the battlefield or is put into a graveyard from the battlefield, draw a card.').
card_first_print('ichor wellspring', 'MBS').
card_image_name('ichor wellspring'/'MBS', 'ichor wellspring').
card_uid('ichor wellspring'/'MBS', 'MBS:Ichor Wellspring:ichor wellspring').
card_rarity('ichor wellspring'/'MBS', 'Common').
card_artist('ichor wellspring'/'MBS', 'Steven Belledin').
card_number('ichor wellspring'/'MBS', '110').
card_flavor_text('ichor wellspring'/'MBS', '\"Our glorious infection has taken hold.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('ichor wellspring'/'MBS', '213737').
card_watermark('ichor wellspring'/'MBS', 'Phyrexian').

card_in_set('inkmoth nexus', 'MBS').
card_original_type('inkmoth nexus'/'MBS', 'Land').
card_original_text('inkmoth nexus'/'MBS', '{T}: Add {1} to your mana pool.\n{1}: Inkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying and infect until end of turn. It\'s still a land. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('inkmoth nexus', 'MBS').
card_image_name('inkmoth nexus'/'MBS', 'inkmoth nexus').
card_uid('inkmoth nexus'/'MBS', 'MBS:Inkmoth Nexus:inkmoth nexus').
card_rarity('inkmoth nexus'/'MBS', 'Rare').
card_artist('inkmoth nexus'/'MBS', 'Jung Park').
card_number('inkmoth nexus'/'MBS', '145').
card_multiverse_id('inkmoth nexus'/'MBS', '213731').
card_watermark('inkmoth nexus'/'MBS', 'Phyrexian').

card_in_set('into the core', 'MBS').
card_original_type('into the core'/'MBS', 'Instant').
card_original_text('into the core'/'MBS', 'Exile two target artifacts.').
card_first_print('into the core', 'MBS').
card_image_name('into the core'/'MBS', 'into the core').
card_uid('into the core'/'MBS', 'MBS:Into the Core:into the core').
card_rarity('into the core'/'MBS', 'Uncommon').
card_artist('into the core'/'MBS', 'Whit Brachna').
card_number('into the core'/'MBS', '67').
card_flavor_text('into the core'/'MBS', '\"They believe they\'re driving us back, but we\'re leading them to their doom.\"\n—Kethek, furnace stoker').
card_multiverse_id('into the core'/'MBS', '213788').
card_watermark('into the core'/'MBS', 'Phyrexian').

card_in_set('island', 'MBS').
card_original_type('island'/'MBS', 'Basic Land — Island').
card_original_text('island'/'MBS', 'U').
card_image_name('island'/'MBS', 'island1').
card_uid('island'/'MBS', 'MBS:Island:island1').
card_rarity('island'/'MBS', 'Basic Land').
card_artist('island'/'MBS', 'Jung Park').
card_number('island'/'MBS', '148').
card_multiverse_id('island'/'MBS', '220369').

card_in_set('island', 'MBS').
card_original_type('island'/'MBS', 'Basic Land — Island').
card_original_text('island'/'MBS', 'U').
card_image_name('island'/'MBS', 'island2').
card_uid('island'/'MBS', 'MBS:Island:island2').
card_rarity('island'/'MBS', 'Basic Land').
card_artist('island'/'MBS', 'Jung Park').
card_number('island'/'MBS', '149').
card_multiverse_id('island'/'MBS', '220365').

card_in_set('kemba\'s legion', 'MBS').
card_original_type('kemba\'s legion'/'MBS', 'Creature — Cat Soldier').
card_original_text('kemba\'s legion'/'MBS', 'Vigilance\nKemba\'s Legion can block an additional creature for each Equipment attached to Kemba\'s Legion.').
card_first_print('kemba\'s legion', 'MBS').
card_image_name('kemba\'s legion'/'MBS', 'kemba\'s legion').
card_uid('kemba\'s legion'/'MBS', 'MBS:Kemba\'s Legion:kemba\'s legion').
card_rarity('kemba\'s legion'/'MBS', 'Uncommon').
card_artist('kemba\'s legion'/'MBS', 'Anthony Francisco').
card_number('kemba\'s legion'/'MBS', '9').
card_flavor_text('kemba\'s legion'/'MBS', 'The squabble over succession was quickly replaced by the struggle to survive.').
card_multiverse_id('kemba\'s legion'/'MBS', '214056').
card_watermark('kemba\'s legion'/'MBS', 'Mirran').

card_in_set('knowledge pool', 'MBS').
card_original_type('knowledge pool'/'MBS', 'Artifact').
card_original_text('knowledge pool'/'MBS', 'Imprint — When Knowledge Pool enters the battlefield, each player exiles the top three cards of his or her library.\nWhenever a player casts a spell from his or her hand, that player exiles it. If the player does, he or she may cast another nonland card exiled with Knowledge Pool without paying that card\'s mana cost.').
card_first_print('knowledge pool', 'MBS').
card_image_name('knowledge pool'/'MBS', 'knowledge pool').
card_uid('knowledge pool'/'MBS', 'MBS:Knowledge Pool:knowledge pool').
card_rarity('knowledge pool'/'MBS', 'Rare').
card_artist('knowledge pool'/'MBS', 'Mike Bierek').
card_number('knowledge pool'/'MBS', '111').
card_multiverse_id('knowledge pool'/'MBS', '214035').
card_watermark('knowledge pool'/'MBS', 'Mirran').

card_in_set('koth\'s courier', 'MBS').
card_original_type('koth\'s courier'/'MBS', 'Creature — Human Rogue').
card_original_text('koth\'s courier'/'MBS', 'Forestwalk').
card_first_print('koth\'s courier', 'MBS').
card_image_name('koth\'s courier'/'MBS', 'koth\'s courier').
card_uid('koth\'s courier'/'MBS', 'MBS:Koth\'s Courier:koth\'s courier').
card_rarity('koth\'s courier'/'MBS', 'Common').
card_artist('koth\'s courier'/'MBS', 'Wayne Reynolds').
card_number('koth\'s courier'/'MBS', '68').
card_flavor_text('koth\'s courier'/'MBS', 'Koth sent partisans into the Tangle to bring survivors to the safe haven of the tunnels below Kuldotha.').
card_multiverse_id('koth\'s courier'/'MBS', '213766').
card_watermark('koth\'s courier'/'MBS', 'Mirran').

card_in_set('kuldotha flamefiend', 'MBS').
card_original_type('kuldotha flamefiend'/'MBS', 'Creature — Elemental').
card_original_text('kuldotha flamefiend'/'MBS', 'When Kuldotha Flamefiend enters the battlefield, you may sacrifice an artifact. If you do, Kuldotha Flamefiend deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_first_print('kuldotha flamefiend', 'MBS').
card_image_name('kuldotha flamefiend'/'MBS', 'kuldotha flamefiend').
card_uid('kuldotha flamefiend'/'MBS', 'MBS:Kuldotha Flamefiend:kuldotha flamefiend').
card_rarity('kuldotha flamefiend'/'MBS', 'Uncommon').
card_artist('kuldotha flamefiend'/'MBS', 'Raymond Swanland').
card_number('kuldotha flamefiend'/'MBS', '69').
card_multiverse_id('kuldotha flamefiend'/'MBS', '213810').
card_watermark('kuldotha flamefiend'/'MBS', 'Mirran').

card_in_set('kuldotha ringleader', 'MBS').
card_original_type('kuldotha ringleader'/'MBS', 'Creature — Giant Berserker').
card_original_text('kuldotha ringleader'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nKuldotha Ringleader attacks each turn if able.').
card_first_print('kuldotha ringleader', 'MBS').
card_image_name('kuldotha ringleader'/'MBS', 'kuldotha ringleader').
card_uid('kuldotha ringleader'/'MBS', 'MBS:Kuldotha Ringleader:kuldotha ringleader').
card_rarity('kuldotha ringleader'/'MBS', 'Common').
card_artist('kuldotha ringleader'/'MBS', 'Greg Staples').
card_number('kuldotha ringleader'/'MBS', '70').
card_flavor_text('kuldotha ringleader'/'MBS', 'Being surrounded by goblins is less objectionable when they\'re fighting for you.').
card_multiverse_id('kuldotha ringleader'/'MBS', '213787').
card_watermark('kuldotha ringleader'/'MBS', 'Mirran').

card_in_set('lead the stampede', 'MBS').
card_original_type('lead the stampede'/'MBS', 'Sorcery').
card_original_text('lead the stampede'/'MBS', 'Look at the top five cards of your library. You may reveal any number of creature cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('lead the stampede', 'MBS').
card_image_name('lead the stampede'/'MBS', 'lead the stampede').
card_uid('lead the stampede'/'MBS', 'MBS:Lead the Stampede:lead the stampede').
card_rarity('lead the stampede'/'MBS', 'Uncommon').
card_artist('lead the stampede'/'MBS', 'Efrem Palacios').
card_number('lead the stampede'/'MBS', '82').
card_multiverse_id('lead the stampede'/'MBS', '213739').
card_watermark('lead the stampede'/'MBS', 'Mirran').

card_in_set('leonin relic-warder', 'MBS').
card_original_type('leonin relic-warder'/'MBS', 'Creature — Cat Cleric').
card_original_text('leonin relic-warder'/'MBS', 'When Leonin Relic-Warder enters the battlefield, you may exile target artifact or enchantment.\nWhen Leonin Relic-Warder leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_first_print('leonin relic-warder', 'MBS').
card_image_name('leonin relic-warder'/'MBS', 'leonin relic-warder').
card_uid('leonin relic-warder'/'MBS', 'MBS:Leonin Relic-Warder:leonin relic-warder').
card_rarity('leonin relic-warder'/'MBS', 'Uncommon').
card_artist('leonin relic-warder'/'MBS', 'Greg Staples').
card_number('leonin relic-warder'/'MBS', '10').
card_multiverse_id('leonin relic-warder'/'MBS', '222860').
card_watermark('leonin relic-warder'/'MBS', 'Mirran').

card_in_set('leonin skyhunter', 'MBS').
card_original_type('leonin skyhunter'/'MBS', 'Creature — Cat Knight').
card_original_text('leonin skyhunter'/'MBS', 'Flying').
card_image_name('leonin skyhunter'/'MBS', 'leonin skyhunter').
card_uid('leonin skyhunter'/'MBS', 'MBS:Leonin Skyhunter:leonin skyhunter').
card_rarity('leonin skyhunter'/'MBS', 'Common').
card_artist('leonin skyhunter'/'MBS', 'Jana Schirmer & Johannes Voss').
card_number('leonin skyhunter'/'MBS', '11').
card_flavor_text('leonin skyhunter'/'MBS', '\"The infection has spread farther than we could glimpse from the heights of Taj-Nar.\"').
card_multiverse_id('leonin skyhunter'/'MBS', '213770').
card_watermark('leonin skyhunter'/'MBS', 'Mirran').

card_in_set('loxodon partisan', 'MBS').
card_original_type('loxodon partisan'/'MBS', 'Creature — Elephant Soldier').
card_original_text('loxodon partisan'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_first_print('loxodon partisan', 'MBS').
card_image_name('loxodon partisan'/'MBS', 'loxodon partisan').
card_uid('loxodon partisan'/'MBS', 'MBS:Loxodon Partisan:loxodon partisan').
card_rarity('loxodon partisan'/'MBS', 'Common').
card_artist('loxodon partisan'/'MBS', 'Matt Stewart').
card_number('loxodon partisan'/'MBS', '12').
card_flavor_text('loxodon partisan'/'MBS', '\"This war is not about loxodon or leonin, Sylvok or Auriok. To defeat these rotters, we must do it together.\"').
card_multiverse_id('loxodon partisan'/'MBS', '213761').
card_watermark('loxodon partisan'/'MBS', 'Mirran').

card_in_set('lumengrid gargoyle', 'MBS').
card_original_type('lumengrid gargoyle'/'MBS', 'Artifact Creature — Gargoyle').
card_original_text('lumengrid gargoyle'/'MBS', 'Flying').
card_first_print('lumengrid gargoyle', 'MBS').
card_image_name('lumengrid gargoyle'/'MBS', 'lumengrid gargoyle').
card_uid('lumengrid gargoyle'/'MBS', 'MBS:Lumengrid Gargoyle:lumengrid gargoyle').
card_rarity('lumengrid gargoyle'/'MBS', 'Uncommon').
card_artist('lumengrid gargoyle'/'MBS', 'Randis Albion').
card_number('lumengrid gargoyle'/'MBS', '112').
card_flavor_text('lumengrid gargoyle'/'MBS', '\"Anything that watches without sleep and fights without fear is a valuable ally against the Phyrexians.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('lumengrid gargoyle'/'MBS', '214059').
card_watermark('lumengrid gargoyle'/'MBS', 'Mirran').

card_in_set('magnetic mine', 'MBS').
card_original_type('magnetic mine'/'MBS', 'Artifact').
card_original_text('magnetic mine'/'MBS', 'Whenever another artifact is put into a graveyard from the battlefield, Magnetic Mine deals 2 damage to that artifact\'s controller.').
card_first_print('magnetic mine', 'MBS').
card_image_name('magnetic mine'/'MBS', 'magnetic mine').
card_uid('magnetic mine'/'MBS', 'MBS:Magnetic Mine:magnetic mine').
card_rarity('magnetic mine'/'MBS', 'Rare').
card_artist('magnetic mine'/'MBS', 'David Rapoza').
card_number('magnetic mine'/'MBS', '113').
card_multiverse_id('magnetic mine'/'MBS', '213740').
card_watermark('magnetic mine'/'MBS', 'Phyrexian').

card_in_set('massacre wurm', 'MBS').
card_original_type('massacre wurm'/'MBS', 'Creature — Wurm').
card_original_text('massacre wurm'/'MBS', 'When Massacre Wurm enters the battlefield, creatures your opponents control get -2/-2 until end of turn.\nWhenever a creature an opponent controls is put into a graveyard from the battlefield, that player loses 2 life.').
card_first_print('massacre wurm', 'MBS').
card_image_name('massacre wurm'/'MBS', 'massacre wurm').
card_uid('massacre wurm'/'MBS', 'MBS:Massacre Wurm:massacre wurm').
card_rarity('massacre wurm'/'MBS', 'Mythic Rare').
card_artist('massacre wurm'/'MBS', 'Jason Chan').
card_number('massacre wurm'/'MBS', '46').
card_multiverse_id('massacre wurm'/'MBS', '214044').
card_watermark('massacre wurm'/'MBS', 'Phyrexian').

card_in_set('master\'s call', 'MBS').
card_original_type('master\'s call'/'MBS', 'Instant').
card_original_text('master\'s call'/'MBS', 'Put two 1/1 colorless Myr artifact creature tokens onto the battlefield.').
card_image_name('master\'s call'/'MBS', 'master\'s call').
card_uid('master\'s call'/'MBS', 'MBS:Master\'s Call:master\'s call').
card_rarity('master\'s call'/'MBS', 'Common').
card_artist('master\'s call'/'MBS', 'David Rapoza').
card_number('master\'s call'/'MBS', '13').
card_flavor_text('master\'s call'/'MBS', 'The need to obey was inscribed on every plate in their bodies.').
card_multiverse_id('master\'s call'/'MBS', '221554').
card_watermark('master\'s call'/'MBS', 'Mirran').

card_in_set('melira\'s keepers', 'MBS').
card_original_type('melira\'s keepers'/'MBS', 'Creature — Human Warrior').
card_original_text('melira\'s keepers'/'MBS', 'Melira\'s Keepers can\'t have counters placed on it.').
card_first_print('melira\'s keepers', 'MBS').
card_image_name('melira\'s keepers'/'MBS', 'melira\'s keepers').
card_uid('melira\'s keepers'/'MBS', 'MBS:Melira\'s Keepers:melira\'s keepers').
card_rarity('melira\'s keepers'/'MBS', 'Uncommon').
card_artist('melira\'s keepers'/'MBS', 'Eric Deschamps').
card_number('melira\'s keepers'/'MBS', '83').
card_flavor_text('melira\'s keepers'/'MBS', 'Her warriors are the last defense against the coming storm.').
card_multiverse_id('melira\'s keepers'/'MBS', '213796').
card_watermark('melira\'s keepers'/'MBS', 'Mirran').

card_in_set('metallic mastery', 'MBS').
card_original_type('metallic mastery'/'MBS', 'Sorcery').
card_original_text('metallic mastery'/'MBS', 'Gain control of target artifact until end of turn. Untap that artifact. It gains haste until end of turn.').
card_first_print('metallic mastery', 'MBS').
card_image_name('metallic mastery'/'MBS', 'metallic mastery').
card_uid('metallic mastery'/'MBS', 'MBS:Metallic Mastery:metallic mastery').
card_rarity('metallic mastery'/'MBS', 'Uncommon').
card_artist('metallic mastery'/'MBS', 'Erica Yang').
card_number('metallic mastery'/'MBS', '71').
card_flavor_text('metallic mastery'/'MBS', '\"All will be reforged in the fires of glory.\"\n—Urabrask the Hidden').
card_multiverse_id('metallic mastery'/'MBS', '213812').
card_watermark('metallic mastery'/'MBS', 'Phyrexian').

card_in_set('mirran crusader', 'MBS').
card_original_type('mirran crusader'/'MBS', 'Creature — Human Knight').
card_original_text('mirran crusader'/'MBS', 'Double strike, protection from black and from green').
card_image_name('mirran crusader'/'MBS', 'mirran crusader').
card_uid('mirran crusader'/'MBS', 'MBS:Mirran Crusader:mirran crusader').
card_rarity('mirran crusader'/'MBS', 'Rare').
card_artist('mirran crusader'/'MBS', 'Eric Deschamps').
card_number('mirran crusader'/'MBS', '14').
card_flavor_text('mirran crusader'/'MBS', 'A symbol of what Mirrodin once was and hope for what it will be again.').
card_multiverse_id('mirran crusader'/'MBS', '213802').
card_watermark('mirran crusader'/'MBS', 'Mirran').

card_in_set('mirran mettle', 'MBS').
card_original_type('mirran mettle'/'MBS', 'Instant').
card_original_text('mirran mettle'/'MBS', 'Target creature gets +2/+2 until end of turn.\nMetalcraft — That creature gets +4/+4 until end of turn instead if you control three or more artifacts.').
card_first_print('mirran mettle', 'MBS').
card_image_name('mirran mettle'/'MBS', 'mirran mettle').
card_uid('mirran mettle'/'MBS', 'MBS:Mirran Mettle:mirran mettle').
card_rarity('mirran mettle'/'MBS', 'Common').
card_artist('mirran mettle'/'MBS', 'Karl Kopinski').
card_number('mirran mettle'/'MBS', '84').
card_flavor_text('mirran mettle'/'MBS', '\"Our land will not yield to Phyrexian corruption!\"').
card_multiverse_id('mirran mettle'/'MBS', '213819').
card_watermark('mirran mettle'/'MBS', 'Mirran').

card_in_set('mirran spy', 'MBS').
card_original_type('mirran spy'/'MBS', 'Creature — Drone').
card_original_text('mirran spy'/'MBS', 'Flying\nWhenever you cast an artifact spell, you may untap target creature.').
card_first_print('mirran spy', 'MBS').
card_image_name('mirran spy'/'MBS', 'mirran spy').
card_uid('mirran spy'/'MBS', 'MBS:Mirran Spy:mirran spy').
card_rarity('mirran spy'/'MBS', 'Common').
card_artist('mirran spy'/'MBS', 'Dave Kendall').
card_number('mirran spy'/'MBS', '26').
card_flavor_text('mirran spy'/'MBS', 'Accurate information is a precious commodity in times of war.').
card_multiverse_id('mirran spy'/'MBS', '213779').
card_watermark('mirran spy'/'MBS', 'Mirran').

card_in_set('mirrorworks', 'MBS').
card_original_type('mirrorworks'/'MBS', 'Artifact').
card_original_text('mirrorworks'/'MBS', 'Whenever another nontoken artifact enters the battlefield under your control, you may pay {2}. If you do, put a token that\'s a copy of that artifact onto the battlefield.').
card_first_print('mirrorworks', 'MBS').
card_image_name('mirrorworks'/'MBS', 'mirrorworks').
card_uid('mirrorworks'/'MBS', 'MBS:Mirrorworks:mirrorworks').
card_rarity('mirrorworks'/'MBS', 'Rare').
card_artist('mirrorworks'/'MBS', 'John Avon').
card_number('mirrorworks'/'MBS', '114').
card_flavor_text('mirrorworks'/'MBS', 'The faces of Geth\'s corpse-dredgers are disturbingly similar.').
card_multiverse_id('mirrorworks'/'MBS', '214068').
card_watermark('mirrorworks'/'MBS', 'Phyrexian').

card_in_set('mitotic manipulation', 'MBS').
card_original_type('mitotic manipulation'/'MBS', 'Sorcery').
card_original_text('mitotic manipulation'/'MBS', 'Look at the top seven cards of your library. You may put one of those cards onto the battlefield if it has the same name as a permanent. Put the rest on the bottom of your library in any order.').
card_first_print('mitotic manipulation', 'MBS').
card_image_name('mitotic manipulation'/'MBS', 'mitotic manipulation').
card_uid('mitotic manipulation'/'MBS', 'MBS:Mitotic Manipulation:mitotic manipulation').
card_rarity('mitotic manipulation'/'MBS', 'Rare').
card_artist('mitotic manipulation'/'MBS', 'Dan Scott').
card_number('mitotic manipulation'/'MBS', '27').
card_flavor_text('mitotic manipulation'/'MBS', '\"They can\'t even comprehend nature. How could they improve it?\"\n—Venser').
card_multiverse_id('mitotic manipulation'/'MBS', '214071').
card_watermark('mitotic manipulation'/'MBS', 'Phyrexian').

card_in_set('morbid plunder', 'MBS').
card_original_type('morbid plunder'/'MBS', 'Sorcery').
card_original_text('morbid plunder'/'MBS', 'Return up to two target creature cards from your graveyard to your hand.').
card_first_print('morbid plunder', 'MBS').
card_image_name('morbid plunder'/'MBS', 'morbid plunder').
card_uid('morbid plunder'/'MBS', 'MBS:Morbid Plunder:morbid plunder').
card_rarity('morbid plunder'/'MBS', 'Common').
card_artist('morbid plunder'/'MBS', 'Mike Bierek').
card_number('morbid plunder'/'MBS', '47').
card_flavor_text('morbid plunder'/'MBS', 'Even the dead are raw materials for the Phyrexian vision of perfection.').
card_multiverse_id('morbid plunder'/'MBS', '222863').
card_watermark('morbid plunder'/'MBS', 'Phyrexian').

card_in_set('mortarpod', 'MBS').
card_original_type('mortarpod'/'MBS', 'Artifact — Equipment').
card_original_text('mortarpod'/'MBS', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +0/+1 and has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"\nEquip {2}').
card_first_print('mortarpod', 'MBS').
card_image_name('mortarpod'/'MBS', 'mortarpod').
card_uid('mortarpod'/'MBS', 'MBS:Mortarpod:mortarpod').
card_rarity('mortarpod'/'MBS', 'Uncommon').
card_artist('mortarpod'/'MBS', 'Eric Deschamps').
card_number('mortarpod'/'MBS', '115').
card_multiverse_id('mortarpod'/'MBS', '213725').
card_watermark('mortarpod'/'MBS', 'Phyrexian').

card_in_set('mountain', 'MBS').
card_original_type('mountain'/'MBS', 'Basic Land — Mountain').
card_original_text('mountain'/'MBS', 'R').
card_image_name('mountain'/'MBS', 'mountain1').
card_uid('mountain'/'MBS', 'MBS:Mountain:mountain1').
card_rarity('mountain'/'MBS', 'Basic Land').
card_artist('mountain'/'MBS', 'Tomasz Jedruszek').
card_number('mountain'/'MBS', '152').
card_multiverse_id('mountain'/'MBS', '214045').

card_in_set('mountain', 'MBS').
card_original_type('mountain'/'MBS', 'Basic Land — Mountain').
card_original_text('mountain'/'MBS', 'R').
card_image_name('mountain'/'MBS', 'mountain2').
card_uid('mountain'/'MBS', 'MBS:Mountain:mountain2').
card_rarity('mountain'/'MBS', 'Basic Land').
card_artist('mountain'/'MBS', 'Tomasz Jedruszek').
card_number('mountain'/'MBS', '153').
card_multiverse_id('mountain'/'MBS', '214066').

card_in_set('myr sire', 'MBS').
card_original_type('myr sire'/'MBS', 'Artifact Creature — Myr').
card_original_text('myr sire'/'MBS', 'When Myr Sire is put into a graveyard from the battlefield, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_first_print('myr sire', 'MBS').
card_image_name('myr sire'/'MBS', 'myr sire').
card_uid('myr sire'/'MBS', 'MBS:Myr Sire:myr sire').
card_rarity('myr sire'/'MBS', 'Common').
card_artist('myr sire'/'MBS', 'Jaime Jones').
card_number('myr sire'/'MBS', '116').
card_flavor_text('myr sire'/'MBS', 'For the Phyrexians, death is not an end, nor a one-time occurrence.').
card_multiverse_id('myr sire'/'MBS', '213734').
card_watermark('myr sire'/'MBS', 'Phyrexian').

card_in_set('myr turbine', 'MBS').
card_original_type('myr turbine'/'MBS', 'Artifact').
card_original_text('myr turbine'/'MBS', '{T}: Put a 1/1 colorless Myr artifact creature token onto the battlefield.\n{T}, Tap five untapped Myr you control: Search your library for a Myr creature card, put it onto the battlefield, then shuffle your library.').
card_first_print('myr turbine', 'MBS').
card_image_name('myr turbine'/'MBS', 'myr turbine').
card_uid('myr turbine'/'MBS', 'MBS:Myr Turbine:myr turbine').
card_rarity('myr turbine'/'MBS', 'Rare').
card_artist('myr turbine'/'MBS', 'Randis Albion').
card_number('myr turbine'/'MBS', '117').
card_multiverse_id('myr turbine'/'MBS', '213757').
card_watermark('myr turbine'/'MBS', 'Mirran').

card_in_set('myr welder', 'MBS').
card_original_type('myr welder'/'MBS', 'Artifact Creature — Myr').
card_original_text('myr welder'/'MBS', 'Imprint — {T}: Exile target artifact card from a graveyard.\nMyr Welder has all activated abilities of all cards exiled with it.').
card_first_print('myr welder', 'MBS').
card_image_name('myr welder'/'MBS', 'myr welder').
card_uid('myr welder'/'MBS', 'MBS:Myr Welder:myr welder').
card_rarity('myr welder'/'MBS', 'Rare').
card_artist('myr welder'/'MBS', 'Austin Hsu').
card_number('myr welder'/'MBS', '118').
card_flavor_text('myr welder'/'MBS', 'Memnarch designed some myr to follow the levelers and reaffix lost parts. Mirran partisans put that instinct to good use.').
card_multiverse_id('myr welder'/'MBS', '214067').
card_watermark('myr welder'/'MBS', 'Mirran').

card_in_set('nested ghoul', 'MBS').
card_original_type('nested ghoul'/'MBS', 'Creature — Zombie Warrior').
card_original_text('nested ghoul'/'MBS', 'Whenever a source deals damage to Nested Ghoul, put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('nested ghoul', 'MBS').
card_image_name('nested ghoul'/'MBS', 'nested ghoul').
card_uid('nested ghoul'/'MBS', 'MBS:Nested Ghoul:nested ghoul').
card_rarity('nested ghoul'/'MBS', 'Uncommon').
card_artist('nested ghoul'/'MBS', 'Dave Kendall').
card_number('nested ghoul'/'MBS', '48').
card_flavor_text('nested ghoul'/'MBS', '\"The chest cavity is cleared of useless meat. I know just what to do with the space.\"\n—Gyed, Vault Priest').
card_multiverse_id('nested ghoul'/'MBS', '213816').
card_watermark('nested ghoul'/'MBS', 'Phyrexian').

card_in_set('neurok commando', 'MBS').
card_original_type('neurok commando'/'MBS', 'Creature — Human Rogue').
card_original_text('neurok commando'/'MBS', 'Shroud\nWhenever Neurok Commando deals combat damage to a player, you may draw a card.').
card_first_print('neurok commando', 'MBS').
card_image_name('neurok commando'/'MBS', 'neurok commando').
card_uid('neurok commando'/'MBS', 'MBS:Neurok Commando:neurok commando').
card_rarity('neurok commando'/'MBS', 'Uncommon').
card_artist('neurok commando'/'MBS', 'Matt Stewart').
card_number('neurok commando'/'MBS', '28').
card_flavor_text('neurok commando'/'MBS', '\"There\'s no more time for secluded study. Answers are there only for those with the courage to take them.\"').
card_multiverse_id('neurok commando'/'MBS', '213754').
card_watermark('neurok commando'/'MBS', 'Mirran').

card_in_set('oculus', 'MBS').
card_original_type('oculus'/'MBS', 'Creature — Homunculus').
card_original_text('oculus'/'MBS', 'When Oculus is put into a graveyard from the battlefield, you may draw a card.').
card_first_print('oculus', 'MBS').
card_image_name('oculus'/'MBS', 'oculus').
card_uid('oculus'/'MBS', 'MBS:Oculus:oculus').
card_rarity('oculus'/'MBS', 'Common').
card_artist('oculus'/'MBS', 'Dan Scott').
card_number('oculus'/'MBS', '29').
card_flavor_text('oculus'/'MBS', '\"In its gaze I saw no mercy, no recognition that I was worthy to exist.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('oculus'/'MBS', '214036').
card_watermark('oculus'/'MBS', 'Phyrexian').

card_in_set('ogre resister', 'MBS').
card_original_type('ogre resister'/'MBS', 'Creature — Ogre').
card_original_text('ogre resister'/'MBS', '').
card_first_print('ogre resister', 'MBS').
card_image_name('ogre resister'/'MBS', 'ogre resister').
card_uid('ogre resister'/'MBS', 'MBS:Ogre Resister:ogre resister').
card_rarity('ogre resister'/'MBS', 'Common').
card_artist('ogre resister'/'MBS', 'Efrem Palacios').
card_number('ogre resister'/'MBS', '72').
card_flavor_text('ogre resister'/'MBS', 'He didn\'t have a word for \"home,\" but he knew it was something to be defended.').
card_multiverse_id('ogre resister'/'MBS', '213795').
card_watermark('ogre resister'/'MBS', 'Mirran').

card_in_set('peace strider', 'MBS').
card_original_type('peace strider'/'MBS', 'Artifact Creature — Construct').
card_original_text('peace strider'/'MBS', 'When Peace Strider enters the battlefield, you gain 3 life.').
card_first_print('peace strider', 'MBS').
card_image_name('peace strider'/'MBS', 'peace strider').
card_uid('peace strider'/'MBS', 'MBS:Peace Strider:peace strider').
card_rarity('peace strider'/'MBS', 'Uncommon').
card_artist('peace strider'/'MBS', 'Igor Kieryluk').
card_number('peace strider'/'MBS', '119').
card_flavor_text('peace strider'/'MBS', '\"The Vanished must have sent it from beyond to aid us in this struggle.\"\n—Kessla, Sylvok shaman').
card_multiverse_id('peace strider'/'MBS', '213781').
card_watermark('peace strider'/'MBS', 'Mirran').

card_in_set('phyresis', 'MBS').
card_original_type('phyresis'/'MBS', 'Enchantment — Aura').
card_original_text('phyresis'/'MBS', 'Enchant creature\nEnchanted creature has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('phyresis', 'MBS').
card_image_name('phyresis'/'MBS', 'phyresis').
card_uid('phyresis'/'MBS', 'MBS:Phyresis:phyresis').
card_rarity('phyresis'/'MBS', 'Common').
card_artist('phyresis'/'MBS', 'Izzy').
card_number('phyresis'/'MBS', '49').
card_flavor_text('phyresis'/'MBS', '\"Perfection is at hand. You have been freed of weakness and made compleat.\"\n—Sheoldred, Whispering One').
card_multiverse_id('phyresis'/'MBS', '213797').
card_watermark('phyresis'/'MBS', 'Phyrexian').

card_in_set('phyrexian crusader', 'MBS').
card_original_type('phyrexian crusader'/'MBS', 'Creature — Zombie Knight').
card_original_text('phyrexian crusader'/'MBS', 'First strike, protection from red and from white\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('phyrexian crusader', 'MBS').
card_image_name('phyrexian crusader'/'MBS', 'phyrexian crusader').
card_uid('phyrexian crusader'/'MBS', 'MBS:Phyrexian Crusader:phyrexian crusader').
card_rarity('phyrexian crusader'/'MBS', 'Rare').
card_artist('phyrexian crusader'/'MBS', 'Eric Deschamps').
card_number('phyrexian crusader'/'MBS', '50').
card_multiverse_id('phyrexian crusader'/'MBS', '213724').
card_watermark('phyrexian crusader'/'MBS', 'Phyrexian').

card_in_set('phyrexian digester', 'MBS').
card_original_type('phyrexian digester'/'MBS', 'Artifact Creature — Construct').
card_original_text('phyrexian digester'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('phyrexian digester', 'MBS').
card_image_name('phyrexian digester'/'MBS', 'phyrexian digester').
card_uid('phyrexian digester'/'MBS', 'MBS:Phyrexian Digester:phyrexian digester').
card_rarity('phyrexian digester'/'MBS', 'Common').
card_artist('phyrexian digester'/'MBS', 'Dave Allsop').
card_number('phyrexian digester'/'MBS', '120').
card_flavor_text('phyrexian digester'/'MBS', '\"Our cause will ripen in the fertile flesh of the unworthy.\"\n—Sheoldred, Whispering One').
card_multiverse_id('phyrexian digester'/'MBS', '213743').
card_watermark('phyrexian digester'/'MBS', 'Phyrexian').

card_in_set('phyrexian hydra', 'MBS').
card_original_type('phyrexian hydra'/'MBS', 'Creature — Hydra').
card_original_text('phyrexian hydra'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nIf damage would be dealt to Phyrexian Hydra, prevent that damage. Put a -1/-1 counter on Phyrexian Hydra for each 1 damage prevented this way.').
card_first_print('phyrexian hydra', 'MBS').
card_image_name('phyrexian hydra'/'MBS', 'phyrexian hydra').
card_uid('phyrexian hydra'/'MBS', 'MBS:Phyrexian Hydra:phyrexian hydra').
card_rarity('phyrexian hydra'/'MBS', 'Rare').
card_artist('phyrexian hydra'/'MBS', 'Mike Bierek').
card_number('phyrexian hydra'/'MBS', '85').
card_multiverse_id('phyrexian hydra'/'MBS', '214060').
card_watermark('phyrexian hydra'/'MBS', 'Phyrexian').

card_in_set('phyrexian juggernaut', 'MBS').
card_original_type('phyrexian juggernaut'/'MBS', 'Artifact Creature — Juggernaut').
card_original_text('phyrexian juggernaut'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nPhyrexian Juggernaut attacks each turn if able.').
card_first_print('phyrexian juggernaut', 'MBS').
card_image_name('phyrexian juggernaut'/'MBS', 'phyrexian juggernaut').
card_uid('phyrexian juggernaut'/'MBS', 'MBS:Phyrexian Juggernaut:phyrexian juggernaut').
card_rarity('phyrexian juggernaut'/'MBS', 'Uncommon').
card_artist('phyrexian juggernaut'/'MBS', 'Kev Walker').
card_number('phyrexian juggernaut'/'MBS', '121').
card_flavor_text('phyrexian juggernaut'/'MBS', 'Where nature impedes, Phyrexians overcome.').
card_multiverse_id('phyrexian juggernaut'/'MBS', '214038').
card_watermark('phyrexian juggernaut'/'MBS', 'Phyrexian').

card_in_set('phyrexian rager', 'MBS').
card_original_type('phyrexian rager'/'MBS', 'Creature — Horror').
card_original_text('phyrexian rager'/'MBS', 'When Phyrexian Rager enters the battlefield, you draw a card and you lose 1 life.').
card_image_name('phyrexian rager'/'MBS', 'phyrexian rager').
card_uid('phyrexian rager'/'MBS', 'MBS:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'MBS', 'Common').
card_artist('phyrexian rager'/'MBS', 'Stephan Martiniere').
card_number('phyrexian rager'/'MBS', '51').
card_flavor_text('phyrexian rager'/'MBS', '\"I believe many worlds will bow to Phyrexia. Mirrodin is merely the first.\"\n—Sheoldred, Whispering One').
card_multiverse_id('phyrexian rager'/'MBS', '213804').
card_watermark('phyrexian rager'/'MBS', 'Phyrexian').

card_in_set('phyrexian rebirth', 'MBS').
card_original_type('phyrexian rebirth'/'MBS', 'Sorcery').
card_original_text('phyrexian rebirth'/'MBS', 'Destroy all creatures, then put an X/X colorless Horror artifact creature token onto the battlefield, where X is the number of creatures destroyed this way.').
card_first_print('phyrexian rebirth', 'MBS').
card_image_name('phyrexian rebirth'/'MBS', 'phyrexian rebirth').
card_uid('phyrexian rebirth'/'MBS', 'MBS:Phyrexian Rebirth:phyrexian rebirth').
card_rarity('phyrexian rebirth'/'MBS', 'Rare').
card_artist('phyrexian rebirth'/'MBS', 'Scott Chou').
card_number('phyrexian rebirth'/'MBS', '15').
card_flavor_text('phyrexian rebirth'/'MBS', 'As long as one drop of the oil exists, the joyous work continues.').
card_multiverse_id('phyrexian rebirth'/'MBS', '214048').
card_watermark('phyrexian rebirth'/'MBS', 'Phyrexian').

card_in_set('phyrexian revoker', 'MBS').
card_original_type('phyrexian revoker'/'MBS', 'Artifact Creature — Horror').
card_original_text('phyrexian revoker'/'MBS', 'As Phyrexian Revoker enters the battlefield, name a nonland card.\nActivated abilities of sources with the chosen name can\'t be activated.').
card_first_print('phyrexian revoker', 'MBS').
card_image_name('phyrexian revoker'/'MBS', 'phyrexian revoker').
card_uid('phyrexian revoker'/'MBS', 'MBS:Phyrexian Revoker:phyrexian revoker').
card_rarity('phyrexian revoker'/'MBS', 'Rare').
card_artist('phyrexian revoker'/'MBS', 'Kev Walker').
card_number('phyrexian revoker'/'MBS', '122').
card_flavor_text('phyrexian revoker'/'MBS', 'Basic senses like sight and taste are reserved for those in power.').
card_multiverse_id('phyrexian revoker'/'MBS', '220589').
card_watermark('phyrexian revoker'/'MBS', 'Phyrexian').

card_in_set('phyrexian vatmother', 'MBS').
card_original_type('phyrexian vatmother'/'MBS', 'Creature — Horror').
card_original_text('phyrexian vatmother'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of your upkeep, you get a poison counter.').
card_first_print('phyrexian vatmother', 'MBS').
card_image_name('phyrexian vatmother'/'MBS', 'phyrexian vatmother').
card_uid('phyrexian vatmother'/'MBS', 'MBS:Phyrexian Vatmother:phyrexian vatmother').
card_rarity('phyrexian vatmother'/'MBS', 'Rare').
card_artist('phyrexian vatmother'/'MBS', 'Stephan Martiniere').
card_number('phyrexian vatmother'/'MBS', '52').
card_multiverse_id('phyrexian vatmother'/'MBS', '214074').
card_watermark('phyrexian vatmother'/'MBS', 'Phyrexian').

card_in_set('pierce strider', 'MBS').
card_original_type('pierce strider'/'MBS', 'Artifact Creature — Construct').
card_original_text('pierce strider'/'MBS', 'When Pierce Strider enters the battlefield, target opponent loses 3 life.').
card_first_print('pierce strider', 'MBS').
card_image_name('pierce strider'/'MBS', 'pierce strider').
card_uid('pierce strider'/'MBS', 'MBS:Pierce Strider:pierce strider').
card_rarity('pierce strider'/'MBS', 'Uncommon').
card_artist('pierce strider'/'MBS', 'Igor Kieryluk').
card_number('pierce strider'/'MBS', '123').
card_flavor_text('pierce strider'/'MBS', '\"Pain isn\'t a negative stimulus. Pain is a sign of your imperfection.\"\n—Sheoldred, Whispering One').
card_multiverse_id('pierce strider'/'MBS', '221561').
card_watermark('pierce strider'/'MBS', 'Phyrexian').

card_in_set('piston sledge', 'MBS').
card_original_type('piston sledge'/'MBS', 'Artifact — Equipment').
card_original_text('piston sledge'/'MBS', 'When Piston Sledge enters the battlefield, attach it to target creature you control.\nEquipped creature gets +3/+1.\nEquip—Sacrifice an artifact.').
card_first_print('piston sledge', 'MBS').
card_image_name('piston sledge'/'MBS', 'piston sledge').
card_uid('piston sledge'/'MBS', 'MBS:Piston Sledge:piston sledge').
card_rarity('piston sledge'/'MBS', 'Uncommon').
card_artist('piston sledge'/'MBS', 'Pete Venters').
card_number('piston sledge'/'MBS', '124').
card_flavor_text('piston sledge'/'MBS', 'Only the goblins could make a simple machine so complex.').
card_multiverse_id('piston sledge'/'MBS', '213742').
card_watermark('piston sledge'/'MBS', 'Mirran').

card_in_set('pistus strike', 'MBS').
card_original_type('pistus strike'/'MBS', 'Instant').
card_original_text('pistus strike'/'MBS', 'Destroy target creature with flying. Its controller gets a poison counter.').
card_first_print('pistus strike', 'MBS').
card_image_name('pistus strike'/'MBS', 'pistus strike').
card_uid('pistus strike'/'MBS', 'MBS:Pistus Strike:pistus strike').
card_rarity('pistus strike'/'MBS', 'Common').
card_artist('pistus strike'/'MBS', 'Jaime Jones').
card_number('pistus strike'/'MBS', '86').
card_flavor_text('pistus strike'/'MBS', '\"Even a nuisance such as the pistus fly has a purpose in our new world.\"\n—Glissa').
card_multiverse_id('pistus strike'/'MBS', '213744').
card_watermark('pistus strike'/'MBS', 'Phyrexian').

card_in_set('plague myr', 'MBS').
card_original_type('plague myr'/'MBS', 'Artifact Creature — Myr').
card_original_text('plague myr'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{T}: Add {1} to your mana pool.').
card_image_name('plague myr'/'MBS', 'plague myr').
card_uid('plague myr'/'MBS', 'MBS:Plague Myr:plague myr').
card_rarity('plague myr'/'MBS', 'Uncommon').
card_artist('plague myr'/'MBS', 'Efrem Palacios').
card_number('plague myr'/'MBS', '125').
card_flavor_text('plague myr'/'MBS', 'They watch for a new master, one more sinister than the last.').
card_multiverse_id('plague myr'/'MBS', '213785').
card_watermark('plague myr'/'MBS', 'Phyrexian').

card_in_set('plaguemaw beast', 'MBS').
card_original_type('plaguemaw beast'/'MBS', 'Creature — Beast').
card_original_text('plaguemaw beast'/'MBS', '{T}, Sacrifice a creature: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('plaguemaw beast', 'MBS').
card_image_name('plaguemaw beast'/'MBS', 'plaguemaw beast').
card_uid('plaguemaw beast'/'MBS', 'MBS:Plaguemaw Beast:plaguemaw beast').
card_rarity('plaguemaw beast'/'MBS', 'Uncommon').
card_artist('plaguemaw beast'/'MBS', 'Whit Brachna').
card_number('plaguemaw beast'/'MBS', '87').
card_flavor_text('plaguemaw beast'/'MBS', 'Phyrexia\'s spiral of consumption grows ever wider and darker.').
card_multiverse_id('plaguemaw beast'/'MBS', '213752').
card_watermark('plaguemaw beast'/'MBS', 'Phyrexian').

card_in_set('plains', 'MBS').
card_original_type('plains'/'MBS', 'Basic Land — Plains').
card_original_text('plains'/'MBS', 'W').
card_image_name('plains'/'MBS', 'plains1').
card_uid('plains'/'MBS', 'MBS:Plains:plains1').
card_rarity('plains'/'MBS', 'Basic Land').
card_artist('plains'/'MBS', 'James Paick').
card_number('plains'/'MBS', '146').
card_multiverse_id('plains'/'MBS', '214057').

card_in_set('plains', 'MBS').
card_original_type('plains'/'MBS', 'Basic Land — Plains').
card_original_text('plains'/'MBS', 'W').
card_image_name('plains'/'MBS', 'plains2').
card_uid('plains'/'MBS', 'MBS:Plains:plains2').
card_rarity('plains'/'MBS', 'Basic Land').
card_artist('plains'/'MBS', 'James Paick').
card_number('plains'/'MBS', '147').
card_multiverse_id('plains'/'MBS', '220366').

card_in_set('praetor\'s counsel', 'MBS').
card_original_type('praetor\'s counsel'/'MBS', 'Sorcery').
card_original_text('praetor\'s counsel'/'MBS', 'Return all cards from your graveyard to your hand. Exile Praetor\'s Counsel. You have no maximum hand size for the rest of the game.').
card_first_print('praetor\'s counsel', 'MBS').
card_image_name('praetor\'s counsel'/'MBS', 'praetor\'s counsel').
card_uid('praetor\'s counsel'/'MBS', 'MBS:Praetor\'s Counsel:praetor\'s counsel').
card_rarity('praetor\'s counsel'/'MBS', 'Mythic Rare').
card_artist('praetor\'s counsel'/'MBS', 'Daarken').
card_number('praetor\'s counsel'/'MBS', '88').
card_flavor_text('praetor\'s counsel'/'MBS', 'As the Phyrexian contagion corroded Karn\'s body, the praetors whispered psalms to corrupt his mind.').
card_multiverse_id('praetor\'s counsel'/'MBS', '214034').
card_watermark('praetor\'s counsel'/'MBS', 'Phyrexian').

card_in_set('priests of norn', 'MBS').
card_original_type('priests of norn'/'MBS', 'Creature — Cleric').
card_original_text('priests of norn'/'MBS', 'Vigilance\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('priests of norn', 'MBS').
card_image_name('priests of norn'/'MBS', 'priests of norn').
card_uid('priests of norn'/'MBS', 'MBS:Priests of Norn:priests of norn').
card_rarity('priests of norn'/'MBS', 'Common').
card_artist('priests of norn'/'MBS', 'Igor Kieryluk').
card_number('priests of norn'/'MBS', '16').
card_flavor_text('priests of norn'/'MBS', '\"May our blessings sever the tongues of the forsaken.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('priests of norn'/'MBS', '213814').
card_watermark('priests of norn'/'MBS', 'Phyrexian').

card_in_set('psychosis crawler', 'MBS').
card_original_type('psychosis crawler'/'MBS', 'Artifact Creature — Horror').
card_original_text('psychosis crawler'/'MBS', 'Psychosis Crawler\'s power and toughness are each equal to the number of cards in your hand.\nWhenever you draw a card, each opponent loses 1 life.').
card_first_print('psychosis crawler', 'MBS').
card_image_name('psychosis crawler'/'MBS', 'psychosis crawler').
card_uid('psychosis crawler'/'MBS', 'MBS:Psychosis Crawler:psychosis crawler').
card_rarity('psychosis crawler'/'MBS', 'Rare').
card_artist('psychosis crawler'/'MBS', 'Stephan Martiniere').
card_number('psychosis crawler'/'MBS', '126').
card_flavor_text('psychosis crawler'/'MBS', '\"If that brain can\'t figure out the secret of the serum, then add more brains.\"\n—Rhmir, Hand of the Augur').
card_multiverse_id('psychosis crawler'/'MBS', '214047').
card_watermark('psychosis crawler'/'MBS', 'Phyrexian').

card_in_set('quicksilver geyser', 'MBS').
card_original_type('quicksilver geyser'/'MBS', 'Instant').
card_original_text('quicksilver geyser'/'MBS', 'Return up to two target nonland permanents to their owners\' hands.').
card_first_print('quicksilver geyser', 'MBS').
card_image_name('quicksilver geyser'/'MBS', 'quicksilver geyser').
card_uid('quicksilver geyser'/'MBS', 'MBS:Quicksilver Geyser:quicksilver geyser').
card_rarity('quicksilver geyser'/'MBS', 'Common').
card_artist('quicksilver geyser'/'MBS', 'Erica Yang').
card_number('quicksilver geyser'/'MBS', '30').
card_flavor_text('quicksilver geyser'/'MBS', '\"Phyrexians are tenacious. That\'s not the same thing as clever.\"\n—Tezzeret').
card_multiverse_id('quicksilver geyser'/'MBS', '213762').
card_watermark('quicksilver geyser'/'MBS', 'Mirran').

card_in_set('quilled slagwurm', 'MBS').
card_original_type('quilled slagwurm'/'MBS', 'Creature — Wurm').
card_original_text('quilled slagwurm'/'MBS', '').
card_first_print('quilled slagwurm', 'MBS').
card_image_name('quilled slagwurm'/'MBS', 'quilled slagwurm').
card_uid('quilled slagwurm'/'MBS', 'MBS:Quilled Slagwurm:quilled slagwurm').
card_rarity('quilled slagwurm'/'MBS', 'Uncommon').
card_artist('quilled slagwurm'/'MBS', 'Matt Stewart').
card_number('quilled slagwurm'/'MBS', '89').
card_flavor_text('quilled slagwurm'/'MBS', 'Vorinclex removed its teeth so it wouldn\'t waste time chewing before moving to the next kill.').
card_multiverse_id('quilled slagwurm'/'MBS', '213755').
card_watermark('quilled slagwurm'/'MBS', 'Phyrexian').

card_in_set('rally the forces', 'MBS').
card_original_type('rally the forces'/'MBS', 'Instant').
card_original_text('rally the forces'/'MBS', 'Attacking creatures get +1/+0 and gain first strike until end of turn.').
card_first_print('rally the forces', 'MBS').
card_image_name('rally the forces'/'MBS', 'rally the forces').
card_uid('rally the forces'/'MBS', 'MBS:Rally the Forces:rally the forces').
card_rarity('rally the forces'/'MBS', 'Common').
card_artist('rally the forces'/'MBS', 'Steven Belledin').
card_number('rally the forces'/'MBS', '73').
card_flavor_text('rally the forces'/'MBS', '\"Drive them back! Make their underworld into their grave!\"\n—Koth of the Hammer').
card_multiverse_id('rally the forces'/'MBS', '213767').
card_watermark('rally the forces'/'MBS', 'Mirran').

card_in_set('razorfield rhino', 'MBS').
card_original_type('razorfield rhino'/'MBS', 'Artifact Creature — Rhino').
card_original_text('razorfield rhino'/'MBS', 'Metalcraft — Razorfield Rhino gets +2/+2 as long as you control three or more artifacts.').
card_first_print('razorfield rhino', 'MBS').
card_image_name('razorfield rhino'/'MBS', 'razorfield rhino').
card_uid('razorfield rhino'/'MBS', 'MBS:Razorfield Rhino:razorfield rhino').
card_rarity('razorfield rhino'/'MBS', 'Common').
card_artist('razorfield rhino'/'MBS', 'Kekai Kotaki').
card_number('razorfield rhino'/'MBS', '127').
card_flavor_text('razorfield rhino'/'MBS', 'Adapted to tread on razorgrass, the rhino proved adept at treading on Phyrexians as well.').
card_multiverse_id('razorfield rhino'/'MBS', '213815').
card_watermark('razorfield rhino'/'MBS', 'Mirran').

card_in_set('red sun\'s zenith', 'MBS').
card_original_type('red sun\'s zenith'/'MBS', 'Sorcery').
card_original_text('red sun\'s zenith'/'MBS', 'Red Sun\'s Zenith deals X damage to target creature or player. If a creature dealt damage this way would be put into a graveyard this turn, exile it instead. Shuffle Red Sun\'s Zenith into its owner\'s library.').
card_first_print('red sun\'s zenith', 'MBS').
card_image_name('red sun\'s zenith'/'MBS', 'red sun\'s zenith').
card_uid('red sun\'s zenith'/'MBS', 'MBS:Red Sun\'s Zenith:red sun\'s zenith').
card_rarity('red sun\'s zenith'/'MBS', 'Rare').
card_artist('red sun\'s zenith'/'MBS', 'Svetlin Velinov').
card_number('red sun\'s zenith'/'MBS', '74').
card_multiverse_id('red sun\'s zenith'/'MBS', '221558').
card_watermark('red sun\'s zenith'/'MBS', 'Mirran').

card_in_set('rot wolf', 'MBS').
card_original_type('rot wolf'/'MBS', 'Creature — Wolf').
card_original_text('rot wolf'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever a creature dealt damage by Rot Wolf this turn is put into a graveyard, you may draw a card.').
card_first_print('rot wolf', 'MBS').
card_image_name('rot wolf'/'MBS', 'rot wolf').
card_uid('rot wolf'/'MBS', 'MBS:Rot Wolf:rot wolf').
card_rarity('rot wolf'/'MBS', 'Common').
card_artist('rot wolf'/'MBS', 'Nils Hamm').
card_number('rot wolf'/'MBS', '90').
card_multiverse_id('rot wolf'/'MBS', '213774').
card_watermark('rot wolf'/'MBS', 'Phyrexian').

card_in_set('rusted slasher', 'MBS').
card_original_type('rusted slasher'/'MBS', 'Artifact Creature — Horror').
card_original_text('rusted slasher'/'MBS', 'Sacrifice an artifact: Regenerate Rusted Slasher.').
card_first_print('rusted slasher', 'MBS').
card_image_name('rusted slasher'/'MBS', 'rusted slasher').
card_uid('rusted slasher'/'MBS', 'MBS:Rusted Slasher:rusted slasher').
card_rarity('rusted slasher'/'MBS', 'Common').
card_artist('rusted slasher'/'MBS', 'Adrian Smith').
card_number('rusted slasher'/'MBS', '128').
card_flavor_text('rusted slasher'/'MBS', '\"It\'s a beautiful vision. Discarded debris is reborn as a singular entity.\"\n—Urabrask the Hidden').
card_multiverse_id('rusted slasher'/'MBS', '213746').
card_watermark('rusted slasher'/'MBS', 'Phyrexian').

card_in_set('sangromancer', 'MBS').
card_original_type('sangromancer'/'MBS', 'Creature — Vampire Shaman').
card_original_text('sangromancer'/'MBS', 'Flying\nWhenever a creature an opponent controls is put into a graveyard from the battlefield, you may gain 3 life.\nWhenever an opponent discards a card, you may gain 3 life.').
card_first_print('sangromancer', 'MBS').
card_image_name('sangromancer'/'MBS', 'sangromancer').
card_uid('sangromancer'/'MBS', 'MBS:Sangromancer:sangromancer').
card_rarity('sangromancer'/'MBS', 'Rare').
card_artist('sangromancer'/'MBS', 'Igor Kieryluk').
card_number('sangromancer'/'MBS', '53').
card_multiverse_id('sangromancer'/'MBS', '214062').
card_watermark('sangromancer'/'MBS', 'Mirran').

card_in_set('scourge servant', 'MBS').
card_original_type('scourge servant'/'MBS', 'Creature — Zombie').
card_original_text('scourge servant'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('scourge servant', 'MBS').
card_image_name('scourge servant'/'MBS', 'scourge servant').
card_uid('scourge servant'/'MBS', 'MBS:Scourge Servant:scourge servant').
card_rarity('scourge servant'/'MBS', 'Common').
card_artist('scourge servant'/'MBS', 'Daarken').
card_number('scourge servant'/'MBS', '54').
card_flavor_text('scourge servant'/'MBS', '\"The union of the oil and necrogen has produced many pleasing reactions.\"\n—Sheoldred, Whispering One').
card_multiverse_id('scourge servant'/'MBS', '213751').
card_watermark('scourge servant'/'MBS', 'Phyrexian').

card_in_set('septic rats', 'MBS').
card_original_type('septic rats'/'MBS', 'Creature — Rat').
card_original_text('septic rats'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Septic Rats attacks, if defending player is poisoned, it gets +1/+1 until end of turn.').
card_first_print('septic rats', 'MBS').
card_image_name('septic rats'/'MBS', 'septic rats').
card_uid('septic rats'/'MBS', 'MBS:Septic Rats:septic rats').
card_rarity('septic rats'/'MBS', 'Uncommon').
card_artist('septic rats'/'MBS', 'Cos Koniotis').
card_number('septic rats'/'MBS', '55').
card_multiverse_id('septic rats'/'MBS', '213748').
card_watermark('septic rats'/'MBS', 'Phyrexian').

card_in_set('serum raker', 'MBS').
card_original_type('serum raker'/'MBS', 'Creature — Drake').
card_original_text('serum raker'/'MBS', 'Flying\nWhen Serum Raker is put into a graveyard from the battlefield, each player discards a card.').
card_first_print('serum raker', 'MBS').
card_image_name('serum raker'/'MBS', 'serum raker').
card_uid('serum raker'/'MBS', 'MBS:Serum Raker:serum raker').
card_rarity('serum raker'/'MBS', 'Common').
card_artist('serum raker'/'MBS', 'Austin Hsu').
card_number('serum raker'/'MBS', '31').
card_flavor_text('serum raker'/'MBS', 'The serum from the blinkmoths they gather greases the joints of witch engines.').
card_multiverse_id('serum raker'/'MBS', '213780').
card_watermark('serum raker'/'MBS', 'Phyrexian').

card_in_set('shimmer myr', 'MBS').
card_original_type('shimmer myr'/'MBS', 'Artifact Creature — Myr').
card_original_text('shimmer myr'/'MBS', 'Flash\nYou may cast artifact cards as though they had flash.').
card_first_print('shimmer myr', 'MBS').
card_image_name('shimmer myr'/'MBS', 'shimmer myr').
card_uid('shimmer myr'/'MBS', 'MBS:Shimmer Myr:shimmer myr').
card_rarity('shimmer myr'/'MBS', 'Rare').
card_artist('shimmer myr'/'MBS', 'Jana Schirmer & Johannes Voss').
card_number('shimmer myr'/'MBS', '129').
card_flavor_text('shimmer myr'/'MBS', 'It evades Phyrexians by hiding in the spaces between seconds.').
card_multiverse_id('shimmer myr'/'MBS', '214042').
card_watermark('shimmer myr'/'MBS', 'Mirran').

card_in_set('shriekhorn', 'MBS').
card_original_type('shriekhorn'/'MBS', 'Artifact').
card_original_text('shriekhorn'/'MBS', 'Shriekhorn enters the battlefield with three charge counters on it.\n{T}, Remove a charge counter from Shriekhorn: Target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('shriekhorn', 'MBS').
card_image_name('shriekhorn'/'MBS', 'shriekhorn').
card_uid('shriekhorn'/'MBS', 'MBS:Shriekhorn:shriekhorn').
card_rarity('shriekhorn'/'MBS', 'Common').
card_artist('shriekhorn'/'MBS', 'Erica Yang').
card_number('shriekhorn'/'MBS', '130').
card_multiverse_id('shriekhorn'/'MBS', '213786').
card_watermark('shriekhorn'/'MBS', 'Mirran').

card_in_set('signal pest', 'MBS').
card_original_type('signal pest'/'MBS', 'Artifact Creature — Pest').
card_original_text('signal pest'/'MBS', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nSignal Pest can\'t be blocked except by creatures with flying or reach.').
card_image_name('signal pest'/'MBS', 'signal pest').
card_uid('signal pest'/'MBS', 'MBS:Signal Pest:signal pest').
card_rarity('signal pest'/'MBS', 'Uncommon').
card_artist('signal pest'/'MBS', 'Mark Zug').
card_number('signal pest'/'MBS', '131').
card_flavor_text('signal pest'/'MBS', 'It leaps from tree to tree, revealing the enemy\'s positions.').
card_multiverse_id('signal pest'/'MBS', '213773').
card_watermark('signal pest'/'MBS', 'Mirran').

card_in_set('silverskin armor', 'MBS').
card_original_type('silverskin armor'/'MBS', 'Artifact — Equipment').
card_original_text('silverskin armor'/'MBS', 'Equipped creature gets +1/+1 and is an artifact in addition to its other types.\nEquip {2}').
card_first_print('silverskin armor', 'MBS').
card_image_name('silverskin armor'/'MBS', 'silverskin armor').
card_uid('silverskin armor'/'MBS', 'MBS:Silverskin Armor:silverskin armor').
card_rarity('silverskin armor'/'MBS', 'Uncommon').
card_artist('silverskin armor'/'MBS', 'Terese Nielsen').
card_number('silverskin armor'/'MBS', '132').
card_flavor_text('silverskin armor'/'MBS', 'Partisan spies warned that no armor would protect the body from Phyrexian infection. Neurok strategists took that as a challenge.').
card_multiverse_id('silverskin armor'/'MBS', '213738').
card_watermark('silverskin armor'/'MBS', 'Mirran').

card_in_set('skinwing', 'MBS').
card_original_type('skinwing'/'MBS', 'Artifact — Equipment').
card_original_text('skinwing'/'MBS', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +2/+2 and has flying.\nEquip {6}').
card_first_print('skinwing', 'MBS').
card_image_name('skinwing'/'MBS', 'skinwing').
card_uid('skinwing'/'MBS', 'MBS:Skinwing:skinwing').
card_rarity('skinwing'/'MBS', 'Uncommon').
card_artist('skinwing'/'MBS', 'Igor Kieryluk').
card_number('skinwing'/'MBS', '133').
card_multiverse_id('skinwing'/'MBS', '221562').
card_watermark('skinwing'/'MBS', 'Phyrexian').

card_in_set('slagstorm', 'MBS').
card_original_type('slagstorm'/'MBS', 'Sorcery').
card_original_text('slagstorm'/'MBS', 'Choose one — Slagstorm deals 3 damage to each creature; or Slagstorm deals 3 damage to each player.').
card_first_print('slagstorm', 'MBS').
card_image_name('slagstorm'/'MBS', 'slagstorm').
card_uid('slagstorm'/'MBS', 'MBS:Slagstorm:slagstorm').
card_rarity('slagstorm'/'MBS', 'Rare').
card_artist('slagstorm'/'MBS', 'Dan Scott').
card_number('slagstorm'/'MBS', '75').
card_flavor_text('slagstorm'/'MBS', '\"As long as we have the will to fight, we are never without weapons.\"\n—Koth of the Hammer').
card_multiverse_id('slagstorm'/'MBS', '214054').
card_watermark('slagstorm'/'MBS', 'Mirran').

card_in_set('sphere of the suns', 'MBS').
card_original_type('sphere of the suns'/'MBS', 'Artifact').
card_original_text('sphere of the suns'/'MBS', 'Sphere of the Suns enters the battlefield tapped and with three charge counters on it.\n{T}, Remove a charge counter from Sphere of the Suns: Add one mana of any color to your mana pool.').
card_first_print('sphere of the suns', 'MBS').
card_image_name('sphere of the suns'/'MBS', 'sphere of the suns').
card_uid('sphere of the suns'/'MBS', 'MBS:Sphere of the Suns:sphere of the suns').
card_rarity('sphere of the suns'/'MBS', 'Uncommon').
card_artist('sphere of the suns'/'MBS', 'Jana Schirmer & Johannes Voss').
card_number('sphere of the suns'/'MBS', '134').
card_multiverse_id('sphere of the suns'/'MBS', '213776').
card_watermark('sphere of the suns'/'MBS', 'Mirran').

card_in_set('spin engine', 'MBS').
card_original_type('spin engine'/'MBS', 'Artifact Creature — Construct').
card_original_text('spin engine'/'MBS', '{R}: Target creature can\'t block Spin Engine this turn.').
card_first_print('spin engine', 'MBS').
card_image_name('spin engine'/'MBS', 'spin engine').
card_uid('spin engine'/'MBS', 'MBS:Spin Engine:spin engine').
card_rarity('spin engine'/'MBS', 'Common').
card_artist('spin engine'/'MBS', 'Pete Venters').
card_number('spin engine'/'MBS', '135').
card_flavor_text('spin engine'/'MBS', '\"It will be battle-ready before our strike at Oxid Ridge, and it will guarantee our victory.\"\n—Ketuc of the Helm').
card_multiverse_id('spin engine'/'MBS', '214069').
card_watermark('spin engine'/'MBS', 'Mirran').

card_in_set('spine of ish sah', 'MBS').
card_original_type('spine of ish sah'/'MBS', 'Artifact').
card_original_text('spine of ish sah'/'MBS', 'When Spine of Ish Sah enters the battlefield, destroy target permanent.\nWhen Spine of Ish Sah is put into a graveyard from the battlefield, return Spine of Ish Sah to its owner\'s hand.').
card_first_print('spine of ish sah', 'MBS').
card_image_name('spine of ish sah'/'MBS', 'spine of ish sah').
card_uid('spine of ish sah'/'MBS', 'MBS:Spine of Ish Sah:spine of ish sah').
card_rarity('spine of ish sah'/'MBS', 'Rare').
card_artist('spine of ish sah'/'MBS', 'Daniel Ljunggren').
card_number('spine of ish sah'/'MBS', '136').
card_multiverse_id('spine of ish sah'/'MBS', '214076').
card_watermark('spine of ish sah'/'MBS', 'Phyrexian').

card_in_set('spiraling duelist', 'MBS').
card_original_type('spiraling duelist'/'MBS', 'Creature — Human Berserker').
card_original_text('spiraling duelist'/'MBS', 'Metalcraft — Spiraling Duelist has double strike as long as you control three or more artifacts.').
card_first_print('spiraling duelist', 'MBS').
card_image_name('spiraling duelist'/'MBS', 'spiraling duelist').
card_uid('spiraling duelist'/'MBS', 'MBS:Spiraling Duelist:spiraling duelist').
card_rarity('spiraling duelist'/'MBS', 'Uncommon').
card_artist('spiraling duelist'/'MBS', 'Karl Kopinski').
card_number('spiraling duelist'/'MBS', '76').
card_flavor_text('spiraling duelist'/'MBS', '\"I never move the same way twice. Those rotters can\'t grasp chaos.\"').
card_multiverse_id('spiraling duelist'/'MBS', '222859').
card_watermark('spiraling duelist'/'MBS', 'Mirran').

card_in_set('spire serpent', 'MBS').
card_original_type('spire serpent'/'MBS', 'Creature — Serpent').
card_original_text('spire serpent'/'MBS', 'Defender\nMetalcraft — As long as you control three or more artifacts, Spire Serpent gets +2/+2 and can attack as though it didn\'t have defender.').
card_first_print('spire serpent', 'MBS').
card_image_name('spire serpent'/'MBS', 'spire serpent').
card_uid('spire serpent'/'MBS', 'MBS:Spire Serpent:spire serpent').
card_rarity('spire serpent'/'MBS', 'Common').
card_artist('spire serpent'/'MBS', 'Johann Bodin').
card_number('spire serpent'/'MBS', '32').
card_flavor_text('spire serpent'/'MBS', 'A mirror to draw its eye, a rod to rouse its rage, and a sword to break its bonds.').
card_multiverse_id('spire serpent'/'MBS', '213801').
card_watermark('spire serpent'/'MBS', 'Mirran').

card_in_set('spread the sickness', 'MBS').
card_original_type('spread the sickness'/'MBS', 'Sorcery').
card_original_text('spread the sickness'/'MBS', 'Destroy target creature, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_first_print('spread the sickness', 'MBS').
card_image_name('spread the sickness'/'MBS', 'spread the sickness').
card_uid('spread the sickness'/'MBS', 'MBS:Spread the Sickness:spread the sickness').
card_rarity('spread the sickness'/'MBS', 'Common').
card_artist('spread the sickness'/'MBS', 'Jaime Jones').
card_number('spread the sickness'/'MBS', '56').
card_flavor_text('spread the sickness'/'MBS', 'Life is ephemeral. Phyrexia is eternal.').
card_multiverse_id('spread the sickness'/'MBS', '221557').
card_watermark('spread the sickness'/'MBS', 'Phyrexian').

card_in_set('steel sabotage', 'MBS').
card_original_type('steel sabotage'/'MBS', 'Instant').
card_original_text('steel sabotage'/'MBS', 'Choose one — Counter target artifact spell; or return target artifact to its owner\'s hand.').
card_first_print('steel sabotage', 'MBS').
card_image_name('steel sabotage'/'MBS', 'steel sabotage').
card_uid('steel sabotage'/'MBS', 'MBS:Steel Sabotage:steel sabotage').
card_rarity('steel sabotage'/'MBS', 'Common').
card_artist('steel sabotage'/'MBS', 'Daarken').
card_number('steel sabotage'/'MBS', '33').
card_flavor_text('steel sabotage'/'MBS', '\"You are hopelessly obsolete, my brothers. Come and join the Great Work.\"\n—Rhmir, Hand of the Augur').
card_multiverse_id('steel sabotage'/'MBS', '213726').
card_watermark('steel sabotage'/'MBS', 'Phyrexian').

card_in_set('strandwalker', 'MBS').
card_original_type('strandwalker'/'MBS', 'Artifact — Equipment').
card_original_text('strandwalker'/'MBS', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +2/+4 and has reach.\nEquip {4}').
card_first_print('strandwalker', 'MBS').
card_image_name('strandwalker'/'MBS', 'strandwalker').
card_uid('strandwalker'/'MBS', 'MBS:Strandwalker:strandwalker').
card_rarity('strandwalker'/'MBS', 'Uncommon').
card_artist('strandwalker'/'MBS', 'Igor Kieryluk').
card_number('strandwalker'/'MBS', '137').
card_multiverse_id('strandwalker'/'MBS', '213790').
card_watermark('strandwalker'/'MBS', 'Phyrexian').

card_in_set('swamp', 'MBS').
card_original_type('swamp'/'MBS', 'Basic Land — Swamp').
card_original_text('swamp'/'MBS', 'B').
card_image_name('swamp'/'MBS', 'swamp1').
card_uid('swamp'/'MBS', 'MBS:Swamp:swamp1').
card_rarity('swamp'/'MBS', 'Basic Land').
card_artist('swamp'/'MBS', 'Lars Grant-West').
card_number('swamp'/'MBS', '150').
card_multiverse_id('swamp'/'MBS', '220367').

card_in_set('swamp', 'MBS').
card_original_type('swamp'/'MBS', 'Basic Land — Swamp').
card_original_text('swamp'/'MBS', 'B').
card_image_name('swamp'/'MBS', 'swamp2').
card_uid('swamp'/'MBS', 'MBS:Swamp:swamp2').
card_rarity('swamp'/'MBS', 'Basic Land').
card_artist('swamp'/'MBS', 'Lars Grant-West').
card_number('swamp'/'MBS', '151').
card_multiverse_id('swamp'/'MBS', '220368').

card_in_set('sword of feast and famine', 'MBS').
card_original_type('sword of feast and famine'/'MBS', 'Artifact — Equipment').
card_original_text('sword of feast and famine'/'MBS', 'Equipped creature gets +2/+2 and has protection from black and from green.\nWhenever equipped creature deals combat damage to a player, that player discards a card and you untap all lands you control.\nEquip {2}').
card_image_name('sword of feast and famine'/'MBS', 'sword of feast and famine').
card_uid('sword of feast and famine'/'MBS', 'MBS:Sword of Feast and Famine:sword of feast and famine').
card_rarity('sword of feast and famine'/'MBS', 'Mythic Rare').
card_artist('sword of feast and famine'/'MBS', 'Chris Rahn').
card_number('sword of feast and famine'/'MBS', '138').
card_multiverse_id('sword of feast and famine'/'MBS', '214070').
card_watermark('sword of feast and famine'/'MBS', 'Mirran').

card_in_set('tangle hulk', 'MBS').
card_original_type('tangle hulk'/'MBS', 'Artifact Creature — Beast').
card_original_text('tangle hulk'/'MBS', '{2}{G}: Regenerate Tangle Hulk.').
card_first_print('tangle hulk', 'MBS').
card_image_name('tangle hulk'/'MBS', 'tangle hulk').
card_uid('tangle hulk'/'MBS', 'MBS:Tangle Hulk:tangle hulk').
card_rarity('tangle hulk'/'MBS', 'Common').
card_artist('tangle hulk'/'MBS', 'Mark Zug').
card_number('tangle hulk'/'MBS', '139').
card_flavor_text('tangle hulk'/'MBS', '\"A true Phyrexian predator. It will never know death, just as nature intended.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('tangle hulk'/'MBS', '214041').
card_watermark('tangle hulk'/'MBS', 'Phyrexian').

card_in_set('tangle mantis', 'MBS').
card_original_type('tangle mantis'/'MBS', 'Creature — Insect').
card_original_text('tangle mantis'/'MBS', 'Trample').
card_first_print('tangle mantis', 'MBS').
card_image_name('tangle mantis'/'MBS', 'tangle mantis').
card_uid('tangle mantis'/'MBS', 'MBS:Tangle Mantis:tangle mantis').
card_rarity('tangle mantis'/'MBS', 'Common').
card_artist('tangle mantis'/'MBS', 'Chris Rahn').
card_number('tangle mantis'/'MBS', '91').
card_flavor_text('tangle mantis'/'MBS', 'The fiercest hunters in the Tangle don\'t seek out the mantises. They are the mantises.').
card_multiverse_id('tangle mantis'/'MBS', '222862').
card_watermark('tangle mantis'/'MBS', 'Mirran').

card_in_set('tezzeret, agent of bolas', 'MBS').
card_original_type('tezzeret, agent of bolas'/'MBS', 'Planeswalker — Tezzeret').
card_original_text('tezzeret, agent of bolas'/'MBS', '+1: Look at the top five cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.\n-1: Target artifact becomes a 5/5 artifact creature.\n-4: Target player loses X life and you gain X life, where X is twice the number of artifacts you control.').
card_first_print('tezzeret, agent of bolas', 'MBS').
card_image_name('tezzeret, agent of bolas'/'MBS', 'tezzeret, agent of bolas').
card_uid('tezzeret, agent of bolas'/'MBS', 'MBS:Tezzeret, Agent of Bolas:tezzeret, agent of bolas').
card_rarity('tezzeret, agent of bolas'/'MBS', 'Mythic Rare').
card_artist('tezzeret, agent of bolas'/'MBS', 'Aleksi Briclot').
card_number('tezzeret, agent of bolas'/'MBS', '97').
card_multiverse_id('tezzeret, agent of bolas'/'MBS', '214065').

card_in_set('thopter assembly', 'MBS').
card_original_type('thopter assembly'/'MBS', 'Artifact Creature — Thopter').
card_original_text('thopter assembly'/'MBS', 'Flying\nAt the beginning of your upkeep, if you control no Thopters other than Thopter Assembly, return Thopter Assembly to its owner\'s hand and put five 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.').
card_image_name('thopter assembly'/'MBS', 'thopter assembly').
card_uid('thopter assembly'/'MBS', 'MBS:Thopter Assembly:thopter assembly').
card_rarity('thopter assembly'/'MBS', 'Rare').
card_artist('thopter assembly'/'MBS', 'Volkan Baga').
card_number('thopter assembly'/'MBS', '140').
card_multiverse_id('thopter assembly'/'MBS', '214039').
card_watermark('thopter assembly'/'MBS', 'Mirran').

card_in_set('thrun, the last troll', 'MBS').
card_original_type('thrun, the last troll'/'MBS', 'Legendary Creature — Troll Shaman').
card_original_text('thrun, the last troll'/'MBS', 'Thrun, the Last Troll can\'t be countered.\nThrun can\'t be the target of spells or abilities your opponents control.\n{1}{G}: Regenerate Thrun.').
card_first_print('thrun, the last troll', 'MBS').
card_image_name('thrun, the last troll'/'MBS', 'thrun, the last troll').
card_uid('thrun, the last troll'/'MBS', 'MBS:Thrun, the Last Troll:thrun, the last troll').
card_rarity('thrun, the last troll'/'MBS', 'Mythic Rare').
card_artist('thrun, the last troll'/'MBS', 'Jason Chan').
card_number('thrun, the last troll'/'MBS', '92').
card_flavor_text('thrun, the last troll'/'MBS', 'His crime was silence, and now he suffers it eternally.').
card_multiverse_id('thrun, the last troll'/'MBS', '214050').
card_watermark('thrun, the last troll'/'MBS', 'Mirran').

card_in_set('tine shrike', 'MBS').
card_original_type('tine shrike'/'MBS', 'Creature — Bird').
card_original_text('tine shrike'/'MBS', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_first_print('tine shrike', 'MBS').
card_image_name('tine shrike'/'MBS', 'tine shrike').
card_uid('tine shrike'/'MBS', 'MBS:Tine Shrike:tine shrike').
card_rarity('tine shrike'/'MBS', 'Common').
card_artist('tine shrike'/'MBS', 'Adrian Smith').
card_number('tine shrike'/'MBS', '17').
card_flavor_text('tine shrike'/'MBS', 'A new bird of prey—one that hunts sentience.').
card_multiverse_id('tine shrike'/'MBS', '213758').
card_watermark('tine shrike'/'MBS', 'Phyrexian').

card_in_set('titan forge', 'MBS').
card_original_type('titan forge'/'MBS', 'Artifact').
card_original_text('titan forge'/'MBS', '{3}, {T}: Put a charge counter on Titan Forge.\n{T}, Remove three charge counters from Titan Forge: Put a 9/9 colorless Golem artifact creature token onto the battlefield.').
card_first_print('titan forge', 'MBS').
card_image_name('titan forge'/'MBS', 'titan forge').
card_uid('titan forge'/'MBS', 'MBS:Titan Forge:titan forge').
card_rarity('titan forge'/'MBS', 'Rare').
card_artist('titan forge'/'MBS', 'Svetlin Velinov').
card_number('titan forge'/'MBS', '141').
card_multiverse_id('titan forge'/'MBS', '213730').
card_watermark('titan forge'/'MBS', 'Mirran').

card_in_set('training drone', 'MBS').
card_original_type('training drone'/'MBS', 'Artifact Creature — Drone').
card_original_text('training drone'/'MBS', 'Training Drone can\'t attack or block unless it\'s equipped.').
card_first_print('training drone', 'MBS').
card_image_name('training drone'/'MBS', 'training drone').
card_uid('training drone'/'MBS', 'MBS:Training Drone:training drone').
card_rarity('training drone'/'MBS', 'Common').
card_artist('training drone'/'MBS', 'Matt Cavotta').
card_number('training drone'/'MBS', '142').
card_flavor_text('training drone'/'MBS', '\"Vulshok flail, Viridian shield, loxodon blade . . . Tazzir, bring the Moriok hook and assemble the rookies.\"\n—Vy Covalt, Neurok agent').
card_multiverse_id('training drone'/'MBS', '213764').
card_watermark('training drone'/'MBS', 'Mirran').

card_in_set('treasure mage', 'MBS').
card_original_type('treasure mage'/'MBS', 'Creature — Human Wizard').
card_original_text('treasure mage'/'MBS', 'When Treasure Mage enters the battlefield, you may search your library for an artifact card with converted mana cost 6 or greater, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_image_name('treasure mage'/'MBS', 'treasure mage').
card_uid('treasure mage'/'MBS', 'MBS:Treasure Mage:treasure mage').
card_rarity('treasure mage'/'MBS', 'Uncommon').
card_artist('treasure mage'/'MBS', 'Ryan Pancoast').
card_number('treasure mage'/'MBS', '34').
card_multiverse_id('treasure mage'/'MBS', '213753').
card_watermark('treasure mage'/'MBS', 'Mirran').

card_in_set('turn the tide', 'MBS').
card_original_type('turn the tide'/'MBS', 'Instant').
card_original_text('turn the tide'/'MBS', 'Creatures your opponents control get -2/-0 until end of turn.').
card_first_print('turn the tide', 'MBS').
card_image_name('turn the tide'/'MBS', 'turn the tide').
card_uid('turn the tide'/'MBS', 'MBS:Turn the Tide:turn the tide').
card_rarity('turn the tide'/'MBS', 'Common').
card_artist('turn the tide'/'MBS', 'Jason Felix').
card_number('turn the tide'/'MBS', '35').
card_flavor_text('turn the tide'/'MBS', '\"Let their mindless armies come and face the might of genius.\"\n—Varil, Neurok partisan').
card_multiverse_id('turn the tide'/'MBS', '213821').
card_watermark('turn the tide'/'MBS', 'Mirran').

card_in_set('unnatural predation', 'MBS').
card_original_type('unnatural predation'/'MBS', 'Instant').
card_original_text('unnatural predation'/'MBS', 'Target creature gets +1/+1 and gains trample until end of turn.').
card_first_print('unnatural predation', 'MBS').
card_image_name('unnatural predation'/'MBS', 'unnatural predation').
card_uid('unnatural predation'/'MBS', 'MBS:Unnatural Predation:unnatural predation').
card_rarity('unnatural predation'/'MBS', 'Common').
card_artist('unnatural predation'/'MBS', 'Shelly Wan').
card_number('unnatural predation'/'MBS', '93').
card_flavor_text('unnatural predation'/'MBS', '\"Domination by the strongest—that is all that matters in the Tangle now.\"\n—Vorinclex, Voice of Hunger').
card_multiverse_id('unnatural predation'/'MBS', '213778').
card_watermark('unnatural predation'/'MBS', 'Phyrexian').

card_in_set('vedalken anatomist', 'MBS').
card_original_type('vedalken anatomist'/'MBS', 'Creature — Vedalken Wizard').
card_original_text('vedalken anatomist'/'MBS', '{2}{U}, {T}: Put a -1/-1 counter on target creature. You may tap or untap that creature.').
card_first_print('vedalken anatomist', 'MBS').
card_image_name('vedalken anatomist'/'MBS', 'vedalken anatomist').
card_uid('vedalken anatomist'/'MBS', 'MBS:Vedalken Anatomist:vedalken anatomist').
card_rarity('vedalken anatomist'/'MBS', 'Uncommon').
card_artist('vedalken anatomist'/'MBS', 'Greg Staples').
card_number('vedalken anatomist'/'MBS', '36').
card_flavor_text('vedalken anatomist'/'MBS', '\"Specimen 211 examination report. Observation: graft sublimation incomplete. Result: death. Prepare specimen 212.\"').
card_multiverse_id('vedalken anatomist'/'MBS', '222861').
card_watermark('vedalken anatomist'/'MBS', 'Phyrexian').

card_in_set('vedalken infuser', 'MBS').
card_original_type('vedalken infuser'/'MBS', 'Creature — Vedalken Wizard').
card_original_text('vedalken infuser'/'MBS', 'At the beginning of your upkeep, you may put a charge counter on target artifact.').
card_first_print('vedalken infuser', 'MBS').
card_image_name('vedalken infuser'/'MBS', 'vedalken infuser').
card_uid('vedalken infuser'/'MBS', 'MBS:Vedalken Infuser:vedalken infuser').
card_rarity('vedalken infuser'/'MBS', 'Uncommon').
card_artist('vedalken infuser'/'MBS', 'Ryan Pancoast').
card_number('vedalken infuser'/'MBS', '37').
card_flavor_text('vedalken infuser'/'MBS', '\"All plans begin as dreams. I intend to awaken them.\"').
card_multiverse_id('vedalken infuser'/'MBS', '213750').
card_watermark('vedalken infuser'/'MBS', 'Mirran').

card_in_set('victory\'s herald', 'MBS').
card_original_type('victory\'s herald'/'MBS', 'Creature — Angel').
card_original_text('victory\'s herald'/'MBS', 'Flying\nWhenever Victory\'s Herald attacks, attacking creatures gain flying and lifelink until end of turn.').
card_first_print('victory\'s herald', 'MBS').
card_image_name('victory\'s herald'/'MBS', 'victory\'s herald').
card_uid('victory\'s herald'/'MBS', 'MBS:Victory\'s Herald:victory\'s herald').
card_rarity('victory\'s herald'/'MBS', 'Rare').
card_artist('victory\'s herald'/'MBS', 'rk post').
card_number('victory\'s herald'/'MBS', '18').
card_flavor_text('victory\'s herald'/'MBS', 'The corruption stirred the heavens above, awaking a shining champion.').
card_multiverse_id('victory\'s herald'/'MBS', '214030').
card_watermark('victory\'s herald'/'MBS', 'Mirran').

card_in_set('viridian claw', 'MBS').
card_original_type('viridian claw'/'MBS', 'Artifact — Equipment').
card_original_text('viridian claw'/'MBS', 'Equipped creature gets +1/+0 and has first strike.\nEquip {1}').
card_first_print('viridian claw', 'MBS').
card_image_name('viridian claw'/'MBS', 'viridian claw').
card_uid('viridian claw'/'MBS', 'MBS:Viridian Claw:viridian claw').
card_rarity('viridian claw'/'MBS', 'Uncommon').
card_artist('viridian claw'/'MBS', 'Marc Simonetti').
card_number('viridian claw'/'MBS', '143').
card_flavor_text('viridian claw'/'MBS', '\"Phyrexians can\'t corrupt what they can\'t get close enough to touch.\"').
card_multiverse_id('viridian claw'/'MBS', '213784').
card_watermark('viridian claw'/'MBS', 'Mirran').

card_in_set('viridian corrupter', 'MBS').
card_original_type('viridian corrupter'/'MBS', 'Creature — Elf Shaman').
card_original_text('viridian corrupter'/'MBS', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Viridian Corrupter enters the battlefield, destroy target artifact.').
card_first_print('viridian corrupter', 'MBS').
card_image_name('viridian corrupter'/'MBS', 'viridian corrupter').
card_uid('viridian corrupter'/'MBS', 'MBS:Viridian Corrupter:viridian corrupter').
card_rarity('viridian corrupter'/'MBS', 'Uncommon').
card_artist('viridian corrupter'/'MBS', 'Matt Cavotta').
card_number('viridian corrupter'/'MBS', '94').
card_multiverse_id('viridian corrupter'/'MBS', '213772').
card_watermark('viridian corrupter'/'MBS', 'Phyrexian').

card_in_set('viridian emissary', 'MBS').
card_original_type('viridian emissary'/'MBS', 'Creature — Elf Scout').
card_original_text('viridian emissary'/'MBS', 'When Viridian Emissary is put into a graveyard from the battlefield, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('viridian emissary', 'MBS').
card_image_name('viridian emissary'/'MBS', 'viridian emissary').
card_uid('viridian emissary'/'MBS', 'MBS:Viridian Emissary:viridian emissary').
card_rarity('viridian emissary'/'MBS', 'Common').
card_artist('viridian emissary'/'MBS', 'Matt Stewart').
card_number('viridian emissary'/'MBS', '95').
card_multiverse_id('viridian emissary'/'MBS', '213728').
card_watermark('viridian emissary'/'MBS', 'Phyrexian').

card_in_set('virulent wound', 'MBS').
card_original_type('virulent wound'/'MBS', 'Instant').
card_original_text('virulent wound'/'MBS', 'Put a -1/-1 counter on target creature. When that creature is put into a graveyard this turn, its controller gets a poison counter.').
card_first_print('virulent wound', 'MBS').
card_image_name('virulent wound'/'MBS', 'virulent wound').
card_uid('virulent wound'/'MBS', 'MBS:Virulent Wound:virulent wound').
card_rarity('virulent wound'/'MBS', 'Common').
card_artist('virulent wound'/'MBS', 'Whit Brachna').
card_number('virulent wound'/'MBS', '57').
card_flavor_text('virulent wound'/'MBS', 'Sadly, the extent of Gerkk\'s healing knowledge was \"don\'t pick at it.\"').
card_multiverse_id('virulent wound'/'MBS', '213777').
card_watermark('virulent wound'/'MBS', 'Phyrexian').

card_in_set('vivisection', 'MBS').
card_original_type('vivisection'/'MBS', 'Sorcery').
card_original_text('vivisection'/'MBS', 'As an additional cost to cast Vivisection, sacrifice a creature.\nDraw three cards.').
card_first_print('vivisection', 'MBS').
card_image_name('vivisection'/'MBS', 'vivisection').
card_uid('vivisection'/'MBS', 'MBS:Vivisection:vivisection').
card_rarity('vivisection'/'MBS', 'Common').
card_artist('vivisection'/'MBS', 'Anthony Francisco').
card_number('vivisection'/'MBS', '38').
card_flavor_text('vivisection'/'MBS', 'Phyrexians research with the grace of surgeons and the finesse of butchers.').
card_multiverse_id('vivisection'/'MBS', '213735').
card_watermark('vivisection'/'MBS', 'Phyrexian').

card_in_set('white sun\'s zenith', 'MBS').
card_original_type('white sun\'s zenith'/'MBS', 'Instant').
card_original_text('white sun\'s zenith'/'MBS', 'Put X 2/2 white Cat creature tokens onto the battlefield. Shuffle White Sun\'s Zenith into its owner\'s library.').
card_first_print('white sun\'s zenith', 'MBS').
card_image_name('white sun\'s zenith'/'MBS', 'white sun\'s zenith').
card_uid('white sun\'s zenith'/'MBS', 'MBS:White Sun\'s Zenith:white sun\'s zenith').
card_rarity('white sun\'s zenith'/'MBS', 'Rare').
card_artist('white sun\'s zenith'/'MBS', 'Mike Bierek').
card_number('white sun\'s zenith'/'MBS', '19').
card_flavor_text('white sun\'s zenith'/'MBS', 'After the Battle of Liet Field, the white sun crested above Taj-Nar, bringing hope to all who survived the carnage.').
card_multiverse_id('white sun\'s zenith'/'MBS', '221555').
card_watermark('white sun\'s zenith'/'MBS', 'Mirran').
