% Limited Edition Beta

set('LEB').
set_name('LEB', 'Limited Edition Beta').
set_release_date('LEB', '1993-10-01').
set_border('LEB', 'black').
set_type('LEB', 'core').

card_in_set('air elemental', 'LEB').
card_original_type('air elemental'/'LEB', 'Summon — Elemental').
card_original_text('air elemental'/'LEB', 'Flying').
card_image_name('air elemental'/'LEB', 'air elemental').
card_uid('air elemental'/'LEB', 'LEB:Air Elemental:air elemental').
card_rarity('air elemental'/'LEB', 'Uncommon').
card_artist('air elemental'/'LEB', 'Richard Thomas').
card_flavor_text('air elemental'/'LEB', 'These spirits of the air are winsome and wild, and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'LEB', '389').

card_in_set('ancestral recall', 'LEB').
card_original_type('ancestral recall'/'LEB', 'Instant').
card_original_text('ancestral recall'/'LEB', 'Draw 3 cards or force opponent to draw 3 cards.').
card_image_name('ancestral recall'/'LEB', 'ancestral recall').
card_uid('ancestral recall'/'LEB', 'LEB:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'LEB', 'Rare').
card_artist('ancestral recall'/'LEB', 'Mark Poole').
card_multiverse_id('ancestral recall'/'LEB', '390').

card_in_set('animate artifact', 'LEB').
card_original_type('animate artifact'/'LEB', 'Enchant Non-Creature Artifact').
card_original_text('animate artifact'/'LEB', 'Target artifact is now a creature with both power and toughness equal to its casting cost; target retains all its original abilities as well. This will destroy artifacts with 0 casting cost.').
card_image_name('animate artifact'/'LEB', 'animate artifact').
card_uid('animate artifact'/'LEB', 'LEB:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'LEB', 'Uncommon').
card_artist('animate artifact'/'LEB', 'Douglas Shuler').
card_multiverse_id('animate artifact'/'LEB', '391').

card_in_set('animate dead', 'LEB').
card_original_type('animate dead'/'LEB', 'Enchant Dead Creature').
card_original_text('animate dead'/'LEB', 'Any creature in either player\'s graveyard comes into play on your side with -1 to its original power. If this enchantment is removed, or at end of game, target creature is returned to its owner\'s graveyard. Target creature may be killed as normal.').
card_image_name('animate dead'/'LEB', 'animate dead').
card_uid('animate dead'/'LEB', 'LEB:Animate Dead:animate dead').
card_rarity('animate dead'/'LEB', 'Uncommon').
card_artist('animate dead'/'LEB', 'Anson Maddocks').
card_multiverse_id('animate dead'/'LEB', '343').

card_in_set('animate wall', 'LEB').
card_original_type('animate wall'/'LEB', 'Enchant Wall').
card_original_text('animate wall'/'LEB', 'Target wall can now attack. Target wall\'s power and toughness are unchanged, even if its power is 0.').
card_image_name('animate wall'/'LEB', 'animate wall').
card_uid('animate wall'/'LEB', 'LEB:Animate Wall:animate wall').
card_rarity('animate wall'/'LEB', 'Rare').
card_artist('animate wall'/'LEB', 'Dan Frazier').
card_multiverse_id('animate wall'/'LEB', '527').

card_in_set('ankh of mishra', 'LEB').
card_original_type('ankh of mishra'/'LEB', 'Continuous Artifact').
card_original_text('ankh of mishra'/'LEB', 'Ankh does 2 damage to anyone who puts a new land into play.').
card_image_name('ankh of mishra'/'LEB', 'ankh of mishra').
card_uid('ankh of mishra'/'LEB', 'LEB:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'LEB', 'Rare').
card_artist('ankh of mishra'/'LEB', 'Amy Weber').
card_multiverse_id('ankh of mishra'/'LEB', '296').

card_in_set('armageddon', 'LEB').
card_original_type('armageddon'/'LEB', 'Sorcery').
card_original_text('armageddon'/'LEB', 'All lands in play are destroyed.').
card_image_name('armageddon'/'LEB', 'armageddon').
card_uid('armageddon'/'LEB', 'LEB:Armageddon:armageddon').
card_rarity('armageddon'/'LEB', 'Rare').
card_artist('armageddon'/'LEB', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'LEB', '528').

card_in_set('aspect of wolf', 'LEB').
card_original_type('aspect of wolf'/'LEB', 'Enchant Creature').
card_original_text('aspect of wolf'/'LEB', 'Target creature\'s power and toughness are increased by half the number of forests you have in play, rounding down for power and up for toughness.').
card_image_name('aspect of wolf'/'LEB', 'aspect of wolf').
card_uid('aspect of wolf'/'LEB', 'LEB:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'LEB', 'Rare').
card_artist('aspect of wolf'/'LEB', 'Jeff A. Menges').
card_multiverse_id('aspect of wolf'/'LEB', '435').

card_in_set('bad moon', 'LEB').
card_original_type('bad moon'/'LEB', 'Enchantment').
card_original_text('bad moon'/'LEB', 'All black creatures in play gain +1/+1.').
card_image_name('bad moon'/'LEB', 'bad moon').
card_uid('bad moon'/'LEB', 'LEB:Bad Moon:bad moon').
card_rarity('bad moon'/'LEB', 'Rare').
card_artist('bad moon'/'LEB', 'Jesper Myrfors').
card_multiverse_id('bad moon'/'LEB', '344').

card_in_set('badlands', 'LEB').
card_original_type('badlands'/'LEB', 'Land').
card_original_text('badlands'/'LEB', 'Counts as both mountains and swamp and is affected by spells that affect either. Tap to add either {R} or {B} to your mana pool.').
card_image_name('badlands'/'LEB', 'badlands').
card_uid('badlands'/'LEB', 'LEB:Badlands:badlands').
card_rarity('badlands'/'LEB', 'Rare').
card_artist('badlands'/'LEB', 'Rob Alexander').
card_multiverse_id('badlands'/'LEB', '576').

card_in_set('balance', 'LEB').
card_original_type('balance'/'LEB', 'Sorcery').
card_original_text('balance'/'LEB', 'Whichever player has more lands in play must discard enough lands of his or her choice to equalize the number of lands both players have in play. Cards in hand and creatures in play must be equalized the same way. Creatures lost in this manner may not be regenerated.').
card_image_name('balance'/'LEB', 'balance').
card_uid('balance'/'LEB', 'LEB:Balance:balance').
card_rarity('balance'/'LEB', 'Rare').
card_artist('balance'/'LEB', 'Mark Poole').
card_multiverse_id('balance'/'LEB', '529').

card_in_set('basalt monolith', 'LEB').
card_original_type('basalt monolith'/'LEB', 'Mono Artifact').
card_original_text('basalt monolith'/'LEB', 'Tap to add 3 colorless mana to your mana pool. Does not untap as normal during untap phase; spend {3} to untap. Tapping this artifact can be played as an interrupt.').
card_image_name('basalt monolith'/'LEB', 'basalt monolith').
card_uid('basalt monolith'/'LEB', 'LEB:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'LEB', 'Uncommon').
card_artist('basalt monolith'/'LEB', 'Jesper Myrfors').
card_multiverse_id('basalt monolith'/'LEB', '297').

card_in_set('bayou', 'LEB').
card_original_type('bayou'/'LEB', 'Land').
card_original_text('bayou'/'LEB', 'Counts as both swamp and forest and is affected by spells that affect either. Tap to add either {B} or {G} to your mana pool.').
card_image_name('bayou'/'LEB', 'bayou').
card_uid('bayou'/'LEB', 'LEB:Bayou:bayou').
card_rarity('bayou'/'LEB', 'Rare').
card_artist('bayou'/'LEB', 'Jesper Myrfors').
card_multiverse_id('bayou'/'LEB', '577').

card_in_set('benalish hero', 'LEB').
card_original_type('benalish hero'/'LEB', 'Summon — Hero').
card_original_text('benalish hero'/'LEB', 'Bands').
card_image_name('benalish hero'/'LEB', 'benalish hero').
card_uid('benalish hero'/'LEB', 'LEB:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'LEB', 'Common').
card_artist('benalish hero'/'LEB', 'Douglas Shuler').
card_flavor_text('benalish hero'/'LEB', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').
card_multiverse_id('benalish hero'/'LEB', '530').

card_in_set('berserk', 'LEB').
card_original_type('berserk'/'LEB', 'Instant').
card_original_text('berserk'/'LEB', 'Until end of turn, target creature\'s current power doubles and it gains trample ability. If it attacks, target creature is destroyed at end of turn. This spell cannot be cast after current turn\'s attack is completed.').
card_image_name('berserk'/'LEB', 'berserk').
card_uid('berserk'/'LEB', 'LEB:Berserk:berserk').
card_rarity('berserk'/'LEB', 'Uncommon').
card_artist('berserk'/'LEB', 'Dan Frazier').
card_multiverse_id('berserk'/'LEB', '436').

card_in_set('birds of paradise', 'LEB').
card_original_type('birds of paradise'/'LEB', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'LEB', 'Flying\nTap to add one mana of any color to your mana pool. This tap may be played as an interrupt.').
card_image_name('birds of paradise'/'LEB', 'birds of paradise').
card_uid('birds of paradise'/'LEB', 'LEB:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'LEB', 'Rare').
card_artist('birds of paradise'/'LEB', 'Mark Poole').
card_multiverse_id('birds of paradise'/'LEB', '437').

card_in_set('black knight', 'LEB').
card_original_type('black knight'/'LEB', 'Summon — Knight').
card_original_text('black knight'/'LEB', 'Protection from white, first strike').
card_image_name('black knight'/'LEB', 'black knight').
card_uid('black knight'/'LEB', 'LEB:Black Knight:black knight').
card_rarity('black knight'/'LEB', 'Uncommon').
card_artist('black knight'/'LEB', 'Jeff A. Menges').
card_flavor_text('black knight'/'LEB', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'LEB', '345').

card_in_set('black lotus', 'LEB').
card_original_type('black lotus'/'LEB', 'Mono Artifact').
card_original_text('black lotus'/'LEB', 'Adds 3 mana of any single color of your choice to your mana pool, then is discarded. Tapping this artifact can be played as an interrupt.').
card_image_name('black lotus'/'LEB', 'black lotus').
card_uid('black lotus'/'LEB', 'LEB:Black Lotus:black lotus').
card_rarity('black lotus'/'LEB', 'Rare').
card_artist('black lotus'/'LEB', 'Christopher Rush').
card_multiverse_id('black lotus'/'LEB', '298').

card_in_set('black vise', 'LEB').
card_original_type('black vise'/'LEB', 'Continuous Artifact').
card_original_text('black vise'/'LEB', 'If opponent has more than four cards in hand during upkeep, black vise does 1 damage to opponent for each card in excess of four.').
card_image_name('black vise'/'LEB', 'black vise').
card_uid('black vise'/'LEB', 'LEB:Black Vise:black vise').
card_rarity('black vise'/'LEB', 'Uncommon').
card_artist('black vise'/'LEB', 'Richard Thomas').
card_multiverse_id('black vise'/'LEB', '299').

card_in_set('black ward', 'LEB').
card_original_type('black ward'/'LEB', 'Enchant Creature').
card_original_text('black ward'/'LEB', 'Target creature gains protection from black.').
card_image_name('black ward'/'LEB', 'black ward').
card_uid('black ward'/'LEB', 'LEB:Black Ward:black ward').
card_rarity('black ward'/'LEB', 'Uncommon').
card_artist('black ward'/'LEB', 'Dan Frazier').
card_multiverse_id('black ward'/'LEB', '531').

card_in_set('blaze of glory', 'LEB').
card_original_type('blaze of glory'/'LEB', 'Instant').
card_original_text('blaze of glory'/'LEB', 'Target defending creature can and must block all attacking creatures it can legally block. For example, a normal non-flying target defender can and must block all normal non-flying attackers at once, but it cannot block any flying attackers. Controller of target defender may distribute damage among attackers as desired. Play before defense is chosen.').
card_image_name('blaze of glory'/'LEB', 'blaze of glory').
card_uid('blaze of glory'/'LEB', 'LEB:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'LEB', 'Rare').
card_artist('blaze of glory'/'LEB', 'Richard Thomas').
card_multiverse_id('blaze of glory'/'LEB', '532').

card_in_set('blessing', 'LEB').
card_original_type('blessing'/'LEB', 'Enchant Creature').
card_original_text('blessing'/'LEB', '{W} Target creature gains +1/+1 until end of turn.').
card_image_name('blessing'/'LEB', 'blessing').
card_uid('blessing'/'LEB', 'LEB:Blessing:blessing').
card_rarity('blessing'/'LEB', 'Rare').
card_artist('blessing'/'LEB', 'Julie Baroh').
card_multiverse_id('blessing'/'LEB', '533').

card_in_set('blue elemental blast', 'LEB').
card_original_type('blue elemental blast'/'LEB', 'Interrupt').
card_original_text('blue elemental blast'/'LEB', 'Counters a red spell being cast or destroys a red card in play.').
card_image_name('blue elemental blast'/'LEB', 'blue elemental blast').
card_uid('blue elemental blast'/'LEB', 'LEB:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'LEB', 'Common').
card_artist('blue elemental blast'/'LEB', 'Richard Thomas').
card_multiverse_id('blue elemental blast'/'LEB', '392').

card_in_set('blue ward', 'LEB').
card_original_type('blue ward'/'LEB', 'Enchant Creature').
card_original_text('blue ward'/'LEB', 'Target creature gains protection from blue.').
card_image_name('blue ward'/'LEB', 'blue ward').
card_uid('blue ward'/'LEB', 'LEB:Blue Ward:blue ward').
card_rarity('blue ward'/'LEB', 'Uncommon').
card_artist('blue ward'/'LEB', 'Dan Frazier').
card_multiverse_id('blue ward'/'LEB', '534').

card_in_set('bog wraith', 'LEB').
card_original_type('bog wraith'/'LEB', 'Summon — Wraith').
card_original_text('bog wraith'/'LEB', 'Swampwalk').
card_image_name('bog wraith'/'LEB', 'bog wraith').
card_uid('bog wraith'/'LEB', 'LEB:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'LEB', 'Uncommon').
card_artist('bog wraith'/'LEB', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'LEB', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'LEB', '346').

card_in_set('braingeyser', 'LEB').
card_original_type('braingeyser'/'LEB', 'Sorcery').
card_original_text('braingeyser'/'LEB', 'Draw X cards or force opponent to draw X cards.').
card_image_name('braingeyser'/'LEB', 'braingeyser').
card_uid('braingeyser'/'LEB', 'LEB:Braingeyser:braingeyser').
card_rarity('braingeyser'/'LEB', 'Rare').
card_artist('braingeyser'/'LEB', 'Mark Tedin').
card_multiverse_id('braingeyser'/'LEB', '393').

card_in_set('burrowing', 'LEB').
card_original_type('burrowing'/'LEB', 'Enchant Creature').
card_original_text('burrowing'/'LEB', 'Target creature gains mountainwalk.').
card_image_name('burrowing'/'LEB', 'burrowing').
card_uid('burrowing'/'LEB', 'LEB:Burrowing:burrowing').
card_rarity('burrowing'/'LEB', 'Uncommon').
card_artist('burrowing'/'LEB', 'Mark Poole').
card_multiverse_id('burrowing'/'LEB', '481').

card_in_set('camouflage', 'LEB').
card_original_type('camouflage'/'LEB', 'Instant').
card_original_text('camouflage'/'LEB', 'You may rearrange your attacking creatures and place them face down, revealing which is which only after defense is chosen. If this results in impossible blocks, such as non-flying creatures blocking flying creatures, illegal blockers cannot block this turn.').
card_image_name('camouflage'/'LEB', 'camouflage').
card_uid('camouflage'/'LEB', 'LEB:Camouflage:camouflage').
card_rarity('camouflage'/'LEB', 'Uncommon').
card_artist('camouflage'/'LEB', 'Jesper Myrfors').
card_multiverse_id('camouflage'/'LEB', '438').

card_in_set('castle', 'LEB').
card_original_type('castle'/'LEB', 'Enchantment').
card_original_text('castle'/'LEB', 'Your untapped creatures gain +0/+2. Attacking creatures lose this bonus.').
card_image_name('castle'/'LEB', 'castle').
card_uid('castle'/'LEB', 'LEB:Castle:castle').
card_rarity('castle'/'LEB', 'Uncommon').
card_artist('castle'/'LEB', 'Dameon Willich').
card_multiverse_id('castle'/'LEB', '535').

card_in_set('celestial prism', 'LEB').
card_original_type('celestial prism'/'LEB', 'Mono Artifact').
card_original_text('celestial prism'/'LEB', '{2}: Provides 1 mana of any color. This use can be played as an interrupt.').
card_image_name('celestial prism'/'LEB', 'celestial prism').
card_uid('celestial prism'/'LEB', 'LEB:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'LEB', 'Uncommon').
card_artist('celestial prism'/'LEB', 'Amy Weber').
card_multiverse_id('celestial prism'/'LEB', '300').

card_in_set('channel', 'LEB').
card_original_type('channel'/'LEB', 'Sorcery').
card_original_text('channel'/'LEB', 'Until end of turn, you may add colorless mana to your mana pool, at a cost of 1 life each. These additions are played with the speed of an interrupt. Effects that prevent damage may not be used to counter this loss of life.').
card_image_name('channel'/'LEB', 'channel').
card_uid('channel'/'LEB', 'LEB:Channel:channel').
card_rarity('channel'/'LEB', 'Uncommon').
card_artist('channel'/'LEB', 'Richard Thomas').
card_multiverse_id('channel'/'LEB', '439').

card_in_set('chaos orb', 'LEB').
card_original_type('chaos orb'/'LEB', 'Mono Artifact').
card_original_text('chaos orb'/'LEB', '{1}: Flip Chaos Orb onto the playing area from a height of at least one foot. Chaos Orb must turn completely over at least once or it is discarded with no effect. When Chaos Orb lands, any cards in play that it touches are destroyed, as is Chaos Orb.').
card_image_name('chaos orb'/'LEB', 'chaos orb').
card_uid('chaos orb'/'LEB', 'LEB:Chaos Orb:chaos orb').
card_rarity('chaos orb'/'LEB', 'Rare').
card_artist('chaos orb'/'LEB', 'Mark Tedin').
card_multiverse_id('chaos orb'/'LEB', '301').

card_in_set('chaoslace', 'LEB').
card_original_type('chaoslace'/'LEB', 'Interrupt').
card_original_text('chaoslace'/'LEB', 'Changes the color of one card either being played or already in play to red. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('chaoslace'/'LEB', 'chaoslace').
card_uid('chaoslace'/'LEB', 'LEB:Chaoslace:chaoslace').
card_rarity('chaoslace'/'LEB', 'Rare').
card_artist('chaoslace'/'LEB', 'Dameon Willich').
card_multiverse_id('chaoslace'/'LEB', '482').

card_in_set('circle of protection: black', 'LEB').
card_original_type('circle of protection: black'/'LEB', 'Enchantment').
card_original_text('circle of protection: black'/'LEB', '{1}: Prevents all damage against you from one black source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_first_print('circle of protection: black', 'LEB').
card_image_name('circle of protection: black'/'LEB', 'circle of protection black').
card_uid('circle of protection: black'/'LEB', 'LEB:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'LEB', 'Common').
card_artist('circle of protection: black'/'LEB', 'Jesper Myrfors').
card_multiverse_id('circle of protection: black'/'LEB', '536').

card_in_set('circle of protection: blue', 'LEB').
card_original_type('circle of protection: blue'/'LEB', 'Enchantment').
card_original_text('circle of protection: blue'/'LEB', '{1}: Prevents all damage against you from one blue source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: blue'/'LEB', 'circle of protection blue').
card_uid('circle of protection: blue'/'LEB', 'LEB:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'LEB', 'Common').
card_artist('circle of protection: blue'/'LEB', 'Dameon Willich').
card_multiverse_id('circle of protection: blue'/'LEB', '537').

card_in_set('circle of protection: green', 'LEB').
card_original_type('circle of protection: green'/'LEB', 'Enchantment').
card_original_text('circle of protection: green'/'LEB', '{1}: Prevents all damage against you from one green source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: green'/'LEB', 'circle of protection green').
card_uid('circle of protection: green'/'LEB', 'LEB:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'LEB', 'Common').
card_artist('circle of protection: green'/'LEB', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'LEB', '538').

card_in_set('circle of protection: red', 'LEB').
card_original_type('circle of protection: red'/'LEB', 'Enchantment').
card_original_text('circle of protection: red'/'LEB', '{1}: Prevents all damage against you from one red source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: red'/'LEB', 'circle of protection red').
card_uid('circle of protection: red'/'LEB', 'LEB:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'LEB', 'Common').
card_artist('circle of protection: red'/'LEB', 'Mark Tedin').
card_multiverse_id('circle of protection: red'/'LEB', '539').

card_in_set('circle of protection: white', 'LEB').
card_original_type('circle of protection: white'/'LEB', 'Enchantment').
card_original_text('circle of protection: white'/'LEB', '{1}: Prevents all damage against you from one white source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: white'/'LEB', 'circle of protection white').
card_uid('circle of protection: white'/'LEB', 'LEB:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'LEB', 'Common').
card_artist('circle of protection: white'/'LEB', 'Douglas Shuler').
card_multiverse_id('circle of protection: white'/'LEB', '540').

card_in_set('clockwork beast', 'LEB').
card_original_type('clockwork beast'/'LEB', 'Artifact Creature').
card_original_text('clockwork beast'/'LEB', 'Put seven +1/+0 counters on Beast. After Beast attacks or blocks a creature, discard a counter. During the untap phase, controller may buy back lost counters for 1 mana per counter instead of tapping Beast; this taps Beast if it wasn\'t tapped already.').
card_image_name('clockwork beast'/'LEB', 'clockwork beast').
card_uid('clockwork beast'/'LEB', 'LEB:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'LEB', 'Rare').
card_artist('clockwork beast'/'LEB', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'LEB', '302').

card_in_set('clone', 'LEB').
card_original_type('clone'/'LEB', 'Summon — Clone').
card_original_text('clone'/'LEB', 'Upon summoning, Clone acquires all characteristics, including color, of any one creature in play on either side; any creature enchantments on original creature are not copied. Clone retains these characteristics even after original creature is destroyed. Clone cannot be played if there are no creatures in play.').
card_image_name('clone'/'LEB', 'clone').
card_uid('clone'/'LEB', 'LEB:Clone:clone').
card_rarity('clone'/'LEB', 'Uncommon').
card_artist('clone'/'LEB', 'Julie Baroh').
card_multiverse_id('clone'/'LEB', '394').

card_in_set('cockatrice', 'LEB').
card_original_type('cockatrice'/'LEB', 'Summon — Cockatrice').
card_original_text('cockatrice'/'LEB', 'Flying\nAny non-wall creature blocking Cockatrice is destroyed, as is any creature blocked by Cockatrice. Creatures destroyed in this way deal their damage before dying.').
card_image_name('cockatrice'/'LEB', 'cockatrice').
card_uid('cockatrice'/'LEB', 'LEB:Cockatrice:cockatrice').
card_rarity('cockatrice'/'LEB', 'Rare').
card_artist('cockatrice'/'LEB', 'Dan Frazier').
card_multiverse_id('cockatrice'/'LEB', '440').

card_in_set('consecrate land', 'LEB').
card_original_type('consecrate land'/'LEB', 'Enchant Land').
card_original_text('consecrate land'/'LEB', 'All enchantments on target land are destroyed. Land cannot be destroyed or further enchanted until Consecrate Land has been destroyed.').
card_image_name('consecrate land'/'LEB', 'consecrate land').
card_uid('consecrate land'/'LEB', 'LEB:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'LEB', 'Uncommon').
card_artist('consecrate land'/'LEB', 'Jeff A. Menges').
card_multiverse_id('consecrate land'/'LEB', '541').

card_in_set('conservator', 'LEB').
card_original_type('conservator'/'LEB', 'Mono Artifact').
card_original_text('conservator'/'LEB', '{3}: Prevent the loss of up to 2 life.').
card_image_name('conservator'/'LEB', 'conservator').
card_uid('conservator'/'LEB', 'LEB:Conservator:conservator').
card_rarity('conservator'/'LEB', 'Uncommon').
card_artist('conservator'/'LEB', 'Amy Weber').
card_multiverse_id('conservator'/'LEB', '303').

card_in_set('contract from below', 'LEB').
card_original_type('contract from below'/'LEB', 'Sorcery').
card_original_text('contract from below'/'LEB', 'Discard your current hand and draw eight new cards, adding the first drawn to your ante. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('contract from below'/'LEB', 'contract from below').
card_uid('contract from below'/'LEB', 'LEB:Contract from Below:contract from below').
card_rarity('contract from below'/'LEB', 'Rare').
card_artist('contract from below'/'LEB', 'Douglas Shuler').
card_multiverse_id('contract from below'/'LEB', '347').

card_in_set('control magic', 'LEB').
card_original_type('control magic'/'LEB', 'Enchant Creature').
card_original_text('control magic'/'LEB', 'You control target creature until enchantment is discarded or game ends. You can\'t tap target creature this turn, but if it was already tapped it stays tapped until you can untap it. If destroyed, target creature is put in its owner\'s graveyard.').
card_image_name('control magic'/'LEB', 'control magic').
card_uid('control magic'/'LEB', 'LEB:Control Magic:control magic').
card_rarity('control magic'/'LEB', 'Uncommon').
card_artist('control magic'/'LEB', 'Dameon Willich').
card_multiverse_id('control magic'/'LEB', '395').

card_in_set('conversion', 'LEB').
card_original_type('conversion'/'LEB', 'Enchantment').
card_original_text('conversion'/'LEB', 'All mountains are considered plains while Conversion is in play. Pay {W}{W} during upkeep or Conversion is discarded.').
card_image_name('conversion'/'LEB', 'conversion').
card_uid('conversion'/'LEB', 'LEB:Conversion:conversion').
card_rarity('conversion'/'LEB', 'Uncommon').
card_artist('conversion'/'LEB', 'Jesper Myrfors').
card_multiverse_id('conversion'/'LEB', '542').

card_in_set('copper tablet', 'LEB').
card_original_type('copper tablet'/'LEB', 'Continuous Artifact').
card_original_text('copper tablet'/'LEB', 'Copper Tablet does 1 damage to each player during his or her upkeep.').
card_image_name('copper tablet'/'LEB', 'copper tablet').
card_uid('copper tablet'/'LEB', 'LEB:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'LEB', 'Uncommon').
card_artist('copper tablet'/'LEB', 'Amy Weber').
card_multiverse_id('copper tablet'/'LEB', '304').

card_in_set('copy artifact', 'LEB').
card_original_type('copy artifact'/'LEB', 'Enchantment').
card_original_text('copy artifact'/'LEB', 'Select any artifact in play. This enchantment acts as a duplicate of that artifact; enchantment copy is affected by cards that affect either enchantments or artifacts. Enchantment copy remains even if original artifact is destroyed.').
card_image_name('copy artifact'/'LEB', 'copy artifact').
card_uid('copy artifact'/'LEB', 'LEB:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'LEB', 'Rare').
card_artist('copy artifact'/'LEB', 'Amy Weber').
card_multiverse_id('copy artifact'/'LEB', '396').

card_in_set('counterspell', 'LEB').
card_original_type('counterspell'/'LEB', 'Interrupt').
card_original_text('counterspell'/'LEB', 'Counters target spell as it is being cast.').
card_image_name('counterspell'/'LEB', 'counterspell').
card_uid('counterspell'/'LEB', 'LEB:Counterspell:counterspell').
card_rarity('counterspell'/'LEB', 'Uncommon').
card_artist('counterspell'/'LEB', 'Mark Poole').
card_multiverse_id('counterspell'/'LEB', '397').

card_in_set('craw wurm', 'LEB').
card_original_type('craw wurm'/'LEB', 'Summon — Wurm').
card_original_text('craw wurm'/'LEB', '').
card_image_name('craw wurm'/'LEB', 'craw wurm').
card_uid('craw wurm'/'LEB', 'LEB:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'LEB', 'Common').
card_artist('craw wurm'/'LEB', 'Daniel Gelon').
card_flavor_text('craw wurm'/'LEB', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'LEB', '441').

card_in_set('creature bond', 'LEB').
card_original_type('creature bond'/'LEB', 'Enchant Creature').
card_original_text('creature bond'/'LEB', 'If target creature is destroyed, Creature Bond does an amount of damage equal to creature\'s toughness to creature\'s controller.').
card_image_name('creature bond'/'LEB', 'creature bond').
card_uid('creature bond'/'LEB', 'LEB:Creature Bond:creature bond').
card_rarity('creature bond'/'LEB', 'Common').
card_artist('creature bond'/'LEB', 'Anson Maddocks').
card_multiverse_id('creature bond'/'LEB', '398').

card_in_set('crusade', 'LEB').
card_original_type('crusade'/'LEB', 'Enchantment').
card_original_text('crusade'/'LEB', 'All white creatures gain +1/+1.').
card_image_name('crusade'/'LEB', 'crusade').
card_uid('crusade'/'LEB', 'LEB:Crusade:crusade').
card_rarity('crusade'/'LEB', 'Rare').
card_artist('crusade'/'LEB', 'Mark Poole').
card_multiverse_id('crusade'/'LEB', '543').

card_in_set('crystal rod', 'LEB').
card_original_type('crystal rod'/'LEB', 'Poly Artifact').
card_original_text('crystal rod'/'LEB', '{1}: Any blue spell cast by any player gives you 1 life.').
card_image_name('crystal rod'/'LEB', 'crystal rod').
card_uid('crystal rod'/'LEB', 'LEB:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'LEB', 'Uncommon').
card_artist('crystal rod'/'LEB', 'Amy Weber').
card_multiverse_id('crystal rod'/'LEB', '305').

card_in_set('cursed land', 'LEB').
card_original_type('cursed land'/'LEB', 'Enchant Land').
card_original_text('cursed land'/'LEB', 'Cursed Land does 1 damage to target land\'s controller during each upkeep.').
card_image_name('cursed land'/'LEB', 'cursed land').
card_uid('cursed land'/'LEB', 'LEB:Cursed Land:cursed land').
card_rarity('cursed land'/'LEB', 'Uncommon').
card_artist('cursed land'/'LEB', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'LEB', '348').

card_in_set('cyclopean tomb', 'LEB').
card_original_type('cyclopean tomb'/'LEB', 'Mono Artifact').
card_original_text('cyclopean tomb'/'LEB', '{2}: Turn any one non-swamp land into swamp during upkeep. Mark the changed lands with tokens. If Cyclopean Tomb is destroyed, remove one token of your choice each upkeep, returning that land to its original nature.').
card_image_name('cyclopean tomb'/'LEB', 'cyclopean tomb').
card_uid('cyclopean tomb'/'LEB', 'LEB:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'LEB', 'Rare').
card_artist('cyclopean tomb'/'LEB', 'Anson Maddocks').
card_multiverse_id('cyclopean tomb'/'LEB', '306').

card_in_set('dark ritual', 'LEB').
card_original_type('dark ritual'/'LEB', 'Interrupt').
card_original_text('dark ritual'/'LEB', 'Add 3 black mana to your mana pool.').
card_image_name('dark ritual'/'LEB', 'dark ritual').
card_uid('dark ritual'/'LEB', 'LEB:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'LEB', 'Common').
card_artist('dark ritual'/'LEB', 'Sandra Everingham').
card_multiverse_id('dark ritual'/'LEB', '349').

card_in_set('darkpact', 'LEB').
card_original_type('darkpact'/'LEB', 'Sorcery').
card_original_text('darkpact'/'LEB', 'Without looking at it first, swap top card of your library with either card of the ante; this swap is permanent. You must have a card in your library to cast this spell. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('darkpact'/'LEB', 'darkpact').
card_uid('darkpact'/'LEB', 'LEB:Darkpact:darkpact').
card_rarity('darkpact'/'LEB', 'Rare').
card_artist('darkpact'/'LEB', 'Quinton Hoover').
card_multiverse_id('darkpact'/'LEB', '350').

card_in_set('death ward', 'LEB').
card_original_type('death ward'/'LEB', 'Instant').
card_original_text('death ward'/'LEB', 'Regenerates target creature.').
card_image_name('death ward'/'LEB', 'death ward').
card_uid('death ward'/'LEB', 'LEB:Death Ward:death ward').
card_rarity('death ward'/'LEB', 'Common').
card_artist('death ward'/'LEB', 'Mark Poole').
card_multiverse_id('death ward'/'LEB', '544').

card_in_set('deathgrip', 'LEB').
card_original_type('deathgrip'/'LEB', 'Enchantment').
card_original_text('deathgrip'/'LEB', '{B}{B} Destroy a green spell as it is being cast. This action may be played as an interrupt, and does not affect green cards already in play.').
card_image_name('deathgrip'/'LEB', 'deathgrip').
card_uid('deathgrip'/'LEB', 'LEB:Deathgrip:deathgrip').
card_rarity('deathgrip'/'LEB', 'Uncommon').
card_artist('deathgrip'/'LEB', 'Anson Maddocks').
card_multiverse_id('deathgrip'/'LEB', '351').

card_in_set('deathlace', 'LEB').
card_original_type('deathlace'/'LEB', 'Interrupt').
card_original_text('deathlace'/'LEB', 'Changes the color of one card either being played or already in play to black. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('deathlace'/'LEB', 'deathlace').
card_uid('deathlace'/'LEB', 'LEB:Deathlace:deathlace').
card_rarity('deathlace'/'LEB', 'Rare').
card_artist('deathlace'/'LEB', 'Sandra Everingham').
card_multiverse_id('deathlace'/'LEB', '352').

card_in_set('demonic attorney', 'LEB').
card_original_type('demonic attorney'/'LEB', 'Sorcery').
card_original_text('demonic attorney'/'LEB', 'If opponent doesn\'t concede the game immediately, each player must ante an additional card from the top of his or her library. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('demonic attorney'/'LEB', 'demonic attorney').
card_uid('demonic attorney'/'LEB', 'LEB:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'LEB', 'Rare').
card_artist('demonic attorney'/'LEB', 'Daniel Gelon').
card_multiverse_id('demonic attorney'/'LEB', '353').

card_in_set('demonic hordes', 'LEB').
card_original_type('demonic hordes'/'LEB', 'Summon — Demons').
card_original_text('demonic hordes'/'LEB', 'Tap to destroy 1 land. Pay {B}{B}{B} during upkeep or the Hordes become tapped and you lose a land of opponent\'s choice.').
card_image_name('demonic hordes'/'LEB', 'demonic hordes').
card_uid('demonic hordes'/'LEB', 'LEB:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'LEB', 'Rare').
card_artist('demonic hordes'/'LEB', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'LEB', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').
card_multiverse_id('demonic hordes'/'LEB', '354').

card_in_set('demonic tutor', 'LEB').
card_original_type('demonic tutor'/'LEB', 'Sorcery').
card_original_text('demonic tutor'/'LEB', 'You may search your library for one card and take it into your hand. Reshuffle your library afterwards.').
card_image_name('demonic tutor'/'LEB', 'demonic tutor').
card_uid('demonic tutor'/'LEB', 'LEB:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'LEB', 'Uncommon').
card_artist('demonic tutor'/'LEB', 'Douglas Shuler').
card_multiverse_id('demonic tutor'/'LEB', '355').

card_in_set('dingus egg', 'LEB').
card_original_type('dingus egg'/'LEB', 'Continuous Artifact').
card_original_text('dingus egg'/'LEB', 'Whenever anyone loses a land, Egg does 2 damage to that player for each land lost.').
card_image_name('dingus egg'/'LEB', 'dingus egg').
card_uid('dingus egg'/'LEB', 'LEB:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'LEB', 'Rare').
card_artist('dingus egg'/'LEB', 'Dan Frazier').
card_multiverse_id('dingus egg'/'LEB', '307').

card_in_set('disenchant', 'LEB').
card_original_type('disenchant'/'LEB', 'Instant').
card_original_text('disenchant'/'LEB', 'Target enchantment or artifact must be discarded.').
card_image_name('disenchant'/'LEB', 'disenchant').
card_uid('disenchant'/'LEB', 'LEB:Disenchant:disenchant').
card_rarity('disenchant'/'LEB', 'Common').
card_artist('disenchant'/'LEB', 'Amy Weber').
card_multiverse_id('disenchant'/'LEB', '545').

card_in_set('disintegrate', 'LEB').
card_original_type('disintegrate'/'LEB', 'Sorcery').
card_original_text('disintegrate'/'LEB', 'Disintegrate does X damage to one target. If target dies this turn, it is removed from game entirely and cannot be regenerated. Return target to its owner\'s deck only when game is over.').
card_image_name('disintegrate'/'LEB', 'disintegrate').
card_uid('disintegrate'/'LEB', 'LEB:Disintegrate:disintegrate').
card_rarity('disintegrate'/'LEB', 'Common').
card_artist('disintegrate'/'LEB', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'LEB', '483').

card_in_set('disrupting scepter', 'LEB').
card_original_type('disrupting scepter'/'LEB', 'Mono Artifact').
card_original_text('disrupting scepter'/'LEB', '{3}: Opponent must discard one card of his or her choice. Can only be used during your turn.').
card_image_name('disrupting scepter'/'LEB', 'disrupting scepter').
card_uid('disrupting scepter'/'LEB', 'LEB:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'LEB', 'Rare').
card_artist('disrupting scepter'/'LEB', 'Dan Frazier').
card_multiverse_id('disrupting scepter'/'LEB', '308').

card_in_set('dragon whelp', 'LEB').
card_original_type('dragon whelp'/'LEB', 'Summon — Dragon').
card_original_text('dragon whelp'/'LEB', 'Flying\n{R} +1/+0 until end of turn; if more than {R}{R}{R} is spent in this way, Dragon Whelp is destroyed at end of turn.').
card_image_name('dragon whelp'/'LEB', 'dragon whelp').
card_uid('dragon whelp'/'LEB', 'LEB:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'LEB', 'Uncommon').
card_artist('dragon whelp'/'LEB', 'Amy Weber').
card_flavor_text('dragon whelp'/'LEB', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').
card_multiverse_id('dragon whelp'/'LEB', '484').

card_in_set('drain life', 'LEB').
card_original_type('drain life'/'LEB', 'Sorcery').
card_original_text('drain life'/'LEB', 'Drain Life does 1 damage to a single target for each {B} spent in addition to the casting cost. Caster gains 1 life for each damage inflicted. If you drain life from a creature, you cannot gain more life than the creature\'s toughness.').
card_image_name('drain life'/'LEB', 'drain life').
card_uid('drain life'/'LEB', 'LEB:Drain Life:drain life').
card_rarity('drain life'/'LEB', 'Common').
card_artist('drain life'/'LEB', 'Douglas Shuler').
card_multiverse_id('drain life'/'LEB', '356').

card_in_set('drain power', 'LEB').
card_original_type('drain power'/'LEB', 'Sorcery').
card_original_text('drain power'/'LEB', 'Tap all opponent\'s land, taking all this mana and all mana in opponent\'s mana pool into your mana pool. You can\'t tap fewer than all opponent\'s lands.').
card_image_name('drain power'/'LEB', 'drain power').
card_uid('drain power'/'LEB', 'LEB:Drain Power:drain power').
card_rarity('drain power'/'LEB', 'Rare').
card_artist('drain power'/'LEB', 'Douglas Shuler').
card_multiverse_id('drain power'/'LEB', '399').

card_in_set('drudge skeletons', 'LEB').
card_original_type('drudge skeletons'/'LEB', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'LEB', '{B} Regenerates').
card_image_name('drudge skeletons'/'LEB', 'drudge skeletons').
card_uid('drudge skeletons'/'LEB', 'LEB:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'LEB', 'Common').
card_artist('drudge skeletons'/'LEB', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'LEB', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').
card_multiverse_id('drudge skeletons'/'LEB', '357').

card_in_set('dwarven demolition team', 'LEB').
card_original_type('dwarven demolition team'/'LEB', 'Summon — Dwarves').
card_original_text('dwarven demolition team'/'LEB', 'Tap to destroy a wall.').
card_image_name('dwarven demolition team'/'LEB', 'dwarven demolition team').
card_uid('dwarven demolition team'/'LEB', 'LEB:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'LEB', 'Uncommon').
card_artist('dwarven demolition team'/'LEB', 'Kev Brockschmidt').
card_flavor_text('dwarven demolition team'/'LEB', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').
card_multiverse_id('dwarven demolition team'/'LEB', '485').

card_in_set('dwarven warriors', 'LEB').
card_original_type('dwarven warriors'/'LEB', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'LEB', 'Tap to make a creature of power no greater than 2 unblockable until end of turn. Other cards may later be used to increase target creature\'s power beyond 2 after defense is chosen.').
card_image_name('dwarven warriors'/'LEB', 'dwarven warriors').
card_uid('dwarven warriors'/'LEB', 'LEB:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'LEB', 'Common').
card_artist('dwarven warriors'/'LEB', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'LEB', '486').

card_in_set('earth elemental', 'LEB').
card_original_type('earth elemental'/'LEB', 'Summon — Elemental').
card_original_text('earth elemental'/'LEB', '').
card_image_name('earth elemental'/'LEB', 'earth elemental').
card_uid('earth elemental'/'LEB', 'LEB:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'LEB', 'Uncommon').
card_artist('earth elemental'/'LEB', 'Dan Frazier').
card_flavor_text('earth elemental'/'LEB', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'LEB', '487').

card_in_set('earthbind', 'LEB').
card_original_type('earthbind'/'LEB', 'Enchant Flying Creature').
card_original_text('earthbind'/'LEB', 'Earthbind does 2 damage to target flying creature, which also loses flying ability.').
card_image_name('earthbind'/'LEB', 'earthbind').
card_uid('earthbind'/'LEB', 'LEB:Earthbind:earthbind').
card_rarity('earthbind'/'LEB', 'Common').
card_artist('earthbind'/'LEB', 'Quinton Hoover').
card_multiverse_id('earthbind'/'LEB', '488').

card_in_set('earthquake', 'LEB').
card_original_type('earthquake'/'LEB', 'Sorcery').
card_original_text('earthquake'/'LEB', 'Does X damage to each player and each non-flying creature in play.').
card_image_name('earthquake'/'LEB', 'earthquake').
card_uid('earthquake'/'LEB', 'LEB:Earthquake:earthquake').
card_rarity('earthquake'/'LEB', 'Rare').
card_artist('earthquake'/'LEB', 'Dan Frazier').
card_multiverse_id('earthquake'/'LEB', '489').

card_in_set('elvish archers', 'LEB').
card_original_type('elvish archers'/'LEB', 'Summon — Elves').
card_original_text('elvish archers'/'LEB', 'First strike').
card_image_name('elvish archers'/'LEB', 'elvish archers').
card_uid('elvish archers'/'LEB', 'LEB:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'LEB', 'Rare').
card_artist('elvish archers'/'LEB', 'Anson Maddocks').
card_flavor_text('elvish archers'/'LEB', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'LEB', '442').

card_in_set('evil presence', 'LEB').
card_original_type('evil presence'/'LEB', 'Enchant Land').
card_original_text('evil presence'/'LEB', 'Target land is now a swamp.').
card_image_name('evil presence'/'LEB', 'evil presence').
card_uid('evil presence'/'LEB', 'LEB:Evil Presence:evil presence').
card_rarity('evil presence'/'LEB', 'Uncommon').
card_artist('evil presence'/'LEB', 'Sandra Everingham').
card_multiverse_id('evil presence'/'LEB', '358').

card_in_set('false orders', 'LEB').
card_original_type('false orders'/'LEB', 'Instant').
card_original_text('false orders'/'LEB', 'You decide whether and how one defending creature blocks, though you can\'t make a choice the defender couldn\'t legally make. Play after defender has chosen defense but before damage is dealt.').
card_image_name('false orders'/'LEB', 'false orders').
card_uid('false orders'/'LEB', 'LEB:False Orders:false orders').
card_rarity('false orders'/'LEB', 'Common').
card_artist('false orders'/'LEB', 'Anson Maddocks').
card_multiverse_id('false orders'/'LEB', '490').

card_in_set('farmstead', 'LEB').
card_original_type('farmstead'/'LEB', 'Enchant Land').
card_original_text('farmstead'/'LEB', 'Target land\'s controller gains 1 life each upkeep if {W}{W} is spent. Target land still generates mana as usual.').
card_image_name('farmstead'/'LEB', 'farmstead').
card_uid('farmstead'/'LEB', 'LEB:Farmstead:farmstead').
card_rarity('farmstead'/'LEB', 'Rare').
card_artist('farmstead'/'LEB', 'Mark Poole').
card_multiverse_id('farmstead'/'LEB', '546').

card_in_set('fastbond', 'LEB').
card_original_type('fastbond'/'LEB', 'Enchantment').
card_original_text('fastbond'/'LEB', 'You may put as many lands into play as you want each turn. Fastbond does 1 damage to you for every land beyond the first that you play in a single turn.').
card_image_name('fastbond'/'LEB', 'fastbond').
card_uid('fastbond'/'LEB', 'LEB:Fastbond:fastbond').
card_rarity('fastbond'/'LEB', 'Rare').
card_artist('fastbond'/'LEB', 'Mark Poole').
card_multiverse_id('fastbond'/'LEB', '443').

card_in_set('fear', 'LEB').
card_original_type('fear'/'LEB', 'Enchant Creature').
card_original_text('fear'/'LEB', 'Target creature cannot be blocked by any creatures other than artifact creatures and black creatures.').
card_image_name('fear'/'LEB', 'fear').
card_uid('fear'/'LEB', 'LEB:Fear:fear').
card_rarity('fear'/'LEB', 'Common').
card_artist('fear'/'LEB', 'Mark Poole').
card_multiverse_id('fear'/'LEB', '359').

card_in_set('feedback', 'LEB').
card_original_type('feedback'/'LEB', 'Enchant Enchantment').
card_original_text('feedback'/'LEB', 'Feedback does 1 damage to controller of target enchantment during each upkeep.').
card_image_name('feedback'/'LEB', 'feedback').
card_uid('feedback'/'LEB', 'LEB:Feedback:feedback').
card_rarity('feedback'/'LEB', 'Uncommon').
card_artist('feedback'/'LEB', 'Quinton Hoover').
card_multiverse_id('feedback'/'LEB', '400').

card_in_set('fire elemental', 'LEB').
card_original_type('fire elemental'/'LEB', 'Summon — Elemental').
card_original_text('fire elemental'/'LEB', '').
card_image_name('fire elemental'/'LEB', 'fire elemental').
card_uid('fire elemental'/'LEB', 'LEB:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'LEB', 'Uncommon').
card_artist('fire elemental'/'LEB', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'LEB', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'LEB', '491').

card_in_set('fireball', 'LEB').
card_original_type('fireball'/'LEB', 'Sorcery').
card_original_text('fireball'/'LEB', 'Fireball does X damage total, divided evenly (round down) among any number of targets. Pay 1 extra mana for each target beyond the first.').
card_image_name('fireball'/'LEB', 'fireball').
card_uid('fireball'/'LEB', 'LEB:Fireball:fireball').
card_rarity('fireball'/'LEB', 'Common').
card_artist('fireball'/'LEB', 'Mark Tedin').
card_multiverse_id('fireball'/'LEB', '492').

card_in_set('firebreathing', 'LEB').
card_original_type('firebreathing'/'LEB', 'Enchant Creature').
card_original_text('firebreathing'/'LEB', '{R} +1/+0').
card_image_name('firebreathing'/'LEB', 'firebreathing').
card_uid('firebreathing'/'LEB', 'LEB:Firebreathing:firebreathing').
card_rarity('firebreathing'/'LEB', 'Common').
card_artist('firebreathing'/'LEB', 'Dan Frazier').
card_flavor_text('firebreathing'/'LEB', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').
card_multiverse_id('firebreathing'/'LEB', '493').

card_in_set('flashfires', 'LEB').
card_original_type('flashfires'/'LEB', 'Sorcery').
card_original_text('flashfires'/'LEB', 'All plains in play are destroyed.').
card_image_name('flashfires'/'LEB', 'flashfires').
card_uid('flashfires'/'LEB', 'LEB:Flashfires:flashfires').
card_rarity('flashfires'/'LEB', 'Uncommon').
card_artist('flashfires'/'LEB', 'Dameon Willich').
card_multiverse_id('flashfires'/'LEB', '494').

card_in_set('flight', 'LEB').
card_original_type('flight'/'LEB', 'Enchant Creature').
card_original_text('flight'/'LEB', 'Target creature is now a flying creature.').
card_image_name('flight'/'LEB', 'flight').
card_uid('flight'/'LEB', 'LEB:Flight:flight').
card_rarity('flight'/'LEB', 'Common').
card_artist('flight'/'LEB', 'Anson Maddocks').
card_multiverse_id('flight'/'LEB', '401').

card_in_set('fog', 'LEB').
card_original_type('fog'/'LEB', 'Instant').
card_original_text('fog'/'LEB', 'Creatures attack and block as normal, but none deal any damage. All attacking creatures are still tapped. Play any time before attack damage is dealt.').
card_image_name('fog'/'LEB', 'fog').
card_uid('fog'/'LEB', 'LEB:Fog:fog').
card_rarity('fog'/'LEB', 'Common').
card_artist('fog'/'LEB', 'Jesper Myrfors').
card_multiverse_id('fog'/'LEB', '444').

card_in_set('force of nature', 'LEB').
card_original_type('force of nature'/'LEB', 'Summon — Force').
card_original_text('force of nature'/'LEB', 'Trample\nYou must pay {G}{G}{G}{G} during upkeep or Force of Nature does 8 damage to you. You may still attack with Force of Nature even if you failed to pay the upkeep.').
card_image_name('force of nature'/'LEB', 'force of nature').
card_uid('force of nature'/'LEB', 'LEB:Force of Nature:force of nature').
card_rarity('force of nature'/'LEB', 'Rare').
card_artist('force of nature'/'LEB', 'Douglas Shuler').
card_multiverse_id('force of nature'/'LEB', '445').

card_in_set('forcefield', 'LEB').
card_original_type('forcefield'/'LEB', 'Poly Artifact').
card_original_text('forcefield'/'LEB', '{1}: Lose only 1 life to an unblocked creature.').
card_image_name('forcefield'/'LEB', 'forcefield').
card_uid('forcefield'/'LEB', 'LEB:Forcefield:forcefield').
card_rarity('forcefield'/'LEB', 'Rare').
card_artist('forcefield'/'LEB', 'Dan Frazier').
card_multiverse_id('forcefield'/'LEB', '309').

card_in_set('forest', 'LEB').
card_original_type('forest'/'LEB', 'Land').
card_original_text('forest'/'LEB', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'LEB', 'forest1').
card_uid('forest'/'LEB', 'LEB:Forest:forest1').
card_rarity('forest'/'LEB', 'Basic Land').
card_artist('forest'/'LEB', 'Christopher Rush').
card_multiverse_id('forest'/'LEB', '588').

card_in_set('forest', 'LEB').
card_original_type('forest'/'LEB', 'Land').
card_original_text('forest'/'LEB', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'LEB', 'forest2').
card_uid('forest'/'LEB', 'LEB:Forest:forest2').
card_rarity('forest'/'LEB', 'Basic Land').
card_artist('forest'/'LEB', 'Christopher Rush').
card_multiverse_id('forest'/'LEB', '586').

card_in_set('forest', 'LEB').
card_original_type('forest'/'LEB', 'Land').
card_original_text('forest'/'LEB', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'LEB', 'forest3').
card_uid('forest'/'LEB', 'LEB:Forest:forest3').
card_rarity('forest'/'LEB', 'Basic Land').
card_artist('forest'/'LEB', 'Christopher Rush').
card_multiverse_id('forest'/'LEB', '587').

card_in_set('fork', 'LEB').
card_original_type('fork'/'LEB', 'Interrupt').
card_original_text('fork'/'LEB', 'Any sorcery or instant spell just cast is doubled. Treat Fork as an exact copy of target spell except that Fork remains red. Copy and original may have different targets.').
card_image_name('fork'/'LEB', 'fork').
card_uid('fork'/'LEB', 'LEB:Fork:fork').
card_rarity('fork'/'LEB', 'Rare').
card_artist('fork'/'LEB', 'Amy Weber').
card_multiverse_id('fork'/'LEB', '495').

card_in_set('frozen shade', 'LEB').
card_original_type('frozen shade'/'LEB', 'Summon — Shade').
card_original_text('frozen shade'/'LEB', '{B} +1/+1').
card_image_name('frozen shade'/'LEB', 'frozen shade').
card_uid('frozen shade'/'LEB', 'LEB:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'LEB', 'Common').
card_artist('frozen shade'/'LEB', 'Douglas Shuler').
card_flavor_text('frozen shade'/'LEB', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'LEB', '360').

card_in_set('fungusaur', 'LEB').
card_original_type('fungusaur'/'LEB', 'Summon — Fungusaur').
card_original_text('fungusaur'/'LEB', 'Each time Fungusaur is damaged but not destroyed, put a +1/+1 counter on it.').
card_image_name('fungusaur'/'LEB', 'fungusaur').
card_uid('fungusaur'/'LEB', 'LEB:Fungusaur:fungusaur').
card_rarity('fungusaur'/'LEB', 'Rare').
card_artist('fungusaur'/'LEB', 'Daniel Gelon').
card_flavor_text('fungusaur'/'LEB', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'LEB', '446').

card_in_set('gaea\'s liege', 'LEB').
card_original_type('gaea\'s liege'/'LEB', 'Summon — Gaea\'s Liege').
card_original_text('gaea\'s liege'/'LEB', 'When defending, Gaea\'s Liege has power and toughness equal to the number of forests you have in play; when it\'s attacking, they are equal to the number of forests opponent has in play. Tap to turn any one land into a forest until Gaea\'s Liege leaves play. Mark changed lands with counters, removing the counters when Gaea\'s Liege leaves play.').
card_image_name('gaea\'s liege'/'LEB', 'gaea\'s liege').
card_uid('gaea\'s liege'/'LEB', 'LEB:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'LEB', 'Rare').
card_artist('gaea\'s liege'/'LEB', 'Dameon Willich').
card_multiverse_id('gaea\'s liege'/'LEB', '447').

card_in_set('gauntlet of might', 'LEB').
card_original_type('gauntlet of might'/'LEB', 'Continuous Artifact').
card_original_text('gauntlet of might'/'LEB', 'All red creatures gain +1/+1 and all mountains provide an extra red mana when tapped.').
card_image_name('gauntlet of might'/'LEB', 'gauntlet of might').
card_uid('gauntlet of might'/'LEB', 'LEB:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'LEB', 'Rare').
card_artist('gauntlet of might'/'LEB', 'Christopher Rush').
card_multiverse_id('gauntlet of might'/'LEB', '310').

card_in_set('giant growth', 'LEB').
card_original_type('giant growth'/'LEB', 'Instant').
card_original_text('giant growth'/'LEB', 'Target creature gains +3/+3 until end of turn.').
card_image_name('giant growth'/'LEB', 'giant growth').
card_uid('giant growth'/'LEB', 'LEB:Giant Growth:giant growth').
card_rarity('giant growth'/'LEB', 'Common').
card_artist('giant growth'/'LEB', 'Sandra Everingham').
card_multiverse_id('giant growth'/'LEB', '448').

card_in_set('giant spider', 'LEB').
card_original_type('giant spider'/'LEB', 'Summon — Spider').
card_original_text('giant spider'/'LEB', 'Does not fly, but can block flying creatures.').
card_image_name('giant spider'/'LEB', 'giant spider').
card_uid('giant spider'/'LEB', 'LEB:Giant Spider:giant spider').
card_rarity('giant spider'/'LEB', 'Common').
card_artist('giant spider'/'LEB', 'Sandra Everingham').
card_flavor_text('giant spider'/'LEB', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'LEB', '449').

card_in_set('glasses of urza', 'LEB').
card_original_type('glasses of urza'/'LEB', 'Mono Artifact').
card_original_text('glasses of urza'/'LEB', 'You may look at opponent\'s hand.').
card_image_name('glasses of urza'/'LEB', 'glasses of urza').
card_uid('glasses of urza'/'LEB', 'LEB:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'LEB', 'Uncommon').
card_artist('glasses of urza'/'LEB', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'LEB', '311').

card_in_set('gloom', 'LEB').
card_original_type('gloom'/'LEB', 'Enchantment').
card_original_text('gloom'/'LEB', 'White spells cost 3 more mana to cast. Circles of Protection cost 3 more mana to use.').
card_image_name('gloom'/'LEB', 'gloom').
card_uid('gloom'/'LEB', 'LEB:Gloom:gloom').
card_rarity('gloom'/'LEB', 'Uncommon').
card_artist('gloom'/'LEB', 'Dan Frazier').
card_multiverse_id('gloom'/'LEB', '361').

card_in_set('goblin balloon brigade', 'LEB').
card_original_type('goblin balloon brigade'/'LEB', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'LEB', '{R} Goblins gain flying ability until end of turn. Controller may not choose to make Goblins fly after they have been blocked.').
card_image_name('goblin balloon brigade'/'LEB', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'LEB', 'LEB:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'LEB', 'Uncommon').
card_artist('goblin balloon brigade'/'LEB', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'LEB', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').
card_multiverse_id('goblin balloon brigade'/'LEB', '496').

card_in_set('goblin king', 'LEB').
card_original_type('goblin king'/'LEB', 'Summon — Goblin King').
card_original_text('goblin king'/'LEB', 'Goblins in play gain mountainwalk and +1/+1 while this card remains in play.').
card_image_name('goblin king'/'LEB', 'goblin king').
card_uid('goblin king'/'LEB', 'LEB:Goblin King:goblin king').
card_rarity('goblin king'/'LEB', 'Rare').
card_artist('goblin king'/'LEB', 'Jesper Myrfors').
card_flavor_text('goblin king'/'LEB', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').
card_multiverse_id('goblin king'/'LEB', '497').

card_in_set('granite gargoyle', 'LEB').
card_original_type('granite gargoyle'/'LEB', 'Summon — Gargoyle').
card_original_text('granite gargoyle'/'LEB', 'Flying; {R} +0/+1 until end of turn.').
card_image_name('granite gargoyle'/'LEB', 'granite gargoyle').
card_uid('granite gargoyle'/'LEB', 'LEB:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'LEB', 'Rare').
card_artist('granite gargoyle'/'LEB', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'LEB', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook, by Asmoranomardicadaistinaculdacar').
card_multiverse_id('granite gargoyle'/'LEB', '498').

card_in_set('gray ogre', 'LEB').
card_original_type('gray ogre'/'LEB', 'Summon — Ogre').
card_original_text('gray ogre'/'LEB', '').
card_image_name('gray ogre'/'LEB', 'gray ogre').
card_uid('gray ogre'/'LEB', 'LEB:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'LEB', 'Common').
card_artist('gray ogre'/'LEB', 'Dan Frazier').
card_flavor_text('gray ogre'/'LEB', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').
card_multiverse_id('gray ogre'/'LEB', '499').

card_in_set('green ward', 'LEB').
card_original_type('green ward'/'LEB', 'Enchant Creature').
card_original_text('green ward'/'LEB', 'Target creature gains protection from green.').
card_image_name('green ward'/'LEB', 'green ward').
card_uid('green ward'/'LEB', 'LEB:Green Ward:green ward').
card_rarity('green ward'/'LEB', 'Uncommon').
card_artist('green ward'/'LEB', 'Dan Frazier').
card_multiverse_id('green ward'/'LEB', '547').

card_in_set('grizzly bears', 'LEB').
card_original_type('grizzly bears'/'LEB', 'Summon — Bears').
card_original_text('grizzly bears'/'LEB', '').
card_image_name('grizzly bears'/'LEB', 'grizzly bears').
card_uid('grizzly bears'/'LEB', 'LEB:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'LEB', 'Common').
card_artist('grizzly bears'/'LEB', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'LEB', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'LEB', '450').

card_in_set('guardian angel', 'LEB').
card_original_type('guardian angel'/'LEB', 'Instant').
card_original_text('guardian angel'/'LEB', 'Prevents X damage from being dealt to any one target. Any further damage to the same target this turn can be canceled by spending 1 mana per point of damage to be canceled.').
card_image_name('guardian angel'/'LEB', 'guardian angel').
card_uid('guardian angel'/'LEB', 'LEB:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'LEB', 'Common').
card_artist('guardian angel'/'LEB', 'Anson Maddocks').
card_multiverse_id('guardian angel'/'LEB', '548').

card_in_set('healing salve', 'LEB').
card_original_type('healing salve'/'LEB', 'Instant').
card_original_text('healing salve'/'LEB', 'Gain 3 life, or prevent up to 3 damage from being dealt to a single target.').
card_image_name('healing salve'/'LEB', 'healing salve').
card_uid('healing salve'/'LEB', 'LEB:Healing Salve:healing salve').
card_rarity('healing salve'/'LEB', 'Common').
card_artist('healing salve'/'LEB', 'Dan Frazier').
card_multiverse_id('healing salve'/'LEB', '549').

card_in_set('helm of chatzuk', 'LEB').
card_original_type('helm of chatzuk'/'LEB', 'Mono Artifact').
card_original_text('helm of chatzuk'/'LEB', '{1}: You may give one creature the ability to band until end of turn.').
card_image_name('helm of chatzuk'/'LEB', 'helm of chatzuk').
card_uid('helm of chatzuk'/'LEB', 'LEB:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'LEB', 'Rare').
card_artist('helm of chatzuk'/'LEB', 'Mark Tedin').
card_multiverse_id('helm of chatzuk'/'LEB', '312').

card_in_set('hill giant', 'LEB').
card_original_type('hill giant'/'LEB', 'Summon — Giant').
card_original_text('hill giant'/'LEB', '').
card_image_name('hill giant'/'LEB', 'hill giant').
card_uid('hill giant'/'LEB', 'LEB:Hill Giant:hill giant').
card_rarity('hill giant'/'LEB', 'Common').
card_artist('hill giant'/'LEB', 'Dan Frazier').
card_flavor_text('hill giant'/'LEB', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'LEB', '500').

card_in_set('holy armor', 'LEB').
card_original_type('holy armor'/'LEB', 'Enchant Creature').
card_original_text('holy armor'/'LEB', 'Target creature gains +0/+2. {W} Target creature gets extra +0/+1 until end of turn.').
card_image_name('holy armor'/'LEB', 'holy armor').
card_uid('holy armor'/'LEB', 'LEB:Holy Armor:holy armor').
card_rarity('holy armor'/'LEB', 'Common').
card_artist('holy armor'/'LEB', 'Melissa A. Benson').
card_multiverse_id('holy armor'/'LEB', '550').

card_in_set('holy strength', 'LEB').
card_original_type('holy strength'/'LEB', 'Enchant Creature').
card_original_text('holy strength'/'LEB', 'Target creature gains +1/+2.').
card_image_name('holy strength'/'LEB', 'holy strength').
card_uid('holy strength'/'LEB', 'LEB:Holy Strength:holy strength').
card_rarity('holy strength'/'LEB', 'Common').
card_artist('holy strength'/'LEB', 'Anson Maddocks').
card_multiverse_id('holy strength'/'LEB', '551').

card_in_set('howl from beyond', 'LEB').
card_original_type('howl from beyond'/'LEB', 'Instant').
card_original_text('howl from beyond'/'LEB', 'Target creature gains +X/+0 until end of turn.').
card_image_name('howl from beyond'/'LEB', 'howl from beyond').
card_uid('howl from beyond'/'LEB', 'LEB:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'LEB', 'Common').
card_artist('howl from beyond'/'LEB', 'Mark Poole').
card_multiverse_id('howl from beyond'/'LEB', '362').

card_in_set('howling mine', 'LEB').
card_original_type('howling mine'/'LEB', 'Continuous Artifact').
card_original_text('howling mine'/'LEB', 'Each player draws one extra card each turn during his or her draw phase.').
card_image_name('howling mine'/'LEB', 'howling mine').
card_uid('howling mine'/'LEB', 'LEB:Howling Mine:howling mine').
card_rarity('howling mine'/'LEB', 'Rare').
card_artist('howling mine'/'LEB', 'Mark Poole').
card_multiverse_id('howling mine'/'LEB', '313').

card_in_set('hurloon minotaur', 'LEB').
card_original_type('hurloon minotaur'/'LEB', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'LEB', '').
card_image_name('hurloon minotaur'/'LEB', 'hurloon minotaur').
card_uid('hurloon minotaur'/'LEB', 'LEB:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'LEB', 'Common').
card_artist('hurloon minotaur'/'LEB', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'LEB', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'LEB', '501').

card_in_set('hurricane', 'LEB').
card_original_type('hurricane'/'LEB', 'Sorcery').
card_original_text('hurricane'/'LEB', 'All players and flying creatures suffer X damage.').
card_image_name('hurricane'/'LEB', 'hurricane').
card_uid('hurricane'/'LEB', 'LEB:Hurricane:hurricane').
card_rarity('hurricane'/'LEB', 'Uncommon').
card_artist('hurricane'/'LEB', 'Dameon Willich').
card_multiverse_id('hurricane'/'LEB', '451').

card_in_set('hypnotic specter', 'LEB').
card_original_type('hypnotic specter'/'LEB', 'Summon — Specter').
card_original_text('hypnotic specter'/'LEB', 'Flying\nAn opponent damaged by Specter must discard a card at random from his or her hand. Ignore this effect if opponent has no cards left in hand.').
card_image_name('hypnotic specter'/'LEB', 'hypnotic specter').
card_uid('hypnotic specter'/'LEB', 'LEB:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'LEB', 'Uncommon').
card_artist('hypnotic specter'/'LEB', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'LEB', '\"...There was no trace/ Of aught on that illumined face...\"\n—Samuel Coleridge, \"Phantom\"').
card_multiverse_id('hypnotic specter'/'LEB', '363').

card_in_set('ice storm', 'LEB').
card_original_type('ice storm'/'LEB', 'Sorcery').
card_original_text('ice storm'/'LEB', 'Destroys any one land.').
card_image_name('ice storm'/'LEB', 'ice storm').
card_uid('ice storm'/'LEB', 'LEB:Ice Storm:ice storm').
card_rarity('ice storm'/'LEB', 'Uncommon').
card_artist('ice storm'/'LEB', 'Dan Frazier').
card_multiverse_id('ice storm'/'LEB', '452').

card_in_set('icy manipulator', 'LEB').
card_original_type('icy manipulator'/'LEB', 'Mono Artifact').
card_original_text('icy manipulator'/'LEB', '{1}: You may tap any land, creature, or artifact in play on either side. No effects are generated by the target card.').
card_image_name('icy manipulator'/'LEB', 'icy manipulator').
card_uid('icy manipulator'/'LEB', 'LEB:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'LEB', 'Uncommon').
card_artist('icy manipulator'/'LEB', 'Douglas Shuler').
card_multiverse_id('icy manipulator'/'LEB', '314').

card_in_set('illusionary mask', 'LEB').
card_original_type('illusionary mask'/'LEB', 'Poly Artifact').
card_original_text('illusionary mask'/'LEB', '{X} You can summon a creature face down so opponent doesn\'t know what it is. The X cost can be any amount of mana, even 0; it serves to hide the true casting cost of the creature, which you still have to spend. As soon as a face-down creature receives damage, deals damage, or is tapped, you must turn it face up.').
card_image_name('illusionary mask'/'LEB', 'illusionary mask').
card_uid('illusionary mask'/'LEB', 'LEB:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'LEB', 'Rare').
card_artist('illusionary mask'/'LEB', 'Amy Weber').
card_multiverse_id('illusionary mask'/'LEB', '315').

card_in_set('instill energy', 'LEB').
card_original_type('instill energy'/'LEB', 'Enchant Creature').
card_original_text('instill energy'/'LEB', 'You may untap target creature both during your untap phase and one additional time during your turn. Target creature may attack the turn it comes into play.').
card_image_name('instill energy'/'LEB', 'instill energy').
card_uid('instill energy'/'LEB', 'LEB:Instill Energy:instill energy').
card_rarity('instill energy'/'LEB', 'Uncommon').
card_artist('instill energy'/'LEB', 'Dameon Willich').
card_multiverse_id('instill energy'/'LEB', '453').

card_in_set('invisibility', 'LEB').
card_original_type('invisibility'/'LEB', 'Enchant Creature').
card_original_text('invisibility'/'LEB', 'Target creature can be blocked only by walls.').
card_image_name('invisibility'/'LEB', 'invisibility').
card_uid('invisibility'/'LEB', 'LEB:Invisibility:invisibility').
card_rarity('invisibility'/'LEB', 'Common').
card_artist('invisibility'/'LEB', 'Anson Maddocks').
card_multiverse_id('invisibility'/'LEB', '402').

card_in_set('iron star', 'LEB').
card_original_type('iron star'/'LEB', 'Poly Artifact').
card_original_text('iron star'/'LEB', '{1}: Any red spell cast by any player gives you 1 life.').
card_image_name('iron star'/'LEB', 'iron star').
card_uid('iron star'/'LEB', 'LEB:Iron Star:iron star').
card_rarity('iron star'/'LEB', 'Uncommon').
card_artist('iron star'/'LEB', 'Dan Frazier').
card_multiverse_id('iron star'/'LEB', '316').

card_in_set('ironclaw orcs', 'LEB').
card_original_type('ironclaw orcs'/'LEB', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'LEB', 'Cannot be used to block any creature of power more than 1.').
card_image_name('ironclaw orcs'/'LEB', 'ironclaw orcs').
card_uid('ironclaw orcs'/'LEB', 'LEB:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'LEB', 'Common').
card_artist('ironclaw orcs'/'LEB', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'LEB', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').
card_multiverse_id('ironclaw orcs'/'LEB', '502').

card_in_set('ironroot treefolk', 'LEB').
card_original_type('ironroot treefolk'/'LEB', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'LEB', '').
card_image_name('ironroot treefolk'/'LEB', 'ironroot treefolk').
card_uid('ironroot treefolk'/'LEB', 'LEB:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'LEB', 'Common').
card_artist('ironroot treefolk'/'LEB', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'LEB', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').
card_multiverse_id('ironroot treefolk'/'LEB', '454').

card_in_set('island', 'LEB').
card_original_type('island'/'LEB', 'Land').
card_original_text('island'/'LEB', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'LEB', 'island1').
card_uid('island'/'LEB', 'LEB:Island:island1').
card_rarity('island'/'LEB', 'Basic Land').
card_artist('island'/'LEB', 'Mark Poole').
card_multiverse_id('island'/'LEB', '594').

card_in_set('island', 'LEB').
card_original_type('island'/'LEB', 'Land').
card_original_text('island'/'LEB', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'LEB', 'island2').
card_uid('island'/'LEB', 'LEB:Island:island2').
card_rarity('island'/'LEB', 'Basic Land').
card_artist('island'/'LEB', 'Mark Poole').
card_multiverse_id('island'/'LEB', '592').

card_in_set('island', 'LEB').
card_original_type('island'/'LEB', 'Land').
card_original_text('island'/'LEB', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'LEB', 'island3').
card_uid('island'/'LEB', 'LEB:Island:island3').
card_rarity('island'/'LEB', 'Basic Land').
card_artist('island'/'LEB', 'Mark Poole').
card_multiverse_id('island'/'LEB', '593').

card_in_set('island sanctuary', 'LEB').
card_original_type('island sanctuary'/'LEB', 'Enchantment').
card_original_text('island sanctuary'/'LEB', 'You may decline to draw a card from your library during draw phase. In exchange, until start of your next turn the only creatures that may attack you are those that fly or have islandwalk.').
card_image_name('island sanctuary'/'LEB', 'island sanctuary').
card_uid('island sanctuary'/'LEB', 'LEB:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'LEB', 'Rare').
card_artist('island sanctuary'/'LEB', 'Mark Poole').
card_multiverse_id('island sanctuary'/'LEB', '552').

card_in_set('ivory cup', 'LEB').
card_original_type('ivory cup'/'LEB', 'Poly Artifact').
card_original_text('ivory cup'/'LEB', '{1}: Any white spell cast by any player gives you 1 life.').
card_image_name('ivory cup'/'LEB', 'ivory cup').
card_uid('ivory cup'/'LEB', 'LEB:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'LEB', 'Uncommon').
card_artist('ivory cup'/'LEB', 'Anson Maddocks').
card_multiverse_id('ivory cup'/'LEB', '317').

card_in_set('jade monolith', 'LEB').
card_original_type('jade monolith'/'LEB', 'Poly Artifact').
card_original_text('jade monolith'/'LEB', '{1}: You may take damage done to any creature on yourself instead, but you must take all of it. Source of damage is unchanged.').
card_image_name('jade monolith'/'LEB', 'jade monolith').
card_uid('jade monolith'/'LEB', 'LEB:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'LEB', 'Rare').
card_artist('jade monolith'/'LEB', 'Anson Maddocks').
card_multiverse_id('jade monolith'/'LEB', '318').

card_in_set('jade statue', 'LEB').
card_original_type('jade statue'/'LEB', 'Artifact').
card_original_text('jade statue'/'LEB', '{2}: Jade Statue becomes a creature for the duration of the current attack exchange. Can be a creature only during an attack or defense.').
card_image_name('jade statue'/'LEB', 'jade statue').
card_uid('jade statue'/'LEB', 'LEB:Jade Statue:jade statue').
card_rarity('jade statue'/'LEB', 'Uncommon').
card_artist('jade statue'/'LEB', 'Dan Frazier').
card_flavor_text('jade statue'/'LEB', '\"Some of the other guys dared me to touch it, but I knew it weren\'t no ordinary hunk o\' rock.\" —Norin the Wary').
card_multiverse_id('jade statue'/'LEB', '319').

card_in_set('jayemdae tome', 'LEB').
card_original_type('jayemdae tome'/'LEB', 'Mono Artifact').
card_original_text('jayemdae tome'/'LEB', '{4}: You may draw one extra card.').
card_image_name('jayemdae tome'/'LEB', 'jayemdae tome').
card_uid('jayemdae tome'/'LEB', 'LEB:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'LEB', 'Rare').
card_artist('jayemdae tome'/'LEB', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'LEB', '320').

card_in_set('juggernaut', 'LEB').
card_original_type('juggernaut'/'LEB', 'Artifact Creature').
card_original_text('juggernaut'/'LEB', 'Must attack each turn if possible. Can\'t be blocked by walls.').
card_image_name('juggernaut'/'LEB', 'juggernaut').
card_uid('juggernaut'/'LEB', 'LEB:Juggernaut:juggernaut').
card_rarity('juggernaut'/'LEB', 'Uncommon').
card_artist('juggernaut'/'LEB', 'Dan Frazier').
card_flavor_text('juggernaut'/'LEB', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').
card_multiverse_id('juggernaut'/'LEB', '321').

card_in_set('jump', 'LEB').
card_original_type('jump'/'LEB', 'Instant').
card_original_text('jump'/'LEB', 'Target creature is a flying creature until end of turn.').
card_image_name('jump'/'LEB', 'jump').
card_uid('jump'/'LEB', 'LEB:Jump:jump').
card_rarity('jump'/'LEB', 'Common').
card_artist('jump'/'LEB', 'Mark Poole').
card_multiverse_id('jump'/'LEB', '403').

card_in_set('karma', 'LEB').
card_original_type('karma'/'LEB', 'Enchantment').
card_original_text('karma'/'LEB', 'For each swamp in play, Karma does 1 damage to the swamp owner during the swamp owner\'s upkeep.').
card_image_name('karma'/'LEB', 'karma').
card_uid('karma'/'LEB', 'LEB:Karma:karma').
card_rarity('karma'/'LEB', 'Uncommon').
card_artist('karma'/'LEB', 'Richard Thomas').
card_multiverse_id('karma'/'LEB', '553').

card_in_set('keldon warlord', 'LEB').
card_original_type('keldon warlord'/'LEB', 'Summon — Lord').
card_original_text('keldon warlord'/'LEB', 'The Xs below are the number of non-wall creatures on your side, including Warlord. Thus if you have 2 other non-wall creatures, Warlord is 3/3. If one of those creatures is killed during the turn, Warlord immediately becomes 2/2.').
card_image_name('keldon warlord'/'LEB', 'keldon warlord').
card_uid('keldon warlord'/'LEB', 'LEB:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'LEB', 'Uncommon').
card_artist('keldon warlord'/'LEB', 'Kev Brockschmidt').
card_multiverse_id('keldon warlord'/'LEB', '503').

card_in_set('kormus bell', 'LEB').
card_original_type('kormus bell'/'LEB', 'Continuous Artifact').
card_original_text('kormus bell'/'LEB', 'Treat all swamps in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. Swamps have no color; they are not considered black cards.').
card_image_name('kormus bell'/'LEB', 'kormus bell').
card_uid('kormus bell'/'LEB', 'LEB:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'LEB', 'Rare').
card_artist('kormus bell'/'LEB', 'Christopher Rush').
card_multiverse_id('kormus bell'/'LEB', '322').

card_in_set('kudzu', 'LEB').
card_original_type('kudzu'/'LEB', 'Enchant Land').
card_original_text('kudzu'/'LEB', 'When target land becomes tapped, it is destroyed. Unless that was the last land in play, Kudzu is not discarded; instead, the player whose land it just destroyed may place it on another land of his or her choice.').
card_image_name('kudzu'/'LEB', 'kudzu').
card_uid('kudzu'/'LEB', 'LEB:Kudzu:kudzu').
card_rarity('kudzu'/'LEB', 'Rare').
card_artist('kudzu'/'LEB', 'Mark Poole').
card_multiverse_id('kudzu'/'LEB', '455').

card_in_set('lance', 'LEB').
card_original_type('lance'/'LEB', 'Enchant Creature').
card_original_text('lance'/'LEB', 'Target creature gains first strike.').
card_image_name('lance'/'LEB', 'lance').
card_uid('lance'/'LEB', 'LEB:Lance:lance').
card_rarity('lance'/'LEB', 'Uncommon').
card_artist('lance'/'LEB', 'Rob Alexander').
card_multiverse_id('lance'/'LEB', '554').

card_in_set('ley druid', 'LEB').
card_original_type('ley druid'/'LEB', 'Summon — Cleric').
card_original_text('ley druid'/'LEB', 'Tap Druid to untap a land of your choice. This action can be played as an interrupt.').
card_image_name('ley druid'/'LEB', 'ley druid').
card_uid('ley druid'/'LEB', 'LEB:Ley Druid:ley druid').
card_rarity('ley druid'/'LEB', 'Uncommon').
card_artist('ley druid'/'LEB', 'Sandra Everingham').
card_flavor_text('ley druid'/'LEB', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'LEB', '456').

card_in_set('library of leng', 'LEB').
card_original_type('library of leng'/'LEB', 'Continuous Artifact').
card_original_text('library of leng'/'LEB', 'There is no limit to size of your hand. If a card forces you to discard, you may choose to discard to top of your library rather than to graveyard. If discard is random, you may look at card before deciding where to discard it.').
card_image_name('library of leng'/'LEB', 'library of leng').
card_uid('library of leng'/'LEB', 'LEB:Library of Leng:library of leng').
card_rarity('library of leng'/'LEB', 'Uncommon').
card_artist('library of leng'/'LEB', 'Daniel Gelon').
card_multiverse_id('library of leng'/'LEB', '323').

card_in_set('lich', 'LEB').
card_original_type('lich'/'LEB', 'Enchantment').
card_original_text('lich'/'LEB', 'You lose all life. If you gain life later in the game, instead draw one card from your library for each life. For each point of damage you suffer, you must destroy one of your cards in play. Creatures destroyed in this way cannot be regenerated. You lose if this enchantment is destroyed or if you suffer a point of damage without sending a card to the graveyard.').
card_image_name('lich'/'LEB', 'lich').
card_uid('lich'/'LEB', 'LEB:Lich:lich').
card_rarity('lich'/'LEB', 'Rare').
card_artist('lich'/'LEB', 'Daniel Gelon').
card_multiverse_id('lich'/'LEB', '364').

card_in_set('lifeforce', 'LEB').
card_original_type('lifeforce'/'LEB', 'Enchantment').
card_original_text('lifeforce'/'LEB', '{G}{G}: Destroy a black spell as it is being cast. This use may be played as an interrupt, and does not affect black cards already in play.').
card_image_name('lifeforce'/'LEB', 'lifeforce').
card_uid('lifeforce'/'LEB', 'LEB:Lifeforce:lifeforce').
card_rarity('lifeforce'/'LEB', 'Uncommon').
card_artist('lifeforce'/'LEB', 'Dameon Willich').
card_multiverse_id('lifeforce'/'LEB', '457').

card_in_set('lifelace', 'LEB').
card_original_type('lifelace'/'LEB', 'Interrupt').
card_original_text('lifelace'/'LEB', 'Changes the color of one card either being played or already in play to green. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('lifelace'/'LEB', 'lifelace').
card_uid('lifelace'/'LEB', 'LEB:Lifelace:lifelace').
card_rarity('lifelace'/'LEB', 'Rare').
card_artist('lifelace'/'LEB', 'Amy Weber').
card_multiverse_id('lifelace'/'LEB', '458').

card_in_set('lifetap', 'LEB').
card_original_type('lifetap'/'LEB', 'Enchantment').
card_original_text('lifetap'/'LEB', 'You gain 1 life each time any forest of opponent\'s becomes tapped.').
card_image_name('lifetap'/'LEB', 'lifetap').
card_uid('lifetap'/'LEB', 'LEB:Lifetap:lifetap').
card_rarity('lifetap'/'LEB', 'Uncommon').
card_artist('lifetap'/'LEB', 'Anson Maddocks').
card_multiverse_id('lifetap'/'LEB', '404').

card_in_set('lightning bolt', 'LEB').
card_original_type('lightning bolt'/'LEB', 'Instant').
card_original_text('lightning bolt'/'LEB', 'Lightning Bolt does 3 damage to one target.').
card_image_name('lightning bolt'/'LEB', 'lightning bolt').
card_uid('lightning bolt'/'LEB', 'LEB:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'LEB', 'Common').
card_artist('lightning bolt'/'LEB', 'Christopher Rush').
card_multiverse_id('lightning bolt'/'LEB', '504').

card_in_set('living artifact', 'LEB').
card_original_type('living artifact'/'LEB', 'Enchant Artifact').
card_original_text('living artifact'/'LEB', 'Put a counter on target artifact for each life you lose. During upkeep you may trade one counter for one life, but you can only trade in one counter each turn.').
card_image_name('living artifact'/'LEB', 'living artifact').
card_uid('living artifact'/'LEB', 'LEB:Living Artifact:living artifact').
card_rarity('living artifact'/'LEB', 'Rare').
card_artist('living artifact'/'LEB', 'Anson Maddocks').
card_multiverse_id('living artifact'/'LEB', '459').

card_in_set('living lands', 'LEB').
card_original_type('living lands'/'LEB', 'Enchantment').
card_original_text('living lands'/'LEB', 'Treat all forests in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. The living lands have no color; they are not considered green cards.').
card_image_name('living lands'/'LEB', 'living lands').
card_uid('living lands'/'LEB', 'LEB:Living Lands:living lands').
card_rarity('living lands'/'LEB', 'Rare').
card_artist('living lands'/'LEB', 'Jesper Myrfors').
card_multiverse_id('living lands'/'LEB', '460').

card_in_set('living wall', 'LEB').
card_original_type('living wall'/'LEB', 'Artifact Creature').
card_original_text('living wall'/'LEB', 'Counts as a wall. {1}: Regenerates.').
card_image_name('living wall'/'LEB', 'living wall').
card_uid('living wall'/'LEB', 'LEB:Living Wall:living wall').
card_rarity('living wall'/'LEB', 'Uncommon').
card_artist('living wall'/'LEB', 'Anson Maddocks').
card_flavor_text('living wall'/'LEB', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').
card_multiverse_id('living wall'/'LEB', '324').

card_in_set('llanowar elves', 'LEB').
card_original_type('llanowar elves'/'LEB', 'Summon — Elves').
card_original_text('llanowar elves'/'LEB', 'Tap to add 1 green mana to your mana pool. This tap can be played as an interrupt.').
card_image_name('llanowar elves'/'LEB', 'llanowar elves').
card_uid('llanowar elves'/'LEB', 'LEB:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'LEB', 'Common').
card_artist('llanowar elves'/'LEB', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'LEB', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').
card_multiverse_id('llanowar elves'/'LEB', '461').

card_in_set('lord of atlantis', 'LEB').
card_original_type('lord of atlantis'/'LEB', 'Summon — Lord of Atlantis').
card_original_text('lord of atlantis'/'LEB', 'All Merfolk in play gain islandwalk and +1/+1 while this card is in play.').
card_image_name('lord of atlantis'/'LEB', 'lord of atlantis').
card_uid('lord of atlantis'/'LEB', 'LEB:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'LEB', 'Rare').
card_artist('lord of atlantis'/'LEB', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'LEB', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'LEB', '405').

card_in_set('lord of the pit', 'LEB').
card_original_type('lord of the pit'/'LEB', 'Summon — Demon').
card_original_text('lord of the pit'/'LEB', 'Flying, trample\nYou must sacrifice one of your own creatures during upkeep or Lord of the Pit does 7 damage to you. You may still attack with Lord of the Pit even if you failed to sacrifice a creature.').
card_image_name('lord of the pit'/'LEB', 'lord of the pit').
card_uid('lord of the pit'/'LEB', 'LEB:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'LEB', 'Rare').
card_artist('lord of the pit'/'LEB', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'LEB', '365').

card_in_set('lure', 'LEB').
card_original_type('lure'/'LEB', 'Enchant Creature').
card_original_text('lure'/'LEB', 'All creatures able to block target creature must do so. If a creature has the ability to block more than one creature, Lure does not prevent this. If there is more than one attacking creature with Lure, defender may choose which of them each defending creature blocks.').
card_image_name('lure'/'LEB', 'lure').
card_uid('lure'/'LEB', 'LEB:Lure:lure').
card_rarity('lure'/'LEB', 'Uncommon').
card_artist('lure'/'LEB', 'Anson Maddocks').
card_multiverse_id('lure'/'LEB', '462').

card_in_set('magical hack', 'LEB').
card_original_type('magical hack'/'LEB', 'Interrupt').
card_original_text('magical hack'/'LEB', 'Change the text of any card being played or already in play by replacing one basic land type with another. For example, you can change \"swampwalk\" to \"plainswalk.\"').
card_image_name('magical hack'/'LEB', 'magical hack').
card_uid('magical hack'/'LEB', 'LEB:Magical Hack:magical hack').
card_rarity('magical hack'/'LEB', 'Rare').
card_artist('magical hack'/'LEB', 'Julie Baroh').
card_multiverse_id('magical hack'/'LEB', '406').

card_in_set('mahamoti djinn', 'LEB').
card_original_type('mahamoti djinn'/'LEB', 'Summon — Djinn').
card_original_text('mahamoti djinn'/'LEB', 'Flying').
card_image_name('mahamoti djinn'/'LEB', 'mahamoti djinn').
card_uid('mahamoti djinn'/'LEB', 'LEB:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'LEB', 'Rare').
card_artist('mahamoti djinn'/'LEB', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'LEB', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'LEB', '407').

card_in_set('mana flare', 'LEB').
card_original_type('mana flare'/'LEB', 'Enchantment').
card_original_text('mana flare'/'LEB', 'Whenever either player taps land for mana, each land produces 1 extra mana of the appropriate type.').
card_image_name('mana flare'/'LEB', 'mana flare').
card_uid('mana flare'/'LEB', 'LEB:Mana Flare:mana flare').
card_rarity('mana flare'/'LEB', 'Rare').
card_artist('mana flare'/'LEB', 'Christopher Rush').
card_multiverse_id('mana flare'/'LEB', '505').

card_in_set('mana short', 'LEB').
card_original_type('mana short'/'LEB', 'Instant').
card_original_text('mana short'/'LEB', 'All opponent\'s lands are tapped, and opponent\'s mana pool is emptied. Opponent takes no damage from unspent mana.').
card_image_name('mana short'/'LEB', 'mana short').
card_uid('mana short'/'LEB', 'LEB:Mana Short:mana short').
card_rarity('mana short'/'LEB', 'Rare').
card_artist('mana short'/'LEB', 'Dameon Willich').
card_multiverse_id('mana short'/'LEB', '408').

card_in_set('mana vault', 'LEB').
card_original_type('mana vault'/'LEB', 'Mono Artifact').
card_original_text('mana vault'/'LEB', 'Tap to add 3 colorless mana to your mana pool. Mana Vault doesn\'t untap normally during untap phase; to untap it, you must pay 4 mana. If Mana Vault remains tapped during upkeep it does 1 damage to you. Tapping this artifact can be played as an interrupt.').
card_image_name('mana vault'/'LEB', 'mana vault').
card_uid('mana vault'/'LEB', 'LEB:Mana Vault:mana vault').
card_rarity('mana vault'/'LEB', 'Rare').
card_artist('mana vault'/'LEB', 'Mark Tedin').
card_multiverse_id('mana vault'/'LEB', '325').

card_in_set('manabarbs', 'LEB').
card_original_type('manabarbs'/'LEB', 'Enchantment').
card_original_text('manabarbs'/'LEB', 'Whenever any land is tapped, Manabarbs does 1 damage to the land\'s controller.').
card_image_name('manabarbs'/'LEB', 'manabarbs').
card_uid('manabarbs'/'LEB', 'LEB:Manabarbs:manabarbs').
card_rarity('manabarbs'/'LEB', 'Rare').
card_artist('manabarbs'/'LEB', 'Christopher Rush').
card_multiverse_id('manabarbs'/'LEB', '506').

card_in_set('meekstone', 'LEB').
card_original_type('meekstone'/'LEB', 'Continuous Artifact').
card_original_text('meekstone'/'LEB', 'Any creature with power greater than 2 may not be untapped as normal during the untap phase.').
card_image_name('meekstone'/'LEB', 'meekstone').
card_uid('meekstone'/'LEB', 'LEB:Meekstone:meekstone').
card_rarity('meekstone'/'LEB', 'Rare').
card_artist('meekstone'/'LEB', 'Quinton Hoover').
card_multiverse_id('meekstone'/'LEB', '326').

card_in_set('merfolk of the pearl trident', 'LEB').
card_original_type('merfolk of the pearl trident'/'LEB', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'LEB', '').
card_image_name('merfolk of the pearl trident'/'LEB', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'LEB', 'LEB:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'LEB', 'Common').
card_artist('merfolk of the pearl trident'/'LEB', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'LEB', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'LEB', '409').

card_in_set('mesa pegasus', 'LEB').
card_original_type('mesa pegasus'/'LEB', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'LEB', 'Flying, bands').
card_image_name('mesa pegasus'/'LEB', 'mesa pegasus').
card_uid('mesa pegasus'/'LEB', 'LEB:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'LEB', 'Common').
card_artist('mesa pegasus'/'LEB', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'LEB', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'LEB', '555').

card_in_set('mind twist', 'LEB').
card_original_type('mind twist'/'LEB', 'Sorcery').
card_original_text('mind twist'/'LEB', 'Opponent must discard X cards at random from hand. If opponent doesn\'t have enough cards in hand, entire hand is discarded.').
card_image_name('mind twist'/'LEB', 'mind twist').
card_uid('mind twist'/'LEB', 'LEB:Mind Twist:mind twist').
card_rarity('mind twist'/'LEB', 'Rare').
card_artist('mind twist'/'LEB', 'Julie Baroh').
card_multiverse_id('mind twist'/'LEB', '366').

card_in_set('mons\'s goblin raiders', 'LEB').
card_original_type('mons\'s goblin raiders'/'LEB', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'LEB', '').
card_image_name('mons\'s goblin raiders'/'LEB', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'LEB', 'LEB:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'LEB', 'Common').
card_artist('mons\'s goblin raiders'/'LEB', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'LEB', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'LEB', '507').

card_in_set('mountain', 'LEB').
card_original_type('mountain'/'LEB', 'Land').
card_original_text('mountain'/'LEB', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'LEB', 'mountain1').
card_uid('mountain'/'LEB', 'LEB:Mountain:mountain1').
card_rarity('mountain'/'LEB', 'Basic Land').
card_artist('mountain'/'LEB', 'Douglas Shuler').
card_multiverse_id('mountain'/'LEB', '591').

card_in_set('mountain', 'LEB').
card_original_type('mountain'/'LEB', 'Land').
card_original_text('mountain'/'LEB', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'LEB', 'mountain2').
card_uid('mountain'/'LEB', 'LEB:Mountain:mountain2').
card_rarity('mountain'/'LEB', 'Basic Land').
card_artist('mountain'/'LEB', 'Douglas Shuler').
card_multiverse_id('mountain'/'LEB', '589').

card_in_set('mountain', 'LEB').
card_original_type('mountain'/'LEB', 'Land').
card_original_text('mountain'/'LEB', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'LEB', 'mountain3').
card_uid('mountain'/'LEB', 'LEB:Mountain:mountain3').
card_rarity('mountain'/'LEB', 'Basic Land').
card_artist('mountain'/'LEB', 'Douglas Shuler').
card_multiverse_id('mountain'/'LEB', '590').

card_in_set('mox emerald', 'LEB').
card_original_type('mox emerald'/'LEB', 'Mono Artifact').
card_original_text('mox emerald'/'LEB', 'Add 1 green mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox emerald'/'LEB', 'mox emerald').
card_uid('mox emerald'/'LEB', 'LEB:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'LEB', 'Rare').
card_artist('mox emerald'/'LEB', 'Dan Frazier').
card_multiverse_id('mox emerald'/'LEB', '327').

card_in_set('mox jet', 'LEB').
card_original_type('mox jet'/'LEB', 'Mono Artifact').
card_original_text('mox jet'/'LEB', 'Add 1 black mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox jet'/'LEB', 'mox jet').
card_uid('mox jet'/'LEB', 'LEB:Mox Jet:mox jet').
card_rarity('mox jet'/'LEB', 'Rare').
card_artist('mox jet'/'LEB', 'Dan Frazier').
card_multiverse_id('mox jet'/'LEB', '328').

card_in_set('mox pearl', 'LEB').
card_original_type('mox pearl'/'LEB', 'Mono Artifact').
card_original_text('mox pearl'/'LEB', 'Add 1 white mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox pearl'/'LEB', 'mox pearl').
card_uid('mox pearl'/'LEB', 'LEB:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'LEB', 'Rare').
card_artist('mox pearl'/'LEB', 'Dan Frazier').
card_multiverse_id('mox pearl'/'LEB', '329').

card_in_set('mox ruby', 'LEB').
card_original_type('mox ruby'/'LEB', 'Mono Artifact').
card_original_text('mox ruby'/'LEB', 'Add 1 red mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox ruby'/'LEB', 'mox ruby').
card_uid('mox ruby'/'LEB', 'LEB:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'LEB', 'Rare').
card_artist('mox ruby'/'LEB', 'Dan Frazier').
card_multiverse_id('mox ruby'/'LEB', '330').

card_in_set('mox sapphire', 'LEB').
card_original_type('mox sapphire'/'LEB', 'Mono Artifact').
card_original_text('mox sapphire'/'LEB', 'Add 1 blue mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox sapphire'/'LEB', 'mox sapphire').
card_uid('mox sapphire'/'LEB', 'LEB:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'LEB', 'Rare').
card_artist('mox sapphire'/'LEB', 'Dan Frazier').
card_multiverse_id('mox sapphire'/'LEB', '331').

card_in_set('natural selection', 'LEB').
card_original_type('natural selection'/'LEB', 'Instant').
card_original_text('natural selection'/'LEB', 'Look at top three cards of any player\'s library. You may opt to rearrange those three cards or shuffle the entire library.').
card_image_name('natural selection'/'LEB', 'natural selection').
card_uid('natural selection'/'LEB', 'LEB:Natural Selection:natural selection').
card_rarity('natural selection'/'LEB', 'Rare').
card_artist('natural selection'/'LEB', 'Mark Poole').
card_multiverse_id('natural selection'/'LEB', '463').

card_in_set('nether shadow', 'LEB').
card_original_type('nether shadow'/'LEB', 'Summon — Shadow').
card_original_text('nether shadow'/'LEB', 'If Shadow is in graveyard with any combination of cards above it that includes at least three creatures, it can be returned to play during upkeep for its normal casting cost. Shadow can attack on same turn summoned or returned to play.').
card_image_name('nether shadow'/'LEB', 'nether shadow').
card_uid('nether shadow'/'LEB', 'LEB:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'LEB', 'Rare').
card_artist('nether shadow'/'LEB', 'Christopher Rush').
card_multiverse_id('nether shadow'/'LEB', '367').

card_in_set('nettling imp', 'LEB').
card_original_type('nettling imp'/'LEB', 'Summon — Imp').
card_original_text('nettling imp'/'LEB', 'Tap to force a particular one of opponent\'s non-wall creatures to attack. If target creature cannot attack, it is destroyed at end of turn. This tap should be played during opponent\'s turn, before the attack. May not be used on creatures summoned this turn.').
card_image_name('nettling imp'/'LEB', 'nettling imp').
card_uid('nettling imp'/'LEB', 'LEB:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'LEB', 'Uncommon').
card_artist('nettling imp'/'LEB', 'Quinton Hoover').
card_multiverse_id('nettling imp'/'LEB', '368').

card_in_set('nevinyrral\'s disk', 'LEB').
card_original_type('nevinyrral\'s disk'/'LEB', 'Mono Artifact').
card_original_text('nevinyrral\'s disk'/'LEB', '{1}: Destroys all creatures, enchantments, and artifacts in play. Disk begins tapped but can be untapped as usual. Disk destroys itself when used.').
card_image_name('nevinyrral\'s disk'/'LEB', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'LEB', 'LEB:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'LEB', 'Rare').
card_artist('nevinyrral\'s disk'/'LEB', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'LEB', '332').

card_in_set('nightmare', 'LEB').
card_original_type('nightmare'/'LEB', 'Summon — Nightmare').
card_original_text('nightmare'/'LEB', 'Flying\nNightmare\'s power and toughness both equal the number of swamps its controller has in play.').
card_image_name('nightmare'/'LEB', 'nightmare').
card_uid('nightmare'/'LEB', 'LEB:Nightmare:nightmare').
card_rarity('nightmare'/'LEB', 'Rare').
card_artist('nightmare'/'LEB', 'Melissa A. Benson').
card_flavor_text('nightmare'/'LEB', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'LEB', '369').

card_in_set('northern paladin', 'LEB').
card_original_type('northern paladin'/'LEB', 'Summon — Paladin').
card_original_text('northern paladin'/'LEB', '{W}{W} and tap: Destroys a black card in play. Cannot be used to cancel a black spell as it is being cast.').
card_image_name('northern paladin'/'LEB', 'northern paladin').
card_uid('northern paladin'/'LEB', 'LEB:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'LEB', 'Rare').
card_artist('northern paladin'/'LEB', 'Douglas Shuler').
card_flavor_text('northern paladin'/'LEB', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').
card_multiverse_id('northern paladin'/'LEB', '556').

card_in_set('obsianus golem', 'LEB').
card_original_type('obsianus golem'/'LEB', 'Artifact Creature').
card_original_text('obsianus golem'/'LEB', '').
card_image_name('obsianus golem'/'LEB', 'obsianus golem').
card_uid('obsianus golem'/'LEB', 'LEB:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'LEB', 'Uncommon').
card_artist('obsianus golem'/'LEB', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'LEB', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone...\"\n—Song of the Artificer').
card_multiverse_id('obsianus golem'/'LEB', '333').

card_in_set('orcish artillery', 'LEB').
card_original_type('orcish artillery'/'LEB', 'Summon — Orcs').
card_original_text('orcish artillery'/'LEB', 'Tap to do 2 damage to any target, but you suffer 3 damage as well.').
card_image_name('orcish artillery'/'LEB', 'orcish artillery').
card_uid('orcish artillery'/'LEB', 'LEB:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'LEB', 'Uncommon').
card_artist('orcish artillery'/'LEB', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'LEB', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'LEB', '508').

card_in_set('orcish oriflamme', 'LEB').
card_original_type('orcish oriflamme'/'LEB', 'Enchantment').
card_original_text('orcish oriflamme'/'LEB', 'When attacking, all of your attacking creatures gain +1/+0.').
card_image_name('orcish oriflamme'/'LEB', 'orcish oriflamme').
card_uid('orcish oriflamme'/'LEB', 'LEB:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'LEB', 'Uncommon').
card_artist('orcish oriflamme'/'LEB', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'LEB', '509').

card_in_set('paralyze', 'LEB').
card_original_type('paralyze'/'LEB', 'Enchant Creature').
card_original_text('paralyze'/'LEB', 'Target creature is not untapped as normal during untap phase unless 4 mana are spent. Tap target creature when Paralyze is cast.').
card_image_name('paralyze'/'LEB', 'paralyze').
card_uid('paralyze'/'LEB', 'LEB:Paralyze:paralyze').
card_rarity('paralyze'/'LEB', 'Common').
card_artist('paralyze'/'LEB', 'Anson Maddocks').
card_multiverse_id('paralyze'/'LEB', '370').

card_in_set('pearled unicorn', 'LEB').
card_original_type('pearled unicorn'/'LEB', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'LEB', '').
card_image_name('pearled unicorn'/'LEB', 'pearled unicorn').
card_uid('pearled unicorn'/'LEB', 'LEB:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'LEB', 'Common').
card_artist('pearled unicorn'/'LEB', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'LEB', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').
card_multiverse_id('pearled unicorn'/'LEB', '557').

card_in_set('personal incarnation', 'LEB').
card_original_type('personal incarnation'/'LEB', 'Summon — Avatar').
card_original_text('personal incarnation'/'LEB', 'Caster can redirect any or all damage done to Personal Incarnation to self instead. The source of the damage is unchanged. If Personal Incarnation is destroyed, caster loses half his or her remaining life points, rounding up the loss.').
card_image_name('personal incarnation'/'LEB', 'personal incarnation').
card_uid('personal incarnation'/'LEB', 'LEB:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'LEB', 'Rare').
card_artist('personal incarnation'/'LEB', 'Kev Brockschmidt').
card_multiverse_id('personal incarnation'/'LEB', '558').

card_in_set('pestilence', 'LEB').
card_original_type('pestilence'/'LEB', 'Enchantment').
card_original_text('pestilence'/'LEB', '{B}: Do 1 damage to each creature and to both players. Pestilence must be discarded at end of any turn in which there are no creatures in play at end of turn.').
card_image_name('pestilence'/'LEB', 'pestilence').
card_uid('pestilence'/'LEB', 'LEB:Pestilence:pestilence').
card_rarity('pestilence'/'LEB', 'Common').
card_artist('pestilence'/'LEB', 'Jesper Myrfors').
card_multiverse_id('pestilence'/'LEB', '371').

card_in_set('phantasmal forces', 'LEB').
card_original_type('phantasmal forces'/'LEB', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'LEB', 'Flying\nController must spend {U} during upkeep to maintain or Phantasmal Forces are destroyed.').
card_image_name('phantasmal forces'/'LEB', 'phantasmal forces').
card_uid('phantasmal forces'/'LEB', 'LEB:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'LEB', 'Uncommon').
card_artist('phantasmal forces'/'LEB', 'Mark Poole').
card_flavor_text('phantasmal forces'/'LEB', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'LEB', '410').

card_in_set('phantasmal terrain', 'LEB').
card_original_type('phantasmal terrain'/'LEB', 'Enchant Land').
card_original_text('phantasmal terrain'/'LEB', 'Target land changes to any basic land type of caster\'s choice. Land type is set when cast and may not be further altered by this enchantment.').
card_image_name('phantasmal terrain'/'LEB', 'phantasmal terrain').
card_uid('phantasmal terrain'/'LEB', 'LEB:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'LEB', 'Common').
card_artist('phantasmal terrain'/'LEB', 'Dameon Willich').
card_multiverse_id('phantasmal terrain'/'LEB', '411').

card_in_set('phantom monster', 'LEB').
card_original_type('phantom monster'/'LEB', 'Summon — Phantasm').
card_original_text('phantom monster'/'LEB', 'Flying').
card_image_name('phantom monster'/'LEB', 'phantom monster').
card_uid('phantom monster'/'LEB', 'LEB:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'LEB', 'Uncommon').
card_artist('phantom monster'/'LEB', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'LEB', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'LEB', '412').

card_in_set('pirate ship', 'LEB').
card_original_type('pirate ship'/'LEB', 'Summon — Ship').
card_original_text('pirate ship'/'LEB', 'Tap to do 1 damage to any target. Cannot attack unless opponent has islands in play, though controller may still tap. Pirate Ship is destroyed immediately if at any time controller has no islands in play.').
card_image_name('pirate ship'/'LEB', 'pirate ship').
card_uid('pirate ship'/'LEB', 'LEB:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'LEB', 'Rare').
card_artist('pirate ship'/'LEB', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'LEB', '413').

card_in_set('plague rats', 'LEB').
card_original_type('plague rats'/'LEB', 'Summon — Rats').
card_original_text('plague rats'/'LEB', 'The Xs below are the number of Plague Rats in play, counting both sides. Thus if there are 2 Plague Rats in play, each has power and toughness 2/2.').
card_image_name('plague rats'/'LEB', 'plague rats').
card_uid('plague rats'/'LEB', 'LEB:Plague Rats:plague rats').
card_rarity('plague rats'/'LEB', 'Common').
card_artist('plague rats'/'LEB', 'Anson Maddocks').
card_flavor_text('plague rats'/'LEB', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you...\"\n—Samuel Coleridge, \"Recantation\"').
card_multiverse_id('plague rats'/'LEB', '372').

card_in_set('plains', 'LEB').
card_original_type('plains'/'LEB', 'Land').
card_original_text('plains'/'LEB', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'LEB', 'plains1').
card_uid('plains'/'LEB', 'LEB:Plains:plains1').
card_rarity('plains'/'LEB', 'Basic Land').
card_artist('plains'/'LEB', 'Jesper Myrfors').
card_multiverse_id('plains'/'LEB', '595').

card_in_set('plains', 'LEB').
card_original_type('plains'/'LEB', 'Land').
card_original_text('plains'/'LEB', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'LEB', 'plains2').
card_uid('plains'/'LEB', 'LEB:Plains:plains2').
card_rarity('plains'/'LEB', 'Basic Land').
card_artist('plains'/'LEB', 'Jesper Myrfors').
card_multiverse_id('plains'/'LEB', '596').

card_in_set('plains', 'LEB').
card_original_type('plains'/'LEB', 'Land').
card_original_text('plains'/'LEB', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'LEB', 'plains3').
card_uid('plains'/'LEB', 'LEB:Plains:plains3').
card_rarity('plains'/'LEB', 'Basic Land').
card_artist('plains'/'LEB', 'Jesper Myrfors').
card_multiverse_id('plains'/'LEB', '597').

card_in_set('plateau', 'LEB').
card_original_type('plateau'/'LEB', 'Land').
card_original_text('plateau'/'LEB', 'Counts as both mountains and plains and is affected by spells that affect either. Tap to add either {R} or {W} to your mana pool.').
card_image_name('plateau'/'LEB', 'plateau').
card_uid('plateau'/'LEB', 'LEB:Plateau:plateau').
card_rarity('plateau'/'LEB', 'Rare').
card_artist('plateau'/'LEB', 'Drew Tucker').
card_multiverse_id('plateau'/'LEB', '578').

card_in_set('power leak', 'LEB').
card_original_type('power leak'/'LEB', 'Enchant Enchantment').
card_original_text('power leak'/'LEB', 'Target enchantment costs 2 extra mana during upkeep. If target enchantment\'s controller cannot or will not pay this extra mana, Power Leak does 1 damage to him or her for each unpaid mana.').
card_image_name('power leak'/'LEB', 'power leak').
card_uid('power leak'/'LEB', 'LEB:Power Leak:power leak').
card_rarity('power leak'/'LEB', 'Common').
card_artist('power leak'/'LEB', 'Drew Tucker').
card_multiverse_id('power leak'/'LEB', '414').

card_in_set('power sink', 'LEB').
card_original_type('power sink'/'LEB', 'Interrupt').
card_original_text('power sink'/'LEB', 'Target spell is countered unless its caster spends X more mana; caster of target spell can\'t choose to let it be countered. If caster of target spell doesn\'t have enough mana, all available mana from lands and mana pool must be paid but target spell will still be countered.').
card_image_name('power sink'/'LEB', 'power sink').
card_uid('power sink'/'LEB', 'LEB:Power Sink:power sink').
card_rarity('power sink'/'LEB', 'Common').
card_artist('power sink'/'LEB', 'Richard Thomas').
card_multiverse_id('power sink'/'LEB', '415').

card_in_set('power surge', 'LEB').
card_original_type('power surge'/'LEB', 'Enchantment').
card_original_text('power surge'/'LEB', 'Before untapping lands at the start of a turn, each player takes 1 damage for each land he or she controls but did not tap during the previous turn.').
card_image_name('power surge'/'LEB', 'power surge').
card_uid('power surge'/'LEB', 'LEB:Power Surge:power surge').
card_rarity('power surge'/'LEB', 'Rare').
card_artist('power surge'/'LEB', 'Douglas Shuler').
card_multiverse_id('power surge'/'LEB', '510').

card_in_set('prodigal sorcerer', 'LEB').
card_original_type('prodigal sorcerer'/'LEB', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'LEB', 'Tap to do 1 damage to any target.').
card_image_name('prodigal sorcerer'/'LEB', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'LEB', 'LEB:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'LEB', 'Common').
card_artist('prodigal sorcerer'/'LEB', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'LEB', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'LEB', '416').

card_in_set('psionic blast', 'LEB').
card_original_type('psionic blast'/'LEB', 'Instant').
card_original_text('psionic blast'/'LEB', 'Psionic Blast does 4 damage to any target, but it does 2 damage to you as well.').
card_image_name('psionic blast'/'LEB', 'psionic blast').
card_uid('psionic blast'/'LEB', 'LEB:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'LEB', 'Uncommon').
card_artist('psionic blast'/'LEB', 'Douglas Shuler').
card_multiverse_id('psionic blast'/'LEB', '417').

card_in_set('psychic venom', 'LEB').
card_original_type('psychic venom'/'LEB', 'Enchant Land').
card_original_text('psychic venom'/'LEB', 'Whenever target land is tapped, Psychic Venom does 2 damage to target land\'s controller.').
card_image_name('psychic venom'/'LEB', 'psychic venom').
card_uid('psychic venom'/'LEB', 'LEB:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'LEB', 'Common').
card_artist('psychic venom'/'LEB', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'LEB', '418').

card_in_set('purelace', 'LEB').
card_original_type('purelace'/'LEB', 'Interrupt').
card_original_text('purelace'/'LEB', 'Changes the color of one card either being played or already in play to white. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('purelace'/'LEB', 'purelace').
card_uid('purelace'/'LEB', 'LEB:Purelace:purelace').
card_rarity('purelace'/'LEB', 'Rare').
card_artist('purelace'/'LEB', 'Sandra Everingham').
card_multiverse_id('purelace'/'LEB', '559').

card_in_set('raging river', 'LEB').
card_original_type('raging river'/'LEB', 'Enchantment').
card_original_text('raging river'/'LEB', 'When you attack, non-flying defending creatures must be divided as opponent wishes between the left and right sides of the River. You then choose on which side of the River to place each attacking creature, and attacking creatures can only be blocked by flying creatures or those on the same side of the River.').
card_image_name('raging river'/'LEB', 'raging river').
card_uid('raging river'/'LEB', 'LEB:Raging River:raging river').
card_rarity('raging river'/'LEB', 'Rare').
card_artist('raging river'/'LEB', 'Sandra Everingham').
card_multiverse_id('raging river'/'LEB', '511').

card_in_set('raise dead', 'LEB').
card_original_type('raise dead'/'LEB', 'Sorcery').
card_original_text('raise dead'/'LEB', 'Return creature from your graveyard to your hand.').
card_image_name('raise dead'/'LEB', 'raise dead').
card_uid('raise dead'/'LEB', 'LEB:Raise Dead:raise dead').
card_rarity('raise dead'/'LEB', 'Common').
card_artist('raise dead'/'LEB', 'Jeff A. Menges').
card_multiverse_id('raise dead'/'LEB', '373').

card_in_set('red elemental blast', 'LEB').
card_original_type('red elemental blast'/'LEB', 'Interrupt').
card_original_text('red elemental blast'/'LEB', 'Counters a blue spell being cast or destroys a blue card in play.').
card_image_name('red elemental blast'/'LEB', 'red elemental blast').
card_uid('red elemental blast'/'LEB', 'LEB:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'LEB', 'Common').
card_artist('red elemental blast'/'LEB', 'Richard Thomas').
card_multiverse_id('red elemental blast'/'LEB', '512').

card_in_set('red ward', 'LEB').
card_original_type('red ward'/'LEB', 'Enchant Creature').
card_original_text('red ward'/'LEB', 'Target creature gains protection from red.').
card_image_name('red ward'/'LEB', 'red ward').
card_uid('red ward'/'LEB', 'LEB:Red Ward:red ward').
card_rarity('red ward'/'LEB', 'Uncommon').
card_artist('red ward'/'LEB', 'Dan Frazier').
card_multiverse_id('red ward'/'LEB', '560').

card_in_set('regeneration', 'LEB').
card_original_type('regeneration'/'LEB', 'Enchant Creature').
card_original_text('regeneration'/'LEB', '{G} Target creature regenerates.').
card_image_name('regeneration'/'LEB', 'regeneration').
card_uid('regeneration'/'LEB', 'LEB:Regeneration:regeneration').
card_rarity('regeneration'/'LEB', 'Common').
card_artist('regeneration'/'LEB', 'Quinton Hoover').
card_multiverse_id('regeneration'/'LEB', '464').

card_in_set('regrowth', 'LEB').
card_original_type('regrowth'/'LEB', 'Sorcery').
card_original_text('regrowth'/'LEB', 'Return any card from your graveyard to your hand.').
card_image_name('regrowth'/'LEB', 'regrowth').
card_uid('regrowth'/'LEB', 'LEB:Regrowth:regrowth').
card_rarity('regrowth'/'LEB', 'Uncommon').
card_artist('regrowth'/'LEB', 'Dameon Willich').
card_multiverse_id('regrowth'/'LEB', '465').

card_in_set('resurrection', 'LEB').
card_original_type('resurrection'/'LEB', 'Sorcery').
card_original_text('resurrection'/'LEB', 'Take a creature from your graveyard and put it directly into play. You can\'t tap it until your next turn.').
card_image_name('resurrection'/'LEB', 'resurrection').
card_uid('resurrection'/'LEB', 'LEB:Resurrection:resurrection').
card_rarity('resurrection'/'LEB', 'Uncommon').
card_artist('resurrection'/'LEB', 'Dan Frazier').
card_multiverse_id('resurrection'/'LEB', '561').

card_in_set('reverse damage', 'LEB').
card_original_type('reverse damage'/'LEB', 'Instant').
card_original_text('reverse damage'/'LEB', 'All damage you have taken from any one source this turn is added to your life total instead of subtracted from it.').
card_image_name('reverse damage'/'LEB', 'reverse damage').
card_uid('reverse damage'/'LEB', 'LEB:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'LEB', 'Rare').
card_artist('reverse damage'/'LEB', 'Dameon Willich').
card_multiverse_id('reverse damage'/'LEB', '562').

card_in_set('righteousness', 'LEB').
card_original_type('righteousness'/'LEB', 'Instant').
card_original_text('righteousness'/'LEB', 'Target defending creature gains +7/+7 until end of turn.').
card_image_name('righteousness'/'LEB', 'righteousness').
card_uid('righteousness'/'LEB', 'LEB:Righteousness:righteousness').
card_rarity('righteousness'/'LEB', 'Rare').
card_artist('righteousness'/'LEB', 'Douglas Shuler').
card_multiverse_id('righteousness'/'LEB', '563').

card_in_set('roc of kher ridges', 'LEB').
card_original_type('roc of kher ridges'/'LEB', 'Summon — Roc').
card_original_text('roc of kher ridges'/'LEB', 'Flying').
card_image_name('roc of kher ridges'/'LEB', 'roc of kher ridges').
card_uid('roc of kher ridges'/'LEB', 'LEB:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'LEB', 'Rare').
card_artist('roc of kher ridges'/'LEB', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'LEB', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').
card_multiverse_id('roc of kher ridges'/'LEB', '513').

card_in_set('rock hydra', 'LEB').
card_original_type('rock hydra'/'LEB', 'Summon — Hydra').
card_original_text('rock hydra'/'LEB', 'Put X +1/+1 counters (heads) on Hydra. Each point of damage Hydra suffers destroys one head unless {R} is spent. During upkeep, new heads may be grown for {R}{R}{R} apiece.').
card_image_name('rock hydra'/'LEB', 'rock hydra').
card_uid('rock hydra'/'LEB', 'LEB:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'LEB', 'Rare').
card_artist('rock hydra'/'LEB', 'Jeff A. Menges').
card_multiverse_id('rock hydra'/'LEB', '514').

card_in_set('rod of ruin', 'LEB').
card_original_type('rod of ruin'/'LEB', 'Mono Artifact').
card_original_text('rod of ruin'/'LEB', '{3}: Rod of Ruin does 1 damage to any target.').
card_image_name('rod of ruin'/'LEB', 'rod of ruin').
card_uid('rod of ruin'/'LEB', 'LEB:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'LEB', 'Uncommon').
card_artist('rod of ruin'/'LEB', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'LEB', '334').

card_in_set('royal assassin', 'LEB').
card_original_type('royal assassin'/'LEB', 'Summon — Assassin').
card_original_text('royal assassin'/'LEB', 'Tap to destroy any tapped creature.').
card_image_name('royal assassin'/'LEB', 'royal assassin').
card_uid('royal assassin'/'LEB', 'LEB:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'LEB', 'Rare').
card_artist('royal assassin'/'LEB', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'LEB', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'LEB', '374').

card_in_set('sacrifice', 'LEB').
card_original_type('sacrifice'/'LEB', 'Interrupt').
card_original_text('sacrifice'/'LEB', 'Destroy one of your creatures without regenerating it, and add to your mana pool a number of black mana equal to creature\'s casting cost.').
card_image_name('sacrifice'/'LEB', 'sacrifice').
card_uid('sacrifice'/'LEB', 'LEB:Sacrifice:sacrifice').
card_rarity('sacrifice'/'LEB', 'Uncommon').
card_artist('sacrifice'/'LEB', 'Dan Frazier').
card_multiverse_id('sacrifice'/'LEB', '375').

card_in_set('samite healer', 'LEB').
card_original_type('samite healer'/'LEB', 'Summon — Cleric').
card_original_text('samite healer'/'LEB', 'Tap to prevent 1 damage to any target.').
card_image_name('samite healer'/'LEB', 'samite healer').
card_uid('samite healer'/'LEB', 'LEB:Samite Healer:samite healer').
card_rarity('samite healer'/'LEB', 'Common').
card_artist('samite healer'/'LEB', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'LEB', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'LEB', '564').

card_in_set('savannah', 'LEB').
card_original_type('savannah'/'LEB', 'Land').
card_original_text('savannah'/'LEB', 'Counts as both plains and forest and is affected by spells that affect either. Tap to add either {W} or {G} to your mana pool.').
card_image_name('savannah'/'LEB', 'savannah').
card_uid('savannah'/'LEB', 'LEB:Savannah:savannah').
card_rarity('savannah'/'LEB', 'Rare').
card_artist('savannah'/'LEB', 'Rob Alexander').
card_multiverse_id('savannah'/'LEB', '579').

card_in_set('savannah lions', 'LEB').
card_original_type('savannah lions'/'LEB', 'Summon — Lions').
card_original_text('savannah lions'/'LEB', '').
card_image_name('savannah lions'/'LEB', 'savannah lions').
card_uid('savannah lions'/'LEB', 'LEB:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'LEB', 'Rare').
card_artist('savannah lions'/'LEB', 'Daniel Gelon').
card_flavor_text('savannah lions'/'LEB', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').
card_multiverse_id('savannah lions'/'LEB', '565').

card_in_set('scathe zombies', 'LEB').
card_original_type('scathe zombies'/'LEB', 'Summon — Zombies').
card_original_text('scathe zombies'/'LEB', '').
card_image_name('scathe zombies'/'LEB', 'scathe zombies').
card_uid('scathe zombies'/'LEB', 'LEB:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'LEB', 'Common').
card_artist('scathe zombies'/'LEB', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'LEB', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/ To have seen those dead men rise.\"/ —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'LEB', '376').

card_in_set('scavenging ghoul', 'LEB').
card_original_type('scavenging ghoul'/'LEB', 'Summon — Ghoul').
card_original_text('scavenging ghoul'/'LEB', 'At the end of each turn, put one counter on the Ghoul for each other creature that was destroyed without regenerating during the turn. If Ghoul dies, you may use a counter to regenerate it; counters remain until used.').
card_image_name('scavenging ghoul'/'LEB', 'scavenging ghoul').
card_uid('scavenging ghoul'/'LEB', 'LEB:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'LEB', 'Uncommon').
card_artist('scavenging ghoul'/'LEB', 'Jeff A. Menges').
card_multiverse_id('scavenging ghoul'/'LEB', '377').

card_in_set('scrubland', 'LEB').
card_original_type('scrubland'/'LEB', 'Land').
card_original_text('scrubland'/'LEB', 'Counts as both plains and swamp and is affected by spells that affect either. Tap to add either {W} or {B} to your mana pool.').
card_image_name('scrubland'/'LEB', 'scrubland').
card_uid('scrubland'/'LEB', 'LEB:Scrubland:scrubland').
card_rarity('scrubland'/'LEB', 'Rare').
card_artist('scrubland'/'LEB', 'Jesper Myrfors').
card_multiverse_id('scrubland'/'LEB', '580').

card_in_set('scryb sprites', 'LEB').
card_original_type('scryb sprites'/'LEB', 'Summon — Faeries').
card_original_text('scryb sprites'/'LEB', 'Flying').
card_image_name('scryb sprites'/'LEB', 'scryb sprites').
card_uid('scryb sprites'/'LEB', 'LEB:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'LEB', 'Common').
card_artist('scryb sprites'/'LEB', 'Amy Weber').
card_flavor_text('scryb sprites'/'LEB', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'LEB', '466').

card_in_set('sea serpent', 'LEB').
card_original_type('sea serpent'/'LEB', 'Summon — Serpent').
card_original_text('sea serpent'/'LEB', 'Serpent cannot attack unless opponent has islands in play. Serpent is destroyed immediately if at any time controller has no islands in play.').
card_image_name('sea serpent'/'LEB', 'sea serpent').
card_uid('sea serpent'/'LEB', 'LEB:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'LEB', 'Common').
card_artist('sea serpent'/'LEB', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'LEB', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'LEB', '419').

card_in_set('sedge troll', 'LEB').
card_original_type('sedge troll'/'LEB', 'Summon — Troll').
card_original_text('sedge troll'/'LEB', '{B} Regenerates. Troll gains +1/+1 if controller has swamps in play.').
card_image_name('sedge troll'/'LEB', 'sedge troll').
card_uid('sedge troll'/'LEB', 'LEB:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'LEB', 'Rare').
card_artist('sedge troll'/'LEB', 'Dan Frazier').
card_flavor_text('sedge troll'/'LEB', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').
card_multiverse_id('sedge troll'/'LEB', '515').

card_in_set('sengir vampire', 'LEB').
card_original_type('sengir vampire'/'LEB', 'Summon — Vampire').
card_original_text('sengir vampire'/'LEB', 'Flying\nVampire gets a +1/+1 counter each time a creature dies during a turn in which Vampire damaged it, unless the dead creature is regenerated.').
card_image_name('sengir vampire'/'LEB', 'sengir vampire').
card_uid('sengir vampire'/'LEB', 'LEB:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'LEB', 'Uncommon').
card_artist('sengir vampire'/'LEB', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'LEB', '378').

card_in_set('serra angel', 'LEB').
card_original_type('serra angel'/'LEB', 'Summon — Angel').
card_original_text('serra angel'/'LEB', 'Flying\nDoes not tap when attacking.').
card_image_name('serra angel'/'LEB', 'serra angel').
card_uid('serra angel'/'LEB', 'LEB:Serra Angel:serra angel').
card_rarity('serra angel'/'LEB', 'Uncommon').
card_artist('serra angel'/'LEB', 'Douglas Shuler').
card_flavor_text('serra angel'/'LEB', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'LEB', '566').

card_in_set('shanodin dryads', 'LEB').
card_original_type('shanodin dryads'/'LEB', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'LEB', 'Forestwalk').
card_image_name('shanodin dryads'/'LEB', 'shanodin dryads').
card_uid('shanodin dryads'/'LEB', 'LEB:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'LEB', 'Common').
card_artist('shanodin dryads'/'LEB', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'LEB', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'LEB', '467').

card_in_set('shatter', 'LEB').
card_original_type('shatter'/'LEB', 'Instant').
card_original_text('shatter'/'LEB', 'Shatter destroys target artifact.').
card_image_name('shatter'/'LEB', 'shatter').
card_uid('shatter'/'LEB', 'LEB:Shatter:shatter').
card_rarity('shatter'/'LEB', 'Common').
card_artist('shatter'/'LEB', 'Amy Weber').
card_multiverse_id('shatter'/'LEB', '516').

card_in_set('shivan dragon', 'LEB').
card_original_type('shivan dragon'/'LEB', 'Summon — Dragon').
card_original_text('shivan dragon'/'LEB', 'Flying, {R} +1/+0 until end of turn.').
card_image_name('shivan dragon'/'LEB', 'shivan dragon').
card_uid('shivan dragon'/'LEB', 'LEB:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'LEB', 'Rare').
card_artist('shivan dragon'/'LEB', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'LEB', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'LEB', '517').

card_in_set('simulacrum', 'LEB').
card_original_type('simulacrum'/'LEB', 'Instant').
card_original_text('simulacrum'/'LEB', 'All damage done to you so far this turn is instead retroactively applied to one of your creatures in play. If this damage kills the creature it can be regenerated; even if there\'s more than enough damage to kill the creature, you don\'t suffer any of it. Further damage this turn is treated normally.').
card_image_name('simulacrum'/'LEB', 'simulacrum').
card_uid('simulacrum'/'LEB', 'LEB:Simulacrum:simulacrum').
card_rarity('simulacrum'/'LEB', 'Uncommon').
card_artist('simulacrum'/'LEB', 'Mark Poole').
card_multiverse_id('simulacrum'/'LEB', '379').

card_in_set('sinkhole', 'LEB').
card_original_type('sinkhole'/'LEB', 'Sorcery').
card_original_text('sinkhole'/'LEB', 'Destroys any one land.').
card_image_name('sinkhole'/'LEB', 'sinkhole').
card_uid('sinkhole'/'LEB', 'LEB:Sinkhole:sinkhole').
card_rarity('sinkhole'/'LEB', 'Common').
card_artist('sinkhole'/'LEB', 'Sandra Everingham').
card_multiverse_id('sinkhole'/'LEB', '380').

card_in_set('siren\'s call', 'LEB').
card_original_type('siren\'s call'/'LEB', 'Instant').
card_original_text('siren\'s call'/'LEB', 'All of opponent\'s creatures that can attack must do so. Any non-wall creatures that cannot attack are destroyed at end of turn. Play only during opponent\'s turn, before opponent\'s attack. Creatures summoned this turn are unaffected by Siren\'s Call.').
card_image_name('siren\'s call'/'LEB', 'siren\'s call').
card_uid('siren\'s call'/'LEB', 'LEB:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'LEB', 'Uncommon').
card_artist('siren\'s call'/'LEB', 'Anson Maddocks').
card_multiverse_id('siren\'s call'/'LEB', '420').

card_in_set('sleight of mind', 'LEB').
card_original_type('sleight of mind'/'LEB', 'Interrupt').
card_original_text('sleight of mind'/'LEB', 'Change the text of any card being played or already in play by replacing one color word with another. For example, you can change \"Counters red spells\" to \"Counters black spells.\" Cannot change mana symbols.').
card_image_name('sleight of mind'/'LEB', 'sleight of mind').
card_uid('sleight of mind'/'LEB', 'LEB:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'LEB', 'Rare').
card_artist('sleight of mind'/'LEB', 'Mark Poole').
card_multiverse_id('sleight of mind'/'LEB', '421').

card_in_set('smoke', 'LEB').
card_original_type('smoke'/'LEB', 'Enchantment').
card_original_text('smoke'/'LEB', 'Each player can only untap one creature during his or her untap phase.').
card_image_name('smoke'/'LEB', 'smoke').
card_uid('smoke'/'LEB', 'LEB:Smoke:smoke').
card_rarity('smoke'/'LEB', 'Rare').
card_artist('smoke'/'LEB', 'Jesper Myrfors').
card_multiverse_id('smoke'/'LEB', '518').

card_in_set('sol ring', 'LEB').
card_original_type('sol ring'/'LEB', 'Mono Artifact').
card_original_text('sol ring'/'LEB', 'Add 2 colorless mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('sol ring'/'LEB', 'sol ring').
card_uid('sol ring'/'LEB', 'LEB:Sol Ring:sol ring').
card_rarity('sol ring'/'LEB', 'Uncommon').
card_artist('sol ring'/'LEB', 'Mark Tedin').
card_multiverse_id('sol ring'/'LEB', '335').

card_in_set('soul net', 'LEB').
card_original_type('soul net'/'LEB', 'Poly Artifact').
card_original_text('soul net'/'LEB', '{1}: You gain 1 life every time a creature is destroyed, unless it is then regenerated.').
card_image_name('soul net'/'LEB', 'soul net').
card_uid('soul net'/'LEB', 'LEB:Soul Net:soul net').
card_rarity('soul net'/'LEB', 'Uncommon').
card_artist('soul net'/'LEB', 'Dameon Willich').
card_multiverse_id('soul net'/'LEB', '336').

card_in_set('spell blast', 'LEB').
card_original_type('spell blast'/'LEB', 'Interrupt').
card_original_text('spell blast'/'LEB', 'Target spell is countered; X is cost of target spell.').
card_image_name('spell blast'/'LEB', 'spell blast').
card_uid('spell blast'/'LEB', 'LEB:Spell Blast:spell blast').
card_rarity('spell blast'/'LEB', 'Common').
card_artist('spell blast'/'LEB', 'Brian Snõddy').
card_multiverse_id('spell blast'/'LEB', '422').

card_in_set('stasis', 'LEB').
card_original_type('stasis'/'LEB', 'Enchantment').
card_original_text('stasis'/'LEB', 'Players do not get an untap phase. Pay {U} during your upkeep or Stasis is destroyed.').
card_image_name('stasis'/'LEB', 'stasis').
card_uid('stasis'/'LEB', 'LEB:Stasis:stasis').
card_rarity('stasis'/'LEB', 'Rare').
card_artist('stasis'/'LEB', 'Fay Jones').
card_multiverse_id('stasis'/'LEB', '423').

card_in_set('steal artifact', 'LEB').
card_original_type('steal artifact'/'LEB', 'Enchant Artifact').
card_original_text('steal artifact'/'LEB', 'You control target artifact until enchantment is discarded or game ends. If target artifact was tapped when stolen, it stays tapped until you can untap it. If destroyed, target artifact is put in its owner\'s graveyard.').
card_image_name('steal artifact'/'LEB', 'steal artifact').
card_uid('steal artifact'/'LEB', 'LEB:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'LEB', 'Uncommon').
card_artist('steal artifact'/'LEB', 'Amy Weber').
card_multiverse_id('steal artifact'/'LEB', '424').

card_in_set('stone giant', 'LEB').
card_original_type('stone giant'/'LEB', 'Summon — Giant').
card_original_text('stone giant'/'LEB', 'Tap to make one of your own creatures a flying creature until end of turn. Target creature, which must have toughness less than Stone Giant\'s power, is destroyed at end of turn.').
card_image_name('stone giant'/'LEB', 'stone giant').
card_uid('stone giant'/'LEB', 'LEB:Stone Giant:stone giant').
card_rarity('stone giant'/'LEB', 'Uncommon').
card_artist('stone giant'/'LEB', 'Dameon Willich').
card_flavor_text('stone giant'/'LEB', 'What goes up, must come down.').
card_multiverse_id('stone giant'/'LEB', '519').

card_in_set('stone rain', 'LEB').
card_original_type('stone rain'/'LEB', 'Sorcery').
card_original_text('stone rain'/'LEB', 'Destroys any one land.').
card_image_name('stone rain'/'LEB', 'stone rain').
card_uid('stone rain'/'LEB', 'LEB:Stone Rain:stone rain').
card_rarity('stone rain'/'LEB', 'Common').
card_artist('stone rain'/'LEB', 'Daniel Gelon').
card_multiverse_id('stone rain'/'LEB', '520').

card_in_set('stream of life', 'LEB').
card_original_type('stream of life'/'LEB', 'Sorcery').
card_original_text('stream of life'/'LEB', 'Target player gains X life.').
card_image_name('stream of life'/'LEB', 'stream of life').
card_uid('stream of life'/'LEB', 'LEB:Stream of Life:stream of life').
card_rarity('stream of life'/'LEB', 'Common').
card_artist('stream of life'/'LEB', 'Mark Poole').
card_multiverse_id('stream of life'/'LEB', '468').

card_in_set('sunglasses of urza', 'LEB').
card_original_type('sunglasses of urza'/'LEB', 'Continuous Artifact').
card_original_text('sunglasses of urza'/'LEB', 'White mana in your mana pool can be used as either white or red mana.').
card_image_name('sunglasses of urza'/'LEB', 'sunglasses of urza').
card_uid('sunglasses of urza'/'LEB', 'LEB:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'LEB', 'Rare').
card_artist('sunglasses of urza'/'LEB', 'Dan Frazier').
card_multiverse_id('sunglasses of urza'/'LEB', '337').

card_in_set('swamp', 'LEB').
card_original_type('swamp'/'LEB', 'Land').
card_original_text('swamp'/'LEB', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'LEB', 'swamp1').
card_uid('swamp'/'LEB', 'LEB:Swamp:swamp1').
card_rarity('swamp'/'LEB', 'Basic Land').
card_artist('swamp'/'LEB', 'Dan Frazier').
card_multiverse_id('swamp'/'LEB', '575').

card_in_set('swamp', 'LEB').
card_original_type('swamp'/'LEB', 'Land').
card_original_text('swamp'/'LEB', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'LEB', 'swamp2').
card_uid('swamp'/'LEB', 'LEB:Swamp:swamp2').
card_rarity('swamp'/'LEB', 'Basic Land').
card_artist('swamp'/'LEB', 'Dan Frazier').
card_multiverse_id('swamp'/'LEB', '574').

card_in_set('swamp', 'LEB').
card_original_type('swamp'/'LEB', 'Land').
card_original_text('swamp'/'LEB', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'LEB', 'swamp3').
card_uid('swamp'/'LEB', 'LEB:Swamp:swamp3').
card_rarity('swamp'/'LEB', 'Basic Land').
card_artist('swamp'/'LEB', 'Dan Frazier').
card_multiverse_id('swamp'/'LEB', '573').

card_in_set('swords to plowshares', 'LEB').
card_original_type('swords to plowshares'/'LEB', 'Instant').
card_original_text('swords to plowshares'/'LEB', 'Target creature is removed from game entirely; return to owner\'s deck only when game is over. Creature\'s controller gains life points equal to creature\'s power.').
card_image_name('swords to plowshares'/'LEB', 'swords to plowshares').
card_uid('swords to plowshares'/'LEB', 'LEB:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'LEB', 'Uncommon').
card_artist('swords to plowshares'/'LEB', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'LEB', '567').

card_in_set('taiga', 'LEB').
card_original_type('taiga'/'LEB', 'Land').
card_original_text('taiga'/'LEB', 'Counts as both forest and mountains and is affected by spells that affect either. Tap to add either {G} or {R} to your mana pool.').
card_image_name('taiga'/'LEB', 'taiga').
card_uid('taiga'/'LEB', 'LEB:Taiga:taiga').
card_rarity('taiga'/'LEB', 'Rare').
card_artist('taiga'/'LEB', 'Rob Alexander').
card_multiverse_id('taiga'/'LEB', '581').

card_in_set('terror', 'LEB').
card_original_type('terror'/'LEB', 'Instant').
card_original_text('terror'/'LEB', 'Destroys target creature without possibility of regeneration. Does not affect black creatures and artifact creatures.').
card_image_name('terror'/'LEB', 'terror').
card_uid('terror'/'LEB', 'LEB:Terror:terror').
card_rarity('terror'/'LEB', 'Common').
card_artist('terror'/'LEB', 'Ron Spencer').
card_multiverse_id('terror'/'LEB', '381').

card_in_set('the hive', 'LEB').
card_original_type('the hive'/'LEB', 'Mono Artifact').
card_original_text('the hive'/'LEB', '{5}: Creates one Giant Wasp, a 1/1 flying creature. Represent Wasps with tokens, making sure to indicate when each Wasp is tapped. Wasps can\'t attack during the turn created. Treat Wasps like artifact creatures in every way, except that they are removed from the game entirely if they ever leave play. If the Hive is destroyed, the Wasps must still be killed individually.').
card_image_name('the hive'/'LEB', 'the hive').
card_uid('the hive'/'LEB', 'LEB:The Hive:the hive').
card_rarity('the hive'/'LEB', 'Rare').
card_artist('the hive'/'LEB', 'Sandra Everingham').
card_multiverse_id('the hive'/'LEB', '338').

card_in_set('thicket basilisk', 'LEB').
card_original_type('thicket basilisk'/'LEB', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'LEB', 'Any non-wall creature blocking Basilisk is destroyed, as is any creature blocked by Basilisk. Creatures destroyed this way deal their damage before dying.').
card_image_name('thicket basilisk'/'LEB', 'thicket basilisk').
card_uid('thicket basilisk'/'LEB', 'LEB:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'LEB', 'Uncommon').
card_artist('thicket basilisk'/'LEB', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'LEB', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'LEB', '469').

card_in_set('thoughtlace', 'LEB').
card_original_type('thoughtlace'/'LEB', 'Interrupt').
card_original_text('thoughtlace'/'LEB', 'Changes the color of one card either being played or already in play to blue. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('thoughtlace'/'LEB', 'thoughtlace').
card_uid('thoughtlace'/'LEB', 'LEB:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'LEB', 'Rare').
card_artist('thoughtlace'/'LEB', 'Mark Poole').
card_multiverse_id('thoughtlace'/'LEB', '425').

card_in_set('throne of bone', 'LEB').
card_original_type('throne of bone'/'LEB', 'Poly Artifact').
card_original_text('throne of bone'/'LEB', '{1}: Any black spell cast by any player gives you 1 life.').
card_image_name('throne of bone'/'LEB', 'throne of bone').
card_uid('throne of bone'/'LEB', 'LEB:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'LEB', 'Uncommon').
card_artist('throne of bone'/'LEB', 'Anson Maddocks').
card_multiverse_id('throne of bone'/'LEB', '339').

card_in_set('timber wolves', 'LEB').
card_original_type('timber wolves'/'LEB', 'Summon — Wolves').
card_original_text('timber wolves'/'LEB', 'Bands').
card_image_name('timber wolves'/'LEB', 'timber wolves').
card_uid('timber wolves'/'LEB', 'LEB:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'LEB', 'Rare').
card_artist('timber wolves'/'LEB', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'LEB', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').
card_multiverse_id('timber wolves'/'LEB', '470').

card_in_set('time vault', 'LEB').
card_original_type('time vault'/'LEB', 'Mono Artifact').
card_original_text('time vault'/'LEB', 'Tap to gain an additional turn after the current one. Time Vault doesn\'t untap normally during untap phase; to untap it, you must skip a turn. Time Vault begins tapped.').
card_image_name('time vault'/'LEB', 'time vault').
card_uid('time vault'/'LEB', 'LEB:Time Vault:time vault').
card_rarity('time vault'/'LEB', 'Rare').
card_artist('time vault'/'LEB', 'Mark Tedin').
card_multiverse_id('time vault'/'LEB', '340').

card_in_set('time walk', 'LEB').
card_original_type('time walk'/'LEB', 'Sorcery').
card_original_text('time walk'/'LEB', 'Take an extra turn after this one.').
card_image_name('time walk'/'LEB', 'time walk').
card_uid('time walk'/'LEB', 'LEB:Time Walk:time walk').
card_rarity('time walk'/'LEB', 'Rare').
card_artist('time walk'/'LEB', 'Amy Weber').
card_multiverse_id('time walk'/'LEB', '426').

card_in_set('timetwister', 'LEB').
card_original_type('timetwister'/'LEB', 'Sorcery').
card_original_text('timetwister'/'LEB', 'Set Timetwister aside in a new graveyard pile. Shuffle your hand, library, and graveyard together into a new library and draw a new hand of seven cards, leaving all cards in play where they are; opponent must do the same.').
card_image_name('timetwister'/'LEB', 'timetwister').
card_uid('timetwister'/'LEB', 'LEB:Timetwister:timetwister').
card_rarity('timetwister'/'LEB', 'Rare').
card_artist('timetwister'/'LEB', 'Mark Tedin').
card_multiverse_id('timetwister'/'LEB', '427').

card_in_set('tranquility', 'LEB').
card_original_type('tranquility'/'LEB', 'Sorcery').
card_original_text('tranquility'/'LEB', 'All enchantments in play must be discarded.').
card_image_name('tranquility'/'LEB', 'tranquility').
card_uid('tranquility'/'LEB', 'LEB:Tranquility:tranquility').
card_rarity('tranquility'/'LEB', 'Common').
card_artist('tranquility'/'LEB', 'Douglas Shuler').
card_multiverse_id('tranquility'/'LEB', '471').

card_in_set('tropical island', 'LEB').
card_original_type('tropical island'/'LEB', 'Land').
card_original_text('tropical island'/'LEB', 'Counts as both forest and islands and is affected by spells that affect either. Tap to add either {G} or {U} to your mana pool.').
card_image_name('tropical island'/'LEB', 'tropical island').
card_uid('tropical island'/'LEB', 'LEB:Tropical Island:tropical island').
card_rarity('tropical island'/'LEB', 'Rare').
card_artist('tropical island'/'LEB', 'Jesper Myrfors').
card_multiverse_id('tropical island'/'LEB', '582').

card_in_set('tsunami', 'LEB').
card_original_type('tsunami'/'LEB', 'Sorcery').
card_original_text('tsunami'/'LEB', 'All islands in play are destroyed.').
card_image_name('tsunami'/'LEB', 'tsunami').
card_uid('tsunami'/'LEB', 'LEB:Tsunami:tsunami').
card_rarity('tsunami'/'LEB', 'Uncommon').
card_artist('tsunami'/'LEB', 'Richard Thomas').
card_multiverse_id('tsunami'/'LEB', '472').

card_in_set('tundra', 'LEB').
card_original_type('tundra'/'LEB', 'Land').
card_original_text('tundra'/'LEB', 'Counts as both islands and plains and is affected by spells that affect either. Tap to add either {U} or {W} to your mana pool.').
card_image_name('tundra'/'LEB', 'tundra').
card_uid('tundra'/'LEB', 'LEB:Tundra:tundra').
card_rarity('tundra'/'LEB', 'Rare').
card_artist('tundra'/'LEB', 'Jesper Myrfors').
card_multiverse_id('tundra'/'LEB', '583').

card_in_set('tunnel', 'LEB').
card_original_type('tunnel'/'LEB', 'Instant').
card_original_text('tunnel'/'LEB', 'Destroys 1 wall. Target wall cannot be regenerated.').
card_image_name('tunnel'/'LEB', 'tunnel').
card_uid('tunnel'/'LEB', 'LEB:Tunnel:tunnel').
card_rarity('tunnel'/'LEB', 'Uncommon').
card_artist('tunnel'/'LEB', 'Dan Frazier').
card_multiverse_id('tunnel'/'LEB', '521').

card_in_set('twiddle', 'LEB').
card_original_type('twiddle'/'LEB', 'Instant').
card_original_text('twiddle'/'LEB', 'Caster may tap or untap any one land, creature, or artifact in play. No effects are generated by the target card.').
card_image_name('twiddle'/'LEB', 'twiddle').
card_uid('twiddle'/'LEB', 'LEB:Twiddle:twiddle').
card_rarity('twiddle'/'LEB', 'Common').
card_artist('twiddle'/'LEB', 'Rob Alexander').
card_multiverse_id('twiddle'/'LEB', '428').

card_in_set('two-headed giant of foriys', 'LEB').
card_original_type('two-headed giant of foriys'/'LEB', 'Summon — Giant').
card_original_text('two-headed giant of foriys'/'LEB', 'Trample\nMay block two attacking creatures; divide damage between them however controller likes.').
card_image_name('two-headed giant of foriys'/'LEB', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'LEB', 'LEB:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'LEB', 'Rare').
card_artist('two-headed giant of foriys'/'LEB', 'Anson Maddocks').
card_flavor_text('two-headed giant of foriys'/'LEB', 'None know if this Giant is the result of aberrant magics, Siamese twins, or a mentalist\'s schizophrenia.').
card_multiverse_id('two-headed giant of foriys'/'LEB', '522').

card_in_set('underground sea', 'LEB').
card_original_type('underground sea'/'LEB', 'Land').
card_original_text('underground sea'/'LEB', 'Counts as both swamp and islands and is affected by spells that affect either. Tap to add either {B} or {U} to your mana pool.').
card_image_name('underground sea'/'LEB', 'underground sea').
card_uid('underground sea'/'LEB', 'LEB:Underground Sea:underground sea').
card_rarity('underground sea'/'LEB', 'Rare').
card_artist('underground sea'/'LEB', 'Rob Alexander').
card_multiverse_id('underground sea'/'LEB', '584').

card_in_set('unholy strength', 'LEB').
card_original_type('unholy strength'/'LEB', 'Enchant Creature').
card_original_text('unholy strength'/'LEB', 'Target creature gains +2/+1.').
card_image_name('unholy strength'/'LEB', 'unholy strength').
card_uid('unholy strength'/'LEB', 'LEB:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'LEB', 'Common').
card_artist('unholy strength'/'LEB', 'Douglas Shuler').
card_multiverse_id('unholy strength'/'LEB', '382').

card_in_set('unsummon', 'LEB').
card_original_type('unsummon'/'LEB', 'Instant').
card_original_text('unsummon'/'LEB', 'Return creature to owner\'s hand; enchantments on creature are discarded. Unsummon cannot be played during the damage-dealing phase of an attack.').
card_image_name('unsummon'/'LEB', 'unsummon').
card_uid('unsummon'/'LEB', 'LEB:Unsummon:unsummon').
card_rarity('unsummon'/'LEB', 'Common').
card_artist('unsummon'/'LEB', 'Douglas Shuler').
card_multiverse_id('unsummon'/'LEB', '429').

card_in_set('uthden troll', 'LEB').
card_original_type('uthden troll'/'LEB', 'Summon — Troll').
card_original_text('uthden troll'/'LEB', '{R} Regenerates').
card_image_name('uthden troll'/'LEB', 'uthden troll').
card_uid('uthden troll'/'LEB', 'LEB:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'LEB', 'Uncommon').
card_artist('uthden troll'/'LEB', 'Douglas Shuler').
card_flavor_text('uthden troll'/'LEB', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').
card_multiverse_id('uthden troll'/'LEB', '523').

card_in_set('verduran enchantress', 'LEB').
card_original_type('verduran enchantress'/'LEB', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'LEB', 'While Enchantress is in play, you may immediately draw a card from your library each time you cast an enchantment.').
card_image_name('verduran enchantress'/'LEB', 'verduran enchantress').
card_uid('verduran enchantress'/'LEB', 'LEB:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'LEB', 'Rare').
card_artist('verduran enchantress'/'LEB', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'LEB', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'LEB', '473').

card_in_set('vesuvan doppelganger', 'LEB').
card_original_type('vesuvan doppelganger'/'LEB', 'Summon — Doppelganger').
card_original_text('vesuvan doppelganger'/'LEB', 'Upon summoning, Doppelganger acquires all normal characteristics (except color) of any one creature in play on either side; any enchantments on the original creature are not copied. During controller\'s upkeep, Doppelganger may take on the characteristics of a different creature in play instead. Doppleganger may continue to copy a creature even after that creature leaves play, but if it switches it won\'t be able to switch back.').
card_image_name('vesuvan doppelganger'/'LEB', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'LEB', 'LEB:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'LEB', 'Rare').
card_artist('vesuvan doppelganger'/'LEB', 'Quinton Hoover').
card_multiverse_id('vesuvan doppelganger'/'LEB', '430').

card_in_set('veteran bodyguard', 'LEB').
card_original_type('veteran bodyguard'/'LEB', 'Summon — Bodyguard').
card_original_text('veteran bodyguard'/'LEB', 'Unless Bodyguard is tapped, any damage done to you by unblocked creatures is done instead to Bodyguard. You may not take this damage yourself, though you can prevent it if possible.').
card_image_name('veteran bodyguard'/'LEB', 'veteran bodyguard').
card_uid('veteran bodyguard'/'LEB', 'LEB:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'LEB', 'Rare').
card_artist('veteran bodyguard'/'LEB', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'LEB', 'Good bodyguards are hard to find, mainly because they don\'t live long.').
card_multiverse_id('veteran bodyguard'/'LEB', '568').

card_in_set('volcanic eruption', 'LEB').
card_original_type('volcanic eruption'/'LEB', 'Sorcery').
card_original_text('volcanic eruption'/'LEB', 'Destroys X mountains of your choice, and does X damage to each player and each creature in play.').
card_image_name('volcanic eruption'/'LEB', 'volcanic eruption').
card_uid('volcanic eruption'/'LEB', 'LEB:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'LEB', 'Rare').
card_artist('volcanic eruption'/'LEB', 'Douglas Shuler').
card_multiverse_id('volcanic eruption'/'LEB', '431').

card_in_set('volcanic island', 'LEB').
card_original_type('volcanic island'/'LEB', 'Land').
card_original_text('volcanic island'/'LEB', 'Counts as both islands and mountains and is affected by spells that affect either. Tap to add either {U} or {R} to your mana pool.').
card_first_print('volcanic island', 'LEB').
card_image_name('volcanic island'/'LEB', 'volcanic island').
card_uid('volcanic island'/'LEB', 'LEB:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'LEB', 'Rare').
card_artist('volcanic island'/'LEB', 'Brian Snõddy').
card_multiverse_id('volcanic island'/'LEB', '585').

card_in_set('wall of air', 'LEB').
card_original_type('wall of air'/'LEB', 'Summon — Wall').
card_original_text('wall of air'/'LEB', 'Flying').
card_image_name('wall of air'/'LEB', 'wall of air').
card_uid('wall of air'/'LEB', 'LEB:Wall of Air:wall of air').
card_rarity('wall of air'/'LEB', 'Uncommon').
card_artist('wall of air'/'LEB', 'Richard Thomas').
card_flavor_text('wall of air'/'LEB', '\"This ‘standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').
card_multiverse_id('wall of air'/'LEB', '432').

card_in_set('wall of bone', 'LEB').
card_original_type('wall of bone'/'LEB', 'Summon — Wall').
card_original_text('wall of bone'/'LEB', '{B} Regenerates').
card_image_name('wall of bone'/'LEB', 'wall of bone').
card_uid('wall of bone'/'LEB', 'LEB:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'LEB', 'Uncommon').
card_artist('wall of bone'/'LEB', 'Anson Maddocks').
card_flavor_text('wall of bone'/'LEB', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').
card_multiverse_id('wall of bone'/'LEB', '383').

card_in_set('wall of brambles', 'LEB').
card_original_type('wall of brambles'/'LEB', 'Summon — Wall').
card_original_text('wall of brambles'/'LEB', '{G} Regenerates').
card_image_name('wall of brambles'/'LEB', 'wall of brambles').
card_uid('wall of brambles'/'LEB', 'LEB:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'LEB', 'Uncommon').
card_artist('wall of brambles'/'LEB', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'LEB', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n —Conrad Aiken').
card_multiverse_id('wall of brambles'/'LEB', '474').

card_in_set('wall of fire', 'LEB').
card_original_type('wall of fire'/'LEB', 'Summon — Wall').
card_original_text('wall of fire'/'LEB', '{R} +1/+0 until end of turn.').
card_image_name('wall of fire'/'LEB', 'wall of fire').
card_uid('wall of fire'/'LEB', 'LEB:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'LEB', 'Uncommon').
card_artist('wall of fire'/'LEB', 'Richard Thomas').
card_flavor_text('wall of fire'/'LEB', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').
card_multiverse_id('wall of fire'/'LEB', '524').

card_in_set('wall of ice', 'LEB').
card_original_type('wall of ice'/'LEB', 'Summon — Wall').
card_original_text('wall of ice'/'LEB', '').
card_image_name('wall of ice'/'LEB', 'wall of ice').
card_uid('wall of ice'/'LEB', 'LEB:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'LEB', 'Uncommon').
card_artist('wall of ice'/'LEB', 'Richard Thomas').
card_flavor_text('wall of ice'/'LEB', '\"And through the drifts the snowy cliffs/ Did send a dismal sheen:/ Nor shapes of men nor beasts we ken—/ The ice was all between.\"/—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('wall of ice'/'LEB', '475').

card_in_set('wall of stone', 'LEB').
card_original_type('wall of stone'/'LEB', 'Summon — Wall').
card_original_text('wall of stone'/'LEB', '').
card_image_name('wall of stone'/'LEB', 'wall of stone').
card_uid('wall of stone'/'LEB', 'LEB:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'LEB', 'Uncommon').
card_artist('wall of stone'/'LEB', 'Dan Frazier').
card_flavor_text('wall of stone'/'LEB', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'LEB', '525').

card_in_set('wall of swords', 'LEB').
card_original_type('wall of swords'/'LEB', 'Summon — Wall').
card_original_text('wall of swords'/'LEB', 'Flying').
card_image_name('wall of swords'/'LEB', 'wall of swords').
card_uid('wall of swords'/'LEB', 'LEB:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'LEB', 'Uncommon').
card_artist('wall of swords'/'LEB', 'Mark Tedin').
card_flavor_text('wall of swords'/'LEB', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'LEB', '569').

card_in_set('wall of water', 'LEB').
card_original_type('wall of water'/'LEB', 'Summon — Wall').
card_original_text('wall of water'/'LEB', '{U} +1/+0 until end of turn.').
card_image_name('wall of water'/'LEB', 'wall of water').
card_uid('wall of water'/'LEB', 'LEB:Wall of Water:wall of water').
card_rarity('wall of water'/'LEB', 'Uncommon').
card_artist('wall of water'/'LEB', 'Richard Thomas').
card_flavor_text('wall of water'/'LEB', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').
card_multiverse_id('wall of water'/'LEB', '433').

card_in_set('wall of wood', 'LEB').
card_original_type('wall of wood'/'LEB', 'Summon — Wall').
card_original_text('wall of wood'/'LEB', '').
card_image_name('wall of wood'/'LEB', 'wall of wood').
card_uid('wall of wood'/'LEB', 'LEB:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'LEB', 'Common').
card_artist('wall of wood'/'LEB', 'Mark Tedin').
card_flavor_text('wall of wood'/'LEB', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').
card_multiverse_id('wall of wood'/'LEB', '476').

card_in_set('wanderlust', 'LEB').
card_original_type('wanderlust'/'LEB', 'Enchant Creature').
card_original_text('wanderlust'/'LEB', 'Wanderlust does 1 damage to target creature\'s controller during upkeep.').
card_image_name('wanderlust'/'LEB', 'wanderlust').
card_uid('wanderlust'/'LEB', 'LEB:Wanderlust:wanderlust').
card_rarity('wanderlust'/'LEB', 'Uncommon').
card_artist('wanderlust'/'LEB', 'Cornelius Brudi').
card_multiverse_id('wanderlust'/'LEB', '477').

card_in_set('war mammoth', 'LEB').
card_original_type('war mammoth'/'LEB', 'Summon — Mammoth').
card_original_text('war mammoth'/'LEB', 'Trample').
card_image_name('war mammoth'/'LEB', 'war mammoth').
card_uid('war mammoth'/'LEB', 'LEB:War Mammoth:war mammoth').
card_rarity('war mammoth'/'LEB', 'Common').
card_artist('war mammoth'/'LEB', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'LEB', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'LEB', '478').

card_in_set('warp artifact', 'LEB').
card_original_type('warp artifact'/'LEB', 'Enchant Artifact').
card_original_text('warp artifact'/'LEB', 'Warp Artifact does 1 damage to target artifact\'s controller at start of each turn.').
card_image_name('warp artifact'/'LEB', 'warp artifact').
card_uid('warp artifact'/'LEB', 'LEB:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'LEB', 'Rare').
card_artist('warp artifact'/'LEB', 'Amy Weber').
card_multiverse_id('warp artifact'/'LEB', '384').

card_in_set('water elemental', 'LEB').
card_original_type('water elemental'/'LEB', 'Summon — Elemental').
card_original_text('water elemental'/'LEB', '').
card_image_name('water elemental'/'LEB', 'water elemental').
card_uid('water elemental'/'LEB', 'LEB:Water Elemental:water elemental').
card_rarity('water elemental'/'LEB', 'Uncommon').
card_artist('water elemental'/'LEB', 'Jeff A. Menges').
card_flavor_text('water elemental'/'LEB', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'LEB', '434').

card_in_set('weakness', 'LEB').
card_original_type('weakness'/'LEB', 'Enchant Creature').
card_original_text('weakness'/'LEB', 'Target creature loses -2/-1; if this drops the creature\'s toughness below 1, it is dead.').
card_image_name('weakness'/'LEB', 'weakness').
card_uid('weakness'/'LEB', 'LEB:Weakness:weakness').
card_rarity('weakness'/'LEB', 'Common').
card_artist('weakness'/'LEB', 'Douglas Shuler').
card_multiverse_id('weakness'/'LEB', '385').

card_in_set('web', 'LEB').
card_original_type('web'/'LEB', 'Enchant Creature').
card_original_text('web'/'LEB', 'Target creature gains +0/+2 and can now block flying creatures, though it does not gain the power to fly.').
card_image_name('web'/'LEB', 'web').
card_uid('web'/'LEB', 'LEB:Web:web').
card_rarity('web'/'LEB', 'Rare').
card_artist('web'/'LEB', 'Rob Alexander').
card_multiverse_id('web'/'LEB', '479').

card_in_set('wheel of fortune', 'LEB').
card_original_type('wheel of fortune'/'LEB', 'Sorcery').
card_original_text('wheel of fortune'/'LEB', 'Both players must discard their hands and draw seven new cards.').
card_image_name('wheel of fortune'/'LEB', 'wheel of fortune').
card_uid('wheel of fortune'/'LEB', 'LEB:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'LEB', 'Rare').
card_artist('wheel of fortune'/'LEB', 'Daniel Gelon').
card_multiverse_id('wheel of fortune'/'LEB', '526').

card_in_set('white knight', 'LEB').
card_original_type('white knight'/'LEB', 'Summon — Knight').
card_original_text('white knight'/'LEB', 'Protection from black, first strike').
card_image_name('white knight'/'LEB', 'white knight').
card_uid('white knight'/'LEB', 'LEB:White Knight:white knight').
card_rarity('white knight'/'LEB', 'Uncommon').
card_artist('white knight'/'LEB', 'Daniel Gelon').
card_flavor_text('white knight'/'LEB', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'LEB', '570').

card_in_set('white ward', 'LEB').
card_original_type('white ward'/'LEB', 'Enchant Creature').
card_original_text('white ward'/'LEB', 'Target creature gains protection from white.').
card_image_name('white ward'/'LEB', 'white ward').
card_uid('white ward'/'LEB', 'LEB:White Ward:white ward').
card_rarity('white ward'/'LEB', 'Uncommon').
card_artist('white ward'/'LEB', 'Dan Frazier').
card_multiverse_id('white ward'/'LEB', '571').

card_in_set('wild growth', 'LEB').
card_original_type('wild growth'/'LEB', 'Enchant Land').
card_original_text('wild growth'/'LEB', 'When tapped, target land provides 1 green mana in addition to the mana it normally provides.').
card_image_name('wild growth'/'LEB', 'wild growth').
card_uid('wild growth'/'LEB', 'LEB:Wild Growth:wild growth').
card_rarity('wild growth'/'LEB', 'Common').
card_artist('wild growth'/'LEB', 'Mark Poole').
card_multiverse_id('wild growth'/'LEB', '480').

card_in_set('will-o\'-the-wisp', 'LEB').
card_original_type('will-o\'-the-wisp'/'LEB', 'Summon — Will-O\'-The-Wisp').
card_original_text('will-o\'-the-wisp'/'LEB', 'Flying; {B} Regenerates').
card_image_name('will-o\'-the-wisp'/'LEB', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'LEB', 'LEB:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'LEB', 'Rare').
card_artist('will-o\'-the-wisp'/'LEB', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'LEB', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('will-o\'-the-wisp'/'LEB', '386').

card_in_set('winter orb', 'LEB').
card_original_type('winter orb'/'LEB', 'Continuous Artifact').
card_original_text('winter orb'/'LEB', 'Players can untap only one land each during untap phase. Creatures and artifacts are untapped as normal.').
card_image_name('winter orb'/'LEB', 'winter orb').
card_uid('winter orb'/'LEB', 'LEB:Winter Orb:winter orb').
card_rarity('winter orb'/'LEB', 'Rare').
card_artist('winter orb'/'LEB', 'Mark Tedin').
card_multiverse_id('winter orb'/'LEB', '341').

card_in_set('wooden sphere', 'LEB').
card_original_type('wooden sphere'/'LEB', 'Poly Artifact').
card_original_text('wooden sphere'/'LEB', '{1}: Any green spell cast by any player gives you 1 life.').
card_image_name('wooden sphere'/'LEB', 'wooden sphere').
card_uid('wooden sphere'/'LEB', 'LEB:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'LEB', 'Uncommon').
card_artist('wooden sphere'/'LEB', 'Mark Tedin').
card_multiverse_id('wooden sphere'/'LEB', '342').

card_in_set('word of command', 'LEB').
card_original_type('word of command'/'LEB', 'Instant').
card_original_text('word of command'/'LEB', 'You may look at opponent\'s hand and choose any card opponent can legally play using mana from his or her mana pool or lands. Opponent must play this card immediately; you make all the decisions it calls for. This spell may not be countered after you have looked at opponent\'s hand.').
card_image_name('word of command'/'LEB', 'word of command').
card_uid('word of command'/'LEB', 'LEB:Word of Command:word of command').
card_rarity('word of command'/'LEB', 'Rare').
card_artist('word of command'/'LEB', 'Jesper Myrfors').
card_multiverse_id('word of command'/'LEB', '387').

card_in_set('wrath of god', 'LEB').
card_original_type('wrath of god'/'LEB', 'Sorcery').
card_original_text('wrath of god'/'LEB', 'All creatures in play are destroyed and cannot regenerate.').
card_image_name('wrath of god'/'LEB', 'wrath of god').
card_uid('wrath of god'/'LEB', 'LEB:Wrath of God:wrath of god').
card_rarity('wrath of god'/'LEB', 'Rare').
card_artist('wrath of god'/'LEB', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'LEB', '572').

card_in_set('zombie master', 'LEB').
card_original_type('zombie master'/'LEB', 'Summon — Lord').
card_original_text('zombie master'/'LEB', 'All zombies in play gain swampwalk and \"{B} Regenerates\" for as long as this card remains in play.').
card_image_name('zombie master'/'LEB', 'zombie master').
card_uid('zombie master'/'LEB', 'LEB:Zombie Master:zombie master').
card_rarity('zombie master'/'LEB', 'Rare').
card_artist('zombie master'/'LEB', 'Jeff A. Menges').
card_flavor_text('zombie master'/'LEB', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'LEB', '388').
