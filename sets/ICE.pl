% Ice Age

set('ICE').
set_name('ICE', 'Ice Age').
set_release_date('ICE', '1995-06-01').
set_border('ICE', 'black').
set_type('ICE', 'expansion').
set_block('ICE', 'Ice Age').

card_in_set('abyssal specter', 'ICE').
card_original_type('abyssal specter'/'ICE', 'Summon — Specter').
card_original_text('abyssal specter'/'ICE', 'Flying\nWhenever Abyssal Specter damages any player, that player chooses and discards a card from his or her hand. Ignore this ability if the player has no cards in hand.').
card_first_print('abyssal specter', 'ICE').
card_image_name('abyssal specter'/'ICE', 'abyssal specter').
card_uid('abyssal specter'/'ICE', 'ICE:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'ICE', 'Uncommon').
card_artist('abyssal specter'/'ICE', 'Ruth Thompson').
card_flavor_text('abyssal specter'/'ICE', '\"Open the gates, and bid them enter.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('abyssal specter'/'ICE', '2437').

card_in_set('adarkar sentinel', 'ICE').
card_original_type('adarkar sentinel'/'ICE', 'Artifact Creature').
card_original_text('adarkar sentinel'/'ICE', '{1}: +0/+1 until end of turn').
card_first_print('adarkar sentinel', 'ICE').
card_image_name('adarkar sentinel'/'ICE', 'adarkar sentinel').
card_uid('adarkar sentinel'/'ICE', 'ICE:Adarkar Sentinel:adarkar sentinel').
card_rarity('adarkar sentinel'/'ICE', 'Uncommon').
card_artist('adarkar sentinel'/'ICE', 'Melissa A. Benson').
card_flavor_text('adarkar sentinel'/'ICE', '\"We encountered the Sentinels in the wastes, near no living thing. Their purpose was inscrutable.\"\n—Disa the Restless, journal entry').
card_multiverse_id('adarkar sentinel'/'ICE', '2392').

card_in_set('adarkar unicorn', 'ICE').
card_original_type('adarkar unicorn'/'ICE', 'Summon — Unicorn').
card_original_text('adarkar unicorn'/'ICE', '{T}: Add either {U} or {U} and one colorless mana to your mana pool. This mana is usable only for cumulative upkeep. Play this ability as an interrupt.').
card_first_print('adarkar unicorn', 'ICE').
card_image_name('adarkar unicorn'/'ICE', 'adarkar unicorn').
card_uid('adarkar unicorn'/'ICE', 'ICE:Adarkar Unicorn:adarkar unicorn').
card_rarity('adarkar unicorn'/'ICE', 'Common').
card_artist('adarkar unicorn'/'ICE', 'Quinton Hoover').
card_flavor_text('adarkar unicorn'/'ICE', '\"There is no nobler creature in all of Terisiare.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('adarkar unicorn'/'ICE', '2661').

card_in_set('adarkar wastes', 'ICE').
card_original_type('adarkar wastes'/'ICE', 'Land').
card_original_text('adarkar wastes'/'ICE', '{T}: Add {1} to your mana pool.\n{T}: Add {W} to your mana pool. Adarkar Wastes deals 1 damage to you.\n{T}: Add {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_first_print('adarkar wastes', 'ICE').
card_image_name('adarkar wastes'/'ICE', 'adarkar wastes').
card_uid('adarkar wastes'/'ICE', 'ICE:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'ICE', 'Rare').
card_artist('adarkar wastes'/'ICE', 'Mike Raabe').
card_multiverse_id('adarkar wastes'/'ICE', '2750').

card_in_set('aegis of the meek', 'ICE').
card_original_type('aegis of the meek'/'ICE', 'Artifact').
card_original_text('aegis of the meek'/'ICE', '{1},{T}: Target 1/1 creature gets +1/+2 until end of turn.').
card_first_print('aegis of the meek', 'ICE').
card_image_name('aegis of the meek'/'ICE', 'aegis of the meek').
card_uid('aegis of the meek'/'ICE', 'ICE:Aegis of the Meek:aegis of the meek').
card_rarity('aegis of the meek'/'ICE', 'Rare').
card_artist('aegis of the meek'/'ICE', 'L. A. Williams').
card_flavor_text('aegis of the meek'/'ICE', '\"With this marvel, even the weak have a fighting chance!\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('aegis of the meek'/'ICE', '2393').

card_in_set('aggression', 'ICE').
card_original_type('aggression'/'ICE', 'Enchant Creature').
card_original_text('aggression'/'ICE', 'Target non-wall creature gains first strike and trample. At the end of its controller\'s turn, destroy that creature if it did not attack that turn.').
card_first_print('aggression', 'ICE').
card_image_name('aggression'/'ICE', 'aggression').
card_uid('aggression'/'ICE', 'ICE:Aggression:aggression').
card_rarity('aggression'/'ICE', 'Uncommon').
card_artist('aggression'/'ICE', 'Rick Emond').
card_flavor_text('aggression'/'ICE', 'The star that burns twice as bright burns half as long.').
card_multiverse_id('aggression'/'ICE', '2605').

card_in_set('altar of bone', 'ICE').
card_original_type('altar of bone'/'ICE', 'Sorcery').
card_original_text('altar of bone'/'ICE', 'Sacrifice a creature to look through your library for a creature card; put that card into your hand after showing it to all other players. Reshuffle your library afterwards.').
card_first_print('altar of bone', 'ICE').
card_image_name('altar of bone'/'ICE', 'altar of bone').
card_uid('altar of bone'/'ICE', 'ICE:Altar of Bone:altar of bone').
card_rarity('altar of bone'/'ICE', 'Rare').
card_artist('altar of bone'/'ICE', 'Melissa A. Benson').
card_multiverse_id('altar of bone'/'ICE', '2717').

card_in_set('amulet of quoz', 'ICE').
card_original_type('amulet of quoz'/'ICE', 'Artifact').
card_original_text('amulet of quoz'/'ICE', 'Remove Amulet of Quoz from your deck before playing if you are not playing for ante.\n{0},{T}: Sacrifice Amulet of Quoz. Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, that opponent loses the game. Otherwise, you lose the game.\nEffects that prevent or redirect damage cannot be used to prevent this loss of life. Use this ability only during your upkeep. The opponent may ante an additional card to counter this effect.').
card_first_print('amulet of quoz', 'ICE').
card_image_name('amulet of quoz'/'ICE', 'amulet of quoz').
card_uid('amulet of quoz'/'ICE', 'ICE:Amulet of Quoz:amulet of quoz').
card_rarity('amulet of quoz'/'ICE', 'Rare').
card_artist('amulet of quoz'/'ICE', 'Dan Frazier').
card_multiverse_id('amulet of quoz'/'ICE', '2394').

card_in_set('anarchy', 'ICE').
card_original_type('anarchy'/'ICE', 'Sorcery').
card_original_text('anarchy'/'ICE', 'Destroy all white permanents.').
card_first_print('anarchy', 'ICE').
card_image_name('anarchy'/'ICE', 'anarchy').
card_uid('anarchy'/'ICE', 'ICE:Anarchy:anarchy').
card_rarity('anarchy'/'ICE', 'Uncommon').
card_artist('anarchy'/'ICE', 'Phil Foglio').
card_flavor_text('anarchy'/'ICE', '\"The Shaman waved the staff, and the land itself went mad.\"\n—Disa the Restless, journal entry').
card_multiverse_id('anarchy'/'ICE', '2606').

card_in_set('arctic foxes', 'ICE').
card_original_type('arctic foxes'/'ICE', 'Summon — Foxes').
card_original_text('arctic foxes'/'ICE', 'If defending player controls any snow-covered lands, no creature with power greater than 1 may be assigned to block Arctic Foxes.').
card_first_print('arctic foxes', 'ICE').
card_image_name('arctic foxes'/'ICE', 'arctic foxes').
card_uid('arctic foxes'/'ICE', 'ICE:Arctic Foxes:arctic foxes').
card_rarity('arctic foxes'/'ICE', 'Common').
card_artist('arctic foxes'/'ICE', 'Mark Poole').
card_flavor_text('arctic foxes'/'ICE', '\"Those Foxes are wily, swift, and ferocious. They are the warriors of the snows.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('arctic foxes'/'ICE', '2662').

card_in_set('arcum\'s sleigh', 'ICE').
card_original_type('arcum\'s sleigh'/'ICE', 'Artifact').
card_original_text('arcum\'s sleigh'/'ICE', '{2},{T}: Attacking this turn does not cause target creature to tap. You cannot use this ability if defending player controls no snow-covered lands.').
card_first_print('arcum\'s sleigh', 'ICE').
card_image_name('arcum\'s sleigh'/'ICE', 'arcum\'s sleigh').
card_uid('arcum\'s sleigh'/'ICE', 'ICE:Arcum\'s Sleigh:arcum\'s sleigh').
card_rarity('arcum\'s sleigh'/'ICE', 'Uncommon').
card_artist('arcum\'s sleigh'/'ICE', 'Tom Wänerstrand').
card_flavor_text('arcum\'s sleigh'/'ICE', '\"With the proper equipment and caution, one can travel anywhere.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('arcum\'s sleigh'/'ICE', '2395').

card_in_set('arcum\'s weathervane', 'ICE').
card_original_type('arcum\'s weathervane'/'ICE', 'Artifact').
card_original_text('arcum\'s weathervane'/'ICE', '{2},{T}: Target snow-covered land becomes a non-snow-covered land of the same type. Mark the changed land with a counter.\n{2},{T}: Target non-snow-covered basic land becomes a snow-covered land of the same type. Mark the changed land with a counter.').
card_first_print('arcum\'s weathervane', 'ICE').
card_image_name('arcum\'s weathervane'/'ICE', 'arcum\'s weathervane').
card_uid('arcum\'s weathervane'/'ICE', 'ICE:Arcum\'s Weathervane:arcum\'s weathervane').
card_rarity('arcum\'s weathervane'/'ICE', 'Uncommon').
card_artist('arcum\'s weathervane'/'ICE', 'Tom Wänerstrand').
card_multiverse_id('arcum\'s weathervane'/'ICE', '2396').

card_in_set('arcum\'s whistle', 'ICE').
card_original_type('arcum\'s whistle'/'ICE', 'Artifact').
card_original_text('arcum\'s whistle'/'ICE', '{3},{T}: Target non-wall creature must attack. At end of turn, destroy that creature if it could not attack. Use this ability only during the creature\'s controller\'s turn before the attack. The creature\'s controller may counter this effect by paying X, where X is equal to the creature\'s casting cost. Arcum\'s Whistle does not affect creatures brought under their controller\'s control this turn.').
card_first_print('arcum\'s whistle', 'ICE').
card_image_name('arcum\'s whistle'/'ICE', 'arcum\'s whistle').
card_uid('arcum\'s whistle'/'ICE', 'ICE:Arcum\'s Whistle:arcum\'s whistle').
card_rarity('arcum\'s whistle'/'ICE', 'Uncommon').
card_artist('arcum\'s whistle'/'ICE', 'Quinton Hoover').
card_multiverse_id('arcum\'s whistle'/'ICE', '2397').

card_in_set('arenson\'s aura', 'ICE').
card_original_type('arenson\'s aura'/'ICE', 'Enchantment').
card_original_text('arenson\'s aura'/'ICE', '{W} Sacrifice an enchantment to destroy target enchantment.\n{3}{U}{U} Counter target enchantment.').
card_first_print('arenson\'s aura', 'ICE').
card_image_name('arenson\'s aura'/'ICE', 'arenson\'s aura').
card_uid('arenson\'s aura'/'ICE', 'ICE:Arenson\'s Aura:arenson\'s aura').
card_rarity('arenson\'s aura'/'ICE', 'Common').
card_artist('arenson\'s aura'/'ICE', 'Nicola Leonard').
card_flavor_text('arenson\'s aura'/'ICE', '\"I have my faith, and I have my prayers. But if push comes to shove, I\'ve also got a little something extra.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('arenson\'s aura'/'ICE', '2663').

card_in_set('armor of faith', 'ICE').
card_original_type('armor of faith'/'ICE', 'Enchant Creature').
card_original_text('armor of faith'/'ICE', 'Target creature gets +1/+1.\n{W} Creature Armor of Faith enchants gets +0/+1 until end of turn.').
card_first_print('armor of faith', 'ICE').
card_image_name('armor of faith'/'ICE', 'armor of faith').
card_uid('armor of faith'/'ICE', 'ICE:Armor of Faith:armor of faith').
card_rarity('armor of faith'/'ICE', 'Common').
card_artist('armor of faith'/'ICE', 'Anson Maddocks').
card_flavor_text('armor of faith'/'ICE', '\"Keep your chainmail, warrior. I have my own form of protection.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('armor of faith'/'ICE', '2664').

card_in_set('arnjlot\'s ascent', 'ICE').
card_original_type('arnjlot\'s ascent'/'ICE', 'Enchantment').
card_original_text('arnjlot\'s ascent'/'ICE', 'Cumulative Upkeep: {U}\n{1}: Target creature gains flying until end of turn.').
card_first_print('arnjlot\'s ascent', 'ICE').
card_image_name('arnjlot\'s ascent'/'ICE', 'arnjlot\'s ascent').
card_uid('arnjlot\'s ascent'/'ICE', 'ICE:Arnjlot\'s Ascent:arnjlot\'s ascent').
card_rarity('arnjlot\'s ascent'/'ICE', 'Common').
card_artist('arnjlot\'s ascent'/'ICE', 'Drew Tucker').
card_flavor_text('arnjlot\'s ascent'/'ICE', '\"The dreams of a child fulfilled:\nThe wind on my brow,\nthe air \'neath my feet.\"\n—Arnjlot Olasson, Sky Mage').
card_multiverse_id('arnjlot\'s ascent'/'ICE', '2493').

card_in_set('ashen ghoul', 'ICE').
card_original_type('ashen ghoul'/'ICE', 'Summon — Ghoul').
card_original_text('ashen ghoul'/'ICE', 'Ashen Ghoul can attack the turn it comes into play.\n{B} Return Ashen Ghoul to play under your control. Use this ability only at the end of your upkeep and only if Ashen Ghoul is in your graveyard with at least three creature cards above it.').
card_first_print('ashen ghoul', 'ICE').
card_image_name('ashen ghoul'/'ICE', 'ashen ghoul').
card_uid('ashen ghoul'/'ICE', 'ICE:Ashen Ghoul:ashen ghoul').
card_rarity('ashen ghoul'/'ICE', 'Uncommon').
card_artist('ashen ghoul'/'ICE', 'Ron Spencer').
card_multiverse_id('ashen ghoul'/'ICE', '2438').

card_in_set('aurochs', 'ICE').
card_original_type('aurochs'/'ICE', 'Summon — Aurochs').
card_original_text('aurochs'/'ICE', 'Trample\nWhen attacking, Aurochs gets +1/+0 for each other Aurochs that attacks.').
card_first_print('aurochs', 'ICE').
card_image_name('aurochs'/'ICE', 'aurochs').
card_uid('aurochs'/'ICE', 'ICE:Aurochs:aurochs').
card_rarity('aurochs'/'ICE', 'Common').
card_artist('aurochs'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('aurochs'/'ICE', 'One Auroch may feed a village, but a herd will flatten it.').
card_multiverse_id('aurochs'/'ICE', '2549').

card_in_set('avalanche', 'ICE').
card_original_type('avalanche'/'ICE', 'Sorcery').
card_original_text('avalanche'/'ICE', 'Destroy X target snow-covered lands.').
card_first_print('avalanche', 'ICE').
card_image_name('avalanche'/'ICE', 'avalanche').
card_uid('avalanche'/'ICE', 'ICE:Avalanche:avalanche').
card_rarity('avalanche'/'ICE', 'Uncommon').
card_artist('avalanche'/'ICE', 'Brian Snõddy').
card_flavor_text('avalanche'/'ICE', '\"The pass was completely blocked by the avalanche; we\'re forced to turn back. Nevertheless, we\'ve gone farther and seen more than any before us.\"\n—Disa the Restless, journal entry').
card_multiverse_id('avalanche'/'ICE', '2607').

card_in_set('balduvian barbarians', 'ICE').
card_original_type('balduvian barbarians'/'ICE', 'Summon — Barbarians').
card_original_text('balduvian barbarians'/'ICE', '').
card_first_print('balduvian barbarians', 'ICE').
card_image_name('balduvian barbarians'/'ICE', 'balduvian barbarians').
card_uid('balduvian barbarians'/'ICE', 'ICE:Balduvian Barbarians:balduvian barbarians').
card_rarity('balduvian barbarians'/'ICE', 'Common').
card_artist('balduvian barbarians'/'ICE', 'Mark Poole').
card_flavor_text('balduvian barbarians'/'ICE', '\"Barbarian raids were a concern to those living in the northwest provinces, but the Skyknights never dealt with the problem in a systematic way. They thought of the Balduvians as an ‘amusing model\' of their forebears\' culture.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('balduvian barbarians'/'ICE', '2608').

card_in_set('balduvian bears', 'ICE').
card_original_type('balduvian bears'/'ICE', 'Summon — Bears').
card_original_text('balduvian bears'/'ICE', '').
card_first_print('balduvian bears', 'ICE').
card_image_name('balduvian bears'/'ICE', 'balduvian bears').
card_uid('balduvian bears'/'ICE', 'ICE:Balduvian Bears:balduvian bears').
card_rarity('balduvian bears'/'ICE', 'Common').
card_artist('balduvian bears'/'ICE', 'Quinton Hoover').
card_flavor_text('balduvian bears'/'ICE', '\"They\'re a hardy bunch, but I\'d still bet that they just slept through the worst of the cold times.\"\n—Disa the Restless, journal entry').
card_multiverse_id('balduvian bears'/'ICE', '2550').

card_in_set('balduvian conjurer', 'ICE').
card_original_type('balduvian conjurer'/'ICE', 'Summon — Wizard').
card_original_text('balduvian conjurer'/'ICE', '{T}: Target snow-covered land becomes a 2/2 creature until end of turn. The target still counts as land but cannot be tapped for mana if it came into play on a side this turn.').
card_first_print('balduvian conjurer', 'ICE').
card_image_name('balduvian conjurer'/'ICE', 'balduvian conjurer').
card_uid('balduvian conjurer'/'ICE', 'ICE:Balduvian Conjurer:balduvian conjurer').
card_rarity('balduvian conjurer'/'ICE', 'Uncommon').
card_artist('balduvian conjurer'/'ICE', 'Mark Tedin').
card_flavor_text('balduvian conjurer'/'ICE', '\"The very lands of Balduvia are alive.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('balduvian conjurer'/'ICE', '2494').

card_in_set('balduvian hydra', 'ICE').
card_original_type('balduvian hydra'/'ICE', 'Summon — Hydra').
card_original_text('balduvian hydra'/'ICE', 'When Balduvian Hydra comes into play, put X +1/+0 counters on it.\n{0}: Remove a +1/+0 counter from Balduvian Hydra to prevent 1 damage to Balduvian Hydra.\n{R}{R}{R}: Put a +1/+0 counter on Balduvian Hydra. Use this ability only during your upkeep.').
card_first_print('balduvian hydra', 'ICE').
card_image_name('balduvian hydra'/'ICE', 'balduvian hydra').
card_uid('balduvian hydra'/'ICE', 'ICE:Balduvian Hydra:balduvian hydra').
card_rarity('balduvian hydra'/'ICE', 'Rare').
card_artist('balduvian hydra'/'ICE', 'Melissa A. Benson').
card_multiverse_id('balduvian hydra'/'ICE', '2609').

card_in_set('balduvian shaman', 'ICE').
card_original_type('balduvian shaman'/'ICE', 'Summon — Cleric').
card_original_text('balduvian shaman'/'ICE', '{T}: Permanently change the text of target white enchantment you control that does not have cumulative upkeep by replacing all instances of one color word with another. For example, you may change \"Counters black spells\" to \"Counters blue spells.\" Balduvian Shaman cannot change mana symbols. That enchantment now has Cumulative Upkeep: {1}.').
card_first_print('balduvian shaman', 'ICE').
card_image_name('balduvian shaman'/'ICE', 'balduvian shaman').
card_uid('balduvian shaman'/'ICE', 'ICE:Balduvian Shaman:balduvian shaman').
card_rarity('balduvian shaman'/'ICE', 'Common').
card_artist('balduvian shaman'/'ICE', 'Quinton Hoover').
card_multiverse_id('balduvian shaman'/'ICE', '2495').

card_in_set('barbarian guides', 'ICE').
card_original_type('barbarian guides'/'ICE', 'Summon — Barbarians').
card_original_text('barbarian guides'/'ICE', '{2}{R},{T}: Target creature you control gains a snow-covered landwalk ability of your choice until end of turn. At end of turn, return that creature to its owner\'s hand.').
card_first_print('barbarian guides', 'ICE').
card_image_name('barbarian guides'/'ICE', 'barbarian guides').
card_uid('barbarian guides'/'ICE', 'ICE:Barbarian Guides:barbarian guides').
card_rarity('barbarian guides'/'ICE', 'Common').
card_artist('barbarian guides'/'ICE', 'Richard Thomas').
card_multiverse_id('barbarian guides'/'ICE', '2610').

card_in_set('barbed sextant', 'ICE').
card_original_type('barbed sextant'/'ICE', 'Artifact').
card_original_text('barbed sextant'/'ICE', '{1},{T}: Sacrifice Barbed Sextant to add one mana of any color to your mana pool. Play this ability as an interrupt. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('barbed sextant', 'ICE').
card_image_name('barbed sextant'/'ICE', 'barbed sextant').
card_uid('barbed sextant'/'ICE', 'ICE:Barbed Sextant:barbed sextant').
card_rarity('barbed sextant'/'ICE', 'Common').
card_artist('barbed sextant'/'ICE', 'Amy Weber').
card_multiverse_id('barbed sextant'/'ICE', '2398').

card_in_set('baton of morale', 'ICE').
card_original_type('baton of morale'/'ICE', 'Artifact').
card_original_text('baton of morale'/'ICE', '{2}: Target creature gains banding until end of turn.').
card_first_print('baton of morale', 'ICE').
card_image_name('baton of morale'/'ICE', 'baton of morale').
card_uid('baton of morale'/'ICE', 'ICE:Baton of Morale:baton of morale').
card_rarity('baton of morale'/'ICE', 'Uncommon').
card_artist('baton of morale'/'ICE', 'Douglas Shuler').
card_flavor_text('baton of morale'/'ICE', '\"The Goblins would kill to get ahold of this one.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('baton of morale'/'ICE', '2399').

card_in_set('battle cry', 'ICE').
card_original_type('battle cry'/'ICE', 'Instant').
card_original_text('battle cry'/'ICE', 'Untap all white creatures you control. Any creature that blocks this turn gets +0/+1 until end of turn.').
card_first_print('battle cry', 'ICE').
card_image_name('battle cry'/'ICE', 'battle cry').
card_uid('battle cry'/'ICE', 'ICE:Battle Cry:battle cry').
card_rarity('battle cry'/'ICE', 'Uncommon').
card_artist('battle cry'/'ICE', 'Douglas Shuler').
card_flavor_text('battle cry'/'ICE', '\"In the thick of battle, you must keep your wits about you. Yelling a lot helps, too.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('battle cry'/'ICE', '2665').

card_in_set('battle frenzy', 'ICE').
card_original_type('battle frenzy'/'ICE', 'Instant').
card_original_text('battle frenzy'/'ICE', 'All green creatures you control get +1/+1 until end of turn. All non-green creatures you control get +1/+0 until end of turn.').
card_first_print('battle frenzy', 'ICE').
card_image_name('battle frenzy'/'ICE', 'battle frenzy').
card_uid('battle frenzy'/'ICE', 'ICE:Battle Frenzy:battle frenzy').
card_rarity('battle frenzy'/'ICE', 'Common').
card_artist('battle frenzy'/'ICE', 'Brian Snõddy').
card_flavor_text('battle frenzy'/'ICE', '\"One day you, too, shall drink the blood of your foes. It is something to look forward to.\"\n—Toothlicker Harj, Orcish Captain').
card_multiverse_id('battle frenzy'/'ICE', '2611').

card_in_set('binding grasp', 'ICE').
card_original_type('binding grasp'/'ICE', 'Enchant Creature').
card_original_text('binding grasp'/'ICE', 'During your upkeep, pay {1}{U} or bury Binding Grasp.\nGain control of target creature; that creature gets +0/+1.').
card_first_print('binding grasp', 'ICE').
card_image_name('binding grasp'/'ICE', 'binding grasp').
card_uid('binding grasp'/'ICE', 'ICE:Binding Grasp:binding grasp').
card_rarity('binding grasp'/'ICE', 'Uncommon').
card_artist('binding grasp'/'ICE', 'Ruth Thompson').
card_flavor_text('binding grasp'/'ICE', '\"What I want, I take.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('binding grasp'/'ICE', '2496').

card_in_set('black scarab', 'ICE').
card_original_type('black scarab'/'ICE', 'Enchant Creature').
card_original_text('black scarab'/'ICE', 'Target creature gets +2/+2 as long as any opponent controls any black cards. That creature cannot be blocked by black creatures.').
card_first_print('black scarab', 'ICE').
card_image_name('black scarab'/'ICE', 'black scarab').
card_uid('black scarab'/'ICE', 'ICE:Black Scarab:black scarab').
card_rarity('black scarab'/'ICE', 'Uncommon').
card_artist('black scarab'/'ICE', 'Kaja Foglio').
card_multiverse_id('black scarab'/'ICE', '2666').

card_in_set('blessed wine', 'ICE').
card_original_type('blessed wine'/'ICE', 'Instant').
card_original_text('blessed wine'/'ICE', 'Gain 1 life. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('blessed wine', 'ICE').
card_image_name('blessed wine'/'ICE', 'blessed wine').
card_uid('blessed wine'/'ICE', 'ICE:Blessed Wine:blessed wine').
card_rarity('blessed wine'/'ICE', 'Common').
card_artist('blessed wine'/'ICE', 'Kaja Foglio').
card_flavor_text('blessed wine'/'ICE', '\"May the bounty of Kjeld enrich your days.\"\n—Halvor Arenson,\nKjeldoran Priest').
card_multiverse_id('blessed wine'/'ICE', '2667').

card_in_set('blinking spirit', 'ICE').
card_original_type('blinking spirit'/'ICE', 'Summon — Blinking Spirit').
card_original_text('blinking spirit'/'ICE', '{0}: Return Blinking Spirit to owner\'s hand.').
card_first_print('blinking spirit', 'ICE').
card_image_name('blinking spirit'/'ICE', 'blinking spirit').
card_uid('blinking spirit'/'ICE', 'ICE:Blinking Spirit:blinking spirit').
card_rarity('blinking spirit'/'ICE', 'Rare').
card_artist('blinking spirit'/'ICE', 'L. A. Williams').
card_flavor_text('blinking spirit'/'ICE', '\"Don\'t look at it! Maybe it\'ll go away!\"\n—Ib Halfheart, Goblin Tactician').
card_multiverse_id('blinking spirit'/'ICE', '2668').

card_in_set('blizzard', 'ICE').
card_original_type('blizzard'/'ICE', 'Enchantment').
card_original_text('blizzard'/'ICE', 'Cumulative Upkeep: {2}\nYou cannot cast Blizzard if you control no snow-covered lands. Creatures with flying do not untap during their controller\'s untap phase.').
card_first_print('blizzard', 'ICE').
card_image_name('blizzard'/'ICE', 'blizzard').
card_uid('blizzard'/'ICE', 'ICE:Blizzard:blizzard').
card_rarity('blizzard'/'ICE', 'Rare').
card_artist('blizzard'/'ICE', 'Anson Maddocks').
card_multiverse_id('blizzard'/'ICE', '2551').

card_in_set('blue scarab', 'ICE').
card_original_type('blue scarab'/'ICE', 'Enchant Creature').
card_original_text('blue scarab'/'ICE', 'Target creature gets +2/+2 as long as any opponent controls any blue cards. That creature cannot be blocked by blue creatures.').
card_first_print('blue scarab', 'ICE').
card_image_name('blue scarab'/'ICE', 'blue scarab').
card_uid('blue scarab'/'ICE', 'ICE:Blue Scarab:blue scarab').
card_rarity('blue scarab'/'ICE', 'Uncommon').
card_artist('blue scarab'/'ICE', 'Amy Weber').
card_multiverse_id('blue scarab'/'ICE', '2669').

card_in_set('bone shaman', 'ICE').
card_original_type('bone shaman'/'ICE', 'Summon — Giant').
card_original_text('bone shaman'/'ICE', '{B} Any creature damaged by Bone Shaman this turn cannot regenerate until end of turn.').
card_first_print('bone shaman', 'ICE').
card_image_name('bone shaman'/'ICE', 'bone shaman').
card_uid('bone shaman'/'ICE', 'ICE:Bone Shaman:bone shaman').
card_rarity('bone shaman'/'ICE', 'Common').
card_artist('bone shaman'/'ICE', 'Anson Maddocks').
card_flavor_text('bone shaman'/'ICE', 'Rattle me bones as I cross the floor\nAnd the bones in your grave will rattle no more.').
card_multiverse_id('bone shaman'/'ICE', '2612').

card_in_set('brainstorm', 'ICE').
card_original_type('brainstorm'/'ICE', 'Instant').
card_original_text('brainstorm'/'ICE', 'Draw three cards; then, take two cards from your hand and put them on top of your library in any order.').
card_first_print('brainstorm', 'ICE').
card_image_name('brainstorm'/'ICE', 'brainstorm').
card_uid('brainstorm'/'ICE', 'ICE:Brainstorm:brainstorm').
card_rarity('brainstorm'/'ICE', 'Common').
card_artist('brainstorm'/'ICE', 'Christopher Rush').
card_flavor_text('brainstorm'/'ICE', '\"I reeled from the blow, and then suddenly, I knew exactly what to do. Within moments, victory was mine.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('brainstorm'/'ICE', '2497').

card_in_set('brand of ill omen', 'ICE').
card_original_type('brand of ill omen'/'ICE', 'Enchant Creature').
card_original_text('brand of ill omen'/'ICE', 'Cumulative Upkeep: {R}\nTarget creature\'s controller cannot cast summon spells.').
card_first_print('brand of ill omen', 'ICE').
card_image_name('brand of ill omen'/'ICE', 'brand of ill omen').
card_uid('brand of ill omen'/'ICE', 'ICE:Brand of Ill Omen:brand of ill omen').
card_rarity('brand of ill omen'/'ICE', 'Rare').
card_artist('brand of ill omen'/'ICE', 'Rob Alexander').
card_flavor_text('brand of ill omen'/'ICE', '\"Let those who bear the brand receive no shelter, no kindness, and no comfort from our people.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('brand of ill omen'/'ICE', '2613').

card_in_set('breath of dreams', 'ICE').
card_original_type('breath of dreams'/'ICE', 'Enchantment').
card_original_text('breath of dreams'/'ICE', 'Cumulative Upkeep: {U}\nGreen creatures each require an additional Cumulative Upkeep: {1}.').
card_first_print('breath of dreams', 'ICE').
card_image_name('breath of dreams'/'ICE', 'breath of dreams').
card_uid('breath of dreams'/'ICE', 'ICE:Breath of Dreams:breath of dreams').
card_rarity('breath of dreams'/'ICE', 'Uncommon').
card_artist('breath of dreams'/'ICE', 'Phil Foglio').
card_flavor_text('breath of dreams'/'ICE', '\"Get up, blast you! They\'re attacking! Why are you all so slow?\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('breath of dreams'/'ICE', '2498').

card_in_set('brine shaman', 'ICE').
card_original_type('brine shaman'/'ICE', 'Summon — Cleric').
card_original_text('brine shaman'/'ICE', '{T}: Sacrifice a creature to give target creature +2/+2 until end of turn.\n{1}{U}{U}: Sacrifice a creature to counter target summon spell.').
card_first_print('brine shaman', 'ICE').
card_image_name('brine shaman'/'ICE', 'brine shaman').
card_uid('brine shaman'/'ICE', 'ICE:Brine Shaman:brine shaman').
card_rarity('brine shaman'/'ICE', 'Common').
card_artist('brine shaman'/'ICE', 'Cornelius Brudi').
card_flavor_text('brine shaman'/'ICE', '\"The Shamans of Marit Lage do her bidding in secret, but they do it gladly.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('brine shaman'/'ICE', '2439').

card_in_set('brown ouphe', 'ICE').
card_original_type('brown ouphe'/'ICE', 'Summon — Ouphe').
card_original_text('brown ouphe'/'ICE', '{1}{G},{T}: Counter target artifact ability requiring an activation cost. Play this ability as an interrupt.').
card_first_print('brown ouphe', 'ICE').
card_image_name('brown ouphe'/'ICE', 'brown ouphe').
card_uid('brown ouphe'/'ICE', 'ICE:Brown Ouphe:brown ouphe').
card_rarity('brown ouphe'/'ICE', 'Common').
card_artist('brown ouphe'/'ICE', 'Daniel Gelon').
card_flavor_text('brown ouphe'/'ICE', '\"Ouphes love trinkets and love to take them apart. I only wish they wouldn\'t do so with the magical ones.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('brown ouphe'/'ICE', '2552').

card_in_set('brushland', 'ICE').
card_original_type('brushland'/'ICE', 'Land').
card_original_text('brushland'/'ICE', '{T}: Add {1} to your mana pool.\n{T}: Add {W} to your mana pool. Brushland deals 1 damage to you.\n{T}: Add {G} to your mana pool. Brushland deals 1 damage to you.').
card_first_print('brushland', 'ICE').
card_image_name('brushland'/'ICE', 'brushland').
card_uid('brushland'/'ICE', 'ICE:Brushland:brushland').
card_rarity('brushland'/'ICE', 'Rare').
card_artist('brushland'/'ICE', 'Bryon Wackwitz').
card_multiverse_id('brushland'/'ICE', '2751').

card_in_set('burnt offering', 'ICE').
card_original_type('burnt offering'/'ICE', 'Interrupt').
card_original_text('burnt offering'/'ICE', 'Sacrifice a creature to add that creature\'s casting cost in any combination of red and/or black mana to your mana pool.').
card_first_print('burnt offering', 'ICE').
card_image_name('burnt offering'/'ICE', 'burnt offering').
card_uid('burnt offering'/'ICE', 'ICE:Burnt Offering:burnt offering').
card_rarity('burnt offering'/'ICE', 'Common').
card_artist('burnt offering'/'ICE', 'Daniel Gelon').
card_flavor_text('burnt offering'/'ICE', '\"My foes serve me twice: in dying, and in death.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('burnt offering'/'ICE', '2440').

card_in_set('call to arms', 'ICE').
card_original_type('call to arms'/'ICE', 'Enchantment').
card_original_text('call to arms'/'ICE', 'Choose a color. As long as target opponent controls more cards of that color than any other color, all white creatures get +1/+1. If at any time that opponent does not control more cards of that color than any other color, bury Call to Arms.').
card_first_print('call to arms', 'ICE').
card_image_name('call to arms'/'ICE', 'call to arms').
card_uid('call to arms'/'ICE', 'ICE:Call to Arms:call to arms').
card_rarity('call to arms'/'ICE', 'Rare').
card_artist('call to arms'/'ICE', 'Randy Gallegos').
card_multiverse_id('call to arms'/'ICE', '2670').

card_in_set('caribou range', 'ICE').
card_original_type('caribou range'/'ICE', 'Enchant Land').
card_original_text('caribou range'/'ICE', 'When Caribou Range comes into play, choose target land you control.\n{W}{W}: Tap land Caribou Range enchants to put a Caribou token into play. Treat this token as a 0/1 white creature.\n{0}: Sacrifice a Caribou token to gain 1 life.').
card_first_print('caribou range', 'ICE').
card_image_name('caribou range'/'ICE', 'caribou range').
card_uid('caribou range'/'ICE', 'ICE:Caribou Range:caribou range').
card_rarity('caribou range'/'ICE', 'Rare').
card_artist('caribou range'/'ICE', 'Ruth Thompson').
card_multiverse_id('caribou range'/'ICE', '2671').

card_in_set('celestial sword', 'ICE').
card_original_type('celestial sword'/'ICE', 'Artifact').
card_original_text('celestial sword'/'ICE', '{3},{T}: Target creature you control gets +3/+3 until end of turn. At end of turn, bury that creature.').
card_first_print('celestial sword', 'ICE').
card_image_name('celestial sword'/'ICE', 'celestial sword').
card_uid('celestial sword'/'ICE', 'ICE:Celestial Sword:celestial sword').
card_rarity('celestial sword'/'ICE', 'Rare').
card_artist('celestial sword'/'ICE', 'Amy Weber').
card_flavor_text('celestial sword'/'ICE', '\"So great is its power, only the chosen can wield it and live.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('celestial sword'/'ICE', '2400').

card_in_set('centaur archer', 'ICE').
card_original_type('centaur archer'/'ICE', 'Summon — Centaur').
card_original_text('centaur archer'/'ICE', '{T}: Centaur Archer deals 1 damage to target creature with flying.').
card_first_print('centaur archer', 'ICE').
card_image_name('centaur archer'/'ICE', 'centaur archer').
card_uid('centaur archer'/'ICE', 'ICE:Centaur Archer:centaur archer').
card_rarity('centaur archer'/'ICE', 'Uncommon').
card_artist('centaur archer'/'ICE', 'Melissa A. Benson').
card_flavor_text('centaur archer'/'ICE', '\"Centaurs will kill our Aesthir if they can; they\'ve always been enemies. Destroy the horse-people on sight.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('centaur archer'/'ICE', '2718').

card_in_set('chaos lord', 'ICE').
card_original_type('chaos lord'/'ICE', 'Summon — Lord').
card_original_text('chaos lord'/'ICE', 'First strike\nChaos Lord can attack the first turn it comes into play on a side, except the turn it first comes into play. During your upkeep, count the number of permanents. If that number is even, target opponent gains control of Chaos Lord.').
card_first_print('chaos lord', 'ICE').
card_image_name('chaos lord'/'ICE', 'chaos lord').
card_uid('chaos lord'/'ICE', 'ICE:Chaos Lord:chaos lord').
card_rarity('chaos lord'/'ICE', 'Rare').
card_artist('chaos lord'/'ICE', 'Brian Snõddy').
card_multiverse_id('chaos lord'/'ICE', '2614').

card_in_set('chaos moon', 'ICE').
card_original_type('chaos moon'/'ICE', 'Enchantment').
card_original_text('chaos moon'/'ICE', 'During each player\'s upkeep, count the number of permanents. If that number is odd, all red creatures get +1/+1 and mountains produce an additional {R} when tapped for mana until end of turn. If the number is even, all red creatures get -1/-1 and mountains produce colorless mana instead of their normal mana until end of turn.').
card_first_print('chaos moon', 'ICE').
card_image_name('chaos moon'/'ICE', 'chaos moon').
card_uid('chaos moon'/'ICE', 'ICE:Chaos Moon:chaos moon').
card_rarity('chaos moon'/'ICE', 'Rare').
card_artist('chaos moon'/'ICE', 'Drew Tucker').
card_multiverse_id('chaos moon'/'ICE', '2615').

card_in_set('chromatic armor', 'ICE').
card_original_type('chromatic armor'/'ICE', 'Enchant Creature').
card_original_text('chromatic armor'/'ICE', 'When Chromatic Armor comes into play, put a sleight counter on it and choose a color. Any damage dealt to target creature by a source of that color is reduced to 0.\n{X} Put a sleight counter on Chromatic Armor and change the color that it protects against. X is equal to the number of sleight counters on Chromatic Armor.').
card_first_print('chromatic armor', 'ICE').
card_image_name('chromatic armor'/'ICE', 'chromatic armor').
card_uid('chromatic armor'/'ICE', 'ICE:Chromatic Armor:chromatic armor').
card_rarity('chromatic armor'/'ICE', 'Rare').
card_artist('chromatic armor'/'ICE', 'Mark Poole').
card_multiverse_id('chromatic armor'/'ICE', '2719').

card_in_set('chub toad', 'ICE').
card_original_type('chub toad'/'ICE', 'Summon — Toad').
card_original_text('chub toad'/'ICE', 'Chub Toad gets +2/+2 until end of turn when blocking or blocked.').
card_first_print('chub toad', 'ICE').
card_image_name('chub toad'/'ICE', 'chub toad').
card_uid('chub toad'/'ICE', 'ICE:Chub Toad:chub toad').
card_rarity('chub toad'/'ICE', 'Common').
card_artist('chub toad'/'ICE', 'Daniel Gelon').
card_flavor_text('chub toad'/'ICE', 'Chub Toad, Chub Toad\nAt the door.\nRun away quick\nOr you\'ll run no more.\n—Traditional children\'s rhyme').
card_multiverse_id('chub toad'/'ICE', '2553').

card_in_set('circle of protection: black', 'ICE').
card_original_type('circle of protection: black'/'ICE', 'Enchantment').
card_original_text('circle of protection: black'/'ICE', '{1}: Prevent all damage against you from one black source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: black'/'ICE', 'circle of protection black').
card_uid('circle of protection: black'/'ICE', 'ICE:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'ICE', 'Common').
card_artist('circle of protection: black'/'ICE', 'Sandra Everingham').
card_multiverse_id('circle of protection: black'/'ICE', '2672').

card_in_set('circle of protection: blue', 'ICE').
card_original_type('circle of protection: blue'/'ICE', 'Enchantment').
card_original_text('circle of protection: blue'/'ICE', '{1}: Prevent all damage against you from one blue source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: blue'/'ICE', 'circle of protection blue').
card_uid('circle of protection: blue'/'ICE', 'ICE:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'ICE', 'Common').
card_artist('circle of protection: blue'/'ICE', 'Pete Venters').
card_multiverse_id('circle of protection: blue'/'ICE', '2673').

card_in_set('circle of protection: green', 'ICE').
card_original_type('circle of protection: green'/'ICE', 'Enchantment').
card_original_text('circle of protection: green'/'ICE', '{1}: Prevent all damage against you from one green source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: green'/'ICE', 'circle of protection green').
card_uid('circle of protection: green'/'ICE', 'ICE:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'ICE', 'Common').
card_artist('circle of protection: green'/'ICE', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'ICE', '2674').

card_in_set('circle of protection: red', 'ICE').
card_original_type('circle of protection: red'/'ICE', 'Enchantment').
card_original_text('circle of protection: red'/'ICE', '{1}: Prevent all damage against you from one red source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: red'/'ICE', 'circle of protection red').
card_uid('circle of protection: red'/'ICE', 'ICE:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'ICE', 'Common').
card_artist('circle of protection: red'/'ICE', 'Pete Venters').
card_multiverse_id('circle of protection: red'/'ICE', '2675').

card_in_set('circle of protection: white', 'ICE').
card_original_type('circle of protection: white'/'ICE', 'Enchantment').
card_original_text('circle of protection: white'/'ICE', '{1}: Prevent all damage against you from one white source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: white'/'ICE', 'circle of protection white').
card_uid('circle of protection: white'/'ICE', 'ICE:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'ICE', 'Common').
card_artist('circle of protection: white'/'ICE', 'Sandra Everingham').
card_multiverse_id('circle of protection: white'/'ICE', '2676').

card_in_set('clairvoyance', 'ICE').
card_original_type('clairvoyance'/'ICE', 'Instant').
card_original_text('clairvoyance'/'ICE', 'Look at target player\'s hand. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('clairvoyance', 'ICE').
card_image_name('clairvoyance'/'ICE', 'clairvoyance').
card_uid('clairvoyance'/'ICE', 'ICE:Clairvoyance:clairvoyance').
card_rarity('clairvoyance'/'ICE', 'Common').
card_artist('clairvoyance'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('clairvoyance'/'ICE', '\"As we followed the ice wall, we had the distinct feeling of being watched. Many of the party wanted to turn back.\"\n—Disa the Restless, journal entry').
card_multiverse_id('clairvoyance'/'ICE', '2499').

card_in_set('cloak of confusion', 'ICE').
card_original_type('cloak of confusion'/'ICE', 'Enchant Creature').
card_original_text('cloak of confusion'/'ICE', 'If target creature you control attacks and is not blocked, you may choose to have it deal no damage to defending player this turn. If you do so, that player discards a card at random from his or her hand. Ignore this ability if that player has no cards in hand.').
card_first_print('cloak of confusion', 'ICE').
card_image_name('cloak of confusion'/'ICE', 'cloak of confusion').
card_uid('cloak of confusion'/'ICE', 'ICE:Cloak of Confusion:cloak of confusion').
card_rarity('cloak of confusion'/'ICE', 'Common').
card_artist('cloak of confusion'/'ICE', 'Margaret Organ-Kean').
card_multiverse_id('cloak of confusion'/'ICE', '2441').

card_in_set('cold snap', 'ICE').
card_original_type('cold snap'/'ICE', 'Enchantment').
card_original_text('cold snap'/'ICE', 'Cumulative Upkeep: {2}\nDuring each player\'s upkeep, Cold Snap deals 1 damage to that player for each snow-covered land he or she controls.').
card_first_print('cold snap', 'ICE').
card_image_name('cold snap'/'ICE', 'cold snap').
card_uid('cold snap'/'ICE', 'ICE:Cold Snap:cold snap').
card_rarity('cold snap'/'ICE', 'Uncommon').
card_artist('cold snap'/'ICE', 'Randy Gallegos').
card_flavor_text('cold snap'/'ICE', 'Who knows from whence the winter comes?').
card_multiverse_id('cold snap'/'ICE', '2677').

card_in_set('conquer', 'ICE').
card_original_type('conquer'/'ICE', 'Enchant Land').
card_original_text('conquer'/'ICE', 'Gain control of target land.').
card_first_print('conquer', 'ICE').
card_image_name('conquer'/'ICE', 'conquer').
card_uid('conquer'/'ICE', 'ICE:Conquer:conquer').
card_rarity('conquer'/'ICE', 'Uncommon').
card_artist('conquer'/'ICE', 'Randy Gallegos').
card_flavor_text('conquer'/'ICE', '\"Why do we trade with those despicable Elves? You don\'t live in forests, you burn them!\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('conquer'/'ICE', '2616').

card_in_set('cooperation', 'ICE').
card_original_type('cooperation'/'ICE', 'Enchant Creature').
card_original_text('cooperation'/'ICE', 'Target creature gains banding.').
card_first_print('cooperation', 'ICE').
card_image_name('cooperation'/'ICE', 'cooperation').
card_uid('cooperation'/'ICE', 'ICE:Cooperation:cooperation').
card_rarity('cooperation'/'ICE', 'Common').
card_artist('cooperation'/'ICE', 'Phil Foglio').
card_flavor_text('cooperation'/'ICE', '\"The Elves train our healers, and we keep the Orcs at bay. Most Elvish bargains aren\'t as fair.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('cooperation'/'ICE', '2678').

card_in_set('counterspell', 'ICE').
card_original_type('counterspell'/'ICE', 'Interrupt').
card_original_text('counterspell'/'ICE', 'Counter target spell.').
card_image_name('counterspell'/'ICE', 'counterspell').
card_uid('counterspell'/'ICE', 'ICE:Counterspell:counterspell').
card_rarity('counterspell'/'ICE', 'Common').
card_artist('counterspell'/'ICE', 'L. A. Williams').
card_flavor_text('counterspell'/'ICE', '\"The duel was going badly for me, and Zur thought I was finished. He boasted that he would eat my soul—but all he ate were his words.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('counterspell'/'ICE', '2500').

card_in_set('crown of the ages', 'ICE').
card_original_type('crown of the ages'/'ICE', 'Artifact').
card_original_text('crown of the ages'/'ICE', '{4},{T}: Switch target enchantment from one creature to another; the enchantment\'s new target must be legal. The controller of the enchantment does not change. Treat the enchantment as though it were just cast on the new target.').
card_first_print('crown of the ages', 'ICE').
card_image_name('crown of the ages'/'ICE', 'crown of the ages').
card_uid('crown of the ages'/'ICE', 'ICE:Crown of the Ages:crown of the ages').
card_rarity('crown of the ages'/'ICE', 'Rare').
card_artist('crown of the ages'/'ICE', 'Dan Frazier').
card_multiverse_id('crown of the ages'/'ICE', '2401').

card_in_set('curse of marit lage', 'ICE').
card_original_type('curse of marit lage'/'ICE', 'Enchantment').
card_original_text('curse of marit lage'/'ICE', 'When Curse of Marit Lage comes into play, tap all islands. Islands do not untap during their controller\'s untap phase.').
card_first_print('curse of marit lage', 'ICE').
card_image_name('curse of marit lage'/'ICE', 'curse of marit lage').
card_uid('curse of marit lage'/'ICE', 'ICE:Curse of Marit Lage:curse of marit lage').
card_rarity('curse of marit lage'/'ICE', 'Rare').
card_artist('curse of marit lage'/'ICE', 'Amy Weber').
card_flavor_text('curse of marit lage'/'ICE', '\"Our world has not felt her thundering steps in lifetimes, but Marit Lage\'s presence is still with us.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('curse of marit lage'/'ICE', '2617').

card_in_set('dance of the dead', 'ICE').
card_original_type('dance of the dead'/'ICE', 'Enchant Dead Creature').
card_original_text('dance of the dead'/'ICE', 'Take target creature from any graveyard and put it directly into play under your control, tapped, with +1/+1. Treat that creature as though it were just summoned. The creature does not untap during its controller\'s untap phase. At the end of his or her upkeep, its controller may pay an additional {1}{B} to untap it. If Dance of the Dead is removed, bury the creature in its owner\'s graveyard.').
card_first_print('dance of the dead', 'ICE').
card_image_name('dance of the dead'/'ICE', 'dance of the dead').
card_uid('dance of the dead'/'ICE', 'ICE:Dance of the Dead:dance of the dead').
card_rarity('dance of the dead'/'ICE', 'Uncommon').
card_artist('dance of the dead'/'ICE', 'Randy Gallegos').
card_multiverse_id('dance of the dead'/'ICE', '2442').

card_in_set('dark banishing', 'ICE').
card_original_type('dark banishing'/'ICE', 'Instant').
card_original_text('dark banishing'/'ICE', 'Bury target non-black creature.').
card_first_print('dark banishing', 'ICE').
card_image_name('dark banishing'/'ICE', 'dark banishing').
card_uid('dark banishing'/'ICE', 'ICE:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'ICE', 'Common').
card_artist('dark banishing'/'ICE', 'Drew Tucker').
card_flavor_text('dark banishing'/'ICE', '\"Will not the mountains quake and hills melt at the coming of the darkness? Share this vision with your enemies, Lim-Dûl, and they shall wither.\"\n—Leshrac, Walker of Night').
card_multiverse_id('dark banishing'/'ICE', '2443').

card_in_set('dark ritual', 'ICE').
card_original_type('dark ritual'/'ICE', 'Interrupt').
card_original_text('dark ritual'/'ICE', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'ICE', 'dark ritual').
card_uid('dark ritual'/'ICE', 'ICE:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'ICE', 'Common').
card_artist('dark ritual'/'ICE', 'Justin Hampton').
card_flavor_text('dark ritual'/'ICE', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('dark ritual'/'ICE', '2444').

card_in_set('death ward', 'ICE').
card_original_type('death ward'/'ICE', 'Instant').
card_original_text('death ward'/'ICE', 'Regenerate target creature.').
card_image_name('death ward'/'ICE', 'death ward').
card_uid('death ward'/'ICE', 'ICE:Death Ward:death ward').
card_rarity('death ward'/'ICE', 'Common').
card_artist('death ward'/'ICE', 'Harold McNeill').
card_flavor_text('death ward'/'ICE', '\"Sometimes, a soul is not ready to complete its journey to the next world.\"\n—Halvor Arenson,\nKjeldoran Priest').
card_multiverse_id('death ward'/'ICE', '2679').

card_in_set('deflection', 'ICE').
card_original_type('deflection'/'ICE', 'Interrupt').
card_original_text('deflection'/'ICE', 'Target spell, which must have a single target, targets a new legal target of your choice.').
card_first_print('deflection', 'ICE').
card_image_name('deflection'/'ICE', 'deflection').
card_uid('deflection'/'ICE', 'ICE:Deflection:deflection').
card_rarity('deflection'/'ICE', 'Rare').
card_artist('deflection'/'ICE', 'Mike Raabe').
card_flavor_text('deflection'/'ICE', 'Up and down,\nover and through,\nback around—\nthe joke\'s on you.').
card_multiverse_id('deflection'/'ICE', '2501').

card_in_set('demonic consultation', 'ICE').
card_original_type('demonic consultation'/'ICE', 'Instant').
card_original_text('demonic consultation'/'ICE', 'Name a card. Remove the top six cards of your library from the game and reveal the next card to all players. If it is the card named, put it into your hand. If not, remove that card from the game and continue revealing the top card of your library and removing it from the game until the named card appears.').
card_first_print('demonic consultation', 'ICE').
card_image_name('demonic consultation'/'ICE', 'demonic consultation').
card_uid('demonic consultation'/'ICE', 'ICE:Demonic Consultation:demonic consultation').
card_rarity('demonic consultation'/'ICE', 'Uncommon').
card_artist('demonic consultation'/'ICE', 'Rob Alexander').
card_multiverse_id('demonic consultation'/'ICE', '2445').

card_in_set('despotic scepter', 'ICE').
card_original_type('despotic scepter'/'ICE', 'Artifact').
card_original_text('despotic scepter'/'ICE', '{T}: Bury target permanent you own.').
card_first_print('despotic scepter', 'ICE').
card_image_name('despotic scepter'/'ICE', 'despotic scepter').
card_uid('despotic scepter'/'ICE', 'ICE:Despotic Scepter:despotic scepter').
card_rarity('despotic scepter'/'ICE', 'Rare').
card_artist('despotic scepter'/'ICE', 'Richard Thomas').
card_flavor_text('despotic scepter'/'ICE', '\"We were not meant to have such terrible things. They should be left where we found them, if not destroyed!\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('despotic scepter'/'ICE', '2402').

card_in_set('diabolic vision', 'ICE').
card_original_type('diabolic vision'/'ICE', 'Sorcery').
card_original_text('diabolic vision'/'ICE', 'Look at the top five cards of your library and put one of them into your hand. Put the remaining four on top of your library in any order.').
card_first_print('diabolic vision', 'ICE').
card_image_name('diabolic vision'/'ICE', 'diabolic vision').
card_uid('diabolic vision'/'ICE', 'ICE:Diabolic Vision:diabolic vision').
card_rarity('diabolic vision'/'ICE', 'Uncommon').
card_artist('diabolic vision'/'ICE', 'Anthony Waters').
card_flavor_text('diabolic vision'/'ICE', '\"I have seen the true path. I will not warm myself by the fire—I will become the flame.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('diabolic vision'/'ICE', '2720').

card_in_set('dire wolves', 'ICE').
card_original_type('dire wolves'/'ICE', 'Summon — Wolves').
card_original_text('dire wolves'/'ICE', 'Gains banding if you control any plains.').
card_first_print('dire wolves', 'ICE').
card_image_name('dire wolves'/'ICE', 'dire wolves').
card_uid('dire wolves'/'ICE', 'ICE:Dire Wolves:dire wolves').
card_rarity('dire wolves'/'ICE', 'Common').
card_artist('dire wolves'/'ICE', 'Ron Spencer').
card_flavor_text('dire wolves'/'ICE', '\"It\'s amazing how scared a city kid can get at a dog. Now, of course, I\'d cross Terisiare alone, and keep no watch if I had a pack of greys hanging on my flanks as I went.\"\n—Oddveig Ulfsson, caravan scout').
card_multiverse_id('dire wolves'/'ICE', '2554').

card_in_set('disenchant', 'ICE').
card_original_type('disenchant'/'ICE', 'Instant').
card_original_text('disenchant'/'ICE', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'ICE', 'disenchant').
card_uid('disenchant'/'ICE', 'ICE:Disenchant:disenchant').
card_rarity('disenchant'/'ICE', 'Common').
card_artist('disenchant'/'ICE', 'Brian Snõddy').
card_flavor_text('disenchant'/'ICE', '\"I implore you not to forget the horrors of the past. You would have us start the Brothers\' War anew!\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('disenchant'/'ICE', '2680').

card_in_set('dread wight', 'ICE').
card_original_type('dread wight'/'ICE', 'Summon — Wight').
card_original_text('dread wight'/'ICE', 'At end of combat, put a paralyzation counter on any creature blocking or blocked by Dread Wight and tap that creature. As long as the creature has a paralyzation counter on it, it does not untap during its controller\'s untap phase. As a non-interrupt fast effect, the creature\'s controller may pay {4} to remove a paralyzation counter.').
card_first_print('dread wight', 'ICE').
card_image_name('dread wight'/'ICE', 'dread wight').
card_uid('dread wight'/'ICE', 'ICE:Dread Wight:dread wight').
card_rarity('dread wight'/'ICE', 'Rare').
card_artist('dread wight'/'ICE', 'Daniel Gelon').
card_multiverse_id('dread wight'/'ICE', '2446').

card_in_set('dreams of the dead', 'ICE').
card_original_type('dreams of the dead'/'ICE', 'Enchantment').
card_original_text('dreams of the dead'/'ICE', '{1}{U} Take target white or black creature from your graveyard and put it directly into play as though it were just summoned. That creature now requires an additional Cumulative Upkeep: {2}. If the creature leaves play, remove it from the game.').
card_first_print('dreams of the dead', 'ICE').
card_image_name('dreams of the dead'/'ICE', 'dreams of the dead').
card_uid('dreams of the dead'/'ICE', 'ICE:Dreams of the Dead:dreams of the dead').
card_rarity('dreams of the dead'/'ICE', 'Uncommon').
card_artist('dreams of the dead'/'ICE', 'Heather Hudson').
card_multiverse_id('dreams of the dead'/'ICE', '2502').

card_in_set('drift of the dead', 'ICE').
card_original_type('drift of the dead'/'ICE', 'Summon — Wall').
card_original_text('drift of the dead'/'ICE', 'Drift of the Dead has power and toughness each equal to the number of snow-covered lands you control.').
card_first_print('drift of the dead', 'ICE').
card_image_name('drift of the dead'/'ICE', 'drift of the dead').
card_uid('drift of the dead'/'ICE', 'ICE:Drift of the Dead:drift of the dead').
card_rarity('drift of the dead'/'ICE', 'Uncommon').
card_artist('drift of the dead'/'ICE', 'Brian Snõddy').
card_flavor_text('drift of the dead'/'ICE', '\"Take their dead, and entomb them in the snow. Risen, they shall serve a new purpose.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('drift of the dead'/'ICE', '2447').

card_in_set('drought', 'ICE').
card_original_type('drought'/'ICE', 'Enchantment').
card_original_text('drought'/'ICE', 'During your upkeep, pay {W}{W} or destroy Drought. Before a spell that requires {B} as part of its casting cost may be cast or an ability that requires {B} as part of its activation cost may be played, the controller of that spell or ability sacrifices a swamp for each {B} in the spell\'s casting cost or the ability\'s activation cost.').
card_first_print('drought', 'ICE').
card_image_name('drought'/'ICE', 'drought').
card_uid('drought'/'ICE', 'ICE:Drought:drought').
card_rarity('drought'/'ICE', 'Uncommon').
card_artist('drought'/'ICE', 'NéNé Thomas').
card_multiverse_id('drought'/'ICE', '2681').

card_in_set('dwarven armory', 'ICE').
card_original_type('dwarven armory'/'ICE', 'Enchantment').
card_original_text('dwarven armory'/'ICE', '{2}: Sacrifice a land to put a +2/+2 counter on target creature. Use this ability only during upkeep.').
card_first_print('dwarven armory', 'ICE').
card_image_name('dwarven armory'/'ICE', 'dwarven armory').
card_uid('dwarven armory'/'ICE', 'ICE:Dwarven Armory:dwarven armory').
card_rarity('dwarven armory'/'ICE', 'Rare').
card_artist('dwarven armory'/'ICE', 'Richard Thomas').
card_flavor_text('dwarven armory'/'ICE', '\"Few of us left, now. Confounded Yeti have opened all the vents. We\'d best hide here with the weapons.\"\n—Perena Deepcutter,\nDwarven Armorer').
card_multiverse_id('dwarven armory'/'ICE', '2618').

card_in_set('earthlink', 'ICE').
card_original_type('earthlink'/'ICE', 'Enchantment').
card_original_text('earthlink'/'ICE', 'During your upkeep, pay {2} or bury Earthlink.\nWhenever a creature is put into the graveyard from play, that creature\'s controller sacrifices a land. Ignore this effect if that player controls no lands.').
card_first_print('earthlink', 'ICE').
card_image_name('earthlink'/'ICE', 'earthlink').
card_uid('earthlink'/'ICE', 'ICE:Earthlink:earthlink').
card_rarity('earthlink'/'ICE', 'Rare').
card_artist('earthlink'/'ICE', 'Richard Kane Ferguson').
card_multiverse_id('earthlink'/'ICE', '2721').

card_in_set('earthlore', 'ICE').
card_original_type('earthlore'/'ICE', 'Enchant Land').
card_original_text('earthlore'/'ICE', 'When Earthlore comes into play, choose target land you control.\n{0}: Tap land Earthlore enchants to give target blocking creature +1/+2 until end of turn.').
card_first_print('earthlore', 'ICE').
card_image_name('earthlore'/'ICE', 'earthlore').
card_uid('earthlore'/'ICE', 'ICE:Earthlore:earthlore').
card_rarity('earthlore'/'ICE', 'Common').
card_artist('earthlore'/'ICE', 'Drew Tucker').
card_flavor_text('earthlore'/'ICE', '\"Even the ground is our friend.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('earthlore'/'ICE', '2555').

card_in_set('elder druid', 'ICE').
card_original_type('elder druid'/'ICE', 'Summon — Cleric').
card_original_text('elder druid'/'ICE', '{3}{G},{T}: Tap or untap target artifact, creature, or land.').
card_first_print('elder druid', 'ICE').
card_image_name('elder druid'/'ICE', 'elder druid').
card_uid('elder druid'/'ICE', 'ICE:Elder Druid:elder druid').
card_rarity('elder druid'/'ICE', 'Rare').
card_artist('elder druid'/'ICE', 'Richard Kane Ferguson').
card_flavor_text('elder druid'/'ICE', '\"I am older than any Elder Druid before me, and still no child has been born with the Mark to take my place. When will the wheel turn?\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('elder druid'/'ICE', '2556').

card_in_set('elemental augury', 'ICE').
card_original_type('elemental augury'/'ICE', 'Enchantment').
card_original_text('elemental augury'/'ICE', '{3}: Look at the top three cards of target player\'s library. Put them on the top of that player\'s library in any order.').
card_first_print('elemental augury', 'ICE').
card_image_name('elemental augury'/'ICE', 'elemental augury').
card_uid('elemental augury'/'ICE', 'ICE:Elemental Augury:elemental augury').
card_rarity('elemental augury'/'ICE', 'Rare').
card_artist('elemental augury'/'ICE', 'Anthony Waters').
card_flavor_text('elemental augury'/'ICE', '\"It is the changing of perception that is important.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('elemental augury'/'ICE', '2722').

card_in_set('elkin bottle', 'ICE').
card_original_type('elkin bottle'/'ICE', 'Artifact').
card_original_text('elkin bottle'/'ICE', '{3},{T}: Take the top card from your library and place it face up in front of you. You may play that card as though it were in your hand; if you do not play it by your next upkeep, remove it from the game.').
card_first_print('elkin bottle', 'ICE').
card_image_name('elkin bottle'/'ICE', 'elkin bottle').
card_uid('elkin bottle'/'ICE', 'ICE:Elkin Bottle:elkin bottle').
card_rarity('elkin bottle'/'ICE', 'Rare').
card_artist('elkin bottle'/'ICE', 'Quinton Hoover').
card_multiverse_id('elkin bottle'/'ICE', '2403').

card_in_set('elvish healer', 'ICE').
card_original_type('elvish healer'/'ICE', 'Summon — Cleric').
card_original_text('elvish healer'/'ICE', '{T}: Prevent 1 damage to any non-green creature or any player or up to 2 damage to any green creature.').
card_first_print('elvish healer', 'ICE').
card_image_name('elvish healer'/'ICE', 'elvish healer').
card_uid('elvish healer'/'ICE', 'ICE:Elvish Healer:elvish healer').
card_rarity('elvish healer'/'ICE', 'Common').
card_artist('elvish healer'/'ICE', 'Rick Emond').
card_flavor_text('elvish healer'/'ICE', '\"The Kjeldorans keep the Orcs at bay and we train their healers. Most human bargains aren\'t as fair.\"\n—Laina of the Elvish Council').
card_multiverse_id('elvish healer'/'ICE', '2682').

card_in_set('enduring renewal', 'ICE').
card_original_type('enduring renewal'/'ICE', 'Enchantment').
card_original_text('enduring renewal'/'ICE', 'Play with the cards in your hand face up on the table. If you draw a creature card from your library, discard it. Whenever a creature goes to your graveyard from play, put that creature into your hand.').
card_first_print('enduring renewal', 'ICE').
card_image_name('enduring renewal'/'ICE', 'enduring renewal').
card_uid('enduring renewal'/'ICE', 'ICE:Enduring Renewal:enduring renewal').
card_rarity('enduring renewal'/'ICE', 'Rare').
card_artist('enduring renewal'/'ICE', 'Harold McNeill').
card_flavor_text('enduring renewal'/'ICE', 'That which lasts longest serves best.').
card_multiverse_id('enduring renewal'/'ICE', '2683').

card_in_set('energy storm', 'ICE').
card_original_type('energy storm'/'ICE', 'Enchantment').
card_original_text('energy storm'/'ICE', 'Cumulative Upkeep: {1}\nDamage dealt by instants, interrupts, and sorceries is reduced to 0.\nCreatures with flying do not untap during their controller\'s untap phase.').
card_first_print('energy storm', 'ICE').
card_image_name('energy storm'/'ICE', 'energy storm').
card_uid('energy storm'/'ICE', 'ICE:Energy Storm:energy storm').
card_rarity('energy storm'/'ICE', 'Rare').
card_artist('energy storm'/'ICE', 'Sandra Everingham').
card_multiverse_id('energy storm'/'ICE', '2684').

card_in_set('enervate', 'ICE').
card_original_type('enervate'/'ICE', 'Instant').
card_original_text('enervate'/'ICE', 'Tap target artifact, creature, or land. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('enervate', 'ICE').
card_image_name('enervate'/'ICE', 'enervate').
card_uid('enervate'/'ICE', 'ICE:Enervate:enervate').
card_rarity('enervate'/'ICE', 'Common').
card_artist('enervate'/'ICE', 'L. A. Williams').
card_flavor_text('enervate'/'ICE', '\"Worlds turn in crucial moments of decision. Make your choice.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('enervate'/'ICE', '2503').

card_in_set('errant minion', 'ICE').
card_original_type('errant minion'/'ICE', 'Enchant Creature').
card_original_text('errant minion'/'ICE', 'During target creature\'s controller\'s upkeep, Errant Minion deals 2 damage to him or her. He or she may pay {1} for each 1 damage he or she wishes to prevent from Errant Minion.').
card_first_print('errant minion', 'ICE').
card_image_name('errant minion'/'ICE', 'errant minion').
card_uid('errant minion'/'ICE', 'ICE:Errant Minion:errant minion').
card_rarity('errant minion'/'ICE', 'Common').
card_artist('errant minion'/'ICE', 'Harold McNeill').
card_flavor_text('errant minion'/'ICE', 'Abandon not your faith when others abandon you.').
card_multiverse_id('errant minion'/'ICE', '2504').

card_in_set('errantry', 'ICE').
card_original_type('errantry'/'ICE', 'Enchant Creature').
card_original_text('errantry'/'ICE', 'Target creature gets +3/+0. If that creature attacks, no other creatures can attack this turn.').
card_first_print('errantry', 'ICE').
card_image_name('errantry'/'ICE', 'errantry').
card_uid('errantry'/'ICE', 'ICE:Errantry:errantry').
card_rarity('errantry'/'ICE', 'Common').
card_artist('errantry'/'ICE', 'L. A. Williams').
card_flavor_text('errantry'/'ICE', '\"There is no shame in solitude. The lone Knight may succeed where a hundred founder.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('errantry'/'ICE', '2619').

card_in_set('essence filter', 'ICE').
card_original_type('essence filter'/'ICE', 'Sorcery').
card_original_text('essence filter'/'ICE', 'Destroy all enchantments or destroy all non-white enchantments.').
card_first_print('essence filter', 'ICE').
card_image_name('essence filter'/'ICE', 'essence filter').
card_uid('essence filter'/'ICE', 'ICE:Essence Filter:essence filter').
card_rarity('essence filter'/'ICE', 'Common').
card_artist('essence filter'/'ICE', 'Rick Emond').
card_flavor_text('essence filter'/'ICE', '\"Freyalise has cleansed our bodies and minds of the plagues of Kjeldor; all she asks in return is that we keep pure our newly given home in Fyndhorn.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('essence filter'/'ICE', '2557').

card_in_set('essence flare', 'ICE').
card_original_type('essence flare'/'ICE', 'Enchant Creature').
card_original_text('essence flare'/'ICE', 'Target creature gets +2/+0. During each of its controller\'s upkeeps, put a -0/-1 counter on the creature. These counters remain even if Essence Flare is removed.').
card_first_print('essence flare', 'ICE').
card_image_name('essence flare'/'ICE', 'essence flare').
card_uid('essence flare'/'ICE', 'ICE:Essence Flare:essence flare').
card_rarity('essence flare'/'ICE', 'Common').
card_artist('essence flare'/'ICE', 'Richard Kane Ferguson').
card_flavor_text('essence flare'/'ICE', 'Never underestimate the power of the soul unleashed.').
card_multiverse_id('essence flare'/'ICE', '2505').

card_in_set('essence vortex', 'ICE').
card_original_type('essence vortex'/'ICE', 'Instant').
card_original_text('essence vortex'/'ICE', 'Bury target creature. That creature\'s controller may counter this spell by paying the creature\'s toughness in life. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('essence vortex', 'ICE').
card_image_name('essence vortex'/'ICE', 'essence vortex').
card_uid('essence vortex'/'ICE', 'ICE:Essence Vortex:essence vortex').
card_rarity('essence vortex'/'ICE', 'Uncommon').
card_artist('essence vortex'/'ICE', 'Margaret Organ-Kean').
card_flavor_text('essence vortex'/'ICE', 'Masters who sacrifice for servants will receive the gift of loyalty.').
card_multiverse_id('essence vortex'/'ICE', '2723').

card_in_set('fanatical fever', 'ICE').
card_original_type('fanatical fever'/'ICE', 'Instant').
card_original_text('fanatical fever'/'ICE', 'Target creature gains trample and gets +3/+0 until end of turn.').
card_first_print('fanatical fever', 'ICE').
card_image_name('fanatical fever'/'ICE', 'fanatical fever').
card_uid('fanatical fever'/'ICE', 'ICE:Fanatical Fever:fanatical fever').
card_rarity('fanatical fever'/'ICE', 'Uncommon').
card_artist('fanatical fever'/'ICE', 'Julie Baroh').
card_flavor_text('fanatical fever'/'ICE', '\"Let go your fury, and hone your anger. Become the fist of Freyalise!\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('fanatical fever'/'ICE', '2558').

card_in_set('fear', 'ICE').
card_original_type('fear'/'ICE', 'Enchant Creature').
card_original_text('fear'/'ICE', 'Target creature cannot be blocked except by artifact creatures or black creatures.').
card_image_name('fear'/'ICE', 'fear').
card_uid('fear'/'ICE', 'ICE:Fear:fear').
card_rarity('fear'/'ICE', 'Common').
card_artist('fear'/'ICE', 'Rick Emond').
card_flavor_text('fear'/'ICE', '\"Even the bravest of warriors yet knows the dark clutch of fright upon his stalwart heart.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('fear'/'ICE', '2448').

card_in_set('fiery justice', 'ICE').
card_original_type('fiery justice'/'ICE', 'Sorcery').
card_original_text('fiery justice'/'ICE', 'Fiery Justice deals 5 damage divided any way you choose among any number of target creatures and/or players. Target opponent gains 5 life.').
card_first_print('fiery justice', 'ICE').
card_image_name('fiery justice'/'ICE', 'fiery justice').
card_uid('fiery justice'/'ICE', 'ICE:Fiery Justice:fiery justice').
card_rarity('fiery justice'/'ICE', 'Rare').
card_artist('fiery justice'/'ICE', 'Melissa A. Benson').
card_flavor_text('fiery justice'/'ICE', '\"The fire of justice burns like nothing else.\" —Lovisa Coldeyes, Balduvian Chieftain').
card_multiverse_id('fiery justice'/'ICE', '2724').

card_in_set('fire covenant', 'ICE').
card_original_type('fire covenant'/'ICE', 'Instant').
card_original_text('fire covenant'/'ICE', 'Fire Covenant deals X damage, divided any way you choose among any number of target creatures, where X is equal to the amount of life you pay. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('fire covenant', 'ICE').
card_image_name('fire covenant'/'ICE', 'fire covenant').
card_uid('fire covenant'/'ICE', 'ICE:Fire Covenant:fire covenant').
card_rarity('fire covenant'/'ICE', 'Uncommon').
card_artist('fire covenant'/'ICE', 'Dan Frazier').
card_multiverse_id('fire covenant'/'ICE', '2725').

card_in_set('flame spirit', 'ICE').
card_original_type('flame spirit'/'ICE', 'Summon — Spirit').
card_original_text('flame spirit'/'ICE', '{R} +1/+0 until end of turn').
card_first_print('flame spirit', 'ICE').
card_image_name('flame spirit'/'ICE', 'flame spirit').
card_uid('flame spirit'/'ICE', 'ICE:Flame Spirit:flame spirit').
card_rarity('flame spirit'/'ICE', 'Uncommon').
card_artist('flame spirit'/'ICE', 'Justin Hampton').
card_flavor_text('flame spirit'/'ICE', '\"The spirit of the flame is the spirit of change.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('flame spirit'/'ICE', '2620').

card_in_set('flare', 'ICE').
card_original_type('flare'/'ICE', 'Instant').
card_original_text('flare'/'ICE', 'Flare deals 1 damage to target creature or player. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('flare', 'ICE').
card_image_name('flare'/'ICE', 'flare').
card_uid('flare'/'ICE', 'ICE:Flare:flare').
card_rarity('flare'/'ICE', 'Common').
card_artist('flare'/'ICE', 'Drew Tucker').
card_flavor_text('flare'/'ICE', '\"I strive for elegance and speed in my work.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('flare'/'ICE', '2621').

card_in_set('flooded woodlands', 'ICE').
card_original_type('flooded woodlands'/'ICE', 'Enchantment').
card_original_text('flooded woodlands'/'ICE', 'No green creature can attack unless its controller sacrifices a land whenever that creature attacks.').
card_first_print('flooded woodlands', 'ICE').
card_image_name('flooded woodlands'/'ICE', 'flooded woodlands').
card_uid('flooded woodlands'/'ICE', 'ICE:Flooded Woodlands:flooded woodlands').
card_rarity('flooded woodlands'/'ICE', 'Rare').
card_artist('flooded woodlands'/'ICE', 'Kaja Foglio').
card_flavor_text('flooded woodlands'/'ICE', 'Freyalise\'s tears bring life and renewal, though they also bring trouble.').
card_multiverse_id('flooded woodlands'/'ICE', '2726').

card_in_set('flow of maggots', 'ICE').
card_original_type('flow of maggots'/'ICE', 'Summon — Insects').
card_original_text('flow of maggots'/'ICE', 'Cumulative Upkeep: {1}\nCannot be blocked by non-wall creatures.').
card_first_print('flow of maggots', 'ICE').
card_image_name('flow of maggots'/'ICE', 'flow of maggots').
card_uid('flow of maggots'/'ICE', 'ICE:Flow of Maggots:flow of maggots').
card_rarity('flow of maggots'/'ICE', 'Rare').
card_artist('flow of maggots'/'ICE', 'Ron Spencer').
card_flavor_text('flow of maggots'/'ICE', '\"The very earth seemed alive and made a sound like the writhing of the damned.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('flow of maggots'/'ICE', '2449').

card_in_set('folk of the pines', 'ICE').
card_original_type('folk of the pines'/'ICE', 'Summon — Dryads').
card_original_text('folk of the pines'/'ICE', '{1}{G} +1/+0 until end of turn').
card_first_print('folk of the pines', 'ICE').
card_image_name('folk of the pines'/'ICE', 'folk of the pines').
card_uid('folk of the pines'/'ICE', 'ICE:Folk of the Pines:folk of the pines').
card_rarity('folk of the pines'/'ICE', 'Common').
card_artist('folk of the pines'/'ICE', 'NéNé Thomas & Catherine Buck').
card_flavor_text('folk of the pines'/'ICE', '\"Our friends of the forest take many forms, yet all serve the will of Freyalise.\"\n—Laina of the Elvish Council').
card_multiverse_id('folk of the pines'/'ICE', '2559').

card_in_set('forbidden lore', 'ICE').
card_original_type('forbidden lore'/'ICE', 'Enchant Land').
card_original_text('forbidden lore'/'ICE', 'When Forbidden Lore comes into play, choose target land.\n{0}: Tap land Forbidden Lore enchants to give target creature +2/+1 until end of turn.').
card_first_print('forbidden lore', 'ICE').
card_image_name('forbidden lore'/'ICE', 'forbidden lore').
card_uid('forbidden lore'/'ICE', 'ICE:Forbidden Lore:forbidden lore').
card_rarity('forbidden lore'/'ICE', 'Rare').
card_artist('forbidden lore'/'ICE', 'Christopher Rush').
card_multiverse_id('forbidden lore'/'ICE', '2560').

card_in_set('force void', 'ICE').
card_original_type('force void'/'ICE', 'Interrupt').
card_original_text('force void'/'ICE', 'Counter target spell unless that spell\'s caster pays an additional {1}. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('force void', 'ICE').
card_image_name('force void'/'ICE', 'force void').
card_uid('force void'/'ICE', 'ICE:Force Void:force void').
card_rarity('force void'/'ICE', 'Uncommon').
card_artist('force void'/'ICE', 'Mark Tedin').
card_flavor_text('force void'/'ICE', '\"My mind and spirit are one—a barrier not readily passed.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('force void'/'ICE', '2506').

card_in_set('forest', 'ICE').
card_original_type('forest'/'ICE', 'Land').
card_original_text('forest'/'ICE', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'ICE', 'forest1').
card_uid('forest'/'ICE', 'ICE:Forest:forest1').
card_rarity('forest'/'ICE', 'Basic Land').
card_artist('forest'/'ICE', 'Pat Morrissey').
card_multiverse_id('forest'/'ICE', '2748').

card_in_set('forest', 'ICE').
card_original_type('forest'/'ICE', 'Land').
card_original_text('forest'/'ICE', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'ICE', 'forest2').
card_uid('forest'/'ICE', 'ICE:Forest:forest2').
card_rarity('forest'/'ICE', 'Basic Land').
card_artist('forest'/'ICE', 'Pat Morrissey').
card_multiverse_id('forest'/'ICE', '2746').

card_in_set('forest', 'ICE').
card_original_type('forest'/'ICE', 'Land').
card_original_text('forest'/'ICE', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'ICE', 'forest3').
card_uid('forest'/'ICE', 'ICE:Forest:forest3').
card_rarity('forest'/'ICE', 'Basic Land').
card_artist('forest'/'ICE', 'Pat Morrissey').
card_multiverse_id('forest'/'ICE', '2747').

card_in_set('forgotten lore', 'ICE').
card_original_type('forgotten lore'/'ICE', 'Sorcery').
card_original_text('forgotten lore'/'ICE', 'Target opponent chooses target card from your graveyard. You may pay {G} to have that opponent choose a new target that he or she has not already chosen. Put the last target card in your hand.').
card_first_print('forgotten lore', 'ICE').
card_image_name('forgotten lore'/'ICE', 'forgotten lore').
card_uid('forgotten lore'/'ICE', 'ICE:Forgotten Lore:forgotten lore').
card_rarity('forgotten lore'/'ICE', 'Uncommon').
card_artist('forgotten lore'/'ICE', 'Harold McNeill').
card_flavor_text('forgotten lore'/'ICE', 'In ashes are the gems of history.').
card_multiverse_id('forgotten lore'/'ICE', '2561').

card_in_set('formation', 'ICE').
card_original_type('formation'/'ICE', 'Instant').
card_original_text('formation'/'ICE', 'Target creature gains banding until end of turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('formation', 'ICE').
card_image_name('formation'/'ICE', 'formation').
card_uid('formation'/'ICE', 'ICE:Formation:formation').
card_rarity('formation'/'ICE', 'Rare').
card_artist('formation'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('formation'/'ICE', '\"I have been a warrior for over four hundred years, and yet each generation of Kjeldorans teaches me new tricks. There can be no better allies.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('formation'/'ICE', '2685').

card_in_set('foul familiar', 'ICE').
card_original_type('foul familiar'/'ICE', 'Summon — Spirit').
card_original_text('foul familiar'/'ICE', 'Cannot be declared as a blocking creature.\n{B} Pay 1 life to return Foul Familiar to owner\'s hand. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('foul familiar', 'ICE').
card_image_name('foul familiar'/'ICE', 'foul familiar').
card_uid('foul familiar'/'ICE', 'ICE:Foul Familiar:foul familiar').
card_rarity('foul familiar'/'ICE', 'Common').
card_artist('foul familiar'/'ICE', 'Anson Maddocks').
card_flavor_text('foul familiar'/'ICE', '\"Serve me, and live forever.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('foul familiar'/'ICE', '2450').

card_in_set('foxfire', 'ICE').
card_original_type('foxfire'/'ICE', 'Instant').
card_original_text('foxfire'/'ICE', 'Untap target attacking creature. That creature neither receives nor deals damage in combat this turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('foxfire', 'ICE').
card_image_name('foxfire'/'ICE', 'foxfire').
card_uid('foxfire'/'ICE', 'ICE:Foxfire:foxfire').
card_rarity('foxfire'/'ICE', 'Common').
card_artist('foxfire'/'ICE', 'Margaret Organ-Kean').
card_flavor_text('foxfire'/'ICE', '\"Only the foolish fear Foxfire.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('foxfire'/'ICE', '2562').

card_in_set('freyalise supplicant', 'ICE').
card_original_type('freyalise supplicant'/'ICE', 'Summon — Cleric').
card_original_text('freyalise supplicant'/'ICE', '{T}: Sacrifice a red or white creature to have Freyalise Supplicant deal an amount of damage equal to half the creature\'s power, rounded down, to target creature or player.').
card_first_print('freyalise supplicant', 'ICE').
card_image_name('freyalise supplicant'/'ICE', 'freyalise supplicant').
card_uid('freyalise supplicant'/'ICE', 'ICE:Freyalise Supplicant:freyalise supplicant').
card_rarity('freyalise supplicant'/'ICE', 'Uncommon').
card_artist('freyalise supplicant'/'ICE', 'Liz Danforth & Douglas Shuler').
card_flavor_text('freyalise supplicant'/'ICE', '\"We have joined with the Druids of the Juniper Order. Our faith is one.\"\n—Laina of the Elvish Council').
card_multiverse_id('freyalise supplicant'/'ICE', '2563').

card_in_set('freyalise\'s charm', 'ICE').
card_original_type('freyalise\'s charm'/'ICE', 'Enchantment').
card_original_text('freyalise\'s charm'/'ICE', '{G}{G} When any opponent successfully casts a black spell, draw a card. Use this ability only once each time a black spell is cast.\n{G}{G} Return Freyalise\'s Charm to owner\'s hand.').
card_first_print('freyalise\'s charm', 'ICE').
card_image_name('freyalise\'s charm'/'ICE', 'freyalise\'s charm').
card_uid('freyalise\'s charm'/'ICE', 'ICE:Freyalise\'s Charm:freyalise\'s charm').
card_rarity('freyalise\'s charm'/'ICE', 'Uncommon').
card_artist('freyalise\'s charm'/'ICE', 'Margaret Organ-Kean').
card_multiverse_id('freyalise\'s charm'/'ICE', '2564').

card_in_set('freyalise\'s winds', 'ICE').
card_original_type('freyalise\'s winds'/'ICE', 'Enchantment').
card_original_text('freyalise\'s winds'/'ICE', 'Whenever a permanent becomes tapped, put a wind counter on it. Permanents with any wind counters on them do not untap during their controller\'s untap phase; instead, remove all wind counters from those permanents.').
card_first_print('freyalise\'s winds', 'ICE').
card_image_name('freyalise\'s winds'/'ICE', 'freyalise\'s winds').
card_uid('freyalise\'s winds'/'ICE', 'ICE:Freyalise\'s Winds:freyalise\'s winds').
card_rarity('freyalise\'s winds'/'ICE', 'Rare').
card_artist('freyalise\'s winds'/'ICE', 'Mark Tedin').
card_multiverse_id('freyalise\'s winds'/'ICE', '2565').

card_in_set('fumarole', 'ICE').
card_original_type('fumarole'/'ICE', 'Sorcery').
card_original_text('fumarole'/'ICE', 'Pay 3 life to destroy target creature and target land. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('fumarole', 'ICE').
card_image_name('fumarole'/'ICE', 'fumarole').
card_uid('fumarole'/'ICE', 'ICE:Fumarole:fumarole').
card_rarity('fumarole'/'ICE', 'Uncommon').
card_artist('fumarole'/'ICE', 'Drew Tucker').
card_flavor_text('fumarole'/'ICE', '\"Too many of us have died in the explosions that wrack these hills.\"\n—Klazina Jansdotter, Leader of the Order of the Sacred Torch').
card_multiverse_id('fumarole'/'ICE', '2727').

card_in_set('fylgja', 'ICE').
card_original_type('fylgja'/'ICE', 'Enchant Creature').
card_original_text('fylgja'/'ICE', 'When Fylgja comes into play, put four healing counters on it.\n{0}: Remove a healing counter from Fylgja to prevent 1 damage to creature Fylgja enchants.\n{2}{W} Put a healing counter on Fylgja.').
card_first_print('fylgja', 'ICE').
card_image_name('fylgja'/'ICE', 'fylgja').
card_uid('fylgja'/'ICE', 'ICE:Fylgja:fylgja').
card_rarity('fylgja'/'ICE', 'Common').
card_artist('fylgja'/'ICE', 'Edward P. Beard, Jr.').
card_multiverse_id('fylgja'/'ICE', '2686').

card_in_set('fyndhorn bow', 'ICE').
card_original_type('fyndhorn bow'/'ICE', 'Artifact').
card_original_text('fyndhorn bow'/'ICE', '{3},{T}: Target creature gains first strike until end of turn.').
card_first_print('fyndhorn bow', 'ICE').
card_image_name('fyndhorn bow'/'ICE', 'fyndhorn bow').
card_uid('fyndhorn bow'/'ICE', 'ICE:Fyndhorn Bow:fyndhorn bow').
card_rarity('fyndhorn bow'/'ICE', 'Uncommon').
card_artist('fyndhorn bow'/'ICE', 'Rob Alexander').
card_flavor_text('fyndhorn bow'/'ICE', '\"With a bow like this, the hunting is always good.\"\n—Taaveti of Kelsinko,\nElvish Hunter').
card_multiverse_id('fyndhorn bow'/'ICE', '2404').

card_in_set('fyndhorn brownie', 'ICE').
card_original_type('fyndhorn brownie'/'ICE', 'Summon — Brownie').
card_original_text('fyndhorn brownie'/'ICE', '{2}{G},{T}: Untap target creature.').
card_first_print('fyndhorn brownie', 'ICE').
card_image_name('fyndhorn brownie'/'ICE', 'fyndhorn brownie').
card_uid('fyndhorn brownie'/'ICE', 'ICE:Fyndhorn Brownie:fyndhorn brownie').
card_rarity('fyndhorn brownie'/'ICE', 'Common').
card_artist('fyndhorn brownie'/'ICE', 'Richard Thomas').
card_flavor_text('fyndhorn brownie'/'ICE', '\"I\'ve been insulted by drunks in a hundred inns, but never as skillfully or annoyingly as by those blasted Brownies.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('fyndhorn brownie'/'ICE', '2566').

card_in_set('fyndhorn elder', 'ICE').
card_original_type('fyndhorn elder'/'ICE', 'Summon — Elf').
card_original_text('fyndhorn elder'/'ICE', '{T}: Add {G}{G} to your mana pool. Play this ability as an interrupt.').
card_first_print('fyndhorn elder', 'ICE').
card_image_name('fyndhorn elder'/'ICE', 'fyndhorn elder').
card_uid('fyndhorn elder'/'ICE', 'ICE:Fyndhorn Elder:fyndhorn elder').
card_rarity('fyndhorn elder'/'ICE', 'Uncommon').
card_artist('fyndhorn elder'/'ICE', 'Christopher Rush').
card_flavor_text('fyndhorn elder'/'ICE', '\"Do we know what we\'re doing? Yes—the will of Freyalise.\"\n—Laina of the Elvish Council').
card_multiverse_id('fyndhorn elder'/'ICE', '2567').

card_in_set('fyndhorn elves', 'ICE').
card_original_type('fyndhorn elves'/'ICE', 'Summon — Elves').
card_original_text('fyndhorn elves'/'ICE', '{T}: Add {G} to your mana pool. Play this ability as an interrupt.').
card_first_print('fyndhorn elves', 'ICE').
card_image_name('fyndhorn elves'/'ICE', 'fyndhorn elves').
card_uid('fyndhorn elves'/'ICE', 'ICE:Fyndhorn Elves:fyndhorn elves').
card_rarity('fyndhorn elves'/'ICE', 'Common').
card_artist('fyndhorn elves'/'ICE', 'Justin Hampton').
card_flavor_text('fyndhorn elves'/'ICE', '\"Living side by side with the Elves for so long leaves me with no doubt that we serve the same goddess.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('fyndhorn elves'/'ICE', '2568').

card_in_set('fyndhorn pollen', 'ICE').
card_original_type('fyndhorn pollen'/'ICE', 'Enchantment').
card_original_text('fyndhorn pollen'/'ICE', 'Cumulative Upkeep: {1}\nAll creatures get -1/-0.\n{1}{G}: All creatures get -1/-0 until end of turn.').
card_first_print('fyndhorn pollen', 'ICE').
card_image_name('fyndhorn pollen'/'ICE', 'fyndhorn pollen').
card_uid('fyndhorn pollen'/'ICE', 'ICE:Fyndhorn Pollen:fyndhorn pollen').
card_rarity('fyndhorn pollen'/'ICE', 'Rare').
card_artist('fyndhorn pollen'/'ICE', 'Phil Foglio').
card_flavor_text('fyndhorn pollen'/'ICE', '\"I breathed deeply, and suddenly I knew not who or where I was.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('fyndhorn pollen'/'ICE', '2569').

card_in_set('game of chaos', 'ICE').
card_original_type('game of chaos'/'ICE', 'Sorcery').
card_original_text('game of chaos'/'ICE', 'Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, you gain 1 life and that opponent loses 1 life. Otherwise, you lose 1 life and the opponent gains 1 life. Effects that prevent or redirect damage cannot be used to counter this loss of life. The winner of each round decides whether to continue. Double the stakes in life each round.').
card_first_print('game of chaos', 'ICE').
card_image_name('game of chaos'/'ICE', 'game of chaos').
card_uid('game of chaos'/'ICE', 'ICE:Game of Chaos:game of chaos').
card_rarity('game of chaos'/'ICE', 'Rare').
card_artist('game of chaos'/'ICE', 'Drew Tucker').
card_multiverse_id('game of chaos'/'ICE', '2622').

card_in_set('gangrenous zombies', 'ICE').
card_original_type('gangrenous zombies'/'ICE', 'Summon — Zombies').
card_original_text('gangrenous zombies'/'ICE', '{T}: Sacrifice Gangrenous Zombies to have it deal 1 damage to each creature and player. If you control any snow-covered swamps, Gangrenous Zombies instead deals 2 damage to each creature and player.').
card_first_print('gangrenous zombies', 'ICE').
card_image_name('gangrenous zombies'/'ICE', 'gangrenous zombies').
card_uid('gangrenous zombies'/'ICE', 'ICE:Gangrenous Zombies:gangrenous zombies').
card_rarity('gangrenous zombies'/'ICE', 'Common').
card_artist('gangrenous zombies'/'ICE', 'Brian Snõddy').
card_multiverse_id('gangrenous zombies'/'ICE', '2451').

card_in_set('gaze of pain', 'ICE').
card_original_type('gaze of pain'/'ICE', 'Sorcery').
card_original_text('gaze of pain'/'ICE', 'For each creature you control that attacks and is not blocked, you may choose to have it deal no damage to defending player this turn. If you do so, it instead deals damage equal to its power to any target creature.').
card_first_print('gaze of pain', 'ICE').
card_image_name('gaze of pain'/'ICE', 'gaze of pain').
card_uid('gaze of pain'/'ICE', 'ICE:Gaze of Pain:gaze of pain').
card_rarity('gaze of pain'/'ICE', 'Common').
card_artist('gaze of pain'/'ICE', 'Anson Maddocks').
card_flavor_text('gaze of pain'/'ICE', 'Remove the guardians to ensure your victory.').
card_multiverse_id('gaze of pain'/'ICE', '2452').

card_in_set('general jarkeld', 'ICE').
card_original_type('general jarkeld'/'ICE', 'Summon — Legend').
card_original_text('general jarkeld'/'ICE', '{T}: Switch the blocking creatures of two target attacking creatures; all defense must remain legal. Use this ability only during combat after defense is chosen and before damage is dealt.').
card_first_print('general jarkeld', 'ICE').
card_image_name('general jarkeld'/'ICE', 'general jarkeld').
card_uid('general jarkeld'/'ICE', 'ICE:General Jarkeld:general jarkeld').
card_rarity('general jarkeld'/'ICE', 'Rare').
card_artist('general jarkeld'/'ICE', 'Richard Thomas').
card_multiverse_id('general jarkeld'/'ICE', '2687').

card_in_set('ghostly flame', 'ICE').
card_original_type('ghostly flame'/'ICE', 'Enchantment').
card_original_text('ghostly flame'/'ICE', 'Both black and red permanents and spells are considered colorless sources of damage.').
card_first_print('ghostly flame', 'ICE').
card_image_name('ghostly flame'/'ICE', 'ghostly flame').
card_uid('ghostly flame'/'ICE', 'ICE:Ghostly Flame:ghostly flame').
card_rarity('ghostly flame'/'ICE', 'Rare').
card_artist('ghostly flame'/'ICE', 'Randy Gallegos').
card_flavor_text('ghostly flame'/'ICE', '\"Walk slowly when in the nether world, and seek that which calls.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('ghostly flame'/'ICE', '2728').

card_in_set('giant growth', 'ICE').
card_original_type('giant growth'/'ICE', 'Instant').
card_original_text('giant growth'/'ICE', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'ICE', 'giant growth').
card_uid('giant growth'/'ICE', 'ICE:Giant Growth:giant growth').
card_rarity('giant growth'/'ICE', 'Common').
card_artist('giant growth'/'ICE', 'L. A. Williams').
card_flavor_text('giant growth'/'ICE', '\"Here in Fyndhorn, the goddess Freyalise is generous to her children.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('giant growth'/'ICE', '2570').

card_in_set('giant trap door spider', 'ICE').
card_original_type('giant trap door spider'/'ICE', 'Summon — Spider').
card_original_text('giant trap door spider'/'ICE', '{1}{R}{G},{T}: Remove from the game target creature, which doesn\'t have flying and is attacking you, and Giant Trap Door Spider.').
card_first_print('giant trap door spider', 'ICE').
card_image_name('giant trap door spider'/'ICE', 'giant trap door spider').
card_uid('giant trap door spider'/'ICE', 'ICE:Giant Trap Door Spider:giant trap door spider').
card_rarity('giant trap door spider'/'ICE', 'Uncommon').
card_artist('giant trap door spider'/'ICE', 'Heather Hudson').
card_flavor_text('giant trap door spider'/'ICE', '\"So large and so quiet—a combination best avoided.\"\n—Disa the Restless, journal entry').
card_multiverse_id('giant trap door spider'/'ICE', '2729').

card_in_set('glacial chasm', 'ICE').
card_original_type('glacial chasm'/'ICE', 'Land').
card_original_text('glacial chasm'/'ICE', 'Cumulative Upkeep: 2 life\nWhen Glacial Chasm comes into play, sacrifice a land. You cannot attack. All damage dealt to you is reduced to 0.').
card_first_print('glacial chasm', 'ICE').
card_image_name('glacial chasm'/'ICE', 'glacial chasm').
card_uid('glacial chasm'/'ICE', 'ICE:Glacial Chasm:glacial chasm').
card_rarity('glacial chasm'/'ICE', 'Uncommon').
card_artist('glacial chasm'/'ICE', 'Liz Danforth').
card_multiverse_id('glacial chasm'/'ICE', '2752').

card_in_set('glacial crevasses', 'ICE').
card_original_type('glacial crevasses'/'ICE', 'Enchantment').
card_original_text('glacial crevasses'/'ICE', '{0}: Sacrifice a snow-covered mountain. No creatures deal damage in combat this turn.').
card_first_print('glacial crevasses', 'ICE').
card_image_name('glacial crevasses'/'ICE', 'glacial crevasses').
card_uid('glacial crevasses'/'ICE', 'ICE:Glacial Crevasses:glacial crevasses').
card_rarity('glacial crevasses'/'ICE', 'Rare').
card_artist('glacial crevasses'/'ICE', 'Mike Raabe').
card_flavor_text('glacial crevasses'/'ICE', '\"We were chasing Lim-Dûl when the ridge in front of us suddenly crumbled. I can\'t believe it was mere coincidence.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('glacial crevasses'/'ICE', '2623').

card_in_set('glacial wall', 'ICE').
card_original_type('glacial wall'/'ICE', 'Summon — Wall').
card_original_text('glacial wall'/'ICE', '').
card_first_print('glacial wall', 'ICE').
card_image_name('glacial wall'/'ICE', 'glacial wall').
card_uid('glacial wall'/'ICE', 'ICE:Glacial Wall:glacial wall').
card_rarity('glacial wall'/'ICE', 'Uncommon').
card_artist('glacial wall'/'ICE', 'Dameon Willich').
card_flavor_text('glacial wall'/'ICE', '\"We are farther west than any could have imagined possible, but I still wish to press on. Unfortunately, huge walls of ice block further travel. We can\'t believe they are natural.\"\n—Disa the Restless, journal entry').
card_multiverse_id('glacial wall'/'ICE', '2507').

card_in_set('glaciers', 'ICE').
card_original_type('glaciers'/'ICE', 'Enchantment').
card_original_text('glaciers'/'ICE', 'During your upkeep, pay {W}{U} or destroy Glaciers. All mountains become plains.').
card_first_print('glaciers', 'ICE').
card_image_name('glaciers'/'ICE', 'glaciers').
card_uid('glaciers'/'ICE', 'ICE:Glaciers:glaciers').
card_rarity('glaciers'/'ICE', 'Rare').
card_artist('glaciers'/'ICE', 'Mark Tedin').
card_flavor_text('glaciers'/'ICE', '\"Even the highest mountain can be ground to dust.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('glaciers'/'ICE', '2730').

card_in_set('goblin lyre', 'ICE').
card_original_type('goblin lyre'/'ICE', 'Artifact').
card_original_text('goblin lyre'/'ICE', '{0}: Sacrifice Goblin Lyre. Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, Goblin Lyre deals * damage to that opponent, where * is equal to the number of creatures you control. Otherwise, Goblin Lyre deals * damage to you, where * is equal to the number of creatures the opponent controls.').
card_first_print('goblin lyre', 'ICE').
card_image_name('goblin lyre'/'ICE', 'goblin lyre').
card_uid('goblin lyre'/'ICE', 'ICE:Goblin Lyre:goblin lyre').
card_rarity('goblin lyre'/'ICE', 'Rare').
card_artist('goblin lyre'/'ICE', 'Mike Kimble').
card_multiverse_id('goblin lyre'/'ICE', '2405').

card_in_set('goblin mutant', 'ICE').
card_original_type('goblin mutant'/'ICE', 'Summon — Goblin').
card_original_text('goblin mutant'/'ICE', 'Trample\nCannot attack if defending player controls an untapped creature with power greater than 2. Cannot be assigned to block any creature with power greater than 2.').
card_first_print('goblin mutant', 'ICE').
card_image_name('goblin mutant'/'ICE', 'goblin mutant').
card_uid('goblin mutant'/'ICE', 'ICE:Goblin Mutant:goblin mutant').
card_rarity('goblin mutant'/'ICE', 'Uncommon').
card_artist('goblin mutant'/'ICE', 'Daniel Gelon').
card_flavor_text('goblin mutant'/'ICE', 'If only it had three brains, too.').
card_multiverse_id('goblin mutant'/'ICE', '2624').

card_in_set('goblin sappers', 'ICE').
card_original_type('goblin sappers'/'ICE', 'Summon — Goblins').
card_original_text('goblin sappers'/'ICE', '{R}{R},{T}: Target creature you control cannot be blocked this turn. At end of combat, destroy that creature and Goblin Sappers.\n{R}{R}{R}{R},{T}: Target creature you control cannot be blocked this turn. At end of combat, destroy that creature.').
card_first_print('goblin sappers', 'ICE').
card_image_name('goblin sappers'/'ICE', 'goblin sappers').
card_uid('goblin sappers'/'ICE', 'ICE:Goblin Sappers:goblin sappers').
card_rarity('goblin sappers'/'ICE', 'Common').
card_artist('goblin sappers'/'ICE', 'Jeff A. Menges').
card_multiverse_id('goblin sappers'/'ICE', '2625').

card_in_set('goblin ski patrol', 'ICE').
card_original_type('goblin ski patrol'/'ICE', 'Summon — Goblins').
card_original_text('goblin ski patrol'/'ICE', '{1}{R} Flying and +2/+0. At end of turn, bury Goblin Ski Patrol. Use this ability only once and only if you control any snow-covered mountains.').
card_first_print('goblin ski patrol', 'ICE').
card_image_name('goblin ski patrol'/'ICE', 'goblin ski patrol').
card_uid('goblin ski patrol'/'ICE', 'ICE:Goblin Ski Patrol:goblin ski patrol').
card_rarity('goblin ski patrol'/'ICE', 'Common').
card_artist('goblin ski patrol'/'ICE', 'Mark Poole').
card_flavor_text('goblin ski patrol'/'ICE', '\"AIIIEEEE!\"\n—Ib Halfheart, Goblin Tactician').
card_multiverse_id('goblin ski patrol'/'ICE', '2626').

card_in_set('goblin snowman', 'ICE').
card_original_type('goblin snowman'/'ICE', 'Summon — Goblins').
card_original_text('goblin snowman'/'ICE', 'When blocking, Goblin Snowman neither deals nor receives damage in combat.\n{T}: Goblin Snowman deals 1 damage to target creature it blocks.').
card_first_print('goblin snowman', 'ICE').
card_image_name('goblin snowman'/'ICE', 'goblin snowman').
card_uid('goblin snowman'/'ICE', 'ICE:Goblin Snowman:goblin snowman').
card_rarity('goblin snowman'/'ICE', 'Uncommon').
card_artist('goblin snowman'/'ICE', 'Daniel Gelon').
card_flavor_text('goblin snowman'/'ICE', '\"Strength in numbers? Right.\"\n—Ib Halfheart, Goblin Tactician').
card_multiverse_id('goblin snowman'/'ICE', '2627').

card_in_set('gorilla pack', 'ICE').
card_original_type('gorilla pack'/'ICE', 'Summon — Gorilla Pack').
card_original_text('gorilla pack'/'ICE', 'Gorilla Pack cannot attack if defending player controls no forests. Bury Gorilla Pack if you control no forests.').
card_first_print('gorilla pack', 'ICE').
card_image_name('gorilla pack'/'ICE', 'gorilla pack').
card_uid('gorilla pack'/'ICE', 'ICE:Gorilla Pack:gorilla pack').
card_rarity('gorilla pack'/'ICE', 'Common').
card_artist('gorilla pack'/'ICE', 'Anthony Waters').
card_flavor_text('gorilla pack'/'ICE', '\"We learned this at a dear price: once you cross the great river, get through the Yavimaya forest at top speed.\"\n—Disa the Restless, journal entry').
card_multiverse_id('gorilla pack'/'ICE', '2571').

card_in_set('gravebind', 'ICE').
card_original_type('gravebind'/'ICE', 'Instant').
card_original_text('gravebind'/'ICE', 'Target creature cannot regenerate this turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('gravebind', 'ICE').
card_image_name('gravebind'/'ICE', 'gravebind').
card_uid('gravebind'/'ICE', 'ICE:Gravebind:gravebind').
card_rarity('gravebind'/'ICE', 'Rare').
card_artist('gravebind'/'ICE', 'Drew Tucker').
card_flavor_text('gravebind'/'ICE', 'One foot in the grave,\nfirmly planted.').
card_multiverse_id('gravebind'/'ICE', '2453').

card_in_set('green scarab', 'ICE').
card_original_type('green scarab'/'ICE', 'Enchant Creature').
card_original_text('green scarab'/'ICE', 'Target creature gets +2/+2 as long as any opponent controls any green cards. That creature cannot be blocked by green creatures.').
card_first_print('green scarab', 'ICE').
card_image_name('green scarab'/'ICE', 'green scarab').
card_uid('green scarab'/'ICE', 'ICE:Green Scarab:green scarab').
card_rarity('green scarab'/'ICE', 'Uncommon').
card_artist('green scarab'/'ICE', 'Nicola Leonard').
card_multiverse_id('green scarab'/'ICE', '2688').

card_in_set('grizzled wolverine', 'ICE').
card_original_type('grizzled wolverine'/'ICE', 'Summon — Wolverine').
card_original_text('grizzled wolverine'/'ICE', '{R} +2/+0 until end of turn. Use this ability only when a creature is assigned to block Grizzled Wolverine and only once each turn.').
card_first_print('grizzled wolverine', 'ICE').
card_image_name('grizzled wolverine'/'ICE', 'grizzled wolverine').
card_uid('grizzled wolverine'/'ICE', 'ICE:Grizzled Wolverine:grizzled wolverine').
card_rarity('grizzled wolverine'/'ICE', 'Common').
card_artist('grizzled wolverine'/'ICE', 'Cornelius Brudi').
card_flavor_text('grizzled wolverine'/'ICE', '\"Before you release a Wolverine from the trap, make sure it\'s really dead.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('grizzled wolverine'/'ICE', '2628').

card_in_set('hallowed ground', 'ICE').
card_original_type('hallowed ground'/'ICE', 'Enchantment').
card_original_text('hallowed ground'/'ICE', '{W}{W} Return target non-snow-covered land you control to owner\'s hand.').
card_first_print('hallowed ground', 'ICE').
card_image_name('hallowed ground'/'ICE', 'hallowed ground').
card_uid('hallowed ground'/'ICE', 'ICE:Hallowed Ground:hallowed ground').
card_rarity('hallowed ground'/'ICE', 'Uncommon').
card_artist('hallowed ground'/'ICE', 'Douglas Shuler').
card_flavor_text('hallowed ground'/'ICE', '\"On this site where Kjeld\'s blood was spilled, let none raise a fist or deny a beggar.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('hallowed ground'/'ICE', '2689').

card_in_set('halls of mist', 'ICE').
card_original_type('halls of mist'/'ICE', 'Land').
card_original_text('halls of mist'/'ICE', 'Cumulative Upkeep: {1}\nNo creature can attack if it attacked during its controller\'s last turn.').
card_first_print('halls of mist', 'ICE').
card_image_name('halls of mist'/'ICE', 'halls of mist').
card_uid('halls of mist'/'ICE', 'ICE:Halls of Mist:halls of mist').
card_rarity('halls of mist'/'ICE', 'Rare').
card_artist('halls of mist'/'ICE', 'Mark Poole').
card_multiverse_id('halls of mist'/'ICE', '2753').

card_in_set('heal', 'ICE').
card_original_type('heal'/'ICE', 'Instant').
card_original_text('heal'/'ICE', 'Prevent 1 damage to any creature or player. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('heal', 'ICE').
card_image_name('heal'/'ICE', 'heal').
card_uid('heal'/'ICE', 'ICE:Heal:heal').
card_rarity('heal'/'ICE', 'Common').
card_artist('heal'/'ICE', 'Mark Tedin').
card_flavor_text('heal'/'ICE', '\"Sometimes even the smallest boon can save a life.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('heal'/'ICE', '2690').

card_in_set('hecatomb', 'ICE').
card_original_type('hecatomb'/'ICE', 'Enchantment').
card_original_text('hecatomb'/'ICE', 'When Hecatomb comes into play, sacrifice four creatures.\n{0}: Tap target swamp you control to have Hecatomb deal 1 damage to target creature or player.').
card_first_print('hecatomb', 'ICE').
card_image_name('hecatomb'/'ICE', 'hecatomb').
card_uid('hecatomb'/'ICE', 'ICE:Hecatomb:hecatomb').
card_rarity('hecatomb'/'ICE', 'Rare').
card_artist('hecatomb'/'ICE', 'NéNé Thomas').
card_multiverse_id('hecatomb'/'ICE', '2454').

card_in_set('hematite talisman', 'ICE').
card_original_type('hematite talisman'/'ICE', 'Artifact').
card_original_text('hematite talisman'/'ICE', '{3}: Untap target permanent.\nUse this ability only when a red spell is successfully cast and only once for each red spell cast.').
card_first_print('hematite talisman', 'ICE').
card_image_name('hematite talisman'/'ICE', 'hematite talisman').
card_uid('hematite talisman'/'ICE', 'ICE:Hematite Talisman:hematite talisman').
card_rarity('hematite talisman'/'ICE', 'Uncommon').
card_artist('hematite talisman'/'ICE', 'L. A. Williams').
card_multiverse_id('hematite talisman'/'ICE', '2406').

card_in_set('hipparion', 'ICE').
card_original_type('hipparion'/'ICE', 'Summon — Hipparion').
card_original_text('hipparion'/'ICE', 'Cannot be assigned to block a creature with power 3 or greater unless you pay an additional {1}.').
card_first_print('hipparion', 'ICE').
card_image_name('hipparion'/'ICE', 'hipparion').
card_uid('hipparion'/'ICE', 'ICE:Hipparion:hipparion').
card_rarity('hipparion'/'ICE', 'Uncommon').
card_artist('hipparion'/'ICE', 'Dameon Willich').
card_flavor_text('hipparion'/'ICE', '\"Someone once said that Hipparions are to Warriors what Aesthir are to Skyknights. Don\'t believe it.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('hipparion'/'ICE', '2691').

card_in_set('hoar shade', 'ICE').
card_original_type('hoar shade'/'ICE', 'Summon — Shade').
card_original_text('hoar shade'/'ICE', '{B} +1/+1 until end of turn').
card_first_print('hoar shade', 'ICE').
card_image_name('hoar shade'/'ICE', 'hoar shade').
card_uid('hoar shade'/'ICE', 'ICE:Hoar Shade:hoar shade').
card_rarity('hoar shade'/'ICE', 'Common').
card_artist('hoar shade'/'ICE', 'Richard Thomas').
card_flavor_text('hoar shade'/'ICE', '\"The creature we fought in the western waste was doubly dangerous: mortally wounded, it rebounded and attacked again.\"\n—Disa the Restless, journal entry').
card_multiverse_id('hoar shade'/'ICE', '2455').

card_in_set('hot springs', 'ICE').
card_original_type('hot springs'/'ICE', 'Enchant Land').
card_original_text('hot springs'/'ICE', 'When Hot Springs comes into play, choose target land you control.\n{0}: Tap land Hot Springs enchants to prevent 1 damage to any creature or player.').
card_first_print('hot springs', 'ICE').
card_image_name('hot springs'/'ICE', 'hot springs').
card_uid('hot springs'/'ICE', 'ICE:Hot Springs:hot springs').
card_rarity('hot springs'/'ICE', 'Rare').
card_artist('hot springs'/'ICE', 'Nicola Leonard').
card_flavor_text('hot springs'/'ICE', 'Warmth is life; heat is peace.').
card_multiverse_id('hot springs'/'ICE', '2572').

card_in_set('howl from beyond', 'ICE').
card_original_type('howl from beyond'/'ICE', 'Instant').
card_original_text('howl from beyond'/'ICE', 'Target creature gets +X/+0 until end of turn.').
card_image_name('howl from beyond'/'ICE', 'howl from beyond').
card_uid('howl from beyond'/'ICE', 'ICE:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'ICE', 'Common').
card_artist('howl from beyond'/'ICE', 'Mark Poole').
card_flavor_text('howl from beyond'/'ICE', '\"From the mouths of my servants, my voice shall emerge to shake the souls of my foes.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('howl from beyond'/'ICE', '2456').

card_in_set('hurricane', 'ICE').
card_original_type('hurricane'/'ICE', 'Sorcery').
card_original_text('hurricane'/'ICE', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'ICE', 'hurricane').
card_uid('hurricane'/'ICE', 'ICE:Hurricane:hurricane').
card_rarity('hurricane'/'ICE', 'Uncommon').
card_artist('hurricane'/'ICE', 'Cornelius Brudi').
card_flavor_text('hurricane'/'ICE', '\"This was quite possibly the least pleasant occurrence on the entire expedition.\"\n—Disa the Restless, journal entry').
card_multiverse_id('hurricane'/'ICE', '2573').

card_in_set('hyalopterous lemure', 'ICE').
card_original_type('hyalopterous lemure'/'ICE', 'Summon — Lemure').
card_original_text('hyalopterous lemure'/'ICE', '{0}: Flying and -1/-0 until end of turn').
card_first_print('hyalopterous lemure', 'ICE').
card_image_name('hyalopterous lemure'/'ICE', 'hyalopterous lemure').
card_uid('hyalopterous lemure'/'ICE', 'ICE:Hyalopterous Lemure:hyalopterous lemure').
card_rarity('hyalopterous lemure'/'ICE', 'Uncommon').
card_artist('hyalopterous lemure'/'ICE', 'Richard Thomas').
card_flavor_text('hyalopterous lemure'/'ICE', '\"The Lemures looked harmless, until they descended on my troops. Within moments, only bones remained.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('hyalopterous lemure'/'ICE', '2457').

card_in_set('hydroblast', 'ICE').
card_original_type('hydroblast'/'ICE', 'Interrupt').
card_original_text('hydroblast'/'ICE', 'Counter target spell if it is red or destroy target permanent if it is red.').
card_first_print('hydroblast', 'ICE').
card_image_name('hydroblast'/'ICE', 'hydroblast').
card_uid('hydroblast'/'ICE', 'ICE:Hydroblast:hydroblast').
card_rarity('hydroblast'/'ICE', 'Common').
card_artist('hydroblast'/'ICE', 'Kaja Foglio').
card_flavor_text('hydroblast'/'ICE', '\"Heed the lessons of our time: the forms of water may move the land itself and hold captive the fires within.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('hydroblast'/'ICE', '2508').

card_in_set('hymn of rebirth', 'ICE').
card_original_type('hymn of rebirth'/'ICE', 'Sorcery').
card_original_text('hymn of rebirth'/'ICE', 'Take target creature from any graveyard and put it directly into play under your control as though it were just summoned.').
card_first_print('hymn of rebirth', 'ICE').
card_image_name('hymn of rebirth'/'ICE', 'hymn of rebirth').
card_uid('hymn of rebirth'/'ICE', 'ICE:Hymn of Rebirth:hymn of rebirth').
card_rarity('hymn of rebirth'/'ICE', 'Uncommon').
card_artist('hymn of rebirth'/'ICE', 'Richard Kane Ferguson').
card_flavor_text('hymn of rebirth'/'ICE', '\"There will come soft rains, and spring shall be amongst us, a welcome friend.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('hymn of rebirth'/'ICE', '2731').

card_in_set('ice cauldron', 'ICE').
card_original_type('ice cauldron'/'ICE', 'Artifact').
card_original_text('ice cauldron'/'ICE', 'X,{T}: Put a charge counter on Ice Cauldron, and put a spell card face up on Ice Cauldron. Note the type and amount of mana used to pay this activation cost. Use this ability only if there are no charge counters on Ice Cauldron. You may play that spell card as though it were in your hand. \n{T}: Remove the charge counter from Ice Cauldron to add mana of the type and amount last used to put a charge counter on Ice Cauldron to your mana pool. This mana is usable only to cast the spell on top of Ice Cauldron.').
card_first_print('ice cauldron', 'ICE').
card_image_name('ice cauldron'/'ICE', 'ice cauldron').
card_uid('ice cauldron'/'ICE', 'ICE:Ice Cauldron:ice cauldron').
card_rarity('ice cauldron'/'ICE', 'Rare').
card_artist('ice cauldron'/'ICE', 'Dan Frazier').
card_multiverse_id('ice cauldron'/'ICE', '2407').

card_in_set('ice floe', 'ICE').
card_original_type('ice floe'/'ICE', 'Land').
card_original_text('ice floe'/'ICE', 'You may choose not to untap Ice Floe during your untap phase.\n{T}: Tap target creature without flying that is attacking you. As long as Ice Floe remains tapped, that creature does not untap during its controller\'s untap phase.').
card_first_print('ice floe', 'ICE').
card_image_name('ice floe'/'ICE', 'ice floe').
card_uid('ice floe'/'ICE', 'ICE:Ice Floe:ice floe').
card_rarity('ice floe'/'ICE', 'Uncommon').
card_artist('ice floe'/'ICE', 'Jeff A. Menges').
card_multiverse_id('ice floe'/'ICE', '2754').

card_in_set('iceberg', 'ICE').
card_original_type('iceberg'/'ICE', 'Enchantment').
card_original_text('iceberg'/'ICE', 'When Iceberg comes into play, put X ice counters on it.\n{3}: Put an ice counter on Iceberg.\n{0}: Remove an ice counter from Iceberg to add one colorless mana to your mana pool. Play this ability as an interrupt.').
card_first_print('iceberg', 'ICE').
card_image_name('iceberg'/'ICE', 'iceberg').
card_uid('iceberg'/'ICE', 'ICE:Iceberg:iceberg').
card_rarity('iceberg'/'ICE', 'Uncommon').
card_artist('iceberg'/'ICE', 'Jeff A. Menges').
card_multiverse_id('iceberg'/'ICE', '2509').

card_in_set('icequake', 'ICE').
card_original_type('icequake'/'ICE', 'Sorcery').
card_original_text('icequake'/'ICE', 'Destroy target land. If that land is a snow-covered land, Icequake deals 1 damage to the land\'s controller.').
card_first_print('icequake', 'ICE').
card_image_name('icequake'/'ICE', 'icequake').
card_uid('icequake'/'ICE', 'ICE:Icequake:icequake').
card_rarity('icequake'/'ICE', 'Uncommon').
card_artist('icequake'/'ICE', 'Richard Kane Ferguson').
card_flavor_text('icequake'/'ICE', '\"When the earth shakes, and their animals are swallowed up by the ground, perhaps they will think twice before attacking again.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('icequake'/'ICE', '2458').

card_in_set('icy manipulator', 'ICE').
card_original_type('icy manipulator'/'ICE', 'Artifact').
card_original_text('icy manipulator'/'ICE', '{1},{T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'ICE', 'icy manipulator').
card_uid('icy manipulator'/'ICE', 'ICE:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'ICE', 'Uncommon').
card_artist('icy manipulator'/'ICE', 'Amy Weber').
card_flavor_text('icy manipulator'/'ICE', '\"The scavengers who first found it called it the ‘Bone Crank.\' Not a bad name, I\'d say.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('icy manipulator'/'ICE', '2408').

card_in_set('icy prison', 'ICE').
card_original_type('icy prison'/'ICE', 'Enchantment').
card_original_text('icy prison'/'ICE', 'When Icy Prison comes into play, remove target creature from the game. When Icy Prison leaves play, return that creature to play under its owner\'s control as though it were just summoned. During your upkeep, destroy Icy Prison. Any player may pay {3} to prevent this.').
card_first_print('icy prison', 'ICE').
card_image_name('icy prison'/'ICE', 'icy prison').
card_uid('icy prison'/'ICE', 'ICE:Icy Prison:icy prison').
card_rarity('icy prison'/'ICE', 'Rare').
card_artist('icy prison'/'ICE', 'Anson Maddocks').
card_multiverse_id('icy prison'/'ICE', '2510').

card_in_set('illusionary forces', 'ICE').
card_original_type('illusionary forces'/'ICE', 'Summon — Illusion').
card_original_text('illusionary forces'/'ICE', 'Flying\nCumulative Upkeep: {U}').
card_first_print('illusionary forces', 'ICE').
card_image_name('illusionary forces'/'ICE', 'illusionary forces').
card_uid('illusionary forces'/'ICE', 'ICE:Illusionary Forces:illusionary forces').
card_rarity('illusionary forces'/'ICE', 'Common').
card_artist('illusionary forces'/'ICE', 'Justin Hampton').
card_flavor_text('illusionary forces'/'ICE', '\"This school was founded in secret, operates in secret, and exists for the teaching of secrets. Those who would alter reality must first escape it.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('illusionary forces'/'ICE', '2511').

card_in_set('illusionary presence', 'ICE').
card_original_type('illusionary presence'/'ICE', 'Summon — Illusion').
card_original_text('illusionary presence'/'ICE', 'Cumulative Upkeep: {U}\nDuring your upkeep, Illusionary Presence gains a landwalk ability of your choice until end of turn.').
card_first_print('illusionary presence', 'ICE').
card_image_name('illusionary presence'/'ICE', 'illusionary presence').
card_uid('illusionary presence'/'ICE', 'ICE:Illusionary Presence:illusionary presence').
card_rarity('illusionary presence'/'ICE', 'Rare').
card_artist('illusionary presence'/'ICE', 'Kaja Foglio').
card_flavor_text('illusionary presence'/'ICE', '\"We could feel the Shaman\'s power, as surely as we felt the storm.\"\n—Disa the Restless, journal entry').
card_multiverse_id('illusionary presence'/'ICE', '2512').

card_in_set('illusionary terrain', 'ICE').
card_original_type('illusionary terrain'/'ICE', 'Enchantment').
card_original_text('illusionary terrain'/'ICE', 'Cumulative Upkeep: {2}\nAll basic lands of one type become basic lands of a different type of your choice.').
card_first_print('illusionary terrain', 'ICE').
card_image_name('illusionary terrain'/'ICE', 'illusionary terrain').
card_uid('illusionary terrain'/'ICE', 'ICE:Illusionary Terrain:illusionary terrain').
card_rarity('illusionary terrain'/'ICE', 'Uncommon').
card_artist('illusionary terrain'/'ICE', 'Rob Alexander').
card_flavor_text('illusionary terrain'/'ICE', '\"The drawing of maps is fruitless in the west near the ice walls; the very earth is formless.\"\n—Disa the Restless, journal entry').
card_multiverse_id('illusionary terrain'/'ICE', '2513').

card_in_set('illusionary wall', 'ICE').
card_original_type('illusionary wall'/'ICE', 'Summon — Wall').
card_original_text('illusionary wall'/'ICE', 'Flying, first strike\nCumulative Upkeep: {U}').
card_first_print('illusionary wall', 'ICE').
card_image_name('illusionary wall'/'ICE', 'illusionary wall').
card_uid('illusionary wall'/'ICE', 'ICE:Illusionary Wall:illusionary wall').
card_rarity('illusionary wall'/'ICE', 'Common').
card_artist('illusionary wall'/'ICE', 'Mark Poole').
card_flavor_text('illusionary wall'/'ICE', '\"Let them see what is not there and feel what does not touch them. When they no longer trust their senses, that is the time to strike.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('illusionary wall'/'ICE', '2514').

card_in_set('illusions of grandeur', 'ICE').
card_original_type('illusions of grandeur'/'ICE', 'Enchantment').
card_original_text('illusions of grandeur'/'ICE', 'Cumulative Upkeep: {2}\nWhen Illusions of Grandeur comes into play, gain 20 life. When Illusions of Grandeur leaves play, lose 20 life. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('illusions of grandeur', 'ICE').
card_image_name('illusions of grandeur'/'ICE', 'illusions of grandeur').
card_uid('illusions of grandeur'/'ICE', 'ICE:Illusions of Grandeur:illusions of grandeur').
card_rarity('illusions of grandeur'/'ICE', 'Rare').
card_artist('illusions of grandeur'/'ICE', 'Quinton Hoover').
card_multiverse_id('illusions of grandeur'/'ICE', '2515').

card_in_set('imposing visage', 'ICE').
card_original_type('imposing visage'/'ICE', 'Enchant Creature').
card_original_text('imposing visage'/'ICE', 'Target creature cannot be blocked by only one creature.').
card_first_print('imposing visage', 'ICE').
card_image_name('imposing visage'/'ICE', 'imposing visage').
card_uid('imposing visage'/'ICE', 'ICE:Imposing Visage:imposing visage').
card_rarity('imposing visage'/'ICE', 'Common').
card_artist('imposing visage'/'ICE', 'Phil Foglio').
card_flavor_text('imposing visage'/'ICE', '\"I can\'t believe they expect me to fight with this rabble. A Goblin in a big mask sends ‘em running for cover.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('imposing visage'/'ICE', '2629').

card_in_set('incinerate', 'ICE').
card_original_type('incinerate'/'ICE', 'Instant').
card_original_text('incinerate'/'ICE', 'Incinerate deals 3 damage to target creature or player. No creature damaged by Incinerate can regenerate this turn.').
card_image_name('incinerate'/'ICE', 'incinerate').
card_uid('incinerate'/'ICE', 'ICE:Incinerate:incinerate').
card_rarity('incinerate'/'ICE', 'Common').
card_artist('incinerate'/'ICE', 'Mark Poole').
card_flavor_text('incinerate'/'ICE', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('incinerate'/'ICE', '2630').

card_in_set('infernal darkness', 'ICE').
card_original_type('infernal darkness'/'ICE', 'Enchantment').
card_original_text('infernal darkness'/'ICE', 'Cumulative Upkeep: {B} and 1 life\nAll mana-producing lands produce {B} instead of their normal mana.').
card_first_print('infernal darkness', 'ICE').
card_image_name('infernal darkness'/'ICE', 'infernal darkness').
card_uid('infernal darkness'/'ICE', 'ICE:Infernal Darkness:infernal darkness').
card_rarity('infernal darkness'/'ICE', 'Rare').
card_artist('infernal darkness'/'ICE', 'Phil Foglio').
card_flavor_text('infernal darkness'/'ICE', '\"I thought the day had brought enough horrors for our ragged band, but the night was far worse.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('infernal darkness'/'ICE', '2459').

card_in_set('infernal denizen', 'ICE').
card_original_type('infernal denizen'/'ICE', 'Summon — Infernal Denizen').
card_original_text('infernal denizen'/'ICE', 'During your upkeep, sacrifice two swamps. If you cannot, tap Infernal Denizen, and target opponent may gain control of target creature of his or her choice you control. The opponent loses control of that creature if Infernal Denizen leaves play.\n{T}: Gain control of target creature. Lose control of that creature if Infernal Denizen leaves play.').
card_first_print('infernal denizen', 'ICE').
card_image_name('infernal denizen'/'ICE', 'infernal denizen').
card_uid('infernal denizen'/'ICE', 'ICE:Infernal Denizen:infernal denizen').
card_rarity('infernal denizen'/'ICE', 'Rare').
card_artist('infernal denizen'/'ICE', 'Drew Tucker').
card_multiverse_id('infernal denizen'/'ICE', '2460').

card_in_set('infinite hourglass', 'ICE').
card_original_type('infinite hourglass'/'ICE', 'Artifact').
card_original_text('infinite hourglass'/'ICE', 'During your upkeep, put a time counter on Infinite Hourglass. During any upkeep, any player may pay {3} to remove a time counter from Infinite Hourglass. All creatures get +1/+0 for each time counter on Infinite Hourglass.').
card_first_print('infinite hourglass', 'ICE').
card_image_name('infinite hourglass'/'ICE', 'infinite hourglass').
card_uid('infinite hourglass'/'ICE', 'ICE:Infinite Hourglass:infinite hourglass').
card_rarity('infinite hourglass'/'ICE', 'Rare').
card_artist('infinite hourglass'/'ICE', 'Harold McNeill').
card_multiverse_id('infinite hourglass'/'ICE', '2409').

card_in_set('infuse', 'ICE').
card_original_type('infuse'/'ICE', 'Instant').
card_original_text('infuse'/'ICE', 'Untap target artifact, creature, or land. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('infuse', 'ICE').
card_image_name('infuse'/'ICE', 'infuse').
card_uid('infuse'/'ICE', 'ICE:Infuse:infuse').
card_rarity('infuse'/'ICE', 'Common').
card_artist('infuse'/'ICE', 'Randy Gallegos').
card_flavor_text('infuse'/'ICE', '\"The potential for change lies in all things. Know a thing\'s nature, know its possibilities, and know it anew.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('infuse'/'ICE', '2516').

card_in_set('island', 'ICE').
card_original_type('island'/'ICE', 'Land').
card_original_text('island'/'ICE', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'ICE', 'island1').
card_uid('island'/'ICE', 'ICE:Island:island1').
card_rarity('island'/'ICE', 'Basic Land').
card_artist('island'/'ICE', 'Anson Maddocks').
card_multiverse_id('island'/'ICE', '2768').

card_in_set('island', 'ICE').
card_original_type('island'/'ICE', 'Land').
card_original_text('island'/'ICE', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'ICE', 'island2').
card_uid('island'/'ICE', 'ICE:Island:island2').
card_rarity('island'/'ICE', 'Basic Land').
card_artist('island'/'ICE', 'Anson Maddocks').
card_multiverse_id('island'/'ICE', '2767').

card_in_set('island', 'ICE').
card_original_type('island'/'ICE', 'Land').
card_original_text('island'/'ICE', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'ICE', 'island3').
card_uid('island'/'ICE', 'ICE:Island:island3').
card_rarity('island'/'ICE', 'Basic Land').
card_artist('island'/'ICE', 'Anson Maddocks').
card_multiverse_id('island'/'ICE', '2769').

card_in_set('jester\'s cap', 'ICE').
card_original_type('jester\'s cap'/'ICE', 'Artifact').
card_original_text('jester\'s cap'/'ICE', '{2},{T}: Sacrifice Jester\'s Cap to look through target player\'s library and remove any three of those cards from the game. Reshuffle that library afterwards.').
card_first_print('jester\'s cap', 'ICE').
card_image_name('jester\'s cap'/'ICE', 'jester\'s cap').
card_uid('jester\'s cap'/'ICE', 'ICE:Jester\'s Cap:jester\'s cap').
card_rarity('jester\'s cap'/'ICE', 'Rare').
card_artist('jester\'s cap'/'ICE', 'Dan Frazier').
card_flavor_text('jester\'s cap'/'ICE', '\"Know your foes\' strengths as well as their weaknesses.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('jester\'s cap'/'ICE', '2410').

card_in_set('jester\'s mask', 'ICE').
card_original_type('jester\'s mask'/'ICE', 'Artifact').
card_original_text('jester\'s mask'/'ICE', 'Comes into play tapped.\n{1},{T}: Sacrifice Jester\'s Mask to look through target opponent\'s hand and library. Give that player a new hand of as many cards as he or she had before. Reshuffle the remaining cards afterwards.').
card_first_print('jester\'s mask', 'ICE').
card_image_name('jester\'s mask'/'ICE', 'jester\'s mask').
card_uid('jester\'s mask'/'ICE', 'ICE:Jester\'s Mask:jester\'s mask').
card_rarity('jester\'s mask'/'ICE', 'Rare').
card_artist('jester\'s mask'/'ICE', 'Dan Frazier').
card_multiverse_id('jester\'s mask'/'ICE', '2411').

card_in_set('jeweled amulet', 'ICE').
card_original_type('jeweled amulet'/'ICE', 'Artifact').
card_original_text('jeweled amulet'/'ICE', '{1},{T}: Put a charge counter on Jeweled Amulet. Note what type of mana was used to pay this activation cost. Use this ability only if there are no charge counters on Jeweled Amulet.\n{T}: Remove the charge counter from Jeweled Amulet to add one mana of the type last used to put a charge counter on Jeweled Amulet to your mana pool. Play this ability as an interrupt.').
card_first_print('jeweled amulet', 'ICE').
card_image_name('jeweled amulet'/'ICE', 'jeweled amulet').
card_uid('jeweled amulet'/'ICE', 'ICE:Jeweled Amulet:jeweled amulet').
card_rarity('jeweled amulet'/'ICE', 'Uncommon').
card_artist('jeweled amulet'/'ICE', 'Dan Frazier').
card_multiverse_id('jeweled amulet'/'ICE', '2412').

card_in_set('johtull wurm', 'ICE').
card_original_type('johtull wurm'/'ICE', 'Summon — Wurm').
card_original_text('johtull wurm'/'ICE', 'For each blocking creature assigned to Johtull Worm beyond the first, Johtull Worm gets -2/-1 until end of turn.').
card_first_print('johtull wurm', 'ICE').
card_image_name('johtull wurm'/'ICE', 'johtull wurm').
card_uid('johtull wurm'/'ICE', 'ICE:Johtull Wurm:johtull wurm').
card_rarity('johtull wurm'/'ICE', 'Uncommon').
card_artist('johtull wurm'/'ICE', 'Daniel Gelon').
card_flavor_text('johtull wurm'/'ICE', '\"To bring her down we must be on all sides at once—leave one avenue open and we\'ll all be dead.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('johtull wurm'/'ICE', '2574').

card_in_set('jokulhaups', 'ICE').
card_original_type('jokulhaups'/'ICE', 'Sorcery').
card_original_text('jokulhaups'/'ICE', 'Bury all artifacts, creatures, and lands.').
card_first_print('jokulhaups', 'ICE').
card_image_name('jokulhaups'/'ICE', 'jokulhaups').
card_uid('jokulhaups'/'ICE', 'ICE:Jokulhaups:jokulhaups').
card_rarity('jokulhaups'/'ICE', 'Rare').
card_artist('jokulhaups'/'ICE', 'Richard Thomas').
card_flavor_text('jokulhaups'/'ICE', '\"I was shocked when I first saw the aftermath of the Yavimaya Valley disaster. The raging waters had swept away trees, bridges, and even houses. My healers had much work to do.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('jokulhaups'/'ICE', '2631').

card_in_set('juniper order druid', 'ICE').
card_original_type('juniper order druid'/'ICE', 'Summon — Cleric').
card_original_text('juniper order druid'/'ICE', '{T}: Untap target land. Play this ability as an interrupt.').
card_first_print('juniper order druid', 'ICE').
card_image_name('juniper order druid'/'ICE', 'juniper order druid').
card_uid('juniper order druid'/'ICE', 'ICE:Juniper Order Druid:juniper order druid').
card_rarity('juniper order druid'/'ICE', 'Common').
card_artist('juniper order druid'/'ICE', 'Jeff A. Menges').
card_flavor_text('juniper order druid'/'ICE', '\"The filthy towns of Kjeldor are no place for anyone to live. Fyndhorn is our home now.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('juniper order druid'/'ICE', '2575').

card_in_set('justice', 'ICE').
card_original_type('justice'/'ICE', 'Enchantment').
card_original_text('justice'/'ICE', 'During your upkeep, pay {W}{W} or destroy Justice. Whenever a red creature or spell deals damage, Justice deals an equal amount of damage to the controller of that creature or spell. If another spell or effect reduces the amount of damage a red creature or spell deals, it does not reduce the amount of damage dealt by Justice.').
card_first_print('justice', 'ICE').
card_image_name('justice'/'ICE', 'justice').
card_uid('justice'/'ICE', 'ICE:Justice:justice').
card_rarity('justice'/'ICE', 'Uncommon').
card_artist('justice'/'ICE', 'Ruth Thompson').
card_multiverse_id('justice'/'ICE', '2692').

card_in_set('karplusan forest', 'ICE').
card_original_type('karplusan forest'/'ICE', 'Land').
card_original_text('karplusan forest'/'ICE', '{T}: Add {1} to your mana pool.\n{T}: Add {R} to your mana pool. Karplusan Forest deals 1 damage to you.\n{T}: Add {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_first_print('karplusan forest', 'ICE').
card_image_name('karplusan forest'/'ICE', 'karplusan forest').
card_uid('karplusan forest'/'ICE', 'ICE:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'ICE', 'Rare').
card_artist('karplusan forest'/'ICE', 'Nicola Leonard').
card_multiverse_id('karplusan forest'/'ICE', '2755').

card_in_set('karplusan giant', 'ICE').
card_original_type('karplusan giant'/'ICE', 'Summon — Giant').
card_original_text('karplusan giant'/'ICE', '{0}: Tap target snow-covered land you control to give Karplusan Giant +1/+1 until end of turn.').
card_first_print('karplusan giant', 'ICE').
card_image_name('karplusan giant'/'ICE', 'karplusan giant').
card_uid('karplusan giant'/'ICE', 'ICE:Karplusan Giant:karplusan giant').
card_rarity('karplusan giant'/'ICE', 'Uncommon').
card_artist('karplusan giant'/'ICE', 'Daniel Gelon').
card_flavor_text('karplusan giant'/'ICE', '\"They aren\'t the brightest or the quickest of Giants. For that matter, the same holds true if you compare them to rocks.\"\n—Disa the Restless, journal entry').
card_multiverse_id('karplusan giant'/'ICE', '2632').

card_in_set('karplusan yeti', 'ICE').
card_original_type('karplusan yeti'/'ICE', 'Summon — Yeti').
card_original_text('karplusan yeti'/'ICE', '{T}: Karplusan Yeti deals an amount of damage equal to its power to target creature. That creature deals an amount of damage equal to its power to Karplusan Yeti.').
card_first_print('karplusan yeti', 'ICE').
card_image_name('karplusan yeti'/'ICE', 'karplusan yeti').
card_uid('karplusan yeti'/'ICE', 'ICE:Karplusan Yeti:karplusan yeti').
card_rarity('karplusan yeti'/'ICE', 'Rare').
card_artist('karplusan yeti'/'ICE', 'Quinton Hoover').
card_flavor_text('karplusan yeti'/'ICE', '\"What\'s that smell?\"\n—Perena Deepcutter,\nDwarven Armorer').
card_multiverse_id('karplusan yeti'/'ICE', '2633').

card_in_set('kelsinko ranger', 'ICE').
card_original_type('kelsinko ranger'/'ICE', 'Summon — Ranger').
card_original_text('kelsinko ranger'/'ICE', '{1}{W} Target green creature gains first strike until end of turn.').
card_first_print('kelsinko ranger', 'ICE').
card_image_name('kelsinko ranger'/'ICE', 'kelsinko ranger').
card_uid('kelsinko ranger'/'ICE', 'ICE:Kelsinko Ranger:kelsinko ranger').
card_rarity('kelsinko ranger'/'ICE', 'Common').
card_artist('kelsinko ranger'/'ICE', 'Mark Poole').
card_flavor_text('kelsinko ranger'/'ICE', '\"Rangers not trained by the Elves just aren\'t the same.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('kelsinko ranger'/'ICE', '2693').

card_in_set('kjeldoran dead', 'ICE').
card_original_type('kjeldoran dead'/'ICE', 'Summon — Dead').
card_original_text('kjeldoran dead'/'ICE', 'When Kjeldoran Dead comes into play, sacrifice a creature.\n{B} Regenerate').
card_first_print('kjeldoran dead', 'ICE').
card_image_name('kjeldoran dead'/'ICE', 'kjeldoran dead').
card_uid('kjeldoran dead'/'ICE', 'ICE:Kjeldoran Dead:kjeldoran dead').
card_rarity('kjeldoran dead'/'ICE', 'Common').
card_artist('kjeldoran dead'/'ICE', 'Melissa A. Benson').
card_flavor_text('kjeldoran dead'/'ICE', '\"They shall kill those whom once they loved.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('kjeldoran dead'/'ICE', '2461').

card_in_set('kjeldoran elite guard', 'ICE').
card_original_type('kjeldoran elite guard'/'ICE', 'Summon — Soldier').
card_original_text('kjeldoran elite guard'/'ICE', '{T}: Target creature gets +2/+2 until end of turn. If that creature leaves play this turn, bury Kjeldoran Elite Guard. Use this ability only when attack or defense is announced.').
card_first_print('kjeldoran elite guard', 'ICE').
card_image_name('kjeldoran elite guard'/'ICE', 'kjeldoran elite guard').
card_uid('kjeldoran elite guard'/'ICE', 'ICE:Kjeldoran Elite Guard:kjeldoran elite guard').
card_rarity('kjeldoran elite guard'/'ICE', 'Uncommon').
card_artist('kjeldoran elite guard'/'ICE', 'Melissa A. Benson').
card_flavor_text('kjeldoran elite guard'/'ICE', 'The winged helms of the Guard are put on for pageants—but taken off for war.').
card_multiverse_id('kjeldoran elite guard'/'ICE', '2694').

card_in_set('kjeldoran frostbeast', 'ICE').
card_original_type('kjeldoran frostbeast'/'ICE', 'Summon — Frostbeast').
card_original_text('kjeldoran frostbeast'/'ICE', 'At end of combat, destroy all creatures blocking or blocked by Kjeldoran Frostbeast.').
card_first_print('kjeldoran frostbeast', 'ICE').
card_image_name('kjeldoran frostbeast'/'ICE', 'kjeldoran frostbeast').
card_uid('kjeldoran frostbeast'/'ICE', 'ICE:Kjeldoran Frostbeast:kjeldoran frostbeast').
card_rarity('kjeldoran frostbeast'/'ICE', 'Uncommon').
card_artist('kjeldoran frostbeast'/'ICE', 'Mark Poole').
card_flavor_text('kjeldoran frostbeast'/'ICE', '\"Two of my Warriors found that the creature was dangerous not only in combat, but also in simple proximity.\"\n—Disa the Restless, journal entry').
card_multiverse_id('kjeldoran frostbeast'/'ICE', '2732').

card_in_set('kjeldoran guard', 'ICE').
card_original_type('kjeldoran guard'/'ICE', 'Summon — Soldier').
card_original_text('kjeldoran guard'/'ICE', '{T}: Target creature gets +1/+1 until end of turn. If that creature leaves play this turn, bury Kjeldoran Guard. Use this ability only when attack or defense is announced and only if defending player controls no snow-covered lands.').
card_first_print('kjeldoran guard', 'ICE').
card_image_name('kjeldoran guard'/'ICE', 'kjeldoran guard').
card_uid('kjeldoran guard'/'ICE', 'ICE:Kjeldoran Guard:kjeldoran guard').
card_rarity('kjeldoran guard'/'ICE', 'Common').
card_artist('kjeldoran guard'/'ICE', 'Anthony Waters').
card_multiverse_id('kjeldoran guard'/'ICE', '2695').

card_in_set('kjeldoran knight', 'ICE').
card_original_type('kjeldoran knight'/'ICE', 'Summon — Knight').
card_original_text('kjeldoran knight'/'ICE', 'Banding\n{1}{W} +1/+0 until end of turn\n{W}{W} +0/+2 until end of turn').
card_first_print('kjeldoran knight', 'ICE').
card_image_name('kjeldoran knight'/'ICE', 'kjeldoran knight').
card_uid('kjeldoran knight'/'ICE', 'ICE:Kjeldoran Knight:kjeldoran knight').
card_rarity('kjeldoran knight'/'ICE', 'Rare').
card_artist('kjeldoran knight'/'ICE', 'Ron Spencer').
card_flavor_text('kjeldoran knight'/'ICE', '\"Those who do not ride the wind on Aesthir still command loyalty and respect.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('kjeldoran knight'/'ICE', '2696').

card_in_set('kjeldoran phalanx', 'ICE').
card_original_type('kjeldoran phalanx'/'ICE', 'Summon — Soldiers').
card_original_text('kjeldoran phalanx'/'ICE', 'Banding, first strike').
card_first_print('kjeldoran phalanx', 'ICE').
card_image_name('kjeldoran phalanx'/'ICE', 'kjeldoran phalanx').
card_uid('kjeldoran phalanx'/'ICE', 'ICE:Kjeldoran Phalanx:kjeldoran phalanx').
card_rarity('kjeldoran phalanx'/'ICE', 'Rare').
card_artist('kjeldoran phalanx'/'ICE', 'Richard Kane Ferguson').
card_flavor_text('kjeldoran phalanx'/'ICE', '\"There\'s nothing I like better than watching a street full of soldiers kicking down the doors of the guilty and the impure.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('kjeldoran phalanx'/'ICE', '2697').

card_in_set('kjeldoran royal guard', 'ICE').
card_original_type('kjeldoran royal guard'/'ICE', 'Summon — Soldiers').
card_original_text('kjeldoran royal guard'/'ICE', '{T}: Redirect to Kjeldoran Royal Guard all damage dealt to you from unblocked creatures this turn.').
card_first_print('kjeldoran royal guard', 'ICE').
card_image_name('kjeldoran royal guard'/'ICE', 'kjeldoran royal guard').
card_uid('kjeldoran royal guard'/'ICE', 'ICE:Kjeldoran Royal Guard:kjeldoran royal guard').
card_rarity('kjeldoran royal guard'/'ICE', 'Rare').
card_artist('kjeldoran royal guard'/'ICE', 'L. A. Williams').
card_flavor_text('kjeldoran royal guard'/'ICE', '\"Honorable in battle,\ngenerous in death.\"\n—Motto of the Kjeldoran Royal Guard').
card_multiverse_id('kjeldoran royal guard'/'ICE', '2698').

card_in_set('kjeldoran skycaptain', 'ICE').
card_original_type('kjeldoran skycaptain'/'ICE', 'Summon — Soldier').
card_original_text('kjeldoran skycaptain'/'ICE', 'Banding, flying, first strike').
card_first_print('kjeldoran skycaptain', 'ICE').
card_image_name('kjeldoran skycaptain'/'ICE', 'kjeldoran skycaptain').
card_uid('kjeldoran skycaptain'/'ICE', 'ICE:Kjeldoran Skycaptain:kjeldoran skycaptain').
card_rarity('kjeldoran skycaptain'/'ICE', 'Uncommon').
card_artist('kjeldoran skycaptain'/'ICE', 'Mark Poole').
card_flavor_text('kjeldoran skycaptain'/'ICE', '\"If we do our duty and uphold our honor, Kjeldor shall stand for a thousand years.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('kjeldoran skycaptain'/'ICE', '2699').

card_in_set('kjeldoran skyknight', 'ICE').
card_original_type('kjeldoran skyknight'/'ICE', 'Summon — Soldier').
card_original_text('kjeldoran skyknight'/'ICE', 'Banding, flying, first strike').
card_first_print('kjeldoran skyknight', 'ICE').
card_image_name('kjeldoran skyknight'/'ICE', 'kjeldoran skyknight').
card_uid('kjeldoran skyknight'/'ICE', 'ICE:Kjeldoran Skyknight:kjeldoran skyknight').
card_rarity('kjeldoran skyknight'/'ICE', 'Common').
card_artist('kjeldoran skyknight'/'ICE', 'Mark Poole').
card_flavor_text('kjeldoran skyknight'/'ICE', '\"My Aesthir is my most trusted ally. We fight as one and live as one, and we will die as one.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('kjeldoran skyknight'/'ICE', '2700').

card_in_set('kjeldoran warrior', 'ICE').
card_original_type('kjeldoran warrior'/'ICE', 'Summon — Hero').
card_original_text('kjeldoran warrior'/'ICE', 'Banding').
card_first_print('kjeldoran warrior', 'ICE').
card_image_name('kjeldoran warrior'/'ICE', 'kjeldoran warrior').
card_uid('kjeldoran warrior'/'ICE', 'ICE:Kjeldoran Warrior:kjeldoran warrior').
card_rarity('kjeldoran warrior'/'ICE', 'Common').
card_artist('kjeldoran warrior'/'ICE', 'Mark Poole').
card_flavor_text('kjeldoran warrior'/'ICE', '\"Give me a thousand such Warriors and I could change the world.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('kjeldoran warrior'/'ICE', '2701').

card_in_set('knight of stromgald', 'ICE').
card_original_type('knight of stromgald'/'ICE', 'Summon — Knight').
card_original_text('knight of stromgald'/'ICE', 'Protection from white\n{B}{B}: +1/+0 until end of turn\n{B}: First strike until end of turn').
card_first_print('knight of stromgald', 'ICE').
card_image_name('knight of stromgald'/'ICE', 'knight of stromgald').
card_uid('knight of stromgald'/'ICE', 'ICE:Knight of Stromgald:knight of stromgald').
card_rarity('knight of stromgald'/'ICE', 'Uncommon').
card_artist('knight of stromgald'/'ICE', 'Mark Poole').
card_flavor_text('knight of stromgald'/'ICE', '\"Kjeldorans should rule supreme, and to the rest, death!\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('knight of stromgald'/'ICE', '2462').

card_in_set('krovikan elementalist', 'ICE').
card_original_type('krovikan elementalist'/'ICE', 'Summon — Wizard').
card_original_text('krovikan elementalist'/'ICE', '{2}{R} Target creature gets +1/+0 until end of turn.\n{U}{U} Target creature you control gains flying until end of turn. At end of turn, bury that creature.').
card_first_print('krovikan elementalist', 'ICE').
card_image_name('krovikan elementalist'/'ICE', 'krovikan elementalist').
card_uid('krovikan elementalist'/'ICE', 'ICE:Krovikan Elementalist:krovikan elementalist').
card_rarity('krovikan elementalist'/'ICE', 'Uncommon').
card_artist('krovikan elementalist'/'ICE', 'Douglas Shuler').
card_flavor_text('krovikan elementalist'/'ICE', '\"The Elementalists\' talents could be useful if turned to other purposes.\"\n—Zur the Enchanter').
card_multiverse_id('krovikan elementalist'/'ICE', '2463').

card_in_set('krovikan fetish', 'ICE').
card_original_type('krovikan fetish'/'ICE', 'Enchant Creature').
card_original_text('krovikan fetish'/'ICE', 'Draw a card at the beginning of the upkeep of the turn after Krovikan Fetish comes into play.\nTarget creature gets +1/+1.').
card_first_print('krovikan fetish', 'ICE').
card_image_name('krovikan fetish'/'ICE', 'krovikan fetish').
card_uid('krovikan fetish'/'ICE', 'ICE:Krovikan Fetish:krovikan fetish').
card_rarity('krovikan fetish'/'ICE', 'Common').
card_artist('krovikan fetish'/'ICE', 'Heather Hudson').
card_flavor_text('krovikan fetish'/'ICE', 'Some Krovikans find strength in the ears and eyes of their victims and wear such fetishes into battle.').
card_multiverse_id('krovikan fetish'/'ICE', '2464').

card_in_set('krovikan sorcerer', 'ICE').
card_original_type('krovikan sorcerer'/'ICE', 'Summon — Wizard').
card_original_text('krovikan sorcerer'/'ICE', '{T}: Choose and discard a card from your hand to draw a card. If the card discarded was black, draw two cards instead of one; keep one and discard the other.').
card_first_print('krovikan sorcerer', 'ICE').
card_image_name('krovikan sorcerer'/'ICE', 'krovikan sorcerer').
card_uid('krovikan sorcerer'/'ICE', 'ICE:Krovikan Sorcerer:krovikan sorcerer').
card_rarity('krovikan sorcerer'/'ICE', 'Common').
card_artist('krovikan sorcerer'/'ICE', 'Pat Morrissey').
card_flavor_text('krovikan sorcerer'/'ICE', '\"These Sorcerers always seem to have another surprise up their sleeves.\"\n—Zur the Enchanter').
card_multiverse_id('krovikan sorcerer'/'ICE', '2517').

card_in_set('krovikan vampire', 'ICE').
card_original_type('krovikan vampire'/'ICE', 'Summon — Vampire').
card_original_text('krovikan vampire'/'ICE', 'At the end of a turn in which any creature is damaged by Krovikan Vampire and put into any graveyard, put that creature directly into play under your control. Treat the creature as though it were just summoned. If you lose control of Krovikan Vampire or Krovikan Vampire leaves play, bury the creature.').
card_first_print('krovikan vampire', 'ICE').
card_image_name('krovikan vampire'/'ICE', 'krovikan vampire').
card_uid('krovikan vampire'/'ICE', 'ICE:Krovikan Vampire:krovikan vampire').
card_rarity('krovikan vampire'/'ICE', 'Uncommon').
card_artist('krovikan vampire'/'ICE', 'Quinton Hoover').
card_multiverse_id('krovikan vampire'/'ICE', '2465').

card_in_set('land cap', 'ICE').
card_original_type('land cap'/'ICE', 'Land').
card_original_text('land cap'/'ICE', 'If there are any depletion counters on Land Cap, it does not untap during your untap phase. At the beginning of your upkeep, remove a depletion counter from Land Cap.\n{T}: Add {W} to your mana pool. Put a depletion counter on Land Cap.\n{T}: Add {U} to your mana pool. Put a depletion counter on Land Cap.').
card_first_print('land cap', 'ICE').
card_image_name('land cap'/'ICE', 'land cap').
card_uid('land cap'/'ICE', 'ICE:Land Cap:land cap').
card_rarity('land cap'/'ICE', 'Rare').
card_artist('land cap'/'ICE', 'L. A. Williams').
card_multiverse_id('land cap'/'ICE', '2756').

card_in_set('lapis lazuli talisman', 'ICE').
card_original_type('lapis lazuli talisman'/'ICE', 'Artifact').
card_original_text('lapis lazuli talisman'/'ICE', '{3}: Untap target permanent.\nUse this ability only when a blue spell is successfully cast and only once for each blue spell cast.').
card_first_print('lapis lazuli talisman', 'ICE').
card_image_name('lapis lazuli talisman'/'ICE', 'lapis lazuli talisman').
card_uid('lapis lazuli talisman'/'ICE', 'ICE:Lapis Lazuli Talisman:lapis lazuli talisman').
card_rarity('lapis lazuli talisman'/'ICE', 'Uncommon').
card_artist('lapis lazuli talisman'/'ICE', 'Amy Weber').
card_multiverse_id('lapis lazuli talisman'/'ICE', '2413').

card_in_set('lava burst', 'ICE').
card_original_type('lava burst'/'ICE', 'Sorcery').
card_original_text('lava burst'/'ICE', 'Lava Burst deals X damage to target creature or player. Effects that prevent or redirect damage cannot be used to protect that creature.').
card_first_print('lava burst', 'ICE').
card_image_name('lava burst'/'ICE', 'lava burst').
card_uid('lava burst'/'ICE', 'ICE:Lava Burst:lava burst').
card_rarity('lava burst'/'ICE', 'Common').
card_artist('lava burst'/'ICE', 'Tom Wänerstrand').
card_flavor_text('lava burst'/'ICE', '\"Overkill? This isn\'t a game of Kick-the-Ouphe!\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('lava burst'/'ICE', '2634').

card_in_set('lava tubes', 'ICE').
card_original_type('lava tubes'/'ICE', 'Land').
card_original_text('lava tubes'/'ICE', 'If there are any depletion counters on Lava Tubes, it does not untap during your untap phase. At the beginning of your upkeep, remove a depletion counter from Lava Tubes.\n{T}: Add {B} to your mana pool. Put a depletion counter on Lava Tubes.\n{T}: Add {R} to your mana pool. Put a depletion counter on Lava Tubes.').
card_first_print('lava tubes', 'ICE').
card_image_name('lava tubes'/'ICE', 'lava tubes').
card_uid('lava tubes'/'ICE', 'ICE:Lava Tubes:lava tubes').
card_rarity('lava tubes'/'ICE', 'Rare').
card_artist('lava tubes'/'ICE', 'Bryon Wackwitz').
card_multiverse_id('lava tubes'/'ICE', '2757').

card_in_set('legions of lim-dûl', 'ICE').
card_original_type('legions of lim-dûl'/'ICE', 'Summon — Zombies').
card_original_text('legions of lim-dûl'/'ICE', 'Snow-covered swampwalk').
card_first_print('legions of lim-dûl', 'ICE').
card_image_name('legions of lim-dûl'/'ICE', 'legions of lim-dul').
card_uid('legions of lim-dûl'/'ICE', 'ICE:Legions of Lim-Dûl:legions of lim-dul').
card_rarity('legions of lim-dûl'/'ICE', 'Common').
card_artist('legions of lim-dûl'/'ICE', 'Anson Maddocks').
card_flavor_text('legions of lim-dûl'/'ICE', '\"I have seen the faces of my dead friends among that grim band, and I can bear no more.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield.').
card_multiverse_id('legions of lim-dûl'/'ICE', '2466').

card_in_set('leshrac\'s rite', 'ICE').
card_original_type('leshrac\'s rite'/'ICE', 'Enchant Creature').
card_original_text('leshrac\'s rite'/'ICE', 'Target creature gains swampwalk.').
card_first_print('leshrac\'s rite', 'ICE').
card_image_name('leshrac\'s rite'/'ICE', 'leshrac\'s rite').
card_uid('leshrac\'s rite'/'ICE', 'ICE:Leshrac\'s Rite:leshrac\'s rite').
card_rarity('leshrac\'s rite'/'ICE', 'Uncommon').
card_artist('leshrac\'s rite'/'ICE', 'Richard Thomas').
card_flavor_text('leshrac\'s rite'/'ICE', '\"Bind me to thee, my soul to thine. I am your servant and your slave. I shall hunger for your word and thirst for your blessing. Blood for blood, flesh for flesh, Leshrac, my lord.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('leshrac\'s rite'/'ICE', '2467').

card_in_set('leshrac\'s sigil', 'ICE').
card_original_type('leshrac\'s sigil'/'ICE', 'Enchantment').
card_original_text('leshrac\'s sigil'/'ICE', '{B}{B} When any opponent successfully casts a green spell, look at that player\'s hand and choose a card; he or she then discards that card. Use this ability only once each time a green spell is cast.\n{B}{B} Return Leshrac\'s Sigil to owner\'s hand.').
card_first_print('leshrac\'s sigil', 'ICE').
card_image_name('leshrac\'s sigil'/'ICE', 'leshrac\'s sigil').
card_uid('leshrac\'s sigil'/'ICE', 'ICE:Leshrac\'s Sigil:leshrac\'s sigil').
card_rarity('leshrac\'s sigil'/'ICE', 'Uncommon').
card_artist('leshrac\'s sigil'/'ICE', 'Drew Tucker').
card_multiverse_id('leshrac\'s sigil'/'ICE', '2468').

card_in_set('lhurgoyf', 'ICE').
card_original_type('lhurgoyf'/'ICE', 'Summon — Lhurgoyf').
card_original_text('lhurgoyf'/'ICE', 'Lhurgoyf has power equal to the total number of creatures in all graveyards and toughness equal to 1 plus the total number of creatures in all graveyards.').
card_first_print('lhurgoyf', 'ICE').
card_image_name('lhurgoyf'/'ICE', 'lhurgoyf').
card_uid('lhurgoyf'/'ICE', 'ICE:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'ICE', 'Rare').
card_artist('lhurgoyf'/'ICE', 'Pete Venters').
card_flavor_text('lhurgoyf'/'ICE', '\"Ach! Hans, run! It\'s the Lhurgoyf!\"\n—Saffi Eriksdotter, last words').
card_multiverse_id('lhurgoyf'/'ICE', '2576').

card_in_set('lightning blow', 'ICE').
card_original_type('lightning blow'/'ICE', 'Instant').
card_original_text('lightning blow'/'ICE', 'Target creature gains first strike until end of turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('lightning blow', 'ICE').
card_image_name('lightning blow'/'ICE', 'lightning blow').
card_uid('lightning blow'/'ICE', 'ICE:Lightning Blow:lightning blow').
card_rarity('lightning blow'/'ICE', 'Rare').
card_artist('lightning blow'/'ICE', 'Harold McNeill').
card_flavor_text('lightning blow'/'ICE', '\"If you do it right, they\'ll never know what hit them.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('lightning blow'/'ICE', '2702').

card_in_set('lim-dûl\'s cohort', 'ICE').
card_original_type('lim-dûl\'s cohort'/'ICE', 'Summon — Zombies').
card_original_text('lim-dûl\'s cohort'/'ICE', 'Creatures blocking or blocked by Lim-Dûl\'s Cohort cannot regenerate this turn.').
card_first_print('lim-dûl\'s cohort', 'ICE').
card_image_name('lim-dûl\'s cohort'/'ICE', 'lim-dul\'s cohort').
card_uid('lim-dûl\'s cohort'/'ICE', 'ICE:Lim-Dûl\'s Cohort:lim-dul\'s cohort').
card_rarity('lim-dûl\'s cohort'/'ICE', 'Common').
card_artist('lim-dûl\'s cohort'/'ICE', 'Douglas Shuler').
card_flavor_text('lim-dûl\'s cohort'/'ICE', '\"Join me in the place of power, you risen dead. Join me where the waters weep and the trees have no hearts.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('lim-dûl\'s cohort'/'ICE', '2469').

card_in_set('lim-dûl\'s hex', 'ICE').
card_original_type('lim-dûl\'s hex'/'ICE', 'Enchantment').
card_original_text('lim-dûl\'s hex'/'ICE', 'During your upkeep, Lim-Dûl\'s Hex deals 1 damage to each player. Each player may pay {B} or {3} to prevent the damage to himself or herself.').
card_first_print('lim-dûl\'s hex', 'ICE').
card_image_name('lim-dûl\'s hex'/'ICE', 'lim-dul\'s hex').
card_uid('lim-dûl\'s hex'/'ICE', 'ICE:Lim-Dûl\'s Hex:lim-dul\'s hex').
card_rarity('lim-dûl\'s hex'/'ICE', 'Uncommon').
card_artist('lim-dûl\'s hex'/'ICE', 'Liz Danforth').
card_flavor_text('lim-dûl\'s hex'/'ICE', '\"The weak shall fall. The strong shall remain.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('lim-dûl\'s hex'/'ICE', '2470').

card_in_set('lost order of jarkeld', 'ICE').
card_original_type('lost order of jarkeld'/'ICE', 'Summon — Knights').
card_original_text('lost order of jarkeld'/'ICE', 'Lost Order of Jarkeld has power and toughness each equal to 1 plus the number of creatures target opponent controls.').
card_first_print('lost order of jarkeld', 'ICE').
card_image_name('lost order of jarkeld'/'ICE', 'lost order of jarkeld').
card_uid('lost order of jarkeld'/'ICE', 'ICE:Lost Order of Jarkeld:lost order of jarkeld').
card_rarity('lost order of jarkeld'/'ICE', 'Rare').
card_artist('lost order of jarkeld'/'ICE', 'Andi Rusu').
card_flavor_text('lost order of jarkeld'/'ICE', '\"Let us remember brave Jarkeld and his troops, who perished in the Adarkar Wastes so long ago.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('lost order of jarkeld'/'ICE', '2703').

card_in_set('lure', 'ICE').
card_original_type('lure'/'ICE', 'Enchant Creature').
card_original_text('lure'/'ICE', 'All creatures able to block target creature must do so. Lure does not prevent a creature from blocking more than one creature if blocker has that ability. If blocker is forced to block more creatures than it is allowed to, defender chooses which of these creatures to block, but must block as many creatures as allowed.').
card_image_name('lure'/'ICE', 'lure').
card_uid('lure'/'ICE', 'ICE:Lure:lure').
card_rarity('lure'/'ICE', 'Uncommon').
card_artist('lure'/'ICE', 'Phil Foglio').
card_multiverse_id('lure'/'ICE', '2577').

card_in_set('maddening wind', 'ICE').
card_original_type('maddening wind'/'ICE', 'Enchant Creature').
card_original_text('maddening wind'/'ICE', 'Cumulative Upkeep: {G}\nDuring target creature\'s controller\'s upkeep, Maddening Wind deals 2 damage to that player.').
card_first_print('maddening wind', 'ICE').
card_image_name('maddening wind'/'ICE', 'maddening wind').
card_uid('maddening wind'/'ICE', 'ICE:Maddening Wind:maddening wind').
card_rarity('maddening wind'/'ICE', 'Uncommon').
card_artist('maddening wind'/'ICE', 'Dameon Willich').
card_flavor_text('maddening wind'/'ICE', '\"It is Freyalise, walking on the world.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('maddening wind'/'ICE', '2578').

card_in_set('magus of the unseen', 'ICE').
card_original_type('magus of the unseen'/'ICE', 'Summon — Wizard').
card_original_text('magus of the unseen'/'ICE', '{1}{U},{T}: Untap target artifact opponent controls and gain control of it until end of turn. If that artifact is an artifact creature, it can attack, and you may use any of its abilities that require {T} as part of the activation cost. When you lose control of the artifact, tap it.').
card_first_print('magus of the unseen', 'ICE').
card_image_name('magus of the unseen'/'ICE', 'magus of the unseen').
card_uid('magus of the unseen'/'ICE', 'ICE:Magus of the Unseen:magus of the unseen').
card_rarity('magus of the unseen'/'ICE', 'Rare').
card_artist('magus of the unseen'/'ICE', 'Kaja Foglio').
card_multiverse_id('magus of the unseen'/'ICE', '2518').

card_in_set('malachite talisman', 'ICE').
card_original_type('malachite talisman'/'ICE', 'Artifact').
card_original_text('malachite talisman'/'ICE', '{3}: Untap target permanent.\nUse this ability only when a green spell is successfully cast and only once for each green spell cast.').
card_first_print('malachite talisman', 'ICE').
card_image_name('malachite talisman'/'ICE', 'malachite talisman').
card_uid('malachite talisman'/'ICE', 'ICE:Malachite Talisman:malachite talisman').
card_rarity('malachite talisman'/'ICE', 'Uncommon').
card_artist('malachite talisman'/'ICE', 'Christopher Rush').
card_multiverse_id('malachite talisman'/'ICE', '2414').

card_in_set('márton stromgald', 'ICE').
card_original_type('márton stromgald'/'ICE', 'Summon — Legend').
card_original_text('márton stromgald'/'ICE', 'If Márton Stromgald attacks, all other attacking creatures get +*/+* until end of turn, where * is equal to the number of other attacking creatures. If Márton blocks, all other blocking creatures get +*/+* until end of turn, where * is equal to the number of other blocking creatures.').
card_first_print('márton stromgald', 'ICE').
card_image_name('márton stromgald'/'ICE', 'marton stromgald').
card_uid('márton stromgald'/'ICE', 'ICE:Márton Stromgald:marton stromgald').
card_rarity('márton stromgald'/'ICE', 'Rare').
card_artist('márton stromgald'/'ICE', 'Mark Poole').
card_multiverse_id('márton stromgald'/'ICE', '2635').

card_in_set('melee', 'ICE').
card_original_type('melee'/'ICE', 'Instant').
card_original_text('melee'/'ICE', 'Cast only on your turn during combat before defense is chosen. Choose how attacking creatures you control are blocked; all defense must be legal. After declaring blocking, untap any unblocked attacking creature. Treat those creatures as though they had not attacked.').
card_first_print('melee', 'ICE').
card_image_name('melee'/'ICE', 'melee').
card_uid('melee'/'ICE', 'ICE:Melee:melee').
card_rarity('melee'/'ICE', 'Uncommon').
card_artist('melee'/'ICE', 'Dameon Willich').
card_multiverse_id('melee'/'ICE', '2636').

card_in_set('melting', 'ICE').
card_original_type('melting'/'ICE', 'Enchantment').
card_original_text('melting'/'ICE', 'All snow-covered lands become non-snow-covered lands of the same type.').
card_first_print('melting', 'ICE').
card_image_name('melting'/'ICE', 'melting').
card_uid('melting'/'ICE', 'ICE:Melting:melting').
card_rarity('melting'/'ICE', 'Uncommon').
card_artist('melting'/'ICE', 'Randy Gallegos').
card_flavor_text('melting'/'ICE', '\"Who needs the sun when you\'ve got me around?\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('melting'/'ICE', '2637').

card_in_set('mercenaries', 'ICE').
card_original_type('mercenaries'/'ICE', 'Summon — Mercenaries').
card_original_text('mercenaries'/'ICE', 'Whenever Mercenaries damages a player, that player may pay {3} to prevent that damage.').
card_first_print('mercenaries', 'ICE').
card_image_name('mercenaries'/'ICE', 'mercenaries').
card_uid('mercenaries'/'ICE', 'ICE:Mercenaries:mercenaries').
card_rarity('mercenaries'/'ICE', 'Rare').
card_artist('mercenaries'/'ICE', 'Cornelius Brudi').
card_flavor_text('mercenaries'/'ICE', '\"Blasted amateurs! Doesn\'t the queen think we can fight well enough on our own?\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('mercenaries'/'ICE', '2704').

card_in_set('merieke ri berit', 'ICE').
card_original_type('merieke ri berit'/'ICE', 'Summon — Legend').
card_original_text('merieke ri berit'/'ICE', 'Does not untap during your untap phase.\n{T}: Gain control of target creature. Lose control of that creature if you lose control of Merieke Ri Berit. If Merieke Ri Berit leaves play or becomes untapped, bury the creature.').
card_first_print('merieke ri berit', 'ICE').
card_image_name('merieke ri berit'/'ICE', 'merieke ri berit').
card_uid('merieke ri berit'/'ICE', 'ICE:Merieke Ri Berit:merieke ri berit').
card_rarity('merieke ri berit'/'ICE', 'Rare').
card_artist('merieke ri berit'/'ICE', 'Heather Hudson').
card_multiverse_id('merieke ri berit'/'ICE', '2733').

card_in_set('mesmeric trance', 'ICE').
card_original_type('mesmeric trance'/'ICE', 'Enchantment').
card_original_text('mesmeric trance'/'ICE', 'Cumulative Upkeep: {1}\n{U} Discard a card from your hand to draw a card.').
card_first_print('mesmeric trance', 'ICE').
card_image_name('mesmeric trance'/'ICE', 'mesmeric trance').
card_uid('mesmeric trance'/'ICE', 'ICE:Mesmeric Trance:mesmeric trance').
card_rarity('mesmeric trance'/'ICE', 'Rare').
card_artist('mesmeric trance'/'ICE', 'Dan Frazier').
card_flavor_text('mesmeric trance'/'ICE', '\"Magic overused can freeze the mind. Creativity is more important than power.\"\n—Zur the Enchanter').
card_multiverse_id('mesmeric trance'/'ICE', '2519').

card_in_set('meteor shower', 'ICE').
card_original_type('meteor shower'/'ICE', 'Sorcery').
card_original_text('meteor shower'/'ICE', 'Meteor Shower deals X+1 damage divided any way you choose among any number of target creatures and/or players.').
card_first_print('meteor shower', 'ICE').
card_image_name('meteor shower'/'ICE', 'meteor shower').
card_uid('meteor shower'/'ICE', 'ICE:Meteor Shower:meteor shower').
card_rarity('meteor shower'/'ICE', 'Common').
card_artist('meteor shower'/'ICE', 'Rick Emond').
card_flavor_text('meteor shower'/'ICE', '\"Eenie, meenie, minie, moe . . . oh, why not all of them?\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('meteor shower'/'ICE', '2638').

card_in_set('mind ravel', 'ICE').
card_original_type('mind ravel'/'ICE', 'Sorcery').
card_original_text('mind ravel'/'ICE', 'Target player chooses and discards a card from his or her hand.\nIgnore this ability if that player has no cards in hand. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('mind ravel', 'ICE').
card_image_name('mind ravel'/'ICE', 'mind ravel').
card_uid('mind ravel'/'ICE', 'ICE:Mind Ravel:mind ravel').
card_rarity('mind ravel'/'ICE', 'Common').
card_artist('mind ravel'/'ICE', 'Mark Tedin').
card_flavor_text('mind ravel'/'ICE', 'An end to reason, an end to order. Forget all that has been.').
card_multiverse_id('mind ravel'/'ICE', '2471').

card_in_set('mind warp', 'ICE').
card_original_type('mind warp'/'ICE', 'Sorcery').
card_original_text('mind warp'/'ICE', 'Look at target player\'s hand and choose X cards; that player then discards those cards. If the player does not have enough cards in hand, his or her entire hand is discarded.').
card_first_print('mind warp', 'ICE').
card_image_name('mind warp'/'ICE', 'mind warp').
card_uid('mind warp'/'ICE', 'ICE:Mind Warp:mind warp').
card_rarity('mind warp'/'ICE', 'Uncommon').
card_artist('mind warp'/'ICE', 'Liz Danforth').
card_multiverse_id('mind warp'/'ICE', '2472').

card_in_set('mind whip', 'ICE').
card_original_type('mind whip'/'ICE', 'Enchant Creature').
card_original_text('mind whip'/'ICE', 'During target creature\'s controller\'s upkeep, he or she pays {3} or Mind Whip deals 2 damage to him or her. If Mind Whip deals damage in this way, tap that creature.').
card_first_print('mind whip', 'ICE').
card_image_name('mind whip'/'ICE', 'mind whip').
card_uid('mind whip'/'ICE', 'ICE:Mind Whip:mind whip').
card_rarity('mind whip'/'ICE', 'Rare').
card_artist('mind whip'/'ICE', 'Drew Tucker').
card_flavor_text('mind whip'/'ICE', '\"A mind in agony is a sparrow without wings.\" —Lim-Dûl, the Necromancer').
card_multiverse_id('mind whip'/'ICE', '2473').

card_in_set('minion of leshrac', 'ICE').
card_original_type('minion of leshrac'/'ICE', 'Summon — Demon').
card_original_text('minion of leshrac'/'ICE', 'Protection from black\nDuring your upkeep, sacrifice a creature or Minion of Leshrac deals 5 damage to you. If Minion of Leshrac deals damage to you in this way, tap it. You cannot sacrifice Minion of Leshrac to itself.\n{T}: Destroy target creature or land.').
card_first_print('minion of leshrac', 'ICE').
card_image_name('minion of leshrac'/'ICE', 'minion of leshrac').
card_uid('minion of leshrac'/'ICE', 'ICE:Minion of Leshrac:minion of leshrac').
card_rarity('minion of leshrac'/'ICE', 'Rare').
card_artist('minion of leshrac'/'ICE', 'L. A. Williams').
card_multiverse_id('minion of leshrac'/'ICE', '2474').

card_in_set('minion of tevesh szat', 'ICE').
card_original_type('minion of tevesh szat'/'ICE', 'Summon — Demon').
card_original_text('minion of tevesh szat'/'ICE', 'During your upkeep, pay {B}{B} or Minion of Tevesh Szat deals 2 damage to you.\n{T}: Target creature gets +3/-2 until end of turn.').
card_first_print('minion of tevesh szat', 'ICE').
card_image_name('minion of tevesh szat'/'ICE', 'minion of tevesh szat').
card_uid('minion of tevesh szat'/'ICE', 'ICE:Minion of Tevesh Szat:minion of tevesh szat').
card_rarity('minion of tevesh szat'/'ICE', 'Rare').
card_artist('minion of tevesh szat'/'ICE', 'Julie Baroh').
card_flavor_text('minion of tevesh szat'/'ICE', '\"A minion given over to Tevesh Szat is a stronger minion gained.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('minion of tevesh szat'/'ICE', '2475').

card_in_set('mistfolk', 'ICE').
card_original_type('mistfolk'/'ICE', 'Summon — Mistfolk').
card_original_text('mistfolk'/'ICE', '{U} Counter target spell that targets Mistfolk.').
card_first_print('mistfolk', 'ICE').
card_image_name('mistfolk'/'ICE', 'mistfolk').
card_uid('mistfolk'/'ICE', 'ICE:Mistfolk:mistfolk').
card_rarity('mistfolk'/'ICE', 'Common').
card_artist('mistfolk'/'ICE', 'Quinton Hoover').
card_flavor_text('mistfolk'/'ICE', '\"Although my official log will state there is no evidence pointing to the existence of the Mistfolk, my certainty is lessened by the cursed consistency of the expedition\'s eyewitness accounts.\"\n—Disa the Restless, journal entry').
card_multiverse_id('mistfolk'/'ICE', '2520').

card_in_set('mole worms', 'ICE').
card_original_type('mole worms'/'ICE', 'Summon — Worms').
card_original_text('mole worms'/'ICE', 'You may choose not to untap Mole Worms during your untap phase.\n{T}: Tap target land. As long as Mole Worms remains tapped, that land does not untap during its controller\'s untap phase.').
card_first_print('mole worms', 'ICE').
card_image_name('mole worms'/'ICE', 'mole worms').
card_uid('mole worms'/'ICE', 'ICE:Mole Worms:mole worms').
card_rarity('mole worms'/'ICE', 'Uncommon').
card_artist('mole worms'/'ICE', 'Daniel Gelon').
card_multiverse_id('mole worms'/'ICE', '2476').

card_in_set('monsoon', 'ICE').
card_original_type('monsoon'/'ICE', 'Enchantment').
card_original_text('monsoon'/'ICE', 'Whenever any island is untapped at the end of its controller\'s turn, tap it; Monsoon deals 1 damage to that player.').
card_first_print('monsoon', 'ICE').
card_image_name('monsoon'/'ICE', 'monsoon').
card_uid('monsoon'/'ICE', 'ICE:Monsoon:monsoon').
card_rarity('monsoon'/'ICE', 'Rare').
card_artist('monsoon'/'ICE', 'NéNé Thomas').
card_flavor_text('monsoon'/'ICE', '\"No one in her right mind would venture off the coast of Kjeldor during this season.\"\n—Disa the Restless, journal entry').
card_multiverse_id('monsoon'/'ICE', '2734').

card_in_set('moor fiend', 'ICE').
card_original_type('moor fiend'/'ICE', 'Summon — Fiend').
card_original_text('moor fiend'/'ICE', 'Swampwalk').
card_first_print('moor fiend', 'ICE').
card_image_name('moor fiend'/'ICE', 'moor fiend').
card_uid('moor fiend'/'ICE', 'ICE:Moor Fiend:moor fiend').
card_rarity('moor fiend'/'ICE', 'Common').
card_artist('moor fiend'/'ICE', 'Anson Maddocks').
card_flavor_text('moor fiend'/'ICE', '\"Let them close the gates of Krov from dusk until dawn if they so choose. It matters not. My fiends shall yet rend their flesh from their bones.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('moor fiend'/'ICE', '2477').

card_in_set('mountain', 'ICE').
card_original_type('mountain'/'ICE', 'Land').
card_original_text('mountain'/'ICE', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'ICE', 'mountain1').
card_uid('mountain'/'ICE', 'ICE:Mountain:mountain1').
card_rarity('mountain'/'ICE', 'Basic Land').
card_artist('mountain'/'ICE', 'Tom Wänerstrand').
card_multiverse_id('mountain'/'ICE', '2763').

card_in_set('mountain', 'ICE').
card_original_type('mountain'/'ICE', 'Land').
card_original_text('mountain'/'ICE', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'ICE', 'mountain2').
card_uid('mountain'/'ICE', 'ICE:Mountain:mountain2').
card_rarity('mountain'/'ICE', 'Basic Land').
card_artist('mountain'/'ICE', 'Tom Wänerstrand').
card_multiverse_id('mountain'/'ICE', '2765').

card_in_set('mountain', 'ICE').
card_original_type('mountain'/'ICE', 'Land').
card_original_text('mountain'/'ICE', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'ICE', 'mountain3').
card_uid('mountain'/'ICE', 'ICE:Mountain:mountain3').
card_rarity('mountain'/'ICE', 'Basic Land').
card_artist('mountain'/'ICE', 'Tom Wänerstrand').
card_multiverse_id('mountain'/'ICE', '2764').

card_in_set('mountain goat', 'ICE').
card_original_type('mountain goat'/'ICE', 'Summon — Goat').
card_original_text('mountain goat'/'ICE', 'Mountainwalk').
card_first_print('mountain goat', 'ICE').
card_image_name('mountain goat'/'ICE', 'mountain goat').
card_uid('mountain goat'/'ICE', 'ICE:Mountain Goat:mountain goat').
card_rarity('mountain goat'/'ICE', 'Common').
card_artist('mountain goat'/'ICE', 'Cornelius Brudi').
card_flavor_text('mountain goat'/'ICE', '\"Folklore has it that to capture a Mountain Goat is a sign of divine blessing. I just know it\'s a sign that dinner is on the way.\"\n—Klazina Jansdotter, Leader of the Order of the Sacred Torch').
card_multiverse_id('mountain goat'/'ICE', '2639').

card_in_set('mountain titan', 'ICE').
card_original_type('mountain titan'/'ICE', 'Summon — Titan').
card_original_text('mountain titan'/'ICE', '{1}{R}{R} For the rest of the turn, put a +1/+1 counter on Mountain Titan whenever you successfully cast a black spell.').
card_first_print('mountain titan', 'ICE').
card_image_name('mountain titan'/'ICE', 'mountain titan').
card_uid('mountain titan'/'ICE', 'ICE:Mountain Titan:mountain titan').
card_rarity('mountain titan'/'ICE', 'Rare').
card_artist('mountain titan'/'ICE', 'Melissa A. Benson').
card_flavor_text('mountain titan'/'ICE', '\"The Titans claimed to draw strength from the rocks, but I suspect them of some darker allegiance.\"\n—Disa the Restless, journal entry').
card_multiverse_id('mountain titan'/'ICE', '2735').

card_in_set('mudslide', 'ICE').
card_original_type('mudslide'/'ICE', 'Enchantment').
card_original_text('mudslide'/'ICE', 'Creatures without flying do not untap during their controller\'s untap phase. At the end of his or her upkeep, each player may pay an additional {2} per creature to untap a creature without flying he or she controls.').
card_first_print('mudslide', 'ICE').
card_image_name('mudslide'/'ICE', 'mudslide').
card_uid('mudslide'/'ICE', 'ICE:Mudslide:mudslide').
card_rarity('mudslide'/'ICE', 'Rare').
card_artist('mudslide'/'ICE', 'Brian Snõddy').
card_multiverse_id('mudslide'/'ICE', '2640').

card_in_set('musician', 'ICE').
card_original_type('musician'/'ICE', 'Summon — Mage').
card_original_text('musician'/'ICE', 'Cumulative Upkeep: {1}\n{T}: Put a music counter on target creature. During that creature\'s controller\'s upkeep, he or she pays {1} for each music counter on the creature, or destroy the creature.').
card_first_print('musician', 'ICE').
card_image_name('musician'/'ICE', 'musician').
card_uid('musician'/'ICE', 'ICE:Musician:musician').
card_rarity('musician'/'ICE', 'Rare').
card_artist('musician'/'ICE', 'Drew Tucker').
card_multiverse_id('musician'/'ICE', '2521').

card_in_set('mystic might', 'ICE').
card_original_type('mystic might'/'ICE', 'Enchant Land').
card_original_text('mystic might'/'ICE', 'Cumulative Upkeep: {1}{U}\nWhen Mystic Might comes into play, choose target land you control.\n{0}: Tap land Mystic Might enchants to give target creature +2/+2 until end of turn.').
card_first_print('mystic might', 'ICE').
card_image_name('mystic might'/'ICE', 'mystic might').
card_uid('mystic might'/'ICE', 'ICE:Mystic Might:mystic might').
card_rarity('mystic might'/'ICE', 'Rare').
card_artist('mystic might'/'ICE', 'Nicola Leonard').
card_multiverse_id('mystic might'/'ICE', '2522').

card_in_set('mystic remora', 'ICE').
card_original_type('mystic remora'/'ICE', 'Enchantment').
card_original_text('mystic remora'/'ICE', 'Cumulative Upkeep: {1}\nWhenever target opponent successfully casts a non-creature spell, you may draw a card. That player may pay {4} to counter this effect.').
card_first_print('mystic remora', 'ICE').
card_image_name('mystic remora'/'ICE', 'mystic remora').
card_uid('mystic remora'/'ICE', 'ICE:Mystic Remora:mystic remora').
card_rarity('mystic remora'/'ICE', 'Common').
card_artist('mystic remora'/'ICE', 'Ken Meyer, Jr.').
card_multiverse_id('mystic remora'/'ICE', '2523').

card_in_set('nacre talisman', 'ICE').
card_original_type('nacre talisman'/'ICE', 'Artifact').
card_original_text('nacre talisman'/'ICE', '{3}: Untap target permanent.\nUse this ability only when a white spell is successfully cast and only once for each white spell cast.').
card_first_print('nacre talisman', 'ICE').
card_image_name('nacre talisman'/'ICE', 'nacre talisman').
card_uid('nacre talisman'/'ICE', 'ICE:Nacre Talisman:nacre talisman').
card_rarity('nacre talisman'/'ICE', 'Uncommon').
card_artist('nacre talisman'/'ICE', 'Mark Tedin').
card_multiverse_id('nacre talisman'/'ICE', '2415').

card_in_set('naked singularity', 'ICE').
card_original_type('naked singularity'/'ICE', 'Artifact').
card_original_text('naked singularity'/'ICE', 'Cumulative Upkeep: {3}\nInstead of their normal mana, plains produce {R}, islands produce {G}, swamps produce {W}, mountains produce {U}, and forests produce {B}.').
card_first_print('naked singularity', 'ICE').
card_image_name('naked singularity'/'ICE', 'naked singularity').
card_uid('naked singularity'/'ICE', 'ICE:Naked Singularity:naked singularity').
card_rarity('naked singularity'/'ICE', 'Rare').
card_artist('naked singularity'/'ICE', 'Mark Tedin').
card_multiverse_id('naked singularity'/'ICE', '2416').

card_in_set('nature\'s lore', 'ICE').
card_original_type('nature\'s lore'/'ICE', 'Sorcery').
card_original_text('nature\'s lore'/'ICE', 'Search your library for any forest and put it directly into play. This does not count towards your one land per turn limit. Reshuffle your library afterwards.').
card_first_print('nature\'s lore', 'ICE').
card_image_name('nature\'s lore'/'ICE', 'nature\'s lore').
card_uid('nature\'s lore'/'ICE', 'ICE:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'ICE', 'Uncommon').
card_artist('nature\'s lore'/'ICE', 'Rick Emond').
card_flavor_text('nature\'s lore'/'ICE', '\"Fyndhorn is our home.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('nature\'s lore'/'ICE', '2579').

card_in_set('necropotence', 'ICE').
card_original_type('necropotence'/'ICE', 'Enchantment').
card_original_text('necropotence'/'ICE', 'Skip your draw phase. If you discard a card from your hand, remove that card from the game.\n{0}: Pay 1 life to set aside the top card of your library. At the beginning of your next discard phase, put that card into your hand. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('necropotence', 'ICE').
card_image_name('necropotence'/'ICE', 'necropotence').
card_uid('necropotence'/'ICE', 'ICE:Necropotence:necropotence').
card_rarity('necropotence'/'ICE', 'Rare').
card_artist('necropotence'/'ICE', 'Mark Tedin').
card_multiverse_id('necropotence'/'ICE', '2478').

card_in_set('norritt', 'ICE').
card_original_type('norritt'/'ICE', 'Summon — Imp').
card_original_text('norritt'/'ICE', '{T}: Untap target blue creature.\n{T}: Force target non-wall creature to attack. If creature cannot attack, destroy it at end of turn. Use this ability only during target creature\'s controller\'s turn, before the attack. Cannot target creatures brought under their controller\'s control this turn.').
card_first_print('norritt', 'ICE').
card_image_name('norritt'/'ICE', 'norritt').
card_uid('norritt'/'ICE', 'ICE:Norritt:norritt').
card_rarity('norritt'/'ICE', 'Common').
card_artist('norritt'/'ICE', 'Mike Raabe').
card_multiverse_id('norritt'/'ICE', '2479').

card_in_set('oath of lim-dûl', 'ICE').
card_original_type('oath of lim-dûl'/'ICE', 'Enchantment').
card_original_text('oath of lim-dûl'/'ICE', 'For each 1 damage dealt to you or 1 life you lose, sacrifice a permanent you control or choose and discard a card from your hand. You cannot sacrifice Oath of Lim-Dûl in this way. Ignore this effect if you control no permanents other than Oath of Lim-Dûl and have no cards in hand.\n{B}{B}: Draw a card.').
card_first_print('oath of lim-dûl', 'ICE').
card_image_name('oath of lim-dûl'/'ICE', 'oath of lim-dul').
card_uid('oath of lim-dûl'/'ICE', 'ICE:Oath of Lim-Dûl:oath of lim-dul').
card_rarity('oath of lim-dûl'/'ICE', 'Rare').
card_artist('oath of lim-dûl'/'ICE', 'Douglas Shuler').
card_multiverse_id('oath of lim-dûl'/'ICE', '2480').

card_in_set('onyx talisman', 'ICE').
card_original_type('onyx talisman'/'ICE', 'Artifact').
card_original_text('onyx talisman'/'ICE', '{3}: Untap target permanent.\nUse this ability only when a black spell is successfully cast and only once for each black spell cast.').
card_first_print('onyx talisman', 'ICE').
card_image_name('onyx talisman'/'ICE', 'onyx talisman').
card_uid('onyx talisman'/'ICE', 'ICE:Onyx Talisman:onyx talisman').
card_rarity('onyx talisman'/'ICE', 'Uncommon').
card_artist('onyx talisman'/'ICE', 'Sandra Everingham').
card_multiverse_id('onyx talisman'/'ICE', '2417').

card_in_set('orcish cannoneers', 'ICE').
card_original_type('orcish cannoneers'/'ICE', 'Summon — Orcs').
card_original_text('orcish cannoneers'/'ICE', '{T}: Orcish Cannoneers deals 2 damage to target creature or player and 3 damage to you.').
card_first_print('orcish cannoneers', 'ICE').
card_image_name('orcish cannoneers'/'ICE', 'orcish cannoneers').
card_uid('orcish cannoneers'/'ICE', 'ICE:Orcish Cannoneers:orcish cannoneers').
card_rarity('orcish cannoneers'/'ICE', 'Uncommon').
card_artist('orcish cannoneers'/'ICE', 'Dan Frazier').
card_flavor_text('orcish cannoneers'/'ICE', '\"It\'s a thankless job, and you\'ll probably die in an explosion. But the pay is pretty good.\"\n—Toothlicker Harj, Orcish Captain').
card_multiverse_id('orcish cannoneers'/'ICE', '2641').

card_in_set('orcish conscripts', 'ICE').
card_original_type('orcish conscripts'/'ICE', 'Summon — Orcs').
card_original_text('orcish conscripts'/'ICE', 'Cannot be declared as attacking unless at least two other creatures are also declared as attacking. Cannot be assigned to block unless at least two other creatures are also assigned to block.').
card_first_print('orcish conscripts', 'ICE').
card_image_name('orcish conscripts'/'ICE', 'orcish conscripts').
card_uid('orcish conscripts'/'ICE', 'ICE:Orcish Conscripts:orcish conscripts').
card_rarity('orcish conscripts'/'ICE', 'Common').
card_artist('orcish conscripts'/'ICE', 'Douglas Shuler').
card_multiverse_id('orcish conscripts'/'ICE', '2642').

card_in_set('orcish farmer', 'ICE').
card_original_type('orcish farmer'/'ICE', 'Summon — Orc').
card_original_text('orcish farmer'/'ICE', '{T}: Target land becomes a swamp until its controller\'s next untap phase.').
card_first_print('orcish farmer', 'ICE').
card_image_name('orcish farmer'/'ICE', 'orcish farmer').
card_uid('orcish farmer'/'ICE', 'ICE:Orcish Farmer:orcish farmer').
card_rarity('orcish farmer'/'ICE', 'Common').
card_artist('orcish farmer'/'ICE', 'Dan Frazier').
card_flavor_text('orcish farmer'/'ICE', '\"Yes, the Farmers keep our soldiers fed. But why do they have to make every battlefield a pigpen?\"\n—Toothlicker Harj, Orcish Captain').
card_multiverse_id('orcish farmer'/'ICE', '2643').

card_in_set('orcish healer', 'ICE').
card_original_type('orcish healer'/'ICE', 'Summon — Cleric').
card_original_text('orcish healer'/'ICE', '{R}{R},{T}: Target creature cannot regenerate this turn.\n{R}{B}{B},{T}: Regenerate target black or green creature.\n{R}{G}{G},{T}: Regenerate target black or green creature.').
card_first_print('orcish healer', 'ICE').
card_image_name('orcish healer'/'ICE', 'orcish healer').
card_uid('orcish healer'/'ICE', 'ICE:Orcish Healer:orcish healer').
card_rarity('orcish healer'/'ICE', 'Uncommon').
card_artist('orcish healer'/'ICE', 'Quinton Hoover').
card_multiverse_id('orcish healer'/'ICE', '2644').

card_in_set('orcish librarian', 'ICE').
card_original_type('orcish librarian'/'ICE', 'Summon — Orc').
card_original_text('orcish librarian'/'ICE', '{R},{T}: Take the top eight cards of your library; remove four of them at random from the game. Put the remaining four on top of your library in any order.').
card_first_print('orcish librarian', 'ICE').
card_image_name('orcish librarian'/'ICE', 'orcish librarian').
card_uid('orcish librarian'/'ICE', 'ICE:Orcish Librarian:orcish librarian').
card_rarity('orcish librarian'/'ICE', 'Rare').
card_artist('orcish librarian'/'ICE', 'Phil Foglio').
card_flavor_text('orcish librarian'/'ICE', 'Us hungry, need food . . . .\nLots of books . . . . Hmm . . . .').
card_multiverse_id('orcish librarian'/'ICE', '2645').

card_in_set('orcish lumberjack', 'ICE').
card_original_type('orcish lumberjack'/'ICE', 'Summon — Orc').
card_original_text('orcish lumberjack'/'ICE', '{T}: Sacrifice a forest to add three mana in any combination of red and/or green to your mana pool. Play this ability as an interrupt.').
card_first_print('orcish lumberjack', 'ICE').
card_image_name('orcish lumberjack'/'ICE', 'orcish lumberjack').
card_uid('orcish lumberjack'/'ICE', 'ICE:Orcish Lumberjack:orcish lumberjack').
card_rarity('orcish lumberjack'/'ICE', 'Common').
card_artist('orcish lumberjack'/'ICE', 'Dan Frazier').
card_flavor_text('orcish lumberjack'/'ICE', '\"How did I ever let myself get talked into this project?\"\n—Toothlicker Harj, Orcish Captain').
card_multiverse_id('orcish lumberjack'/'ICE', '2646').

card_in_set('orcish squatters', 'ICE').
card_original_type('orcish squatters'/'ICE', 'Summon — Orcs').
card_original_text('orcish squatters'/'ICE', 'If Orcish Squatters attacks and is not blocked, you may gain control of target land controlled by defending player. If you do so, Orcish Squatters deals no damage in combat this turn. Lose control of that land if Orcish Squatters leaves play or if you lose control of Orcish Squatters.').
card_first_print('orcish squatters', 'ICE').
card_image_name('orcish squatters'/'ICE', 'orcish squatters').
card_uid('orcish squatters'/'ICE', 'ICE:Orcish Squatters:orcish squatters').
card_rarity('orcish squatters'/'ICE', 'Rare').
card_artist('orcish squatters'/'ICE', 'Richard Kane Ferguson').
card_multiverse_id('orcish squatters'/'ICE', '2647').

card_in_set('order of the sacred torch', 'ICE').
card_original_type('order of the sacred torch'/'ICE', 'Summon — Paladin').
card_original_text('order of the sacred torch'/'ICE', '{T}: Pay 1 life to destroy target black spell. Effects that prevent or redirect damage cannot be used to counter this loss of life. Play this ability as an interrupt.').
card_first_print('order of the sacred torch', 'ICE').
card_image_name('order of the sacred torch'/'ICE', 'order of the sacred torch').
card_uid('order of the sacred torch'/'ICE', 'ICE:Order of the Sacred Torch:order of the sacred torch').
card_rarity('order of the sacred torch'/'ICE', 'Rare').
card_artist('order of the sacred torch'/'ICE', 'Ruth Thompson').
card_multiverse_id('order of the sacred torch'/'ICE', '2705').

card_in_set('order of the white shield', 'ICE').
card_original_type('order of the white shield'/'ICE', 'Summon — Knights').
card_original_text('order of the white shield'/'ICE', 'Protection from black\n{W} First strike until end of turn\n{W}{W} +1/+0 until end of turn').
card_first_print('order of the white shield', 'ICE').
card_image_name('order of the white shield'/'ICE', 'order of the white shield').
card_uid('order of the white shield'/'ICE', 'ICE:Order of the White Shield:order of the white shield').
card_rarity('order of the white shield'/'ICE', 'Uncommon').
card_artist('order of the white shield'/'ICE', 'Ruth Thompson').
card_flavor_text('order of the white shield'/'ICE', '\"Shall we turn away a worthy soul because his parents were peasants? I think not.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('order of the white shield'/'ICE', '2706').

card_in_set('pale bears', 'ICE').
card_original_type('pale bears'/'ICE', 'Summon — Bears').
card_original_text('pale bears'/'ICE', 'Islandwalk').
card_first_print('pale bears', 'ICE').
card_image_name('pale bears'/'ICE', 'pale bears').
card_uid('pale bears'/'ICE', 'ICE:Pale Bears:pale bears').
card_rarity('pale bears'/'ICE', 'Rare').
card_artist('pale bears'/'ICE', 'Anthony Waters').
card_flavor_text('pale bears'/'ICE', '\"Daughter, on the day you have killed your Pale Bear, then will I give you your true name.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('pale bears'/'ICE', '2580').

card_in_set('panic', 'ICE').
card_original_type('panic'/'ICE', 'Instant').
card_original_text('panic'/'ICE', 'Target creature cannot block this turn. Cast only during combat before defense is chosen. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('panic', 'ICE').
card_image_name('panic'/'ICE', 'panic').
card_uid('panic'/'ICE', 'ICE:Panic:panic').
card_rarity('panic'/'ICE', 'Common').
card_artist('panic'/'ICE', 'Mike Kimble').
card_flavor_text('panic'/'ICE', '\"If you\'d been there, you would\'ve run from that deer, too!\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('panic'/'ICE', '2648').

card_in_set('pentagram of the ages', 'ICE').
card_original_type('pentagram of the ages'/'ICE', 'Artifact').
card_original_text('pentagram of the ages'/'ICE', '{4},{T}: Prevent all damage dealt to you from one source. Pentagram of the Ages does not prevent the same source damaging you again later this turn.').
card_first_print('pentagram of the ages', 'ICE').
card_image_name('pentagram of the ages'/'ICE', 'pentagram of the ages').
card_uid('pentagram of the ages'/'ICE', 'ICE:Pentagram of the Ages:pentagram of the ages').
card_rarity('pentagram of the ages'/'ICE', 'Rare').
card_artist('pentagram of the ages'/'ICE', 'Douglas Shuler').
card_flavor_text('pentagram of the ages'/'ICE', '\"Take this item, for instance. How would it destroy us, Relicbane?\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('pentagram of the ages'/'ICE', '2418').

card_in_set('pestilence rats', 'ICE').
card_original_type('pestilence rats'/'ICE', 'Summon — Rats').
card_original_text('pestilence rats'/'ICE', 'Pestilence Rats has power equal to the total number of other Rats in play, no matter who controls them. For example, as long as there are two other Rats in play, Pestilence Rats has power and toughness 2/3.').
card_first_print('pestilence rats', 'ICE').
card_image_name('pestilence rats'/'ICE', 'pestilence rats').
card_uid('pestilence rats'/'ICE', 'ICE:Pestilence Rats:pestilence rats').
card_rarity('pestilence rats'/'ICE', 'Common').
card_artist('pestilence rats'/'ICE', 'Jeff A. Menges').
card_multiverse_id('pestilence rats'/'ICE', '2481').

card_in_set('phantasmal mount', 'ICE').
card_original_type('phantasmal mount'/'ICE', 'Summon — Phantasm').
card_original_text('phantasmal mount'/'ICE', 'Flying\n{T}: Target creature you control, which has toughness less than 3, gains flying and gets +1/+1 until end of turn. Other effects may later be used to increase that creature\'s toughness beyond 3. If Phantasmal Mount leaves play before end of turn, bury the creature. If the creature leaves play before end of turn, bury Phantasmal Mount.').
card_first_print('phantasmal mount', 'ICE').
card_image_name('phantasmal mount'/'ICE', 'phantasmal mount').
card_uid('phantasmal mount'/'ICE', 'ICE:Phantasmal Mount:phantasmal mount').
card_rarity('phantasmal mount'/'ICE', 'Uncommon').
card_artist('phantasmal mount'/'ICE', 'Melissa A. Benson').
card_multiverse_id('phantasmal mount'/'ICE', '2524').

card_in_set('pit trap', 'ICE').
card_original_type('pit trap'/'ICE', 'Artifact').
card_original_text('pit trap'/'ICE', '{2},{T}: Sacrifice Pit Trap to bury target creature without flying that is attacking you.').
card_first_print('pit trap', 'ICE').
card_image_name('pit trap'/'ICE', 'pit trap').
card_uid('pit trap'/'ICE', 'ICE:Pit Trap:pit trap').
card_rarity('pit trap'/'ICE', 'Uncommon').
card_artist('pit trap'/'ICE', 'Anson Maddocks').
card_flavor_text('pit trap'/'ICE', '\"These traps are truly a symbol of great cruelty and sinister cunning.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('pit trap'/'ICE', '2419').

card_in_set('plains', 'ICE').
card_original_type('plains'/'ICE', 'Land').
card_original_text('plains'/'ICE', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'ICE', 'plains1').
card_uid('plains'/'ICE', 'ICE:Plains:plains1').
card_rarity('plains'/'ICE', 'Basic Land').
card_artist('plains'/'ICE', 'Christopher Rush').
card_multiverse_id('plains'/'ICE', '2773').

card_in_set('plains', 'ICE').
card_original_type('plains'/'ICE', 'Land').
card_original_text('plains'/'ICE', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'ICE', 'plains2').
card_uid('plains'/'ICE', 'ICE:Plains:plains2').
card_rarity('plains'/'ICE', 'Basic Land').
card_artist('plains'/'ICE', 'Christopher Rush').
card_multiverse_id('plains'/'ICE', '2771').

card_in_set('plains', 'ICE').
card_original_type('plains'/'ICE', 'Land').
card_original_text('plains'/'ICE', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'ICE', 'plains3').
card_uid('plains'/'ICE', 'ICE:Plains:plains3').
card_rarity('plains'/'ICE', 'Basic Land').
card_artist('plains'/'ICE', 'Christopher Rush').
card_multiverse_id('plains'/'ICE', '2772').

card_in_set('polar kraken', 'ICE').
card_original_type('polar kraken'/'ICE', 'Summon — Kraken').
card_original_text('polar kraken'/'ICE', 'Trample\nCumulative Upkeep: Sacrifice a land.\nComes into play tapped.').
card_first_print('polar kraken', 'ICE').
card_image_name('polar kraken'/'ICE', 'polar kraken').
card_uid('polar kraken'/'ICE', 'ICE:Polar Kraken:polar kraken').
card_rarity('polar kraken'/'ICE', 'Rare').
card_artist('polar kraken'/'ICE', 'Mark Tedin').
card_flavor_text('polar kraken'/'ICE', '\"It was big. Really, really, big. No, bigger than that. It was big!\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('polar kraken'/'ICE', '2525').

card_in_set('portent', 'ICE').
card_original_type('portent'/'ICE', 'Sorcery').
card_original_text('portent'/'ICE', 'Look at the top three cards of target player\'s library; then, either shuffle that library or put those three cards on top of the library in any order. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('portent', 'ICE').
card_image_name('portent'/'ICE', 'portent').
card_uid('portent'/'ICE', 'ICE:Portent:portent').
card_rarity('portent'/'ICE', 'Common').
card_artist('portent'/'ICE', 'Liz Danforth').
card_multiverse_id('portent'/'ICE', '2526').

card_in_set('power sink', 'ICE').
card_original_type('power sink'/'ICE', 'Interrupt').
card_original_text('power sink'/'ICE', 'Counter target spell unless that spell\'s caster pays an additional {X}. That player must draw and pay all available mana from lands and mana pool until {X} is paid; he or she may also pay mana from other sources if desired.').
card_image_name('power sink'/'ICE', 'power sink').
card_uid('power sink'/'ICE', 'ICE:Power Sink:power sink').
card_rarity('power sink'/'ICE', 'Common').
card_artist('power sink'/'ICE', 'Mark Poole').
card_multiverse_id('power sink'/'ICE', '2527').

card_in_set('pox', 'ICE').
card_original_type('pox'/'ICE', 'Sorcery').
card_original_text('pox'/'ICE', 'Each player loses 1/3 of his or her life; then chooses and discards 1/3 of the cards in his or her hand; then sacrifices 1/3 of the creatures he or she controls; and finally sacrifices 1/3 of the lands he or she controls. Round each loss up. Effects that prevent or redirect damage cannot be used to counter the this loss of life.').
card_first_print('pox', 'ICE').
card_image_name('pox'/'ICE', 'pox').
card_uid('pox'/'ICE', 'ICE:Pox:pox').
card_rarity('pox'/'ICE', 'Rare').
card_artist('pox'/'ICE', 'Cornelius Brudi').
card_multiverse_id('pox'/'ICE', '2482').

card_in_set('prismatic ward', 'ICE').
card_original_type('prismatic ward'/'ICE', 'Enchant Creature').
card_original_text('prismatic ward'/'ICE', 'When Prismatic Ward comes into play, choose a color; all damage dealt to target creature by sources of that color is reduced to 0.').
card_first_print('prismatic ward', 'ICE').
card_image_name('prismatic ward'/'ICE', 'prismatic ward').
card_uid('prismatic ward'/'ICE', 'ICE:Prismatic Ward:prismatic ward').
card_rarity('prismatic ward'/'ICE', 'Common').
card_artist('prismatic ward'/'ICE', 'L. A. Williams').
card_flavor_text('prismatic ward'/'ICE', '\"These, of all spells, must have common components I can isolate and use.\" —Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('prismatic ward'/'ICE', '2707').

card_in_set('pygmy allosaurus', 'ICE').
card_original_type('pygmy allosaurus'/'ICE', 'Summon — Dinosaur').
card_original_text('pygmy allosaurus'/'ICE', 'Swampwalk').
card_first_print('pygmy allosaurus', 'ICE').
card_image_name('pygmy allosaurus'/'ICE', 'pygmy allosaurus').
card_uid('pygmy allosaurus'/'ICE', 'ICE:Pygmy Allosaurus:pygmy allosaurus').
card_rarity('pygmy allosaurus'/'ICE', 'Rare').
card_artist('pygmy allosaurus'/'ICE', 'Anson Maddocks').
card_flavor_text('pygmy allosaurus'/'ICE', '\"I don\'t understand the appeal of keeping these things as pets, unless you want your children eaten.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('pygmy allosaurus'/'ICE', '2581').

card_in_set('pyknite', 'ICE').
card_original_type('pyknite'/'ICE', 'Summon — Pyknite').
card_original_text('pyknite'/'ICE', 'Draw a card at the beginning of the upkeep of the turn after Pyknite comes into play.').
card_first_print('pyknite', 'ICE').
card_image_name('pyknite'/'ICE', 'pyknite').
card_uid('pyknite'/'ICE', 'ICE:Pyknite:pyknite').
card_rarity('pyknite'/'ICE', 'Common').
card_artist('pyknite'/'ICE', 'Edward P. Beard, Jr.').
card_flavor_text('pyknite'/'ICE', '\"Never cross a Pyknite, if you value your scalp.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('pyknite'/'ICE', '2582').

card_in_set('pyroblast', 'ICE').
card_original_type('pyroblast'/'ICE', 'Interrupt').
card_original_text('pyroblast'/'ICE', 'Counter target spell if it is blue or destroy target permanent if it is blue.').
card_first_print('pyroblast', 'ICE').
card_image_name('pyroblast'/'ICE', 'pyroblast').
card_uid('pyroblast'/'ICE', 'ICE:Pyroblast:pyroblast').
card_rarity('pyroblast'/'ICE', 'Common').
card_artist('pyroblast'/'ICE', 'Kaja Foglio').
card_flavor_text('pyroblast'/'ICE', '\"Just the thing for those pesky water mages.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('pyroblast'/'ICE', '2649').

card_in_set('pyroclasm', 'ICE').
card_original_type('pyroclasm'/'ICE', 'Sorcery').
card_original_text('pyroclasm'/'ICE', 'Pyroclasm deals 2 damage to each creature.').
card_first_print('pyroclasm', 'ICE').
card_image_name('pyroclasm'/'ICE', 'pyroclasm').
card_uid('pyroclasm'/'ICE', 'ICE:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'ICE', 'Uncommon').
card_artist('pyroclasm'/'ICE', 'Pat Morrissey').
card_flavor_text('pyroclasm'/'ICE', '\"Leaves more room for the big ones to fight in, you know.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('pyroclasm'/'ICE', '2650').

card_in_set('rally', 'ICE').
card_original_type('rally'/'ICE', 'Instant').
card_original_text('rally'/'ICE', 'All blocking creatures get +1/+1 until end of turn.').
card_first_print('rally', 'ICE').
card_image_name('rally'/'ICE', 'rally').
card_uid('rally'/'ICE', 'ICE:Rally:rally').
card_rarity('rally'/'ICE', 'Common').
card_artist('rally'/'ICE', 'Heather Hudson').
card_flavor_text('rally'/'ICE', '\"Stand your ground, troops! This shall be our finest hour!\"\n—General Jarkeld, the Arctic Fox, last words').
card_multiverse_id('rally'/'ICE', '2708').

card_in_set('ray of command', 'ICE').
card_original_type('ray of command'/'ICE', 'Instant').
card_original_text('ray of command'/'ICE', 'Untap target creature opponent controls and gain control of it until end of turn. That creature can attack or use abilities that require {T} as part of the activation cost. When you lose control of the creature, tap it.').
card_first_print('ray of command', 'ICE').
card_image_name('ray of command'/'ICE', 'ray of command').
card_uid('ray of command'/'ICE', 'ICE:Ray of Command:ray of command').
card_rarity('ray of command'/'ICE', 'Common').
card_artist('ray of command'/'ICE', 'Harold McNeill').
card_multiverse_id('ray of command'/'ICE', '2528').

card_in_set('ray of erasure', 'ICE').
card_original_type('ray of erasure'/'ICE', 'Instant').
card_original_text('ray of erasure'/'ICE', 'Target player takes the top card of his or her library and puts it in his or her graveyard. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('ray of erasure', 'ICE').
card_image_name('ray of erasure'/'ICE', 'ray of erasure').
card_uid('ray of erasure'/'ICE', 'ICE:Ray of Erasure:ray of erasure').
card_rarity('ray of erasure'/'ICE', 'Common').
card_artist('ray of erasure'/'ICE', 'Mike Raabe').
card_flavor_text('ray of erasure'/'ICE', '\"What is real can be unreal.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('ray of erasure'/'ICE', '2529').

card_in_set('reality twist', 'ICE').
card_original_type('reality twist'/'ICE', 'Enchantment').
card_original_text('reality twist'/'ICE', 'Cumulative Upkeep: {1}{U}{U}\nInstead of their normal mana, plains produce {R}, swamps produce {G}, mountains produce {W}, and forests produce {B}.').
card_first_print('reality twist', 'ICE').
card_image_name('reality twist'/'ICE', 'reality twist').
card_uid('reality twist'/'ICE', 'ICE:Reality Twist:reality twist').
card_rarity('reality twist'/'ICE', 'Rare').
card_artist('reality twist'/'ICE', 'James Ernest').
card_flavor_text('reality twist'/'ICE', '\"Nothing is as it seems.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('reality twist'/'ICE', '2530').

card_in_set('reclamation', 'ICE').
card_original_type('reclamation'/'ICE', 'Enchantment').
card_original_text('reclamation'/'ICE', 'No black creature can attack unless its controller sacrifices a land whenever that creature attacks.').
card_first_print('reclamation', 'ICE').
card_image_name('reclamation'/'ICE', 'reclamation').
card_uid('reclamation'/'ICE', 'ICE:Reclamation:reclamation').
card_rarity('reclamation'/'ICE', 'Rare').
card_artist('reclamation'/'ICE', 'Dameon Willich').
card_flavor_text('reclamation'/'ICE', '\"We shall oppose Lim-Dûl and his forces by any means necessary, even if the very earth be torn asunder.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('reclamation'/'ICE', '2736').

card_in_set('red scarab', 'ICE').
card_original_type('red scarab'/'ICE', 'Enchant Creature').
card_original_text('red scarab'/'ICE', 'Target creature gets +2/+2 as long as any opponent controls any red cards. That creature cannot be blocked by red creatures.').
card_first_print('red scarab', 'ICE').
card_image_name('red scarab'/'ICE', 'red scarab').
card_uid('red scarab'/'ICE', 'ICE:Red Scarab:red scarab').
card_rarity('red scarab'/'ICE', 'Uncommon').
card_artist('red scarab'/'ICE', 'Sandra Everingham').
card_multiverse_id('red scarab'/'ICE', '2709').

card_in_set('regeneration', 'ICE').
card_original_type('regeneration'/'ICE', 'Enchant Creature').
card_original_text('regeneration'/'ICE', 'When Regeneration comes into play, choose target creature.\n{G} Regenerate creature Regeneration enchants.').
card_image_name('regeneration'/'ICE', 'regeneration').
card_uid('regeneration'/'ICE', 'ICE:Regeneration:regeneration').
card_rarity('regeneration'/'ICE', 'Common').
card_artist('regeneration'/'ICE', 'Justin Hampton').
card_flavor_text('regeneration'/'ICE', '\"Faith in Freyalise has given me the gift, not the curse, of unprecedented longevity.\"\n—Laina of the Elvish Council').
card_multiverse_id('regeneration'/'ICE', '2583').

card_in_set('rime dryad', 'ICE').
card_original_type('rime dryad'/'ICE', 'Summon — Dryad').
card_original_text('rime dryad'/'ICE', 'Snow-covered forestwalk').
card_first_print('rime dryad', 'ICE').
card_image_name('rime dryad'/'ICE', 'rime dryad').
card_uid('rime dryad'/'ICE', 'ICE:Rime Dryad:rime dryad').
card_rarity('rime dryad'/'ICE', 'Common').
card_artist('rime dryad'/'ICE', 'Heather Hudson').
card_flavor_text('rime dryad'/'ICE', '\"The Dryads told us to stay, as no new horrors would come when it was so cold. Three of us left anyway. The rest believed their lies, and their skeletons weren\'t found until the thaw.\"\n—Disa the Restless, journal entry').
card_multiverse_id('rime dryad'/'ICE', '2584').

card_in_set('ritual of subdual', 'ICE').
card_original_type('ritual of subdual'/'ICE', 'Enchantment').
card_original_text('ritual of subdual'/'ICE', 'Cumulative Upkeep: {2}\nAll mana-producing lands produce colorless mana instead of their normal mana.').
card_first_print('ritual of subdual', 'ICE').
card_image_name('ritual of subdual'/'ICE', 'ritual of subdual').
card_uid('ritual of subdual'/'ICE', 'ICE:Ritual of Subdual:ritual of subdual').
card_rarity('ritual of subdual'/'ICE', 'Rare').
card_artist('ritual of subdual'/'ICE', 'Justin Hampton').
card_flavor_text('ritual of subdual'/'ICE', '\"That which does not bend to the will of Freyalise shall surely break.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('ritual of subdual'/'ICE', '2585').

card_in_set('river delta', 'ICE').
card_original_type('river delta'/'ICE', 'Land').
card_original_text('river delta'/'ICE', 'If there are any depletion counters on River Delta, it does not untap during your untap phase. At the beginning of your upkeep, remove a depletion counter from River Delta.\n{T}: Add {U} to your mana pool. Put a depletion counter on River Delta.\n{T}: Add {B} to your mana pool. Put a depletion counter on River Delta.').
card_first_print('river delta', 'ICE').
card_image_name('river delta'/'ICE', 'river delta').
card_uid('river delta'/'ICE', 'ICE:River Delta:river delta').
card_rarity('river delta'/'ICE', 'Rare').
card_artist('river delta'/'ICE', 'Sandra Everingham').
card_multiverse_id('river delta'/'ICE', '2758').

card_in_set('runed arch', 'ICE').
card_original_type('runed arch'/'ICE', 'Artifact').
card_original_text('runed arch'/'ICE', 'Comes into play tapped.\n{X},{T}: Sacrifice Runed Arch. X target creatures with power no greater than 2 cannot be blocked this turn. Other effects may later be used to increase a creature\'s power beyond 2.').
card_first_print('runed arch', 'ICE').
card_image_name('runed arch'/'ICE', 'runed arch').
card_uid('runed arch'/'ICE', 'ICE:Runed Arch:runed arch').
card_rarity('runed arch'/'ICE', 'Rare').
card_artist('runed arch'/'ICE', 'Phil Foglio').
card_multiverse_id('runed arch'/'ICE', '2420').

card_in_set('sabretooth tiger', 'ICE').
card_original_type('sabretooth tiger'/'ICE', 'Summon — Tiger').
card_original_text('sabretooth tiger'/'ICE', 'First strike').
card_first_print('sabretooth tiger', 'ICE').
card_image_name('sabretooth tiger'/'ICE', 'sabretooth tiger').
card_uid('sabretooth tiger'/'ICE', 'ICE:Sabretooth Tiger:sabretooth tiger').
card_rarity('sabretooth tiger'/'ICE', 'Common').
card_artist('sabretooth tiger'/'ICE', 'Melissa A. Benson').
card_flavor_text('sabretooth tiger'/'ICE', '\"Daughter, it is now your turn to hunt the Tiger, and make a blanket of its fur.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('sabretooth tiger'/'ICE', '2651').

card_in_set('sacred boon', 'ICE').
card_original_type('sacred boon'/'ICE', 'Instant').
card_original_text('sacred boon'/'ICE', 'Prevent up to 3 damage to target creature. At end of turn, put a +0/+1 counter on that creature for each 1 damage prevented by Sacred Boon.').
card_first_print('sacred boon', 'ICE').
card_image_name('sacred boon'/'ICE', 'sacred boon').
card_uid('sacred boon'/'ICE', 'ICE:Sacred Boon:sacred boon').
card_rarity('sacred boon'/'ICE', 'Uncommon').
card_artist('sacred boon'/'ICE', 'Mike Raabe').
card_flavor_text('sacred boon'/'ICE', '\"Divine gifts are granted to those who are worthy.\"\n—Halvor Arenson, Kjeldoran Priest').
card_multiverse_id('sacred boon'/'ICE', '2710').

card_in_set('scaled wurm', 'ICE').
card_original_type('scaled wurm'/'ICE', 'Summon — Wurm').
card_original_text('scaled wurm'/'ICE', '').
card_first_print('scaled wurm', 'ICE').
card_image_name('scaled wurm'/'ICE', 'scaled wurm').
card_uid('scaled wurm'/'ICE', 'ICE:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'ICE', 'Common').
card_artist('scaled wurm'/'ICE', 'Daniel Gelon').
card_flavor_text('scaled wurm'/'ICE', '\"Flourishing during the Ice Age, these Wurms were the bane of all Kjeldorans. Their great size and ferocity made them the subject of countless nightmares—they embodied the worst of the Ice Age.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('scaled wurm'/'ICE', '2586').

card_in_set('sea spirit', 'ICE').
card_original_type('sea spirit'/'ICE', 'Summon — Spirit').
card_original_text('sea spirit'/'ICE', '{U}: +1/+0 until end of turn').
card_first_print('sea spirit', 'ICE').
card_image_name('sea spirit'/'ICE', 'sea spirit').
card_uid('sea spirit'/'ICE', 'ICE:Sea Spirit:sea spirit').
card_rarity('sea spirit'/'ICE', 'Uncommon').
card_artist('sea spirit'/'ICE', 'Rob Alexander').
card_flavor_text('sea spirit'/'ICE', '\"It rose above our heads, above the ship, and still higher yet. No foggy, ice-laden sea in the world could frighten me more.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('sea spirit'/'ICE', '2531').

card_in_set('seizures', 'ICE').
card_original_type('seizures'/'ICE', 'Enchant Creature').
card_original_text('seizures'/'ICE', 'Whenever target creature becomes tapped, that creature\'s controller pays {3} or Seizures deals 3 damage to him or her.').
card_first_print('seizures', 'ICE').
card_image_name('seizures'/'ICE', 'seizures').
card_uid('seizures'/'ICE', 'ICE:Seizures:seizures').
card_rarity('seizures'/'ICE', 'Common').
card_artist('seizures'/'ICE', 'Julie Baroh').
card_flavor_text('seizures'/'ICE', '\"Sheathe your sword, warrior, lest ye fall on it when stricken.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('seizures'/'ICE', '2483').

card_in_set('seraph', 'ICE').
card_original_type('seraph'/'ICE', 'Summon — Angel').
card_original_text('seraph'/'ICE', 'Flying\nAt the end of a turn in which any creature is damaged by Seraph and put into the graveyard, put that creature directly into play under your control as though it were just summoned. If you lose control of Seraph or if Seraph leaves play, bury the creature.').
card_first_print('seraph', 'ICE').
card_image_name('seraph'/'ICE', 'seraph').
card_uid('seraph'/'ICE', 'ICE:Seraph:seraph').
card_rarity('seraph'/'ICE', 'Rare').
card_artist('seraph'/'ICE', 'Christopher Rush').
card_multiverse_id('seraph'/'ICE', '2711').

card_in_set('shambling strider', 'ICE').
card_original_type('shambling strider'/'ICE', 'Summon — Strider').
card_original_text('shambling strider'/'ICE', '{R}{G} +1/-1 until end of turn').
card_first_print('shambling strider', 'ICE').
card_image_name('shambling strider'/'ICE', 'shambling strider').
card_uid('shambling strider'/'ICE', 'ICE:Shambling Strider:shambling strider').
card_rarity('shambling strider'/'ICE', 'Common').
card_artist('shambling strider'/'ICE', 'Douglas Shuler').
card_flavor_text('shambling strider'/'ICE', 'Freyalise forbid that any stranger should wander into the Striders\' territory.').
card_multiverse_id('shambling strider'/'ICE', '2587').

card_in_set('shatter', 'ICE').
card_original_type('shatter'/'ICE', 'Instant').
card_original_text('shatter'/'ICE', 'Destroy target artifact.').
card_image_name('shatter'/'ICE', 'shatter').
card_uid('shatter'/'ICE', 'ICE:Shatter:shatter').
card_rarity('shatter'/'ICE', 'Common').
card_artist('shatter'/'ICE', 'Bryon Wackwitz').
card_flavor_text('shatter'/'ICE', '\"Let the past be the past. Do not call up that which you cannot put down. Destroy that which destroyed us, so long ago.\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('shatter'/'ICE', '2652').

card_in_set('shield bearer', 'ICE').
card_original_type('shield bearer'/'ICE', 'Summon — Soldier').
card_original_text('shield bearer'/'ICE', 'Banding').
card_first_print('shield bearer', 'ICE').
card_image_name('shield bearer'/'ICE', 'shield bearer').
card_uid('shield bearer'/'ICE', 'ICE:Shield Bearer:shield bearer').
card_rarity('shield bearer'/'ICE', 'Common').
card_artist('shield bearer'/'ICE', 'Dan Frazier').
card_flavor_text('shield bearer'/'ICE', '\"You have almost completed your four years, my son. Soon you shall be a Skyknight.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('shield bearer'/'ICE', '2712').

card_in_set('shield of the ages', 'ICE').
card_original_type('shield of the ages'/'ICE', 'Artifact').
card_original_text('shield of the ages'/'ICE', '{2}: Prevent 1 damage to you.').
card_first_print('shield of the ages', 'ICE').
card_image_name('shield of the ages'/'ICE', 'shield of the ages').
card_uid('shield of the ages'/'ICE', 'ICE:Shield of the Ages:shield of the ages').
card_rarity('shield of the ages'/'ICE', 'Uncommon').
card_artist('shield of the ages'/'ICE', 'Anson Maddocks').
card_flavor_text('shield of the ages'/'ICE', '\"This shield is a true rarity: an artifact whose purpose is obvious.\"\n—Arcum Dagsson,\nSoldevi Machinist').
card_multiverse_id('shield of the ages'/'ICE', '2421').

card_in_set('shyft', 'ICE').
card_original_type('shyft'/'ICE', 'Summon — Shyft').
card_original_text('shyft'/'ICE', 'During your upkeep, you may change the color of Shyft to any color or combination of colors.').
card_first_print('shyft', 'ICE').
card_image_name('shyft'/'ICE', 'shyft').
card_uid('shyft'/'ICE', 'ICE:Shyft:shyft').
card_rarity('shyft'/'ICE', 'Rare').
card_artist('shyft'/'ICE', 'Richard Thomas').
card_flavor_text('shyft'/'ICE', '\"Capturing this specimen was not easy. Study it well, that you someday might be as versatile.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('shyft'/'ICE', '2532').

card_in_set('sibilant spirit', 'ICE').
card_original_type('sibilant spirit'/'ICE', 'Summon — Spirit').
card_original_text('sibilant spirit'/'ICE', 'Flying\nWhenever Sibilant Spirit is declared as an attacker, defending player may draw a card.').
card_first_print('sibilant spirit', 'ICE').
card_image_name('sibilant spirit'/'ICE', 'sibilant spirit').
card_uid('sibilant spirit'/'ICE', 'ICE:Sibilant Spirit:sibilant spirit').
card_rarity('sibilant spirit'/'ICE', 'Rare').
card_artist('sibilant spirit'/'ICE', 'Ron Spencer').
card_flavor_text('sibilant spirit'/'ICE', '\"Relax. I\'m sure it\'s just a snake hissing.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('sibilant spirit'/'ICE', '2533').

card_in_set('silver erne', 'ICE').
card_original_type('silver erne'/'ICE', 'Summon — Erne').
card_original_text('silver erne'/'ICE', 'Flying, trample').
card_first_print('silver erne', 'ICE').
card_image_name('silver erne'/'ICE', 'silver erne').
card_uid('silver erne'/'ICE', 'ICE:Silver Erne:silver erne').
card_rarity('silver erne'/'ICE', 'Uncommon').
card_artist('silver erne'/'ICE', 'Melissa A. Benson').
card_flavor_text('silver erne'/'ICE', '\"I\'ve seen a larger Erne knock a Giant to the ground and stay airborne. They move not with the wind, but as the wind.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('silver erne'/'ICE', '2534').

card_in_set('skeleton ship', 'ICE').
card_original_type('skeleton ship'/'ICE', 'Summon — Legend').
card_original_text('skeleton ship'/'ICE', 'If at any time you control no islands, bury Skeleton Ship.\n{T}: Put a -1/-1 counter on target creature.').
card_first_print('skeleton ship', 'ICE').
card_image_name('skeleton ship'/'ICE', 'skeleton ship').
card_uid('skeleton ship'/'ICE', 'ICE:Skeleton Ship:skeleton ship').
card_rarity('skeleton ship'/'ICE', 'Rare').
card_artist('skeleton ship'/'ICE', 'Amy Weber & Tom Wänerstrand').
card_flavor_text('skeleton ship'/'ICE', '\"The sea gives up her dead as easily as the soil.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('skeleton ship'/'ICE', '2737').

card_in_set('skull catapult', 'ICE').
card_original_type('skull catapult'/'ICE', 'Artifact').
card_original_text('skull catapult'/'ICE', '{1},{T}: Sacrifice a creature to have Skull Catapult deal 2 damage to target creature or player.').
card_first_print('skull catapult', 'ICE').
card_image_name('skull catapult'/'ICE', 'skull catapult').
card_uid('skull catapult'/'ICE', 'ICE:Skull Catapult:skull catapult').
card_rarity('skull catapult'/'ICE', 'Uncommon').
card_artist('skull catapult'/'ICE', 'Bryon Wackwitz').
card_flavor_text('skull catapult'/'ICE', '\"Let any who doubt the evil of using the ancient devices look at this infernal machine. What manner of fiend would design such a sadistic device?\"\n—Sorine Relicbane, Soldevi Heretic').
card_multiverse_id('skull catapult'/'ICE', '2422').

card_in_set('sleight of mind', 'ICE').
card_original_type('sleight of mind'/'ICE', 'Interrupt').
card_original_text('sleight of mind'/'ICE', 'Change the text of target spell or permanent by replacing all instances of one color word with another. For example, you may change \"Counters black spells\" to \"Counters blue spells.\" Sleight of Mind cannot change mana symbols.').
card_image_name('sleight of mind'/'ICE', 'sleight of mind').
card_uid('sleight of mind'/'ICE', 'ICE:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'ICE', 'Uncommon').
card_artist('sleight of mind'/'ICE', 'Nicola Leonard').
card_multiverse_id('sleight of mind'/'ICE', '2535').

card_in_set('snow devil', 'ICE').
card_original_type('snow devil'/'ICE', 'Enchant Creature').
card_original_text('snow devil'/'ICE', 'Target creature gains flying. As long as you control any snow-covered lands, that creature also gains first strike when blocking.').
card_first_print('snow devil', 'ICE').
card_image_name('snow devil'/'ICE', 'snow devil').
card_uid('snow devil'/'ICE', 'ICE:Snow Devil:snow devil').
card_rarity('snow devil'/'ICE', 'Common').
card_artist('snow devil'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('snow devil'/'ICE', '\"Give me wings to fly and speed to strike. In return, the glory I earn shall be yours.\" —Steinar Icefist, Balduvian Shaman').
card_multiverse_id('snow devil'/'ICE', '2536').

card_in_set('snow fortress', 'ICE').
card_original_type('snow fortress'/'ICE', 'Artifact Creature').
card_original_text('snow fortress'/'ICE', 'Counts as a wall\n{1}: +1/+0 until end of turn\n{1}: +0/+1 until end of turn\n{3}: Snow Fortress deals 1 damage to target creature without flying that is attacking you.').
card_first_print('snow fortress', 'ICE').
card_image_name('snow fortress'/'ICE', 'snow fortress').
card_uid('snow fortress'/'ICE', 'ICE:Snow Fortress:snow fortress').
card_rarity('snow fortress'/'ICE', 'Rare').
card_artist('snow fortress'/'ICE', 'Jeff A. Menges').
card_multiverse_id('snow fortress'/'ICE', '2423').

card_in_set('snow hound', 'ICE').
card_original_type('snow hound'/'ICE', 'Summon — Dog').
card_original_text('snow hound'/'ICE', '{1},{T}: Return Snow Hound to owner\'s hand and target blue or green creature you control to owner\'s hand.').
card_first_print('snow hound', 'ICE').
card_image_name('snow hound'/'ICE', 'snow hound').
card_uid('snow hound'/'ICE', 'ICE:Snow Hound:snow hound').
card_rarity('snow hound'/'ICE', 'Uncommon').
card_artist('snow hound'/'ICE', 'Pat Morrissey').
card_flavor_text('snow hound'/'ICE', '\"If you\'re starving, eat your horses, your dead, or yourself—but never eat your dog.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('snow hound'/'ICE', '2713').

card_in_set('snow-covered forest', 'ICE').
card_original_type('snow-covered forest'/'ICE', 'Land').
card_original_text('snow-covered forest'/'ICE', '{T}: Add {G} to your mana pool.').
card_first_print('snow-covered forest', 'ICE').
card_image_name('snow-covered forest'/'ICE', 'snow-covered forest').
card_uid('snow-covered forest'/'ICE', 'ICE:Snow-Covered Forest:snow-covered forest').
card_rarity('snow-covered forest'/'ICE', 'Basic Land').
card_artist('snow-covered forest'/'ICE', 'Pat Morrissey').
card_multiverse_id('snow-covered forest'/'ICE', '2749').

card_in_set('snow-covered island', 'ICE').
card_original_type('snow-covered island'/'ICE', 'Land').
card_original_text('snow-covered island'/'ICE', '{T}: Add {U} to your mana pool.').
card_first_print('snow-covered island', 'ICE').
card_image_name('snow-covered island'/'ICE', 'snow-covered island').
card_uid('snow-covered island'/'ICE', 'ICE:Snow-Covered Island:snow-covered island').
card_rarity('snow-covered island'/'ICE', 'Basic Land').
card_artist('snow-covered island'/'ICE', 'Anson Maddocks').
card_multiverse_id('snow-covered island'/'ICE', '2770').

card_in_set('snow-covered mountain', 'ICE').
card_original_type('snow-covered mountain'/'ICE', 'Land').
card_original_text('snow-covered mountain'/'ICE', '{T}: Add {R} to your mana pool.').
card_first_print('snow-covered mountain', 'ICE').
card_image_name('snow-covered mountain'/'ICE', 'snow-covered mountain').
card_uid('snow-covered mountain'/'ICE', 'ICE:Snow-Covered Mountain:snow-covered mountain').
card_rarity('snow-covered mountain'/'ICE', 'Basic Land').
card_artist('snow-covered mountain'/'ICE', 'Tom Wänerstrand').
card_multiverse_id('snow-covered mountain'/'ICE', '2766').

card_in_set('snow-covered plains', 'ICE').
card_original_type('snow-covered plains'/'ICE', 'Land').
card_original_text('snow-covered plains'/'ICE', '{T}: Add {W} to your mana pool.').
card_first_print('snow-covered plains', 'ICE').
card_image_name('snow-covered plains'/'ICE', 'snow-covered plains').
card_uid('snow-covered plains'/'ICE', 'ICE:Snow-Covered Plains:snow-covered plains').
card_rarity('snow-covered plains'/'ICE', 'Basic Land').
card_artist('snow-covered plains'/'ICE', 'Christopher Rush').
card_multiverse_id('snow-covered plains'/'ICE', '2774').

card_in_set('snow-covered swamp', 'ICE').
card_original_type('snow-covered swamp'/'ICE', 'Land').
card_original_text('snow-covered swamp'/'ICE', '{T}: Add {B} to your mana pool.').
card_first_print('snow-covered swamp', 'ICE').
card_image_name('snow-covered swamp'/'ICE', 'snow-covered swamp').
card_uid('snow-covered swamp'/'ICE', 'ICE:Snow-Covered Swamp:snow-covered swamp').
card_rarity('snow-covered swamp'/'ICE', 'Basic Land').
card_artist('snow-covered swamp'/'ICE', 'Douglas Shuler').
card_multiverse_id('snow-covered swamp'/'ICE', '2742').

card_in_set('snowblind', 'ICE').
card_original_type('snowblind'/'ICE', 'Enchant Creature').
card_original_text('snowblind'/'ICE', 'Target creature gets -*/-*. When that creature attacks, * is equal to the number of snow-covered lands defending player controls. At other times, * is equal to the number of snow-covered lands its controller controls. If this reduces the creature\'s toughness to less than 1, the creature\'s toughness is 1.').
card_first_print('snowblind', 'ICE').
card_image_name('snowblind'/'ICE', 'snowblind').
card_uid('snowblind'/'ICE', 'ICE:Snowblind:snowblind').
card_rarity('snowblind'/'ICE', 'Rare').
card_artist('snowblind'/'ICE', 'Douglas Shuler').
card_multiverse_id('snowblind'/'ICE', '2588').

card_in_set('snowfall', 'ICE').
card_original_type('snowfall'/'ICE', 'Enchantment').
card_original_text('snowfall'/'ICE', 'Cumulative Upkeep: {U}\nIslands may produce an additional {U} when tapped for mana. This mana is usable only for cumulative upkeep.\nSnow-covered islands may produce either an additional {U}{U} or an additional {U} when tapped for mana. This mana is usable only for cumulative upkeep.').
card_first_print('snowfall', 'ICE').
card_image_name('snowfall'/'ICE', 'snowfall').
card_uid('snowfall'/'ICE', 'ICE:Snowfall:snowfall').
card_rarity('snowfall'/'ICE', 'Common').
card_artist('snowfall'/'ICE', 'Phil Foglio').
card_multiverse_id('snowfall'/'ICE', '2537').

card_in_set('soldevi golem', 'ICE').
card_original_type('soldevi golem'/'ICE', 'Artifact Creature').
card_original_text('soldevi golem'/'ICE', 'Does not untap during your untap phase.\n{0}: Untap target creature opponent controls to untap Soldevi Golem at the end of your upkeep. Use this ability only during your upkeep.').
card_first_print('soldevi golem', 'ICE').
card_image_name('soldevi golem'/'ICE', 'soldevi golem').
card_uid('soldevi golem'/'ICE', 'ICE:Soldevi Golem:soldevi golem').
card_rarity('soldevi golem'/'ICE', 'Rare').
card_artist('soldevi golem'/'ICE', 'Anson Maddocks').
card_flavor_text('soldevi golem'/'ICE', 'Slow and steady wins the race.').
card_multiverse_id('soldevi golem'/'ICE', '2424').

card_in_set('soldevi machinist', 'ICE').
card_original_type('soldevi machinist'/'ICE', 'Summon — Wizard').
card_original_text('soldevi machinist'/'ICE', '{T}: Add two colorless mana to your mana pool. This mana may only be used to pay the activation cost of an artifact. Play this ability as an interrupt.').
card_first_print('soldevi machinist', 'ICE').
card_image_name('soldevi machinist'/'ICE', 'soldevi machinist').
card_uid('soldevi machinist'/'ICE', 'ICE:Soldevi Machinist:soldevi machinist').
card_rarity('soldevi machinist'/'ICE', 'Uncommon').
card_artist('soldevi machinist'/'ICE', 'Jeff A. Menges').
card_flavor_text('soldevi machinist'/'ICE', '\"Perhaps this time the power of the artificers shall be used wisely.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('soldevi machinist'/'ICE', '2538').

card_in_set('soldevi simulacrum', 'ICE').
card_original_type('soldevi simulacrum'/'ICE', 'Artifact Creature').
card_original_text('soldevi simulacrum'/'ICE', 'Cumulative Upkeep: {1}\n{1}: +1/+0 until end of turn').
card_first_print('soldevi simulacrum', 'ICE').
card_image_name('soldevi simulacrum'/'ICE', 'soldevi simulacrum').
card_uid('soldevi simulacrum'/'ICE', 'ICE:Soldevi Simulacrum:soldevi simulacrum').
card_rarity('soldevi simulacrum'/'ICE', 'Uncommon').
card_artist('soldevi simulacrum'/'ICE', 'Dan Frazier').
card_flavor_text('soldevi simulacrum'/'ICE', 'They look human—\nuntil they bleed.').
card_multiverse_id('soldevi simulacrum'/'ICE', '2425').

card_in_set('songs of the damned', 'ICE').
card_original_type('songs of the damned'/'ICE', 'Interrupt').
card_original_text('songs of the damned'/'ICE', 'Add {B} to your mana pool for each creature in your graveyard.').
card_first_print('songs of the damned', 'ICE').
card_image_name('songs of the damned'/'ICE', 'songs of the damned').
card_uid('songs of the damned'/'ICE', 'ICE:Songs of the Damned:songs of the damned').
card_rarity('songs of the damned'/'ICE', 'Common').
card_artist('songs of the damned'/'ICE', 'Pete Venters').
card_flavor_text('songs of the damned'/'ICE', 'Not wind, but the breath of the dead.').
card_multiverse_id('songs of the damned'/'ICE', '2484').

card_in_set('soul barrier', 'ICE').
card_original_type('soul barrier'/'ICE', 'Enchantment').
card_original_text('soul barrier'/'ICE', 'Whenever target opponent casts a summon spell, Soul Barrier deals 2 damage to him or her. That player may pay {2} to prevent this damage.').
card_first_print('soul barrier', 'ICE').
card_image_name('soul barrier'/'ICE', 'soul barrier').
card_uid('soul barrier'/'ICE', 'ICE:Soul Barrier:soul barrier').
card_rarity('soul barrier'/'ICE', 'Uncommon').
card_artist('soul barrier'/'ICE', 'Harold McNeill').
card_flavor_text('soul barrier'/'ICE', '\"There\'s more than one way to skin an Ouphe—or thwart a mage.\"\n—Gustha Ebbasdotter,\nKjeldoran Royal Mage').
card_multiverse_id('soul barrier'/'ICE', '2539').

card_in_set('soul burn', 'ICE').
card_original_type('soul burn'/'ICE', 'Sorcery').
card_original_text('soul burn'/'ICE', 'Soul Burn deals 1 damage to a single target creature or player for each {B} or {R} you pay in addition to the casting cost. Gain 1 life for each {B} you spend in this way. You cannot gain more life than the toughness of the creature or the total life of the targeted player.').
card_first_print('soul burn', 'ICE').
card_image_name('soul burn'/'ICE', 'soul burn').
card_uid('soul burn'/'ICE', 'ICE:Soul Burn:soul burn').
card_rarity('soul burn'/'ICE', 'Common').
card_artist('soul burn'/'ICE', 'Rob Alexander').
card_multiverse_id('soul burn'/'ICE', '2485').

card_in_set('soul kiss', 'ICE').
card_original_type('soul kiss'/'ICE', 'Enchant Creature').
card_original_text('soul kiss'/'ICE', 'When Soul Kiss comes into play, choose target creature.\n{B}: Pay 1 life to give creature Soul Kiss enchants +2/+2 until end of turn. You cannot spend more than {B}{B}{B} in this way each turn. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('soul kiss', 'ICE').
card_image_name('soul kiss'/'ICE', 'soul kiss').
card_uid('soul kiss'/'ICE', 'ICE:Soul Kiss:soul kiss').
card_rarity('soul kiss'/'ICE', 'Common').
card_artist('soul kiss'/'ICE', 'Nicola Leonard').
card_multiverse_id('soul kiss'/'ICE', '2486').

card_in_set('spectral shield', 'ICE').
card_original_type('spectral shield'/'ICE', 'Enchant Creature').
card_original_text('spectral shield'/'ICE', 'Target creature gets +0/+2. That creature cannot be the target of further spells.').
card_first_print('spectral shield', 'ICE').
card_image_name('spectral shield'/'ICE', 'spectral shield').
card_uid('spectral shield'/'ICE', 'ICE:Spectral Shield:spectral shield').
card_rarity('spectral shield'/'ICE', 'Uncommon').
card_artist('spectral shield'/'ICE', 'Margaret Organ-Kean').
card_flavor_text('spectral shield'/'ICE', '\"What can be a stronger shield than concealment? Have you ever defeated an enemy you didn\'t know existed?\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('spectral shield'/'ICE', '2738').

card_in_set('spoils of evil', 'ICE').
card_original_type('spoils of evil'/'ICE', 'Interrupt').
card_original_text('spoils of evil'/'ICE', 'For each artifact or creature in target opponent\'s graveyard, add one colorless mana to your mana pool and gain 1 life.').
card_first_print('spoils of evil', 'ICE').
card_image_name('spoils of evil'/'ICE', 'spoils of evil').
card_uid('spoils of evil'/'ICE', 'ICE:Spoils of Evil:spoils of evil').
card_rarity('spoils of evil'/'ICE', 'Rare').
card_artist('spoils of evil'/'ICE', 'Quinton Hoover').
card_flavor_text('spoils of evil'/'ICE', '\"Virtue has its rewards, as does its opposite.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('spoils of evil'/'ICE', '2487').

card_in_set('spoils of war', 'ICE').
card_original_type('spoils of war'/'ICE', 'Sorcery').
card_original_text('spoils of war'/'ICE', 'Put X +1/+1 counters on any number of target creatures, distributed any way you choose, where X is equal to the number of creatures and artifacts in target opponent\'s graveyard.').
card_first_print('spoils of war', 'ICE').
card_image_name('spoils of war'/'ICE', 'spoils of war').
card_uid('spoils of war'/'ICE', 'ICE:Spoils of War:spoils of war').
card_rarity('spoils of war'/'ICE', 'Rare').
card_artist('spoils of war'/'ICE', 'Pete Venters').
card_flavor_text('spoils of war'/'ICE', '\"This is the fun part!\"\n—Ib Halfheart, Goblin Tactician').
card_multiverse_id('spoils of war'/'ICE', '2488').

card_in_set('staff of the ages', 'ICE').
card_original_type('staff of the ages'/'ICE', 'Artifact').
card_original_text('staff of the ages'/'ICE', 'Creatures with any landwalk ability may be blocked as though they did not have those abilities.').
card_first_print('staff of the ages', 'ICE').
card_image_name('staff of the ages'/'ICE', 'staff of the ages').
card_uid('staff of the ages'/'ICE', 'ICE:Staff of the Ages:staff of the ages').
card_rarity('staff of the ages'/'ICE', 'Rare').
card_artist('staff of the ages'/'ICE', 'Daniel Gelon').
card_flavor_text('staff of the ages'/'ICE', '\"We found this staff useful on our visit to this very commission. Would Relicbane prefer we had been slain by Lim-Dûl\'s horrors?\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('staff of the ages'/'ICE', '2426').

card_in_set('stampede', 'ICE').
card_original_type('stampede'/'ICE', 'Instant').
card_original_text('stampede'/'ICE', 'All attacking creatures gain trample and get +1/+0 until end of turn.').
card_first_print('stampede', 'ICE').
card_image_name('stampede'/'ICE', 'stampede').
card_uid('stampede'/'ICE', 'ICE:Stampede:stampede').
card_rarity('stampede'/'ICE', 'Rare').
card_artist('stampede'/'ICE', 'Jeff A. Menges').
card_flavor_text('stampede'/'ICE', '\"We could see the horizon blacken with the great beasts, but it was too late. The icefield offered no immediate safety, but luckily most of us reached a crevasse in which we could take cover.\"\n—Disa the Restless, journal entry').
card_multiverse_id('stampede'/'ICE', '2589').

card_in_set('stench of evil', 'ICE').
card_original_type('stench of evil'/'ICE', 'Sorcery').
card_original_text('stench of evil'/'ICE', 'Destroy all plains. Stench of Evil deals 1 damage to each player for each plains he or she controls that is destroyed in this way. Each player may pay {2} for each 1 damage he or she wishes to prevent from Stench of Evil.').
card_first_print('stench of evil', 'ICE').
card_image_name('stench of evil'/'ICE', 'stench of evil').
card_uid('stench of evil'/'ICE', 'ICE:Stench of Evil:stench of evil').
card_rarity('stench of evil'/'ICE', 'Uncommon').
card_artist('stench of evil'/'ICE', 'Mark Tedin').
card_multiverse_id('stench of evil'/'ICE', '2489').

card_in_set('stone rain', 'ICE').
card_original_type('stone rain'/'ICE', 'Sorcery').
card_original_text('stone rain'/'ICE', 'Destroy target land.').
card_image_name('stone rain'/'ICE', 'stone rain').
card_uid('stone rain'/'ICE', 'ICE:Stone Rain:stone rain').
card_rarity('stone rain'/'ICE', 'Common').
card_artist('stone rain'/'ICE', 'Kaja Foglio').
card_flavor_text('stone rain'/'ICE', '\"May the forces that took Argoth beneath the waves never come among us again.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('stone rain'/'ICE', '2653').

card_in_set('stone spirit', 'ICE').
card_original_type('stone spirit'/'ICE', 'Summon — Spirit').
card_original_text('stone spirit'/'ICE', 'Cannot be blocked by creatures with flying.').
card_first_print('stone spirit', 'ICE').
card_image_name('stone spirit'/'ICE', 'stone spirit').
card_uid('stone spirit'/'ICE', 'ICE:Stone Spirit:stone spirit').
card_rarity('stone spirit'/'ICE', 'Uncommon').
card_artist('stone spirit'/'ICE', 'Jeff A. Menges').
card_flavor_text('stone spirit'/'ICE', '\"The spirit of the stone is the spirit of strength.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('stone spirit'/'ICE', '2654').

card_in_set('stonehands', 'ICE').
card_original_type('stonehands'/'ICE', 'Enchant Creature').
card_original_text('stonehands'/'ICE', 'Target creature gets +0/+2.\n{R}: Creature Stonehands enchants gets +1/+0 until end of turn.').
card_first_print('stonehands', 'ICE').
card_image_name('stonehands'/'ICE', 'stonehands').
card_uid('stonehands'/'ICE', 'ICE:Stonehands:stonehands').
card_rarity('stonehands'/'ICE', 'Common').
card_artist('stonehands'/'ICE', 'Dan Frazier').
card_flavor_text('stonehands'/'ICE', '\"Trust in the power of stone. Stone is strong; stone shatters swords; stone breaks bones. Trust in stone.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('stonehands'/'ICE', '2655').

card_in_set('storm spirit', 'ICE').
card_original_type('storm spirit'/'ICE', 'Summon — Spirit').
card_original_text('storm spirit'/'ICE', 'Flying\n{T}: Storm Spirit deals 2 damage to target creature.').
card_first_print('storm spirit', 'ICE').
card_image_name('storm spirit'/'ICE', 'storm spirit').
card_uid('storm spirit'/'ICE', 'ICE:Storm Spirit:storm spirit').
card_rarity('storm spirit'/'ICE', 'Rare').
card_artist('storm spirit'/'ICE', 'Pete Venters').
card_flavor_text('storm spirit'/'ICE', '\"Come to us, with your lightning. Come to us, with your thunder. Serve us with your strength, and smite our foes with your power.\"\n—Steinar Icefist, Balduvian Shaman').
card_multiverse_id('storm spirit'/'ICE', '2739').

card_in_set('stormbind', 'ICE').
card_original_type('stormbind'/'ICE', 'Enchantment').
card_original_text('stormbind'/'ICE', '{2}: Discard a card at random from your hand to have Stormbind deal 2 damage to target creature or player.').
card_first_print('stormbind', 'ICE').
card_image_name('stormbind'/'ICE', 'stormbind').
card_uid('stormbind'/'ICE', 'ICE:Stormbind:stormbind').
card_rarity('stormbind'/'ICE', 'Rare').
card_artist('stormbind'/'ICE', 'NéNé Thomas & Phillip Mosness').
card_flavor_text('stormbind'/'ICE', '\"Once, our people could call down the storm itself to do our bidding.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('stormbind'/'ICE', '2740').

card_in_set('stromgald cabal', 'ICE').
card_original_type('stromgald cabal'/'ICE', 'Summon — Knights').
card_original_text('stromgald cabal'/'ICE', '{T}: Pay 1 life to counter target white spell. Effects that prevent or redirect damage cannot be used to counter this loss of life. Play this ability as an interrupt.').
card_first_print('stromgald cabal', 'ICE').
card_image_name('stromgald cabal'/'ICE', 'stromgald cabal').
card_uid('stromgald cabal'/'ICE', 'ICE:Stromgald Cabal:stromgald cabal').
card_rarity('stromgald cabal'/'ICE', 'Rare').
card_artist('stromgald cabal'/'ICE', 'Anson Maddocks').
card_flavor_text('stromgald cabal'/'ICE', '\"Kjeldor must be supreme at any cost.\"\n—Avram Garrisson, Leader of the Knights of Stromgald').
card_multiverse_id('stromgald cabal'/'ICE', '2490').

card_in_set('stunted growth', 'ICE').
card_original_type('stunted growth'/'ICE', 'Sorcery').
card_original_text('stunted growth'/'ICE', 'Target player chooses three cards from his or her hand and puts them on top of his or her library in any order. If that player does not have enough cards in hand, his or her entire hand is put on top of his or her library in any order.').
card_first_print('stunted growth', 'ICE').
card_image_name('stunted growth'/'ICE', 'stunted growth').
card_uid('stunted growth'/'ICE', 'ICE:Stunted Growth:stunted growth').
card_rarity('stunted growth'/'ICE', 'Rare').
card_artist('stunted growth'/'ICE', 'NéNé Thomas').
card_multiverse_id('stunted growth'/'ICE', '2590').

card_in_set('sulfurous springs', 'ICE').
card_original_type('sulfurous springs'/'ICE', 'Land').
card_original_text('sulfurous springs'/'ICE', '{T}: Add {1} to your mana pool.\n{T}: Add {B} to your mana pool. Sulfurous Springs deals 1 damage to you.\n{T}: Add {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_first_print('sulfurous springs', 'ICE').
card_image_name('sulfurous springs'/'ICE', 'sulfurous springs').
card_uid('sulfurous springs'/'ICE', 'ICE:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'ICE', 'Rare').
card_artist('sulfurous springs'/'ICE', 'Phil Foglio').
card_multiverse_id('sulfurous springs'/'ICE', '2759').

card_in_set('sunstone', 'ICE').
card_original_type('sunstone'/'ICE', 'Artifact').
card_original_text('sunstone'/'ICE', '{2}: Sacrifice a snow-covered land to have all creatures deal no damage in combat this turn.').
card_first_print('sunstone', 'ICE').
card_image_name('sunstone'/'ICE', 'sunstone').
card_uid('sunstone'/'ICE', 'ICE:Sunstone:sunstone').
card_rarity('sunstone'/'ICE', 'Uncommon').
card_artist('sunstone'/'ICE', 'Phil Foglio').
card_flavor_text('sunstone'/'ICE', '\"I dispute Relicbane\'s hysterical allegations, Commissioner, but it is true that these artifacts can be dangerous.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('sunstone'/'ICE', '2427').

card_in_set('swamp', 'ICE').
card_original_type('swamp'/'ICE', 'Land').
card_original_text('swamp'/'ICE', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'ICE', 'swamp1').
card_uid('swamp'/'ICE', 'ICE:Swamp:swamp1').
card_rarity('swamp'/'ICE', 'Basic Land').
card_artist('swamp'/'ICE', 'Douglas Shuler').
card_multiverse_id('swamp'/'ICE', '2744').

card_in_set('swamp', 'ICE').
card_original_type('swamp'/'ICE', 'Land').
card_original_text('swamp'/'ICE', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'ICE', 'swamp2').
card_uid('swamp'/'ICE', 'ICE:Swamp:swamp2').
card_rarity('swamp'/'ICE', 'Basic Land').
card_artist('swamp'/'ICE', 'Douglas Shuler').
card_multiverse_id('swamp'/'ICE', '2743').

card_in_set('swamp', 'ICE').
card_original_type('swamp'/'ICE', 'Land').
card_original_text('swamp'/'ICE', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'ICE', 'swamp3').
card_uid('swamp'/'ICE', 'ICE:Swamp:swamp3').
card_rarity('swamp'/'ICE', 'Basic Land').
card_artist('swamp'/'ICE', 'Douglas Shuler').
card_multiverse_id('swamp'/'ICE', '2745').

card_in_set('swords to plowshares', 'ICE').
card_original_type('swords to plowshares'/'ICE', 'Instant').
card_original_text('swords to plowshares'/'ICE', 'Remove target creature from the game. That creature\'s controller gains life equal to its power.').
card_image_name('swords to plowshares'/'ICE', 'swords to plowshares').
card_uid('swords to plowshares'/'ICE', 'ICE:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'ICE', 'Uncommon').
card_artist('swords to plowshares'/'ICE', 'Kaja Foglio').
card_flavor_text('swords to plowshares'/'ICE', '\"The so-called Barbarians will not respect us for our military might—they will respect us for our honor.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('swords to plowshares'/'ICE', '2714').

card_in_set('tarpan', 'ICE').
card_original_type('tarpan'/'ICE', 'Summon — Tarpan').
card_original_text('tarpan'/'ICE', 'If Tarpan is put into the graveyard from play, gain 1 life.').
card_first_print('tarpan', 'ICE').
card_image_name('tarpan'/'ICE', 'tarpan').
card_uid('tarpan'/'ICE', 'ICE:Tarpan:tarpan').
card_rarity('tarpan'/'ICE', 'Common').
card_artist('tarpan'/'ICE', 'Margaret Organ-Kean').
card_flavor_text('tarpan'/'ICE', '\"A good Tarpan will serve you, faithful and true. A bad one will kick you in the head.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('tarpan'/'ICE', '2591').

card_in_set('thermokarst', 'ICE').
card_original_type('thermokarst'/'ICE', 'Sorcery').
card_original_text('thermokarst'/'ICE', 'Destroy target land. If that land is a snow-covered land, gain 1 life.').
card_first_print('thermokarst', 'ICE').
card_image_name('thermokarst'/'ICE', 'thermokarst').
card_uid('thermokarst'/'ICE', 'ICE:Thermokarst:thermokarst').
card_rarity('thermokarst'/'ICE', 'Uncommon').
card_artist('thermokarst'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('thermokarst'/'ICE', '\"Finally we understand the lesson of our home: loss begets renewal.\"\n—Kolbjörn, Elder Druid of the Juniper Order').
card_multiverse_id('thermokarst'/'ICE', '2592').

card_in_set('thoughtleech', 'ICE').
card_original_type('thoughtleech'/'ICE', 'Enchantment').
card_original_text('thoughtleech'/'ICE', 'Whenever an island controlled by target opponent becomes tapped, gain 1 life.').
card_first_print('thoughtleech', 'ICE').
card_image_name('thoughtleech'/'ICE', 'thoughtleech').
card_uid('thoughtleech'/'ICE', 'ICE:Thoughtleech:thoughtleech').
card_rarity('thoughtleech'/'ICE', 'Uncommon').
card_artist('thoughtleech'/'ICE', 'Mark Tedin').
card_flavor_text('thoughtleech'/'ICE', '\"A resourceful mage has many sources of information. The best one is your foe.\"\n—Zur the Enchanter').
card_multiverse_id('thoughtleech'/'ICE', '2593').

card_in_set('thunder wall', 'ICE').
card_original_type('thunder wall'/'ICE', 'Summon — Wall').
card_original_text('thunder wall'/'ICE', 'Flying\n{U} +1/+1 until end of turn').
card_first_print('thunder wall', 'ICE').
card_image_name('thunder wall'/'ICE', 'thunder wall').
card_uid('thunder wall'/'ICE', 'ICE:Thunder Wall:thunder wall').
card_rarity('thunder wall'/'ICE', 'Uncommon').
card_artist('thunder wall'/'ICE', 'Richard Thomas').
card_flavor_text('thunder wall'/'ICE', '\"The Lemures had barely taken wing when the sky roared with thunder. The swarm of little beasts wavered, divided, and fell, crashing to the earth.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('thunder wall'/'ICE', '2540').

card_in_set('timberline ridge', 'ICE').
card_original_type('timberline ridge'/'ICE', 'Land').
card_original_text('timberline ridge'/'ICE', 'If there are any depletion counters on Timberline Ridge, it does not untap during your untap phase. At the beginning of your upkeep, remove a depletion counter from Timberline Ridge.\n{T}: Add {R} to your mana pool. Put a depletion counter on Timberline Ridge.\n{T}: Add {G} to your mana pool. Put a depletion counter on Timberline Ridge.').
card_first_print('timberline ridge', 'ICE').
card_image_name('timberline ridge'/'ICE', 'timberline ridge').
card_uid('timberline ridge'/'ICE', 'ICE:Timberline Ridge:timberline ridge').
card_rarity('timberline ridge'/'ICE', 'Rare').
card_artist('timberline ridge'/'ICE', 'Jeff A. Menges').
card_multiverse_id('timberline ridge'/'ICE', '2760').

card_in_set('time bomb', 'ICE').
card_original_type('time bomb'/'ICE', 'Artifact').
card_original_text('time bomb'/'ICE', 'During your upkeep, put a time counter on Time Bomb.\n{1},{T}: Sacrifice Time Bomb to have it deal * damage to each creature and player, where * is equal to the number of time counters on Time Bomb.').
card_first_print('time bomb', 'ICE').
card_image_name('time bomb'/'ICE', 'time bomb').
card_uid('time bomb'/'ICE', 'ICE:Time Bomb:time bomb').
card_rarity('time bomb'/'ICE', 'Rare').
card_artist('time bomb'/'ICE', 'Amy Weber').
card_multiverse_id('time bomb'/'ICE', '2428').

card_in_set('tinder wall', 'ICE').
card_original_type('tinder wall'/'ICE', 'Summon — Wall').
card_original_text('tinder wall'/'ICE', '{0}: Sacrifice Tinder Wall to add {R}{R} to your mana pool. Play this ability as an interrupt.\n{R} Sacrifice Tinder Wall to have it deal 2 damage to target creature it blocks.').
card_first_print('tinder wall', 'ICE').
card_image_name('tinder wall'/'ICE', 'tinder wall').
card_uid('tinder wall'/'ICE', 'ICE:Tinder Wall:tinder wall').
card_rarity('tinder wall'/'ICE', 'Common').
card_artist('tinder wall'/'ICE', 'Rick Emond').
card_multiverse_id('tinder wall'/'ICE', '2594').

card_in_set('tor giant', 'ICE').
card_original_type('tor giant'/'ICE', 'Summon — Giant').
card_original_text('tor giant'/'ICE', '').
card_first_print('tor giant', 'ICE').
card_image_name('tor giant'/'ICE', 'tor giant').
card_uid('tor giant'/'ICE', 'ICE:Tor Giant:tor giant').
card_rarity('tor giant'/'ICE', 'Common').
card_artist('tor giant'/'ICE', 'Douglas Shuler').
card_flavor_text('tor giant'/'ICE', '\"What do you do then? Run. Run very fast. Don\'t stop until you see the camp—or a bigger Giant.\"\n—Toothlicker Harj,\nOrcish Captain').
card_multiverse_id('tor giant'/'ICE', '2656').

card_in_set('total war', 'ICE').
card_original_type('total war'/'ICE', 'Enchantment').
card_original_text('total war'/'ICE', 'Whenever any player declares an attack, destroy all untapped non-wall creatures that player controls that don\'t attack. Do not destroy creatures the player did not control at the beginning of the turn.').
card_first_print('total war', 'ICE').
card_image_name('total war'/'ICE', 'total war').
card_uid('total war'/'ICE', 'ICE:Total War:total war').
card_rarity('total war'/'ICE', 'Rare').
card_artist('total war'/'ICE', 'Drew Tucker').
card_multiverse_id('total war'/'ICE', '2657').

card_in_set('touch of death', 'ICE').
card_original_type('touch of death'/'ICE', 'Sorcery').
card_original_text('touch of death'/'ICE', 'Touch of Death deals 1 damage to target player, and you gain 1 life. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('touch of death', 'ICE').
card_image_name('touch of death'/'ICE', 'touch of death').
card_uid('touch of death'/'ICE', 'ICE:Touch of Death:touch of death').
card_rarity('touch of death'/'ICE', 'Common').
card_artist('touch of death'/'ICE', 'Melissa A. Benson').
card_flavor_text('touch of death'/'ICE', '\"What was yours is mine. Your land, your people, and now your life.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('touch of death'/'ICE', '2491').

card_in_set('touch of vitae', 'ICE').
card_original_type('touch of vitae'/'ICE', 'Instant').
card_original_text('touch of vitae'/'ICE', 'Target creature may untap one additional time this turn. That creature may attack or use abilities that require {T} as part of the activation cost this turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('touch of vitae', 'ICE').
card_image_name('touch of vitae'/'ICE', 'touch of vitae').
card_uid('touch of vitae'/'ICE', 'ICE:Touch of Vitae:touch of vitae').
card_rarity('touch of vitae'/'ICE', 'Uncommon').
card_artist('touch of vitae'/'ICE', 'L. A. Williams').
card_multiverse_id('touch of vitae'/'ICE', '2595').

card_in_set('trailblazer', 'ICE').
card_original_type('trailblazer'/'ICE', 'Instant').
card_original_text('trailblazer'/'ICE', 'Target creature cannot be blocked this turn.').
card_first_print('trailblazer', 'ICE').
card_image_name('trailblazer'/'ICE', 'trailblazer').
card_uid('trailblazer'/'ICE', 'ICE:Trailblazer:trailblazer').
card_rarity('trailblazer'/'ICE', 'Rare').
card_artist('trailblazer'/'ICE', 'Julie Baroh').
card_flavor_text('trailblazer'/'ICE', '\"Our Elvish Hunter Taaveti led us swiftly along hidden paths through the dense forest. We caught the Orcs from behind, and completely by surprise.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('trailblazer'/'ICE', '2596').

card_in_set('underground river', 'ICE').
card_original_type('underground river'/'ICE', 'Land').
card_original_text('underground river'/'ICE', '{T}: Add {1} to your mana pool.\n{T}: Add {U} to your mana pool. Underground River deals 1 damage to you.\n{T}: Add {B} to your mana pool. Underground River deals 1 damage to you.').
card_first_print('underground river', 'ICE').
card_image_name('underground river'/'ICE', 'underground river').
card_uid('underground river'/'ICE', 'ICE:Underground River:underground river').
card_rarity('underground river'/'ICE', 'Rare').
card_artist('underground river'/'ICE', 'NéNé Thomas').
card_multiverse_id('underground river'/'ICE', '2761').

card_in_set('updraft', 'ICE').
card_original_type('updraft'/'ICE', 'Instant').
card_original_text('updraft'/'ICE', 'Target creature gains flying until end of turn. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('updraft', 'ICE').
card_image_name('updraft'/'ICE', 'updraft').
card_uid('updraft'/'ICE', 'ICE:Updraft:updraft').
card_rarity('updraft'/'ICE', 'Uncommon').
card_artist('updraft'/'ICE', 'L. A. Williams').
card_flavor_text('updraft'/'ICE', '\"The power of flight has but one equal in battle: surprise. Understanding how to use both is the key to victory.\"\n—Arnjlot Olasson, Sky Mage').
card_multiverse_id('updraft'/'ICE', '2541').

card_in_set('urza\'s bauble', 'ICE').
card_original_type('urza\'s bauble'/'ICE', 'Artifact').
card_original_text('urza\'s bauble'/'ICE', '{T}: Sacrifice Urza\'s Bauble to choose a card at random from target player\'s hand; look at that card. Ignore this ability if that player has no cards left in hand. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('urza\'s bauble', 'ICE').
card_image_name('urza\'s bauble'/'ICE', 'urza\'s bauble').
card_uid('urza\'s bauble'/'ICE', 'ICE:Urza\'s Bauble:urza\'s bauble').
card_rarity('urza\'s bauble'/'ICE', 'Uncommon').
card_artist('urza\'s bauble'/'ICE', 'Christopher Rush').
card_multiverse_id('urza\'s bauble'/'ICE', '2429').

card_in_set('veldt', 'ICE').
card_original_type('veldt'/'ICE', 'Land').
card_original_text('veldt'/'ICE', 'If there are any depletion counters on Veldt, it does not untap during your untap phase. At the beginning of your upkeep, remove a depletion counter from Veldt.\n{T}: Add {W} to your mana pool. Put a depletion counter on Veldt.\n{T}: Add {G} to your mana pool. Put a depletion counter on Veldt.').
card_first_print('veldt', 'ICE').
card_image_name('veldt'/'ICE', 'veldt').
card_uid('veldt'/'ICE', 'ICE:Veldt:veldt').
card_rarity('veldt'/'ICE', 'Rare').
card_artist('veldt'/'ICE', 'Bryon Wackwitz').
card_multiverse_id('veldt'/'ICE', '2762').

card_in_set('venomous breath', 'ICE').
card_original_type('venomous breath'/'ICE', 'Instant').
card_original_text('venomous breath'/'ICE', 'At end of combat, destroy all creatures blocking or blocked by target creature this turn.').
card_first_print('venomous breath', 'ICE').
card_image_name('venomous breath'/'ICE', 'venomous breath').
card_uid('venomous breath'/'ICE', 'ICE:Venomous Breath:venomous breath').
card_rarity('venomous breath'/'ICE', 'Uncommon').
card_artist('venomous breath'/'ICE', 'L. A. Williams').
card_multiverse_id('venomous breath'/'ICE', '2597').

card_in_set('vertigo', 'ICE').
card_original_type('vertigo'/'ICE', 'Instant').
card_original_text('vertigo'/'ICE', 'Vertigo deals 2 damage to target creature with flying; that creature loses flying until end of turn.').
card_first_print('vertigo', 'ICE').
card_image_name('vertigo'/'ICE', 'vertigo').
card_uid('vertigo'/'ICE', 'ICE:Vertigo:vertigo').
card_rarity('vertigo'/'ICE', 'Uncommon').
card_artist('vertigo'/'ICE', 'Drew Tucker').
card_flavor_text('vertigo'/'ICE', '\"I saw the wizard gesturing, but I didn\'t guess his plan until my Aesthir cried out and went into a dive.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('vertigo'/'ICE', '2658').

card_in_set('vexing arcanix', 'ICE').
card_original_type('vexing arcanix'/'ICE', 'Artifact').
card_original_text('vexing arcanix'/'ICE', '{3},{T}: Target player names a card and then turns over the top card of his or her library. If that is the card named, put it into the player\'s hand. Otherwise, put it into the player\'s graveyard, and Vexing Arcanix deals 2 damage to that player.').
card_first_print('vexing arcanix', 'ICE').
card_image_name('vexing arcanix'/'ICE', 'vexing arcanix').
card_uid('vexing arcanix'/'ICE', 'ICE:Vexing Arcanix:vexing arcanix').
card_rarity('vexing arcanix'/'ICE', 'Rare').
card_artist('vexing arcanix'/'ICE', 'Randy Gallegos').
card_multiverse_id('vexing arcanix'/'ICE', '2430').

card_in_set('vibrating sphere', 'ICE').
card_original_type('vibrating sphere'/'ICE', 'Artifact').
card_original_text('vibrating sphere'/'ICE', 'During your turn, all creatures you control get +2/+0. During all other turns, all creatures you control get -0/-2.').
card_first_print('vibrating sphere', 'ICE').
card_image_name('vibrating sphere'/'ICE', 'vibrating sphere').
card_uid('vibrating sphere'/'ICE', 'ICE:Vibrating Sphere:vibrating sphere').
card_rarity('vibrating sphere'/'ICE', 'Rare').
card_artist('vibrating sphere'/'ICE', 'Richard Thomas').
card_flavor_text('vibrating sphere'/'ICE', '\"Unearthly and invisible fibers emanate from this sphere, entangling all who draw near.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('vibrating sphere'/'ICE', '2431').

card_in_set('walking wall', 'ICE').
card_original_type('walking wall'/'ICE', 'Artifact Creature').
card_original_text('walking wall'/'ICE', 'Counts as a wall\n{3}: Walking Wall gets +3/-1 until end of turn and can attack this turn. Walking Wall cannot attack the turn it comes under your control. Use this ability only once a turn.').
card_first_print('walking wall', 'ICE').
card_image_name('walking wall'/'ICE', 'walking wall').
card_uid('walking wall'/'ICE', 'ICE:Walking Wall:walking wall').
card_rarity('walking wall'/'ICE', 'Uncommon').
card_artist('walking wall'/'ICE', 'Anthony Waters').
card_flavor_text('walking wall'/'ICE', '\"The fortress is not what it seems.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('walking wall'/'ICE', '2432').

card_in_set('wall of lava', 'ICE').
card_original_type('wall of lava'/'ICE', 'Summon — Wall').
card_original_text('wall of lava'/'ICE', '{R} +1/+1 until end of turn').
card_first_print('wall of lava', 'ICE').
card_image_name('wall of lava'/'ICE', 'wall of lava').
card_uid('wall of lava'/'ICE', 'ICE:Wall of Lava:wall of lava').
card_rarity('wall of lava'/'ICE', 'Uncommon').
card_artist('wall of lava'/'ICE', 'Pete Venters').
card_flavor_text('wall of lava'/'ICE', '\"Now there\'s something you don\'t see every day.\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('wall of lava'/'ICE', '2659').

card_in_set('wall of pine needles', 'ICE').
card_original_type('wall of pine needles'/'ICE', 'Summon — Wall').
card_original_text('wall of pine needles'/'ICE', '{G} Regenerate').
card_first_print('wall of pine needles', 'ICE').
card_image_name('wall of pine needles'/'ICE', 'wall of pine needles').
card_uid('wall of pine needles'/'ICE', 'ICE:Wall of Pine Needles:wall of pine needles').
card_rarity('wall of pine needles'/'ICE', 'Uncommon').
card_artist('wall of pine needles'/'ICE', 'Brian Snõddy').
card_flavor_text('wall of pine needles'/'ICE', 'The power of the forest takes a hundred forms. Some are more surprising than others.').
card_multiverse_id('wall of pine needles'/'ICE', '2598').

card_in_set('wall of shields', 'ICE').
card_original_type('wall of shields'/'ICE', 'Artifact Creature').
card_original_text('wall of shields'/'ICE', 'Banding, counts as a wall').
card_first_print('wall of shields', 'ICE').
card_image_name('wall of shields'/'ICE', 'wall of shields').
card_uid('wall of shields'/'ICE', 'ICE:Wall of Shields:wall of shields').
card_rarity('wall of shields'/'ICE', 'Uncommon').
card_artist('wall of shields'/'ICE', 'Randy Gallegos').
card_flavor_text('wall of shields'/'ICE', '\"It\'s the pokey bits that hurt the most.\"\n—Ib Halfheart, Goblin Tactician').
card_multiverse_id('wall of shields'/'ICE', '2433').

card_in_set('war chariot', 'ICE').
card_original_type('war chariot'/'ICE', 'Artifact').
card_original_text('war chariot'/'ICE', '{3},{T}: Target creature gains trample until end of turn.').
card_first_print('war chariot', 'ICE').
card_image_name('war chariot'/'ICE', 'war chariot').
card_uid('war chariot'/'ICE', 'ICE:War Chariot:war chariot').
card_rarity('war chariot'/'ICE', 'Uncommon').
card_artist('war chariot'/'ICE', 'Dameon Willich').
card_flavor_text('war chariot'/'ICE', '\"I wouldn\'t advise using it with a Woolly Mammoth, but it\'s quite appropriate for many other beasts.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('war chariot'/'ICE', '2434').

card_in_set('warning', 'ICE').
card_original_type('warning'/'ICE', 'Instant').
card_original_text('warning'/'ICE', 'Target attacking creature deals no damage in combat this turn.').
card_first_print('warning', 'ICE').
card_image_name('warning'/'ICE', 'warning').
card_uid('warning'/'ICE', 'ICE:Warning:warning').
card_rarity('warning'/'ICE', 'Common').
card_artist('warning'/'ICE', 'Pat Morrissey').
card_flavor_text('warning'/'ICE', '\"The folk of the Karplusan Mountains are impossible to ambush.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').
card_multiverse_id('warning'/'ICE', '2715').

card_in_set('whalebone glider', 'ICE').
card_original_type('whalebone glider'/'ICE', 'Artifact').
card_original_text('whalebone glider'/'ICE', '{2},{T}: Target creature with power no greater than 3 gains flying until end of turn. Other effects may later be used to increase that creature\'s power beyond 3.').
card_first_print('whalebone glider', 'ICE').
card_image_name('whalebone glider'/'ICE', 'whalebone glider').
card_uid('whalebone glider'/'ICE', 'ICE:Whalebone Glider:whalebone glider').
card_rarity('whalebone glider'/'ICE', 'Uncommon').
card_artist('whalebone glider'/'ICE', 'Amy Weber').
card_flavor_text('whalebone glider'/'ICE', '\"It\'s no Ornithopter, but then I\'m no Urza.\"\n—Arcum Dagsson, Soldevi Machinist').
card_multiverse_id('whalebone glider'/'ICE', '2435').

card_in_set('white scarab', 'ICE').
card_original_type('white scarab'/'ICE', 'Enchant Creature').
card_original_text('white scarab'/'ICE', 'Target creature gets +2/+2 as long as any opponent controls any white cards. That creature cannot be blocked by white creatures.').
card_first_print('white scarab', 'ICE').
card_image_name('white scarab'/'ICE', 'white scarab').
card_uid('white scarab'/'ICE', 'ICE:White Scarab:white scarab').
card_rarity('white scarab'/'ICE', 'Uncommon').
card_artist('white scarab'/'ICE', 'Phil Foglio').
card_multiverse_id('white scarab'/'ICE', '2716').

card_in_set('whiteout', 'ICE').
card_original_type('whiteout'/'ICE', 'Instant').
card_original_text('whiteout'/'ICE', 'All creatures with flying lose flying until end of turn.\nIf Whiteout is in your graveyard, you may sacrifice a snow-covered land to return Whiteout to your hand.').
card_first_print('whiteout', 'ICE').
card_image_name('whiteout'/'ICE', 'whiteout').
card_uid('whiteout'/'ICE', 'ICE:Whiteout:whiteout').
card_rarity('whiteout'/'ICE', 'Uncommon').
card_artist('whiteout'/'ICE', 'NéNé Thomas').
card_multiverse_id('whiteout'/'ICE', '2599').

card_in_set('wiitigo', 'ICE').
card_original_type('wiitigo'/'ICE', 'Summon — Wiitigo').
card_original_text('wiitigo'/'ICE', 'When Wiitigo comes into play, put six +1/+1 counters on it.\nDuring your upkeep, put a +1/+1 counter on Wiitigo if it has blocked or been blocked since your last upkeep. Otherwise, remove a +1/+1 counter from Wiitigo. Ignore this effect if there are no counters left on Wiitigo.').
card_first_print('wiitigo', 'ICE').
card_image_name('wiitigo'/'ICE', 'wiitigo').
card_uid('wiitigo'/'ICE', 'ICE:Wiitigo:wiitigo').
card_rarity('wiitigo'/'ICE', 'Rare').
card_artist('wiitigo'/'ICE', 'Melissa A. Benson').
card_multiverse_id('wiitigo'/'ICE', '2600').

card_in_set('wild growth', 'ICE').
card_original_type('wild growth'/'ICE', 'Enchant Land').
card_original_text('wild growth'/'ICE', 'Wild Growth adds {G} to your mana pool whenever target land is tapped for mana.').
card_image_name('wild growth'/'ICE', 'wild growth').
card_uid('wild growth'/'ICE', 'ICE:Wild Growth:wild growth').
card_rarity('wild growth'/'ICE', 'Common').
card_artist('wild growth'/'ICE', 'Mike Raabe').
card_flavor_text('wild growth'/'ICE', '\"Here in Fyndhorn, where Freyalise\'s power is strongest, the forest has its own opinion.\"\n—Laina of the Elvish Council').
card_multiverse_id('wild growth'/'ICE', '2601').

card_in_set('wind spirit', 'ICE').
card_original_type('wind spirit'/'ICE', 'Summon — Spirit').
card_original_text('wind spirit'/'ICE', 'Flying\nCannot be blocked by only one creature.').
card_first_print('wind spirit', 'ICE').
card_image_name('wind spirit'/'ICE', 'wind spirit').
card_uid('wind spirit'/'ICE', 'ICE:Wind Spirit:wind spirit').
card_rarity('wind spirit'/'ICE', 'Uncommon').
card_artist('wind spirit'/'ICE', 'Kaja Foglio').
card_flavor_text('wind spirit'/'ICE', '\"To visit the sky requires bravery, and thought, and little else. To master the sky requires the binding of its masters, and little else.\"\n—Arnjlot Olasson, Sky Mage').
card_multiverse_id('wind spirit'/'ICE', '2542').

card_in_set('wings of aesthir', 'ICE').
card_original_type('wings of aesthir'/'ICE', 'Enchant Creature').
card_original_text('wings of aesthir'/'ICE', 'Target creature gains flying and first strike and gets +1/+0.').
card_first_print('wings of aesthir', 'ICE').
card_image_name('wings of aesthir'/'ICE', 'wings of aesthir').
card_uid('wings of aesthir'/'ICE', 'ICE:Wings of Aesthir:wings of aesthir').
card_rarity('wings of aesthir'/'ICE', 'Uncommon').
card_artist('wings of aesthir'/'ICE', 'Edward P. Beard, Jr.').
card_flavor_text('wings of aesthir'/'ICE', '\"For those of courage, even the sky holds no limit.\"\n—Arnjlot Olasson, Sky Mage').
card_multiverse_id('wings of aesthir'/'ICE', '2741').

card_in_set('winter\'s chill', 'ICE').
card_original_type('winter\'s chill'/'ICE', 'Instant').
card_original_text('winter\'s chill'/'ICE', 'Cast only during combat before defense is chosen. At end of combat, destroy X target attacking creatures; X cannot be greater than the number of snow-covered lands you control. For each attacking creature, its controller may pay {1} or {2} to prevent it from being destroyed in this way. If that player pays {1}, the creature neither deals nor receives damage in combat. If that player pays {2}, the creature deals and receives damage in combat as normal.').
card_first_print('winter\'s chill', 'ICE').
card_image_name('winter\'s chill'/'ICE', 'winter\'s chill').
card_uid('winter\'s chill'/'ICE', 'ICE:Winter\'s Chill:winter\'s chill').
card_rarity('winter\'s chill'/'ICE', 'Rare').
card_artist('winter\'s chill'/'ICE', 'Edward P. Beard, Jr.').
card_multiverse_id('winter\'s chill'/'ICE', '2543').

card_in_set('withering wisps', 'ICE').
card_original_type('withering wisps'/'ICE', 'Enchantment').
card_original_text('withering wisps'/'ICE', 'At the end of any turn, if there are no creatures in play, bury Withering Wisps.\n{B} Withering Wisps deals 1 damage to each creature and each player. You cannot spend more {B} in this way each turn than the number of snow-covered swamps you control.').
card_first_print('withering wisps', 'ICE').
card_image_name('withering wisps'/'ICE', 'withering wisps').
card_uid('withering wisps'/'ICE', 'ICE:Withering Wisps:withering wisps').
card_rarity('withering wisps'/'ICE', 'Uncommon').
card_artist('withering wisps'/'ICE', 'NéNé Thomas').
card_multiverse_id('withering wisps'/'ICE', '2492').

card_in_set('woolly mammoths', 'ICE').
card_original_type('woolly mammoths'/'ICE', 'Summon — Mammoths').
card_original_text('woolly mammoths'/'ICE', 'Gains trample as long as you control any snow-covered lands.').
card_first_print('woolly mammoths', 'ICE').
card_image_name('woolly mammoths'/'ICE', 'woolly mammoths').
card_uid('woolly mammoths'/'ICE', 'ICE:Woolly Mammoths:woolly mammoths').
card_rarity('woolly mammoths'/'ICE', 'Common').
card_artist('woolly mammoths'/'ICE', 'Dan Frazier').
card_flavor_text('woolly mammoths'/'ICE', '\"Mammoths may be good to ride on, but they\'re certainly bad to fall off of!\"\n—Disa the Restless, journal entry').
card_multiverse_id('woolly mammoths'/'ICE', '2602').

card_in_set('woolly spider', 'ICE').
card_original_type('woolly spider'/'ICE', 'Summon — Spider').
card_original_text('woolly spider'/'ICE', 'Can block creatures with flying.\nIf Woolly Spider is assigned to block a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_first_print('woolly spider', 'ICE').
card_image_name('woolly spider'/'ICE', 'woolly spider').
card_uid('woolly spider'/'ICE', 'ICE:Woolly Spider:woolly spider').
card_rarity('woolly spider'/'ICE', 'Common').
card_artist('woolly spider'/'ICE', 'Daniel Gelon').
card_flavor_text('woolly spider'/'ICE', '\"We need not fear the forces of the air; I\'ve yet to see a Spider without an appetite.\"\n—Taaveti of Kelsinko, Elvish Hunter').
card_multiverse_id('woolly spider'/'ICE', '2603').

card_in_set('word of blasting', 'ICE').
card_original_type('word of blasting'/'ICE', 'Instant').
card_original_text('word of blasting'/'ICE', 'Bury target wall. Word of Blasting deals an amount of damage equal to that wall\'s casting cost to the wall\'s controller.').
card_first_print('word of blasting', 'ICE').
card_image_name('word of blasting'/'ICE', 'word of blasting').
card_uid('word of blasting'/'ICE', 'ICE:Word of Blasting:word of blasting').
card_rarity('word of blasting'/'ICE', 'Uncommon').
card_artist('word of blasting'/'ICE', 'Ken Meyer, Jr.').
card_flavor_text('word of blasting'/'ICE', '\"Walls? What walls?\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('word of blasting'/'ICE', '2660').

card_in_set('word of undoing', 'ICE').
card_original_type('word of undoing'/'ICE', 'Instant').
card_original_text('word of undoing'/'ICE', 'Return target creature to owner\'s hand. Return any white enchantments you own on that creature to your hand.').
card_first_print('word of undoing', 'ICE').
card_image_name('word of undoing'/'ICE', 'word of undoing').
card_uid('word of undoing'/'ICE', 'ICE:Word of Undoing:word of undoing').
card_rarity('word of undoing'/'ICE', 'Common').
card_artist('word of undoing'/'ICE', 'Christopher Rush').
card_flavor_text('word of undoing'/'ICE', '\"It was in Urza\'s journals that I finally found the secret at the core of the summonings.\"\n—Journal, author unknown').
card_multiverse_id('word of undoing'/'ICE', '2544').

card_in_set('wrath of marit lage', 'ICE').
card_original_type('wrath of marit lage'/'ICE', 'Enchantment').
card_original_text('wrath of marit lage'/'ICE', 'When Wrath of Marit Lage comes into play, tap all red creatures. Red creatures do not untap during their controller\'s untap phase.').
card_first_print('wrath of marit lage', 'ICE').
card_image_name('wrath of marit lage'/'ICE', 'wrath of marit lage').
card_uid('wrath of marit lage'/'ICE', 'ICE:Wrath of Marit Lage:wrath of marit lage').
card_rarity('wrath of marit lage'/'ICE', 'Rare').
card_artist('wrath of marit lage'/'ICE', 'Mike Raabe').
card_flavor_text('wrath of marit lage'/'ICE', 'Dread Marit Lage lies dreaming, not dead.').
card_multiverse_id('wrath of marit lage'/'ICE', '2545').

card_in_set('yavimaya gnats', 'ICE').
card_original_type('yavimaya gnats'/'ICE', 'Summon — Insects').
card_original_text('yavimaya gnats'/'ICE', 'Flying\n{G} Regenerate').
card_first_print('yavimaya gnats', 'ICE').
card_image_name('yavimaya gnats'/'ICE', 'yavimaya gnats').
card_uid('yavimaya gnats'/'ICE', 'ICE:Yavimaya Gnats:yavimaya gnats').
card_rarity('yavimaya gnats'/'ICE', 'Uncommon').
card_artist('yavimaya gnats'/'ICE', 'Dan Frazier').
card_flavor_text('yavimaya gnats'/'ICE', '\"It is our third day of travel on the Yavimaya River, and still these creatures plague us. Davin Lansson, our naturalist, has facetiously labeled them ‘gnats,\' and the name has stuck.\"\n—Disa the Restless, journal entry').
card_multiverse_id('yavimaya gnats'/'ICE', '2604').

card_in_set('zur\'s weirding', 'ICE').
card_original_type('zur\'s weirding'/'ICE', 'Enchantment').
card_original_text('zur\'s weirding'/'ICE', 'All players play with the cards in their hands face up on the table. Whenever any player draws a card, any other player may pay 2 life to force the drawing player to discard that card. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_first_print('zur\'s weirding', 'ICE').
card_image_name('zur\'s weirding'/'ICE', 'zur\'s weirding').
card_uid('zur\'s weirding'/'ICE', 'ICE:Zur\'s Weirding:zur\'s weirding').
card_rarity('zur\'s weirding'/'ICE', 'Rare').
card_artist('zur\'s weirding'/'ICE', 'Liz Danforth').
card_multiverse_id('zur\'s weirding'/'ICE', '2546').

card_in_set('zuran enchanter', 'ICE').
card_original_type('zuran enchanter'/'ICE', 'Summon — Wizard').
card_original_text('zuran enchanter'/'ICE', '{2}{B},{T}: Target player chooses and discards one card from his or her hand. Ignore this ability if that player has no cards in his or her hand. Use this ability only during your turn.').
card_first_print('zuran enchanter', 'ICE').
card_image_name('zuran enchanter'/'ICE', 'zuran enchanter').
card_uid('zuran enchanter'/'ICE', 'ICE:Zuran Enchanter:zuran enchanter').
card_rarity('zuran enchanter'/'ICE', 'Common').
card_artist('zuran enchanter'/'ICE', 'Douglas Shuler').
card_flavor_text('zuran enchanter'/'ICE', '\"We are Kjeldorans no more.\"\n—Zur the Enchanter').
card_multiverse_id('zuran enchanter'/'ICE', '2547').

card_in_set('zuran orb', 'ICE').
card_original_type('zuran orb'/'ICE', 'Artifact').
card_original_text('zuran orb'/'ICE', '{0}: Sacrifice a land to gain 2 life.').
card_first_print('zuran orb', 'ICE').
card_image_name('zuran orb'/'ICE', 'zuran orb').
card_uid('zuran orb'/'ICE', 'ICE:Zuran Orb:zuran orb').
card_rarity('zuran orb'/'ICE', 'Uncommon').
card_artist('zuran orb'/'ICE', 'Sandra Everingham').
card_flavor_text('zuran orb'/'ICE', '\"I will go to any length to achieve my goal. Eternal life is worth any sacrifice.\"\n—Zur the Enchanter').
card_multiverse_id('zuran orb'/'ICE', '2436').

card_in_set('zuran spellcaster', 'ICE').
card_original_type('zuran spellcaster'/'ICE', 'Summon — Wizard').
card_original_text('zuran spellcaster'/'ICE', '{T}: Zuran Spellcaster deals 1 damage to target creature or player.').
card_first_print('zuran spellcaster', 'ICE').
card_image_name('zuran spellcaster'/'ICE', 'zuran spellcaster').
card_uid('zuran spellcaster'/'ICE', 'ICE:Zuran Spellcaster:zuran spellcaster').
card_rarity('zuran spellcaster'/'ICE', 'Common').
card_artist('zuran spellcaster'/'ICE', 'Edward P. Beard, Jr.').
card_flavor_text('zuran spellcaster'/'ICE', '\"A mage must be precise as well as potent; cautious, as well as clever.\"\n—Zur the Enchanter').
card_multiverse_id('zuran spellcaster'/'ICE', '2548').
