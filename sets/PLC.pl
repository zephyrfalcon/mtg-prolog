% Planar Chaos

set('PLC').
set_name('PLC', 'Planar Chaos').
set_release_date('PLC', '2007-02-02').
set_border('PLC', 'black').
set_type('PLC', 'expansion').
set_block('PLC', 'Time Spiral').

card_in_set('aeon chronicler', 'PLC').
card_original_type('aeon chronicler'/'PLC', 'Creature — Avatar').
card_original_text('aeon chronicler'/'PLC', 'Aeon Chronicler\'s power and toughness are each equal to the number of cards in your hand.\nSuspend X—{X}{3}{U}. X can\'t be 0.\nWhenever a time counter is removed from Aeon Chronicler while it\'s removed from the game, draw a card.').
card_first_print('aeon chronicler', 'PLC').
card_image_name('aeon chronicler'/'PLC', 'aeon chronicler').
card_uid('aeon chronicler'/'PLC', 'PLC:Aeon Chronicler:aeon chronicler').
card_rarity('aeon chronicler'/'PLC', 'Rare').
card_artist('aeon chronicler'/'PLC', 'Dan Dos Santos').
card_number('aeon chronicler'/'PLC', '32').
card_multiverse_id('aeon chronicler'/'PLC', '122449').

card_in_set('æther membrane', 'PLC').
card_original_type('æther membrane'/'PLC', 'Creature — Wall').
card_original_text('æther membrane'/'PLC', 'Defender\nÆther Membrane can block as though it had flying.\nWhenever Æther Membrane blocks a creature, return that creature to its owner\'s hand at end of combat.').
card_first_print('æther membrane', 'PLC').
card_image_name('æther membrane'/'PLC', 'aether membrane').
card_uid('æther membrane'/'PLC', 'PLC:Æther Membrane:aether membrane').
card_rarity('æther membrane'/'PLC', 'Uncommon').
card_artist('æther membrane'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('æther membrane'/'PLC', '93').
card_multiverse_id('æther membrane'/'PLC', '124504').

card_in_set('akroma, angel of fury', 'PLC').
card_original_type('akroma, angel of fury'/'PLC', 'Legendary Creature — Angel').
card_original_text('akroma, angel of fury'/'PLC', 'Akroma, Angel of Fury can\'t be countered.\nFlying, trample, protection from white, protection from blue\n{R}: Akroma, Angel of Fury gets +1/+0 until end of turn.\nMorph {3}{R}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('akroma, angel of fury', 'PLC').
card_image_name('akroma, angel of fury'/'PLC', 'akroma, angel of fury').
card_uid('akroma, angel of fury'/'PLC', 'PLC:Akroma, Angel of Fury:akroma, angel of fury').
card_rarity('akroma, angel of fury'/'PLC', 'Rare').
card_artist('akroma, angel of fury'/'PLC', 'Daren Bader').
card_number('akroma, angel of fury'/'PLC', '94').
card_multiverse_id('akroma, angel of fury'/'PLC', '122432').

card_in_set('ana battlemage', 'PLC').
card_original_type('ana battlemage'/'PLC', 'Creature — Human Wizard').
card_original_text('ana battlemage'/'PLC', 'Kicker {2}{U} and/or {1}{B}\nWhen Ana Battlemage comes into play, if the {2}{U} kicker cost was paid, target player discards three cards.\nWhen Ana Battlemage comes into play, if the {1}{B} kicker cost was paid, tap target untapped creature and that creature deals damage equal to its power to its controller.').
card_first_print('ana battlemage', 'PLC').
card_image_name('ana battlemage'/'PLC', 'ana battlemage').
card_uid('ana battlemage'/'PLC', 'PLC:Ana Battlemage:ana battlemage').
card_rarity('ana battlemage'/'PLC', 'Uncommon').
card_artist('ana battlemage'/'PLC', 'Jim Nelson').
card_number('ana battlemage'/'PLC', '124').
card_multiverse_id('ana battlemage'/'PLC', '124342').

card_in_set('aquamorph entity', 'PLC').
card_original_type('aquamorph entity'/'PLC', 'Creature — Shapeshifter').
card_original_text('aquamorph entity'/'PLC', 'As Aquamorph Entity comes into play or is turned face up, it becomes your choice of 5/1 or 1/5.\nMorph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('aquamorph entity', 'PLC').
card_image_name('aquamorph entity'/'PLC', 'aquamorph entity').
card_uid('aquamorph entity'/'PLC', 'PLC:Aquamorph Entity:aquamorph entity').
card_rarity('aquamorph entity'/'PLC', 'Common').
card_artist('aquamorph entity'/'PLC', 'Brian Despain').
card_number('aquamorph entity'/'PLC', '33').
card_multiverse_id('aquamorph entity'/'PLC', '126024').

card_in_set('auramancer\'s guise', 'PLC').
card_original_type('auramancer\'s guise'/'PLC', 'Enchantment — Aura').
card_original_text('auramancer\'s guise'/'PLC', 'Enchant creature\nEnchanted creature gets +2/+2 for each Aura attached to it and has vigilance.').
card_first_print('auramancer\'s guise', 'PLC').
card_image_name('auramancer\'s guise'/'PLC', 'auramancer\'s guise').
card_uid('auramancer\'s guise'/'PLC', 'PLC:Auramancer\'s Guise:auramancer\'s guise').
card_rarity('auramancer\'s guise'/'PLC', 'Uncommon').
card_artist('auramancer\'s guise'/'PLC', 'Greg Staples').
card_number('auramancer\'s guise'/'PLC', '34').
card_flavor_text('auramancer\'s guise'/'PLC', '\"Consider not what the mask hides, but what it can reveal.\"').
card_multiverse_id('auramancer\'s guise'/'PLC', '130813').

card_in_set('aven riftwatcher', 'PLC').
card_original_type('aven riftwatcher'/'PLC', 'Creature — Bird Rebel Soldier').
card_original_text('aven riftwatcher'/'PLC', 'Flying\nVanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Aven Riftwatcher comes into play or leaves play, you gain 2 life.').
card_first_print('aven riftwatcher', 'PLC').
card_image_name('aven riftwatcher'/'PLC', 'aven riftwatcher').
card_uid('aven riftwatcher'/'PLC', 'PLC:Aven Riftwatcher:aven riftwatcher').
card_rarity('aven riftwatcher'/'PLC', 'Common').
card_artist('aven riftwatcher'/'PLC', 'Don Hazeltine').
card_number('aven riftwatcher'/'PLC', '1').
card_multiverse_id('aven riftwatcher'/'PLC', '108899').

card_in_set('battering sliver', 'PLC').
card_original_type('battering sliver'/'PLC', 'Creature — Sliver').
card_original_text('battering sliver'/'PLC', 'All Slivers have trample.').
card_first_print('battering sliver', 'PLC').
card_image_name('battering sliver'/'PLC', 'battering sliver').
card_uid('battering sliver'/'PLC', 'PLC:Battering Sliver:battering sliver').
card_rarity('battering sliver'/'PLC', 'Common').
card_artist('battering sliver'/'PLC', 'Greg Staples').
card_number('battering sliver'/'PLC', '95').
card_flavor_text('battering sliver'/'PLC', 'Covered with hard shell-like plates, these slivers burrow through solid rock to carve out new nests for their hives.').
card_multiverse_id('battering sliver'/'PLC', '126015').

card_in_set('benalish commander', 'PLC').
card_original_type('benalish commander'/'PLC', 'Creature — Human Soldier Lord').
card_original_text('benalish commander'/'PLC', 'Benalish Commander\'s power and toughness are each equal to the number of Soldiers you control.\nSuspend X—{X}{W}{W}. X can\'t be 0.\nWhenever a time counter is removed from Benalish Commander while it\'s removed from the game, put a 1/1 white Soldier creature token into play.').
card_first_print('benalish commander', 'PLC').
card_image_name('benalish commander'/'PLC', 'benalish commander').
card_uid('benalish commander'/'PLC', 'PLC:Benalish Commander:benalish commander').
card_rarity('benalish commander'/'PLC', 'Rare').
card_artist('benalish commander'/'PLC', 'Paolo Parente').
card_number('benalish commander'/'PLC', '2').
card_multiverse_id('benalish commander'/'PLC', '122408').

card_in_set('big game hunter', 'PLC').
card_original_type('big game hunter'/'PLC', 'Creature — Human Rebel Assassin').
card_original_text('big game hunter'/'PLC', 'When Big Game Hunter comes into play, destroy target creature with power 4 or greater. It can\'t be regenerated.\nMadness {B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('big game hunter', 'PLC').
card_image_name('big game hunter'/'PLC', 'big game hunter').
card_uid('big game hunter'/'PLC', 'PLC:Big Game Hunter:big game hunter').
card_rarity('big game hunter'/'PLC', 'Uncommon').
card_artist('big game hunter'/'PLC', 'Carl Critchlow').
card_number('big game hunter'/'PLC', '63').
card_multiverse_id('big game hunter'/'PLC', '134739').

card_in_set('blightspeaker', 'PLC').
card_original_type('blightspeaker'/'PLC', 'Creature — Human Rebel Cleric').
card_original_text('blightspeaker'/'PLC', '{T}: Target player loses 1 life.\n{4}, {T}: Search your library for a Rebel card with converted mana cost 3 or less and put it into play. Then shuffle your library.').
card_first_print('blightspeaker', 'PLC').
card_image_name('blightspeaker'/'PLC', 'blightspeaker').
card_uid('blightspeaker'/'PLC', 'PLC:Blightspeaker:blightspeaker').
card_rarity('blightspeaker'/'PLC', 'Common').
card_artist('blightspeaker'/'PLC', 'Ron Spears').
card_number('blightspeaker'/'PLC', '64').
card_flavor_text('blightspeaker'/'PLC', 'One croaked sermon spreads propaganda and plague.').
card_multiverse_id('blightspeaker'/'PLC', '124506').

card_in_set('blood knight', 'PLC').
card_original_type('blood knight'/'PLC', 'Creature — Human Knight').
card_original_text('blood knight'/'PLC', 'First strike, protection from white').
card_image_name('blood knight'/'PLC', 'blood knight').
card_uid('blood knight'/'PLC', 'PLC:Blood Knight:blood knight').
card_rarity('blood knight'/'PLC', 'Uncommon').
card_artist('blood knight'/'PLC', 'Matt Cavotta').
card_number('blood knight'/'PLC', '115').
card_flavor_text('blood knight'/'PLC', 'His is the fury of the wildfire, the boiling blood of the volcano. He fights you not because you\'ve wronged him, but because you\'re there.').
card_multiverse_id('blood knight'/'PLC', '130715').
card_timeshifted('blood knight'/'PLC')
.
card_in_set('body double', 'PLC').
card_original_type('body double'/'PLC', 'Creature — Shapeshifter').
card_original_text('body double'/'PLC', 'As Body Double comes into play, you may choose a creature card in a graveyard. If you do, Body Double comes into play as a copy of that card.').
card_first_print('body double', 'PLC').
card_image_name('body double'/'PLC', 'body double').
card_uid('body double'/'PLC', 'PLC:Body Double:body double').
card_rarity('body double'/'PLC', 'Rare').
card_artist('body double'/'PLC', 'Steve Prescott').
card_number('body double'/'PLC', '35').
card_flavor_text('body double'/'PLC', 'Where the dead outnumbered the living, mimics scavenged faces from the fallen.').
card_multiverse_id('body double'/'PLC', '124443').

card_in_set('bog serpent', 'PLC').
card_original_type('bog serpent'/'PLC', 'Creature — Serpent').
card_original_text('bog serpent'/'PLC', 'Bog Serpent can\'t attack unless defending player controls a Swamp.\nWhen you control no Swamps, sacrifice Bog Serpent.').
card_first_print('bog serpent', 'PLC').
card_image_name('bog serpent'/'PLC', 'bog serpent').
card_uid('bog serpent'/'PLC', 'PLC:Bog Serpent:bog serpent').
card_rarity('bog serpent'/'PLC', 'Common').
card_artist('bog serpent'/'PLC', 'Thomas M. Baxa').
card_number('bog serpent'/'PLC', '84').
card_flavor_text('bog serpent'/'PLC', '\"We thought it was a fallen tree trunk. Then it ate Snod.\"\n—Sneed, goblin barkwright').
card_multiverse_id('bog serpent'/'PLC', '122321').
card_timeshifted('bog serpent'/'PLC')
.
card_in_set('boom', 'PLC').
card_original_type('boom'/'PLC', 'Sorcery').
card_original_text('boom'/'PLC', 'Destroy target land you control and target land you don\'t control.\n//\nBust\n{5}{R}\nSorcery\nDestroy all lands.').
card_first_print('boom', 'PLC').
card_image_name('boom'/'PLC', 'boombust').
card_uid('boom'/'PLC', 'PLC:Boom:boombust').
card_rarity('boom'/'PLC', 'Rare').
card_artist('boom'/'PLC', 'John Avon').
card_number('boom'/'PLC', '112a').
card_multiverse_id('boom'/'PLC', '126218').

card_in_set('braids, conjurer adept', 'PLC').
card_original_type('braids, conjurer adept'/'PLC', 'Legendary Creature — Human Wizard').
card_original_text('braids, conjurer adept'/'PLC', 'At the beginning of each player\'s upkeep, that player may put an artifact, creature, or land card from his or her hand into play.').
card_first_print('braids, conjurer adept', 'PLC').
card_image_name('braids, conjurer adept'/'PLC', 'braids, conjurer adept').
card_uid('braids, conjurer adept'/'PLC', 'PLC:Braids, Conjurer Adept:braids, conjurer adept').
card_rarity('braids, conjurer adept'/'PLC', 'Rare').
card_artist('braids, conjurer adept'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('braids, conjurer adept'/'PLC', '36').
card_flavor_text('braids, conjurer adept'/'PLC', 'The rifted multiverse became a sea of conflicting realities, each peopled by possible versions of every living being.').
card_multiverse_id('braids, conjurer adept'/'PLC', '124316').

card_in_set('brain gorgers', 'PLC').
card_original_type('brain gorgers'/'PLC', 'Creature — Zombie').
card_original_text('brain gorgers'/'PLC', 'When you play Brain Gorgers, any player may sacrifice a creature. If a player does, counter Brain Gorgers.\nMadness {1}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('brain gorgers', 'PLC').
card_image_name('brain gorgers'/'PLC', 'brain gorgers').
card_uid('brain gorgers'/'PLC', 'PLC:Brain Gorgers:brain gorgers').
card_rarity('brain gorgers'/'PLC', 'Common').
card_artist('brain gorgers'/'PLC', 'Kev Walker').
card_number('brain gorgers'/'PLC', '65').
card_flavor_text('brain gorgers'/'PLC', 'In spite of their hunger, they\'re slow eaters.').
card_multiverse_id('brain gorgers'/'PLC', '124040').

card_in_set('brute force', 'PLC').
card_original_type('brute force'/'PLC', 'Instant').
card_original_text('brute force'/'PLC', 'Target creature gets +3/+3 until end of turn.').
card_first_print('brute force', 'PLC').
card_image_name('brute force'/'PLC', 'brute force').
card_uid('brute force'/'PLC', 'PLC:Brute Force:brute force').
card_rarity('brute force'/'PLC', 'Common').
card_artist('brute force'/'PLC', 'Wayne Reynolds').
card_number('brute force'/'PLC', '116').
card_flavor_text('brute force'/'PLC', 'Blood, bone, and sinew are magnified, as is the rage that drives them. The brain, however, remains unchanged—a little bean, swinging by a strand in a cavernous, raving head.').
card_multiverse_id('brute force'/'PLC', '122373').
card_timeshifted('brute force'/'PLC')
.
card_in_set('bust', 'PLC').
card_original_type('bust'/'PLC', 'Sorcery').
card_original_text('bust'/'PLC', 'Destroy target land you control and target land you don\'t control.\n//\nBust\n{5}{R}\nSorcery\nDestroy all lands.').
card_first_print('bust', 'PLC').
card_image_name('bust'/'PLC', 'boombust').
card_uid('bust'/'PLC', 'PLC:Bust:boombust').
card_rarity('bust'/'PLC', 'Rare').
card_artist('bust'/'PLC', 'John Avon').
card_number('bust'/'PLC', '112b').
card_multiverse_id('bust'/'PLC', '126218').

card_in_set('calciderm', 'PLC').
card_original_type('calciderm'/'PLC', 'Creature — Beast').
card_original_text('calciderm'/'PLC', 'Vanishing 4 (This permanent comes into play with four time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nCalciderm can\'t be the target of spells or abilities.').
card_image_name('calciderm'/'PLC', 'calciderm').
card_uid('calciderm'/'PLC', 'PLC:Calciderm:calciderm').
card_rarity('calciderm'/'PLC', 'Uncommon').
card_artist('calciderm'/'PLC', 'Dave Kendall').
card_number('calciderm'/'PLC', '23').
card_multiverse_id('calciderm'/'PLC', '122360').
card_timeshifted('calciderm'/'PLC')
.
card_in_set('cautery sliver', 'PLC').
card_original_type('cautery sliver'/'PLC', 'Creature — Sliver').
card_original_text('cautery sliver'/'PLC', 'All Slivers have \"{1}, Sacrifice this creature: This creature deals 1 damage to target creature or player.\"\nAll Slivers have \"{1}, Sacrifice this creature: Prevent the next 1 damage that would be dealt to target Sliver or player this turn.\"').
card_first_print('cautery sliver', 'PLC').
card_image_name('cautery sliver'/'PLC', 'cautery sliver').
card_uid('cautery sliver'/'PLC', 'PLC:Cautery Sliver:cautery sliver').
card_rarity('cautery sliver'/'PLC', 'Uncommon').
card_artist('cautery sliver'/'PLC', 'Dany Orizio').
card_number('cautery sliver'/'PLC', '154').
card_multiverse_id('cautery sliver'/'PLC', '126021').

card_in_set('chronozoa', 'PLC').
card_original_type('chronozoa'/'PLC', 'Creature — Illusion').
card_original_text('chronozoa'/'PLC', 'Flying\nVanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Chronozoa is put into a graveyard from play, if it had no time counters on it, put two tokens into play that are copies of it.').
card_first_print('chronozoa', 'PLC').
card_image_name('chronozoa'/'PLC', 'chronozoa').
card_uid('chronozoa'/'PLC', 'PLC:Chronozoa:chronozoa').
card_rarity('chronozoa'/'PLC', 'Rare').
card_artist('chronozoa'/'PLC', 'James Wong').
card_number('chronozoa'/'PLC', '37').
card_multiverse_id('chronozoa'/'PLC', '111066').

card_in_set('circle of affliction', 'PLC').
card_original_type('circle of affliction'/'PLC', 'Enchantment').
card_original_text('circle of affliction'/'PLC', 'As Circle of Affliction comes into play, choose a color.\nWhenever a source of the chosen color deals damage to you, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.').
card_first_print('circle of affliction', 'PLC').
card_image_name('circle of affliction'/'PLC', 'circle of affliction').
card_uid('circle of affliction'/'PLC', 'PLC:Circle of Affliction:circle of affliction').
card_rarity('circle of affliction'/'PLC', 'Uncommon').
card_artist('circle of affliction'/'PLC', 'Rob Alexander').
card_number('circle of affliction'/'PLC', '66').
card_multiverse_id('circle of affliction'/'PLC', '126803').

card_in_set('citanul woodreaders', 'PLC').
card_original_type('citanul woodreaders'/'PLC', 'Creature — Human Druid').
card_original_text('citanul woodreaders'/'PLC', 'Kicker {2}{G} (You may pay an additional {2}{G} as you play this spell.)\nWhen Citanul Woodreaders comes into play, if the kicker cost was paid, draw two cards.').
card_first_print('citanul woodreaders', 'PLC').
card_image_name('citanul woodreaders'/'PLC', 'citanul woodreaders').
card_uid('citanul woodreaders'/'PLC', 'PLC:Citanul Woodreaders:citanul woodreaders').
card_rarity('citanul woodreaders'/'PLC', 'Common').
card_artist('citanul woodreaders'/'PLC', 'Steven Belledin').
card_number('citanul woodreaders'/'PLC', '125').
card_flavor_text('citanul woodreaders'/'PLC', 'They seek out living trees to glean age-old secrets from sap and wood.').
card_multiverse_id('citanul woodreaders'/'PLC', '122290').

card_in_set('cradle to grave', 'PLC').
card_original_type('cradle to grave'/'PLC', 'Instant').
card_original_text('cradle to grave'/'PLC', 'Destroy target nonblack creature that came into play this turn.').
card_first_print('cradle to grave', 'PLC').
card_image_name('cradle to grave'/'PLC', 'cradle to grave').
card_uid('cradle to grave'/'PLC', 'PLC:Cradle to Grave:cradle to grave').
card_rarity('cradle to grave'/'PLC', 'Common').
card_artist('cradle to grave'/'PLC', 'Dave Kendall').
card_number('cradle to grave'/'PLC', '67').
card_flavor_text('cradle to grave'/'PLC', '\"As the fell soil\'s appetite grows, it gulps down passersby upon their first footfall.\"\n—Ezrith, druid of the Dark Hours').
card_multiverse_id('cradle to grave'/'PLC', '110536').

card_in_set('crovax, ascendant hero', 'PLC').
card_original_type('crovax, ascendant hero'/'PLC', 'Legendary Creature — Human Lord').
card_original_text('crovax, ascendant hero'/'PLC', 'Other white creatures get +1/+1.\nNonwhite creatures get -1/-1.\nPay 2 life: Return Crovax, Ascendant Hero to its owner\'s hand.').
card_first_print('crovax, ascendant hero', 'PLC').
card_image_name('crovax, ascendant hero'/'PLC', 'crovax, ascendant hero').
card_uid('crovax, ascendant hero'/'PLC', 'PLC:Crovax, Ascendant Hero:crovax, ascendant hero').
card_rarity('crovax, ascendant hero'/'PLC', 'Rare').
card_artist('crovax, ascendant hero'/'PLC', 'Pete Venters').
card_number('crovax, ascendant hero'/'PLC', '3').
card_flavor_text('crovax, ascendant hero'/'PLC', 'Crovax was destined for an angel\'s curse, but one warped timeline saw the noble redeemed.').
card_multiverse_id('crovax, ascendant hero'/'PLC', '122487').

card_in_set('damnation', 'PLC').
card_original_type('damnation'/'PLC', 'Sorcery').
card_original_text('damnation'/'PLC', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('damnation'/'PLC', 'damnation').
card_uid('damnation'/'PLC', 'PLC:Damnation:damnation').
card_rarity('damnation'/'PLC', 'Rare').
card_artist('damnation'/'PLC', 'Kev Walker').
card_number('damnation'/'PLC', '85').
card_multiverse_id('damnation'/'PLC', '122423').
card_timeshifted('damnation'/'PLC')
.
card_in_set('darkheart sliver', 'PLC').
card_original_type('darkheart sliver'/'PLC', 'Creature — Sliver').
card_original_text('darkheart sliver'/'PLC', 'All Slivers have \"Sacrifice this creature: You gain 3 life.\"').
card_first_print('darkheart sliver', 'PLC').
card_image_name('darkheart sliver'/'PLC', 'darkheart sliver').
card_uid('darkheart sliver'/'PLC', 'PLC:Darkheart Sliver:darkheart sliver').
card_rarity('darkheart sliver'/'PLC', 'Uncommon').
card_artist('darkheart sliver'/'PLC', 'rk post').
card_number('darkheart sliver'/'PLC', '155').
card_flavor_text('darkheart sliver'/'PLC', '\"At first we thought we were in some haunted wood. Then the branches twisted and scuttled toward us.\"\n—Merrik Aidar, Benalish patrol').
card_multiverse_id('darkheart sliver'/'PLC', '126012').

card_in_set('dash hopes', 'PLC').
card_original_type('dash hopes'/'PLC', 'Instant').
card_original_text('dash hopes'/'PLC', 'When you play Dash Hopes, any player may pay 5 life. If a player does, counter Dash Hopes. \nCounter target spell.').
card_first_print('dash hopes', 'PLC').
card_image_name('dash hopes'/'PLC', 'dash hopes').
card_uid('dash hopes'/'PLC', 'PLC:Dash Hopes:dash hopes').
card_rarity('dash hopes'/'PLC', 'Common').
card_artist('dash hopes'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('dash hopes'/'PLC', '68').
card_flavor_text('dash hopes'/'PLC', '\"Focus an enemy\'s mind on what\'s already gone, and it\'s easier to take away more.\"\n—Holux, stronghold racketeer').
card_multiverse_id('dash hopes'/'PLC', '126812').

card_in_set('dawn charm', 'PLC').
card_original_type('dawn charm'/'PLC', 'Instant').
card_original_text('dawn charm'/'PLC', 'Choose one Prevent all combat damage that would be dealt this turn; or regenerate target creature; or counter target spell that targets you.').
card_first_print('dawn charm', 'PLC').
card_image_name('dawn charm'/'PLC', 'dawn charm').
card_uid('dawn charm'/'PLC', 'PLC:Dawn Charm:dawn charm').
card_rarity('dawn charm'/'PLC', 'Common').
card_artist('dawn charm'/'PLC', 'John Avon').
card_number('dawn charm'/'PLC', '4').
card_multiverse_id('dawn charm'/'PLC', '124080').

card_in_set('dead', 'PLC').
card_original_type('dead'/'PLC', 'Instant').
card_original_text('dead'/'PLC', 'Dead deals 2 damage to target creature.\n// \nGone\n{2}{R}\nInstant\nReturn target creature you don\'t control to its owner\'s hand.').
card_first_print('dead', 'PLC').
card_image_name('dead'/'PLC', 'deadgone').
card_uid('dead'/'PLC', 'PLC:Dead:deadgone').
card_rarity('dead'/'PLC', 'Common').
card_artist('dead'/'PLC', 'Tomas Giorello').
card_number('dead'/'PLC', '113a').
card_multiverse_id('dead'/'PLC', '126419').

card_in_set('deadly grub', 'PLC').
card_original_type('deadly grub'/'PLC', 'Creature — Insect').
card_original_text('deadly grub'/'PLC', 'Vanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Deadly Grub is put into a graveyard from play, if it had no time counters on it, put a 6/1 green Insect creature token into play with \"This creature can\'t be the target of spells or abilities.\"').
card_first_print('deadly grub', 'PLC').
card_image_name('deadly grub'/'PLC', 'deadly grub').
card_uid('deadly grub'/'PLC', 'PLC:Deadly Grub:deadly grub').
card_rarity('deadly grub'/'PLC', 'Common').
card_artist('deadly grub'/'PLC', 'E. M. Gist').
card_number('deadly grub'/'PLC', '69').
card_multiverse_id('deadly grub'/'PLC', '111046').

card_in_set('deadwood treefolk', 'PLC').
card_original_type('deadwood treefolk'/'PLC', 'Creature — Treefolk').
card_original_text('deadwood treefolk'/'PLC', 'Vanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Deadwood Treefolk comes into play or leaves play, return another target creature card from your graveyard to your hand.').
card_first_print('deadwood treefolk', 'PLC').
card_image_name('deadwood treefolk'/'PLC', 'deadwood treefolk').
card_uid('deadwood treefolk'/'PLC', 'PLC:Deadwood Treefolk:deadwood treefolk').
card_rarity('deadwood treefolk'/'PLC', 'Uncommon').
card_artist('deadwood treefolk'/'PLC', 'Don Hazeltine').
card_number('deadwood treefolk'/'PLC', '126').
card_multiverse_id('deadwood treefolk'/'PLC', '109740').

card_in_set('detritivore', 'PLC').
card_original_type('detritivore'/'PLC', 'Creature — Lhurgoyf').
card_original_text('detritivore'/'PLC', 'Detritivore\'s power and toughness are each equal to the number of nonbasic land cards in your opponents\' graveyards.\nSuspend X—{X}{3}{R}. X can\'t be 0.\nWhenever a time counter is removed from Detritivore while it\'s removed from the game, destroy target nonbasic land.').
card_first_print('detritivore', 'PLC').
card_image_name('detritivore'/'PLC', 'detritivore').
card_uid('detritivore'/'PLC', 'PLC:Detritivore:detritivore').
card_rarity('detritivore'/'PLC', 'Rare').
card_artist('detritivore'/'PLC', 'Paolo Parente').
card_number('detritivore'/'PLC', '96').
card_multiverse_id('detritivore'/'PLC', '126811').

card_in_set('dichotomancy', 'PLC').
card_original_type('dichotomancy'/'PLC', 'Sorcery').
card_original_text('dichotomancy'/'PLC', 'For each tapped nonland permanent target opponent controls, search that player\'s library for a card with the same name as that permanent and put it into play under your control. Then that player shuffles his or her library.\nSuspend 3—{1}{U}{U}').
card_first_print('dichotomancy', 'PLC').
card_image_name('dichotomancy'/'PLC', 'dichotomancy').
card_uid('dichotomancy'/'PLC', 'PLC:Dichotomancy:dichotomancy').
card_rarity('dichotomancy'/'PLC', 'Rare').
card_artist('dichotomancy'/'PLC', 'Steven Belledin').
card_number('dichotomancy'/'PLC', '38').
card_multiverse_id('dichotomancy'/'PLC', '122455').

card_in_set('dismal failure', 'PLC').
card_original_type('dismal failure'/'PLC', 'Instant').
card_original_text('dismal failure'/'PLC', 'Counter target spell. Its controller discards a card.').
card_first_print('dismal failure', 'PLC').
card_image_name('dismal failure'/'PLC', 'dismal failure').
card_uid('dismal failure'/'PLC', 'PLC:Dismal Failure:dismal failure').
card_rarity('dismal failure'/'PLC', 'Uncommon').
card_artist('dismal failure'/'PLC', 'Dan Scott').
card_number('dismal failure'/'PLC', '39').
card_flavor_text('dismal failure'/'PLC', '\"Two magi could trade spells all day and never crown a victor. The real battle is not one of power but of will. If your confidence breaks, so too shall you.\"\n—Venser').
card_multiverse_id('dismal failure'/'PLC', '129022').

card_in_set('dormant sliver', 'PLC').
card_original_type('dormant sliver'/'PLC', 'Creature — Sliver').
card_original_text('dormant sliver'/'PLC', 'All Slivers have defender and \"When this creature comes into play, draw a card.\"').
card_first_print('dormant sliver', 'PLC').
card_image_name('dormant sliver'/'PLC', 'dormant sliver').
card_uid('dormant sliver'/'PLC', 'PLC:Dormant Sliver:dormant sliver').
card_rarity('dormant sliver'/'PLC', 'Uncommon').
card_artist('dormant sliver'/'PLC', 'Lars Grant-West').
card_number('dormant sliver'/'PLC', '156').
card_flavor_text('dormant sliver'/'PLC', '\"It triggers the hive\'s period of Ætheric fertility—a time when even feeding is a lower priority than reproduction.\"\n—Rukarumel, field journal').
card_multiverse_id('dormant sliver'/'PLC', '126025').

card_in_set('dreamscape artist', 'PLC').
card_original_type('dreamscape artist'/'PLC', 'Creature — Human Spellshaper').
card_original_text('dreamscape artist'/'PLC', '{2}{U}, {T}, Discard a card, Sacrifice a land: Search your library for up to two basic land cards and put them into play. Then shuffle your library.').
card_first_print('dreamscape artist', 'PLC').
card_image_name('dreamscape artist'/'PLC', 'dreamscape artist').
card_uid('dreamscape artist'/'PLC', 'PLC:Dreamscape Artist:dreamscape artist').
card_rarity('dreamscape artist'/'PLC', 'Common').
card_artist('dreamscape artist'/'PLC', 'Jim Murray').
card_number('dreamscape artist'/'PLC', '40').
card_flavor_text('dreamscape artist'/'PLC', 'He\'s mastered the art of sleight of land.').
card_multiverse_id('dreamscape artist'/'PLC', '134743').

card_in_set('dunerider outlaw', 'PLC').
card_original_type('dunerider outlaw'/'PLC', 'Creature — Human Rebel Rogue').
card_original_text('dunerider outlaw'/'PLC', 'Protection from green\nAt end of turn, if Dunerider Outlaw dealt damage to an opponent this turn, put a +1/+1 counter on it.').
card_first_print('dunerider outlaw', 'PLC').
card_image_name('dunerider outlaw'/'PLC', 'dunerider outlaw').
card_uid('dunerider outlaw'/'PLC', 'PLC:Dunerider Outlaw:dunerider outlaw').
card_rarity('dunerider outlaw'/'PLC', 'Uncommon').
card_artist('dunerider outlaw'/'PLC', 'Alan Pollack').
card_number('dunerider outlaw'/'PLC', '86').
card_flavor_text('dunerider outlaw'/'PLC', 'Tales of the outlaw\'s cruelty and might grew more embellished with each crime.').
card_multiverse_id('dunerider outlaw'/'PLC', '128940').
card_timeshifted('dunerider outlaw'/'PLC')
.
card_in_set('dust corona', 'PLC').
card_original_type('dust corona'/'PLC', 'Enchantment — Aura').
card_original_text('dust corona'/'PLC', 'Enchant creature\nEnchanted creature gets +2/+0 and can\'t be blocked by creatures with flying.').
card_first_print('dust corona', 'PLC').
card_image_name('dust corona'/'PLC', 'dust corona').
card_uid('dust corona'/'PLC', 'PLC:Dust Corona:dust corona').
card_rarity('dust corona'/'PLC', 'Common').
card_artist('dust corona'/'PLC', 'Luca Zontini').
card_number('dust corona'/'PLC', '97').
card_flavor_text('dust corona'/'PLC', '\"They infiltrated deep into the enemy camp under cover of darkness . . . and dirt.\"\n—Corus, viashino warrior').
card_multiverse_id('dust corona'/'PLC', '110501').

card_in_set('dust elemental', 'PLC').
card_original_type('dust elemental'/'PLC', 'Creature — Elemental').
card_original_text('dust elemental'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nFlying, fear\nWhen Dust Elemental comes into play, return three creatures you control to their owner\'s hand.').
card_first_print('dust elemental', 'PLC').
card_image_name('dust elemental'/'PLC', 'dust elemental').
card_uid('dust elemental'/'PLC', 'PLC:Dust Elemental:dust elemental').
card_rarity('dust elemental'/'PLC', 'Rare').
card_artist('dust elemental'/'PLC', 'rk post').
card_number('dust elemental'/'PLC', '5').
card_multiverse_id('dust elemental'/'PLC', '124343').

card_in_set('enslave', 'PLC').
card_original_type('enslave'/'PLC', 'Enchantment — Aura').
card_original_text('enslave'/'PLC', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, enchanted creature deals 1 damage to its owner.').
card_first_print('enslave', 'PLC').
card_image_name('enslave'/'PLC', 'enslave').
card_uid('enslave'/'PLC', 'PLC:Enslave:enslave').
card_rarity('enslave'/'PLC', 'Uncommon').
card_artist('enslave'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('enslave'/'PLC', '70').
card_multiverse_id('enslave'/'PLC', '122413').

card_in_set('erratic mutation', 'PLC').
card_original_type('erratic mutation'/'PLC', 'Instant').
card_original_text('erratic mutation'/'PLC', 'Choose target creature. Reveal cards from the top of your library until you reveal a nonland card. That creature gets +X/-X until end of turn, where X is that card\'s converted mana cost. Put all cards revealed this way on the bottom of your library in any order.').
card_first_print('erratic mutation', 'PLC').
card_image_name('erratic mutation'/'PLC', 'erratic mutation').
card_uid('erratic mutation'/'PLC', 'PLC:Erratic Mutation:erratic mutation').
card_rarity('erratic mutation'/'PLC', 'Common').
card_artist('erratic mutation'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('erratic mutation'/'PLC', '41').
card_multiverse_id('erratic mutation'/'PLC', '124476').

card_in_set('essence warden', 'PLC').
card_original_type('essence warden'/'PLC', 'Creature — Elf Shaman').
card_original_text('essence warden'/'PLC', 'Whenever another creature comes into play, you gain 1 life.').
card_first_print('essence warden', 'PLC').
card_image_name('essence warden'/'PLC', 'essence warden').
card_uid('essence warden'/'PLC', 'PLC:Essence Warden:essence warden').
card_rarity('essence warden'/'PLC', 'Common').
card_artist('essence warden'/'PLC', 'Terese Nielsen').
card_number('essence warden'/'PLC', '145').
card_flavor_text('essence warden'/'PLC', '\"The more our numbers grow, the more I gain hope that Volrath and his cursed stronghold will one day fall.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('essence warden'/'PLC', '122428').
card_timeshifted('essence warden'/'PLC')
.
card_in_set('evolution charm', 'PLC').
card_original_type('evolution charm'/'PLC', 'Instant').
card_original_text('evolution charm'/'PLC', 'Choose one Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library; or return target creature card from your graveyard to your hand; or target creature gains flying until end of turn.').
card_first_print('evolution charm', 'PLC').
card_image_name('evolution charm'/'PLC', 'evolution charm').
card_uid('evolution charm'/'PLC', 'PLC:Evolution Charm:evolution charm').
card_rarity('evolution charm'/'PLC', 'Common').
card_artist('evolution charm'/'PLC', 'John Avon').
card_number('evolution charm'/'PLC', '127').
card_multiverse_id('evolution charm'/'PLC', '124047').

card_in_set('extirpate', 'PLC').
card_original_type('extirpate'/'PLC', 'Instant').
card_original_text('extirpate'/'PLC', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nChoose target card in a graveyard other than a basic land. Search its owner\'s graveyard, hand, and library for all cards with the same name as that card and remove them from the game. Then that player shuffles his or her library.').
card_first_print('extirpate', 'PLC').
card_image_name('extirpate'/'PLC', 'extirpate').
card_uid('extirpate'/'PLC', 'PLC:Extirpate:extirpate').
card_rarity('extirpate'/'PLC', 'Rare').
card_artist('extirpate'/'PLC', 'Jon Foster').
card_number('extirpate'/'PLC', '71').
card_multiverse_id('extirpate'/'PLC', '126804').

card_in_set('fa\'adiyah seer', 'PLC').
card_original_type('fa\'adiyah seer'/'PLC', 'Creature — Human Shaman').
card_original_text('fa\'adiyah seer'/'PLC', '{T}: Draw a card and reveal it. If it isn\'t a land card, discard it.').
card_first_print('fa\'adiyah seer', 'PLC').
card_image_name('fa\'adiyah seer'/'PLC', 'fa\'adiyah seer').
card_uid('fa\'adiyah seer'/'PLC', 'PLC:Fa\'adiyah Seer:fa\'adiyah seer').
card_rarity('fa\'adiyah seer'/'PLC', 'Common').
card_artist('fa\'adiyah seer'/'PLC', 'Heather Hudson').
card_number('fa\'adiyah seer'/'PLC', '146').
card_flavor_text('fa\'adiyah seer'/'PLC', '\"My visions don\'t show me home. They show me where home is yet to be found.\"').
card_multiverse_id('fa\'adiyah seer'/'PLC', '122374').
card_timeshifted('fa\'adiyah seer'/'PLC')
.
card_in_set('fatal frenzy', 'PLC').
card_original_type('fatal frenzy'/'PLC', 'Instant').
card_original_text('fatal frenzy'/'PLC', 'Until end of turn, target creature you control gains trample and gets +X/+0, where X is its power. Sacrifice it at end of turn.').
card_first_print('fatal frenzy', 'PLC').
card_image_name('fatal frenzy'/'PLC', 'fatal frenzy').
card_uid('fatal frenzy'/'PLC', 'PLC:Fatal Frenzy:fatal frenzy').
card_rarity('fatal frenzy'/'PLC', 'Rare').
card_artist('fatal frenzy'/'PLC', 'Steve Ellis').
card_number('fatal frenzy'/'PLC', '98').
card_flavor_text('fatal frenzy'/'PLC', '\"I hope to have such a death—lying in triumph upon the broken bodies of those who slew me.\"\n—Radha, Keldon warlord').
card_multiverse_id('fatal frenzy'/'PLC', '124748').

card_in_set('firefright mage', 'PLC').
card_original_type('firefright mage'/'PLC', 'Creature — Goblin Spellshaper').
card_original_text('firefright mage'/'PLC', '{1}{R}, {T}, Discard a card: Target creature can\'t be blocked this turn except by artifact creatures and/or red creatures.').
card_first_print('firefright mage', 'PLC').
card_image_name('firefright mage'/'PLC', 'firefright mage').
card_uid('firefright mage'/'PLC', 'PLC:Firefright Mage:firefright mage').
card_rarity('firefright mage'/'PLC', 'Common').
card_artist('firefright mage'/'PLC', 'Greg Staples').
card_number('firefright mage'/'PLC', '99').
card_flavor_text('firefright mage'/'PLC', 'After millennia of advancement in goblin military theory, Toggo VI realized that almost everyone is afraid of fire.').
card_multiverse_id('firefright mage'/'PLC', '130720').

card_in_set('frenetic sliver', 'PLC').
card_original_type('frenetic sliver'/'PLC', 'Creature — Sliver').
card_original_text('frenetic sliver'/'PLC', 'All Slivers have \"{0}: If this creature is in play, flip a coin. If you win the flip, remove this creature from the game and return it to play under its owner\'s control at end of turn. If you lose the flip, sacrifice it.\"').
card_first_print('frenetic sliver', 'PLC').
card_image_name('frenetic sliver'/'PLC', 'frenetic sliver').
card_uid('frenetic sliver'/'PLC', 'PLC:Frenetic Sliver:frenetic sliver').
card_rarity('frenetic sliver'/'PLC', 'Uncommon').
card_artist('frenetic sliver'/'PLC', 'Luca Zontini').
card_number('frenetic sliver'/'PLC', '157').
card_multiverse_id('frenetic sliver'/'PLC', '126011').

card_in_set('frozen æther', 'PLC').
card_original_type('frozen æther'/'PLC', 'Enchantment').
card_original_text('frozen æther'/'PLC', 'Artifacts, creatures, and lands your opponents control come into play tapped.').
card_first_print('frozen æther', 'PLC').
card_image_name('frozen æther'/'PLC', 'frozen aether').
card_uid('frozen æther'/'PLC', 'PLC:Frozen Æther:frozen aether').
card_rarity('frozen æther'/'PLC', 'Uncommon').
card_artist('frozen æther'/'PLC', 'Dan Dos Santos').
card_number('frozen æther'/'PLC', '54').
card_flavor_text('frozen æther'/'PLC', 'Gjornersen entertained his followers by taking bets on which would move first—the drowsing land wurm or the frozen goblins.').
card_multiverse_id('frozen æther'/'PLC', '122402').
card_timeshifted('frozen æther'/'PLC')
.
card_in_set('fungal behemoth', 'PLC').
card_original_type('fungal behemoth'/'PLC', 'Creature — Fungus').
card_original_text('fungal behemoth'/'PLC', 'Fungal Behemoth\'s power and toughness are each equal to the number of +1/+1 counters on creatures you control.\nSuspend X—{X}{G}{G}. X can\'t be 0.\nWhenever a time counter is removed from Fungal Behemoth while it\'s removed from the game, you may put a +1/+1 counter on target creature.').
card_first_print('fungal behemoth', 'PLC').
card_image_name('fungal behemoth'/'PLC', 'fungal behemoth').
card_uid('fungal behemoth'/'PLC', 'PLC:Fungal Behemoth:fungal behemoth').
card_rarity('fungal behemoth'/'PLC', 'Rare').
card_artist('fungal behemoth'/'PLC', 'Mark Tedin').
card_number('fungal behemoth'/'PLC', '128').
card_multiverse_id('fungal behemoth'/'PLC', '124065').

card_in_set('fury charm', 'PLC').
card_original_type('fury charm'/'PLC', 'Instant').
card_original_text('fury charm'/'PLC', 'Choose one Destroy target artifact; or target creature gets +1/+1 and gains trample until end of turn; or remove two time counters from target permanent or suspended card.').
card_first_print('fury charm', 'PLC').
card_image_name('fury charm'/'PLC', 'fury charm').
card_uid('fury charm'/'PLC', 'PLC:Fury Charm:fury charm').
card_rarity('fury charm'/'PLC', 'Common').
card_artist('fury charm'/'PLC', 'John Avon').
card_number('fury charm'/'PLC', '100').
card_multiverse_id('fury charm'/'PLC', '124039').

card_in_set('gaea\'s anthem', 'PLC').
card_original_type('gaea\'s anthem'/'PLC', 'Enchantment').
card_original_text('gaea\'s anthem'/'PLC', 'Creatures you control get +1/+1.').
card_first_print('gaea\'s anthem', 'PLC').
card_image_name('gaea\'s anthem'/'PLC', 'gaea\'s anthem').
card_uid('gaea\'s anthem'/'PLC', 'PLC:Gaea\'s Anthem:gaea\'s anthem').
card_rarity('gaea\'s anthem'/'PLC', 'Rare').
card_artist('gaea\'s anthem'/'PLC', 'Greg Staples').
card_number('gaea\'s anthem'/'PLC', '147').
card_flavor_text('gaea\'s anthem'/'PLC', '\"To those who can hear it, Gaea\'s battle song brings power as swift as sunlight and as enduring as the deep roots of the forest.\"\n—Gamelen, Citanul elder').
card_multiverse_id('gaea\'s anthem'/'PLC', '122367').
card_timeshifted('gaea\'s anthem'/'PLC')
.
card_in_set('ghost tactician', 'PLC').
card_original_type('ghost tactician'/'PLC', 'Creature — Spirit Spellshaper').
card_original_text('ghost tactician'/'PLC', '{W}, {T}, Discard a card: Creatures you control get +1/+0 until end of turn.').
card_first_print('ghost tactician', 'PLC').
card_image_name('ghost tactician'/'PLC', 'ghost tactician').
card_uid('ghost tactician'/'PLC', 'PLC:Ghost Tactician:ghost tactician').
card_rarity('ghost tactician'/'PLC', 'Common').
card_artist('ghost tactician'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('ghost tactician'/'PLC', '6').
card_flavor_text('ghost tactician'/'PLC', 'Its ethereal hand confers a lifetime of experience with combat and steel.').
card_multiverse_id('ghost tactician'/'PLC', '126809').

card_in_set('giant dustwasp', 'PLC').
card_original_type('giant dustwasp'/'PLC', 'Creature — Insect').
card_original_text('giant dustwasp'/'PLC', 'Flying\nSuspend 4—{1}{G} (Rather than play this card from your hand, you may pay {1}{G} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)').
card_first_print('giant dustwasp', 'PLC').
card_image_name('giant dustwasp'/'PLC', 'giant dustwasp').
card_uid('giant dustwasp'/'PLC', 'PLC:Giant Dustwasp:giant dustwasp').
card_rarity('giant dustwasp'/'PLC', 'Common').
card_artist('giant dustwasp'/'PLC', 'Greg Hildebrandt').
card_number('giant dustwasp'/'PLC', '129').
card_multiverse_id('giant dustwasp'/'PLC', '122313').

card_in_set('gone', 'PLC').
card_original_type('gone'/'PLC', 'Instant').
card_original_text('gone'/'PLC', 'Dead deals 2 damage to target creature.\n// \nGone\n{2}{R}\nInstant\nReturn target creature you don\'t control to its owner\'s hand.').
card_first_print('gone', 'PLC').
card_image_name('gone'/'PLC', 'deadgone').
card_uid('gone'/'PLC', 'PLC:Gone:deadgone').
card_rarity('gone'/'PLC', 'Common').
card_artist('gone'/'PLC', 'Tomas Giorello').
card_number('gone'/'PLC', '113b').
card_multiverse_id('gone'/'PLC', '126419').

card_in_set('gossamer phantasm', 'PLC').
card_original_type('gossamer phantasm'/'PLC', 'Creature — Illusion').
card_original_text('gossamer phantasm'/'PLC', 'Flying\nWhen Gossamer Phantasm becomes the target of a spell or ability, sacrifice it.').
card_first_print('gossamer phantasm', 'PLC').
card_image_name('gossamer phantasm'/'PLC', 'gossamer phantasm').
card_uid('gossamer phantasm'/'PLC', 'PLC:Gossamer Phantasm:gossamer phantasm').
card_rarity('gossamer phantasm'/'PLC', 'Common').
card_artist('gossamer phantasm'/'PLC', 'Jon Foster').
card_number('gossamer phantasm'/'PLC', '55').
card_flavor_text('gossamer phantasm'/'PLC', '\"Belief matters more than truth. Every moment, belief in imaginary things alters lives while truth sits unnoticed and waits.\"\n—Hakim, Loreweaver').
card_multiverse_id('gossamer phantasm'/'PLC', '122280').
card_timeshifted('gossamer phantasm'/'PLC')
.
card_in_set('groundbreaker', 'PLC').
card_original_type('groundbreaker'/'PLC', 'Creature — Elemental').
card_original_text('groundbreaker'/'PLC', 'Trample, haste\nAt end of turn, sacrifice Groundbreaker.').
card_image_name('groundbreaker'/'PLC', 'groundbreaker').
card_uid('groundbreaker'/'PLC', 'PLC:Groundbreaker:groundbreaker').
card_rarity('groundbreaker'/'PLC', 'Rare').
card_artist('groundbreaker'/'PLC', 'Matt Cavotta').
card_number('groundbreaker'/'PLC', '148').
card_flavor_text('groundbreaker'/'PLC', 'The earth\'s memory is long, its retribution brief.').
card_multiverse_id('groundbreaker'/'PLC', '122429').
card_timeshifted('groundbreaker'/'PLC')
.
card_in_set('hammerheim deadeye', 'PLC').
card_original_type('hammerheim deadeye'/'PLC', 'Creature — Giant Warrior').
card_original_text('hammerheim deadeye'/'PLC', 'Echo {5}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Hammerheim Deadeye comes into play, destroy target creature with flying.').
card_first_print('hammerheim deadeye', 'PLC').
card_image_name('hammerheim deadeye'/'PLC', 'hammerheim deadeye').
card_uid('hammerheim deadeye'/'PLC', 'PLC:Hammerheim Deadeye:hammerheim deadeye').
card_rarity('hammerheim deadeye'/'PLC', 'Uncommon').
card_artist('hammerheim deadeye'/'PLC', 'Carl Critchlow').
card_number('hammerheim deadeye'/'PLC', '101').
card_multiverse_id('hammerheim deadeye'/'PLC', '128948').

card_in_set('harmonize', 'PLC').
card_original_type('harmonize'/'PLC', 'Sorcery').
card_original_text('harmonize'/'PLC', 'Draw three cards.').
card_image_name('harmonize'/'PLC', 'harmonize').
card_uid('harmonize'/'PLC', 'PLC:Harmonize:harmonize').
card_rarity('harmonize'/'PLC', 'Uncommon').
card_artist('harmonize'/'PLC', 'Rob Alexander').
card_number('harmonize'/'PLC', '149').
card_flavor_text('harmonize'/'PLC', '\"Life\'s greatest lessons don\'t come from focus or concentration. They come from breathing and simply noticing.\"\n—Seton, centaur druid').
card_multiverse_id('harmonize'/'PLC', '122362').
card_timeshifted('harmonize'/'PLC')
.
card_in_set('healing leaves', 'PLC').
card_original_type('healing leaves'/'PLC', 'Instant').
card_original_text('healing leaves'/'PLC', 'Choose one Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_first_print('healing leaves', 'PLC').
card_image_name('healing leaves'/'PLC', 'healing leaves').
card_uid('healing leaves'/'PLC', 'PLC:Healing Leaves:healing leaves').
card_rarity('healing leaves'/'PLC', 'Common').
card_artist('healing leaves'/'PLC', 'Michael Sutfin').
card_number('healing leaves'/'PLC', '150').
card_flavor_text('healing leaves'/'PLC', 'The elves of Llanowar don\'t trust in alchemy. They rely instead on pure herbs harvested from the forest\'s sacred heart.').
card_multiverse_id('healing leaves'/'PLC', '122371').
card_timeshifted('healing leaves'/'PLC')
.
card_in_set('hedge troll', 'PLC').
card_original_type('hedge troll'/'PLC', 'Creature — Troll Cleric').
card_original_text('hedge troll'/'PLC', 'Hedge Troll gets +1/+1 as long as you control a Plains.\n{W}: Regenerate Hedge Troll.').
card_image_name('hedge troll'/'PLC', 'hedge troll').
card_uid('hedge troll'/'PLC', 'PLC:Hedge Troll:hedge troll').
card_rarity('hedge troll'/'PLC', 'Uncommon').
card_artist('hedge troll'/'PLC', 'Paolo Parente').
card_number('hedge troll'/'PLC', '151').
card_flavor_text('hedge troll'/'PLC', 'His abode was clean and bare, not a morsel in sight. I asked him what he ate, fearing the answer. He smiled and said his faith alone sustained him.').
card_multiverse_id('hedge troll'/'PLC', '122405').
card_timeshifted('hedge troll'/'PLC')
.
card_in_set('heroes remembered', 'PLC').
card_original_type('heroes remembered'/'PLC', 'Sorcery').
card_original_text('heroes remembered'/'PLC', 'You gain 20 life.\nSuspend 10—{W} (Rather than play this card from your hand, you may pay {W} and remove it from the game with ten time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('heroes remembered', 'PLC').
card_image_name('heroes remembered'/'PLC', 'heroes remembered').
card_uid('heroes remembered'/'PLC', 'PLC:Heroes Remembered:heroes remembered').
card_rarity('heroes remembered'/'PLC', 'Rare').
card_artist('heroes remembered'/'PLC', 'Michael Phillippi').
card_number('heroes remembered'/'PLC', '7').
card_multiverse_id('heroes remembered'/'PLC', '122438').

card_in_set('hunting wilds', 'PLC').
card_original_type('hunting wilds'/'PLC', 'Sorcery').
card_original_text('hunting wilds'/'PLC', 'Kicker {3}{G} (You may pay an additional {3}{G} as you play this spell.)\nSearch your library for up to two Forest cards and put them into play tapped. Then shuffle your library.\nIf the kicker cost was paid, untap all Forests put into play this way. They become 3/3 green creatures with haste that are still lands.').
card_first_print('hunting wilds', 'PLC').
card_image_name('hunting wilds'/'PLC', 'hunting wilds').
card_uid('hunting wilds'/'PLC', 'PLC:Hunting Wilds:hunting wilds').
card_rarity('hunting wilds'/'PLC', 'Uncommon').
card_artist('hunting wilds'/'PLC', 'Steve Ellis').
card_number('hunting wilds'/'PLC', '130').
card_multiverse_id('hunting wilds'/'PLC', '122458').

card_in_set('imp\'s mischief', 'PLC').
card_original_type('imp\'s mischief'/'PLC', 'Instant').
card_original_text('imp\'s mischief'/'PLC', 'Change the target of target spell with a single target. You lose life equal to that spell\'s converted mana cost.').
card_first_print('imp\'s mischief', 'PLC').
card_image_name('imp\'s mischief'/'PLC', 'imp\'s mischief').
card_uid('imp\'s mischief'/'PLC', 'PLC:Imp\'s Mischief:imp\'s mischief').
card_rarity('imp\'s mischief'/'PLC', 'Rare').
card_artist('imp\'s mischief'/'PLC', 'Thomas M. Baxa').
card_number('imp\'s mischief'/'PLC', '72').
card_flavor_text('imp\'s mischief'/'PLC', '\"Do the innocent pay for the crimes of the guilty? Of course they do. That\'s the fate of the weak.\"\n—Nicol Bolas').
card_multiverse_id('imp\'s mischief'/'PLC', '122434').

card_in_set('intet, the dreamer', 'PLC').
card_original_type('intet, the dreamer'/'PLC', 'Legendary Creature — Dragon').
card_original_text('intet, the dreamer'/'PLC', 'Flying\nWhenever Intet, the Dreamer deals combat damage to a player, you may pay {2}{U}. If you do, remove the top card of your library from the game face down. You may look at that card as long as it remains removed from the game. You may play that card without paying its mana cost as long as Intet remains in play.').
card_first_print('intet, the dreamer', 'PLC').
card_image_name('intet, the dreamer'/'PLC', 'intet, the dreamer').
card_uid('intet, the dreamer'/'PLC', 'PLC:Intet, the Dreamer:intet, the dreamer').
card_rarity('intet, the dreamer'/'PLC', 'Rare').
card_artist('intet, the dreamer'/'PLC', 'Dan Scott').
card_number('intet, the dreamer'/'PLC', '158').
card_multiverse_id('intet, the dreamer'/'PLC', '124073').

card_in_set('jedit ojanen of efrava', 'PLC').
card_original_type('jedit ojanen of efrava'/'PLC', 'Legendary Creature — Cat Warrior Lord').
card_original_text('jedit ojanen of efrava'/'PLC', 'Forestwalk\nWhenever Jedit Ojanen of Efrava attacks or blocks, put a 2/2 green Cat Warrior creature token with forestwalk into play.').
card_first_print('jedit ojanen of efrava', 'PLC').
card_image_name('jedit ojanen of efrava'/'PLC', 'jedit ojanen of efrava').
card_uid('jedit ojanen of efrava'/'PLC', 'PLC:Jedit Ojanen of Efrava:jedit ojanen of efrava').
card_rarity('jedit ojanen of efrava'/'PLC', 'Rare').
card_artist('jedit ojanen of efrava'/'PLC', 'Carl Critchlow').
card_number('jedit ojanen of efrava'/'PLC', '131').
card_flavor_text('jedit ojanen of efrava'/'PLC', 'The cat warriors recognized this Jedit\'s face, but not his fierce loyalty to Efrava.').
card_multiverse_id('jedit ojanen of efrava'/'PLC', '124344').

card_in_set('jodah\'s avenger', 'PLC').
card_original_type('jodah\'s avenger'/'PLC', 'Creature — Shapeshifter').
card_original_text('jodah\'s avenger'/'PLC', '{0}: Until end of turn, Jodah\'s Avenger gets -1/-1 and gains your choice of double strike, protection from red, vigilance, or shadow. (A creature with shadow can block or be blocked by only creatures with shadow.)').
card_first_print('jodah\'s avenger', 'PLC').
card_image_name('jodah\'s avenger'/'PLC', 'jodah\'s avenger').
card_uid('jodah\'s avenger'/'PLC', 'PLC:Jodah\'s Avenger:jodah\'s avenger').
card_rarity('jodah\'s avenger'/'PLC', 'Uncommon').
card_artist('jodah\'s avenger'/'PLC', 'Pete Venters').
card_number('jodah\'s avenger'/'PLC', '42').
card_multiverse_id('jodah\'s avenger'/'PLC', '124482').

card_in_set('kavu predator', 'PLC').
card_original_type('kavu predator'/'PLC', 'Creature — Kavu').
card_original_text('kavu predator'/'PLC', 'Trample\nWhenever an opponent gains life, put that many +1/+1 counters on Kavu Predator.').
card_first_print('kavu predator', 'PLC').
card_image_name('kavu predator'/'PLC', 'kavu predator').
card_uid('kavu predator'/'PLC', 'PLC:Kavu Predator:kavu predator').
card_rarity('kavu predator'/'PLC', 'Uncommon').
card_artist('kavu predator'/'PLC', 'Dan Scott').
card_number('kavu predator'/'PLC', '132').
card_flavor_text('kavu predator'/'PLC', 'In a withered world, the scent of healthy prey is enough to drive a predator to frenzy.').
card_multiverse_id('kavu predator'/'PLC', '126813').

card_in_set('keen sense', 'PLC').
card_original_type('keen sense'/'PLC', 'Enchantment — Aura').
card_original_text('keen sense'/'PLC', 'Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_first_print('keen sense', 'PLC').
card_image_name('keen sense'/'PLC', 'keen sense').
card_uid('keen sense'/'PLC', 'PLC:Keen Sense:keen sense').
card_rarity('keen sense'/'PLC', 'Uncommon').
card_artist('keen sense'/'PLC', 'Jim Nelson').
card_number('keen sense'/'PLC', '152').
card_flavor_text('keen sense'/'PLC', 'Crovax sensed that Mirri wasn\'t ready for the curse taking hold of her. Weeping in his heart, he fled.').
card_multiverse_id('keen sense'/'PLC', '122451').
card_timeshifted('keen sense'/'PLC')
.
card_in_set('keldon marauders', 'PLC').
card_original_type('keldon marauders'/'PLC', 'Creature — Human Warrior').
card_original_text('keldon marauders'/'PLC', 'Vanishing 2 (This permanent comes into play with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Keldon Marauders comes into play or leaves play, it deals 1 damage to target player.').
card_first_print('keldon marauders', 'PLC').
card_image_name('keldon marauders'/'PLC', 'keldon marauders').
card_uid('keldon marauders'/'PLC', 'PLC:Keldon Marauders:keldon marauders').
card_rarity('keldon marauders'/'PLC', 'Common').
card_artist('keldon marauders'/'PLC', 'Alex Horley-Orlandelli').
card_number('keldon marauders'/'PLC', '102').
card_multiverse_id('keldon marauders'/'PLC', '125885').

card_in_set('kor dirge', 'PLC').
card_original_type('kor dirge'/'PLC', 'Instant').
card_original_text('kor dirge'/'PLC', 'All damage that would be dealt this turn to target creature you control by a source of your choice is dealt to another target creature instead.').
card_first_print('kor dirge', 'PLC').
card_image_name('kor dirge'/'PLC', 'kor dirge').
card_uid('kor dirge'/'PLC', 'PLC:Kor Dirge:kor dirge').
card_rarity('kor dirge'/'PLC', 'Uncommon').
card_artist('kor dirge'/'PLC', 'Alex Horley-Orlandelli').
card_number('kor dirge'/'PLC', '87').
card_flavor_text('kor dirge'/'PLC', 'Rath\'s new evincar eliminated the brutish moggs and took a new slave race, one more reminiscent of her own feline grace.').
card_multiverse_id('kor dirge'/'PLC', '131006').
card_timeshifted('kor dirge'/'PLC')
.
card_in_set('lavacore elemental', 'PLC').
card_original_type('lavacore elemental'/'PLC', 'Creature — Elemental').
card_original_text('lavacore elemental'/'PLC', 'Vanishing 1 (This permanent comes into play with a time counter on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhenever a creature you control deals combat damage to a player, put a time counter on Lavacore Elemental.').
card_first_print('lavacore elemental', 'PLC').
card_image_name('lavacore elemental'/'PLC', 'lavacore elemental').
card_uid('lavacore elemental'/'PLC', 'PLC:Lavacore Elemental:lavacore elemental').
card_rarity('lavacore elemental'/'PLC', 'Uncommon').
card_artist('lavacore elemental'/'PLC', 'E. M. Gist').
card_number('lavacore elemental'/'PLC', '103').
card_multiverse_id('lavacore elemental'/'PLC', '111055').

card_in_set('life and limb', 'PLC').
card_original_type('life and limb'/'PLC', 'Enchantment').
card_original_text('life and limb'/'PLC', 'All Forests and all Saprolings are 1/1 green Saproling creatures and Forest lands in addition to their other types.').
card_first_print('life and limb', 'PLC').
card_image_name('life and limb'/'PLC', 'life and limb').
card_uid('life and limb'/'PLC', 'PLC:Life and Limb:life and limb').
card_rarity('life and limb'/'PLC', 'Rare').
card_artist('life and limb'/'PLC', 'Jim Nelson').
card_number('life and limb'/'PLC', '133').
card_flavor_text('life and limb'/'PLC', '\"It was a sight of pain and awe—a twisted forest, migrating across the salt plain in search of richer soil.\"\n—Edahlis, greenseeker').
card_multiverse_id('life and limb'/'PLC', '124471').

card_in_set('magus of the arena', 'PLC').
card_original_type('magus of the arena'/'PLC', 'Creature — Human Wizard').
card_original_text('magus of the arena'/'PLC', '{3}, {T}: Tap target creature you control and target creature of an opponent\'s choice he or she controls. Each of those creatures deals damage equal to its power to the other.').
card_first_print('magus of the arena', 'PLC').
card_image_name('magus of the arena'/'PLC', 'magus of the arena').
card_uid('magus of the arena'/'PLC', 'PLC:Magus of the Arena:magus of the arena').
card_rarity('magus of the arena'/'PLC', 'Rare').
card_artist('magus of the arena'/'PLC', 'Thomas M. Baxa').
card_number('magus of the arena'/'PLC', '104').
card_flavor_text('magus of the arena'/'PLC', 'The magus still hears the roar of the crowds. They\'re gone, but the will to compete remains.').
card_multiverse_id('magus of the arena'/'PLC', '134746').

card_in_set('magus of the bazaar', 'PLC').
card_original_type('magus of the bazaar'/'PLC', 'Creature — Human Wizard').
card_original_text('magus of the bazaar'/'PLC', '{T}: Draw two cards, then discard three cards.').
card_first_print('magus of the bazaar', 'PLC').
card_image_name('magus of the bazaar'/'PLC', 'magus of the bazaar').
card_uid('magus of the bazaar'/'PLC', 'PLC:Magus of the Bazaar:magus of the bazaar').
card_rarity('magus of the bazaar'/'PLC', 'Rare').
card_artist('magus of the bazaar'/'PLC', 'Rob Alexander').
card_number('magus of the bazaar'/'PLC', '43').
card_flavor_text('magus of the bazaar'/'PLC', '\"Some trade in goods, some in secrets. My soul has walked the futures, and I offer the rare coin of possibility.\"').
card_multiverse_id('magus of the bazaar'/'PLC', '131008').

card_in_set('magus of the coffers', 'PLC').
card_original_type('magus of the coffers'/'PLC', 'Creature — Human Wizard').
card_original_text('magus of the coffers'/'PLC', '{2}, {T}: Add {B} to your mana pool for each Swamp you control.').
card_first_print('magus of the coffers', 'PLC').
card_image_name('magus of the coffers'/'PLC', 'magus of the coffers').
card_uid('magus of the coffers'/'PLC', 'PLC:Magus of the Coffers:magus of the coffers').
card_rarity('magus of the coffers'/'PLC', 'Rare').
card_artist('magus of the coffers'/'PLC', 'Don Hazeltine').
card_number('magus of the coffers'/'PLC', '73').
card_flavor_text('magus of the coffers'/'PLC', 'All that remains of the Cabal are the echoes in its deepest vault. Yet those who hear the echoes feel a power undiminished by the dust of ages.').
card_multiverse_id('magus of the coffers'/'PLC', '124507').

card_in_set('magus of the library', 'PLC').
card_original_type('magus of the library'/'PLC', 'Creature — Human Wizard').
card_original_text('magus of the library'/'PLC', '{T}: Add {1} to your mana pool.\n{T}: Draw a card. Play this ability only if you have exactly seven cards in hand.').
card_first_print('magus of the library', 'PLC').
card_image_name('magus of the library'/'PLC', 'magus of the library').
card_uid('magus of the library'/'PLC', 'PLC:Magus of the Library:magus of the library').
card_rarity('magus of the library'/'PLC', 'Rare').
card_artist('magus of the library'/'PLC', 'Wayne Reynolds').
card_number('magus of the library'/'PLC', '134').
card_flavor_text('magus of the library'/'PLC', 'The ancient books slowly crumbled, their secrets turning to dust. But their every word sings within the magus\'s head.').
card_multiverse_id('magus of the library'/'PLC', '134741').

card_in_set('magus of the tabernacle', 'PLC').
card_original_type('magus of the tabernacle'/'PLC', 'Creature — Human Wizard').
card_original_text('magus of the tabernacle'/'PLC', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay {1}.\"').
card_first_print('magus of the tabernacle', 'PLC').
card_image_name('magus of the tabernacle'/'PLC', 'magus of the tabernacle').
card_uid('magus of the tabernacle'/'PLC', 'PLC:Magus of the Tabernacle:magus of the tabernacle').
card_rarity('magus of the tabernacle'/'PLC', 'Rare').
card_artist('magus of the tabernacle'/'PLC', 'Randy Gallegos').
card_number('magus of the tabernacle'/'PLC', '8').
card_flavor_text('magus of the tabernacle'/'PLC', 'The Tabernacle\'s disciples channel the emanations from Pendrell Vale, spreading its paradoxical demand to be both worshipped and left alone.').
card_multiverse_id('magus of the tabernacle'/'PLC', '130719').

card_in_set('malach of the dawn', 'PLC').
card_original_type('malach of the dawn'/'PLC', 'Creature — Angel').
card_original_text('malach of the dawn'/'PLC', 'Flying\n{W}{W}{W}: Regenerate Malach of the Dawn.').
card_first_print('malach of the dawn', 'PLC').
card_image_name('malach of the dawn'/'PLC', 'malach of the dawn').
card_uid('malach of the dawn'/'PLC', 'PLC:Malach of the Dawn:malach of the dawn').
card_rarity('malach of the dawn'/'PLC', 'Uncommon').
card_artist('malach of the dawn'/'PLC', 'Steve Prescott').
card_number('malach of the dawn'/'PLC', '24').
card_flavor_text('malach of the dawn'/'PLC', '\"The sun rises, but the world still feels dark. Pray for the arrival of the malachim—they\'ll bring Dawn to the world and to our hearts.\"\n—Sister Betje, Miracles of the Saints').
card_multiverse_id('malach of the dawn'/'PLC', '122481').
card_timeshifted('malach of the dawn'/'PLC')
.
card_in_set('mana tithe', 'PLC').
card_original_type('mana tithe'/'PLC', 'Instant').
card_original_text('mana tithe'/'PLC', 'Counter target spell unless its controller pays {1}.').
card_image_name('mana tithe'/'PLC', 'mana tithe').
card_uid('mana tithe'/'PLC', 'PLC:Mana Tithe:mana tithe').
card_rarity('mana tithe'/'PLC', 'Common').
card_artist('mana tithe'/'PLC', 'Martina Pilcerova').
card_number('mana tithe'/'PLC', '25').
card_flavor_text('mana tithe'/'PLC', '\"Those who seek to upset the balance must be taxed for such ambitions.\"\n—Verithain, mesa high priest').
card_multiverse_id('mana tithe'/'PLC', '122324').
card_timeshifted('mana tithe'/'PLC')
.
card_in_set('mantle of leadership', 'PLC').
card_original_type('mantle of leadership'/'PLC', 'Enchantment — Aura').
card_original_text('mantle of leadership'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nWhenever a creature comes into play, enchanted creature gets +2/+2 until end of turn.').
card_first_print('mantle of leadership', 'PLC').
card_image_name('mantle of leadership'/'PLC', 'mantle of leadership').
card_uid('mantle of leadership'/'PLC', 'PLC:Mantle of Leadership:mantle of leadership').
card_rarity('mantle of leadership'/'PLC', 'Uncommon').
card_artist('mantle of leadership'/'PLC', 'Thomas M. Baxa').
card_number('mantle of leadership'/'PLC', '9').
card_multiverse_id('mantle of leadership'/'PLC', '122349').

card_in_set('melancholy', 'PLC').
card_original_type('melancholy'/'PLC', 'Enchantment — Aura').
card_original_text('melancholy'/'PLC', 'Enchant creature\nWhen Melancholy comes into play, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of your upkeep, sacrifice Melancholy unless you pay {B}.').
card_first_print('melancholy', 'PLC').
card_image_name('melancholy'/'PLC', 'melancholy').
card_uid('melancholy'/'PLC', 'PLC:Melancholy:melancholy').
card_rarity('melancholy'/'PLC', 'Common').
card_artist('melancholy'/'PLC', 'Lars Grant-West').
card_number('melancholy'/'PLC', '88').
card_multiverse_id('melancholy'/'PLC', '130714').
card_timeshifted('melancholy'/'PLC')
.
card_in_set('merfolk thaumaturgist', 'PLC').
card_original_type('merfolk thaumaturgist'/'PLC', 'Creature — Merfolk Wizard').
card_original_text('merfolk thaumaturgist'/'PLC', '{T}: Switch target creature\'s power and toughness until end of turn.').
card_first_print('merfolk thaumaturgist', 'PLC').
card_image_name('merfolk thaumaturgist'/'PLC', 'merfolk thaumaturgist').
card_uid('merfolk thaumaturgist'/'PLC', 'PLC:Merfolk Thaumaturgist:merfolk thaumaturgist').
card_rarity('merfolk thaumaturgist'/'PLC', 'Common').
card_artist('merfolk thaumaturgist'/'PLC', 'Steve Prescott').
card_number('merfolk thaumaturgist'/'PLC', '56').
card_flavor_text('merfolk thaumaturgist'/'PLC', '\"Blasted fishtails! It\'s not enough that they meddle with my head, but they twist my body as well.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('merfolk thaumaturgist'/'PLC', '125877').
card_timeshifted('merfolk thaumaturgist'/'PLC')
.
card_in_set('mesa enchantress', 'PLC').
card_original_type('mesa enchantress'/'PLC', 'Creature — Human Druid').
card_original_text('mesa enchantress'/'PLC', 'Whenever you play an enchantment spell, you may draw a card.').
card_first_print('mesa enchantress', 'PLC').
card_image_name('mesa enchantress'/'PLC', 'mesa enchantress').
card_uid('mesa enchantress'/'PLC', 'PLC:Mesa Enchantress:mesa enchantress').
card_rarity('mesa enchantress'/'PLC', 'Rare').
card_artist('mesa enchantress'/'PLC', 'Randy Gallegos').
card_number('mesa enchantress'/'PLC', '26').
card_flavor_text('mesa enchantress'/'PLC', 'She shepherds mysteries and dust as others would a flock of sheep.').
card_multiverse_id('mesa enchantress'/'PLC', '126214').
card_timeshifted('mesa enchantress'/'PLC')
.
card_in_set('midnight charm', 'PLC').
card_original_type('midnight charm'/'PLC', 'Instant').
card_original_text('midnight charm'/'PLC', 'Choose one Midnight Charm deals 1 damage to target creature and you gain 1 life; or target creature gains first strike until end of turn; or tap target creature.').
card_first_print('midnight charm', 'PLC').
card_image_name('midnight charm'/'PLC', 'midnight charm').
card_uid('midnight charm'/'PLC', 'PLC:Midnight Charm:midnight charm').
card_rarity('midnight charm'/'PLC', 'Common').
card_artist('midnight charm'/'PLC', 'John Avon').
card_number('midnight charm'/'PLC', '74').
card_multiverse_id('midnight charm'/'PLC', '124034').

card_in_set('mire boa', 'PLC').
card_original_type('mire boa'/'PLC', 'Creature — Snake').
card_original_text('mire boa'/'PLC', 'Swampwalk\n{G}: Regenerate Mire Boa.').
card_first_print('mire boa', 'PLC').
card_image_name('mire boa'/'PLC', 'mire boa').
card_uid('mire boa'/'PLC', 'PLC:Mire Boa:mire boa').
card_rarity('mire boa'/'PLC', 'Common').
card_artist('mire boa'/'PLC', 'Greg Hildebrandt').
card_number('mire boa'/'PLC', '135').
card_flavor_text('mire boa'/'PLC', 'Mire slime courses through its veins in place of blood. No sooner does it bleed than it opens its mouth to replace the loss.').
card_multiverse_id('mire boa'/'PLC', '122420').

card_in_set('mirri the cursed', 'PLC').
card_original_type('mirri the cursed'/'PLC', 'Legendary Creature — Vampire Cat').
card_original_text('mirri the cursed'/'PLC', 'Flying, first strike, haste\nWhenever Mirri the Cursed deals combat damage to a creature, put a +1/+1 counter on Mirri the Cursed.').
card_first_print('mirri the cursed', 'PLC').
card_image_name('mirri the cursed'/'PLC', 'mirri the cursed').
card_uid('mirri the cursed'/'PLC', 'PLC:Mirri the Cursed:mirri the cursed').
card_rarity('mirri the cursed'/'PLC', 'Rare').
card_artist('mirri the cursed'/'PLC', 'Kev Walker').
card_number('mirri the cursed'/'PLC', '75').
card_flavor_text('mirri the cursed'/'PLC', 'A hero fails, a martyr falls. Time twists and destinies interchange.').
card_multiverse_id('mirri the cursed'/'PLC', '122406').

card_in_set('molten firebird', 'PLC').
card_original_type('molten firebird'/'PLC', 'Creature — Phoenix').
card_original_text('molten firebird'/'PLC', 'Flying\nWhen Molten Firebird is put into a graveyard from play, return it to play under its owner\'s control at end of turn and you skip your next draw step.\n{4}{R}: Remove Molten Firebird from the game.').
card_first_print('molten firebird', 'PLC').
card_image_name('molten firebird'/'PLC', 'molten firebird').
card_uid('molten firebird'/'PLC', 'PLC:Molten Firebird:molten firebird').
card_rarity('molten firebird'/'PLC', 'Rare').
card_artist('molten firebird'/'PLC', 'Christopher Moeller').
card_number('molten firebird'/'PLC', '117').
card_multiverse_id('molten firebird'/'PLC', '134738').
card_timeshifted('molten firebird'/'PLC')
.
card_in_set('muck drubb', 'PLC').
card_original_type('muck drubb'/'PLC', 'Creature — Beast').
card_original_text('muck drubb'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nWhen Muck Drubb comes into play, change the target of target spell that targets only a single creature to Muck Drubb.\nMadness {2}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('muck drubb', 'PLC').
card_image_name('muck drubb'/'PLC', 'muck drubb').
card_uid('muck drubb'/'PLC', 'PLC:Muck Drubb:muck drubb').
card_rarity('muck drubb'/'PLC', 'Uncommon').
card_artist('muck drubb'/'PLC', 'Jim Nelson').
card_number('muck drubb'/'PLC', '76').
card_multiverse_id('muck drubb'/'PLC', '122478').

card_in_set('mycologist', 'PLC').
card_original_type('mycologist'/'PLC', 'Creature — Human Druid').
card_original_text('mycologist'/'PLC', 'At the beginning of your upkeep, put a spore counter on Mycologist.\nRemove three spore counters from Mycologist: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: You gain 2 life.').
card_first_print('mycologist', 'PLC').
card_image_name('mycologist'/'PLC', 'mycologist').
card_uid('mycologist'/'PLC', 'PLC:Mycologist:mycologist').
card_rarity('mycologist'/'PLC', 'Uncommon').
card_artist('mycologist'/'PLC', 'Kev Walker').
card_number('mycologist'/'PLC', '27').
card_multiverse_id('mycologist'/'PLC', '124033').
card_timeshifted('mycologist'/'PLC')
.
card_in_set('necrotic sliver', 'PLC').
card_original_type('necrotic sliver'/'PLC', 'Creature — Sliver').
card_original_text('necrotic sliver'/'PLC', 'All Slivers have \"{3}, Sacrifice this creature: Destroy target permanent.\"').
card_first_print('necrotic sliver', 'PLC').
card_image_name('necrotic sliver'/'PLC', 'necrotic sliver').
card_uid('necrotic sliver'/'PLC', 'PLC:Necrotic Sliver:necrotic sliver').
card_rarity('necrotic sliver'/'PLC', 'Uncommon').
card_artist('necrotic sliver'/'PLC', 'Dave Allsop').
card_number('necrotic sliver'/'PLC', '159').
card_flavor_text('necrotic sliver'/'PLC', 'Though Volrath is long dead, the slivers have become everything he wanted them to be: mindless instruments of destruction and despair.').
card_multiverse_id('necrotic sliver'/'PLC', '126023').

card_in_set('needlepeak spider', 'PLC').
card_original_type('needlepeak spider'/'PLC', 'Creature — Spider').
card_original_text('needlepeak spider'/'PLC', 'Needlepeak Spider can block as though it had flying.').
card_first_print('needlepeak spider', 'PLC').
card_image_name('needlepeak spider'/'PLC', 'needlepeak spider').
card_uid('needlepeak spider'/'PLC', 'PLC:Needlepeak Spider:needlepeak spider').
card_rarity('needlepeak spider'/'PLC', 'Common').
card_artist('needlepeak spider'/'PLC', 'Dany Orizio').
card_number('needlepeak spider'/'PLC', '105').
card_flavor_text('needlepeak spider'/'PLC', '\"It\'s a testament to the forests\' devastation that giant spiders now make their homes amid Dominaria\'s barren spires.\"\n—Aznaph, greenseeker').
card_multiverse_id('needlepeak spider'/'PLC', '122268').

card_in_set('null profusion', 'PLC').
card_original_type('null profusion'/'PLC', 'Enchantment').
card_original_text('null profusion'/'PLC', 'Skip your draw step.\nWhenever you play a card, draw a card.\nYour maximum hand size is two.').
card_first_print('null profusion', 'PLC').
card_image_name('null profusion'/'PLC', 'null profusion').
card_uid('null profusion'/'PLC', 'PLC:Null Profusion:null profusion').
card_rarity('null profusion'/'PLC', 'Rare').
card_artist('null profusion'/'PLC', 'Kev Walker').
card_number('null profusion'/'PLC', '89').
card_flavor_text('null profusion'/'PLC', '\"Some say that time is cyclical and that history inevitably repeats. My will is my own. I won\'t bow to fate.\"\n—Volrath').
card_multiverse_id('null profusion'/'PLC', '125874').
card_timeshifted('null profusion'/'PLC')
.
card_in_set('numot, the devastator', 'PLC').
card_original_type('numot, the devastator'/'PLC', 'Legendary Creature — Dragon').
card_original_text('numot, the devastator'/'PLC', 'Flying\nWhenever Numot, the Devastator deals combat damage to a player, you may pay {2}{R}. If you do, destroy up to two target lands.').
card_first_print('numot, the devastator', 'PLC').
card_image_name('numot, the devastator'/'PLC', 'numot, the devastator').
card_uid('numot, the devastator'/'PLC', 'PLC:Numot, the Devastator:numot, the devastator').
card_rarity('numot, the devastator'/'PLC', 'Rare').
card_artist('numot, the devastator'/'PLC', 'Dan Dos Santos').
card_number('numot, the devastator'/'PLC', '160').
card_multiverse_id('numot, the devastator'/'PLC', '124062').

card_in_set('oros, the avenger', 'PLC').
card_original_type('oros, the avenger'/'PLC', 'Legendary Creature — Dragon').
card_original_text('oros, the avenger'/'PLC', 'Flying\nWhenever Oros, the Avenger deals combat damage to a player, you may pay {2}{W}. If you do, Oros deals 3 damage to each nonwhite creature.').
card_image_name('oros, the avenger'/'PLC', 'oros, the avenger').
card_uid('oros, the avenger'/'PLC', 'PLC:Oros, the Avenger:oros, the avenger').
card_rarity('oros, the avenger'/'PLC', 'Rare').
card_artist('oros, the avenger'/'PLC', 'Daren Bader').
card_number('oros, the avenger'/'PLC', '161').
card_multiverse_id('oros, the avenger'/'PLC', '136170').

card_in_set('ovinize', 'PLC').
card_original_type('ovinize'/'PLC', 'Instant').
card_original_text('ovinize'/'PLC', 'Target creature loses all abilities and becomes 0/1 until end of turn.').
card_first_print('ovinize', 'PLC').
card_image_name('ovinize'/'PLC', 'ovinize').
card_uid('ovinize'/'PLC', 'PLC:Ovinize:ovinize').
card_rarity('ovinize'/'PLC', 'Uncommon').
card_artist('ovinize'/'PLC', 'Terese Nielsen').
card_number('ovinize'/'PLC', '57').
card_flavor_text('ovinize'/'PLC', '\"You wish for me to cow your enemies? I can do better than that . . . .\"\n—Teferi, second-level student').
card_multiverse_id('ovinize'/'PLC', '126212').
card_timeshifted('ovinize'/'PLC')
.
card_in_set('pallid mycoderm', 'PLC').
card_original_type('pallid mycoderm'/'PLC', 'Creature — Fungus').
card_original_text('pallid mycoderm'/'PLC', 'At the beginning of your upkeep, put a spore counter on Pallid Mycoderm.\nRemove three spore counters from Pallid Mycoderm: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Each Fungus and each Saproling you control gets +1/+1 until end of turn.').
card_first_print('pallid mycoderm', 'PLC').
card_image_name('pallid mycoderm'/'PLC', 'pallid mycoderm').
card_uid('pallid mycoderm'/'PLC', 'PLC:Pallid Mycoderm:pallid mycoderm').
card_rarity('pallid mycoderm'/'PLC', 'Common').
card_artist('pallid mycoderm'/'PLC', 'Jim Nelson').
card_number('pallid mycoderm'/'PLC', '10').
card_multiverse_id('pallid mycoderm'/'PLC', '124508').

card_in_set('phantasmagorian', 'PLC').
card_original_type('phantasmagorian'/'PLC', 'Creature — Horror').
card_original_text('phantasmagorian'/'PLC', 'When you play Phantasmagorian, any player may discard three cards. If a player does, counter Phantasmagorian.\nDiscard three cards: Return Phantasmagorian from your graveyard to your hand.').
card_first_print('phantasmagorian', 'PLC').
card_image_name('phantasmagorian'/'PLC', 'phantasmagorian').
card_uid('phantasmagorian'/'PLC', 'PLC:Phantasmagorian:phantasmagorian').
card_rarity('phantasmagorian'/'PLC', 'Uncommon').
card_artist('phantasmagorian'/'PLC', 'Steve Ellis').
card_number('phantasmagorian'/'PLC', '77').
card_multiverse_id('phantasmagorian'/'PLC', '124472').

card_in_set('piracy charm', 'PLC').
card_original_type('piracy charm'/'PLC', 'Instant').
card_original_text('piracy charm'/'PLC', 'Choose one Target creature gains islandwalk until end of turn; or target creature gets +2/-1 until end of turn; or target player discards a card.').
card_first_print('piracy charm', 'PLC').
card_image_name('piracy charm'/'PLC', 'piracy charm').
card_uid('piracy charm'/'PLC', 'PLC:Piracy Charm:piracy charm').
card_rarity('piracy charm'/'PLC', 'Common').
card_artist('piracy charm'/'PLC', 'John Avon').
card_number('piracy charm'/'PLC', '58').
card_multiverse_id('piracy charm'/'PLC', '124066').
card_timeshifted('piracy charm'/'PLC')
.
card_in_set('pongify', 'PLC').
card_original_type('pongify'/'PLC', 'Instant').
card_original_text('pongify'/'PLC', 'Destroy target creature. It can\'t be regenerated. That creature\'s controller puts a 3/3 green Ape creature token into play.').
card_first_print('pongify', 'PLC').
card_image_name('pongify'/'PLC', 'pongify').
card_uid('pongify'/'PLC', 'PLC:Pongify:pongify').
card_rarity('pongify'/'PLC', 'Uncommon').
card_artist('pongify'/'PLC', 'Heather Hudson').
card_number('pongify'/'PLC', '44').
card_flavor_text('pongify'/'PLC', 'Some spellcrafting mistakes go on to become spells of their own.').
card_multiverse_id('pongify'/'PLC', '129015').

card_in_set('porphyry nodes', 'PLC').
card_original_type('porphyry nodes'/'PLC', 'Enchantment').
card_original_text('porphyry nodes'/'PLC', 'At the beginning of your upkeep, destroy the creature with the least power. It can\'t be regenerated. If two or more creatures are tied for least power, you choose one of them.\nWhen there are no creatures in play, sacrifice Porphyry Nodes.').
card_first_print('porphyry nodes', 'PLC').
card_image_name('porphyry nodes'/'PLC', 'porphyry nodes').
card_uid('porphyry nodes'/'PLC', 'PLC:Porphyry Nodes:porphyry nodes').
card_rarity('porphyry nodes'/'PLC', 'Rare').
card_artist('porphyry nodes'/'PLC', 'Alan Pollack').
card_number('porphyry nodes'/'PLC', '28').
card_multiverse_id('porphyry nodes'/'PLC', '124470').
card_timeshifted('porphyry nodes'/'PLC')
.
card_in_set('poultice sliver', 'PLC').
card_original_type('poultice sliver'/'PLC', 'Creature — Sliver').
card_original_text('poultice sliver'/'PLC', 'All Slivers have \"{2}, {T}: Regenerate target Sliver.\"').
card_first_print('poultice sliver', 'PLC').
card_image_name('poultice sliver'/'PLC', 'poultice sliver').
card_uid('poultice sliver'/'PLC', 'PLC:Poultice Sliver:poultice sliver').
card_rarity('poultice sliver'/'PLC', 'Common').
card_artist('poultice sliver'/'PLC', 'Randy Gallegos').
card_number('poultice sliver'/'PLC', '11').
card_flavor_text('poultice sliver'/'PLC', '\"Its broad claw suggests a chitinous shield, but in fact it conceals glands that secrete a remarkably swift healing agent.\"\n—Rukarumel, field journal').
card_multiverse_id('poultice sliver'/'PLC', '126014').

card_in_set('pouncing wurm', 'PLC').
card_original_type('pouncing wurm'/'PLC', 'Creature — Wurm').
card_original_text('pouncing wurm'/'PLC', 'Kicker {2}{G} (You may pay an additional {2}{G} as you play this spell.)\nIf the kicker cost was paid, Pouncing Wurm comes into play with three +1/+1 counters on it and with haste.').
card_first_print('pouncing wurm', 'PLC').
card_image_name('pouncing wurm'/'PLC', 'pouncing wurm').
card_uid('pouncing wurm'/'PLC', 'PLC:Pouncing Wurm:pouncing wurm').
card_rarity('pouncing wurm'/'PLC', 'Uncommon').
card_artist('pouncing wurm'/'PLC', 'William O\'Connor').
card_number('pouncing wurm'/'PLC', '136').
card_multiverse_id('pouncing wurm'/'PLC', '122323').

card_in_set('primal plasma', 'PLC').
card_original_type('primal plasma'/'PLC', 'Creature — Elemental Shapeshifter').
card_original_text('primal plasma'/'PLC', 'As Primal Plasma comes into play, it becomes your choice of a 3/3 creature, a 2/2 creature with flying, or a 1/6 creature with defender.').
card_first_print('primal plasma', 'PLC').
card_image_name('primal plasma'/'PLC', 'primal plasma').
card_uid('primal plasma'/'PLC', 'PLC:Primal Plasma:primal plasma').
card_rarity('primal plasma'/'PLC', 'Common').
card_artist('primal plasma'/'PLC', 'Luca Zontini').
card_number('primal plasma'/'PLC', '59').
card_flavor_text('primal plasma'/'PLC', 'Tocasia brushed the gears and cogs from the table. There, before two wide-eyed brothers, she began a lesson on raw elemental magic.').
card_multiverse_id('primal plasma'/'PLC', '124757').
card_timeshifted('primal plasma'/'PLC')
.
card_in_set('prodigal pyromancer', 'PLC').
card_original_type('prodigal pyromancer'/'PLC', 'Creature — Human Wizard').
card_original_text('prodigal pyromancer'/'PLC', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_first_print('prodigal pyromancer', 'PLC').
card_image_name('prodigal pyromancer'/'PLC', 'prodigal pyromancer').
card_uid('prodigal pyromancer'/'PLC', 'PLC:Prodigal Pyromancer:prodigal pyromancer').
card_rarity('prodigal pyromancer'/'PLC', 'Common').
card_artist('prodigal pyromancer'/'PLC', 'Jeremy Jarvis').
card_number('prodigal pyromancer'/'PLC', '118').
card_flavor_text('prodigal pyromancer'/'PLC', '\"Learn to burn!\"\n—Institute of Arcane Studies motto').
card_multiverse_id('prodigal pyromancer'/'PLC', '122338').
card_timeshifted('prodigal pyromancer'/'PLC')
.
card_in_set('psychotrope thallid', 'PLC').
card_original_type('psychotrope thallid'/'PLC', 'Creature — Fungus').
card_original_text('psychotrope thallid'/'PLC', 'At the beginning of your upkeep, put a spore counter on Psychotrope Thallid.\nRemove three spore counters from Psychotrope Thallid: Put a 1/1 green Saproling creature token into play.\n{1}, Sacrifice a Saproling: Draw a card.').
card_first_print('psychotrope thallid', 'PLC').
card_image_name('psychotrope thallid'/'PLC', 'psychotrope thallid').
card_uid('psychotrope thallid'/'PLC', 'PLC:Psychotrope Thallid:psychotrope thallid').
card_rarity('psychotrope thallid'/'PLC', 'Uncommon').
card_artist('psychotrope thallid'/'PLC', 'Dave Kendall').
card_number('psychotrope thallid'/'PLC', '137').
card_multiverse_id('psychotrope thallid'/'PLC', '128944').

card_in_set('pyrohemia', 'PLC').
card_original_type('pyrohemia'/'PLC', 'Enchantment').
card_original_text('pyrohemia'/'PLC', 'At end of turn, if no creatures are in play, sacrifice Pyrohemia.\n{R}: Pyrohemia deals 1 damage to each creature and each player.').
card_first_print('pyrohemia', 'PLC').
card_image_name('pyrohemia'/'PLC', 'pyrohemia').
card_uid('pyrohemia'/'PLC', 'PLC:Pyrohemia:pyrohemia').
card_rarity('pyrohemia'/'PLC', 'Uncommon').
card_artist('pyrohemia'/'PLC', 'Stephen Tappin').
card_number('pyrohemia'/'PLC', '119').
card_multiverse_id('pyrohemia'/'PLC', '122436').
card_timeshifted('pyrohemia'/'PLC')
.
card_in_set('radha, heir to keld', 'PLC').
card_original_type('radha, heir to keld'/'PLC', 'Legendary Creature — Elf Warrior').
card_original_text('radha, heir to keld'/'PLC', 'Whenever Radha, Heir to Keld attacks, you may add {R}{R} to your mana pool.\n{T}: Add {G} to your mana pool.').
card_first_print('radha, heir to keld', 'PLC').
card_image_name('radha, heir to keld'/'PLC', 'radha, heir to keld').
card_uid('radha, heir to keld'/'PLC', 'PLC:Radha, Heir to Keld:radha, heir to keld').
card_rarity('radha, heir to keld'/'PLC', 'Rare').
card_artist('radha, heir to keld'/'PLC', 'Jim Murray').
card_number('radha, heir to keld'/'PLC', '162').
card_flavor_text('radha, heir to keld'/'PLC', '\"Run home, cur. I\'ve already taken your master\'s head. Don\'t make me thrash you with it.\"').
card_multiverse_id('radha, heir to keld'/'PLC', '128949').

card_in_set('rathi trapper', 'PLC').
card_original_type('rathi trapper'/'PLC', 'Creature — Human Rebel Rogue').
card_original_text('rathi trapper'/'PLC', '{B}, {T}: Tap target creature.').
card_first_print('rathi trapper', 'PLC').
card_image_name('rathi trapper'/'PLC', 'rathi trapper').
card_uid('rathi trapper'/'PLC', 'PLC:Rathi Trapper:rathi trapper').
card_rarity('rathi trapper'/'PLC', 'Common').
card_artist('rathi trapper'/'PLC', 'Pete Venters').
card_number('rathi trapper'/'PLC', '90').
card_flavor_text('rathi trapper'/'PLC', 'Tangling vines, fetid murk, paralyzing poisons, and crawling dead. The swamp is nature\'s trap waiting to be exploited by unnatural minds.').
card_multiverse_id('rathi trapper'/'PLC', '130718').
card_timeshifted('rathi trapper'/'PLC')
.
card_in_set('reality acid', 'PLC').
card_original_type('reality acid'/'PLC', 'Enchantment — Aura').
card_original_text('reality acid'/'PLC', 'Enchant permanent\nVanishing 3 (This permanent comes into play with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Reality Acid leaves play, enchanted permanent\'s controller sacrifices it.').
card_first_print('reality acid', 'PLC').
card_image_name('reality acid'/'PLC', 'reality acid').
card_uid('reality acid'/'PLC', 'PLC:Reality Acid:reality acid').
card_rarity('reality acid'/'PLC', 'Common').
card_artist('reality acid'/'PLC', 'James Wong').
card_number('reality acid'/'PLC', '45').
card_multiverse_id('reality acid'/'PLC', '125880').

card_in_set('rebuff the wicked', 'PLC').
card_original_type('rebuff the wicked'/'PLC', 'Instant').
card_original_text('rebuff the wicked'/'PLC', 'Counter target spell that targets a permanent you control.').
card_first_print('rebuff the wicked', 'PLC').
card_image_name('rebuff the wicked'/'PLC', 'rebuff the wicked').
card_uid('rebuff the wicked'/'PLC', 'PLC:Rebuff the Wicked:rebuff the wicked').
card_rarity('rebuff the wicked'/'PLC', 'Uncommon').
card_artist('rebuff the wicked'/'PLC', 'Stephen Tappin').
card_number('rebuff the wicked'/'PLC', '12').
card_flavor_text('rebuff the wicked'/'PLC', '\"I cannot teach you their brand of magic, but I can teach you how to defend against it.\"\n—Tavalus, priest of Korlis').
card_multiverse_id('rebuff the wicked'/'PLC', '122287').

card_in_set('reckless wurm', 'PLC').
card_original_type('reckless wurm'/'PLC', 'Creature — Wurm').
card_original_text('reckless wurm'/'PLC', 'Trample\nMadness {2}{R} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_image_name('reckless wurm'/'PLC', 'reckless wurm').
card_uid('reckless wurm'/'PLC', 'PLC:Reckless Wurm:reckless wurm').
card_rarity('reckless wurm'/'PLC', 'Uncommon').
card_artist('reckless wurm'/'PLC', 'Greg Staples').
card_number('reckless wurm'/'PLC', '120').
card_flavor_text('reckless wurm'/'PLC', 'Bred for battle in the Grand Coliseum, these wurms annihilated whole ecosystems when released into the wild.').
card_multiverse_id('reckless wurm'/'PLC', '126818').
card_timeshifted('reckless wurm'/'PLC')
.
card_in_set('reflex sliver', 'PLC').
card_original_type('reflex sliver'/'PLC', 'Creature — Sliver').
card_original_text('reflex sliver'/'PLC', 'All Slivers have haste.').
card_first_print('reflex sliver', 'PLC').
card_image_name('reflex sliver'/'PLC', 'reflex sliver').
card_uid('reflex sliver'/'PLC', 'PLC:Reflex Sliver:reflex sliver').
card_rarity('reflex sliver'/'PLC', 'Common').
card_artist('reflex sliver'/'PLC', 'Luca Zontini').
card_number('reflex sliver'/'PLC', '138').
card_flavor_text('reflex sliver'/'PLC', '\"This sliver comes into the world a perfect predator. It\'s ready to hunt and devour its first meal within seconds of hatching.\"\n—Rukarumel, field journal').
card_multiverse_id('reflex sliver'/'PLC', '126018').

card_in_set('retether', 'PLC').
card_original_type('retether'/'PLC', 'Sorcery').
card_original_text('retether'/'PLC', 'Return each Aura card from your graveyard to play. Only creatures can be enchanted this way. (Aura cards that can\'t enchant a creature in play remain in your graveyard.)').
card_first_print('retether', 'PLC').
card_image_name('retether'/'PLC', 'retether').
card_uid('retether'/'PLC', 'PLC:Retether:retether').
card_rarity('retether'/'PLC', 'Rare').
card_artist('retether'/'PLC', 'Dan Scott').
card_number('retether'/'PLC', '13').
card_flavor_text('retether'/'PLC', 'The rifts reach into infinite time streams, bringing the divergent products of alternate pasts into the present.').
card_multiverse_id('retether'/'PLC', '131007').

card_in_set('revered dead', 'PLC').
card_original_type('revered dead'/'PLC', 'Creature — Spirit Soldier').
card_original_text('revered dead'/'PLC', '{W}: Regenerate Revered Dead.').
card_first_print('revered dead', 'PLC').
card_image_name('revered dead'/'PLC', 'revered dead').
card_uid('revered dead'/'PLC', 'PLC:Revered Dead:revered dead').
card_rarity('revered dead'/'PLC', 'Common').
card_artist('revered dead'/'PLC', 'Ron Spears').
card_number('revered dead'/'PLC', '29').
card_flavor_text('revered dead'/'PLC', '\"The mists coalesced into silent warriors. We charged them and broke through their lines, only to see them swirl and re-form behind us.\"\n—Golas Mahr, black knight').
card_multiverse_id('revered dead'/'PLC', '122282').
card_timeshifted('revered dead'/'PLC')
.
card_in_set('ridged kusite', 'PLC').
card_original_type('ridged kusite'/'PLC', 'Creature — Horror Spellshaper').
card_original_text('ridged kusite'/'PLC', '{1}{B}, {T}, Discard a card: Target creature gets +1/+0 and gains first strike until end of turn.').
card_first_print('ridged kusite', 'PLC').
card_image_name('ridged kusite'/'PLC', 'ridged kusite').
card_uid('ridged kusite'/'PLC', 'PLC:Ridged Kusite:ridged kusite').
card_rarity('ridged kusite'/'PLC', 'Common').
card_artist('ridged kusite'/'PLC', 'rk post').
card_number('ridged kusite'/'PLC', '78').
card_flavor_text('ridged kusite'/'PLC', '\"It offers but a taste of the power that the shadows have to offer. But even that is a heady wine indeed.\"\n—Ratadrabik of Urborg').
card_multiverse_id('ridged kusite'/'PLC', '130717').

card_in_set('riftmarked knight', 'PLC').
card_original_type('riftmarked knight'/'PLC', 'Creature — Human Rebel Knight').
card_original_text('riftmarked knight'/'PLC', 'Flanking, protection from black\nSuspend 3—{1}{W}{W}\nWhen the last time counter is removed from Riftmarked Knight while it\'s removed from the game, put a 2/2 black Knight creature token with flanking, protection from white, and haste into play.').
card_first_print('riftmarked knight', 'PLC').
card_image_name('riftmarked knight'/'PLC', 'riftmarked knight').
card_uid('riftmarked knight'/'PLC', 'PLC:Riftmarked Knight:riftmarked knight').
card_rarity('riftmarked knight'/'PLC', 'Uncommon').
card_artist('riftmarked knight'/'PLC', 'William O\'Connor').
card_number('riftmarked knight'/'PLC', '14').
card_multiverse_id('riftmarked knight'/'PLC', '126816').

card_in_set('riptide pilferer', 'PLC').
card_original_type('riptide pilferer'/'PLC', 'Creature — Merfolk Rogue').
card_original_text('riptide pilferer'/'PLC', 'Whenever Riptide Pilferer deals combat damage to a player, that player discards a card.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('riptide pilferer', 'PLC').
card_image_name('riptide pilferer'/'PLC', 'riptide pilferer').
card_uid('riptide pilferer'/'PLC', 'PLC:Riptide Pilferer:riptide pilferer').
card_rarity('riptide pilferer'/'PLC', 'Uncommon').
card_artist('riptide pilferer'/'PLC', 'Steve Prescott').
card_number('riptide pilferer'/'PLC', '60').
card_multiverse_id('riptide pilferer'/'PLC', '122325').
card_timeshifted('riptide pilferer'/'PLC')
.
card_in_set('roiling horror', 'PLC').
card_original_type('roiling horror'/'PLC', 'Creature — Horror').
card_original_text('roiling horror'/'PLC', 'Roiling Horror\'s power and toughness are each equal to your life total minus the life total of an opponent with the most life.\nSuspend X—{X}{B}{B}{B}. X can\'t be 0.\nWhenever a time counter is removed from Roiling Horror while it\'s removed from the game, target player loses 1 life and you gain 1 life.').
card_first_print('roiling horror', 'PLC').
card_image_name('roiling horror'/'PLC', 'roiling horror').
card_uid('roiling horror'/'PLC', 'PLC:Roiling Horror:roiling horror').
card_rarity('roiling horror'/'PLC', 'Rare').
card_artist('roiling horror'/'PLC', 'John Avon').
card_number('roiling horror'/'PLC', '79').
card_multiverse_id('roiling horror'/'PLC', '122431').

card_in_set('rough', 'PLC').
card_original_type('rough'/'PLC', 'Sorcery').
card_original_text('rough'/'PLC', 'Rough deals 2 damage to each creature without flying.\n//\nTumble\n{5}{R}\nSorcery\nTumble deals 6 damage to each creature with flying.').
card_first_print('rough', 'PLC').
card_image_name('rough'/'PLC', 'roughtumble').
card_uid('rough'/'PLC', 'PLC:Rough:roughtumble').
card_rarity('rough'/'PLC', 'Uncommon').
card_artist('rough'/'PLC', 'Luca Zontini').
card_number('rough'/'PLC', '114a').
card_multiverse_id('rough'/'PLC', '126420').

card_in_set('saltblast', 'PLC').
card_original_type('saltblast'/'PLC', 'Sorcery').
card_original_text('saltblast'/'PLC', 'Destroy target nonwhite permanent.').
card_first_print('saltblast', 'PLC').
card_image_name('saltblast'/'PLC', 'saltblast').
card_uid('saltblast'/'PLC', 'PLC:Saltblast:saltblast').
card_rarity('saltblast'/'PLC', 'Uncommon').
card_artist('saltblast'/'PLC', 'Paolo Parente').
card_number('saltblast'/'PLC', '15').
card_flavor_text('saltblast'/'PLC', 'Dominaria erodes with each passing gust.').
card_multiverse_id('saltblast'/'PLC', '122435').

card_in_set('saltfield recluse', 'PLC').
card_original_type('saltfield recluse'/'PLC', 'Creature — Human Rebel Cleric').
card_original_text('saltfield recluse'/'PLC', '{T}: Target creature gets -2/-0 until end of turn.').
card_first_print('saltfield recluse', 'PLC').
card_image_name('saltfield recluse'/'PLC', 'saltfield recluse').
card_uid('saltfield recluse'/'PLC', 'PLC:Saltfield Recluse:saltfield recluse').
card_rarity('saltfield recluse'/'PLC', 'Common').
card_artist('saltfield recluse'/'PLC', 'Brian Despain').
card_number('saltfield recluse'/'PLC', '16').
card_flavor_text('saltfield recluse'/'PLC', 'He remembers a past of light and healing. But he lives the bitter present—parching salt, scouring wind, and the withering heat of the desert.').
card_multiverse_id('saltfield recluse'/'PLC', '124037').

card_in_set('seal of primordium', 'PLC').
card_original_type('seal of primordium'/'PLC', 'Enchantment').
card_original_text('seal of primordium'/'PLC', 'Sacrifice Seal of Primordium: Destroy target artifact or enchantment.').
card_first_print('seal of primordium', 'PLC').
card_image_name('seal of primordium'/'PLC', 'seal of primordium').
card_uid('seal of primordium'/'PLC', 'PLC:Seal of Primordium:seal of primordium').
card_rarity('seal of primordium'/'PLC', 'Common').
card_artist('seal of primordium'/'PLC', 'Christopher Moeller').
card_number('seal of primordium'/'PLC', '153').
card_flavor_text('seal of primordium'/'PLC', '\"I am the simplifier, the root that drags all artifice to earth.\"\n—Seal inscription').
card_multiverse_id('seal of primordium'/'PLC', '130816').
card_timeshifted('seal of primordium'/'PLC')
.
card_in_set('serendib sorcerer', 'PLC').
card_original_type('serendib sorcerer'/'PLC', 'Creature — Human Wizard').
card_original_text('serendib sorcerer'/'PLC', '{T}: Target creature other than Serendib Sorcerer becomes 0/2 until end of turn.').
card_first_print('serendib sorcerer', 'PLC').
card_image_name('serendib sorcerer'/'PLC', 'serendib sorcerer').
card_uid('serendib sorcerer'/'PLC', 'PLC:Serendib Sorcerer:serendib sorcerer').
card_rarity('serendib sorcerer'/'PLC', 'Rare').
card_artist('serendib sorcerer'/'PLC', 'Dan Scott').
card_number('serendib sorcerer'/'PLC', '61').
card_flavor_text('serendib sorcerer'/'PLC', '\"Even a dragon is humble as a kitten if it thinks itself a slave.\"').
card_multiverse_id('serendib sorcerer'/'PLC', '122442').
card_timeshifted('serendib sorcerer'/'PLC')
.
card_in_set('serra sphinx', 'PLC').
card_original_type('serra sphinx'/'PLC', 'Creature — Sphinx').
card_original_text('serra sphinx'/'PLC', 'Flying, vigilance').
card_first_print('serra sphinx', 'PLC').
card_image_name('serra sphinx'/'PLC', 'serra sphinx').
card_uid('serra sphinx'/'PLC', 'PLC:Serra Sphinx:serra sphinx').
card_rarity('serra sphinx'/'PLC', 'Rare').
card_artist('serra sphinx'/'PLC', 'Daren Bader').
card_number('serra sphinx'/'PLC', '62').
card_flavor_text('serra sphinx'/'PLC', 'Sphinxes drink from the mystic meres of Serra\'s realm, where their keen eyes watch reflections of what is and what is yet to come.').
card_multiverse_id('serra sphinx'/'PLC', '125873').
card_timeshifted('serra sphinx'/'PLC')
.
card_in_set('serra\'s boon', 'PLC').
card_original_type('serra\'s boon'/'PLC', 'Enchantment — Aura').
card_original_text('serra\'s boon'/'PLC', 'Enchant creature\nEnchanted creature gets +1/+2 as long as it\'s white. Otherwise, it gets -2/-1.').
card_first_print('serra\'s boon', 'PLC').
card_image_name('serra\'s boon'/'PLC', 'serra\'s boon').
card_uid('serra\'s boon'/'PLC', 'PLC:Serra\'s Boon:serra\'s boon').
card_rarity('serra\'s boon'/'PLC', 'Uncommon').
card_artist('serra\'s boon'/'PLC', 'Steven Belledin').
card_number('serra\'s boon'/'PLC', '17').
card_flavor_text('serra\'s boon'/'PLC', '\"The light of an angel\'s glance is warm, but her fixed stare blinds and burns.\"\n—Calexis, deacon of the New Order of Serra').
card_multiverse_id('serra\'s boon'/'PLC', '129014').

card_in_set('shade of trokair', 'PLC').
card_original_type('shade of trokair'/'PLC', 'Creature — Shade').
card_original_text('shade of trokair'/'PLC', '{W}: Shade of Trokair gets +1/+1 until end of turn.\nSuspend 3—{W} (Rather than play this card from your hand, you may pay {W} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)').
card_first_print('shade of trokair', 'PLC').
card_image_name('shade of trokair'/'PLC', 'shade of trokair').
card_uid('shade of trokair'/'PLC', 'PLC:Shade of Trokair:shade of trokair').
card_rarity('shade of trokair'/'PLC', 'Common').
card_artist('shade of trokair'/'PLC', 'William O\'Connor').
card_number('shade of trokair'/'PLC', '18').
card_multiverse_id('shade of trokair'/'PLC', '126408').

card_in_set('shaper parasite', 'PLC').
card_original_type('shaper parasite'/'PLC', 'Creature — Illusion').
card_original_text('shaper parasite'/'PLC', 'Morph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Shaper Parasite is turned face up, target creature gets +2/-2 or -2/+2 until end of turn.').
card_first_print('shaper parasite', 'PLC').
card_image_name('shaper parasite'/'PLC', 'shaper parasite').
card_uid('shaper parasite'/'PLC', 'PLC:Shaper Parasite:shaper parasite').
card_rarity('shaper parasite'/'PLC', 'Common').
card_artist('shaper parasite'/'PLC', 'rk post').
card_number('shaper parasite'/'PLC', '46').
card_multiverse_id('shaper parasite'/'PLC', '122375').

card_in_set('shivan meteor', 'PLC').
card_original_type('shivan meteor'/'PLC', 'Sorcery').
card_original_text('shivan meteor'/'PLC', 'Shivan Meteor deals 13 damage to target creature.\nSuspend 2—{1}{R}{R} (Rather than play this card from your hand, you may pay {1}{R}{R} and remove it from the game with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)').
card_first_print('shivan meteor', 'PLC').
card_image_name('shivan meteor'/'PLC', 'shivan meteor').
card_uid('shivan meteor'/'PLC', 'PLC:Shivan Meteor:shivan meteor').
card_rarity('shivan meteor'/'PLC', 'Uncommon').
card_artist('shivan meteor'/'PLC', 'Chippy').
card_number('shivan meteor'/'PLC', '106').
card_multiverse_id('shivan meteor'/'PLC', '134740').

card_in_set('shivan wumpus', 'PLC').
card_original_type('shivan wumpus'/'PLC', 'Creature — Beast').
card_original_text('shivan wumpus'/'PLC', 'Trample\nWhen Shivan Wumpus comes into play, any player may sacrifice a land. If a player does, put Shivan Wumpus on top of its owner\'s library.').
card_first_print('shivan wumpus', 'PLC').
card_image_name('shivan wumpus'/'PLC', 'shivan wumpus').
card_uid('shivan wumpus'/'PLC', 'PLC:Shivan Wumpus:shivan wumpus').
card_rarity('shivan wumpus'/'PLC', 'Rare').
card_artist('shivan wumpus'/'PLC', 'Ron Spears').
card_number('shivan wumpus'/'PLC', '121').
card_flavor_text('shivan wumpus'/'PLC', 'It\'s easier to move the village than to divert the wumpus.').
card_multiverse_id('shivan wumpus'/'PLC', '124445').
card_timeshifted('shivan wumpus'/'PLC')
.
card_in_set('shrouded lore', 'PLC').
card_original_type('shrouded lore'/'PLC', 'Sorcery').
card_original_text('shrouded lore'/'PLC', 'Target opponent chooses a card in your graveyard. You may pay {B}. If you do, repeat this process except that opponent can\'t choose a card already chosen for Shrouded Lore. Then put the last chosen card into your hand.').
card_first_print('shrouded lore', 'PLC').
card_image_name('shrouded lore'/'PLC', 'shrouded lore').
card_uid('shrouded lore'/'PLC', 'PLC:Shrouded Lore:shrouded lore').
card_rarity('shrouded lore'/'PLC', 'Uncommon').
card_artist('shrouded lore'/'PLC', 'Kev Walker').
card_number('shrouded lore'/'PLC', '91').
card_multiverse_id('shrouded lore'/'PLC', '122285').
card_timeshifted('shrouded lore'/'PLC')
.
card_in_set('simian spirit guide', 'PLC').
card_original_type('simian spirit guide'/'PLC', 'Creature — Ape Spirit').
card_original_text('simian spirit guide'/'PLC', 'Remove Simian Spirit Guide in your hand from the game: Add {R} to your mana pool.').
card_first_print('simian spirit guide', 'PLC').
card_image_name('simian spirit guide'/'PLC', 'simian spirit guide').
card_uid('simian spirit guide'/'PLC', 'PLC:Simian Spirit Guide:simian spirit guide').
card_rarity('simian spirit guide'/'PLC', 'Common').
card_artist('simian spirit guide'/'PLC', 'Dave DeVries').
card_number('simian spirit guide'/'PLC', '122').
card_flavor_text('simian spirit guide'/'PLC', '\"All my spells smell like burnt hair lately.\"\n—Jaya Ballard, task mage').
card_multiverse_id('simian spirit guide'/'PLC', '124474').
card_timeshifted('simian spirit guide'/'PLC')
.
card_in_set('sinew sliver', 'PLC').
card_original_type('sinew sliver'/'PLC', 'Creature — Sliver').
card_original_text('sinew sliver'/'PLC', 'All Slivers get +1/+1.').
card_first_print('sinew sliver', 'PLC').
card_image_name('sinew sliver'/'PLC', 'sinew sliver').
card_uid('sinew sliver'/'PLC', 'PLC:Sinew Sliver:sinew sliver').
card_rarity('sinew sliver'/'PLC', 'Common').
card_artist('sinew sliver'/'PLC', 'Steven Belledin').
card_number('sinew sliver'/'PLC', '30').
card_flavor_text('sinew sliver'/'PLC', 'As the muscle cords of the creature twitched, Hanna saw an unsettling unanimity in the others\' rippling flesh. She didn\'t know what it meant, but she urged Sisay to keep the ship at a safe distance.').
card_multiverse_id('sinew sliver'/'PLC', '125879').
card_timeshifted('sinew sliver'/'PLC')
.
card_in_set('skirk shaman', 'PLC').
card_original_type('skirk shaman'/'PLC', 'Creature — Goblin Shaman').
card_original_text('skirk shaman'/'PLC', 'Skirk Shaman can\'t be blocked except by artifact creatures and/or red creatures.').
card_first_print('skirk shaman', 'PLC').
card_image_name('skirk shaman'/'PLC', 'skirk shaman').
card_uid('skirk shaman'/'PLC', 'PLC:Skirk Shaman:skirk shaman').
card_rarity('skirk shaman'/'PLC', 'Common').
card_artist('skirk shaman'/'PLC', 'Chippy').
card_number('skirk shaman'/'PLC', '123').
card_flavor_text('skirk shaman'/'PLC', 'In a shaman\'s grimy hands, a concoction of dried Skirk Ridge herbs can become the face of panic.').
card_multiverse_id('skirk shaman'/'PLC', '131011').
card_timeshifted('skirk shaman'/'PLC')
.
card_in_set('sophic centaur', 'PLC').
card_original_type('sophic centaur'/'PLC', 'Creature — Centaur Spellshaper').
card_original_text('sophic centaur'/'PLC', '{2}{G}{G}, {T}, Discard a card: You gain 2 life for each card in your hand.').
card_first_print('sophic centaur', 'PLC').
card_image_name('sophic centaur'/'PLC', 'sophic centaur').
card_uid('sophic centaur'/'PLC', 'PLC:Sophic Centaur:sophic centaur').
card_rarity('sophic centaur'/'PLC', 'Uncommon').
card_artist('sophic centaur'/'PLC', 'Dan Dos Santos').
card_number('sophic centaur'/'PLC', '139').
card_flavor_text('sophic centaur'/'PLC', 'He endures the pain of remembering Krosa as it once was and hopes that a future generation will have need of his memories.').
card_multiverse_id('sophic centaur'/'PLC', '124505').

card_in_set('spellshift', 'PLC').
card_original_type('spellshift'/'PLC', 'Instant').
card_original_text('spellshift'/'PLC', 'Counter target instant or sorcery spell. Its controller reveals cards from the top of his or her library until he or she reveals an instant or sorcery card. That player may play that card without paying its mana cost. Then he or she shuffles his or her library.').
card_first_print('spellshift', 'PLC').
card_image_name('spellshift'/'PLC', 'spellshift').
card_uid('spellshift'/'PLC', 'PLC:Spellshift:spellshift').
card_rarity('spellshift'/'PLC', 'Rare').
card_artist('spellshift'/'PLC', 'Stephen Tappin').
card_number('spellshift'/'PLC', '47').
card_multiverse_id('spellshift'/'PLC', '122459').

card_in_set('spitting sliver', 'PLC').
card_original_type('spitting sliver'/'PLC', 'Creature — Sliver').
card_original_text('spitting sliver'/'PLC', 'All Slivers have first strike.').
card_first_print('spitting sliver', 'PLC').
card_image_name('spitting sliver'/'PLC', 'spitting sliver').
card_uid('spitting sliver'/'PLC', 'PLC:Spitting Sliver:spitting sliver').
card_rarity('spitting sliver'/'PLC', 'Common').
card_artist('spitting sliver'/'PLC', 'Steve Ellis').
card_number('spitting sliver'/'PLC', '80').
card_flavor_text('spitting sliver'/'PLC', '\"Our lances\' reach had been our saving grace in our fight against the hive. Now even that advantage is taken from us.\"\n—Adom Capashen, Benalish knight').
card_multiverse_id('spitting sliver'/'PLC', '126022').

card_in_set('stingscourger', 'PLC').
card_original_type('stingscourger'/'PLC', 'Creature — Goblin Warrior').
card_original_text('stingscourger'/'PLC', 'Echo {3}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Stingscourger comes into play, return target creature an opponent controls to its owner\'s hand.').
card_first_print('stingscourger', 'PLC').
card_image_name('stingscourger'/'PLC', 'stingscourger').
card_uid('stingscourger'/'PLC', 'PLC:Stingscourger:stingscourger').
card_rarity('stingscourger'/'PLC', 'Common').
card_artist('stingscourger'/'PLC', 'Wayne Reynolds').
card_number('stingscourger'/'PLC', '107').
card_multiverse_id('stingscourger'/'PLC', '134744').

card_in_set('stonecloaker', 'PLC').
card_original_type('stonecloaker'/'PLC', 'Creature — Gargoyle').
card_original_text('stonecloaker'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nWhen Stonecloaker comes into play, return a creature you control to its owner\'s hand.\nWhen Stonecloaker comes into play, remove target card in a graveyard from the game.').
card_first_print('stonecloaker', 'PLC').
card_image_name('stonecloaker'/'PLC', 'stonecloaker').
card_uid('stonecloaker'/'PLC', 'PLC:Stonecloaker:stonecloaker').
card_rarity('stonecloaker'/'PLC', 'Uncommon').
card_artist('stonecloaker'/'PLC', 'Tomas Giorello').
card_number('stonecloaker'/'PLC', '19').
card_multiverse_id('stonecloaker'/'PLC', '122469').

card_in_set('stormfront riders', 'PLC').
card_original_type('stormfront riders'/'PLC', 'Creature — Human Soldier').
card_original_text('stormfront riders'/'PLC', 'Flying\nWhen Stormfront Riders comes into play, return two creatures you control to their owner\'s hand.\nWhenever Stormfront Riders or another creature is returned to your hand from play, put a 1/1 white Soldier creature token into play.').
card_first_print('stormfront riders', 'PLC').
card_image_name('stormfront riders'/'PLC', 'stormfront riders').
card_uid('stormfront riders'/'PLC', 'PLC:Stormfront Riders:stormfront riders').
card_rarity('stormfront riders'/'PLC', 'Uncommon').
card_artist('stormfront riders'/'PLC', 'Wayne Reynolds').
card_number('stormfront riders'/'PLC', '20').
card_multiverse_id('stormfront riders'/'PLC', '122412').

card_in_set('sulfur elemental', 'PLC').
card_original_type('sulfur elemental'/'PLC', 'Creature — Elemental').
card_original_text('sulfur elemental'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nSplit second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nWhite creatures get +1/-1.').
card_first_print('sulfur elemental', 'PLC').
card_image_name('sulfur elemental'/'PLC', 'sulfur elemental').
card_uid('sulfur elemental'/'PLC', 'PLC:Sulfur Elemental:sulfur elemental').
card_rarity('sulfur elemental'/'PLC', 'Uncommon').
card_artist('sulfur elemental'/'PLC', 'Dave Kendall').
card_number('sulfur elemental'/'PLC', '108').
card_multiverse_id('sulfur elemental'/'PLC', '122416').

card_in_set('sunlance', 'PLC').
card_original_type('sunlance'/'PLC', 'Sorcery').
card_original_text('sunlance'/'PLC', 'Sunlance deals 3 damage to target nonwhite creature.').
card_first_print('sunlance', 'PLC').
card_image_name('sunlance'/'PLC', 'sunlance').
card_uid('sunlance'/'PLC', 'PLC:Sunlance:sunlance').
card_rarity('sunlance'/'PLC', 'Common').
card_artist('sunlance'/'PLC', 'Volkan Baga').
card_number('sunlance'/'PLC', '31').
card_flavor_text('sunlance'/'PLC', '\"It\'s easy for the innocent to speak of justice. They seldom feel its terrible power.\"\n—Orim, Samite inquisitor').
card_multiverse_id('sunlance'/'PLC', '122355').
card_timeshifted('sunlance'/'PLC')
.
card_in_set('synchronous sliver', 'PLC').
card_original_type('synchronous sliver'/'PLC', 'Creature — Sliver').
card_original_text('synchronous sliver'/'PLC', 'All Slivers have vigilance.').
card_first_print('synchronous sliver', 'PLC').
card_image_name('synchronous sliver'/'PLC', 'synchronous sliver').
card_uid('synchronous sliver'/'PLC', 'PLC:Synchronous Sliver:synchronous sliver').
card_rarity('synchronous sliver'/'PLC', 'Common').
card_artist('synchronous sliver'/'PLC', 'E. M. Gist').
card_number('synchronous sliver'/'PLC', '48').
card_flavor_text('synchronous sliver'/'PLC', '\"With a twitch of its muscles, its timeline forks. Then, just as quickly, its two selves reintegrate. Causality, strangely, seems not to mind.\"\n—Rukarumel, field journal').
card_multiverse_id('synchronous sliver'/'PLC', '124044').

card_in_set('temporal extortion', 'PLC').
card_original_type('temporal extortion'/'PLC', 'Sorcery').
card_original_text('temporal extortion'/'PLC', 'When you play Temporal Extortion, any player may pay half his or her life, rounded up. If a player does, counter Temporal Extortion.\nTake an extra turn after this one.').
card_first_print('temporal extortion', 'PLC').
card_image_name('temporal extortion'/'PLC', 'temporal extortion').
card_uid('temporal extortion'/'PLC', 'PLC:Temporal Extortion:temporal extortion').
card_rarity('temporal extortion'/'PLC', 'Rare').
card_artist('temporal extortion'/'PLC', 'Steven Belledin').
card_number('temporal extortion'/'PLC', '81').
card_flavor_text('temporal extortion'/'PLC', '\"The scythe of time or my blade at your throat—the choice is yours.\"\n—Holux, stronghold racketeer').
card_multiverse_id('temporal extortion'/'PLC', '126814').

card_in_set('teneb, the harvester', 'PLC').
card_original_type('teneb, the harvester'/'PLC', 'Legendary Creature — Dragon').
card_original_text('teneb, the harvester'/'PLC', 'Flying\nWhenever Teneb, the Harvester deals combat damage to a player, you may pay {2}{B}. If you do, put target creature card in a graveyard into play under your control.').
card_first_print('teneb, the harvester', 'PLC').
card_image_name('teneb, the harvester'/'PLC', 'teneb, the harvester').
card_uid('teneb, the harvester'/'PLC', 'PLC:Teneb, the Harvester:teneb, the harvester').
card_rarity('teneb, the harvester'/'PLC', 'Rare').
card_artist('teneb, the harvester'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('teneb, the harvester'/'PLC', '163').
card_multiverse_id('teneb, the harvester'/'PLC', '124075').

card_in_set('tidewalker', 'PLC').
card_original_type('tidewalker'/'PLC', 'Creature — Elemental').
card_original_text('tidewalker'/'PLC', 'Tidewalker comes into play with a time counter on it for each Island you control.\nVanishing (At the beginning of your upkeep, remove a time counter from this permanent. When the last is removed, sacrifice it.)\nTidewalker\'s power and toughness are each equal to the number of time counters on it.').
card_first_print('tidewalker', 'PLC').
card_image_name('tidewalker'/'PLC', 'tidewalker').
card_uid('tidewalker'/'PLC', 'PLC:Tidewalker:tidewalker').
card_rarity('tidewalker'/'PLC', 'Uncommon').
card_artist('tidewalker'/'PLC', 'Dave Kendall').
card_number('tidewalker'/'PLC', '49').
card_multiverse_id('tidewalker'/'PLC', '130814').

card_in_set('timbermare', 'PLC').
card_original_type('timbermare'/'PLC', 'Creature — Elemental').
card_original_text('timbermare'/'PLC', 'Haste\nEcho {5}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Timbermare comes into play, tap all other creatures.').
card_first_print('timbermare', 'PLC').
card_image_name('timbermare'/'PLC', 'timbermare').
card_uid('timbermare'/'PLC', 'PLC:Timbermare:timbermare').
card_rarity('timbermare'/'PLC', 'Rare').
card_artist('timbermare'/'PLC', 'Dan Dos Santos').
card_number('timbermare'/'PLC', '140').
card_flavor_text('timbermare'/'PLC', 'Only nature wreaks such lovely havoc.').
card_multiverse_id('timbermare'/'PLC', '124478').

card_in_set('timebender', 'PLC').
card_original_type('timebender'/'PLC', 'Creature — Human Wizard').
card_original_text('timebender'/'PLC', 'Morph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Timebender is turned face up, choose one Remove two time counters from target permanent or suspended card; or put two time counters on target permanent with a time counter on it or suspended card.').
card_first_print('timebender', 'PLC').
card_image_name('timebender'/'PLC', 'timebender').
card_uid('timebender'/'PLC', 'PLC:Timebender:timebender').
card_rarity('timebender'/'PLC', 'Uncommon').
card_artist('timebender'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('timebender'/'PLC', '50').
card_multiverse_id('timebender'/'PLC', '122370').

card_in_set('timecrafting', 'PLC').
card_original_type('timecrafting'/'PLC', 'Instant').
card_original_text('timecrafting'/'PLC', 'Choose one Remove X time counters from target permanent or suspended card; or put X time counters on target permanent with a time counter on it or suspended card.').
card_first_print('timecrafting', 'PLC').
card_image_name('timecrafting'/'PLC', 'timecrafting').
card_uid('timecrafting'/'PLC', 'PLC:Timecrafting:timecrafting').
card_rarity('timecrafting'/'PLC', 'Uncommon').
card_artist('timecrafting'/'PLC', 'Volkan Baga').
card_number('timecrafting'/'PLC', '109').
card_flavor_text('timecrafting'/'PLC', 'Oddly, an unstable time stream makes wizards want to tinker with it even more.').
card_multiverse_id('timecrafting'/'PLC', '129012').

card_in_set('torchling', 'PLC').
card_original_type('torchling'/'PLC', 'Creature — Shapeshifter').
card_original_text('torchling'/'PLC', '{R}: Untap Torchling.\n{R}: Target creature blocks Torchling this turn if able.\n{R}: Change the target of target spell that targets only Torchling.\n{1}: Torchling gets +1/-1 until end of turn.\n{1}: Torchling gets -1/+1 until end of turn.').
card_first_print('torchling', 'PLC').
card_image_name('torchling'/'PLC', 'torchling').
card_uid('torchling'/'PLC', 'PLC:Torchling:torchling').
card_rarity('torchling'/'PLC', 'Rare').
card_artist('torchling'/'PLC', 'rk post').
card_number('torchling'/'PLC', '110').
card_multiverse_id('torchling'/'PLC', '126810').

card_in_set('treacherous urge', 'PLC').
card_original_type('treacherous urge'/'PLC', 'Instant').
card_original_text('treacherous urge'/'PLC', 'Target opponent reveals his or her hand. You may put a creature card from it into play under your control. That creature has haste. Sacrifice it at end of turn.').
card_first_print('treacherous urge', 'PLC').
card_image_name('treacherous urge'/'PLC', 'treacherous urge').
card_uid('treacherous urge'/'PLC', 'PLC:Treacherous Urge:treacherous urge').
card_rarity('treacherous urge'/'PLC', 'Uncommon').
card_artist('treacherous urge'/'PLC', 'Steven Belledin').
card_number('treacherous urge'/'PLC', '82').
card_multiverse_id('treacherous urge'/'PLC', '131002').

card_in_set('tumble', 'PLC').
card_original_type('tumble'/'PLC', 'Sorcery').
card_original_text('tumble'/'PLC', 'Rough deals 2 damage to each creature without flying.\n//\nTumble\n{5}{R}\nSorcery\nTumble deals 6 damage to each creature with flying.').
card_first_print('tumble', 'PLC').
card_image_name('tumble'/'PLC', 'roughtumble').
card_uid('tumble'/'PLC', 'PLC:Tumble:roughtumble').
card_rarity('tumble'/'PLC', 'Uncommon').
card_artist('tumble'/'PLC', 'Luca Zontini').
card_number('tumble'/'PLC', '114b').
card_multiverse_id('tumble'/'PLC', '126420').

card_in_set('uktabi drake', 'PLC').
card_original_type('uktabi drake'/'PLC', 'Creature — Drake').
card_original_text('uktabi drake'/'PLC', 'Flying, haste\nEcho {1}{G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_first_print('uktabi drake', 'PLC').
card_image_name('uktabi drake'/'PLC', 'uktabi drake').
card_uid('uktabi drake'/'PLC', 'PLC:Uktabi Drake:uktabi drake').
card_rarity('uktabi drake'/'PLC', 'Common').
card_artist('uktabi drake'/'PLC', 'Matt Cavotta').
card_number('uktabi drake'/'PLC', '141').
card_multiverse_id('uktabi drake'/'PLC', '126306').

card_in_set('urborg, tomb of yawgmoth', 'PLC').
card_original_type('urborg, tomb of yawgmoth'/'PLC', 'Legendary Land').
card_original_text('urborg, tomb of yawgmoth'/'PLC', 'Each land is a Swamp in addition to its other land types.').
card_first_print('urborg, tomb of yawgmoth', 'PLC').
card_image_name('urborg, tomb of yawgmoth'/'PLC', 'urborg, tomb of yawgmoth').
card_uid('urborg, tomb of yawgmoth'/'PLC', 'PLC:Urborg, Tomb of Yawgmoth:urborg, tomb of yawgmoth').
card_rarity('urborg, tomb of yawgmoth'/'PLC', 'Rare').
card_artist('urborg, tomb of yawgmoth'/'PLC', 'John Avon').
card_number('urborg, tomb of yawgmoth'/'PLC', '165').
card_flavor_text('urborg, tomb of yawgmoth'/'PLC', '\"Yawgmoth\'s corpse is a wound in the universe. His foul blood seeps out, infecting the land with his final curse.\"\n—Lord Windgrace').
card_multiverse_id('urborg, tomb of yawgmoth'/'PLC', '131005').

card_in_set('utopia vow', 'PLC').
card_original_type('utopia vow'/'PLC', 'Enchantment — Aura').
card_original_text('utopia vow'/'PLC', 'Enchant creature\nEnchanted creature can\'t attack or block.\nEnchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('utopia vow', 'PLC').
card_image_name('utopia vow'/'PLC', 'utopia vow').
card_uid('utopia vow'/'PLC', 'PLC:Utopia Vow:utopia vow').
card_rarity('utopia vow'/'PLC', 'Common').
card_artist('utopia vow'/'PLC', 'Heather Hudson').
card_number('utopia vow'/'PLC', '142').
card_flavor_text('utopia vow'/'PLC', '\"I\'ll guard this fruit and its seedling until the land is healed. My life now belongs to Dominaria.\"').
card_multiverse_id('utopia vow'/'PLC', '131004').

card_in_set('vampiric link', 'PLC').
card_original_type('vampiric link'/'PLC', 'Enchantment — Aura').
card_original_text('vampiric link'/'PLC', 'Enchant creature\nWhenever enchanted creature deals damage, you gain that much life.').
card_first_print('vampiric link', 'PLC').
card_image_name('vampiric link'/'PLC', 'vampiric link').
card_uid('vampiric link'/'PLC', 'PLC:Vampiric Link:vampiric link').
card_rarity('vampiric link'/'PLC', 'Common').
card_artist('vampiric link'/'PLC', 'Kev Walker').
card_number('vampiric link'/'PLC', '92').
card_flavor_text('vampiric link'/'PLC', 'The familiar\'s fangs deliver the master\'s bite.').
card_multiverse_id('vampiric link'/'PLC', '122366').
card_timeshifted('vampiric link'/'PLC')
.
card_in_set('veiling oddity', 'PLC').
card_original_type('veiling oddity'/'PLC', 'Creature — Illusion').
card_original_text('veiling oddity'/'PLC', 'Suspend 4—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)\nWhen the last time counter is removed from Veiling Oddity while it\'s removed from the game, creatures are unblockable this turn.').
card_first_print('veiling oddity', 'PLC').
card_image_name('veiling oddity'/'PLC', 'veiling oddity').
card_uid('veiling oddity'/'PLC', 'PLC:Veiling Oddity:veiling oddity').
card_rarity('veiling oddity'/'PLC', 'Common').
card_artist('veiling oddity'/'PLC', 'Dave DeVries').
card_number('veiling oddity'/'PLC', '51').
card_multiverse_id('veiling oddity'/'PLC', '126806').

card_in_set('venarian glimmer', 'PLC').
card_original_type('venarian glimmer'/'PLC', 'Instant').
card_original_text('venarian glimmer'/'PLC', 'Target player reveals his or her hand. Choose a nonland card with converted mana cost X or less from it. That player discards that card.').
card_first_print('venarian glimmer', 'PLC').
card_image_name('venarian glimmer'/'PLC', 'venarian glimmer').
card_uid('venarian glimmer'/'PLC', 'PLC:Venarian Glimmer:venarian glimmer').
card_rarity('venarian glimmer'/'PLC', 'Uncommon').
card_artist('venarian glimmer'/'PLC', 'John Avon').
card_number('venarian glimmer'/'PLC', '52').
card_flavor_text('venarian glimmer'/'PLC', 'Dream of nothing, and wake to a dream come true.').
card_multiverse_id('venarian glimmer'/'PLC', '126216').

card_in_set('vitaspore thallid', 'PLC').
card_original_type('vitaspore thallid'/'PLC', 'Creature — Fungus').
card_original_text('vitaspore thallid'/'PLC', 'At the beginning of your upkeep, put a spore counter on Vitaspore Thallid.\nRemove three spore counters from Vitaspore Thallid: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Target creature gains haste until end of turn.').
card_first_print('vitaspore thallid', 'PLC').
card_image_name('vitaspore thallid'/'PLC', 'vitaspore thallid').
card_uid('vitaspore thallid'/'PLC', 'PLC:Vitaspore Thallid:vitaspore thallid').
card_rarity('vitaspore thallid'/'PLC', 'Common').
card_artist('vitaspore thallid'/'PLC', 'Christopher Moeller').
card_number('vitaspore thallid'/'PLC', '143').
card_multiverse_id('vitaspore thallid'/'PLC', '128941').

card_in_set('voidstone gargoyle', 'PLC').
card_original_type('voidstone gargoyle'/'PLC', 'Creature — Gargoyle').
card_original_text('voidstone gargoyle'/'PLC', 'Flying\nAs Voidstone Gargoyle comes into play, name a nonland card.\nThe named card can\'t be played.\nActivated abilities of permanents with that name can\'t be played.\nActivated abilities of cards with that name that aren\'t in play can\'t be played.').
card_first_print('voidstone gargoyle', 'PLC').
card_image_name('voidstone gargoyle'/'PLC', 'voidstone gargoyle').
card_uid('voidstone gargoyle'/'PLC', 'PLC:Voidstone Gargoyle:voidstone gargoyle').
card_rarity('voidstone gargoyle'/'PLC', 'Rare').
card_artist('voidstone gargoyle'/'PLC', 'Terese Nielsen').
card_number('voidstone gargoyle'/'PLC', '21').
card_multiverse_id('voidstone gargoyle'/'PLC', '125882').

card_in_set('volcano hellion', 'PLC').
card_original_type('volcano hellion'/'PLC', 'Creature — Hellion').
card_original_text('volcano hellion'/'PLC', 'Volcano Hellion has echo {X}, where X is your life total.\nWhen Volcano Hellion comes into play, it deals an amount of damage of your choice to you and target creature. The damage can\'t be prevented.').
card_first_print('volcano hellion', 'PLC').
card_image_name('volcano hellion'/'PLC', 'volcano hellion').
card_uid('volcano hellion'/'PLC', 'PLC:Volcano Hellion:volcano hellion').
card_rarity('volcano hellion'/'PLC', 'Rare').
card_artist('volcano hellion'/'PLC', 'Wayne Reynolds').
card_number('volcano hellion'/'PLC', '111').
card_multiverse_id('volcano hellion'/'PLC', '126303').

card_in_set('vorosh, the hunter', 'PLC').
card_original_type('vorosh, the hunter'/'PLC', 'Legendary Creature — Dragon').
card_original_text('vorosh, the hunter'/'PLC', 'Flying\nWhenever Vorosh, the Hunter deals combat damage to a player, you may pay {2}{G}. If you do, put six +1/+1 counters on Vorosh.').
card_first_print('vorosh, the hunter', 'PLC').
card_image_name('vorosh, the hunter'/'PLC', 'vorosh, the hunter').
card_uid('vorosh, the hunter'/'PLC', 'PLC:Vorosh, the Hunter:vorosh, the hunter').
card_rarity('vorosh, the hunter'/'PLC', 'Rare').
card_artist('vorosh, the hunter'/'PLC', 'Mark Zug').
card_number('vorosh, the hunter'/'PLC', '164').
card_multiverse_id('vorosh, the hunter'/'PLC', '124057').

card_in_set('waning wurm', 'PLC').
card_original_type('waning wurm'/'PLC', 'Creature — Zombie Wurm').
card_original_text('waning wurm'/'PLC', 'Vanishing 2 (This permanent comes into play with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)').
card_first_print('waning wurm', 'PLC').
card_image_name('waning wurm'/'PLC', 'waning wurm').
card_uid('waning wurm'/'PLC', 'PLC:Waning Wurm:waning wurm').
card_rarity('waning wurm'/'PLC', 'Uncommon').
card_artist('waning wurm'/'PLC', 'Alan Pollack').
card_number('waning wurm'/'PLC', '83').
card_multiverse_id('waning wurm'/'PLC', '110503').

card_in_set('whitemane lion', 'PLC').
card_original_type('whitemane lion'/'PLC', 'Creature — Cat').
card_original_text('whitemane lion'/'PLC', 'Flash (You may play this spell any time you could play an instant.)\nWhen Whitemane Lion comes into play, return a creature you control to its owner\'s hand.').
card_first_print('whitemane lion', 'PLC').
card_image_name('whitemane lion'/'PLC', 'whitemane lion').
card_uid('whitemane lion'/'PLC', 'PLC:Whitemane Lion:whitemane lion').
card_rarity('whitemane lion'/'PLC', 'Common').
card_artist('whitemane lion'/'PLC', 'Zoltan Boros & Gabor Szikszai').
card_number('whitemane lion'/'PLC', '22').
card_flavor_text('whitemane lion'/'PLC', 'Saltfield nomads call a sudden storm a \"whitemane.\"').
card_multiverse_id('whitemane lion'/'PLC', '122477').

card_in_set('wild pair', 'PLC').
card_original_type('wild pair'/'PLC', 'Enchantment').
card_original_text('wild pair'/'PLC', 'Whenever a creature comes into play, if you played it from your hand, you may search your library for a creature card with the same total power and toughness and put it into play. If you do, shuffle your library.').
card_first_print('wild pair', 'PLC').
card_image_name('wild pair'/'PLC', 'wild pair').
card_uid('wild pair'/'PLC', 'PLC:Wild Pair:wild pair').
card_rarity('wild pair'/'PLC', 'Rare').
card_artist('wild pair'/'PLC', 'Mark Brill').
card_number('wild pair'/'PLC', '144').
card_multiverse_id('wild pair'/'PLC', '122468').

card_in_set('wistful thinking', 'PLC').
card_original_type('wistful thinking'/'PLC', 'Sorcery').
card_original_text('wistful thinking'/'PLC', 'Target player draws two cards, then discards four cards.').
card_first_print('wistful thinking', 'PLC').
card_image_name('wistful thinking'/'PLC', 'wistful thinking').
card_uid('wistful thinking'/'PLC', 'PLC:Wistful Thinking:wistful thinking').
card_rarity('wistful thinking'/'PLC', 'Common').
card_artist('wistful thinking'/'PLC', 'Brian Despain').
card_number('wistful thinking'/'PLC', '53').
card_flavor_text('wistful thinking'/'PLC', '\"To probe the wonders of the multiverse, to gaze upon worlds unspoiled by blade or spell . . . it\'s enough to make one weep for the possibilities denied.\"').
card_multiverse_id('wistful thinking'/'PLC', '124055').
