% Exodus

set('EXO').
set_name('EXO', 'Exodus').
set_release_date('EXO', '1998-06-15').
set_border('EXO', 'black').
set_type('EXO', 'expansion').
set_block('EXO', 'Tempest').

card_in_set('æther tide', 'EXO').
card_original_type('æther tide'/'EXO', 'Sorcery').
card_original_text('æther tide'/'EXO', 'Choose and discard X creature cards: Return X target creatures to their owner\'s hand.').
card_first_print('æther tide', 'EXO').
card_image_name('æther tide'/'EXO', 'aether tide').
card_uid('æther tide'/'EXO', 'EXO:Æther Tide:aether tide').
card_rarity('æther tide'/'EXO', 'Common').
card_artist('æther tide'/'EXO', 'Andrew Robinson').
card_number('æther tide'/'EXO', '27').
card_flavor_text('æther tide'/'EXO', 'The tide of magic brought them here. Now it would take them home.').
card_multiverse_id('æther tide'/'EXO', '6071').

card_in_set('allay', 'EXO').
card_original_type('allay'/'EXO', 'Instant').
card_original_text('allay'/'EXO', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nDestroy target enchantment.').
card_first_print('allay', 'EXO').
card_image_name('allay'/'EXO', 'allay').
card_uid('allay'/'EXO', 'EXO:Allay:allay').
card_rarity('allay'/'EXO', 'Common').
card_artist('allay'/'EXO', 'Randy Gallegos').
card_number('allay'/'EXO', '1').
card_multiverse_id('allay'/'EXO', '6036').

card_in_set('anarchist', 'EXO').
card_original_type('anarchist'/'EXO', 'Summon — Townsfolk').
card_original_text('anarchist'/'EXO', 'When Anarchist comes into play, you may return target sorcery card from your graveyard to your hand.').
card_first_print('anarchist', 'EXO').
card_image_name('anarchist'/'EXO', 'anarchist').
card_uid('anarchist'/'EXO', 'EXO:Anarchist:anarchist').
card_rarity('anarchist'/'EXO', 'Common').
card_artist('anarchist'/'EXO', 'Brom').
card_number('anarchist'/'EXO', '79').
card_flavor_text('anarchist'/'EXO', '\"I\'ll take that. You won\'t need it when the revolution comes.\"').
card_multiverse_id('anarchist'/'EXO', '6110').

card_in_set('angelic blessing', 'EXO').
card_original_type('angelic blessing'/'EXO', 'Sorcery').
card_original_text('angelic blessing'/'EXO', 'Target creature gets +3/+3 and gains flying until end of turn.').
card_image_name('angelic blessing'/'EXO', 'angelic blessing').
card_uid('angelic blessing'/'EXO', 'EXO:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'EXO', 'Common').
card_artist('angelic blessing'/'EXO', 'Mark Zug').
card_number('angelic blessing'/'EXO', '2').
card_flavor_text('angelic blessing'/'EXO', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'EXO', '6040').

card_in_set('avenging druid', 'EXO').
card_original_type('avenging druid'/'EXO', 'Summon — Druid').
card_original_text('avenging druid'/'EXO', 'If Avenging Druid damages any opponent, you may reveal cards from your library until you reveal a land card. Put that land into play and put all other revealed cards into your graveyard.').
card_first_print('avenging druid', 'EXO').
card_image_name('avenging druid'/'EXO', 'avenging druid').
card_uid('avenging druid'/'EXO', 'EXO:Avenging Druid:avenging druid').
card_rarity('avenging druid'/'EXO', 'Common').
card_artist('avenging druid'/'EXO', 'Daren Bader').
card_number('avenging druid'/'EXO', '105').
card_multiverse_id('avenging druid'/'EXO', '6137').

card_in_set('bequeathal', 'EXO').
card_original_type('bequeathal'/'EXO', 'Enchant Creature').
card_original_text('bequeathal'/'EXO', 'If enchanted creature is put into any graveyard, draw two cards.').
card_first_print('bequeathal', 'EXO').
card_image_name('bequeathal'/'EXO', 'bequeathal').
card_uid('bequeathal'/'EXO', 'EXO:Bequeathal:bequeathal').
card_rarity('bequeathal'/'EXO', 'Common').
card_artist('bequeathal'/'EXO', 'D. Alexander Gregory').
card_number('bequeathal'/'EXO', '106').
card_flavor_text('bequeathal'/'EXO', '\"On my death, I give you this treasure: the knowledge that life is hard, yet too soon past.\"\n—Dal funeral rite').
card_multiverse_id('bequeathal'/'EXO', '6147').

card_in_set('carnophage', 'EXO').
card_original_type('carnophage'/'EXO', 'Summon — Zombie').
card_original_text('carnophage'/'EXO', 'During your upkeep, pay 1 life or tap Carnophage.').
card_first_print('carnophage', 'EXO').
card_image_name('carnophage'/'EXO', 'carnophage').
card_uid('carnophage'/'EXO', 'EXO:Carnophage:carnophage').
card_rarity('carnophage'/'EXO', 'Common').
card_artist('carnophage'/'EXO', 'Pete Venters').
card_number('carnophage'/'EXO', '53').
card_flavor_text('carnophage'/'EXO', 'Eating is all it knows.').
card_multiverse_id('carnophage'/'EXO', '6079').

card_in_set('cartographer', 'EXO').
card_original_type('cartographer'/'EXO', 'Summon — Townsfolk').
card_original_text('cartographer'/'EXO', 'When Cartographer comes into play, you may return target land card from your graveyard to your hand.').
card_first_print('cartographer', 'EXO').
card_image_name('cartographer'/'EXO', 'cartographer').
card_uid('cartographer'/'EXO', 'EXO:Cartographer:cartographer').
card_rarity('cartographer'/'EXO', 'Uncommon').
card_artist('cartographer'/'EXO', 'Jeff Laubenstein').
card_number('cartographer'/'EXO', '107').
card_flavor_text('cartographer'/'EXO', 'How do you map a world when the land moves beneath your feet?').
card_multiverse_id('cartographer'/'EXO', '6144').

card_in_set('cat burglar', 'EXO').
card_original_type('cat burglar'/'EXO', 'Summon — Minion').
card_original_text('cat burglar'/'EXO', '{2}{B}, {T}: Target player chooses and discards a card. Play this ability as a sorcery.').
card_first_print('cat burglar', 'EXO').
card_image_name('cat burglar'/'EXO', 'cat burglar').
card_uid('cat burglar'/'EXO', 'EXO:Cat Burglar:cat burglar').
card_rarity('cat burglar'/'EXO', 'Common').
card_artist('cat burglar'/'EXO', 'DiTerlizzi').
card_number('cat burglar'/'EXO', '54').
card_flavor_text('cat burglar'/'EXO', 'The il-Kor no longer roam the open plains, but they are still hunters.').
card_multiverse_id('cat burglar'/'EXO', '6080').

card_in_set('cataclysm', 'EXO').
card_original_type('cataclysm'/'EXO', 'Sorcery').
card_original_text('cataclysm'/'EXO', 'Each player chooses from the permanents he or she controls an artifact, a creature, an enchantment, and a land and sacrifices the rest.').
card_first_print('cataclysm', 'EXO').
card_image_name('cataclysm'/'EXO', 'cataclysm').
card_uid('cataclysm'/'EXO', 'EXO:Cataclysm:cataclysm').
card_rarity('cataclysm'/'EXO', 'Rare').
card_artist('cataclysm'/'EXO', 'Jim Nelson').
card_number('cataclysm'/'EXO', '3').
card_flavor_text('cataclysm'/'EXO', 'The Weatherlight dragged the Predator behind it, the cradle hauling the casket.').
card_multiverse_id('cataclysm'/'EXO', '6050').

card_in_set('charging paladin', 'EXO').
card_original_type('charging paladin'/'EXO', 'Summon — Knight').
card_original_text('charging paladin'/'EXO', 'If Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_image_name('charging paladin'/'EXO', 'charging paladin').
card_uid('charging paladin'/'EXO', 'EXO:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'EXO', 'Common').
card_artist('charging paladin'/'EXO', 'Ciruelo').
card_number('charging paladin'/'EXO', '4').
card_flavor_text('charging paladin'/'EXO', '\"Hope shall be the blade that severs our bonds.\"').
card_multiverse_id('charging paladin'/'EXO', '6035').

card_in_set('cinder crawler', 'EXO').
card_original_type('cinder crawler'/'EXO', 'Summon — Salamander').
card_original_text('cinder crawler'/'EXO', '{R} Cinder Crawler gets +1/+0 until end of turn. Use this ability only if Cinder Crawler is blocked.').
card_first_print('cinder crawler', 'EXO').
card_image_name('cinder crawler'/'EXO', 'cinder crawler').
card_uid('cinder crawler'/'EXO', 'EXO:Cinder Crawler:cinder crawler').
card_rarity('cinder crawler'/'EXO', 'Common').
card_artist('cinder crawler'/'EXO', 'Jim Nelson').
card_number('cinder crawler'/'EXO', '80').
card_flavor_text('cinder crawler'/'EXO', 'The crawler doesn\'t care what gets in its way. It pushes everything into the boiling muck and waits for the meal to cook.').
card_multiverse_id('cinder crawler'/'EXO', '6105').

card_in_set('city of traitors', 'EXO').
card_original_type('city of traitors'/'EXO', 'Land').
card_original_text('city of traitors'/'EXO', 'If you play a land, sacrifice City of Traitors.\n{T}: Add two colorless mana to your mana pool.').
card_first_print('city of traitors', 'EXO').
card_image_name('city of traitors'/'EXO', 'city of traitors').
card_uid('city of traitors'/'EXO', 'EXO:City of Traitors:city of traitors').
card_rarity('city of traitors'/'EXO', 'Rare').
card_artist('city of traitors'/'EXO', 'Kev Walker').
card_number('city of traitors'/'EXO', '143').
card_flavor_text('city of traitors'/'EXO', '\"While we fought, the il surrendered.\"\n—Oracle en-Vec').
card_multiverse_id('city of traitors'/'EXO', '6168').

card_in_set('coat of arms', 'EXO').
card_original_type('coat of arms'/'EXO', 'Artifact').
card_original_text('coat of arms'/'EXO', 'Each creature gets +1/+1 for each other creature in play of the same creature type. (For example, if there are three Goblins in play, each of them gets +2/+2.)').
card_first_print('coat of arms', 'EXO').
card_image_name('coat of arms'/'EXO', 'coat of arms').
card_uid('coat of arms'/'EXO', 'EXO:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'EXO', 'Rare').
card_artist('coat of arms'/'EXO', 'Scott M. Fischer').
card_number('coat of arms'/'EXO', '131').
card_flavor_text('coat of arms'/'EXO', '\"Hup, two, three, four,\nDunno how to count no more.\"\n—Mogg march').
card_multiverse_id('coat of arms'/'EXO', '6162').

card_in_set('convalescence', 'EXO').
card_original_type('convalescence'/'EXO', 'Enchantment').
card_original_text('convalescence'/'EXO', 'During your upkeep, if you have 10 or less life, gain 1 life.').
card_first_print('convalescence', 'EXO').
card_image_name('convalescence'/'EXO', 'convalescence').
card_uid('convalescence'/'EXO', 'EXO:Convalescence:convalescence').
card_rarity('convalescence'/'EXO', 'Rare').
card_artist('convalescence'/'EXO', 'D. Alexander Gregory').
card_number('convalescence'/'EXO', '5').
card_flavor_text('convalescence'/'EXO', 'Selenia had hurt Mirri terribly, but injuries of the flesh could be treated. The wounds she had torn in Crovax\'s soul would never close.').
card_multiverse_id('convalescence'/'EXO', '6051').

card_in_set('crashing boars', 'EXO').
card_original_type('crashing boars'/'EXO', 'Summon — Boars').
card_original_text('crashing boars'/'EXO', 'If Crashing Boars attacks, defending player chooses an untapped creature he or she controls. That creature blocks Crashing Boars this turn if able.').
card_first_print('crashing boars', 'EXO').
card_image_name('crashing boars'/'EXO', 'crashing boars').
card_uid('crashing boars'/'EXO', 'EXO:Crashing Boars:crashing boars').
card_rarity('crashing boars'/'EXO', 'Uncommon').
card_artist('crashing boars'/'EXO', 'Ron Spencer').
card_number('crashing boars'/'EXO', '108').
card_flavor_text('crashing boars'/'EXO', 'Bores ruin a party. Boars are party to ruin.').
card_multiverse_id('crashing boars'/'EXO', '6145').

card_in_set('culling the weak', 'EXO').
card_original_type('culling the weak'/'EXO', 'Mana Source').
card_original_text('culling the weak'/'EXO', 'Sacrifice a creature: Add {B}{B}{B}{B} to your mana pool.').
card_first_print('culling the weak', 'EXO').
card_image_name('culling the weak'/'EXO', 'culling the weak').
card_uid('culling the weak'/'EXO', 'EXO:Culling the Weak:culling the weak').
card_rarity('culling the weak'/'EXO', 'Common').
card_artist('culling the weak'/'EXO', 'Scott M. Fischer').
card_number('culling the weak'/'EXO', '55').
card_flavor_text('culling the weak'/'EXO', '\"The blood of the weak shall be the ink with which we scribe great Yawgmoth\'s glorious name.\"\n—Stronghold architect, journal').
card_multiverse_id('culling the weak'/'EXO', '6085').

card_in_set('cunning', 'EXO').
card_original_type('cunning'/'EXO', 'Enchant Creature').
card_original_text('cunning'/'EXO', 'Enchanted creature gets +3/+3.\nIf enchanted creature attacks or blocks, sacrifice Cunning at end of turn.').
card_first_print('cunning', 'EXO').
card_image_name('cunning'/'EXO', 'cunning').
card_uid('cunning'/'EXO', 'EXO:Cunning:cunning').
card_rarity('cunning'/'EXO', 'Common').
card_artist('cunning'/'EXO', 'Kev Walker').
card_number('cunning'/'EXO', '28').
card_flavor_text('cunning'/'EXO', '\". . . Dry-walkers die\nWe feed on sweet flesh . . . .\"\n—Rootwater Saga').
card_multiverse_id('cunning'/'EXO', '6062').

card_in_set('curiosity', 'EXO').
card_original_type('curiosity'/'EXO', 'Enchant Creature').
card_original_text('curiosity'/'EXO', 'If enchanted creature damages an opponent, you may draw a card.').
card_first_print('curiosity', 'EXO').
card_image_name('curiosity'/'EXO', 'curiosity').
card_uid('curiosity'/'EXO', 'EXO:Curiosity:curiosity').
card_rarity('curiosity'/'EXO', 'Uncommon').
card_artist('curiosity'/'EXO', 'Val Mayerik').
card_number('curiosity'/'EXO', '29').
card_flavor_text('curiosity'/'EXO', 'All Mirri wanted to do was rest, but she couldn\'t ignore a nagging suspicion as she followed Crovax\'s skulking form.').
card_multiverse_id('curiosity'/'EXO', '6060').

card_in_set('cursed flesh', 'EXO').
card_original_type('cursed flesh'/'EXO', 'Enchant Creature').
card_original_text('cursed flesh'/'EXO', 'Enchanted creature gets -1/-1 and cannot be blocked except by artifact creatures and black creatures.').
card_first_print('cursed flesh', 'EXO').
card_image_name('cursed flesh'/'EXO', 'cursed flesh').
card_uid('cursed flesh'/'EXO', 'EXO:Cursed Flesh:cursed flesh').
card_rarity('cursed flesh'/'EXO', 'Common').
card_artist('cursed flesh'/'EXO', 'Ron Spencer').
card_number('cursed flesh'/'EXO', '56').
card_flavor_text('cursed flesh'/'EXO', 'A farewell to arms . . . and feet . . . and legs . . . .').
card_multiverse_id('cursed flesh'/'EXO', '6089').

card_in_set('dauthi cutthroat', 'EXO').
card_original_type('dauthi cutthroat'/'EXO', 'Summon — Minion').
card_original_text('dauthi cutthroat'/'EXO', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{B}, {T}: Destroy target creature with shadow.').
card_first_print('dauthi cutthroat', 'EXO').
card_image_name('dauthi cutthroat'/'EXO', 'dauthi cutthroat').
card_uid('dauthi cutthroat'/'EXO', 'EXO:Dauthi Cutthroat:dauthi cutthroat').
card_rarity('dauthi cutthroat'/'EXO', 'Uncommon').
card_artist('dauthi cutthroat'/'EXO', 'Dermot Power').
card_number('dauthi cutthroat'/'EXO', '57').
card_flavor_text('dauthi cutthroat'/'EXO', 'In their twisted logic, to empty the shadow world is to escape it.').
card_multiverse_id('dauthi cutthroat'/'EXO', '6091').

card_in_set('dauthi jackal', 'EXO').
card_original_type('dauthi jackal'/'EXO', 'Summon — Hound').
card_original_text('dauthi jackal'/'EXO', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{B}{B}, Sacrifice Dauthi Jackal: Destroy target blocking creature.').
card_first_print('dauthi jackal', 'EXO').
card_image_name('dauthi jackal'/'EXO', 'dauthi jackal').
card_uid('dauthi jackal'/'EXO', 'EXO:Dauthi Jackal:dauthi jackal').
card_rarity('dauthi jackal'/'EXO', 'Common').
card_artist('dauthi jackal'/'EXO', 'Adam Rex').
card_number('dauthi jackal'/'EXO', '58').
card_flavor_text('dauthi jackal'/'EXO', '\"All Dauthi are jackals, regardless of breed.\"\n—Lyna, Soltari emissary').
card_multiverse_id('dauthi jackal'/'EXO', '6081').

card_in_set('dauthi warlord', 'EXO').
card_original_type('dauthi warlord'/'EXO', 'Summon — Soldier').
card_original_text('dauthi warlord'/'EXO', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Warlord has power equal to the number of creatures with shadow in play.').
card_first_print('dauthi warlord', 'EXO').
card_image_name('dauthi warlord'/'EXO', 'dauthi warlord').
card_uid('dauthi warlord'/'EXO', 'EXO:Dauthi Warlord:dauthi warlord').
card_rarity('dauthi warlord'/'EXO', 'Uncommon').
card_artist('dauthi warlord'/'EXO', 'Kev Walker').
card_number('dauthi warlord'/'EXO', '59').
card_multiverse_id('dauthi warlord'/'EXO', '6094').

card_in_set('death\'s duet', 'EXO').
card_original_type('death\'s duet'/'EXO', 'Sorcery').
card_original_text('death\'s duet'/'EXO', 'Return two target creature cards from your graveyard to your hand.').
card_first_print('death\'s duet', 'EXO').
card_image_name('death\'s duet'/'EXO', 'death\'s duet').
card_uid('death\'s duet'/'EXO', 'EXO:Death\'s Duet:death\'s duet').
card_rarity('death\'s duet'/'EXO', 'Common').
card_artist('death\'s duet'/'EXO', 'Keith Parkinson').
card_number('death\'s duet'/'EXO', '60').
card_flavor_text('death\'s duet'/'EXO', 'Life has always been a dance. It is only fitting that death sing the tune.').
card_multiverse_id('death\'s duet'/'EXO', '6087').

card_in_set('dizzying gaze', 'EXO').
card_original_type('dizzying gaze'/'EXO', 'Enchant Creature').
card_original_text('dizzying gaze'/'EXO', 'Play Dizzying Gaze only on a creature you control.\n{R} Enchanted creature deals 1 damage to target creature with flying.').
card_first_print('dizzying gaze', 'EXO').
card_image_name('dizzying gaze'/'EXO', 'dizzying gaze').
card_uid('dizzying gaze'/'EXO', 'EXO:Dizzying Gaze:dizzying gaze').
card_rarity('dizzying gaze'/'EXO', 'Common').
card_artist('dizzying gaze'/'EXO', 'Thomas M. Baxa').
card_number('dizzying gaze'/'EXO', '81').
card_flavor_text('dizzying gaze'/'EXO', '\"When I cut down my angel I only started my fall.\"\n—Crovax').
card_multiverse_id('dizzying gaze'/'EXO', '6114').

card_in_set('dominating licid', 'EXO').
card_original_type('dominating licid'/'EXO', 'Summon — Licid').
card_original_text('dominating licid'/'EXO', '{1}{U}{U}, {T}: Dominating Licid loses this ability and becomes a creature enchantment that reads \"Gain control of enchanted creature\" instead of any other type of permanent. Move Dominating Licid onto target creature. You may pay {U} to end this effect.').
card_first_print('dominating licid', 'EXO').
card_image_name('dominating licid'/'EXO', 'dominating licid').
card_uid('dominating licid'/'EXO', 'EXO:Dominating Licid:dominating licid').
card_rarity('dominating licid'/'EXO', 'Rare').
card_artist('dominating licid'/'EXO', 'Heather Hudson').
card_number('dominating licid'/'EXO', '30').
card_multiverse_id('dominating licid'/'EXO', '5139').

card_in_set('elven palisade', 'EXO').
card_original_type('elven palisade'/'EXO', 'Enchantment').
card_original_text('elven palisade'/'EXO', 'Sacrifice a forest: Target attacking creature gets -3/-0 until end of turn.').
card_first_print('elven palisade', 'EXO').
card_image_name('elven palisade'/'EXO', 'elven palisade').
card_uid('elven palisade'/'EXO', 'EXO:Elven Palisade:elven palisade').
card_rarity('elven palisade'/'EXO', 'Uncommon').
card_artist('elven palisade'/'EXO', 'Mark Zug').
card_number('elven palisade'/'EXO', '109').
card_flavor_text('elven palisade'/'EXO', '\"Volrath is not the only one who can shape this world to serve him.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('elven palisade'/'EXO', '6146').

card_in_set('elvish berserker', 'EXO').
card_original_type('elvish berserker'/'EXO', 'Summon — Elf').
card_original_text('elvish berserker'/'EXO', 'For each creature that blocks it, Elvish Berserker gets +1/+1 until end of turn.').
card_first_print('elvish berserker', 'EXO').
card_image_name('elvish berserker'/'EXO', 'elvish berserker').
card_uid('elvish berserker'/'EXO', 'EXO:Elvish Berserker:elvish berserker').
card_rarity('elvish berserker'/'EXO', 'Common').
card_artist('elvish berserker'/'EXO', 'Paolo Parente').
card_number('elvish berserker'/'EXO', '110').
card_flavor_text('elvish berserker'/'EXO', 'Their fury scatters enemies like a pile of dry leaves.').
card_multiverse_id('elvish berserker'/'EXO', '6133').

card_in_set('entropic specter', 'EXO').
card_original_type('entropic specter'/'EXO', 'Summon — Spirit').
card_original_text('entropic specter'/'EXO', 'Flying\nEntropic Specter has power and toughness each equal to the number of cards in target opponent\'s hand.\nIf Entropic Specter damages any player, that player chooses and discards a card.').
card_first_print('entropic specter', 'EXO').
card_image_name('entropic specter'/'EXO', 'entropic specter').
card_uid('entropic specter'/'EXO', 'EXO:Entropic Specter:entropic specter').
card_rarity('entropic specter'/'EXO', 'Rare').
card_artist('entropic specter'/'EXO', 'Ron Spencer').
card_number('entropic specter'/'EXO', '61').
card_multiverse_id('entropic specter'/'EXO', '6099').

card_in_set('ephemeron', 'EXO').
card_original_type('ephemeron'/'EXO', 'Summon — Illusion').
card_original_text('ephemeron'/'EXO', 'Flying\nChoose and discard a card: Return Ephemeron to owner\'s hand.').
card_first_print('ephemeron', 'EXO').
card_image_name('ephemeron'/'EXO', 'ephemeron').
card_uid('ephemeron'/'EXO', 'EXO:Ephemeron:ephemeron').
card_rarity('ephemeron'/'EXO', 'Rare').
card_artist('ephemeron'/'EXO', 'Keith Parkinson').
card_number('ephemeron'/'EXO', '31').
card_flavor_text('ephemeron'/'EXO', 'From nothing came teeth.').
card_multiverse_id('ephemeron'/'EXO', '6073').

card_in_set('equilibrium', 'EXO').
card_original_type('equilibrium'/'EXO', 'Enchantment').
card_original_text('equilibrium'/'EXO', 'Whenever you successfully cast a creature spell, you may pay {1} to return target creature to owner\'s hand.').
card_first_print('equilibrium', 'EXO').
card_image_name('equilibrium'/'EXO', 'equilibrium').
card_uid('equilibrium'/'EXO', 'EXO:Equilibrium:equilibrium').
card_rarity('equilibrium'/'EXO', 'Rare').
card_artist('equilibrium'/'EXO', 'Jeff Miracola').
card_number('equilibrium'/'EXO', '32').
card_flavor_text('equilibrium'/'EXO', 'In Rath, there are only so many souls to go around.').
card_multiverse_id('equilibrium'/'EXO', '6075').

card_in_set('erratic portal', 'EXO').
card_original_type('erratic portal'/'EXO', 'Artifact').
card_original_text('erratic portal'/'EXO', '{1}, {T}: Return target creature to owner\'s hand unless its controller pays {1}.').
card_first_print('erratic portal', 'EXO').
card_image_name('erratic portal'/'EXO', 'erratic portal').
card_uid('erratic portal'/'EXO', 'EXO:Erratic Portal:erratic portal').
card_rarity('erratic portal'/'EXO', 'Rare').
card_artist('erratic portal'/'EXO', 'John Matson').
card_number('erratic portal'/'EXO', '132').
card_flavor_text('erratic portal'/'EXO', '\"In Barrin\'s name!\" cried Lyna as Hanna\'s sword passed through her, \"Ertai sends word that the portal is open—but not for long!\"').
card_multiverse_id('erratic portal'/'EXO', '6163').

card_in_set('ertai, wizard adept', 'EXO').
card_original_type('ertai, wizard adept'/'EXO', 'Summon — Legend').
card_original_text('ertai, wizard adept'/'EXO', 'Ertai, Wizard Adept counts as a Wizard.\n{2}{U}{U}, {T}: Counter target spell. Play this ability as an interrupt.').
card_first_print('ertai, wizard adept', 'EXO').
card_image_name('ertai, wizard adept'/'EXO', 'ertai, wizard adept').
card_uid('ertai, wizard adept'/'EXO', 'EXO:Ertai, Wizard Adept:ertai, wizard adept').
card_rarity('ertai, wizard adept'/'EXO', 'Rare').
card_artist('ertai, wizard adept'/'EXO', 'Terese Nielsen').
card_number('ertai, wizard adept'/'EXO', '33').
card_flavor_text('ertai, wizard adept'/'EXO', '\"Was that it?\"\n—Ertai, wizard adept').
card_multiverse_id('ertai, wizard adept'/'EXO', '5146').

card_in_set('exalted dragon', 'EXO').
card_original_type('exalted dragon'/'EXO', 'Summon — Dragon').
card_original_text('exalted dragon'/'EXO', 'Flying\nEach turn, Exalted Dragon cannot attack unless you sacrifice a land.').
card_first_print('exalted dragon', 'EXO').
card_image_name('exalted dragon'/'EXO', 'exalted dragon').
card_uid('exalted dragon'/'EXO', 'EXO:Exalted Dragon:exalted dragon').
card_rarity('exalted dragon'/'EXO', 'Rare').
card_artist('exalted dragon'/'EXO', 'Matthew D. Wilson').
card_number('exalted dragon'/'EXO', '6').
card_flavor_text('exalted dragon'/'EXO', 'If dragons excel at anything more than vanity, it is greed.').
card_multiverse_id('exalted dragon'/'EXO', '6053').

card_in_set('fade away', 'EXO').
card_original_type('fade away'/'EXO', 'Sorcery').
card_original_text('fade away'/'EXO', 'For each creature, that creature\'s controller pays {1} or sacrifices a permanent.').
card_first_print('fade away', 'EXO').
card_image_name('fade away'/'EXO', 'fade away').
card_uid('fade away'/'EXO', 'EXO:Fade Away:fade away').
card_rarity('fade away'/'EXO', 'Common').
card_artist('fade away'/'EXO', 'Jeff Miracola').
card_number('fade away'/'EXO', '34').
card_flavor_text('fade away'/'EXO', '\"Consider the fable of the greedy herder. As her flock grew, so did her weariness; she slept, and the sheep wandered off.\"\n—Karn, silver golem').
card_multiverse_id('fade away'/'EXO', '6061').

card_in_set('fighting chance', 'EXO').
card_original_type('fighting chance'/'EXO', 'Instant').
card_original_text('fighting chance'/'EXO', 'For each blocking creature, flip a coin. If you win the flip, that creature deals no combat damage this turn.').
card_first_print('fighting chance', 'EXO').
card_image_name('fighting chance'/'EXO', 'fighting chance').
card_uid('fighting chance'/'EXO', 'EXO:Fighting Chance:fighting chance').
card_rarity('fighting chance'/'EXO', 'Rare').
card_artist('fighting chance'/'EXO', 'Mike Raabe').
card_number('fighting chance'/'EXO', '82').
card_flavor_text('fighting chance'/'EXO', 'A stroke of luck can smite an army.').
card_multiverse_id('fighting chance'/'EXO', '5184').

card_in_set('flowstone flood', 'EXO').
card_original_type('flowstone flood'/'EXO', 'Sorcery').
card_original_text('flowstone flood'/'EXO', 'Buyback—Pay 3 life, Discard a card at random. (You may pay 3 life and discard a card at random in addition to any other costs when you play this spell. If you do, put Flowstone Flood into your hand instead of your graveyard as part of the spell\'s effect.)\nDestroy target land.').
card_first_print('flowstone flood', 'EXO').
card_image_name('flowstone flood'/'EXO', 'flowstone flood').
card_uid('flowstone flood'/'EXO', 'EXO:Flowstone Flood:flowstone flood').
card_rarity('flowstone flood'/'EXO', 'Uncommon').
card_artist('flowstone flood'/'EXO', 'Paolo Parente').
card_number('flowstone flood'/'EXO', '83').
card_multiverse_id('flowstone flood'/'EXO', '6120').

card_in_set('forbid', 'EXO').
card_original_type('forbid'/'EXO', 'Interrupt').
card_original_text('forbid'/'EXO', 'Buyback—Choose and discard two cards. (You may choose and discard two cards in addition to any other costs when you play this spell. If you do, put Forbid into your hand instead of your graveyard as part of the spell\'s effect.)\nCounter target spell.').
card_first_print('forbid', 'EXO').
card_image_name('forbid'/'EXO', 'forbid').
card_uid('forbid'/'EXO', 'EXO:Forbid:forbid').
card_rarity('forbid'/'EXO', 'Uncommon').
card_artist('forbid'/'EXO', 'Scott Kirschner').
card_number('forbid'/'EXO', '35').
card_multiverse_id('forbid'/'EXO', '5157').

card_in_set('fugue', 'EXO').
card_original_type('fugue'/'EXO', 'Sorcery').
card_original_text('fugue'/'EXO', 'Target player chooses and discards three cards.').
card_first_print('fugue', 'EXO').
card_image_name('fugue'/'EXO', 'fugue').
card_uid('fugue'/'EXO', 'EXO:Fugue:fugue').
card_rarity('fugue'/'EXO', 'Uncommon').
card_artist('fugue'/'EXO', 'Randy Gallegos').
card_number('fugue'/'EXO', '62').
card_flavor_text('fugue'/'EXO', '\"My ship . . . ,\" Sisay mumbled. \"Intact,\" Gerrard reassured her, \"which is more than I can say for you, my friend.\" Sisay laughed despite the pain.').
card_multiverse_id('fugue'/'EXO', '5159').

card_in_set('furnace brood', 'EXO').
card_original_type('furnace brood'/'EXO', 'Summon — Elementals').
card_original_text('furnace brood'/'EXO', '{R} Target creature cannot be regenerated this turn.').
card_first_print('furnace brood', 'EXO').
card_image_name('furnace brood'/'EXO', 'furnace brood').
card_uid('furnace brood'/'EXO', 'EXO:Furnace Brood:furnace brood').
card_rarity('furnace brood'/'EXO', 'Common').
card_artist('furnace brood'/'EXO', 'Jeff Miracola').
card_number('furnace brood'/'EXO', '84').
card_flavor_text('furnace brood'/'EXO', 'Furnace air crackles with magmatic cackles.').
card_multiverse_id('furnace brood'/'EXO', '6107').

card_in_set('grollub', 'EXO').
card_original_type('grollub'/'EXO', 'Summon — Beast').
card_original_text('grollub'/'EXO', 'For each 1 damage dealt to Grollub, each opponent gains 1 life.').
card_first_print('grollub', 'EXO').
card_image_name('grollub'/'EXO', 'grollub').
card_uid('grollub'/'EXO', 'EXO:Grollub:grollub').
card_rarity('grollub'/'EXO', 'Common').
card_artist('grollub'/'EXO', 'Chippy').
card_number('grollub'/'EXO', '63').
card_flavor_text('grollub'/'EXO', 'Mogglings caught in a misdeed often blame the grollub. Even though the creature is usually innocent, no taskmaster can resist an excuse to beat it.').
card_multiverse_id('grollub'/'EXO', '6083').

card_in_set('hatred', 'EXO').
card_original_type('hatred'/'EXO', 'Instant').
card_original_text('hatred'/'EXO', 'Pay X life: Target creature gets +X/+0 until end of turn.').
card_first_print('hatred', 'EXO').
card_image_name('hatred'/'EXO', 'hatred').
card_uid('hatred'/'EXO', 'EXO:Hatred:hatred').
card_rarity('hatred'/'EXO', 'Rare').
card_artist('hatred'/'EXO', 'Brom').
card_number('hatred'/'EXO', '64').
card_flavor_text('hatred'/'EXO', '\"I will flay the skin from your flesh and the flesh from your bones and scrape your bones dry. And still you will not have suffered enough.\"\n—Greven il-Vec, to Gerrard').
card_multiverse_id('hatred'/'EXO', '6104').

card_in_set('high ground', 'EXO').
card_original_type('high ground'/'EXO', 'Enchantment').
card_original_text('high ground'/'EXO', 'Each creature you control may block one additional creature. (All defense must be legal.)').
card_first_print('high ground', 'EXO').
card_image_name('high ground'/'EXO', 'high ground').
card_uid('high ground'/'EXO', 'EXO:High Ground:high ground').
card_rarity('high ground'/'EXO', 'Uncommon').
card_artist('high ground'/'EXO', 'rk post').
card_number('high ground'/'EXO', '7').
card_flavor_text('high ground'/'EXO', 'In war, as in society, position is everything.').
card_multiverse_id('high ground'/'EXO', '6169').

card_in_set('jackalope herd', 'EXO').
card_original_type('jackalope herd'/'EXO', 'Summon — Beasts').
card_original_text('jackalope herd'/'EXO', 'If you play any spell, return Jackalope Herd to owner\'s hand.').
card_first_print('jackalope herd', 'EXO').
card_image_name('jackalope herd'/'EXO', 'jackalope herd').
card_uid('jackalope herd'/'EXO', 'EXO:Jackalope Herd:jackalope herd').
card_rarity('jackalope herd'/'EXO', 'Common').
card_artist('jackalope herd'/'EXO', 'Ron Spencer').
card_number('jackalope herd'/'EXO', '111').
card_flavor_text('jackalope herd'/'EXO', 'There\'s little more demeaning than getting your butt kicked by a bunch of bunnies.').
card_multiverse_id('jackalope herd'/'EXO', '6138').

card_in_set('keeper of the beasts', 'EXO').
card_original_type('keeper of the beasts'/'EXO', 'Summon — Wizard').
card_original_text('keeper of the beasts'/'EXO', '{G}, {T}: Put a Beast token into play. Treat this token as a 2/2 green creature. Play this ability only if target opponent controls more creatures than you.').
card_first_print('keeper of the beasts', 'EXO').
card_image_name('keeper of the beasts'/'EXO', 'keeper of the beasts').
card_uid('keeper of the beasts'/'EXO', 'EXO:Keeper of the Beasts:keeper of the beasts').
card_rarity('keeper of the beasts'/'EXO', 'Uncommon').
card_artist('keeper of the beasts'/'EXO', 'rk post').
card_number('keeper of the beasts'/'EXO', '112').
card_multiverse_id('keeper of the beasts'/'EXO', '6142').

card_in_set('keeper of the dead', 'EXO').
card_original_type('keeper of the dead'/'EXO', 'Summon — Wizard').
card_original_text('keeper of the dead'/'EXO', '{B}, {T}: Destroy target nonblack creature. Play this ability only if that creature\'s controller has at least two fewer creature cards in his or her graveyard than you have in yours.').
card_first_print('keeper of the dead', 'EXO').
card_image_name('keeper of the dead'/'EXO', 'keeper of the dead').
card_uid('keeper of the dead'/'EXO', 'EXO:Keeper of the Dead:keeper of the dead').
card_rarity('keeper of the dead'/'EXO', 'Uncommon').
card_artist('keeper of the dead'/'EXO', 'Brom').
card_number('keeper of the dead'/'EXO', '65').
card_multiverse_id('keeper of the dead'/'EXO', '6090').

card_in_set('keeper of the flame', 'EXO').
card_original_type('keeper of the flame'/'EXO', 'Summon — Wizard').
card_original_text('keeper of the flame'/'EXO', '{R}, {T}: Keeper of the Flame deals 2 damage to target opponent. Use this ability only if that opponent has more life than you.').
card_first_print('keeper of the flame', 'EXO').
card_image_name('keeper of the flame'/'EXO', 'keeper of the flame').
card_uid('keeper of the flame'/'EXO', 'EXO:Keeper of the Flame:keeper of the flame').
card_rarity('keeper of the flame'/'EXO', 'Uncommon').
card_artist('keeper of the flame'/'EXO', 'Terese Nielsen').
card_number('keeper of the flame'/'EXO', '85').
card_multiverse_id('keeper of the flame'/'EXO', '6116').

card_in_set('keeper of the light', 'EXO').
card_original_type('keeper of the light'/'EXO', 'Summon — Wizard').
card_original_text('keeper of the light'/'EXO', '{W}, {T}: Gain 3 life. Play this ability only if you have less life than target opponent.').
card_first_print('keeper of the light', 'EXO').
card_image_name('keeper of the light'/'EXO', 'keeper of the light').
card_uid('keeper of the light'/'EXO', 'EXO:Keeper of the Light:keeper of the light').
card_rarity('keeper of the light'/'EXO', 'Uncommon').
card_artist('keeper of the light'/'EXO', 'D. Alexander Gregory').
card_number('keeper of the light'/'EXO', '8').
card_multiverse_id('keeper of the light'/'EXO', '6041').

card_in_set('keeper of the mind', 'EXO').
card_original_type('keeper of the mind'/'EXO', 'Summon — Wizard').
card_original_text('keeper of the mind'/'EXO', '{U}, {T}: Draw a card. Play this ability only if target opponent has at least two more cards in hand than you.').
card_first_print('keeper of the mind', 'EXO').
card_image_name('keeper of the mind'/'EXO', 'keeper of the mind').
card_uid('keeper of the mind'/'EXO', 'EXO:Keeper of the Mind:keeper of the mind').
card_rarity('keeper of the mind'/'EXO', 'Uncommon').
card_artist('keeper of the mind'/'EXO', 'Matthew D. Wilson').
card_number('keeper of the mind'/'EXO', '36').
card_multiverse_id('keeper of the mind'/'EXO', '6064').

card_in_set('killer whale', 'EXO').
card_original_type('killer whale'/'EXO', 'Summon — Whale').
card_original_text('killer whale'/'EXO', '{U} Killer Whale gains flying until end of turn.').
card_first_print('killer whale', 'EXO').
card_image_name('killer whale'/'EXO', 'killer whale').
card_uid('killer whale'/'EXO', 'EXO:Killer Whale:killer whale').
card_rarity('killer whale'/'EXO', 'Uncommon').
card_artist('killer whale'/'EXO', 'Stephen Daniele').
card_number('killer whale'/'EXO', '37').
card_flavor_text('killer whale'/'EXO', 'Hunger is like the sea: deep, endless, and unforgiving.').
card_multiverse_id('killer whale'/'EXO', '6065').

card_in_set('kor chant', 'EXO').
card_original_type('kor chant'/'EXO', 'Instant').
card_original_text('kor chant'/'EXO', 'Redirect to target creature all damage dealt to any one creature you control from any one source.').
card_first_print('kor chant', 'EXO').
card_image_name('kor chant'/'EXO', 'kor chant').
card_uid('kor chant'/'EXO', 'EXO:Kor Chant:kor chant').
card_rarity('kor chant'/'EXO', 'Common').
card_artist('kor chant'/'EXO', 'John Matson').
card_number('kor chant'/'EXO', '9').
card_flavor_text('kor chant'/'EXO', 'The true treasure no thief can touch.').
card_multiverse_id('kor chant'/'EXO', '6045').

card_in_set('limited resources', 'EXO').
card_original_type('limited resources'/'EXO', 'Enchantment').
card_original_text('limited resources'/'EXO', 'When Limited Resources comes into play, each player chooses five lands he or she controls and sacrifices the rest.\nAs long as there are ten or more lands in play, players cannot play lands.').
card_first_print('limited resources', 'EXO').
card_image_name('limited resources'/'EXO', 'limited resources').
card_uid('limited resources'/'EXO', 'EXO:Limited Resources:limited resources').
card_rarity('limited resources'/'EXO', 'Rare').
card_artist('limited resources'/'EXO', 'Keith Parkinson').
card_number('limited resources'/'EXO', '10').
card_multiverse_id('limited resources'/'EXO', '6049').

card_in_set('mage il-vec', 'EXO').
card_original_type('mage il-vec'/'EXO', 'Summon — Wizard').
card_original_text('mage il-vec'/'EXO', '{T}, Discard a card at random: Mage il-Vec deals 1 damage to target creature or player.').
card_first_print('mage il-vec', 'EXO').
card_image_name('mage il-vec'/'EXO', 'mage il-vec').
card_uid('mage il-vec'/'EXO', 'EXO:Mage il-Vec:mage il-vec').
card_rarity('mage il-vec'/'EXO', 'Common').
card_artist('mage il-vec'/'EXO', 'John Matson').
card_number('mage il-vec'/'EXO', '86').
card_flavor_text('mage il-vec'/'EXO', 'Living below a flowstone foundry produces a disproportionate number of fire mages among the il.').
card_multiverse_id('mage il-vec'/'EXO', '6111').

card_in_set('mana breach', 'EXO').
card_original_type('mana breach'/'EXO', 'Enchantment').
card_original_text('mana breach'/'EXO', 'Whenever any player plays a spell, that player returns a land he or she controls to owner\'s hand.').
card_first_print('mana breach', 'EXO').
card_image_name('mana breach'/'EXO', 'mana breach').
card_uid('mana breach'/'EXO', 'EXO:Mana Breach:mana breach').
card_rarity('mana breach'/'EXO', 'Uncommon').
card_artist('mana breach'/'EXO', 'Rebecca Guay').
card_number('mana breach'/'EXO', '38').
card_flavor_text('mana breach'/'EXO', 'The wizard had it all—until she tried to use it.').
card_multiverse_id('mana breach'/'EXO', '6078').

card_in_set('manabond', 'EXO').
card_original_type('manabond'/'EXO', 'Enchantment').
card_original_text('manabond'/'EXO', 'During your discard phase, you may choose to put all land cards from your hand into play. If you do, discard the rest of your hand.').
card_first_print('manabond', 'EXO').
card_image_name('manabond'/'EXO', 'manabond').
card_uid('manabond'/'EXO', 'EXO:Manabond:manabond').
card_rarity('manabond'/'EXO', 'Rare').
card_artist('manabond'/'EXO', 'Stephen Daniele').
card_number('manabond'/'EXO', '113').
card_multiverse_id('manabond'/'EXO', '6154').

card_in_set('maniacal rage', 'EXO').
card_original_type('maniacal rage'/'EXO', 'Enchant Creature').
card_original_text('maniacal rage'/'EXO', 'Enchanted creature gets +2/+2 and cannot block.').
card_first_print('maniacal rage', 'EXO').
card_image_name('maniacal rage'/'EXO', 'maniacal rage').
card_uid('maniacal rage'/'EXO', 'EXO:Maniacal Rage:maniacal rage').
card_rarity('maniacal rage'/'EXO', 'Common').
card_artist('maniacal rage'/'EXO', 'Pete Venters').
card_number('maniacal rage'/'EXO', '87').
card_flavor_text('maniacal rage'/'EXO', 'Only by sacrificing any semblance of defense could Gerrard best Greven.').
card_multiverse_id('maniacal rage'/'EXO', '6109').

card_in_set('medicine bag', 'EXO').
card_original_type('medicine bag'/'EXO', 'Artifact').
card_original_text('medicine bag'/'EXO', '{1}, {T}, Choose and discard a card: Regenerate target creature.').
card_first_print('medicine bag', 'EXO').
card_image_name('medicine bag'/'EXO', 'medicine bag').
card_uid('medicine bag'/'EXO', 'EXO:Medicine Bag:medicine bag').
card_rarity('medicine bag'/'EXO', 'Uncommon').
card_artist('medicine bag'/'EXO', 'DiTerlizzi').
card_number('medicine bag'/'EXO', '133').
card_flavor_text('medicine bag'/'EXO', '\"My medicine bag and I have treated countless wounds and illnesses. But never have I seen so many made so sick for so long. We will never eat Squee\'s cooking again.\"\n—Orim, journal').
card_multiverse_id('medicine bag'/'EXO', '6165').

card_in_set('memory crystal', 'EXO').
card_original_type('memory crystal'/'EXO', 'Artifact').
card_original_text('memory crystal'/'EXO', 'All buyback costs are reduced by {2}.').
card_first_print('memory crystal', 'EXO').
card_image_name('memory crystal'/'EXO', 'memory crystal').
card_uid('memory crystal'/'EXO', 'EXO:Memory Crystal:memory crystal').
card_rarity('memory crystal'/'EXO', 'Rare').
card_artist('memory crystal'/'EXO', 'Michael Sutfin').
card_number('memory crystal'/'EXO', '134').
card_flavor_text('memory crystal'/'EXO', 'Hues of recollection flicker like facets of a gem.').
card_multiverse_id('memory crystal'/'EXO', '6167').

card_in_set('merfolk looter', 'EXO').
card_original_type('merfolk looter'/'EXO', 'Summon — Merfolk').
card_original_text('merfolk looter'/'EXO', '{T}: Draw a card, then choose and discard a card.').
card_first_print('merfolk looter', 'EXO').
card_image_name('merfolk looter'/'EXO', 'merfolk looter').
card_uid('merfolk looter'/'EXO', 'EXO:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'EXO', 'Common').
card_artist('merfolk looter'/'EXO', 'Ron Spencer').
card_number('merfolk looter'/'EXO', '39').
card_flavor_text('merfolk looter'/'EXO', 'In the depths of Rootwater, salvage and sewage differ only in texture.').
card_multiverse_id('merfolk looter'/'EXO', '6056').

card_in_set('mind maggots', 'EXO').
card_original_type('mind maggots'/'EXO', 'Summon — Insects').
card_original_text('mind maggots'/'EXO', 'When Mind Maggots comes into play, choose and discard any number of creature cards. For each card discarded this way, put two +1/+1 counters on Mind Maggots.').
card_first_print('mind maggots', 'EXO').
card_image_name('mind maggots'/'EXO', 'mind maggots').
card_uid('mind maggots'/'EXO', 'EXO:Mind Maggots:mind maggots').
card_rarity('mind maggots'/'EXO', 'Uncommon').
card_artist('mind maggots'/'EXO', 'Ron Spencer').
card_number('mind maggots'/'EXO', '66').
card_multiverse_id('mind maggots'/'EXO', '6093').

card_in_set('mind over matter', 'EXO').
card_original_type('mind over matter'/'EXO', 'Enchantment').
card_original_text('mind over matter'/'EXO', 'Choose and discard a card: Tap or untap target artifact, creature, or land.').
card_first_print('mind over matter', 'EXO').
card_image_name('mind over matter'/'EXO', 'mind over matter').
card_uid('mind over matter'/'EXO', 'EXO:Mind Over Matter:mind over matter').
card_rarity('mind over matter'/'EXO', 'Rare').
card_artist('mind over matter'/'EXO', 'Keith Parkinson').
card_number('mind over matter'/'EXO', '40').
card_flavor_text('mind over matter'/'EXO', 'Lyna turned to the figure beside her. \"They\'re gone. What now?\"\n\"As ever,\" said Urza, \"we wait.\"').
card_multiverse_id('mind over matter'/'EXO', '6076').

card_in_set('mindless automaton', 'EXO').
card_original_type('mindless automaton'/'EXO', 'Artifact Creature').
card_original_text('mindless automaton'/'EXO', 'Mindless Automaton comes into play with two +1/+1 counters on it.\n{1}, Choose and discard a card: Put a +1/+1 counter on Mindless Automaton.\nRemove two +1/+1 counters from Mindless Automaton: Draw a card.').
card_first_print('mindless automaton', 'EXO').
card_image_name('mindless automaton'/'EXO', 'mindless automaton').
card_uid('mindless automaton'/'EXO', 'EXO:Mindless Automaton:mindless automaton').
card_rarity('mindless automaton'/'EXO', 'Rare').
card_artist('mindless automaton'/'EXO', 'Brian Snõddy').
card_number('mindless automaton'/'EXO', '135').
card_multiverse_id('mindless automaton'/'EXO', '5171').

card_in_set('mirozel', 'EXO').
card_original_type('mirozel'/'EXO', 'Summon — Illusion').
card_original_text('mirozel'/'EXO', 'Flying\nIf Mirozel is the target of any spell or ability, return Mirozel to owner\'s hand.').
card_first_print('mirozel', 'EXO').
card_image_name('mirozel'/'EXO', 'mirozel').
card_uid('mirozel'/'EXO', 'EXO:Mirozel:mirozel').
card_rarity('mirozel'/'EXO', 'Uncommon').
card_artist('mirozel'/'EXO', 'Jim Nelson').
card_number('mirozel'/'EXO', '41').
card_flavor_text('mirozel'/'EXO', 'The mirozels are the only stars in Rath\'s opaque skies.').
card_multiverse_id('mirozel'/'EXO', '5189').

card_in_set('mirri, cat warrior', 'EXO').
card_original_type('mirri, cat warrior'/'EXO', 'Summon — Legend').
card_original_text('mirri, cat warrior'/'EXO', 'Mirri, Cat Warrior counts as a Cat Warrior.\nFirst strike; forestwalk (If defending player controls any forests, this creature is unblockable.)\nAttacking does not cause Mirri to tap.').
card_first_print('mirri, cat warrior', 'EXO').
card_image_name('mirri, cat warrior'/'EXO', 'mirri, cat warrior').
card_uid('mirri, cat warrior'/'EXO', 'EXO:Mirri, Cat Warrior:mirri, cat warrior').
card_rarity('mirri, cat warrior'/'EXO', 'Rare').
card_artist('mirri, cat warrior'/'EXO', 'Daren Bader').
card_number('mirri, cat warrior'/'EXO', '114').
card_multiverse_id('mirri, cat warrior'/'EXO', '6155').

card_in_set('mogg assassin', 'EXO').
card_original_type('mogg assassin'/'EXO', 'Summon — Goblin').
card_original_text('mogg assassin'/'EXO', '{T}: Flip a coin. If you win the flip, destroy target creature an opponent controls. Otherwise, destroy target creature of that opponent\'s choice.').
card_first_print('mogg assassin', 'EXO').
card_image_name('mogg assassin'/'EXO', 'mogg assassin').
card_uid('mogg assassin'/'EXO', 'EXO:Mogg Assassin:mogg assassin').
card_rarity('mogg assassin'/'EXO', 'Uncommon').
card_artist('mogg assassin'/'EXO', 'Dermot Power').
card_number('mogg assassin'/'EXO', '88').
card_multiverse_id('mogg assassin'/'EXO', '6118').

card_in_set('monstrous hound', 'EXO').
card_original_type('monstrous hound'/'EXO', 'Summon — Hound').
card_original_text('monstrous hound'/'EXO', 'Monstrous Hound cannot attack unless you control more lands than defending player.\nMonstrous Hound cannot block unless you control more lands than attacking player.').
card_image_name('monstrous hound'/'EXO', 'monstrous hound').
card_uid('monstrous hound'/'EXO', 'EXO:Monstrous Hound:monstrous hound').
card_rarity('monstrous hound'/'EXO', 'Rare').
card_artist('monstrous hound'/'EXO', 'Dermot Power').
card_number('monstrous hound'/'EXO', '89').
card_flavor_text('monstrous hound'/'EXO', '\"Stay.\"').
card_multiverse_id('monstrous hound'/'EXO', '6128').

card_in_set('nausea', 'EXO').
card_original_type('nausea'/'EXO', 'Sorcery').
card_original_text('nausea'/'EXO', 'All creatures get -1/-1 until end of turn.').
card_first_print('nausea', 'EXO').
card_image_name('nausea'/'EXO', 'nausea').
card_uid('nausea'/'EXO', 'EXO:Nausea:nausea').
card_rarity('nausea'/'EXO', 'Common').
card_artist('nausea'/'EXO', 'Jeff Miracola').
card_number('nausea'/'EXO', '67').
card_flavor_text('nausea'/'EXO', 'Any mogg will tell you there\'s nothing more nauseating than the smell of cute.').
card_multiverse_id('nausea'/'EXO', '6088').

card_in_set('necrologia', 'EXO').
card_original_type('necrologia'/'EXO', 'Instant').
card_original_text('necrologia'/'EXO', 'Play Necrologia only during your discard phase.\nPay X life: Draw X cards.').
card_first_print('necrologia', 'EXO').
card_image_name('necrologia'/'EXO', 'necrologia').
card_uid('necrologia'/'EXO', 'EXO:Necrologia:necrologia').
card_rarity('necrologia'/'EXO', 'Uncommon').
card_artist('necrologia'/'EXO', 'Brom').
card_number('necrologia'/'EXO', '68').
card_flavor_text('necrologia'/'EXO', '\"My enemies\' death yields threefold benefit: removal, reuse, and research.\"\n—Volrath').
card_multiverse_id('necrologia'/'EXO', '6095').

card_in_set('null brooch', 'EXO').
card_original_type('null brooch'/'EXO', 'Artifact').
card_original_text('null brooch'/'EXO', '{2}, {T}, Discard your hand: Counter target noncreature spell. Play this ability as an interrupt.').
card_first_print('null brooch', 'EXO').
card_image_name('null brooch'/'EXO', 'null brooch').
card_uid('null brooch'/'EXO', 'EXO:Null Brooch:null brooch').
card_rarity('null brooch'/'EXO', 'Rare').
card_artist('null brooch'/'EXO', 'DiTerlizzi').
card_number('null brooch'/'EXO', '136').
card_flavor_text('null brooch'/'EXO', 'Give away everything so others have nothing.\n—Brooch inscription').
card_multiverse_id('null brooch'/'EXO', '5176').

card_in_set('oath of druids', 'EXO').
card_original_type('oath of druids'/'EXO', 'Enchantment').
card_original_text('oath of druids'/'EXO', 'During each player\'s upkeep, if that player controls fewer creatures than target opponent, the player may reveal cards from his or her library until he or she reveals a creature card. The player puts that creature into play and all other revealed cards into his or her graveyard.').
card_image_name('oath of druids'/'EXO', 'oath of druids').
card_uid('oath of druids'/'EXO', 'EXO:Oath of Druids:oath of druids').
card_rarity('oath of druids'/'EXO', 'Rare').
card_artist('oath of druids'/'EXO', 'Daren Bader').
card_number('oath of druids'/'EXO', '115').
card_multiverse_id('oath of druids'/'EXO', '6151').

card_in_set('oath of ghouls', 'EXO').
card_original_type('oath of ghouls'/'EXO', 'Enchantment').
card_original_text('oath of ghouls'/'EXO', 'During each player\'s upkeep, if there are more creature cards in that player\'s graveyard than in target opponent\'s graveyard, the player may return a creature card from his or her graveyard to his or her hand.').
card_first_print('oath of ghouls', 'EXO').
card_image_name('oath of ghouls'/'EXO', 'oath of ghouls').
card_uid('oath of ghouls'/'EXO', 'EXO:Oath of Ghouls:oath of ghouls').
card_rarity('oath of ghouls'/'EXO', 'Rare').
card_artist('oath of ghouls'/'EXO', 'Brom').
card_number('oath of ghouls'/'EXO', '69').
card_multiverse_id('oath of ghouls'/'EXO', '6098').

card_in_set('oath of lieges', 'EXO').
card_original_type('oath of lieges'/'EXO', 'Enchantment').
card_original_text('oath of lieges'/'EXO', 'During each player\'s upkeep, if that player controls fewer lands than target opponent, the player may search his or her library for a basic land card and put that land into play. The player shuffles his or her library afterwards.').
card_first_print('oath of lieges', 'EXO').
card_image_name('oath of lieges'/'EXO', 'oath of lieges').
card_uid('oath of lieges'/'EXO', 'EXO:Oath of Lieges:oath of lieges').
card_rarity('oath of lieges'/'EXO', 'Rare').
card_artist('oath of lieges'/'EXO', 'Mark Zug').
card_number('oath of lieges'/'EXO', '11').
card_multiverse_id('oath of lieges'/'EXO', '6048').

card_in_set('oath of mages', 'EXO').
card_original_type('oath of mages'/'EXO', 'Enchantment').
card_original_text('oath of mages'/'EXO', 'During each player\'s upkeep, if that player has less life than target opponent, he or she may have Oath of Mages deal 1 damage to that opponent.').
card_first_print('oath of mages', 'EXO').
card_image_name('oath of mages'/'EXO', 'oath of mages').
card_uid('oath of mages'/'EXO', 'EXO:Oath of Mages:oath of mages').
card_rarity('oath of mages'/'EXO', 'Rare').
card_artist('oath of mages'/'EXO', 'Keith Parkinson').
card_number('oath of mages'/'EXO', '90').
card_multiverse_id('oath of mages'/'EXO', '6124').

card_in_set('oath of scholars', 'EXO').
card_original_type('oath of scholars'/'EXO', 'Enchantment').
card_original_text('oath of scholars'/'EXO', 'During each player\'s upkeep, if that player has fewer cards in hand than target opponent, the player may discard his or her hand and draw three cards.').
card_first_print('oath of scholars', 'EXO').
card_image_name('oath of scholars'/'EXO', 'oath of scholars').
card_uid('oath of scholars'/'EXO', 'EXO:Oath of Scholars:oath of scholars').
card_rarity('oath of scholars'/'EXO', 'Rare').
card_artist('oath of scholars'/'EXO', 'Michael Sutfin').
card_number('oath of scholars'/'EXO', '42').
card_multiverse_id('oath of scholars'/'EXO', '6072').

card_in_set('ogre shaman', 'EXO').
card_original_type('ogre shaman'/'EXO', 'Summon — Ogre').
card_original_text('ogre shaman'/'EXO', '{2}, Discard a card at random: Ogre Shaman deals 2 damage to target creature or player.').
card_first_print('ogre shaman', 'EXO').
card_image_name('ogre shaman'/'EXO', 'ogre shaman').
card_uid('ogre shaman'/'EXO', 'EXO:Ogre Shaman:ogre shaman').
card_rarity('ogre shaman'/'EXO', 'Rare').
card_artist('ogre shaman'/'EXO', 'Paolo Parente').
card_number('ogre shaman'/'EXO', '91').
card_flavor_text('ogre shaman'/'EXO', 'Ogre shamans must be bright enough to learn their invocations and dim enough to use them.').
card_multiverse_id('ogre shaman'/'EXO', '6127').

card_in_set('onslaught', 'EXO').
card_original_type('onslaught'/'EXO', 'Enchantment').
card_original_text('onslaught'/'EXO', 'Whenever you successfully cast a creature spell, tap target creature.').
card_first_print('onslaught', 'EXO').
card_image_name('onslaught'/'EXO', 'onslaught').
card_uid('onslaught'/'EXO', 'EXO:Onslaught:onslaught').
card_rarity('onslaught'/'EXO', 'Common').
card_artist('onslaught'/'EXO', 'Paolo Parente').
card_number('onslaught'/'EXO', '92').
card_flavor_text('onslaught'/'EXO', 'The last thing to go through the mogg\'s mind was its teeth.').
card_multiverse_id('onslaught'/'EXO', '6115').

card_in_set('paladin en-vec', 'EXO').
card_original_type('paladin en-vec'/'EXO', 'Summon — Knight').
card_original_text('paladin en-vec'/'EXO', 'First strike, protection from black, protection from red').
card_first_print('paladin en-vec', 'EXO').
card_image_name('paladin en-vec'/'EXO', 'paladin en-vec').
card_uid('paladin en-vec'/'EXO', 'EXO:Paladin en-Vec:paladin en-vec').
card_rarity('paladin en-vec'/'EXO', 'Rare').
card_artist('paladin en-vec'/'EXO', 'Randy Elliott').
card_number('paladin en-vec'/'EXO', '12').
card_flavor_text('paladin en-vec'/'EXO', '\"Our belief shall be the lance that pierces Volrath\'s heart.\"').
card_multiverse_id('paladin en-vec'/'EXO', '5199').

card_in_set('pandemonium', 'EXO').
card_original_type('pandemonium'/'EXO', 'Enchantment').
card_original_text('pandemonium'/'EXO', 'Whenever any creature comes into play, that creature\'s controller may choose to have it deal damage equal to its power to target creature or player.').
card_first_print('pandemonium', 'EXO').
card_image_name('pandemonium'/'EXO', 'pandemonium').
card_uid('pandemonium'/'EXO', 'EXO:Pandemonium:pandemonium').
card_rarity('pandemonium'/'EXO', 'Rare').
card_artist('pandemonium'/'EXO', 'Pete Venters').
card_number('pandemonium'/'EXO', '93').
card_flavor_text('pandemonium'/'EXO', '\"If we cannot live proudly, we die so!\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('pandemonium'/'EXO', '6126').

card_in_set('paroxysm', 'EXO').
card_original_type('paroxysm'/'EXO', 'Enchant Creature').
card_original_text('paroxysm'/'EXO', 'During the upkeep of enchanted creature\'s controller, reveal the top card of that player\'s library to all players. If that card is a land card, destroy enchanted creature. Otherwise, enchanted creature gets +3/+3 until end of turn. (Return the card to the top of the player\'s library, face down.)').
card_first_print('paroxysm', 'EXO').
card_image_name('paroxysm'/'EXO', 'paroxysm').
card_uid('paroxysm'/'EXO', 'EXO:Paroxysm:paroxysm').
card_rarity('paroxysm'/'EXO', 'Uncommon').
card_artist('paroxysm'/'EXO', 'Scott Kirschner').
card_number('paroxysm'/'EXO', '94').
card_multiverse_id('paroxysm'/'EXO', '6113').

card_in_set('peace of mind', 'EXO').
card_original_type('peace of mind'/'EXO', 'Enchantment').
card_original_text('peace of mind'/'EXO', '{W}, Choose and discard a card: Gain 3 life.').
card_first_print('peace of mind', 'EXO').
card_image_name('peace of mind'/'EXO', 'peace of mind').
card_uid('peace of mind'/'EXO', 'EXO:Peace of Mind:peace of mind').
card_rarity('peace of mind'/'EXO', 'Uncommon').
card_artist('peace of mind'/'EXO', 'Randy Elliott').
card_number('peace of mind'/'EXO', '13').
card_flavor_text('peace of mind'/'EXO', 'Three are the paths of the soul:\nOne out—roam across the plain,\nOne back—dwell with tribe and tent,\nOne in—turn an enemy\'s hate.').
card_multiverse_id('peace of mind'/'EXO', '6039').

card_in_set('pegasus stampede', 'EXO').
card_original_type('pegasus stampede'/'EXO', 'Sorcery').
card_original_text('pegasus stampede'/'EXO', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs when you play this spell. If you do, put Pegasus Stampede into your hand instead of your graveyard as part of the spell\'s effect.)\nPut a Pegasus token into play. Treat this token as a 1/1 white creature with flying.').
card_first_print('pegasus stampede', 'EXO').
card_image_name('pegasus stampede'/'EXO', 'pegasus stampede').
card_uid('pegasus stampede'/'EXO', 'EXO:Pegasus Stampede:pegasus stampede').
card_rarity('pegasus stampede'/'EXO', 'Uncommon').
card_artist('pegasus stampede'/'EXO', 'Mark Zug').
card_number('pegasus stampede'/'EXO', '14').
card_multiverse_id('pegasus stampede'/'EXO', '6044').

card_in_set('penance', 'EXO').
card_original_type('penance'/'EXO', 'Enchantment').
card_original_text('penance'/'EXO', 'Choose a card from your hand and put that card on top of your library: Prevent all damage from a black or red source. (Treat further damage from that source normally.)').
card_first_print('penance', 'EXO').
card_image_name('penance'/'EXO', 'penance').
card_uid('penance'/'EXO', 'EXO:Penance:penance').
card_rarity('penance'/'EXO', 'Uncommon').
card_artist('penance'/'EXO', 'Terese Nielsen').
card_number('penance'/'EXO', '15').
card_multiverse_id('penance'/'EXO', '6047').

card_in_set('pit spawn', 'EXO').
card_original_type('pit spawn'/'EXO', 'Summon — Beast').
card_original_text('pit spawn'/'EXO', 'First strike\nDuring your upkeep, pay {B}{B} or sacrifice Pit Spawn.\nIf Pit Spawn damages any creature, remove that creature from the game.').
card_first_print('pit spawn', 'EXO').
card_image_name('pit spawn'/'EXO', 'pit spawn').
card_uid('pit spawn'/'EXO', 'EXO:Pit Spawn:pit spawn').
card_rarity('pit spawn'/'EXO', 'Rare').
card_artist('pit spawn'/'EXO', 'Thomas M. Baxa').
card_number('pit spawn'/'EXO', '70').
card_multiverse_id('pit spawn'/'EXO', '5133').

card_in_set('plaguebearer', 'EXO').
card_original_type('plaguebearer'/'EXO', 'Summon — Zombie').
card_original_text('plaguebearer'/'EXO', '{X}{X}{B} Destroy target nonblack creature with total casting cost equal to X.').
card_first_print('plaguebearer', 'EXO').
card_image_name('plaguebearer'/'EXO', 'plaguebearer').
card_uid('plaguebearer'/'EXO', 'EXO:Plaguebearer:plaguebearer').
card_rarity('plaguebearer'/'EXO', 'Rare').
card_artist('plaguebearer'/'EXO', 'Ron Spencer').
card_number('plaguebearer'/'EXO', '71').
card_flavor_text('plaguebearer'/'EXO', 'Its respiration is your expiration.').
card_multiverse_id('plaguebearer'/'EXO', '5200').

card_in_set('plated rootwalla', 'EXO').
card_original_type('plated rootwalla'/'EXO', 'Summon — Lizard').
card_original_text('plated rootwalla'/'EXO', '{2}{G} Plated Rootwalla gets +3/+3 until end of turn. Play this ability only once each turn.').
card_first_print('plated rootwalla', 'EXO').
card_image_name('plated rootwalla'/'EXO', 'plated rootwalla').
card_uid('plated rootwalla'/'EXO', 'EXO:Plated Rootwalla:plated rootwalla').
card_rarity('plated rootwalla'/'EXO', 'Common').
card_artist('plated rootwalla'/'EXO', 'Randy Elliott').
card_number('plated rootwalla'/'EXO', '116').
card_flavor_text('plated rootwalla'/'EXO', '\" . . . And the third little boar built his house out of rootwalla plates . . . .\"\n—Skyshroud children\'s story').
card_multiverse_id('plated rootwalla'/'EXO', '5201').

card_in_set('predatory hunger', 'EXO').
card_original_type('predatory hunger'/'EXO', 'Enchant Creature').
card_original_text('predatory hunger'/'EXO', 'Whenever any opponent successfully casts a creature spell, put a +1/+1 counter on enchanted creature.').
card_first_print('predatory hunger', 'EXO').
card_image_name('predatory hunger'/'EXO', 'predatory hunger').
card_uid('predatory hunger'/'EXO', 'EXO:Predatory Hunger:predatory hunger').
card_rarity('predatory hunger'/'EXO', 'Common').
card_artist('predatory hunger'/'EXO', 'Brom').
card_number('predatory hunger'/'EXO', '117').
card_flavor_text('predatory hunger'/'EXO', 'Hunger growls, never purrs.').
card_multiverse_id('predatory hunger'/'EXO', '6141').

card_in_set('price of progress', 'EXO').
card_original_type('price of progress'/'EXO', 'Instant').
card_original_text('price of progress'/'EXO', 'Price of Progress deals 2 damage to each player for each nonbasic land he or she controls.').
card_first_print('price of progress', 'EXO').
card_image_name('price of progress'/'EXO', 'price of progress').
card_uid('price of progress'/'EXO', 'EXO:Price of Progress:price of progress').
card_rarity('price of progress'/'EXO', 'Uncommon').
card_artist('price of progress'/'EXO', 'Richard Kane Ferguson').
card_number('price of progress'/'EXO', '95').
card_flavor_text('price of progress'/'EXO', 'Man versus nature is not a fair fight.').
card_multiverse_id('price of progress'/'EXO', '6123').

card_in_set('pygmy troll', 'EXO').
card_original_type('pygmy troll'/'EXO', 'Summon — Troll').
card_original_text('pygmy troll'/'EXO', 'For each creature that blocks it, Pygmy Troll gets +1/+1 until end of turn.\n{G} Regenerate Pygmy Troll.').
card_first_print('pygmy troll', 'EXO').
card_image_name('pygmy troll'/'EXO', 'pygmy troll').
card_uid('pygmy troll'/'EXO', 'EXO:Pygmy Troll:pygmy troll').
card_rarity('pygmy troll'/'EXO', 'Common').
card_artist('pygmy troll'/'EXO', 'Daniel Gelon').
card_number('pygmy troll'/'EXO', '118').
card_flavor_text('pygmy troll'/'EXO', '\"Who you callin\' ‘shorty\'?\"').
card_multiverse_id('pygmy troll'/'EXO', '6131').

card_in_set('rabid wolverines', 'EXO').
card_original_type('rabid wolverines'/'EXO', 'Summon — Wolverines').
card_original_text('rabid wolverines'/'EXO', 'For each creature that blocks it, Rabid Wolverines gets +1/+1 until end of turn.').
card_first_print('rabid wolverines', 'EXO').
card_image_name('rabid wolverines'/'EXO', 'rabid wolverines').
card_uid('rabid wolverines'/'EXO', 'EXO:Rabid Wolverines:rabid wolverines').
card_rarity('rabid wolverines'/'EXO', 'Common').
card_artist('rabid wolverines'/'EXO', 'Daren Bader').
card_number('rabid wolverines'/'EXO', '119').
card_flavor_text('rabid wolverines'/'EXO', 'Is there any other kind?').
card_multiverse_id('rabid wolverines'/'EXO', '6134').

card_in_set('raging goblin', 'EXO').
card_original_type('raging goblin'/'EXO', 'Summon — Goblin').
card_original_text('raging goblin'/'EXO', 'Raging Goblin is unaffected by summoning sickness.').
card_image_name('raging goblin'/'EXO', 'raging goblin').
card_uid('raging goblin'/'EXO', 'EXO:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'EXO', 'Common').
card_artist('raging goblin'/'EXO', 'Brian Snõddy').
card_number('raging goblin'/'EXO', '96').
card_flavor_text('raging goblin'/'EXO', 'Volrath has bred them to fear only him. Are they charging into battle, or merely fleeing his wrath?').
card_multiverse_id('raging goblin'/'EXO', '6106').

card_in_set('ravenous baboons', 'EXO').
card_original_type('ravenous baboons'/'EXO', 'Summon — Apes').
card_original_text('ravenous baboons'/'EXO', 'When Ravenous Baboons comes into play, destroy target nonbasic land.').
card_first_print('ravenous baboons', 'EXO').
card_image_name('ravenous baboons'/'EXO', 'ravenous baboons').
card_uid('ravenous baboons'/'EXO', 'EXO:Ravenous Baboons:ravenous baboons').
card_rarity('ravenous baboons'/'EXO', 'Rare').
card_artist('ravenous baboons'/'EXO', 'Daren Bader').
card_number('ravenous baboons'/'EXO', '97').
card_flavor_text('ravenous baboons'/'EXO', 'Build something and the baboons will tear it down. Leave the pieces lying and they will scatter them.').
card_multiverse_id('ravenous baboons'/'EXO', '5207').

card_in_set('reaping the rewards', 'EXO').
card_original_type('reaping the rewards'/'EXO', 'Instant').
card_original_text('reaping the rewards'/'EXO', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs when you play this spell. If you do, put Reaping the Rewards into your hand instead of your graveyard as part of the spell\'s effect.)\nGain 2 life.').
card_first_print('reaping the rewards', 'EXO').
card_image_name('reaping the rewards'/'EXO', 'reaping the rewards').
card_uid('reaping the rewards'/'EXO', 'EXO:Reaping the Rewards:reaping the rewards').
card_rarity('reaping the rewards'/'EXO', 'Common').
card_artist('reaping the rewards'/'EXO', 'Heather Hudson').
card_number('reaping the rewards'/'EXO', '16').
card_multiverse_id('reaping the rewards'/'EXO', '6037').

card_in_set('reckless ogre', 'EXO').
card_original_type('reckless ogre'/'EXO', 'Summon — Ogre').
card_original_text('reckless ogre'/'EXO', 'If Reckless Ogre attacks and no other creatures do, it gets +3/+0 until end of turn.').
card_first_print('reckless ogre', 'EXO').
card_image_name('reckless ogre'/'EXO', 'reckless ogre').
card_uid('reckless ogre'/'EXO', 'EXO:Reckless Ogre:reckless ogre').
card_rarity('reckless ogre'/'EXO', 'Common').
card_artist('reckless ogre'/'EXO', 'Paolo Parente').
card_number('reckless ogre'/'EXO', '98').
card_flavor_text('reckless ogre'/'EXO', 'There is no such thing as a regiment of ogres.').
card_multiverse_id('reckless ogre'/'EXO', '6108').

card_in_set('reclaim', 'EXO').
card_original_type('reclaim'/'EXO', 'Instant').
card_original_text('reclaim'/'EXO', 'Put target card from your graveyard on top of your library.').
card_first_print('reclaim', 'EXO').
card_image_name('reclaim'/'EXO', 'reclaim').
card_uid('reclaim'/'EXO', 'EXO:Reclaim:reclaim').
card_rarity('reclaim'/'EXO', 'Common').
card_artist('reclaim'/'EXO', 'Andrew Robinson').
card_number('reclaim'/'EXO', '120').
card_flavor_text('reclaim'/'EXO', 'The wise pay as much attention to what they throw away as to what they keep.').
card_multiverse_id('reclaim'/'EXO', '6139').

card_in_set('reconnaissance', 'EXO').
card_original_type('reconnaissance'/'EXO', 'Enchantment').
card_original_text('reconnaissance'/'EXO', '{0}: Remove target attacking creature you control from combat and untap it. (That creature neither deals nor receives combat damage this turn.)').
card_first_print('reconnaissance', 'EXO').
card_image_name('reconnaissance'/'EXO', 'reconnaissance').
card_uid('reconnaissance'/'EXO', 'EXO:Reconnaissance:reconnaissance').
card_rarity('reconnaissance'/'EXO', 'Uncommon').
card_artist('reconnaissance'/'EXO', 'Val Mayerik').
card_number('reconnaissance'/'EXO', '17').
card_flavor_text('reconnaissance'/'EXO', 'War favors the informed.').
card_multiverse_id('reconnaissance'/'EXO', '6046').

card_in_set('recurring nightmare', 'EXO').
card_original_type('recurring nightmare'/'EXO', 'Enchantment').
card_original_text('recurring nightmare'/'EXO', 'Sacrifice a creature, Return Recurring Nightmare to owner\'s hand: Put target creature card from your graveyard into play. Play this ability as a sorcery.').
card_first_print('recurring nightmare', 'EXO').
card_image_name('recurring nightmare'/'EXO', 'recurring nightmare').
card_uid('recurring nightmare'/'EXO', 'EXO:Recurring Nightmare:recurring nightmare').
card_rarity('recurring nightmare'/'EXO', 'Rare').
card_artist('recurring nightmare'/'EXO', 'Jeff Laubenstein').
card_number('recurring nightmare'/'EXO', '72').
card_flavor_text('recurring nightmare'/'EXO', '\"I am confined by sleep and defined by nightmare.\"\n—Crovax').
card_multiverse_id('recurring nightmare'/'EXO', '6103').

card_in_set('resuscitate', 'EXO').
card_original_type('resuscitate'/'EXO', 'Instant').
card_original_text('resuscitate'/'EXO', 'Until end of turn, each creature you control gains \"{1}: Regenerate this creature.\"').
card_first_print('resuscitate', 'EXO').
card_image_name('resuscitate'/'EXO', 'resuscitate').
card_uid('resuscitate'/'EXO', 'EXO:Resuscitate:resuscitate').
card_rarity('resuscitate'/'EXO', 'Uncommon').
card_artist('resuscitate'/'EXO', 'Rebecca Guay').
card_number('resuscitate'/'EXO', '121').
card_flavor_text('resuscitate'/'EXO', '\"It is the art of convincing someone to live.\"\n—Orim, Samite healer').
card_multiverse_id('resuscitate'/'EXO', '6140').

card_in_set('robe of mirrors', 'EXO').
card_original_type('robe of mirrors'/'EXO', 'Enchant Creature').
card_original_text('robe of mirrors'/'EXO', 'Enchanted creature cannot be the target of spells or abilities.').
card_first_print('robe of mirrors', 'EXO').
card_image_name('robe of mirrors'/'EXO', 'robe of mirrors').
card_uid('robe of mirrors'/'EXO', 'EXO:Robe of Mirrors:robe of mirrors').
card_rarity('robe of mirrors'/'EXO', 'Common').
card_artist('robe of mirrors'/'EXO', 'John Matson').
card_number('robe of mirrors'/'EXO', '43').
card_flavor_text('robe of mirrors'/'EXO', '\"Does this make me look fat?\"').
card_multiverse_id('robe of mirrors'/'EXO', '6059').

card_in_set('rootwater alligator', 'EXO').
card_original_type('rootwater alligator'/'EXO', 'Summon — Alligator').
card_original_text('rootwater alligator'/'EXO', 'Sacrifice a forest: Regenerate Rootwater Alligator.').
card_first_print('rootwater alligator', 'EXO').
card_image_name('rootwater alligator'/'EXO', 'rootwater alligator').
card_uid('rootwater alligator'/'EXO', 'EXO:Rootwater Alligator:rootwater alligator').
card_rarity('rootwater alligator'/'EXO', 'Common').
card_artist('rootwater alligator'/'EXO', 'Stephen Daniele').
card_number('rootwater alligator'/'EXO', '122').
card_flavor_text('rootwater alligator'/'EXO', 'Subduing and skinning an alligator is the first rite of maturity adolescent merfolk must pass.').
card_multiverse_id('rootwater alligator'/'EXO', '6136').

card_in_set('rootwater mystic', 'EXO').
card_original_type('rootwater mystic'/'EXO', 'Summon — Merfolk').
card_original_text('rootwater mystic'/'EXO', '{1}{U} Look at the top card of target player\'s library.').
card_first_print('rootwater mystic', 'EXO').
card_image_name('rootwater mystic'/'EXO', 'rootwater mystic').
card_uid('rootwater mystic'/'EXO', 'EXO:Rootwater Mystic:rootwater mystic').
card_rarity('rootwater mystic'/'EXO', 'Common').
card_artist('rootwater mystic'/'EXO', 'Michael Sutfin').
card_number('rootwater mystic'/'EXO', '44').
card_flavor_text('rootwater mystic'/'EXO', '\"Read the roots    tell the tale\nFuture forms    in waving weeds\nHigher than truth   is hope.\"\n—Rootwater Saga').
card_multiverse_id('rootwater mystic'/'EXO', '6055').

card_in_set('sabertooth wyvern', 'EXO').
card_original_type('sabertooth wyvern'/'EXO', 'Summon — Drake').
card_original_text('sabertooth wyvern'/'EXO', 'Flying, first strike').
card_first_print('sabertooth wyvern', 'EXO').
card_image_name('sabertooth wyvern'/'EXO', 'sabertooth wyvern').
card_uid('sabertooth wyvern'/'EXO', 'EXO:Sabertooth Wyvern:sabertooth wyvern').
card_rarity('sabertooth wyvern'/'EXO', 'Uncommon').
card_artist('sabertooth wyvern'/'EXO', 'Keith Parkinson').
card_number('sabertooth wyvern'/'EXO', '99').
card_flavor_text('sabertooth wyvern'/'EXO', 'Most creatures grow out of their baby teeth, but sabertooth wyverns grow into theirs.').
card_multiverse_id('sabertooth wyvern'/'EXO', '6129').

card_in_set('scalding salamander', 'EXO').
card_original_type('scalding salamander'/'EXO', 'Summon — Salamander').
card_original_text('scalding salamander'/'EXO', '{0}: Scalding Salamander deals 1 damage to each creature without flying defending player controls. Play this ability only if Scalding Salamander is attacking and only once each turn.').
card_first_print('scalding salamander', 'EXO').
card_image_name('scalding salamander'/'EXO', 'scalding salamander').
card_uid('scalding salamander'/'EXO', 'EXO:Scalding Salamander:scalding salamander').
card_rarity('scalding salamander'/'EXO', 'Uncommon').
card_artist('scalding salamander'/'EXO', 'Terese Nielsen').
card_number('scalding salamander'/'EXO', '100').
card_multiverse_id('scalding salamander'/'EXO', '6117').

card_in_set('scare tactics', 'EXO').
card_original_type('scare tactics'/'EXO', 'Instant').
card_original_text('scare tactics'/'EXO', 'All creatures you control get +1/+0 until end of turn.').
card_first_print('scare tactics', 'EXO').
card_image_name('scare tactics'/'EXO', 'scare tactics').
card_uid('scare tactics'/'EXO', 'EXO:Scare Tactics:scare tactics').
card_rarity('scare tactics'/'EXO', 'Common').
card_artist('scare tactics'/'EXO', 'DiTerlizzi').
card_number('scare tactics'/'EXO', '73').
card_flavor_text('scare tactics'/'EXO', '\"Fear rules those weak enough to accept it.\"\n—Greven il-Vec').
card_multiverse_id('scare tactics'/'EXO', '6086').

card_in_set('school of piranha', 'EXO').
card_original_type('school of piranha'/'EXO', 'Summon — Fish').
card_original_text('school of piranha'/'EXO', 'During your upkeep, pay {1}{U} or sacrifice School of Piranha.').
card_first_print('school of piranha', 'EXO').
card_image_name('school of piranha'/'EXO', 'school of piranha').
card_uid('school of piranha'/'EXO', 'EXO:School of Piranha:school of piranha').
card_rarity('school of piranha'/'EXO', 'Common').
card_artist('school of piranha'/'EXO', 'Daren Bader').
card_number('school of piranha'/'EXO', '45').
card_flavor_text('school of piranha'/'EXO', 'Some schools aren\'t worth getting into.').
card_multiverse_id('school of piranha'/'EXO', '6054').

card_in_set('scrivener', 'EXO').
card_original_type('scrivener'/'EXO', 'Summon — Townsfolk').
card_original_text('scrivener'/'EXO', 'When Scrivener comes into play, you may return target instant or interrupt card from your graveyard to your hand.').
card_first_print('scrivener', 'EXO').
card_image_name('scrivener'/'EXO', 'scrivener').
card_uid('scrivener'/'EXO', 'EXO:Scrivener:scrivener').
card_rarity('scrivener'/'EXO', 'Uncommon').
card_artist('scrivener'/'EXO', 'Heather Hudson').
card_number('scrivener'/'EXO', '46').
card_flavor_text('scrivener'/'EXO', '\"History is a potent weapon.\"\n—Karn, silver golem').
card_multiverse_id('scrivener'/'EXO', '6067').

card_in_set('seismic assault', 'EXO').
card_original_type('seismic assault'/'EXO', 'Enchantment').
card_original_text('seismic assault'/'EXO', 'Choose and discard a land card: Seismic Assault deals 2 damage to target creature or player.').
card_first_print('seismic assault', 'EXO').
card_image_name('seismic assault'/'EXO', 'seismic assault').
card_uid('seismic assault'/'EXO', 'EXO:Seismic Assault:seismic assault').
card_rarity('seismic assault'/'EXO', 'Rare').
card_artist('seismic assault'/'EXO', 'Dermot Power').
card_number('seismic assault'/'EXO', '101').
card_flavor_text('seismic assault'/'EXO', 'The flowstone seemed for a moment to assume Greven\'s rage—it lunged like a wild beast at the Weatherlight.').
card_multiverse_id('seismic assault'/'EXO', '6130').

card_in_set('shackles', 'EXO').
card_original_type('shackles'/'EXO', 'Enchant Creature').
card_original_text('shackles'/'EXO', 'Enchanted creature does not untap during its controller\'s untap phase.\n{W} Return Shackles to owner\'s hand.').
card_first_print('shackles', 'EXO').
card_image_name('shackles'/'EXO', 'shackles').
card_uid('shackles'/'EXO', 'EXO:Shackles:shackles').
card_rarity('shackles'/'EXO', 'Common').
card_artist('shackles'/'EXO', 'Heather Hudson').
card_number('shackles'/'EXO', '18').
card_flavor_text('shackles'/'EXO', 'Shackles of gold are still shackles.').
card_multiverse_id('shackles'/'EXO', '6038').

card_in_set('shattering pulse', 'EXO').
card_original_type('shattering pulse'/'EXO', 'Instant').
card_original_text('shattering pulse'/'EXO', 'Buyback {3} (You may pay an additional {3} when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nDestroy target artifact.').
card_first_print('shattering pulse', 'EXO').
card_image_name('shattering pulse'/'EXO', 'shattering pulse').
card_uid('shattering pulse'/'EXO', 'EXO:Shattering Pulse:shattering pulse').
card_rarity('shattering pulse'/'EXO', 'Common').
card_artist('shattering pulse'/'EXO', 'Donato Giancola').
card_number('shattering pulse'/'EXO', '102').
card_multiverse_id('shattering pulse'/'EXO', '5221').

card_in_set('shield mate', 'EXO').
card_original_type('shield mate'/'EXO', 'Summon — Soldier').
card_original_text('shield mate'/'EXO', 'Sacrifice Shield Mate: Target creature gets +0/+4 until end of turn.').
card_first_print('shield mate', 'EXO').
card_image_name('shield mate'/'EXO', 'shield mate').
card_uid('shield mate'/'EXO', 'EXO:Shield Mate:shield mate').
card_rarity('shield mate'/'EXO', 'Common').
card_artist('shield mate'/'EXO', 'Randy Elliott').
card_number('shield mate'/'EXO', '19').
card_flavor_text('shield mate'/'EXO', 'They bear weightier burdens than the shields they carry.').
card_multiverse_id('shield mate'/'EXO', '6030').

card_in_set('skyshaper', 'EXO').
card_original_type('skyshaper'/'EXO', 'Artifact').
card_original_text('skyshaper'/'EXO', 'Sacrifice Skyshaper: All creatures you control gain flying until end of turn.').
card_first_print('skyshaper', 'EXO').
card_image_name('skyshaper'/'EXO', 'skyshaper').
card_uid('skyshaper'/'EXO', 'EXO:Skyshaper:skyshaper').
card_rarity('skyshaper'/'EXO', 'Uncommon').
card_artist('skyshaper'/'EXO', 'Donato Giancola').
card_number('skyshaper'/'EXO', '137').
card_flavor_text('skyshaper'/'EXO', '\"It\'ll get us to the portal, but I can\'t guarantee what it\'ll do to the ship.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('skyshaper'/'EXO', '5227').

card_in_set('skyshroud elite', 'EXO').
card_original_type('skyshroud elite'/'EXO', 'Summon — Elves').
card_original_text('skyshroud elite'/'EXO', 'Skyshroud Elite gets +1/+2 as long as any opponent controls any nonbasic lands.').
card_first_print('skyshroud elite', 'EXO').
card_image_name('skyshroud elite'/'EXO', 'skyshroud elite').
card_uid('skyshroud elite'/'EXO', 'EXO:Skyshroud Elite:skyshroud elite').
card_rarity('skyshroud elite'/'EXO', 'Uncommon').
card_artist('skyshroud elite'/'EXO', 'Paolo Parente').
card_number('skyshroud elite'/'EXO', '123').
card_flavor_text('skyshroud elite'/'EXO', '\"Civilization is a conspiracy to disguise the mutilation of nature.\"\n—Skyshroud elite creed').
card_multiverse_id('skyshroud elite'/'EXO', '5229').

card_in_set('skyshroud war beast', 'EXO').
card_original_type('skyshroud war beast'/'EXO', 'Summon — Beast').
card_original_text('skyshroud war beast'/'EXO', 'Trample\nSkyshroud War Beast has power and toughness each equal to the number of nonbasic lands target opponent controls.').
card_first_print('skyshroud war beast', 'EXO').
card_image_name('skyshroud war beast'/'EXO', 'skyshroud war beast').
card_uid('skyshroud war beast'/'EXO', 'EXO:Skyshroud War Beast:skyshroud war beast').
card_rarity('skyshroud war beast'/'EXO', 'Rare').
card_artist('skyshroud war beast'/'EXO', 'Jim Nelson').
card_number('skyshroud war beast'/'EXO', '124').
card_multiverse_id('skyshroud war beast'/'EXO', '5231').

card_in_set('slaughter', 'EXO').
card_original_type('slaughter'/'EXO', 'Instant').
card_original_text('slaughter'/'EXO', 'Buyback—Pay 4 life. (You may pay 4 life in addition to any other costs when you play this spell. If you do, put it into your hand instead of your graveyard as part of the spell\'s effect.)\nDestroy target nonblack creature. That creature cannot be regenerated this turn.').
card_first_print('slaughter', 'EXO').
card_image_name('slaughter'/'EXO', 'slaughter').
card_uid('slaughter'/'EXO', 'EXO:Slaughter:slaughter').
card_rarity('slaughter'/'EXO', 'Uncommon').
card_artist('slaughter'/'EXO', 'Pete Venters').
card_number('slaughter'/'EXO', '74').
card_multiverse_id('slaughter'/'EXO', '6097').

card_in_set('soltari visionary', 'EXO').
card_original_type('soltari visionary'/'EXO', 'Summon — Cleric').
card_original_text('soltari visionary'/'EXO', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nIf Soltari Visionary damages any player, destroy target enchantment that player controls.').
card_first_print('soltari visionary', 'EXO').
card_image_name('soltari visionary'/'EXO', 'soltari visionary').
card_uid('soltari visionary'/'EXO', 'EXO:Soltari Visionary:soltari visionary').
card_rarity('soltari visionary'/'EXO', 'Common').
card_artist('soltari visionary'/'EXO', 'Adam Rex').
card_number('soltari visionary'/'EXO', '20').
card_multiverse_id('soltari visionary'/'EXO', '6031').

card_in_set('song of serenity', 'EXO').
card_original_type('song of serenity'/'EXO', 'Enchantment').
card_original_text('song of serenity'/'EXO', 'Creatures with any enchantments on them cannot attack or block.').
card_first_print('song of serenity', 'EXO').
card_image_name('song of serenity'/'EXO', 'song of serenity').
card_uid('song of serenity'/'EXO', 'EXO:Song of Serenity:song of serenity').
card_rarity('song of serenity'/'EXO', 'Uncommon').
card_artist('song of serenity'/'EXO', 'DiTerlizzi').
card_number('song of serenity'/'EXO', '125').
card_flavor_text('song of serenity'/'EXO', 'Heartwood brings peace in great symphonies where dryads conduct the trees through every note.').
card_multiverse_id('song of serenity'/'EXO', '6149').

card_in_set('sonic burst', 'EXO').
card_original_type('sonic burst'/'EXO', 'Instant').
card_original_text('sonic burst'/'EXO', 'Discard a card at random: Sonic Burst deals 4 damage to target creature or player.').
card_first_print('sonic burst', 'EXO').
card_image_name('sonic burst'/'EXO', 'sonic burst').
card_uid('sonic burst'/'EXO', 'EXO:Sonic Burst:sonic burst').
card_rarity('sonic burst'/'EXO', 'Common').
card_artist('sonic burst'/'EXO', 'Brian Snõddy').
card_number('sonic burst'/'EXO', '103').
card_flavor_text('sonic burst'/'EXO', 'Music scythes the savage beast.').
card_multiverse_id('sonic burst'/'EXO', '6112').

card_in_set('soul warden', 'EXO').
card_original_type('soul warden'/'EXO', 'Summon — Cleric').
card_original_text('soul warden'/'EXO', 'Whenever any other creature comes into play, gain 1 life.').
card_first_print('soul warden', 'EXO').
card_image_name('soul warden'/'EXO', 'soul warden').
card_uid('soul warden'/'EXO', 'EXO:Soul Warden:soul warden').
card_rarity('soul warden'/'EXO', 'Common').
card_artist('soul warden'/'EXO', 'Randy Gallegos').
card_number('soul warden'/'EXO', '21').
card_flavor_text('soul warden'/'EXO', 'Count carefully the souls and see that none are lost.\n—Vec teaching').
card_multiverse_id('soul warden'/'EXO', '6033').

card_in_set('spellbook', 'EXO').
card_original_type('spellbook'/'EXO', 'Artifact').
card_original_text('spellbook'/'EXO', 'Skip your discard phase.').
card_first_print('spellbook', 'EXO').
card_image_name('spellbook'/'EXO', 'spellbook').
card_uid('spellbook'/'EXO', 'EXO:Spellbook:spellbook').
card_rarity('spellbook'/'EXO', 'Uncommon').
card_artist('spellbook'/'EXO', 'Ciruelo').
card_number('spellbook'/'EXO', '138').
card_flavor_text('spellbook'/'EXO', '\"Everything the wise woman learned she wrote in a book, and when the pages were black with ink, she took white ink and began again.\"\n—Karn, silver golem').
card_multiverse_id('spellbook'/'EXO', '6158').

card_in_set('spellshock', 'EXO').
card_original_type('spellshock'/'EXO', 'Enchantment').
card_original_text('spellshock'/'EXO', 'Whenever any player successfully casts a spell, Spellshock deals 2 damage to him or her.').
card_first_print('spellshock', 'EXO').
card_image_name('spellshock'/'EXO', 'spellshock').
card_uid('spellshock'/'EXO', 'EXO:Spellshock:spellshock').
card_rarity('spellshock'/'EXO', 'Uncommon').
card_artist('spellshock'/'EXO', 'Thomas M. Baxa').
card_number('spellshock'/'EXO', '104').
card_flavor_text('spellshock'/'EXO', 'A snap of fingers, a snap of teeth.').
card_multiverse_id('spellshock'/'EXO', '6121').

card_in_set('sphere of resistance', 'EXO').
card_original_type('sphere of resistance'/'EXO', 'Artifact').
card_original_text('sphere of resistance'/'EXO', 'All spells cost an additional {1} to play.').
card_first_print('sphere of resistance', 'EXO').
card_image_name('sphere of resistance'/'EXO', 'sphere of resistance').
card_uid('sphere of resistance'/'EXO', 'EXO:Sphere of Resistance:sphere of resistance').
card_rarity('sphere of resistance'/'EXO', 'Rare').
card_artist('sphere of resistance'/'EXO', 'Doug Chaffee').
card_number('sphere of resistance'/'EXO', '139').
card_flavor_text('sphere of resistance'/'EXO', 'A sphere pushes equally in all directions.').
card_multiverse_id('sphere of resistance'/'EXO', '6160').

card_in_set('spike cannibal', 'EXO').
card_original_type('spike cannibal'/'EXO', 'Summon — Spike').
card_original_text('spike cannibal'/'EXO', 'Spike Cannibal comes into play with one +1/+1 counter on it.\nWhen Spike Cannibal comes into play, move all +1/+1 counters from all creatures onto Spike Cannibal.').
card_first_print('spike cannibal', 'EXO').
card_image_name('spike cannibal'/'EXO', 'spike cannibal').
card_uid('spike cannibal'/'EXO', 'EXO:Spike Cannibal:spike cannibal').
card_rarity('spike cannibal'/'EXO', 'Uncommon').
card_artist('spike cannibal'/'EXO', 'Joel Biske').
card_number('spike cannibal'/'EXO', '75').
card_multiverse_id('spike cannibal'/'EXO', '6092').

card_in_set('spike hatcher', 'EXO').
card_original_type('spike hatcher'/'EXO', 'Summon — Spike').
card_original_text('spike hatcher'/'EXO', 'Spike Hatcher comes into play with six +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Hatcher: Put a +1/+1 counter on target creature.\n{1}, Remove a +1/+1 counter from Spike Hatcher: Regenerate Spike Hatcher.').
card_first_print('spike hatcher', 'EXO').
card_image_name('spike hatcher'/'EXO', 'spike hatcher').
card_uid('spike hatcher'/'EXO', 'EXO:Spike Hatcher:spike hatcher').
card_rarity('spike hatcher'/'EXO', 'Rare').
card_artist('spike hatcher'/'EXO', 'Stephen Daniele').
card_number('spike hatcher'/'EXO', '126').
card_multiverse_id('spike hatcher'/'EXO', '9842').

card_in_set('spike rogue', 'EXO').
card_original_type('spike rogue'/'EXO', 'Summon — Spike').
card_original_text('spike rogue'/'EXO', 'Spike Rogue comes into play with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Rogue: Put a +1/+1 counter on target creature.\n{2}, Remove a +1/+1 counter from any creature you control: Put a +1/+1 counter on Spike Rogue.').
card_first_print('spike rogue', 'EXO').
card_image_name('spike rogue'/'EXO', 'spike rogue').
card_uid('spike rogue'/'EXO', 'EXO:Spike Rogue:spike rogue').
card_rarity('spike rogue'/'EXO', 'Uncommon').
card_artist('spike rogue'/'EXO', 'Heather Hudson').
card_number('spike rogue'/'EXO', '127').
card_multiverse_id('spike rogue'/'EXO', '6143').

card_in_set('spike weaver', 'EXO').
card_original_type('spike weaver'/'EXO', 'Summon — Spike').
card_original_text('spike weaver'/'EXO', 'Spike Weaver comes into play with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Weaver: Put a +1/+1 counter on target creature.\n{1}, Remove a +1/+1 counter from Spike Weaver: Creatures deal no combat damage this turn.').
card_first_print('spike weaver', 'EXO').
card_image_name('spike weaver'/'EXO', 'spike weaver').
card_uid('spike weaver'/'EXO', 'EXO:Spike Weaver:spike weaver').
card_rarity('spike weaver'/'EXO', 'Rare').
card_artist('spike weaver'/'EXO', 'Mike Raabe').
card_number('spike weaver'/'EXO', '128').
card_multiverse_id('spike weaver'/'EXO', '6152').

card_in_set('standing troops', 'EXO').
card_original_type('standing troops'/'EXO', 'Summon — Soldiers').
card_original_text('standing troops'/'EXO', 'Attacking does not cause Standing Troops to tap.').
card_first_print('standing troops', 'EXO').
card_image_name('standing troops'/'EXO', 'standing troops').
card_uid('standing troops'/'EXO', 'EXO:Standing Troops:standing troops').
card_rarity('standing troops'/'EXO', 'Common').
card_artist('standing troops'/'EXO', 'Daren Bader').
card_number('standing troops'/'EXO', '22').
card_flavor_text('standing troops'/'EXO', 'The less you have, the harder you fight for it.').
card_multiverse_id('standing troops'/'EXO', '6032').

card_in_set('survival of the fittest', 'EXO').
card_original_type('survival of the fittest'/'EXO', 'Enchantment').
card_original_text('survival of the fittest'/'EXO', '{G}, Choose and discard a creature card: Search your library for a creature card, reveal that card to all players, and put it into your hand. Shuffle your library afterwards.').
card_image_name('survival of the fittest'/'EXO', 'survival of the fittest').
card_uid('survival of the fittest'/'EXO', 'EXO:Survival of the Fittest:survival of the fittest').
card_rarity('survival of the fittest'/'EXO', 'Rare').
card_artist('survival of the fittest'/'EXO', 'Pete Venters').
card_number('survival of the fittest'/'EXO', '129').
card_multiverse_id('survival of the fittest'/'EXO', '6150').

card_in_set('thalakos drifters', 'EXO').
card_original_type('thalakos drifters'/'EXO', 'Summon — Townsfolk').
card_original_text('thalakos drifters'/'EXO', 'Choose and discard a card: Thalakos Drifters gains shadow until end of turn. (This creature can block or be blocked by only creatures with shadow.)').
card_first_print('thalakos drifters', 'EXO').
card_image_name('thalakos drifters'/'EXO', 'thalakos drifters').
card_uid('thalakos drifters'/'EXO', 'EXO:Thalakos Drifters:thalakos drifters').
card_rarity('thalakos drifters'/'EXO', 'Rare').
card_artist('thalakos drifters'/'EXO', 'Andrew Robinson').
card_number('thalakos drifters'/'EXO', '47').
card_flavor_text('thalakos drifters'/'EXO', 'They drift in and out of shadow and ever on toward madness.').
card_multiverse_id('thalakos drifters'/'EXO', '6068').

card_in_set('thalakos scout', 'EXO').
card_original_type('thalakos scout'/'EXO', 'Summon — Soldier').
card_original_text('thalakos scout'/'EXO', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nChoose and discard a card: Return Thalakos Scout to owner\'s hand.').
card_first_print('thalakos scout', 'EXO').
card_image_name('thalakos scout'/'EXO', 'thalakos scout').
card_uid('thalakos scout'/'EXO', 'EXO:Thalakos Scout:thalakos scout').
card_rarity('thalakos scout'/'EXO', 'Common').
card_artist('thalakos scout'/'EXO', 'Daren Bader').
card_number('thalakos scout'/'EXO', '48').
card_multiverse_id('thalakos scout'/'EXO', '6058').

card_in_set('theft of dreams', 'EXO').
card_original_type('theft of dreams'/'EXO', 'Sorcery').
card_original_text('theft of dreams'/'EXO', 'For each tapped creature target opponent controls, draw a card.').
card_image_name('theft of dreams'/'EXO', 'theft of dreams').
card_uid('theft of dreams'/'EXO', 'EXO:Theft of Dreams:theft of dreams').
card_rarity('theft of dreams'/'EXO', 'Common').
card_artist('theft of dreams'/'EXO', 'Richard Kane Ferguson').
card_number('theft of dreams'/'EXO', '49').
card_flavor_text('theft of dreams'/'EXO', '\"Volrath stole everything there was of me.\"\n—Takara').
card_multiverse_id('theft of dreams'/'EXO', '5254').

card_in_set('thopter squadron', 'EXO').
card_original_type('thopter squadron'/'EXO', 'Artifact Creature').
card_original_text('thopter squadron'/'EXO', 'Flying\nThopter Squadron comes into play with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Thopter Squadron: Put a Thopter token into play. Treat this token as a 1/1 artifact creature with flying. Play this ability as a sorcery.\n{1}, Sacrifice a Thopter: Put a +1/+1 counter on Thopter Squadron. Play this ability as a sorcery.').
card_first_print('thopter squadron', 'EXO').
card_image_name('thopter squadron'/'EXO', 'thopter squadron').
card_uid('thopter squadron'/'EXO', 'EXO:Thopter Squadron:thopter squadron').
card_rarity('thopter squadron'/'EXO', 'Rare').
card_artist('thopter squadron'/'EXO', 'Doug Chaffee').
card_number('thopter squadron'/'EXO', '140').
card_multiverse_id('thopter squadron'/'EXO', '6164').

card_in_set('thrull surgeon', 'EXO').
card_original_type('thrull surgeon'/'EXO', 'Summon — Thrull').
card_original_text('thrull surgeon'/'EXO', '{1}{B}, Sacrifice Thrull Surgeon: Look at target player\'s hand and choose one of those cards. That player discards that card. Play this ability as a sorcery.').
card_first_print('thrull surgeon', 'EXO').
card_image_name('thrull surgeon'/'EXO', 'thrull surgeon').
card_uid('thrull surgeon'/'EXO', 'EXO:Thrull Surgeon:thrull surgeon').
card_rarity('thrull surgeon'/'EXO', 'Common').
card_artist('thrull surgeon'/'EXO', 'rk post').
card_number('thrull surgeon'/'EXO', '76').
card_flavor_text('thrull surgeon'/'EXO', '\"Just take a little off the top.\"').
card_multiverse_id('thrull surgeon'/'EXO', '6082').

card_in_set('transmogrifying licid', 'EXO').
card_original_type('transmogrifying licid'/'EXO', 'Artifact Creature').
card_original_text('transmogrifying licid'/'EXO', 'Transmogrifying Licid counts as a Licid.\n{1}, {T}: Transmogrifying Licid loses this ability and becomes a creature enchantment that reads \"Enchanted creature gets +1/+1 and counts as an artifact\" instead of any other type of permanent. Move Transmogrifying Licid onto target creature. You may pay {1} to end this effect.').
card_first_print('transmogrifying licid', 'EXO').
card_image_name('transmogrifying licid'/'EXO', 'transmogrifying licid').
card_uid('transmogrifying licid'/'EXO', 'EXO:Transmogrifying Licid:transmogrifying licid').
card_rarity('transmogrifying licid'/'EXO', 'Uncommon').
card_artist('transmogrifying licid'/'EXO', 'Jim Nelson').
card_number('transmogrifying licid'/'EXO', '141').
card_multiverse_id('transmogrifying licid'/'EXO', '6159').

card_in_set('treasure hunter', 'EXO').
card_original_type('treasure hunter'/'EXO', 'Summon — Townsfolk').
card_original_text('treasure hunter'/'EXO', 'When Treasure Hunter comes into play, you may return target artifact card from your graveyard to your hand.').
card_first_print('treasure hunter', 'EXO').
card_image_name('treasure hunter'/'EXO', 'treasure hunter').
card_uid('treasure hunter'/'EXO', 'EXO:Treasure Hunter:treasure hunter').
card_rarity('treasure hunter'/'EXO', 'Uncommon').
card_artist('treasure hunter'/'EXO', 'Adam Rex').
card_number('treasure hunter'/'EXO', '23').
card_flavor_text('treasure hunter'/'EXO', '\"What separates junk from treasure is imagination.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('treasure hunter'/'EXO', '6042').

card_in_set('treasure trove', 'EXO').
card_original_type('treasure trove'/'EXO', 'Enchantment').
card_original_text('treasure trove'/'EXO', '{2}{U}{U} Draw a card.').
card_first_print('treasure trove', 'EXO').
card_image_name('treasure trove'/'EXO', 'treasure trove').
card_uid('treasure trove'/'EXO', 'EXO:Treasure Trove:treasure trove').
card_rarity('treasure trove'/'EXO', 'Uncommon').
card_artist('treasure trove'/'EXO', 'Michael Sutfin').
card_number('treasure trove'/'EXO', '50').
card_flavor_text('treasure trove'/'EXO', 'Karn and Hanna stood over the recovered Legacy artifacts like loving parents over sleeping children.').
card_multiverse_id('treasure trove'/'EXO', '6070').

card_in_set('vampire hounds', 'EXO').
card_original_type('vampire hounds'/'EXO', 'Summon — Hounds').
card_original_text('vampire hounds'/'EXO', 'Choose and discard a creature card: Vampire Hounds gets +2/+2 until end of turn.').
card_first_print('vampire hounds', 'EXO').
card_image_name('vampire hounds'/'EXO', 'vampire hounds').
card_uid('vampire hounds'/'EXO', 'EXO:Vampire Hounds:vampire hounds').
card_rarity('vampire hounds'/'EXO', 'Common').
card_artist('vampire hounds'/'EXO', 'Kev Walker').
card_number('vampire hounds'/'EXO', '77').
card_flavor_text('vampire hounds'/'EXO', 'The hounds\' barks are a horrifying chorus of screams, moans, and whispers.').
card_multiverse_id('vampire hounds'/'EXO', '6084').

card_in_set('volrath\'s dungeon', 'EXO').
card_original_type('volrath\'s dungeon'/'EXO', 'Enchantment').
card_original_text('volrath\'s dungeon'/'EXO', 'Any player may pay 5 life during his or her turn to destroy Volrath\'s Dungeon.\nChoose and discard a card: Target player chooses a card in his or her hand and puts that card on top of his or her library. Play this ability as a sorcery.').
card_first_print('volrath\'s dungeon', 'EXO').
card_image_name('volrath\'s dungeon'/'EXO', 'volrath\'s dungeon').
card_uid('volrath\'s dungeon'/'EXO', 'EXO:Volrath\'s Dungeon:volrath\'s dungeon').
card_rarity('volrath\'s dungeon'/'EXO', 'Rare').
card_artist('volrath\'s dungeon'/'EXO', 'Stephen Daniele').
card_number('volrath\'s dungeon'/'EXO', '78').
card_multiverse_id('volrath\'s dungeon'/'EXO', '6100').

card_in_set('wall of nets', 'EXO').
card_original_type('wall of nets'/'EXO', 'Summon — Wall').
card_original_text('wall of nets'/'EXO', '(Walls cannot attack.)\nAt end of combat, remove from the game all creatures blocked by Wall of Nets.\nIf Wall of Nets leaves play, return to play under their owners\' control all creatures removed from the game with Wall of Nets.').
card_first_print('wall of nets', 'EXO').
card_image_name('wall of nets'/'EXO', 'wall of nets').
card_uid('wall of nets'/'EXO', 'EXO:Wall of Nets:wall of nets').
card_rarity('wall of nets'/'EXO', 'Rare').
card_artist('wall of nets'/'EXO', 'Terese Nielsen').
card_number('wall of nets'/'EXO', '24').
card_multiverse_id('wall of nets'/'EXO', '6052').

card_in_set('wayward soul', 'EXO').
card_original_type('wayward soul'/'EXO', 'Summon — Spirit').
card_original_text('wayward soul'/'EXO', 'Flying\n{U} Put Wayward Soul on top of owner\'s library.').
card_first_print('wayward soul', 'EXO').
card_image_name('wayward soul'/'EXO', 'wayward soul').
card_uid('wayward soul'/'EXO', 'EXO:Wayward Soul:wayward soul').
card_rarity('wayward soul'/'EXO', 'Common').
card_artist('wayward soul'/'EXO', 'DiTerlizzi').
card_number('wayward soul'/'EXO', '51').
card_flavor_text('wayward soul'/'EXO', '\"no home    no heart    no hope\"\n—Stronghold graffito').
card_multiverse_id('wayward soul'/'EXO', '5273').

card_in_set('welkin hawk', 'EXO').
card_original_type('welkin hawk'/'EXO', 'Summon — Bird').
card_original_text('welkin hawk'/'EXO', 'Flying\nIf Welkin Hawk is put into any graveyard from play, you may search your library for a Welkin Hawk card, reveal that card to all players, and put it into your hand. Shuffle your library afterwards.').
card_first_print('welkin hawk', 'EXO').
card_image_name('welkin hawk'/'EXO', 'welkin hawk').
card_uid('welkin hawk'/'EXO', 'EXO:Welkin Hawk:welkin hawk').
card_rarity('welkin hawk'/'EXO', 'Common').
card_artist('welkin hawk'/'EXO', 'Rob Alexander').
card_number('welkin hawk'/'EXO', '25').
card_multiverse_id('welkin hawk'/'EXO', '6034').

card_in_set('whiptongue frog', 'EXO').
card_original_type('whiptongue frog'/'EXO', 'Summon — Frog').
card_original_text('whiptongue frog'/'EXO', '{U} Whiptongue Frog gains flying until end of turn.').
card_first_print('whiptongue frog', 'EXO').
card_image_name('whiptongue frog'/'EXO', 'whiptongue frog').
card_uid('whiptongue frog'/'EXO', 'EXO:Whiptongue Frog:whiptongue frog').
card_rarity('whiptongue frog'/'EXO', 'Common').
card_artist('whiptongue frog'/'EXO', 'Jeff Miracola').
card_number('whiptongue frog'/'EXO', '52').
card_flavor_text('whiptongue frog'/'EXO', 'Their favorite food? Wind dancers.').
card_multiverse_id('whiptongue frog'/'EXO', '6057').

card_in_set('wood elves', 'EXO').
card_original_type('wood elves'/'EXO', 'Summon — Elves').
card_original_text('wood elves'/'EXO', 'When Wood Elves comes into play, search your library for a forest card and put that forest into play. Shuffle your library afterwards.').
card_image_name('wood elves'/'EXO', 'wood elves').
card_uid('wood elves'/'EXO', 'EXO:Wood Elves:wood elves').
card_rarity('wood elves'/'EXO', 'Common').
card_artist('wood elves'/'EXO', 'Rebecca Guay').
card_number('wood elves'/'EXO', '130').
card_flavor_text('wood elves'/'EXO', 'Secure in the embrace of wood, they want no part of Eladamri\'s war.').
card_multiverse_id('wood elves'/'EXO', '6135').

card_in_set('workhorse', 'EXO').
card_original_type('workhorse'/'EXO', 'Artifact Creature').
card_original_text('workhorse'/'EXO', 'Workhorse comes into play with four +1/+1 counters on it.\nRemove a +1/+1 counter from Workhorse: Add one colorless mana to your mana pool. Play this ability as a mana source.').
card_first_print('workhorse', 'EXO').
card_image_name('workhorse'/'EXO', 'workhorse').
card_uid('workhorse'/'EXO', 'EXO:Workhorse:workhorse').
card_rarity('workhorse'/'EXO', 'Rare').
card_artist('workhorse'/'EXO', 'DiTerlizzi').
card_number('workhorse'/'EXO', '142').
card_multiverse_id('workhorse'/'EXO', '6156').

card_in_set('zealots en-dal', 'EXO').
card_original_type('zealots en-dal'/'EXO', 'Summon — Soldiers').
card_original_text('zealots en-dal'/'EXO', 'During your upkeep, if all nonland permanents you control are white, gain 1 life.').
card_first_print('zealots en-dal', 'EXO').
card_image_name('zealots en-dal'/'EXO', 'zealots en-dal').
card_uid('zealots en-dal'/'EXO', 'EXO:Zealots en-Dal:zealots en-dal').
card_rarity('zealots en-dal'/'EXO', 'Uncommon').
card_artist('zealots en-dal'/'EXO', 'Brom').
card_number('zealots en-dal'/'EXO', '26').
card_flavor_text('zealots en-dal'/'EXO', '\"Great chains shall free our warriors.\"\n—Oracle en-Vec').
card_multiverse_id('zealots en-dal'/'EXO', '6043').
