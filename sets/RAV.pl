% Ravnica: City of Guilds

set('RAV').
set_name('RAV', 'Ravnica: City of Guilds').
set_release_date('RAV', '2005-10-07').
set_border('RAV', 'black').
set_type('RAV', 'expansion').
set_block('RAV', 'Ravnica').

card_in_set('agrus kos, wojek veteran', 'RAV').
card_original_type('agrus kos, wojek veteran'/'RAV', 'Legendary Creature — Human Soldier').
card_original_text('agrus kos, wojek veteran'/'RAV', 'Whenever Agrus Kos, Wojek Veteran attacks, attacking red creatures get +2/+0 and attacking white creatures get +0/+2 until end of turn.').
card_first_print('agrus kos, wojek veteran', 'RAV').
card_image_name('agrus kos, wojek veteran'/'RAV', 'agrus kos, wojek veteran').
card_uid('agrus kos, wojek veteran'/'RAV', 'RAV:Agrus Kos, Wojek Veteran:agrus kos, wojek veteran').
card_rarity('agrus kos, wojek veteran'/'RAV', 'Rare').
card_artist('agrus kos, wojek veteran'/'RAV', 'Donato Giancola').
card_number('agrus kos, wojek veteran'/'RAV', '190').
card_flavor_text('agrus kos, wojek veteran'/'RAV', 'Lieutenant Kos has never met a group of recruits he could not inspire or a mob he could not quell.').
card_multiverse_id('agrus kos, wojek veteran'/'RAV', '89101').
card_watermark('agrus kos, wojek veteran'/'RAV', 'Boros').

card_in_set('auratouched mage', 'RAV').
card_original_type('auratouched mage'/'RAV', 'Creature — Human Wizard').
card_original_text('auratouched mage'/'RAV', 'When Auratouched Mage comes into play, search your library for an Aura card that could enchant it. If Auratouched Mage is still in play, attach that Aura to it. Otherwise, reveal the Aura card and put it into your hand. Then shuffle your library.').
card_first_print('auratouched mage', 'RAV').
card_image_name('auratouched mage'/'RAV', 'auratouched mage').
card_uid('auratouched mage'/'RAV', 'RAV:Auratouched Mage:auratouched mage').
card_rarity('auratouched mage'/'RAV', 'Uncommon').
card_artist('auratouched mage'/'RAV', 'Jeff Miracola').
card_number('auratouched mage'/'RAV', '1').
card_multiverse_id('auratouched mage'/'RAV', '87952').

card_in_set('autochthon wurm', 'RAV').
card_original_type('autochthon wurm'/'RAV', 'Creature — Wurm').
card_original_text('autochthon wurm'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nTrample').
card_first_print('autochthon wurm', 'RAV').
card_image_name('autochthon wurm'/'RAV', 'autochthon wurm').
card_uid('autochthon wurm'/'RAV', 'RAV:Autochthon Wurm:autochthon wurm').
card_rarity('autochthon wurm'/'RAV', 'Rare').
card_artist('autochthon wurm'/'RAV', 'Michael Phillippi').
card_number('autochthon wurm'/'RAV', '191').
card_flavor_text('autochthon wurm'/'RAV', 'The trainer awoke to begin the journey from the wurm\'s tail to its head. The sun was setting when she arrived.').
card_multiverse_id('autochthon wurm'/'RAV', '89096').
card_watermark('autochthon wurm'/'RAV', 'Selesnya').

card_in_set('barbarian riftcutter', 'RAV').
card_original_type('barbarian riftcutter'/'RAV', 'Creature — Human Barbarian').
card_original_text('barbarian riftcutter'/'RAV', '{R}, Sacrifice Barbarian Riftcutter: Destroy target land.').
card_first_print('barbarian riftcutter', 'RAV').
card_image_name('barbarian riftcutter'/'RAV', 'barbarian riftcutter').
card_uid('barbarian riftcutter'/'RAV', 'RAV:Barbarian Riftcutter:barbarian riftcutter').
card_rarity('barbarian riftcutter'/'RAV', 'Common').
card_artist('barbarian riftcutter'/'RAV', 'Carl Critchlow').
card_number('barbarian riftcutter'/'RAV', '114').
card_flavor_text('barbarian riftcutter'/'RAV', '\"Riftcutters are suicidal anarchists, waving those lightning axes around so much that the walls are as likely to fall as their foes.\"\n—Heruj, Selesnya initiate').
card_multiverse_id('barbarian riftcutter'/'RAV', '87972').

card_in_set('bathe in light', 'RAV').
card_original_type('bathe in light'/'RAV', 'Instant').
card_original_text('bathe in light'/'RAV', 'Radiance Choose a color. Target creature and each other creature that shares a color with it gain protection from the chosen color until end of turn.').
card_first_print('bathe in light', 'RAV').
card_image_name('bathe in light'/'RAV', 'bathe in light').
card_uid('bathe in light'/'RAV', 'RAV:Bathe in Light:bathe in light').
card_rarity('bathe in light'/'RAV', 'Uncommon').
card_artist('bathe in light'/'RAV', 'Alex Horley-Orlandelli').
card_number('bathe in light'/'RAV', '2').
card_flavor_text('bathe in light'/'RAV', '\"Truth shines even in darkness. Those who march on the side of truth walk always in righteous light.\"').
card_multiverse_id('bathe in light'/'RAV', '87910').
card_watermark('bathe in light'/'RAV', 'Boros').

card_in_set('belltower sphinx', 'RAV').
card_original_type('belltower sphinx'/'RAV', 'Creature — Sphinx').
card_original_text('belltower sphinx'/'RAV', 'Flying\nWhenever a source deals damage to Belltower Sphinx, that source\'s controller puts that many cards from the top of his or her library into his or her graveyard.').
card_first_print('belltower sphinx', 'RAV').
card_image_name('belltower sphinx'/'RAV', 'belltower sphinx').
card_uid('belltower sphinx'/'RAV', 'RAV:Belltower Sphinx:belltower sphinx').
card_rarity('belltower sphinx'/'RAV', 'Uncommon').
card_artist('belltower sphinx'/'RAV', 'Jim Nelson').
card_number('belltower sphinx'/'RAV', '38').
card_multiverse_id('belltower sphinx'/'RAV', '88981').

card_in_set('benevolent ancestor', 'RAV').
card_original_type('benevolent ancestor'/'RAV', 'Creature — Spirit').
card_original_text('benevolent ancestor'/'RAV', 'Defender (This creature can\'t attack.)\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('benevolent ancestor', 'RAV').
card_image_name('benevolent ancestor'/'RAV', 'benevolent ancestor').
card_uid('benevolent ancestor'/'RAV', 'RAV:Benevolent Ancestor:benevolent ancestor').
card_rarity('benevolent ancestor'/'RAV', 'Common').
card_artist('benevolent ancestor'/'RAV', 'Nick Percival').
card_number('benevolent ancestor'/'RAV', '3').
card_flavor_text('benevolent ancestor'/'RAV', 'Although the door is flimsy and the lock pathetically small, Josuri\'s family never fears the night outside.').
card_multiverse_id('benevolent ancestor'/'RAV', '89047').

card_in_set('birds of paradise', 'RAV').
card_original_type('birds of paradise'/'RAV', 'Creature — Bird').
card_original_text('birds of paradise'/'RAV', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'RAV', 'birds of paradise').
card_uid('birds of paradise'/'RAV', 'RAV:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'RAV', 'Rare').
card_artist('birds of paradise'/'RAV', 'Marcelo Vignali').
card_number('birds of paradise'/'RAV', '153').
card_flavor_text('birds of paradise'/'RAV', 'Long ago, birds of paradise littered the skies. Thanks to the city\'s sprawl, most now exist as pets of society\'s elite.').
card_multiverse_id('birds of paradise'/'RAV', '83688').

card_in_set('blazing archon', 'RAV').
card_original_type('blazing archon'/'RAV', 'Creature — Archon').
card_original_text('blazing archon'/'RAV', 'Flying\nCreatures can\'t attack you.').
card_first_print('blazing archon', 'RAV').
card_image_name('blazing archon'/'RAV', 'blazing archon').
card_uid('blazing archon'/'RAV', 'RAV:Blazing Archon:blazing archon').
card_rarity('blazing archon'/'RAV', 'Rare').
card_artist('blazing archon'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('blazing archon'/'RAV', '4').
card_flavor_text('blazing archon'/'RAV', '\"Through the haze of battle I saw the glint of sun on golden mane, the sheen of glory clad in mail, and I dropped my sword and wept at the idiocy of war.\"\n—Dravin, Gruul deserter').
card_multiverse_id('blazing archon'/'RAV', '83754').

card_in_set('blockbuster', 'RAV').
card_original_type('blockbuster'/'RAV', 'Enchantment').
card_original_text('blockbuster'/'RAV', '{1}{R}, Sacrifice Blockbuster: Blockbuster deals 3 damage to each tapped creature and each player.').
card_first_print('blockbuster', 'RAV').
card_image_name('blockbuster'/'RAV', 'blockbuster').
card_uid('blockbuster'/'RAV', 'RAV:Blockbuster:blockbuster').
card_rarity('blockbuster'/'RAV', 'Uncommon').
card_artist('blockbuster'/'RAV', 'Dave Dorman').
card_number('blockbuster'/'RAV', '115').
card_flavor_text('blockbuster'/'RAV', 'Vendors hung cords heavy with roots and laid out boxes of salted meat, unaware of the fiery disaster swiftly approaching.').
card_multiverse_id('blockbuster'/'RAV', '87977').

card_in_set('blood funnel', 'RAV').
card_original_type('blood funnel'/'RAV', 'Enchantment').
card_original_text('blood funnel'/'RAV', 'Noncreature spells you play cost {2} less to play.\nWhenever you play a noncreature spell, counter that spell unless you sacrifice a creature.').
card_first_print('blood funnel', 'RAV').
card_image_name('blood funnel'/'RAV', 'blood funnel').
card_uid('blood funnel'/'RAV', 'RAV:Blood Funnel:blood funnel').
card_rarity('blood funnel'/'RAV', 'Rare').
card_artist('blood funnel'/'RAV', 'Thomas M. Baxa').
card_number('blood funnel'/'RAV', '77').
card_multiverse_id('blood funnel'/'RAV', '89102').

card_in_set('bloodbond march', 'RAV').
card_original_type('bloodbond march'/'RAV', 'Enchantment').
card_original_text('bloodbond march'/'RAV', 'Whenever a creature spell is played, each player returns all cards with the same name as that spell from his or her graveyard to play.').
card_first_print('bloodbond march', 'RAV').
card_image_name('bloodbond march'/'RAV', 'bloodbond march').
card_uid('bloodbond march'/'RAV', 'RAV:Bloodbond March:bloodbond march').
card_rarity('bloodbond march'/'RAV', 'Rare').
card_artist('bloodbond march'/'RAV', 'Jim Nelson').
card_number('bloodbond march'/'RAV', '192').
card_flavor_text('bloodbond march'/'RAV', 'The Golgari support a vast army because death never ends its soldiers\' service.').
card_multiverse_id('bloodbond march'/'RAV', '89000').
card_watermark('bloodbond march'/'RAV', 'Golgari').

card_in_set('bloodletter quill', 'RAV').
card_original_type('bloodletter quill'/'RAV', 'Artifact').
card_original_text('bloodletter quill'/'RAV', '{2}, {T}, Put a blood counter on Bloodletter Quill: Draw a card, then lose 1 life for each blood counter on Bloodletter Quill.\n{U}{B}: Remove a blood counter from Bloodletter Quill.').
card_first_print('bloodletter quill', 'RAV').
card_image_name('bloodletter quill'/'RAV', 'bloodletter quill').
card_uid('bloodletter quill'/'RAV', 'RAV:Bloodletter Quill:bloodletter quill').
card_rarity('bloodletter quill'/'RAV', 'Rare').
card_artist('bloodletter quill'/'RAV', 'Dan Scott').
card_number('bloodletter quill'/'RAV', '254').
card_multiverse_id('bloodletter quill'/'RAV', '83928').
card_watermark('bloodletter quill'/'RAV', 'Dimir').

card_in_set('boros fury-shield', 'RAV').
card_original_type('boros fury-shield'/'RAV', 'Instant').
card_original_text('boros fury-shield'/'RAV', 'Prevent all combat damage that would be dealt by target attacking or blocking creature this turn. If {R} was spent to play Boros Fury-Shield, it deals damage to that creature\'s controller equal to the creature\'s power.').
card_first_print('boros fury-shield', 'RAV').
card_image_name('boros fury-shield'/'RAV', 'boros fury-shield').
card_uid('boros fury-shield'/'RAV', 'RAV:Boros Fury-Shield:boros fury-shield').
card_rarity('boros fury-shield'/'RAV', 'Common').
card_artist('boros fury-shield'/'RAV', 'Wayne England').
card_number('boros fury-shield'/'RAV', '5').
card_multiverse_id('boros fury-shield'/'RAV', '89006').
card_watermark('boros fury-shield'/'RAV', 'Boros').

card_in_set('boros garrison', 'RAV').
card_original_type('boros garrison'/'RAV', 'Land').
card_original_text('boros garrison'/'RAV', 'Boros Garrison comes into play tapped.\nWhen Boros Garrison comes into play, return a land you control to its owner\'s hand.\n{T}: Add {R}{W} to your mana pool.').
card_first_print('boros garrison', 'RAV').
card_image_name('boros garrison'/'RAV', 'boros garrison').
card_uid('boros garrison'/'RAV', 'RAV:Boros Garrison:boros garrison').
card_rarity('boros garrison'/'RAV', 'Common').
card_artist('boros garrison'/'RAV', 'John Avon').
card_number('boros garrison'/'RAV', '275').
card_multiverse_id('boros garrison'/'RAV', '83900').
card_watermark('boros garrison'/'RAV', 'Boros').

card_in_set('boros guildmage', 'RAV').
card_original_type('boros guildmage'/'RAV', 'Creature — Human Wizard').
card_original_text('boros guildmage'/'RAV', '({R/W} can be paid with either {R} or {W}.)\n{1}{R}: Target creature gains haste until end of turn.\n{1}{W}: Target creature gains first strike until end of turn.').
card_first_print('boros guildmage', 'RAV').
card_image_name('boros guildmage'/'RAV', 'boros guildmage').
card_uid('boros guildmage'/'RAV', 'RAV:Boros Guildmage:boros guildmage').
card_rarity('boros guildmage'/'RAV', 'Uncommon').
card_artist('boros guildmage'/'RAV', 'Paolo Parente').
card_number('boros guildmage'/'RAV', '242').
card_multiverse_id('boros guildmage'/'RAV', '87918').
card_watermark('boros guildmage'/'RAV', 'Boros').

card_in_set('boros recruit', 'RAV').
card_original_type('boros recruit'/'RAV', 'Creature — Goblin Soldier').
card_original_text('boros recruit'/'RAV', '({R/W} can be paid with either {R} or {W}.)\nFirst strike').
card_first_print('boros recruit', 'RAV').
card_image_name('boros recruit'/'RAV', 'boros recruit').
card_uid('boros recruit'/'RAV', 'RAV:Boros Recruit:boros recruit').
card_rarity('boros recruit'/'RAV', 'Common').
card_artist('boros recruit'/'RAV', 'Keith Garletts').
card_number('boros recruit'/'RAV', '243').
card_flavor_text('boros recruit'/'RAV', '\"Look at this goblin, once wild, untamed, and full of idiocy. The Boros have given him the skill of a soldier and focused his heart with a single cause.\"\n—Razia').
card_multiverse_id('boros recruit'/'RAV', '88992').
card_watermark('boros recruit'/'RAV', 'Boros').

card_in_set('boros signet', 'RAV').
card_original_type('boros signet'/'RAV', 'Artifact').
card_original_text('boros signet'/'RAV', '{1}, {T}: Add {R}{W} to your mana pool.').
card_first_print('boros signet', 'RAV').
card_image_name('boros signet'/'RAV', 'boros signet').
card_uid('boros signet'/'RAV', 'RAV:Boros Signet:boros signet').
card_rarity('boros signet'/'RAV', 'Common').
card_artist('boros signet'/'RAV', 'Tim Hildebrandt').
card_number('boros signet'/'RAV', '255').
card_flavor_text('boros signet'/'RAV', '\"Have you ever held a Boros signet? There\'s a weight to it that belies its size—a weight of strength and of pride.\"\n—Agrus Kos').
card_multiverse_id('boros signet'/'RAV', '95537').
card_watermark('boros signet'/'RAV', 'Boros').

card_in_set('boros swiftblade', 'RAV').
card_original_type('boros swiftblade'/'RAV', 'Creature — Human Soldier').
card_original_text('boros swiftblade'/'RAV', 'Double strike').
card_first_print('boros swiftblade', 'RAV').
card_image_name('boros swiftblade'/'RAV', 'boros swiftblade').
card_uid('boros swiftblade'/'RAV', 'RAV:Boros Swiftblade:boros swiftblade').
card_rarity('boros swiftblade'/'RAV', 'Uncommon').
card_artist('boros swiftblade'/'RAV', 'Doug Chaffee').
card_number('boros swiftblade'/'RAV', '193').
card_flavor_text('boros swiftblade'/'RAV', 'When the Boros Legion attacks, swiftblades enter the fray first. They pick off the archers and mages, softening the enemy front before the flame-kin and giants go in.').
card_multiverse_id('boros swiftblade'/'RAV', '83588').
card_watermark('boros swiftblade'/'RAV', 'Boros').

card_in_set('bottled cloister', 'RAV').
card_original_type('bottled cloister'/'RAV', 'Artifact').
card_original_text('bottled cloister'/'RAV', 'At the beginning of each opponent\'s upkeep, remove your hand from the game face down.\nAt the beginning of your upkeep, return all cards removed from the game with Bottled Cloister to your hand, then draw a card.').
card_first_print('bottled cloister', 'RAV').
card_image_name('bottled cloister'/'RAV', 'bottled cloister').
card_uid('bottled cloister'/'RAV', 'RAV:Bottled Cloister:bottled cloister').
card_rarity('bottled cloister'/'RAV', 'Rare').
card_artist('bottled cloister'/'RAV', 'Luca Zontini').
card_number('bottled cloister'/'RAV', '256').
card_multiverse_id('bottled cloister'/'RAV', '89018').

card_in_set('brainspoil', 'RAV').
card_original_type('brainspoil'/'RAV', 'Sorcery').
card_original_text('brainspoil'/'RAV', 'Destroy target creature that isn\'t enchanted. It can\'t be regenerated.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('brainspoil', 'RAV').
card_image_name('brainspoil'/'RAV', 'brainspoil').
card_uid('brainspoil'/'RAV', 'RAV:Brainspoil:brainspoil').
card_rarity('brainspoil'/'RAV', 'Common').
card_artist('brainspoil'/'RAV', 'Tomas Giorello').
card_number('brainspoil'/'RAV', '78').
card_multiverse_id('brainspoil'/'RAV', '88965').
card_watermark('brainspoil'/'RAV', 'Dimir').

card_in_set('bramble elemental', 'RAV').
card_original_type('bramble elemental'/'RAV', 'Creature — Elemental').
card_original_text('bramble elemental'/'RAV', 'Whenever an Aura becomes attached to Bramble Elemental, put two 1/1 green Saproling creature tokens into play.').
card_first_print('bramble elemental', 'RAV').
card_image_name('bramble elemental'/'RAV', 'bramble elemental').
card_uid('bramble elemental'/'RAV', 'RAV:Bramble Elemental:bramble elemental').
card_rarity('bramble elemental'/'RAV', 'Common').
card_artist('bramble elemental'/'RAV', 'Tomas Giorello').
card_number('bramble elemental'/'RAV', '154').
card_flavor_text('bramble elemental'/'RAV', 'Ravnica is a seamless urban tapestry, each city bleeding into the next. In abandoned corners, however, nature has begun to reclaim what it once owned.').
card_multiverse_id('bramble elemental'/'RAV', '87964').

card_in_set('breath of fury', 'RAV').
card_original_type('breath of fury'/'RAV', 'Enchantment — Aura').
card_original_text('breath of fury'/'RAV', 'Enchant creature you control\nWhen enchanted creature deals combat damage to a player, sacrifice it and attach Breath of Fury to a creature you control. If you do, untap all creatures you control and after this phase, there is an additional combat phase.').
card_first_print('breath of fury', 'RAV').
card_image_name('breath of fury'/'RAV', 'breath of fury').
card_uid('breath of fury'/'RAV', 'RAV:Breath of Fury:breath of fury').
card_rarity('breath of fury'/'RAV', 'Rare').
card_artist('breath of fury'/'RAV', 'Kev Walker').
card_number('breath of fury'/'RAV', '116').
card_multiverse_id('breath of fury'/'RAV', '89099').

card_in_set('brightflame', 'RAV').
card_original_type('brightflame'/'RAV', 'Sorcery').
card_original_text('brightflame'/'RAV', 'Radiance Brightflame deals X damage to target creature and each other creature that shares a color with it. You gain life equal to the damage dealt this way.').
card_first_print('brightflame', 'RAV').
card_image_name('brightflame'/'RAV', 'brightflame').
card_uid('brightflame'/'RAV', 'RAV:Brightflame:brightflame').
card_rarity('brightflame'/'RAV', 'Rare').
card_artist('brightflame'/'RAV', 'Dave Dorman').
card_number('brightflame'/'RAV', '194').
card_flavor_text('brightflame'/'RAV', '\"Let the pyres of the unbelievers light our way.\"\n—Razia').
card_multiverse_id('brightflame'/'RAV', '89068').
card_watermark('brightflame'/'RAV', 'Boros').

card_in_set('caregiver', 'RAV').
card_original_type('caregiver'/'RAV', 'Creature — Human Cleric').
card_original_text('caregiver'/'RAV', '{W}, Sacrifice a creature: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('caregiver', 'RAV').
card_image_name('caregiver'/'RAV', 'caregiver').
card_uid('caregiver'/'RAV', 'RAV:Caregiver:caregiver').
card_rarity('caregiver'/'RAV', 'Common').
card_artist('caregiver'/'RAV', 'William Simpson').
card_number('caregiver'/'RAV', '6').
card_flavor_text('caregiver'/'RAV', '\"The guilds each believe its way is right, but their ways only bring blood to Ravnica\'s streets and tears to Ravnica\'s families.\"').
card_multiverse_id('caregiver'/'RAV', '83612').

card_in_set('carrion howler', 'RAV').
card_original_type('carrion howler'/'RAV', 'Creature — Zombie Wolf').
card_original_text('carrion howler'/'RAV', 'Pay 1 life: Carrion Howler gets +2/-1 until end of turn.').
card_first_print('carrion howler', 'RAV').
card_image_name('carrion howler'/'RAV', 'carrion howler').
card_uid('carrion howler'/'RAV', 'RAV:Carrion Howler:carrion howler').
card_rarity('carrion howler'/'RAV', 'Uncommon').
card_artist('carrion howler'/'RAV', 'John Zeleznik').
card_number('carrion howler'/'RAV', '79').
card_flavor_text('carrion howler'/'RAV', 'They chase tirelessly after the bone-carts that speed along the streets of the Plague Quarter.').
card_multiverse_id('carrion howler'/'RAV', '88974').

card_in_set('carven caryatid', 'RAV').
card_original_type('carven caryatid'/'RAV', 'Creature — Spirit').
card_original_text('carven caryatid'/'RAV', 'Defender (This creature can\'t attack.)\nWhen Carven Caryatid comes into play, draw a card.').
card_first_print('carven caryatid', 'RAV').
card_image_name('carven caryatid'/'RAV', 'carven caryatid').
card_uid('carven caryatid'/'RAV', 'RAV:Carven Caryatid:carven caryatid').
card_rarity('carven caryatid'/'RAV', 'Uncommon').
card_artist('carven caryatid'/'RAV', 'Jim Nelson').
card_number('carven caryatid'/'RAV', '155').
card_flavor_text('carven caryatid'/'RAV', 'So-called \"old wood\" is rare in deforested Ravnica. Statues carved from it are said to house the last of the ancient nature spirits.').
card_multiverse_id('carven caryatid'/'RAV', '89048').

card_in_set('centaur safeguard', 'RAV').
card_original_type('centaur safeguard'/'RAV', 'Creature — Centaur Warrior').
card_original_text('centaur safeguard'/'RAV', '({G/W} can be paid with either {G} or {W}.)\nWhen Centaur Safeguard is put into a graveyard from play, you may gain 3 life.').
card_first_print('centaur safeguard', 'RAV').
card_image_name('centaur safeguard'/'RAV', 'centaur safeguard').
card_uid('centaur safeguard'/'RAV', 'RAV:Centaur Safeguard:centaur safeguard').
card_rarity('centaur safeguard'/'RAV', 'Common').
card_artist('centaur safeguard'/'RAV', 'Glenn Fabry').
card_number('centaur safeguard'/'RAV', '244').
card_flavor_text('centaur safeguard'/'RAV', 'Swift, strong, and selfless, the centaurs are the shields of the Conclave.').
card_multiverse_id('centaur safeguard'/'RAV', '87959').
card_watermark('centaur safeguard'/'RAV', 'Selesnya').

card_in_set('cerulean sphinx', 'RAV').
card_original_type('cerulean sphinx'/'RAV', 'Creature — Sphinx').
card_original_text('cerulean sphinx'/'RAV', 'Flying\n{U}: Cerulean Sphinx\'s owner shuffles it into his or her library.').
card_first_print('cerulean sphinx', 'RAV').
card_image_name('cerulean sphinx'/'RAV', 'cerulean sphinx').
card_uid('cerulean sphinx'/'RAV', 'RAV:Cerulean Sphinx:cerulean sphinx').
card_rarity('cerulean sphinx'/'RAV', 'Rare').
card_artist('cerulean sphinx'/'RAV', 'Jim Murray').
card_number('cerulean sphinx'/'RAV', '39').
card_flavor_text('cerulean sphinx'/'RAV', '\"About the sphinx, I have mixed feelings. Their wisdom I crave, but their secrecy I can\'t tolerate.\"\n—Szadek').
card_multiverse_id('cerulean sphinx'/'RAV', '89095').

card_in_set('chant of vitu-ghazi', 'RAV').
card_original_type('chant of vitu-ghazi'/'RAV', 'Instant').
card_original_text('chant of vitu-ghazi'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nPrevent all damage that would be dealt by creatures this turn. You gain 1 life for each damage prevented this way.').
card_first_print('chant of vitu-ghazi', 'RAV').
card_image_name('chant of vitu-ghazi'/'RAV', 'chant of vitu-ghazi').
card_uid('chant of vitu-ghazi'/'RAV', 'RAV:Chant of Vitu-Ghazi:chant of vitu-ghazi').
card_rarity('chant of vitu-ghazi'/'RAV', 'Uncommon').
card_artist('chant of vitu-ghazi'/'RAV', 'Stephen Tappin').
card_number('chant of vitu-ghazi'/'RAV', '7').
card_multiverse_id('chant of vitu-ghazi'/'RAV', '87957').
card_watermark('chant of vitu-ghazi'/'RAV', 'Selesnya').

card_in_set('char', 'RAV').
card_original_type('char'/'RAV', 'Instant').
card_original_text('char'/'RAV', 'Char deals 4 damage to target creature or player and 2 damage to you.').
card_first_print('char', 'RAV').
card_image_name('char'/'RAV', 'char').
card_uid('char'/'RAV', 'RAV:Char:char').
card_rarity('char'/'RAV', 'Rare').
card_artist('char'/'RAV', 'Adam Rex').
card_number('char'/'RAV', '117').
card_flavor_text('char'/'RAV', 'Izzet mages often acquire their magic reagents from dubious sources, so the potency of their spells is never predictable.').
card_multiverse_id('char'/'RAV', '87942').

card_in_set('chord of calling', 'RAV').
card_original_type('chord of calling'/'RAV', 'Instant').
card_original_text('chord of calling'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nSearch your library for a creature card with converted mana cost X or less and put it into play. Then shuffle your library.').
card_first_print('chord of calling', 'RAV').
card_image_name('chord of calling'/'RAV', 'chord of calling').
card_uid('chord of calling'/'RAV', 'RAV:Chord of Calling:chord of calling').
card_rarity('chord of calling'/'RAV', 'Rare').
card_artist('chord of calling'/'RAV', 'Heather Hudson').
card_number('chord of calling'/'RAV', '156').
card_multiverse_id('chord of calling'/'RAV', '89064').
card_watermark('chord of calling'/'RAV', 'Selesnya').

card_in_set('chorus of the conclave', 'RAV').
card_original_type('chorus of the conclave'/'RAV', 'Legendary Creature — Dryad Lord').
card_original_text('chorus of the conclave'/'RAV', 'Forestwalk\nAs an additional cost to play creature spells, you may pay any amount of mana. If you do, that creature comes into play with that many additional +1/+1 counters on it.').
card_first_print('chorus of the conclave', 'RAV').
card_image_name('chorus of the conclave'/'RAV', 'chorus of the conclave').
card_uid('chorus of the conclave'/'RAV', 'RAV:Chorus of the Conclave:chorus of the conclave').
card_rarity('chorus of the conclave'/'RAV', 'Rare').
card_artist('chorus of the conclave'/'RAV', 'Brian Despain').
card_number('chorus of the conclave'/'RAV', '195').
card_flavor_text('chorus of the conclave'/'RAV', '\"We are many, yet one. We are separate in body, yet speak with a single voice. Join us in our chorus.\"').
card_multiverse_id('chorus of the conclave'/'RAV', '89053').
card_watermark('chorus of the conclave'/'RAV', 'Selesnya').

card_in_set('circu, dimir lobotomist', 'RAV').
card_original_type('circu, dimir lobotomist'/'RAV', 'Legendary Creature — Human Wizard').
card_original_text('circu, dimir lobotomist'/'RAV', 'Whenever you play a blue spell, remove the top card of target library from the game.\nWhenever you play a black spell, remove the top card of target library from the game.\nYour opponents can\'t play nonland cards with the same name as a card removed from the game with Circu, Dimir Lobotomist.').
card_first_print('circu, dimir lobotomist', 'RAV').
card_image_name('circu, dimir lobotomist'/'RAV', 'circu, dimir lobotomist').
card_uid('circu, dimir lobotomist'/'RAV', 'RAV:Circu, Dimir Lobotomist:circu, dimir lobotomist').
card_rarity('circu, dimir lobotomist'/'RAV', 'Rare').
card_artist('circu, dimir lobotomist'/'RAV', 'Cyril Van Der Haegen').
card_number('circu, dimir lobotomist'/'RAV', '196').
card_multiverse_id('circu, dimir lobotomist'/'RAV', '89111').
card_watermark('circu, dimir lobotomist'/'RAV', 'Dimir').

card_in_set('civic wayfinder', 'RAV').
card_original_type('civic wayfinder'/'RAV', 'Creature — Elf Warrior Druid').
card_original_text('civic wayfinder'/'RAV', 'When Civic Wayfinder comes into play, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('civic wayfinder', 'RAV').
card_image_name('civic wayfinder'/'RAV', 'civic wayfinder').
card_uid('civic wayfinder'/'RAV', 'RAV:Civic Wayfinder:civic wayfinder').
card_rarity('civic wayfinder'/'RAV', 'Common').
card_artist('civic wayfinder'/'RAV', 'Cyril Van Der Haegen').
card_number('civic wayfinder'/'RAV', '157').
card_flavor_text('civic wayfinder'/'RAV', '\"These alleys are not safe. Come, I can guide you back to the market square.\"').
card_multiverse_id('civic wayfinder'/'RAV', '83584').

card_in_set('cleansing beam', 'RAV').
card_original_type('cleansing beam'/'RAV', 'Instant').
card_original_text('cleansing beam'/'RAV', 'Radiance Cleansing Beam deals 2 damage to target creature and each other creature that shares a color with it.').
card_first_print('cleansing beam', 'RAV').
card_image_name('cleansing beam'/'RAV', 'cleansing beam').
card_uid('cleansing beam'/'RAV', 'RAV:Cleansing Beam:cleansing beam').
card_rarity('cleansing beam'/'RAV', 'Uncommon').
card_artist('cleansing beam'/'RAV', 'Pat Lee').
card_number('cleansing beam'/'RAV', '118').
card_flavor_text('cleansing beam'/'RAV', '\"Justice is toothless without punishment. Righteousness cannot succeed without the suffering of the guilty.\"\n—Razia').
card_multiverse_id('cleansing beam'/'RAV', '83567').
card_watermark('cleansing beam'/'RAV', 'Boros').

card_in_set('clinging darkness', 'RAV').
card_original_type('clinging darkness'/'RAV', 'Enchantment — Aura').
card_original_text('clinging darkness'/'RAV', 'Enchant creature\nEnchanted creature gets -4/-1.').
card_first_print('clinging darkness', 'RAV').
card_image_name('clinging darkness'/'RAV', 'clinging darkness').
card_uid('clinging darkness'/'RAV', 'RAV:Clinging Darkness:clinging darkness').
card_rarity('clinging darkness'/'RAV', 'Common').
card_artist('clinging darkness'/'RAV', 'Glen Angus').
card_number('clinging darkness'/'RAV', '80').
card_flavor_text('clinging darkness'/'RAV', 'There\'s an experience worse than blindness—it\'s the certainty that your vision is perfect and the horror that there\'s no world around you to see.').
card_multiverse_id('clinging darkness'/'RAV', '83822').

card_in_set('cloudstone curio', 'RAV').
card_original_type('cloudstone curio'/'RAV', 'Artifact').
card_original_text('cloudstone curio'/'RAV', 'Whenever a nonartifact permanent comes into play under your control, you may return another permanent you control that shares a permanent type with it to its owner\'s hand.').
card_first_print('cloudstone curio', 'RAV').
card_image_name('cloudstone curio'/'RAV', 'cloudstone curio').
card_uid('cloudstone curio'/'RAV', 'RAV:Cloudstone Curio:cloudstone curio').
card_rarity('cloudstone curio'/'RAV', 'Rare').
card_artist('cloudstone curio'/'RAV', 'Heather Hudson').
card_number('cloudstone curio'/'RAV', '257').
card_flavor_text('cloudstone curio'/'RAV', 'It wants to remain a mystery, banishing the curious in favor of less inquisitive company.').
card_multiverse_id('cloudstone curio'/'RAV', '89089').

card_in_set('clutch of the undercity', 'RAV').
card_original_type('clutch of the undercity'/'RAV', 'Instant').
card_original_text('clutch of the undercity'/'RAV', 'Return target permanent to its owner\'s hand. Its controller loses 3 life.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('clutch of the undercity', 'RAV').
card_image_name('clutch of the undercity'/'RAV', 'clutch of the undercity').
card_uid('clutch of the undercity'/'RAV', 'RAV:Clutch of the Undercity:clutch of the undercity').
card_rarity('clutch of the undercity'/'RAV', 'Uncommon').
card_artist('clutch of the undercity'/'RAV', 'Pete Venters').
card_number('clutch of the undercity'/'RAV', '197').
card_multiverse_id('clutch of the undercity'/'RAV', '89037').
card_watermark('clutch of the undercity'/'RAV', 'Dimir').

card_in_set('coalhauler swine', 'RAV').
card_original_type('coalhauler swine'/'RAV', 'Creature — Beast').
card_original_text('coalhauler swine'/'RAV', 'Whenever Coalhauler Swine is dealt damage, it deals that much damage to each player.').
card_first_print('coalhauler swine', 'RAV').
card_image_name('coalhauler swine'/'RAV', 'coalhauler swine').
card_uid('coalhauler swine'/'RAV', 'RAV:Coalhauler Swine:coalhauler swine').
card_rarity('coalhauler swine'/'RAV', 'Common').
card_artist('coalhauler swine'/'RAV', 'Daren Bader').
card_number('coalhauler swine'/'RAV', '119').
card_flavor_text('coalhauler swine'/'RAV', 'Nothing stops industry in Ravnica—certainly not the safety of its workers.').
card_multiverse_id('coalhauler swine'/'RAV', '87949').

card_in_set('compulsive research', 'RAV').
card_original_type('compulsive research'/'RAV', 'Sorcery').
card_original_text('compulsive research'/'RAV', 'Target player draws three cards. Then that player discards two cards unless he or she discards a land card.').
card_first_print('compulsive research', 'RAV').
card_image_name('compulsive research'/'RAV', 'compulsive research').
card_uid('compulsive research'/'RAV', 'RAV:Compulsive Research:compulsive research').
card_rarity('compulsive research'/'RAV', 'Common').
card_artist('compulsive research'/'RAV', 'Michael Sutfin').
card_number('compulsive research'/'RAV', '40').
card_flavor_text('compulsive research'/'RAV', '\"Four parts molten bronze, yes . . . one part frozen mercury, yes, yes . . . but then what?\"').
card_multiverse_id('compulsive research'/'RAV', '87991').

card_in_set('concerted effort', 'RAV').
card_original_type('concerted effort'/'RAV', 'Enchantment').
card_original_text('concerted effort'/'RAV', 'At the beginning of each player\'s upkeep, all creatures you control gain flying until end of turn if a creature you control has flying. The same is true for fear, first strike, double strike, landwalk, protection, trample, and vigilance.').
card_first_print('concerted effort', 'RAV').
card_image_name('concerted effort'/'RAV', 'concerted effort').
card_uid('concerted effort'/'RAV', 'RAV:Concerted Effort:concerted effort').
card_rarity('concerted effort'/'RAV', 'Rare').
card_artist('concerted effort'/'RAV', 'Michael Sutfin').
card_number('concerted effort'/'RAV', '8').
card_multiverse_id('concerted effort'/'RAV', '89109').

card_in_set('conclave equenaut', 'RAV').
card_original_type('conclave equenaut'/'RAV', 'Creature — Human Soldier').
card_original_text('conclave equenaut'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nFlying').
card_first_print('conclave equenaut', 'RAV').
card_image_name('conclave equenaut'/'RAV', 'conclave equenaut').
card_uid('conclave equenaut'/'RAV', 'RAV:Conclave Equenaut:conclave equenaut').
card_rarity('conclave equenaut'/'RAV', 'Common').
card_artist('conclave equenaut'/'RAV', 'Terese Nielsen').
card_number('conclave equenaut'/'RAV', '9').
card_flavor_text('conclave equenaut'/'RAV', 'Equenauts are the seeds of the Conclave, scattered on the four winds, searching for new places to take root.').
card_multiverse_id('conclave equenaut'/'RAV', '87915').
card_watermark('conclave equenaut'/'RAV', 'Selesnya').

card_in_set('conclave phalanx', 'RAV').
card_original_type('conclave phalanx'/'RAV', 'Creature — Human Soldier').
card_original_text('conclave phalanx'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nWhen Conclave Phalanx comes into play, you gain 1 life for each creature you control.').
card_first_print('conclave phalanx', 'RAV').
card_image_name('conclave phalanx'/'RAV', 'conclave phalanx').
card_uid('conclave phalanx'/'RAV', 'RAV:Conclave Phalanx:conclave phalanx').
card_rarity('conclave phalanx'/'RAV', 'Uncommon').
card_artist('conclave phalanx'/'RAV', 'Wayne Reynolds').
card_number('conclave phalanx'/'RAV', '10').
card_multiverse_id('conclave phalanx'/'RAV', '89010').
card_watermark('conclave phalanx'/'RAV', 'Selesnya').

card_in_set('conclave\'s blessing', 'RAV').
card_original_type('conclave\'s blessing'/'RAV', 'Enchantment — Aura').
card_original_text('conclave\'s blessing'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nEnchant creature\nEnchanted creature gets +0/+2 for each other creature you control.').
card_first_print('conclave\'s blessing', 'RAV').
card_image_name('conclave\'s blessing'/'RAV', 'conclave\'s blessing').
card_uid('conclave\'s blessing'/'RAV', 'RAV:Conclave\'s Blessing:conclave\'s blessing').
card_rarity('conclave\'s blessing'/'RAV', 'Common').
card_artist('conclave\'s blessing'/'RAV', 'Shishizaru').
card_number('conclave\'s blessing'/'RAV', '11').
card_multiverse_id('conclave\'s blessing'/'RAV', '87969').
card_watermark('conclave\'s blessing'/'RAV', 'Selesnya').

card_in_set('congregation at dawn', 'RAV').
card_original_type('congregation at dawn'/'RAV', 'Instant').
card_original_text('congregation at dawn'/'RAV', 'Search your library for up to three creature cards and reveal them. Shuffle your library, then put those cards on top of it in any order.').
card_first_print('congregation at dawn', 'RAV').
card_image_name('congregation at dawn'/'RAV', 'congregation at dawn').
card_uid('congregation at dawn'/'RAV', 'RAV:Congregation at Dawn:congregation at dawn').
card_rarity('congregation at dawn'/'RAV', 'Uncommon').
card_artist('congregation at dawn'/'RAV', 'Randy Gallegos').
card_number('congregation at dawn'/'RAV', '198').
card_flavor_text('congregation at dawn'/'RAV', '\"It is fitting that we meet in the first hour of light, for we are called by the Conclave to pierce a threat of darkness.\"').
card_multiverse_id('congregation at dawn'/'RAV', '87995').
card_watermark('congregation at dawn'/'RAV', 'Selesnya').

card_in_set('consult the necrosages', 'RAV').
card_original_type('consult the necrosages'/'RAV', 'Sorcery').
card_original_text('consult the necrosages'/'RAV', 'Choose one Target player draws two cards; or target player discards two cards.').
card_first_print('consult the necrosages', 'RAV').
card_image_name('consult the necrosages'/'RAV', 'consult the necrosages').
card_uid('consult the necrosages'/'RAV', 'RAV:Consult the Necrosages:consult the necrosages').
card_rarity('consult the necrosages'/'RAV', 'Common').
card_artist('consult the necrosages'/'RAV', 'Paolo Parente').
card_number('consult the necrosages'/'RAV', '199').
card_flavor_text('consult the necrosages'/'RAV', 'Dimir rank and file never see nor hear their guildmaster. All orders are given through mysterious necrosages who appear from the shadows, tersely toss out a command, and then melt into the darkness.').
card_multiverse_id('consult the necrosages'/'RAV', '87923').
card_watermark('consult the necrosages'/'RAV', 'Dimir').

card_in_set('convolute', 'RAV').
card_original_type('convolute'/'RAV', 'Instant').
card_original_text('convolute'/'RAV', 'Counter target spell unless its controller pays {4}.').
card_first_print('convolute', 'RAV').
card_image_name('convolute'/'RAV', 'convolute').
card_uid('convolute'/'RAV', 'RAV:Convolute:convolute').
card_rarity('convolute'/'RAV', 'Common').
card_artist('convolute'/'RAV', 'Dany Orizio').
card_number('convolute'/'RAV', '41').
card_flavor_text('convolute'/'RAV', 'The words came to the sorcerer\'s lips but refused to budge any further.').
card_multiverse_id('convolute'/'RAV', '88998').

card_in_set('copy enchantment', 'RAV').
card_original_type('copy enchantment'/'RAV', 'Enchantment').
card_original_text('copy enchantment'/'RAV', 'As Copy Enchantment comes into play, you may choose an enchantment in play. If you do, Copy Enchantment comes into play as a copy of that enchantment.').
card_first_print('copy enchantment', 'RAV').
card_image_name('copy enchantment'/'RAV', 'copy enchantment').
card_uid('copy enchantment'/'RAV', 'RAV:Copy Enchantment:copy enchantment').
card_rarity('copy enchantment'/'RAV', 'Rare').
card_artist('copy enchantment'/'RAV', 'Joel Thomas').
card_number('copy enchantment'/'RAV', '42').
card_flavor_text('copy enchantment'/'RAV', 'Simic mages create redundant backups of their experiments to reduce the consequences of catastrophe.').
card_multiverse_id('copy enchantment'/'RAV', '83807').

card_in_set('courier hawk', 'RAV').
card_original_type('courier hawk'/'RAV', 'Creature — Bird').
card_original_text('courier hawk'/'RAV', 'Flying, vigilance').
card_first_print('courier hawk', 'RAV').
card_image_name('courier hawk'/'RAV', 'courier hawk').
card_uid('courier hawk'/'RAV', 'RAV:Courier Hawk:courier hawk').
card_rarity('courier hawk'/'RAV', 'Common').
card_artist('courier hawk'/'RAV', 'Mark Poole').
card_number('courier hawk'/'RAV', '12').
card_flavor_text('courier hawk'/'RAV', 'The Orzhov started using hawks as messengers in order to bypass the many toll roads and bridges that dot Ravnica. Soon the service became one of their most profitable businesses.').
card_multiverse_id('courier hawk'/'RAV', '87913').

card_in_set('crown of convergence', 'RAV').
card_original_type('crown of convergence'/'RAV', 'Artifact').
card_original_text('crown of convergence'/'RAV', 'Play with the top card of your library revealed.\nAs long as the top card of your library is a creature card, creatures you control that share a color with that card get +1/+1.\n{G}{W}: Put the top card of your library on the bottom of your library.').
card_first_print('crown of convergence', 'RAV').
card_image_name('crown of convergence'/'RAV', 'crown of convergence').
card_uid('crown of convergence'/'RAV', 'RAV:Crown of Convergence:crown of convergence').
card_rarity('crown of convergence'/'RAV', 'Rare').
card_artist('crown of convergence'/'RAV', 'Jen Page').
card_number('crown of convergence'/'RAV', '258').
card_multiverse_id('crown of convergence'/'RAV', '83903').
card_watermark('crown of convergence'/'RAV', 'Selesnya').

card_in_set('cyclopean snare', 'RAV').
card_original_type('cyclopean snare'/'RAV', 'Artifact').
card_original_text('cyclopean snare'/'RAV', '{3}, {T}: Tap target creature, then return Cyclopean Snare to its owner\'s hand.').
card_first_print('cyclopean snare', 'RAV').
card_image_name('cyclopean snare'/'RAV', 'cyclopean snare').
card_uid('cyclopean snare'/'RAV', 'RAV:Cyclopean Snare:cyclopean snare').
card_rarity('cyclopean snare'/'RAV', 'Uncommon').
card_artist('cyclopean snare'/'RAV', 'Keith Garletts').
card_number('cyclopean snare'/'RAV', '259').
card_flavor_text('cyclopean snare'/'RAV', 'The spoiled, gluttonous halls of the Orzhov teem with gaudy, needless wealth. Even their security measures are made for beauty first, efficiency second.').
card_multiverse_id('cyclopean snare'/'RAV', '89035').

card_in_set('dark confidant', 'RAV').
card_original_type('dark confidant'/'RAV', 'Creature — Human Wizard').
card_original_text('dark confidant'/'RAV', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_image_name('dark confidant'/'RAV', 'dark confidant').
card_uid('dark confidant'/'RAV', 'RAV:Dark Confidant:dark confidant').
card_rarity('dark confidant'/'RAV', 'Rare').
card_artist('dark confidant'/'RAV', 'Ron Spears').
card_number('dark confidant'/'RAV', '81').
card_flavor_text('dark confidant'/'RAV', 'Greatness, at any cost.').
card_multiverse_id('dark confidant'/'RAV', '83771').

card_in_set('dark heart of the wood', 'RAV').
card_original_type('dark heart of the wood'/'RAV', 'Enchantment').
card_original_text('dark heart of the wood'/'RAV', 'Sacrifice a Forest: You gain 3 life.').
card_image_name('dark heart of the wood'/'RAV', 'dark heart of the wood').
card_uid('dark heart of the wood'/'RAV', 'RAV:Dark Heart of the Wood:dark heart of the wood').
card_rarity('dark heart of the wood'/'RAV', 'Uncommon').
card_artist('dark heart of the wood'/'RAV', 'Mark Tedin').
card_number('dark heart of the wood'/'RAV', '200').
card_flavor_text('dark heart of the wood'/'RAV', '\"The soil here is rich with the rotting dead, and the sun\'s fingers barely penetrate the dark canopy. Plant our brethren here. This shall be our sanctuary.\"\n—Savra').
card_multiverse_id('dark heart of the wood'/'RAV', '89017').
card_watermark('dark heart of the wood'/'RAV', 'Golgari').

card_in_set('darkblast', 'RAV').
card_original_type('darkblast'/'RAV', 'Instant').
card_original_text('darkblast'/'RAV', 'Target creature gets -1/-1 until end of turn.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('darkblast', 'RAV').
card_image_name('darkblast'/'RAV', 'darkblast').
card_uid('darkblast'/'RAV', 'RAV:Darkblast:darkblast').
card_rarity('darkblast'/'RAV', 'Uncommon').
card_artist('darkblast'/'RAV', 'Randy Gallegos').
card_number('darkblast'/'RAV', '82').
card_multiverse_id('darkblast'/'RAV', '87922').
card_watermark('darkblast'/'RAV', 'Golgari').

card_in_set('devouring light', 'RAV').
card_original_type('devouring light'/'RAV', 'Instant').
card_original_text('devouring light'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nRemove target attacking or blocking creature from the game.').
card_first_print('devouring light', 'RAV').
card_image_name('devouring light'/'RAV', 'devouring light').
card_uid('devouring light'/'RAV', 'RAV:Devouring Light:devouring light').
card_rarity('devouring light'/'RAV', 'Uncommon').
card_artist('devouring light'/'RAV', 'Pete Venters').
card_number('devouring light'/'RAV', '13').
card_flavor_text('devouring light'/'RAV', 'Into the light the good are welcomed, and with the light the evil are banished.').
card_multiverse_id('devouring light'/'RAV', '89040').
card_watermark('devouring light'/'RAV', 'Selesnya').

card_in_set('dimir aqueduct', 'RAV').
card_original_type('dimir aqueduct'/'RAV', 'Land').
card_original_text('dimir aqueduct'/'RAV', 'Dimir Aqueduct comes into play tapped.\nWhen Dimir Aqueduct comes into play, return a land you control to its owner\'s hand.\n{T}: Add {U}{B} to your mana pool.').
card_first_print('dimir aqueduct', 'RAV').
card_image_name('dimir aqueduct'/'RAV', 'dimir aqueduct').
card_uid('dimir aqueduct'/'RAV', 'RAV:Dimir Aqueduct:dimir aqueduct').
card_rarity('dimir aqueduct'/'RAV', 'Common').
card_artist('dimir aqueduct'/'RAV', 'John Avon').
card_number('dimir aqueduct'/'RAV', '276').
card_multiverse_id('dimir aqueduct'/'RAV', '87929').
card_watermark('dimir aqueduct'/'RAV', 'Dimir').

card_in_set('dimir cutpurse', 'RAV').
card_original_type('dimir cutpurse'/'RAV', 'Creature — Spirit').
card_original_text('dimir cutpurse'/'RAV', 'Whenever Dimir Cutpurse deals combat damage to a player, that player discards a card and you draw a card.').
card_first_print('dimir cutpurse', 'RAV').
card_image_name('dimir cutpurse'/'RAV', 'dimir cutpurse').
card_uid('dimir cutpurse'/'RAV', 'RAV:Dimir Cutpurse:dimir cutpurse').
card_rarity('dimir cutpurse'/'RAV', 'Rare').
card_artist('dimir cutpurse'/'RAV', 'Kev Walker').
card_number('dimir cutpurse'/'RAV', '201').
card_flavor_text('dimir cutpurse'/'RAV', 'Other guilds demand tolls from those who travel their territories, but not House Dimir. It takes its share secretly, one coin purse at a time.').
card_multiverse_id('dimir cutpurse'/'RAV', '87906').
card_watermark('dimir cutpurse'/'RAV', 'Dimir').

card_in_set('dimir doppelganger', 'RAV').
card_original_type('dimir doppelganger'/'RAV', 'Creature — Shapeshifter').
card_original_text('dimir doppelganger'/'RAV', '{1}{U}{B}: Remove target creature card in a graveyard from the game. Dimir Doppelganger becomes a copy of that card and gains this ability.').
card_first_print('dimir doppelganger', 'RAV').
card_image_name('dimir doppelganger'/'RAV', 'dimir doppelganger').
card_uid('dimir doppelganger'/'RAV', 'RAV:Dimir Doppelganger:dimir doppelganger').
card_rarity('dimir doppelganger'/'RAV', 'Rare').
card_artist('dimir doppelganger'/'RAV', 'Jim Murray').
card_number('dimir doppelganger'/'RAV', '202').
card_flavor_text('dimir doppelganger'/'RAV', '\"Fear not. Your life will not go unlived.\"').
card_multiverse_id('dimir doppelganger'/'RAV', '83850').
card_watermark('dimir doppelganger'/'RAV', 'Dimir').

card_in_set('dimir guildmage', 'RAV').
card_original_type('dimir guildmage'/'RAV', 'Creature — Human Wizard').
card_original_text('dimir guildmage'/'RAV', '({U/B} can be paid with either {U} or {B}.)\n{3}{U}: Target player draws a card. Play this ability only any time you could play a sorcery.\n{3}{B}: Target player discards a card. Play this ability only any time you could play a sorcery.').
card_image_name('dimir guildmage'/'RAV', 'dimir guildmage').
card_uid('dimir guildmage'/'RAV', 'RAV:Dimir Guildmage:dimir guildmage').
card_rarity('dimir guildmage'/'RAV', 'Uncommon').
card_artist('dimir guildmage'/'RAV', 'Adam Rex').
card_number('dimir guildmage'/'RAV', '245').
card_multiverse_id('dimir guildmage'/'RAV', '87990').
card_watermark('dimir guildmage'/'RAV', 'Dimir').

card_in_set('dimir house guard', 'RAV').
card_original_type('dimir house guard'/'RAV', 'Creature — Skeleton').
card_original_text('dimir house guard'/'RAV', 'Fear\nSacrifice a creature: Regenerate Dimir House Guard.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('dimir house guard', 'RAV').
card_image_name('dimir house guard'/'RAV', 'dimir house guard').
card_uid('dimir house guard'/'RAV', 'RAV:Dimir House Guard:dimir house guard').
card_rarity('dimir house guard'/'RAV', 'Common').
card_artist('dimir house guard'/'RAV', 'John Zeleznik').
card_number('dimir house guard'/'RAV', '83').
card_multiverse_id('dimir house guard'/'RAV', '87926').
card_watermark('dimir house guard'/'RAV', 'Dimir').

card_in_set('dimir infiltrator', 'RAV').
card_original_type('dimir infiltrator'/'RAV', 'Creature — Spirit').
card_original_text('dimir infiltrator'/'RAV', 'Dimir Infiltrator is unblockable.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('dimir infiltrator', 'RAV').
card_image_name('dimir infiltrator'/'RAV', 'dimir infiltrator').
card_uid('dimir infiltrator'/'RAV', 'RAV:Dimir Infiltrator:dimir infiltrator').
card_rarity('dimir infiltrator'/'RAV', 'Common').
card_artist('dimir infiltrator'/'RAV', 'Jim Nelson').
card_number('dimir infiltrator'/'RAV', '203').
card_multiverse_id('dimir infiltrator'/'RAV', '87987').
card_watermark('dimir infiltrator'/'RAV', 'Dimir').

card_in_set('dimir machinations', 'RAV').
card_original_type('dimir machinations'/'RAV', 'Sorcery').
card_original_text('dimir machinations'/'RAV', 'Look at the top three cards of target player\'s library. Remove any number of those cards from the game, then put the rest back in any order.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('dimir machinations', 'RAV').
card_image_name('dimir machinations'/'RAV', 'dimir machinations').
card_uid('dimir machinations'/'RAV', 'RAV:Dimir Machinations:dimir machinations').
card_rarity('dimir machinations'/'RAV', 'Uncommon').
card_artist('dimir machinations'/'RAV', 'Greg Staples').
card_number('dimir machinations'/'RAV', '84').
card_multiverse_id('dimir machinations'/'RAV', '87981').
card_watermark('dimir machinations'/'RAV', 'Dimir').

card_in_set('dimir signet', 'RAV').
card_original_type('dimir signet'/'RAV', 'Artifact').
card_original_text('dimir signet'/'RAV', '{1}, {T}: Add {U}{B} to your mana pool.').
card_first_print('dimir signet', 'RAV').
card_image_name('dimir signet'/'RAV', 'dimir signet').
card_uid('dimir signet'/'RAV', 'RAV:Dimir Signet:dimir signet').
card_rarity('dimir signet'/'RAV', 'Common').
card_artist('dimir signet'/'RAV', 'Tim Hildebrandt').
card_number('dimir signet'/'RAV', '260').
card_flavor_text('dimir signet'/'RAV', 'An emblem of a secret guild, the Dimir insignia is seen only by its own members—and the doomed.').
card_multiverse_id('dimir signet'/'RAV', '95535').
card_watermark('dimir signet'/'RAV', 'Dimir').

card_in_set('disembowel', 'RAV').
card_original_type('disembowel'/'RAV', 'Instant').
card_original_text('disembowel'/'RAV', 'Destroy target creature with converted mana cost X.').
card_first_print('disembowel', 'RAV').
card_image_name('disembowel'/'RAV', 'disembowel').
card_uid('disembowel'/'RAV', 'RAV:Disembowel:disembowel').
card_rarity('disembowel'/'RAV', 'Common').
card_artist('disembowel'/'RAV', 'Chengo McFlingers').
card_number('disembowel'/'RAV', '85').
card_flavor_text('disembowel'/'RAV', '\"We thought we found the victim\'s clothes lumped in the street. As it turned out, it was the victim, insides sucked out—just a sack of skin and robes.\"\n—Agrus Kos').
card_multiverse_id('disembowel'/'RAV', '87917').

card_in_set('divebomber griffin', 'RAV').
card_original_type('divebomber griffin'/'RAV', 'Creature — Griffin').
card_original_text('divebomber griffin'/'RAV', 'Flying\n{T}, Sacrifice Divebomber Griffin: Divebomber Griffin deals 3 damage to target attacking or blocking creature.').
card_first_print('divebomber griffin', 'RAV').
card_image_name('divebomber griffin'/'RAV', 'divebomber griffin').
card_uid('divebomber griffin'/'RAV', 'RAV:Divebomber Griffin:divebomber griffin').
card_rarity('divebomber griffin'/'RAV', 'Uncommon').
card_artist('divebomber griffin'/'RAV', 'Kev Walker').
card_number('divebomber griffin'/'RAV', '14').
card_flavor_text('divebomber griffin'/'RAV', 'Justice flies swiftly on angry wings.').
card_multiverse_id('divebomber griffin'/'RAV', '88975').

card_in_set('dizzy spell', 'RAV').
card_original_type('dizzy spell'/'RAV', 'Instant').
card_original_text('dizzy spell'/'RAV', 'Target creature gets -3/-0 until end of turn.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('dizzy spell', 'RAV').
card_image_name('dizzy spell'/'RAV', 'dizzy spell').
card_uid('dizzy spell'/'RAV', 'RAV:Dizzy Spell:dizzy spell').
card_rarity('dizzy spell'/'RAV', 'Common').
card_artist('dizzy spell'/'RAV', 'Christopher Moeller').
card_number('dizzy spell'/'RAV', '43').
card_multiverse_id('dizzy spell'/'RAV', '87925').
card_watermark('dizzy spell'/'RAV', 'Dimir').

card_in_set('dogpile', 'RAV').
card_original_type('dogpile'/'RAV', 'Instant').
card_original_text('dogpile'/'RAV', 'Dogpile deals damage to target creature or player equal to the number of attacking creatures you control.').
card_first_print('dogpile', 'RAV').
card_image_name('dogpile'/'RAV', 'dogpile').
card_uid('dogpile'/'RAV', 'RAV:Dogpile:dogpile').
card_rarity('dogpile'/'RAV', 'Common').
card_artist('dogpile'/'RAV', 'Jeff Miracola').
card_number('dogpile'/'RAV', '120').
card_flavor_text('dogpile'/'RAV', 'There is no shelter from this storm, its rain of fists, or the thunderous cackles of bloodlust.').
card_multiverse_id('dogpile'/'RAV', '87921').

card_in_set('doubling season', 'RAV').
card_original_type('doubling season'/'RAV', 'Enchantment').
card_original_text('doubling season'/'RAV', 'If an effect would put one or more tokens into play under your control, it puts twice that many into play instead.\nIf an effect would place one or more counters on a permanent you control, it places twice that many on that permanent instead.').
card_image_name('doubling season'/'RAV', 'doubling season').
card_uid('doubling season'/'RAV', 'RAV:Doubling Season:doubling season').
card_rarity('doubling season'/'RAV', 'Rare').
card_artist('doubling season'/'RAV', 'Wayne Reynolds').
card_number('doubling season'/'RAV', '158').
card_multiverse_id('doubling season'/'RAV', '89116').

card_in_set('dowsing shaman', 'RAV').
card_original_type('dowsing shaman'/'RAV', 'Creature — Centaur Shaman').
card_original_text('dowsing shaman'/'RAV', '{2}{G}, {T}: Return target enchantment card from your graveyard to your hand.').
card_first_print('dowsing shaman', 'RAV').
card_image_name('dowsing shaman'/'RAV', 'dowsing shaman').
card_uid('dowsing shaman'/'RAV', 'RAV:Dowsing Shaman:dowsing shaman').
card_rarity('dowsing shaman'/'RAV', 'Uncommon').
card_artist('dowsing shaman'/'RAV', 'Wayne Reynolds').
card_number('dowsing shaman'/'RAV', '159').
card_flavor_text('dowsing shaman'/'RAV', 'He combs the streets looking for the lost light, the discarded enigmas of the city.').
card_multiverse_id('dowsing shaman'/'RAV', '88968').

card_in_set('drake familiar', 'RAV').
card_original_type('drake familiar'/'RAV', 'Creature — Drake').
card_original_text('drake familiar'/'RAV', 'Flying\nWhen Drake Familiar comes into play, sacrifice it unless you return an enchantment in play to its owner\'s hand.').
card_first_print('drake familiar', 'RAV').
card_image_name('drake familiar'/'RAV', 'drake familiar').
card_uid('drake familiar'/'RAV', 'RAV:Drake Familiar:drake familiar').
card_rarity('drake familiar'/'RAV', 'Common').
card_artist('drake familiar'/'RAV', 'Darrell Riche').
card_number('drake familiar'/'RAV', '44').
card_flavor_text('drake familiar'/'RAV', '\"Falconry? A fine sport I suppose, if you\'re attracted to the frailty of birds.\"\n—Trivaz, Izzet mage').
card_multiverse_id('drake familiar'/'RAV', '87960').

card_in_set('dream leash', 'RAV').
card_original_type('dream leash'/'RAV', 'Enchantment — Aura').
card_original_text('dream leash'/'RAV', 'Enchant permanent\nYou may play Dream Leash only on a tapped permanent.\nYou control enchanted permanent.').
card_first_print('dream leash', 'RAV').
card_image_name('dream leash'/'RAV', 'dream leash').
card_uid('dream leash'/'RAV', 'RAV:Dream Leash:dream leash').
card_rarity('dream leash'/'RAV', 'Rare').
card_artist('dream leash'/'RAV', 'Alex Horley-Orlandelli').
card_number('dream leash'/'RAV', '45').
card_flavor_text('dream leash'/'RAV', 'The tendrils of sorcery find easy purchase on the soft underbelly of dreams.').
card_multiverse_id('dream leash'/'RAV', '88945').

card_in_set('drift of phantasms', 'RAV').
card_original_type('drift of phantasms'/'RAV', 'Creature — Spirit').
card_original_text('drift of phantasms'/'RAV', 'Defender (This creature can\'t attack.)\nFlying\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('drift of phantasms', 'RAV').
card_image_name('drift of phantasms'/'RAV', 'drift of phantasms').
card_uid('drift of phantasms'/'RAV', 'RAV:Drift of Phantasms:drift of phantasms').
card_rarity('drift of phantasms'/'RAV', 'Common').
card_artist('drift of phantasms'/'RAV', 'Michael Phillippi').
card_number('drift of phantasms'/'RAV', '46').
card_multiverse_id('drift of phantasms'/'RAV', '87994').
card_watermark('drift of phantasms'/'RAV', 'Dimir').

card_in_set('dromad purebred', 'RAV').
card_original_type('dromad purebred'/'RAV', 'Creature — Beast').
card_original_text('dromad purebred'/'RAV', 'Whenever Dromad Purebred is dealt damage, you gain 1 life.').
card_first_print('dromad purebred', 'RAV').
card_image_name('dromad purebred'/'RAV', 'dromad purebred').
card_uid('dromad purebred'/'RAV', 'RAV:Dromad Purebred:dromad purebred').
card_rarity('dromad purebred'/'RAV', 'Common').
card_artist('dromad purebred'/'RAV', 'Carl Critchlow').
card_number('dromad purebred'/'RAV', '15').
card_flavor_text('dromad purebred'/'RAV', '\"I have seen much from the back of my dromad, most of it terribly wrong. The more I see, the more I am convinced of the rightness of my path.\"\n—Heruj, Selesnya initiate').
card_multiverse_id('dromad purebred'/'RAV', '87911').

card_in_set('drooling groodion', 'RAV').
card_original_type('drooling groodion'/'RAV', 'Creature — Beast').
card_original_text('drooling groodion'/'RAV', '{2}{B}{G}, Sacrifice a creature: Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_first_print('drooling groodion', 'RAV').
card_image_name('drooling groodion'/'RAV', 'drooling groodion').
card_uid('drooling groodion'/'RAV', 'RAV:Drooling Groodion:drooling groodion').
card_rarity('drooling groodion'/'RAV', 'Uncommon').
card_artist('drooling groodion'/'RAV', 'Kev Walker').
card_number('drooling groodion'/'RAV', '204').
card_flavor_text('drooling groodion'/'RAV', '\"The Golgari expand, yes, but I refuse to call their tainted creations ‘growth.\'\"\n—Veszka, Selesnya evangel').
card_multiverse_id('drooling groodion'/'RAV', '87989').
card_watermark('drooling groodion'/'RAV', 'Golgari').

card_in_set('dryad\'s caress', 'RAV').
card_original_type('dryad\'s caress'/'RAV', 'Instant').
card_original_text('dryad\'s caress'/'RAV', 'You gain 1 life for each creature in play. If {W} was spent to play Dryad\'s Caress, untap all creatures you control.').
card_first_print('dryad\'s caress', 'RAV').
card_image_name('dryad\'s caress'/'RAV', 'dryad\'s caress').
card_uid('dryad\'s caress'/'RAV', 'RAV:Dryad\'s Caress:dryad\'s caress').
card_rarity('dryad\'s caress'/'RAV', 'Common').
card_artist('dryad\'s caress'/'RAV', 'Randy Gallegos').
card_number('dryad\'s caress'/'RAV', '160').
card_flavor_text('dryad\'s caress'/'RAV', '\"I awoke to the face of beauty, my body fully healed. In that moment I knew my destiny was the Conclave\'s to shape.\"\n—Rogad, Selesnya initiate').
card_multiverse_id('dryad\'s caress'/'RAV', '87958').
card_watermark('dryad\'s caress'/'RAV', 'Selesnya').

card_in_set('duskmantle, house of shadow', 'RAV').
card_original_type('duskmantle, house of shadow'/'RAV', 'Land').
card_original_text('duskmantle, house of shadow'/'RAV', '{T}: Add {1} to your mana pool.\n{U}{B}, {T}: Target player puts the top card of his or her library into his or her graveyard.').
card_first_print('duskmantle, house of shadow', 'RAV').
card_image_name('duskmantle, house of shadow'/'RAV', 'duskmantle, house of shadow').
card_uid('duskmantle, house of shadow'/'RAV', 'RAV:Duskmantle, House of Shadow:duskmantle, house of shadow').
card_rarity('duskmantle, house of shadow'/'RAV', 'Uncommon').
card_artist('duskmantle, house of shadow'/'RAV', 'Martina Pilcerova').
card_number('duskmantle, house of shadow'/'RAV', '277').
card_flavor_text('duskmantle, house of shadow'/'RAV', 'In a space where there is no room, in a structure that was never built, meets the guild that doesn\'t exist.').
card_multiverse_id('duskmantle, house of shadow'/'RAV', '88943').
card_watermark('duskmantle, house of shadow'/'RAV', 'Dimir').

card_in_set('elves of deep shadow', 'RAV').
card_original_type('elves of deep shadow'/'RAV', 'Creature — Elf Druid').
card_original_text('elves of deep shadow'/'RAV', '{T}: Add {B} to your mana pool. Elves of Deep Shadow deals 1 damage to you.').
card_image_name('elves of deep shadow'/'RAV', 'elves of deep shadow').
card_uid('elves of deep shadow'/'RAV', 'RAV:Elves of Deep Shadow:elves of deep shadow').
card_rarity('elves of deep shadow'/'RAV', 'Common').
card_artist('elves of deep shadow'/'RAV', 'Justin Sweet').
card_number('elves of deep shadow'/'RAV', '161').
card_flavor_text('elves of deep shadow'/'RAV', 'Cast out of the Conclave generations ago, these elves found a home in the corrupted districts of the Golgari.').
card_multiverse_id('elves of deep shadow'/'RAV', '83833').
card_watermark('elves of deep shadow'/'RAV', 'Golgari').

card_in_set('elvish skysweeper', 'RAV').
card_original_type('elvish skysweeper'/'RAV', 'Creature — Elf Warrior').
card_original_text('elvish skysweeper'/'RAV', '{4}{G}, Sacrifice a creature: Destroy target creature with flying.').
card_first_print('elvish skysweeper', 'RAV').
card_image_name('elvish skysweeper'/'RAV', 'elvish skysweeper').
card_uid('elvish skysweeper'/'RAV', 'RAV:Elvish Skysweeper:elvish skysweeper').
card_rarity('elvish skysweeper'/'RAV', 'Common').
card_artist('elvish skysweeper'/'RAV', 'Mark Tedin').
card_number('elvish skysweeper'/'RAV', '162').
card_flavor_text('elvish skysweeper'/'RAV', 'The spires of Ravnica are no different from the tall trees of other planes. The elves navigate and protect them just the same.').
card_multiverse_id('elvish skysweeper'/'RAV', '87928').

card_in_set('empty the catacombs', 'RAV').
card_original_type('empty the catacombs'/'RAV', 'Sorcery').
card_original_text('empty the catacombs'/'RAV', 'Each player returns all creature cards from his or her graveyard to his or her hand.').
card_first_print('empty the catacombs', 'RAV').
card_image_name('empty the catacombs'/'RAV', 'empty the catacombs').
card_uid('empty the catacombs'/'RAV', 'RAV:Empty the Catacombs:empty the catacombs').
card_rarity('empty the catacombs'/'RAV', 'Rare').
card_artist('empty the catacombs'/'RAV', 'Mark A. Nelson').
card_number('empty the catacombs'/'RAV', '86').
card_flavor_text('empty the catacombs'/'RAV', 'When the dead are laid to rest in Ravnica, it\'s usually just a nap.').
card_multiverse_id('empty the catacombs'/'RAV', '89015').

card_in_set('ethereal usher', 'RAV').
card_original_type('ethereal usher'/'RAV', 'Creature — Spirit').
card_original_text('ethereal usher'/'RAV', '{U}, {T}: Target creature is unblockable this turn.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('ethereal usher', 'RAV').
card_image_name('ethereal usher'/'RAV', 'ethereal usher').
card_uid('ethereal usher'/'RAV', 'RAV:Ethereal Usher:ethereal usher').
card_rarity('ethereal usher'/'RAV', 'Uncommon').
card_artist('ethereal usher'/'RAV', 'Mark A. Nelson').
card_number('ethereal usher'/'RAV', '47').
card_multiverse_id('ethereal usher'/'RAV', '89023').
card_watermark('ethereal usher'/'RAV', 'Dimir').

card_in_set('excruciator', 'RAV').
card_original_type('excruciator'/'RAV', 'Creature — Avatar').
card_original_text('excruciator'/'RAV', 'Damage that would be dealt by Excruciator can\'t be prevented.').
card_first_print('excruciator', 'RAV').
card_image_name('excruciator'/'RAV', 'excruciator').
card_uid('excruciator'/'RAV', 'RAV:Excruciator:excruciator').
card_rarity('excruciator'/'RAV', 'Rare').
card_artist('excruciator'/'RAV', 'Paolo Parente').
card_number('excruciator'/'RAV', '121').
card_flavor_text('excruciator'/'RAV', '\"Though used as a piercing weapon, the tusk is more akin to a stinger, spreading pain instantly throughout the body of its victim. This specimen deserves further study.\"\n—Simic research notes').
card_multiverse_id('excruciator'/'RAV', '87900').

card_in_set('eye of the storm', 'RAV').
card_original_type('eye of the storm'/'RAV', 'Enchantment').
card_original_text('eye of the storm'/'RAV', 'Whenever a player plays an instant or sorcery card, remove it from the game. Then that player copies each instant or sorcery card removed from the game with Eye of the Storm. For each copy, the player may play the copy without paying its mana cost.').
card_first_print('eye of the storm', 'RAV').
card_image_name('eye of the storm'/'RAV', 'eye of the storm').
card_uid('eye of the storm'/'RAV', 'RAV:Eye of the Storm:eye of the storm').
card_rarity('eye of the storm'/'RAV', 'Rare').
card_artist('eye of the storm'/'RAV', 'Hideaki Takamura').
card_number('eye of the storm'/'RAV', '48').
card_multiverse_id('eye of the storm'/'RAV', '83791').

card_in_set('faith\'s fetters', 'RAV').
card_original_type('faith\'s fetters'/'RAV', 'Enchantment — Aura').
card_original_text('faith\'s fetters'/'RAV', 'Enchant permanent\nWhen Faith\'s Fetters comes into play, you gain 4 life.\nEnchanted permanent\'s activated abilities can\'t be played unless they\'re mana abilities. If enchanted permanent is a creature, it can\'t attack or block.').
card_first_print('faith\'s fetters', 'RAV').
card_image_name('faith\'s fetters'/'RAV', 'faith\'s fetters').
card_uid('faith\'s fetters'/'RAV', 'RAV:Faith\'s Fetters:faith\'s fetters').
card_rarity('faith\'s fetters'/'RAV', 'Common').
card_artist('faith\'s fetters'/'RAV', 'Chippy').
card_number('faith\'s fetters'/'RAV', '16').
card_multiverse_id('faith\'s fetters'/'RAV', '83609').

card_in_set('farseek', 'RAV').
card_original_type('farseek'/'RAV', 'Sorcery').
card_original_text('farseek'/'RAV', 'Search your library for a Plains, Island, Swamp, or Mountain card and put it into play tapped. Then shuffle your library.').
card_image_name('farseek'/'RAV', 'farseek').
card_uid('farseek'/'RAV', 'RAV:Farseek:farseek').
card_rarity('farseek'/'RAV', 'Common').
card_artist('farseek'/'RAV', 'Martina Pilcerova').
card_number('farseek'/'RAV', '163').
card_flavor_text('farseek'/'RAV', '\"How truly vast this city must be, that I have traveled so far and seen so much, yet never once found the place where the buildings fail.\"').
card_multiverse_id('farseek'/'RAV', '87970').

card_in_set('festival of the guildpact', 'RAV').
card_original_type('festival of the guildpact'/'RAV', 'Instant').
card_original_text('festival of the guildpact'/'RAV', 'Prevent the next X damage that would be dealt to you this turn.\nDraw a card.').
card_first_print('festival of the guildpact', 'RAV').
card_image_name('festival of the guildpact'/'RAV', 'festival of the guildpact').
card_uid('festival of the guildpact'/'RAV', 'RAV:Festival of the Guildpact:festival of the guildpact').
card_rarity('festival of the guildpact'/'RAV', 'Uncommon').
card_artist('festival of the guildpact'/'RAV', 'Alex Horley-Orlandelli').
card_number('festival of the guildpact'/'RAV', '17').
card_flavor_text('festival of the guildpact'/'RAV', 'Everyone knows that violence at the Festival brings the worst of luck, so for at least one day a year arms are laid down in favor of brimming goblets.').
card_multiverse_id('festival of the guildpact'/'RAV', '83884').

card_in_set('fiery conclusion', 'RAV').
card_original_type('fiery conclusion'/'RAV', 'Instant').
card_original_text('fiery conclusion'/'RAV', 'As an additional cost to play Fiery Conclusion, sacrifice a creature.\nFiery Conclusion deals 5 damage to target creature.').
card_first_print('fiery conclusion', 'RAV').
card_image_name('fiery conclusion'/'RAV', 'fiery conclusion').
card_uid('fiery conclusion'/'RAV', 'RAV:Fiery Conclusion:fiery conclusion').
card_rarity('fiery conclusion'/'RAV', 'Common').
card_artist('fiery conclusion'/'RAV', 'Paolo Parente').
card_number('fiery conclusion'/'RAV', '122').
card_flavor_text('fiery conclusion'/'RAV', 'The Boros legionnaire saw a noble sacrifice, the Rakdos thug a blazing suicide, and the Izzet alchemist an experiment gone awry.').
card_multiverse_id('fiery conclusion'/'RAV', '83620').

card_in_set('firemane angel', 'RAV').
card_original_type('firemane angel'/'RAV', 'Creature — Angel').
card_original_text('firemane angel'/'RAV', 'Flying, first strike\nAt the beginning of your upkeep, if Firemane Angel is in your graveyard or in play, you may gain 1 life.\n{6}{R}{R}{W}{W}: Return Firemane Angel from your graveyard to play. Play this ability only during your upkeep.').
card_first_print('firemane angel', 'RAV').
card_image_name('firemane angel'/'RAV', 'firemane angel').
card_uid('firemane angel'/'RAV', 'RAV:Firemane Angel:firemane angel').
card_rarity('firemane angel'/'RAV', 'Rare').
card_artist('firemane angel'/'RAV', 'Matt Cavotta').
card_number('firemane angel'/'RAV', '205').
card_multiverse_id('firemane angel'/'RAV', '89074').
card_watermark('firemane angel'/'RAV', 'Boros').

card_in_set('fists of ironwood', 'RAV').
card_original_type('fists of ironwood'/'RAV', 'Enchantment — Aura').
card_original_text('fists of ironwood'/'RAV', 'Enchant creature\nWhen Fists of Ironwood comes into play, put two 1/1 green Saproling creature tokens into play.\nEnchanted creature has trample.').
card_first_print('fists of ironwood', 'RAV').
card_image_name('fists of ironwood'/'RAV', 'fists of ironwood').
card_uid('fists of ironwood'/'RAV', 'RAV:Fists of Ironwood:fists of ironwood').
card_rarity('fists of ironwood'/'RAV', 'Common').
card_artist('fists of ironwood'/'RAV', 'Glen Angus').
card_number('fists of ironwood'/'RAV', '164').
card_flavor_text('fists of ironwood'/'RAV', 'Saprolings add the three and the four to the \"one-two punch.\"').
card_multiverse_id('fists of ironwood'/'RAV', '83672').

card_in_set('flame fusillade', 'RAV').
card_original_type('flame fusillade'/'RAV', 'Sorcery').
card_original_text('flame fusillade'/'RAV', 'Until end of turn, permanents you control gain \"{T}: This permanent deals 1 damage to target creature or player.\"').
card_first_print('flame fusillade', 'RAV').
card_image_name('flame fusillade'/'RAV', 'flame fusillade').
card_uid('flame fusillade'/'RAV', 'RAV:Flame Fusillade:flame fusillade').
card_rarity('flame fusillade'/'RAV', 'Rare').
card_artist('flame fusillade'/'RAV', 'Dany Orizio').
card_number('flame fusillade'/'RAV', '123').
card_flavor_text('flame fusillade'/'RAV', '\"A single item acting as lantern and lance? Now that\'s military efficiency.\"\n—Brev Grezar, Boros lieutenant').
card_multiverse_id('flame fusillade'/'RAV', '83912').

card_in_set('flame-kin zealot', 'RAV').
card_original_type('flame-kin zealot'/'RAV', 'Creature — Elemental Berserker').
card_original_text('flame-kin zealot'/'RAV', 'When Flame-Kin Zealot comes into play, creatures you control get +1/+1 and gain haste until end of turn.').
card_first_print('flame-kin zealot', 'RAV').
card_image_name('flame-kin zealot'/'RAV', 'flame-kin zealot').
card_uid('flame-kin zealot'/'RAV', 'RAV:Flame-Kin Zealot:flame-kin zealot').
card_rarity('flame-kin zealot'/'RAV', 'Uncommon').
card_artist('flame-kin zealot'/'RAV', 'Arnie Swekel').
card_number('flame-kin zealot'/'RAV', '206').
card_flavor_text('flame-kin zealot'/'RAV', 'Boros soldiers are like a cache of bombs ready to explode, and the flame-kin light their fuses.').
card_multiverse_id('flame-kin zealot'/'RAV', '83562').
card_watermark('flame-kin zealot'/'RAV', 'Boros').

card_in_set('flash conscription', 'RAV').
card_original_type('flash conscription'/'RAV', 'Instant').
card_original_text('flash conscription'/'RAV', 'Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn. If {W} was spent to play Flash Conscription, the creature gains \"Whenever this creature deals combat damage, you gain that much life\" until end of turn.').
card_first_print('flash conscription', 'RAV').
card_image_name('flash conscription'/'RAV', 'flash conscription').
card_uid('flash conscription'/'RAV', 'RAV:Flash Conscription:flash conscription').
card_rarity('flash conscription'/'RAV', 'Uncommon').
card_artist('flash conscription'/'RAV', 'Stephen Tappin').
card_number('flash conscription'/'RAV', '124').
card_multiverse_id('flash conscription'/'RAV', '87953').
card_watermark('flash conscription'/'RAV', 'Boros').

card_in_set('flickerform', 'RAV').
card_original_type('flickerform'/'RAV', 'Enchantment — Aura').
card_original_text('flickerform'/'RAV', 'Enchant creature\n{2}{W}{W}: Remove enchanted creature and all Auras attached to it from the game. At end of turn, return that card to play under its owner\'s control. If you do, return those Auras to play under their owners\' control enchanting that creature.').
card_first_print('flickerform', 'RAV').
card_image_name('flickerform'/'RAV', 'flickerform').
card_uid('flickerform'/'RAV', 'RAV:Flickerform:flickerform').
card_rarity('flickerform'/'RAV', 'Rare').
card_artist('flickerform'/'RAV', 'Ron Spears').
card_number('flickerform'/'RAV', '18').
card_multiverse_id('flickerform'/'RAV', '83602').

card_in_set('flight of fancy', 'RAV').
card_original_type('flight of fancy'/'RAV', 'Enchantment — Aura').
card_original_text('flight of fancy'/'RAV', 'Enchant creature\nWhen Flight of Fancy comes into play, draw two cards.\nEnchanted creature has flying.').
card_first_print('flight of fancy', 'RAV').
card_image_name('flight of fancy'/'RAV', 'flight of fancy').
card_uid('flight of fancy'/'RAV', 'RAV:Flight of Fancy:flight of fancy').
card_rarity('flight of fancy'/'RAV', 'Common').
card_artist('flight of fancy'/'RAV', 'Glen Angus').
card_number('flight of fancy'/'RAV', '49').
card_flavor_text('flight of fancy'/'RAV', 'The view from above is an inspiration to the newly winged.').
card_multiverse_id('flight of fancy'/'RAV', '83677').

card_in_set('flow of ideas', 'RAV').
card_original_type('flow of ideas'/'RAV', 'Sorcery').
card_original_text('flow of ideas'/'RAV', 'Draw a card for each Island you control.').
card_first_print('flow of ideas', 'RAV').
card_image_name('flow of ideas'/'RAV', 'flow of ideas').
card_uid('flow of ideas'/'RAV', 'RAV:Flow of Ideas:flow of ideas').
card_rarity('flow of ideas'/'RAV', 'Uncommon').
card_artist('flow of ideas'/'RAV', 'Dan Scott').
card_number('flow of ideas'/'RAV', '50').
card_flavor_text('flow of ideas'/'RAV', '\"A system to direct the flow of Ravnica\'s entire water supply? Thinking a bit small, aren\'t we?\"\n—Trivaz, Izzet mage').
card_multiverse_id('flow of ideas'/'RAV', '87978').

card_in_set('followed footsteps', 'RAV').
card_original_type('followed footsteps'/'RAV', 'Enchantment — Aura').
card_original_text('followed footsteps'/'RAV', 'Enchant creature\nAt the beginning of your upkeep, put a creature token into play that\'s a copy of enchanted creature.').
card_first_print('followed footsteps', 'RAV').
card_image_name('followed footsteps'/'RAV', 'followed footsteps').
card_uid('followed footsteps'/'RAV', 'RAV:Followed Footsteps:followed footsteps').
card_rarity('followed footsteps'/'RAV', 'Rare').
card_artist('followed footsteps'/'RAV', 'Glen Angus').
card_number('followed footsteps'/'RAV', '51').
card_flavor_text('followed footsteps'/'RAV', 'Æther can turn a footprint into a blueprint.').
card_multiverse_id('followed footsteps'/'RAV', '83699').

card_in_set('forest', 'RAV').
card_original_type('forest'/'RAV', 'Basic Land — Forest').
card_original_text('forest'/'RAV', 'G').
card_image_name('forest'/'RAV', 'forest1').
card_uid('forest'/'RAV', 'RAV:Forest:forest1').
card_rarity('forest'/'RAV', 'Basic Land').
card_artist('forest'/'RAV', 'Stephan Martiniere').
card_number('forest'/'RAV', '303').
card_multiverse_id('forest'/'RAV', '95097').

card_in_set('forest', 'RAV').
card_original_type('forest'/'RAV', 'Basic Land — Forest').
card_original_text('forest'/'RAV', 'G').
card_image_name('forest'/'RAV', 'forest2').
card_uid('forest'/'RAV', 'RAV:Forest:forest2').
card_rarity('forest'/'RAV', 'Basic Land').
card_artist('forest'/'RAV', 'Christopher Moeller').
card_number('forest'/'RAV', '304').
card_multiverse_id('forest'/'RAV', '95099').

card_in_set('forest', 'RAV').
card_original_type('forest'/'RAV', 'Basic Land — Forest').
card_original_text('forest'/'RAV', 'G').
card_image_name('forest'/'RAV', 'forest3').
card_uid('forest'/'RAV', 'RAV:Forest:forest3').
card_rarity('forest'/'RAV', 'Basic Land').
card_artist('forest'/'RAV', 'Anthony S. Waters').
card_number('forest'/'RAV', '305').
card_multiverse_id('forest'/'RAV', '95098').

card_in_set('forest', 'RAV').
card_original_type('forest'/'RAV', 'Basic Land — Forest').
card_original_text('forest'/'RAV', 'G').
card_image_name('forest'/'RAV', 'forest4').
card_uid('forest'/'RAV', 'RAV:Forest:forest4').
card_rarity('forest'/'RAV', 'Basic Land').
card_artist('forest'/'RAV', 'Richard Wright').
card_number('forest'/'RAV', '306').
card_multiverse_id('forest'/'RAV', '95106').

card_in_set('frenzied goblin', 'RAV').
card_original_type('frenzied goblin'/'RAV', 'Creature — Goblin Berserker').
card_original_text('frenzied goblin'/'RAV', 'Whenever Frenzied Goblin attacks, you may pay {R}. If you do, target creature can\'t block this turn.').
card_image_name('frenzied goblin'/'RAV', 'frenzied goblin').
card_uid('frenzied goblin'/'RAV', 'RAV:Frenzied Goblin:frenzied goblin').
card_rarity('frenzied goblin'/'RAV', 'Uncommon').
card_artist('frenzied goblin'/'RAV', 'Carl Critchlow').
card_number('frenzied goblin'/'RAV', '125').
card_flavor_text('frenzied goblin'/'RAV', 'The upside to not thinking about the consequences is that you\'ll always surprise those who do.').
card_multiverse_id('frenzied goblin'/'RAV', '87947').

card_in_set('galvanic arc', 'RAV').
card_original_type('galvanic arc'/'RAV', 'Enchantment — Aura').
card_original_text('galvanic arc'/'RAV', 'Enchant creature\nWhen Galvanic Arc comes into play, it deals 3 damage to target creature or player.\nEnchanted creature has first strike.').
card_first_print('galvanic arc', 'RAV').
card_image_name('galvanic arc'/'RAV', 'galvanic arc').
card_uid('galvanic arc'/'RAV', 'RAV:Galvanic Arc:galvanic arc').
card_rarity('galvanic arc'/'RAV', 'Common').
card_artist('galvanic arc'/'RAV', 'Michael Sutfin').
card_number('galvanic arc'/'RAV', '126').
card_multiverse_id('galvanic arc'/'RAV', '87968').

card_in_set('gate hound', 'RAV').
card_original_type('gate hound'/'RAV', 'Creature — Hound').
card_original_text('gate hound'/'RAV', 'Creatures you control have vigilance as long as Gate Hound is enchanted.').
card_first_print('gate hound', 'RAV').
card_image_name('gate hound'/'RAV', 'gate hound').
card_uid('gate hound'/'RAV', 'RAV:Gate Hound:gate hound').
card_rarity('gate hound'/'RAV', 'Common').
card_artist('gate hound'/'RAV', 'Ralph Horsley').
card_number('gate hound'/'RAV', '19').
card_flavor_text('gate hound'/'RAV', '\"Ditri, I leave this checkpoint in your capable teeth.\"\n—Kitov, nightguard patrol').
card_multiverse_id('gate hound'/'RAV', '87982').

card_in_set('gather courage', 'RAV').
card_original_type('gather courage'/'RAV', 'Instant').
card_original_text('gather courage'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nTarget creature gets +2/+2 until end of turn.').
card_first_print('gather courage', 'RAV').
card_image_name('gather courage'/'RAV', 'gather courage').
card_uid('gather courage'/'RAV', 'RAV:Gather Courage:gather courage').
card_rarity('gather courage'/'RAV', 'Common').
card_artist('gather courage'/'RAV', 'Brian Despain').
card_number('gather courage'/'RAV', '165').
card_multiverse_id('gather courage'/'RAV', '87940').
card_watermark('gather courage'/'RAV', 'Selesnya').

card_in_set('gaze of the gorgon', 'RAV').
card_original_type('gaze of the gorgon'/'RAV', 'Instant').
card_original_text('gaze of the gorgon'/'RAV', '({B/G} can be paid with either {B} or {G}.)\nRegenerate target creature. At end of combat, destroy all creatures that blocked or were blocked by that creature this turn.').
card_first_print('gaze of the gorgon', 'RAV').
card_image_name('gaze of the gorgon'/'RAV', 'gaze of the gorgon').
card_uid('gaze of the gorgon'/'RAV', 'RAV:Gaze of the Gorgon:gaze of the gorgon').
card_rarity('gaze of the gorgon'/'RAV', 'Common').
card_artist('gaze of the gorgon'/'RAV', 'Stephen Tappin').
card_number('gaze of the gorgon'/'RAV', '246').
card_flavor_text('gaze of the gorgon'/'RAV', 'Even the dead give way to the mineral gaze of the gorgon.').
card_multiverse_id('gaze of the gorgon'/'RAV', '83933').
card_watermark('gaze of the gorgon'/'RAV', 'Golgari').

card_in_set('ghosts of the innocent', 'RAV').
card_original_type('ghosts of the innocent'/'RAV', 'Creature — Spirit').
card_original_text('ghosts of the innocent'/'RAV', 'If a source would deal damage to a creature or player, it deals half that damage, rounded down, to that creature or player instead.').
card_first_print('ghosts of the innocent', 'RAV').
card_image_name('ghosts of the innocent'/'RAV', 'ghosts of the innocent').
card_uid('ghosts of the innocent'/'RAV', 'RAV:Ghosts of the Innocent:ghosts of the innocent').
card_rarity('ghosts of the innocent'/'RAV', 'Rare').
card_artist('ghosts of the innocent'/'RAV', 'Kev Walker').
card_number('ghosts of the innocent'/'RAV', '20').
card_flavor_text('ghosts of the innocent'/'RAV', '\"Ma said we should offer up blini-cakes and salt to the good ones, but I get that chill up my spine and just shut the door.\"\n—Otak, Tin Street shopkeep').
card_multiverse_id('ghosts of the innocent'/'RAV', '89042').

card_in_set('glare of subdual', 'RAV').
card_original_type('glare of subdual'/'RAV', 'Enchantment').
card_original_text('glare of subdual'/'RAV', 'Tap an untapped creature you control: Tap target artifact or creature.').
card_first_print('glare of subdual', 'RAV').
card_image_name('glare of subdual'/'RAV', 'glare of subdual').
card_uid('glare of subdual'/'RAV', 'RAV:Glare of Subdual:glare of subdual').
card_rarity('glare of subdual'/'RAV', 'Rare').
card_artist('glare of subdual'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('glare of subdual'/'RAV', '207').
card_flavor_text('glare of subdual'/'RAV', 'The righteous light of Selesnya is channeled through the devout, striking out to blind the nonbelievers.').
card_multiverse_id('glare of subdual'/'RAV', '89027').
card_watermark('glare of subdual'/'RAV', 'Selesnya').

card_in_set('glass golem', 'RAV').
card_original_type('glass golem'/'RAV', 'Artifact Creature — Golem').
card_original_text('glass golem'/'RAV', '').
card_first_print('glass golem', 'RAV').
card_image_name('glass golem'/'RAV', 'glass golem').
card_uid('glass golem'/'RAV', 'RAV:Glass Golem:glass golem').
card_rarity('glass golem'/'RAV', 'Uncommon').
card_artist('glass golem'/'RAV', 'Glen Angus').
card_number('glass golem'/'RAV', '261').
card_flavor_text('glass golem'/'RAV', 'Izzet artificers have learned to steer their beautiful constructs clear of Boros warhammers—and the opera house.').
card_multiverse_id('glass golem'/'RAV', '83719').

card_in_set('gleancrawler', 'RAV').
card_original_type('gleancrawler'/'RAV', 'Creature — Insect Horror').
card_original_text('gleancrawler'/'RAV', '({B/G} can be paid with either {B} or {G}.)\nTrample\nAt the end of your turn, return to your hand all creature cards in your graveyard that were put into your graveyard from play this turn.').
card_image_name('gleancrawler'/'RAV', 'gleancrawler').
card_uid('gleancrawler'/'RAV', 'RAV:Gleancrawler:gleancrawler').
card_rarity('gleancrawler'/'RAV', 'Rare').
card_artist('gleancrawler'/'RAV', 'Dave Allsop').
card_number('gleancrawler'/'RAV', '247').
card_multiverse_id('gleancrawler'/'RAV', '83702').
card_watermark('gleancrawler'/'RAV', 'Golgari').

card_in_set('glimpse the unthinkable', 'RAV').
card_original_type('glimpse the unthinkable'/'RAV', 'Sorcery').
card_original_text('glimpse the unthinkable'/'RAV', 'Target player puts the top ten cards of his or her library into his or her graveyard.').
card_first_print('glimpse the unthinkable', 'RAV').
card_image_name('glimpse the unthinkable'/'RAV', 'glimpse the unthinkable').
card_uid('glimpse the unthinkable'/'RAV', 'RAV:Glimpse the Unthinkable:glimpse the unthinkable').
card_rarity('glimpse the unthinkable'/'RAV', 'Rare').
card_artist('glimpse the unthinkable'/'RAV', 'Brandon Kitkouski').
card_number('glimpse the unthinkable'/'RAV', '208').
card_flavor_text('glimpse the unthinkable'/'RAV', '\"I am confident that if anyone actually penetrates our facades, even the most perceptive would still be fundamentally unprepared for the truth of House Dimir.\"\n—Szadek').
card_multiverse_id('glimpse the unthinkable'/'RAV', '83597').
card_watermark('glimpse the unthinkable'/'RAV', 'Dimir').

card_in_set('goblin fire fiend', 'RAV').
card_original_type('goblin fire fiend'/'RAV', 'Creature — Goblin Berserker').
card_original_text('goblin fire fiend'/'RAV', 'Haste\nDefending player blocks Goblin Fire Fiend if able.\n{R}: Goblin Fire Fiend gets +1/+0 until end of turn.').
card_first_print('goblin fire fiend', 'RAV').
card_image_name('goblin fire fiend'/'RAV', 'goblin fire fiend').
card_uid('goblin fire fiend'/'RAV', 'RAV:Goblin Fire Fiend:goblin fire fiend').
card_rarity('goblin fire fiend'/'RAV', 'Common').
card_artist('goblin fire fiend'/'RAV', 'Paolo Parente').
card_number('goblin fire fiend'/'RAV', '127').
card_multiverse_id('goblin fire fiend'/'RAV', '87965').

card_in_set('goblin spelunkers', 'RAV').
card_original_type('goblin spelunkers'/'RAV', 'Creature — Goblin Warrior').
card_original_text('goblin spelunkers'/'RAV', 'Mountainwalk').
card_image_name('goblin spelunkers'/'RAV', 'goblin spelunkers').
card_uid('goblin spelunkers'/'RAV', 'RAV:Goblin Spelunkers:goblin spelunkers').
card_rarity('goblin spelunkers'/'RAV', 'Common').
card_artist('goblin spelunkers'/'RAV', 'Glenn Fabry').
card_number('goblin spelunkers'/'RAV', '128').
card_flavor_text('goblin spelunkers'/'RAV', 'Chimney sweeps, explorers of abandoned buildings, spire climbers . . . goblin spelunkers have found countless niches within Ravnica\'s metropolis.').
card_multiverse_id('goblin spelunkers'/'RAV', '83803').

card_in_set('golgari brownscale', 'RAV').
card_original_type('golgari brownscale'/'RAV', 'Creature — Lizard').
card_original_text('golgari brownscale'/'RAV', 'When Golgari Brownscale is put into your hand from your graveyard, you gain 2 life.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('golgari brownscale', 'RAV').
card_image_name('golgari brownscale'/'RAV', 'golgari brownscale').
card_uid('golgari brownscale'/'RAV', 'RAV:Golgari Brownscale:golgari brownscale').
card_rarity('golgari brownscale'/'RAV', 'Common').
card_artist('golgari brownscale'/'RAV', 'Carl Critchlow').
card_number('golgari brownscale'/'RAV', '166').
card_multiverse_id('golgari brownscale'/'RAV', '89019').
card_watermark('golgari brownscale'/'RAV', 'Golgari').

card_in_set('golgari germination', 'RAV').
card_original_type('golgari germination'/'RAV', 'Enchantment').
card_original_text('golgari germination'/'RAV', 'Whenever a nontoken creature you control is put into a graveyard from play, put a 1/1 green Saproling creature token into play.').
card_first_print('golgari germination', 'RAV').
card_image_name('golgari germination'/'RAV', 'golgari germination').
card_uid('golgari germination'/'RAV', 'RAV:Golgari Germination:golgari germination').
card_rarity('golgari germination'/'RAV', 'Uncommon').
card_artist('golgari germination'/'RAV', 'Thomas M. Baxa').
card_number('golgari germination'/'RAV', '209').
card_flavor_text('golgari germination'/'RAV', 'The Golgari don\'t bury their dead. They plant them.').
card_multiverse_id('golgari germination'/'RAV', '89069').
card_watermark('golgari germination'/'RAV', 'Golgari').

card_in_set('golgari grave-troll', 'RAV').
card_original_type('golgari grave-troll'/'RAV', 'Creature — Skeleton Troll').
card_original_text('golgari grave-troll'/'RAV', 'Golgari Grave-Troll comes into play with a +1/+1 counter on it for each creature card in your graveyard.\n{1}, Remove a +1/+1 counter from Golgari Grave-Troll: Regenerate Golgari Grave-Troll.\nDredge 6').
card_first_print('golgari grave-troll', 'RAV').
card_image_name('golgari grave-troll'/'RAV', 'golgari grave-troll').
card_uid('golgari grave-troll'/'RAV', 'RAV:Golgari Grave-Troll:golgari grave-troll').
card_rarity('golgari grave-troll'/'RAV', 'Rare').
card_artist('golgari grave-troll'/'RAV', 'Greg Hildebrandt').
card_number('golgari grave-troll'/'RAV', '167').
card_multiverse_id('golgari grave-troll'/'RAV', '88960').
card_watermark('golgari grave-troll'/'RAV', 'Golgari').

card_in_set('golgari guildmage', 'RAV').
card_original_type('golgari guildmage'/'RAV', 'Creature — Elf Shaman').
card_original_text('golgari guildmage'/'RAV', '({B/G} can be paid with either {B} or {G}.)\n{4}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.\n{4}{G}: Put a +1/+1 counter on target creature.').
card_first_print('golgari guildmage', 'RAV').
card_image_name('golgari guildmage'/'RAV', 'golgari guildmage').
card_uid('golgari guildmage'/'RAV', 'RAV:Golgari Guildmage:golgari guildmage').
card_rarity('golgari guildmage'/'RAV', 'Uncommon').
card_artist('golgari guildmage'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('golgari guildmage'/'RAV', '248').
card_multiverse_id('golgari guildmage'/'RAV', '83838').
card_watermark('golgari guildmage'/'RAV', 'Golgari').

card_in_set('golgari rot farm', 'RAV').
card_original_type('golgari rot farm'/'RAV', 'Land').
card_original_text('golgari rot farm'/'RAV', 'Golgari Rot Farm comes into play tapped.\nWhen Golgari Rot Farm comes into play, return a land you control to its owner\'s hand.\n{T}: Add {B}{G} to your mana pool.').
card_first_print('golgari rot farm', 'RAV').
card_image_name('golgari rot farm'/'RAV', 'golgari rot farm').
card_uid('golgari rot farm'/'RAV', 'RAV:Golgari Rot Farm:golgari rot farm').
card_rarity('golgari rot farm'/'RAV', 'Common').
card_artist('golgari rot farm'/'RAV', 'John Avon').
card_number('golgari rot farm'/'RAV', '278').
card_multiverse_id('golgari rot farm'/'RAV', '87939').
card_watermark('golgari rot farm'/'RAV', 'Golgari').

card_in_set('golgari rotwurm', 'RAV').
card_original_type('golgari rotwurm'/'RAV', 'Creature — Zombie Wurm').
card_original_text('golgari rotwurm'/'RAV', '{B}, Sacrifice a creature: Target player loses 1 life.').
card_first_print('golgari rotwurm', 'RAV').
card_image_name('golgari rotwurm'/'RAV', 'golgari rotwurm').
card_uid('golgari rotwurm'/'RAV', 'RAV:Golgari Rotwurm:golgari rotwurm').
card_rarity('golgari rotwurm'/'RAV', 'Common').
card_artist('golgari rotwurm'/'RAV', 'Wayne England').
card_number('golgari rotwurm'/'RAV', '210').
card_flavor_text('golgari rotwurm'/'RAV', 'Like corpse-worms through a carapace, rotwurms slide through the hollowed bones of the undercity.').
card_multiverse_id('golgari rotwurm'/'RAV', '89038').
card_watermark('golgari rotwurm'/'RAV', 'Golgari').

card_in_set('golgari signet', 'RAV').
card_original_type('golgari signet'/'RAV', 'Artifact').
card_original_text('golgari signet'/'RAV', '{1}, {T}: Add {B}{G} to your mana pool.').
card_first_print('golgari signet', 'RAV').
card_image_name('golgari signet'/'RAV', 'golgari signet').
card_uid('golgari signet'/'RAV', 'RAV:Golgari Signet:golgari signet').
card_rarity('golgari signet'/'RAV', 'Common').
card_artist('golgari signet'/'RAV', 'Greg Hildebrandt').
card_number('golgari signet'/'RAV', '262').
card_flavor_text('golgari signet'/'RAV', 'Depending on your point of view, the seal represents a proud guardian of the natural cycle or one who has sold her soul to darkness for eternal life.').
card_multiverse_id('golgari signet'/'RAV', '95536').
card_watermark('golgari signet'/'RAV', 'Golgari').

card_in_set('golgari thug', 'RAV').
card_original_type('golgari thug'/'RAV', 'Creature — Human Warrior').
card_original_text('golgari thug'/'RAV', 'When Golgari Thug is put into a graveyard from play, put target creature card in your graveyard on top of your library.\nDredge 4 (If you would draw a card, instead you may put exactly four cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('golgari thug', 'RAV').
card_image_name('golgari thug'/'RAV', 'golgari thug').
card_uid('golgari thug'/'RAV', 'RAV:Golgari Thug:golgari thug').
card_rarity('golgari thug'/'RAV', 'Uncommon').
card_artist('golgari thug'/'RAV', 'Kev Walker').
card_number('golgari thug'/'RAV', '87').
card_multiverse_id('golgari thug'/'RAV', '88994').
card_watermark('golgari thug'/'RAV', 'Golgari').

card_in_set('goliath spider', 'RAV').
card_original_type('goliath spider'/'RAV', 'Creature — Spider').
card_original_text('goliath spider'/'RAV', 'Goliath Spider can block as though it had flying.').
card_first_print('goliath spider', 'RAV').
card_image_name('goliath spider'/'RAV', 'goliath spider').
card_uid('goliath spider'/'RAV', 'RAV:Goliath Spider:goliath spider').
card_rarity('goliath spider'/'RAV', 'Uncommon').
card_artist('goliath spider'/'RAV', 'Alan Pollack').
card_number('goliath spider'/'RAV', '168').
card_flavor_text('goliath spider'/'RAV', 'The glittering strands of its complex web catch everything from a buzzing gnat to a passing bloatdrake.').
card_multiverse_id('goliath spider'/'RAV', '88959').

card_in_set('grave-shell scarab', 'RAV').
card_original_type('grave-shell scarab'/'RAV', 'Creature — Insect').
card_original_text('grave-shell scarab'/'RAV', '{1}, Sacrifice Grave-Shell Scarab: Draw a card.\nDredge 1 (If you would draw a card, instead you may put exactly one card from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('grave-shell scarab', 'RAV').
card_image_name('grave-shell scarab'/'RAV', 'grave-shell scarab').
card_uid('grave-shell scarab'/'RAV', 'RAV:Grave-Shell Scarab:grave-shell scarab').
card_rarity('grave-shell scarab'/'RAV', 'Rare').
card_artist('grave-shell scarab'/'RAV', 'Pete Venters').
card_number('grave-shell scarab'/'RAV', '211').
card_multiverse_id('grave-shell scarab'/'RAV', '89073').
card_watermark('grave-shell scarab'/'RAV', 'Golgari').

card_in_set('grayscaled gharial', 'RAV').
card_original_type('grayscaled gharial'/'RAV', 'Creature — Crocodile').
card_original_text('grayscaled gharial'/'RAV', 'Islandwalk').
card_first_print('grayscaled gharial', 'RAV').
card_image_name('grayscaled gharial'/'RAV', 'grayscaled gharial').
card_uid('grayscaled gharial'/'RAV', 'RAV:Grayscaled Gharial:grayscaled gharial').
card_rarity('grayscaled gharial'/'RAV', 'Common').
card_artist('grayscaled gharial'/'RAV', 'Chris Dien').
card_number('grayscaled gharial'/'RAV', '52').
card_flavor_text('grayscaled gharial'/'RAV', 'Each tooth carries enough bacteria to kill a person in a day or two. But then, gharials do have 138 teeth.').
card_multiverse_id('grayscaled gharial'/'RAV', '87914').

card_in_set('greater forgeling', 'RAV').
card_original_type('greater forgeling'/'RAV', 'Creature — Elemental').
card_original_text('greater forgeling'/'RAV', '{1}{R}: Greater Forgeling gets +3/-3 until end of turn.').
card_first_print('greater forgeling', 'RAV').
card_image_name('greater forgeling'/'RAV', 'greater forgeling').
card_uid('greater forgeling'/'RAV', 'RAV:Greater Forgeling:greater forgeling').
card_rarity('greater forgeling'/'RAV', 'Uncommon').
card_artist('greater forgeling'/'RAV', 'Marcelo Vignali').
card_number('greater forgeling'/'RAV', '129').
card_flavor_text('greater forgeling'/'RAV', 'Few have seen the inner workings of Ravnica\'s furnaces, but neighboring districts catch brief glimpses in the horrors that escape the flames.').
card_multiverse_id('greater forgeling'/'RAV', '87934').

card_in_set('greater mossdog', 'RAV').
card_original_type('greater mossdog'/'RAV', 'Creature — Hound').
card_original_text('greater mossdog'/'RAV', 'Dredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('greater mossdog', 'RAV').
card_image_name('greater mossdog'/'RAV', 'greater mossdog').
card_uid('greater mossdog'/'RAV', 'RAV:Greater Mossdog:greater mossdog').
card_rarity('greater mossdog'/'RAV', 'Common').
card_artist('greater mossdog'/'RAV', 'Chippy').
card_number('greater mossdog'/'RAV', '169').
card_flavor_text('greater mossdog'/'RAV', 'Man\'s best fungus.').
card_multiverse_id('greater mossdog'/'RAV', '87899').
card_watermark('greater mossdog'/'RAV', 'Golgari').

card_in_set('grifter\'s blade', 'RAV').
card_original_type('grifter\'s blade'/'RAV', 'Artifact — Equipment').
card_original_text('grifter\'s blade'/'RAV', 'You may play Grifter\'s Blade any time you could play an instant.\nGrifter\'s Blade comes into play equipping a creature of your choice you control.\nEquipped creature gets +1/+1.\nEquip {1}').
card_first_print('grifter\'s blade', 'RAV').
card_image_name('grifter\'s blade'/'RAV', 'grifter\'s blade').
card_uid('grifter\'s blade'/'RAV', 'RAV:Grifter\'s Blade:grifter\'s blade').
card_rarity('grifter\'s blade'/'RAV', 'Uncommon').
card_artist('grifter\'s blade'/'RAV', 'Alan Pollack').
card_number('grifter\'s blade'/'RAV', '263').
card_multiverse_id('grifter\'s blade'/'RAV', '83619').

card_in_set('grozoth', 'RAV').
card_original_type('grozoth'/'RAV', 'Creature — Leviathan').
card_original_text('grozoth'/'RAV', 'Defender (This creature can\'t attack.)\nWhen Grozoth comes into play, you may search your library for any number of cards that have converted mana cost 9, reveal them, and put them into your hand. If you do, shuffle your library.\n{4}: Grozoth loses defender until end of turn.\nTransmute {1}{U}{U}').
card_first_print('grozoth', 'RAV').
card_image_name('grozoth'/'RAV', 'grozoth').
card_uid('grozoth'/'RAV', 'RAV:Grozoth:grozoth').
card_rarity('grozoth'/'RAV', 'Rare').
card_artist('grozoth'/'RAV', 'Cyril Van Der Haegen').
card_number('grozoth'/'RAV', '53').
card_multiverse_id('grozoth'/'RAV', '83832').
card_watermark('grozoth'/'RAV', 'Dimir').

card_in_set('guardian of vitu-ghazi', 'RAV').
card_original_type('guardian of vitu-ghazi'/'RAV', 'Creature — Elemental').
card_original_text('guardian of vitu-ghazi'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nVigilance').
card_first_print('guardian of vitu-ghazi', 'RAV').
card_image_name('guardian of vitu-ghazi'/'RAV', 'guardian of vitu-ghazi').
card_uid('guardian of vitu-ghazi'/'RAV', 'RAV:Guardian of Vitu-Ghazi:guardian of vitu-ghazi').
card_rarity('guardian of vitu-ghazi'/'RAV', 'Common').
card_artist('guardian of vitu-ghazi'/'RAV', 'Wayne Reynolds').
card_number('guardian of vitu-ghazi'/'RAV', '212').
card_flavor_text('guardian of vitu-ghazi'/'RAV', 'It stands sentinel to the great City-Tree, home of the Selesnya Conclave.').
card_multiverse_id('guardian of vitu-ghazi'/'RAV', '89030').
card_watermark('guardian of vitu-ghazi'/'RAV', 'Selesnya').

card_in_set('halcyon glaze', 'RAV').
card_original_type('halcyon glaze'/'RAV', 'Enchantment').
card_original_text('halcyon glaze'/'RAV', 'Whenever you play a creature spell, Halcyon Glaze becomes a 4/4 Illusion creature with flying until end of turn. It\'s still an enchantment.').
card_first_print('halcyon glaze', 'RAV').
card_image_name('halcyon glaze'/'RAV', 'halcyon glaze').
card_uid('halcyon glaze'/'RAV', 'RAV:Halcyon Glaze:halcyon glaze').
card_rarity('halcyon glaze'/'RAV', 'Uncommon').
card_artist('halcyon glaze'/'RAV', 'John Avon').
card_number('halcyon glaze'/'RAV', '54').
card_flavor_text('halcyon glaze'/'RAV', '\"Earth to æther, window to wonder, stillness to the sky.\"\n—Glazing incantation').
card_multiverse_id('halcyon glaze'/'RAV', '83764').

card_in_set('hammerfist giant', 'RAV').
card_original_type('hammerfist giant'/'RAV', 'Creature — Giant Warrior').
card_original_text('hammerfist giant'/'RAV', '{T}: Hammerfist Giant deals 4 damage to each creature without flying and each player.').
card_first_print('hammerfist giant', 'RAV').
card_image_name('hammerfist giant'/'RAV', 'hammerfist giant').
card_uid('hammerfist giant'/'RAV', 'RAV:Hammerfist Giant:hammerfist giant').
card_rarity('hammerfist giant'/'RAV', 'Rare').
card_artist('hammerfist giant'/'RAV', 'Carl Critchlow').
card_number('hammerfist giant'/'RAV', '130').
card_flavor_text('hammerfist giant'/'RAV', '\"Oh sure, they buy a lot of ale, but I always find myself needing to rebuild after the giants have paid a visit.\"\n—Belko, owner of Titan\'s Keg tavern').
card_multiverse_id('hammerfist giant'/'RAV', '83712').

card_in_set('helldozer', 'RAV').
card_original_type('helldozer'/'RAV', 'Creature — Zombie Giant').
card_original_text('helldozer'/'RAV', '{B}{B}{B}, {T}: Destroy target land. If that land is nonbasic, untap Helldozer.').
card_first_print('helldozer', 'RAV').
card_image_name('helldozer'/'RAV', 'helldozer').
card_uid('helldozer'/'RAV', 'RAV:Helldozer:helldozer').
card_rarity('helldozer'/'RAV', 'Rare').
card_artist('helldozer'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('helldozer'/'RAV', '88').
card_flavor_text('helldozer'/'RAV', 'Sometimes you go to hell, and sometimes hell comes to you.').
card_multiverse_id('helldozer'/'RAV', '88990').

card_in_set('hex', 'RAV').
card_original_type('hex'/'RAV', 'Sorcery').
card_original_text('hex'/'RAV', 'Destroy six target creatures.').
card_first_print('hex', 'RAV').
card_image_name('hex'/'RAV', 'hex').
card_uid('hex'/'RAV', 'RAV:Hex:hex').
card_rarity('hex'/'RAV', 'Rare').
card_artist('hex'/'RAV', 'Michael Sutfin').
card_number('hex'/'RAV', '89').
card_flavor_text('hex'/'RAV', 'When killing five just isn\'t enough.').
card_multiverse_id('hex'/'RAV', '89059').

card_in_set('hour of reckoning', 'RAV').
card_original_type('hour of reckoning'/'RAV', 'Sorcery').
card_original_text('hour of reckoning'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nDestroy all nontoken creatures.').
card_first_print('hour of reckoning', 'RAV').
card_image_name('hour of reckoning'/'RAV', 'hour of reckoning').
card_uid('hour of reckoning'/'RAV', 'RAV:Hour of Reckoning:hour of reckoning').
card_rarity('hour of reckoning'/'RAV', 'Rare').
card_artist('hour of reckoning'/'RAV', 'Randy Gallegos').
card_number('hour of reckoning'/'RAV', '21').
card_flavor_text('hour of reckoning'/'RAV', '\"Ravnica, like a hedge, must be pruned, leaving only leaves of verdant uniformity.\"\n—Niszka, Selesnya evangel').
card_multiverse_id('hour of reckoning'/'RAV', '88985').
card_watermark('hour of reckoning'/'RAV', 'Selesnya').

card_in_set('hunted dragon', 'RAV').
card_original_type('hunted dragon'/'RAV', 'Creature — Dragon').
card_original_text('hunted dragon'/'RAV', 'Flying, haste\nWhen Hunted Dragon comes into play, put three 2/2 white Knight creature tokens with first strike into play under target opponent\'s control.').
card_first_print('hunted dragon', 'RAV').
card_image_name('hunted dragon'/'RAV', 'hunted dragon').
card_uid('hunted dragon'/'RAV', 'RAV:Hunted Dragon:hunted dragon').
card_rarity('hunted dragon'/'RAV', 'Rare').
card_artist('hunted dragon'/'RAV', 'Mark Zug').
card_number('hunted dragon'/'RAV', '131').
card_flavor_text('hunted dragon'/'RAV', 'The knights see a mighty quarry. The dragon sees breakfast, lunch, and dinner.').
card_multiverse_id('hunted dragon'/'RAV', '89086').

card_in_set('hunted horror', 'RAV').
card_original_type('hunted horror'/'RAV', 'Creature — Horror').
card_original_text('hunted horror'/'RAV', 'Trample\nWhen Hunted Horror comes into play, put two 3/3 green Centaur creature tokens with protection from black into play under target opponent\'s control.').
card_first_print('hunted horror', 'RAV').
card_image_name('hunted horror'/'RAV', 'hunted horror').
card_uid('hunted horror'/'RAV', 'RAV:Hunted Horror:hunted horror').
card_rarity('hunted horror'/'RAV', 'Rare').
card_artist('hunted horror'/'RAV', 'Paolo Parente').
card_number('hunted horror'/'RAV', '90').
card_multiverse_id('hunted horror'/'RAV', '89005').

card_in_set('hunted lammasu', 'RAV').
card_original_type('hunted lammasu'/'RAV', 'Creature — Lammasu').
card_original_text('hunted lammasu'/'RAV', 'Flying\nWhen Hunted Lammasu comes into play, put a 4/4 black Horror creature token into play under target opponent\'s control.').
card_first_print('hunted lammasu', 'RAV').
card_image_name('hunted lammasu'/'RAV', 'hunted lammasu').
card_uid('hunted lammasu'/'RAV', 'RAV:Hunted Lammasu:hunted lammasu').
card_rarity('hunted lammasu'/'RAV', 'Rare').
card_artist('hunted lammasu'/'RAV', 'Mark Zug').
card_number('hunted lammasu'/'RAV', '22').
card_flavor_text('hunted lammasu'/'RAV', 'The lammasu ruled the velds before the city grew. Now they roam Ravnica\'s skies, but their ancient enemies have not forgotten them.').
card_multiverse_id('hunted lammasu'/'RAV', '89051').

card_in_set('hunted phantasm', 'RAV').
card_original_type('hunted phantasm'/'RAV', 'Creature — Spirit').
card_original_text('hunted phantasm'/'RAV', 'Hunted Phantasm is unblockable.\nWhen Hunted Phantasm comes into play, put five 1/1 red Goblin creature tokens into play under target opponent\'s control.').
card_first_print('hunted phantasm', 'RAV').
card_image_name('hunted phantasm'/'RAV', 'hunted phantasm').
card_uid('hunted phantasm'/'RAV', 'RAV:Hunted Phantasm:hunted phantasm').
card_rarity('hunted phantasm'/'RAV', 'Rare').
card_artist('hunted phantasm'/'RAV', 'Justin Sweet').
card_number('hunted phantasm'/'RAV', '55').
card_multiverse_id('hunted phantasm'/'RAV', '83573').

card_in_set('hunted troll', 'RAV').
card_original_type('hunted troll'/'RAV', 'Creature — Troll Warrior').
card_original_text('hunted troll'/'RAV', 'When Hunted Troll comes into play, put four 1/1 blue Faerie creature tokens with flying into play under target opponent\'s control.\n{G}: Regenerate Hunted Troll.').
card_first_print('hunted troll', 'RAV').
card_image_name('hunted troll'/'RAV', 'hunted troll').
card_uid('hunted troll'/'RAV', 'RAV:Hunted Troll:hunted troll').
card_rarity('hunted troll'/'RAV', 'Rare').
card_artist('hunted troll'/'RAV', 'Greg Staples').
card_number('hunted troll'/'RAV', '170').
card_multiverse_id('hunted troll'/'RAV', '89079').

card_in_set('incite hysteria', 'RAV').
card_original_type('incite hysteria'/'RAV', 'Sorcery').
card_original_text('incite hysteria'/'RAV', 'Radiance Creatures that share a color with target creature can\'t block this turn.').
card_first_print('incite hysteria', 'RAV').
card_image_name('incite hysteria'/'RAV', 'incite hysteria').
card_uid('incite hysteria'/'RAV', 'RAV:Incite Hysteria:incite hysteria').
card_rarity('incite hysteria'/'RAV', 'Common').
card_artist('incite hysteria'/'RAV', 'Paolo Parente').
card_number('incite hysteria'/'RAV', '132').
card_flavor_text('incite hysteria'/'RAV', '\"The Boros say they want to bring order to Ravnica. Funny then, how well they use chaos.\"\n—Trigori, Azorius senator').
card_multiverse_id('incite hysteria'/'RAV', '87993').
card_watermark('incite hysteria'/'RAV', 'Boros').

card_in_set('indentured oaf', 'RAV').
card_original_type('indentured oaf'/'RAV', 'Creature — Ogre Warrior').
card_original_text('indentured oaf'/'RAV', 'Prevent all damage that Indentured Oaf would deal to red creatures.').
card_first_print('indentured oaf', 'RAV').
card_image_name('indentured oaf'/'RAV', 'indentured oaf').
card_uid('indentured oaf'/'RAV', 'RAV:Indentured Oaf:indentured oaf').
card_rarity('indentured oaf'/'RAV', 'Uncommon').
card_artist('indentured oaf'/'RAV', 'Glenn Fabry').
card_number('indentured oaf'/'RAV', '133').
card_flavor_text('indentured oaf'/'RAV', 'All it knows is the difference between friend and food.').
card_multiverse_id('indentured oaf'/'RAV', '87897').

card_in_set('induce paranoia', 'RAV').
card_original_type('induce paranoia'/'RAV', 'Instant').
card_original_text('induce paranoia'/'RAV', 'Counter target spell. If {B} was spent to play Induce Paranoia, that spell\'s controller puts the top X cards of his or her library into his or her graveyard, where X is the spell\'s converted mana cost.').
card_first_print('induce paranoia', 'RAV').
card_image_name('induce paranoia'/'RAV', 'induce paranoia').
card_uid('induce paranoia'/'RAV', 'RAV:Induce Paranoia:induce paranoia').
card_rarity('induce paranoia'/'RAV', 'Common').
card_artist('induce paranoia'/'RAV', 'Jim Murray').
card_number('induce paranoia'/'RAV', '56').
card_multiverse_id('induce paranoia'/'RAV', '87924').
card_watermark('induce paranoia'/'RAV', 'Dimir').

card_in_set('infectious host', 'RAV').
card_original_type('infectious host'/'RAV', 'Creature — Zombie').
card_original_text('infectious host'/'RAV', 'When Infectious Host is put into a graveyard from play, target player loses 2 life.').
card_first_print('infectious host', 'RAV').
card_image_name('infectious host'/'RAV', 'infectious host').
card_uid('infectious host'/'RAV', 'RAV:Infectious Host:infectious host').
card_rarity('infectious host'/'RAV', 'Common').
card_artist('infectious host'/'RAV', 'rk post').
card_number('infectious host'/'RAV', '91').
card_flavor_text('infectious host'/'RAV', '\"Lost man, dead man, knocking on the door.\nCankerman, soreman, knock no more.\"\n—Ravnican children\'s rhyme').
card_multiverse_id('infectious host'/'RAV', '87966').

card_in_set('instill furor', 'RAV').
card_original_type('instill furor'/'RAV', 'Enchantment — Aura').
card_original_text('instill furor'/'RAV', 'Enchant creature\nEnchanted creature has \"At the end of your turn, sacrifice this creature unless it attacked this turn.\"').
card_first_print('instill furor', 'RAV').
card_image_name('instill furor'/'RAV', 'instill furor').
card_uid('instill furor'/'RAV', 'RAV:Instill Furor:instill furor').
card_rarity('instill furor'/'RAV', 'Uncommon').
card_artist('instill furor'/'RAV', 'Jim Nelson').
card_number('instill furor'/'RAV', '134').
card_flavor_text('instill furor'/'RAV', '\"The Rakdos know little of technology, but they definitely know how to push buttons.\"\n—Trivaz, Izzet mage').
card_multiverse_id('instill furor'/'RAV', '88997').

card_in_set('island', 'RAV').
card_original_type('island'/'RAV', 'Basic Land — Island').
card_original_text('island'/'RAV', 'U').
card_image_name('island'/'RAV', 'island1').
card_uid('island'/'RAV', 'RAV:Island:island1').
card_rarity('island'/'RAV', 'Basic Land').
card_artist('island'/'RAV', 'Stephan Martiniere').
card_number('island'/'RAV', '291').
card_multiverse_id('island'/'RAV', '95103').

card_in_set('island', 'RAV').
card_original_type('island'/'RAV', 'Basic Land — Island').
card_original_text('island'/'RAV', 'U').
card_image_name('island'/'RAV', 'island2').
card_uid('island'/'RAV', 'RAV:Island:island2').
card_rarity('island'/'RAV', 'Basic Land').
card_artist('island'/'RAV', 'Christopher Moeller').
card_number('island'/'RAV', '292').
card_multiverse_id('island'/'RAV', '95107').

card_in_set('island', 'RAV').
card_original_type('island'/'RAV', 'Basic Land — Island').
card_original_text('island'/'RAV', 'U').
card_image_name('island'/'RAV', 'island3').
card_uid('island'/'RAV', 'RAV:Island:island3').
card_rarity('island'/'RAV', 'Basic Land').
card_artist('island'/'RAV', 'Anthony S. Waters').
card_number('island'/'RAV', '293').
card_multiverse_id('island'/'RAV', '95100').

card_in_set('island', 'RAV').
card_original_type('island'/'RAV', 'Basic Land — Island').
card_original_text('island'/'RAV', 'U').
card_image_name('island'/'RAV', 'island4').
card_uid('island'/'RAV', 'RAV:Island:island4').
card_rarity('island'/'RAV', 'Basic Land').
card_artist('island'/'RAV', 'Richard Wright').
card_number('island'/'RAV', '294').
card_multiverse_id('island'/'RAV', '95113').

card_in_set('ivy dancer', 'RAV').
card_original_type('ivy dancer'/'RAV', 'Creature — Dryad Shaman').
card_original_text('ivy dancer'/'RAV', '{T}: Target creature gains forestwalk until end of turn.').
card_first_print('ivy dancer', 'RAV').
card_image_name('ivy dancer'/'RAV', 'ivy dancer').
card_uid('ivy dancer'/'RAV', 'RAV:Ivy Dancer:ivy dancer').
card_rarity('ivy dancer'/'RAV', 'Uncommon').
card_artist('ivy dancer'/'RAV', 'Terese Nielsen').
card_number('ivy dancer'/'RAV', '171').
card_flavor_text('ivy dancer'/'RAV', 'Her ivy locks are a remnant of the wooded wilds now lost to Ravnica forever.').
card_multiverse_id('ivy dancer'/'RAV', '87916').

card_in_set('junktroller', 'RAV').
card_original_type('junktroller'/'RAV', 'Artifact Creature — Golem').
card_original_text('junktroller'/'RAV', 'Defender (This creature can\'t attack.)\n{T}: Put target card in a graveyard on the bottom of its owner\'s library.').
card_first_print('junktroller', 'RAV').
card_image_name('junktroller'/'RAV', 'junktroller').
card_uid('junktroller'/'RAV', 'RAV:Junktroller:junktroller').
card_rarity('junktroller'/'RAV', 'Uncommon').
card_artist('junktroller'/'RAV', 'Chippy').
card_number('junktroller'/'RAV', '264').
card_flavor_text('junktroller'/'RAV', 'One man\'s trash is another man\'s troller.').
card_multiverse_id('junktroller'/'RAV', '88977').

card_in_set('keening banshee', 'RAV').
card_original_type('keening banshee'/'RAV', 'Creature — Spirit').
card_original_text('keening banshee'/'RAV', 'Flying\nWhen Keening Banshee comes into play, target creature gets -2/-2 until end of turn.').
card_first_print('keening banshee', 'RAV').
card_image_name('keening banshee'/'RAV', 'keening banshee').
card_uid('keening banshee'/'RAV', 'RAV:Keening Banshee:keening banshee').
card_rarity('keening banshee'/'RAV', 'Uncommon').
card_artist('keening banshee'/'RAV', 'Robert Bliss').
card_number('keening banshee'/'RAV', '92').
card_flavor_text('keening banshee'/'RAV', 'Her cold wail echoes in the alleys and under-eaves, finding the unfortunate and turning their blood to ice.').
card_multiverse_id('keening banshee'/'RAV', '83599').

card_in_set('last gasp', 'RAV').
card_original_type('last gasp'/'RAV', 'Instant').
card_original_text('last gasp'/'RAV', 'Target creature gets -3/-3 until end of turn.').
card_first_print('last gasp', 'RAV').
card_image_name('last gasp'/'RAV', 'last gasp').
card_uid('last gasp'/'RAV', 'RAV:Last Gasp:last gasp').
card_rarity('last gasp'/'RAV', 'Common').
card_artist('last gasp'/'RAV', 'Thomas M. Baxa').
card_number('last gasp'/'RAV', '93').
card_flavor_text('last gasp'/'RAV', '\"Allow me a moment to catch your breath.\"').
card_multiverse_id('last gasp'/'RAV', '89056').

card_in_set('leashling', 'RAV').
card_original_type('leashling'/'RAV', 'Artifact Creature — Golem').
card_original_text('leashling'/'RAV', 'Put a card in your hand on top of your library: Return Leashling to its owner\'s hand.').
card_first_print('leashling', 'RAV').
card_image_name('leashling'/'RAV', 'leashling').
card_uid('leashling'/'RAV', 'RAV:Leashling:leashling').
card_rarity('leashling'/'RAV', 'Uncommon').
card_artist('leashling'/'RAV', 'Carl Critchlow').
card_number('leashling'/'RAV', '265').
card_flavor_text('leashling'/'RAV', 'Constructed of leather and irony.').
card_multiverse_id('leashling'/'RAV', '89008').

card_in_set('leave no trace', 'RAV').
card_original_type('leave no trace'/'RAV', 'Instant').
card_original_text('leave no trace'/'RAV', 'Radiance Destroy target enchantment and each other enchantment that shares a color with it.').
card_first_print('leave no trace', 'RAV').
card_image_name('leave no trace'/'RAV', 'leave no trace').
card_uid('leave no trace'/'RAV', 'RAV:Leave No Trace:leave no trace').
card_rarity('leave no trace'/'RAV', 'Common').
card_artist('leave no trace'/'RAV', 'Pat Lee').
card_number('leave no trace'/'RAV', '23').
card_flavor_text('leave no trace'/'RAV', 'The magic of the Boros patrols the streets even when their soldiers do not.').
card_multiverse_id('leave no trace'/'RAV', '87954').
card_watermark('leave no trace'/'RAV', 'Boros').

card_in_set('life from the loam', 'RAV').
card_original_type('life from the loam'/'RAV', 'Sorcery').
card_original_text('life from the loam'/'RAV', 'Return up to three target land cards from your graveyard to your hand.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('life from the loam', 'RAV').
card_image_name('life from the loam'/'RAV', 'life from the loam').
card_uid('life from the loam'/'RAV', 'RAV:Life from the Loam:life from the loam').
card_rarity('life from the loam'/'RAV', 'Rare').
card_artist('life from the loam'/'RAV', 'Terese Nielsen').
card_number('life from the loam'/'RAV', '172').
card_multiverse_id('life from the loam'/'RAV', '89001').
card_watermark('life from the loam'/'RAV', 'Golgari').

card_in_set('light of sanction', 'RAV').
card_original_type('light of sanction'/'RAV', 'Enchantment').
card_original_text('light of sanction'/'RAV', 'Prevent all damage that would be dealt to creatures you control by sources you control.').
card_first_print('light of sanction', 'RAV').
card_image_name('light of sanction'/'RAV', 'light of sanction').
card_uid('light of sanction'/'RAV', 'RAV:Light of Sanction:light of sanction').
card_rarity('light of sanction'/'RAV', 'Rare').
card_artist('light of sanction'/'RAV', 'Michael Phillippi').
card_number('light of sanction'/'RAV', '24').
card_flavor_text('light of sanction'/'RAV', 'The Legion looks after its own.').
card_multiverse_id('light of sanction'/'RAV', '88952').

card_in_set('lightning helix', 'RAV').
card_original_type('lightning helix'/'RAV', 'Instant').
card_original_text('lightning helix'/'RAV', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('lightning helix'/'RAV', 'lightning helix').
card_uid('lightning helix'/'RAV', 'RAV:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'RAV', 'Uncommon').
card_artist('lightning helix'/'RAV', 'Kev Walker').
card_number('lightning helix'/'RAV', '213').
card_flavor_text('lightning helix'/'RAV', 'Though less well-known than its army of soldiers, the Boros Legion\'s mage-priests are as respected by the innocent as they are hated by the ghosts of the guilty.').
card_multiverse_id('lightning helix'/'RAV', '87908').
card_watermark('lightning helix'/'RAV', 'Boros').

card_in_set('lore broker', 'RAV').
card_original_type('lore broker'/'RAV', 'Creature — Human Rogue').
card_original_text('lore broker'/'RAV', '{T}: Each player draws a card, then discards a card.').
card_first_print('lore broker', 'RAV').
card_image_name('lore broker'/'RAV', 'lore broker').
card_uid('lore broker'/'RAV', 'RAV:Lore Broker:lore broker').
card_rarity('lore broker'/'RAV', 'Uncommon').
card_artist('lore broker'/'RAV', 'Alan Pollack').
card_number('lore broker'/'RAV', '57').
card_flavor_text('lore broker'/'RAV', 'Lies are sold as often as truths—and used just as effectively.').
card_multiverse_id('lore broker'/'RAV', '87941').

card_in_set('loxodon gatekeeper', 'RAV').
card_original_type('loxodon gatekeeper'/'RAV', 'Creature — Elephant Soldier').
card_original_text('loxodon gatekeeper'/'RAV', 'Artifacts, creatures, and lands your opponents control come into play tapped.').
card_first_print('loxodon gatekeeper', 'RAV').
card_image_name('loxodon gatekeeper'/'RAV', 'loxodon gatekeeper').
card_uid('loxodon gatekeeper'/'RAV', 'RAV:Loxodon Gatekeeper:loxodon gatekeeper').
card_rarity('loxodon gatekeeper'/'RAV', 'Rare').
card_artist('loxodon gatekeeper'/'RAV', 'Carl Critchlow').
card_number('loxodon gatekeeper'/'RAV', '25').
card_flavor_text('loxodon gatekeeper'/'RAV', 'The gatekeepers are so fastidious that even the winds must wait to pass.').
card_multiverse_id('loxodon gatekeeper'/'RAV', '83651').

card_in_set('loxodon hierarch', 'RAV').
card_original_type('loxodon hierarch'/'RAV', 'Creature — Elephant Cleric').
card_original_text('loxodon hierarch'/'RAV', 'When Loxodon Hierarch comes into play, you gain 4 life.\n{G}{W}, Sacrifice Loxodon Hierarch: Regenerate each creature you control.').
card_first_print('loxodon hierarch', 'RAV').
card_image_name('loxodon hierarch'/'RAV', 'loxodon hierarch').
card_uid('loxodon hierarch'/'RAV', 'RAV:Loxodon Hierarch:loxodon hierarch').
card_rarity('loxodon hierarch'/'RAV', 'Rare').
card_artist('loxodon hierarch'/'RAV', 'Kev Walker').
card_number('loxodon hierarch'/'RAV', '214').
card_flavor_text('loxodon hierarch'/'RAV', '\"I have lived long, and I remember how this city once was. If my death serves to bring back the Ravnica in my memory, then so be it.\"').
card_multiverse_id('loxodon hierarch'/'RAV', '83738').
card_watermark('loxodon hierarch'/'RAV', 'Selesnya').

card_in_set('lurking informant', 'RAV').
card_original_type('lurking informant'/'RAV', 'Creature — Human Rogue').
card_original_text('lurking informant'/'RAV', '({U/B} can be paid with either {U} or {B}.)\n{2}, {T}: Look at the top card of target player\'s library. You may put that card into that player\'s graveyard.').
card_first_print('lurking informant', 'RAV').
card_image_name('lurking informant'/'RAV', 'lurking informant').
card_uid('lurking informant'/'RAV', 'RAV:Lurking Informant:lurking informant').
card_rarity('lurking informant'/'RAV', 'Common').
card_artist('lurking informant'/'RAV', 'Ron Spears').
card_number('lurking informant'/'RAV', '249').
card_flavor_text('lurking informant'/'RAV', 'In the undercity, forgetfulness is often encouraged at the point of a blade.').
card_multiverse_id('lurking informant'/'RAV', '83834').
card_watermark('lurking informant'/'RAV', 'Dimir').

card_in_set('mark of eviction', 'RAV').
card_original_type('mark of eviction'/'RAV', 'Enchantment — Aura').
card_original_text('mark of eviction'/'RAV', 'Enchant creature\nAt the beginning of your upkeep, return enchanted creature and all Auras attached to that creature to their owners\' hands.').
card_first_print('mark of eviction', 'RAV').
card_image_name('mark of eviction'/'RAV', 'mark of eviction').
card_uid('mark of eviction'/'RAV', 'RAV:Mark of Eviction:mark of eviction').
card_rarity('mark of eviction'/'RAV', 'Uncommon').
card_artist('mark of eviction'/'RAV', 'Tsutomu Kawade').
card_number('mark of eviction'/'RAV', '58').
card_flavor_text('mark of eviction'/'RAV', 'Tharashk the Bold suddenly wasn\'t himself. Or anyone else, for that matter.').
card_multiverse_id('mark of eviction'/'RAV', '83921').

card_in_set('master warcraft', 'RAV').
card_original_type('master warcraft'/'RAV', 'Instant').
card_original_text('master warcraft'/'RAV', '({R/W} can be paid with either {R} or {W}.)\nPlay Master Warcraft only before attackers are declared.\nYou choose which creatures attack this turn. You choose how each creature blocks this turn.').
card_first_print('master warcraft', 'RAV').
card_image_name('master warcraft'/'RAV', 'master warcraft').
card_uid('master warcraft'/'RAV', 'RAV:Master Warcraft:master warcraft').
card_rarity('master warcraft'/'RAV', 'Rare').
card_artist('master warcraft'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('master warcraft'/'RAV', '250').
card_multiverse_id('master warcraft'/'RAV', '83572').
card_watermark('master warcraft'/'RAV', 'Boros').

card_in_set('mausoleum turnkey', 'RAV').
card_original_type('mausoleum turnkey'/'RAV', 'Creature — Ogre Rogue').
card_original_text('mausoleum turnkey'/'RAV', 'When Mausoleum Turnkey comes into play, return target creature card of an opponent\'s choice from your graveyard to your hand.').
card_first_print('mausoleum turnkey', 'RAV').
card_image_name('mausoleum turnkey'/'RAV', 'mausoleum turnkey').
card_uid('mausoleum turnkey'/'RAV', 'RAV:Mausoleum Turnkey:mausoleum turnkey').
card_rarity('mausoleum turnkey'/'RAV', 'Uncommon').
card_artist('mausoleum turnkey'/'RAV', 'Darrell Riche').
card_number('mausoleum turnkey'/'RAV', '94').
card_flavor_text('mausoleum turnkey'/'RAV', 'When he reaches for his key ring, you should be running for the exit.').
card_multiverse_id('mausoleum turnkey'/'RAV', '83893').

card_in_set('mindleech mass', 'RAV').
card_original_type('mindleech mass'/'RAV', 'Creature — Horror').
card_original_text('mindleech mass'/'RAV', 'Trample\nWhenever Mindleech Mass deals combat damage to a player, you may look at that player\'s hand. If you do, you may play a nonland card in it without paying that card\'s mana cost.').
card_first_print('mindleech mass', 'RAV').
card_image_name('mindleech mass'/'RAV', 'mindleech mass').
card_uid('mindleech mass'/'RAV', 'RAV:Mindleech Mass:mindleech mass').
card_rarity('mindleech mass'/'RAV', 'Rare').
card_artist('mindleech mass'/'RAV', 'Kev Walker').
card_number('mindleech mass'/'RAV', '215').
card_multiverse_id('mindleech mass'/'RAV', '89054').
card_watermark('mindleech mass'/'RAV', 'Dimir').

card_in_set('mindmoil', 'RAV').
card_original_type('mindmoil'/'RAV', 'Enchantment').
card_original_text('mindmoil'/'RAV', 'Whenever you play a spell, put the cards in your hand on the bottom of your library in any order, then draw that many cards.').
card_first_print('mindmoil', 'RAV').
card_image_name('mindmoil'/'RAV', 'mindmoil').
card_uid('mindmoil'/'RAV', 'RAV:Mindmoil:mindmoil').
card_rarity('mindmoil'/'RAV', 'Rare').
card_artist('mindmoil'/'RAV', 'Alex Horley-Orlandelli').
card_number('mindmoil'/'RAV', '135').
card_flavor_text('mindmoil'/'RAV', '\"My criticism of the Izzet is that their impulse for learning seems too much like impulse and too little like learning.\"\n—Trigori, Azorius senator').
card_multiverse_id('mindmoil'/'RAV', '88982').

card_in_set('mnemonic nexus', 'RAV').
card_original_type('mnemonic nexus'/'RAV', 'Instant').
card_original_text('mnemonic nexus'/'RAV', 'Each player shuffles his or her graveyard into his or her library.').
card_first_print('mnemonic nexus', 'RAV').
card_image_name('mnemonic nexus'/'RAV', 'mnemonic nexus').
card_uid('mnemonic nexus'/'RAV', 'RAV:Mnemonic Nexus:mnemonic nexus').
card_rarity('mnemonic nexus'/'RAV', 'Uncommon').
card_artist('mnemonic nexus'/'RAV', 'Stephen Tappin').
card_number('mnemonic nexus'/'RAV', '59').
card_flavor_text('mnemonic nexus'/'RAV', 'True enlightenment comes not with a new thought, but with understanding of all the old ones.').
card_multiverse_id('mnemonic nexus'/'RAV', '88963').

card_in_set('moldervine cloak', 'RAV').
card_original_type('moldervine cloak'/'RAV', 'Enchantment — Aura').
card_original_text('moldervine cloak'/'RAV', 'Enchant creature\nEnchanted creature gets +3/+3.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('moldervine cloak', 'RAV').
card_image_name('moldervine cloak'/'RAV', 'moldervine cloak').
card_uid('moldervine cloak'/'RAV', 'RAV:Moldervine Cloak:moldervine cloak').
card_rarity('moldervine cloak'/'RAV', 'Uncommon').
card_artist('moldervine cloak'/'RAV', 'Wayne England').
card_number('moldervine cloak'/'RAV', '173').
card_multiverse_id('moldervine cloak'/'RAV', '89009').
card_watermark('moldervine cloak'/'RAV', 'Golgari').

card_in_set('molten sentry', 'RAV').
card_original_type('molten sentry'/'RAV', 'Creature — Elemental').
card_original_text('molten sentry'/'RAV', 'As Molten Sentry comes into play, flip a coin. If the coin comes up heads, Molten Sentry comes into play as a 5/2 creature with haste. If it comes up tails, Molten Sentry comes into play as a 2/5 creature with defender.').
card_first_print('molten sentry', 'RAV').
card_image_name('molten sentry'/'RAV', 'molten sentry').
card_uid('molten sentry'/'RAV', 'RAV:Molten Sentry:molten sentry').
card_rarity('molten sentry'/'RAV', 'Rare').
card_artist('molten sentry'/'RAV', 'Kev Walker').
card_number('molten sentry'/'RAV', '136').
card_flavor_text('molten sentry'/'RAV', 'Some take after their father, Stone, some after their mother, Fire.').
card_multiverse_id('molten sentry'/'RAV', '89080').

card_in_set('moonlight bargain', 'RAV').
card_original_type('moonlight bargain'/'RAV', 'Instant').
card_original_text('moonlight bargain'/'RAV', 'Look at the top five cards of your library. For each card, put that card into your graveyard unless you pay 2 life. Then put the rest into your hand.').
card_first_print('moonlight bargain', 'RAV').
card_image_name('moonlight bargain'/'RAV', 'moonlight bargain').
card_uid('moonlight bargain'/'RAV', 'RAV:Moonlight Bargain:moonlight bargain').
card_rarity('moonlight bargain'/'RAV', 'Rare').
card_artist('moonlight bargain'/'RAV', 'Nick Percival').
card_number('moonlight bargain'/'RAV', '95').
card_flavor_text('moonlight bargain'/'RAV', 'At every fifth full moon, the Moon Market convenes to peddle Ravnica\'s most forbidden wares.').
card_multiverse_id('moonlight bargain'/'RAV', '89088').

card_in_set('moroii', 'RAV').
card_original_type('moroii'/'RAV', 'Creature — Vampire').
card_original_text('moroii'/'RAV', 'Flying\nAt the beginning of your upkeep, you lose 1 life.').
card_first_print('moroii', 'RAV').
card_image_name('moroii'/'RAV', 'moroii').
card_uid('moroii'/'RAV', 'RAV:Moroii:moroii').
card_rarity('moroii'/'RAV', 'Uncommon').
card_artist('moroii'/'RAV', 'Dan Scott').
card_number('moroii'/'RAV', '216').
card_flavor_text('moroii'/'RAV', '\"Touched by moroii\"\n—Undercity slang meaning \"to grow old\"').
card_multiverse_id('moroii'/'RAV', '83631').
card_watermark('moroii'/'RAV', 'Dimir').

card_in_set('mortipede', 'RAV').
card_original_type('mortipede'/'RAV', 'Creature — Insect').
card_original_text('mortipede'/'RAV', '{2}{G}: All creatures able to block Mortipede this turn do so.').
card_first_print('mortipede', 'RAV').
card_image_name('mortipede'/'RAV', 'mortipede').
card_uid('mortipede'/'RAV', 'RAV:Mortipede:mortipede').
card_rarity('mortipede'/'RAV', 'Common').
card_artist('mortipede'/'RAV', 'Dave Dorman').
card_number('mortipede'/'RAV', '96').
card_flavor_text('mortipede'/'RAV', 'Mortipede pheromones choke the streets with sickly sweetness, luring the rash and unwary into its waiting jaws.').
card_multiverse_id('mortipede'/'RAV', '87955').
card_watermark('mortipede'/'RAV', 'Golgari').

card_in_set('mountain', 'RAV').
card_original_type('mountain'/'RAV', 'Basic Land — Mountain').
card_original_text('mountain'/'RAV', 'R').
card_image_name('mountain'/'RAV', 'mountain1').
card_uid('mountain'/'RAV', 'RAV:Mountain:mountain1').
card_rarity('mountain'/'RAV', 'Basic Land').
card_artist('mountain'/'RAV', 'Stephan Martiniere').
card_number('mountain'/'RAV', '299').
card_multiverse_id('mountain'/'RAV', '95096').

card_in_set('mountain', 'RAV').
card_original_type('mountain'/'RAV', 'Basic Land — Mountain').
card_original_text('mountain'/'RAV', 'R').
card_image_name('mountain'/'RAV', 'mountain2').
card_uid('mountain'/'RAV', 'RAV:Mountain:mountain2').
card_rarity('mountain'/'RAV', 'Basic Land').
card_artist('mountain'/'RAV', 'Christopher Moeller').
card_number('mountain'/'RAV', '300').
card_multiverse_id('mountain'/'RAV', '95104').

card_in_set('mountain', 'RAV').
card_original_type('mountain'/'RAV', 'Basic Land — Mountain').
card_original_text('mountain'/'RAV', 'R').
card_image_name('mountain'/'RAV', 'mountain3').
card_uid('mountain'/'RAV', 'RAV:Mountain:mountain3').
card_rarity('mountain'/'RAV', 'Basic Land').
card_artist('mountain'/'RAV', 'Anthony S. Waters').
card_number('mountain'/'RAV', '301').
card_multiverse_id('mountain'/'RAV', '95102').

card_in_set('mountain', 'RAV').
card_original_type('mountain'/'RAV', 'Basic Land — Mountain').
card_original_text('mountain'/'RAV', 'R').
card_image_name('mountain'/'RAV', 'mountain4').
card_uid('mountain'/'RAV', 'RAV:Mountain:mountain4').
card_rarity('mountain'/'RAV', 'Basic Land').
card_artist('mountain'/'RAV', 'Richard Wright').
card_number('mountain'/'RAV', '302').
card_multiverse_id('mountain'/'RAV', '95109').

card_in_set('muddle the mixture', 'RAV').
card_original_type('muddle the mixture'/'RAV', 'Instant').
card_original_text('muddle the mixture'/'RAV', 'Counter target instant or sorcery spell.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('muddle the mixture', 'RAV').
card_image_name('muddle the mixture'/'RAV', 'muddle the mixture').
card_uid('muddle the mixture'/'RAV', 'RAV:Muddle the Mixture:muddle the mixture').
card_rarity('muddle the mixture'/'RAV', 'Common').
card_artist('muddle the mixture'/'RAV', 'Luca Zontini').
card_number('muddle the mixture'/'RAV', '60').
card_multiverse_id('muddle the mixture'/'RAV', '88955').
card_watermark('muddle the mixture'/'RAV', 'Dimir').

card_in_set('necromantic thirst', 'RAV').
card_original_type('necromantic thirst'/'RAV', 'Enchantment — Aura').
card_original_text('necromantic thirst'/'RAV', 'Enchant creature\nWhenever enchanted creature deals combat damage to a player, you may return target creature card from your graveyard to your hand.').
card_first_print('necromantic thirst', 'RAV').
card_image_name('necromantic thirst'/'RAV', 'necromantic thirst').
card_uid('necromantic thirst'/'RAV', 'RAV:Necromantic Thirst:necromantic thirst').
card_rarity('necromantic thirst'/'RAV', 'Common').
card_artist('necromantic thirst'/'RAV', 'Brandon Kitkouski').
card_number('necromantic thirst'/'RAV', '97').
card_multiverse_id('necromantic thirst'/'RAV', '87985').

card_in_set('necroplasm', 'RAV').
card_original_type('necroplasm'/'RAV', 'Creature — Ooze').
card_original_text('necroplasm'/'RAV', 'At the beginning of your upkeep, put a +1/+1 counter on Necroplasm.\nAt the end of your turn, destroy each creature with converted mana cost equal to the number of +1/+1 counters on Necroplasm.\nDredge 2').
card_first_print('necroplasm', 'RAV').
card_image_name('necroplasm'/'RAV', 'necroplasm').
card_uid('necroplasm'/'RAV', 'RAV:Necroplasm:necroplasm').
card_rarity('necroplasm'/'RAV', 'Rare').
card_artist('necroplasm'/'RAV', 'rk post').
card_number('necroplasm'/'RAV', '98').
card_multiverse_id('necroplasm'/'RAV', '88956').
card_watermark('necroplasm'/'RAV', 'Golgari').

card_in_set('netherborn phalanx', 'RAV').
card_original_type('netherborn phalanx'/'RAV', 'Creature — Horror').
card_original_text('netherborn phalanx'/'RAV', 'When Netherborn Phalanx comes into play, each opponent loses 1 life for each creature he or she controls.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('netherborn phalanx', 'RAV').
card_image_name('netherborn phalanx'/'RAV', 'netherborn phalanx').
card_uid('netherborn phalanx'/'RAV', 'RAV:Netherborn Phalanx:netherborn phalanx').
card_rarity('netherborn phalanx'/'RAV', 'Uncommon').
card_artist('netherborn phalanx'/'RAV', 'Christopher Rush').
card_number('netherborn phalanx'/'RAV', '99').
card_multiverse_id('netherborn phalanx'/'RAV', '88972').
card_watermark('netherborn phalanx'/'RAV', 'Dimir').

card_in_set('nightguard patrol', 'RAV').
card_original_type('nightguard patrol'/'RAV', 'Creature — Human Soldier').
card_original_text('nightguard patrol'/'RAV', 'First strike, vigilance').
card_first_print('nightguard patrol', 'RAV').
card_image_name('nightguard patrol'/'RAV', 'nightguard patrol').
card_uid('nightguard patrol'/'RAV', 'RAV:Nightguard Patrol:nightguard patrol').
card_rarity('nightguard patrol'/'RAV', 'Common').
card_artist('nightguard patrol'/'RAV', 'Dany Orizio').
card_number('nightguard patrol'/'RAV', '26').
card_flavor_text('nightguard patrol'/'RAV', '\"The Wojek defend the laws. We defend the people.\"').
card_multiverse_id('nightguard patrol'/'RAV', '87975').

card_in_set('nightmare void', 'RAV').
card_original_type('nightmare void'/'RAV', 'Sorcery').
card_original_text('nightmare void'/'RAV', 'Target player reveals his or her hand. Choose a card from it. That player discards that card.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('nightmare void', 'RAV').
card_image_name('nightmare void'/'RAV', 'nightmare void').
card_uid('nightmare void'/'RAV', 'RAV:Nightmare Void:nightmare void').
card_rarity('nightmare void'/'RAV', 'Uncommon').
card_artist('nightmare void'/'RAV', 'Chippy').
card_number('nightmare void'/'RAV', '100').
card_multiverse_id('nightmare void'/'RAV', '89062').
card_watermark('nightmare void'/'RAV', 'Golgari').

card_in_set('nullmage shepherd', 'RAV').
card_original_type('nullmage shepherd'/'RAV', 'Creature — Elf Shaman').
card_original_text('nullmage shepherd'/'RAV', 'Tap four untapped creatures you control: Destroy target artifact or enchantment.').
card_first_print('nullmage shepherd', 'RAV').
card_image_name('nullmage shepherd'/'RAV', 'nullmage shepherd').
card_uid('nullmage shepherd'/'RAV', 'RAV:Nullmage Shepherd:nullmage shepherd').
card_rarity('nullmage shepherd'/'RAV', 'Uncommon').
card_artist('nullmage shepherd'/'RAV', 'Stephen Tappin').
card_number('nullmage shepherd'/'RAV', '174').
card_flavor_text('nullmage shepherd'/'RAV', 'The shepherds work in secret, seeking out abominations against nature and returning them to earth and dust.').
card_multiverse_id('nullmage shepherd'/'RAV', '83913').

card_in_set('nullstone gargoyle', 'RAV').
card_original_type('nullstone gargoyle'/'RAV', 'Artifact Creature — Gargoyle').
card_original_text('nullstone gargoyle'/'RAV', 'Flying\nWhenever the first noncreature spell each turn is played, counter that spell.').
card_first_print('nullstone gargoyle', 'RAV').
card_image_name('nullstone gargoyle'/'RAV', 'nullstone gargoyle').
card_uid('nullstone gargoyle'/'RAV', 'RAV:Nullstone Gargoyle:nullstone gargoyle').
card_rarity('nullstone gargoyle'/'RAV', 'Rare').
card_artist('nullstone gargoyle'/'RAV', 'Glenn Fabry').
card_number('nullstone gargoyle'/'RAV', '266').
card_flavor_text('nullstone gargoyle'/'RAV', 'Nullstone absorbs and dampens most forms of magical energy, as well as light, sound, and heat.').
card_multiverse_id('nullstone gargoyle'/'RAV', '83726').

card_in_set('oathsworn giant', 'RAV').
card_original_type('oathsworn giant'/'RAV', 'Creature — Giant Soldier').
card_original_text('oathsworn giant'/'RAV', 'Vigilance\nOther creatures you control get +0/+2 and have vigilance.').
card_first_print('oathsworn giant', 'RAV').
card_image_name('oathsworn giant'/'RAV', 'oathsworn giant').
card_uid('oathsworn giant'/'RAV', 'RAV:Oathsworn Giant:oathsworn giant').
card_rarity('oathsworn giant'/'RAV', 'Uncommon').
card_artist('oathsworn giant'/'RAV', 'Daren Bader').
card_number('oathsworn giant'/'RAV', '27').
card_flavor_text('oathsworn giant'/'RAV', 'The town stands on his ancestors\' former burial grounds. Even with the graves long gone, he continues his determined vigil.').
card_multiverse_id('oathsworn giant'/'RAV', '89012').

card_in_set('ordruun commando', 'RAV').
card_original_type('ordruun commando'/'RAV', 'Creature — Minotaur Soldier').
card_original_text('ordruun commando'/'RAV', '{W}: Prevent the next 1 damage that would be dealt to Ordruun Commando this turn.').
card_first_print('ordruun commando', 'RAV').
card_image_name('ordruun commando'/'RAV', 'ordruun commando').
card_uid('ordruun commando'/'RAV', 'RAV:Ordruun Commando:ordruun commando').
card_rarity('ordruun commando'/'RAV', 'Common').
card_artist('ordruun commando'/'RAV', 'Stephen Tappin').
card_number('ordruun commando'/'RAV', '137').
card_flavor_text('ordruun commando'/'RAV', 'Thick of muscle, stout of heart, and possessing a burning love of justice and the battlefield, the Ordruun minotaurs are the foundation of the Boros Legion.').
card_multiverse_id('ordruun commando'/'RAV', '87902').
card_watermark('ordruun commando'/'RAV', 'Boros').

card_in_set('overgrown tomb', 'RAV').
card_original_type('overgrown tomb'/'RAV', 'Land — Swamp Forest').
card_original_text('overgrown tomb'/'RAV', '({T}: Add {B} or {G} to your mana pool.)\nAs Overgrown Tomb comes into play, you may pay 2 life. If you don\'t, Overgrown Tomb comes into play tapped instead.').
card_first_print('overgrown tomb', 'RAV').
card_image_name('overgrown tomb'/'RAV', 'overgrown tomb').
card_uid('overgrown tomb'/'RAV', 'RAV:Overgrown Tomb:overgrown tomb').
card_rarity('overgrown tomb'/'RAV', 'Rare').
card_artist('overgrown tomb'/'RAV', 'Rob Alexander').
card_number('overgrown tomb'/'RAV', '279').
card_multiverse_id('overgrown tomb'/'RAV', '89072').
card_watermark('overgrown tomb'/'RAV', 'Golgari').

card_in_set('overwhelm', 'RAV').
card_original_type('overwhelm'/'RAV', 'Sorcery').
card_original_text('overwhelm'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nCreatures you control get +3/+3 until end of turn.').
card_first_print('overwhelm', 'RAV').
card_image_name('overwhelm'/'RAV', 'overwhelm').
card_uid('overwhelm'/'RAV', 'RAV:Overwhelm:overwhelm').
card_rarity('overwhelm'/'RAV', 'Uncommon').
card_artist('overwhelm'/'RAV', 'Wayne Reynolds').
card_number('overwhelm'/'RAV', '175').
card_flavor_text('overwhelm'/'RAV', '\"Let the song of Selesnya be heard above the rhythm of our thundering hordes!\"').
card_multiverse_id('overwhelm'/'RAV', '89044').
card_watermark('overwhelm'/'RAV', 'Selesnya').

card_in_set('pariah\'s shield', 'RAV').
card_original_type('pariah\'s shield'/'RAV', 'Artifact — Equipment').
card_original_text('pariah\'s shield'/'RAV', 'All damage that would be dealt to you is dealt to equipped creature instead.\nEquip {3}').
card_first_print('pariah\'s shield', 'RAV').
card_image_name('pariah\'s shield'/'RAV', 'pariah\'s shield').
card_uid('pariah\'s shield'/'RAV', 'RAV:Pariah\'s Shield:pariah\'s shield').
card_rarity('pariah\'s shield'/'RAV', 'Rare').
card_artist('pariah\'s shield'/'RAV', 'Doug Chaffee').
card_number('pariah\'s shield'/'RAV', '267').
card_flavor_text('pariah\'s shield'/'RAV', 'To bear the shield of the pariah is the highest honor a Boros can receive—and the last.').
card_multiverse_id('pariah\'s shield'/'RAV', '83730').

card_in_set('peel from reality', 'RAV').
card_original_type('peel from reality'/'RAV', 'Instant').
card_original_text('peel from reality'/'RAV', 'Return target creature you control and target creature you don\'t control to their owners\' hands.').
card_first_print('peel from reality', 'RAV').
card_image_name('peel from reality'/'RAV', 'peel from reality').
card_uid('peel from reality'/'RAV', 'RAV:Peel from Reality:peel from reality').
card_rarity('peel from reality'/'RAV', 'Common').
card_artist('peel from reality'/'RAV', 'Puddnhead').
card_number('peel from reality'/'RAV', '61').
card_flavor_text('peel from reality'/'RAV', 'When House Dimir\'s secrets are in danger of exposure, the guild takes drastic measures to cover its tracks.').
card_multiverse_id('peel from reality'/'RAV', '87951').

card_in_set('peregrine mask', 'RAV').
card_original_type('peregrine mask'/'RAV', 'Artifact — Equipment').
card_original_text('peregrine mask'/'RAV', 'Equipped creature has defender, flying, and first strike.\nEquip {2}').
card_first_print('peregrine mask', 'RAV').
card_image_name('peregrine mask'/'RAV', 'peregrine mask').
card_uid('peregrine mask'/'RAV', 'RAV:Peregrine Mask:peregrine mask').
card_rarity('peregrine mask'/'RAV', 'Uncommon').
card_artist('peregrine mask'/'RAV', 'Edward P. Beard, Jr.').
card_number('peregrine mask'/'RAV', '268').
card_flavor_text('peregrine mask'/'RAV', 'The mask confers both the prowess of a falcon and its loyalty.').
card_multiverse_id('peregrine mask'/'RAV', '89090').

card_in_set('perilous forays', 'RAV').
card_original_type('perilous forays'/'RAV', 'Enchantment').
card_original_text('perilous forays'/'RAV', '{1}, Sacrifice a creature: Search your library for a land card with a basic land type and put it into play tapped. Then shuffle your library.').
card_first_print('perilous forays', 'RAV').
card_image_name('perilous forays'/'RAV', 'perilous forays').
card_uid('perilous forays'/'RAV', 'RAV:Perilous Forays:perilous forays').
card_rarity('perilous forays'/'RAV', 'Uncommon').
card_artist('perilous forays'/'RAV', 'Chris Dien').
card_number('perilous forays'/'RAV', '176').
card_flavor_text('perilous forays'/'RAV', '\"This is the place? This map has got to be wrong . . . .\"\n—Svania Trul, wayfinder novice, last words').
card_multiverse_id('perilous forays'/'RAV', '83744').

card_in_set('perplex', 'RAV').
card_original_type('perplex'/'RAV', 'Instant').
card_original_text('perplex'/'RAV', 'Counter target spell unless its controller discards his or her hand.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('perplex', 'RAV').
card_image_name('perplex'/'RAV', 'perplex').
card_uid('perplex'/'RAV', 'RAV:Perplex:perplex').
card_rarity('perplex'/'RAV', 'Common').
card_artist('perplex'/'RAV', 'Tsutomu Kawade').
card_number('perplex'/'RAV', '217').
card_multiverse_id('perplex'/'RAV', '87992').
card_watermark('perplex'/'RAV', 'Dimir').

card_in_set('phytohydra', 'RAV').
card_original_type('phytohydra'/'RAV', 'Creature — Plant Hydra').
card_original_text('phytohydra'/'RAV', 'If damage would be dealt to Phytohydra, put that many +1/+1 counters on it instead.').
card_first_print('phytohydra', 'RAV').
card_image_name('phytohydra'/'RAV', 'phytohydra').
card_uid('phytohydra'/'RAV', 'RAV:Phytohydra:phytohydra').
card_rarity('phytohydra'/'RAV', 'Rare').
card_artist('phytohydra'/'RAV', 'Jim Murray').
card_number('phytohydra'/'RAV', '218').
card_flavor_text('phytohydra'/'RAV', '\"Look at this creature, so like the Conclave it serves. Cut one head and it grows two. Damage it and it flourishes.\"\n—Veszka, Selesnya evangel').
card_multiverse_id('phytohydra'/'RAV', '88969').
card_watermark('phytohydra'/'RAV', 'Selesnya').

card_in_set('plague boiler', 'RAV').
card_original_type('plague boiler'/'RAV', 'Artifact').
card_original_text('plague boiler'/'RAV', 'At the beginning of your upkeep, put a plague counter on Plague Boiler.\n{1}{B}{G}: Put a plague counter on Plague Boiler or remove a plague counter from it.\nWhen Plague Boiler has three or more plague counters on it, sacrifice it. If you do, destroy all nonland permanents.').
card_first_print('plague boiler', 'RAV').
card_image_name('plague boiler'/'RAV', 'plague boiler').
card_uid('plague boiler'/'RAV', 'RAV:Plague Boiler:plague boiler').
card_rarity('plague boiler'/'RAV', 'Rare').
card_artist('plague boiler'/'RAV', 'Mark Tedin').
card_number('plague boiler'/'RAV', '269').
card_multiverse_id('plague boiler'/'RAV', '83556').
card_watermark('plague boiler'/'RAV', 'Golgari').

card_in_set('plains', 'RAV').
card_original_type('plains'/'RAV', 'Basic Land — Plains').
card_original_text('plains'/'RAV', 'W').
card_image_name('plains'/'RAV', 'plains1').
card_uid('plains'/'RAV', 'RAV:Plains:plains1').
card_rarity('plains'/'RAV', 'Basic Land').
card_artist('plains'/'RAV', 'Stephan Martiniere').
card_number('plains'/'RAV', '287').
card_multiverse_id('plains'/'RAV', '95108').

card_in_set('plains', 'RAV').
card_original_type('plains'/'RAV', 'Basic Land — Plains').
card_original_text('plains'/'RAV', 'W').
card_image_name('plains'/'RAV', 'plains2').
card_uid('plains'/'RAV', 'RAV:Plains:plains2').
card_rarity('plains'/'RAV', 'Basic Land').
card_artist('plains'/'RAV', 'Christopher Moeller').
card_number('plains'/'RAV', '288').
card_multiverse_id('plains'/'RAV', '95112').

card_in_set('plains', 'RAV').
card_original_type('plains'/'RAV', 'Basic Land — Plains').
card_original_text('plains'/'RAV', 'W').
card_image_name('plains'/'RAV', 'plains3').
card_uid('plains'/'RAV', 'RAV:Plains:plains3').
card_rarity('plains'/'RAV', 'Basic Land').
card_artist('plains'/'RAV', 'Anthony S. Waters').
card_number('plains'/'RAV', '289').
card_multiverse_id('plains'/'RAV', '95105').

card_in_set('plains', 'RAV').
card_original_type('plains'/'RAV', 'Basic Land — Plains').
card_original_text('plains'/'RAV', 'W').
card_image_name('plains'/'RAV', 'plains4').
card_uid('plains'/'RAV', 'RAV:Plains:plains4').
card_rarity('plains'/'RAV', 'Basic Land').
card_artist('plains'/'RAV', 'Richard Wright').
card_number('plains'/'RAV', '290').
card_multiverse_id('plains'/'RAV', '95115').

card_in_set('pollenbright wings', 'RAV').
card_original_type('pollenbright wings'/'RAV', 'Enchantment — Aura').
card_original_text('pollenbright wings'/'RAV', 'Enchant creature\nEnchanted creature has flying.\nWhenever enchanted creature deals combat damage to a player, put that many 1/1 green Saproling creature tokens into play.').
card_first_print('pollenbright wings', 'RAV').
card_image_name('pollenbright wings'/'RAV', 'pollenbright wings').
card_uid('pollenbright wings'/'RAV', 'RAV:Pollenbright Wings:pollenbright wings').
card_rarity('pollenbright wings'/'RAV', 'Uncommon').
card_artist('pollenbright wings'/'RAV', 'Terese Nielsen').
card_number('pollenbright wings'/'RAV', '219').
card_multiverse_id('pollenbright wings'/'RAV', '89011').
card_watermark('pollenbright wings'/'RAV', 'Selesnya').

card_in_set('primordial sage', 'RAV').
card_original_type('primordial sage'/'RAV', 'Creature — Spirit').
card_original_text('primordial sage'/'RAV', 'Whenever you play a creature spell, you may draw a card.').
card_first_print('primordial sage', 'RAV').
card_image_name('primordial sage'/'RAV', 'primordial sage').
card_uid('primordial sage'/'RAV', 'RAV:Primordial Sage:primordial sage').
card_rarity('primordial sage'/'RAV', 'Rare').
card_artist('primordial sage'/'RAV', 'Justin Sweet').
card_number('primordial sage'/'RAV', '177').
card_flavor_text('primordial sage'/'RAV', 'For each creature that arrives in its audience, the sage imparts another piece of ancient wisdom for all to hear.').
card_multiverse_id('primordial sage'/'RAV', '89083').

card_in_set('privileged position', 'RAV').
card_original_type('privileged position'/'RAV', 'Enchantment').
card_original_text('privileged position'/'RAV', '({G/W} can be paid with either {G} or {W}.)\nOther permanents you control can\'t be the targets of spells or abilities your opponents control.').
card_first_print('privileged position', 'RAV').
card_image_name('privileged position'/'RAV', 'privileged position').
card_uid('privileged position'/'RAV', 'RAV:Privileged Position:privileged position').
card_rarity('privileged position'/'RAV', 'Rare').
card_artist('privileged position'/'RAV', 'Wayne England').
card_number('privileged position'/'RAV', '251').
card_multiverse_id('privileged position'/'RAV', '83720').
card_watermark('privileged position'/'RAV', 'Selesnya').

card_in_set('psychic drain', 'RAV').
card_original_type('psychic drain'/'RAV', 'Sorcery').
card_original_text('psychic drain'/'RAV', 'Target player puts the top X cards of his or her library into his or her graveyard and you gain X life.').
card_first_print('psychic drain', 'RAV').
card_image_name('psychic drain'/'RAV', 'psychic drain').
card_uid('psychic drain'/'RAV', 'RAV:Psychic Drain:psychic drain').
card_rarity('psychic drain'/'RAV', 'Uncommon').
card_artist('psychic drain'/'RAV', 'Nick Percival').
card_number('psychic drain'/'RAV', '220').
card_flavor_text('psychic drain'/'RAV', '\"Gold can be reearned, goods restored. The moroii steal youth, more precious than either, and once gone, it\'s gone forever.\"\n—Berta Suszat, civic healer').
card_multiverse_id('psychic drain'/'RAV', '89114').
card_watermark('psychic drain'/'RAV', 'Dimir').

card_in_set('putrefy', 'RAV').
card_original_type('putrefy'/'RAV', 'Instant').
card_original_text('putrefy'/'RAV', 'Destroy target artifact or creature. It can\'t be regenerated.').
card_image_name('putrefy'/'RAV', 'putrefy').
card_uid('putrefy'/'RAV', 'RAV:Putrefy:putrefy').
card_rarity('putrefy'/'RAV', 'Uncommon').
card_artist('putrefy'/'RAV', 'Jim Nelson').
card_number('putrefy'/'RAV', '221').
card_flavor_text('putrefy'/'RAV', '\"All matter, animate or not, rots when exposed to time. We merely speed up the process.\"\n—Ezoc, Golgari rot farmer').
card_multiverse_id('putrefy'/'RAV', '89063').
card_watermark('putrefy'/'RAV', 'Golgari').

card_in_set('quickchange', 'RAV').
card_original_type('quickchange'/'RAV', 'Instant').
card_original_text('quickchange'/'RAV', 'Target creature\'s color becomes the color or colors of your choice until end of turn.\nDraw a card.').
card_first_print('quickchange', 'RAV').
card_image_name('quickchange'/'RAV', 'quickchange').
card_uid('quickchange'/'RAV', 'RAV:Quickchange:quickchange').
card_rarity('quickchange'/'RAV', 'Common').
card_artist('quickchange'/'RAV', 'Christopher Moeller').
card_number('quickchange'/'RAV', '62').
card_flavor_text('quickchange'/'RAV', 'Most Ravnicans lead lives of desperate survival. Those who thrive are malleable enough to change with the ever-shifting politics of the guilds.').
card_multiverse_id('quickchange'/'RAV', '87979').

card_in_set('rain of embers', 'RAV').
card_original_type('rain of embers'/'RAV', 'Sorcery').
card_original_text('rain of embers'/'RAV', 'Rain of Embers deals 1 damage to each creature and each player.').
card_first_print('rain of embers', 'RAV').
card_image_name('rain of embers'/'RAV', 'rain of embers').
card_uid('rain of embers'/'RAV', 'RAV:Rain of Embers:rain of embers').
card_rarity('rain of embers'/'RAV', 'Common').
card_artist('rain of embers'/'RAV', 'Tsutomu Kawade').
card_number('rain of embers'/'RAV', '138').
card_flavor_text('rain of embers'/'RAV', 'There\'s plenty to talk about in the marketplaces of Ravnica, be it the latest feud between guilds or the terrible weather.').
card_multiverse_id('rain of embers'/'RAV', '87943').

card_in_set('rally the righteous', 'RAV').
card_original_type('rally the righteous'/'RAV', 'Instant').
card_original_text('rally the righteous'/'RAV', 'Radiance Untap target creature and each other creature that shares a color with it. Those creatures get +2/+0 until end of turn.').
card_first_print('rally the righteous', 'RAV').
card_image_name('rally the righteous'/'RAV', 'rally the righteous').
card_uid('rally the righteous'/'RAV', 'RAV:Rally the Righteous:rally the righteous').
card_rarity('rally the righteous'/'RAV', 'Common').
card_artist('rally the righteous'/'RAV', 'Dan Scott').
card_number('rally the righteous'/'RAV', '222').
card_flavor_text('rally the righteous'/'RAV', 'Yuri took up the ragged Boros banner, and his brethren, inspired by the act, followed him back into the fight.').
card_multiverse_id('rally the righteous'/'RAV', '83680').
card_watermark('rally the righteous'/'RAV', 'Boros').

card_in_set('razia\'s purification', 'RAV').
card_original_type('razia\'s purification'/'RAV', 'Sorcery').
card_original_text('razia\'s purification'/'RAV', 'Each player chooses three permanents he or she controls, then sacrifices the rest.').
card_first_print('razia\'s purification', 'RAV').
card_image_name('razia\'s purification'/'RAV', 'razia\'s purification').
card_uid('razia\'s purification'/'RAV', 'RAV:Razia\'s Purification:razia\'s purification').
card_rarity('razia\'s purification'/'RAV', 'Rare').
card_artist('razia\'s purification'/'RAV', 'Shishizaru').
card_number('razia\'s purification'/'RAV', '224').
card_flavor_text('razia\'s purification'/'RAV', 'Only the chosen ones are spared.').
card_multiverse_id('razia\'s purification'/'RAV', '89115').
card_watermark('razia\'s purification'/'RAV', 'Boros').

card_in_set('razia, boros archangel', 'RAV').
card_original_type('razia, boros archangel'/'RAV', 'Legendary Creature — Angel').
card_original_text('razia, boros archangel'/'RAV', 'Flying, vigilance, haste\n{T}: The next 3 damage that would be dealt to target creature you control this turn is dealt to another target creature instead.').
card_first_print('razia, boros archangel', 'RAV').
card_image_name('razia, boros archangel'/'RAV', 'razia, boros archangel').
card_uid('razia, boros archangel'/'RAV', 'RAV:Razia, Boros Archangel:razia, boros archangel').
card_rarity('razia, boros archangel'/'RAV', 'Rare').
card_artist('razia, boros archangel'/'RAV', 'Donato Giancola').
card_number('razia, boros archangel'/'RAV', '223').
card_flavor_text('razia, boros archangel'/'RAV', 'Her sword burns with such brightness that foes avert their eyes and arrows divert their paths.').
card_multiverse_id('razia, boros archangel'/'RAV', '89061').
card_watermark('razia, boros archangel'/'RAV', 'Boros').

card_in_set('recollect', 'RAV').
card_original_type('recollect'/'RAV', 'Sorcery').
card_original_text('recollect'/'RAV', 'Return target card from your graveyard to your hand.').
card_image_name('recollect'/'RAV', 'recollect').
card_uid('recollect'/'RAV', 'RAV:Recollect:recollect').
card_rarity('recollect'/'RAV', 'Uncommon').
card_artist('recollect'/'RAV', 'Pete Venters').
card_number('recollect'/'RAV', '178').
card_flavor_text('recollect'/'RAV', '\"The bones of the past will tell their tales—if you know how to speak their language.\"\n—Savra').
card_multiverse_id('recollect'/'RAV', '88966').

card_in_set('remand', 'RAV').
card_original_type('remand'/'RAV', 'Instant').
card_original_text('remand'/'RAV', 'Counter target spell. If you do, return that spell card to its owner\'s hand.\nDraw a card.').
card_image_name('remand'/'RAV', 'remand').
card_uid('remand'/'RAV', 'RAV:Remand:remand').
card_rarity('remand'/'RAV', 'Uncommon').
card_artist('remand'/'RAV', 'Mark A. Nelson').
card_number('remand'/'RAV', '63').
card_flavor_text('remand'/'RAV', '\"Well, at least all of that arm-waving and arcane babbling you did was impressive.\"').
card_multiverse_id('remand'/'RAV', '87919').

card_in_set('reroute', 'RAV').
card_original_type('reroute'/'RAV', 'Instant').
card_original_text('reroute'/'RAV', 'Change the target of target activated ability with a single target.\nDraw a card.').
card_first_print('reroute', 'RAV').
card_image_name('reroute'/'RAV', 'reroute').
card_uid('reroute'/'RAV', 'RAV:Reroute:reroute').
card_rarity('reroute'/'RAV', 'Uncommon').
card_artist('reroute'/'RAV', 'Christopher Rush').
card_number('reroute'/'RAV', '139').
card_flavor_text('reroute'/'RAV', 'Three hundred years of practice thwarted by an instant of mischief.').
card_multiverse_id('reroute'/'RAV', '89087').

card_in_set('ribbons of night', 'RAV').
card_original_type('ribbons of night'/'RAV', 'Sorcery').
card_original_text('ribbons of night'/'RAV', 'Ribbons of Night deals 4 damage to target creature and you gain 4 life. If {U} was spent to play Ribbons of Night, draw a card.').
card_first_print('ribbons of night', 'RAV').
card_image_name('ribbons of night'/'RAV', 'ribbons of night').
card_uid('ribbons of night'/'RAV', 'RAV:Ribbons of Night:ribbons of night').
card_rarity('ribbons of night'/'RAV', 'Uncommon').
card_artist('ribbons of night'/'RAV', 'Ron Spears').
card_number('ribbons of night'/'RAV', '101').
card_flavor_text('ribbons of night'/'RAV', '\"My favorite meal is angel\'s flesh, slain in agony and iced with black vinegar.\"\n—Aszala of House Dimir').
card_multiverse_id('ribbons of night'/'RAV', '83634').
card_watermark('ribbons of night'/'RAV', 'Dimir').

card_in_set('rolling spoil', 'RAV').
card_original_type('rolling spoil'/'RAV', 'Sorcery').
card_original_text('rolling spoil'/'RAV', 'Destroy target land. If {B} was spent to play Rolling Spoil, all creatures get -1/-1 until end of turn.').
card_first_print('rolling spoil', 'RAV').
card_image_name('rolling spoil'/'RAV', 'rolling spoil').
card_uid('rolling spoil'/'RAV', 'RAV:Rolling Spoil:rolling spoil').
card_rarity('rolling spoil'/'RAV', 'Uncommon').
card_artist('rolling spoil'/'RAV', 'Ron Spencer').
card_number('rolling spoil'/'RAV', '179').
card_flavor_text('rolling spoil'/'RAV', 'The shadow that fell over the grove was silent yet horribly alive, roiling with millions of tiny minions dedicated to the process of rot.').
card_multiverse_id('rolling spoil'/'RAV', '89024').
card_watermark('rolling spoil'/'RAV', 'Golgari').

card_in_set('roofstalker wight', 'RAV').
card_original_type('roofstalker wight'/'RAV', 'Creature — Zombie').
card_original_text('roofstalker wight'/'RAV', '{1}{U}: Roofstalker Wight gains flying until end of turn.').
card_first_print('roofstalker wight', 'RAV').
card_image_name('roofstalker wight'/'RAV', 'roofstalker wight').
card_uid('roofstalker wight'/'RAV', 'RAV:Roofstalker Wight:roofstalker wight').
card_rarity('roofstalker wight'/'RAV', 'Common').
card_artist('roofstalker wight'/'RAV', 'Puddnhead').
card_number('roofstalker wight'/'RAV', '102').
card_flavor_text('roofstalker wight'/'RAV', 'No heartbeat to hear, no greed to bribe, no fear of death.').
card_multiverse_id('roofstalker wight'/'RAV', '87945').
card_watermark('roofstalker wight'/'RAV', 'Dimir').

card_in_set('root-kin ally', 'RAV').
card_original_type('root-kin ally'/'RAV', 'Creature — Elemental Warrior').
card_original_text('root-kin ally'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nTap two untapped creatures you control: Root-Kin Ally gets +2/+2 until end of turn.').
card_first_print('root-kin ally', 'RAV').
card_image_name('root-kin ally'/'RAV', 'root-kin ally').
card_uid('root-kin ally'/'RAV', 'RAV:Root-Kin Ally:root-kin ally').
card_rarity('root-kin ally'/'RAV', 'Uncommon').
card_artist('root-kin ally'/'RAV', 'Arnie Swekel').
card_number('root-kin ally'/'RAV', '180').
card_multiverse_id('root-kin ally'/'RAV', '89026').
card_watermark('root-kin ally'/'RAV', 'Selesnya').

card_in_set('sabertooth alley cat', 'RAV').
card_original_type('sabertooth alley cat'/'RAV', 'Creature — Cat').
card_original_text('sabertooth alley cat'/'RAV', 'Sabertooth Alley Cat attacks each turn if able.\n{1}{R}: Creatures without defender can\'t block Sabertooth Alley Cat this turn.').
card_first_print('sabertooth alley cat', 'RAV').
card_image_name('sabertooth alley cat'/'RAV', 'sabertooth alley cat').
card_uid('sabertooth alley cat'/'RAV', 'RAV:Sabertooth Alley Cat:sabertooth alley cat').
card_rarity('sabertooth alley cat'/'RAV', 'Common').
card_artist('sabertooth alley cat'/'RAV', 'Carl Critchlow').
card_number('sabertooth alley cat'/'RAV', '140').
card_flavor_text('sabertooth alley cat'/'RAV', 'It has eight lives\' worth of hunger to satisfy.').
card_multiverse_id('sabertooth alley cat'/'RAV', '87937').

card_in_set('sacred foundry', 'RAV').
card_original_type('sacred foundry'/'RAV', 'Land — Mountain Plains').
card_original_text('sacred foundry'/'RAV', '({T}: Add {R} or {W} to your mana pool.)\nAs Sacred Foundry comes into play, you may pay 2 life. If you don\'t, Sacred Foundry comes into play tapped instead.').
card_first_print('sacred foundry', 'RAV').
card_image_name('sacred foundry'/'RAV', 'sacred foundry').
card_uid('sacred foundry'/'RAV', 'RAV:Sacred Foundry:sacred foundry').
card_rarity('sacred foundry'/'RAV', 'Rare').
card_artist('sacred foundry'/'RAV', 'Rob Alexander').
card_number('sacred foundry'/'RAV', '280').
card_multiverse_id('sacred foundry'/'RAV', '89066').
card_watermark('sacred foundry'/'RAV', 'Boros').

card_in_set('sadistic augermage', 'RAV').
card_original_type('sadistic augermage'/'RAV', 'Creature — Human Wizard').
card_original_text('sadistic augermage'/'RAV', 'When Sadistic Augermage is put into a graveyard from play, each player puts a card from his or her hand on top of his or her library.').
card_first_print('sadistic augermage', 'RAV').
card_image_name('sadistic augermage'/'RAV', 'sadistic augermage').
card_uid('sadistic augermage'/'RAV', 'RAV:Sadistic Augermage:sadistic augermage').
card_rarity('sadistic augermage'/'RAV', 'Common').
card_artist('sadistic augermage'/'RAV', 'Nick Percival').
card_number('sadistic augermage'/'RAV', '103').
card_flavor_text('sadistic augermage'/'RAV', '\"Don\'t worry. I know the drill.\"').
card_multiverse_id('sadistic augermage'/'RAV', '83745').

card_in_set('sandsower', 'RAV').
card_original_type('sandsower'/'RAV', 'Creature — Spirit').
card_original_text('sandsower'/'RAV', 'Tap three untapped creatures you control: Tap target creature.').
card_first_print('sandsower', 'RAV').
card_image_name('sandsower'/'RAV', 'sandsower').
card_uid('sandsower'/'RAV', 'RAV:Sandsower:sandsower').
card_rarity('sandsower'/'RAV', 'Uncommon').
card_artist('sandsower'/'RAV', 'Kev Walker').
card_number('sandsower'/'RAV', '28').
card_flavor_text('sandsower'/'RAV', 'It drifts through the streets as a breeze of collective sighs, wilting the bustle with dreams and heavy eyelids.').
card_multiverse_id('sandsower'/'RAV', '88983').

card_in_set('savra, queen of the golgari', 'RAV').
card_original_type('savra, queen of the golgari'/'RAV', 'Legendary Creature — Elf Shaman').
card_original_text('savra, queen of the golgari'/'RAV', 'Whenever you sacrifice a black creature, you may pay 2 life. If you do, each other player sacrifices a creature.\nWhenever you sacrifice a green creature, you may gain 2 life.').
card_first_print('savra, queen of the golgari', 'RAV').
card_image_name('savra, queen of the golgari'/'RAV', 'savra, queen of the golgari').
card_uid('savra, queen of the golgari'/'RAV', 'RAV:Savra, Queen of the Golgari:savra, queen of the golgari').
card_rarity('savra, queen of the golgari'/'RAV', 'Rare').
card_artist('savra, queen of the golgari'/'RAV', 'Scott M. Fischer').
card_number('savra, queen of the golgari'/'RAV', '225').
card_flavor_text('savra, queen of the golgari'/'RAV', '\"Nature\'s most raw beauty is the circle: perfect in its continuance, with no break between death and life.\"').
card_multiverse_id('savra, queen of the golgari'/'RAV', '89113').
card_watermark('savra, queen of the golgari'/'RAV', 'Golgari').

card_in_set('scatter the seeds', 'RAV').
card_original_type('scatter the seeds'/'RAV', 'Instant').
card_original_text('scatter the seeds'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nPut three 1/1 green Saproling creature tokens into play.').
card_first_print('scatter the seeds', 'RAV').
card_image_name('scatter the seeds'/'RAV', 'scatter the seeds').
card_uid('scatter the seeds'/'RAV', 'RAV:Scatter the Seeds:scatter the seeds').
card_rarity('scatter the seeds'/'RAV', 'Common').
card_artist('scatter the seeds'/'RAV', 'Rob Alexander').
card_number('scatter the seeds'/'RAV', '181').
card_flavor_text('scatter the seeds'/'RAV', 'From the seeds of faith, great forests grow.').
card_multiverse_id('scatter the seeds'/'RAV', '87931').
card_watermark('scatter the seeds'/'RAV', 'Selesnya').

card_in_set('scion of the wild', 'RAV').
card_original_type('scion of the wild'/'RAV', 'Creature — Avatar').
card_original_text('scion of the wild'/'RAV', 'Scion of the Wild\'s power and toughness are each equal to the number of creatures you control.').
card_first_print('scion of the wild', 'RAV').
card_image_name('scion of the wild'/'RAV', 'scion of the wild').
card_uid('scion of the wild'/'RAV', 'RAV:Scion of the Wild:scion of the wild').
card_rarity('scion of the wild'/'RAV', 'Rare').
card_artist('scion of the wild'/'RAV', 'Kev Walker').
card_number('scion of the wild'/'RAV', '182').
card_flavor_text('scion of the wild'/'RAV', 'It has a hundred thousand extinctions to avenge.').
card_multiverse_id('scion of the wild'/'RAV', '83647').

card_in_set('screeching griffin', 'RAV').
card_original_type('screeching griffin'/'RAV', 'Creature — Griffin').
card_original_text('screeching griffin'/'RAV', 'Flying\n{R}: Target creature can\'t block Screeching Griffin this turn.').
card_first_print('screeching griffin', 'RAV').
card_image_name('screeching griffin'/'RAV', 'screeching griffin').
card_uid('screeching griffin'/'RAV', 'RAV:Screeching Griffin:screeching griffin').
card_rarity('screeching griffin'/'RAV', 'Common').
card_artist('screeching griffin'/'RAV', 'Tim Hildebrandt').
card_number('screeching griffin'/'RAV', '29').
card_flavor_text('screeching griffin'/'RAV', 'They were master fishers, but their seas are now the streets and their catch, the goblins that run pell-mell from the screeching in the sky.').
card_multiverse_id('screeching griffin'/'RAV', '87907').
card_watermark('screeching griffin'/'RAV', 'Boros').

card_in_set('searing meditation', 'RAV').
card_original_type('searing meditation'/'RAV', 'Enchantment').
card_original_text('searing meditation'/'RAV', 'Whenever you gain life, you may pay {2}. If you do, Searing Meditation deals 2 damage to target creature or player.').
card_first_print('searing meditation', 'RAV').
card_image_name('searing meditation'/'RAV', 'searing meditation').
card_uid('searing meditation'/'RAV', 'RAV:Searing Meditation:searing meditation').
card_rarity('searing meditation'/'RAV', 'Rare').
card_artist('searing meditation'/'RAV', 'Dave Dorman').
card_number('searing meditation'/'RAV', '226').
card_flavor_text('searing meditation'/'RAV', '\"When I meditate I see the world as it should be. All that does not fit, I remove.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('searing meditation'/'RAV', '89117').
card_watermark('searing meditation'/'RAV', 'Boros').

card_in_set('seed spark', 'RAV').
card_original_type('seed spark'/'RAV', 'Instant').
card_original_text('seed spark'/'RAV', 'Destroy target artifact or enchantment. If {G} was spent to play Seed Spark, put two 1/1 green Saproling creature tokens into play.').
card_first_print('seed spark', 'RAV').
card_image_name('seed spark'/'RAV', 'seed spark').
card_uid('seed spark'/'RAV', 'RAV:Seed Spark:seed spark').
card_rarity('seed spark'/'RAV', 'Uncommon').
card_artist('seed spark'/'RAV', 'Jeff Miracola').
card_number('seed spark'/'RAV', '30').
card_flavor_text('seed spark'/'RAV', '\"If you ask me, those root-lovers value mindless dogma over progress.\"\n—Trivaz, Izzet mage').
card_multiverse_id('seed spark'/'RAV', '88984').
card_watermark('seed spark'/'RAV', 'Selesnya').

card_in_set('seeds of strength', 'RAV').
card_original_type('seeds of strength'/'RAV', 'Instant').
card_original_text('seeds of strength'/'RAV', 'Target creature gets +1/+1 until end of turn.\nTarget creature gets +1/+1 until end of turn.\nTarget creature gets +1/+1 until end of turn.').
card_first_print('seeds of strength', 'RAV').
card_image_name('seeds of strength'/'RAV', 'seeds of strength').
card_uid('seeds of strength'/'RAV', 'RAV:Seeds of Strength:seeds of strength').
card_rarity('seeds of strength'/'RAV', 'Common').
card_artist('seeds of strength'/'RAV', 'Ralph Horsley').
card_number('seeds of strength'/'RAV', '227').
card_flavor_text('seeds of strength'/'RAV', 'Beneath the beauty of light and seed is the might of Vitu-Ghazi.').
card_multiverse_id('seeds of strength'/'RAV', '89075').
card_watermark('seeds of strength'/'RAV', 'Selesnya').

card_in_set('seismic spike', 'RAV').
card_original_type('seismic spike'/'RAV', 'Sorcery').
card_original_text('seismic spike'/'RAV', 'Destroy target land. Add {R}{R} to your mana pool.').
card_first_print('seismic spike', 'RAV').
card_image_name('seismic spike'/'RAV', 'seismic spike').
card_uid('seismic spike'/'RAV', 'RAV:Seismic Spike:seismic spike').
card_rarity('seismic spike'/'RAV', 'Common').
card_artist('seismic spike'/'RAV', 'John Avon').
card_number('seismic spike'/'RAV', '141').
card_flavor_text('seismic spike'/'RAV', 'Natural disasters in Ravnica are largely nullified by the Izzet. Unnatural disasters, on the other hand, happen all of the time.').
card_multiverse_id('seismic spike'/'RAV', '83829').

card_in_set('selesnya evangel', 'RAV').
card_original_type('selesnya evangel'/'RAV', 'Creature — Elf Shaman').
card_original_text('selesnya evangel'/'RAV', '{1}, {T}, Tap an untapped creature you control: Put a 1/1 green Saproling creature token into play.').
card_first_print('selesnya evangel', 'RAV').
card_image_name('selesnya evangel'/'RAV', 'selesnya evangel').
card_uid('selesnya evangel'/'RAV', 'RAV:Selesnya Evangel:selesnya evangel').
card_rarity('selesnya evangel'/'RAV', 'Common').
card_artist('selesnya evangel'/'RAV', 'Rob Alexander').
card_number('selesnya evangel'/'RAV', '228').
card_flavor_text('selesnya evangel'/'RAV', '\"The clamor of the city drowns all voices. But together we can sing a harmony that will resonate from Ravnica\'s tallest spires to her deepest wells.\"').
card_multiverse_id('selesnya evangel'/'RAV', '87936').
card_watermark('selesnya evangel'/'RAV', 'Selesnya').

card_in_set('selesnya guildmage', 'RAV').
card_original_type('selesnya guildmage'/'RAV', 'Creature — Elf Wizard').
card_original_text('selesnya guildmage'/'RAV', '({G/W} can be paid with either {G} or {W}.)\n{3}{G}: Put a 1/1 green Saproling creature token into play.\n{3}{W}: Creatures you control get +1/+1 until end of turn.').
card_first_print('selesnya guildmage', 'RAV').
card_image_name('selesnya guildmage'/'RAV', 'selesnya guildmage').
card_uid('selesnya guildmage'/'RAV', 'RAV:Selesnya Guildmage:selesnya guildmage').
card_rarity('selesnya guildmage'/'RAV', 'Uncommon').
card_artist('selesnya guildmage'/'RAV', 'Mark Zug').
card_number('selesnya guildmage'/'RAV', '252').
card_multiverse_id('selesnya guildmage'/'RAV', '87988').
card_watermark('selesnya guildmage'/'RAV', 'Selesnya').

card_in_set('selesnya sagittars', 'RAV').
card_original_type('selesnya sagittars'/'RAV', 'Creature — Elf Archer').
card_original_text('selesnya sagittars'/'RAV', 'Selesnya Sagittars can block as though it had flying.\nSelesnya Sagittars can block an additional creature.').
card_first_print('selesnya sagittars', 'RAV').
card_image_name('selesnya sagittars'/'RAV', 'selesnya sagittars').
card_uid('selesnya sagittars'/'RAV', 'RAV:Selesnya Sagittars:selesnya sagittars').
card_rarity('selesnya sagittars'/'RAV', 'Uncommon').
card_artist('selesnya sagittars'/'RAV', 'Edward P. Beard, Jr.').
card_number('selesnya sagittars'/'RAV', '229').
card_flavor_text('selesnya sagittars'/'RAV', '\"What\'s their strike range, you ask? Let\'s put it this way: sagittars aim their bows using maps.\"\n—Otak, Tin Street shopkeep').
card_multiverse_id('selesnya sagittars'/'RAV', '83880').
card_watermark('selesnya sagittars'/'RAV', 'Selesnya').

card_in_set('selesnya sanctuary', 'RAV').
card_original_type('selesnya sanctuary'/'RAV', 'Land').
card_original_text('selesnya sanctuary'/'RAV', 'Selesnya Sanctuary comes into play tapped.\nWhen Selesnya Sanctuary comes into play, return a land you control to its owner\'s hand.\n{T}: Add {G}{W} to your mana pool.').
card_first_print('selesnya sanctuary', 'RAV').
card_image_name('selesnya sanctuary'/'RAV', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'RAV', 'RAV:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'RAV', 'Common').
card_artist('selesnya sanctuary'/'RAV', 'John Avon').
card_number('selesnya sanctuary'/'RAV', '281').
card_multiverse_id('selesnya sanctuary'/'RAV', '87963').
card_watermark('selesnya sanctuary'/'RAV', 'Selesnya').

card_in_set('selesnya signet', 'RAV').
card_original_type('selesnya signet'/'RAV', 'Artifact').
card_original_text('selesnya signet'/'RAV', '{1}, {T}: Add {G}{W} to your mana pool.').
card_first_print('selesnya signet', 'RAV').
card_image_name('selesnya signet'/'RAV', 'selesnya signet').
card_uid('selesnya signet'/'RAV', 'RAV:Selesnya Signet:selesnya signet').
card_rarity('selesnya signet'/'RAV', 'Common').
card_artist('selesnya signet'/'RAV', 'Greg Hildebrandt').
card_number('selesnya signet'/'RAV', '270').
card_flavor_text('selesnya signet'/'RAV', 'The symbol of the Conclave is one of unity, with tree supporting sun and sun feeding tree.').
card_multiverse_id('selesnya signet'/'RAV', '95538').
card_watermark('selesnya signet'/'RAV', 'Selesnya').

card_in_set('sell-sword brute', 'RAV').
card_original_type('sell-sword brute'/'RAV', 'Creature — Human Mercenary').
card_original_text('sell-sword brute'/'RAV', 'When Sell-Sword Brute is put into a graveyard from play, it deals 2 damage to you.').
card_first_print('sell-sword brute', 'RAV').
card_image_name('sell-sword brute'/'RAV', 'sell-sword brute').
card_uid('sell-sword brute'/'RAV', 'RAV:Sell-Sword Brute:sell-sword brute').
card_rarity('sell-sword brute'/'RAV', 'Common').
card_artist('sell-sword brute'/'RAV', 'Jeff Miracola').
card_number('sell-sword brute'/'RAV', '142').
card_flavor_text('sell-sword brute'/'RAV', '\"Killing is easy. Just wrap your hand around the haft, and wrap your enemy around the blade.\"').
card_multiverse_id('sell-sword brute'/'RAV', '87909').

card_in_set('sewerdreg', 'RAV').
card_original_type('sewerdreg'/'RAV', 'Creature — Spirit').
card_original_text('sewerdreg'/'RAV', 'Swampwalk\nSacrifice Sewerdreg: Remove target card in a graveyard from the game.').
card_first_print('sewerdreg', 'RAV').
card_image_name('sewerdreg'/'RAV', 'sewerdreg').
card_uid('sewerdreg'/'RAV', 'RAV:Sewerdreg:sewerdreg').
card_rarity('sewerdreg'/'RAV', 'Common').
card_artist('sewerdreg'/'RAV', 'Joel Thomas').
card_number('sewerdreg'/'RAV', '104').
card_flavor_text('sewerdreg'/'RAV', 'They hardly have form, dripping through pipe and grate with the slip and stench of flowing sewage.').
card_multiverse_id('sewerdreg'/'RAV', '88995').

card_in_set('shadow of doubt', 'RAV').
card_original_type('shadow of doubt'/'RAV', 'Instant').
card_original_text('shadow of doubt'/'RAV', '({U/B} can be paid with either {U} or {B}.)\nPlayers can\'t search libraries this turn.\nDraw a card.').
card_first_print('shadow of doubt', 'RAV').
card_image_name('shadow of doubt'/'RAV', 'shadow of doubt').
card_uid('shadow of doubt'/'RAV', 'RAV:Shadow of Doubt:shadow of doubt').
card_rarity('shadow of doubt'/'RAV', 'Rare').
card_artist('shadow of doubt'/'RAV', 'Greg Staples').
card_number('shadow of doubt'/'RAV', '253').
card_flavor_text('shadow of doubt'/'RAV', '\"Your ignorance is my bliss.\"\n—Szadek').
card_multiverse_id('shadow of doubt'/'RAV', '83827').
card_watermark('shadow of doubt'/'RAV', 'Dimir').

card_in_set('shambling shell', 'RAV').
card_original_type('shambling shell'/'RAV', 'Creature — Plant Zombie').
card_original_text('shambling shell'/'RAV', 'Sacrifice Shambling Shell: Put a +1/+1 counter on target creature.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('shambling shell', 'RAV').
card_image_name('shambling shell'/'RAV', 'shambling shell').
card_uid('shambling shell'/'RAV', 'RAV:Shambling Shell:shambling shell').
card_rarity('shambling shell'/'RAV', 'Common').
card_artist('shambling shell'/'RAV', 'Joel Thomas').
card_number('shambling shell'/'RAV', '230').
card_multiverse_id('shambling shell'/'RAV', '89021').
card_watermark('shambling shell'/'RAV', 'Golgari').

card_in_set('shred memory', 'RAV').
card_original_type('shred memory'/'RAV', 'Instant').
card_original_text('shred memory'/'RAV', 'Remove up to four target cards in a single graveyard from the game.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Play only as a sorcery.)').
card_first_print('shred memory', 'RAV').
card_image_name('shred memory'/'RAV', 'shred memory').
card_uid('shred memory'/'RAV', 'RAV:Shred Memory:shred memory').
card_rarity('shred memory'/'RAV', 'Common').
card_artist('shred memory'/'RAV', 'Hideaki Takamura').
card_number('shred memory'/'RAV', '105').
card_multiverse_id('shred memory'/'RAV', '87962').
card_watermark('shred memory'/'RAV', 'Dimir').

card_in_set('siege wurm', 'RAV').
card_original_type('siege wurm'/'RAV', 'Creature — Wurm').
card_original_text('siege wurm'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nTrample').
card_first_print('siege wurm', 'RAV').
card_image_name('siege wurm'/'RAV', 'siege wurm').
card_uid('siege wurm'/'RAV', 'RAV:Siege Wurm:siege wurm').
card_rarity('siege wurm'/'RAV', 'Common').
card_artist('siege wurm'/'RAV', 'Carl Critchlow').
card_number('siege wurm'/'RAV', '183').
card_flavor_text('siege wurm'/'RAV', 'The Rubblefield was once among the wealthiest districts in Ravnica. All it took was a single wurm to reduce it to ruins.').
card_multiverse_id('siege wurm'/'RAV', '87932').
card_watermark('siege wurm'/'RAV', 'Selesnya').

card_in_set('sins of the past', 'RAV').
card_original_type('sins of the past'/'RAV', 'Sorcery').
card_original_text('sins of the past'/'RAV', 'Until end of turn, you may play target instant or sorcery card in your graveyard without paying its mana cost. If that card would be put into your graveyard this turn, remove it from the game instead. Remove Sins of the Past from the game.').
card_first_print('sins of the past', 'RAV').
card_image_name('sins of the past'/'RAV', 'sins of the past').
card_uid('sins of the past'/'RAV', 'RAV:Sins of the Past:sins of the past').
card_rarity('sins of the past'/'RAV', 'Rare').
card_artist('sins of the past'/'RAV', 'Jeremy Jarvis').
card_number('sins of the past'/'RAV', '106').
card_multiverse_id('sins of the past'/'RAV', '83648').

card_in_set('sisters of stone death', 'RAV').
card_original_type('sisters of stone death'/'RAV', 'Legendary Creature — Gorgon').
card_original_text('sisters of stone death'/'RAV', '{G}: Target creature blocks Sisters of Stone Death this turn if able.\n{B}{G}: Remove from the game target creature blocking or blocked by Sisters of Stone Death.\n{2}{B}: Put a creature card removed from the game with Sisters of Stone Death into play under your control.').
card_first_print('sisters of stone death', 'RAV').
card_image_name('sisters of stone death'/'RAV', 'sisters of stone death').
card_uid('sisters of stone death'/'RAV', 'RAV:Sisters of Stone Death:sisters of stone death').
card_rarity('sisters of stone death'/'RAV', 'Rare').
card_artist('sisters of stone death'/'RAV', 'Donato Giancola').
card_number('sisters of stone death'/'RAV', '231').
card_multiverse_id('sisters of stone death'/'RAV', '83746').
card_watermark('sisters of stone death'/'RAV', 'Golgari').

card_in_set('skyknight legionnaire', 'RAV').
card_original_type('skyknight legionnaire'/'RAV', 'Creature — Human Knight').
card_original_text('skyknight legionnaire'/'RAV', 'Flying, haste').
card_image_name('skyknight legionnaire'/'RAV', 'skyknight legionnaire').
card_uid('skyknight legionnaire'/'RAV', 'RAV:Skyknight Legionnaire:skyknight legionnaire').
card_rarity('skyknight legionnaire'/'RAV', 'Common').
card_artist('skyknight legionnaire'/'RAV', 'Jim Murray').
card_number('skyknight legionnaire'/'RAV', '232').
card_flavor_text('skyknight legionnaire'/'RAV', '\"I do not know which gives me more pride: the legions of soldiers marching in thunderous lockstep, or the cry of the skyknights riding their rocs like jagged lightning overhead.\"\n—Razia').
card_multiverse_id('skyknight legionnaire'/'RAV', '109082').
card_watermark('skyknight legionnaire'/'RAV', 'Boros').

card_in_set('smash', 'RAV').
card_original_type('smash'/'RAV', 'Instant').
card_original_text('smash'/'RAV', 'Destroy target artifact.\nDraw a card.').
card_image_name('smash'/'RAV', 'smash').
card_uid('smash'/'RAV', 'RAV:Smash:smash').
card_rarity('smash'/'RAV', 'Common').
card_artist('smash'/'RAV', 'Paolo Parente').
card_number('smash'/'RAV', '143').
card_flavor_text('smash'/'RAV', 'Ravnica\'s laws protect not its citizens but its industry. Aging equipment is destroyed rather than restored, to bring more money into the factories\' coffers.').
card_multiverse_id('smash'/'RAV', '83646').

card_in_set('snapping drake', 'RAV').
card_original_type('snapping drake'/'RAV', 'Creature — Drake').
card_original_text('snapping drake'/'RAV', 'Flying').
card_image_name('snapping drake'/'RAV', 'snapping drake').
card_uid('snapping drake'/'RAV', 'RAV:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'RAV', 'Common').
card_artist('snapping drake'/'RAV', 'Dave Dorman').
card_number('snapping drake'/'RAV', '64').
card_flavor_text('snapping drake'/'RAV', 'The irritable drakes are solitary nesters. Even brooding females barely contain the urge to attack their own young.').
card_multiverse_id('snapping drake'/'RAV', '87986').

card_in_set('sparkmage apprentice', 'RAV').
card_original_type('sparkmage apprentice'/'RAV', 'Creature — Human Wizard').
card_original_text('sparkmage apprentice'/'RAV', 'When Sparkmage Apprentice comes into play, it deals 1 damage to target creature or player.').
card_first_print('sparkmage apprentice', 'RAV').
card_image_name('sparkmage apprentice'/'RAV', 'sparkmage apprentice').
card_uid('sparkmage apprentice'/'RAV', 'RAV:Sparkmage Apprentice:sparkmage apprentice').
card_rarity('sparkmage apprentice'/'RAV', 'Common').
card_artist('sparkmage apprentice'/'RAV', 'Mark Poole').
card_number('sparkmage apprentice'/'RAV', '144').
card_flavor_text('sparkmage apprentice'/'RAV', 'Pajik was amazed and pleased at his newfound powers. His pet cat Rivin was slightly less enthusiastic.').
card_multiverse_id('sparkmage apprentice'/'RAV', '87967').

card_in_set('spawnbroker', 'RAV').
card_original_type('spawnbroker'/'RAV', 'Creature — Human Wizard').
card_original_text('spawnbroker'/'RAV', 'When Spawnbroker comes into play, you may exchange control of target creature you control and target creature with power less than or equal to that creature\'s power an opponent controls.').
card_first_print('spawnbroker', 'RAV').
card_image_name('spawnbroker'/'RAV', 'spawnbroker').
card_uid('spawnbroker'/'RAV', 'RAV:Spawnbroker:spawnbroker').
card_rarity('spawnbroker'/'RAV', 'Rare').
card_artist('spawnbroker'/'RAV', 'Wayne England').
card_number('spawnbroker'/'RAV', '65').
card_flavor_text('spawnbroker'/'RAV', '\"The trick isn\'t setting up the bad trade. It\'s making each side think it got the better deal.\"').
card_multiverse_id('spawnbroker'/'RAV', '83748').

card_in_set('spectral searchlight', 'RAV').
card_original_type('spectral searchlight'/'RAV', 'Artifact').
card_original_text('spectral searchlight'/'RAV', '{T}: Choose a player. That player adds one mana of any color he or she chooses to his or her mana pool.').
card_first_print('spectral searchlight', 'RAV').
card_image_name('spectral searchlight'/'RAV', 'spectral searchlight').
card_uid('spectral searchlight'/'RAV', 'RAV:Spectral Searchlight:spectral searchlight').
card_rarity('spectral searchlight'/'RAV', 'Uncommon').
card_artist('spectral searchlight'/'RAV', 'Martina Pilcerova').
card_number('spectral searchlight'/'RAV', '271').
card_flavor_text('spectral searchlight'/'RAV', 'The first searchlights were given as gifts, symbols of cooperation, to the emissaries present at the signing of the Guildpact.').
card_multiverse_id('spectral searchlight'/'RAV', '89003').

card_in_set('stasis cell', 'RAV').
card_original_type('stasis cell'/'RAV', 'Enchantment — Aura').
card_original_text('stasis cell'/'RAV', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\n{3}{U}: Attach Stasis Cell to target creature.').
card_first_print('stasis cell', 'RAV').
card_image_name('stasis cell'/'RAV', 'stasis cell').
card_uid('stasis cell'/'RAV', 'RAV:Stasis Cell:stasis cell').
card_rarity('stasis cell'/'RAV', 'Common').
card_artist('stasis cell'/'RAV', 'Mark A. Nelson').
card_number('stasis cell'/'RAV', '66').
card_flavor_text('stasis cell'/'RAV', 'The Simic created the cells to preserve their experiments. The Azorius put the cells to use on the guilty.').
card_multiverse_id('stasis cell'/'RAV', '88944').

card_in_set('stinkweed imp', 'RAV').
card_original_type('stinkweed imp'/'RAV', 'Creature — Imp').
card_original_text('stinkweed imp'/'RAV', 'Flying\nWhenever Stinkweed Imp deals combat damage to a creature, destroy that creature.\nDredge 5 (If you would draw a card, instead you may put exactly five cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_first_print('stinkweed imp', 'RAV').
card_image_name('stinkweed imp'/'RAV', 'stinkweed imp').
card_uid('stinkweed imp'/'RAV', 'RAV:Stinkweed Imp:stinkweed imp').
card_rarity('stinkweed imp'/'RAV', 'Common').
card_artist('stinkweed imp'/'RAV', 'Edward P. Beard, Jr.').
card_number('stinkweed imp'/'RAV', '107').
card_multiverse_id('stinkweed imp'/'RAV', '87984').
card_watermark('stinkweed imp'/'RAV', 'Golgari').

card_in_set('stone-seeder hierophant', 'RAV').
card_original_type('stone-seeder hierophant'/'RAV', 'Creature — Human Druid').
card_original_text('stone-seeder hierophant'/'RAV', 'Whenever a land comes into play under your control, untap Stone-Seeder Hierophant.\n{T}: Untap target land.').
card_first_print('stone-seeder hierophant', 'RAV').
card_image_name('stone-seeder hierophant'/'RAV', 'stone-seeder hierophant').
card_uid('stone-seeder hierophant'/'RAV', 'RAV:Stone-Seeder Hierophant:stone-seeder hierophant').
card_rarity('stone-seeder hierophant'/'RAV', 'Common').
card_artist('stone-seeder hierophant'/'RAV', 'William Simpson').
card_number('stone-seeder hierophant'/'RAV', '184').
card_flavor_text('stone-seeder hierophant'/'RAV', 'Her touch brings to bare stone what water and sun bring to rich soil.').
card_multiverse_id('stone-seeder hierophant'/'RAV', '89041').

card_in_set('stoneshaker shaman', 'RAV').
card_original_type('stoneshaker shaman'/'RAV', 'Creature — Human Shaman').
card_original_text('stoneshaker shaman'/'RAV', 'At the end of each player\'s turn, that player sacrifices an untapped land.').
card_first_print('stoneshaker shaman', 'RAV').
card_image_name('stoneshaker shaman'/'RAV', 'stoneshaker shaman').
card_uid('stoneshaker shaman'/'RAV', 'RAV:Stoneshaker Shaman:stoneshaker shaman').
card_rarity('stoneshaker shaman'/'RAV', 'Uncommon').
card_artist('stoneshaker shaman'/'RAV', 'Jeff Miracola').
card_number('stoneshaker shaman'/'RAV', '145').
card_flavor_text('stoneshaker shaman'/'RAV', 'There is no place in Ravnica for stagnation, no room for lands unfilled and untilled.').
card_multiverse_id('stoneshaker shaman'/'RAV', '89105').

card_in_set('strands of undeath', 'RAV').
card_original_type('strands of undeath'/'RAV', 'Enchantment — Aura').
card_original_text('strands of undeath'/'RAV', 'Enchant creature\nWhen Strands of Undeath comes into play, target player discards two cards.\n{B}: Regenerate enchanted creature.').
card_first_print('strands of undeath', 'RAV').
card_image_name('strands of undeath'/'RAV', 'strands of undeath').
card_uid('strands of undeath'/'RAV', 'RAV:Strands of Undeath:strands of undeath').
card_rarity('strands of undeath'/'RAV', 'Common').
card_artist('strands of undeath'/'RAV', 'Dave Allsop').
card_number('strands of undeath'/'RAV', '108').
card_flavor_text('strands of undeath'/'RAV', '\"Why limit yourself to mortal law when you can outlive those who enforce it?\"\n—Czaric, Orzhov prelate').
card_multiverse_id('strands of undeath'/'RAV', '83819').

card_in_set('sundering vitae', 'RAV').
card_original_type('sundering vitae'/'RAV', 'Instant').
card_original_text('sundering vitae'/'RAV', 'Convoke (Each creature you tap while playing this spell reduces its cost by {1} or by one mana of that creature\'s color.)\nDestroy target artifact or enchantment.').
card_first_print('sundering vitae', 'RAV').
card_image_name('sundering vitae'/'RAV', 'sundering vitae').
card_uid('sundering vitae'/'RAV', 'RAV:Sundering Vitae:sundering vitae').
card_rarity('sundering vitae'/'RAV', 'Common').
card_artist('sundering vitae'/'RAV', 'Shishizaru').
card_number('sundering vitae'/'RAV', '185').
card_flavor_text('sundering vitae'/'RAV', 'Centuries of wind, rain, and roots compressed into an instant of destruction: such is the power of Selesnya.').
card_multiverse_id('sundering vitae'/'RAV', '87927').
card_watermark('sundering vitae'/'RAV', 'Selesnya').

card_in_set('sunforger', 'RAV').
card_original_type('sunforger'/'RAV', 'Artifact — Equipment').
card_original_text('sunforger'/'RAV', 'Equipped creature gets +4/+0.\n{R}{W}, Unattach Sunforger: Search your library for a red or white instant card with converted mana cost 4 or less and play that card without paying its mana cost. Then shuffle your library.\nEquip {3}').
card_first_print('sunforger', 'RAV').
card_image_name('sunforger'/'RAV', 'sunforger').
card_uid('sunforger'/'RAV', 'RAV:Sunforger:sunforger').
card_rarity('sunforger'/'RAV', 'Rare').
card_artist('sunforger'/'RAV', 'Darrell Riche').
card_number('sunforger'/'RAV', '272').
card_multiverse_id('sunforger'/'RAV', '83618').
card_watermark('sunforger'/'RAV', 'Boros').

card_in_set('sunhome enforcer', 'RAV').
card_original_type('sunhome enforcer'/'RAV', 'Creature — Giant Soldier').
card_original_text('sunhome enforcer'/'RAV', 'Whenever Sunhome Enforcer deals combat damage, you gain that much life.\n{1}{R}: Sunhome Enforcer gets +1/+0 until end of turn.').
card_first_print('sunhome enforcer', 'RAV').
card_image_name('sunhome enforcer'/'RAV', 'sunhome enforcer').
card_uid('sunhome enforcer'/'RAV', 'RAV:Sunhome Enforcer:sunhome enforcer').
card_rarity('sunhome enforcer'/'RAV', 'Uncommon').
card_artist('sunhome enforcer'/'RAV', 'Greg Staples').
card_number('sunhome enforcer'/'RAV', '233').
card_flavor_text('sunhome enforcer'/'RAV', '\"Law soothed his savage heart. Where fury once burned, the enduring flame of order now shines.\"\n—Razia').
card_multiverse_id('sunhome enforcer'/'RAV', '89070').
card_watermark('sunhome enforcer'/'RAV', 'Boros').

card_in_set('sunhome, fortress of the legion', 'RAV').
card_original_type('sunhome, fortress of the legion'/'RAV', 'Land').
card_original_text('sunhome, fortress of the legion'/'RAV', '{T}: Add {1} to your mana pool.\n{2}{R}{W}, {T}: Target creature gains double strike until end of turn.').
card_first_print('sunhome, fortress of the legion', 'RAV').
card_image_name('sunhome, fortress of the legion'/'RAV', 'sunhome, fortress of the legion').
card_uid('sunhome, fortress of the legion'/'RAV', 'RAV:Sunhome, Fortress of the Legion:sunhome, fortress of the legion').
card_rarity('sunhome, fortress of the legion'/'RAV', 'Uncommon').
card_artist('sunhome, fortress of the legion'/'RAV', 'Martina Pilcerova').
card_number('sunhome, fortress of the legion'/'RAV', '282').
card_flavor_text('sunhome, fortress of the legion'/'RAV', 'Sunhome—the stalwart shield, the towering sentinel, the seat of justice.').
card_multiverse_id('sunhome, fortress of the legion'/'RAV', '83794').
card_watermark('sunhome, fortress of the legion'/'RAV', 'Boros').

card_in_set('suppression field', 'RAV').
card_original_type('suppression field'/'RAV', 'Enchantment').
card_original_text('suppression field'/'RAV', 'Activated abilities cost {2} more to play unless they\'re mana abilities.').
card_first_print('suppression field', 'RAV').
card_image_name('suppression field'/'RAV', 'suppression field').
card_uid('suppression field'/'RAV', 'RAV:Suppression Field:suppression field').
card_rarity('suppression field'/'RAV', 'Uncommon').
card_artist('suppression field'/'RAV', 'John Avon').
card_number('suppression field'/'RAV', '31').
card_flavor_text('suppression field'/'RAV', 'The most feared of Azorius punishments is to be freed—sent back out into the world, stripped of all magical defenses.').
card_multiverse_id('suppression field'/'RAV', '83617').

card_in_set('surge of zeal', 'RAV').
card_original_type('surge of zeal'/'RAV', 'Instant').
card_original_text('surge of zeal'/'RAV', 'Radiance Target creature and each other creature that shares a color with it gain haste until end of turn.').
card_first_print('surge of zeal', 'RAV').
card_image_name('surge of zeal'/'RAV', 'surge of zeal').
card_uid('surge of zeal'/'RAV', 'RAV:Surge of Zeal:surge of zeal').
card_rarity('surge of zeal'/'RAV', 'Common').
card_artist('surge of zeal'/'RAV', 'Justin Sweet').
card_number('surge of zeal'/'RAV', '146').
card_flavor_text('surge of zeal'/'RAV', '\"If only my poxes were as infectious as their zealotry.\"\n—Savra').
card_multiverse_id('surge of zeal'/'RAV', '87956').
card_watermark('surge of zeal'/'RAV', 'Boros').

card_in_set('surveilling sprite', 'RAV').
card_original_type('surveilling sprite'/'RAV', 'Creature — Faerie Rogue').
card_original_text('surveilling sprite'/'RAV', 'Flying\nWhen Surveilling Sprite is put into a graveyard from play, you may draw a card.').
card_first_print('surveilling sprite', 'RAV').
card_image_name('surveilling sprite'/'RAV', 'surveilling sprite').
card_uid('surveilling sprite'/'RAV', 'RAV:Surveilling Sprite:surveilling sprite').
card_rarity('surveilling sprite'/'RAV', 'Common').
card_artist('surveilling sprite'/'RAV', 'Terese Nielsen').
card_number('surveilling sprite'/'RAV', '67').
card_flavor_text('surveilling sprite'/'RAV', 'Their natural curiosity, combined with a knack for trespassing, makes sprites excellent spies.').
card_multiverse_id('surveilling sprite'/'RAV', '87904').

card_in_set('svogthos, the restless tomb', 'RAV').
card_original_type('svogthos, the restless tomb'/'RAV', 'Land').
card_original_text('svogthos, the restless tomb'/'RAV', '{T}: Add {1} to your mana pool.\n{3}{B}{G}: Until end of turn, Svogthos, the Restless Tomb becomes a black and green Plant Zombie creature with \"This creature\'s power and toughness are each equal to the number of creature cards in your graveyard.\" It\'s still a land.').
card_first_print('svogthos, the restless tomb', 'RAV').
card_image_name('svogthos, the restless tomb'/'RAV', 'svogthos, the restless tomb').
card_uid('svogthos, the restless tomb'/'RAV', 'RAV:Svogthos, the Restless Tomb:svogthos, the restless tomb').
card_rarity('svogthos, the restless tomb'/'RAV', 'Uncommon').
card_artist('svogthos, the restless tomb'/'RAV', 'Martina Pilcerova').
card_number('svogthos, the restless tomb'/'RAV', '283').
card_multiverse_id('svogthos, the restless tomb'/'RAV', '89045').
card_watermark('svogthos, the restless tomb'/'RAV', 'Golgari').

card_in_set('swamp', 'RAV').
card_original_type('swamp'/'RAV', 'Basic Land — Swamp').
card_original_text('swamp'/'RAV', 'B').
card_image_name('swamp'/'RAV', 'swamp1').
card_uid('swamp'/'RAV', 'RAV:Swamp:swamp1').
card_rarity('swamp'/'RAV', 'Basic Land').
card_artist('swamp'/'RAV', 'Stephan Martiniere').
card_number('swamp'/'RAV', '295').
card_multiverse_id('swamp'/'RAV', '95111').

card_in_set('swamp', 'RAV').
card_original_type('swamp'/'RAV', 'Basic Land — Swamp').
card_original_text('swamp'/'RAV', 'B').
card_image_name('swamp'/'RAV', 'swamp2').
card_uid('swamp'/'RAV', 'RAV:Swamp:swamp2').
card_rarity('swamp'/'RAV', 'Basic Land').
card_artist('swamp'/'RAV', 'Christopher Moeller').
card_number('swamp'/'RAV', '296').
card_multiverse_id('swamp'/'RAV', '95110').

card_in_set('swamp', 'RAV').
card_original_type('swamp'/'RAV', 'Basic Land — Swamp').
card_original_text('swamp'/'RAV', 'B').
card_image_name('swamp'/'RAV', 'swamp3').
card_uid('swamp'/'RAV', 'RAV:Swamp:swamp3').
card_rarity('swamp'/'RAV', 'Basic Land').
card_artist('swamp'/'RAV', 'Anthony S. Waters').
card_number('swamp'/'RAV', '297').
card_multiverse_id('swamp'/'RAV', '95101').

card_in_set('swamp', 'RAV').
card_original_type('swamp'/'RAV', 'Basic Land — Swamp').
card_original_text('swamp'/'RAV', 'B').
card_image_name('swamp'/'RAV', 'swamp4').
card_uid('swamp'/'RAV', 'RAV:Swamp:swamp4').
card_rarity('swamp'/'RAV', 'Basic Land').
card_artist('swamp'/'RAV', 'Richard Wright').
card_number('swamp'/'RAV', '298').
card_multiverse_id('swamp'/'RAV', '95114').

card_in_set('szadek, lord of secrets', 'RAV').
card_original_type('szadek, lord of secrets'/'RAV', 'Legendary Creature — Vampire').
card_original_text('szadek, lord of secrets'/'RAV', 'Flying\nIf Szadek, Lord of Secrets would deal combat damage to a player, instead put that many +1/+1 counters on Szadek and that player puts that many cards from the top of his or her library into his or her graveyard.').
card_first_print('szadek, lord of secrets', 'RAV').
card_image_name('szadek, lord of secrets'/'RAV', 'szadek, lord of secrets').
card_uid('szadek, lord of secrets'/'RAV', 'RAV:Szadek, Lord of Secrets:szadek, lord of secrets').
card_rarity('szadek, lord of secrets'/'RAV', 'Rare').
card_artist('szadek, lord of secrets'/'RAV', 'Donato Giancola').
card_number('szadek, lord of secrets'/'RAV', '234').
card_multiverse_id('szadek, lord of secrets'/'RAV', '89092').
card_watermark('szadek, lord of secrets'/'RAV', 'Dimir').

card_in_set('tattered drake', 'RAV').
card_original_type('tattered drake'/'RAV', 'Creature — Zombie Drake').
card_original_text('tattered drake'/'RAV', 'Flying\n{B}: Regenerate Tattered Drake.').
card_first_print('tattered drake', 'RAV').
card_image_name('tattered drake'/'RAV', 'tattered drake').
card_uid('tattered drake'/'RAV', 'RAV:Tattered Drake:tattered drake').
card_rarity('tattered drake'/'RAV', 'Common').
card_artist('tattered drake'/'RAV', 'Glenn Fabry').
card_number('tattered drake'/'RAV', '68').
card_flavor_text('tattered drake'/'RAV', '\"Drakes embody the worst of Ravnica: great potential, twisted by selfishness and greed.\"\n—Razia').
card_multiverse_id('tattered drake'/'RAV', '87930').
card_watermark('tattered drake'/'RAV', 'Dimir').

card_in_set('telling time', 'RAV').
card_original_type('telling time'/'RAV', 'Instant').
card_original_text('telling time'/'RAV', 'Look at the top three cards of your library. Put one of those cards into your hand, one on top of your library, and one on the bottom of your library.').
card_first_print('telling time', 'RAV').
card_image_name('telling time'/'RAV', 'telling time').
card_uid('telling time'/'RAV', 'RAV:Telling Time:telling time').
card_rarity('telling time'/'RAV', 'Uncommon').
card_artist('telling time'/'RAV', 'Scott M. Fischer').
card_number('telling time'/'RAV', '69').
card_flavor_text('telling time'/'RAV', 'Mastery is achieved when \"telling time\" becomes \"telling time what to do.\"').
card_multiverse_id('telling time'/'RAV', '88988').

card_in_set('temple garden', 'RAV').
card_original_type('temple garden'/'RAV', 'Land — Forest Plains').
card_original_text('temple garden'/'RAV', '({T}: Add {G} or {W} to your mana pool.)\nAs Temple Garden comes into play, you may pay 2 life. If you don\'t, Temple Garden comes into play tapped instead.').
card_first_print('temple garden', 'RAV').
card_image_name('temple garden'/'RAV', 'temple garden').
card_uid('temple garden'/'RAV', 'RAV:Temple Garden:temple garden').
card_rarity('temple garden'/'RAV', 'Rare').
card_artist('temple garden'/'RAV', 'Rob Alexander').
card_number('temple garden'/'RAV', '284').
card_multiverse_id('temple garden'/'RAV', '89093').
card_watermark('temple garden'/'RAV', 'Selesnya').

card_in_set('terraformer', 'RAV').
card_original_type('terraformer'/'RAV', 'Creature — Human Wizard').
card_original_text('terraformer'/'RAV', '{1}: Choose a basic land type. The land type of each land you control becomes that type until end of turn.').
card_first_print('terraformer', 'RAV').
card_image_name('terraformer'/'RAV', 'terraformer').
card_uid('terraformer'/'RAV', 'RAV:Terraformer:terraformer').
card_rarity('terraformer'/'RAV', 'Common').
card_artist('terraformer'/'RAV', 'Luca Zontini').
card_number('terraformer'/'RAV', '70').
card_flavor_text('terraformer'/'RAV', '\"This feels a little more like home.\"').
card_multiverse_id('terraformer'/'RAV', '87905').

card_in_set('terrarion', 'RAV').
card_original_type('terrarion'/'RAV', 'Artifact').
card_original_text('terrarion'/'RAV', 'Terrarion comes into play tapped.\n{2}, {T}, Sacrifice Terrarion: Add two mana of any combination of colors to your mana pool.\nWhen Terrarion is put into a graveyard from play, draw a card.').
card_first_print('terrarion', 'RAV').
card_image_name('terrarion'/'RAV', 'terrarion').
card_uid('terrarion'/'RAV', 'RAV:Terrarion:terrarion').
card_rarity('terrarion'/'RAV', 'Common').
card_artist('terrarion'/'RAV', 'Luca Zontini').
card_number('terrarion'/'RAV', '273').
card_multiverse_id('terrarion'/'RAV', '83629').

card_in_set('thoughtpicker witch', 'RAV').
card_original_type('thoughtpicker witch'/'RAV', 'Creature — Human Wizard').
card_original_text('thoughtpicker witch'/'RAV', '{1}, Sacrifice a creature: Look at the top two cards of target opponent\'s library, then remove one of them from the game.').
card_first_print('thoughtpicker witch', 'RAV').
card_image_name('thoughtpicker witch'/'RAV', 'thoughtpicker witch').
card_uid('thoughtpicker witch'/'RAV', 'RAV:Thoughtpicker Witch:thoughtpicker witch').
card_rarity('thoughtpicker witch'/'RAV', 'Common').
card_artist('thoughtpicker witch'/'RAV', 'Pete Venters').
card_number('thoughtpicker witch'/'RAV', '109').
card_flavor_text('thoughtpicker witch'/'RAV', '\"Once the brew gets the brains nice and pickled, they\'re a lot easier to pick through.\"').
card_multiverse_id('thoughtpicker witch'/'RAV', '83576').

card_in_set('three dreams', 'RAV').
card_original_type('three dreams'/'RAV', 'Sorcery').
card_original_text('three dreams'/'RAV', 'Search your library for up to three Aura cards with different names, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('three dreams', 'RAV').
card_image_name('three dreams'/'RAV', 'three dreams').
card_uid('three dreams'/'RAV', 'RAV:Three Dreams:three dreams').
card_rarity('three dreams'/'RAV', 'Rare').
card_artist('three dreams'/'RAV', 'Shishizaru').
card_number('three dreams'/'RAV', '32').
card_flavor_text('three dreams'/'RAV', '\"Choose one to heal, one to harm, and one to grant you the prudence to use them.\"\n—Miotri, auratouched mage').
card_multiverse_id('three dreams'/'RAV', '83905').

card_in_set('thundersong trumpeter', 'RAV').
card_original_type('thundersong trumpeter'/'RAV', 'Creature — Human Soldier').
card_original_text('thundersong trumpeter'/'RAV', '{T}: Target creature can\'t attack or block this turn.').
card_first_print('thundersong trumpeter', 'RAV').
card_image_name('thundersong trumpeter'/'RAV', 'thundersong trumpeter').
card_uid('thundersong trumpeter'/'RAV', 'RAV:Thundersong Trumpeter:thundersong trumpeter').
card_rarity('thundersong trumpeter'/'RAV', 'Common').
card_artist('thundersong trumpeter'/'RAV', 'Michael Sutfin').
card_number('thundersong trumpeter'/'RAV', '235').
card_flavor_text('thundersong trumpeter'/'RAV', '\"Hear that? Those notes mean we\'ve arrived at Sunhome! Let our allies\' hearts soar and our enemies\' hearts shatter at the sound!\"\n—Klattic, Boros legionnaire').
card_multiverse_id('thundersong trumpeter'/'RAV', '83611').
card_watermark('thundersong trumpeter'/'RAV', 'Boros').

card_in_set('tidewater minion', 'RAV').
card_original_type('tidewater minion'/'RAV', 'Creature — Elemental').
card_original_text('tidewater minion'/'RAV', 'Defender (This creature can\'t attack.)\n{4}: Tidewater Minion loses defender until end of turn.\n{T}: Untap target permanent.').
card_first_print('tidewater minion', 'RAV').
card_image_name('tidewater minion'/'RAV', 'tidewater minion').
card_uid('tidewater minion'/'RAV', 'RAV:Tidewater Minion:tidewater minion').
card_rarity('tidewater minion'/'RAV', 'Common').
card_artist('tidewater minion'/'RAV', 'Tomas Giorello').
card_number('tidewater minion'/'RAV', '71').
card_multiverse_id('tidewater minion'/'RAV', '83679').

card_in_set('tolsimir wolfblood', 'RAV').
card_original_type('tolsimir wolfblood'/'RAV', 'Legendary Creature — Elf Warrior').
card_original_text('tolsimir wolfblood'/'RAV', 'Other green creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\n{T}: Put a legendary 2/2 green and white Wolf creature token named Voja into play.').
card_first_print('tolsimir wolfblood', 'RAV').
card_image_name('tolsimir wolfblood'/'RAV', 'tolsimir wolfblood').
card_uid('tolsimir wolfblood'/'RAV', 'RAV:Tolsimir Wolfblood:tolsimir wolfblood').
card_rarity('tolsimir wolfblood'/'RAV', 'Rare').
card_artist('tolsimir wolfblood'/'RAV', 'Donato Giancola').
card_number('tolsimir wolfblood'/'RAV', '236').
card_multiverse_id('tolsimir wolfblood'/'RAV', '89110').
card_watermark('tolsimir wolfblood'/'RAV', 'Selesnya').

card_in_set('torpid moloch', 'RAV').
card_original_type('torpid moloch'/'RAV', 'Creature — Lizard').
card_original_text('torpid moloch'/'RAV', 'Defender (This creature can\'t attack.)\nSacrifice three lands: Torpid Moloch loses defender until end of turn.').
card_first_print('torpid moloch', 'RAV').
card_image_name('torpid moloch'/'RAV', 'torpid moloch').
card_uid('torpid moloch'/'RAV', 'RAV:Torpid Moloch:torpid moloch').
card_rarity('torpid moloch'/'RAV', 'Common').
card_artist('torpid moloch'/'RAV', 'Paolo Parente').
card_number('torpid moloch'/'RAV', '147').
card_flavor_text('torpid moloch'/'RAV', 'Market hucksters sell molochs for use as watchdogs. Of course, that\'s all molochs do. Sit around . . . and watch.').
card_multiverse_id('torpid moloch'/'RAV', '89004').

card_in_set('transluminant', 'RAV').
card_original_type('transluminant'/'RAV', 'Creature — Dryad Shaman').
card_original_text('transluminant'/'RAV', '{W}, Sacrifice Transluminant: Put a 1/1 white Spirit creature token with flying into play at end of turn.').
card_first_print('transluminant', 'RAV').
card_image_name('transluminant'/'RAV', 'transluminant').
card_uid('transluminant'/'RAV', 'RAV:Transluminant:transluminant').
card_rarity('transluminant'/'RAV', 'Common').
card_artist('transluminant'/'RAV', 'Greg Hildebrandt').
card_number('transluminant'/'RAV', '186').
card_flavor_text('transluminant'/'RAV', '\"Forget yourself. Forget your city. Forget your homes, your families, the debts and obligations that hold you to this world.\"').
card_multiverse_id('transluminant'/'RAV', '87946').
card_watermark('transluminant'/'RAV', 'Selesnya').

card_in_set('trophy hunter', 'RAV').
card_original_type('trophy hunter'/'RAV', 'Creature — Human Archer').
card_original_text('trophy hunter'/'RAV', '{1}{G}: Trophy Hunter deals 1 damage to target creature with flying.\nWhenever a creature with flying dealt damage by Trophy Hunter this turn is put into a graveyard, put a +1/+1 counter on Trophy Hunter.').
card_first_print('trophy hunter', 'RAV').
card_image_name('trophy hunter'/'RAV', 'trophy hunter').
card_uid('trophy hunter'/'RAV', 'RAV:Trophy Hunter:trophy hunter').
card_rarity('trophy hunter'/'RAV', 'Uncommon').
card_artist('trophy hunter'/'RAV', 'rk post').
card_number('trophy hunter'/'RAV', '187').
card_multiverse_id('trophy hunter'/'RAV', '89107').

card_in_set('tunnel vision', 'RAV').
card_original_type('tunnel vision'/'RAV', 'Sorcery').
card_original_text('tunnel vision'/'RAV', 'Name a card. Target player reveals cards from the top of his or her library until the named card is revealed. If it is, that player puts the rest of the revealed cards into his or her graveyard and puts the named card on top of his or her library. Otherwise, the player shuffles his or her library.').
card_first_print('tunnel vision', 'RAV').
card_image_name('tunnel vision'/'RAV', 'tunnel vision').
card_uid('tunnel vision'/'RAV', 'RAV:Tunnel Vision:tunnel vision').
card_rarity('tunnel vision'/'RAV', 'Rare').
card_artist('tunnel vision'/'RAV', 'Dany Orizio').
card_number('tunnel vision'/'RAV', '72').
card_multiverse_id('tunnel vision'/'RAV', '89022').

card_in_set('twilight drover', 'RAV').
card_original_type('twilight drover'/'RAV', 'Creature — Spirit').
card_original_text('twilight drover'/'RAV', 'Whenever a creature token leaves play, put a +1/+1 counter on Twilight Drover.\n{2}{W}, Remove a +1/+1 counter from Twilight Drover: Put two 1/1 white Spirit creature tokens with flying into play.').
card_first_print('twilight drover', 'RAV').
card_image_name('twilight drover'/'RAV', 'twilight drover').
card_uid('twilight drover'/'RAV', 'RAV:Twilight Drover:twilight drover').
card_rarity('twilight drover'/'RAV', 'Rare').
card_artist('twilight drover'/'RAV', 'Dave Allsop').
card_number('twilight drover'/'RAV', '33').
card_multiverse_id('twilight drover'/'RAV', '88973').

card_in_set('twisted justice', 'RAV').
card_original_type('twisted justice'/'RAV', 'Sorcery').
card_original_text('twisted justice'/'RAV', 'Target player sacrifices a creature. You draw cards equal to that creature\'s power.').
card_first_print('twisted justice', 'RAV').
card_image_name('twisted justice'/'RAV', 'twisted justice').
card_uid('twisted justice'/'RAV', 'RAV:Twisted Justice:twisted justice').
card_rarity('twisted justice'/'RAV', 'Uncommon').
card_artist('twisted justice'/'RAV', 'Ralph Horsley').
card_number('twisted justice'/'RAV', '237').
card_flavor_text('twisted justice'/'RAV', 'In Otiev\'s mind, he ruled in favor of the accused. But in his courtroom he was only a spectator, watching his hand deliver the sign of death.').
card_multiverse_id('twisted justice'/'RAV', '89052').
card_watermark('twisted justice'/'RAV', 'Dimir').

card_in_set('undercity shade', 'RAV').
card_original_type('undercity shade'/'RAV', 'Creature — Shade').
card_original_text('undercity shade'/'RAV', 'Fear\n{B}: Undercity Shade gets +1/+1 until end of turn.').
card_first_print('undercity shade', 'RAV').
card_image_name('undercity shade'/'RAV', 'undercity shade').
card_uid('undercity shade'/'RAV', 'RAV:Undercity Shade:undercity shade').
card_rarity('undercity shade'/'RAV', 'Uncommon').
card_artist('undercity shade'/'RAV', 'Dana Knutson').
card_number('undercity shade'/'RAV', '110').
card_flavor_text('undercity shade'/'RAV', '\"Officially, there\'s nothing under that bridge to hunt . . . Unofficially, my torchers are too terrified to go there after nightfall.\"\n—Agrus Kos').
card_multiverse_id('undercity shade'/'RAV', '88951').

card_in_set('ursapine', 'RAV').
card_original_type('ursapine'/'RAV', 'Creature — Beast').
card_original_text('ursapine'/'RAV', '{G}: Target creature gets +1/+1 until end of turn.').
card_first_print('ursapine', 'RAV').
card_image_name('ursapine'/'RAV', 'ursapine').
card_uid('ursapine'/'RAV', 'RAV:Ursapine:ursapine').
card_rarity('ursapine'/'RAV', 'Rare').
card_artist('ursapine'/'RAV', 'Jim Murray').
card_number('ursapine'/'RAV', '188').
card_flavor_text('ursapine'/'RAV', 'A friendly ursapine means a constant supply of sharp spears. An unfriendly one means a constant supply of small holes.').
card_multiverse_id('ursapine'/'RAV', '88979').

card_in_set('vedalken dismisser', 'RAV').
card_original_type('vedalken dismisser'/'RAV', 'Creature — Vedalken Wizard').
card_original_text('vedalken dismisser'/'RAV', 'When Vedalken Dismisser comes into play, put target creature on top of its owner\'s library.').
card_first_print('vedalken dismisser', 'RAV').
card_image_name('vedalken dismisser'/'RAV', 'vedalken dismisser').
card_uid('vedalken dismisser'/'RAV', 'RAV:Vedalken Dismisser:vedalken dismisser').
card_rarity('vedalken dismisser'/'RAV', 'Common').
card_artist('vedalken dismisser'/'RAV', 'Dan Scott').
card_number('vedalken dismisser'/'RAV', '73').
card_flavor_text('vedalken dismisser'/'RAV', '\"I crept up on him, quiet as a guttermouse. And then suddenly I found myself facedown in a latrine pit behind Tin Street Market.\"\n—Sirislav, Dimir spy').
card_multiverse_id('vedalken dismisser'/'RAV', '87898').

card_in_set('vedalken entrancer', 'RAV').
card_original_type('vedalken entrancer'/'RAV', 'Creature — Vedalken Wizard').
card_original_text('vedalken entrancer'/'RAV', '{U}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('vedalken entrancer', 'RAV').
card_image_name('vedalken entrancer'/'RAV', 'vedalken entrancer').
card_uid('vedalken entrancer'/'RAV', 'RAV:Vedalken Entrancer:vedalken entrancer').
card_rarity('vedalken entrancer'/'RAV', 'Common').
card_artist('vedalken entrancer'/'RAV', 'Dan Scott').
card_number('vedalken entrancer'/'RAV', '74').
card_flavor_text('vedalken entrancer'/'RAV', 'Their denial reaches far into your future.').
card_multiverse_id('vedalken entrancer'/'RAV', '89028').

card_in_set('veteran armorer', 'RAV').
card_original_type('veteran armorer'/'RAV', 'Creature — Human Soldier').
card_original_text('veteran armorer'/'RAV', 'Other creatures you control get +0/+1.').
card_first_print('veteran armorer', 'RAV').
card_image_name('veteran armorer'/'RAV', 'veteran armorer').
card_uid('veteran armorer'/'RAV', 'RAV:Veteran Armorer:veteran armorer').
card_rarity('veteran armorer'/'RAV', 'Common').
card_artist('veteran armorer'/'RAV', 'Ralph Horsley').
card_number('veteran armorer'/'RAV', '34').
card_flavor_text('veteran armorer'/'RAV', '\"Give me a sword and I\'ll kill a few enemies. But give me a hammer and a fiery forge, and I\'ll turn the tide of battle.\"').
card_multiverse_id('veteran armorer'/'RAV', '87950').

card_in_set('viashino fangtail', 'RAV').
card_original_type('viashino fangtail'/'RAV', 'Creature — Viashino Warrior').
card_original_text('viashino fangtail'/'RAV', '{T}: Viashino Fangtail deals 1 damage to target creature or player.').
card_first_print('viashino fangtail', 'RAV').
card_image_name('viashino fangtail'/'RAV', 'viashino fangtail').
card_uid('viashino fangtail'/'RAV', 'RAV:Viashino Fangtail:viashino fangtail').
card_rarity('viashino fangtail'/'RAV', 'Common').
card_artist('viashino fangtail'/'RAV', 'Alex Horley-Orlandelli').
card_number('viashino fangtail'/'RAV', '148').
card_flavor_text('viashino fangtail'/'RAV', '\"When the bony terminus gets too large, it\'s shed from the tail in coinciding periods of heightened aggression and hunger.\"\n—Simic research notes').
card_multiverse_id('viashino fangtail'/'RAV', '87974').

card_in_set('viashino slasher', 'RAV').
card_original_type('viashino slasher'/'RAV', 'Creature — Viashino Warrior').
card_original_text('viashino slasher'/'RAV', '{R}: Viashino Slasher gets +1/-1 until end of turn.').
card_first_print('viashino slasher', 'RAV').
card_image_name('viashino slasher'/'RAV', 'viashino slasher').
card_uid('viashino slasher'/'RAV', 'RAV:Viashino Slasher:viashino slasher').
card_rarity('viashino slasher'/'RAV', 'Common').
card_artist('viashino slasher'/'RAV', 'Glenn Fabry').
card_number('viashino slasher'/'RAV', '149').
card_flavor_text('viashino slasher'/'RAV', 'Citizens of Ravnica stick to the main streets. Shortcuts may look tempting, but down each alley, dozens of hungry hands await, and screams do not carry into the busy market throng.').
card_multiverse_id('viashino slasher'/'RAV', '83605').

card_in_set('vigor mortis', 'RAV').
card_original_type('vigor mortis'/'RAV', 'Sorcery').
card_original_text('vigor mortis'/'RAV', 'Return target creature card from your graveyard to play. If {G} was spent to play Vigor Mortis, that creature comes into play with a +1/+1 counter on it.').
card_first_print('vigor mortis', 'RAV').
card_image_name('vigor mortis'/'RAV', 'vigor mortis').
card_uid('vigor mortis'/'RAV', 'RAV:Vigor Mortis:vigor mortis').
card_rarity('vigor mortis'/'RAV', 'Uncommon').
card_artist('vigor mortis'/'RAV', 'Dan Scott').
card_number('vigor mortis'/'RAV', '111').
card_flavor_text('vigor mortis'/'RAV', 'After fear and weakness rot away, that which remains is stronger than its living form.').
card_multiverse_id('vigor mortis'/'RAV', '88986').
card_watermark('vigor mortis'/'RAV', 'Golgari').

card_in_set('vindictive mob', 'RAV').
card_original_type('vindictive mob'/'RAV', 'Creature — Human Berserker').
card_original_text('vindictive mob'/'RAV', 'When Vindictive Mob comes into play, sacrifice a creature.\nVindictive Mob can\'t be blocked by Saprolings.').
card_first_print('vindictive mob', 'RAV').
card_image_name('vindictive mob'/'RAV', 'vindictive mob').
card_uid('vindictive mob'/'RAV', 'RAV:Vindictive Mob:vindictive mob').
card_rarity('vindictive mob'/'RAV', 'Uncommon').
card_artist('vindictive mob'/'RAV', 'Wayne Reynolds').
card_number('vindictive mob'/'RAV', '112').
card_flavor_text('vindictive mob'/'RAV', 'Many minds, a single madness.').
card_multiverse_id('vindictive mob'/'RAV', '89002').

card_in_set('vinelasher kudzu', 'RAV').
card_original_type('vinelasher kudzu'/'RAV', 'Creature — Plant').
card_original_text('vinelasher kudzu'/'RAV', 'Whenever a land comes into play under your control, put a +1/+1 counter on Vinelasher Kudzu.').
card_first_print('vinelasher kudzu', 'RAV').
card_image_name('vinelasher kudzu'/'RAV', 'vinelasher kudzu').
card_uid('vinelasher kudzu'/'RAV', 'RAV:Vinelasher Kudzu:vinelasher kudzu').
card_rarity('vinelasher kudzu'/'RAV', 'Rare').
card_artist('vinelasher kudzu'/'RAV', 'Mark Tedin').
card_number('vinelasher kudzu'/'RAV', '189').
card_flavor_text('vinelasher kudzu'/'RAV', 'It grows to hate you.').
card_multiverse_id('vinelasher kudzu'/'RAV', '83559').

card_in_set('vitu-ghazi, the city-tree', 'RAV').
card_original_type('vitu-ghazi, the city-tree'/'RAV', 'Land').
card_original_text('vitu-ghazi, the city-tree'/'RAV', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token into play.').
card_first_print('vitu-ghazi, the city-tree', 'RAV').
card_image_name('vitu-ghazi, the city-tree'/'RAV', 'vitu-ghazi, the city-tree').
card_uid('vitu-ghazi, the city-tree'/'RAV', 'RAV:Vitu-Ghazi, the City-Tree:vitu-ghazi, the city-tree').
card_rarity('vitu-ghazi, the city-tree'/'RAV', 'Uncommon').
card_artist('vitu-ghazi, the city-tree'/'RAV', 'Martina Pilcerova').
card_number('vitu-ghazi, the city-tree'/'RAV', '285').
card_flavor_text('vitu-ghazi, the city-tree'/'RAV', 'In the autumn, she casts her seeds across the streets below, and come spring, her children rise in service to the Conclave.').
card_multiverse_id('vitu-ghazi, the city-tree'/'RAV', '89016').
card_watermark('vitu-ghazi, the city-tree'/'RAV', 'Selesnya').

card_in_set('votary of the conclave', 'RAV').
card_original_type('votary of the conclave'/'RAV', 'Creature — Human Soldier').
card_original_text('votary of the conclave'/'RAV', '{2}{G}: Regenerate Votary of the Conclave.').
card_first_print('votary of the conclave', 'RAV').
card_image_name('votary of the conclave'/'RAV', 'votary of the conclave').
card_uid('votary of the conclave'/'RAV', 'RAV:Votary of the Conclave:votary of the conclave').
card_rarity('votary of the conclave'/'RAV', 'Common').
card_artist('votary of the conclave'/'RAV', 'Alan Pollack').
card_number('votary of the conclave'/'RAV', '35').
card_flavor_text('votary of the conclave'/'RAV', 'An ageless order, the votaries receive the gift of rejuvenation from the temple gardens they guard.').
card_multiverse_id('votary of the conclave'/'RAV', '87948').
card_watermark('votary of the conclave'/'RAV', 'Selesnya').

card_in_set('voyager staff', 'RAV').
card_original_type('voyager staff'/'RAV', 'Artifact').
card_original_text('voyager staff'/'RAV', '{2}, Sacrifice Voyager Staff: Remove target creature from the game. Return that creature to play under its owner\'s control at end of turn.').
card_first_print('voyager staff', 'RAV').
card_image_name('voyager staff'/'RAV', 'voyager staff').
card_uid('voyager staff'/'RAV', 'RAV:Voyager Staff:voyager staff').
card_rarity('voyager staff'/'RAV', 'Uncommon').
card_artist('voyager staff'/'RAV', 'Tsutomu Kawade').
card_number('voyager staff'/'RAV', '274').
card_flavor_text('voyager staff'/'RAV', 'Voyager staffs are sought by those hoping to find an escape from the sprawling glut of Ravnica.').
card_multiverse_id('voyager staff'/'RAV', '88991').

card_in_set('vulturous zombie', 'RAV').
card_original_type('vulturous zombie'/'RAV', 'Creature — Plant Zombie').
card_original_text('vulturous zombie'/'RAV', 'Flying\nWhenever a card is put into an opponent\'s graveyard from anywhere, put a +1/+1 counter on Vulturous Zombie.').
card_first_print('vulturous zombie', 'RAV').
card_image_name('vulturous zombie'/'RAV', 'vulturous zombie').
card_uid('vulturous zombie'/'RAV', 'RAV:Vulturous Zombie:vulturous zombie').
card_rarity('vulturous zombie'/'RAV', 'Rare').
card_artist('vulturous zombie'/'RAV', 'Greg Staples').
card_number('vulturous zombie'/'RAV', '238').
card_flavor_text('vulturous zombie'/'RAV', '\"When something dies, all things benefit. Well okay, just our things.\"\n—Ezoc, Golgari rot farmer').
card_multiverse_id('vulturous zombie'/'RAV', '89082').
card_watermark('vulturous zombie'/'RAV', 'Golgari').

card_in_set('war-torch goblin', 'RAV').
card_original_type('war-torch goblin'/'RAV', 'Creature — Goblin Warrior').
card_original_text('war-torch goblin'/'RAV', '{R}, Sacrifice War-Torch Goblin: War-Torch Goblin deals 2 damage to target blocking creature.').
card_first_print('war-torch goblin', 'RAV').
card_image_name('war-torch goblin'/'RAV', 'war-torch goblin').
card_uid('war-torch goblin'/'RAV', 'RAV:War-Torch Goblin:war-torch goblin').
card_rarity('war-torch goblin'/'RAV', 'Common').
card_artist('war-torch goblin'/'RAV', 'Alex Horley-Orlandelli').
card_number('war-torch goblin'/'RAV', '151').
card_flavor_text('war-torch goblin'/'RAV', '\"In a town shaped by the subtle machinations and political intrigue of its guilds, it\'s reassuring to see a goblin waving his torch and screaming about some nonsense or other.\"\n—Agrus Kos').
card_multiverse_id('war-torch goblin'/'RAV', '87971').

card_in_set('warp world', 'RAV').
card_original_type('warp world'/'RAV', 'Sorcery').
card_original_text('warp world'/'RAV', 'Each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, and land cards revealed this way into play, then puts all enchantment cards revealed this way into play, then puts the rest on the bottom of his or her library in any order.').
card_first_print('warp world', 'RAV').
card_image_name('warp world'/'RAV', 'warp world').
card_uid('warp world'/'RAV', 'RAV:Warp World:warp world').
card_rarity('warp world'/'RAV', 'Rare').
card_artist('warp world'/'RAV', 'Ron Spencer').
card_number('warp world'/'RAV', '150').
card_multiverse_id('warp world'/'RAV', '89084').

card_in_set('watchwolf', 'RAV').
card_original_type('watchwolf'/'RAV', 'Creature — Wolf').
card_original_text('watchwolf'/'RAV', '').
card_image_name('watchwolf'/'RAV', 'watchwolf').
card_uid('watchwolf'/'RAV', 'RAV:Watchwolf:watchwolf').
card_rarity('watchwolf'/'RAV', 'Uncommon').
card_artist('watchwolf'/'RAV', 'Kev Walker').
card_number('watchwolf'/'RAV', '239').
card_flavor_text('watchwolf'/'RAV', 'Only in Ravnica do the wolves watch the flock.').
card_multiverse_id('watchwolf'/'RAV', '83625').
card_watermark('watchwolf'/'RAV', 'Selesnya').

card_in_set('watery grave', 'RAV').
card_original_type('watery grave'/'RAV', 'Land — Island Swamp').
card_original_text('watery grave'/'RAV', '({T}: Add {U} or {B} to your mana pool.)\nAs Watery Grave comes into play, you may pay 2 life. If you don\'t, Watery Grave comes into play tapped instead.').
card_first_print('watery grave', 'RAV').
card_image_name('watery grave'/'RAV', 'watery grave').
card_uid('watery grave'/'RAV', 'RAV:Watery Grave:watery grave').
card_rarity('watery grave'/'RAV', 'Rare').
card_artist('watery grave'/'RAV', 'Rob Alexander').
card_number('watery grave'/'RAV', '286').
card_multiverse_id('watery grave'/'RAV', '83731').
card_watermark('watery grave'/'RAV', 'Dimir').

card_in_set('wizened snitches', 'RAV').
card_original_type('wizened snitches'/'RAV', 'Creature — Faerie Rogue').
card_original_text('wizened snitches'/'RAV', 'Flying\nPlayers play with the top card of their libraries revealed.').
card_first_print('wizened snitches', 'RAV').
card_image_name('wizened snitches'/'RAV', 'wizened snitches').
card_uid('wizened snitches'/'RAV', 'RAV:Wizened Snitches:wizened snitches').
card_rarity('wizened snitches'/'RAV', 'Uncommon').
card_artist('wizened snitches'/'RAV', 'Greg Staples').
card_number('wizened snitches'/'RAV', '75').
card_flavor_text('wizened snitches'/'RAV', '\"Be careful what information you ask them to find, for it will likely be the subject of tale time at the alehouse.\"\n—Sirislav, Dimir spy').
card_multiverse_id('wizened snitches'/'RAV', '88971').

card_in_set('woebringer demon', 'RAV').
card_original_type('woebringer demon'/'RAV', 'Creature — Demon').
card_original_text('woebringer demon'/'RAV', 'Flying\nAt the beginning of each player\'s upkeep, that player sacrifices a creature. If the player can\'t, sacrifice Woebringer Demon.').
card_first_print('woebringer demon', 'RAV').
card_image_name('woebringer demon'/'RAV', 'woebringer demon').
card_uid('woebringer demon'/'RAV', 'RAV:Woebringer Demon:woebringer demon').
card_rarity('woebringer demon'/'RAV', 'Rare').
card_artist('woebringer demon'/'RAV', 'Daren Bader').
card_number('woebringer demon'/'RAV', '113').
card_flavor_text('woebringer demon'/'RAV', 'Each soul he devours adds its hunger to his own.').
card_multiverse_id('woebringer demon'/'RAV', '89049').

card_in_set('wojek apothecary', 'RAV').
card_original_type('wojek apothecary'/'RAV', 'Creature — Human Cleric').
card_original_text('wojek apothecary'/'RAV', 'Radiance {T}: Prevent the next 1 damage that would be dealt to target creature and each other creature that shares a color with it this turn.').
card_first_print('wojek apothecary', 'RAV').
card_image_name('wojek apothecary'/'RAV', 'wojek apothecary').
card_uid('wojek apothecary'/'RAV', 'RAV:Wojek Apothecary:wojek apothecary').
card_rarity('wojek apothecary'/'RAV', 'Uncommon').
card_artist('wojek apothecary'/'RAV', 'Keith Garletts').
card_number('wojek apothecary'/'RAV', '36').
card_flavor_text('wojek apothecary'/'RAV', '\"A few arrows aren\'t enough to pierce your faith, soldier. You\'ll be back on the battlefield by sun\'s dawn.\"').
card_multiverse_id('wojek apothecary'/'RAV', '88993').
card_watermark('wojek apothecary'/'RAV', 'Boros').

card_in_set('wojek embermage', 'RAV').
card_original_type('wojek embermage'/'RAV', 'Creature — Human Wizard').
card_original_text('wojek embermage'/'RAV', 'Radiance {T}: Wojek Embermage deals 1 damage to target creature and each other creature that shares a color with it.').
card_first_print('wojek embermage', 'RAV').
card_image_name('wojek embermage'/'RAV', 'wojek embermage').
card_uid('wojek embermage'/'RAV', 'RAV:Wojek Embermage:wojek embermage').
card_rarity('wojek embermage'/'RAV', 'Uncommon').
card_artist('wojek embermage'/'RAV', 'Luca Zontini').
card_number('wojek embermage'/'RAV', '152').
card_flavor_text('wojek embermage'/'RAV', '\"Your brother\'s crimes are your crimes. You stood by and lent support, so you too must face judgment.\"').
card_multiverse_id('wojek embermage'/'RAV', '88946').
card_watermark('wojek embermage'/'RAV', 'Boros').

card_in_set('wojek siren', 'RAV').
card_original_type('wojek siren'/'RAV', 'Instant').
card_original_text('wojek siren'/'RAV', 'Radiance Target creature and each other creature that shares a color with it get +1/+1 until end of turn.').
card_first_print('wojek siren', 'RAV').
card_image_name('wojek siren'/'RAV', 'wojek siren').
card_uid('wojek siren'/'RAV', 'RAV:Wojek Siren:wojek siren').
card_rarity('wojek siren'/'RAV', 'Common').
card_artist('wojek siren'/'RAV', 'Zoltan Boros & Gabor Szikszai').
card_number('wojek siren'/'RAV', '37').
card_flavor_text('wojek siren'/'RAV', 'It is the call to arms, the call to fury, the call to blood.').
card_multiverse_id('wojek siren'/'RAV', '87896').
card_watermark('wojek siren'/'RAV', 'Boros').

card_in_set('woodwraith corrupter', 'RAV').
card_original_type('woodwraith corrupter'/'RAV', 'Creature — Elemental Horror').
card_original_text('woodwraith corrupter'/'RAV', '{1}{B}{G}, {T}: Target Forest becomes a 4/4 black and green Elemental Horror creature. It\'s still a land.').
card_first_print('woodwraith corrupter', 'RAV').
card_image_name('woodwraith corrupter'/'RAV', 'woodwraith corrupter').
card_uid('woodwraith corrupter'/'RAV', 'RAV:Woodwraith Corrupter:woodwraith corrupter').
card_rarity('woodwraith corrupter'/'RAV', 'Rare').
card_artist('woodwraith corrupter'/'RAV', 'Greg Staples').
card_number('woodwraith corrupter'/'RAV', '240').
card_flavor_text('woodwraith corrupter'/'RAV', '\"Its darkened sap penetrates natural fibers and spreads its own genes. Emulation experiments are underway.\"\n—Simic research notes').
card_multiverse_id('woodwraith corrupter'/'RAV', '83747').
card_watermark('woodwraith corrupter'/'RAV', 'Golgari').

card_in_set('woodwraith strangler', 'RAV').
card_original_type('woodwraith strangler'/'RAV', 'Creature — Plant Zombie').
card_original_text('woodwraith strangler'/'RAV', 'Remove a creature card in your graveyard from the game: Regenerate Woodwraith Strangler.').
card_first_print('woodwraith strangler', 'RAV').
card_image_name('woodwraith strangler'/'RAV', 'woodwraith strangler').
card_uid('woodwraith strangler'/'RAV', 'RAV:Woodwraith Strangler:woodwraith strangler').
card_rarity('woodwraith strangler'/'RAV', 'Common').
card_artist('woodwraith strangler'/'RAV', 'Pat Lee').
card_number('woodwraith strangler'/'RAV', '241').
card_flavor_text('woodwraith strangler'/'RAV', '\"Nothing could be more natural than roots sucking nourishment from the dead.\"\n—Savra').
card_multiverse_id('woodwraith strangler'/'RAV', '83908').
card_watermark('woodwraith strangler'/'RAV', 'Golgari').

card_in_set('zephyr spirit', 'RAV').
card_original_type('zephyr spirit'/'RAV', 'Creature — Spirit').
card_original_text('zephyr spirit'/'RAV', 'When Zephyr Spirit blocks, return it to its owner\'s hand.').
card_first_print('zephyr spirit', 'RAV').
card_image_name('zephyr spirit'/'RAV', 'zephyr spirit').
card_uid('zephyr spirit'/'RAV', 'RAV:Zephyr Spirit:zephyr spirit').
card_rarity('zephyr spirit'/'RAV', 'Common').
card_artist('zephyr spirit'/'RAV', 'Tomas Giorello').
card_number('zephyr spirit'/'RAV', '76').
card_flavor_text('zephyr spirit'/'RAV', 'A spiteful force exists on Ravnica that binds these ghosts to the land of the living but forbids them to touch it.').
card_multiverse_id('zephyr spirit'/'RAV', '88962').
