% Duel Decks: Phyrexia vs. the Coalition

set('DDE').
set_name('DDE', 'Duel Decks: Phyrexia vs. the Coalition').
set_release_date('DDE', '2010-03-19').
set_border('DDE', 'black').
set_type('DDE', 'duel deck').

card_in_set('allied strategies', 'DDE').
card_original_type('allied strategies'/'DDE', 'Sorcery').
card_original_text('allied strategies'/'DDE', 'Domain — Target player draws a card for each basic land type among lands he or she controls.').
card_image_name('allied strategies'/'DDE', 'allied strategies').
card_uid('allied strategies'/'DDE', 'DDE:Allied Strategies:allied strategies').
card_rarity('allied strategies'/'DDE', 'Uncommon').
card_artist('allied strategies'/'DDE', 'Paolo Parente').
card_number('allied strategies'/'DDE', '63').
card_flavor_text('allied strategies'/'DDE', 'Each commander looked at the others and wondered who would be first to break the alliance.').
card_multiverse_id('allied strategies'/'DDE', '209148').

card_in_set('armadillo cloak', 'DDE').
card_original_type('armadillo cloak'/'DDE', 'Enchantment — Aura').
card_original_text('armadillo cloak'/'DDE', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample.\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('armadillo cloak'/'DDE', 'armadillo cloak').
card_uid('armadillo cloak'/'DDE', 'DDE:Armadillo Cloak:armadillo cloak').
card_rarity('armadillo cloak'/'DDE', 'Common').
card_artist('armadillo cloak'/'DDE', 'Wayne Reynolds').
card_number('armadillo cloak'/'DDE', '58').
card_flavor_text('armadillo cloak'/'DDE', '\"Don\'t laugh. It works.\"').
card_multiverse_id('armadillo cloak'/'DDE', '207890').

card_in_set('bone shredder', 'DDE').
card_original_type('bone shredder'/'DDE', 'Creature — Minion').
card_original_text('bone shredder'/'DDE', 'Flying\nEcho {2}{B} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Bone Shredder enters the battlefield, destroy target nonartifact, nonblack creature.').
card_image_name('bone shredder'/'DDE', 'bone shredder').
card_uid('bone shredder'/'DDE', 'DDE:Bone Shredder:bone shredder').
card_rarity('bone shredder'/'DDE', 'Uncommon').
card_artist('bone shredder'/'DDE', 'Ron Spencer').
card_number('bone shredder'/'DDE', '5').
card_multiverse_id('bone shredder'/'DDE', '209123').

card_in_set('carrion feeder', 'DDE').
card_original_type('carrion feeder'/'DDE', 'Creature — Zombie').
card_original_text('carrion feeder'/'DDE', 'Carrion Feeder can\'t block.\nSacrifice a creature: Put a +1/+1 counter on Carrion Feeder.').
card_image_name('carrion feeder'/'DDE', 'carrion feeder').
card_uid('carrion feeder'/'DDE', 'DDE:Carrion Feeder:carrion feeder').
card_rarity('carrion feeder'/'DDE', 'Common').
card_artist('carrion feeder'/'DDE', 'Brian Snõddy').
card_number('carrion feeder'/'DDE', '2').
card_flavor_text('carrion feeder'/'DDE', 'Stinking of rot, it leaps between gravestones in search of its next meal.').
card_multiverse_id('carrion feeder'/'DDE', '210133').

card_in_set('charging troll', 'DDE').
card_original_type('charging troll'/'DDE', 'Creature — Troll').
card_original_text('charging troll'/'DDE', 'Vigilance\n{G}: Regenerate Charging Troll.').
card_image_name('charging troll'/'DDE', 'charging troll').
card_uid('charging troll'/'DDE', 'DDE:Charging Troll:charging troll').
card_rarity('charging troll'/'DDE', 'Uncommon').
card_artist('charging troll'/'DDE', 'Dave Dorman').
card_number('charging troll'/'DDE', '45').
card_flavor_text('charging troll'/'DDE', 'They stop for nothing, not even the end of a battle.').
card_multiverse_id('charging troll'/'DDE', '210556').

card_in_set('coalition relic', 'DDE').
card_original_type('coalition relic'/'DDE', 'Artifact').
card_original_text('coalition relic'/'DDE', '{T}: Add one mana of any color to your mana pool.\n{T}: Put a charge counter on Coalition Relic.\nAt the beginning of your precombat main phase, remove all charge counters from Coalition Relic. Add one mana of any color to your mana pool for each counter removed this way.').
card_image_name('coalition relic'/'DDE', 'coalition relic').
card_uid('coalition relic'/'DDE', 'DDE:Coalition Relic:coalition relic').
card_rarity('coalition relic'/'DDE', 'Rare').
card_artist('coalition relic'/'DDE', 'Donato Giancola').
card_number('coalition relic'/'DDE', '54').
card_multiverse_id('coalition relic'/'DDE', '209158').

card_in_set('darigaaz\'s charm', 'DDE').
card_original_type('darigaaz\'s charm'/'DDE', 'Instant').
card_original_text('darigaaz\'s charm'/'DDE', 'Choose one — Return target creature card from your graveyard to your hand; or Darigaaz\'s Charm deals 3 damage to target creature or player; or target creature gets +3/+3 until end of turn.').
card_image_name('darigaaz\'s charm'/'DDE', 'darigaaz\'s charm').
card_uid('darigaaz\'s charm'/'DDE', 'DDE:Darigaaz\'s Charm:darigaaz\'s charm').
card_rarity('darigaaz\'s charm'/'DDE', 'Uncommon').
card_artist('darigaaz\'s charm'/'DDE', 'David Martin').
card_number('darigaaz\'s charm'/'DDE', '59').
card_multiverse_id('darigaaz\'s charm'/'DDE', '209149').

card_in_set('darigaaz, the igniter', 'DDE').
card_original_type('darigaaz, the igniter'/'DDE', 'Legendary Creature — Dragon').
card_original_text('darigaaz, the igniter'/'DDE', 'Flying\nWhenever Darigaaz, the Igniter deals combat damage to a player, you may pay {2}{R}. If you do, choose a color, then that player reveals his or her hand and Darigaaz deals damage to the player equal to the number of cards of that color revealed this way.').
card_image_name('darigaaz, the igniter'/'DDE', 'darigaaz, the igniter').
card_uid('darigaaz, the igniter'/'DDE', 'DDE:Darigaaz, the Igniter:darigaaz, the igniter').
card_rarity('darigaaz, the igniter'/'DDE', 'Rare').
card_artist('darigaaz, the igniter'/'DDE', 'Mark Zug').
card_number('darigaaz, the igniter'/'DDE', '47').
card_multiverse_id('darigaaz, the igniter'/'DDE', '209111').

card_in_set('dark ritual', 'DDE').
card_original_type('dark ritual'/'DDE', 'Instant').
card_original_text('dark ritual'/'DDE', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'DDE', 'dark ritual').
card_uid('dark ritual'/'DDE', 'DDE:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'DDE', 'Common').
card_artist('dark ritual'/'DDE', 'Tom Fleming').
card_number('dark ritual'/'DDE', '18').
card_flavor_text('dark ritual'/'DDE', '\"From void evolved Phyrexia. Great Yawgmoth, Father of Machines, saw its perfection. Thus the Grand Evolution began.\"\n—Phyrexian Scriptures').
card_multiverse_id('dark ritual'/'DDE', '209137').

card_in_set('elfhame palace', 'DDE').
card_original_type('elfhame palace'/'DDE', 'Land').
card_original_text('elfhame palace'/'DDE', 'Elfhame Palace enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('elfhame palace'/'DDE', 'elfhame palace').
card_uid('elfhame palace'/'DDE', 'DDE:Elfhame Palace:elfhame palace').
card_rarity('elfhame palace'/'DDE', 'Uncommon').
card_artist('elfhame palace'/'DDE', 'Jerry Tiritilli').
card_number('elfhame palace'/'DDE', '64').
card_flavor_text('elfhame palace'/'DDE', 'Llanowar has seven elfhames, or kingdoms, each with its own ruler. Their palaces are objects of awe, wonder, and envy.').
card_multiverse_id('elfhame palace'/'DDE', '209112').

card_in_set('evasive action', 'DDE').
card_original_type('evasive action'/'DDE', 'Instant').
card_original_text('evasive action'/'DDE', 'Domain — Counter target spell unless its controller pays {1} for each basic land type among lands you control.').
card_image_name('evasive action'/'DDE', 'evasive action').
card_uid('evasive action'/'DDE', 'DDE:Evasive Action:evasive action').
card_rarity('evasive action'/'DDE', 'Uncommon').
card_artist('evasive action'/'DDE', 'Brian Snõddy').
card_number('evasive action'/'DDE', '50').
card_flavor_text('evasive action'/'DDE', 'Effective use of terrain is a lesson good commanders learn quickly.').
card_multiverse_id('evasive action'/'DDE', '210153').

card_in_set('exotic curse', 'DDE').
card_original_type('exotic curse'/'DDE', 'Enchantment — Aura').
card_original_text('exotic curse'/'DDE', 'Enchant creature\nDomain — Enchanted creature gets -1/-1 for each basic land type among lands you control.').
card_image_name('exotic curse'/'DDE', 'exotic curse').
card_uid('exotic curse'/'DDE', 'DDE:Exotic Curse:exotic curse').
card_rarity('exotic curse'/'DDE', 'Common').
card_artist('exotic curse'/'DDE', 'Dany Orizio').
card_number('exotic curse'/'DDE', '56').
card_flavor_text('exotic curse'/'DDE', 'Fouler than a necromancer\'s kiss.\n—Jamuraan expression').
card_multiverse_id('exotic curse'/'DDE', '210143').

card_in_set('fertile ground', 'DDE').
card_original_type('fertile ground'/'DDE', 'Enchantment — Aura').
card_original_text('fertile ground'/'DDE', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_image_name('fertile ground'/'DDE', 'fertile ground').
card_uid('fertile ground'/'DDE', 'DDE:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'DDE', 'Common').
card_artist('fertile ground'/'DDE', 'Carl Critchlow').
card_number('fertile ground'/'DDE', '52').
card_flavor_text('fertile ground'/'DDE', 'As Phyrexians descended, Multani paused to reflect on the beauty that might never be seen again.').
card_multiverse_id('fertile ground'/'DDE', '209113').

card_in_set('forest', 'DDE').
card_original_type('forest'/'DDE', 'Basic Land — Forest').
card_original_text('forest'/'DDE', 'G').
card_image_name('forest'/'DDE', 'forest1').
card_uid('forest'/'DDE', 'DDE:Forest:forest1').
card_rarity('forest'/'DDE', 'Basic Land').
card_artist('forest'/'DDE', 'John Avon').
card_number('forest'/'DDE', '70').
card_multiverse_id('forest'/'DDE', '210509').

card_in_set('forest', 'DDE').
card_original_type('forest'/'DDE', 'Basic Land — Forest').
card_original_text('forest'/'DDE', 'G').
card_image_name('forest'/'DDE', 'forest2').
card_uid('forest'/'DDE', 'DDE:Forest:forest2').
card_rarity('forest'/'DDE', 'Basic Land').
card_artist('forest'/'DDE', 'Alan Pollack').
card_number('forest'/'DDE', '71').
card_multiverse_id('forest'/'DDE', '210510').

card_in_set('gerrard capashen', 'DDE').
card_original_type('gerrard capashen'/'DDE', 'Legendary Creature — Human Soldier').
card_original_text('gerrard capashen'/'DDE', 'At the beginning of your upkeep, you gain 1 life for each card in target opponent\'s hand.\n{3}{W}: Tap target creature. Activate this ability only if Gerrard Capashen is attacking.').
card_image_name('gerrard capashen'/'DDE', 'gerrard capashen').
card_uid('gerrard capashen'/'DDE', 'DDE:Gerrard Capashen:gerrard capashen').
card_rarity('gerrard capashen'/'DDE', 'Rare').
card_artist('gerrard capashen'/'DDE', 'Brom').
card_number('gerrard capashen'/'DDE', '46').
card_multiverse_id('gerrard capashen'/'DDE', '209157').

card_in_set('gerrard\'s command', 'DDE').
card_original_type('gerrard\'s command'/'DDE', 'Instant').
card_original_text('gerrard\'s command'/'DDE', 'Untap target creature. It gets +3/+3 until end of turn.').
card_image_name('gerrard\'s command'/'DDE', 'gerrard\'s command').
card_uid('gerrard\'s command'/'DDE', 'DDE:Gerrard\'s Command:gerrard\'s command').
card_rarity('gerrard\'s command'/'DDE', 'Common').
card_artist('gerrard\'s command'/'DDE', 'Roger Raupp').
card_number('gerrard\'s command'/'DDE', '53').
card_flavor_text('gerrard\'s command'/'DDE', '\"Unless you like the idea of becoming some goblin\'s lunch, get up and move!\"').
card_multiverse_id('gerrard\'s command'/'DDE', '210559').

card_in_set('harrow', 'DDE').
card_original_type('harrow'/'DDE', 'Instant').
card_original_text('harrow'/'DDE', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('harrow'/'DDE', 'harrow').
card_uid('harrow'/'DDE', 'DDE:Harrow:harrow').
card_rarity('harrow'/'DDE', 'Common').
card_artist('harrow'/'DDE', 'Rob Alexander').
card_number('harrow'/'DDE', '57').
card_flavor_text('harrow'/'DDE', 'No spot in nature is truly barren.').
card_multiverse_id('harrow'/'DDE', '209114').

card_in_set('hideous end', 'DDE').
card_original_type('hideous end'/'DDE', 'Instant').
card_original_text('hideous end'/'DDE', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_image_name('hideous end'/'DDE', 'hideous end').
card_uid('hideous end'/'DDE', 'DDE:Hideous End:hideous end').
card_rarity('hideous end'/'DDE', 'Common').
card_artist('hideous end'/'DDE', 'Zoltan Boros & Gabor Szikszai').
card_number('hideous end'/'DDE', '26').
card_flavor_text('hideous end'/'DDE', 'Death is a generous author, always willing to grant a stirring final chapter.').
card_multiverse_id('hideous end'/'DDE', '210141').

card_in_set('hornet', 'DDE').
card_original_type('hornet'/'DDE', 'Artifact Creature — Insect').
card_original_text('hornet'/'DDE', 'Flying, haste').
card_first_print('hornet', 'DDE').
card_image_name('hornet'/'DDE', 'hornet').
card_uid('hornet'/'DDE', 'DDE:Hornet:hornet').
card_rarity('hornet'/'DDE', 'Common').
card_artist('hornet'/'DDE', 'Ron Spencer').
card_number('hornet'/'DDE', 'T1').
card_multiverse_id('hornet'/'DDE', '209163').

card_in_set('hornet cannon', 'DDE').
card_original_type('hornet cannon'/'DDE', 'Artifact').
card_original_text('hornet cannon'/'DDE', '{3}, {T}: Put a 1/1 colorless Insect artifact creature token with flying and haste named Hornet onto the battlefield. Destroy it at the beginning of the next end step.').
card_image_name('hornet cannon'/'DDE', 'hornet cannon').
card_uid('hornet cannon'/'DDE', 'DDE:Hornet Cannon:hornet cannon').
card_rarity('hornet cannon'/'DDE', 'Uncommon').
card_artist('hornet cannon'/'DDE', 'Ron Spencer').
card_number('hornet cannon'/'DDE', '28').
card_multiverse_id('hornet cannon'/'DDE', '209146').

card_in_set('island', 'DDE').
card_original_type('island'/'DDE', 'Basic Land — Island').
card_original_text('island'/'DDE', 'U').
card_image_name('island'/'DDE', 'island').
card_uid('island'/'DDE', 'DDE:Island:island').
card_rarity('island'/'DDE', 'Basic Land').
card_artist('island'/'DDE', 'Terese Nielsen').
card_number('island'/'DDE', '68').
card_multiverse_id('island'/'DDE', '210511').

card_in_set('lightning greaves', 'DDE').
card_original_type('lightning greaves'/'DDE', 'Artifact — Equipment').
card_original_text('lightning greaves'/'DDE', 'Equipped creature has shroud and haste.\nEquip {0}').
card_image_name('lightning greaves'/'DDE', 'lightning greaves').
card_uid('lightning greaves'/'DDE', 'DDE:Lightning Greaves:lightning greaves').
card_rarity('lightning greaves'/'DDE', 'Uncommon').
card_artist('lightning greaves'/'DDE', 'Jeremy Jarvis').
card_number('lightning greaves'/'DDE', '19').
card_flavor_text('lightning greaves'/'DDE', 'After lightning struck the cliffs, the ore became iron, the iron became steel, and the steel became greaves. The lightning never left.').
card_multiverse_id('lightning greaves'/'DDE', '209141').

card_in_set('living death', 'DDE').
card_original_type('living death'/'DDE', 'Sorcery').
card_original_text('living death'/'DDE', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_image_name('living death'/'DDE', 'living death').
card_uid('living death'/'DDE', 'DDE:Living Death:living death').
card_rarity('living death'/'DDE', 'Rare').
card_artist('living death'/'DDE', 'Charles Gillespie').
card_number('living death'/'DDE', '31').
card_multiverse_id('living death'/'DDE', '210138').

card_in_set('minion', 'DDE').
card_original_type('minion'/'DDE', 'Creature — Minion').
card_original_text('minion'/'DDE', '').
card_first_print('minion', 'DDE').
card_image_name('minion'/'DDE', 'minion').
card_uid('minion'/'DDE', 'DDE:Minion:minion').
card_rarity('minion'/'DDE', 'Common').
card_artist('minion'/'DDE', 'Dave Kendall').
card_number('minion'/'DDE', 'T2').
card_multiverse_id('minion'/'DDE', '207998').

card_in_set('mountain', 'DDE').
card_original_type('mountain'/'DDE', 'Basic Land — Mountain').
card_original_text('mountain'/'DDE', 'R').
card_image_name('mountain'/'DDE', 'mountain').
card_uid('mountain'/'DDE', 'DDE:Mountain:mountain').
card_rarity('mountain'/'DDE', 'Basic Land').
card_artist('mountain'/'DDE', 'Scott Bailey').
card_number('mountain'/'DDE', '69').
card_multiverse_id('mountain'/'DDE', '210512').

card_in_set('narrow escape', 'DDE').
card_original_type('narrow escape'/'DDE', 'Instant').
card_original_text('narrow escape'/'DDE', 'Return target permanent you control to its owner\'s hand. You gain 4 life.').
card_image_name('narrow escape'/'DDE', 'narrow escape').
card_uid('narrow escape'/'DDE', 'DDE:Narrow Escape:narrow escape').
card_rarity('narrow escape'/'DDE', 'Common').
card_artist('narrow escape'/'DDE', 'Karl Kopinski').
card_number('narrow escape'/'DDE', '55').
card_flavor_text('narrow escape'/'DDE', 'He could find no strength left to budge, no power left to fight, no will left to live. Luckily, they could find him.').
card_multiverse_id('narrow escape'/'DDE', '210152').

card_in_set('nomadic elf', 'DDE').
card_original_type('nomadic elf'/'DDE', 'Creature — Elf Nomad').
card_original_text('nomadic elf'/'DDE', '{1}{G}: Add one mana of any color to your mana pool.').
card_image_name('nomadic elf'/'DDE', 'nomadic elf').
card_uid('nomadic elf'/'DDE', 'DDE:Nomadic Elf:nomadic elf').
card_rarity('nomadic elf'/'DDE', 'Common').
card_artist('nomadic elf'/'DDE', 'D. J. Cleland-Hura').
card_number('nomadic elf'/'DDE', '38').
card_flavor_text('nomadic elf'/'DDE', '\"I\'ve journeyed across Dominaria. Phyrexians are everywhere. Plague is everywhere. There\'s no place left to hide.\"').
card_multiverse_id('nomadic elf'/'DDE', '209115').

card_in_set('order of yawgmoth', 'DDE').
card_original_type('order of yawgmoth'/'DDE', 'Creature — Zombie Knight').
card_original_text('order of yawgmoth'/'DDE', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Order of Yawgmoth deals damage to a player, that player discards a card.').
card_image_name('order of yawgmoth'/'DDE', 'order of yawgmoth').
card_uid('order of yawgmoth'/'DDE', 'DDE:Order of Yawgmoth:order of yawgmoth').
card_rarity('order of yawgmoth'/'DDE', 'Uncommon').
card_artist('order of yawgmoth'/'DDE', 'Chippy').
card_number('order of yawgmoth'/'DDE', '11').
card_multiverse_id('order of yawgmoth'/'DDE', '209128').

card_in_set('phyrexian arena', 'DDE').
card_original_type('phyrexian arena'/'DDE', 'Enchantment').
card_original_text('phyrexian arena'/'DDE', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_image_name('phyrexian arena'/'DDE', 'phyrexian arena').
card_uid('phyrexian arena'/'DDE', 'DDE:Phyrexian Arena:phyrexian arena').
card_rarity('phyrexian arena'/'DDE', 'Rare').
card_artist('phyrexian arena'/'DDE', 'Pete Venters').
card_number('phyrexian arena'/'DDE', '27').
card_flavor_text('phyrexian arena'/'DDE', 'An audience of one with the malice of thousands.').
card_multiverse_id('phyrexian arena'/'DDE', '209132').

card_in_set('phyrexian battleflies', 'DDE').
card_original_type('phyrexian battleflies'/'DDE', 'Creature — Insect').
card_original_text('phyrexian battleflies'/'DDE', 'Flying\n{B}: Phyrexian Battleflies gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_image_name('phyrexian battleflies'/'DDE', 'phyrexian battleflies').
card_uid('phyrexian battleflies'/'DDE', 'DDE:Phyrexian Battleflies:phyrexian battleflies').
card_rarity('phyrexian battleflies'/'DDE', 'Common').
card_artist('phyrexian battleflies'/'DDE', 'Dan Frazier').
card_number('phyrexian battleflies'/'DDE', '3').
card_flavor_text('phyrexian battleflies'/'DDE', 'After encountering them, Squee finally lost his appetite for bugs.').
card_multiverse_id('phyrexian battleflies'/'DDE', '209116').

card_in_set('phyrexian broodlings', 'DDE').
card_original_type('phyrexian broodlings'/'DDE', 'Creature — Minion').
card_original_text('phyrexian broodlings'/'DDE', '{1}, Sacrifice a creature: Put a +1/+1 counter on Phyrexian Broodlings.').
card_image_name('phyrexian broodlings'/'DDE', 'phyrexian broodlings').
card_uid('phyrexian broodlings'/'DDE', 'DDE:Phyrexian Broodlings:phyrexian broodlings').
card_rarity('phyrexian broodlings'/'DDE', 'Common').
card_artist('phyrexian broodlings'/'DDE', 'Daren Bader').
card_number('phyrexian broodlings'/'DDE', '8').
card_flavor_text('phyrexian broodlings'/'DDE', 'With limited resources and near unlimited time, Kerrick used parts from one beast to build another.').
card_multiverse_id('phyrexian broodlings'/'DDE', '210134').

card_in_set('phyrexian colossus', 'DDE').
card_original_type('phyrexian colossus'/'DDE', 'Artifact Creature — Golem').
card_original_text('phyrexian colossus'/'DDE', 'Phyrexian Colossus doesn\'t untap during your untap step.\nPay 8 life: Untap Phyrexian Colossus.\nPhyrexian Colossus can\'t be blocked except by three or more creatures.').
card_image_name('phyrexian colossus'/'DDE', 'phyrexian colossus').
card_uid('phyrexian colossus'/'DDE', 'DDE:Phyrexian Colossus:phyrexian colossus').
card_rarity('phyrexian colossus'/'DDE', 'Rare').
card_artist('phyrexian colossus'/'DDE', 'Mark Tedin').
card_number('phyrexian colossus'/'DDE', '16').
card_multiverse_id('phyrexian colossus'/'DDE', '209160').

card_in_set('phyrexian debaser', 'DDE').
card_original_type('phyrexian debaser'/'DDE', 'Creature — Carrier').
card_original_text('phyrexian debaser'/'DDE', 'Flying\n{T}, Sacrifice Phyrexian Debaser: Target creature gets -2/-2 until end of turn.').
card_image_name('phyrexian debaser'/'DDE', 'phyrexian debaser').
card_uid('phyrexian debaser'/'DDE', 'DDE:Phyrexian Debaser:phyrexian debaser').
card_rarity('phyrexian debaser'/'DDE', 'Common').
card_artist('phyrexian debaser'/'DDE', 'Mark Tedin').
card_number('phyrexian debaser'/'DDE', '10').
card_flavor_text('phyrexian debaser'/'DDE', '\"The second stage of the illness: high fever and severe infectiousness.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian debaser'/'DDE', '209124').

card_in_set('phyrexian defiler', 'DDE').
card_original_type('phyrexian defiler'/'DDE', 'Creature — Carrier').
card_original_text('phyrexian defiler'/'DDE', '{T}, Sacrifice Phyrexian Defiler: Target creature gets -3/-3 until end of turn.').
card_image_name('phyrexian defiler'/'DDE', 'phyrexian defiler').
card_uid('phyrexian defiler'/'DDE', 'DDE:Phyrexian Defiler:phyrexian defiler').
card_rarity('phyrexian defiler'/'DDE', 'Uncommon').
card_artist('phyrexian defiler'/'DDE', 'DiTerlizzi').
card_number('phyrexian defiler'/'DDE', '12').
card_flavor_text('phyrexian defiler'/'DDE', '\"The third stage of the illness: muscle aches and persistent cough.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian defiler'/'DDE', '209125').

card_in_set('phyrexian denouncer', 'DDE').
card_original_type('phyrexian denouncer'/'DDE', 'Creature — Carrier').
card_original_text('phyrexian denouncer'/'DDE', '{T}, Sacrifice Phyrexian Denouncer: Target creature gets -1/-1 until end of turn.').
card_image_name('phyrexian denouncer'/'DDE', 'phyrexian denouncer').
card_uid('phyrexian denouncer'/'DDE', 'DDE:Phyrexian Denouncer:phyrexian denouncer').
card_rarity('phyrexian denouncer'/'DDE', 'Common').
card_artist('phyrexian denouncer'/'DDE', 'Brian Snõddy').
card_number('phyrexian denouncer'/'DDE', '4').
card_flavor_text('phyrexian denouncer'/'DDE', '\"The first stage of the illness: rash and nausea.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian denouncer'/'DDE', '209126').

card_in_set('phyrexian gargantua', 'DDE').
card_original_type('phyrexian gargantua'/'DDE', 'Creature — Horror').
card_original_text('phyrexian gargantua'/'DDE', 'When Phyrexian Gargantua enters the battlefield, you draw two cards and you lose 2 life.').
card_image_name('phyrexian gargantua'/'DDE', 'phyrexian gargantua').
card_uid('phyrexian gargantua'/'DDE', 'DDE:Phyrexian Gargantua:phyrexian gargantua').
card_rarity('phyrexian gargantua'/'DDE', 'Uncommon').
card_artist('phyrexian gargantua'/'DDE', 'Carl Critchlow').
card_number('phyrexian gargantua'/'DDE', '15').
card_flavor_text('phyrexian gargantua'/'DDE', 'Other Phyrexians have nightmares about the gargantua.').
card_multiverse_id('phyrexian gargantua'/'DDE', '210135').

card_in_set('phyrexian ghoul', 'DDE').
card_original_type('phyrexian ghoul'/'DDE', 'Creature — Zombie').
card_original_text('phyrexian ghoul'/'DDE', 'Sacrifice a creature: Phyrexian Ghoul gets +2/+2 until end of turn.').
card_image_name('phyrexian ghoul'/'DDE', 'phyrexian ghoul').
card_uid('phyrexian ghoul'/'DDE', 'DDE:Phyrexian Ghoul:phyrexian ghoul').
card_rarity('phyrexian ghoul'/'DDE', 'Common').
card_artist('phyrexian ghoul'/'DDE', 'Pete Venters').
card_number('phyrexian ghoul'/'DDE', '6').
card_flavor_text('phyrexian ghoul'/'DDE', 'Phyrexia wastes nothing. Its food chain is a spiraling cycle.').
card_multiverse_id('phyrexian ghoul'/'DDE', '209129').

card_in_set('phyrexian hulk', 'DDE').
card_original_type('phyrexian hulk'/'DDE', 'Artifact Creature — Golem').
card_original_text('phyrexian hulk'/'DDE', '').
card_image_name('phyrexian hulk'/'DDE', 'phyrexian hulk').
card_uid('phyrexian hulk'/'DDE', 'DDE:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'DDE', 'Uncommon').
card_artist('phyrexian hulk'/'DDE', 'Matthew D. Wilson').
card_number('phyrexian hulk'/'DDE', '14').
card_flavor_text('phyrexian hulk'/'DDE', 'It doesn\'t think. It doesn\'t feel.\nIt doesn\'t laugh or cry.\nAll it does from dusk till dawn\nIs make the soldiers die.\n—Onean children\'s rhyme').
card_multiverse_id('phyrexian hulk'/'DDE', '209133').

card_in_set('phyrexian negator', 'DDE').
card_original_type('phyrexian negator'/'DDE', 'Creature — Horror').
card_original_text('phyrexian negator'/'DDE', 'Trample\nWhenever Phyrexian Negator is dealt damage, sacrifice that many permanents.').
card_image_name('phyrexian negator'/'DDE', 'phyrexian negator').
card_uid('phyrexian negator'/'DDE', 'DDE:Phyrexian Negator:phyrexian negator').
card_rarity('phyrexian negator'/'DDE', 'Mythic Rare').
card_artist('phyrexian negator'/'DDE', 'Jim Murray').
card_number('phyrexian negator'/'DDE', '1').
card_flavor_text('phyrexian negator'/'DDE', 'They exist to cease.').
card_multiverse_id('phyrexian negator'/'DDE', '207891').

card_in_set('phyrexian plaguelord', 'DDE').
card_original_type('phyrexian plaguelord'/'DDE', 'Creature — Carrier').
card_original_text('phyrexian plaguelord'/'DDE', '{T}, Sacrifice Phyrexian Plaguelord: Target creature gets -4/-4 until end of turn.\nSacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_image_name('phyrexian plaguelord'/'DDE', 'phyrexian plaguelord').
card_uid('phyrexian plaguelord'/'DDE', 'DDE:Phyrexian Plaguelord:phyrexian plaguelord').
card_rarity('phyrexian plaguelord'/'DDE', 'Rare').
card_artist('phyrexian plaguelord'/'DDE', 'Kev Walker').
card_number('phyrexian plaguelord'/'DDE', '13').
card_flavor_text('phyrexian plaguelord'/'DDE', '\"The final stage of the illness: delirium, convulsions, and death.\"\n—Phyrexian progress notes').
card_multiverse_id('phyrexian plaguelord'/'DDE', '209127').

card_in_set('phyrexian processor', 'DDE').
card_original_type('phyrexian processor'/'DDE', 'Artifact').
card_original_text('phyrexian processor'/'DDE', 'As Phyrexian Processor enters the battlefield, pay any amount of life.\n{4}, {T}: Put an X/X black Minion creature token onto the battlefield, where X is the life paid as Phyrexian Processor entered the battlefield.').
card_image_name('phyrexian processor'/'DDE', 'phyrexian processor').
card_uid('phyrexian processor'/'DDE', 'DDE:Phyrexian Processor:phyrexian processor').
card_rarity('phyrexian processor'/'DDE', 'Rare').
card_artist('phyrexian processor'/'DDE', 'Dave Kendall').
card_number('phyrexian processor'/'DDE', '29').
card_multiverse_id('phyrexian processor'/'DDE', '207888').

card_in_set('phyrexian totem', 'DDE').
card_original_type('phyrexian totem'/'DDE', 'Artifact').
card_original_text('phyrexian totem'/'DDE', '{T}: Add {B} to your mana pool.\n{2}{B}: Phyrexian Totem becomes a 5/5 black Horror artifact creature with trample until end of turn.\nWhenever Phyrexian Totem is dealt damage, if it\'s a creature, sacrifice that many permanents.').
card_image_name('phyrexian totem'/'DDE', 'phyrexian totem').
card_uid('phyrexian totem'/'DDE', 'DDE:Phyrexian Totem:phyrexian totem').
card_rarity('phyrexian totem'/'DDE', 'Uncommon').
card_artist('phyrexian totem'/'DDE', 'John Zeleznik').
card_number('phyrexian totem'/'DDE', '20').
card_multiverse_id('phyrexian totem'/'DDE', '209142').

card_in_set('phyrexian vault', 'DDE').
card_original_type('phyrexian vault'/'DDE', 'Artifact').
card_original_text('phyrexian vault'/'DDE', '{2}, {T}, Sacrifice a creature: Draw a card.').
card_image_name('phyrexian vault'/'DDE', 'phyrexian vault').
card_uid('phyrexian vault'/'DDE', 'DDE:Phyrexian Vault:phyrexian vault').
card_rarity('phyrexian vault'/'DDE', 'Uncommon').
card_artist('phyrexian vault'/'DDE', 'Hannibal King').
card_number('phyrexian vault'/'DDE', '21').
card_flavor_text('phyrexian vault'/'DDE', '\"The secrets of Phyrexia are expensive. You will pay in brass and bone, steel and sinew.\"\n—Kaervek').
card_multiverse_id('phyrexian vault'/'DDE', '210136').

card_in_set('plains', 'DDE').
card_original_type('plains'/'DDE', 'Basic Land — Plains').
card_original_text('plains'/'DDE', 'W').
card_image_name('plains'/'DDE', 'plains').
card_uid('plains'/'DDE', 'DDE:Plains:plains').
card_rarity('plains'/'DDE', 'Basic Land').
card_artist('plains'/'DDE', 'D. J. Cleland-Hura').
card_number('plains'/'DDE', '67').
card_multiverse_id('plains'/'DDE', '210513').

card_in_set('power armor', 'DDE').
card_original_type('power armor'/'DDE', 'Artifact').
card_original_text('power armor'/'DDE', 'Domain — {3}, {T}: Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_image_name('power armor'/'DDE', 'power armor').
card_uid('power armor'/'DDE', 'DDE:Power Armor:power armor').
card_rarity('power armor'/'DDE', 'Uncommon').
card_artist('power armor'/'DDE', 'Doug Chaffee').
card_number('power armor'/'DDE', '62').
card_flavor_text('power armor'/'DDE', '\"Great peril demands formidable weaponry.\"\n—Urza').
card_multiverse_id('power armor'/'DDE', '209117').

card_in_set('priest of gix', 'DDE').
card_original_type('priest of gix'/'DDE', 'Creature — Human Cleric Minion').
card_original_text('priest of gix'/'DDE', 'When Priest of Gix enters the battlefield, add {B}{B}{B} to your mana pool.').
card_image_name('priest of gix'/'DDE', 'priest of gix').
card_uid('priest of gix'/'DDE', 'DDE:Priest of Gix:priest of gix').
card_rarity('priest of gix'/'DDE', 'Uncommon').
card_artist('priest of gix'/'DDE', 'Brian Despain').
card_number('priest of gix'/'DDE', '7').
card_flavor_text('priest of gix'/'DDE', '\"Gix doesn\'t want a people to rule but puppets to entertain his madness.\"\n—Xantcha, Phyrexian outcast').
card_multiverse_id('priest of gix'/'DDE', '209130').

card_in_set('puppet strings', 'DDE').
card_original_type('puppet strings'/'DDE', 'Artifact').
card_original_text('puppet strings'/'DDE', '{2}, {T}: You may tap or untap target creature.').
card_image_name('puppet strings'/'DDE', 'puppet strings').
card_uid('puppet strings'/'DDE', 'DDE:Puppet Strings:puppet strings').
card_rarity('puppet strings'/'DDE', 'Uncommon').
card_artist('puppet strings'/'DDE', 'Scott Kirschner').
card_number('puppet strings'/'DDE', '22').
card_flavor_text('puppet strings'/'DDE', '\"Have no illusions,\" Volrath warned Greven, \"about free will.\"').
card_multiverse_id('puppet strings'/'DDE', '209139').

card_in_set('quirion elves', 'DDE').
card_original_type('quirion elves'/'DDE', 'Creature — Elf Druid').
card_original_text('quirion elves'/'DDE', 'As Quirion Elves enters the battlefield, choose a color.\n{T}: Add {G} to your mana pool.\n{T}: Add one mana of the chosen color to your mana pool.').
card_image_name('quirion elves'/'DDE', 'quirion elves').
card_uid('quirion elves'/'DDE', 'DDE:Quirion Elves:quirion elves').
card_rarity('quirion elves'/'DDE', 'Common').
card_artist('quirion elves'/'DDE', 'Douglas Shuler').
card_number('quirion elves'/'DDE', '39').
card_multiverse_id('quirion elves'/'DDE', '210557').

card_in_set('rith\'s charm', 'DDE').
card_original_type('rith\'s charm'/'DDE', 'Instant').
card_original_text('rith\'s charm'/'DDE', 'Choose one — Destroy target nonbasic land; or put three 1/1 green Saproling creature tokens onto the battlefield; or prevent all damage a source of your choice would deal this turn.').
card_image_name('rith\'s charm'/'DDE', 'rith\'s charm').
card_uid('rith\'s charm'/'DDE', 'DDE:Rith\'s Charm:rith\'s charm').
card_rarity('rith\'s charm'/'DDE', 'Uncommon').
card_artist('rith\'s charm'/'DDE', 'David Martin').
card_number('rith\'s charm'/'DDE', '60').
card_multiverse_id('rith\'s charm'/'DDE', '209152').

card_in_set('rith, the awakener', 'DDE').
card_original_type('rith, the awakener'/'DDE', 'Legendary Creature — Dragon').
card_original_text('rith, the awakener'/'DDE', 'Flying\nWhenever Rith, the Awakener deals combat damage to a player, you may pay {2}{G}. If you do, choose a color, then put a 1/1 green Saproling creature token onto the battlefield for each permanent of that color.').
card_image_name('rith, the awakener'/'DDE', 'rith, the awakener').
card_uid('rith, the awakener'/'DDE', 'DDE:Rith, the Awakener:rith, the awakener').
card_rarity('rith, the awakener'/'DDE', 'Rare').
card_artist('rith, the awakener'/'DDE', 'Carl Critchlow').
card_number('rith, the awakener'/'DDE', '48').
card_multiverse_id('rith, the awakener'/'DDE', '209118').

card_in_set('sanguine guard', 'DDE').
card_original_type('sanguine guard'/'DDE', 'Creature — Zombie Knight').
card_original_text('sanguine guard'/'DDE', 'First strike\n{1}{B}: Regenerate Sanguine Guard.').
card_image_name('sanguine guard'/'DDE', 'sanguine guard').
card_uid('sanguine guard'/'DDE', 'DDE:Sanguine Guard:sanguine guard').
card_rarity('sanguine guard'/'DDE', 'Uncommon').
card_artist('sanguine guard'/'DDE', 'Kev Walker').
card_number('sanguine guard'/'DDE', '9').
card_flavor_text('sanguine guard'/'DDE', '\"Father of Machines! Your filigree gaze carves us, and the scars dance upon our grateful flesh.\"\n—Phyrexian Scriptures').
card_multiverse_id('sanguine guard'/'DDE', '209161').

card_in_set('saproling', 'DDE').
card_original_type('saproling'/'DDE', 'Creature — Saproling').
card_original_text('saproling'/'DDE', '').
card_first_print('saproling', 'DDE').
card_image_name('saproling'/'DDE', 'saproling').
card_uid('saproling'/'DDE', 'DDE:Saproling:saproling').
card_rarity('saproling'/'DDE', 'Common').
card_artist('saproling'/'DDE', 'Warren Mahy').
card_number('saproling'/'DDE', 'T3').
card_multiverse_id('saproling'/'DDE', '209162').

card_in_set('shivan oasis', 'DDE').
card_original_type('shivan oasis'/'DDE', 'Land').
card_original_text('shivan oasis'/'DDE', 'Shivan Oasis enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('shivan oasis'/'DDE', 'shivan oasis').
card_uid('shivan oasis'/'DDE', 'DDE:Shivan Oasis:shivan oasis').
card_rarity('shivan oasis'/'DDE', 'Uncommon').
card_artist('shivan oasis'/'DDE', 'Rob Alexander').
card_number('shivan oasis'/'DDE', '65').
card_flavor_text('shivan oasis'/'DDE', 'Only the hardiest explorers survive to eat the fruit.').
card_multiverse_id('shivan oasis'/'DDE', '209119').

card_in_set('slay', 'DDE').
card_original_type('slay'/'DDE', 'Instant').
card_original_text('slay'/'DDE', 'Destroy target green creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('slay'/'DDE', 'slay').
card_uid('slay'/'DDE', 'DDE:Slay:slay').
card_rarity('slay'/'DDE', 'Uncommon').
card_artist('slay'/'DDE', 'Ben Thompson').
card_number('slay'/'DDE', '25').
card_flavor_text('slay'/'DDE', 'The elves had the edge in guile, skill, and valor. But in the end, only sheer numbers mattered.').
card_multiverse_id('slay'/'DDE', '209134').

card_in_set('sunscape battlemage', 'DDE').
card_original_type('sunscape battlemage'/'DDE', 'Creature — Human Wizard').
card_original_text('sunscape battlemage'/'DDE', 'Kicker {1}{G} and/or {2}{U} (You may pay an additional {1}{G} and/or {2}{U} as you cast this spell.)\nWhen Sunscape Battlemage enters the battlefield, if it was kicked with its {1}{G} kicker, destroy target creature with flying.\nWhen Sunscape Battlemage enters the battlefield, if it was kicked with its {2}{U} kicker, draw two cards.').
card_image_name('sunscape battlemage'/'DDE', 'sunscape battlemage').
card_uid('sunscape battlemage'/'DDE', 'DDE:Sunscape Battlemage:sunscape battlemage').
card_rarity('sunscape battlemage'/'DDE', 'Uncommon').
card_artist('sunscape battlemage'/'DDE', 'Tony Szczudlo').
card_number('sunscape battlemage'/'DDE', '40').
card_multiverse_id('sunscape battlemage'/'DDE', '209153').

card_in_set('swamp', 'DDE').
card_original_type('swamp'/'DDE', 'Basic Land — Swamp').
card_original_text('swamp'/'DDE', 'B').
card_image_name('swamp'/'DDE', 'swamp1').
card_uid('swamp'/'DDE', 'DDE:Swamp:swamp1').
card_rarity('swamp'/'DDE', 'Basic Land').
card_artist('swamp'/'DDE', 'Rob Alexander').
card_number('swamp'/'DDE', '32').
card_multiverse_id('swamp'/'DDE', '210507').

card_in_set('swamp', 'DDE').
card_original_type('swamp'/'DDE', 'Basic Land — Swamp').
card_original_text('swamp'/'DDE', 'B').
card_image_name('swamp'/'DDE', 'swamp2').
card_uid('swamp'/'DDE', 'DDE:Swamp:swamp2').
card_rarity('swamp'/'DDE', 'Basic Land').
card_artist('swamp'/'DDE', 'Rob Alexander').
card_number('swamp'/'DDE', '33').
card_multiverse_id('swamp'/'DDE', '210506').

card_in_set('swamp', 'DDE').
card_original_type('swamp'/'DDE', 'Basic Land — Swamp').
card_original_text('swamp'/'DDE', 'B').
card_image_name('swamp'/'DDE', 'swamp3').
card_uid('swamp'/'DDE', 'DDE:Swamp:swamp3').
card_rarity('swamp'/'DDE', 'Basic Land').
card_artist('swamp'/'DDE', 'Ron Spencer').
card_number('swamp'/'DDE', '34').
card_multiverse_id('swamp'/'DDE', '210505').

card_in_set('swamp', 'DDE').
card_original_type('swamp'/'DDE', 'Basic Land — Swamp').
card_original_text('swamp'/'DDE', 'B').
card_image_name('swamp'/'DDE', 'swamp4').
card_uid('swamp'/'DDE', 'DDE:Swamp:swamp4').
card_rarity('swamp'/'DDE', 'Basic Land').
card_artist('swamp'/'DDE', 'Ron Spencer').
card_number('swamp'/'DDE', '35').
card_multiverse_id('swamp'/'DDE', '210508').

card_in_set('tendrils of corruption', 'DDE').
card_original_type('tendrils of corruption'/'DDE', 'Instant').
card_original_text('tendrils of corruption'/'DDE', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_image_name('tendrils of corruption'/'DDE', 'tendrils of corruption').
card_uid('tendrils of corruption'/'DDE', 'DDE:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'DDE', 'Common').
card_artist('tendrils of corruption'/'DDE', 'Vance Kovacs').
card_number('tendrils of corruption'/'DDE', '30').
card_flavor_text('tendrils of corruption'/'DDE', 'Darkness doesn\'t always send its minions to do its dirty work.').
card_multiverse_id('tendrils of corruption'/'DDE', '210139').

card_in_set('terramorphic expanse', 'DDE').
card_original_type('terramorphic expanse'/'DDE', 'Land').
card_original_text('terramorphic expanse'/'DDE', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'DDE', 'terramorphic expanse').
card_uid('terramorphic expanse'/'DDE', 'DDE:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'DDE', 'Common').
card_artist('terramorphic expanse'/'DDE', 'Dan Scott').
card_number('terramorphic expanse'/'DDE', '66').
card_multiverse_id('terramorphic expanse'/'DDE', '209159').

card_in_set('thornscape apprentice', 'DDE').
card_original_type('thornscape apprentice'/'DDE', 'Creature — Human Wizard').
card_original_text('thornscape apprentice'/'DDE', '{W}, {T}: Tap target creature.\n{R}, {T}: Target creature gains first strike until end of turn.').
card_image_name('thornscape apprentice'/'DDE', 'thornscape apprentice').
card_uid('thornscape apprentice'/'DDE', 'DDE:Thornscape Apprentice:thornscape apprentice').
card_rarity('thornscape apprentice'/'DDE', 'Common').
card_artist('thornscape apprentice'/'DDE', 'Randy Gallegos').
card_number('thornscape apprentice'/'DDE', '37').
card_multiverse_id('thornscape apprentice'/'DDE', '209120').

card_in_set('thornscape battlemage', 'DDE').
card_original_type('thornscape battlemage'/'DDE', 'Creature — Elf Wizard').
card_original_text('thornscape battlemage'/'DDE', 'Kicker {R} and/or {W}(You may pay an additional {R} and/or {W} as you cast this spell.)\nWhen Thornscape Battlemage enters the battlefield, if it was kicked with its {R} kicker, it deals 2 damage to target creature or player.\nWhen Thornscape Battlemage enters the battlefield, if it was kicked with its {W} kicker, destroy target artifact.').
card_image_name('thornscape battlemage'/'DDE', 'thornscape battlemage').
card_uid('thornscape battlemage'/'DDE', 'DDE:Thornscape Battlemage:thornscape battlemage').
card_rarity('thornscape battlemage'/'DDE', 'Uncommon').
card_artist('thornscape battlemage'/'DDE', 'Matt Cavotta').
card_number('thornscape battlemage'/'DDE', '42').
card_multiverse_id('thornscape battlemage'/'DDE', '209144').

card_in_set('thunderscape battlemage', 'DDE').
card_original_type('thunderscape battlemage'/'DDE', 'Creature — Human Wizard').
card_original_text('thunderscape battlemage'/'DDE', 'Kicker {1}{B} and/or {G} (You may pay an additional {1}{B} and/or {G} as you cast this spell.)\nWhen Thunderscape Battlemage enters the battlefield, if it was kicked with its {1}{B} kicker, target player discards two cards.\nWhen Thunderscape Battlemage enters the battlefield, if it was kicked with its {G} kicker, destroy target enchantment.').
card_image_name('thunderscape battlemage'/'DDE', 'thunderscape battlemage').
card_uid('thunderscape battlemage'/'DDE', 'DDE:Thunderscape Battlemage:thunderscape battlemage').
card_rarity('thunderscape battlemage'/'DDE', 'Uncommon').
card_artist('thunderscape battlemage'/'DDE', 'Mike Ploog').
card_number('thunderscape battlemage'/'DDE', '41').
card_multiverse_id('thunderscape battlemage'/'DDE', '209155').

card_in_set('treva\'s charm', 'DDE').
card_original_type('treva\'s charm'/'DDE', 'Instant').
card_original_text('treva\'s charm'/'DDE', 'Choose one — Destroy target enchantment; or exile target attacking creature; or draw a card, then discard a card.').
card_image_name('treva\'s charm'/'DDE', 'treva\'s charm').
card_uid('treva\'s charm'/'DDE', 'DDE:Treva\'s Charm:treva\'s charm').
card_rarity('treva\'s charm'/'DDE', 'Uncommon').
card_artist('treva\'s charm'/'DDE', 'David Martin').
card_number('treva\'s charm'/'DDE', '61').
card_multiverse_id('treva\'s charm'/'DDE', '209156').

card_in_set('treva, the renewer', 'DDE').
card_original_type('treva, the renewer'/'DDE', 'Legendary Creature — Dragon').
card_original_text('treva, the renewer'/'DDE', 'Flying\nWhenever Treva, the Renewer deals combat damage to a player, you may pay {2}{W}. If you do, choose a color, then you gain 1 life for each permanent of that color.').
card_image_name('treva, the renewer'/'DDE', 'treva, the renewer').
card_uid('treva, the renewer'/'DDE', 'DDE:Treva, the Renewer:treva, the renewer').
card_rarity('treva, the renewer'/'DDE', 'Rare').
card_artist('treva, the renewer'/'DDE', 'Ciruelo').
card_number('treva, the renewer'/'DDE', '49').
card_multiverse_id('treva, the renewer'/'DDE', '209121').

card_in_set('tribal flames', 'DDE').
card_original_type('tribal flames'/'DDE', 'Sorcery').
card_original_text('tribal flames'/'DDE', 'Domain — Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_image_name('tribal flames'/'DDE', 'tribal flames').
card_uid('tribal flames'/'DDE', 'DDE:Tribal Flames:tribal flames').
card_rarity('tribal flames'/'DDE', 'Common').
card_artist('tribal flames'/'DDE', 'Tony Szczudlo').
card_number('tribal flames'/'DDE', '51').
card_flavor_text('tribal flames'/'DDE', '\"Fire is the universal language.\"\n—Jhoira, master artificer').
card_multiverse_id('tribal flames'/'DDE', '209145').

card_in_set('urza\'s rage', 'DDE').
card_original_type('urza\'s rage'/'DDE', 'Instant').
card_original_text('urza\'s rage'/'DDE', 'Kicker {8}{R} (You may pay an additional {8}{R} as you cast this spell.)\nUrza\'s Rage can\'t be countered by spells or abilities.\nUrza\'s Rage deals 3 damage to target creature or player. If Urza\'s Rage was kicked, instead Urza\'s Rage deals 10 damage to that creature or player and the damage can\'t be prevented.').
card_image_name('urza\'s rage'/'DDE', 'urza\'s rage').
card_uid('urza\'s rage'/'DDE', 'DDE:Urza\'s Rage:urza\'s rage').
card_rarity('urza\'s rage'/'DDE', 'Mythic Rare').
card_artist('urza\'s rage'/'DDE', 'Jim Murray').
card_number('urza\'s rage'/'DDE', '36').
card_multiverse_id('urza\'s rage'/'DDE', '207886').

card_in_set('verduran emissary', 'DDE').
card_original_type('verduran emissary'/'DDE', 'Creature — Human Wizard').
card_original_text('verduran emissary'/'DDE', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nWhen Verduran Emissary enters the battlefield, if it was kicked, destroy target artifact. It can\'t be regenerated.').
card_image_name('verduran emissary'/'DDE', 'verduran emissary').
card_uid('verduran emissary'/'DDE', 'DDE:Verduran Emissary:verduran emissary').
card_rarity('verduran emissary'/'DDE', 'Uncommon').
card_artist('verduran emissary'/'DDE', 'Alton Lawson').
card_number('verduran emissary'/'DDE', '43').
card_multiverse_id('verduran emissary'/'DDE', '210558').

card_in_set('voltaic key', 'DDE').
card_original_type('voltaic key'/'DDE', 'Artifact').
card_original_text('voltaic key'/'DDE', '{1}, {T}: Untap target artifact.').
card_image_name('voltaic key'/'DDE', 'voltaic key').
card_uid('voltaic key'/'DDE', 'DDE:Voltaic Key:voltaic key').
card_rarity('voltaic key'/'DDE', 'Uncommon').
card_artist('voltaic key'/'DDE', 'Franz Vohwinkel').
card_number('voltaic key'/'DDE', '17').
card_flavor_text('voltaic key'/'DDE', 'The key did not work on a single lock, yet it opened many doors.').
card_multiverse_id('voltaic key'/'DDE', '207889').

card_in_set('whispersilk cloak', 'DDE').
card_original_type('whispersilk cloak'/'DDE', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'DDE', 'Equipped creature is unblockable and has shroud.\nEquip {2}').
card_image_name('whispersilk cloak'/'DDE', 'whispersilk cloak').
card_uid('whispersilk cloak'/'DDE', 'DDE:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'DDE', 'Uncommon').
card_artist('whispersilk cloak'/'DDE', 'Daren Bader').
card_number('whispersilk cloak'/'DDE', '23').
card_flavor_text('whispersilk cloak'/'DDE', 'Such cloaks are in high demand both by assassins and by those who fear them.').
card_multiverse_id('whispersilk cloak'/'DDE', '210140').

card_in_set('worn powerstone', 'DDE').
card_original_type('worn powerstone'/'DDE', 'Artifact').
card_original_text('worn powerstone'/'DDE', 'Worn Powerstone enters the battlefield tapped.\n{T}: Add {2} to your mana pool.').
card_image_name('worn powerstone'/'DDE', 'worn powerstone').
card_uid('worn powerstone'/'DDE', 'DDE:Worn Powerstone:worn powerstone').
card_rarity('worn powerstone'/'DDE', 'Uncommon').
card_artist('worn powerstone'/'DDE', 'Henry G. Higgenbotham').
card_number('worn powerstone'/'DDE', '24').
card_flavor_text('worn powerstone'/'DDE', 'Even a fragment of a powerstone contains within it not just energy, but space—vast dimensions trapped in fragile crystal.').
card_multiverse_id('worn powerstone'/'DDE', '210137').

card_in_set('yavimaya elder', 'DDE').
card_original_type('yavimaya elder'/'DDE', 'Creature — Human Druid').
card_original_text('yavimaya elder'/'DDE', 'When Yavimaya Elder is put into a graveyard from the battlefield, you may search your library for up to two basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.\n{2}, Sacrifice Yavimaya Elder: Draw a card.').
card_image_name('yavimaya elder'/'DDE', 'yavimaya elder').
card_uid('yavimaya elder'/'DDE', 'DDE:Yavimaya Elder:yavimaya elder').
card_rarity('yavimaya elder'/'DDE', 'Common').
card_artist('yavimaya elder'/'DDE', 'Matt Cavotta').
card_number('yavimaya elder'/'DDE', '44').
card_multiverse_id('yavimaya elder'/'DDE', '207887').
