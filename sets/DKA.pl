% Dark Ascension

set('DKA').
set_name('DKA', 'Dark Ascension').
set_release_date('DKA', '2012-02-03').
set_border('DKA', 'black').
set_type('DKA', 'expansion').
set_block('DKA', 'Innistrad').

card_in_set('afflicted deserter', 'DKA').
card_original_type('afflicted deserter'/'DKA', 'Creature — Human Werewolf').
card_original_text('afflicted deserter'/'DKA', 'At the beginning of each upkeep, if no spells were cast last turn, transform Afflicted Deserter.').
card_first_print('afflicted deserter', 'DKA').
card_image_name('afflicted deserter'/'DKA', 'afflicted deserter').
card_uid('afflicted deserter'/'DKA', 'DKA:Afflicted Deserter:afflicted deserter').
card_rarity('afflicted deserter'/'DKA', 'Uncommon').
card_artist('afflicted deserter'/'DKA', 'David Palumbo').
card_number('afflicted deserter'/'DKA', '81a').
card_flavor_text('afflicted deserter'/'DKA', 'The rising of the first full moon eliminated any doubt as to the horrible truth lurking within.').
card_multiverse_id('afflicted deserter'/'DKA', '262675').

card_in_set('alpha brawl', 'DKA').
card_original_type('alpha brawl'/'DKA', 'Sorcery').
card_original_text('alpha brawl'/'DKA', 'Target creature an opponent controls deals damage equal to its power to each other creature that player controls, then each of those creatures deals damage equal to its power to that creature.').
card_first_print('alpha brawl', 'DKA').
card_image_name('alpha brawl'/'DKA', 'alpha brawl').
card_uid('alpha brawl'/'DKA', 'DKA:Alpha Brawl:alpha brawl').
card_rarity('alpha brawl'/'DKA', 'Rare').
card_artist('alpha brawl'/'DKA', 'Randy Gallegos').
card_number('alpha brawl'/'DKA', '82').
card_flavor_text('alpha brawl'/'DKA', 'Being an alpha means proving it every full moon.').
card_multiverse_id('alpha brawl'/'DKA', '262657').

card_in_set('altar of the lost', 'DKA').
card_original_type('altar of the lost'/'DKA', 'Artifact').
card_original_text('altar of the lost'/'DKA', 'Altar of the Lost enters the battlefield tapped.\n{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to cast spells with flashback from a graveyard.').
card_first_print('altar of the lost', 'DKA').
card_image_name('altar of the lost'/'DKA', 'altar of the lost').
card_uid('altar of the lost'/'DKA', 'DKA:Altar of the Lost:altar of the lost').
card_rarity('altar of the lost'/'DKA', 'Uncommon').
card_artist('altar of the lost'/'DKA', 'Daarken').
card_number('altar of the lost'/'DKA', '144').
card_multiverse_id('altar of the lost'/'DKA', '262846').

card_in_set('archangel\'s light', 'DKA').
card_original_type('archangel\'s light'/'DKA', 'Sorcery').
card_original_text('archangel\'s light'/'DKA', 'You gain 2 life for each card in your graveyard, then shuffle your graveyard into your library.').
card_first_print('archangel\'s light', 'DKA').
card_image_name('archangel\'s light'/'DKA', 'archangel\'s light').
card_uid('archangel\'s light'/'DKA', 'DKA:Archangel\'s Light:archangel\'s light').
card_rarity('archangel\'s light'/'DKA', 'Mythic Rare').
card_artist('archangel\'s light'/'DKA', 'Volkan Baga').
card_number('archangel\'s light'/'DKA', '1').
card_flavor_text('archangel\'s light'/'DKA', '\"This is the light of Avacyn. Even in her absence she offers us hope.\"\n—Radulf, priest of Avacyn').
card_multiverse_id('archangel\'s light'/'DKA', '244691').

card_in_set('archdemon of greed', 'DKA').
card_original_type('archdemon of greed'/'DKA', 'Creature — Demon').
card_original_text('archdemon of greed'/'DKA', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a Human. If you can\'t, tap Archdemon of Greed and it deals 9 damage to you.').
card_image_name('archdemon of greed'/'DKA', 'archdemon of greed').
card_uid('archdemon of greed'/'DKA', 'DKA:Archdemon of Greed:archdemon of greed').
card_rarity('archdemon of greed'/'DKA', 'Rare').
card_artist('archdemon of greed'/'DKA', 'Igor Kieryluk').
card_number('archdemon of greed'/'DKA', '71b').
card_flavor_text('archdemon of greed'/'DKA', '\"The price is paid. The power is mine!\"').
card_multiverse_id('archdemon of greed'/'DKA', '227405').

card_in_set('artful dodge', 'DKA').
card_original_type('artful dodge'/'DKA', 'Sorcery').
card_original_text('artful dodge'/'DKA', 'Target creature is unblockable this turn.\nFlashback {U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('artful dodge', 'DKA').
card_image_name('artful dodge'/'DKA', 'artful dodge').
card_uid('artful dodge'/'DKA', 'DKA:Artful Dodge:artful dodge').
card_rarity('artful dodge'/'DKA', 'Common').
card_artist('artful dodge'/'DKA', 'Tomasz Jedruszek').
card_number('artful dodge'/'DKA', '27').
card_flavor_text('artful dodge'/'DKA', 'Those who know the alleys and sewers of the Erdwal can disappear like smoke.').
card_multiverse_id('artful dodge'/'DKA', '262840').

card_in_set('avacyn\'s collar', 'DKA').
card_original_type('avacyn\'s collar'/'DKA', 'Artifact — Equipment').
card_original_text('avacyn\'s collar'/'DKA', 'Equipped creature gets +1/+0 and has vigilance.\nWhenever equipped creature dies, if it was a Human, put a 1/1 white Spirit creature token with flying onto the battlefield.\nEquip {2}').
card_first_print('avacyn\'s collar', 'DKA').
card_image_name('avacyn\'s collar'/'DKA', 'avacyn\'s collar').
card_uid('avacyn\'s collar'/'DKA', 'DKA:Avacyn\'s Collar:avacyn\'s collar').
card_rarity('avacyn\'s collar'/'DKA', 'Uncommon').
card_artist('avacyn\'s collar'/'DKA', 'James Paick').
card_number('avacyn\'s collar'/'DKA', '145').
card_multiverse_id('avacyn\'s collar'/'DKA', '262863').

card_in_set('bar the door', 'DKA').
card_original_type('bar the door'/'DKA', 'Instant').
card_original_text('bar the door'/'DKA', 'Creatures you control get +0/+4 until end of turn.').
card_first_print('bar the door', 'DKA').
card_image_name('bar the door'/'DKA', 'bar the door').
card_uid('bar the door'/'DKA', 'DKA:Bar the Door:bar the door').
card_rarity('bar the door'/'DKA', 'Common').
card_artist('bar the door'/'DKA', 'Ryan Pancoast').
card_number('bar the door'/'DKA', '2').
card_flavor_text('bar the door'/'DKA', '\"It may not look like much, but it\'s a good old door with strong wards. It\'ll hold.\"\n—Olgard of the Skiltfolk').
card_multiverse_id('bar the door'/'DKA', '262858').

card_in_set('beguiler of wills', 'DKA').
card_original_type('beguiler of wills'/'DKA', 'Creature — Human Wizard').
card_original_text('beguiler of wills'/'DKA', '{T}: Gain control of target creature with power less than or equal to the number of creatures you control.').
card_first_print('beguiler of wills', 'DKA').
card_image_name('beguiler of wills'/'DKA', 'beguiler of wills').
card_uid('beguiler of wills'/'DKA', 'DKA:Beguiler of Wills:beguiler of wills').
card_rarity('beguiler of wills'/'DKA', 'Mythic Rare').
card_artist('beguiler of wills'/'DKA', 'Eric Deschamps').
card_number('beguiler of wills'/'DKA', '28').
card_flavor_text('beguiler of wills'/'DKA', '\"Come, let me free you of the tyranny of thought.\"').
card_multiverse_id('beguiler of wills'/'DKA', '227080').

card_in_set('black cat', 'DKA').
card_original_type('black cat'/'DKA', 'Creature — Zombie Cat').
card_original_text('black cat'/'DKA', 'When Black Cat dies, target opponent discards a card at random.').
card_first_print('black cat', 'DKA').
card_image_name('black cat'/'DKA', 'black cat').
card_uid('black cat'/'DKA', 'DKA:Black Cat:black cat').
card_rarity('black cat'/'DKA', 'Common').
card_artist('black cat'/'DKA', 'David Palumbo').
card_number('black cat'/'DKA', '54').
card_flavor_text('black cat'/'DKA', 'Its last life is spent tormenting your dreams.').
card_multiverse_id('black cat'/'DKA', '262862').

card_in_set('blood feud', 'DKA').
card_original_type('blood feud'/'DKA', 'Sorcery').
card_original_text('blood feud'/'DKA', 'Target creature fights another target creature. (Each deals damage equal to its power to the other.)').
card_first_print('blood feud', 'DKA').
card_image_name('blood feud'/'DKA', 'blood feud').
card_uid('blood feud'/'DKA', 'DKA:Blood Feud:blood feud').
card_rarity('blood feud'/'DKA', 'Uncommon').
card_artist('blood feud'/'DKA', 'Winona Nelson').
card_number('blood feud'/'DKA', '83').
card_flavor_text('blood feud'/'DKA', 'Succession is a matter of blood, and by blood it is often decided.').
card_multiverse_id('blood feud'/'DKA', '244731').

card_in_set('bone to ash', 'DKA').
card_original_type('bone to ash'/'DKA', 'Instant').
card_original_text('bone to ash'/'DKA', 'Counter target creature spell.\nDraw a card.').
card_first_print('bone to ash', 'DKA').
card_image_name('bone to ash'/'DKA', 'bone to ash').
card_uid('bone to ash'/'DKA', 'DKA:Bone to Ash:bone to ash').
card_rarity('bone to ash'/'DKA', 'Common').
card_artist('bone to ash'/'DKA', 'Clint Cearley').
card_number('bone to ash'/'DKA', '29').
card_flavor_text('bone to ash'/'DKA', '\"I can think of worse ways to go. On second thought, maybe not.\"\n—Ludevic, necro-alchemist').
card_multiverse_id('bone to ash'/'DKA', '262845').

card_in_set('break of day', 'DKA').
card_original_type('break of day'/'DKA', 'Instant').
card_original_text('break of day'/'DKA', 'Creatures you control get +1/+1 until end of turn.\nFateful hour — If you have 5 or less life, those creatures also are indestructible this turn. (Lethal damage and effects that say \"destroy\" don\'t destroy them.)').
card_first_print('break of day', 'DKA').
card_image_name('break of day'/'DKA', 'break of day').
card_uid('break of day'/'DKA', 'DKA:Break of Day:break of day').
card_rarity('break of day'/'DKA', 'Common').
card_artist('break of day'/'DKA', 'Karl Kopinski').
card_number('break of day'/'DKA', '3').
card_multiverse_id('break of day'/'DKA', '262702').

card_in_set('briarpack alpha', 'DKA').
card_original_type('briarpack alpha'/'DKA', 'Creature — Wolf').
card_original_text('briarpack alpha'/'DKA', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Briarpack Alpha enters the battlefield, target creature gets +2/+2 until end of turn.').
card_first_print('briarpack alpha', 'DKA').
card_image_name('briarpack alpha'/'DKA', 'briarpack alpha').
card_uid('briarpack alpha'/'DKA', 'DKA:Briarpack Alpha:briarpack alpha').
card_rarity('briarpack alpha'/'DKA', 'Uncommon').
card_artist('briarpack alpha'/'DKA', 'Daarken').
card_number('briarpack alpha'/'DKA', '108').
card_flavor_text('briarpack alpha'/'DKA', '\"The wolves turned on us and a chill swept over me. The pack had a new leader.\"\n—Alena, trapper of Kessig').
card_multiverse_id('briarpack alpha'/'DKA', '262664').

card_in_set('burden of guilt', 'DKA').
card_original_type('burden of guilt'/'DKA', 'Enchantment — Aura').
card_original_text('burden of guilt'/'DKA', 'Enchant creature\n{1}: Tap enchanted creature.').
card_first_print('burden of guilt', 'DKA').
card_image_name('burden of guilt'/'DKA', 'burden of guilt').
card_uid('burden of guilt'/'DKA', 'DKA:Burden of Guilt:burden of guilt').
card_rarity('burden of guilt'/'DKA', 'Common').
card_artist('burden of guilt'/'DKA', 'John Stanko').
card_number('burden of guilt'/'DKA', '4').
card_flavor_text('burden of guilt'/'DKA', '\"Grab an axe and defend the gate! Your despair is an extravagance we can ill afford.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('burden of guilt'/'DKA', '262873').

card_in_set('burning oil', 'DKA').
card_original_type('burning oil'/'DKA', 'Instant').
card_original_text('burning oil'/'DKA', 'Burning Oil deals 3 damage to target attacking or blocking creature.\nFlashback {3}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('burning oil', 'DKA').
card_image_name('burning oil'/'DKA', 'burning oil').
card_uid('burning oil'/'DKA', 'DKA:Burning Oil:burning oil').
card_rarity('burning oil'/'DKA', 'Uncommon').
card_artist('burning oil'/'DKA', 'Trevor Claxton').
card_number('burning oil'/'DKA', '84').
card_flavor_text('burning oil'/'DKA', 'Every now and then, a devil\'s prank can give you a good idea.').
card_multiverse_id('burning oil'/'DKA', '262705').

card_in_set('call to the kindred', 'DKA').
card_original_type('call to the kindred'/'DKA', 'Enchantment — Aura').
card_original_text('call to the kindred'/'DKA', 'Enchant creature\nAt the beginning of your upkeep, you may look at the top five cards of your library. If you do, you may put a creature card that shares a creature type with enchanted creature from among them onto the battlefield, then you put the rest of those cards on the bottom of your library in any order.').
card_first_print('call to the kindred', 'DKA').
card_image_name('call to the kindred'/'DKA', 'call to the kindred').
card_uid('call to the kindred'/'DKA', 'DKA:Call to the Kindred:call to the kindred').
card_rarity('call to the kindred'/'DKA', 'Rare').
card_artist('call to the kindred'/'DKA', 'Jason A. Engle').
card_number('call to the kindred'/'DKA', '30').
card_multiverse_id('call to the kindred'/'DKA', '262706').

card_in_set('chalice of death', 'DKA').
card_original_type('chalice of death'/'DKA', 'Artifact').
card_original_text('chalice of death'/'DKA', '{T}: Target player loses 5 life.').
card_first_print('chalice of death', 'DKA').
card_image_name('chalice of death'/'DKA', 'chalice of death').
card_uid('chalice of death'/'DKA', 'DKA:Chalice of Death:chalice of death').
card_rarity('chalice of death'/'DKA', 'Uncommon').
card_artist('chalice of death'/'DKA', 'Ryan Yee').
card_number('chalice of death'/'DKA', '146b').
card_flavor_text('chalice of death'/'DKA', 'The bitter taste of life\'s only certainty.').
card_multiverse_id('chalice of death'/'DKA', '226721').

card_in_set('chalice of life', 'DKA').
card_original_type('chalice of life'/'DKA', 'Artifact').
card_original_text('chalice of life'/'DKA', '{T}: You gain 1 life. Then if you have at least 10 life more than your starting life total, transform Chalice of Life.').
card_first_print('chalice of life', 'DKA').
card_image_name('chalice of life'/'DKA', 'chalice of life').
card_uid('chalice of life'/'DKA', 'DKA:Chalice of Life:chalice of life').
card_rarity('chalice of life'/'DKA', 'Uncommon').
card_artist('chalice of life'/'DKA', 'Ryan Yee').
card_number('chalice of life'/'DKA', '146a').
card_flavor_text('chalice of life'/'DKA', 'The sweet taste of hope\'s promise.').
card_multiverse_id('chalice of life'/'DKA', '226735').

card_in_set('chant of the skifsang', 'DKA').
card_original_type('chant of the skifsang'/'DKA', 'Enchantment — Aura').
card_original_text('chant of the skifsang'/'DKA', 'Enchant creature\nEnchanted creature gets -13/-0.').
card_first_print('chant of the skifsang', 'DKA').
card_image_name('chant of the skifsang'/'DKA', 'chant of the skifsang').
card_uid('chant of the skifsang'/'DKA', 'DKA:Chant of the Skifsang:chant of the skifsang').
card_rarity('chant of the skifsang'/'DKA', 'Common').
card_artist('chant of the skifsang'/'DKA', 'Nils Hamm').
card_number('chant of the skifsang'/'DKA', '31').
card_flavor_text('chant of the skifsang'/'DKA', 'The skifsang, seafarers of Nephalia, craft spells like other sailors craft nets—making them strong enough to snare even the deadliest catch.').
card_multiverse_id('chant of the skifsang'/'DKA', '262829').

card_in_set('chill of foreboding', 'DKA').
card_original_type('chill of foreboding'/'DKA', 'Sorcery').
card_original_text('chill of foreboding'/'DKA', 'Each player puts the top five cards of his or her library into his or her graveyard.\nFlashback {7}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('chill of foreboding', 'DKA').
card_image_name('chill of foreboding'/'DKA', 'chill of foreboding').
card_uid('chill of foreboding'/'DKA', 'DKA:Chill of Foreboding:chill of foreboding').
card_rarity('chill of foreboding'/'DKA', 'Uncommon').
card_artist('chill of foreboding'/'DKA', 'Terese Nielsen').
card_number('chill of foreboding'/'DKA', '32').
card_flavor_text('chill of foreboding'/'DKA', '\"Wait . . . did you fellows hear something?\"').
card_multiverse_id('chill of foreboding'/'DKA', '242511').

card_in_set('chosen of markov', 'DKA').
card_original_type('chosen of markov'/'DKA', 'Creature — Human').
card_original_text('chosen of markov'/'DKA', '{T}, Tap an untapped Vampire you control: Transform Chosen of Markov.').
card_first_print('chosen of markov', 'DKA').
card_image_name('chosen of markov'/'DKA', 'chosen of markov').
card_uid('chosen of markov'/'DKA', 'DKA:Chosen of Markov:chosen of markov').
card_rarity('chosen of markov'/'DKA', 'Common').
card_artist('chosen of markov'/'DKA', 'Steve Argyle').
card_number('chosen of markov'/'DKA', '55a').
card_flavor_text('chosen of markov'/'DKA', '\"I have been found worthy by this great house . . .\"').
card_multiverse_id('chosen of markov'/'DKA', '243229').

card_in_set('clinging mists', 'DKA').
card_original_type('clinging mists'/'DKA', 'Instant').
card_original_text('clinging mists'/'DKA', 'Prevent all combat damage that would be dealt this turn.\nFateful hour — If you have 5 or less life, tap all attacking creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_first_print('clinging mists', 'DKA').
card_image_name('clinging mists'/'DKA', 'clinging mists').
card_uid('clinging mists'/'DKA', 'DKA:Clinging Mists:clinging mists').
card_rarity('clinging mists'/'DKA', 'Common').
card_artist('clinging mists'/'DKA', 'Anthony Francisco').
card_number('clinging mists'/'DKA', '109').
card_multiverse_id('clinging mists'/'DKA', '262844').

card_in_set('counterlash', 'DKA').
card_original_type('counterlash'/'DKA', 'Instant').
card_original_text('counterlash'/'DKA', 'Counter target spell. You may cast a nonland card in your hand that shares a card type with that spell without paying its mana cost.').
card_first_print('counterlash', 'DKA').
card_image_name('counterlash'/'DKA', 'counterlash').
card_uid('counterlash'/'DKA', 'DKA:Counterlash:counterlash').
card_rarity('counterlash'/'DKA', 'Rare').
card_artist('counterlash'/'DKA', 'Austin Hsu').
card_number('counterlash'/'DKA', '33').
card_flavor_text('counterlash'/'DKA', '\"Pathetic. Let me show you how it\'s done.\"').
card_multiverse_id('counterlash'/'DKA', '262830').

card_in_set('crushing vines', 'DKA').
card_original_type('crushing vines'/'DKA', 'Instant').
card_original_text('crushing vines'/'DKA', 'Choose one — Destroy target creature with flying; or destroy target artifact.').
card_first_print('crushing vines', 'DKA').
card_image_name('crushing vines'/'DKA', 'crushing vines').
card_uid('crushing vines'/'DKA', 'DKA:Crushing Vines:crushing vines').
card_rarity('crushing vines'/'DKA', 'Common').
card_artist('crushing vines'/'DKA', 'Scott Chou').
card_number('crushing vines'/'DKA', '110').
card_flavor_text('crushing vines'/'DKA', '\"Your veil\'s curse won\'t stop me, deathmage. I have taken down countless night hunters and this dark plane is not big enough for you to hide.\"\n—Garruk Wildspeaker, to Liliana Vess').
card_multiverse_id('crushing vines'/'DKA', '262867').

card_in_set('curse of bloodletting', 'DKA').
card_original_type('curse of bloodletting'/'DKA', 'Enchantment — Aura Curse').
card_original_text('curse of bloodletting'/'DKA', 'Enchant player\nIf a source would deal damage to enchanted player, it deals double that damage to that player instead.').
card_first_print('curse of bloodletting', 'DKA').
card_image_name('curse of bloodletting'/'DKA', 'curse of bloodletting').
card_uid('curse of bloodletting'/'DKA', 'DKA:Curse of Bloodletting:curse of bloodletting').
card_rarity('curse of bloodletting'/'DKA', 'Rare').
card_artist('curse of bloodletting'/'DKA', 'Michael C. Hayes').
card_number('curse of bloodletting'/'DKA', '85').
card_flavor_text('curse of bloodletting'/'DKA', 'It is the demon\'s mark, an infernal claim on the flesh of the guilty.').
card_multiverse_id('curse of bloodletting'/'DKA', '227091').

card_in_set('curse of echoes', 'DKA').
card_original_type('curse of echoes'/'DKA', 'Enchantment — Aura Curse').
card_original_text('curse of echoes'/'DKA', 'Enchant player\nWhenever enchanted player casts an instant or sorcery spell, each other player may copy that spell and may choose new targets for the copy he or she controls.').
card_first_print('curse of echoes', 'DKA').
card_image_name('curse of echoes'/'DKA', 'curse of echoes').
card_uid('curse of echoes'/'DKA', 'DKA:Curse of Echoes:curse of echoes').
card_rarity('curse of echoes'/'DKA', 'Rare').
card_artist('curse of echoes'/'DKA', 'Slawomir Maniak').
card_number('curse of echoes'/'DKA', '34').
card_multiverse_id('curse of echoes'/'DKA', '262841').

card_in_set('curse of exhaustion', 'DKA').
card_original_type('curse of exhaustion'/'DKA', 'Enchantment — Aura Curse').
card_original_text('curse of exhaustion'/'DKA', 'Enchant player\nEnchanted player can\'t cast more than one spell each turn.').
card_first_print('curse of exhaustion', 'DKA').
card_image_name('curse of exhaustion'/'DKA', 'curse of exhaustion').
card_uid('curse of exhaustion'/'DKA', 'DKA:Curse of Exhaustion:curse of exhaustion').
card_rarity('curse of exhaustion'/'DKA', 'Uncommon').
card_artist('curse of exhaustion'/'DKA', 'Slawomir Maniak').
card_number('curse of exhaustion'/'DKA', '5').
card_flavor_text('curse of exhaustion'/'DKA', '\"I do believe your heresy will prove more difficult now that you have more pressing concerns.\"\n—Bishop Argust').
card_multiverse_id('curse of exhaustion'/'DKA', '226729').

card_in_set('curse of misfortunes', 'DKA').
card_original_type('curse of misfortunes'/'DKA', 'Enchantment — Aura Curse').
card_original_text('curse of misfortunes'/'DKA', 'Enchant player\nAt the beginning of your upkeep, you may search your library for a Curse card that doesn\'t have the same name as a Curse attached to enchanted player, put it onto the battlefield attached to that player, then shuffle your library.').
card_first_print('curse of misfortunes', 'DKA').
card_image_name('curse of misfortunes'/'DKA', 'curse of misfortunes').
card_uid('curse of misfortunes'/'DKA', 'DKA:Curse of Misfortunes:curse of misfortunes').
card_rarity('curse of misfortunes'/'DKA', 'Rare').
card_artist('curse of misfortunes'/'DKA', 'Terese Nielsen').
card_number('curse of misfortunes'/'DKA', '56').
card_multiverse_id('curse of misfortunes'/'DKA', '262874').

card_in_set('curse of thirst', 'DKA').
card_original_type('curse of thirst'/'DKA', 'Enchantment — Aura Curse').
card_original_text('curse of thirst'/'DKA', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, Curse of Thirst deals damage to that player equal to the number of Curses attached to him or her.').
card_image_name('curse of thirst'/'DKA', 'curse of thirst').
card_uid('curse of thirst'/'DKA', 'DKA:Curse of Thirst:curse of thirst').
card_rarity('curse of thirst'/'DKA', 'Uncommon').
card_artist('curse of thirst'/'DKA', 'Christopher Moeller').
card_number('curse of thirst'/'DKA', '57').
card_flavor_text('curse of thirst'/'DKA', '\"May you drink until you drown but never be sated.\"\n—Odila, witch of Morkrut').
card_multiverse_id('curse of thirst'/'DKA', '220003').

card_in_set('dawntreader elk', 'DKA').
card_original_type('dawntreader elk'/'DKA', 'Creature — Elk').
card_original_text('dawntreader elk'/'DKA', '{G}, Sacrifice Dawntreader Elk: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('dawntreader elk', 'DKA').
card_image_name('dawntreader elk'/'DKA', 'dawntreader elk').
card_uid('dawntreader elk'/'DKA', 'DKA:Dawntreader Elk:dawntreader elk').
card_rarity('dawntreader elk'/'DKA', 'Common').
card_artist('dawntreader elk'/'DKA', 'John Avon').
card_number('dawntreader elk'/'DKA', '111').
card_flavor_text('dawntreader elk'/'DKA', 'Silent as winter snow, it seeks wild places unspoiled by the walking dead.').
card_multiverse_id('dawntreader elk'/'DKA', '242532').

card_in_set('deadly allure', 'DKA').
card_original_type('deadly allure'/'DKA', 'Sorcery').
card_original_text('deadly allure'/'DKA', 'Target creature gains deathtouch until end of turn and must be blocked this turn if able.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('deadly allure', 'DKA').
card_image_name('deadly allure'/'DKA', 'deadly allure').
card_uid('deadly allure'/'DKA', 'DKA:Deadly Allure:deadly allure').
card_rarity('deadly allure'/'DKA', 'Uncommon').
card_artist('deadly allure'/'DKA', 'Steve Argyle').
card_number('deadly allure'/'DKA', '58').
card_flavor_text('deadly allure'/'DKA', 'What could be more irresistible than death?').
card_multiverse_id('deadly allure'/'DKA', '262690').

card_in_set('death\'s caress', 'DKA').
card_original_type('death\'s caress'/'DKA', 'Sorcery').
card_original_text('death\'s caress'/'DKA', 'Destroy target creature. If that creature was a Human, you gain life equal to its toughness.').
card_first_print('death\'s caress', 'DKA').
card_image_name('death\'s caress'/'DKA', 'death\'s caress').
card_uid('death\'s caress'/'DKA', 'DKA:Death\'s Caress:death\'s caress').
card_rarity('death\'s caress'/'DKA', 'Common').
card_artist('death\'s caress'/'DKA', 'James Ryman').
card_number('death\'s caress'/'DKA', '59').
card_flavor_text('death\'s caress'/'DKA', 'The faint smell of cloves, the rustling of the wind, and a paralyzing descent into an airless, fathomless tomb.').
card_multiverse_id('death\'s caress'/'DKA', '262854').

card_in_set('deranged outcast', 'DKA').
card_original_type('deranged outcast'/'DKA', 'Creature — Human Rogue').
card_original_text('deranged outcast'/'DKA', '{1}{G}, Sacrifice a Human: Put two +1/+1 counters on target creature.').
card_first_print('deranged outcast', 'DKA').
card_image_name('deranged outcast'/'DKA', 'deranged outcast').
card_uid('deranged outcast'/'DKA', 'DKA:Deranged Outcast:deranged outcast').
card_rarity('deranged outcast'/'DKA', 'Rare').
card_artist('deranged outcast'/'DKA', 'Nils Hamm').
card_number('deranged outcast'/'DKA', '112').
card_flavor_text('deranged outcast'/'DKA', 'Cast out by his village, he now takes grim offense at refugees who seek shelter in his forest.').
card_multiverse_id('deranged outcast'/'DKA', '249901').

card_in_set('diregraf captain', 'DKA').
card_original_type('diregraf captain'/'DKA', 'Creature — Zombie Soldier').
card_original_text('diregraf captain'/'DKA', 'Deathtouch\nOther Zombie creatures you control get +1/+1.\nWhenever another Zombie you control dies, target opponent loses 1 life.').
card_first_print('diregraf captain', 'DKA').
card_image_name('diregraf captain'/'DKA', 'diregraf captain').
card_uid('diregraf captain'/'DKA', 'DKA:Diregraf Captain:diregraf captain').
card_rarity('diregraf captain'/'DKA', 'Uncommon').
card_artist('diregraf captain'/'DKA', 'Slawomir Maniak').
card_number('diregraf captain'/'DKA', '135').
card_flavor_text('diregraf captain'/'DKA', 'Though its mind has long since rotted away, it wields a sword with deadly skill.').
card_multiverse_id('diregraf captain'/'DKA', '262663').

card_in_set('divination', 'DKA').
card_original_type('divination'/'DKA', 'Sorcery').
card_original_text('divination'/'DKA', 'Draw two cards.').
card_image_name('divination'/'DKA', 'divination').
card_uid('divination'/'DKA', 'DKA:Divination:divination').
card_rarity('divination'/'DKA', 'Common').
card_artist('divination'/'DKA', 'Scott Chou').
card_number('divination'/'DKA', '35').
card_flavor_text('divination'/'DKA', 'Even the House of Galan, who takes the most scholarly approach to the mystic traditions, has resorted to exploring more primitive methods in Avacyn\'s absence.').
card_multiverse_id('divination'/'DKA', '262848').

card_in_set('drogskol captain', 'DKA').
card_original_type('drogskol captain'/'DKA', 'Creature — Spirit Soldier').
card_original_text('drogskol captain'/'DKA', 'Flying\nOther Spirit creatures you control get +1/+1 and have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_first_print('drogskol captain', 'DKA').
card_image_name('drogskol captain'/'DKA', 'drogskol captain').
card_uid('drogskol captain'/'DKA', 'DKA:Drogskol Captain:drogskol captain').
card_rarity('drogskol captain'/'DKA', 'Uncommon').
card_artist('drogskol captain'/'DKA', 'Peter Mohrbacher').
card_number('drogskol captain'/'DKA', '136').
card_flavor_text('drogskol captain'/'DKA', 'Dead or alive, true leaders can inspire an entire army.').
card_multiverse_id('drogskol captain'/'DKA', '244773').

card_in_set('drogskol reaver', 'DKA').
card_original_type('drogskol reaver'/'DKA', 'Creature — Spirit').
card_original_text('drogskol reaver'/'DKA', 'Flying, double strike, lifelink\nWhenever you gain life, draw a card.').
card_first_print('drogskol reaver', 'DKA').
card_image_name('drogskol reaver'/'DKA', 'drogskol reaver').
card_uid('drogskol reaver'/'DKA', 'DKA:Drogskol Reaver:drogskol reaver').
card_rarity('drogskol reaver'/'DKA', 'Mythic Rare').
card_artist('drogskol reaver'/'DKA', 'Vincent Proce').
card_number('drogskol reaver'/'DKA', '137').
card_flavor_text('drogskol reaver'/'DKA', 'Wracked and warped with anguish, it obsessively seeks out other souls to hoard within its eternal prison.').
card_multiverse_id('drogskol reaver'/'DKA', '262860').

card_in_set('dungeon geists', 'DKA').
card_original_type('dungeon geists'/'DKA', 'Creature — Spirit').
card_original_text('dungeon geists'/'DKA', 'Flying\nWhen Dungeon Geists enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Dungeon Geists.').
card_first_print('dungeon geists', 'DKA').
card_image_name('dungeon geists'/'DKA', 'dungeon geists').
card_uid('dungeon geists'/'DKA', 'DKA:Dungeon Geists:dungeon geists').
card_rarity('dungeon geists'/'DKA', 'Rare').
card_artist('dungeon geists'/'DKA', 'Nils Hamm').
card_number('dungeon geists'/'DKA', '36').
card_multiverse_id('dungeon geists'/'DKA', '249875').

card_in_set('elbrus, the binding blade', 'DKA').
card_original_type('elbrus, the binding blade'/'DKA', 'Legendary Artifact — Equipment').
card_original_text('elbrus, the binding blade'/'DKA', 'Equipped creature gets +1/+0.\nWhen equipped creature deals combat damage to a player, unattach Elbrus, the Binding Blade, then transform it.\nEquip {1}').
card_first_print('elbrus, the binding blade', 'DKA').
card_image_name('elbrus, the binding blade'/'DKA', 'elbrus, the binding blade').
card_uid('elbrus, the binding blade'/'DKA', 'DKA:Elbrus, the Binding Blade:elbrus, the binding blade').
card_rarity('elbrus, the binding blade'/'DKA', 'Mythic Rare').
card_artist('elbrus, the binding blade'/'DKA', 'Eric Deschamps').
card_number('elbrus, the binding blade'/'DKA', '147a').
card_flavor_text('elbrus, the binding blade'/'DKA', 'Those who grasp its hilt soon hear the demon\'s call.').
card_multiverse_id('elbrus, the binding blade'/'DKA', '244740').

card_in_set('elgaud inquisitor', 'DKA').
card_original_type('elgaud inquisitor'/'DKA', 'Creature — Human Cleric').
card_original_text('elgaud inquisitor'/'DKA', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nWhen Elgaud Inquisitor dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('elgaud inquisitor', 'DKA').
card_image_name('elgaud inquisitor'/'DKA', 'elgaud inquisitor').
card_uid('elgaud inquisitor'/'DKA', 'DKA:Elgaud Inquisitor:elgaud inquisitor').
card_rarity('elgaud inquisitor'/'DKA', 'Common').
card_artist('elgaud inquisitor'/'DKA', 'Slawomir Maniak').
card_number('elgaud inquisitor'/'DKA', '6').
card_flavor_text('elgaud inquisitor'/'DKA', '\"It will take more than steel alone to purge this world of evil.\"').
card_multiverse_id('elgaud inquisitor'/'DKA', '245219').

card_in_set('erdwal ripper', 'DKA').
card_original_type('erdwal ripper'/'DKA', 'Creature — Vampire').
card_original_text('erdwal ripper'/'DKA', 'Haste\nWhenever Erdwal Ripper deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('erdwal ripper', 'DKA').
card_image_name('erdwal ripper'/'DKA', 'erdwal ripper').
card_uid('erdwal ripper'/'DKA', 'DKA:Erdwal Ripper:erdwal ripper').
card_rarity('erdwal ripper'/'DKA', 'Common').
card_artist('erdwal ripper'/'DKA', 'Kev Walker').
card_number('erdwal ripper'/'DKA', '86').
card_flavor_text('erdwal ripper'/'DKA', 'Savage vampires lurk in the Erdwal\'s network of passageways where prey is plentiful and easy to catch.').
card_multiverse_id('erdwal ripper'/'DKA', '249979').

card_in_set('evolving wilds', 'DKA').
card_original_type('evolving wilds'/'DKA', 'Land').
card_original_text('evolving wilds'/'DKA', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DKA', 'evolving wilds').
card_uid('evolving wilds'/'DKA', 'DKA:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DKA', 'Common').
card_artist('evolving wilds'/'DKA', 'Cliff Childs').
card_number('evolving wilds'/'DKA', '155').
card_flavor_text('evolving wilds'/'DKA', '\"Our world is vast, but Thraben is its heart. The cathedral must stand even if the hinterlands are lost.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('evolving wilds'/'DKA', '262680').

card_in_set('executioner\'s hood', 'DKA').
card_original_type('executioner\'s hood'/'DKA', 'Artifact — Equipment').
card_original_text('executioner\'s hood'/'DKA', 'Equipped creature has intimidate. (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('executioner\'s hood', 'DKA').
card_image_name('executioner\'s hood'/'DKA', 'executioner\'s hood').
card_uid('executioner\'s hood'/'DKA', 'DKA:Executioner\'s Hood:executioner\'s hood').
card_rarity('executioner\'s hood'/'DKA', 'Common').
card_artist('executioner\'s hood'/'DKA', 'Anthony Palumbo').
card_number('executioner\'s hood'/'DKA', '148').
card_multiverse_id('executioner\'s hood'/'DKA', '244737').

card_in_set('faith\'s shield', 'DKA').
card_original_type('faith\'s shield'/'DKA', 'Instant').
card_original_text('faith\'s shield'/'DKA', 'Target permanent you control gains protection from the color of your choice until end of turn.\nFateful hour — If you have 5 or less life, instead you and each permanent you control gain protection from the color of your choice until end of turn.').
card_first_print('faith\'s shield', 'DKA').
card_image_name('faith\'s shield'/'DKA', 'faith\'s shield').
card_uid('faith\'s shield'/'DKA', 'DKA:Faith\'s Shield:faith\'s shield').
card_rarity('faith\'s shield'/'DKA', 'Uncommon').
card_artist('faith\'s shield'/'DKA', 'Svetlin Velinov').
card_number('faith\'s shield'/'DKA', '7').
card_multiverse_id('faith\'s shield'/'DKA', '262696').

card_in_set('faithless looting', 'DKA').
card_original_type('faithless looting'/'DKA', 'Sorcery').
card_original_text('faithless looting'/'DKA', 'Draw two cards, then discard two cards.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('faithless looting'/'DKA', 'faithless looting').
card_uid('faithless looting'/'DKA', 'DKA:Faithless Looting:faithless looting').
card_rarity('faithless looting'/'DKA', 'Common').
card_artist('faithless looting'/'DKA', 'Gabor Szikszai').
card_number('faithless looting'/'DKA', '87').
card_flavor_text('faithless looting'/'DKA', '\"Avacyn has abandoned us! We have nothing left except what we can take!\"').
card_multiverse_id('faithless looting'/'DKA', '245299').

card_in_set('falkenrath aristocrat', 'DKA').
card_original_type('falkenrath aristocrat'/'DKA', 'Creature — Vampire').
card_original_text('falkenrath aristocrat'/'DKA', 'Flying, haste\nSacrifice a creature: Falkenrath Aristocrat is indestructible this turn. If the sacrificed creature was a Human, put a +1/+1 counter on Falkenrath Aristocrat.').
card_first_print('falkenrath aristocrat', 'DKA').
card_image_name('falkenrath aristocrat'/'DKA', 'falkenrath aristocrat').
card_uid('falkenrath aristocrat'/'DKA', 'DKA:Falkenrath Aristocrat:falkenrath aristocrat').
card_rarity('falkenrath aristocrat'/'DKA', 'Mythic Rare').
card_artist('falkenrath aristocrat'/'DKA', 'Igor Kieryluk').
card_number('falkenrath aristocrat'/'DKA', '138').
card_flavor_text('falkenrath aristocrat'/'DKA', '\"Your blood is quite pleasing. A pity there\'s not enough for both of us.\"').
card_multiverse_id('falkenrath aristocrat'/'DKA', '262847').

card_in_set('falkenrath torturer', 'DKA').
card_original_type('falkenrath torturer'/'DKA', 'Creature — Vampire').
card_original_text('falkenrath torturer'/'DKA', 'Sacrifice a creature: Falkenrath Torturer gains flying until end of turn. If the sacrificed creature was a Human, put a +1/+1 counter on Falkenrath Torturer.').
card_first_print('falkenrath torturer', 'DKA').
card_image_name('falkenrath torturer'/'DKA', 'falkenrath torturer').
card_uid('falkenrath torturer'/'DKA', 'DKA:Falkenrath Torturer:falkenrath torturer').
card_rarity('falkenrath torturer'/'DKA', 'Common').
card_artist('falkenrath torturer'/'DKA', 'Steve Argyle').
card_number('falkenrath torturer'/'DKA', '60').
card_flavor_text('falkenrath torturer'/'DKA', '\"There was a time when our cages rang with wailing. Now only a handful of these whimpering fools remain.\"').
card_multiverse_id('falkenrath torturer'/'DKA', '262697').

card_in_set('farbog boneflinger', 'DKA').
card_original_type('farbog boneflinger'/'DKA', 'Creature — Zombie').
card_original_text('farbog boneflinger'/'DKA', 'When Farbog Boneflinger enters the battlefield, target creature gets -2/-2 until end of turn.').
card_first_print('farbog boneflinger', 'DKA').
card_image_name('farbog boneflinger'/'DKA', 'farbog boneflinger').
card_uid('farbog boneflinger'/'DKA', 'DKA:Farbog Boneflinger:farbog boneflinger').
card_rarity('farbog boneflinger'/'DKA', 'Uncommon').
card_artist('farbog boneflinger'/'DKA', 'Tomasz Jedruszek').
card_number('farbog boneflinger'/'DKA', '61').
card_flavor_text('farbog boneflinger'/'DKA', '\"Here are some bones for you to choke on, dear brother!\"\n—Gisa, to Geralf').
card_multiverse_id('farbog boneflinger'/'DKA', '262677').

card_in_set('favor of the woods', 'DKA').
card_original_type('favor of the woods'/'DKA', 'Enchantment — Aura').
card_original_text('favor of the woods'/'DKA', 'Enchant creature\nWhenever enchanted creature blocks, you gain 3 life.').
card_first_print('favor of the woods', 'DKA').
card_image_name('favor of the woods'/'DKA', 'favor of the woods').
card_uid('favor of the woods'/'DKA', 'DKA:Favor of the Woods:favor of the woods').
card_rarity('favor of the woods'/'DKA', 'Common').
card_artist('favor of the woods'/'DKA', 'Tomasz Jedruszek').
card_number('favor of the woods'/'DKA', '113').
card_flavor_text('favor of the woods'/'DKA', 'Feeling abandoned by Avacyn, some Kessigers turned to the pagan rituals of their ancestors.').
card_multiverse_id('favor of the woods'/'DKA', '262687').

card_in_set('feed the pack', 'DKA').
card_original_type('feed the pack'/'DKA', 'Enchantment').
card_original_text('feed the pack'/'DKA', 'At the beginning of your end step, you may sacrifice a nontoken creature. If you do, put X 2/2 green Wolf creature tokens onto the battlefield, where X is the sacrificed creature\'s toughness.').
card_first_print('feed the pack', 'DKA').
card_image_name('feed the pack'/'DKA', 'feed the pack').
card_uid('feed the pack'/'DKA', 'DKA:Feed the Pack:feed the pack').
card_rarity('feed the pack'/'DKA', 'Rare').
card_artist('feed the pack'/'DKA', 'Steve Prescott').
card_number('feed the pack'/'DKA', '114').
card_multiverse_id('feed the pack'/'DKA', '262857').

card_in_set('fiend of the shadows', 'DKA').
card_original_type('fiend of the shadows'/'DKA', 'Creature — Vampire Wizard').
card_original_text('fiend of the shadows'/'DKA', 'Flying\nWhenever Fiend of the Shadows deals combat damage to a player, that player exiles a card from his or her hand. You may play that card for as long as it remains exiled.\nSacrifice a Human: Regenerate Fiend of the Shadows.').
card_first_print('fiend of the shadows', 'DKA').
card_image_name('fiend of the shadows'/'DKA', 'fiend of the shadows').
card_uid('fiend of the shadows'/'DKA', 'DKA:Fiend of the Shadows:fiend of the shadows').
card_rarity('fiend of the shadows'/'DKA', 'Rare').
card_artist('fiend of the shadows'/'DKA', 'Igor Kieryluk').
card_number('fiend of the shadows'/'DKA', '62').
card_multiverse_id('fiend of the shadows'/'DKA', '262837').

card_in_set('fires of undeath', 'DKA').
card_original_type('fires of undeath'/'DKA', 'Instant').
card_original_text('fires of undeath'/'DKA', 'Fires of Undeath deals 2 damage to target creature or player.\nFlashback {5}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('fires of undeath', 'DKA').
card_image_name('fires of undeath'/'DKA', 'fires of undeath').
card_uid('fires of undeath'/'DKA', 'DKA:Fires of Undeath:fires of undeath').
card_rarity('fires of undeath'/'DKA', 'Common').
card_artist('fires of undeath'/'DKA', 'Jason Chan').
card_number('fires of undeath'/'DKA', '88').
card_flavor_text('fires of undeath'/'DKA', '\"I drink of those who are worthy of my palate. The rest I burn.\"').
card_multiverse_id('fires of undeath'/'DKA', '262832').

card_in_set('flayer of the hatebound', 'DKA').
card_original_type('flayer of the hatebound'/'DKA', 'Creature — Devil').
card_original_text('flayer of the hatebound'/'DKA', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)\nWhenever Flayer of the Hatebound or another creature enters the battlefield from your graveyard, that creature deals damage equal to its power to target creature or player.').
card_first_print('flayer of the hatebound', 'DKA').
card_image_name('flayer of the hatebound'/'DKA', 'flayer of the hatebound').
card_uid('flayer of the hatebound'/'DKA', 'DKA:Flayer of the Hatebound:flayer of the hatebound').
card_rarity('flayer of the hatebound'/'DKA', 'Rare').
card_artist('flayer of the hatebound'/'DKA', 'Jana Schirmer & Johannes Voss').
card_number('flayer of the hatebound'/'DKA', '89').
card_multiverse_id('flayer of the hatebound'/'DKA', '262853').

card_in_set('fling', 'DKA').
card_original_type('fling'/'DKA', 'Instant').
card_original_text('fling'/'DKA', 'As an additional cost to cast Fling, sacrifice a creature.\nFling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('fling'/'DKA', 'fling').
card_uid('fling'/'DKA', 'DKA:Fling:fling').
card_rarity('fling'/'DKA', 'Common').
card_artist('fling'/'DKA', 'Izzy').
card_number('fling'/'DKA', '90').
card_flavor_text('fling'/'DKA', 'Masters of the sword know the blade is not the only weapon at their disposal.').
card_multiverse_id('fling'/'DKA', '262871').

card_in_set('forge devil', 'DKA').
card_original_type('forge devil'/'DKA', 'Creature — Devil').
card_original_text('forge devil'/'DKA', 'When Forge Devil enters the battlefield, it deals 1 damage to target creature and 1 damage to you.').
card_first_print('forge devil', 'DKA').
card_image_name('forge devil'/'DKA', 'forge devil').
card_uid('forge devil'/'DKA', 'DKA:Forge Devil:forge devil').
card_rarity('forge devil'/'DKA', 'Common').
card_artist('forge devil'/'DKA', 'Austin Hsu').
card_number('forge devil'/'DKA', '91').
card_flavor_text('forge devil'/'DKA', 'Devils infiltrated the lower levels of Thraben Cathedral, making cracks in load-bearing pillars and setting fire to precious archives.').
card_multiverse_id('forge devil'/'DKA', '221174').

card_in_set('gather the townsfolk', 'DKA').
card_original_type('gather the townsfolk'/'DKA', 'Sorcery').
card_original_text('gather the townsfolk'/'DKA', 'Put two 1/1 white Human creature tokens onto the battlefield.\nFateful hour — If you have 5 or less life, put five of those tokens onto the battlefield instead.').
card_image_name('gather the townsfolk'/'DKA', 'gather the townsfolk').
card_uid('gather the townsfolk'/'DKA', 'DKA:Gather the Townsfolk:gather the townsfolk').
card_rarity('gather the townsfolk'/'DKA', 'Common').
card_artist('gather the townsfolk'/'DKA', 'Dan Scott').
card_number('gather the townsfolk'/'DKA', '8').
card_flavor_text('gather the townsfolk'/'DKA', 'In the memories of those they lost lies the strength needed to defend their city.').
card_multiverse_id('gather the townsfolk'/'DKA', '262686').

card_in_set('gavony ironwright', 'DKA').
card_original_type('gavony ironwright'/'DKA', 'Creature — Human Soldier').
card_original_text('gavony ironwright'/'DKA', 'Fateful hour — As long as you have 5 or less life, other creatures you control get +1/+4.').
card_first_print('gavony ironwright', 'DKA').
card_image_name('gavony ironwright'/'DKA', 'gavony ironwright').
card_uid('gavony ironwright'/'DKA', 'DKA:Gavony Ironwright:gavony ironwright').
card_rarity('gavony ironwright'/'DKA', 'Uncommon').
card_artist('gavony ironwright'/'DKA', 'Karl Kopinski').
card_number('gavony ironwright'/'DKA', '9').
card_flavor_text('gavony ironwright'/'DKA', '\"I wish my work could shelter everyone, yet I pray we are never so few that it does.\"').
card_multiverse_id('gavony ironwright'/'DKA', '262685').

card_in_set('geralf\'s messenger', 'DKA').
card_original_type('geralf\'s messenger'/'DKA', 'Creature — Zombie').
card_original_text('geralf\'s messenger'/'DKA', 'Geralf\'s Messenger enters the battlefield tapped.\nWhen Geralf\'s Messenger enters the battlefield, target opponent loses 2 life.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('geralf\'s messenger', 'DKA').
card_image_name('geralf\'s messenger'/'DKA', 'geralf\'s messenger').
card_uid('geralf\'s messenger'/'DKA', 'DKA:Geralf\'s Messenger:geralf\'s messenger').
card_rarity('geralf\'s messenger'/'DKA', 'Rare').
card_artist('geralf\'s messenger'/'DKA', 'Kev Walker').
card_number('geralf\'s messenger'/'DKA', '63').
card_multiverse_id('geralf\'s messenger'/'DKA', '243250').

card_in_set('geralf\'s mindcrusher', 'DKA').
card_original_type('geralf\'s mindcrusher'/'DKA', 'Creature — Zombie Horror').
card_original_text('geralf\'s mindcrusher'/'DKA', 'When Geralf\'s Mindcrusher enters the battlefield, target player puts the top five cards of his or her library into his or her graveyard.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('geralf\'s mindcrusher', 'DKA').
card_image_name('geralf\'s mindcrusher'/'DKA', 'geralf\'s mindcrusher').
card_uid('geralf\'s mindcrusher'/'DKA', 'DKA:Geralf\'s Mindcrusher:geralf\'s mindcrusher').
card_rarity('geralf\'s mindcrusher'/'DKA', 'Rare').
card_artist('geralf\'s mindcrusher'/'DKA', 'Steven Belledin').
card_number('geralf\'s mindcrusher'/'DKA', '37').
card_multiverse_id('geralf\'s mindcrusher'/'DKA', '262839').

card_in_set('ghastly haunting', 'DKA').
card_original_type('ghastly haunting'/'DKA', 'Enchantment — Aura').
card_original_text('ghastly haunting'/'DKA', 'Enchant creature\nYou control enchanted creature.').
card_first_print('ghastly haunting', 'DKA').
card_image_name('ghastly haunting'/'DKA', 'ghastly haunting').
card_uid('ghastly haunting'/'DKA', 'DKA:Ghastly Haunting:ghastly haunting').
card_rarity('ghastly haunting'/'DKA', 'Uncommon').
card_artist('ghastly haunting'/'DKA', 'Lucas Graciano').
card_number('ghastly haunting'/'DKA', '50b').
card_flavor_text('ghastly haunting'/'DKA', 'The body often twists and flails spasmodically as the spirit explores its new home.').
card_multiverse_id('ghastly haunting'/'DKA', '242498').

card_in_set('ghoultree', 'DKA').
card_original_type('ghoultree'/'DKA', 'Creature — Zombie Treefolk').
card_original_text('ghoultree'/'DKA', 'Ghoultree costs {1} less to cast for each creature card in your graveyard.').
card_first_print('ghoultree', 'DKA').
card_image_name('ghoultree'/'DKA', 'ghoultree').
card_uid('ghoultree'/'DKA', 'DKA:Ghoultree:ghoultree').
card_rarity('ghoultree'/'DKA', 'Rare').
card_artist('ghoultree'/'DKA', 'Volkan Baga').
card_number('ghoultree'/'DKA', '115').
card_flavor_text('ghoultree'/'DKA', 'It uproots entire fengrafs to add to its twisted frame.').
card_multiverse_id('ghoultree'/'DKA', '262662').

card_in_set('grafdigger\'s cage', 'DKA').
card_original_type('grafdigger\'s cage'/'DKA', 'Artifact').
card_original_text('grafdigger\'s cage'/'DKA', 'Creature cards can\'t enter the battlefield from graveyards or libraries.\nPlayers can\'t cast cards in graveyards or libraries.').
card_first_print('grafdigger\'s cage', 'DKA').
card_image_name('grafdigger\'s cage'/'DKA', 'grafdigger\'s cage').
card_uid('grafdigger\'s cage'/'DKA', 'DKA:Grafdigger\'s Cage:grafdigger\'s cage').
card_rarity('grafdigger\'s cage'/'DKA', 'Rare').
card_artist('grafdigger\'s cage'/'DKA', 'Daniel Ljunggren').
card_number('grafdigger\'s cage'/'DKA', '149').
card_flavor_text('grafdigger\'s cage'/'DKA', '\"If you wind up in one of mine, you can be sure as silver it will be your last.\"\n—Grafdigger Wulmer').
card_multiverse_id('grafdigger\'s cage'/'DKA', '278452').

card_in_set('gravecrawler', 'DKA').
card_original_type('gravecrawler'/'DKA', 'Creature — Zombie').
card_original_text('gravecrawler'/'DKA', 'Gravecrawler can\'t block.\nYou may cast Gravecrawler from your graveyard as long as you control a Zombie.').
card_image_name('gravecrawler'/'DKA', 'gravecrawler').
card_uid('gravecrawler'/'DKA', 'DKA:Gravecrawler:gravecrawler').
card_rarity('gravecrawler'/'DKA', 'Rare').
card_artist('gravecrawler'/'DKA', 'Steven Belledin').
card_number('gravecrawler'/'DKA', '64').
card_flavor_text('gravecrawler'/'DKA', '\"Innistrad\'s ghoulcallers are talented enough, but let me show you what someone with real power can create.\"\n—Liliana Vess').
card_multiverse_id('gravecrawler'/'DKA', '222902').

card_in_set('gravepurge', 'DKA').
card_original_type('gravepurge'/'DKA', 'Instant').
card_original_text('gravepurge'/'DKA', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_first_print('gravepurge', 'DKA').
card_image_name('gravepurge'/'DKA', 'gravepurge').
card_uid('gravepurge'/'DKA', 'DKA:Gravepurge:gravepurge').
card_rarity('gravepurge'/'DKA', 'Common').
card_artist('gravepurge'/'DKA', 'Zoltan Boros').
card_number('gravepurge'/'DKA', '65').
card_flavor_text('gravepurge'/'DKA', '\"The Blessed Sleep? Ha! Wake up my lovelies, there\'s work to be done!\"\n—Gisa, ghoulcaller of Gavony').
card_multiverse_id('gravepurge'/'DKA', '220056').

card_in_set('gravetiller wurm', 'DKA').
card_original_type('gravetiller wurm'/'DKA', 'Creature — Wurm').
card_original_text('gravetiller wurm'/'DKA', 'Trample\nMorbid — Gravetiller Wurm enters the battlefield with four +1/+1 counters on it if a creature died this turn.').
card_first_print('gravetiller wurm', 'DKA').
card_image_name('gravetiller wurm'/'DKA', 'gravetiller wurm').
card_uid('gravetiller wurm'/'DKA', 'DKA:Gravetiller Wurm:gravetiller wurm').
card_rarity('gravetiller wurm'/'DKA', 'Uncommon').
card_artist('gravetiller wurm'/'DKA', 'Slawomir Maniak').
card_number('gravetiller wurm'/'DKA', '116').
card_flavor_text('gravetiller wurm'/'DKA', 'As the creatures of the night closed in on humanity, Innistrad\'s primeval forces stirred, as though waking to watch the slaughter.').
card_multiverse_id('gravetiller wurm'/'DKA', '262673').

card_in_set('grim backwoods', 'DKA').
card_original_type('grim backwoods'/'DKA', 'Land').
card_original_text('grim backwoods'/'DKA', '{T}: Add {1} to your mana pool.\n{2}{B}{G}, {T}, Sacrifice a creature: Draw a card.').
card_first_print('grim backwoods', 'DKA').
card_image_name('grim backwoods'/'DKA', 'grim backwoods').
card_uid('grim backwoods'/'DKA', 'DKA:Grim Backwoods:grim backwoods').
card_rarity('grim backwoods'/'DKA', 'Rare').
card_artist('grim backwoods'/'DKA', 'Vincent Proce').
card_number('grim backwoods'/'DKA', '156').
card_flavor_text('grim backwoods'/'DKA', '\"I love what they\'ve done with the place.\"\n—Liliana Vess').
card_multiverse_id('grim backwoods'/'DKA', '270939').

card_in_set('grim flowering', 'DKA').
card_original_type('grim flowering'/'DKA', 'Sorcery').
card_original_text('grim flowering'/'DKA', 'Draw a card for each creature card in your graveyard.').
card_first_print('grim flowering', 'DKA').
card_image_name('grim flowering'/'DKA', 'grim flowering').
card_uid('grim flowering'/'DKA', 'DKA:Grim Flowering:grim flowering').
card_rarity('grim flowering'/'DKA', 'Uncommon').
card_artist('grim flowering'/'DKA', 'Adam Paquette').
card_number('grim flowering'/'DKA', '117').
card_flavor_text('grim flowering'/'DKA', '\"Nothing in nature goes to waste, not even the rotting corpse of a good-for-nothing, blood-sucking vampire.\"\n—Halana of Ulvenwald').
card_multiverse_id('grim flowering'/'DKA', '262842').

card_in_set('griptide', 'DKA').
card_original_type('griptide'/'DKA', 'Instant').
card_original_text('griptide'/'DKA', 'Put target creature on top of its owner\'s library.').
card_first_print('griptide', 'DKA').
card_image_name('griptide'/'DKA', 'griptide').
card_uid('griptide'/'DKA', 'DKA:Griptide:griptide').
card_rarity('griptide'/'DKA', 'Common').
card_artist('griptide'/'DKA', 'Igor Kieryluk').
card_number('griptide'/'DKA', '38').
card_flavor_text('griptide'/'DKA', '\"Beware the seagrafs just off the shore. These waters are filled with hungry geists looking for an easy meal.\"\n—Captain Eberhart').
card_multiverse_id('griptide'/'DKA', '243224').

card_in_set('gruesome discovery', 'DKA').
card_original_type('gruesome discovery'/'DKA', 'Sorcery').
card_original_text('gruesome discovery'/'DKA', 'Target player discards two cards.\nMorbid — If a creature died this turn, instead that player reveals his or her hand, you choose two cards from it, then that player discards those cards.').
card_first_print('gruesome discovery', 'DKA').
card_image_name('gruesome discovery'/'DKA', 'gruesome discovery').
card_uid('gruesome discovery'/'DKA', 'DKA:Gruesome Discovery:gruesome discovery').
card_rarity('gruesome discovery'/'DKA', 'Common').
card_artist('gruesome discovery'/'DKA', 'John Stanko').
card_number('gruesome discovery'/'DKA', '66').
card_multiverse_id('gruesome discovery'/'DKA', '262843').

card_in_set('harrowing journey', 'DKA').
card_original_type('harrowing journey'/'DKA', 'Sorcery').
card_original_text('harrowing journey'/'DKA', 'Target player draws three cards and loses 3 life.').
card_first_print('harrowing journey', 'DKA').
card_image_name('harrowing journey'/'DKA', 'harrowing journey').
card_uid('harrowing journey'/'DKA', 'DKA:Harrowing Journey:harrowing journey').
card_rarity('harrowing journey'/'DKA', 'Uncommon').
card_artist('harrowing journey'/'DKA', 'James Paick').
card_number('harrowing journey'/'DKA', '67').
card_flavor_text('harrowing journey'/'DKA', '\"There are always fools who attempt to traverse Kruin Pass before sundown and every time they wind up in a race for their lives.\"\n—Old Rutstein').
card_multiverse_id('harrowing journey'/'DKA', '262689').

card_in_set('haunted fengraf', 'DKA').
card_original_type('haunted fengraf'/'DKA', 'Land').
card_original_text('haunted fengraf'/'DKA', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice Haunted Fengraf: Return a creature card at random from your graveyard to your hand.').
card_first_print('haunted fengraf', 'DKA').
card_image_name('haunted fengraf'/'DKA', 'haunted fengraf').
card_uid('haunted fengraf'/'DKA', 'DKA:Haunted Fengraf:haunted fengraf').
card_rarity('haunted fengraf'/'DKA', 'Common').
card_artist('haunted fengraf'/'DKA', 'Adam Paquette').
card_number('haunted fengraf'/'DKA', '157').
card_flavor_text('haunted fengraf'/'DKA', 'A ghoulcaller\'s playground.').
card_multiverse_id('haunted fengraf'/'DKA', '245289').

card_in_set('havengul lich', 'DKA').
card_original_type('havengul lich'/'DKA', 'Creature — Zombie Wizard').
card_original_text('havengul lich'/'DKA', '{1}: You may cast target creature card in a graveyard this turn. When you cast that card this turn, Havengul Lich gains all activated abilities of that card until end of turn.').
card_first_print('havengul lich', 'DKA').
card_image_name('havengul lich'/'DKA', 'havengul lich').
card_uid('havengul lich'/'DKA', 'DKA:Havengul Lich:havengul lich').
card_rarity('havengul lich'/'DKA', 'Mythic Rare').
card_artist('havengul lich'/'DKA', 'James Ryman').
card_number('havengul lich'/'DKA', '139').
card_flavor_text('havengul lich'/'DKA', 'Fusing intelligence with the power of undeath requires inspiration of the darkest kind.').
card_multiverse_id('havengul lich'/'DKA', '262852').

card_in_set('havengul runebinder', 'DKA').
card_original_type('havengul runebinder'/'DKA', 'Creature — Human Wizard').
card_original_text('havengul runebinder'/'DKA', '{2}{U}, {T}, Exile a creature card from your graveyard: Put a 2/2 black Zombie creature token onto the battlefield, then put a +1/+1 counter on each Zombie creature you control.').
card_first_print('havengul runebinder', 'DKA').
card_image_name('havengul runebinder'/'DKA', 'havengul runebinder').
card_uid('havengul runebinder'/'DKA', 'DKA:Havengul Runebinder:havengul runebinder').
card_rarity('havengul runebinder'/'DKA', 'Rare').
card_artist('havengul runebinder'/'DKA', 'Bud Cook').
card_number('havengul runebinder'/'DKA', '39').
card_flavor_text('havengul runebinder'/'DKA', '\"So many corpses, so little time.\"').
card_multiverse_id('havengul runebinder'/'DKA', '249878').

card_in_set('headless skaab', 'DKA').
card_original_type('headless skaab'/'DKA', 'Creature — Zombie Warrior').
card_original_text('headless skaab'/'DKA', 'As an additional cost to cast Headless Skaab, exile a creature card from your graveyard.\nHeadless Skaab enters the battlefield tapped.').
card_first_print('headless skaab', 'DKA').
card_image_name('headless skaab'/'DKA', 'headless skaab').
card_uid('headless skaab'/'DKA', 'DKA:Headless Skaab:headless skaab').
card_rarity('headless skaab'/'DKA', 'Common').
card_artist('headless skaab'/'DKA', 'Johann Bodin').
card_number('headless skaab'/'DKA', '40').
card_flavor_text('headless skaab'/'DKA', 'A head is a needless encumbrance when muscle is all that\'s required.').
card_multiverse_id('headless skaab'/'DKA', '244761').

card_in_set('heavy mattock', 'DKA').
card_original_type('heavy mattock'/'DKA', 'Artifact — Equipment').
card_original_text('heavy mattock'/'DKA', 'Equipped creature gets +1/+1.\nAs long as equipped creature is a Human, it gets an additional +1/+1.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('heavy mattock', 'DKA').
card_image_name('heavy mattock'/'DKA', 'heavy mattock').
card_uid('heavy mattock'/'DKA', 'DKA:Heavy Mattock:heavy mattock').
card_rarity('heavy mattock'/'DKA', 'Common').
card_artist('heavy mattock'/'DKA', 'Winona Nelson').
card_number('heavy mattock'/'DKA', '150').
card_multiverse_id('heavy mattock'/'DKA', '244766').

card_in_set('heckling fiends', 'DKA').
card_original_type('heckling fiends'/'DKA', 'Creature — Devil').
card_original_text('heckling fiends'/'DKA', '{2}{R}: Target creature attacks this turn if able.').
card_first_print('heckling fiends', 'DKA').
card_image_name('heckling fiends'/'DKA', 'heckling fiends').
card_uid('heckling fiends'/'DKA', 'DKA:Heckling Fiends:heckling fiends').
card_rarity('heckling fiends'/'DKA', 'Uncommon').
card_artist('heckling fiends'/'DKA', 'Clint Cearley').
card_number('heckling fiends'/'DKA', '92').
card_flavor_text('heckling fiends'/'DKA', 'Their language may be a cacophonous agglomeration of chittering snorts and mad barking, but the message they send is all too clear.').
card_multiverse_id('heckling fiends'/'DKA', '262704').

card_in_set('hellrider', 'DKA').
card_original_type('hellrider'/'DKA', 'Creature — Devil').
card_original_text('hellrider'/'DKA', 'Haste\nWhenever a creature you control attacks, Hellrider deals 1 damage to defending player.').
card_first_print('hellrider', 'DKA').
card_image_name('hellrider'/'DKA', 'hellrider').
card_uid('hellrider'/'DKA', 'DKA:Hellrider:hellrider').
card_rarity('hellrider'/'DKA', 'Rare').
card_artist('hellrider'/'DKA', 'Svetlin Velinov').
card_number('hellrider'/'DKA', '93').
card_flavor_text('hellrider'/'DKA', '\"Behind every devil\'s mayhem lurks a demon\'s scheme.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('hellrider'/'DKA', '226874').

card_in_set('helvault', 'DKA').
card_original_type('helvault'/'DKA', 'Legendary Artifact').
card_original_text('helvault'/'DKA', '{1}, {T}: Exile target creature you control.\n{7}, {T}: Exile target creature you don\'t control.\nWhen Helvault is put into a graveyard from the battlefield, return all cards exiled with it to the battlefield under their owners\' control.').
card_first_print('helvault', 'DKA').
card_image_name('helvault'/'DKA', 'helvault').
card_uid('helvault'/'DKA', 'DKA:Helvault:helvault').
card_rarity('helvault'/'DKA', 'Mythic Rare').
card_artist('helvault'/'DKA', 'Jaime Jones').
card_number('helvault'/'DKA', '151').
card_multiverse_id('helvault'/'DKA', '262679').

card_in_set('highborn ghoul', 'DKA').
card_original_type('highborn ghoul'/'DKA', 'Creature — Zombie').
card_original_text('highborn ghoul'/'DKA', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('highborn ghoul', 'DKA').
card_image_name('highborn ghoul'/'DKA', 'highborn ghoul').
card_uid('highborn ghoul'/'DKA', 'DKA:Highborn Ghoul:highborn ghoul').
card_rarity('highborn ghoul'/'DKA', 'Common').
card_artist('highborn ghoul'/'DKA', 'Volkan Baga').
card_number('highborn ghoul'/'DKA', '68').
card_flavor_text('highborn ghoul'/'DKA', '\"All the dead sleep lightly here, no matter how refined their beds may be.\"\n—Grafdigger Wulmer').
card_multiverse_id('highborn ghoul'/'DKA', '242503').

card_in_set('hinterland hermit', 'DKA').
card_original_type('hinterland hermit'/'DKA', 'Creature — Human Werewolf').
card_original_text('hinterland hermit'/'DKA', 'At the beginning of each upkeep, if no spells were cast last turn, transform Hinterland Hermit.').
card_first_print('hinterland hermit', 'DKA').
card_image_name('hinterland hermit'/'DKA', 'hinterland hermit').
card_uid('hinterland hermit'/'DKA', 'DKA:Hinterland Hermit:hinterland hermit').
card_rarity('hinterland hermit'/'DKA', 'Common').
card_artist('hinterland hermit'/'DKA', 'Steven Belledin').
card_number('hinterland hermit'/'DKA', '94a').
card_flavor_text('hinterland hermit'/'DKA', 'He tried to live far from those he could hurt . . .').
card_multiverse_id('hinterland hermit'/'DKA', '247125').

card_in_set('hinterland scourge', 'DKA').
card_original_type('hinterland scourge'/'DKA', 'Creature — Werewolf').
card_original_text('hinterland scourge'/'DKA', 'Hinterland Scourge must be blocked if able.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Hinterland Scourge.').
card_first_print('hinterland scourge', 'DKA').
card_image_name('hinterland scourge'/'DKA', 'hinterland scourge').
card_uid('hinterland scourge'/'DKA', 'DKA:Hinterland Scourge:hinterland scourge').
card_rarity('hinterland scourge'/'DKA', 'Common').
card_artist('hinterland scourge'/'DKA', 'Steven Belledin').
card_number('hinterland scourge'/'DKA', '94b').
card_flavor_text('hinterland scourge'/'DKA', '. . . but the monster within was a tireless hunter.').
card_multiverse_id('hinterland scourge'/'DKA', '247122').

card_in_set('hollowhenge beast', 'DKA').
card_original_type('hollowhenge beast'/'DKA', 'Creature — Beast').
card_original_text('hollowhenge beast'/'DKA', '').
card_first_print('hollowhenge beast', 'DKA').
card_image_name('hollowhenge beast'/'DKA', 'hollowhenge beast').
card_uid('hollowhenge beast'/'DKA', 'DKA:Hollowhenge Beast:hollowhenge beast').
card_rarity('hollowhenge beast'/'DKA', 'Common').
card_artist('hollowhenge beast'/'DKA', 'Dave Kendall').
card_number('hollowhenge beast'/'DKA', '118').
card_flavor_text('hollowhenge beast'/'DKA', 'In a world of monsters, it\'s the stuff of nightmares.').
card_multiverse_id('hollowhenge beast'/'DKA', '262868').

card_in_set('hollowhenge spirit', 'DKA').
card_original_type('hollowhenge spirit'/'DKA', 'Creature — Spirit').
card_original_text('hollowhenge spirit'/'DKA', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Hollowhenge Spirit enters the battlefield, remove target attacking or blocking creature from combat.').
card_first_print('hollowhenge spirit', 'DKA').
card_image_name('hollowhenge spirit'/'DKA', 'hollowhenge spirit').
card_uid('hollowhenge spirit'/'DKA', 'DKA:Hollowhenge Spirit:hollowhenge spirit').
card_rarity('hollowhenge spirit'/'DKA', 'Uncommon').
card_artist('hollowhenge spirit'/'DKA', 'Lars Grant-West').
card_number('hollowhenge spirit'/'DKA', '10').
card_flavor_text('hollowhenge spirit'/'DKA', 'Some spirits mourn. Others terrorize.').
card_multiverse_id('hollowhenge spirit'/'DKA', '222001').

card_in_set('hunger of the howlpack', 'DKA').
card_original_type('hunger of the howlpack'/'DKA', 'Instant').
card_original_text('hunger of the howlpack'/'DKA', 'Put a +1/+1 counter on target creature.\nMorbid — Put three +1/+1 counters on that creature instead if a creature died this turn.').
card_first_print('hunger of the howlpack', 'DKA').
card_image_name('hunger of the howlpack'/'DKA', 'hunger of the howlpack').
card_uid('hunger of the howlpack'/'DKA', 'DKA:Hunger of the Howlpack:hunger of the howlpack').
card_rarity('hunger of the howlpack'/'DKA', 'Common').
card_artist('hunger of the howlpack'/'DKA', 'Nils Hamm').
card_number('hunger of the howlpack'/'DKA', '119').
card_flavor_text('hunger of the howlpack'/'DKA', '\"Werewolves are an unholy mix of a predator\'s instinct and a human\'s hatred.\"\n—Alena, trapper of Kessig').
card_multiverse_id('hunger of the howlpack'/'DKA', '243257').

card_in_set('huntmaster of the fells', 'DKA').
card_original_type('huntmaster of the fells'/'DKA', 'Creature — Human Werewolf').
card_original_text('huntmaster of the fells'/'DKA', 'Whenever this creature enters the battlefield or transforms into Huntmaster of the Fells, put a 2/2 green Wolf creature token onto the battlefield and you gain 2 life.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Huntmaster of the Fells.').
card_first_print('huntmaster of the fells', 'DKA').
card_image_name('huntmaster of the fells'/'DKA', 'huntmaster of the fells').
card_uid('huntmaster of the fells'/'DKA', 'DKA:Huntmaster of the Fells:huntmaster of the fells').
card_rarity('huntmaster of the fells'/'DKA', 'Mythic Rare').
card_artist('huntmaster of the fells'/'DKA', 'Chris Rahn').
card_number('huntmaster of the fells'/'DKA', '140a').
card_multiverse_id('huntmaster of the fells'/'DKA', '262875').

card_in_set('immerwolf', 'DKA').
card_original_type('immerwolf'/'DKA', 'Creature — Wolf').
card_original_text('immerwolf'/'DKA', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nOther Wolf and Werewolf creatures you control get +1/+1.\nNon-Human Werewolves you control can\'t transform.').
card_first_print('immerwolf', 'DKA').
card_image_name('immerwolf'/'DKA', 'immerwolf').
card_uid('immerwolf'/'DKA', 'DKA:Immerwolf:immerwolf').
card_rarity('immerwolf'/'DKA', 'Uncommon').
card_artist('immerwolf'/'DKA', 'Terese Nielsen').
card_number('immerwolf'/'DKA', '141').
card_multiverse_id('immerwolf'/'DKA', '262700').

card_in_set('increasing ambition', 'DKA').
card_original_type('increasing ambition'/'DKA', 'Sorcery').
card_original_text('increasing ambition'/'DKA', 'Search your library for a card and put that card into your hand. If Increasing Ambition was cast from a graveyard, instead search your library for two cards and put those cards into your hand. Then shuffle your library.\nFlashback {7}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('increasing ambition', 'DKA').
card_image_name('increasing ambition'/'DKA', 'increasing ambition').
card_uid('increasing ambition'/'DKA', 'DKA:Increasing Ambition:increasing ambition').
card_rarity('increasing ambition'/'DKA', 'Rare').
card_artist('increasing ambition'/'DKA', 'Volkan Baga').
card_number('increasing ambition'/'DKA', '69').
card_multiverse_id('increasing ambition'/'DKA', '262833').

card_in_set('increasing confusion', 'DKA').
card_original_type('increasing confusion'/'DKA', 'Sorcery').
card_original_text('increasing confusion'/'DKA', 'Target player puts the top X cards of his or her library into his or her graveyard. If Increasing Confusion was cast from a graveyard, that player puts twice that many cards into his or her graveyard instead.\nFlashback {X}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('increasing confusion', 'DKA').
card_image_name('increasing confusion'/'DKA', 'increasing confusion').
card_uid('increasing confusion'/'DKA', 'DKA:Increasing Confusion:increasing confusion').
card_rarity('increasing confusion'/'DKA', 'Rare').
card_artist('increasing confusion'/'DKA', 'Dan Scott').
card_number('increasing confusion'/'DKA', '41').
card_multiverse_id('increasing confusion'/'DKA', '262856').

card_in_set('increasing devotion', 'DKA').
card_original_type('increasing devotion'/'DKA', 'Sorcery').
card_original_text('increasing devotion'/'DKA', 'Put five 1/1 white Human creature tokens onto the battlefield. If Increasing Devotion was cast from a graveyard, put ten of those tokens onto the battlefield instead.\nFlashback {7}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('increasing devotion', 'DKA').
card_image_name('increasing devotion'/'DKA', 'increasing devotion').
card_uid('increasing devotion'/'DKA', 'DKA:Increasing Devotion:increasing devotion').
card_rarity('increasing devotion'/'DKA', 'Rare').
card_artist('increasing devotion'/'DKA', 'Daniel Ljunggren').
card_number('increasing devotion'/'DKA', '11').
card_multiverse_id('increasing devotion'/'DKA', '262870').

card_in_set('increasing savagery', 'DKA').
card_original_type('increasing savagery'/'DKA', 'Sorcery').
card_original_text('increasing savagery'/'DKA', 'Put five +1/+1 counters on target creature. If Increasing Savagery was cast from a graveyard, put ten +1/+1 counters on that creature instead.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('increasing savagery', 'DKA').
card_image_name('increasing savagery'/'DKA', 'increasing savagery').
card_uid('increasing savagery'/'DKA', 'DKA:Increasing Savagery:increasing savagery').
card_rarity('increasing savagery'/'DKA', 'Rare').
card_artist('increasing savagery'/'DKA', 'Steve Prescott').
card_number('increasing savagery'/'DKA', '120').
card_multiverse_id('increasing savagery'/'DKA', '262834').

card_in_set('increasing vengeance', 'DKA').
card_original_type('increasing vengeance'/'DKA', 'Instant').
card_original_text('increasing vengeance'/'DKA', 'Copy target instant or sorcery spell you control. If Increasing Vengeance was cast from a graveyard, copy that spell twice instead. You may choose new targets for the copies.\nFlashback {3}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('increasing vengeance', 'DKA').
card_image_name('increasing vengeance'/'DKA', 'increasing vengeance').
card_uid('increasing vengeance'/'DKA', 'DKA:Increasing Vengeance:increasing vengeance').
card_rarity('increasing vengeance'/'DKA', 'Rare').
card_artist('increasing vengeance'/'DKA', 'Anthony Francisco').
card_number('increasing vengeance'/'DKA', '95').
card_multiverse_id('increasing vengeance'/'DKA', '262661').

card_in_set('jar of eyeballs', 'DKA').
card_original_type('jar of eyeballs'/'DKA', 'Artifact').
card_original_text('jar of eyeballs'/'DKA', 'Whenever a creature you control dies, put two eyeball counters on Jar of Eyeballs.\n{3}, {T}, Remove all eyeball counters from Jar of Eyeballs: Look at the top X cards of your library, where X is the number of eyeball counters removed this way. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('jar of eyeballs', 'DKA').
card_image_name('jar of eyeballs'/'DKA', 'jar of eyeballs').
card_uid('jar of eyeballs'/'DKA', 'DKA:Jar of Eyeballs:jar of eyeballs').
card_rarity('jar of eyeballs'/'DKA', 'Rare').
card_artist('jar of eyeballs'/'DKA', 'Jaime Jones').
card_number('jar of eyeballs'/'DKA', '152').
card_multiverse_id('jar of eyeballs'/'DKA', '244695').

card_in_set('kessig recluse', 'DKA').
card_original_type('kessig recluse'/'DKA', 'Creature — Spider').
card_original_text('kessig recluse'/'DKA', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('kessig recluse', 'DKA').
card_image_name('kessig recluse'/'DKA', 'kessig recluse').
card_uid('kessig recluse'/'DKA', 'DKA:Kessig Recluse:kessig recluse').
card_rarity('kessig recluse'/'DKA', 'Common').
card_artist('kessig recluse'/'DKA', 'Vincent Proce').
card_number('kessig recluse'/'DKA', '121').
card_flavor_text('kessig recluse'/'DKA', 'Its silk is highly prized, but then again, so is your life.').
card_multiverse_id('kessig recluse'/'DKA', '243233').

card_in_set('krallenhorde killer', 'DKA').
card_original_type('krallenhorde killer'/'DKA', 'Creature — Werewolf').
card_original_text('krallenhorde killer'/'DKA', '{3}{G}: Krallenhorde Killer gets +4/+4 until end of turn. Activate this ability only once each turn.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Krallenhorde Killer.').
card_first_print('krallenhorde killer', 'DKA').
card_image_name('krallenhorde killer'/'DKA', 'krallenhorde killer').
card_uid('krallenhorde killer'/'DKA', 'DKA:Krallenhorde Killer:krallenhorde killer').
card_rarity('krallenhorde killer'/'DKA', 'Rare').
card_artist('krallenhorde killer'/'DKA', 'Zoltan Boros').
card_number('krallenhorde killer'/'DKA', '133b').
card_multiverse_id('krallenhorde killer'/'DKA', '253431').

card_in_set('lambholt elder', 'DKA').
card_original_type('lambholt elder'/'DKA', 'Creature — Human Werewolf').
card_original_text('lambholt elder'/'DKA', 'At the beginning of each upkeep, if no spells were cast last turn, transform Lambholt Elder.').
card_first_print('lambholt elder', 'DKA').
card_image_name('lambholt elder'/'DKA', 'lambholt elder').
card_uid('lambholt elder'/'DKA', 'DKA:Lambholt Elder:lambholt elder').
card_rarity('lambholt elder'/'DKA', 'Uncommon').
card_artist('lambholt elder'/'DKA', 'Matt Stewart').
card_number('lambholt elder'/'DKA', '122a').
card_flavor_text('lambholt elder'/'DKA', '\"Be wary of the seemingly gentle souls. The weak here were slaughtered long ago.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('lambholt elder'/'DKA', '242537').

card_in_set('lingering souls', 'DKA').
card_original_type('lingering souls'/'DKA', 'Sorcery').
card_original_text('lingering souls'/'DKA', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.\nFlashback {1}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('lingering souls'/'DKA', 'lingering souls').
card_uid('lingering souls'/'DKA', 'DKA:Lingering Souls:lingering souls').
card_rarity('lingering souls'/'DKA', 'Uncommon').
card_artist('lingering souls'/'DKA', 'Bud Cook').
card_number('lingering souls'/'DKA', '12').
card_flavor_text('lingering souls'/'DKA', 'The murdered inhabitants of Hollowhenge impart to the living the terror they felt in death.').
card_multiverse_id('lingering souls'/'DKA', '262695').

card_in_set('lost in the woods', 'DKA').
card_original_type('lost in the woods'/'DKA', 'Enchantment').
card_original_text('lost in the woods'/'DKA', 'Whenever a creature attacks you or a planeswalker you control, reveal the top card of your library. If it\'s a Forest card, remove that creature from combat. Then put the revealed card on the bottom of your library.').
card_first_print('lost in the woods', 'DKA').
card_image_name('lost in the woods'/'DKA', 'lost in the woods').
card_uid('lost in the woods'/'DKA', 'DKA:Lost in the Woods:lost in the woods').
card_rarity('lost in the woods'/'DKA', 'Rare').
card_artist('lost in the woods'/'DKA', 'Matt Stewart').
card_number('lost in the woods'/'DKA', '123').
card_multiverse_id('lost in the woods'/'DKA', '242539').

card_in_set('loyal cathar', 'DKA').
card_original_type('loyal cathar'/'DKA', 'Creature — Human Soldier').
card_original_text('loyal cathar'/'DKA', 'Vigilance\nWhen Loyal Cathar dies, return it to the battlefield transformed under your control at the beginning of the next end step.').
card_first_print('loyal cathar', 'DKA').
card_image_name('loyal cathar'/'DKA', 'loyal cathar').
card_uid('loyal cathar'/'DKA', 'DKA:Loyal Cathar:loyal cathar').
card_rarity('loyal cathar'/'DKA', 'Common').
card_artist('loyal cathar'/'DKA', 'Ryan Pancoast').
card_number('loyal cathar'/'DKA', '13a').
card_flavor_text('loyal cathar'/'DKA', '\"I trust myself to do my duty, even unto death. It\'s what comes after that I\'m afraid of.\"\n—Constable Visil, to Thalia').
card_multiverse_id('loyal cathar'/'DKA', '244724').

card_in_set('markov blademaster', 'DKA').
card_original_type('markov blademaster'/'DKA', 'Creature — Vampire Warrior').
card_original_text('markov blademaster'/'DKA', 'Double strike\nWhenever Markov Blademaster deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('markov blademaster', 'DKA').
card_image_name('markov blademaster'/'DKA', 'markov blademaster').
card_uid('markov blademaster'/'DKA', 'DKA:Markov Blademaster:markov blademaster').
card_rarity('markov blademaster'/'DKA', 'Rare').
card_artist('markov blademaster'/'DKA', 'Jana Schirmer & Johannes Voss').
card_number('markov blademaster'/'DKA', '96').
card_flavor_text('markov blademaster'/'DKA', '\"Mortals practice swordplay for a few decades at best. How can they ever attain the exquisite mastery that eternity has to offer?\"').
card_multiverse_id('markov blademaster'/'DKA', '249419').

card_in_set('markov warlord', 'DKA').
card_original_type('markov warlord'/'DKA', 'Creature — Vampire Warrior').
card_original_text('markov warlord'/'DKA', 'Haste\nWhen Markov Warlord enters the battlefield, up to two target creatures can\'t block this turn.').
card_first_print('markov warlord', 'DKA').
card_image_name('markov warlord'/'DKA', 'markov warlord').
card_uid('markov warlord'/'DKA', 'DKA:Markov Warlord:markov warlord').
card_rarity('markov warlord'/'DKA', 'Uncommon').
card_artist('markov warlord'/'DKA', 'Cynthia Sheppard').
card_number('markov warlord'/'DKA', '97').
card_flavor_text('markov warlord'/'DKA', '\"What use is a stake or holy symbol in the hands of a coward?\"').
card_multiverse_id('markov warlord'/'DKA', '262693').

card_in_set('markov\'s servant', 'DKA').
card_original_type('markov\'s servant'/'DKA', 'Creature — Vampire').
card_original_text('markov\'s servant'/'DKA', '').
card_first_print('markov\'s servant', 'DKA').
card_image_name('markov\'s servant'/'DKA', 'markov\'s servant').
card_uid('markov\'s servant'/'DKA', 'DKA:Markov\'s Servant:markov\'s servant').
card_rarity('markov\'s servant'/'DKA', 'Common').
card_artist('markov\'s servant'/'DKA', 'Steve Argyle').
card_number('markov\'s servant'/'DKA', '55b').
card_flavor_text('markov\'s servant'/'DKA', '\". . . and for that I shall be eternally grateful.\"').
card_multiverse_id('markov\'s servant'/'DKA', '244712').

card_in_set('midnight guard', 'DKA').
card_original_type('midnight guard'/'DKA', 'Creature — Human Soldier').
card_original_text('midnight guard'/'DKA', 'Whenever another creature enters the battlefield, untap Midnight Guard.').
card_first_print('midnight guard', 'DKA').
card_image_name('midnight guard'/'DKA', 'midnight guard').
card_uid('midnight guard'/'DKA', 'DKA:Midnight Guard:midnight guard').
card_rarity('midnight guard'/'DKA', 'Common').
card_artist('midnight guard'/'DKA', 'Jason A. Engle').
card_number('midnight guard'/'DKA', '14').
card_flavor_text('midnight guard'/'DKA', '\"When you\'re on watch, no noise is harmless and no shadow can be ignored.\"\n—Olgard of the Skiltfolk').
card_multiverse_id('midnight guard'/'DKA', '262859').

card_in_set('mikaeus, the unhallowed', 'DKA').
card_original_type('mikaeus, the unhallowed'/'DKA', 'Legendary Creature — Zombie Cleric').
card_original_text('mikaeus, the unhallowed'/'DKA', 'Intimidate\nWhenever a Human deals damage to you, destroy it.\nOther non-Human creatures you control get +1/+1 and have undying. (When a creature with undying dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('mikaeus, the unhallowed', 'DKA').
card_image_name('mikaeus, the unhallowed'/'DKA', 'mikaeus, the unhallowed').
card_uid('mikaeus, the unhallowed'/'DKA', 'DKA:Mikaeus, the Unhallowed:mikaeus, the unhallowed').
card_rarity('mikaeus, the unhallowed'/'DKA', 'Mythic Rare').
card_artist('mikaeus, the unhallowed'/'DKA', 'Chris Rahn').
card_number('mikaeus, the unhallowed'/'DKA', '70').
card_multiverse_id('mikaeus, the unhallowed'/'DKA', '249896').

card_in_set('mondronen shaman', 'DKA').
card_original_type('mondronen shaman'/'DKA', 'Creature — Human Werewolf Shaman').
card_original_text('mondronen shaman'/'DKA', 'At the beginning of each upkeep, if no spells were cast last turn, transform Mondronen Shaman.').
card_image_name('mondronen shaman'/'DKA', 'mondronen shaman').
card_uid('mondronen shaman'/'DKA', 'DKA:Mondronen Shaman:mondronen shaman').
card_rarity('mondronen shaman'/'DKA', 'Rare').
card_artist('mondronen shaman'/'DKA', 'Mike Sass').
card_number('mondronen shaman'/'DKA', '98a').
card_flavor_text('mondronen shaman'/'DKA', '\"Tovolar, my master. Gather the howlpack, for the Hunter\'s Moon is nigh and Thraben\'s walls grow weak.\"').
card_multiverse_id('mondronen shaman'/'DKA', '253433').

card_in_set('moonscarred werewolf', 'DKA').
card_original_type('moonscarred werewolf'/'DKA', 'Creature — Werewolf').
card_original_text('moonscarred werewolf'/'DKA', 'Vigilance\n{T}: Add {G}{G} to your mana pool.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Moonscarred Werewolf.').
card_first_print('moonscarred werewolf', 'DKA').
card_image_name('moonscarred werewolf'/'DKA', 'moonscarred werewolf').
card_uid('moonscarred werewolf'/'DKA', 'DKA:Moonscarred Werewolf:moonscarred werewolf').
card_rarity('moonscarred werewolf'/'DKA', 'Common').
card_artist('moonscarred werewolf'/'DKA', 'Cynthia Sheppard').
card_number('moonscarred werewolf'/'DKA', '125b').
card_flavor_text('moonscarred werewolf'/'DKA', '\". . . and I will bring the fury of the wild back to my village.\"').
card_multiverse_id('moonscarred werewolf'/'DKA', '262659').

card_in_set('moonveil dragon', 'DKA').
card_original_type('moonveil dragon'/'DKA', 'Creature — Dragon').
card_original_text('moonveil dragon'/'DKA', 'Flying\n{R}: Each creature you control gets +1/+0 until end of turn.').
card_first_print('moonveil dragon', 'DKA').
card_image_name('moonveil dragon'/'DKA', 'moonveil dragon').
card_uid('moonveil dragon'/'DKA', 'DKA:Moonveil Dragon:moonveil dragon').
card_rarity('moonveil dragon'/'DKA', 'Mythic Rare').
card_artist('moonveil dragon'/'DKA', 'Todd Lockwood').
card_number('moonveil dragon'/'DKA', '99').
card_flavor_text('moonveil dragon'/'DKA', '\"Their hearts beat, their lungs draw breath, and they have one true form. I count them as allies of the living.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('moonveil dragon'/'DKA', '249885').

card_in_set('mystic retrieval', 'DKA').
card_original_type('mystic retrieval'/'DKA', 'Sorcery').
card_original_text('mystic retrieval'/'DKA', 'Return target instant or sorcery card from your graveyard to your hand.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('mystic retrieval', 'DKA').
card_image_name('mystic retrieval'/'DKA', 'mystic retrieval').
card_uid('mystic retrieval'/'DKA', 'DKA:Mystic Retrieval:mystic retrieval').
card_rarity('mystic retrieval'/'DKA', 'Uncommon').
card_artist('mystic retrieval'/'DKA', 'Scott Chou').
card_number('mystic retrieval'/'DKA', '42').
card_multiverse_id('mystic retrieval'/'DKA', '262678').

card_in_set('nearheath stalker', 'DKA').
card_original_type('nearheath stalker'/'DKA', 'Creature — Vampire Rogue').
card_original_text('nearheath stalker'/'DKA', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_image_name('nearheath stalker'/'DKA', 'nearheath stalker').
card_uid('nearheath stalker'/'DKA', 'DKA:Nearheath Stalker:nearheath stalker').
card_rarity('nearheath stalker'/'DKA', 'Common').
card_artist('nearheath stalker'/'DKA', 'Michael C. Hayes').
card_number('nearheath stalker'/'DKA', '100').
card_flavor_text('nearheath stalker'/'DKA', '\"Do not speak of this to the elders. They look unfavorably upon my indulgences.\"').
card_multiverse_id('nearheath stalker'/'DKA', '262681').

card_in_set('nephalia seakite', 'DKA').
card_original_type('nephalia seakite'/'DKA', 'Creature — Bird').
card_original_text('nephalia seakite'/'DKA', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_first_print('nephalia seakite', 'DKA').
card_image_name('nephalia seakite'/'DKA', 'nephalia seakite').
card_uid('nephalia seakite'/'DKA', 'DKA:Nephalia Seakite:nephalia seakite').
card_rarity('nephalia seakite'/'DKA', 'Common').
card_artist('nephalia seakite'/'DKA', 'Wayne England').
card_number('nephalia seakite'/'DKA', '43').
card_flavor_text('nephalia seakite'/'DKA', '\"Keep one eye on the cliff road or you may fall to your death. Keep one eye on the sky or your death may fall on you.\"\n—Manfried Ulmach, Elgaud Master-at-Arms').
card_multiverse_id('nephalia seakite'/'DKA', '247126').

card_in_set('niblis of the breath', 'DKA').
card_original_type('niblis of the breath'/'DKA', 'Creature — Spirit').
card_original_text('niblis of the breath'/'DKA', 'Flying\n{U}, {T}: You may tap or untap target creature.').
card_first_print('niblis of the breath', 'DKA').
card_image_name('niblis of the breath'/'DKA', 'niblis of the breath').
card_uid('niblis of the breath'/'DKA', 'DKA:Niblis of the Breath:niblis of the breath').
card_rarity('niblis of the breath'/'DKA', 'Uncommon').
card_artist('niblis of the breath'/'DKA', 'Igor Kieryluk').
card_number('niblis of the breath'/'DKA', '44').
card_flavor_text('niblis of the breath'/'DKA', '\"Why do the dead remain among us? Where is Flight Alabaster to lead them home? Where is Avacyn?\"\n—Eruth of Lambholt').
card_multiverse_id('niblis of the breath'/'DKA', '226724').

card_in_set('niblis of the mist', 'DKA').
card_original_type('niblis of the mist'/'DKA', 'Creature — Spirit').
card_original_text('niblis of the mist'/'DKA', 'Flying\nWhen Niblis of the Mist enters the battlefield, you may tap target creature.').
card_first_print('niblis of the mist', 'DKA').
card_image_name('niblis of the mist'/'DKA', 'niblis of the mist').
card_uid('niblis of the mist'/'DKA', 'DKA:Niblis of the Mist:niblis of the mist').
card_rarity('niblis of the mist'/'DKA', 'Common').
card_artist('niblis of the mist'/'DKA', 'Igor Kieryluk').
card_number('niblis of the mist'/'DKA', '15').
card_flavor_text('niblis of the mist'/'DKA', '\"Not even a roaring fire could thaw the chill it put in my heart.\"\n—Vonn, geist-trapper').
card_multiverse_id('niblis of the mist'/'DKA', '245292').

card_in_set('niblis of the urn', 'DKA').
card_original_type('niblis of the urn'/'DKA', 'Creature — Spirit').
card_original_text('niblis of the urn'/'DKA', 'Flying\nWhenever Niblis of the Urn attacks, you may tap target creature.').
card_first_print('niblis of the urn', 'DKA').
card_image_name('niblis of the urn'/'DKA', 'niblis of the urn').
card_uid('niblis of the urn'/'DKA', 'DKA:Niblis of the Urn:niblis of the urn').
card_rarity('niblis of the urn'/'DKA', 'Uncommon').
card_artist('niblis of the urn'/'DKA', 'Igor Kieryluk').
card_number('niblis of the urn'/'DKA', '16').
card_flavor_text('niblis of the urn'/'DKA', '\"I never had the nerve to open that bottle. I\'ll bet you\'re wishing right now you hadn\'t been so curious yourself.\"\n—Old Rutstein').
card_multiverse_id('niblis of the urn'/'DKA', '244744').

card_in_set('predator ooze', 'DKA').
card_original_type('predator ooze'/'DKA', 'Creature — Ooze').
card_original_text('predator ooze'/'DKA', 'Predator Ooze is indestructible.\nWhenever Predator Ooze attacks, put a +1/+1 counter on it.\nWhenever a creature dealt damage by Predator Ooze this turn dies, put a +1/+1 counter on Predator Ooze.').
card_first_print('predator ooze', 'DKA').
card_image_name('predator ooze'/'DKA', 'predator ooze').
card_uid('predator ooze'/'DKA', 'DKA:Predator Ooze:predator ooze').
card_rarity('predator ooze'/'DKA', 'Rare').
card_artist('predator ooze'/'DKA', 'Ryan Yee').
card_number('predator ooze'/'DKA', '124').
card_multiverse_id('predator ooze'/'DKA', '262866').

card_in_set('pyreheart wolf', 'DKA').
card_original_type('pyreheart wolf'/'DKA', 'Creature — Wolf').
card_original_text('pyreheart wolf'/'DKA', 'Whenever Pyreheart Wolf attacks, each creature you control can\'t be blocked this turn except by two or more creatures.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('pyreheart wolf', 'DKA').
card_image_name('pyreheart wolf'/'DKA', 'pyreheart wolf').
card_uid('pyreheart wolf'/'DKA', 'DKA:Pyreheart Wolf:pyreheart wolf').
card_rarity('pyreheart wolf'/'DKA', 'Uncommon').
card_artist('pyreheart wolf'/'DKA', 'Lars Grant-West').
card_number('pyreheart wolf'/'DKA', '101').
card_multiverse_id('pyreheart wolf'/'DKA', '249983').

card_in_set('ravager of the fells', 'DKA').
card_original_type('ravager of the fells'/'DKA', 'Creature — Werewolf').
card_original_text('ravager of the fells'/'DKA', 'Trample\nWhenever this creature transforms into Ravager of the Fells, it deals 2 damage to target opponent and 2 damage to up to one target creature that player controls.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ravager of the Fells.').
card_first_print('ravager of the fells', 'DKA').
card_image_name('ravager of the fells'/'DKA', 'ravager of the fells').
card_uid('ravager of the fells'/'DKA', 'DKA:Ravager of the Fells:ravager of the fells').
card_rarity('ravager of the fells'/'DKA', 'Mythic Rare').
card_artist('ravager of the fells'/'DKA', 'Chris Rahn').
card_number('ravager of the fells'/'DKA', '140b').
card_multiverse_id('ravager of the fells'/'DKA', '262699').

card_in_set('ravenous demon', 'DKA').
card_original_type('ravenous demon'/'DKA', 'Creature — Demon').
card_original_text('ravenous demon'/'DKA', 'Sacrifice a Human: Transform Ravenous Demon. Activate this ability only any time you could cast a sorcery.').
card_image_name('ravenous demon'/'DKA', 'ravenous demon').
card_uid('ravenous demon'/'DKA', 'DKA:Ravenous Demon:ravenous demon').
card_rarity('ravenous demon'/'DKA', 'Rare').
card_artist('ravenous demon'/'DKA', 'Igor Kieryluk').
card_number('ravenous demon'/'DKA', '71a').
card_flavor_text('ravenous demon'/'DKA', 'His twisted words entice the weak.').
card_multiverse_id('ravenous demon'/'DKA', '227417').

card_in_set('ray of revelation', 'DKA').
card_original_type('ray of revelation'/'DKA', 'Instant').
card_original_text('ray of revelation'/'DKA', 'Destroy target enchantment.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('ray of revelation'/'DKA', 'ray of revelation').
card_uid('ray of revelation'/'DKA', 'DKA:Ray of Revelation:ray of revelation').
card_rarity('ray of revelation'/'DKA', 'Common').
card_artist('ray of revelation'/'DKA', 'Cliff Childs').
card_number('ray of revelation'/'DKA', '17').
card_flavor_text('ray of revelation'/'DKA', '\"May angels carry him into the Blessed Sleep with dreams of sunlit days and moonless nights.\"\n—Tomb inscription of Saint Raban').
card_multiverse_id('ray of revelation'/'DKA', '245288').

card_in_set('reap the seagraf', 'DKA').
card_original_type('reap the seagraf'/'DKA', 'Sorcery').
card_original_text('reap the seagraf'/'DKA', 'Put a 2/2 black Zombie creature token onto the battlefield.\nFlashback {4}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('reap the seagraf', 'DKA').
card_image_name('reap the seagraf'/'DKA', 'reap the seagraf').
card_uid('reap the seagraf'/'DKA', 'DKA:Reap the Seagraf:reap the seagraf').
card_rarity('reap the seagraf'/'DKA', 'Common').
card_artist('reap the seagraf'/'DKA', 'James Ryman').
card_number('reap the seagraf'/'DKA', '72').
card_flavor_text('reap the seagraf'/'DKA', 'The wreck hung on the jagged reef, each night disgorging more of its gruesome cargo.').
card_multiverse_id('reap the seagraf'/'DKA', '244759').

card_in_set('relentless skaabs', 'DKA').
card_original_type('relentless skaabs'/'DKA', 'Creature — Zombie').
card_original_text('relentless skaabs'/'DKA', 'As an additional cost to cast Relentless Skaabs, exile a creature card from your graveyard.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('relentless skaabs', 'DKA').
card_image_name('relentless skaabs'/'DKA', 'relentless skaabs').
card_uid('relentless skaabs'/'DKA', 'DKA:Relentless Skaabs:relentless skaabs').
card_rarity('relentless skaabs'/'DKA', 'Uncommon').
card_artist('relentless skaabs'/'DKA', 'Karl Kopinski').
card_number('relentless skaabs'/'DKA', '45').
card_multiverse_id('relentless skaabs'/'DKA', '262701').

card_in_set('requiem angel', 'DKA').
card_original_type('requiem angel'/'DKA', 'Creature — Angel').
card_original_text('requiem angel'/'DKA', 'Flying\nWhenever another non-Spirit creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('requiem angel', 'DKA').
card_image_name('requiem angel'/'DKA', 'requiem angel').
card_uid('requiem angel'/'DKA', 'DKA:Requiem Angel:requiem angel').
card_rarity('requiem angel'/'DKA', 'Rare').
card_artist('requiem angel'/'DKA', 'Eric Deschamps').
card_number('requiem angel'/'DKA', '18').
card_flavor_text('requiem angel'/'DKA', 'When angels despair, what hope can remain for mortals?').
card_multiverse_id('requiem angel'/'DKA', '270779').

card_in_set('russet wolves', 'DKA').
card_original_type('russet wolves'/'DKA', 'Creature — Wolf').
card_original_text('russet wolves'/'DKA', '').
card_first_print('russet wolves', 'DKA').
card_image_name('russet wolves'/'DKA', 'russet wolves').
card_uid('russet wolves'/'DKA', 'DKA:Russet Wolves:russet wolves').
card_rarity('russet wolves'/'DKA', 'Common').
card_artist('russet wolves'/'DKA', 'Christopher Moeller').
card_number('russet wolves'/'DKA', '102').
card_flavor_text('russet wolves'/'DKA', '\"The wolves of our valley are bred to detest the scent of ghouls. For centuries they have kept our estates clear of such carrion.\"\n—Olivia Voldaren').
card_multiverse_id('russet wolves'/'DKA', '244726').

card_in_set('sanctuary cat', 'DKA').
card_original_type('sanctuary cat'/'DKA', 'Creature — Cat').
card_original_text('sanctuary cat'/'DKA', '').
card_first_print('sanctuary cat', 'DKA').
card_image_name('sanctuary cat'/'DKA', 'sanctuary cat').
card_uid('sanctuary cat'/'DKA', 'DKA:Sanctuary Cat:sanctuary cat').
card_rarity('sanctuary cat'/'DKA', 'Common').
card_artist('sanctuary cat'/'DKA', 'David Palumbo').
card_number('sanctuary cat'/'DKA', '19').
card_flavor_text('sanctuary cat'/'DKA', 'Cats prowl the corridors of Avacyn\'s churches in search of devils or signs of their mischief.').
card_multiverse_id('sanctuary cat'/'DKA', '262864').

card_in_set('saving grasp', 'DKA').
card_original_type('saving grasp'/'DKA', 'Instant').
card_original_text('saving grasp'/'DKA', 'Return target creature you own to your hand.\nFlashback {W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('saving grasp', 'DKA').
card_image_name('saving grasp'/'DKA', 'saving grasp').
card_uid('saving grasp'/'DKA', 'DKA:Saving Grasp:saving grasp').
card_rarity('saving grasp'/'DKA', 'Common').
card_artist('saving grasp'/'DKA', 'Matt Stewart').
card_number('saving grasp'/'DKA', '46').
card_flavor_text('saving grasp'/'DKA', 'Nothing\'s more valuable than perfect timing.').
card_multiverse_id('saving grasp'/'DKA', '245296').

card_in_set('scorch the fields', 'DKA').
card_original_type('scorch the fields'/'DKA', 'Sorcery').
card_original_text('scorch the fields'/'DKA', 'Destroy target land. Scorch the Fields deals 1 damage to each Human creature.').
card_first_print('scorch the fields', 'DKA').
card_image_name('scorch the fields'/'DKA', 'scorch the fields').
card_uid('scorch the fields'/'DKA', 'DKA:Scorch the Fields:scorch the fields').
card_rarity('scorch the fields'/'DKA', 'Common').
card_artist('scorch the fields'/'DKA', 'Jaime Jones').
card_number('scorch the fields'/'DKA', '103').
card_flavor_text('scorch the fields'/'DKA', '\"Your fields will turn as black as a ghoul\'s tongue and fire shall be your only harvest.\"\n—Minaldra, the Vizag Atum').
card_multiverse_id('scorch the fields'/'DKA', '262828').

card_in_set('scorned villager', 'DKA').
card_original_type('scorned villager'/'DKA', 'Creature — Human Werewolf').
card_original_text('scorned villager'/'DKA', '{T}: Add {G} to your mana pool.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Scorned Villager.').
card_first_print('scorned villager', 'DKA').
card_image_name('scorned villager'/'DKA', 'scorned villager').
card_uid('scorned villager'/'DKA', 'DKA:Scorned Villager:scorned villager').
card_rarity('scorned villager'/'DKA', 'Common').
card_artist('scorned villager'/'DKA', 'Cynthia Sheppard').
card_number('scorned villager'/'DKA', '125a').
card_flavor_text('scorned villager'/'DKA', '\"My village\'s fear drove me into the wild . . .\"').
card_multiverse_id('scorned villager'/'DKA', '262694').

card_in_set('screeching skaab', 'DKA').
card_original_type('screeching skaab'/'DKA', 'Creature — Zombie').
card_original_text('screeching skaab'/'DKA', 'When Screeching Skaab enters the battlefield, put the top two cards of your library into your graveyard.').
card_first_print('screeching skaab', 'DKA').
card_image_name('screeching skaab'/'DKA', 'screeching skaab').
card_uid('screeching skaab'/'DKA', 'DKA:Screeching Skaab:screeching skaab').
card_rarity('screeching skaab'/'DKA', 'Common').
card_artist('screeching skaab'/'DKA', 'Clint Cearley').
card_number('screeching skaab'/'DKA', '47').
card_flavor_text('screeching skaab'/'DKA', 'Its screeching is the sound of you losing your mind.').
card_multiverse_id('screeching skaab'/'DKA', '245207').

card_in_set('séance', 'DKA').
card_original_type('séance'/'DKA', 'Enchantment').
card_original_text('séance'/'DKA', 'At the beginning of each upkeep, you may exile target creature card from your graveyard. If you do, put a token onto the battlefield that\'s a copy of that card except it\'s a Spirit in addition to its other types. Exile it at the beginning of the next end step.').
card_first_print('séance', 'DKA').
card_image_name('séance'/'DKA', 'seance').
card_uid('séance'/'DKA', 'DKA:Séance:seance').
card_rarity('séance'/'DKA', 'Rare').
card_artist('séance'/'DKA', 'David Rapoza').
card_number('séance'/'DKA', '20').
card_multiverse_id('séance'/'DKA', '249422').

card_in_set('secrets of the dead', 'DKA').
card_original_type('secrets of the dead'/'DKA', 'Enchantment').
card_original_text('secrets of the dead'/'DKA', 'Whenever you cast a spell from your graveyard, draw a card.').
card_first_print('secrets of the dead', 'DKA').
card_image_name('secrets of the dead'/'DKA', 'secrets of the dead').
card_uid('secrets of the dead'/'DKA', 'DKA:Secrets of the Dead:secrets of the dead').
card_rarity('secrets of the dead'/'DKA', 'Uncommon').
card_artist('secrets of the dead'/'DKA', 'Eytan Zana').
card_number('secrets of the dead'/'DKA', '48').
card_flavor_text('secrets of the dead'/'DKA', '\"The veil between the living and the dead is so very thin that the whispers of the ancients can finally be heard.\"\n—Amalric of Midvast Hall').
card_multiverse_id('secrets of the dead'/'DKA', '230621').

card_in_set('shattered perception', 'DKA').
card_original_type('shattered perception'/'DKA', 'Sorcery').
card_original_text('shattered perception'/'DKA', 'Discard all the cards in your hand, then draw that many cards.\nFlashback {5}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('shattered perception', 'DKA').
card_image_name('shattered perception'/'DKA', 'shattered perception').
card_uid('shattered perception'/'DKA', 'DKA:Shattered Perception:shattered perception').
card_rarity('shattered perception'/'DKA', 'Uncommon').
card_artist('shattered perception'/'DKA', 'Terese Nielsen').
card_number('shattered perception'/'DKA', '104').
card_flavor_text('shattered perception'/'DKA', '\"You must shatter the fetters of the past. Only then can you truly act.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('shattered perception'/'DKA', '254058').

card_in_set('shriekgeist', 'DKA').
card_original_type('shriekgeist'/'DKA', 'Creature — Spirit').
card_original_text('shriekgeist'/'DKA', 'Flying\nWhenever Shriekgeist deals combat damage to a player, that player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('shriekgeist', 'DKA').
card_image_name('shriekgeist'/'DKA', 'shriekgeist').
card_uid('shriekgeist'/'DKA', 'DKA:Shriekgeist:shriekgeist').
card_rarity('shriekgeist'/'DKA', 'Common').
card_artist('shriekgeist'/'DKA', 'Raymond Swanland').
card_number('shriekgeist'/'DKA', '49').
card_flavor_text('shriekgeist'/'DKA', 'Wounds of the flesh are easy to heal. Wounds of the mind may never be undone.').
card_multiverse_id('shriekgeist'/'DKA', '242525').

card_in_set('sightless ghoul', 'DKA').
card_original_type('sightless ghoul'/'DKA', 'Creature — Zombie Soldier').
card_original_text('sightless ghoul'/'DKA', 'Sightless Ghoul can\'t block.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('sightless ghoul', 'DKA').
card_image_name('sightless ghoul'/'DKA', 'sightless ghoul').
card_uid('sightless ghoul'/'DKA', 'DKA:Sightless Ghoul:sightless ghoul').
card_rarity('sightless ghoul'/'DKA', 'Common').
card_artist('sightless ghoul'/'DKA', 'Svetlin Velinov').
card_number('sightless ghoul'/'DKA', '73').
card_flavor_text('sightless ghoul'/'DKA', 'It blindly marches on Thraben\'s gates, guided by its master\'s direful will.').
card_multiverse_id('sightless ghoul'/'DKA', '262682').

card_in_set('silverclaw griffin', 'DKA').
card_original_type('silverclaw griffin'/'DKA', 'Creature — Griffin').
card_original_text('silverclaw griffin'/'DKA', 'Flying, first strike').
card_first_print('silverclaw griffin', 'DKA').
card_image_name('silverclaw griffin'/'DKA', 'silverclaw griffin').
card_uid('silverclaw griffin'/'DKA', 'DKA:Silverclaw Griffin:silverclaw griffin').
card_rarity('silverclaw griffin'/'DKA', 'Common').
card_artist('silverclaw griffin'/'DKA', 'Daniel Ljunggren').
card_number('silverclaw griffin'/'DKA', '21').
card_flavor_text('silverclaw griffin'/'DKA', 'It patrols the grafs for miles around Thraben, ensuring that the unhallowed do not wake for long.').
card_multiverse_id('silverclaw griffin'/'DKA', '262876').

card_in_set('silverpelt werewolf', 'DKA').
card_original_type('silverpelt werewolf'/'DKA', 'Creature — Werewolf').
card_original_text('silverpelt werewolf'/'DKA', 'Whenever Silverpelt Werewolf deals combat damage to a player, draw a card.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Silverpelt Werewolf.').
card_first_print('silverpelt werewolf', 'DKA').
card_image_name('silverpelt werewolf'/'DKA', 'silverpelt werewolf').
card_uid('silverpelt werewolf'/'DKA', 'DKA:Silverpelt Werewolf:silverpelt werewolf').
card_rarity('silverpelt werewolf'/'DKA', 'Uncommon').
card_artist('silverpelt werewolf'/'DKA', 'Matt Stewart').
card_number('silverpelt werewolf'/'DKA', '122b').
card_multiverse_id('silverpelt werewolf'/'DKA', '242509').

card_in_set('skillful lunge', 'DKA').
card_original_type('skillful lunge'/'DKA', 'Instant').
card_original_text('skillful lunge'/'DKA', 'Target creature gets +2/+0 and gains first strike until end of turn.').
card_first_print('skillful lunge', 'DKA').
card_image_name('skillful lunge'/'DKA', 'skillful lunge').
card_uid('skillful lunge'/'DKA', 'DKA:Skillful Lunge:skillful lunge').
card_rarity('skillful lunge'/'DKA', 'Common').
card_artist('skillful lunge'/'DKA', 'Jason Felix').
card_number('skillful lunge'/'DKA', '22').
card_flavor_text('skillful lunge'/'DKA', '\"Valor is a currency of the living that serves us well.\"\n—Cosper Lowe of the Silbern Guard').
card_multiverse_id('skillful lunge'/'DKA', '262877').

card_in_set('skirsdag flayer', 'DKA').
card_original_type('skirsdag flayer'/'DKA', 'Creature — Human Cleric').
card_original_text('skirsdag flayer'/'DKA', '{3}{B}, {T}, Sacrifice a Human: Destroy target creature.').
card_first_print('skirsdag flayer', 'DKA').
card_image_name('skirsdag flayer'/'DKA', 'skirsdag flayer').
card_uid('skirsdag flayer'/'DKA', 'DKA:Skirsdag Flayer:skirsdag flayer').
card_rarity('skirsdag flayer'/'DKA', 'Uncommon').
card_artist('skirsdag flayer'/'DKA', 'Austin Hsu').
card_number('skirsdag flayer'/'DKA', '74').
card_flavor_text('skirsdag flayer'/'DKA', 'Certain initiates into the Skirsdag cult take a vow of blindness, pledging obedience to none but demonkind.').
card_multiverse_id('skirsdag flayer'/'DKA', '262667').

card_in_set('somberwald dryad', 'DKA').
card_original_type('somberwald dryad'/'DKA', 'Creature — Dryad').
card_original_text('somberwald dryad'/'DKA', 'Forestwalk (This creature is unblockable as long as defending player controls a Forest.)').
card_first_print('somberwald dryad', 'DKA').
card_image_name('somberwald dryad'/'DKA', 'somberwald dryad').
card_uid('somberwald dryad'/'DKA', 'DKA:Somberwald Dryad:somberwald dryad').
card_rarity('somberwald dryad'/'DKA', 'Common').
card_artist('somberwald dryad'/'DKA', 'Jaime Jones').
card_number('somberwald dryad'/'DKA', '126').
card_flavor_text('somberwald dryad'/'DKA', 'The Somberwald is her domain, and she has yet to hear an acceptable excuse for trespassing.').
card_multiverse_id('somberwald dryad'/'DKA', '244768').

card_in_set('sorin, lord of innistrad', 'DKA').
card_original_type('sorin, lord of innistrad'/'DKA', 'Planeswalker — Sorin').
card_original_text('sorin, lord of innistrad'/'DKA', '+1: Put a 1/1 black Vampire creature token with lifelink onto the battlefield.\n-2: You get an emblem with \"Creatures you control get +1/+0.\"\n-6: Destroy up to three target creatures and/or other planeswalkers. Return each card put into a graveyard this way to the battlefield under your control.').
card_first_print('sorin, lord of innistrad', 'DKA').
card_image_name('sorin, lord of innistrad'/'DKA', 'sorin, lord of innistrad').
card_uid('sorin, lord of innistrad'/'DKA', 'DKA:Sorin, Lord of Innistrad:sorin, lord of innistrad').
card_rarity('sorin, lord of innistrad'/'DKA', 'Mythic Rare').
card_artist('sorin, lord of innistrad'/'DKA', 'Michael Komarck').
card_number('sorin, lord of innistrad'/'DKA', '142').
card_multiverse_id('sorin, lord of innistrad'/'DKA', '249985').

card_in_set('soul seizer', 'DKA').
card_original_type('soul seizer'/'DKA', 'Creature — Spirit').
card_original_text('soul seizer'/'DKA', 'Flying\nWhen Soul Seizer deals combat damage to a player, you may transform it. If you do, attach it to target creature that player controls.').
card_first_print('soul seizer', 'DKA').
card_image_name('soul seizer'/'DKA', 'soul seizer').
card_uid('soul seizer'/'DKA', 'DKA:Soul Seizer:soul seizer').
card_rarity('soul seizer'/'DKA', 'Uncommon').
card_artist('soul seizer'/'DKA', 'Lucas Graciano').
card_number('soul seizer'/'DKA', '50a').
card_multiverse_id('soul seizer'/'DKA', '222178').

card_in_set('spiteful shadows', 'DKA').
card_original_type('spiteful shadows'/'DKA', 'Enchantment — Aura').
card_original_text('spiteful shadows'/'DKA', 'Enchant creature\nWhenever enchanted creature is dealt damage, it deals that much damage to its controller.').
card_first_print('spiteful shadows', 'DKA').
card_image_name('spiteful shadows'/'DKA', 'spiteful shadows').
card_uid('spiteful shadows'/'DKA', 'DKA:Spiteful Shadows:spiteful shadows').
card_rarity('spiteful shadows'/'DKA', 'Common').
card_artist('spiteful shadows'/'DKA', 'John Stanko').
card_number('spiteful shadows'/'DKA', '75').
card_flavor_text('spiteful shadows'/'DKA', '\"Let us remind these human cattle why they fear the shadows of this world.\"\n—Aldreg, Skirsdag cultist').
card_multiverse_id('spiteful shadows'/'DKA', '262668').

card_in_set('stormbound geist', 'DKA').
card_original_type('stormbound geist'/'DKA', 'Creature — Spirit').
card_original_text('stormbound geist'/'DKA', 'Flying\nStormbound Geist can block only creatures with flying.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('stormbound geist', 'DKA').
card_image_name('stormbound geist'/'DKA', 'stormbound geist').
card_uid('stormbound geist'/'DKA', 'DKA:Stormbound Geist:stormbound geist').
card_rarity('stormbound geist'/'DKA', 'Common').
card_artist('stormbound geist'/'DKA', 'Dan Scott').
card_number('stormbound geist'/'DKA', '51').
card_multiverse_id('stormbound geist'/'DKA', '262660').

card_in_set('strangleroot geist', 'DKA').
card_original_type('strangleroot geist'/'DKA', 'Creature — Spirit').
card_original_text('strangleroot geist'/'DKA', 'Haste\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_image_name('strangleroot geist'/'DKA', 'strangleroot geist').
card_uid('strangleroot geist'/'DKA', 'DKA:Strangleroot Geist:strangleroot geist').
card_rarity('strangleroot geist'/'DKA', 'Uncommon').
card_artist('strangleroot geist'/'DKA', 'Jason Chan').
card_number('strangleroot geist'/'DKA', '127').
card_flavor_text('strangleroot geist'/'DKA', 'Geists of the hanged often haunt the tree on which they died.').
card_multiverse_id('strangleroot geist'/'DKA', '262671').

card_in_set('stromkirk captain', 'DKA').
card_original_type('stromkirk captain'/'DKA', 'Creature — Vampire Soldier').
card_original_text('stromkirk captain'/'DKA', 'First strike\nOther Vampire creatures you control get +1/+1 and have first strike.').
card_first_print('stromkirk captain', 'DKA').
card_image_name('stromkirk captain'/'DKA', 'stromkirk captain').
card_uid('stromkirk captain'/'DKA', 'DKA:Stromkirk Captain:stromkirk captain').
card_rarity('stromkirk captain'/'DKA', 'Uncommon').
card_artist('stromkirk captain'/'DKA', 'Jana Schirmer & Johannes Voss').
card_number('stromkirk captain'/'DKA', '143').
card_flavor_text('stromkirk captain'/'DKA', '\"No longer can we allow our human populations to be mindlessly slaughtered by ghouls. Slay all who trespass.\"\n—Runo Stromkirk').
card_multiverse_id('stromkirk captain'/'DKA', '262676').

card_in_set('sudden disappearance', 'DKA').
card_original_type('sudden disappearance'/'DKA', 'Sorcery').
card_original_text('sudden disappearance'/'DKA', 'Exile all nonland permanents target player controls. Return the exiled cards to the battlefield under their owner\'s control at the beginning of the next end step.').
card_first_print('sudden disappearance', 'DKA').
card_image_name('sudden disappearance'/'DKA', 'sudden disappearance').
card_uid('sudden disappearance'/'DKA', 'DKA:Sudden Disappearance:sudden disappearance').
card_rarity('sudden disappearance'/'DKA', 'Rare').
card_artist('sudden disappearance'/'DKA', 'Cliff Childs').
card_number('sudden disappearance'/'DKA', '23').
card_multiverse_id('sudden disappearance'/'DKA', '262865').

card_in_set('talons of falkenrath', 'DKA').
card_original_type('talons of falkenrath'/'DKA', 'Enchantment — Aura').
card_original_text('talons of falkenrath'/'DKA', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature has \"{1}{R}: This creature gets +2/+0 until end of turn.\"').
card_first_print('talons of falkenrath', 'DKA').
card_image_name('talons of falkenrath'/'DKA', 'talons of falkenrath').
card_uid('talons of falkenrath'/'DKA', 'DKA:Talons of Falkenrath:talons of falkenrath').
card_rarity('talons of falkenrath'/'DKA', 'Common').
card_artist('talons of falkenrath'/'DKA', 'Svetlin Velinov').
card_number('talons of falkenrath'/'DKA', '105').
card_multiverse_id('talons of falkenrath'/'DKA', '262855').

card_in_set('thalia, guardian of thraben', 'DKA').
card_original_type('thalia, guardian of thraben'/'DKA', 'Legendary Creature — Human Soldier').
card_original_text('thalia, guardian of thraben'/'DKA', 'First strike\nNoncreature spells cost {1} more to cast.').
card_first_print('thalia, guardian of thraben', 'DKA').
card_image_name('thalia, guardian of thraben'/'DKA', 'thalia, guardian of thraben').
card_uid('thalia, guardian of thraben'/'DKA', 'DKA:Thalia, Guardian of Thraben:thalia, guardian of thraben').
card_rarity('thalia, guardian of thraben'/'DKA', 'Rare').
card_artist('thalia, guardian of thraben'/'DKA', 'Jana Schirmer & Johannes Voss').
card_number('thalia, guardian of thraben'/'DKA', '24').
card_flavor_text('thalia, guardian of thraben'/'DKA', '\"Thraben is our home and I will not see it fall to this unhallowed horde.\"').
card_multiverse_id('thalia, guardian of thraben'/'DKA', '270445').

card_in_set('thought scour', 'DKA').
card_original_type('thought scour'/'DKA', 'Instant').
card_original_text('thought scour'/'DKA', 'Target player puts the top two cards of his or her library into his or her graveyard.\nDraw a card.').
card_first_print('thought scour', 'DKA').
card_image_name('thought scour'/'DKA', 'thought scour').
card_uid('thought scour'/'DKA', 'DKA:Thought Scour:thought scour').
card_rarity('thought scour'/'DKA', 'Common').
card_artist('thought scour'/'DKA', 'David Rapoza').
card_number('thought scour'/'DKA', '52').
card_flavor_text('thought scour'/'DKA', '\"As you inject the viscus vitae into the brain stem, don\'t let the spastic moaning bother you. It will soon become music to your ears.\"\n—Stitcher Geralf').
card_multiverse_id('thought scour'/'DKA', '262838').

card_in_set('thraben doomsayer', 'DKA').
card_original_type('thraben doomsayer'/'DKA', 'Creature — Human Cleric').
card_original_text('thraben doomsayer'/'DKA', '{T}: Put a 1/1 white Human creature token onto the battlefield.\nFateful hour — As long as you have 5 or less life, other creatures you control get +2/+2.').
card_first_print('thraben doomsayer', 'DKA').
card_image_name('thraben doomsayer'/'DKA', 'thraben doomsayer').
card_uid('thraben doomsayer'/'DKA', 'DKA:Thraben Doomsayer:thraben doomsayer').
card_rarity('thraben doomsayer'/'DKA', 'Rare').
card_artist('thraben doomsayer'/'DKA', 'John Stanko').
card_number('thraben doomsayer'/'DKA', '25').
card_flavor_text('thraben doomsayer'/'DKA', '\"Swarms of the unhallowed claw at Thraben\'s gates! Do you still deny the end approaches?\"').
card_multiverse_id('thraben doomsayer'/'DKA', '262692').

card_in_set('thraben heretic', 'DKA').
card_original_type('thraben heretic'/'DKA', 'Creature — Human Wizard').
card_original_text('thraben heretic'/'DKA', '{T}: Exile target creature card from a graveyard.').
card_first_print('thraben heretic', 'DKA').
card_image_name('thraben heretic'/'DKA', 'thraben heretic').
card_uid('thraben heretic'/'DKA', 'DKA:Thraben Heretic:thraben heretic').
card_rarity('thraben heretic'/'DKA', 'Uncommon').
card_artist('thraben heretic'/'DKA', 'James Ryman').
card_number('thraben heretic'/'DKA', '26').
card_flavor_text('thraben heretic'/'DKA', '\"Let them decry me for burning the dead. I\'m not giving those ghoulcallers any more fuel for their madness.\"').
card_multiverse_id('thraben heretic'/'DKA', '244709').

card_in_set('torch fiend', 'DKA').
card_original_type('torch fiend'/'DKA', 'Creature — Devil').
card_original_text('torch fiend'/'DKA', '{R}, Sacrifice Torch Fiend: Destroy target artifact.').
card_first_print('torch fiend', 'DKA').
card_image_name('torch fiend'/'DKA', 'torch fiend').
card_uid('torch fiend'/'DKA', 'DKA:Torch Fiend:torch fiend').
card_rarity('torch fiend'/'DKA', 'Common').
card_artist('torch fiend'/'DKA', 'Winona Nelson').
card_number('torch fiend'/'DKA', '106').
card_flavor_text('torch fiend'/'DKA', 'Devils redecorate every room with fire.').
card_multiverse_id('torch fiend'/'DKA', '244702').

card_in_set('tovolar\'s magehunter', 'DKA').
card_original_type('tovolar\'s magehunter'/'DKA', 'Creature — Werewolf').
card_original_text('tovolar\'s magehunter'/'DKA', 'Whenever an opponent casts a spell, Tovolar\'s Magehunter deals 2 damage to that player.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Tovolar\'s Magehunter.').
card_image_name('tovolar\'s magehunter'/'DKA', 'tovolar\'s magehunter').
card_uid('tovolar\'s magehunter'/'DKA', 'DKA:Tovolar\'s Magehunter:tovolar\'s magehunter').
card_rarity('tovolar\'s magehunter'/'DKA', 'Rare').
card_artist('tovolar\'s magehunter'/'DKA', 'Mike Sass').
card_number('tovolar\'s magehunter'/'DKA', '98b').
card_multiverse_id('tovolar\'s magehunter'/'DKA', '253429').

card_in_set('tower geist', 'DKA').
card_original_type('tower geist'/'DKA', 'Creature — Spirit').
card_original_text('tower geist'/'DKA', 'Flying\nWhen Tower Geist enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other into your graveyard.').
card_first_print('tower geist', 'DKA').
card_image_name('tower geist'/'DKA', 'tower geist').
card_uid('tower geist'/'DKA', 'DKA:Tower Geist:tower geist').
card_rarity('tower geist'/'DKA', 'Uncommon').
card_artist('tower geist'/'DKA', 'Izzy').
card_number('tower geist'/'DKA', '53').
card_flavor_text('tower geist'/'DKA', 'Jenrik\'s tower is served by those who once sought to enter it uninvited.').
card_multiverse_id('tower geist'/'DKA', '262665').

card_in_set('tracker\'s instincts', 'DKA').
card_original_type('tracker\'s instincts'/'DKA', 'Sorcery').
card_original_text('tracker\'s instincts'/'DKA', 'Reveal the top four cards of your library. Put a creature card from among them into your hand and the rest into your graveyard.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('tracker\'s instincts', 'DKA').
card_image_name('tracker\'s instincts'/'DKA', 'tracker\'s instincts').
card_uid('tracker\'s instincts'/'DKA', 'DKA:Tracker\'s Instincts:tracker\'s instincts').
card_rarity('tracker\'s instincts'/'DKA', 'Uncommon').
card_artist('tracker\'s instincts'/'DKA', 'Jung Park').
card_number('tracker\'s instincts'/'DKA', '128').
card_multiverse_id('tracker\'s instincts'/'DKA', '247116').

card_in_set('tragic slip', 'DKA').
card_original_type('tragic slip'/'DKA', 'Instant').
card_original_text('tragic slip'/'DKA', 'Target creature gets -1/-1 until end of turn.\nMorbid — That creature gets -13/-13 until end of turn instead if a creature died this turn.').
card_first_print('tragic slip', 'DKA').
card_image_name('tragic slip'/'DKA', 'tragic slip').
card_uid('tragic slip'/'DKA', 'DKA:Tragic Slip:tragic slip').
card_rarity('tragic slip'/'DKA', 'Common').
card_artist('tragic slip'/'DKA', 'Christopher Moeller').
card_number('tragic slip'/'DKA', '76').
card_flavor_text('tragic slip'/'DKA', 'Linger on death\'s door and risk being invited in.').
card_multiverse_id('tragic slip'/'DKA', '242500').

card_in_set('ulvenwald bear', 'DKA').
card_original_type('ulvenwald bear'/'DKA', 'Creature — Bear').
card_original_text('ulvenwald bear'/'DKA', 'Morbid — When Ulvenwald Bear enters the battlefield, if a creature died this turn, put two +1/+1 counters on target creature.').
card_first_print('ulvenwald bear', 'DKA').
card_image_name('ulvenwald bear'/'DKA', 'ulvenwald bear').
card_uid('ulvenwald bear'/'DKA', 'DKA:Ulvenwald Bear:ulvenwald bear').
card_rarity('ulvenwald bear'/'DKA', 'Common').
card_artist('ulvenwald bear'/'DKA', 'Jason A. Engle').
card_number('ulvenwald bear'/'DKA', '129').
card_flavor_text('ulvenwald bear'/'DKA', 'Out chasing bears\n—Kessig expression meaning \"died in the woods\"').
card_multiverse_id('ulvenwald bear'/'DKA', '244727').

card_in_set('undying evil', 'DKA').
card_original_type('undying evil'/'DKA', 'Instant').
card_original_text('undying evil'/'DKA', 'Target creature gains undying until end of turn. (When it dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('undying evil', 'DKA').
card_image_name('undying evil'/'DKA', 'undying evil').
card_uid('undying evil'/'DKA', 'DKA:Undying Evil:undying evil').
card_rarity('undying evil'/'DKA', 'Common').
card_artist('undying evil'/'DKA', 'Kev Walker').
card_number('undying evil'/'DKA', '77').
card_flavor_text('undying evil'/'DKA', '\"Is it true the evil that people do follows them into death? Let\'s find out.\"\n—Liliana Vess').
card_multiverse_id('undying evil'/'DKA', '262835').

card_in_set('unhallowed cathar', 'DKA').
card_original_type('unhallowed cathar'/'DKA', 'Creature — Zombie Soldier').
card_original_text('unhallowed cathar'/'DKA', 'Unhallowed Cathar can\'t block.').
card_first_print('unhallowed cathar', 'DKA').
card_image_name('unhallowed cathar'/'DKA', 'unhallowed cathar').
card_uid('unhallowed cathar'/'DKA', 'DKA:Unhallowed Cathar:unhallowed cathar').
card_rarity('unhallowed cathar'/'DKA', 'Common').
card_artist('unhallowed cathar'/'DKA', 'Ryan Pancoast').
card_number('unhallowed cathar'/'DKA', '13b').
card_flavor_text('unhallowed cathar'/'DKA', '\"Just fight without fear. Your soul is protected by the hand of Avacyn and will never submit to evil.\"\n—Thalia, to Constable Visil').
card_multiverse_id('unhallowed cathar'/'DKA', '244734').

card_in_set('vault of the archangel', 'DKA').
card_original_type('vault of the archangel'/'DKA', 'Land').
card_original_text('vault of the archangel'/'DKA', '{T}: Add {1} to your mana pool.\n{2}{W}{B}, {T}: Creatures you control gain deathtouch and lifelink until end of turn.').
card_first_print('vault of the archangel', 'DKA').
card_image_name('vault of the archangel'/'DKA', 'vault of the archangel').
card_uid('vault of the archangel'/'DKA', 'DKA:Vault of the Archangel:vault of the archangel').
card_rarity('vault of the archangel'/'DKA', 'Rare').
card_artist('vault of the archangel'/'DKA', 'John Avon').
card_number('vault of the archangel'/'DKA', '158').
card_flavor_text('vault of the archangel'/'DKA', '\"For centuries my creation kept this world in balance. Now only her shadow remains.\"\n—Sorin Markov').
card_multiverse_id('vault of the archangel'/'DKA', '270938').

card_in_set('vengeful vampire', 'DKA').
card_original_type('vengeful vampire'/'DKA', 'Creature — Vampire').
card_original_text('vengeful vampire'/'DKA', 'Flying\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('vengeful vampire', 'DKA').
card_image_name('vengeful vampire'/'DKA', 'vengeful vampire').
card_uid('vengeful vampire'/'DKA', 'DKA:Vengeful Vampire:vengeful vampire').
card_rarity('vengeful vampire'/'DKA', 'Uncommon').
card_artist('vengeful vampire'/'DKA', 'Lucas Graciano').
card_number('vengeful vampire'/'DKA', '78').
card_flavor_text('vengeful vampire'/'DKA', 'He wields the full power of wrath unfettered by the sympathies of a soul.').
card_multiverse_id('vengeful vampire'/'DKA', '262691').

card_in_set('village survivors', 'DKA').
card_original_type('village survivors'/'DKA', 'Creature — Human').
card_original_text('village survivors'/'DKA', 'Vigilance\nFateful hour — As long as you have 5 or less life, other creatures you control have vigilance.').
card_first_print('village survivors', 'DKA').
card_image_name('village survivors'/'DKA', 'village survivors').
card_uid('village survivors'/'DKA', 'DKA:Village Survivors:village survivors').
card_rarity('village survivors'/'DKA', 'Uncommon').
card_artist('village survivors'/'DKA', 'David Rapoza').
card_number('village survivors'/'DKA', '130').
card_flavor_text('village survivors'/'DKA', 'As their villages fell to werewolves, fleeing refugees discovered they too had remarkable survival instincts.').
card_multiverse_id('village survivors'/'DKA', '262688').

card_in_set('vorapede', 'DKA').
card_original_type('vorapede'/'DKA', 'Creature — Insect').
card_original_text('vorapede'/'DKA', 'Vigilance, trample\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('vorapede', 'DKA').
card_image_name('vorapede'/'DKA', 'vorapede').
card_uid('vorapede'/'DKA', 'DKA:Vorapede:vorapede').
card_rarity('vorapede'/'DKA', 'Mythic Rare').
card_artist('vorapede'/'DKA', 'Slawomir Maniak').
card_number('vorapede'/'DKA', '131').
card_flavor_text('vorapede'/'DKA', '\"If you think you\'ve killed one, guess again.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('vorapede'/'DKA', '262850').

card_in_set('wakedancer', 'DKA').
card_original_type('wakedancer'/'DKA', 'Creature — Human Shaman').
card_original_text('wakedancer'/'DKA', 'Morbid — When Wakedancer enters the battlefield, if a creature died this turn, put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('wakedancer', 'DKA').
card_image_name('wakedancer'/'DKA', 'wakedancer').
card_uid('wakedancer'/'DKA', 'DKA:Wakedancer:wakedancer').
card_rarity('wakedancer'/'DKA', 'Uncommon').
card_artist('wakedancer'/'DKA', 'Austin Hsu').
card_number('wakedancer'/'DKA', '79').
card_flavor_text('wakedancer'/'DKA', 'Hers is an ancient form of necromancy, steeped in shamanic trance and ritual that few skaberen or ghoulcallers comprehend.').
card_multiverse_id('wakedancer'/'DKA', '262836').

card_in_set('warden of the wall', 'DKA').
card_original_type('warden of the wall'/'DKA', 'Artifact').
card_original_text('warden of the wall'/'DKA', 'Warden of the Wall enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\nAs long as it\'s not your turn, Warden of the Wall is a 2/3 Gargoyle artifact creature with flying.').
card_first_print('warden of the wall', 'DKA').
card_image_name('warden of the wall'/'DKA', 'warden of the wall').
card_uid('warden of the wall'/'DKA', 'DKA:Warden of the Wall:warden of the wall').
card_rarity('warden of the wall'/'DKA', 'Uncommon').
card_artist('warden of the wall'/'DKA', 'Daniel Ljunggren').
card_number('warden of the wall'/'DKA', '153').
card_multiverse_id('warden of the wall'/'DKA', '247130').

card_in_set('werewolf ransacker', 'DKA').
card_original_type('werewolf ransacker'/'DKA', 'Creature — Werewolf').
card_original_text('werewolf ransacker'/'DKA', 'Whenever this creature transforms into Werewolf Ransacker, you may destroy target artifact. If that artifact is put into a graveyard this way, Werewolf Ransacker deals 3 damage to that artifact\'s controller.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Werewolf Ransacker.').
card_first_print('werewolf ransacker', 'DKA').
card_image_name('werewolf ransacker'/'DKA', 'werewolf ransacker').
card_uid('werewolf ransacker'/'DKA', 'DKA:Werewolf Ransacker:werewolf ransacker').
card_rarity('werewolf ransacker'/'DKA', 'Uncommon').
card_artist('werewolf ransacker'/'DKA', 'David Palumbo').
card_number('werewolf ransacker'/'DKA', '81b').
card_multiverse_id('werewolf ransacker'/'DKA', '262698').

card_in_set('wild hunger', 'DKA').
card_original_type('wild hunger'/'DKA', 'Instant').
card_original_text('wild hunger'/'DKA', 'Target creature gets +3/+1 and gains trample until end of turn.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_first_print('wild hunger', 'DKA').
card_image_name('wild hunger'/'DKA', 'wild hunger').
card_uid('wild hunger'/'DKA', 'DKA:Wild Hunger:wild hunger').
card_rarity('wild hunger'/'DKA', 'Common').
card_artist('wild hunger'/'DKA', 'Karl Kopinski').
card_number('wild hunger'/'DKA', '132').
card_flavor_text('wild hunger'/'DKA', 'Otto the Hunter was filled with many grand ambitions. The snake was filled only with Otto.').
card_multiverse_id('wild hunger'/'DKA', '245291').

card_in_set('withengar unbound', 'DKA').
card_original_type('withengar unbound'/'DKA', 'Legendary Creature — Demon').
card_original_text('withengar unbound'/'DKA', 'Flying, intimidate, trample\nWhenever a player loses the game, put thirteen +1/+1 counters on Withengar Unbound.').
card_first_print('withengar unbound', 'DKA').
card_image_name('withengar unbound'/'DKA', 'withengar unbound').
card_uid('withengar unbound'/'DKA', 'DKA:Withengar Unbound:withengar unbound').
card_rarity('withengar unbound'/'DKA', 'Mythic Rare').
card_artist('withengar unbound'/'DKA', 'Eric Deschamps').
card_number('withengar unbound'/'DKA', '147b').
card_flavor_text('withengar unbound'/'DKA', 'There are not enough lives on Innistrad to satisfy his thirst for retribution.').
card_multiverse_id('withengar unbound'/'DKA', '244738').

card_in_set('wolfbitten captive', 'DKA').
card_original_type('wolfbitten captive'/'DKA', 'Creature — Human Werewolf').
card_original_text('wolfbitten captive'/'DKA', '{1}{G}: Wolfbitten Captive gets +2/+2 until end of turn. Activate this ability only once each turn.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Wolfbitten Captive.').
card_first_print('wolfbitten captive', 'DKA').
card_image_name('wolfbitten captive'/'DKA', 'wolfbitten captive').
card_uid('wolfbitten captive'/'DKA', 'DKA:Wolfbitten Captive:wolfbitten captive').
card_rarity('wolfbitten captive'/'DKA', 'Rare').
card_artist('wolfbitten captive'/'DKA', 'Zoltan Boros').
card_number('wolfbitten captive'/'DKA', '133a').
card_multiverse_id('wolfbitten captive'/'DKA', '253426').

card_in_set('wolfhunter\'s quiver', 'DKA').
card_original_type('wolfhunter\'s quiver'/'DKA', 'Artifact — Equipment').
card_original_text('wolfhunter\'s quiver'/'DKA', 'Equipped creature has \"{T}: This creature deals 1 damage to target creature or player\" and \"{T}: This creature deals 3 damage to target Werewolf creature.\"\nEquip {5}').
card_first_print('wolfhunter\'s quiver', 'DKA').
card_image_name('wolfhunter\'s quiver'/'DKA', 'wolfhunter\'s quiver').
card_uid('wolfhunter\'s quiver'/'DKA', 'DKA:Wolfhunter\'s Quiver:wolfhunter\'s quiver').
card_rarity('wolfhunter\'s quiver'/'DKA', 'Uncommon').
card_artist('wolfhunter\'s quiver'/'DKA', 'Daniel Ljunggren').
card_number('wolfhunter\'s quiver'/'DKA', '154').
card_flavor_text('wolfhunter\'s quiver'/'DKA', '\"Steel in the heart, silver in the bow.\"\n—Wolfhunter\'s creed').
card_multiverse_id('wolfhunter\'s quiver'/'DKA', '247124').

card_in_set('wrack with madness', 'DKA').
card_original_type('wrack with madness'/'DKA', 'Sorcery').
card_original_text('wrack with madness'/'DKA', 'Target creature deals damage to itself equal to its power.').
card_first_print('wrack with madness', 'DKA').
card_image_name('wrack with madness'/'DKA', 'wrack with madness').
card_uid('wrack with madness'/'DKA', 'DKA:Wrack with Madness:wrack with madness').
card_rarity('wrack with madness'/'DKA', 'Common').
card_artist('wrack with madness'/'DKA', 'Todd Lockwood').
card_number('wrack with madness'/'DKA', '107').
card_flavor_text('wrack with madness'/'DKA', 'The skathul, spirits consumed by revenge, fester and seethe while looking for weak minds to assault.').
card_multiverse_id('wrack with madness'/'DKA', '243231').

card_in_set('young wolf', 'DKA').
card_original_type('young wolf'/'DKA', 'Creature — Wolf').
card_original_text('young wolf'/'DKA', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_first_print('young wolf', 'DKA').
card_image_name('young wolf'/'DKA', 'young wolf').
card_uid('young wolf'/'DKA', 'DKA:Young Wolf:young wolf').
card_rarity('young wolf'/'DKA', 'Common').
card_artist('young wolf'/'DKA', 'Ryan Pancoast').
card_number('young wolf'/'DKA', '134').
card_flavor_text('young wolf'/'DKA', 'The Ulvenwald makes no allowances for youth. Today\'s newborn is either tomorrow\'s hunter or tomorrow\'s lunch.').
card_multiverse_id('young wolf'/'DKA', '262872').

card_in_set('zombie apocalypse', 'DKA').
card_original_type('zombie apocalypse'/'DKA', 'Sorcery').
card_original_text('zombie apocalypse'/'DKA', 'Return all Zombie creature cards from your graveyard to the battlefield tapped, then destroy all Humans.').
card_image_name('zombie apocalypse'/'DKA', 'zombie apocalypse').
card_uid('zombie apocalypse'/'DKA', 'DKA:Zombie Apocalypse:zombie apocalypse').
card_rarity('zombie apocalypse'/'DKA', 'Rare').
card_artist('zombie apocalypse'/'DKA', 'Peter Mohrbacher').
card_number('zombie apocalypse'/'DKA', '80').
card_flavor_text('zombie apocalypse'/'DKA', '\"There will come a day so dark you will pray for death. On that day your prayers will be answered.\"\n—Minaldra, the Vizag Atum').
card_multiverse_id('zombie apocalypse'/'DKA', '262669').
