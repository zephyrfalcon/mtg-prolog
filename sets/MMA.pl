% Modern Masters

set('MMA').
set_name('MMA', 'Modern Masters').
set_release_date('MMA', '2013-06-07').
set_border('MMA', 'black').
set_type('MMA', 'reprint').

card_in_set('absorb vis', 'MMA').
card_original_type('absorb vis'/'MMA', 'Sorcery').
card_original_text('absorb vis'/'MMA', 'Target player loses 4 life and you gain 4 life.\nBasic landcycling {1}{B} ({1}{B}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('absorb vis'/'MMA', 'absorb vis').
card_uid('absorb vis'/'MMA', 'MMA:Absorb Vis:absorb vis').
card_rarity('absorb vis'/'MMA', 'Common').
card_artist('absorb vis'/'MMA', 'Brandon Kitkouski').
card_number('absorb vis'/'MMA', '71').
card_multiverse_id('absorb vis'/'MMA', '370360').

card_in_set('academy ruins', 'MMA').
card_original_type('academy ruins'/'MMA', 'Legendary Land').
card_original_text('academy ruins'/'MMA', '{T}: Add {1} to your mana pool.\n{1}{U}, {T}: Put target artifact card from your graveyard on top of your library.').
card_image_name('academy ruins'/'MMA', 'academy ruins').
card_uid('academy ruins'/'MMA', 'MMA:Academy Ruins:academy ruins').
card_rarity('academy ruins'/'MMA', 'Rare').
card_artist('academy ruins'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('academy ruins'/'MMA', '219').
card_flavor_text('academy ruins'/'MMA', 'Its secrets once wrought the greatest artifice ever known. Now crabs loot the rubble to decorate their shells.').
card_multiverse_id('academy ruins'/'MMA', '370424').

card_in_set('adarkar valkyrie', 'MMA').
card_original_type('adarkar valkyrie'/'MMA', 'Snow Creature — Angel').
card_original_text('adarkar valkyrie'/'MMA', 'Flying, vigilance\n{T}: When target creature other than Adarkar Valkyrie dies this turn, return that card to the battlefield under your control.').
card_image_name('adarkar valkyrie'/'MMA', 'adarkar valkyrie').
card_uid('adarkar valkyrie'/'MMA', 'MMA:Adarkar Valkyrie:adarkar valkyrie').
card_rarity('adarkar valkyrie'/'MMA', 'Rare').
card_artist('adarkar valkyrie'/'MMA', 'Jeremy Jarvis').
card_number('adarkar valkyrie'/'MMA', '1').
card_flavor_text('adarkar valkyrie'/'MMA', 'She doesn\'t escort the dead to the afterlife, but instead raises them to fight and die again.').
card_multiverse_id('adarkar valkyrie'/'MMA', '370543').

card_in_set('æther spellbomb', 'MMA').
card_original_type('æther spellbomb'/'MMA', 'Artifact').
card_original_text('æther spellbomb'/'MMA', '{U}, Sacrifice Æther Spellbomb: Return target creature to its owner\'s hand.\n{1}, Sacrifice Æther Spellbomb: Draw a card.').
card_image_name('æther spellbomb'/'MMA', 'aether spellbomb').
card_uid('æther spellbomb'/'MMA', 'MMA:Æther Spellbomb:aether spellbomb').
card_rarity('æther spellbomb'/'MMA', 'Common').
card_artist('æther spellbomb'/'MMA', 'Jim Nelson').
card_number('æther spellbomb'/'MMA', '196').
card_flavor_text('æther spellbomb'/'MMA', '\"Release that which was never caged.\"\n—Spellbomb inscription').
card_multiverse_id('æther spellbomb'/'MMA', '370524').

card_in_set('æther vial', 'MMA').
card_original_type('æther vial'/'MMA', 'Artifact').
card_original_text('æther vial'/'MMA', 'At the beginning of your upkeep, you may put a charge counter on Æther Vial.\n{T}: You may put a creature card with converted mana cost equal to the number of charge counters on Æther Vial from your hand onto the battlefield.').
card_image_name('æther vial'/'MMA', 'aether vial').
card_uid('æther vial'/'MMA', 'MMA:Æther Vial:aether vial').
card_rarity('æther vial'/'MMA', 'Rare').
card_artist('æther vial'/'MMA', 'Karl Kopinski').
card_number('æther vial'/'MMA', '197').
card_multiverse_id('æther vial'/'MMA', '370514').

card_in_set('æthersnipe', 'MMA').
card_original_type('æthersnipe'/'MMA', 'Creature — Elemental').
card_original_text('æthersnipe'/'MMA', 'When Æthersnipe enters the battlefield, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('æthersnipe'/'MMA', 'aethersnipe').
card_uid('æthersnipe'/'MMA', 'MMA:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'MMA', 'Common').
card_artist('æthersnipe'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'MMA', '36').
card_multiverse_id('æthersnipe'/'MMA', '370430').

card_in_set('amrou scout', 'MMA').
card_original_type('amrou scout'/'MMA', 'Creature — Kithkin Rebel Scout').
card_original_text('amrou scout'/'MMA', '{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_image_name('amrou scout'/'MMA', 'amrou scout').
card_uid('amrou scout'/'MMA', 'MMA:Amrou Scout:amrou scout').
card_rarity('amrou scout'/'MMA', 'Common').
card_artist('amrou scout'/'MMA', 'Quinton Hoover').
card_number('amrou scout'/'MMA', '2').
card_flavor_text('amrou scout'/'MMA', 'The people of Amrou were scattered by war and driven into hiding. Scouts maintain the beacon fires that signal \"return\" and \"home.\"').
card_multiverse_id('amrou scout'/'MMA', '370571').

card_in_set('amrou seekers', 'MMA').
card_original_type('amrou seekers'/'MMA', 'Creature — Kithkin Rebel').
card_original_text('amrou seekers'/'MMA', 'Amrou Seekers can\'t be blocked except by artifact creatures and/or white creatures.').
card_image_name('amrou seekers'/'MMA', 'amrou seekers').
card_uid('amrou seekers'/'MMA', 'MMA:Amrou Seekers:amrou seekers').
card_rarity('amrou seekers'/'MMA', 'Common').
card_artist('amrou seekers'/'MMA', 'Quinton Hoover').
card_number('amrou seekers'/'MMA', '3').
card_flavor_text('amrou seekers'/'MMA', 'The paradise of Amrou Haven had become a field of cinders. Its people struggled to survive, hoping to see visions of its past in the strange storms that quickly passed.').
card_multiverse_id('amrou seekers'/'MMA', '370576').

card_in_set('angel\'s grace', 'MMA').
card_original_type('angel\'s grace'/'MMA', 'Instant').
card_original_text('angel\'s grace'/'MMA', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nYou can\'t lose the game this turn and your opponents can\'t win the game this turn. Until end of turn, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('angel\'s grace'/'MMA', 'angel\'s grace').
card_uid('angel\'s grace'/'MMA', 'MMA:Angel\'s Grace:angel\'s grace').
card_rarity('angel\'s grace'/'MMA', 'Rare').
card_artist('angel\'s grace'/'MMA', 'Mark Zug').
card_number('angel\'s grace'/'MMA', '4').
card_multiverse_id('angel\'s grace'/'MMA', '370545').

card_in_set('arcbound ravager', 'MMA').
card_original_type('arcbound ravager'/'MMA', 'Artifact Creature — Beast').
card_original_text('arcbound ravager'/'MMA', 'Sacrifice an artifact: Put a +1/+1 counter on Arcbound Ravager.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound ravager'/'MMA', 'arcbound ravager').
card_uid('arcbound ravager'/'MMA', 'MMA:Arcbound Ravager:arcbound ravager').
card_rarity('arcbound ravager'/'MMA', 'Rare').
card_artist('arcbound ravager'/'MMA', 'Kev Walker').
card_number('arcbound ravager'/'MMA', '198').
card_multiverse_id('arcbound ravager'/'MMA', '370510').

card_in_set('arcbound stinger', 'MMA').
card_original_type('arcbound stinger'/'MMA', 'Artifact Creature — Insect').
card_original_text('arcbound stinger'/'MMA', 'Flying\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound stinger'/'MMA', 'arcbound stinger').
card_uid('arcbound stinger'/'MMA', 'MMA:Arcbound Stinger:arcbound stinger').
card_rarity('arcbound stinger'/'MMA', 'Common').
card_artist('arcbound stinger'/'MMA', 'Darrell Riche').
card_number('arcbound stinger'/'MMA', '199').
card_multiverse_id('arcbound stinger'/'MMA', '370443').

card_in_set('arcbound wanderer', 'MMA').
card_original_type('arcbound wanderer'/'MMA', 'Artifact Creature — Golem').
card_original_text('arcbound wanderer'/'MMA', 'Modular—Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound wanderer'/'MMA', 'arcbound wanderer').
card_uid('arcbound wanderer'/'MMA', 'MMA:Arcbound Wanderer:arcbound wanderer').
card_rarity('arcbound wanderer'/'MMA', 'Common').
card_artist('arcbound wanderer'/'MMA', 'Christopher Moeller').
card_number('arcbound wanderer'/'MMA', '200').
card_multiverse_id('arcbound wanderer'/'MMA', '370523').

card_in_set('arcbound worker', 'MMA').
card_original_type('arcbound worker'/'MMA', 'Artifact Creature — Construct').
card_original_text('arcbound worker'/'MMA', 'Modular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_image_name('arcbound worker'/'MMA', 'arcbound worker').
card_uid('arcbound worker'/'MMA', 'MMA:Arcbound Worker:arcbound worker').
card_rarity('arcbound worker'/'MMA', 'Common').
card_artist('arcbound worker'/'MMA', 'Darrell Riche').
card_number('arcbound worker'/'MMA', '201').
card_flavor_text('arcbound worker'/'MMA', 'The parts are as strong as the whole.').
card_multiverse_id('arcbound worker'/'MMA', '370517').

card_in_set('auntie\'s snitch', 'MMA').
card_original_type('auntie\'s snitch'/'MMA', 'Creature — Goblin Rogue').
card_original_text('auntie\'s snitch'/'MMA', 'Auntie\'s Snitch can\'t block.\nProwl {1}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhenever a Goblin or Rogue you control deals combat damage to a player, if Auntie\'s Snitch is in your graveyard, you may return Auntie\'s Snitch to your hand.').
card_image_name('auntie\'s snitch'/'MMA', 'auntie\'s snitch').
card_uid('auntie\'s snitch'/'MMA', 'MMA:Auntie\'s Snitch:auntie\'s snitch').
card_rarity('auntie\'s snitch'/'MMA', 'Uncommon').
card_artist('auntie\'s snitch'/'MMA', 'Warren Mahy').
card_number('auntie\'s snitch'/'MMA', '72').
card_multiverse_id('auntie\'s snitch'/'MMA', '370533').

card_in_set('auriok salvagers', 'MMA').
card_original_type('auriok salvagers'/'MMA', 'Creature — Human Soldier').
card_original_text('auriok salvagers'/'MMA', '{1}{W}: Return target artifact card with converted mana cost 1 or less from your graveyard to your hand.').
card_image_name('auriok salvagers'/'MMA', 'auriok salvagers').
card_uid('auriok salvagers'/'MMA', 'MMA:Auriok Salvagers:auriok salvagers').
card_rarity('auriok salvagers'/'MMA', 'Rare').
card_artist('auriok salvagers'/'MMA', 'Randy Gallegos').
card_number('auriok salvagers'/'MMA', '5').
card_flavor_text('auriok salvagers'/'MMA', '\"Memnarch or the vedalken salvage most of the large machines, leaving us only scraps. Scraps are enough.\"').
card_multiverse_id('auriok salvagers'/'MMA', '370370').

card_in_set('avian changeling', 'MMA').
card_original_type('avian changeling'/'MMA', 'Creature — Shapeshifter').
card_original_text('avian changeling'/'MMA', 'Changeling (This card is every creature type at all times.)\nFlying').
card_image_name('avian changeling'/'MMA', 'avian changeling').
card_uid('avian changeling'/'MMA', 'MMA:Avian Changeling:avian changeling').
card_rarity('avian changeling'/'MMA', 'Common').
card_artist('avian changeling'/'MMA', 'Heather Hudson').
card_number('avian changeling'/'MMA', '6').
card_flavor_text('avian changeling'/'MMA', 'Today it flies with the flock. Tomorrow it may wake to find them gone, its body in an unfamiliar form.').
card_multiverse_id('avian changeling'/'MMA', '370489').

card_in_set('blightspeaker', 'MMA').
card_original_type('blightspeaker'/'MMA', 'Creature — Human Rebel Cleric').
card_original_text('blightspeaker'/'MMA', '{T}: Target player loses 1 life.\n{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_image_name('blightspeaker'/'MMA', 'blightspeaker').
card_uid('blightspeaker'/'MMA', 'MMA:Blightspeaker:blightspeaker').
card_rarity('blightspeaker'/'MMA', 'Common').
card_artist('blightspeaker'/'MMA', 'Ron Spears').
card_number('blightspeaker'/'MMA', '73').
card_flavor_text('blightspeaker'/'MMA', 'One croaked sermon spreads propaganda and plague.').
card_multiverse_id('blightspeaker'/'MMA', '370567').

card_in_set('blind-spot giant', 'MMA').
card_original_type('blind-spot giant'/'MMA', 'Creature — Giant Warrior').
card_original_text('blind-spot giant'/'MMA', 'Blind-Spot Giant can\'t attack or block unless you control another Giant.').
card_image_name('blind-spot giant'/'MMA', 'blind-spot giant').
card_uid('blind-spot giant'/'MMA', 'MMA:Blind-Spot Giant:blind-spot giant').
card_rarity('blind-spot giant'/'MMA', 'Common').
card_artist('blind-spot giant'/'MMA', 'Jim Murray').
card_number('blind-spot giant'/'MMA', '105').
card_flavor_text('blind-spot giant'/'MMA', 'Among the solitude-loving giantkind, teamwork is unusual. But he appreciates hearing the occasional \"Swing down and to your left.\"').
card_multiverse_id('blind-spot giant'/'MMA', '370377').

card_in_set('blinding beam', 'MMA').
card_original_type('blinding beam'/'MMA', 'Instant').
card_original_text('blinding beam'/'MMA', 'Choose one — Tap two target creatures; or creatures don\'t untap during target player\'s next untap step.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_image_name('blinding beam'/'MMA', 'blinding beam').
card_uid('blinding beam'/'MMA', 'MMA:Blinding Beam:blinding beam').
card_rarity('blinding beam'/'MMA', 'Common').
card_artist('blinding beam'/'MMA', 'Doug Chaffee').
card_number('blinding beam'/'MMA', '7').
card_multiverse_id('blinding beam'/'MMA', '370431').

card_in_set('blinkmoth nexus', 'MMA').
card_original_type('blinkmoth nexus'/'MMA', 'Land').
card_original_text('blinkmoth nexus'/'MMA', '{T}: Add {1} to your mana pool.\n{1}: Blinkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying until end of turn. It\'s still a land.\n{1}, {T}: Target Blinkmoth creature gets +1/+1 until end of turn.').
card_image_name('blinkmoth nexus'/'MMA', 'blinkmoth nexus').
card_uid('blinkmoth nexus'/'MMA', 'MMA:Blinkmoth Nexus:blinkmoth nexus').
card_rarity('blinkmoth nexus'/'MMA', 'Rare').
card_artist('blinkmoth nexus'/'MMA', 'Sam Burley').
card_number('blinkmoth nexus'/'MMA', '220').
card_multiverse_id('blinkmoth nexus'/'MMA', '370407').

card_in_set('blood moon', 'MMA').
card_original_type('blood moon'/'MMA', 'Enchantment').
card_original_text('blood moon'/'MMA', 'Nonbasic lands are Mountains.').
card_image_name('blood moon'/'MMA', 'blood moon').
card_uid('blood moon'/'MMA', 'MMA:Blood Moon:blood moon').
card_rarity('blood moon'/'MMA', 'Rare').
card_artist('blood moon'/'MMA', 'Franz Vohwinkel').
card_number('blood moon'/'MMA', '106').
card_flavor_text('blood moon'/'MMA', 'Heavy light flooded across the landscape, cloaking everything in deep crimson.').
card_multiverse_id('blood moon'/'MMA', '370419').

card_in_set('bonesplitter', 'MMA').
card_original_type('bonesplitter'/'MMA', 'Artifact — Equipment').
card_original_text('bonesplitter'/'MMA', 'Equipped creature gets +2/+0.\nEquip {1}').
card_image_name('bonesplitter'/'MMA', 'bonesplitter').
card_uid('bonesplitter'/'MMA', 'MMA:Bonesplitter:bonesplitter').
card_rarity('bonesplitter'/'MMA', 'Common').
card_artist('bonesplitter'/'MMA', 'Darrell Riche').
card_number('bonesplitter'/'MMA', '202').
card_flavor_text('bonesplitter'/'MMA', '\"It puts an end to what ails you.\"\n—Jeorg, Valla battlemaster').
card_multiverse_id('bonesplitter'/'MMA', '370442').

card_in_set('bound in silence', 'MMA').
card_original_type('bound in silence'/'MMA', 'Tribal Enchantment — Rebel Aura').
card_original_text('bound in silence'/'MMA', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('bound in silence'/'MMA', 'bound in silence').
card_uid('bound in silence'/'MMA', 'MMA:Bound in Silence:bound in silence').
card_rarity('bound in silence'/'MMA', 'Common').
card_artist('bound in silence'/'MMA', 'William Simpson').
card_number('bound in silence'/'MMA', '8').
card_flavor_text('bound in silence'/'MMA', 'A fight put off forever is already won.').
card_multiverse_id('bound in silence'/'MMA', '370396').

card_in_set('bridge from below', 'MMA').
card_original_type('bridge from below'/'MMA', 'Enchantment').
card_original_text('bridge from below'/'MMA', 'Whenever a nontoken creature is put into your graveyard from the battlefield, if Bridge from Below is in your graveyard, put a 2/2 black Zombie creature token onto the battlefield.\nWhen a creature is put into an opponent\'s graveyard from the battlefield, if Bridge from Below is in your graveyard, exile Bridge from Below.').
card_image_name('bridge from below'/'MMA', 'bridge from below').
card_uid('bridge from below'/'MMA', 'MMA:Bridge from Below:bridge from below').
card_rarity('bridge from below'/'MMA', 'Rare').
card_artist('bridge from below'/'MMA', 'Daarken').
card_number('bridge from below'/'MMA', '74').
card_multiverse_id('bridge from below'/'MMA', '370353').

card_in_set('brute force', 'MMA').
card_original_type('brute force'/'MMA', 'Instant').
card_original_text('brute force'/'MMA', 'Target creature gets +3/+3 until end of turn.').
card_image_name('brute force'/'MMA', 'brute force').
card_uid('brute force'/'MMA', 'MMA:Brute Force:brute force').
card_rarity('brute force'/'MMA', 'Common').
card_artist('brute force'/'MMA', 'Wayne Reynolds').
card_number('brute force'/'MMA', '107').
card_flavor_text('brute force'/'MMA', 'Blood, bone, and sinew are magnified, as is the rage that drives them. The brain, however, remains unchanged—a little bean, swinging by a strand in a cavernous, raving head.').
card_multiverse_id('brute force'/'MMA', '370454').

card_in_set('careful consideration', 'MMA').
card_original_type('careful consideration'/'MMA', 'Instant').
card_original_text('careful consideration'/'MMA', 'Target player draws four cards, then discards three cards. If you cast this spell during your main phase, instead that player draws four cards, then discards two cards.').
card_image_name('careful consideration'/'MMA', 'careful consideration').
card_uid('careful consideration'/'MMA', 'MMA:Careful Consideration:careful consideration').
card_rarity('careful consideration'/'MMA', 'Uncommon').
card_artist('careful consideration'/'MMA', 'Janine Johnston').
card_number('careful consideration'/'MMA', '37').
card_flavor_text('careful consideration'/'MMA', '\"Sages who take the time to verify their sources are rewarded for their diligence.\"\n—Jhoira, to Teferi').
card_multiverse_id('careful consideration'/'MMA', '370531').

card_in_set('cenn\'s enlistment', 'MMA').
card_original_type('cenn\'s enlistment'/'MMA', 'Sorcery').
card_original_text('cenn\'s enlistment'/'MMA', 'Put two 1/1 white Kithkin Soldier creature tokens onto the battlefield.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_image_name('cenn\'s enlistment'/'MMA', 'cenn\'s enlistment').
card_uid('cenn\'s enlistment'/'MMA', 'MMA:Cenn\'s Enlistment:cenn\'s enlistment').
card_rarity('cenn\'s enlistment'/'MMA', 'Common').
card_artist('cenn\'s enlistment'/'MMA', 'Matt Cavotta').
card_number('cenn\'s enlistment'/'MMA', '9').
card_flavor_text('cenn\'s enlistment'/'MMA', 'The more the scarier.').
card_multiverse_id('cenn\'s enlistment'/'MMA', '370373').

card_in_set('chalice of the void', 'MMA').
card_original_type('chalice of the void'/'MMA', 'Artifact').
card_original_text('chalice of the void'/'MMA', 'Chalice of the Void enters the battlefield with X charge counters on it.\nWhenever a player casts a spell with converted mana cost equal to the number of charge counters on Chalice of the Void, counter that spell.').
card_image_name('chalice of the void'/'MMA', 'chalice of the void').
card_uid('chalice of the void'/'MMA', 'MMA:Chalice of the Void:chalice of the void').
card_rarity('chalice of the void'/'MMA', 'Rare').
card_artist('chalice of the void'/'MMA', 'Mark Zug').
card_number('chalice of the void'/'MMA', '203').
card_multiverse_id('chalice of the void'/'MMA', '370411').

card_in_set('citanul woodreaders', 'MMA').
card_original_type('citanul woodreaders'/'MMA', 'Creature — Human Druid').
card_original_text('citanul woodreaders'/'MMA', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nWhen Citanul Woodreaders enters the battlefield, if it was kicked, draw two cards.').
card_image_name('citanul woodreaders'/'MMA', 'citanul woodreaders').
card_uid('citanul woodreaders'/'MMA', 'MMA:Citanul Woodreaders:citanul woodreaders').
card_rarity('citanul woodreaders'/'MMA', 'Common').
card_artist('citanul woodreaders'/'MMA', 'Steven Belledin').
card_number('citanul woodreaders'/'MMA', '140').
card_flavor_text('citanul woodreaders'/'MMA', 'They seek out living trees to glean age-old secrets from sap and wood.').
card_multiverse_id('citanul woodreaders'/'MMA', '370501').

card_in_set('city of brass', 'MMA').
card_original_type('city of brass'/'MMA', 'Land').
card_original_text('city of brass'/'MMA', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'MMA', 'city of brass').
card_uid('city of brass'/'MMA', 'MMA:City of Brass:city of brass').
card_rarity('city of brass'/'MMA', 'Rare').
card_artist('city of brass'/'MMA', 'Jung Park').
card_number('city of brass'/'MMA', '221').
card_flavor_text('city of brass'/'MMA', '\"There is much to learn here, but few can endure the ringing of the spires.\"\n—Nulakam the Archivist').
card_multiverse_id('city of brass'/'MMA', '370490').

card_in_set('cloudgoat ranger', 'MMA').
card_original_type('cloudgoat ranger'/'MMA', 'Creature — Giant Warrior').
card_original_text('cloudgoat ranger'/'MMA', 'When Cloudgoat Ranger enters the battlefield, put three 1/1 white Kithkin Soldier creature tokens onto the battlefield.\nTap three untapped Kithkin you control: Cloudgoat Ranger gets +2/+0 and gains flying until end of turn.').
card_image_name('cloudgoat ranger'/'MMA', 'cloudgoat ranger').
card_uid('cloudgoat ranger'/'MMA', 'MMA:Cloudgoat Ranger:cloudgoat ranger').
card_rarity('cloudgoat ranger'/'MMA', 'Uncommon').
card_artist('cloudgoat ranger'/'MMA', 'Adam Rex').
card_number('cloudgoat ranger'/'MMA', '10').
card_multiverse_id('cloudgoat ranger'/'MMA', '370553').

card_in_set('cold-eyed selkie', 'MMA').
card_original_type('cold-eyed selkie'/'MMA', 'Creature — Merfolk Rogue').
card_original_text('cold-eyed selkie'/'MMA', 'Islandwalk\nWhenever Cold-Eyed Selkie deals combat damage to a player, you may draw that many cards.').
card_image_name('cold-eyed selkie'/'MMA', 'cold-eyed selkie').
card_uid('cold-eyed selkie'/'MMA', 'MMA:Cold-Eyed Selkie:cold-eyed selkie').
card_rarity('cold-eyed selkie'/'MMA', 'Rare').
card_artist('cold-eyed selkie'/'MMA', 'Jaime Jones').
card_number('cold-eyed selkie'/'MMA', '186').
card_flavor_text('cold-eyed selkie'/'MMA', 'She follows the old tributaries built when merrows still shaped the tides.').
card_multiverse_id('cold-eyed selkie'/'MMA', '370505').

card_in_set('countryside crusher', 'MMA').
card_original_type('countryside crusher'/'MMA', 'Creature — Giant Warrior').
card_original_text('countryside crusher'/'MMA', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a land card, put it into your graveyard and repeat this process.\nWhenever a land card is put into your graveyard from anywhere, put a +1/+1 counter on Countryside Crusher.').
card_image_name('countryside crusher'/'MMA', 'countryside crusher').
card_uid('countryside crusher'/'MMA', 'MMA:Countryside Crusher:countryside crusher').
card_rarity('countryside crusher'/'MMA', 'Rare').
card_artist('countryside crusher'/'MMA', 'Volkan Baga').
card_number('countryside crusher'/'MMA', '108').
card_multiverse_id('countryside crusher'/'MMA', '370544').

card_in_set('court homunculus', 'MMA').
card_original_type('court homunculus'/'MMA', 'Artifact Creature — Homunculus').
card_original_text('court homunculus'/'MMA', 'Court Homunculus gets +1/+1 as long as you control another artifact.').
card_image_name('court homunculus'/'MMA', 'court homunculus').
card_uid('court homunculus'/'MMA', 'MMA:Court Homunculus:court homunculus').
card_rarity('court homunculus'/'MMA', 'Common').
card_artist('court homunculus'/'MMA', 'Matt Cavotta').
card_number('court homunculus'/'MMA', '11').
card_flavor_text('court homunculus'/'MMA', 'Mages of Esper measure their wealth and status by the number of servants in their retinues.').
card_multiverse_id('court homunculus'/'MMA', '370495').

card_in_set('crush underfoot', 'MMA').
card_original_type('crush underfoot'/'MMA', 'Tribal Instant — Giant').
card_original_text('crush underfoot'/'MMA', 'Choose a Giant creature you control. It deals damage equal to its power to target creature.').
card_image_name('crush underfoot'/'MMA', 'crush underfoot').
card_uid('crush underfoot'/'MMA', 'MMA:Crush Underfoot:crush underfoot').
card_rarity('crush underfoot'/'MMA', 'Common').
card_artist('crush underfoot'/'MMA', 'Steven Belledin').
card_number('crush underfoot'/'MMA', '109').
card_flavor_text('crush underfoot'/'MMA', 'Five-toed grave\n—Kithkin phrase meaning\n\"a giant\'s footprint\"').
card_multiverse_id('crush underfoot'/'MMA', '370446').

card_in_set('cryptic command', 'MMA').
card_original_type('cryptic command'/'MMA', 'Instant').
card_original_text('cryptic command'/'MMA', 'Choose two — Counter target spell; or return target permanent to its owner\'s hand; or tap all creatures your opponents control; or draw a card.').
card_image_name('cryptic command'/'MMA', 'cryptic command').
card_uid('cryptic command'/'MMA', 'MMA:Cryptic Command:cryptic command').
card_rarity('cryptic command'/'MMA', 'Rare').
card_artist('cryptic command'/'MMA', 'Wayne England').
card_number('cryptic command'/'MMA', '38').
card_multiverse_id('cryptic command'/'MMA', '370439').

card_in_set('dakmor salvage', 'MMA').
card_original_type('dakmor salvage'/'MMA', 'Land').
card_original_text('dakmor salvage'/'MMA', 'Dakmor Salvage enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('dakmor salvage'/'MMA', 'dakmor salvage').
card_uid('dakmor salvage'/'MMA', 'MMA:Dakmor Salvage:dakmor salvage').
card_rarity('dakmor salvage'/'MMA', 'Uncommon').
card_artist('dakmor salvage'/'MMA', 'John Avon').
card_number('dakmor salvage'/'MMA', '222').
card_multiverse_id('dakmor salvage'/'MMA', '370456').

card_in_set('dampen thought', 'MMA').
card_original_type('dampen thought'/'MMA', 'Instant — Arcane').
card_original_text('dampen thought'/'MMA', 'Target player puts the top four cards of his or her library into his or her graveyard.\nSplice onto Arcane {1}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('dampen thought'/'MMA', 'dampen thought').
card_uid('dampen thought'/'MMA', 'MMA:Dampen Thought:dampen thought').
card_rarity('dampen thought'/'MMA', 'Common').
card_artist('dampen thought'/'MMA', 'Arnie Swekel').
card_number('dampen thought'/'MMA', '39').
card_multiverse_id('dampen thought'/'MMA', '370538').

card_in_set('dark confidant', 'MMA').
card_original_type('dark confidant'/'MMA', 'Creature — Human Wizard').
card_original_text('dark confidant'/'MMA', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_image_name('dark confidant'/'MMA', 'dark confidant').
card_uid('dark confidant'/'MMA', 'MMA:Dark Confidant:dark confidant').
card_rarity('dark confidant'/'MMA', 'Mythic Rare').
card_artist('dark confidant'/'MMA', 'Scott M. Fischer').
card_number('dark confidant'/'MMA', '75').
card_flavor_text('dark confidant'/'MMA', 'Greatness, at any cost.').
card_multiverse_id('dark confidant'/'MMA', '370413').

card_in_set('death cloud', 'MMA').
card_original_type('death cloud'/'MMA', 'Sorcery').
card_original_text('death cloud'/'MMA', 'Each player loses X life, discards X cards, sacrifices X creatures, then sacrifices X lands.').
card_image_name('death cloud'/'MMA', 'death cloud').
card_uid('death cloud'/'MMA', 'MMA:Death Cloud:death cloud').
card_rarity('death cloud'/'MMA', 'Rare').
card_artist('death cloud'/'MMA', 'Stephen Tappin').
card_number('death cloud'/'MMA', '76').
card_flavor_text('death cloud'/'MMA', 'The swarm\'s million wings stir the foulest of breezes.').
card_multiverse_id('death cloud'/'MMA', '370381').

card_in_set('death denied', 'MMA').
card_original_type('death denied'/'MMA', 'Instant — Arcane').
card_original_text('death denied'/'MMA', 'Return X target creature cards from your graveyard to your hand.').
card_image_name('death denied'/'MMA', 'death denied').
card_uid('death denied'/'MMA', 'MMA:Death Denied:death denied').
card_rarity('death denied'/'MMA', 'Common').
card_artist('death denied'/'MMA', 'James Paick').
card_number('death denied'/'MMA', '77').
card_flavor_text('death denied'/'MMA', 'Takenuma was filled with a chorus of moans, shrieks, and wails. Some came from the living, some from the dying, and some, most horribly, from the dead.').
card_multiverse_id('death denied'/'MMA', '370492').

card_in_set('death rattle', 'MMA').
card_original_type('death rattle'/'MMA', 'Instant').
card_original_text('death rattle'/'MMA', 'Delve (You may exile any number of cards from your graveyard as you cast this spell. It costs {1} less to cast for each card exiled this way.)\nDestroy target nongreen creature. It can\'t be regenerated.').
card_image_name('death rattle'/'MMA', 'death rattle').
card_uid('death rattle'/'MMA', 'MMA:Death Rattle:death rattle').
card_rarity('death rattle'/'MMA', 'Uncommon').
card_artist('death rattle'/'MMA', 'Vance Kovacs').
card_number('death rattle'/'MMA', '78').
card_multiverse_id('death rattle'/'MMA', '370477').

card_in_set('deepcavern imp', 'MMA').
card_original_type('deepcavern imp'/'MMA', 'Creature — Imp Rebel').
card_original_text('deepcavern imp'/'MMA', 'Flying, haste\nEcho—Discard a card. (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_image_name('deepcavern imp'/'MMA', 'deepcavern imp').
card_uid('deepcavern imp'/'MMA', 'MMA:Deepcavern Imp:deepcavern imp').
card_rarity('deepcavern imp'/'MMA', 'Common').
card_artist('deepcavern imp'/'MMA', 'Scott Altmann').
card_number('deepcavern imp'/'MMA', '79').
card_multiverse_id('deepcavern imp'/'MMA', '370569').

card_in_set('demigod of revenge', 'MMA').
card_original_type('demigod of revenge'/'MMA', 'Creature — Spirit Avatar').
card_original_text('demigod of revenge'/'MMA', 'Flying, haste\nWhen you cast Demigod of Revenge, return all cards named Demigod of Revenge from your graveyard to the battlefield.').
card_image_name('demigod of revenge'/'MMA', 'demigod of revenge').
card_uid('demigod of revenge'/'MMA', 'MMA:Demigod of Revenge:demigod of revenge').
card_rarity('demigod of revenge'/'MMA', 'Rare').
card_artist('demigod of revenge'/'MMA', 'Jim Murray').
card_number('demigod of revenge'/'MMA', '187').
card_flavor_text('demigod of revenge'/'MMA', '\"His laugh, a bellowing, deathly din, slices through the heavens, making them bleed.\"\n—The Seer\'s Parables').
card_multiverse_id('demigod of revenge'/'MMA', '370463').

card_in_set('desperate ritual', 'MMA').
card_original_type('desperate ritual'/'MMA', 'Instant — Arcane').
card_original_text('desperate ritual'/'MMA', 'Add {R}{R}{R} to your mana pool.\nSplice onto Arcane {1}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('desperate ritual'/'MMA', 'desperate ritual').
card_uid('desperate ritual'/'MMA', 'MMA:Desperate Ritual:desperate ritual').
card_rarity('desperate ritual'/'MMA', 'Uncommon').
card_artist('desperate ritual'/'MMA', 'Darrell Riche').
card_number('desperate ritual'/'MMA', '110').
card_multiverse_id('desperate ritual'/'MMA', '370546').

card_in_set('dispeller\'s capsule', 'MMA').
card_original_type('dispeller\'s capsule'/'MMA', 'Artifact').
card_original_text('dispeller\'s capsule'/'MMA', '{2}{W}, {T}, Sacrifice Dispeller\'s Capsule: Destroy target artifact or enchantment.').
card_image_name('dispeller\'s capsule'/'MMA', 'dispeller\'s capsule').
card_uid('dispeller\'s capsule'/'MMA', 'MMA:Dispeller\'s Capsule:dispeller\'s capsule').
card_rarity('dispeller\'s capsule'/'MMA', 'Common').
card_artist('dispeller\'s capsule'/'MMA', 'Franz Vohwinkel').
card_number('dispeller\'s capsule'/'MMA', '12').
card_flavor_text('dispeller\'s capsule'/'MMA', '\"I find its symmetry pleasing. It rids our world of offensive refuse while disposing of itself.\"\n—Dolomarus, Proctor of the Clean').
card_multiverse_id('dispeller\'s capsule'/'MMA', '370527').

card_in_set('divinity of pride', 'MMA').
card_original_type('divinity of pride'/'MMA', 'Creature — Spirit Avatar').
card_original_text('divinity of pride'/'MMA', 'Flying, lifelink\nDivinity of Pride gets +4/+4 as long as you have 25 or more life.').
card_image_name('divinity of pride'/'MMA', 'divinity of pride').
card_uid('divinity of pride'/'MMA', 'MMA:Divinity of Pride:divinity of pride').
card_rarity('divinity of pride'/'MMA', 'Rare').
card_artist('divinity of pride'/'MMA', 'Greg Staples').
card_number('divinity of pride'/'MMA', '188').
card_flavor_text('divinity of pride'/'MMA', '\"She spoke of mystery and portent, of such unfathomable things that my mind screamed for pity.\"\n—The Seer\'s Parables').
card_multiverse_id('divinity of pride'/'MMA', '370565').

card_in_set('doubling season', 'MMA').
card_original_type('doubling season'/'MMA', 'Enchantment').
card_original_text('doubling season'/'MMA', 'If an effect would put one or more tokens onto the battlefield under your control, it puts twice that many of those tokens onto the battlefield instead.\nIf an effect would place one or more counters on a permanent you control, it places twice that many of those counters on that permanent instead.').
card_image_name('doubling season'/'MMA', 'doubling season').
card_uid('doubling season'/'MMA', 'MMA:Doubling Season:doubling season').
card_rarity('doubling season'/'MMA', 'Rare').
card_artist('doubling season'/'MMA', 'Chuck Lukacs').
card_number('doubling season'/'MMA', '141').
card_multiverse_id('doubling season'/'MMA', '370387').

card_in_set('drag down', 'MMA').
card_original_type('drag down'/'MMA', 'Instant').
card_original_text('drag down'/'MMA', 'Domain — Target creature gets -1/-1 until end of turn for each basic land type among lands you control.').
card_image_name('drag down'/'MMA', 'drag down').
card_uid('drag down'/'MMA', 'MMA:Drag Down:drag down').
card_rarity('drag down'/'MMA', 'Common').
card_artist('drag down'/'MMA', 'Trevor Claxton').
card_number('drag down'/'MMA', '80').
card_flavor_text('drag down'/'MMA', 'The barbarians of Jund believe the bottomless tar pits extend forever into other, darker worlds.').
card_multiverse_id('drag down'/'MMA', '370391').

card_in_set('dragonstorm', 'MMA').
card_original_type('dragonstorm'/'MMA', 'Sorcery').
card_original_text('dragonstorm'/'MMA', 'Search your library for a Dragon permanent card and put it onto the battlefield. Then shuffle your library.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('dragonstorm'/'MMA', 'dragonstorm').
card_uid('dragonstorm'/'MMA', 'MMA:Dragonstorm:dragonstorm').
card_rarity('dragonstorm'/'MMA', 'Rare').
card_artist('dragonstorm'/'MMA', 'Kev Walker').
card_number('dragonstorm'/'MMA', '111').
card_multiverse_id('dragonstorm'/'MMA', '370537').

card_in_set('dreamspoiler witches', 'MMA').
card_original_type('dreamspoiler witches'/'MMA', 'Creature — Faerie Wizard').
card_original_text('dreamspoiler witches'/'MMA', 'Flying\nWhenever you cast a spell during an opponent\'s turn, you may have target creature get -1/-1 until end of turn.').
card_image_name('dreamspoiler witches'/'MMA', 'dreamspoiler witches').
card_uid('dreamspoiler witches'/'MMA', 'MMA:Dreamspoiler Witches:dreamspoiler witches').
card_rarity('dreamspoiler witches'/'MMA', 'Common').
card_artist('dreamspoiler witches'/'MMA', 'Jeff Easley').
card_number('dreamspoiler witches'/'MMA', '81').
card_flavor_text('dreamspoiler witches'/'MMA', 'At night, the faeries steal dreamstuff for their queen. At daybreak, countless creatures wake weak and hollow.').
card_multiverse_id('dreamspoiler witches'/'MMA', '370473').

card_in_set('durkwood baloth', 'MMA').
card_original_type('durkwood baloth'/'MMA', 'Creature — Beast').
card_original_text('durkwood baloth'/'MMA', 'Suspend 5—{G} (Rather than cast this card from your hand, you may pay {G} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('durkwood baloth'/'MMA', 'durkwood baloth').
card_uid('durkwood baloth'/'MMA', 'MMA:Durkwood Baloth:durkwood baloth').
card_rarity('durkwood baloth'/'MMA', 'Common').
card_artist('durkwood baloth'/'MMA', 'Dan Frazier').
card_number('durkwood baloth'/'MMA', '142').
card_multiverse_id('durkwood baloth'/'MMA', '370482').

card_in_set('earwig squad', 'MMA').
card_original_type('earwig squad'/'MMA', 'Creature — Goblin Rogue').
card_original_text('earwig squad'/'MMA', 'Prowl {2}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhen Earwig Squad enters the battlefield, if its prowl cost was paid, search target opponent\'s library for three cards and exile them. Then that player shuffles his or her library.').
card_image_name('earwig squad'/'MMA', 'earwig squad').
card_uid('earwig squad'/'MMA', 'MMA:Earwig Squad:earwig squad').
card_rarity('earwig squad'/'MMA', 'Rare').
card_artist('earwig squad'/'MMA', 'Warren Mahy').
card_number('earwig squad'/'MMA', '82').
card_multiverse_id('earwig squad'/'MMA', '370530').

card_in_set('echoing courage', 'MMA').
card_original_type('echoing courage'/'MMA', 'Instant').
card_original_text('echoing courage'/'MMA', 'Target creature and all other creatures with the same name as that creature get +2/+2 until end of turn.').
card_image_name('echoing courage'/'MMA', 'echoing courage').
card_uid('echoing courage'/'MMA', 'MMA:Echoing Courage:echoing courage').
card_rarity('echoing courage'/'MMA', 'Common').
card_artist('echoing courage'/'MMA', 'Greg Staples').
card_number('echoing courage'/'MMA', '143').
card_flavor_text('echoing courage'/'MMA', 'A single seed unleashes a flurry of growth.').
card_multiverse_id('echoing courage'/'MMA', '370393').

card_in_set('echoing truth', 'MMA').
card_original_type('echoing truth'/'MMA', 'Instant').
card_original_text('echoing truth'/'MMA', 'Return target nonland permanent and all other permanents with the same name as that permanent to their owners\' hands.').
card_image_name('echoing truth'/'MMA', 'echoing truth').
card_uid('echoing truth'/'MMA', 'MMA:Echoing Truth:echoing truth').
card_rarity('echoing truth'/'MMA', 'Common').
card_artist('echoing truth'/'MMA', 'Greg Staples').
card_number('echoing truth'/'MMA', '40').
card_flavor_text('echoing truth'/'MMA', 'A single lie unleashes a tide of disbelief.').
card_multiverse_id('echoing truth'/'MMA', '370394').

card_in_set('electrolyze', 'MMA').
card_original_type('electrolyze'/'MMA', 'Instant').
card_original_text('electrolyze'/'MMA', 'Electrolyze deals 2 damage divided as you choose among one or two target creatures and/or players.\nDraw a card.').
card_image_name('electrolyze'/'MMA', 'electrolyze').
card_uid('electrolyze'/'MMA', 'MMA:Electrolyze:electrolyze').
card_rarity('electrolyze'/'MMA', 'Uncommon').
card_artist('electrolyze'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('electrolyze'/'MMA', '175').
card_flavor_text('electrolyze'/'MMA', 'The Izzet learn something from every lesson they teach.').
card_multiverse_id('electrolyze'/'MMA', '370376').
card_watermark('electrolyze'/'MMA', 'Izzet').

card_in_set('elspeth, knight-errant', 'MMA').
card_original_type('elspeth, knight-errant'/'MMA', 'Planeswalker — Elspeth').
card_original_text('elspeth, knight-errant'/'MMA', '+1: Put a 1/1 white Soldier creature token onto the battlefield.\n+1: Target creature gets +3/+3 and gains flying until end of turn.\n-8: You get an emblem with \"Artifacts, creatures, enchantments, and lands you control are indestructible.\"').
card_image_name('elspeth, knight-errant'/'MMA', 'elspeth, knight-errant').
card_uid('elspeth, knight-errant'/'MMA', 'MMA:Elspeth, Knight-Errant:elspeth, knight-errant').
card_rarity('elspeth, knight-errant'/'MMA', 'Mythic Rare').
card_artist('elspeth, knight-errant'/'MMA', 'Volkan Baga').
card_number('elspeth, knight-errant'/'MMA', '13').
card_multiverse_id('elspeth, knight-errant'/'MMA', '370551').

card_in_set('empty the warrens', 'MMA').
card_original_type('empty the warrens'/'MMA', 'Sorcery').
card_original_text('empty the warrens'/'MMA', 'Put two 1/1 red Goblin creature tokens onto the battlefield.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('empty the warrens'/'MMA', 'empty the warrens').
card_uid('empty the warrens'/'MMA', 'MMA:Empty the Warrens:empty the warrens').
card_rarity('empty the warrens'/'MMA', 'Common').
card_artist('empty the warrens'/'MMA', 'Jasper Sandner').
card_number('empty the warrens'/'MMA', '112').
card_flavor_text('empty the warrens'/'MMA', '\"They\'d pour out of the warrens to make war (and to make room for the littering matrons).\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('empty the warrens'/'MMA', '370480').

card_in_set('engineered explosives', 'MMA').
card_original_type('engineered explosives'/'MMA', 'Artifact').
card_original_text('engineered explosives'/'MMA', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\n{2}, Sacrifice Engineered Explosives: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Engineered Explosives.').
card_image_name('engineered explosives'/'MMA', 'engineered explosives').
card_uid('engineered explosives'/'MMA', 'MMA:Engineered Explosives:engineered explosives').
card_rarity('engineered explosives'/'MMA', 'Rare').
card_artist('engineered explosives'/'MMA', 'Lars Grant-West').
card_number('engineered explosives'/'MMA', '204').
card_multiverse_id('engineered explosives'/'MMA', '370549').

card_in_set('epochrasite', 'MMA').
card_original_type('epochrasite'/'MMA', 'Artifact Creature — Construct').
card_original_text('epochrasite'/'MMA', 'Epochrasite enters the battlefield with three +1/+1 counters on it if you didn\'t cast it from your hand.\nWhen Epochrasite dies, exile it with three time counters on it and it gains suspend. (At the beginning of your upkeep, remove a time counter. When the last is removed, cast this card without paying its mana cost. It has haste.)').
card_image_name('epochrasite'/'MMA', 'epochrasite').
card_uid('epochrasite'/'MMA', 'MMA:Epochrasite:epochrasite').
card_rarity('epochrasite'/'MMA', 'Uncommon').
card_artist('epochrasite'/'MMA', 'Michael Bruinsma').
card_number('epochrasite'/'MMA', '205').
card_multiverse_id('epochrasite'/'MMA', '370497').

card_in_set('errant ephemeron', 'MMA').
card_original_type('errant ephemeron'/'MMA', 'Creature — Illusion').
card_original_text('errant ephemeron'/'MMA', 'Flying\nSuspend 4—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('errant ephemeron'/'MMA', 'errant ephemeron').
card_uid('errant ephemeron'/'MMA', 'MMA:Errant Ephemeron:errant ephemeron').
card_rarity('errant ephemeron'/'MMA', 'Common').
card_artist('errant ephemeron'/'MMA', 'Luca Zontini').
card_number('errant ephemeron'/'MMA', '41').
card_multiverse_id('errant ephemeron'/'MMA', '370491').

card_in_set('erratic mutation', 'MMA').
card_original_type('erratic mutation'/'MMA', 'Instant').
card_original_text('erratic mutation'/'MMA', 'Choose target creature. Reveal cards from the top of your library until you reveal a nonland card. That creature gets +X/-X until end of turn, where X is that card\'s converted mana cost. Put all cards revealed this way on the bottom of your library in any order.').
card_image_name('erratic mutation'/'MMA', 'erratic mutation').
card_uid('erratic mutation'/'MMA', 'MMA:Erratic Mutation:erratic mutation').
card_rarity('erratic mutation'/'MMA', 'Common').
card_artist('erratic mutation'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('erratic mutation'/'MMA', '42').
card_multiverse_id('erratic mutation'/'MMA', '370575').

card_in_set('esperzoa', 'MMA').
card_original_type('esperzoa'/'MMA', 'Artifact Creature — Jellyfish').
card_original_text('esperzoa'/'MMA', 'Flying\nAt the beginning of your upkeep, return an artifact you control to its owner\'s hand.').
card_image_name('esperzoa'/'MMA', 'esperzoa').
card_uid('esperzoa'/'MMA', 'MMA:Esperzoa:esperzoa').
card_rarity('esperzoa'/'MMA', 'Uncommon').
card_artist('esperzoa'/'MMA', 'Warren Mahy').
card_number('esperzoa'/'MMA', '43').
card_flavor_text('esperzoa'/'MMA', 'The more metal it digests, the more its jelly will fetch on the alchemists\' market.').
card_multiverse_id('esperzoa'/'MMA', '370498').

card_in_set('etched oracle', 'MMA').
card_original_type('etched oracle'/'MMA', 'Artifact Creature — Wizard').
card_original_text('etched oracle'/'MMA', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\n{1}, Remove four +1/+1 counters from Etched Oracle: Target player draws three cards.').
card_image_name('etched oracle'/'MMA', 'etched oracle').
card_uid('etched oracle'/'MMA', 'MMA:Etched Oracle:etched oracle').
card_rarity('etched oracle'/'MMA', 'Uncommon').
card_artist('etched oracle'/'MMA', 'Matt Cavotta').
card_number('etched oracle'/'MMA', '206').
card_multiverse_id('etched oracle'/'MMA', '370435').

card_in_set('eternal witness', 'MMA').
card_original_type('eternal witness'/'MMA', 'Creature — Human Shaman').
card_original_text('eternal witness'/'MMA', 'When Eternal Witness enters the battlefield, you may return target card from your graveyard to your hand.').
card_image_name('eternal witness'/'MMA', 'eternal witness').
card_uid('eternal witness'/'MMA', 'MMA:Eternal Witness:eternal witness').
card_rarity('eternal witness'/'MMA', 'Uncommon').
card_artist('eternal witness'/'MMA', 'Terese Nielsen').
card_number('eternal witness'/'MMA', '144').
card_flavor_text('eternal witness'/'MMA', 'She remembers every word spoken, from the hero\'s oath to the baby\'s cry.').
card_multiverse_id('eternal witness'/'MMA', '370427').

card_in_set('etherium sculptor', 'MMA').
card_original_type('etherium sculptor'/'MMA', 'Artifact Creature — Vedalken Artificer').
card_original_text('etherium sculptor'/'MMA', 'Artifact spells you cast cost {1} less to cast.').
card_image_name('etherium sculptor'/'MMA', 'etherium sculptor').
card_uid('etherium sculptor'/'MMA', 'MMA:Etherium Sculptor:etherium sculptor').
card_rarity('etherium sculptor'/'MMA', 'Common').
card_artist('etherium sculptor'/'MMA', 'Steven Belledin').
card_number('etherium sculptor'/'MMA', '44').
card_flavor_text('etherium sculptor'/'MMA', 'The greatest masters of the craft abandon tools altogether, shaping metal with hand and mind alone.').
card_multiverse_id('etherium sculptor'/'MMA', '370378').

card_in_set('ethersworn canonist', 'MMA').
card_original_type('ethersworn canonist'/'MMA', 'Artifact Creature — Human Cleric').
card_original_text('ethersworn canonist'/'MMA', 'Each player who has cast a nonartifact spell this turn can\'t cast additional nonartifact spells.').
card_image_name('ethersworn canonist'/'MMA', 'ethersworn canonist').
card_uid('ethersworn canonist'/'MMA', 'MMA:Ethersworn Canonist:ethersworn canonist').
card_rarity('ethersworn canonist'/'MMA', 'Rare').
card_artist('ethersworn canonist'/'MMA', 'Izzy').
card_number('ethersworn canonist'/'MMA', '14').
card_flavor_text('ethersworn canonist'/'MMA', '\"The noble work of our order is to infuse all life on Esper with etherium. Our goal will be reached more rapidly if new life is . . . suppressed.\"').
card_multiverse_id('ethersworn canonist'/'MMA', '370504').

card_in_set('executioner\'s capsule', 'MMA').
card_original_type('executioner\'s capsule'/'MMA', 'Artifact').
card_original_text('executioner\'s capsule'/'MMA', '{1}{B}, {T}, Sacrifice Executioner\'s Capsule: Destroy target nonblack creature.').
card_image_name('executioner\'s capsule'/'MMA', 'executioner\'s capsule').
card_uid('executioner\'s capsule'/'MMA', 'MMA:Executioner\'s Capsule:executioner\'s capsule').
card_rarity('executioner\'s capsule'/'MMA', 'Uncommon').
card_artist('executioner\'s capsule'/'MMA', 'Warren Mahy').
card_number('executioner\'s capsule'/'MMA', '83').
card_flavor_text('executioner\'s capsule'/'MMA', 'There is always a moment of trepidation before opening a message capsule, for fear of the judgment that might be contained within.').
card_multiverse_id('executioner\'s capsule'/'MMA', '370462').

card_in_set('extirpate', 'MMA').
card_original_type('extirpate'/'MMA', 'Instant').
card_original_text('extirpate'/'MMA', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nChoose target card in a graveyard other than a basic land card. Search its owner\'s graveyard, hand, and library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('extirpate'/'MMA', 'extirpate').
card_uid('extirpate'/'MMA', 'MMA:Extirpate:extirpate').
card_rarity('extirpate'/'MMA', 'Rare').
card_artist('extirpate'/'MMA', 'Jon Foster').
card_number('extirpate'/'MMA', '84').
card_multiverse_id('extirpate'/'MMA', '370384').

card_in_set('facevaulter', 'MMA').
card_original_type('facevaulter'/'MMA', 'Creature — Goblin Warrior').
card_original_text('facevaulter'/'MMA', '{B}, Sacrifice a Goblin: Facevaulter gets +2/+2 until end of turn.').
card_image_name('facevaulter'/'MMA', 'facevaulter').
card_uid('facevaulter'/'MMA', 'MMA:Facevaulter:facevaulter').
card_rarity('facevaulter'/'MMA', 'Common').
card_artist('facevaulter'/'MMA', 'Wayne Reynolds').
card_number('facevaulter'/'MMA', '85').
card_flavor_text('facevaulter'/'MMA', 'Boggarts get so excited when they find something new to smash that they really don\'t notice who gets underfoot.').
card_multiverse_id('facevaulter'/'MMA', '370499').

card_in_set('faerie macabre', 'MMA').
card_original_type('faerie macabre'/'MMA', 'Creature — Faerie Rogue').
card_original_text('faerie macabre'/'MMA', 'Flying\nDiscard Faerie Macabre: Exile up to two target cards from graveyards.').
card_image_name('faerie macabre'/'MMA', 'faerie macabre').
card_uid('faerie macabre'/'MMA', 'MMA:Faerie Macabre:faerie macabre').
card_rarity('faerie macabre'/'MMA', 'Common').
card_artist('faerie macabre'/'MMA', 'rk post').
card_number('faerie macabre'/'MMA', '86').
card_flavor_text('faerie macabre'/'MMA', 'The line between dream and death is gauzy and fragile. She leads those too near it from one side to the other.').
card_multiverse_id('faerie macabre'/'MMA', '370410').

card_in_set('faerie mechanist', 'MMA').
card_original_type('faerie mechanist'/'MMA', 'Artifact Creature — Faerie Artificer').
card_original_text('faerie mechanist'/'MMA', 'Flying\nWhen Faerie Mechanist enters the battlefield, look at the top three cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('faerie mechanist'/'MMA', 'faerie mechanist').
card_uid('faerie mechanist'/'MMA', 'MMA:Faerie Mechanist:faerie mechanist').
card_rarity('faerie mechanist'/'MMA', 'Common').
card_artist('faerie mechanist'/'MMA', 'Matt Cavotta').
card_number('faerie mechanist'/'MMA', '45').
card_multiverse_id('faerie mechanist'/'MMA', '370421').

card_in_set('festering goblin', 'MMA').
card_original_type('festering goblin'/'MMA', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'MMA', 'When Festering Goblin dies, target creature gets -1/-1 until end of turn.').
card_image_name('festering goblin'/'MMA', 'festering goblin').
card_uid('festering goblin'/'MMA', 'MMA:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'MMA', 'Common').
card_artist('festering goblin'/'MMA', 'Thomas M. Baxa').
card_number('festering goblin'/'MMA', '87').
card_flavor_text('festering goblin'/'MMA', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'MMA', '370358').

card_in_set('feudkiller\'s verdict', 'MMA').
card_original_type('feudkiller\'s verdict'/'MMA', 'Tribal Sorcery — Giant').
card_original_text('feudkiller\'s verdict'/'MMA', 'You gain 10 life. Then if you have more life than an opponent, put a 5/5 white Giant Warrior creature token onto the battlefield.').
card_image_name('feudkiller\'s verdict'/'MMA', 'feudkiller\'s verdict').
card_uid('feudkiller\'s verdict'/'MMA', 'MMA:Feudkiller\'s Verdict:feudkiller\'s verdict').
card_rarity('feudkiller\'s verdict'/'MMA', 'Uncommon').
card_artist('feudkiller\'s verdict'/'MMA', 'Dan Scott').
card_number('feudkiller\'s verdict'/'MMA', '15').
card_flavor_text('feudkiller\'s verdict'/'MMA', '\"There are all kinds of strengths, but if you have strength of soul, the others will follow.\"\n—Galanda Feudkiller').
card_multiverse_id('feudkiller\'s verdict'/'MMA', '370438').

card_in_set('fiery fall', 'MMA').
card_original_type('fiery fall'/'MMA', 'Instant').
card_original_text('fiery fall'/'MMA', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('fiery fall'/'MMA', 'fiery fall').
card_uid('fiery fall'/'MMA', 'MMA:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'MMA', 'Common').
card_artist('fiery fall'/'MMA', 'Daarken').
card_number('fiery fall'/'MMA', '113').
card_flavor_text('fiery fall'/'MMA', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'MMA', '370367').

card_in_set('figure of destiny', 'MMA').
card_original_type('figure of destiny'/'MMA', 'Creature — Kithkin').
card_original_text('figure of destiny'/'MMA', '{R/W}: Figure of Destiny becomes a 2/2 Kithkin Spirit.\n{R/W}{R/W}{R/W}: If Figure of Destiny is a Spirit, it becomes a 4/4 Kithkin Spirit Warrior.\n{R/W}{R/W}{R/W}{R/W}{R/W}{R/W}: If Figure of Destiny is a Warrior, it becomes an 8/8 Kithkin Spirit Warrior Avatar with flying and first strike.').
card_image_name('figure of destiny'/'MMA', 'figure of destiny').
card_uid('figure of destiny'/'MMA', 'MMA:Figure of Destiny:figure of destiny').
card_rarity('figure of destiny'/'MMA', 'Rare').
card_artist('figure of destiny'/'MMA', 'Scott M. Fischer').
card_number('figure of destiny'/'MMA', '189').
card_multiverse_id('figure of destiny'/'MMA', '370466').

card_in_set('flickerwisp', 'MMA').
card_original_type('flickerwisp'/'MMA', 'Creature — Elemental').
card_original_text('flickerwisp'/'MMA', 'Flying\nWhen Flickerwisp enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('flickerwisp'/'MMA', 'flickerwisp').
card_uid('flickerwisp'/'MMA', 'MMA:Flickerwisp:flickerwisp').
card_rarity('flickerwisp'/'MMA', 'Uncommon').
card_artist('flickerwisp'/'MMA', 'Jeremy Enecio').
card_number('flickerwisp'/'MMA', '16').
card_flavor_text('flickerwisp'/'MMA', 'Its wings disturb more than air.').
card_multiverse_id('flickerwisp'/'MMA', '370449').

card_in_set('frogmite', 'MMA').
card_original_type('frogmite'/'MMA', 'Artifact Creature — Frog').
card_original_text('frogmite'/'MMA', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('frogmite'/'MMA', 'frogmite').
card_uid('frogmite'/'MMA', 'MMA:Frogmite:frogmite').
card_rarity('frogmite'/'MMA', 'Common').
card_artist('frogmite'/'MMA', 'Terese Nielsen').
card_number('frogmite'/'MMA', '207').
card_flavor_text('frogmite'/'MMA', 'At first, vedalken observers thought blinkmoths naturally avoided certain places. Then they realized those places were frogmite feeding grounds.').
card_multiverse_id('frogmite'/'MMA', '370434').

card_in_set('fury charm', 'MMA').
card_original_type('fury charm'/'MMA', 'Instant').
card_original_text('fury charm'/'MMA', 'Choose one — Destroy target artifact; or target creature gets +1/+1 and gains trample until end of turn; or remove two time counters from target permanent or suspended card.').
card_image_name('fury charm'/'MMA', 'fury charm').
card_uid('fury charm'/'MMA', 'MMA:Fury Charm:fury charm').
card_rarity('fury charm'/'MMA', 'Common').
card_artist('fury charm'/'MMA', 'John Avon').
card_number('fury charm'/'MMA', '114').
card_multiverse_id('fury charm'/'MMA', '370465').

card_in_set('giant dustwasp', 'MMA').
card_original_type('giant dustwasp'/'MMA', 'Creature — Insect').
card_original_text('giant dustwasp'/'MMA', 'Flying\nSuspend 4—{1}{G} (Rather than cast this card from your hand, you may pay {1}{G} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('giant dustwasp'/'MMA', 'giant dustwasp').
card_uid('giant dustwasp'/'MMA', 'MMA:Giant Dustwasp:giant dustwasp').
card_rarity('giant dustwasp'/'MMA', 'Common').
card_artist('giant dustwasp'/'MMA', 'Greg Hildebrandt').
card_number('giant dustwasp'/'MMA', '145').
card_multiverse_id('giant dustwasp'/'MMA', '370433').

card_in_set('gifts ungiven', 'MMA').
card_original_type('gifts ungiven'/'MMA', 'Instant').
card_original_text('gifts ungiven'/'MMA', 'Search your library for up to four cards with different names and reveal them. Target opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_image_name('gifts ungiven'/'MMA', 'gifts ungiven').
card_uid('gifts ungiven'/'MMA', 'MMA:Gifts Ungiven:gifts ungiven').
card_rarity('gifts ungiven'/'MMA', 'Rare').
card_artist('gifts ungiven'/'MMA', 'D. Alexander Gregory').
card_number('gifts ungiven'/'MMA', '46').
card_multiverse_id('gifts ungiven'/'MMA', '370368').

card_in_set('glacial ray', 'MMA').
card_original_type('glacial ray'/'MMA', 'Instant — Arcane').
card_original_text('glacial ray'/'MMA', 'Glacial Ray deals 2 damage to target creature or player.\nSplice onto Arcane {1}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('glacial ray'/'MMA', 'glacial ray').
card_uid('glacial ray'/'MMA', 'MMA:Glacial Ray:glacial ray').
card_rarity('glacial ray'/'MMA', 'Common').
card_artist('glacial ray'/'MMA', 'Jim Murray').
card_number('glacial ray'/'MMA', '115').
card_multiverse_id('glacial ray'/'MMA', '370552').

card_in_set('gleam of resistance', 'MMA').
card_original_type('gleam of resistance'/'MMA', 'Instant').
card_original_text('gleam of resistance'/'MMA', 'Creatures you control get +1/+2 until end of turn. Untap those creatures.\nBasic landcycling {1}{W} ({1}{W}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('gleam of resistance'/'MMA', 'gleam of resistance').
card_uid('gleam of resistance'/'MMA', 'MMA:Gleam of Resistance:gleam of resistance').
card_rarity('gleam of resistance'/'MMA', 'Common').
card_artist('gleam of resistance'/'MMA', 'Matt Stewart').
card_number('gleam of resistance'/'MMA', '17').
card_multiverse_id('gleam of resistance'/'MMA', '370371').

card_in_set('glen elendra archmage', 'MMA').
card_original_type('glen elendra archmage'/'MMA', 'Creature — Faerie Wizard').
card_original_text('glen elendra archmage'/'MMA', 'Flying\n{U}, Sacrifice Glen Elendra Archmage: Counter target noncreature spell.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('glen elendra archmage'/'MMA', 'glen elendra archmage').
card_uid('glen elendra archmage'/'MMA', 'MMA:Glen Elendra Archmage:glen elendra archmage').
card_rarity('glen elendra archmage'/'MMA', 'Rare').
card_artist('glen elendra archmage'/'MMA', 'Warren Mahy').
card_number('glen elendra archmage'/'MMA', '47').
card_multiverse_id('glen elendra archmage'/'MMA', '370522').

card_in_set('glimmervoid', 'MMA').
card_original_type('glimmervoid'/'MMA', 'Land').
card_original_text('glimmervoid'/'MMA', 'At the beginning of the end step, if you control no artifacts, sacrifice Glimmervoid.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('glimmervoid'/'MMA', 'glimmervoid').
card_uid('glimmervoid'/'MMA', 'MMA:Glimmervoid:glimmervoid').
card_rarity('glimmervoid'/'MMA', 'Rare').
card_artist('glimmervoid'/'MMA', 'Lars Grant-West').
card_number('glimmervoid'/'MMA', '223').
card_flavor_text('glimmervoid'/'MMA', 'An empty canvas holds infinite possibilities.').
card_multiverse_id('glimmervoid'/'MMA', '370425').

card_in_set('grand arbiter augustin iv', 'MMA').
card_original_type('grand arbiter augustin iv'/'MMA', 'Legendary Creature — Human Advisor').
card_original_text('grand arbiter augustin iv'/'MMA', 'White spells you cast cost {1} less to cast.\nBlue spells you cast cost {1} less to cast.\nSpells your opponents cast cost {1} more to cast.').
card_image_name('grand arbiter augustin iv'/'MMA', 'grand arbiter augustin iv').
card_uid('grand arbiter augustin iv'/'MMA', 'MMA:Grand Arbiter Augustin IV:grand arbiter augustin iv').
card_rarity('grand arbiter augustin iv'/'MMA', 'Rare').
card_artist('grand arbiter augustin iv'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('grand arbiter augustin iv'/'MMA', '176').
card_flavor_text('grand arbiter augustin iv'/'MMA', 'The Arbiter is a conduit of justice, a will so disciplined that it dispenses justice without ego or remorse.').
card_multiverse_id('grand arbiter augustin iv'/'MMA', '370420').
card_watermark('grand arbiter augustin iv'/'MMA', 'Azorius').

card_in_set('grapeshot', 'MMA').
card_original_type('grapeshot'/'MMA', 'Sorcery').
card_original_text('grapeshot'/'MMA', 'Grapeshot deals 1 damage to target creature or player.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_image_name('grapeshot'/'MMA', 'grapeshot').
card_uid('grapeshot'/'MMA', 'MMA:Grapeshot:grapeshot').
card_rarity('grapeshot'/'MMA', 'Common').
card_artist('grapeshot'/'MMA', 'Clint Cearley').
card_number('grapeshot'/'MMA', '116').
card_flavor_text('grapeshot'/'MMA', 'Mages often seek to emulate the powerful relics lost to time and apocalypse.').
card_multiverse_id('grapeshot'/'MMA', '370472').

card_in_set('greater gargadon', 'MMA').
card_original_type('greater gargadon'/'MMA', 'Creature — Beast').
card_original_text('greater gargadon'/'MMA', 'Suspend 10—{R}\nSacrifice an artifact, creature, or land: Remove a time counter from Greater Gargadon. Activate this ability only if Greater Gargadon is suspended.').
card_image_name('greater gargadon'/'MMA', 'greater gargadon').
card_uid('greater gargadon'/'MMA', 'MMA:Greater Gargadon:greater gargadon').
card_rarity('greater gargadon'/'MMA', 'Rare').
card_artist('greater gargadon'/'MMA', 'Rob Alexander').
card_number('greater gargadon'/'MMA', '117').
card_multiverse_id('greater gargadon'/'MMA', '370560').

card_in_set('greater mossdog', 'MMA').
card_original_type('greater mossdog'/'MMA', 'Creature — Plant Hound').
card_original_text('greater mossdog'/'MMA', 'Dredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('greater mossdog'/'MMA', 'greater mossdog').
card_uid('greater mossdog'/'MMA', 'MMA:Greater Mossdog:greater mossdog').
card_rarity('greater mossdog'/'MMA', 'Common').
card_artist('greater mossdog'/'MMA', 'Chippy').
card_number('greater mossdog'/'MMA', '146').
card_flavor_text('greater mossdog'/'MMA', 'Man\'s best fungus.').
card_multiverse_id('greater mossdog'/'MMA', '370511').
card_watermark('greater mossdog'/'MMA', 'Golgari').

card_in_set('grinning ignus', 'MMA').
card_original_type('grinning ignus'/'MMA', 'Creature — Elemental').
card_original_text('grinning ignus'/'MMA', '{R}, Return Grinning Ignus to its owner\'s hand: Add {2}{R} to your mana pool. Activate this ability only any time you could cast a sorcery.').
card_image_name('grinning ignus'/'MMA', 'grinning ignus').
card_uid('grinning ignus'/'MMA', 'MMA:Grinning Ignus:grinning ignus').
card_rarity('grinning ignus'/'MMA', 'Uncommon').
card_artist('grinning ignus'/'MMA', 'James Kei').
card_number('grinning ignus'/'MMA', '118').
card_flavor_text('grinning ignus'/'MMA', '\"Take care what you offer the ignus. Food, perhaps. Coins. But nothing flammable!\"\n—Stovic, village eccentric').
card_multiverse_id('grinning ignus'/'MMA', '370418').

card_in_set('hammerheim deadeye', 'MMA').
card_original_type('hammerheim deadeye'/'MMA', 'Creature — Giant Warrior').
card_original_text('hammerheim deadeye'/'MMA', 'Echo {5}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Hammerheim Deadeye enters the battlefield, destroy target creature with flying.').
card_image_name('hammerheim deadeye'/'MMA', 'hammerheim deadeye').
card_uid('hammerheim deadeye'/'MMA', 'MMA:Hammerheim Deadeye:hammerheim deadeye').
card_rarity('hammerheim deadeye'/'MMA', 'Common').
card_artist('hammerheim deadeye'/'MMA', 'Carl Critchlow').
card_number('hammerheim deadeye'/'MMA', '119').
card_multiverse_id('hammerheim deadeye'/'MMA', '370401').

card_in_set('hana kami', 'MMA').
card_original_type('hana kami'/'MMA', 'Creature — Spirit').
card_original_text('hana kami'/'MMA', '{1}{G}, Sacrifice Hana Kami: Return target Arcane card from your graveyard to your hand.').
card_image_name('hana kami'/'MMA', 'hana kami').
card_uid('hana kami'/'MMA', 'MMA:Hana Kami:hana kami').
card_rarity('hana kami'/'MMA', 'Common').
card_artist('hana kami'/'MMA', 'Rebecca Guay').
card_number('hana kami'/'MMA', '147').
card_flavor_text('hana kami'/'MMA', 'It grew in lands lit by pride and watered by tears.').
card_multiverse_id('hana kami'/'MMA', '370475').

card_in_set('hillcomber giant', 'MMA').
card_original_type('hillcomber giant'/'MMA', 'Creature — Giant Scout').
card_original_text('hillcomber giant'/'MMA', 'Mountainwalk').
card_image_name('hillcomber giant'/'MMA', 'hillcomber giant').
card_uid('hillcomber giant'/'MMA', 'MMA:Hillcomber Giant:hillcomber giant').
card_rarity('hillcomber giant'/'MMA', 'Common').
card_artist('hillcomber giant'/'MMA', 'Ralph Horsley').
card_number('hillcomber giant'/'MMA', '18').
card_flavor_text('hillcomber giant'/'MMA', 'The giants believe the fossils they find in Lorwyn\'s rocky heights are dreams frozen in time, and they treasure them.').
card_multiverse_id('hillcomber giant'/'MMA', '370386').

card_in_set('horobi\'s whisper', 'MMA').
card_original_type('horobi\'s whisper'/'MMA', 'Instant — Arcane').
card_original_text('horobi\'s whisper'/'MMA', 'If you control a Swamp, destroy target nonblack creature.\nSplice onto Arcane—Exile four cards from your graveyard. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('horobi\'s whisper'/'MMA', 'horobi\'s whisper').
card_uid('horobi\'s whisper'/'MMA', 'MMA:Horobi\'s Whisper:horobi\'s whisper').
card_rarity('horobi\'s whisper'/'MMA', 'Uncommon').
card_artist('horobi\'s whisper'/'MMA', 'Aleksi Briclot').
card_number('horobi\'s whisper'/'MMA', '88').
card_multiverse_id('horobi\'s whisper'/'MMA', '370426').

card_in_set('imperiosaur', 'MMA').
card_original_type('imperiosaur'/'MMA', 'Creature — Lizard').
card_original_text('imperiosaur'/'MMA', 'Spend only mana produced by basic lands to cast Imperiosaur.').
card_image_name('imperiosaur'/'MMA', 'imperiosaur').
card_uid('imperiosaur'/'MMA', 'MMA:Imperiosaur:imperiosaur').
card_rarity('imperiosaur'/'MMA', 'Common').
card_artist('imperiosaur'/'MMA', 'Lars Grant-West').
card_number('imperiosaur'/'MMA', '148').
card_flavor_text('imperiosaur'/'MMA', '\"An ancient, powerful force has overtaken the valley. I sympathize for its former inhabitants, but I rejoice for the land itself.\"\n—Olanti, Muraganda druid').
card_multiverse_id('imperiosaur'/'MMA', '370561').

card_in_set('incremental growth', 'MMA').
card_original_type('incremental growth'/'MMA', 'Sorcery').
card_original_text('incremental growth'/'MMA', 'Put a +1/+1 counter on target creature, two +1/+1 counters on another target creature, and three +1/+1 counters on a third target creature.').
card_image_name('incremental growth'/'MMA', 'incremental growth').
card_uid('incremental growth'/'MMA', 'MMA:Incremental Growth:incremental growth').
card_rarity('incremental growth'/'MMA', 'Uncommon').
card_artist('incremental growth'/'MMA', 'Chuck Lukacs').
card_number('incremental growth'/'MMA', '149').
card_flavor_text('incremental growth'/'MMA', '\"Tie a mouse under a bridge, and your cabbages will triple in size.\"\n—Kithkin superstition').
card_multiverse_id('incremental growth'/'MMA', '370541').

card_in_set('ivory giant', 'MMA').
card_original_type('ivory giant'/'MMA', 'Creature — Giant').
card_original_text('ivory giant'/'MMA', 'When Ivory Giant enters the battlefield, tap all nonwhite creatures.\nSuspend 5—{W} (Rather than cast this card from your hand, you may pay {W} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('ivory giant'/'MMA', 'ivory giant').
card_uid('ivory giant'/'MMA', 'MMA:Ivory Giant:ivory giant').
card_rarity('ivory giant'/'MMA', 'Common').
card_artist('ivory giant'/'MMA', 'Zack Stella').
card_number('ivory giant'/'MMA', '19').
card_multiverse_id('ivory giant'/'MMA', '370415').

card_in_set('jhoira of the ghitu', 'MMA').
card_original_type('jhoira of the ghitu'/'MMA', 'Legendary Creature — Human Wizard').
card_original_text('jhoira of the ghitu'/'MMA', '{2}, Exile a nonland card from your hand: Put four time counters on the exiled card. If it doesn\'t have suspend, it gains suspend. (At the beginning of your upkeep, remove a time counter from that card. When the last is removed, cast it without paying its mana cost. If it\'s a creature, it has haste.)').
card_image_name('jhoira of the ghitu'/'MMA', 'jhoira of the ghitu').
card_uid('jhoira of the ghitu'/'MMA', 'MMA:Jhoira of the Ghitu:jhoira of the ghitu').
card_rarity('jhoira of the ghitu'/'MMA', 'Rare').
card_artist('jhoira of the ghitu'/'MMA', 'Kev Walker').
card_number('jhoira of the ghitu'/'MMA', '177').
card_multiverse_id('jhoira of the ghitu'/'MMA', '370548').

card_in_set('jugan, the rising star', 'MMA').
card_original_type('jugan, the rising star'/'MMA', 'Legendary Creature — Dragon Spirit').
card_original_text('jugan, the rising star'/'MMA', 'Flying\nWhen Jugan, the Rising Star dies, you may distribute five +1/+1 counters among any number of target creatures.').
card_image_name('jugan, the rising star'/'MMA', 'jugan, the rising star').
card_uid('jugan, the rising star'/'MMA', 'MMA:Jugan, the Rising Star:jugan, the rising star').
card_rarity('jugan, the rising star'/'MMA', 'Mythic Rare').
card_artist('jugan, the rising star'/'MMA', 'Shishizaru').
card_number('jugan, the rising star'/'MMA', '150').
card_multiverse_id('jugan, the rising star'/'MMA', '370564').

card_in_set('kataki, war\'s wage', 'MMA').
card_original_type('kataki, war\'s wage'/'MMA', 'Legendary Creature — Spirit').
card_original_text('kataki, war\'s wage'/'MMA', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {1}.\"').
card_image_name('kataki, war\'s wage'/'MMA', 'kataki, war\'s wage').
card_uid('kataki, war\'s wage'/'MMA', 'MMA:Kataki, War\'s Wage:kataki, war\'s wage').
card_rarity('kataki, war\'s wage'/'MMA', 'Rare').
card_artist('kataki, war\'s wage'/'MMA', 'Matt Thompson').
card_number('kataki, war\'s wage'/'MMA', '20').
card_flavor_text('kataki, war\'s wage'/'MMA', '\"Before the war, we prayed to Kataki to sharpen our swords and harden our armor. Without his blessing our weapons are all but useless against the kami hordes.\"\n—Kenzo the Hardhearted').
card_multiverse_id('kataki, war\'s wage'/'MMA', '370414').

card_in_set('keiga, the tide star', 'MMA').
card_original_type('keiga, the tide star'/'MMA', 'Legendary Creature — Dragon Spirit').
card_original_text('keiga, the tide star'/'MMA', 'Flying\nWhen Keiga, the Tide Star dies, gain control of target creature.').
card_image_name('keiga, the tide star'/'MMA', 'keiga, the tide star').
card_uid('keiga, the tide star'/'MMA', 'MMA:Keiga, the Tide Star:keiga, the tide star').
card_rarity('keiga, the tide star'/'MMA', 'Mythic Rare').
card_artist('keiga, the tide star'/'MMA', 'Ittoku').
card_number('keiga, the tide star'/'MMA', '48').
card_multiverse_id('keiga, the tide star'/'MMA', '370542').

card_in_set('kiki-jiki, mirror breaker', 'MMA').
card_original_type('kiki-jiki, mirror breaker'/'MMA', 'Legendary Creature — Goblin Shaman').
card_original_text('kiki-jiki, mirror breaker'/'MMA', 'Haste\n{T}: Put a token that\'s a copy of target nonlegendary creature you control onto the battlefield. That token has haste. Sacrifice it at the beginning of the next end step.').
card_image_name('kiki-jiki, mirror breaker'/'MMA', 'kiki-jiki, mirror breaker').
card_uid('kiki-jiki, mirror breaker'/'MMA', 'MMA:Kiki-Jiki, Mirror Breaker:kiki-jiki, mirror breaker').
card_rarity('kiki-jiki, mirror breaker'/'MMA', 'Mythic Rare').
card_artist('kiki-jiki, mirror breaker'/'MMA', 'Steven Belledin').
card_number('kiki-jiki, mirror breaker'/'MMA', '120').
card_multiverse_id('kiki-jiki, mirror breaker'/'MMA', '370534').

card_in_set('kira, great glass-spinner', 'MMA').
card_original_type('kira, great glass-spinner'/'MMA', 'Legendary Creature — Spirit').
card_original_text('kira, great glass-spinner'/'MMA', 'Flying\nCreatures you control have \"Whenever this creature becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.\"').
card_image_name('kira, great glass-spinner'/'MMA', 'kira, great glass-spinner').
card_uid('kira, great glass-spinner'/'MMA', 'MMA:Kira, Great Glass-Spinner:kira, great glass-spinner').
card_rarity('kira, great glass-spinner'/'MMA', 'Rare').
card_artist('kira, great glass-spinner'/'MMA', 'Kev Walker').
card_number('kira, great glass-spinner'/'MMA', '49').
card_flavor_text('kira, great glass-spinner'/'MMA', 'Each spell is an intricate tapestry, and Kira is the great unraveler.').
card_multiverse_id('kira, great glass-spinner'/'MMA', '370349').

card_in_set('kitchen finks', 'MMA').
card_original_type('kitchen finks'/'MMA', 'Creature — Ouphe').
card_original_text('kitchen finks'/'MMA', 'When Kitchen Finks enters the battlefield, you gain 2 life.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('kitchen finks'/'MMA', 'kitchen finks').
card_uid('kitchen finks'/'MMA', 'MMA:Kitchen Finks:kitchen finks').
card_rarity('kitchen finks'/'MMA', 'Uncommon').
card_artist('kitchen finks'/'MMA', 'Kev Walker').
card_number('kitchen finks'/'MMA', '190').
card_flavor_text('kitchen finks'/'MMA', 'Accept one favor from an ouphe, and you\'re doomed to accept another.').
card_multiverse_id('kitchen finks'/'MMA', '370458').

card_in_set('kithkin greatheart', 'MMA').
card_original_type('kithkin greatheart'/'MMA', 'Creature — Kithkin Soldier').
card_original_text('kithkin greatheart'/'MMA', 'As long as you control a Giant, Kithkin Greatheart gets +1/+1 and has first strike.').
card_image_name('kithkin greatheart'/'MMA', 'kithkin greatheart').
card_uid('kithkin greatheart'/'MMA', 'MMA:Kithkin Greatheart:kithkin greatheart').
card_rarity('kithkin greatheart'/'MMA', 'Common').
card_artist('kithkin greatheart'/'MMA', 'Greg Staples').
card_number('kithkin greatheart'/'MMA', '21').
card_flavor_text('kithkin greatheart'/'MMA', 'Sometimes a curious giant singles out a \"little one\" to follow for a few days, never realizing the effect it will have on the little one\'s life.').
card_multiverse_id('kithkin greatheart'/'MMA', '370422').

card_in_set('knight of the reliquary', 'MMA').
card_original_type('knight of the reliquary'/'MMA', 'Creature — Human Knight').
card_original_text('knight of the reliquary'/'MMA', 'Knight of the Reliquary gets +1/+1 for each land card in your graveyard.\n{T}, Sacrifice a Forest or Plains: Search your library for a land card, put it onto the battlefield, then shuffle your library.').
card_image_name('knight of the reliquary'/'MMA', 'knight of the reliquary').
card_uid('knight of the reliquary'/'MMA', 'MMA:Knight of the Reliquary:knight of the reliquary').
card_rarity('knight of the reliquary'/'MMA', 'Rare').
card_artist('knight of the reliquary'/'MMA', 'Michael Komarck').
card_number('knight of the reliquary'/'MMA', '178').
card_flavor_text('knight of the reliquary'/'MMA', '\"Knowledge of Bant\'s landscape and ruins is a weapon that the invaders can\'t comprehend.\"\n—Elspeth').
card_multiverse_id('knight of the reliquary'/'MMA', '370379').

card_in_set('kodama\'s reach', 'MMA').
card_original_type('kodama\'s reach'/'MMA', 'Sorcery — Arcane').
card_original_text('kodama\'s reach'/'MMA', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('kodama\'s reach'/'MMA', 'kodama\'s reach').
card_uid('kodama\'s reach'/'MMA', 'MMA:Kodama\'s Reach:kodama\'s reach').
card_rarity('kodama\'s reach'/'MMA', 'Common').
card_artist('kodama\'s reach'/'MMA', 'Heather Hudson').
card_number('kodama\'s reach'/'MMA', '151').
card_flavor_text('kodama\'s reach'/'MMA', '\"The land grows only where the kami will it.\"\n—Dosan the Falling Leaf').
card_multiverse_id('kodama\'s reach'/'MMA', '370417').

card_in_set('kokusho, the evening star', 'MMA').
card_original_type('kokusho, the evening star'/'MMA', 'Legendary Creature — Dragon Spirit').
card_original_text('kokusho, the evening star'/'MMA', 'Flying\nWhen Kokusho, the Evening Star dies, each opponent loses 5 life. You gain life equal to the life lost this way.').
card_image_name('kokusho, the evening star'/'MMA', 'kokusho, the evening star').
card_uid('kokusho, the evening star'/'MMA', 'MMA:Kokusho, the Evening Star:kokusho, the evening star').
card_rarity('kokusho, the evening star'/'MMA', 'Mythic Rare').
card_artist('kokusho, the evening star'/'MMA', 'Tsutomu Kawade').
card_number('kokusho, the evening star'/'MMA', '89').
card_flavor_text('kokusho, the evening star'/'MMA', 'The fall of the evening star never heralds a gentle dawn.').
card_multiverse_id('kokusho, the evening star'/'MMA', '370502').

card_in_set('krosan grip', 'MMA').
card_original_type('krosan grip'/'MMA', 'Instant').
card_original_text('krosan grip'/'MMA', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nDestroy target artifact or enchantment.').
card_image_name('krosan grip'/'MMA', 'krosan grip').
card_uid('krosan grip'/'MMA', 'MMA:Krosan Grip:krosan grip').
card_rarity('krosan grip'/'MMA', 'Uncommon').
card_artist('krosan grip'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('krosan grip'/'MMA', '152').
card_flavor_text('krosan grip'/'MMA', '\"Give up these unnatural weapons, these scrolls. Heart and mind and fist are enough.\"\n—Zyd, Kamahlite druid').
card_multiverse_id('krosan grip'/'MMA', '370557').

card_in_set('latchkey faerie', 'MMA').
card_original_type('latchkey faerie'/'MMA', 'Creature — Faerie Rogue').
card_original_text('latchkey faerie'/'MMA', 'Flying\nProwl {2}{U} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Faerie or Rogue.)\nWhen Latchkey Faerie enters the battlefield, if its prowl cost was paid, draw a card.').
card_image_name('latchkey faerie'/'MMA', 'latchkey faerie').
card_uid('latchkey faerie'/'MMA', 'MMA:Latchkey Faerie:latchkey faerie').
card_rarity('latchkey faerie'/'MMA', 'Common').
card_artist('latchkey faerie'/'MMA', 'Warren Mahy').
card_number('latchkey faerie'/'MMA', '50').
card_multiverse_id('latchkey faerie'/'MMA', '370382').

card_in_set('lava spike', 'MMA').
card_original_type('lava spike'/'MMA', 'Sorcery — Arcane').
card_original_text('lava spike'/'MMA', 'Lava Spike deals 3 damage to target player.').
card_image_name('lava spike'/'MMA', 'lava spike').
card_uid('lava spike'/'MMA', 'MMA:Lava Spike:lava spike').
card_rarity('lava spike'/'MMA', 'Common').
card_artist('lava spike'/'MMA', 'Mark Tedin').
card_number('lava spike'/'MMA', '121').
card_flavor_text('lava spike'/'MMA', 'Some kami attacks during the war were rife with trickery, subterfuge, and subtlety, draining hope and pride from the mortal world. Other attacks were a lot more straightforward.').
card_multiverse_id('lava spike'/'MMA', '370409').

card_in_set('life from the loam', 'MMA').
card_original_type('life from the loam'/'MMA', 'Sorcery').
card_original_text('life from the loam'/'MMA', 'Return up to three target land cards from your graveyard to your hand.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('life from the loam'/'MMA', 'life from the loam').
card_uid('life from the loam'/'MMA', 'MMA:Life from the Loam:life from the loam').
card_rarity('life from the loam'/'MMA', 'Rare').
card_artist('life from the loam'/'MMA', 'Terese Nielsen').
card_number('life from the loam'/'MMA', '153').
card_multiverse_id('life from the loam'/'MMA', '370398').
card_watermark('life from the loam'/'MMA', 'Golgari').

card_in_set('lightning helix', 'MMA').
card_original_type('lightning helix'/'MMA', 'Instant').
card_original_text('lightning helix'/'MMA', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('lightning helix'/'MMA', 'lightning helix').
card_uid('lightning helix'/'MMA', 'MMA:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'MMA', 'Uncommon').
card_artist('lightning helix'/'MMA', 'Raymond Swanland').
card_number('lightning helix'/'MMA', '179').
card_flavor_text('lightning helix'/'MMA', '\"Rage is not the answer. Rage followed by fitting vengeance is the answer.\"\n—Ajani').
card_multiverse_id('lightning helix'/'MMA', '370528').

card_in_set('logic knot', 'MMA').
card_original_type('logic knot'/'MMA', 'Instant').
card_original_text('logic knot'/'MMA', 'Delve (You may exile any number of cards from your graveyard as you cast this spell. It costs {1} less to cast for each card exiled this way.)\nCounter target spell unless its controller pays {X}.').
card_image_name('logic knot'/'MMA', 'logic knot').
card_uid('logic knot'/'MMA', 'MMA:Logic Knot:logic knot').
card_rarity('logic knot'/'MMA', 'Common').
card_artist('logic knot'/'MMA', 'Glen Angus').
card_number('logic knot'/'MMA', '51').
card_multiverse_id('logic knot'/'MMA', '370529').

card_in_set('lotus bloom', 'MMA').
card_original_type('lotus bloom'/'MMA', 'Artifact').
card_original_text('lotus bloom'/'MMA', 'Suspend 3—{0} (Rather than cast this card from your hand, pay {0} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\n{T}, Sacrifice Lotus Bloom: Add three mana of any one color to your mana pool.').
card_image_name('lotus bloom'/'MMA', 'lotus bloom').
card_uid('lotus bloom'/'MMA', 'MMA:Lotus Bloom:lotus bloom').
card_rarity('lotus bloom'/'MMA', 'Rare').
card_artist('lotus bloom'/'MMA', 'Mark Zug').
card_number('lotus bloom'/'MMA', '208').
card_multiverse_id('lotus bloom'/'MMA', '370562').

card_in_set('mad auntie', 'MMA').
card_original_type('mad auntie'/'MMA', 'Creature — Goblin Shaman').
card_original_text('mad auntie'/'MMA', 'Other Goblin creatures you control get +1/+1.\n{T}: Regenerate another target Goblin.').
card_image_name('mad auntie'/'MMA', 'mad auntie').
card_uid('mad auntie'/'MMA', 'MMA:Mad Auntie:mad auntie').
card_rarity('mad auntie'/'MMA', 'Uncommon').
card_artist('mad auntie'/'MMA', 'Wayne Reynolds').
card_number('mad auntie'/'MMA', '90').
card_flavor_text('mad auntie'/'MMA', 'One part cunning, one part wise, and many, many parts demented.').
card_multiverse_id('mad auntie'/'MMA', '370496').

card_in_set('maelstrom pulse', 'MMA').
card_original_type('maelstrom pulse'/'MMA', 'Sorcery').
card_original_text('maelstrom pulse'/'MMA', 'Destroy target nonland permanent and all other permanents with the same name as that permanent.').
card_image_name('maelstrom pulse'/'MMA', 'maelstrom pulse').
card_uid('maelstrom pulse'/'MMA', 'MMA:Maelstrom Pulse:maelstrom pulse').
card_rarity('maelstrom pulse'/'MMA', 'Rare').
card_artist('maelstrom pulse'/'MMA', 'John Avon').
card_number('maelstrom pulse'/'MMA', '180').
card_flavor_text('maelstrom pulse'/'MMA', 'During the collision of the shards, entire ways of life disappeared without a trace.').
card_multiverse_id('maelstrom pulse'/'MMA', '370521').

card_in_set('manamorphose', 'MMA').
card_original_type('manamorphose'/'MMA', 'Instant').
card_original_text('manamorphose'/'MMA', 'Add two mana in any combination of colors to your mana pool.\nDraw a card.').
card_image_name('manamorphose'/'MMA', 'manamorphose').
card_uid('manamorphose'/'MMA', 'MMA:Manamorphose:manamorphose').
card_rarity('manamorphose'/'MMA', 'Uncommon').
card_artist('manamorphose'/'MMA', 'Adam Paquette').
card_number('manamorphose'/'MMA', '191').
card_flavor_text('manamorphose'/'MMA', '\"Master the chaotic forces of nature, and you shall master magic.\"\n—Yare-Tiva, warden of Gramur forest').
card_multiverse_id('manamorphose'/'MMA', '370568').

card_in_set('marsh flitter', 'MMA').
card_original_type('marsh flitter'/'MMA', 'Creature — Faerie Rogue').
card_original_text('marsh flitter'/'MMA', 'Flying\nWhen Marsh Flitter enters the battlefield, put two 1/1 black Goblin Rogue creature tokens onto the battlefield.\nSacrifice a Goblin: Marsh Flitter becomes 3/3 until end of turn.').
card_image_name('marsh flitter'/'MMA', 'marsh flitter').
card_uid('marsh flitter'/'MMA', 'MMA:Marsh Flitter:marsh flitter').
card_rarity('marsh flitter'/'MMA', 'Uncommon').
card_artist('marsh flitter'/'MMA', 'Wayne Reynolds').
card_number('marsh flitter'/'MMA', '91').
card_multiverse_id('marsh flitter'/'MMA', '370374').

card_in_set('masked admirers', 'MMA').
card_original_type('masked admirers'/'MMA', 'Creature — Elf Shaman').
card_original_text('masked admirers'/'MMA', 'When Masked Admirers enters the battlefield, draw a card.\nWhenever you cast a creature spell, you may pay {G}{G}. If you do, return Masked Admirers from your graveyard to your hand.').
card_image_name('masked admirers'/'MMA', 'masked admirers').
card_uid('masked admirers'/'MMA', 'MMA:Masked Admirers:masked admirers').
card_rarity('masked admirers'/'MMA', 'Uncommon').
card_artist('masked admirers'/'MMA', 'Eric Fortune').
card_number('masked admirers'/'MMA', '154').
card_flavor_text('masked admirers'/'MMA', '\"Beauty determines value, and we determine beauty.\"').
card_multiverse_id('masked admirers'/'MMA', '370416').

card_in_set('meadowboon', 'MMA').
card_original_type('meadowboon'/'MMA', 'Creature — Elemental').
card_original_text('meadowboon'/'MMA', 'When Meadowboon leaves the battlefield, put a +1/+1 counter on each creature target player controls.\nEvoke {3}{W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('meadowboon'/'MMA', 'meadowboon').
card_uid('meadowboon'/'MMA', 'MMA:Meadowboon:meadowboon').
card_rarity('meadowboon'/'MMA', 'Uncommon').
card_artist('meadowboon'/'MMA', 'Steven Belledin').
card_number('meadowboon'/'MMA', '22').
card_multiverse_id('meadowboon'/'MMA', '370513').

card_in_set('meloku the clouded mirror', 'MMA').
card_original_type('meloku the clouded mirror'/'MMA', 'Legendary Creature — Moonfolk Wizard').
card_original_text('meloku the clouded mirror'/'MMA', 'Flying\n{1}, Return a land you control to its owner\'s hand: Put a 1/1 blue Illusion creature token with flying onto the battlefield.').
card_image_name('meloku the clouded mirror'/'MMA', 'meloku the clouded mirror').
card_uid('meloku the clouded mirror'/'MMA', 'MMA:Meloku the Clouded Mirror:meloku the clouded mirror').
card_rarity('meloku the clouded mirror'/'MMA', 'Rare').
card_artist('meloku the clouded mirror'/'MMA', 'Scott M. Fischer').
card_number('meloku the clouded mirror'/'MMA', '52').
card_flavor_text('meloku the clouded mirror'/'MMA', 'He loved his cities in the clouds. When he traveled to the lands below, he brought many reminders of his home.').
card_multiverse_id('meloku the clouded mirror'/'MMA', '370385').

card_in_set('mind funeral', 'MMA').
card_original_type('mind funeral'/'MMA', 'Sorcery').
card_original_text('mind funeral'/'MMA', 'Target opponent reveals cards from the top of his or her library until four land cards are revealed. That player puts all cards revealed this way into his or her graveyard.').
card_image_name('mind funeral'/'MMA', 'mind funeral').
card_uid('mind funeral'/'MMA', 'MMA:Mind Funeral:mind funeral').
card_rarity('mind funeral'/'MMA', 'Uncommon').
card_artist('mind funeral'/'MMA', 'rk post').
card_number('mind funeral'/'MMA', '181').
card_flavor_text('mind funeral'/'MMA', '\"The soul is only as eternal as I say it is.\"\n—Sharuum the Hegemon').
card_multiverse_id('mind funeral'/'MMA', '370509').

card_in_set('mogg war marshal', 'MMA').
card_original_type('mogg war marshal'/'MMA', 'Creature — Goblin Warrior').
card_original_text('mogg war marshal'/'MMA', 'Echo {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Mogg War Marshal enters the battlefield or dies, put a 1/1 red Goblin creature token onto the battlefield.').
card_image_name('mogg war marshal'/'MMA', 'mogg war marshal').
card_uid('mogg war marshal'/'MMA', 'MMA:Mogg War Marshal:mogg war marshal').
card_rarity('mogg war marshal'/'MMA', 'Common').
card_artist('mogg war marshal'/'MMA', 'Wayne England').
card_number('mogg war marshal'/'MMA', '122').
card_multiverse_id('mogg war marshal'/'MMA', '370547').

card_in_set('moldervine cloak', 'MMA').
card_original_type('moldervine cloak'/'MMA', 'Enchantment — Aura').
card_original_text('moldervine cloak'/'MMA', 'Enchant creature\nEnchanted creature gets +3/+3.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('moldervine cloak'/'MMA', 'moldervine cloak').
card_uid('moldervine cloak'/'MMA', 'MMA:Moldervine Cloak:moldervine cloak').
card_rarity('moldervine cloak'/'MMA', 'Common').
card_artist('moldervine cloak'/'MMA', 'Wayne England').
card_number('moldervine cloak'/'MMA', '155').
card_multiverse_id('moldervine cloak'/'MMA', '370444').
card_watermark('moldervine cloak'/'MMA', 'Golgari').

card_in_set('molten disaster', 'MMA').
card_original_type('molten disaster'/'MMA', 'Sorcery').
card_original_text('molten disaster'/'MMA', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nIf Molten Disaster was kicked, it has split second. (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nMolten Disaster deals X damage to each creature without flying and each player.').
card_image_name('molten disaster'/'MMA', 'molten disaster').
card_uid('molten disaster'/'MMA', 'MMA:Molten Disaster:molten disaster').
card_rarity('molten disaster'/'MMA', 'Rare').
card_artist('molten disaster'/'MMA', 'Ittoku').
card_number('molten disaster'/'MMA', '123').
card_multiverse_id('molten disaster'/'MMA', '370437').

card_in_set('mothdust changeling', 'MMA').
card_original_type('mothdust changeling'/'MMA', 'Creature — Shapeshifter').
card_original_text('mothdust changeling'/'MMA', 'Changeling (This card is every creature type at all times.)\nTap an untapped creature you control: Mothdust Changeling gains flying until end of turn.').
card_image_name('mothdust changeling'/'MMA', 'mothdust changeling').
card_uid('mothdust changeling'/'MMA', 'MMA:Mothdust Changeling:mothdust changeling').
card_rarity('mothdust changeling'/'MMA', 'Common').
card_artist('mothdust changeling'/'MMA', 'Shelly Wan').
card_number('mothdust changeling'/'MMA', '53').
card_flavor_text('mothdust changeling'/'MMA', '\"Ever seen a changeling fly into a lantern?\"\n—Calydd, kithkin farmer').
card_multiverse_id('mothdust changeling'/'MMA', '370508').

card_in_set('mulldrifter', 'MMA').
card_original_type('mulldrifter'/'MMA', 'Creature — Elemental').
card_original_text('mulldrifter'/'MMA', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('mulldrifter'/'MMA', 'mulldrifter').
card_uid('mulldrifter'/'MMA', 'MMA:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'MMA', 'Uncommon').
card_artist('mulldrifter'/'MMA', 'Eric Fortune').
card_number('mulldrifter'/'MMA', '54').
card_multiverse_id('mulldrifter'/'MMA', '370535').

card_in_set('murderous redcap', 'MMA').
card_original_type('murderous redcap'/'MMA', 'Creature — Goblin Assassin').
card_original_text('murderous redcap'/'MMA', 'When Murderous Redcap enters the battlefield, it deals damage equal to its power to target creature or player.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('murderous redcap'/'MMA', 'murderous redcap').
card_uid('murderous redcap'/'MMA', 'MMA:Murderous Redcap:murderous redcap').
card_rarity('murderous redcap'/'MMA', 'Uncommon').
card_artist('murderous redcap'/'MMA', 'Dave Allsop').
card_number('murderous redcap'/'MMA', '192').
card_multiverse_id('murderous redcap'/'MMA', '370518').

card_in_set('myr enforcer', 'MMA').
card_original_type('myr enforcer'/'MMA', 'Artifact Creature — Myr').
card_original_text('myr enforcer'/'MMA', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('myr enforcer'/'MMA', 'myr enforcer').
card_uid('myr enforcer'/'MMA', 'MMA:Myr Enforcer:myr enforcer').
card_rarity('myr enforcer'/'MMA', 'Common').
card_artist('myr enforcer'/'MMA', 'Jim Murray').
card_number('myr enforcer'/'MMA', '209').
card_flavor_text('myr enforcer'/'MMA', 'Most myr monitor other species. Some myr monitor other myr.').
card_multiverse_id('myr enforcer'/'MMA', '370436').

card_in_set('myr retriever', 'MMA').
card_original_type('myr retriever'/'MMA', 'Artifact Creature — Myr').
card_original_text('myr retriever'/'MMA', 'When Myr Retriever dies, return another target artifact card from your graveyard to your hand.').
card_image_name('myr retriever'/'MMA', 'myr retriever').
card_uid('myr retriever'/'MMA', 'MMA:Myr Retriever:myr retriever').
card_rarity('myr retriever'/'MMA', 'Uncommon').
card_artist('myr retriever'/'MMA', 'Trevor Hairsine').
card_number('myr retriever'/'MMA', '210').
card_flavor_text('myr retriever'/'MMA', 'Mephidross gives up treasure easily . . . as long as you take its place.').
card_multiverse_id('myr retriever'/'MMA', '370520').

card_in_set('nantuko shaman', 'MMA').
card_original_type('nantuko shaman'/'MMA', 'Creature — Insect Shaman').
card_original_text('nantuko shaman'/'MMA', 'When Nantuko Shaman enters the battlefield, if you control no tapped lands, draw a card.\nSuspend 1—{2}{G}{G} (Rather than cast this card from your hand, you may pay {2}{G}{G} and exile it with a time counter on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('nantuko shaman'/'MMA', 'nantuko shaman').
card_uid('nantuko shaman'/'MMA', 'MMA:Nantuko Shaman:nantuko shaman').
card_rarity('nantuko shaman'/'MMA', 'Common').
card_artist('nantuko shaman'/'MMA', 'Daren Bader').
card_number('nantuko shaman'/'MMA', '156').
card_multiverse_id('nantuko shaman'/'MMA', '370484').

card_in_set('narcomoeba', 'MMA').
card_original_type('narcomoeba'/'MMA', 'Creature — Illusion').
card_original_text('narcomoeba'/'MMA', 'Flying\nWhen Narcomoeba is put into your graveyard from your library, you may put it onto the battlefield.').
card_image_name('narcomoeba'/'MMA', 'narcomoeba').
card_uid('narcomoeba'/'MMA', 'MMA:Narcomoeba:narcomoeba').
card_rarity('narcomoeba'/'MMA', 'Uncommon').
card_artist('narcomoeba'/'MMA', 'Matt Stewart').
card_number('narcomoeba'/'MMA', '55').
card_flavor_text('narcomoeba'/'MMA', 'It was created by the Iquati as a living memory—one that objects to being forgotten.').
card_multiverse_id('narcomoeba'/'MMA', '370359').

card_in_set('oona, queen of the fae', 'MMA').
card_original_type('oona, queen of the fae'/'MMA', 'Legendary Creature — Faerie Wizard').
card_original_text('oona, queen of the fae'/'MMA', 'Flying\n{X}{U/B}: Choose a color. Target opponent exiles the top X cards of his or her library. For each card of the chosen color exiled this way, put a 1/1 blue and black Faerie Rogue creature token with flying onto the battlefield.').
card_image_name('oona, queen of the fae'/'MMA', 'oona, queen of the fae').
card_uid('oona, queen of the fae'/'MMA', 'MMA:Oona, Queen of the Fae:oona, queen of the fae').
card_rarity('oona, queen of the fae'/'MMA', 'Rare').
card_artist('oona, queen of the fae'/'MMA', 'Adam Rex').
card_number('oona, queen of the fae'/'MMA', '193').
card_multiverse_id('oona, queen of the fae'/'MMA', '370429').

card_in_set('otherworldly journey', 'MMA').
card_original_type('otherworldly journey'/'MMA', 'Instant — Arcane').
card_original_text('otherworldly journey'/'MMA', 'Exile target creature. At the beginning of the next end step, return that card to the battlefield under its owner\'s control with a +1/+1 counter on it.').
card_image_name('otherworldly journey'/'MMA', 'otherworldly journey').
card_uid('otherworldly journey'/'MMA', 'MMA:Otherworldly Journey:otherworldly journey').
card_rarity('otherworldly journey'/'MMA', 'Common').
card_artist('otherworldly journey'/'MMA', 'Vance Kovacs').
card_number('otherworldly journey'/'MMA', '23').
card_flavor_text('otherworldly journey'/'MMA', '\"The landscape shimmered and I felt a chill breeze. When my vision cleared, I found myself alone among the corpses of my fallen friends.\"\n—Journal found in Numai').
card_multiverse_id('otherworldly journey'/'MMA', '370460').

card_in_set('pact of negation', 'MMA').
card_original_type('pact of negation'/'MMA', 'Instant').
card_original_text('pact of negation'/'MMA', 'Counter target spell.\nAt the beginning of your next upkeep, pay {3}{U}{U}. If you don\'t, you lose the game.').
card_image_name('pact of negation'/'MMA', 'pact of negation').
card_uid('pact of negation'/'MMA', 'MMA:Pact of Negation:pact of negation').
card_rarity('pact of negation'/'MMA', 'Rare').
card_artist('pact of negation'/'MMA', 'Jason Chan').
card_number('pact of negation'/'MMA', '56').
card_flavor_text('pact of negation'/'MMA', 'Those who expect betrayal at every turn are seldom disappointed.').
card_multiverse_id('pact of negation'/'MMA', '370354').

card_in_set('pallid mycoderm', 'MMA').
card_original_type('pallid mycoderm'/'MMA', 'Creature — Fungus').
card_original_text('pallid mycoderm'/'MMA', 'At the beginning of your upkeep, put a spore counter on Pallid Mycoderm.\nRemove three spore counters from Pallid Mycoderm: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Each creature you control that\'s a Fungus or a Saproling gets +1/+1 until end of turn.').
card_image_name('pallid mycoderm'/'MMA', 'pallid mycoderm').
card_uid('pallid mycoderm'/'MMA', 'MMA:Pallid Mycoderm:pallid mycoderm').
card_rarity('pallid mycoderm'/'MMA', 'Common').
card_artist('pallid mycoderm'/'MMA', 'Jim Nelson').
card_number('pallid mycoderm'/'MMA', '24').
card_multiverse_id('pallid mycoderm'/'MMA', '370348').

card_in_set('paradise mantle', 'MMA').
card_original_type('paradise mantle'/'MMA', 'Artifact — Equipment').
card_original_text('paradise mantle'/'MMA', 'Equipped creature has \"{T}: Add one mana of any color to your mana pool.\"\nEquip {1}').
card_image_name('paradise mantle'/'MMA', 'paradise mantle').
card_uid('paradise mantle'/'MMA', 'MMA:Paradise Mantle:paradise mantle').
card_rarity('paradise mantle'/'MMA', 'Uncommon').
card_artist('paradise mantle'/'MMA', 'Greg Hildebrandt').
card_number('paradise mantle'/'MMA', '211').
card_flavor_text('paradise mantle'/'MMA', '\"It contains the wisdom of generations of our people. We permit only a chosen few to don the raiment.\"\n—Imran, Viridian elder').
card_multiverse_id('paradise mantle'/'MMA', '370448').

card_in_set('pardic dragon', 'MMA').
card_original_type('pardic dragon'/'MMA', 'Creature — Dragon').
card_original_text('pardic dragon'/'MMA', 'Flying\n{R}: Pardic Dragon gets +1/+0 until end of turn.\nSuspend 2—{R}{R}\nWhenever an opponent casts a spell, if Pardic Dragon is suspended, that player may put a time counter on Pardic Dragon.').
card_image_name('pardic dragon'/'MMA', 'pardic dragon').
card_uid('pardic dragon'/'MMA', 'MMA:Pardic Dragon:pardic dragon').
card_rarity('pardic dragon'/'MMA', 'Uncommon').
card_artist('pardic dragon'/'MMA', 'Zoltan Boros & Gabor Szikszai').
card_number('pardic dragon'/'MMA', '124').
card_multiverse_id('pardic dragon'/'MMA', '370464').

card_in_set('path to exile', 'MMA').
card_original_type('path to exile'/'MMA', 'Instant').
card_original_text('path to exile'/'MMA', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_image_name('path to exile'/'MMA', 'path to exile').
card_uid('path to exile'/'MMA', 'MMA:Path to Exile:path to exile').
card_rarity('path to exile'/'MMA', 'Uncommon').
card_artist('path to exile'/'MMA', 'Todd Lockwood').
card_number('path to exile'/'MMA', '25').
card_multiverse_id('path to exile'/'MMA', '370408').

card_in_set('peer through depths', 'MMA').
card_original_type('peer through depths'/'MMA', 'Instant — Arcane').
card_original_text('peer through depths'/'MMA', 'Look at the top five cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('peer through depths'/'MMA', 'peer through depths').
card_uid('peer through depths'/'MMA', 'MMA:Peer Through Depths:peer through depths').
card_rarity('peer through depths'/'MMA', 'Common').
card_artist('peer through depths'/'MMA', 'Anthony S. Waters').
card_number('peer through depths'/'MMA', '57').
card_multiverse_id('peer through depths'/'MMA', '370540').

card_in_set('penumbra spider', 'MMA').
card_original_type('penumbra spider'/'MMA', 'Creature — Spider').
card_original_text('penumbra spider'/'MMA', 'Reach\nWhen Penumbra Spider dies, put a 2/4 black Spider creature token with reach onto the battlefield.').
card_image_name('penumbra spider'/'MMA', 'penumbra spider').
card_uid('penumbra spider'/'MMA', 'MMA:Penumbra Spider:penumbra spider').
card_rarity('penumbra spider'/'MMA', 'Common').
card_artist('penumbra spider'/'MMA', 'Jeff Easley').
card_number('penumbra spider'/'MMA', '157').
card_flavor_text('penumbra spider'/'MMA', 'When it snared a passing cockatrice, its own soul darkly doubled.').
card_multiverse_id('penumbra spider'/'MMA', '370516').

card_in_set('peppersmoke', 'MMA').
card_original_type('peppersmoke'/'MMA', 'Tribal Instant — Faerie').
card_original_text('peppersmoke'/'MMA', 'Target creature gets -1/-1 until end of turn. If you control a Faerie, draw a card.').
card_image_name('peppersmoke'/'MMA', 'peppersmoke').
card_uid('peppersmoke'/'MMA', 'MMA:Peppersmoke:peppersmoke').
card_rarity('peppersmoke'/'MMA', 'Common').
card_artist('peppersmoke'/'MMA', 'Rebecca Guay').
card_number('peppersmoke'/'MMA', '92').
card_flavor_text('peppersmoke'/'MMA', 'Like being trapped in a perpetual sneeze, faerie-dust poisoning is both exhilarating and agonizing.').
card_multiverse_id('peppersmoke'/'MMA', '370481').

card_in_set('perilous research', 'MMA').
card_original_type('perilous research'/'MMA', 'Instant').
card_original_text('perilous research'/'MMA', 'Draw two cards, then sacrifice a permanent.').
card_image_name('perilous research'/'MMA', 'perilous research').
card_uid('perilous research'/'MMA', 'MMA:Perilous Research:perilous research').
card_rarity('perilous research'/'MMA', 'Common').
card_artist('perilous research'/'MMA', 'Donato Giancola').
card_number('perilous research'/'MMA', '58').
card_flavor_text('perilous research'/'MMA', 'When the School of the Unseen fell, so many magical treasures lay abandoned that no amount of death could deter the stream of thieves and desperate scholars.').
card_multiverse_id('perilous research'/'MMA', '370487').

card_in_set('pestermite', 'MMA').
card_original_type('pestermite'/'MMA', 'Creature — Faerie Rogue').
card_original_text('pestermite'/'MMA', 'Flash\nFlying\nWhen Pestermite enters the battlefield, you may tap or untap target permanent.').
card_image_name('pestermite'/'MMA', 'pestermite').
card_uid('pestermite'/'MMA', 'MMA:Pestermite:pestermite').
card_rarity('pestermite'/'MMA', 'Common').
card_artist('pestermite'/'MMA', 'Christopher Moeller').
card_number('pestermite'/'MMA', '59').
card_flavor_text('pestermite'/'MMA', 'The fae know when they\'re not wanted. That\'s precisely why they show up.').
card_multiverse_id('pestermite'/'MMA', '370440').

card_in_set('petals of insight', 'MMA').
card_original_type('petals of insight'/'MMA', 'Sorcery — Arcane').
card_original_text('petals of insight'/'MMA', 'Look at the top three cards of your library. You may put those cards on the bottom of your library in any order. If you do, return Petals of Insight to its owner\'s hand. Otherwise, draw three cards.').
card_image_name('petals of insight'/'MMA', 'petals of insight').
card_uid('petals of insight'/'MMA', 'MMA:Petals of Insight:petals of insight').
card_rarity('petals of insight'/'MMA', 'Common').
card_artist('petals of insight'/'MMA', 'Anthony S. Waters').
card_number('petals of insight'/'MMA', '60').
card_multiverse_id('petals of insight'/'MMA', '370395').

card_in_set('phthisis', 'MMA').
card_original_type('phthisis'/'MMA', 'Sorcery').
card_original_text('phthisis'/'MMA', 'Destroy target creature. Its controller loses life equal to its power plus its toughness.\nSuspend 5—{1}{B} (Rather than cast this card from your hand, you may pay {1}{B} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_image_name('phthisis'/'MMA', 'phthisis').
card_uid('phthisis'/'MMA', 'MMA:Phthisis:phthisis').
card_rarity('phthisis'/'MMA', 'Uncommon').
card_artist('phthisis'/'MMA', 'Carl Critchlow').
card_number('phthisis'/'MMA', '93').
card_multiverse_id('phthisis'/'MMA', '370383').

card_in_set('plumeveil', 'MMA').
card_original_type('plumeveil'/'MMA', 'Creature — Elemental').
card_original_text('plumeveil'/'MMA', 'Flash\nDefender, flying').
card_image_name('plumeveil'/'MMA', 'plumeveil').
card_uid('plumeveil'/'MMA', 'MMA:Plumeveil:plumeveil').
card_rarity('plumeveil'/'MMA', 'Uncommon').
card_artist('plumeveil'/'MMA', 'Nils Hamm').
card_number('plumeveil'/'MMA', '194').
card_flavor_text('plumeveil'/'MMA', '\"It was vast, a great sheet of soaring wings, and equally silent. It caught us unawares and blocked our view of the kithkin stronghold.\"\n—Grensch, merrow cutthroat').
card_multiverse_id('plumeveil'/'MMA', '370423').

card_in_set('progenitus', 'MMA').
card_original_type('progenitus'/'MMA', 'Legendary Creature — Hydra Avatar').
card_original_text('progenitus'/'MMA', 'Protection from everything\nIf Progenitus would be put into a graveyard from anywhere, reveal Progenitus and shuffle it into its owner\'s library instead.').
card_image_name('progenitus'/'MMA', 'progenitus').
card_uid('progenitus'/'MMA', 'MMA:Progenitus:progenitus').
card_rarity('progenitus'/'MMA', 'Mythic Rare').
card_artist('progenitus'/'MMA', 'Jaime Jones').
card_number('progenitus'/'MMA', '182').
card_flavor_text('progenitus'/'MMA', 'The Soul of the World has returned.').
card_multiverse_id('progenitus'/'MMA', '370405').

card_in_set('pyrite spellbomb', 'MMA').
card_original_type('pyrite spellbomb'/'MMA', 'Artifact').
card_original_text('pyrite spellbomb'/'MMA', '{R}, Sacrifice Pyrite Spellbomb: Pyrite Spellbomb deals 2 damage to target creature or player.\n{1}, Sacrifice Pyrite Spellbomb: Draw a card.').
card_image_name('pyrite spellbomb'/'MMA', 'pyrite spellbomb').
card_uid('pyrite spellbomb'/'MMA', 'MMA:Pyrite Spellbomb:pyrite spellbomb').
card_rarity('pyrite spellbomb'/'MMA', 'Common').
card_artist('pyrite spellbomb'/'MMA', 'Jim Nelson').
card_number('pyrite spellbomb'/'MMA', '212').
card_flavor_text('pyrite spellbomb'/'MMA', '\"Melt that which was never frozen.\"\n—Spellbomb inscription').
card_multiverse_id('pyrite spellbomb'/'MMA', '370512').

card_in_set('pyromancer\'s swath', 'MMA').
card_original_type('pyromancer\'s swath'/'MMA', 'Enchantment').
card_original_text('pyromancer\'s swath'/'MMA', 'If an instant or sorcery source you control would deal damage to a creature or player, it deals that much damage plus 2 to that creature or player instead.\nAt the beginning of each end step, discard your hand.').
card_image_name('pyromancer\'s swath'/'MMA', 'pyromancer\'s swath').
card_uid('pyromancer\'s swath'/'MMA', 'MMA:Pyromancer\'s Swath:pyromancer\'s swath').
card_rarity('pyromancer\'s swath'/'MMA', 'Rare').
card_artist('pyromancer\'s swath'/'MMA', 'Hideaki Takamura').
card_number('pyromancer\'s swath'/'MMA', '125').
card_multiverse_id('pyromancer\'s swath'/'MMA', '370397').

card_in_set('rathi trapper', 'MMA').
card_original_type('rathi trapper'/'MMA', 'Creature — Human Rebel Rogue').
card_original_text('rathi trapper'/'MMA', '{B}, {T}: Tap target creature.').
card_image_name('rathi trapper'/'MMA', 'rathi trapper').
card_uid('rathi trapper'/'MMA', 'MMA:Rathi Trapper:rathi trapper').
card_rarity('rathi trapper'/'MMA', 'Common').
card_artist('rathi trapper'/'MMA', 'Pete Venters').
card_number('rathi trapper'/'MMA', '94').
card_flavor_text('rathi trapper'/'MMA', 'Tangling vines, fetid murk, paralyzing poisons, and crawling dead. The swamp is nature\'s trap waiting to be exploited by unnatural minds.').
card_multiverse_id('rathi trapper'/'MMA', '370570').

card_in_set('raven\'s crime', 'MMA').
card_original_type('raven\'s crime'/'MMA', 'Sorcery').
card_original_text('raven\'s crime'/'MMA', 'Target player discards a card.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_image_name('raven\'s crime'/'MMA', 'raven\'s crime').
card_uid('raven\'s crime'/'MMA', 'MMA:Raven\'s Crime:raven\'s crime').
card_rarity('raven\'s crime'/'MMA', 'Common').
card_artist('raven\'s crime'/'MMA', 'Warren Mahy').
card_number('raven\'s crime'/'MMA', '95').
card_flavor_text('raven\'s crime'/'MMA', 'It plucks away memories like choice bits of carrion.').
card_multiverse_id('raven\'s crime'/'MMA', '370478').

card_in_set('reach of branches', 'MMA').
card_original_type('reach of branches'/'MMA', 'Tribal Instant — Treefolk').
card_original_text('reach of branches'/'MMA', 'Put a 2/5 green Treefolk Shaman creature token onto the battlefield.\nWhenever a Forest enters the battlefield under your control, you may return Reach of Branches from your graveyard to your hand.').
card_image_name('reach of branches'/'MMA', 'reach of branches').
card_uid('reach of branches'/'MMA', 'MMA:Reach of Branches:reach of branches').
card_rarity('reach of branches'/'MMA', 'Uncommon').
card_artist('reach of branches'/'MMA', 'Zack Stella').
card_number('reach of branches'/'MMA', '158').
card_flavor_text('reach of branches'/'MMA', 'Growth has no limits.').
card_multiverse_id('reach of branches'/'MMA', '370412').

card_in_set('reach through mists', 'MMA').
card_original_type('reach through mists'/'MMA', 'Instant — Arcane').
card_original_text('reach through mists'/'MMA', 'Draw a card.').
card_image_name('reach through mists'/'MMA', 'reach through mists').
card_uid('reach through mists'/'MMA', 'MMA:Reach Through Mists:reach through mists').
card_rarity('reach through mists'/'MMA', 'Common').
card_artist('reach through mists'/'MMA', 'Anthony S. Waters').
card_number('reach through mists'/'MMA', '61').
card_flavor_text('reach through mists'/'MMA', '\"Know one part of the name, obsession begins. Know two parts, paranoia sets in. Know three parts, madness descends. Know all, and only the kami know what will become of you.\"\n—Lady Azami').
card_multiverse_id('reach through mists'/'MMA', '370506').

card_in_set('relic of progenitus', 'MMA').
card_original_type('relic of progenitus'/'MMA', 'Artifact').
card_original_text('relic of progenitus'/'MMA', '{T}: Target player exiles a card from his or her graveyard.\n{1}, Exile Relic of Progenitus: Exile all cards from all graveyards. Draw a card.').
card_image_name('relic of progenitus'/'MMA', 'relic of progenitus').
card_uid('relic of progenitus'/'MMA', 'MMA:Relic of Progenitus:relic of progenitus').
card_rarity('relic of progenitus'/'MMA', 'Uncommon').
card_artist('relic of progenitus'/'MMA', 'Jean-Sébastien Rossbach').
card_number('relic of progenitus'/'MMA', '213').
card_flavor_text('relic of progenitus'/'MMA', 'Elves believe the hydra-god Progenitus sleeps beneath Naya, feeding on forgotten magics.').
card_multiverse_id('relic of progenitus'/'MMA', '370556').

card_in_set('reveillark', 'MMA').
card_original_type('reveillark'/'MMA', 'Creature — Elemental').
card_original_text('reveillark'/'MMA', 'Flying\nWhen Reveillark leaves the battlefield, return up to two target creature cards with power 2 or less from your graveyard to the battlefield.\nEvoke {5}{W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('reveillark'/'MMA', 'reveillark').
card_uid('reveillark'/'MMA', 'MMA:Reveillark:reveillark').
card_rarity('reveillark'/'MMA', 'Rare').
card_artist('reveillark'/'MMA', 'Jim Murray').
card_number('reveillark'/'MMA', '26').
card_multiverse_id('reveillark'/'MMA', '370493').

card_in_set('rift bolt', 'MMA').
card_original_type('rift bolt'/'MMA', 'Sorcery').
card_original_text('rift bolt'/'MMA', 'Rift Bolt deals 3 damage to target creature or player.\nSuspend 1—{R} (Rather than cast this card from your hand, you may pay {R} and exile it with a time counter on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_image_name('rift bolt'/'MMA', 'rift bolt').
card_uid('rift bolt'/'MMA', 'MMA:Rift Bolt:rift bolt').
card_rarity('rift bolt'/'MMA', 'Common').
card_artist('rift bolt'/'MMA', 'Daniel Ljunggren').
card_number('rift bolt'/'MMA', '126').
card_multiverse_id('rift bolt'/'MMA', '370469').

card_in_set('rift elemental', 'MMA').
card_original_type('rift elemental'/'MMA', 'Creature — Elemental').
card_original_text('rift elemental'/'MMA', '{1}{R}, Remove a time counter from a permanent you control or suspended card you own: Rift Elemental gets +2/+0 until end of turn.').
card_image_name('rift elemental'/'MMA', 'rift elemental').
card_uid('rift elemental'/'MMA', 'MMA:Rift Elemental:rift elemental').
card_rarity('rift elemental'/'MMA', 'Common').
card_artist('rift elemental'/'MMA', 'Ron Spears').
card_number('rift elemental'/'MMA', '127').
card_flavor_text('rift elemental'/'MMA', '\"It\'s difficult to heal a rift after it begins wandering about on its own.\"\n—Teferi').
card_multiverse_id('rift elemental'/'MMA', '370468').

card_in_set('riftsweeper', 'MMA').
card_original_type('riftsweeper'/'MMA', 'Creature — Elf Shaman').
card_original_text('riftsweeper'/'MMA', 'When Riftsweeper enters the battlefield, choose target face-up exiled card. Its owner shuffles it into his or her library.').
card_image_name('riftsweeper'/'MMA', 'riftsweeper').
card_uid('riftsweeper'/'MMA', 'MMA:Riftsweeper:riftsweeper').
card_rarity('riftsweeper'/'MMA', 'Uncommon').
card_artist('riftsweeper'/'MMA', 'Brian Despain').
card_number('riftsweeper'/'MMA', '159').
card_flavor_text('riftsweeper'/'MMA', '\"Beings of the rifts are not natural. No longer will these abominations tread upon the body of Gaea.\"').
card_multiverse_id('riftsweeper'/'MMA', '370365').

card_in_set('riftwing cloudskate', 'MMA').
card_original_type('riftwing cloudskate'/'MMA', 'Creature — Illusion').
card_original_text('riftwing cloudskate'/'MMA', 'Flying\nWhen Riftwing Cloudskate enters the battlefield, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('riftwing cloudskate'/'MMA', 'riftwing cloudskate').
card_uid('riftwing cloudskate'/'MMA', 'MMA:Riftwing Cloudskate:riftwing cloudskate').
card_rarity('riftwing cloudskate'/'MMA', 'Uncommon').
card_artist('riftwing cloudskate'/'MMA', 'Carl Critchlow').
card_number('riftwing cloudskate'/'MMA', '62').
card_multiverse_id('riftwing cloudskate'/'MMA', '370350').

card_in_set('rude awakening', 'MMA').
card_original_type('rude awakening'/'MMA', 'Sorcery').
card_original_text('rude awakening'/'MMA', 'Choose one — Untap all lands you control; or until end of turn, lands you control become 2/2 creatures that are still lands.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_image_name('rude awakening'/'MMA', 'rude awakening').
card_uid('rude awakening'/'MMA', 'MMA:Rude Awakening:rude awakening').
card_rarity('rude awakening'/'MMA', 'Rare').
card_artist('rude awakening'/'MMA', 'Ittoku').
card_number('rude awakening'/'MMA', '160').
card_multiverse_id('rude awakening'/'MMA', '370470').

card_in_set('runed stalactite', 'MMA').
card_original_type('runed stalactite'/'MMA', 'Artifact — Equipment').
card_original_text('runed stalactite'/'MMA', 'Equipped creature gets +1/+1 and is every creature type.\nEquip {2}').
card_image_name('runed stalactite'/'MMA', 'runed stalactite').
card_uid('runed stalactite'/'MMA', 'MMA:Runed Stalactite:runed stalactite').
card_rarity('runed stalactite'/'MMA', 'Common').
card_artist('runed stalactite'/'MMA', 'Jim Pavelec').
card_number('runed stalactite'/'MMA', '214').
card_flavor_text('runed stalactite'/'MMA', 'When a changeling adopts a form no other changeling has taken, a rune appears in the caverns of Velis Vel to mark the event.').
card_multiverse_id('runed stalactite'/'MMA', '370445').

card_in_set('ryusei, the falling star', 'MMA').
card_original_type('ryusei, the falling star'/'MMA', 'Legendary Creature — Dragon Spirit').
card_original_text('ryusei, the falling star'/'MMA', 'Flying\nWhen Ryusei, the Falling Star dies, it deals 5 damage to each creature without flying.').
card_image_name('ryusei, the falling star'/'MMA', 'ryusei, the falling star').
card_uid('ryusei, the falling star'/'MMA', 'MMA:Ryusei, the Falling Star:ryusei, the falling star').
card_rarity('ryusei, the falling star'/'MMA', 'Mythic Rare').
card_artist('ryusei, the falling star'/'MMA', 'Nottsuo').
card_number('ryusei, the falling star'/'MMA', '128').
card_multiverse_id('ryusei, the falling star'/'MMA', '370559').

card_in_set('saltfield recluse', 'MMA').
card_original_type('saltfield recluse'/'MMA', 'Creature — Human Rebel Cleric').
card_original_text('saltfield recluse'/'MMA', '{T}: Target creature gets -2/-0 until end of turn.').
card_image_name('saltfield recluse'/'MMA', 'saltfield recluse').
card_uid('saltfield recluse'/'MMA', 'MMA:Saltfield Recluse:saltfield recluse').
card_rarity('saltfield recluse'/'MMA', 'Common').
card_artist('saltfield recluse'/'MMA', 'Brian Despain').
card_number('saltfield recluse'/'MMA', '27').
card_flavor_text('saltfield recluse'/'MMA', 'He remembers a past of light and healing. But he lives the bitter present—parching salt, scouring wind, and the withering heat of the desert.').
card_multiverse_id('saltfield recluse'/'MMA', '370536').

card_in_set('sanctum gargoyle', 'MMA').
card_original_type('sanctum gargoyle'/'MMA', 'Artifact Creature — Gargoyle').
card_original_text('sanctum gargoyle'/'MMA', 'Flying\nWhen Sanctum Gargoyle enters the battlefield, you may return target artifact card from your graveyard to your hand.').
card_image_name('sanctum gargoyle'/'MMA', 'sanctum gargoyle').
card_uid('sanctum gargoyle'/'MMA', 'MMA:Sanctum Gargoyle:sanctum gargoyle').
card_rarity('sanctum gargoyle'/'MMA', 'Common').
card_artist('sanctum gargoyle'/'MMA', 'Shelly Wan').
card_number('sanctum gargoyle'/'MMA', '28').
card_flavor_text('sanctum gargoyle'/'MMA', 'As their supplies of etherium dwindled, mechanists sent gargoyles farther and farther afield in search of salvage.').
card_multiverse_id('sanctum gargoyle'/'MMA', '370485').

card_in_set('sandsower', 'MMA').
card_original_type('sandsower'/'MMA', 'Creature — Spirit').
card_original_text('sandsower'/'MMA', 'Tap three untapped creatures you control: Tap target creature.').
card_image_name('sandsower'/'MMA', 'sandsower').
card_uid('sandsower'/'MMA', 'MMA:Sandsower:sandsower').
card_rarity('sandsower'/'MMA', 'Uncommon').
card_artist('sandsower'/'MMA', 'Kev Walker').
card_number('sandsower'/'MMA', '29').
card_flavor_text('sandsower'/'MMA', 'It drifts through the streets as a breeze of collective sighs, wilting the bustle with dreams and heavy eyelids.').
card_multiverse_id('sandsower'/'MMA', '370392').

card_in_set('sarkhan vol', 'MMA').
card_original_type('sarkhan vol'/'MMA', 'Planeswalker — Sarkhan').
card_original_text('sarkhan vol'/'MMA', '+1: Creatures you control get +1/+1 and gain haste until end of turn.\n-2: Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.\n-6: Put five 4/4 red Dragon creature tokens with flying onto the battlefield.').
card_image_name('sarkhan vol'/'MMA', 'sarkhan vol').
card_uid('sarkhan vol'/'MMA', 'MMA:Sarkhan Vol:sarkhan vol').
card_rarity('sarkhan vol'/'MMA', 'Mythic Rare').
card_artist('sarkhan vol'/'MMA', 'Daarken').
card_number('sarkhan vol'/'MMA', '183').
card_multiverse_id('sarkhan vol'/'MMA', '370566').

card_in_set('scion of oona', 'MMA').
card_original_type('scion of oona'/'MMA', 'Creature — Faerie Soldier').
card_original_text('scion of oona'/'MMA', 'Flash\nFlying\nOther Faerie creatures you control get +1/+1.\nOther Faeries you control have shroud.').
card_image_name('scion of oona'/'MMA', 'scion of oona').
card_uid('scion of oona'/'MMA', 'MMA:Scion of Oona:scion of oona').
card_rarity('scion of oona'/'MMA', 'Rare').
card_artist('scion of oona'/'MMA', 'Eric Fortune').
card_number('scion of oona'/'MMA', '63').
card_multiverse_id('scion of oona'/'MMA', '370572').

card_in_set('search for tomorrow', 'MMA').
card_original_type('search for tomorrow'/'MMA', 'Sorcery').
card_original_text('search for tomorrow'/'MMA', 'Search your library for a basic land card and put it onto the battlefield. Then shuffle your library.\nSuspend 2—{G} (Rather than cast this card from your hand, you may pay {G} and exile it with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_image_name('search for tomorrow'/'MMA', 'search for tomorrow').
card_uid('search for tomorrow'/'MMA', 'MMA:Search for Tomorrow:search for tomorrow').
card_rarity('search for tomorrow'/'MMA', 'Common').
card_artist('search for tomorrow'/'MMA', 'Randy Gallegos').
card_number('search for tomorrow'/'MMA', '161').
card_multiverse_id('search for tomorrow'/'MMA', '370526').

card_in_set('shrapnel blast', 'MMA').
card_original_type('shrapnel blast'/'MMA', 'Instant').
card_original_text('shrapnel blast'/'MMA', 'As an additional cost to cast Shrapnel Blast, sacrifice an artifact.\nShrapnel Blast deals 5 damage to target creature or player.').
card_image_name('shrapnel blast'/'MMA', 'shrapnel blast').
card_uid('shrapnel blast'/'MMA', 'MMA:Shrapnel Blast:shrapnel blast').
card_rarity('shrapnel blast'/'MMA', 'Uncommon').
card_artist('shrapnel blast'/'MMA', 'Hideaki Takamura').
card_number('shrapnel blast'/'MMA', '129').
card_flavor_text('shrapnel blast'/'MMA', 'From trinket to trauma.').
card_multiverse_id('shrapnel blast'/'MMA', '370573').

card_in_set('skeletal vampire', 'MMA').
card_original_type('skeletal vampire'/'MMA', 'Creature — Vampire Skeleton').
card_original_text('skeletal vampire'/'MMA', 'Flying\nWhen Skeletal Vampire enters the battlefield, put two 1/1 black Bat creature tokens with flying onto the battlefield.\n{3}{B}{B}, Sacrifice a Bat: Put two 1/1 black Bat creature tokens with flying onto the battlefield.\nSacrifice a Bat: Regenerate Skeletal Vampire.').
card_image_name('skeletal vampire'/'MMA', 'skeletal vampire').
card_uid('skeletal vampire'/'MMA', 'MMA:Skeletal Vampire:skeletal vampire').
card_rarity('skeletal vampire'/'MMA', 'Rare').
card_artist('skeletal vampire'/'MMA', 'Wayne Reynolds').
card_number('skeletal vampire'/'MMA', '96').
card_multiverse_id('skeletal vampire'/'MMA', '370363').

card_in_set('skyreach manta', 'MMA').
card_original_type('skyreach manta'/'MMA', 'Artifact Creature — Fish').
card_original_text('skyreach manta'/'MMA', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\nFlying').
card_image_name('skyreach manta'/'MMA', 'skyreach manta').
card_uid('skyreach manta'/'MMA', 'MMA:Skyreach Manta:skyreach manta').
card_rarity('skyreach manta'/'MMA', 'Common').
card_artist('skyreach manta'/'MMA', 'Christopher Moeller').
card_number('skyreach manta'/'MMA', '215').
card_flavor_text('skyreach manta'/'MMA', 'As the dawns break, the manta soars.').
card_multiverse_id('skyreach manta'/'MMA', '370356').

card_in_set('slaughter pact', 'MMA').
card_original_type('slaughter pact'/'MMA', 'Instant').
card_original_text('slaughter pact'/'MMA', 'Destroy target nonblack creature.\nAt the beginning of your next upkeep, pay {2}{B}. If you don\'t, you lose the game.').
card_image_name('slaughter pact'/'MMA', 'slaughter pact').
card_uid('slaughter pact'/'MMA', 'MMA:Slaughter Pact:slaughter pact').
card_rarity('slaughter pact'/'MMA', 'Rare').
card_artist('slaughter pact'/'MMA', 'Kev Walker').
card_number('slaughter pact'/'MMA', '97').
card_flavor_text('slaughter pact'/'MMA', 'Death is only the beginning of the end.').
card_multiverse_id('slaughter pact'/'MMA', '370457').

card_in_set('spell snare', 'MMA').
card_original_type('spell snare'/'MMA', 'Instant').
card_original_text('spell snare'/'MMA', 'Counter target spell with converted mana cost 2.').
card_image_name('spell snare'/'MMA', 'spell snare').
card_uid('spell snare'/'MMA', 'MMA:Spell Snare:spell snare').
card_rarity('spell snare'/'MMA', 'Uncommon').
card_artist('spell snare'/'MMA', 'Hideaki Takamura').
card_number('spell snare'/'MMA', '64').
card_flavor_text('spell snare'/'MMA', 'Every culture has its unlucky numbers. In a city where you\'re either alone, in a crowd, or being stabbed in the back, two is the worst number of all.').
card_multiverse_id('spell snare'/'MMA', '370447').

card_in_set('spellstutter sprite', 'MMA').
card_original_type('spellstutter sprite'/'MMA', 'Creature — Faerie Wizard').
card_original_text('spellstutter sprite'/'MMA', 'Flash\nFlying\nWhen Spellstutter Sprite enters the battlefield, counter target spell with converted mana cost X or less, where X is the number of Faeries you control.').
card_image_name('spellstutter sprite'/'MMA', 'spellstutter sprite').
card_uid('spellstutter sprite'/'MMA', 'MMA:Spellstutter Sprite:spellstutter sprite').
card_rarity('spellstutter sprite'/'MMA', 'Common').
card_artist('spellstutter sprite'/'MMA', 'Rebecca Guay').
card_number('spellstutter sprite'/'MMA', '65').
card_multiverse_id('spellstutter sprite'/'MMA', '370380').

card_in_set('sporesower thallid', 'MMA').
card_original_type('sporesower thallid'/'MMA', 'Creature — Fungus').
card_original_text('sporesower thallid'/'MMA', 'At the beginning of your upkeep, put a spore counter on each Fungus you control.\nRemove three spore counters from Sporesower Thallid: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('sporesower thallid'/'MMA', 'sporesower thallid').
card_uid('sporesower thallid'/'MMA', 'MMA:Sporesower Thallid:sporesower thallid').
card_rarity('sporesower thallid'/'MMA', 'Uncommon').
card_artist('sporesower thallid'/'MMA', 'Ron Spencer').
card_number('sporesower thallid'/'MMA', '162').
card_multiverse_id('sporesower thallid'/'MMA', '370441').

card_in_set('sporoloth ancient', 'MMA').
card_original_type('sporoloth ancient'/'MMA', 'Creature — Fungus').
card_original_text('sporoloth ancient'/'MMA', 'At the beginning of your upkeep, put a spore counter on Sporoloth Ancient.\nCreatures you control have \"Remove two spore counters from this creature: Put a 1/1 green Saproling creature token onto the battlefield.\"').
card_image_name('sporoloth ancient'/'MMA', 'sporoloth ancient').
card_uid('sporoloth ancient'/'MMA', 'MMA:Sporoloth Ancient:sporoloth ancient').
card_rarity('sporoloth ancient'/'MMA', 'Common').
card_artist('sporoloth ancient'/'MMA', 'James Kei').
card_number('sporoloth ancient'/'MMA', '163').
card_multiverse_id('sporoloth ancient'/'MMA', '370503').

card_in_set('squee, goblin nabob', 'MMA').
card_original_type('squee, goblin nabob'/'MMA', 'Legendary Creature — Goblin').
card_original_text('squee, goblin nabob'/'MMA', 'At the beginning of your upkeep, you may return Squee, Goblin Nabob from your graveyard to your hand.').
card_image_name('squee, goblin nabob'/'MMA', 'squee, goblin nabob').
card_uid('squee, goblin nabob'/'MMA', 'MMA:Squee, Goblin Nabob:squee, goblin nabob').
card_rarity('squee, goblin nabob'/'MMA', 'Rare').
card_artist('squee, goblin nabob'/'MMA', 'Greg Staples').
card_number('squee, goblin nabob'/'MMA', '130').
card_flavor_text('squee, goblin nabob'/'MMA', '\"Some goblins are expendable. Some are impossible to get rid of. But he\'s both—at the same time!\"\n—Starke').
card_multiverse_id('squee, goblin nabob'/'MMA', '370461').

card_in_set('stingscourger', 'MMA').
card_original_type('stingscourger'/'MMA', 'Creature — Goblin Warrior').
card_original_text('stingscourger'/'MMA', 'Echo {3}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Stingscourger enters the battlefield, return target creature an opponent controls to its owner\'s hand.').
card_image_name('stingscourger'/'MMA', 'stingscourger').
card_uid('stingscourger'/'MMA', 'MMA:Stingscourger:stingscourger').
card_rarity('stingscourger'/'MMA', 'Common').
card_artist('stingscourger'/'MMA', 'Wayne Reynolds').
card_number('stingscourger'/'MMA', '131').
card_multiverse_id('stingscourger'/'MMA', '370402').

card_in_set('stinkdrinker daredevil', 'MMA').
card_original_type('stinkdrinker daredevil'/'MMA', 'Creature — Goblin Rogue').
card_original_text('stinkdrinker daredevil'/'MMA', 'Giant spells you cast cost {2} less to cast.').
card_image_name('stinkdrinker daredevil'/'MMA', 'stinkdrinker daredevil').
card_uid('stinkdrinker daredevil'/'MMA', 'MMA:Stinkdrinker Daredevil:stinkdrinker daredevil').
card_rarity('stinkdrinker daredevil'/'MMA', 'Common').
card_artist('stinkdrinker daredevil'/'MMA', 'Pete Venters').
card_number('stinkdrinker daredevil'/'MMA', '132').
card_flavor_text('stinkdrinker daredevil'/'MMA', 'Boggarts constantly strive to outdo each other with the things they bring back to the warren, each hoping the exploit will become as well-known as those of Auntie Grub.').
card_multiverse_id('stinkdrinker daredevil'/'MMA', '370483').

card_in_set('stinkweed imp', 'MMA').
card_original_type('stinkweed imp'/'MMA', 'Creature — Imp').
card_original_text('stinkweed imp'/'MMA', 'Flying\nWhenever Stinkweed Imp deals combat damage to a creature, destroy that creature.\nDredge 5 (If you would draw a card, instead you may put exactly five cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('stinkweed imp'/'MMA', 'stinkweed imp').
card_uid('stinkweed imp'/'MMA', 'MMA:Stinkweed Imp:stinkweed imp').
card_rarity('stinkweed imp'/'MMA', 'Common').
card_artist('stinkweed imp'/'MMA', 'Nils Hamm').
card_number('stinkweed imp'/'MMA', '98').
card_multiverse_id('stinkweed imp'/'MMA', '370450').
card_watermark('stinkweed imp'/'MMA', 'Golgari').

card_in_set('stir the pride', 'MMA').
card_original_type('stir the pride'/'MMA', 'Instant').
card_original_text('stir the pride'/'MMA', 'Choose one — Creatures you control get +2/+2 until end of turn; or until end of turn, creatures you control gain \"Whenever this creature deals damage, you gain that much life.\"\nEntwine {1}{W} (Choose both if you pay the entwine cost.)').
card_image_name('stir the pride'/'MMA', 'stir the pride').
card_uid('stir the pride'/'MMA', 'MMA:Stir the Pride:stir the pride').
card_rarity('stir the pride'/'MMA', 'Uncommon').
card_artist('stir the pride'/'MMA', 'Matt Cavotta').
card_number('stir the pride'/'MMA', '30').
card_multiverse_id('stir the pride'/'MMA', '370507').

card_in_set('stonehewer giant', 'MMA').
card_original_type('stonehewer giant'/'MMA', 'Creature — Giant Warrior').
card_original_text('stonehewer giant'/'MMA', 'Vigilance\n{1}{W}, {T}: Search your library for an Equipment card and put it onto the battlefield. Attach it to a creature you control. Then shuffle your library.').
card_image_name('stonehewer giant'/'MMA', 'stonehewer giant').
card_uid('stonehewer giant'/'MMA', 'MMA:Stonehewer Giant:stonehewer giant').
card_rarity('stonehewer giant'/'MMA', 'Rare').
card_artist('stonehewer giant'/'MMA', 'Steve Prescott').
card_number('stonehewer giant'/'MMA', '31').
card_flavor_text('stonehewer giant'/'MMA', '\"No matter how strong, an unarmed fighter is no more than a fool.\"').
card_multiverse_id('stonehewer giant'/'MMA', '370515').

card_in_set('street wraith', 'MMA').
card_original_type('street wraith'/'MMA', 'Creature — Wraith').
card_original_text('street wraith'/'MMA', 'Swampwalk\nCycling—Pay 2 life. (Pay 2 life, Discard this card: Draw a card.)').
card_image_name('street wraith'/'MMA', 'street wraith').
card_uid('street wraith'/'MMA', 'MMA:Street Wraith:street wraith').
card_rarity('street wraith'/'MMA', 'Common').
card_artist('street wraith'/'MMA', 'Cyril Van Der Haegen').
card_number('street wraith'/'MMA', '99').
card_flavor_text('street wraith'/'MMA', 'The lamps on Wyndmoor Street snuff themselves at midnight and refuse to relight, afraid to illuminate what lies in the darkness.').
card_multiverse_id('street wraith'/'MMA', '370428').

card_in_set('sudden shock', 'MMA').
card_original_type('sudden shock'/'MMA', 'Instant').
card_original_text('sudden shock'/'MMA', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nSudden Shock deals 2 damage to target creature or player.').
card_image_name('sudden shock'/'MMA', 'sudden shock').
card_uid('sudden shock'/'MMA', 'MMA:Sudden Shock:sudden shock').
card_rarity('sudden shock'/'MMA', 'Uncommon').
card_artist('sudden shock'/'MMA', 'Vance Kovacs').
card_number('sudden shock'/'MMA', '133').
card_flavor_text('sudden shock'/'MMA', 'Arc mages aren\'t known for their patience.').
card_multiverse_id('sudden shock'/'MMA', '370388').

card_in_set('summoner\'s pact', 'MMA').
card_original_type('summoner\'s pact'/'MMA', 'Instant').
card_original_text('summoner\'s pact'/'MMA', 'Search your library for a green creature card, reveal it, and put it into your hand. Then shuffle your library.\nAt the beginning of your next upkeep, pay {2}{G}{G}. If you don\'t, you lose the game.').
card_image_name('summoner\'s pact'/'MMA', 'summoner\'s pact').
card_uid('summoner\'s pact'/'MMA', 'MMA:Summoner\'s Pact:summoner\'s pact').
card_rarity('summoner\'s pact'/'MMA', 'Rare').
card_artist('summoner\'s pact'/'MMA', 'Chippy').
card_number('summoner\'s pact'/'MMA', '164').
card_multiverse_id('summoner\'s pact'/'MMA', '370563').

card_in_set('sword of fire and ice', 'MMA').
card_original_type('sword of fire and ice'/'MMA', 'Artifact — Equipment').
card_original_text('sword of fire and ice'/'MMA', 'Equipped creature gets +2/+2 and has protection from red and from blue.\nWhenever equipped creature deals combat damage to a player, Sword of Fire and Ice deals 2 damage to target creature or player and you draw a card.\nEquip {2}').
card_image_name('sword of fire and ice'/'MMA', 'sword of fire and ice').
card_uid('sword of fire and ice'/'MMA', 'MMA:Sword of Fire and Ice:sword of fire and ice').
card_rarity('sword of fire and ice'/'MMA', 'Mythic Rare').
card_artist('sword of fire and ice'/'MMA', 'Chris Rahn').
card_number('sword of fire and ice'/'MMA', '216').
card_multiverse_id('sword of fire and ice'/'MMA', '370471').

card_in_set('sword of light and shadow', 'MMA').
card_original_type('sword of light and shadow'/'MMA', 'Artifact — Equipment').
card_original_text('sword of light and shadow'/'MMA', 'Equipped creature gets +2/+2 and has protection from white and from black.\nWhenever equipped creature deals combat damage to a player, you gain 3 life and you may return up to one target creature card from your graveyard to your hand.\nEquip {2}').
card_image_name('sword of light and shadow'/'MMA', 'sword of light and shadow').
card_uid('sword of light and shadow'/'MMA', 'MMA:Sword of Light and Shadow:sword of light and shadow').
card_rarity('sword of light and shadow'/'MMA', 'Mythic Rare').
card_artist('sword of light and shadow'/'MMA', 'Chris Rahn').
card_number('sword of light and shadow'/'MMA', '217').
card_multiverse_id('sword of light and shadow'/'MMA', '370455').

card_in_set('sylvan bounty', 'MMA').
card_original_type('sylvan bounty'/'MMA', 'Instant').
card_original_text('sylvan bounty'/'MMA', 'Target player gains 8 life.\nBasic landcycling {1}{G} ({1}{G}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('sylvan bounty'/'MMA', 'sylvan bounty').
card_uid('sylvan bounty'/'MMA', 'MMA:Sylvan Bounty:sylvan bounty').
card_rarity('sylvan bounty'/'MMA', 'Common').
card_artist('sylvan bounty'/'MMA', 'Chris Rahn').
card_number('sylvan bounty'/'MMA', '165').
card_flavor_text('sylvan bounty'/'MMA', 'Some who scouted new lands chose to stay.').
card_multiverse_id('sylvan bounty'/'MMA', '370372').

card_in_set('syphon life', 'MMA').
card_original_type('syphon life'/'MMA', 'Sorcery').
card_original_text('syphon life'/'MMA', 'Target player loses 2 life and you gain 2 life.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_image_name('syphon life'/'MMA', 'syphon life').
card_uid('syphon life'/'MMA', 'MMA:Syphon Life:syphon life').
card_rarity('syphon life'/'MMA', 'Common').
card_artist('syphon life'/'MMA', 'Dan Seagrave').
card_number('syphon life'/'MMA', '100').
card_flavor_text('syphon life'/'MMA', 'Leeches never tire of feeding.').
card_multiverse_id('syphon life'/'MMA', '370474').

card_in_set('take possession', 'MMA').
card_original_type('take possession'/'MMA', 'Enchantment — Aura').
card_original_text('take possession'/'MMA', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nEnchant permanent\nYou control enchanted permanent.').
card_image_name('take possession'/'MMA', 'take possession').
card_uid('take possession'/'MMA', 'MMA:Take Possession:take possession').
card_rarity('take possession'/'MMA', 'Uncommon').
card_artist('take possession'/'MMA', 'Michael Phillippi').
card_number('take possession'/'MMA', '66').
card_multiverse_id('take possession'/'MMA', '370453').

card_in_set('tar pitcher', 'MMA').
card_original_type('tar pitcher'/'MMA', 'Creature — Goblin Shaman').
card_original_text('tar pitcher'/'MMA', '{T}, Sacrifice a Goblin: Tar Pitcher deals 2 damage to target creature or player.').
card_image_name('tar pitcher'/'MMA', 'tar pitcher').
card_uid('tar pitcher'/'MMA', 'MMA:Tar Pitcher:tar pitcher').
card_rarity('tar pitcher'/'MMA', 'Uncommon').
card_artist('tar pitcher'/'MMA', 'Omar Rayyan').
card_number('tar pitcher'/'MMA', '134').
card_flavor_text('tar pitcher'/'MMA', '\"Auntie Grub had caught a goat, a bleating doe that still squirmed in her arms. At the same time, her warren decided to share with her a sensory greeting of hot tar . . . .\"\n—A tale of Auntie Grub').
card_multiverse_id('tar pitcher'/'MMA', '370467').

card_in_set('tarmogoyf', 'MMA').
card_original_type('tarmogoyf'/'MMA', 'Creature — Lhurgoyf').
card_original_text('tarmogoyf'/'MMA', 'Tarmogoyf\'s power is equal to the number of card types among cards in all graveyards and its toughness is equal to that number plus 1.').
card_image_name('tarmogoyf'/'MMA', 'tarmogoyf').
card_uid('tarmogoyf'/'MMA', 'MMA:Tarmogoyf:tarmogoyf').
card_rarity('tarmogoyf'/'MMA', 'Mythic Rare').
card_artist('tarmogoyf'/'MMA', 'Ryan Barger').
card_number('tarmogoyf'/'MMA', '166').
card_flavor_text('tarmogoyf'/'MMA', 'What doesn\'t grow, dies. And what dies grows the tarmogoyf.').
card_multiverse_id('tarmogoyf'/'MMA', '370404').

card_in_set('terashi\'s grasp', 'MMA').
card_original_type('terashi\'s grasp'/'MMA', 'Sorcery — Arcane').
card_original_text('terashi\'s grasp'/'MMA', 'Destroy target artifact or enchantment. You gain life equal to its converted mana cost.').
card_image_name('terashi\'s grasp'/'MMA', 'terashi\'s grasp').
card_uid('terashi\'s grasp'/'MMA', 'MMA:Terashi\'s Grasp:terashi\'s grasp').
card_rarity('terashi\'s grasp'/'MMA', 'Uncommon').
card_artist('terashi\'s grasp'/'MMA', 'Mark Tedin').
card_number('terashi\'s grasp'/'MMA', '32').
card_flavor_text('terashi\'s grasp'/'MMA', '\"The jeweler, the potter, the smith . . . They all imbue a bit of their souls into their creations. The kami destroy that crafted mortal shell and absorb the soul within.\"\n—Noboru, master kitemaker').
card_multiverse_id('terashi\'s grasp'/'MMA', '370555').

card_in_set('terramorphic expanse', 'MMA').
card_original_type('terramorphic expanse'/'MMA', 'Land').
card_original_text('terramorphic expanse'/'MMA', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'MMA', 'terramorphic expanse').
card_uid('terramorphic expanse'/'MMA', 'MMA:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'MMA', 'Common').
card_artist('terramorphic expanse'/'MMA', 'Dan Scott').
card_number('terramorphic expanse'/'MMA', '224').
card_flavor_text('terramorphic expanse'/'MMA', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'MMA', '370459').

card_in_set('test of faith', 'MMA').
card_original_type('test of faith'/'MMA', 'Instant').
card_original_text('test of faith'/'MMA', 'Prevent the next 3 damage that would be dealt to target creature this turn, and put a +1/+1 counter on that creature for each 1 damage prevented this way.').
card_image_name('test of faith'/'MMA', 'test of faith').
card_uid('test of faith'/'MMA', 'MMA:Test of Faith:test of faith').
card_rarity('test of faith'/'MMA', 'Common').
card_artist('test of faith'/'MMA', 'Vance Kovacs').
card_number('test of faith'/'MMA', '33').
card_flavor_text('test of faith'/'MMA', 'Those who survive the test bear a mark of power anyone can recognize.').
card_multiverse_id('test of faith'/'MMA', '370554').

card_in_set('thallid', 'MMA').
card_original_type('thallid'/'MMA', 'Creature — Fungus').
card_original_text('thallid'/'MMA', 'At the beginning of your upkeep, put a spore counter on Thallid.\nRemove three spore counters from Thallid: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('thallid'/'MMA', 'thallid').
card_uid('thallid'/'MMA', 'MMA:Thallid:thallid').
card_rarity('thallid'/'MMA', 'Common').
card_artist('thallid'/'MMA', 'Trevor Claxton').
card_number('thallid'/'MMA', '167').
card_multiverse_id('thallid'/'MMA', '370352').

card_in_set('thallid germinator', 'MMA').
card_original_type('thallid germinator'/'MMA', 'Creature — Fungus').
card_original_text('thallid germinator'/'MMA', 'At the beginning of your upkeep, put a spore counter on Thallid Germinator.\nRemove three spore counters from Thallid Germinator: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Target creature gets +1/+1 until end of turn.').
card_image_name('thallid germinator'/'MMA', 'thallid germinator').
card_uid('thallid germinator'/'MMA', 'MMA:Thallid Germinator:thallid germinator').
card_rarity('thallid germinator'/'MMA', 'Common').
card_artist('thallid germinator'/'MMA', 'Marco Nelor').
card_number('thallid germinator'/'MMA', '168').
card_multiverse_id('thallid germinator'/'MMA', '370500').

card_in_set('thallid shell-dweller', 'MMA').
card_original_type('thallid shell-dweller'/'MMA', 'Creature — Fungus').
card_original_text('thallid shell-dweller'/'MMA', 'Defender\nAt the beginning of your upkeep, put a spore counter on Thallid Shell-Dweller.\nRemove three spore counters from Thallid Shell-Dweller: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('thallid shell-dweller'/'MMA', 'thallid shell-dweller').
card_uid('thallid shell-dweller'/'MMA', 'MMA:Thallid Shell-Dweller:thallid shell-dweller').
card_rarity('thallid shell-dweller'/'MMA', 'Common').
card_artist('thallid shell-dweller'/'MMA', 'Carl Critchlow').
card_number('thallid shell-dweller'/'MMA', '169').
card_multiverse_id('thallid shell-dweller'/'MMA', '370486').

card_in_set('thieving sprite', 'MMA').
card_original_type('thieving sprite'/'MMA', 'Creature — Faerie Rogue').
card_original_text('thieving sprite'/'MMA', 'Flying\nWhen Thieving Sprite enters the battlefield, target player reveals X cards from his or her hand, where X is the number of Faeries you control. You choose one of those cards. That player discards that card.').
card_image_name('thieving sprite'/'MMA', 'thieving sprite').
card_uid('thieving sprite'/'MMA', 'MMA:Thieving Sprite:thieving sprite').
card_rarity('thieving sprite'/'MMA', 'Common').
card_artist('thieving sprite'/'MMA', 'Dan Scott').
card_number('thieving sprite'/'MMA', '101').
card_multiverse_id('thieving sprite'/'MMA', '370451').

card_in_set('thirst for knowledge', 'MMA').
card_original_type('thirst for knowledge'/'MMA', 'Instant').
card_original_text('thirst for knowledge'/'MMA', 'Draw three cards. Then discard two cards unless you discard an artifact card.').
card_image_name('thirst for knowledge'/'MMA', 'thirst for knowledge').
card_uid('thirst for knowledge'/'MMA', 'MMA:Thirst for Knowledge:thirst for knowledge').
card_rarity('thirst for knowledge'/'MMA', 'Uncommon').
card_artist('thirst for knowledge'/'MMA', 'Anthony Francisco').
card_number('thirst for knowledge'/'MMA', '67').
card_flavor_text('thirst for knowledge'/'MMA', '\"To truly know something, you must become it. The trick is to not lose yourself in the process.\"').
card_multiverse_id('thirst for knowledge'/'MMA', '370525').

card_in_set('thundercloud shaman', 'MMA').
card_original_type('thundercloud shaman'/'MMA', 'Creature — Giant Shaman').
card_original_text('thundercloud shaman'/'MMA', 'When Thundercloud Shaman enters the battlefield, it deals damage equal to the number of Giants you control to each non-Giant creature.').
card_image_name('thundercloud shaman'/'MMA', 'thundercloud shaman').
card_uid('thundercloud shaman'/'MMA', 'MMA:Thundercloud Shaman:thundercloud shaman').
card_rarity('thundercloud shaman'/'MMA', 'Uncommon').
card_artist('thundercloud shaman'/'MMA', 'Greg Staples').
card_number('thundercloud shaman'/'MMA', '135').
card_flavor_text('thundercloud shaman'/'MMA', 'He cares not for the disasters his storm brings as long as his path ahead is clear.').
card_multiverse_id('thundercloud shaman'/'MMA', '370399').

card_in_set('thundering giant', 'MMA').
card_original_type('thundering giant'/'MMA', 'Creature — Giant').
card_original_text('thundering giant'/'MMA', 'Haste').
card_image_name('thundering giant'/'MMA', 'thundering giant').
card_uid('thundering giant'/'MMA', 'MMA:Thundering Giant:thundering giant').
card_rarity('thundering giant'/'MMA', 'Common').
card_artist('thundering giant'/'MMA', 'Mark Zug').
card_number('thundering giant'/'MMA', '136').
card_flavor_text('thundering giant'/'MMA', 'The giant was felt a few seconds before he was seen.').
card_multiverse_id('thundering giant'/'MMA', '370452').

card_in_set('tidehollow sculler', 'MMA').
card_original_type('tidehollow sculler'/'MMA', 'Artifact Creature — Zombie').
card_original_text('tidehollow sculler'/'MMA', 'When Tidehollow Sculler enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Tidehollow Sculler leaves the battlefield, return the exiled card to its owner\'s hand.').
card_image_name('tidehollow sculler'/'MMA', 'tidehollow sculler').
card_uid('tidehollow sculler'/'MMA', 'MMA:Tidehollow Sculler:tidehollow sculler').
card_rarity('tidehollow sculler'/'MMA', 'Uncommon').
card_artist('tidehollow sculler'/'MMA', 'rk post').
card_number('tidehollow sculler'/'MMA', '184').
card_multiverse_id('tidehollow sculler'/'MMA', '370375').

card_in_set('tombstalker', 'MMA').
card_original_type('tombstalker'/'MMA', 'Creature — Demon').
card_original_text('tombstalker'/'MMA', 'Flying\nDelve (You may exile any number of cards from your graveyard as you cast this spell. It costs {1} less to cast for each card exiled this way.)').
card_image_name('tombstalker'/'MMA', 'tombstalker').
card_uid('tombstalker'/'MMA', 'MMA:Tombstalker:tombstalker').
card_rarity('tombstalker'/'MMA', 'Rare').
card_artist('tombstalker'/'MMA', 'Aleksi Briclot').
card_number('tombstalker'/'MMA', '102').
card_multiverse_id('tombstalker'/'MMA', '370539').

card_in_set('tooth and nail', 'MMA').
card_original_type('tooth and nail'/'MMA', 'Sorcery').
card_original_text('tooth and nail'/'MMA', 'Choose one — Search your library for up to two creature cards, reveal them, put them into your hand, then shuffle your library; or put up to two creature cards from your hand onto the battlefield.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_image_name('tooth and nail'/'MMA', 'tooth and nail').
card_uid('tooth and nail'/'MMA', 'MMA:Tooth and Nail:tooth and nail').
card_rarity('tooth and nail'/'MMA', 'Rare').
card_artist('tooth and nail'/'MMA', 'Jesper Ejsing').
card_number('tooth and nail'/'MMA', '170').
card_multiverse_id('tooth and nail'/'MMA', '370432').

card_in_set('torrent of stone', 'MMA').
card_original_type('torrent of stone'/'MMA', 'Instant — Arcane').
card_original_text('torrent of stone'/'MMA', 'Torrent of Stone deals 4 damage to target creature.\nSplice onto Arcane—Sacrifice two Mountains. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_image_name('torrent of stone'/'MMA', 'torrent of stone').
card_uid('torrent of stone'/'MMA', 'MMA:Torrent of Stone:torrent of stone').
card_rarity('torrent of stone'/'MMA', 'Common').
card_artist('torrent of stone'/'MMA', 'Greg Staples').
card_number('torrent of stone'/'MMA', '137').
card_multiverse_id('torrent of stone'/'MMA', '370558').

card_in_set('traumatic visions', 'MMA').
card_original_type('traumatic visions'/'MMA', 'Instant').
card_original_text('traumatic visions'/'MMA', 'Counter target spell.\nBasic landcycling {1}{U} ({1}{U}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('traumatic visions'/'MMA', 'traumatic visions').
card_uid('traumatic visions'/'MMA', 'MMA:Traumatic Visions:traumatic visions').
card_rarity('traumatic visions'/'MMA', 'Common').
card_artist('traumatic visions'/'MMA', 'Cyril Van Der Haegen').
card_number('traumatic visions'/'MMA', '68').
card_multiverse_id('traumatic visions'/'MMA', '370369').

card_in_set('tribal flames', 'MMA').
card_original_type('tribal flames'/'MMA', 'Sorcery').
card_original_text('tribal flames'/'MMA', 'Domain — Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_image_name('tribal flames'/'MMA', 'tribal flames').
card_uid('tribal flames'/'MMA', 'MMA:Tribal Flames:tribal flames').
card_rarity('tribal flames'/'MMA', 'Uncommon').
card_artist('tribal flames'/'MMA', 'Zack Stella').
card_number('tribal flames'/'MMA', '138').
card_flavor_text('tribal flames'/'MMA', '\"Fire is the universal language.\"\n—Jhoira, master artificer').
card_multiverse_id('tribal flames'/'MMA', '370362').

card_in_set('tromp the domains', 'MMA').
card_original_type('tromp the domains'/'MMA', 'Sorcery').
card_original_text('tromp the domains'/'MMA', 'Domain — Until end of turn, creatures you control gain trample and get +1/+1 for each basic land type among lands you control.').
card_image_name('tromp the domains'/'MMA', 'tromp the domains').
card_uid('tromp the domains'/'MMA', 'MMA:Tromp the Domains:tromp the domains').
card_rarity('tromp the domains'/'MMA', 'Uncommon').
card_artist('tromp the domains'/'MMA', 'Mike Dringenberg').
card_number('tromp the domains'/'MMA', '171').
card_flavor_text('tromp the domains'/'MMA', 'Ground into the footprints of the ravaging herd were clumps of salt from Benalia, moss from Llanowar, dust from Hurloon, and ash from as far as Urborg.').
card_multiverse_id('tromp the domains'/'MMA', '370364').

card_in_set('trygon predator', 'MMA').
card_original_type('trygon predator'/'MMA', 'Creature — Beast').
card_original_text('trygon predator'/'MMA', 'Flying\nWhenever Trygon Predator deals combat damage to a player, you may destroy target artifact or enchantment that player controls.').
card_image_name('trygon predator'/'MMA', 'trygon predator').
card_uid('trygon predator'/'MMA', 'MMA:Trygon Predator:trygon predator').
card_rarity('trygon predator'/'MMA', 'Uncommon').
card_artist('trygon predator'/'MMA', 'Jack Wang').
card_number('trygon predator'/'MMA', '185').
card_flavor_text('trygon predator'/'MMA', 'Held aloft by metabolized magic, trygons are ravenous for sources of mystic fuel.').
card_multiverse_id('trygon predator'/'MMA', '370476').
card_watermark('trygon predator'/'MMA', 'Simic').

card_in_set('vedalken dismisser', 'MMA').
card_original_type('vedalken dismisser'/'MMA', 'Creature — Vedalken Wizard').
card_original_text('vedalken dismisser'/'MMA', 'When Vedalken Dismisser enters the battlefield, put target creature on top of its owner\'s library.').
card_image_name('vedalken dismisser'/'MMA', 'vedalken dismisser').
card_uid('vedalken dismisser'/'MMA', 'MMA:Vedalken Dismisser:vedalken dismisser').
card_rarity('vedalken dismisser'/'MMA', 'Common').
card_artist('vedalken dismisser'/'MMA', 'Dan Scott').
card_number('vedalken dismisser'/'MMA', '69').
card_flavor_text('vedalken dismisser'/'MMA', '\"I crept up on him, quiet as a guttermouse. And then suddenly I found myself facedown in a latrine pit behind Tin Street Market.\"\n—Sirislav, Dimir spy').
card_multiverse_id('vedalken dismisser'/'MMA', '370550').

card_in_set('vedalken shackles', 'MMA').
card_original_type('vedalken shackles'/'MMA', 'Artifact').
card_original_text('vedalken shackles'/'MMA', 'You may choose not to untap Vedalken Shackles during your untap step.\n{2}, {T}: Gain control of target creature with power less than or equal to the number of Islands you control for as long as Vedalken Shackles remains tapped.').
card_image_name('vedalken shackles'/'MMA', 'vedalken shackles').
card_uid('vedalken shackles'/'MMA', 'MMA:Vedalken Shackles:vedalken shackles').
card_rarity('vedalken shackles'/'MMA', 'Mythic Rare').
card_artist('vedalken shackles'/'MMA', 'Svetlin Velinov').
card_number('vedalken shackles'/'MMA', '218').
card_multiverse_id('vedalken shackles'/'MMA', '370366').

card_in_set('vendilion clique', 'MMA').
card_original_type('vendilion clique'/'MMA', 'Legendary Creature — Faerie Wizard').
card_original_text('vendilion clique'/'MMA', 'Flash\nFlying\nWhen Vendilion Clique enters the battlefield, look at target player\'s hand. You may choose a nonland card from it. If you do, that player reveals the chosen card, puts it on the bottom of his or her library, then draws a card.').
card_image_name('vendilion clique'/'MMA', 'vendilion clique').
card_uid('vendilion clique'/'MMA', 'MMA:Vendilion Clique:vendilion clique').
card_rarity('vendilion clique'/'MMA', 'Mythic Rare').
card_artist('vendilion clique'/'MMA', 'Michael Sutfin').
card_number('vendilion clique'/'MMA', '70').
card_multiverse_id('vendilion clique'/'MMA', '370390').

card_in_set('verdeloth the ancient', 'MMA').
card_original_type('verdeloth the ancient'/'MMA', 'Legendary Creature — Treefolk').
card_original_text('verdeloth the ancient'/'MMA', 'Kicker {X} (You may pay an additional {X} as you cast this spell.)\nSaproling creatures and other Treefolk creatures get +1/+1.\nWhen Verdeloth the Ancient enters the battlefield, if it was kicked, put X 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('verdeloth the ancient'/'MMA', 'verdeloth the ancient').
card_uid('verdeloth the ancient'/'MMA', 'MMA:Verdeloth the Ancient:verdeloth the ancient').
card_rarity('verdeloth the ancient'/'MMA', 'Rare').
card_artist('verdeloth the ancient'/'MMA', 'Daren Bader').
card_number('verdeloth the ancient'/'MMA', '172').
card_multiverse_id('verdeloth the ancient'/'MMA', '370494').

card_in_set('veteran armorer', 'MMA').
card_original_type('veteran armorer'/'MMA', 'Creature — Human Soldier').
card_original_text('veteran armorer'/'MMA', 'Other creatures you control get +0/+1.').
card_image_name('veteran armorer'/'MMA', 'veteran armorer').
card_uid('veteran armorer'/'MMA', 'MMA:Veteran Armorer:veteran armorer').
card_rarity('veteran armorer'/'MMA', 'Common').
card_artist('veteran armorer'/'MMA', 'Kev Walker').
card_number('veteran armorer'/'MMA', '34').
card_flavor_text('veteran armorer'/'MMA', '\"Give me a sword and I\'ll kill a few enemies. But give me a hammer and a fiery forge, and I\'ll turn the tide of battle.\"').
card_multiverse_id('veteran armorer'/'MMA', '370361').

card_in_set('vivid crag', 'MMA').
card_original_type('vivid crag'/'MMA', 'Land').
card_original_text('vivid crag'/'MMA', 'Vivid Crag enters the battlefield tapped with two charge counters on it.\n{T}: Add {R} to your mana pool.\n{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.').
card_image_name('vivid crag'/'MMA', 'vivid crag').
card_uid('vivid crag'/'MMA', 'MMA:Vivid Crag:vivid crag').
card_rarity('vivid crag'/'MMA', 'Uncommon').
card_artist('vivid crag'/'MMA', 'Martina Pilcerova').
card_number('vivid crag'/'MMA', '225').
card_multiverse_id('vivid crag'/'MMA', '370400').

card_in_set('vivid creek', 'MMA').
card_original_type('vivid creek'/'MMA', 'Land').
card_original_text('vivid creek'/'MMA', 'Vivid Creek enters the battlefield tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_image_name('vivid creek'/'MMA', 'vivid creek').
card_uid('vivid creek'/'MMA', 'MMA:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'MMA', 'Uncommon').
card_artist('vivid creek'/'MMA', 'Fred Fields').
card_number('vivid creek'/'MMA', '226').
card_multiverse_id('vivid creek'/'MMA', '370355').

card_in_set('vivid grove', 'MMA').
card_original_type('vivid grove'/'MMA', 'Land').
card_original_text('vivid grove'/'MMA', 'Vivid Grove enters the battlefield tapped with two charge counters on it.\n{T}: Add {G} to your mana pool.\n{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_image_name('vivid grove'/'MMA', 'vivid grove').
card_uid('vivid grove'/'MMA', 'MMA:Vivid Grove:vivid grove').
card_rarity('vivid grove'/'MMA', 'Uncommon').
card_artist('vivid grove'/'MMA', 'Howard Lyon').
card_number('vivid grove'/'MMA', '227').
card_multiverse_id('vivid grove'/'MMA', '370403').

card_in_set('vivid marsh', 'MMA').
card_original_type('vivid marsh'/'MMA', 'Land').
card_original_text('vivid marsh'/'MMA', 'Vivid Marsh enters the battlefield tapped with two charge counters on it.\n{T}: Add {B} to your mana pool.\n{T}, Remove a charge counter from Vivid Marsh: Add one mana of any color to your mana pool.').
card_image_name('vivid marsh'/'MMA', 'vivid marsh').
card_uid('vivid marsh'/'MMA', 'MMA:Vivid Marsh:vivid marsh').
card_rarity('vivid marsh'/'MMA', 'Uncommon').
card_artist('vivid marsh'/'MMA', 'John Avon').
card_number('vivid marsh'/'MMA', '228').
card_multiverse_id('vivid marsh'/'MMA', '370389').

card_in_set('vivid meadow', 'MMA').
card_original_type('vivid meadow'/'MMA', 'Land').
card_original_text('vivid meadow'/'MMA', 'Vivid Meadow enters the battlefield tapped with two charge counters on it.\n{T}: Add {W} to your mana pool.\n{T}, Remove a charge counter from Vivid Meadow: Add one mana of any color to your mana pool.').
card_image_name('vivid meadow'/'MMA', 'vivid meadow').
card_uid('vivid meadow'/'MMA', 'MMA:Vivid Meadow:vivid meadow').
card_rarity('vivid meadow'/'MMA', 'Uncommon').
card_artist('vivid meadow'/'MMA', 'Rob Alexander').
card_number('vivid meadow'/'MMA', '229').
card_multiverse_id('vivid meadow'/'MMA', '370351').

card_in_set('walker of the grove', 'MMA').
card_original_type('walker of the grove'/'MMA', 'Creature — Elemental').
card_original_text('walker of the grove'/'MMA', 'When Walker of the Grove leaves the battlefield, put a 4/4 green Elemental creature token onto the battlefield.\nEvoke {4}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('walker of the grove'/'MMA', 'walker of the grove').
card_uid('walker of the grove'/'MMA', 'MMA:Walker of the Grove:walker of the grove').
card_rarity('walker of the grove'/'MMA', 'Common').
card_artist('walker of the grove'/'MMA', 'Todd Lockwood').
card_number('walker of the grove'/'MMA', '173').
card_multiverse_id('walker of the grove'/'MMA', '370519').

card_in_set('war-spike changeling', 'MMA').
card_original_type('war-spike changeling'/'MMA', 'Creature — Shapeshifter').
card_original_text('war-spike changeling'/'MMA', 'Changeling (This card is every creature type at all times.)\n{R}: War-Spike Changeling gains first strike until end of turn.').
card_image_name('war-spike changeling'/'MMA', 'war-spike changeling').
card_uid('war-spike changeling'/'MMA', 'MMA:War-Spike Changeling:war-spike changeling').
card_rarity('war-spike changeling'/'MMA', 'Common').
card_artist('war-spike changeling'/'MMA', 'Mark Poole').
card_number('war-spike changeling'/'MMA', '139').
card_flavor_text('war-spike changeling'/'MMA', '\"Aren\'t there boggarts enough in Lorwyn? Couldn\'t it turn into a sheep? Or a sunflower?\"\n—Olly of Goldmeadow').
card_multiverse_id('war-spike changeling'/'MMA', '370479').

card_in_set('warren pilferers', 'MMA').
card_original_type('warren pilferers'/'MMA', 'Creature — Goblin Rogue').
card_original_text('warren pilferers'/'MMA', 'When Warren Pilferers enters the battlefield, return target creature card from your graveyard to your hand. If that card is a Goblin card, Warren Pilferers gains haste until end of turn.').
card_image_name('warren pilferers'/'MMA', 'warren pilferers').
card_uid('warren pilferers'/'MMA', 'MMA:Warren Pilferers:warren pilferers').
card_rarity('warren pilferers'/'MMA', 'Common').
card_artist('warren pilferers'/'MMA', 'Wayne Reynolds').
card_number('warren pilferers'/'MMA', '103').
card_flavor_text('warren pilferers'/'MMA', '\"What do they need all this stuff for? They\'re dead. We\'re alive. Simple enough.\"').
card_multiverse_id('warren pilferers'/'MMA', '370357').

card_in_set('warren weirding', 'MMA').
card_original_type('warren weirding'/'MMA', 'Tribal Sorcery — Goblin').
card_original_text('warren weirding'/'MMA', 'Target player sacrifices a creature. If a Goblin is sacrificed this way, that player puts two 1/1 black Goblin Rogue creature tokens onto the battlefield, and those tokens gain haste until end of turn.').
card_image_name('warren weirding'/'MMA', 'warren weirding').
card_uid('warren weirding'/'MMA', 'MMA:Warren Weirding:warren weirding').
card_rarity('warren weirding'/'MMA', 'Common').
card_artist('warren weirding'/'MMA', 'Matt Cavotta').
card_number('warren weirding'/'MMA', '104').
card_flavor_text('warren weirding'/'MMA', '\"And that\'s when it was discovered that boggarts have just half a brain.\"\n—The Book of Other Folk').
card_multiverse_id('warren weirding'/'MMA', '370488').

card_in_set('woodfall primus', 'MMA').
card_original_type('woodfall primus'/'MMA', 'Creature — Treefolk Shaman').
card_original_text('woodfall primus'/'MMA', 'Trample\nWhen Woodfall Primus enters the battlefield, destroy target noncreature permanent.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('woodfall primus'/'MMA', 'woodfall primus').
card_uid('woodfall primus'/'MMA', 'MMA:Woodfall Primus:woodfall primus').
card_rarity('woodfall primus'/'MMA', 'Rare').
card_artist('woodfall primus'/'MMA', 'Adam Rex').
card_number('woodfall primus'/'MMA', '174').
card_multiverse_id('woodfall primus'/'MMA', '370406').

card_in_set('worm harvest', 'MMA').
card_original_type('worm harvest'/'MMA', 'Sorcery').
card_original_text('worm harvest'/'MMA', 'Put a 1/1 black and green Worm creature token onto the battlefield for each land card in your graveyard.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_image_name('worm harvest'/'MMA', 'worm harvest').
card_uid('worm harvest'/'MMA', 'MMA:Worm Harvest:worm harvest').
card_rarity('worm harvest'/'MMA', 'Uncommon').
card_artist('worm harvest'/'MMA', 'Chuck Lukacs').
card_number('worm harvest'/'MMA', '195').
card_multiverse_id('worm harvest'/'MMA', '370532').

card_in_set('yosei, the morning star', 'MMA').
card_original_type('yosei, the morning star'/'MMA', 'Legendary Creature — Dragon Spirit').
card_original_text('yosei, the morning star'/'MMA', 'Flying\nWhen Yosei, the Morning Star dies, target player skips his or her next untap step. Tap up to five target permanents that player controls.').
card_image_name('yosei, the morning star'/'MMA', 'yosei, the morning star').
card_uid('yosei, the morning star'/'MMA', 'MMA:Yosei, the Morning Star:yosei, the morning star').
card_rarity('yosei, the morning star'/'MMA', 'Mythic Rare').
card_artist('yosei, the morning star'/'MMA', 'Hiro Izawa').
card_number('yosei, the morning star'/'MMA', '35').
card_multiverse_id('yosei, the morning star'/'MMA', '370574').
