% Morningtide

set('MOR').
set_name('MOR', 'Morningtide').
set_release_date('MOR', '2008-02-01').
set_border('MOR', 'black').
set_type('MOR', 'expansion').
set_block('MOR', 'Lorwyn').

card_in_set('ambassador oak', 'MOR').
card_original_type('ambassador oak'/'MOR', 'Creature — Treefolk Warrior').
card_original_text('ambassador oak'/'MOR', 'When Ambassador Oak comes into play, put a 1/1 green Elf Warrior creature token into play.').
card_first_print('ambassador oak', 'MOR').
card_image_name('ambassador oak'/'MOR', 'ambassador oak').
card_uid('ambassador oak'/'MOR', 'MOR:Ambassador Oak:ambassador oak').
card_rarity('ambassador oak'/'MOR', 'Common').
card_artist('ambassador oak'/'MOR', 'Steve Prescott').
card_number('ambassador oak'/'MOR', '113').
card_flavor_text('ambassador oak'/'MOR', 'Treefolk and elves share a common interest in the forests, but really the elves just feel at home with the view from above.').
card_multiverse_id('ambassador oak'/'MOR', '152998').

card_in_set('auntie\'s snitch', 'MOR').
card_original_type('auntie\'s snitch'/'MOR', 'Creature — Goblin Rogue').
card_original_text('auntie\'s snitch'/'MOR', 'Auntie\'s Snitch can\'t block.\nProwl {1}{B} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhenever a Goblin or Rogue you control deals combat damage to a player, if Auntie\'s Snitch is in your graveyard, you may return Auntie\'s Snitch to your hand.').
card_first_print('auntie\'s snitch', 'MOR').
card_image_name('auntie\'s snitch'/'MOR', 'auntie\'s snitch').
card_uid('auntie\'s snitch'/'MOR', 'MOR:Auntie\'s Snitch:auntie\'s snitch').
card_rarity('auntie\'s snitch'/'MOR', 'Rare').
card_artist('auntie\'s snitch'/'MOR', 'Warren Mahy').
card_number('auntie\'s snitch'/'MOR', '57').
card_multiverse_id('auntie\'s snitch'/'MOR', '153280').

card_in_set('ballyrush banneret', 'MOR').
card_original_type('ballyrush banneret'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('ballyrush banneret'/'MOR', 'Kithkin spells and Soldier spells you play cost {1} less to play.').
card_first_print('ballyrush banneret', 'MOR').
card_image_name('ballyrush banneret'/'MOR', 'ballyrush banneret').
card_uid('ballyrush banneret'/'MOR', 'MOR:Ballyrush Banneret:ballyrush banneret').
card_rarity('ballyrush banneret'/'MOR', 'Common').
card_artist('ballyrush banneret'/'MOR', 'Ralph Horsley').
card_number('ballyrush banneret'/'MOR', '1').
card_flavor_text('ballyrush banneret'/'MOR', 'Only wool from the side of the springjack turned most often to the sun can be woven into kithkin battle standards. Jackherds record every movement of their woolly-jacks until shearing.').
card_multiverse_id('ballyrush banneret'/'MOR', '152623').

card_in_set('battletide alchemist', 'MOR').
card_original_type('battletide alchemist'/'MOR', 'Creature — Kithkin Cleric').
card_original_text('battletide alchemist'/'MOR', 'If a source would deal damage to a player, you may prevent X of that damage, where X is the number of Clerics you control.').
card_first_print('battletide alchemist', 'MOR').
card_image_name('battletide alchemist'/'MOR', 'battletide alchemist').
card_uid('battletide alchemist'/'MOR', 'MOR:Battletide Alchemist:battletide alchemist').
card_rarity('battletide alchemist'/'MOR', 'Rare').
card_artist('battletide alchemist'/'MOR', 'Steve Prescott').
card_number('battletide alchemist'/'MOR', '2').
card_flavor_text('battletide alchemist'/'MOR', '\"I do not heal. Healing means that the pain has already been felt. To stop suffering, one must prevent the world\'s wounds altogether.\"').
card_multiverse_id('battletide alchemist'/'MOR', '153451').

card_in_set('bitterblossom', 'MOR').
card_original_type('bitterblossom'/'MOR', 'Tribal Enchantment — Faerie').
card_original_text('bitterblossom'/'MOR', 'At the beginning of your upkeep, you lose 1 life and put a 1/1 black Faerie Rogue creature token with flying into play.').
card_image_name('bitterblossom'/'MOR', 'bitterblossom').
card_uid('bitterblossom'/'MOR', 'MOR:Bitterblossom:bitterblossom').
card_rarity('bitterblossom'/'MOR', 'Rare').
card_artist('bitterblossom'/'MOR', 'Rebecca Guay').
card_number('bitterblossom'/'MOR', '58').
card_flavor_text('bitterblossom'/'MOR', 'In Lorwyn\'s brief evenings, the sun pauses at the horizon long enough for a certain species of violet to bloom with the fragrance of mischief.').
card_multiverse_id('bitterblossom'/'MOR', '152648').

card_in_set('blightsoil druid', 'MOR').
card_original_type('blightsoil druid'/'MOR', 'Creature — Elf Druid').
card_original_text('blightsoil druid'/'MOR', '{T}, Pay 1 life: Add {G} to your mana pool.').
card_first_print('blightsoil druid', 'MOR').
card_image_name('blightsoil druid'/'MOR', 'blightsoil druid').
card_uid('blightsoil druid'/'MOR', 'MOR:Blightsoil Druid:blightsoil druid').
card_rarity('blightsoil druid'/'MOR', 'Common').
card_artist('blightsoil druid'/'MOR', 'Nils Hamm').
card_number('blightsoil druid'/'MOR', '59').
card_flavor_text('blightsoil druid'/'MOR', '\"See the beauty in death: the clean white bones lying in the fertile soil and the brightly colored moonglove sprouting from the fell earth.\"').
card_multiverse_id('blightsoil druid'/'MOR', '152705').

card_in_set('boldwyr heavyweights', 'MOR').
card_original_type('boldwyr heavyweights'/'MOR', 'Creature — Giant Warrior').
card_original_text('boldwyr heavyweights'/'MOR', 'Trample\nWhen Boldwyr Heavyweights comes into play, each opponent may search his or her library for a creature card and put it into play. Then each player who searched his or her library this way shuffles it.').
card_first_print('boldwyr heavyweights', 'MOR').
card_image_name('boldwyr heavyweights'/'MOR', 'boldwyr heavyweights').
card_uid('boldwyr heavyweights'/'MOR', 'MOR:Boldwyr Heavyweights:boldwyr heavyweights').
card_rarity('boldwyr heavyweights'/'MOR', 'Rare').
card_artist('boldwyr heavyweights'/'MOR', 'Wayne Reynolds').
card_number('boldwyr heavyweights'/'MOR', '85').
card_flavor_text('boldwyr heavyweights'/'MOR', 'Even if giants bring nothing else on a journey, they bring attention.').
card_multiverse_id('boldwyr heavyweights'/'MOR', '153137').

card_in_set('boldwyr intimidator', 'MOR').
card_original_type('boldwyr intimidator'/'MOR', 'Creature — Giant Warrior').
card_original_text('boldwyr intimidator'/'MOR', 'Cowards can\'t block Warriors.\n{R}: Target creature becomes a Coward until end of turn.\n{2}{R}: Target creature becomes a Warrior until end of turn.').
card_image_name('boldwyr intimidator'/'MOR', 'boldwyr intimidator').
card_uid('boldwyr intimidator'/'MOR', 'MOR:Boldwyr Intimidator:boldwyr intimidator').
card_rarity('boldwyr intimidator'/'MOR', 'Uncommon').
card_artist('boldwyr intimidator'/'MOR', 'Christopher Moeller').
card_number('boldwyr intimidator'/'MOR', '86').
card_flavor_text('boldwyr intimidator'/'MOR', '\"Now everyone knows what you are.\"').
card_multiverse_id('boldwyr intimidator'/'MOR', '152974').

card_in_set('borderland behemoth', 'MOR').
card_original_type('borderland behemoth'/'MOR', 'Creature — Giant Warrior').
card_original_text('borderland behemoth'/'MOR', 'Trample\nBorderland Behemoth gets +4/+4 for each other Giant you control.').
card_first_print('borderland behemoth', 'MOR').
card_image_name('borderland behemoth'/'MOR', 'borderland behemoth').
card_uid('borderland behemoth'/'MOR', 'MOR:Borderland Behemoth:borderland behemoth').
card_rarity('borderland behemoth'/'MOR', 'Rare').
card_artist('borderland behemoth'/'MOR', 'Jesper Ejsing').
card_number('borderland behemoth'/'MOR', '87').
card_flavor_text('borderland behemoth'/'MOR', 'Giants share nothing but their fury.').
card_multiverse_id('borderland behemoth'/'MOR', '153102').

card_in_set('bosk banneret', 'MOR').
card_original_type('bosk banneret'/'MOR', 'Creature — Treefolk Shaman').
card_original_text('bosk banneret'/'MOR', 'Treefolk spells and Shaman spells you play cost {1} less to play.').
card_first_print('bosk banneret', 'MOR').
card_image_name('bosk banneret'/'MOR', 'bosk banneret').
card_uid('bosk banneret'/'MOR', 'MOR:Bosk Banneret:bosk banneret').
card_rarity('bosk banneret'/'MOR', 'Common').
card_artist('bosk banneret'/'MOR', 'Ralph Horsley').
card_number('bosk banneret'/'MOR', '114').
card_flavor_text('bosk banneret'/'MOR', 'Deep in a murmuring bosk, where the trees\' memories span centuries, the simple banner of green and copper signifies an elder whose rings number more than a faerie\'s days.').
card_multiverse_id('bosk banneret'/'MOR', '152557').

card_in_set('bramblewood paragon', 'MOR').
card_original_type('bramblewood paragon'/'MOR', 'Creature — Elf Warrior').
card_original_text('bramblewood paragon'/'MOR', 'Each other Warrior creature you control comes into play with an additional +1/+1 counter on it.\nEach creature you control with a +1/+1 counter on it has trample.').
card_image_name('bramblewood paragon'/'MOR', 'bramblewood paragon').
card_uid('bramblewood paragon'/'MOR', 'MOR:Bramblewood Paragon:bramblewood paragon').
card_rarity('bramblewood paragon'/'MOR', 'Uncommon').
card_artist('bramblewood paragon'/'MOR', 'Jim Murray').
card_number('bramblewood paragon'/'MOR', '115').
card_flavor_text('bramblewood paragon'/'MOR', 'Those who seek to escape her blades succeed only in dying on their stomachs.').
card_multiverse_id('bramblewood paragon'/'MOR', '153139').

card_in_set('brighthearth banneret', 'MOR').
card_original_type('brighthearth banneret'/'MOR', 'Creature — Elemental Warrior').
card_original_text('brighthearth banneret'/'MOR', 'Elemental spells and Warrior spells you play cost {1} less to play.\nReinforce 1—{1}{R} ({1}{R}, Discard this card: Put a +1/+1 counter on target creature.)').
card_first_print('brighthearth banneret', 'MOR').
card_image_name('brighthearth banneret'/'MOR', 'brighthearth banneret').
card_uid('brighthearth banneret'/'MOR', 'MOR:Brighthearth Banneret:brighthearth banneret').
card_rarity('brighthearth banneret'/'MOR', 'Common').
card_artist('brighthearth banneret'/'MOR', 'Ralph Horsley').
card_number('brighthearth banneret'/'MOR', '88').
card_flavor_text('brighthearth banneret'/'MOR', 'The banner symbolizes the goodwill of the Brighthearth. Their emissaries bring the mastery of fire to other races.').
card_multiverse_id('brighthearth banneret'/'MOR', '152651').

card_in_set('burrenton bombardier', 'MOR').
card_original_type('burrenton bombardier'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('burrenton bombardier'/'MOR', 'Flying\nReinforce 2—{2}{W} ({2}{W}, Discard this card: Put two +1/+1 counters on target creature.)').
card_first_print('burrenton bombardier', 'MOR').
card_image_name('burrenton bombardier'/'MOR', 'burrenton bombardier').
card_uid('burrenton bombardier'/'MOR', 'MOR:Burrenton Bombardier:burrenton bombardier').
card_rarity('burrenton bombardier'/'MOR', 'Common').
card_artist('burrenton bombardier'/'MOR', 'Ron Spencer').
card_number('burrenton bombardier'/'MOR', '3').
card_flavor_text('burrenton bombardier'/'MOR', 'The flasks provide ballast, a means of steering, and—with a little luck—a way to deliver ward-spells to cohorts below.').
card_multiverse_id('burrenton bombardier'/'MOR', '152694').

card_in_set('burrenton shield-bearers', 'MOR').
card_original_type('burrenton shield-bearers'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('burrenton shield-bearers'/'MOR', 'Whenever Burrenton Shield-Bearers attacks, target creature gets +0/+3 until end of turn.').
card_first_print('burrenton shield-bearers', 'MOR').
card_image_name('burrenton shield-bearers'/'MOR', 'burrenton shield-bearers').
card_uid('burrenton shield-bearers'/'MOR', 'MOR:Burrenton Shield-Bearers:burrenton shield-bearers').
card_rarity('burrenton shield-bearers'/'MOR', 'Common').
card_artist('burrenton shield-bearers'/'MOR', 'Daren Bader').
card_number('burrenton shield-bearers'/'MOR', '4').
card_flavor_text('burrenton shield-bearers'/'MOR', 'Soul and bones, kithkin are woven into their clans. They fight fiercely and without self-interest.').
card_multiverse_id('burrenton shield-bearers'/'MOR', '152668').

card_in_set('cenn\'s tactician', 'MOR').
card_original_type('cenn\'s tactician'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('cenn\'s tactician'/'MOR', '{W}, {T}: Put a +1/+1 counter on target Soldier creature.\nEach creature you control with a +1/+1 counter on it can block an additional creature.').
card_image_name('cenn\'s tactician'/'MOR', 'cenn\'s tactician').
card_uid('cenn\'s tactician'/'MOR', 'MOR:Cenn\'s Tactician:cenn\'s tactician').
card_rarity('cenn\'s tactician'/'MOR', 'Uncommon').
card_artist('cenn\'s tactician'/'MOR', 'Zoltan Boros & Gabor Szikszai').
card_number('cenn\'s tactician'/'MOR', '5').
card_flavor_text('cenn\'s tactician'/'MOR', 'The choke point was the plan, but devoted camaraderie was always her strategy.').
card_multiverse_id('cenn\'s tactician'/'MOR', '152878').

card_in_set('chameleon colossus', 'MOR').
card_original_type('chameleon colossus'/'MOR', 'Creature — Shapeshifter').
card_original_text('chameleon colossus'/'MOR', 'Changeling (This card is every creature type at all times.)\nProtection from black\n{2}{G}{G}: Chameleon Colossus gets +X/+X until end of turn, where X is its power.').
card_first_print('chameleon colossus', 'MOR').
card_image_name('chameleon colossus'/'MOR', 'chameleon colossus').
card_uid('chameleon colossus'/'MOR', 'MOR:Chameleon Colossus:chameleon colossus').
card_rarity('chameleon colossus'/'MOR', 'Rare').
card_artist('chameleon colossus'/'MOR', 'Darrell Riche').
card_number('chameleon colossus'/'MOR', '116').
card_multiverse_id('chameleon colossus'/'MOR', '153450').

card_in_set('changeling sentinel', 'MOR').
card_original_type('changeling sentinel'/'MOR', 'Creature — Shapeshifter').
card_original_text('changeling sentinel'/'MOR', 'Changeling (This card is every creature type at all times.)\nVigilance').
card_first_print('changeling sentinel', 'MOR').
card_image_name('changeling sentinel'/'MOR', 'changeling sentinel').
card_uid('changeling sentinel'/'MOR', 'MOR:Changeling Sentinel:changeling sentinel').
card_rarity('changeling sentinel'/'MOR', 'Common').
card_artist('changeling sentinel'/'MOR', 'Chuck Lukacs').
card_number('changeling sentinel'/'MOR', '6').
card_flavor_text('changeling sentinel'/'MOR', 'There\'s nothing more unsettling than locking blades with something that looks just like you.').
card_multiverse_id('changeling sentinel'/'MOR', '143230').

card_in_set('cloak and dagger', 'MOR').
card_original_type('cloak and dagger'/'MOR', 'Tribal Artifact — Rogue Equipment').
card_original_text('cloak and dagger'/'MOR', 'Equipped creature gets +2/+0 and has shroud. (It can\'t be the target of spells or abilities.)\nWhenever a Rogue creature comes into play, you may attach Cloak and Dagger to it.\nEquip {3}').
card_first_print('cloak and dagger', 'MOR').
card_image_name('cloak and dagger'/'MOR', 'cloak and dagger').
card_uid('cloak and dagger'/'MOR', 'MOR:Cloak and Dagger:cloak and dagger').
card_rarity('cloak and dagger'/'MOR', 'Uncommon').
card_artist('cloak and dagger'/'MOR', 'Daren Bader').
card_number('cloak and dagger'/'MOR', '141').
card_multiverse_id('cloak and dagger'/'MOR', '152604').

card_in_set('coordinated barrage', 'MOR').
card_original_type('coordinated barrage'/'MOR', 'Instant').
card_original_text('coordinated barrage'/'MOR', 'Choose a creature type. Coordinated Barrage deals damage to target attacking or blocking creature equal to the number of permanents you control of the chosen type.').
card_first_print('coordinated barrage', 'MOR').
card_image_name('coordinated barrage'/'MOR', 'coordinated barrage').
card_uid('coordinated barrage'/'MOR', 'MOR:Coordinated Barrage:coordinated barrage').
card_rarity('coordinated barrage'/'MOR', 'Common').
card_artist('coordinated barrage'/'MOR', 'Franz Vohwinkel').
card_number('coordinated barrage'/'MOR', '7').
card_multiverse_id('coordinated barrage'/'MOR', '153151').

card_in_set('countryside crusher', 'MOR').
card_original_type('countryside crusher'/'MOR', 'Creature — Giant Warrior').
card_original_text('countryside crusher'/'MOR', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a land card, put it into your graveyard and repeat this process.\nWhenever a land card is put into your graveyard from anywhere, put a +1/+1 counter on Countryside Crusher.').
card_first_print('countryside crusher', 'MOR').
card_image_name('countryside crusher'/'MOR', 'countryside crusher').
card_uid('countryside crusher'/'MOR', 'MOR:Countryside Crusher:countryside crusher').
card_rarity('countryside crusher'/'MOR', 'Rare').
card_artist('countryside crusher'/'MOR', 'Brian Snõddy').
card_number('countryside crusher'/'MOR', '89').
card_multiverse_id('countryside crusher'/'MOR', '152980').

card_in_set('cream of the crop', 'MOR').
card_original_type('cream of the crop'/'MOR', 'Enchantment').
card_original_text('cream of the crop'/'MOR', 'Whenever a creature comes into play under your control, you may look at the top X cards of your library, where X is that creature\'s power. If you do, put one of those cards on top of your library and the rest on the bottom of your library in any order.').
card_first_print('cream of the crop', 'MOR').
card_image_name('cream of the crop'/'MOR', 'cream of the crop').
card_uid('cream of the crop'/'MOR', 'MOR:Cream of the Crop:cream of the crop').
card_rarity('cream of the crop'/'MOR', 'Rare').
card_artist('cream of the crop'/'MOR', 'Howard Lyon').
card_number('cream of the crop'/'MOR', '117').
card_multiverse_id('cream of the crop'/'MOR', '157423').

card_in_set('daily regimen', 'MOR').
card_original_type('daily regimen'/'MOR', 'Enchantment — Aura').
card_original_text('daily regimen'/'MOR', 'Enchant creature\n{1}{W}: Put a +1/+1 counter on enchanted creature.').
card_first_print('daily regimen', 'MOR').
card_image_name('daily regimen'/'MOR', 'daily regimen').
card_uid('daily regimen'/'MOR', 'MOR:Daily Regimen:daily regimen').
card_rarity('daily regimen'/'MOR', 'Uncommon').
card_artist('daily regimen'/'MOR', 'Warren Mahy').
card_number('daily regimen'/'MOR', '8').
card_flavor_text('daily regimen'/'MOR', 'What self-indulgence tears down, discipline builds up again.').
card_multiverse_id('daily regimen'/'MOR', '152838').

card_in_set('declaration of naught', 'MOR').
card_original_type('declaration of naught'/'MOR', 'Enchantment').
card_original_text('declaration of naught'/'MOR', 'As Declaration of Naught comes into play, name a card.\n{U}: Counter target spell with that name.').
card_first_print('declaration of naught', 'MOR').
card_image_name('declaration of naught'/'MOR', 'declaration of naught').
card_uid('declaration of naught'/'MOR', 'MOR:Declaration of Naught:declaration of naught').
card_rarity('declaration of naught'/'MOR', 'Rare').
card_artist('declaration of naught'/'MOR', 'Rob Alexander').
card_number('declaration of naught'/'MOR', '29').
card_flavor_text('declaration of naught'/'MOR', 'Every story, even a faerie tale, comes to an end.').
card_multiverse_id('declaration of naught'/'MOR', '153169').

card_in_set('deglamer', 'MOR').
card_original_type('deglamer'/'MOR', 'Instant').
card_original_text('deglamer'/'MOR', 'Choose target artifact or enchantment. Its owner shuffles it into his or her library.').
card_first_print('deglamer', 'MOR').
card_image_name('deglamer'/'MOR', 'deglamer').
card_uid('deglamer'/'MOR', 'MOR:Deglamer:deglamer').
card_rarity('deglamer'/'MOR', 'Common').
card_artist('deglamer'/'MOR', 'Zoltan Boros & Gabor Szikszai').
card_number('deglamer'/'MOR', '118').
card_flavor_text('deglamer'/'MOR', 'The more pleasant an illusion\'s garb, the more indecent its form laid bare.').
card_multiverse_id('deglamer'/'MOR', '154160').

card_in_set('dewdrop spy', 'MOR').
card_original_type('dewdrop spy'/'MOR', 'Creature — Faerie Rogue').
card_original_text('dewdrop spy'/'MOR', 'Flash\nFlying\nWhen Dewdrop Spy comes into play, look at the top card of target player\'s library.').
card_first_print('dewdrop spy', 'MOR').
card_image_name('dewdrop spy'/'MOR', 'dewdrop spy').
card_uid('dewdrop spy'/'MOR', 'MOR:Dewdrop Spy:dewdrop spy').
card_rarity('dewdrop spy'/'MOR', 'Common').
card_artist('dewdrop spy'/'MOR', 'Wayne England').
card_number('dewdrop spy'/'MOR', '30').
card_flavor_text('dewdrop spy'/'MOR', 'Only in dew from Oona herself is there clarity enough to see a victim\'s mind.').
card_multiverse_id('dewdrop spy'/'MOR', '152828').

card_in_set('disperse', 'MOR').
card_original_type('disperse'/'MOR', 'Instant').
card_original_text('disperse'/'MOR', 'Return target nonland permanent to its owner\'s hand.').
card_first_print('disperse', 'MOR').
card_image_name('disperse'/'MOR', 'disperse').
card_uid('disperse'/'MOR', 'MOR:Disperse:disperse').
card_rarity('disperse'/'MOR', 'Common').
card_artist('disperse'/'MOR', 'Steve Ellis').
card_number('disperse'/'MOR', '31').
card_flavor_text('disperse'/'MOR', 'Gryffid scowled at the sky. A perfect day for the hunt tainted by clouds. He wished them gone. High above, the clouds looked down, scowled, and made a wish of their own.').
card_multiverse_id('disperse'/'MOR', '152727').

card_in_set('distant melody', 'MOR').
card_original_type('distant melody'/'MOR', 'Sorcery').
card_original_text('distant melody'/'MOR', 'Choose a creature type. Draw a card for each permanent you control of that type.').
card_first_print('distant melody', 'MOR').
card_image_name('distant melody'/'MOR', 'distant melody').
card_uid('distant melody'/'MOR', 'MOR:Distant Melody:distant melody').
card_rarity('distant melody'/'MOR', 'Common').
card_artist('distant melody'/'MOR', 'Omar Rayyan').
card_number('distant melody'/'MOR', '32').
card_flavor_text('distant melody'/'MOR', 'Oona\'s song is like a twisted dinner chime. All the faeries return home, but it is Oona who feasts—on the stolen dreams and rumors they serve her.').
card_multiverse_id('distant melody'/'MOR', '153133').

card_in_set('diviner\'s wand', 'MOR').
card_original_type('diviner\'s wand'/'MOR', 'Tribal Artifact — Wizard Equipment').
card_original_text('diviner\'s wand'/'MOR', 'Equipped creature has \"Whenever you draw a card, this creature gets +1/+1 and gains flying until end of turn\" and \"{4}: Draw a card.\"\nWhenever a Wizard creature comes into play, you may attach Diviner\'s Wand to it.\nEquip {3}').
card_first_print('diviner\'s wand', 'MOR').
card_image_name('diviner\'s wand'/'MOR', 'diviner\'s wand').
card_uid('diviner\'s wand'/'MOR', 'MOR:Diviner\'s Wand:diviner\'s wand').
card_rarity('diviner\'s wand'/'MOR', 'Uncommon').
card_artist('diviner\'s wand'/'MOR', 'Wayne England').
card_number('diviner\'s wand'/'MOR', '142').
card_multiverse_id('diviner\'s wand'/'MOR', '154163').

card_in_set('door of destinies', 'MOR').
card_original_type('door of destinies'/'MOR', 'Artifact').
card_original_text('door of destinies'/'MOR', 'As Door of Destinies comes into play, choose a creature type.\nWhenever you play a spell of that type, put a charge counter on Door of Destinies.\nCreatures you control of that type get +1/+1 for each charge counter on Door of Destinies.').
card_image_name('door of destinies'/'MOR', 'door of destinies').
card_uid('door of destinies'/'MOR', 'MOR:Door of Destinies:door of destinies').
card_rarity('door of destinies'/'MOR', 'Rare').
card_artist('door of destinies'/'MOR', 'Larry MacDougall').
card_number('door of destinies'/'MOR', '143').
card_multiverse_id('door of destinies'/'MOR', '152526').

card_in_set('earthbrawn', 'MOR').
card_original_type('earthbrawn'/'MOR', 'Instant').
card_original_text('earthbrawn'/'MOR', 'Target creature gets +3/+3 until end of turn.\nReinforce 1—{1}{G} ({1}{G}, Discard this card: Put a +1/+1 counter on target creature.)').
card_first_print('earthbrawn', 'MOR').
card_image_name('earthbrawn'/'MOR', 'earthbrawn').
card_uid('earthbrawn'/'MOR', 'MOR:Earthbrawn:earthbrawn').
card_rarity('earthbrawn'/'MOR', 'Common').
card_artist('earthbrawn'/'MOR', 'Kev Walker').
card_number('earthbrawn'/'MOR', '119').
card_flavor_text('earthbrawn'/'MOR', 'Gremil finally felt in touch with nature—and felt a need to share the experience with others.').
card_multiverse_id('earthbrawn'/'MOR', '152710').

card_in_set('earwig squad', 'MOR').
card_original_type('earwig squad'/'MOR', 'Creature — Goblin Rogue').
card_original_text('earwig squad'/'MOR', 'Prowl {2}{B} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhen Earwig Squad comes into play, if its prowl cost was paid, search target opponent\'s library for three cards and remove them from the game. Then that player shuffles his or her library.').
card_image_name('earwig squad'/'MOR', 'earwig squad').
card_uid('earwig squad'/'MOR', 'MOR:Earwig Squad:earwig squad').
card_rarity('earwig squad'/'MOR', 'Rare').
card_artist('earwig squad'/'MOR', 'Warren Mahy').
card_number('earwig squad'/'MOR', '60').
card_multiverse_id('earwig squad'/'MOR', '153135').

card_in_set('elvish warrior', 'MOR').
card_original_type('elvish warrior'/'MOR', 'Creature — Elf Warrior').
card_original_text('elvish warrior'/'MOR', '').
card_image_name('elvish warrior'/'MOR', 'elvish warrior').
card_uid('elvish warrior'/'MOR', 'MOR:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'MOR', 'Common').
card_artist('elvish warrior'/'MOR', 'Kev Walker').
card_number('elvish warrior'/'MOR', '120').
card_flavor_text('elvish warrior'/'MOR', 'As graceful as a deer leaping a stream and as deadly as the wolf waiting in ambush on the other side, elvish warriors are the eyes of the forest as well as its unsheathed claws.').
card_multiverse_id('elvish warrior'/'MOR', '153003').

card_in_set('everbark shaman', 'MOR').
card_original_type('everbark shaman'/'MOR', 'Creature — Treefolk Shaman').
card_original_text('everbark shaman'/'MOR', '{T}, Remove a Treefolk card in your graveyard from the game: Search your library for two Forest cards and put them into play tapped. Then shuffle your library.').
card_first_print('everbark shaman', 'MOR').
card_image_name('everbark shaman'/'MOR', 'everbark shaman').
card_uid('everbark shaman'/'MOR', 'MOR:Everbark Shaman:everbark shaman').
card_rarity('everbark shaman'/'MOR', 'Common').
card_artist('everbark shaman'/'MOR', 'Larry MacDougall').
card_number('everbark shaman'/'MOR', '121').
card_flavor_text('everbark shaman'/'MOR', '\"May you rise again, from seed to sapling to sentience.\"').
card_multiverse_id('everbark shaman'/'MOR', '152968').

card_in_set('fencer clique', 'MOR').
card_original_type('fencer clique'/'MOR', 'Creature — Faerie Soldier').
card_original_text('fencer clique'/'MOR', 'Flying\n{U}: Put Fencer Clique on top of its owner\'s library.').
card_first_print('fencer clique', 'MOR').
card_image_name('fencer clique'/'MOR', 'fencer clique').
card_uid('fencer clique'/'MOR', 'MOR:Fencer Clique:fencer clique').
card_rarity('fencer clique'/'MOR', 'Common').
card_artist('fencer clique'/'MOR', 'William O\'Connor').
card_number('fencer clique'/'MOR', '33').
card_flavor_text('fencer clique'/'MOR', 'You may as well try to swat a faerie\n—Flamekin expression meaning\n\"that\'s impossible\"').
card_multiverse_id('fencer clique'/'MOR', '157424').

card_in_set('fendeep summoner', 'MOR').
card_original_type('fendeep summoner'/'MOR', 'Creature — Treefolk Shaman').
card_original_text('fendeep summoner'/'MOR', '{T}: Up to two target Swamps each become 3/5 Treefolk Warrior creatures in addition to their other types until end of turn.').
card_first_print('fendeep summoner', 'MOR').
card_image_name('fendeep summoner'/'MOR', 'fendeep summoner').
card_uid('fendeep summoner'/'MOR', 'MOR:Fendeep Summoner:fendeep summoner').
card_rarity('fendeep summoner'/'MOR', 'Rare').
card_artist('fendeep summoner'/'MOR', 'Jesper Ejsing').
card_number('fendeep summoner'/'MOR', '61').
card_flavor_text('fendeep summoner'/'MOR', '\"What is drowned is not forgotten. I will fulfill my oath. Come back to me, my brethren, as I have come back for you.\"').
card_multiverse_id('fendeep summoner'/'MOR', '152607').

card_in_set('fertilid', 'MOR').
card_original_type('fertilid'/'MOR', 'Creature — Elemental').
card_original_text('fertilid'/'MOR', 'Fertilid comes into play with two +1/+1 counters on it.\n{1}{G}, Remove a +1/+1 counter from Fertilid: Target player searches his or her library for a basic land card and puts it into play tapped. Then that player shuffles his or her library.').
card_first_print('fertilid', 'MOR').
card_image_name('fertilid'/'MOR', 'fertilid').
card_uid('fertilid'/'MOR', 'MOR:Fertilid:fertilid').
card_rarity('fertilid'/'MOR', 'Common').
card_artist('fertilid'/'MOR', 'Wayne Reynolds').
card_number('fertilid'/'MOR', '122').
card_multiverse_id('fertilid'/'MOR', '152926').

card_in_set('festercreep', 'MOR').
card_original_type('festercreep'/'MOR', 'Creature — Elemental').
card_original_text('festercreep'/'MOR', 'Festercreep comes into play with a +1/+1 counter on it.\n{1}{B}, Remove a +1/+1 counter from Festercreep: All other creatures get -1/-1 until end of turn.').
card_first_print('festercreep', 'MOR').
card_image_name('festercreep'/'MOR', 'festercreep').
card_uid('festercreep'/'MOR', 'MOR:Festercreep:festercreep').
card_rarity('festercreep'/'MOR', 'Common').
card_artist('festercreep'/'MOR', 'Jeff Easley').
card_number('festercreep'/'MOR', '62').
card_flavor_text('festercreep'/'MOR', 'A single festercreep isn\'t alone. It\'s already an infestation.').
card_multiverse_id('festercreep'/'MOR', '152944').

card_in_set('feudkiller\'s verdict', 'MOR').
card_original_type('feudkiller\'s verdict'/'MOR', 'Tribal Sorcery — Giant').
card_original_text('feudkiller\'s verdict'/'MOR', 'You gain 10 life. Then if you have more life than an opponent, put a 5/5 white Giant Warrior creature token into play.').
card_first_print('feudkiller\'s verdict', 'MOR').
card_image_name('feudkiller\'s verdict'/'MOR', 'feudkiller\'s verdict').
card_uid('feudkiller\'s verdict'/'MOR', 'MOR:Feudkiller\'s Verdict:feudkiller\'s verdict').
card_rarity('feudkiller\'s verdict'/'MOR', 'Rare').
card_artist('feudkiller\'s verdict'/'MOR', 'Dan Scott').
card_number('feudkiller\'s verdict'/'MOR', '9').
card_flavor_text('feudkiller\'s verdict'/'MOR', '\"There are all kinds of strengths, but if you have strength of soul, the others will follow.\"\n—Galanda Feudkiller').
card_multiverse_id('feudkiller\'s verdict'/'MOR', '152649').

card_in_set('final-sting faerie', 'MOR').
card_original_type('final-sting faerie'/'MOR', 'Creature — Faerie Assassin').
card_original_text('final-sting faerie'/'MOR', 'Flying\nWhen Final-Sting Faerie comes into play, destroy target creature that was dealt damage this turn.').
card_first_print('final-sting faerie', 'MOR').
card_image_name('final-sting faerie'/'MOR', 'final-sting faerie').
card_uid('final-sting faerie'/'MOR', 'MOR:Final-Sting Faerie:final-sting faerie').
card_rarity('final-sting faerie'/'MOR', 'Common').
card_artist('final-sting faerie'/'MOR', 'Wayne Reynolds').
card_number('final-sting faerie'/'MOR', '63').
card_flavor_text('final-sting faerie'/'MOR', 'Many heroes\' last sight is a grin at the other end of a rapier.').
card_multiverse_id('final-sting faerie'/'MOR', '152641').

card_in_set('fire juggler', 'MOR').
card_original_type('fire juggler'/'MOR', 'Creature — Goblin Shaman').
card_original_text('fire juggler'/'MOR', 'Whenever Fire Juggler becomes blocked, clash with an opponent. If you win, Fire Juggler deals 4 damage to each creature blocking it. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('fire juggler', 'MOR').
card_image_name('fire juggler'/'MOR', 'fire juggler').
card_uid('fire juggler'/'MOR', 'MOR:Fire Juggler:fire juggler').
card_rarity('fire juggler'/'MOR', 'Common').
card_artist('fire juggler'/'MOR', 'Thomas Denmark').
card_number('fire juggler'/'MOR', '90').
card_multiverse_id('fire juggler'/'MOR', '152534').

card_in_set('floodchaser', 'MOR').
card_original_type('floodchaser'/'MOR', 'Creature — Elemental').
card_original_text('floodchaser'/'MOR', 'Floodchaser comes into play with six +1/+1 counters on it.\nFloodchaser can\'t attack unless defending player controls an Island.\n{U}, Remove a +1/+1 counter from Floodchaser: Target land becomes an Island until end of turn.').
card_first_print('floodchaser', 'MOR').
card_image_name('floodchaser'/'MOR', 'floodchaser').
card_uid('floodchaser'/'MOR', 'MOR:Floodchaser:floodchaser').
card_rarity('floodchaser'/'MOR', 'Common').
card_artist('floodchaser'/'MOR', 'Eric Fortune').
card_number('floodchaser'/'MOR', '34').
card_multiverse_id('floodchaser'/'MOR', '152830').

card_in_set('forfend', 'MOR').
card_original_type('forfend'/'MOR', 'Instant').
card_original_text('forfend'/'MOR', 'Prevent all damage that would be dealt to creatures this turn.').
card_first_print('forfend', 'MOR').
card_image_name('forfend'/'MOR', 'forfend').
card_uid('forfend'/'MOR', 'MOR:Forfend:forfend').
card_rarity('forfend'/'MOR', 'Common').
card_artist('forfend'/'MOR', 'Franz Vohwinkel').
card_number('forfend'/'MOR', '10').
card_flavor_text('forfend'/'MOR', '\"Suddenly there stood a fortress protecting the clachan, its walls hewn of valor and mortared with honor.\"\n—Clachan Tales').
card_multiverse_id('forfend'/'MOR', '157431').

card_in_set('frogtosser banneret', 'MOR').
card_original_type('frogtosser banneret'/'MOR', 'Creature — Goblin Rogue').
card_original_text('frogtosser banneret'/'MOR', 'Haste\nGoblin spells and Rogue spells you play cost {1} less to play.').
card_first_print('frogtosser banneret', 'MOR').
card_image_name('frogtosser banneret'/'MOR', 'frogtosser banneret').
card_uid('frogtosser banneret'/'MOR', 'MOR:Frogtosser Banneret:frogtosser banneret').
card_rarity('frogtosser banneret'/'MOR', 'Common').
card_artist('frogtosser banneret'/'MOR', 'Ralph Horsley').
card_number('frogtosser banneret'/'MOR', '64').
card_flavor_text('frogtosser banneret'/'MOR', 'The Frogtossers thread feathers, bones, and trophies from past raids into their standards, believing they fuel the warren with the anger of its victims.').
card_multiverse_id('frogtosser banneret'/'MOR', '152587').

card_in_set('game-trail changeling', 'MOR').
card_original_type('game-trail changeling'/'MOR', 'Creature — Shapeshifter').
card_original_text('game-trail changeling'/'MOR', 'Changeling (This card is every creature type at all times.)\nTrample').
card_first_print('game-trail changeling', 'MOR').
card_image_name('game-trail changeling'/'MOR', 'game-trail changeling').
card_uid('game-trail changeling'/'MOR', 'MOR:Game-Trail Changeling:game-trail changeling').
card_rarity('game-trail changeling'/'MOR', 'Common').
card_artist('game-trail changeling'/'MOR', 'Martina Pilcerova').
card_number('game-trail changeling'/'MOR', '123').
card_flavor_text('game-trail changeling'/'MOR', '\"I pity them, never knowing the pleasures of a single familiar form, but at least they find a noble shape at times.\"\n—Desmera, perfect of Wren\'s Run').
card_multiverse_id('game-trail changeling'/'MOR', '143253').

card_in_set('gilt-leaf archdruid', 'MOR').
card_original_type('gilt-leaf archdruid'/'MOR', 'Creature — Elf Druid').
card_original_text('gilt-leaf archdruid'/'MOR', 'Whenever you play a Druid spell, you may draw a card.\nTap seven untapped Druids you control: Gain control of all lands target player controls.').
card_first_print('gilt-leaf archdruid', 'MOR').
card_image_name('gilt-leaf archdruid'/'MOR', 'gilt-leaf archdruid').
card_uid('gilt-leaf archdruid'/'MOR', 'MOR:Gilt-Leaf Archdruid:gilt-leaf archdruid').
card_rarity('gilt-leaf archdruid'/'MOR', 'Rare').
card_artist('gilt-leaf archdruid'/'MOR', 'Steve Prescott').
card_number('gilt-leaf archdruid'/'MOR', '124').
card_flavor_text('gilt-leaf archdruid'/'MOR', '\"In our hands, the natural world finds more beauty than nature alone could ever provide.\"').
card_multiverse_id('gilt-leaf archdruid'/'MOR', '153453').

card_in_set('graceful reprieve', 'MOR').
card_original_type('graceful reprieve'/'MOR', 'Instant').
card_original_text('graceful reprieve'/'MOR', 'When target creature is put into a graveyard this turn, return that card to play under its owner\'s control.').
card_first_print('graceful reprieve', 'MOR').
card_image_name('graceful reprieve'/'MOR', 'graceful reprieve').
card_uid('graceful reprieve'/'MOR', 'MOR:Graceful Reprieve:graceful reprieve').
card_rarity('graceful reprieve'/'MOR', 'Uncommon').
card_artist('graceful reprieve'/'MOR', 'William O\'Connor').
card_number('graceful reprieve'/'MOR', '11').
card_flavor_text('graceful reprieve'/'MOR', '\"In my moment of death, I had a vision of the world, dark and unwelcoming. I wanted to hide in the deepest meander, but the light pulled me back.\"').
card_multiverse_id('graceful reprieve'/'MOR', '153138').

card_in_set('greatbow doyen', 'MOR').
card_original_type('greatbow doyen'/'MOR', 'Creature — Elf Archer').
card_original_text('greatbow doyen'/'MOR', 'Other Archer creatures you control get +1/+1.\nWhenever an Archer you control deals damage to a creature, that Archer deals that much damage to that creature\'s controller.').
card_first_print('greatbow doyen', 'MOR').
card_image_name('greatbow doyen'/'MOR', 'greatbow doyen').
card_uid('greatbow doyen'/'MOR', 'MOR:Greatbow Doyen:greatbow doyen').
card_rarity('greatbow doyen'/'MOR', 'Rare').
card_artist('greatbow doyen'/'MOR', 'Steven Belledin').
card_number('greatbow doyen'/'MOR', '125').
card_flavor_text('greatbow doyen'/'MOR', '\"My arrows will ever find you.\"').
card_multiverse_id('greatbow doyen'/'MOR', '153163').

card_in_set('grimoire thief', 'MOR').
card_original_type('grimoire thief'/'MOR', 'Creature — Merfolk Rogue').
card_original_text('grimoire thief'/'MOR', 'Whenever Grimoire Thief becomes tapped, remove the top three cards of target opponent\'s library from the game face down.\nYou may look at cards removed from the game with Grimoire Thief.\n{U}, Sacrifice Grimoire Thief: Turn all cards removed from the game with Grimoire Thief face up. Counter all spells with those names.').
card_first_print('grimoire thief', 'MOR').
card_image_name('grimoire thief'/'MOR', 'grimoire thief').
card_uid('grimoire thief'/'MOR', 'MOR:Grimoire Thief:grimoire thief').
card_rarity('grimoire thief'/'MOR', 'Rare').
card_artist('grimoire thief'/'MOR', 'Randy Gallegos').
card_number('grimoire thief'/'MOR', '35').
card_multiverse_id('grimoire thief'/'MOR', '152870').

card_in_set('heritage druid', 'MOR').
card_original_type('heritage druid'/'MOR', 'Creature — Elf Druid').
card_original_text('heritage druid'/'MOR', 'Tap three untapped Elves you control: Add {G}{G}{G} to your mana pool.').
card_first_print('heritage druid', 'MOR').
card_image_name('heritage druid'/'MOR', 'heritage druid').
card_uid('heritage druid'/'MOR', 'MOR:Heritage Druid:heritage druid').
card_rarity('heritage druid'/'MOR', 'Uncommon').
card_artist('heritage druid'/'MOR', 'Larry MacDougall').
card_number('heritage druid'/'MOR', '126').
card_flavor_text('heritage druid'/'MOR', '\"Study the great spirits of our age and those of the past. Prune secrets from the branching traceries of our lineage.\"\n—Twila, Gilt-Leaf archdruid').
card_multiverse_id('heritage druid'/'MOR', '152556').

card_in_set('hostile realm', 'MOR').
card_original_type('hostile realm'/'MOR', 'Enchantment — Aura').
card_original_text('hostile realm'/'MOR', 'Enchant land\nEnchanted land has \"{T}: Target creature can\'t block this turn.\"').
card_first_print('hostile realm', 'MOR').
card_image_name('hostile realm'/'MOR', 'hostile realm').
card_uid('hostile realm'/'MOR', 'MOR:Hostile Realm:hostile realm').
card_rarity('hostile realm'/'MOR', 'Common').
card_artist('hostile realm'/'MOR', 'John Avon').
card_number('hostile realm'/'MOR', '91').
card_flavor_text('hostile realm'/'MOR', 'Lorwyn\'s crust explodes with power when the greater elementals are born. The flamekin use the fallout as cover for forays into enemy territory.').
card_multiverse_id('hostile realm'/'MOR', '152555').

card_in_set('hunting triad', 'MOR').
card_original_type('hunting triad'/'MOR', 'Tribal Sorcery — Elf').
card_original_text('hunting triad'/'MOR', 'Put three 1/1 green Elf Warrior creature tokens into play.\nReinforce 3—{3}{G} ({3}{G}, Discard this card: Put three +1/+1 counters on target creature.)').
card_first_print('hunting triad', 'MOR').
card_image_name('hunting triad'/'MOR', 'hunting triad').
card_uid('hunting triad'/'MOR', 'MOR:Hunting Triad:hunting triad').
card_rarity('hunting triad'/'MOR', 'Uncommon').
card_artist('hunting triad'/'MOR', 'Jim Nelson').
card_number('hunting triad'/'MOR', '127').
card_flavor_text('hunting triad'/'MOR', '\"Eyeblights are easy to track. Just follow the imperfections.\"').
card_multiverse_id('hunting triad'/'MOR', '152963').

card_in_set('idyllic tutor', 'MOR').
card_original_type('idyllic tutor'/'MOR', 'Sorcery').
card_original_text('idyllic tutor'/'MOR', 'Search your library for an enchantment card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('idyllic tutor', 'MOR').
card_image_name('idyllic tutor'/'MOR', 'idyllic tutor').
card_uid('idyllic tutor'/'MOR', 'MOR:Idyllic Tutor:idyllic tutor').
card_rarity('idyllic tutor'/'MOR', 'Rare').
card_artist('idyllic tutor'/'MOR', 'Howard Lyon').
card_number('idyllic tutor'/'MOR', '12').
card_flavor_text('idyllic tutor'/'MOR', '\"If one\'s life is blessed, solutions to all life\'s problems will appear at the right moment.\"\n—The Book of Kith and Kin').
card_multiverse_id('idyllic tutor'/'MOR', '152938').

card_in_set('indomitable ancients', 'MOR').
card_original_type('indomitable ancients'/'MOR', 'Creature — Treefolk Warrior').
card_original_text('indomitable ancients'/'MOR', '').
card_first_print('indomitable ancients', 'MOR').
card_image_name('indomitable ancients'/'MOR', 'indomitable ancients').
card_uid('indomitable ancients'/'MOR', 'MOR:Indomitable Ancients:indomitable ancients').
card_rarity('indomitable ancients'/'MOR', 'Rare').
card_artist('indomitable ancients'/'MOR', 'Pete Venters').
card_number('indomitable ancients'/'MOR', '13').
card_flavor_text('indomitable ancients'/'MOR', '\"Odum and Broadbark were the only beings mighty enough to challenge the giant Moran the Destroyer. Their battle lasted a hundred dawns, until Moran became so exhausted that he fell into namesleep. He awoke as Moran the Gardener.\"\n—The Tale of Odum and Broadbark').
card_multiverse_id('indomitable ancients'/'MOR', '152967').

card_in_set('ink dissolver', 'MOR').
card_original_type('ink dissolver'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('ink dissolver'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Ink Dissolver, you may reveal it. If you do, each opponent puts the top three cards of his or her library into his or her graveyard.').
card_first_print('ink dissolver', 'MOR').
card_image_name('ink dissolver'/'MOR', 'ink dissolver').
card_uid('ink dissolver'/'MOR', 'MOR:Ink Dissolver:ink dissolver').
card_rarity('ink dissolver'/'MOR', 'Common').
card_artist('ink dissolver'/'MOR', 'Brandon Dorman').
card_number('ink dissolver'/'MOR', '36').
card_multiverse_id('ink dissolver'/'MOR', '152736').

card_in_set('inspired sprite', 'MOR').
card_original_type('inspired sprite'/'MOR', 'Creature — Faerie Wizard').
card_original_text('inspired sprite'/'MOR', 'Flash\nFlying\nWhenever you play a Wizard spell, you may untap Inspired Sprite.\n{T}: Draw a card, then discard a card.').
card_first_print('inspired sprite', 'MOR').
card_image_name('inspired sprite'/'MOR', 'inspired sprite').
card_uid('inspired sprite'/'MOR', 'MOR:Inspired Sprite:inspired sprite').
card_rarity('inspired sprite'/'MOR', 'Uncommon').
card_artist('inspired sprite'/'MOR', 'Michael Sutfin').
card_number('inspired sprite'/'MOR', '37').
card_multiverse_id('inspired sprite'/'MOR', '152826').

card_in_set('kindled fury', 'MOR').
card_original_type('kindled fury'/'MOR', 'Instant').
card_original_text('kindled fury'/'MOR', 'Target creature gets +1/+0 and gains first strike until end of turn.').
card_first_print('kindled fury', 'MOR').
card_image_name('kindled fury'/'MOR', 'kindled fury').
card_uid('kindled fury'/'MOR', 'MOR:Kindled Fury:kindled fury').
card_rarity('kindled fury'/'MOR', 'Common').
card_artist('kindled fury'/'MOR', 'Shelly Wan').
card_number('kindled fury'/'MOR', '92').
card_flavor_text('kindled fury'/'MOR', '\"All beings carry the fire inside them. The challenge is to unleash it before they dwindle into oblivion.\"\n—Illulia, flamekin soulstoke').
card_multiverse_id('kindled fury'/'MOR', '152563').

card_in_set('kinsbaile borderguard', 'MOR').
card_original_type('kinsbaile borderguard'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('kinsbaile borderguard'/'MOR', 'Kinsbaile Borderguard comes into play with a +1/+1 counter on it for each other Kithkin you control.\nWhen Kinsbaile Borderguard is put into a graveyard from play, put a 1/1 white Kithkin Soldier creature token into play for each counter on it.').
card_first_print('kinsbaile borderguard', 'MOR').
card_image_name('kinsbaile borderguard'/'MOR', 'kinsbaile borderguard').
card_uid('kinsbaile borderguard'/'MOR', 'MOR:Kinsbaile Borderguard:kinsbaile borderguard').
card_rarity('kinsbaile borderguard'/'MOR', 'Rare').
card_artist('kinsbaile borderguard'/'MOR', 'Christopher Moeller').
card_number('kinsbaile borderguard'/'MOR', '14').
card_multiverse_id('kinsbaile borderguard'/'MOR', '152977').

card_in_set('kinsbaile cavalier', 'MOR').
card_original_type('kinsbaile cavalier'/'MOR', 'Creature — Kithkin Knight').
card_original_text('kinsbaile cavalier'/'MOR', 'Knight creatures you control have double strike.').
card_first_print('kinsbaile cavalier', 'MOR').
card_image_name('kinsbaile cavalier'/'MOR', 'kinsbaile cavalier').
card_uid('kinsbaile cavalier'/'MOR', 'MOR:Kinsbaile Cavalier:kinsbaile cavalier').
card_rarity('kinsbaile cavalier'/'MOR', 'Rare').
card_artist('kinsbaile cavalier'/'MOR', 'Wayne Reynolds').
card_number('kinsbaile cavalier'/'MOR', '15').
card_flavor_text('kinsbaile cavalier'/'MOR', 'Ambidexterity is common among kithkin. The thoughtweft links the minds of the left- and right-handed, giving each the knack of the other.').
card_multiverse_id('kinsbaile cavalier'/'MOR', '153279').

card_in_set('kithkin zephyrnaut', 'MOR').
card_original_type('kithkin zephyrnaut'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('kithkin zephyrnaut'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Kithkin Zephyrnaut, you may reveal it. If you do, Kithkin Zephyrnaut gets +2/+2 and gains flying and vigilance until end of turn.').
card_first_print('kithkin zephyrnaut', 'MOR').
card_image_name('kithkin zephyrnaut'/'MOR', 'kithkin zephyrnaut').
card_uid('kithkin zephyrnaut'/'MOR', 'MOR:Kithkin Zephyrnaut:kithkin zephyrnaut').
card_rarity('kithkin zephyrnaut'/'MOR', 'Common').
card_artist('kithkin zephyrnaut'/'MOR', 'Quinton Hoover & Val Mayerik').
card_number('kithkin zephyrnaut'/'MOR', '16').
card_multiverse_id('kithkin zephyrnaut'/'MOR', '152858').

card_in_set('knowledge exploitation', 'MOR').
card_original_type('knowledge exploitation'/'MOR', 'Tribal Sorcery — Rogue').
card_original_text('knowledge exploitation'/'MOR', 'Prowl {3}{U} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nSearch target opponent\'s library for an instant or sorcery card. You may play that card without paying its mana cost. Then that player shuffles his or her library.').
card_first_print('knowledge exploitation', 'MOR').
card_image_name('knowledge exploitation'/'MOR', 'knowledge exploitation').
card_uid('knowledge exploitation'/'MOR', 'MOR:Knowledge Exploitation:knowledge exploitation').
card_rarity('knowledge exploitation'/'MOR', 'Rare').
card_artist('knowledge exploitation'/'MOR', 'Darrell Riche').
card_number('knowledge exploitation'/'MOR', '38').
card_multiverse_id('knowledge exploitation'/'MOR', '152664').

card_in_set('latchkey faerie', 'MOR').
card_original_type('latchkey faerie'/'MOR', 'Creature — Faerie Rogue').
card_original_text('latchkey faerie'/'MOR', 'Flying\nProwl {2}{U} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Faerie or Rogue.)\nWhen Latchkey Faerie comes into play, if its prowl cost was paid, draw a card.').
card_first_print('latchkey faerie', 'MOR').
card_image_name('latchkey faerie'/'MOR', 'latchkey faerie').
card_uid('latchkey faerie'/'MOR', 'MOR:Latchkey Faerie:latchkey faerie').
card_rarity('latchkey faerie'/'MOR', 'Common').
card_artist('latchkey faerie'/'MOR', 'Warren Mahy').
card_number('latchkey faerie'/'MOR', '39').
card_multiverse_id('latchkey faerie'/'MOR', '153122').

card_in_set('leaf-crowned elder', 'MOR').
card_original_type('leaf-crowned elder'/'MOR', 'Creature — Treefolk Shaman').
card_original_text('leaf-crowned elder'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Leaf-Crowned Elder, you may reveal it. If you do, you may play that card without paying its mana cost.').
card_first_print('leaf-crowned elder', 'MOR').
card_image_name('leaf-crowned elder'/'MOR', 'leaf-crowned elder').
card_uid('leaf-crowned elder'/'MOR', 'MOR:Leaf-Crowned Elder:leaf-crowned elder').
card_rarity('leaf-crowned elder'/'MOR', 'Rare').
card_artist('leaf-crowned elder'/'MOR', 'Wayne Reynolds').
card_number('leaf-crowned elder'/'MOR', '128').
card_multiverse_id('leaf-crowned elder'/'MOR', '152682').

card_in_set('lightning crafter', 'MOR').
card_original_type('lightning crafter'/'MOR', 'Creature — Goblin Shaman').
card_original_text('lightning crafter'/'MOR', 'Champion a Goblin or Shaman (When this comes into play, sacrifice it unless you remove another Goblin or Shaman you control from the game. When this leaves play, that card returns to play.)\n{T}: Lightning Crafter deals 3 damage to target creature or player.').
card_first_print('lightning crafter', 'MOR').
card_image_name('lightning crafter'/'MOR', 'lightning crafter').
card_uid('lightning crafter'/'MOR', 'MOR:Lightning Crafter:lightning crafter').
card_rarity('lightning crafter'/'MOR', 'Rare').
card_artist('lightning crafter'/'MOR', 'Dan Scott').
card_number('lightning crafter'/'MOR', '93').
card_multiverse_id('lightning crafter'/'MOR', '152893').

card_in_set('luminescent rain', 'MOR').
card_original_type('luminescent rain'/'MOR', 'Instant').
card_original_text('luminescent rain'/'MOR', 'Choose a creature type. You gain 2 life for each permanent you control of that type.').
card_first_print('luminescent rain', 'MOR').
card_image_name('luminescent rain'/'MOR', 'luminescent rain').
card_uid('luminescent rain'/'MOR', 'MOR:Luminescent Rain:luminescent rain').
card_rarity('luminescent rain'/'MOR', 'Common').
card_artist('luminescent rain'/'MOR', 'John Avon').
card_number('luminescent rain'/'MOR', '129').
card_flavor_text('luminescent rain'/'MOR', 'Storms in Lorwyn don\'t patter on village rooftops, but rather drench the clachan in rays of amber light.').
card_multiverse_id('luminescent rain'/'MOR', '153147').

card_in_set('lunk errant', 'MOR').
card_original_type('lunk errant'/'MOR', 'Creature — Giant Warrior').
card_original_text('lunk errant'/'MOR', 'Whenever Lunk Errant attacks alone, it gets +1/+1 and gains trample until end of turn.').
card_first_print('lunk errant', 'MOR').
card_image_name('lunk errant'/'MOR', 'lunk errant').
card_uid('lunk errant'/'MOR', 'MOR:Lunk Errant:lunk errant').
card_rarity('lunk errant'/'MOR', 'Common').
card_artist('lunk errant'/'MOR', 'Warren Mahy').
card_number('lunk errant'/'MOR', '94').
card_flavor_text('lunk errant'/'MOR', '\"One\'s plenty,\" observed the merrow.\n\"Way too plenty,\" agreed the boggart.').
card_multiverse_id('lunk errant'/'MOR', '152581').

card_in_set('lys alana bowmaster', 'MOR').
card_original_type('lys alana bowmaster'/'MOR', 'Creature — Elf Archer').
card_original_text('lys alana bowmaster'/'MOR', 'Reach (This can block creatures with flying.)\nWhenever you play an Elf spell, you may have Lys Alana Bowmaster deal 2 damage to target creature with flying.').
card_first_print('lys alana bowmaster', 'MOR').
card_image_name('lys alana bowmaster'/'MOR', 'lys alana bowmaster').
card_uid('lys alana bowmaster'/'MOR', 'MOR:Lys Alana Bowmaster:lys alana bowmaster').
card_rarity('lys alana bowmaster'/'MOR', 'Common').
card_artist('lys alana bowmaster'/'MOR', 'Dan Scott').
card_number('lys alana bowmaster'/'MOR', '130').
card_multiverse_id('lys alana bowmaster'/'MOR', '153462').

card_in_set('maralen of the mornsong', 'MOR').
card_original_type('maralen of the mornsong'/'MOR', 'Legendary Creature — Elf Wizard').
card_original_text('maralen of the mornsong'/'MOR', 'Players can\'t draw cards.\nAt the beginning of each player\'s draw step, that player loses 3 life, searches his or her library for a card, puts it into his or her hand, then shuffles his or her library.').
card_first_print('maralen of the mornsong', 'MOR').
card_image_name('maralen of the mornsong'/'MOR', 'maralen of the mornsong').
card_uid('maralen of the mornsong'/'MOR', 'MOR:Maralen of the Mornsong:maralen of the mornsong').
card_rarity('maralen of the mornsong'/'MOR', 'Rare').
card_artist('maralen of the mornsong'/'MOR', 'Mark Zug').
card_number('maralen of the mornsong'/'MOR', '65').
card_flavor_text('maralen of the mornsong'/'MOR', 'Maralen sent Veesa, Endry, and Iliona—the Vendilion clique—on the gravest of tasks.').
card_multiverse_id('maralen of the mornsong'/'MOR', '152546').

card_in_set('meadowboon', 'MOR').
card_original_type('meadowboon'/'MOR', 'Creature — Elemental').
card_original_text('meadowboon'/'MOR', 'When Meadowboon leaves play, put a +1/+1 counter on each creature target player controls.\nEvoke {3}{W} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('meadowboon', 'MOR').
card_image_name('meadowboon'/'MOR', 'meadowboon').
card_uid('meadowboon'/'MOR', 'MOR:Meadowboon:meadowboon').
card_rarity('meadowboon'/'MOR', 'Uncommon').
card_artist('meadowboon'/'MOR', 'Steven Belledin').
card_number('meadowboon'/'MOR', '17').
card_multiverse_id('meadowboon'/'MOR', '157432').

card_in_set('merrow witsniper', 'MOR').
card_original_type('merrow witsniper'/'MOR', 'Creature — Merfolk Rogue').
card_original_text('merrow witsniper'/'MOR', 'When Merrow Witsniper comes into play, target player puts the top card of his or her library into his or her graveyard.').
card_first_print('merrow witsniper', 'MOR').
card_image_name('merrow witsniper'/'MOR', 'merrow witsniper').
card_uid('merrow witsniper'/'MOR', 'MOR:Merrow Witsniper:merrow witsniper').
card_rarity('merrow witsniper'/'MOR', 'Common').
card_artist('merrow witsniper'/'MOR', 'Steve Prescott').
card_number('merrow witsniper'/'MOR', '40').
card_flavor_text('merrow witsniper'/'MOR', '\"The world above the waterline must be monitored—and, when necessary, adjusted.\"').
card_multiverse_id('merrow witsniper'/'MOR', '152699').

card_in_set('mind shatter', 'MOR').
card_original_type('mind shatter'/'MOR', 'Sorcery').
card_original_text('mind shatter'/'MOR', 'Target player discards X cards at random.').
card_first_print('mind shatter', 'MOR').
card_image_name('mind shatter'/'MOR', 'mind shatter').
card_uid('mind shatter'/'MOR', 'MOR:Mind Shatter:mind shatter').
card_rarity('mind shatter'/'MOR', 'Rare').
card_artist('mind shatter'/'MOR', 'Michael Sutfin').
card_number('mind shatter'/'MOR', '66').
card_flavor_text('mind shatter'/'MOR', 'Dark thoughts hatch and twist within the mind, straining to take wing.').
card_multiverse_id('mind shatter'/'MOR', '157422').

card_in_set('mind spring', 'MOR').
card_original_type('mind spring'/'MOR', 'Sorcery').
card_original_text('mind spring'/'MOR', 'Draw X cards.').
card_first_print('mind spring', 'MOR').
card_image_name('mind spring'/'MOR', 'mind spring').
card_uid('mind spring'/'MOR', 'MOR:Mind Spring:mind spring').
card_rarity('mind spring'/'MOR', 'Rare').
card_artist('mind spring'/'MOR', 'Mark Zug').
card_number('mind spring'/'MOR', '41').
card_flavor_text('mind spring'/'MOR', 'Fragments of thought refract and multiply, surging in a geyser of dizzying insight.').
card_multiverse_id('mind spring'/'MOR', '152628').

card_in_set('moonglove changeling', 'MOR').
card_original_type('moonglove changeling'/'MOR', 'Creature — Shapeshifter').
card_original_text('moonglove changeling'/'MOR', 'Changeling (This card is every creature type at all times.)\n{B}: Moonglove Changeling gains deathtouch until end of turn. (Whenever it deals damage to a creature, destroy that creature.)').
card_first_print('moonglove changeling', 'MOR').
card_image_name('moonglove changeling'/'MOR', 'moonglove changeling').
card_uid('moonglove changeling'/'MOR', 'MOR:Moonglove Changeling:moonglove changeling').
card_rarity('moonglove changeling'/'MOR', 'Common').
card_artist('moonglove changeling'/'MOR', 'Wayne England').
card_number('moonglove changeling'/'MOR', '67').
card_multiverse_id('moonglove changeling'/'MOR', '143218').

card_in_set('morsel theft', 'MOR').
card_original_type('morsel theft'/'MOR', 'Tribal Sorcery — Rogue').
card_original_text('morsel theft'/'MOR', 'Prowl {1}{B} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nTarget player loses 3 life and you gain 3 life. If Morsel Theft\'s prowl cost was paid, draw a card.').
card_first_print('morsel theft', 'MOR').
card_image_name('morsel theft'/'MOR', 'morsel theft').
card_uid('morsel theft'/'MOR', 'MOR:Morsel Theft:morsel theft').
card_rarity('morsel theft'/'MOR', 'Common').
card_artist('morsel theft'/'MOR', 'Darrell Riche').
card_number('morsel theft'/'MOR', '68').
card_multiverse_id('morsel theft'/'MOR', '152903').

card_in_set('mosquito guard', 'MOR').
card_original_type('mosquito guard'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('mosquito guard'/'MOR', 'First strike\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_first_print('mosquito guard', 'MOR').
card_image_name('mosquito guard'/'MOR', 'mosquito guard').
card_uid('mosquito guard'/'MOR', 'MOR:Mosquito Guard:mosquito guard').
card_rarity('mosquito guard'/'MOR', 'Common').
card_artist('mosquito guard'/'MOR', 'Randy Gallegos').
card_number('mosquito guard'/'MOR', '18').
card_flavor_text('mosquito guard'/'MOR', 'His aim is as sure as a mosquito\'s sting, but with none of the warning.').
card_multiverse_id('mosquito guard'/'MOR', '152585').

card_in_set('mothdust changeling', 'MOR').
card_original_type('mothdust changeling'/'MOR', 'Creature — Shapeshifter').
card_original_text('mothdust changeling'/'MOR', 'Changeling (This card is every creature type at all times.)\nTap an untapped creature you control: Mothdust Changeling gains flying until end of turn.').
card_first_print('mothdust changeling', 'MOR').
card_image_name('mothdust changeling'/'MOR', 'mothdust changeling').
card_uid('mothdust changeling'/'MOR', 'MOR:Mothdust Changeling:mothdust changeling').
card_rarity('mothdust changeling'/'MOR', 'Common').
card_artist('mothdust changeling'/'MOR', 'Shelly Wan').
card_number('mothdust changeling'/'MOR', '42').
card_flavor_text('mothdust changeling'/'MOR', '\"Ever seen a changeling fly into a lantern?\"\n—Calydd, kithkin farmer').
card_multiverse_id('mothdust changeling'/'MOR', '143333').

card_in_set('mudbutton clanger', 'MOR').
card_original_type('mudbutton clanger'/'MOR', 'Creature — Goblin Warrior').
card_original_text('mudbutton clanger'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Mudbutton Clanger, you may reveal it. If you do, Mudbutton Clanger gets +1/+1 until end of turn.').
card_first_print('mudbutton clanger', 'MOR').
card_image_name('mudbutton clanger'/'MOR', 'mudbutton clanger').
card_uid('mudbutton clanger'/'MOR', 'MOR:Mudbutton Clanger:mudbutton clanger').
card_rarity('mudbutton clanger'/'MOR', 'Common').
card_artist('mudbutton clanger'/'MOR', 'Larry MacDougall').
card_number('mudbutton clanger'/'MOR', '95').
card_multiverse_id('mudbutton clanger'/'MOR', '152876').

card_in_set('murmuring bosk', 'MOR').
card_original_type('murmuring bosk'/'MOR', 'Land — Forest').
card_original_text('murmuring bosk'/'MOR', '({T}: Add {G} to your mana pool.)\nAs Murmuring Bosk comes into play, you may reveal a Treefolk card from your hand. If you don\'t, Murmuring Bosk comes into play tapped.\n{T}: Add {W} or {B} to your mana pool. Murmuring Bosk deals 1 damage to you.').
card_first_print('murmuring bosk', 'MOR').
card_image_name('murmuring bosk'/'MOR', 'murmuring bosk').
card_uid('murmuring bosk'/'MOR', 'MOR:Murmuring Bosk:murmuring bosk').
card_rarity('murmuring bosk'/'MOR', 'Rare').
card_artist('murmuring bosk'/'MOR', 'John Avon').
card_number('murmuring bosk'/'MOR', '147').
card_multiverse_id('murmuring bosk'/'MOR', '153467').

card_in_set('mutavault', 'MOR').
card_original_type('mutavault'/'MOR', 'Land').
card_original_text('mutavault'/'MOR', '{T}: Add {1} to your mana pool.\n{1}: Mutavault becomes a 2/2 creature with all creature types until end of turn. It\'s still a land.').
card_image_name('mutavault'/'MOR', 'mutavault').
card_uid('mutavault'/'MOR', 'MOR:Mutavault:mutavault').
card_rarity('mutavault'/'MOR', 'Rare').
card_artist('mutavault'/'MOR', 'Fred Fields').
card_number('mutavault'/'MOR', '148').
card_flavor_text('mutavault'/'MOR', 'Some changelings born at Velis Vel never return, but their essence never leaves.').
card_multiverse_id('mutavault'/'MOR', '152724').

card_in_set('negate', 'MOR').
card_original_type('negate'/'MOR', 'Instant').
card_original_text('negate'/'MOR', 'Counter target noncreature spell.').
card_image_name('negate'/'MOR', 'negate').
card_uid('negate'/'MOR', 'MOR:Negate:negate').
card_rarity('negate'/'MOR', 'Common').
card_artist('negate'/'MOR', 'Jeremy Jarvis').
card_number('negate'/'MOR', '43').
card_flavor_text('negate'/'MOR', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'MOR', '152570').

card_in_set('nevermaker', 'MOR').
card_original_type('nevermaker'/'MOR', 'Creature — Elemental').
card_original_text('nevermaker'/'MOR', 'Flying\nWhen Nevermaker leaves play, put target nonland permanent on top of its owner\'s library.\nEvoke {3}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('nevermaker', 'MOR').
card_image_name('nevermaker'/'MOR', 'nevermaker').
card_uid('nevermaker'/'MOR', 'MOR:Nevermaker:nevermaker').
card_rarity('nevermaker'/'MOR', 'Uncommon').
card_artist('nevermaker'/'MOR', 'Chuck Lukacs').
card_number('nevermaker'/'MOR', '44').
card_multiverse_id('nevermaker'/'MOR', '152934').

card_in_set('nightshade schemers', 'MOR').
card_original_type('nightshade schemers'/'MOR', 'Creature — Faerie Wizard').
card_original_text('nightshade schemers'/'MOR', 'Flying\nKinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Nightshade Schemers, you may reveal it. If you do, each opponent loses 2 life.').
card_first_print('nightshade schemers', 'MOR').
card_image_name('nightshade schemers'/'MOR', 'nightshade schemers').
card_uid('nightshade schemers'/'MOR', 'MOR:Nightshade Schemers:nightshade schemers').
card_rarity('nightshade schemers'/'MOR', 'Uncommon').
card_artist('nightshade schemers'/'MOR', 'Paolo Parente').
card_number('nightshade schemers'/'MOR', '69').
card_multiverse_id('nightshade schemers'/'MOR', '153001').

card_in_set('noggin whack', 'MOR').
card_original_type('noggin whack'/'MOR', 'Tribal Sorcery — Rogue').
card_original_text('noggin whack'/'MOR', 'Prowl {1}{B} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nTarget player reveals three cards from his or her hand. You choose two of them. That player discards those cards.').
card_first_print('noggin whack', 'MOR').
card_image_name('noggin whack'/'MOR', 'noggin whack').
card_uid('noggin whack'/'MOR', 'MOR:Noggin Whack:noggin whack').
card_rarity('noggin whack'/'MOR', 'Uncommon').
card_artist('noggin whack'/'MOR', 'Alan Pollack').
card_number('noggin whack'/'MOR', '70').
card_multiverse_id('noggin whack'/'MOR', '152844').

card_in_set('notorious throng', 'MOR').
card_original_type('notorious throng'/'MOR', 'Tribal Sorcery — Rogue').
card_original_text('notorious throng'/'MOR', 'Prowl {5}{U} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nPut X 1/1 black Faerie Rogue creature tokens with flying into play, where X is the damage dealt to your opponents this turn. If Notorious Throng\'s prowl cost was paid, take an extra turn after this one.').
card_first_print('notorious throng', 'MOR').
card_image_name('notorious throng'/'MOR', 'notorious throng').
card_uid('notorious throng'/'MOR', 'MOR:Notorious Throng:notorious throng').
card_rarity('notorious throng'/'MOR', 'Rare').
card_artist('notorious throng'/'MOR', 'Thomas Denmark').
card_number('notorious throng'/'MOR', '45').
card_multiverse_id('notorious throng'/'MOR', '152579').

card_in_set('obsidian battle-axe', 'MOR').
card_original_type('obsidian battle-axe'/'MOR', 'Tribal Artifact — Warrior Equipment').
card_original_text('obsidian battle-axe'/'MOR', 'Equipped creature gets +2/+1 and has haste.\nWhenever a Warrior creature comes into play, you may attach Obsidian Battle-Axe to it.\nEquip {3}').
card_first_print('obsidian battle-axe', 'MOR').
card_image_name('obsidian battle-axe'/'MOR', 'obsidian battle-axe').
card_uid('obsidian battle-axe'/'MOR', 'MOR:Obsidian Battle-Axe:obsidian battle-axe').
card_rarity('obsidian battle-axe'/'MOR', 'Uncommon').
card_artist('obsidian battle-axe'/'MOR', 'Jeff Easley').
card_number('obsidian battle-axe'/'MOR', '144').
card_flavor_text('obsidian battle-axe'/'MOR', 'It\'s the flint that sparks a warrior\'s rage.').
card_multiverse_id('obsidian battle-axe'/'MOR', '154164').

card_in_set('offalsnout', 'MOR').
card_original_type('offalsnout'/'MOR', 'Creature — Elemental').
card_original_text('offalsnout'/'MOR', 'Flash\nWhen Offalsnout leaves play, remove target card in a graveyard from the game.\nEvoke {B} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('offalsnout', 'MOR').
card_image_name('offalsnout'/'MOR', 'offalsnout').
card_uid('offalsnout'/'MOR', 'MOR:Offalsnout:offalsnout').
card_rarity('offalsnout'/'MOR', 'Uncommon').
card_artist('offalsnout'/'MOR', 'Alex Horley-Orlandelli').
card_number('offalsnout'/'MOR', '71').
card_multiverse_id('offalsnout'/'MOR', '152571').

card_in_set('oona\'s blackguard', 'MOR').
card_original_type('oona\'s blackguard'/'MOR', 'Creature — Faerie Rogue').
card_original_text('oona\'s blackguard'/'MOR', 'Flying\nEach other Rogue creature you control comes into play with an additional +1/+1 counter on it.\nWhenever a creature you control with a +1/+1 counter on it deals combat damage to a player, that player discards a card.').
card_image_name('oona\'s blackguard'/'MOR', 'oona\'s blackguard').
card_uid('oona\'s blackguard'/'MOR', 'MOR:Oona\'s Blackguard:oona\'s blackguard').
card_rarity('oona\'s blackguard'/'MOR', 'Uncommon').
card_artist('oona\'s blackguard'/'MOR', 'Kev Walker').
card_number('oona\'s blackguard'/'MOR', '72').
card_multiverse_id('oona\'s blackguard'/'MOR', '153099').

card_in_set('orchard warden', 'MOR').
card_original_type('orchard warden'/'MOR', 'Creature — Treefolk Shaman').
card_original_text('orchard warden'/'MOR', 'Whenever another Treefolk creature comes into play under your control, you may gain life equal to that creature\'s toughness.').
card_first_print('orchard warden', 'MOR').
card_image_name('orchard warden'/'MOR', 'orchard warden').
card_uid('orchard warden'/'MOR', 'MOR:Orchard Warden:orchard warden').
card_rarity('orchard warden'/'MOR', 'Uncommon').
card_artist('orchard warden'/'MOR', 'Rebecca Guay').
card_number('orchard warden'/'MOR', '131').
card_flavor_text('orchard warden'/'MOR', '\"After the Rising, a treefolk\'s mind is as limber and green as its limbs, and is at its most receptive to our teachings.\"').
card_multiverse_id('orchard warden'/'MOR', '152991').

card_in_set('order of the golden cricket', 'MOR').
card_original_type('order of the golden cricket'/'MOR', 'Creature — Kithkin Knight').
card_original_text('order of the golden cricket'/'MOR', 'Whenever Order of the Golden Cricket attacks, you may pay {W}. If you do, it gains flying until end of turn.').
card_first_print('order of the golden cricket', 'MOR').
card_image_name('order of the golden cricket'/'MOR', 'order of the golden cricket').
card_uid('order of the golden cricket'/'MOR', 'MOR:Order of the Golden Cricket:order of the golden cricket').
card_rarity('order of the golden cricket'/'MOR', 'Common').
card_artist('order of the golden cricket'/'MOR', 'Mark Zug').
card_number('order of the golden cricket'/'MOR', '19').
card_flavor_text('order of the golden cricket'/'MOR', '\"Should you take it in mind to ride a springjack, remember: there are easier ways to fly, and harder ways to break your skull.\"\n—Lann of Cloverdell').
card_multiverse_id('order of the golden cricket'/'MOR', '153463').

card_in_set('pack\'s disdain', 'MOR').
card_original_type('pack\'s disdain'/'MOR', 'Instant').
card_original_text('pack\'s disdain'/'MOR', 'Choose a creature type. Target creature gets -1/-1 until end of turn for each permanent of the chosen type you control.').
card_first_print('pack\'s disdain', 'MOR').
card_image_name('pack\'s disdain'/'MOR', 'pack\'s disdain').
card_uid('pack\'s disdain'/'MOR', 'MOR:Pack\'s Disdain:pack\'s disdain').
card_rarity('pack\'s disdain'/'MOR', 'Common').
card_artist('pack\'s disdain'/'MOR', 'Pete Venters').
card_number('pack\'s disdain'/'MOR', '73').
card_flavor_text('pack\'s disdain'/'MOR', 'Like the sun to a flower, the adoring gaze of the tribe is sustenance to an elf. To be shunned is to wither.').
card_multiverse_id('pack\'s disdain'/'MOR', '153165').

card_in_set('preeminent captain', 'MOR').
card_original_type('preeminent captain'/'MOR', 'Creature — Kithkin Soldier').
card_original_text('preeminent captain'/'MOR', 'First strike\nWhenever Preeminent Captain attacks, you may put a Soldier creature card from your hand into play tapped and attacking.').
card_first_print('preeminent captain', 'MOR').
card_image_name('preeminent captain'/'MOR', 'preeminent captain').
card_uid('preeminent captain'/'MOR', 'MOR:Preeminent Captain:preeminent captain').
card_rarity('preeminent captain'/'MOR', 'Rare').
card_artist('preeminent captain'/'MOR', 'Greg Staples').
card_number('preeminent captain'/'MOR', '20').
card_flavor_text('preeminent captain'/'MOR', '\"If you need an example to lead others to the front lines, consider the precedent set.\"').
card_multiverse_id('preeminent captain'/'MOR', '152595').

card_in_set('prickly boggart', 'MOR').
card_original_type('prickly boggart'/'MOR', 'Creature — Goblin Rogue').
card_original_text('prickly boggart'/'MOR', 'Fear').
card_first_print('prickly boggart', 'MOR').
card_image_name('prickly boggart'/'MOR', 'prickly boggart').
card_uid('prickly boggart'/'MOR', 'MOR:Prickly Boggart:prickly boggart').
card_rarity('prickly boggart'/'MOR', 'Common').
card_artist('prickly boggart'/'MOR', 'Jesper Ejsing').
card_number('prickly boggart'/'MOR', '74').
card_flavor_text('prickly boggart'/'MOR', '\"Even without the spines, who would dare to touch it?\"\n—Desmera, perfect of Wren\'s Run').
card_multiverse_id('prickly boggart'/'MOR', '152718').

card_in_set('primal beyond', 'MOR').
card_original_type('primal beyond'/'MOR', 'Land').
card_original_text('primal beyond'/'MOR', 'As Primal Beyond comes into play, you may reveal an Elemental card from your hand. If you don\'t, Primal Beyond comes into play tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to play Elemental spells or activated abilities of Elementals.').
card_first_print('primal beyond', 'MOR').
card_image_name('primal beyond'/'MOR', 'primal beyond').
card_uid('primal beyond'/'MOR', 'MOR:Primal Beyond:primal beyond').
card_rarity('primal beyond'/'MOR', 'Rare').
card_artist('primal beyond'/'MOR', 'Mark Tedin').
card_number('primal beyond'/'MOR', '149').
card_multiverse_id('primal beyond'/'MOR', '153464').

card_in_set('pulling teeth', 'MOR').
card_original_type('pulling teeth'/'MOR', 'Sorcery').
card_original_text('pulling teeth'/'MOR', 'Clash with an opponent. If you win, target player discards two cards. Otherwise, that player discards a card. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('pulling teeth', 'MOR').
card_image_name('pulling teeth'/'MOR', 'pulling teeth').
card_uid('pulling teeth'/'MOR', 'MOR:Pulling Teeth:pulling teeth').
card_rarity('pulling teeth'/'MOR', 'Common').
card_artist('pulling teeth'/'MOR', 'Jim Pavelec').
card_number('pulling teeth'/'MOR', '75').
card_multiverse_id('pulling teeth'/'MOR', '152667').

card_in_set('pyroclast consul', 'MOR').
card_original_type('pyroclast consul'/'MOR', 'Creature — Elemental Shaman').
card_original_text('pyroclast consul'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Pyroclast Consul, you may reveal it. If you do, Pyroclast Consul deals 2 damage to each creature.').
card_first_print('pyroclast consul', 'MOR').
card_image_name('pyroclast consul'/'MOR', 'pyroclast consul').
card_uid('pyroclast consul'/'MOR', 'MOR:Pyroclast Consul:pyroclast consul').
card_rarity('pyroclast consul'/'MOR', 'Uncommon').
card_artist('pyroclast consul'/'MOR', 'Christopher Moeller').
card_number('pyroclast consul'/'MOR', '96').
card_multiverse_id('pyroclast consul'/'MOR', '153007').

card_in_set('rage forger', 'MOR').
card_original_type('rage forger'/'MOR', 'Creature — Elemental Shaman').
card_original_text('rage forger'/'MOR', 'When Rage Forger comes into play, put a +1/+1 counter on each other Shaman creature you control.\nWhenever a creature you control with a +1/+1 counter on it attacks, you may have that creature deal 1 damage to target player.').
card_first_print('rage forger', 'MOR').
card_image_name('rage forger'/'MOR', 'rage forger').
card_uid('rage forger'/'MOR', 'MOR:Rage Forger:rage forger').
card_rarity('rage forger'/'MOR', 'Uncommon').
card_artist('rage forger'/'MOR', 'Dominick Domingo').
card_number('rage forger'/'MOR', '97').
card_multiverse_id('rage forger'/'MOR', '153104').

card_in_set('reach of branches', 'MOR').
card_original_type('reach of branches'/'MOR', 'Tribal Instant — Treefolk').
card_original_text('reach of branches'/'MOR', 'Put a 2/5 green Treefolk Shaman creature token into play.\nWhenever a Forest comes into play under your control, you may return Reach of Branches from your graveyard to your hand.').
card_first_print('reach of branches', 'MOR').
card_image_name('reach of branches'/'MOR', 'reach of branches').
card_uid('reach of branches'/'MOR', 'MOR:Reach of Branches:reach of branches').
card_rarity('reach of branches'/'MOR', 'Rare').
card_artist('reach of branches'/'MOR', 'Scott Hampton').
card_number('reach of branches'/'MOR', '132').
card_flavor_text('reach of branches'/'MOR', 'Growth has no limits.').
card_multiverse_id('reach of branches'/'MOR', '154162').

card_in_set('recross the paths', 'MOR').
card_original_type('recross the paths'/'MOR', 'Sorcery').
card_original_text('recross the paths'/'MOR', 'Reveal cards from the top of your library until you reveal a land card. Put that card into play and the rest on the bottom of your library in any order. Clash with an opponent. If you win, return Recross the Paths to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('recross the paths', 'MOR').
card_image_name('recross the paths'/'MOR', 'recross the paths').
card_uid('recross the paths'/'MOR', 'MOR:Recross the Paths:recross the paths').
card_rarity('recross the paths'/'MOR', 'Uncommon').
card_artist('recross the paths'/'MOR', 'Greg Hildebrandt').
card_number('recross the paths'/'MOR', '133').
card_multiverse_id('recross the paths'/'MOR', '152874').

card_in_set('redeem the lost', 'MOR').
card_original_type('redeem the lost'/'MOR', 'Instant').
card_original_text('redeem the lost'/'MOR', 'Target creature you control gains protection from the color of your choice until end of turn. Clash with an opponent. If you win, return Redeem the Lost to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('redeem the lost', 'MOR').
card_image_name('redeem the lost'/'MOR', 'redeem the lost').
card_uid('redeem the lost'/'MOR', 'MOR:Redeem the Lost:redeem the lost').
card_rarity('redeem the lost'/'MOR', 'Uncommon').
card_artist('redeem the lost'/'MOR', 'Scott M. Fischer').
card_number('redeem the lost'/'MOR', '21').
card_multiverse_id('redeem the lost'/'MOR', '152616').

card_in_set('reins of the vinesteed', 'MOR').
card_original_type('reins of the vinesteed'/'MOR', 'Enchantment — Aura').
card_original_text('reins of the vinesteed'/'MOR', 'Enchant creature\nEnchanted creature gets +2/+2.\nWhen enchanted creature is put into a graveyard, you may return Reins of the Vinesteed from your graveyard to play attached to a creature that shares a creature type with that creature.').
card_first_print('reins of the vinesteed', 'MOR').
card_image_name('reins of the vinesteed'/'MOR', 'reins of the vinesteed').
card_uid('reins of the vinesteed'/'MOR', 'MOR:Reins of the Vinesteed:reins of the vinesteed').
card_rarity('reins of the vinesteed'/'MOR', 'Common').
card_artist('reins of the vinesteed'/'MOR', 'Jim Pavelec').
card_number('reins of the vinesteed'/'MOR', '134').
card_multiverse_id('reins of the vinesteed'/'MOR', '152625').

card_in_set('release the ants', 'MOR').
card_original_type('release the ants'/'MOR', 'Instant').
card_original_text('release the ants'/'MOR', 'Release the Ants deals 1 damage to target creature or player. Clash with an opponent. If you win, return Release the Ants to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('release the ants', 'MOR').
card_image_name('release the ants'/'MOR', 'release the ants').
card_uid('release the ants'/'MOR', 'MOR:Release the Ants:release the ants').
card_rarity('release the ants'/'MOR', 'Uncommon').
card_artist('release the ants'/'MOR', 'Ron Spencer').
card_number('release the ants'/'MOR', '98').
card_multiverse_id('release the ants'/'MOR', '152619').

card_in_set('research the deep', 'MOR').
card_original_type('research the deep'/'MOR', 'Sorcery').
card_original_text('research the deep'/'MOR', 'Draw a card. Clash with an opponent. If you win, return Research the Deep to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('research the deep', 'MOR').
card_image_name('research the deep'/'MOR', 'research the deep').
card_uid('research the deep'/'MOR', 'MOR:Research the Deep:research the deep').
card_rarity('research the deep'/'MOR', 'Uncommon').
card_artist('research the deep'/'MOR', 'Eric Fortune').
card_number('research the deep'/'MOR', '46').
card_multiverse_id('research the deep'/'MOR', '152618').

card_in_set('reveillark', 'MOR').
card_original_type('reveillark'/'MOR', 'Creature — Elemental').
card_original_text('reveillark'/'MOR', 'Flying\nWhen Reveillark leaves play, return up to two target creature cards with power 2 or less from your graveyard to play.\nEvoke {5}{W} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('reveillark', 'MOR').
card_image_name('reveillark'/'MOR', 'reveillark').
card_uid('reveillark'/'MOR', 'MOR:Reveillark:reveillark').
card_rarity('reveillark'/'MOR', 'Rare').
card_artist('reveillark'/'MOR', 'Jim Murray').
card_number('reveillark'/'MOR', '22').
card_multiverse_id('reveillark'/'MOR', '152716').

card_in_set('revive the fallen', 'MOR').
card_original_type('revive the fallen'/'MOR', 'Sorcery').
card_original_text('revive the fallen'/'MOR', 'Return target creature card in a graveyard to its owner\'s hand. Clash with an opponent. If you win, return Revive the Fallen to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('revive the fallen', 'MOR').
card_image_name('revive the fallen'/'MOR', 'revive the fallen').
card_uid('revive the fallen'/'MOR', 'MOR:Revive the Fallen:revive the fallen').
card_rarity('revive the fallen'/'MOR', 'Uncommon').
card_artist('revive the fallen'/'MOR', 'Steven Belledin').
card_number('revive the fallen'/'MOR', '76').
card_multiverse_id('revive the fallen'/'MOR', '152836').

card_in_set('rhys the exiled', 'MOR').
card_original_type('rhys the exiled'/'MOR', 'Legendary Creature — Elf Warrior').
card_original_text('rhys the exiled'/'MOR', 'Whenever Rhys the Exiled attacks, you gain 1 life for each Elf you control.\n{B}, Sacrifice an Elf: Regenerate Rhys the Exiled.').
card_first_print('rhys the exiled', 'MOR').
card_image_name('rhys the exiled'/'MOR', 'rhys the exiled').
card_uid('rhys the exiled'/'MOR', 'MOR:Rhys the Exiled:rhys the exiled').
card_rarity('rhys the exiled'/'MOR', 'Rare').
card_artist('rhys the exiled'/'MOR', 'Steve Prescott').
card_number('rhys the exiled'/'MOR', '135').
card_flavor_text('rhys the exiled'/'MOR', 'Once a famed hunter and packmaster, now a renegade seeking his own path.').
card_multiverse_id('rhys the exiled'/'MOR', '152643').

card_in_set('rivals\' duel', 'MOR').
card_original_type('rivals\' duel'/'MOR', 'Sorcery').
card_original_text('rivals\' duel'/'MOR', 'Choose two target creatures that share no creature types. Each of those creatures deals damage equal to its power to the other.').
card_first_print('rivals\' duel', 'MOR').
card_image_name('rivals\' duel'/'MOR', 'rivals\' duel').
card_uid('rivals\' duel'/'MOR', 'MOR:Rivals\' Duel:rivals\' duel').
card_rarity('rivals\' duel'/'MOR', 'Uncommon').
card_artist('rivals\' duel'/'MOR', 'Zoltan Boros & Gabor Szikszai').
card_number('rivals\' duel'/'MOR', '99').
card_flavor_text('rivals\' duel'/'MOR', 'They could agree on one thing only: one of them must die.').
card_multiverse_id('rivals\' duel'/'MOR', '152586').

card_in_set('roar of the crowd', 'MOR').
card_original_type('roar of the crowd'/'MOR', 'Sorcery').
card_original_text('roar of the crowd'/'MOR', 'Choose a creature type. Roar of the Crowd deals damage to target creature or player equal to the number of permanents you control of the chosen type.').
card_first_print('roar of the crowd', 'MOR').
card_image_name('roar of the crowd'/'MOR', 'roar of the crowd').
card_uid('roar of the crowd'/'MOR', 'MOR:Roar of the Crowd:roar of the crowd').
card_rarity('roar of the crowd'/'MOR', 'Common').
card_artist('roar of the crowd'/'MOR', 'Dave Dorman').
card_number('roar of the crowd'/'MOR', '100').
card_flavor_text('roar of the crowd'/'MOR', 'One voice is but a pebble to the rest of the world. A thousand voices at once is an avalanche.').
card_multiverse_id('roar of the crowd'/'MOR', '153158').

card_in_set('rustic clachan', 'MOR').
card_original_type('rustic clachan'/'MOR', 'Land').
card_original_text('rustic clachan'/'MOR', 'As Rustic Clachan comes into play, you may reveal a Kithkin card from your hand. If you don\'t, Rustic Clachan comes into play tapped.\n{T}: Add {W} to your mana pool.\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_first_print('rustic clachan', 'MOR').
card_image_name('rustic clachan'/'MOR', 'rustic clachan').
card_uid('rustic clachan'/'MOR', 'MOR:Rustic Clachan:rustic clachan').
card_rarity('rustic clachan'/'MOR', 'Rare').
card_artist('rustic clachan'/'MOR', 'Fred Fields').
card_number('rustic clachan'/'MOR', '150').
card_multiverse_id('rustic clachan'/'MOR', '153460').

card_in_set('sage of fables', 'MOR').
card_original_type('sage of fables'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('sage of fables'/'MOR', 'Each other Wizard creature you control comes into play with an additional +1/+1 counter on it.\n{2}, Remove a +1/+1 counter from a creature you control: Draw a card.').
card_first_print('sage of fables', 'MOR').
card_image_name('sage of fables'/'MOR', 'sage of fables').
card_uid('sage of fables'/'MOR', 'MOR:Sage of Fables:sage of fables').
card_rarity('sage of fables'/'MOR', 'Uncommon').
card_artist('sage of fables'/'MOR', 'Shelly Wan').
card_number('sage of fables'/'MOR', '47').
card_flavor_text('sage of fables'/'MOR', '\"There is no secret that cannot be sold.\"').
card_multiverse_id('sage of fables'/'MOR', '152871').

card_in_set('sage\'s dousing', 'MOR').
card_original_type('sage\'s dousing'/'MOR', 'Tribal Instant — Wizard').
card_original_text('sage\'s dousing'/'MOR', 'Counter target spell unless its controller pays {3}. If you control a Wizard, draw a card.').
card_first_print('sage\'s dousing', 'MOR').
card_image_name('sage\'s dousing'/'MOR', 'sage\'s dousing').
card_uid('sage\'s dousing'/'MOR', 'MOR:Sage\'s Dousing:sage\'s dousing').
card_rarity('sage\'s dousing'/'MOR', 'Uncommon').
card_artist('sage\'s dousing'/'MOR', 'Richard Sardinha').
card_number('sage\'s dousing'/'MOR', '48').
card_flavor_text('sage\'s dousing'/'MOR', '\"Curse these merrows and their meddling! Since coming near the river, I can\'t so much as sneeze without being soaked.\"\n—Ashling the Pilgrim').
card_multiverse_id('sage\'s dousing'/'MOR', '152988').

card_in_set('scapeshift', 'MOR').
card_original_type('scapeshift'/'MOR', 'Sorcery').
card_original_text('scapeshift'/'MOR', 'Sacrifice any number of lands. Search your library for that many land cards, put them into play tapped, then shuffle your library.').
card_first_print('scapeshift', 'MOR').
card_image_name('scapeshift'/'MOR', 'scapeshift').
card_uid('scapeshift'/'MOR', 'MOR:Scapeshift:scapeshift').
card_rarity('scapeshift'/'MOR', 'Rare').
card_artist('scapeshift'/'MOR', 'Fred Fields').
card_number('scapeshift'/'MOR', '136').
card_flavor_text('scapeshift'/'MOR', '\"Changes far greater than the turning of the leaves await us at season\'s end.\"\n—Colfenor, the Last Yew').
card_multiverse_id('scapeshift'/'MOR', '146593').

card_in_set('scarblade elite', 'MOR').
card_original_type('scarblade elite'/'MOR', 'Creature — Elf Assassin').
card_original_text('scarblade elite'/'MOR', '{T}, Remove an Assassin card in your graveyard from the game: Destroy target creature.').
card_first_print('scarblade elite', 'MOR').
card_image_name('scarblade elite'/'MOR', 'scarblade elite').
card_uid('scarblade elite'/'MOR', 'MOR:Scarblade Elite:scarblade elite').
card_rarity('scarblade elite'/'MOR', 'Rare').
card_artist('scarblade elite'/'MOR', 'Greg Staples').
card_number('scarblade elite'/'MOR', '77').
card_flavor_text('scarblade elite'/'MOR', '\"Rejoice, eyeblight. In your last hours above the earth, those who bury you will finally make you beautiful.\"').
card_multiverse_id('scarblade elite'/'MOR', '153278').

card_in_set('seething pathblazer', 'MOR').
card_original_type('seething pathblazer'/'MOR', 'Creature — Elemental Warrior').
card_original_text('seething pathblazer'/'MOR', 'Sacrifice an Elemental: Seething Pathblazer gets +2/+0 and gains first strike until end of turn.').
card_first_print('seething pathblazer', 'MOR').
card_image_name('seething pathblazer'/'MOR', 'seething pathblazer').
card_uid('seething pathblazer'/'MOR', 'MOR:Seething Pathblazer:seething pathblazer').
card_rarity('seething pathblazer'/'MOR', 'Common').
card_artist('seething pathblazer'/'MOR', 'Steve Prescott').
card_number('seething pathblazer'/'MOR', '101').
card_flavor_text('seething pathblazer'/'MOR', '\"Flamekin death rites are elaborate and spectacular affairs. Their greatest fear is death in obscurity.\"\n—Tollek Worldwatcher, journal').
card_multiverse_id('seething pathblazer'/'MOR', '153101').

card_in_set('sensation gorger', 'MOR').
card_original_type('sensation gorger'/'MOR', 'Creature — Goblin Shaman').
card_original_text('sensation gorger'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Sensation Gorger, you may reveal it. If you do, each player discards his or her hand and draws four cards.').
card_first_print('sensation gorger', 'MOR').
card_image_name('sensation gorger'/'MOR', 'sensation gorger').
card_uid('sensation gorger'/'MOR', 'MOR:Sensation Gorger:sensation gorger').
card_rarity('sensation gorger'/'MOR', 'Rare').
card_artist('sensation gorger'/'MOR', 'Matt Cavotta').
card_number('sensation gorger'/'MOR', '102').
card_flavor_text('sensation gorger'/'MOR', 'More, more, more!').
card_multiverse_id('sensation gorger'/'MOR', '152539').

card_in_set('shard volley', 'MOR').
card_original_type('shard volley'/'MOR', 'Instant').
card_original_text('shard volley'/'MOR', 'As an additional cost to play Shard Volley, sacrifice a land.\nShard Volley deals 3 damage to target creature or player.').
card_first_print('shard volley', 'MOR').
card_image_name('shard volley'/'MOR', 'shard volley').
card_uid('shard volley'/'MOR', 'MOR:Shard Volley:shard volley').
card_rarity('shard volley'/'MOR', 'Common').
card_artist('shard volley'/'MOR', 'Franz Vohwinkel').
card_number('shard volley'/'MOR', '103').
card_flavor_text('shard volley'/'MOR', '\"Let the mountain\'s teeth pierce our oppressors.\"').
card_multiverse_id('shard volley'/'MOR', '152837').

card_in_set('shared animosity', 'MOR').
card_original_type('shared animosity'/'MOR', 'Enchantment').
card_original_text('shared animosity'/'MOR', 'Whenever a creature you control attacks, it gets +1/+0 until end of turn for each other attacking creature that shares a creature type with it.').
card_first_print('shared animosity', 'MOR').
card_image_name('shared animosity'/'MOR', 'shared animosity').
card_uid('shared animosity'/'MOR', 'MOR:Shared Animosity:shared animosity').
card_rarity('shared animosity'/'MOR', 'Rare').
card_artist('shared animosity'/'MOR', 'Chuck Lukacs').
card_number('shared animosity'/'MOR', '104').
card_flavor_text('shared animosity'/'MOR', '\"It is the nature of souls that they burn more brightly together than apart.\"\n—Vessifrus, flamekin demagogue').
card_multiverse_id('shared animosity'/'MOR', '157433').

card_in_set('shinewend', 'MOR').
card_original_type('shinewend'/'MOR', 'Creature — Elemental').
card_original_text('shinewend'/'MOR', 'Flying\nShinewend comes into play with a +1/+1 counter on it.\n{1}{W}, Remove a +1/+1 counter from Shinewend: Destroy target enchantment.').
card_first_print('shinewend', 'MOR').
card_image_name('shinewend'/'MOR', 'shinewend').
card_uid('shinewend'/'MOR', 'MOR:Shinewend:shinewend').
card_rarity('shinewend'/'MOR', 'Common').
card_artist('shinewend'/'MOR', 'Terese Nielsen').
card_number('shinewend'/'MOR', '23').
card_multiverse_id('shinewend'/'MOR', '152659').

card_in_set('sigil tracer', 'MOR').
card_original_type('sigil tracer'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('sigil tracer'/'MOR', '{1}{U}, Tap two untapped Wizards you control: Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('sigil tracer', 'MOR').
card_image_name('sigil tracer'/'MOR', 'sigil tracer').
card_uid('sigil tracer'/'MOR', 'MOR:Sigil Tracer:sigil tracer').
card_rarity('sigil tracer'/'MOR', 'Rare').
card_artist('sigil tracer'/'MOR', 'Dan Scott').
card_number('sigil tracer'/'MOR', '49').
card_flavor_text('sigil tracer'/'MOR', '\"The reflection is true. It is you who are distorted and false.\"').
card_multiverse_id('sigil tracer'/'MOR', '152590').

card_in_set('slithermuse', 'MOR').
card_original_type('slithermuse'/'MOR', 'Creature — Elemental').
card_original_text('slithermuse'/'MOR', 'When Slithermuse leaves play, choose an opponent. If that player has more cards in hand than you, draw cards equal to the difference.\nEvoke {3}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('slithermuse', 'MOR').
card_image_name('slithermuse'/'MOR', 'slithermuse').
card_uid('slithermuse'/'MOR', 'MOR:Slithermuse:slithermuse').
card_rarity('slithermuse'/'MOR', 'Rare').
card_artist('slithermuse'/'MOR', 'Steven Belledin').
card_number('slithermuse'/'MOR', '50').
card_multiverse_id('slithermuse'/'MOR', '157428').

card_in_set('spitebellows', 'MOR').
card_original_type('spitebellows'/'MOR', 'Creature — Elemental').
card_original_text('spitebellows'/'MOR', 'When Spitebellows leaves play, it deals 6 damage to target creature.\nEvoke {1}{R}{R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('spitebellows', 'MOR').
card_image_name('spitebellows'/'MOR', 'spitebellows').
card_uid('spitebellows'/'MOR', 'MOR:Spitebellows:spitebellows').
card_rarity('spitebellows'/'MOR', 'Uncommon').
card_artist('spitebellows'/'MOR', 'Larry MacDougall').
card_number('spitebellows'/'MOR', '105').
card_flavor_text('spitebellows'/'MOR', 'Disaster stalks with gaping jaws across unready lands.').
card_multiverse_id('spitebellows'/'MOR', '152887').

card_in_set('squeaking pie grubfellows', 'MOR').
card_original_type('squeaking pie grubfellows'/'MOR', 'Creature — Goblin Shaman').
card_original_text('squeaking pie grubfellows'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Squeaking Pie Grubfellows, you may reveal it. If you do, each opponent discards a card.').
card_first_print('squeaking pie grubfellows', 'MOR').
card_image_name('squeaking pie grubfellows'/'MOR', 'squeaking pie grubfellows').
card_uid('squeaking pie grubfellows'/'MOR', 'MOR:Squeaking Pie Grubfellows:squeaking pie grubfellows').
card_rarity('squeaking pie grubfellows'/'MOR', 'Common').
card_artist('squeaking pie grubfellows'/'MOR', 'Jim Pavelec').
card_number('squeaking pie grubfellows'/'MOR', '78').
card_multiverse_id('squeaking pie grubfellows'/'MOR', '152847').

card_in_set('stenchskipper', 'MOR').
card_original_type('stenchskipper'/'MOR', 'Creature — Elemental').
card_original_text('stenchskipper'/'MOR', 'Flying\nAt end of turn, if you control no Goblins, sacrifice Stenchskipper.').
card_first_print('stenchskipper', 'MOR').
card_image_name('stenchskipper'/'MOR', 'stenchskipper').
card_uid('stenchskipper'/'MOR', 'MOR:Stenchskipper:stenchskipper').
card_rarity('stenchskipper'/'MOR', 'Rare').
card_artist('stenchskipper'/'MOR', 'Howard Lyon').
card_number('stenchskipper'/'MOR', '79').
card_flavor_text('stenchskipper'/'MOR', '\"Many believe it to be the manifestation of all that is vile about the boggarts. I believe it should be avoided—from upwind, if possible.\"\n—Cenn Deagan').
card_multiverse_id('stenchskipper'/'MOR', '153126').

card_in_set('stingmoggie', 'MOR').
card_original_type('stingmoggie'/'MOR', 'Creature — Elemental').
card_original_text('stingmoggie'/'MOR', 'Stingmoggie comes into play with two +1/+1 counters on it.\n{3}{R}, Remove a +1/+1 counter from Stingmoggie: Destroy target artifact or land.').
card_first_print('stingmoggie', 'MOR').
card_image_name('stingmoggie'/'MOR', 'stingmoggie').
card_uid('stingmoggie'/'MOR', 'MOR:Stingmoggie:stingmoggie').
card_rarity('stingmoggie'/'MOR', 'Common').
card_artist('stingmoggie'/'MOR', 'Omar Rayyan').
card_number('stingmoggie'/'MOR', '106').
card_flavor_text('stingmoggie'/'MOR', 'A path vexed by the stingmoggie was probably the wrong path anyhow.').
card_multiverse_id('stingmoggie'/'MOR', '152704').

card_in_set('stinkdrinker bandit', 'MOR').
card_original_type('stinkdrinker bandit'/'MOR', 'Creature — Goblin Rogue').
card_original_text('stinkdrinker bandit'/'MOR', 'Prowl {1}{B} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhenever a Rogue you control attacks and isn\'t blocked, it gets +2/+1 until end of turn.').
card_first_print('stinkdrinker bandit', 'MOR').
card_image_name('stinkdrinker bandit'/'MOR', 'stinkdrinker bandit').
card_uid('stinkdrinker bandit'/'MOR', 'MOR:Stinkdrinker Bandit:stinkdrinker bandit').
card_rarity('stinkdrinker bandit'/'MOR', 'Uncommon').
card_artist('stinkdrinker bandit'/'MOR', 'Brandon Dorman').
card_number('stinkdrinker bandit'/'MOR', '80').
card_multiverse_id('stinkdrinker bandit'/'MOR', '152917').

card_in_set('stomping slabs', 'MOR').
card_original_type('stomping slabs'/'MOR', 'Sorcery').
card_original_text('stomping slabs'/'MOR', 'Reveal the top seven cards of your library, then put those cards on the bottom of your library in any order. If a card named Stomping Slabs was revealed this way, Stomping Slabs deals 7 damage to target creature or player.').
card_first_print('stomping slabs', 'MOR').
card_image_name('stomping slabs'/'MOR', 'stomping slabs').
card_uid('stomping slabs'/'MOR', 'MOR:Stomping Slabs:stomping slabs').
card_rarity('stomping slabs'/'MOR', 'Uncommon').
card_artist('stomping slabs'/'MOR', 'Alex Horley-Orlandelli').
card_number('stomping slabs'/'MOR', '107').
card_multiverse_id('stomping slabs'/'MOR', '157421').

card_in_set('stonehewer giant', 'MOR').
card_original_type('stonehewer giant'/'MOR', 'Creature — Giant Warrior').
card_original_text('stonehewer giant'/'MOR', 'Vigilance\n{1}{W}, {T}: Search your library for an Equipment card and put it into play. Attach it to a creature you control. Then shuffle your library.').
card_first_print('stonehewer giant', 'MOR').
card_image_name('stonehewer giant'/'MOR', 'stonehewer giant').
card_uid('stonehewer giant'/'MOR', 'MOR:Stonehewer Giant:stonehewer giant').
card_rarity('stonehewer giant'/'MOR', 'Rare').
card_artist('stonehewer giant'/'MOR', 'Steve Prescott').
card_number('stonehewer giant'/'MOR', '24').
card_flavor_text('stonehewer giant'/'MOR', '\"No matter how strong, an unarmed fighter is no more than a fool.\"').
card_multiverse_id('stonehewer giant'/'MOR', '152588').

card_in_set('stonybrook banneret', 'MOR').
card_original_type('stonybrook banneret'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('stonybrook banneret'/'MOR', 'Islandwalk\nMerfolk spells and Wizard spells you play cost {1} less to play.').
card_first_print('stonybrook banneret', 'MOR').
card_image_name('stonybrook banneret'/'MOR', 'stonybrook banneret').
card_uid('stonybrook banneret'/'MOR', 'MOR:Stonybrook Banneret:stonybrook banneret').
card_rarity('stonybrook banneret'/'MOR', 'Common').
card_artist('stonybrook banneret'/'MOR', 'Ralph Horsley').
card_number('stonybrook banneret'/'MOR', '51').
card_flavor_text('stonybrook banneret'/'MOR', 'Made from trout scales and crawfish whiskers, merrows\' shimmering banners flash in the water like lightning.').
card_multiverse_id('stonybrook banneret'/'MOR', '152652').

card_in_set('stonybrook schoolmaster', 'MOR').
card_original_type('stonybrook schoolmaster'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('stonybrook schoolmaster'/'MOR', 'Whenever Stonybrook Schoolmaster becomes tapped, you may put a 1/1 blue Merfolk Wizard creature token into play.').
card_first_print('stonybrook schoolmaster', 'MOR').
card_image_name('stonybrook schoolmaster'/'MOR', 'stonybrook schoolmaster').
card_uid('stonybrook schoolmaster'/'MOR', 'MOR:Stonybrook Schoolmaster:stonybrook schoolmaster').
card_rarity('stonybrook schoolmaster'/'MOR', 'Common').
card_artist('stonybrook schoolmaster'/'MOR', 'Quinton Hoover & Val Mayerik').
card_number('stonybrook schoolmaster'/'MOR', '25').
card_flavor_text('stonybrook schoolmaster'/'MOR', 'Merrow schools rarely form by design. They come together naturally as eager learners surround the wisest teachers.').
card_multiverse_id('stonybrook schoolmaster'/'MOR', '153166').

card_in_set('stream of unconsciousness', 'MOR').
card_original_type('stream of unconsciousness'/'MOR', 'Tribal Instant — Wizard').
card_original_text('stream of unconsciousness'/'MOR', 'Target creature gets -4/-0 until end of turn. If you control a Wizard, draw a card.').
card_first_print('stream of unconsciousness', 'MOR').
card_image_name('stream of unconsciousness'/'MOR', 'stream of unconsciousness').
card_uid('stream of unconsciousness'/'MOR', 'MOR:Stream of Unconsciousness:stream of unconsciousness').
card_rarity('stream of unconsciousness'/'MOR', 'Common').
card_artist('stream of unconsciousness'/'MOR', 'Rebecca Guay').
card_number('stream of unconsciousness'/'MOR', '52').
card_flavor_text('stream of unconsciousness'/'MOR', 'Dreams are tempting because one is so powerful in them. Dreams are dangerous because that power is a lie.').
card_multiverse_id('stream of unconsciousness'/'MOR', '152720').

card_in_set('sunflare shaman', 'MOR').
card_original_type('sunflare shaman'/'MOR', 'Creature — Elemental Shaman').
card_original_text('sunflare shaman'/'MOR', '{1}{R}, {T}: Sunflare Shaman deals X damage to target creature or player and X damage to itself, where X is the number of Elemental cards in your graveyard.').
card_first_print('sunflare shaman', 'MOR').
card_image_name('sunflare shaman'/'MOR', 'sunflare shaman').
card_uid('sunflare shaman'/'MOR', 'MOR:Sunflare Shaman:sunflare shaman').
card_rarity('sunflare shaman'/'MOR', 'Common').
card_artist('sunflare shaman'/'MOR', 'Dave Dorman').
card_number('sunflare shaman'/'MOR', '108').
card_flavor_text('sunflare shaman'/'MOR', 'A spark to light the guttered heart.').
card_multiverse_id('sunflare shaman'/'MOR', '153155').

card_in_set('supreme exemplar', 'MOR').
card_original_type('supreme exemplar'/'MOR', 'Creature — Elemental').
card_original_text('supreme exemplar'/'MOR', 'Flying\nChampion an Elemental (When this comes into play, sacrifice it unless you remove another Elemental you control from the game. When this leaves play, that card returns to play.)').
card_first_print('supreme exemplar', 'MOR').
card_image_name('supreme exemplar'/'MOR', 'supreme exemplar').
card_uid('supreme exemplar'/'MOR', 'MOR:Supreme Exemplar:supreme exemplar').
card_rarity('supreme exemplar'/'MOR', 'Rare').
card_artist('supreme exemplar'/'MOR', 'Mark Tedin').
card_number('supreme exemplar'/'MOR', '53').
card_multiverse_id('supreme exemplar'/'MOR', '152533').

card_in_set('swell of courage', 'MOR').
card_original_type('swell of courage'/'MOR', 'Instant').
card_original_text('swell of courage'/'MOR', 'Creatures you control get +2/+2 until end of turn.\nReinforce X—{X}{W}{W} ({X}{W}{W}, Discard this card: Put X +1/+1 counters on target creature.)').
card_first_print('swell of courage', 'MOR').
card_image_name('swell of courage'/'MOR', 'swell of courage').
card_uid('swell of courage'/'MOR', 'MOR:Swell of Courage:swell of courage').
card_rarity('swell of courage'/'MOR', 'Uncommon').
card_artist('swell of courage'/'MOR', 'Jim Nelson').
card_number('swell of courage'/'MOR', '26').
card_flavor_text('swell of courage'/'MOR', '\"Tideshaping is more than creating a few\nnew puddles.\"').
card_multiverse_id('swell of courage'/'MOR', '152729').

card_in_set('taurean mauler', 'MOR').
card_original_type('taurean mauler'/'MOR', 'Creature — Shapeshifter').
card_original_text('taurean mauler'/'MOR', 'Changeling (This card is every creature type at all times.)\nWhenever an opponent plays a spell, you may put a +1/+1 counter on Taurean Mauler.').
card_first_print('taurean mauler', 'MOR').
card_image_name('taurean mauler'/'MOR', 'taurean mauler').
card_uid('taurean mauler'/'MOR', 'MOR:Taurean Mauler:taurean mauler').
card_rarity('taurean mauler'/'MOR', 'Rare').
card_artist('taurean mauler'/'MOR', 'Dominick Domingo').
card_number('taurean mauler'/'MOR', '109').
card_flavor_text('taurean mauler'/'MOR', 'The power of a waterfall. The fury of an avalanche. The intellect of a gale-force wind.').
card_multiverse_id('taurean mauler'/'MOR', '153452').

card_in_set('thieves\' fortune', 'MOR').
card_original_type('thieves\' fortune'/'MOR', 'Tribal Instant — Rogue').
card_original_text('thieves\' fortune'/'MOR', 'Prowl {U} (You may play this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nLook at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('thieves\' fortune', 'MOR').
card_image_name('thieves\' fortune'/'MOR', 'thieves\' fortune').
card_uid('thieves\' fortune'/'MOR', 'MOR:Thieves\' Fortune:thieves\' fortune').
card_rarity('thieves\' fortune'/'MOR', 'Uncommon').
card_artist('thieves\' fortune'/'MOR', 'Zoltan Boros & Gabor Szikszai').
card_number('thieves\' fortune'/'MOR', '54').
card_multiverse_id('thieves\' fortune'/'MOR', '152815').

card_in_set('thornbite staff', 'MOR').
card_original_type('thornbite staff'/'MOR', 'Tribal Artifact — Shaman Equipment').
card_original_text('thornbite staff'/'MOR', 'Equipped creature has \"{2}, {T}: This creature deals 1 damage to target creature or player\" and \"Whenever a creature is put into a graveyard from play, untap this creature.\"\nWhenever a Shaman creature comes into play, you may attach Thornbite Staff to it.\nEquip {4}').
card_first_print('thornbite staff', 'MOR').
card_image_name('thornbite staff'/'MOR', 'thornbite staff').
card_uid('thornbite staff'/'MOR', 'MOR:Thornbite Staff:thornbite staff').
card_rarity('thornbite staff'/'MOR', 'Uncommon').
card_artist('thornbite staff'/'MOR', 'Jesper Ejsing').
card_number('thornbite staff'/'MOR', '145').
card_multiverse_id('thornbite staff'/'MOR', '152904').

card_in_set('titan\'s revenge', 'MOR').
card_original_type('titan\'s revenge'/'MOR', 'Sorcery').
card_original_text('titan\'s revenge'/'MOR', 'Titan\'s Revenge deals X damage to target creature or player. Clash with an opponent. If you win, return Titan\'s Revenge to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_first_print('titan\'s revenge', 'MOR').
card_image_name('titan\'s revenge'/'MOR', 'titan\'s revenge').
card_uid('titan\'s revenge'/'MOR', 'MOR:Titan\'s Revenge:titan\'s revenge').
card_rarity('titan\'s revenge'/'MOR', 'Rare').
card_artist('titan\'s revenge'/'MOR', 'Christopher Moeller').
card_number('titan\'s revenge'/'MOR', '110').
card_multiverse_id('titan\'s revenge'/'MOR', '152552').

card_in_set('unstoppable ash', 'MOR').
card_original_type('unstoppable ash'/'MOR', 'Creature — Treefolk Warrior').
card_original_text('unstoppable ash'/'MOR', 'Trample\nChampion a Treefolk or Warrior (When this comes into play, sacrifice it unless you remove another Treefolk or Warrior you control from the game. When this leaves play, that card returns to play.)\nWhenever a creature you control becomes blocked, it gets +0/+5 until end of turn.').
card_first_print('unstoppable ash', 'MOR').
card_image_name('unstoppable ash'/'MOR', 'unstoppable ash').
card_uid('unstoppable ash'/'MOR', 'MOR:Unstoppable Ash:unstoppable ash').
card_rarity('unstoppable ash'/'MOR', 'Rare').
card_artist('unstoppable ash'/'MOR', 'Brian Snõddy').
card_number('unstoppable ash'/'MOR', '137').
card_multiverse_id('unstoppable ash'/'MOR', '152937').

card_in_set('vendilion clique', 'MOR').
card_original_type('vendilion clique'/'MOR', 'Legendary Creature — Faerie Wizard').
card_original_text('vendilion clique'/'MOR', 'Flash\nFlying\nWhen Vendilion Clique comes into play, look at target player\'s hand. You may choose a nonland card from it. If you do, that player reveals the chosen card, puts it on the bottom of his or her library, then draws a card.').
card_image_name('vendilion clique'/'MOR', 'vendilion clique').
card_uid('vendilion clique'/'MOR', 'MOR:Vendilion Clique:vendilion clique').
card_rarity('vendilion clique'/'MOR', 'Rare').
card_artist('vendilion clique'/'MOR', 'Michael Sutfin').
card_number('vendilion clique'/'MOR', '55').
card_multiverse_id('vendilion clique'/'MOR', '152549').

card_in_set('vengeful firebrand', 'MOR').
card_original_type('vengeful firebrand'/'MOR', 'Creature — Elemental Warrior').
card_original_text('vengeful firebrand'/'MOR', 'Vengeful Firebrand has haste as long as a Warrior card is in your graveyard.\n{R}: Vengeful Firebrand gets +1/+0 until end of turn.').
card_first_print('vengeful firebrand', 'MOR').
card_image_name('vengeful firebrand'/'MOR', 'vengeful firebrand').
card_uid('vengeful firebrand'/'MOR', 'MOR:Vengeful Firebrand:vengeful firebrand').
card_rarity('vengeful firebrand'/'MOR', 'Rare').
card_artist('vengeful firebrand'/'MOR', 'William O\'Connor').
card_number('vengeful firebrand'/'MOR', '111').
card_flavor_text('vengeful firebrand'/'MOR', 'The flame of a warrior is never extinguished.').
card_multiverse_id('vengeful firebrand'/'MOR', '153153').

card_in_set('veteran\'s armaments', 'MOR').
card_original_type('veteran\'s armaments'/'MOR', 'Tribal Artifact — Soldier Equipment').
card_original_text('veteran\'s armaments'/'MOR', 'Equipped creature has \"Whenever this creature attacks or blocks, it gets +1/+1 until end of turn for each attacking creature.\"\nWhenever a Soldier creature comes into play, you may attach Veteran\'s Armaments to it.\nEquip {2}').
card_first_print('veteran\'s armaments', 'MOR').
card_image_name('veteran\'s armaments'/'MOR', 'veteran\'s armaments').
card_uid('veteran\'s armaments'/'MOR', 'MOR:Veteran\'s Armaments:veteran\'s armaments').
card_rarity('veteran\'s armaments'/'MOR', 'Uncommon').
card_artist('veteran\'s armaments'/'MOR', 'Alan Pollack').
card_number('veteran\'s armaments'/'MOR', '146').
card_multiverse_id('veteran\'s armaments'/'MOR', '153012').

card_in_set('violet pall', 'MOR').
card_original_type('violet pall'/'MOR', 'Tribal Instant — Faerie').
card_original_text('violet pall'/'MOR', 'Destroy target nonblack creature. Put a 1/1 black Faerie Rogue creature token with flying into play.').
card_first_print('violet pall', 'MOR').
card_image_name('violet pall'/'MOR', 'violet pall').
card_uid('violet pall'/'MOR', 'MOR:Violet Pall:violet pall').
card_rarity('violet pall'/'MOR', 'Common').
card_artist('violet pall'/'MOR', 'Jeff Miracola').
card_number('violet pall'/'MOR', '81').
card_flavor_text('violet pall'/'MOR', 'A faerie is the offspring of Oona and mischief.').
card_multiverse_id('violet pall'/'MOR', '157426').

card_in_set('walker of the grove', 'MOR').
card_original_type('walker of the grove'/'MOR', 'Creature — Elemental').
card_original_text('walker of the grove'/'MOR', 'When Walker of the Grove leaves play, put a 4/4 green Elemental creature token into play.\nEvoke {4}{G} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_first_print('walker of the grove', 'MOR').
card_image_name('walker of the grove'/'MOR', 'walker of the grove').
card_uid('walker of the grove'/'MOR', 'MOR:Walker of the Grove:walker of the grove').
card_rarity('walker of the grove'/'MOR', 'Uncommon').
card_artist('walker of the grove'/'MOR', 'Todd Lockwood').
card_number('walker of the grove'/'MOR', '138').
card_multiverse_id('walker of the grove'/'MOR', '152553').

card_in_set('wandering graybeard', 'MOR').
card_original_type('wandering graybeard'/'MOR', 'Creature — Giant Wizard').
card_original_text('wandering graybeard'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Wandering Graybeard, you may reveal it. If you do, you gain 4 life.').
card_first_print('wandering graybeard', 'MOR').
card_image_name('wandering graybeard'/'MOR', 'wandering graybeard').
card_uid('wandering graybeard'/'MOR', 'MOR:Wandering Graybeard:wandering graybeard').
card_rarity('wandering graybeard'/'MOR', 'Uncommon').
card_artist('wandering graybeard'/'MOR', 'Nils Hamm').
card_number('wandering graybeard'/'MOR', '27').
card_flavor_text('wandering graybeard'/'MOR', 'His tales are as tall as he is, and only a giant\'s life is long enough to listen to them all.').
card_multiverse_id('wandering graybeard'/'MOR', '152672').

card_in_set('war-spike changeling', 'MOR').
card_original_type('war-spike changeling'/'MOR', 'Creature — Shapeshifter').
card_original_text('war-spike changeling'/'MOR', 'Changeling (This card is every creature type at all times.)\n{R}: War-Spike Changeling gains first strike until end of turn.').
card_first_print('war-spike changeling', 'MOR').
card_image_name('war-spike changeling'/'MOR', 'war-spike changeling').
card_uid('war-spike changeling'/'MOR', 'MOR:War-Spike Changeling:war-spike changeling').
card_rarity('war-spike changeling'/'MOR', 'Common').
card_artist('war-spike changeling'/'MOR', 'Mark Poole').
card_number('war-spike changeling'/'MOR', '112').
card_flavor_text('war-spike changeling'/'MOR', '\"Aren\'t there boggarts enough in Lorwyn? Couldn\'t it turn into a sheep? Or a sunflower?\"\n—Olly of Goldmeadow').
card_multiverse_id('war-spike changeling'/'MOR', '143199').

card_in_set('warren weirding', 'MOR').
card_original_type('warren weirding'/'MOR', 'Tribal Sorcery — Goblin').
card_original_text('warren weirding'/'MOR', 'Target player sacrifices a creature. If a Goblin is sacrificed this way, that player puts two 1/1 black Goblin Rogue creature tokens into play, and those tokens gain haste until end of turn.').
card_first_print('warren weirding', 'MOR').
card_image_name('warren weirding'/'MOR', 'warren weirding').
card_uid('warren weirding'/'MOR', 'MOR:Warren Weirding:warren weirding').
card_rarity('warren weirding'/'MOR', 'Uncommon').
card_artist('warren weirding'/'MOR', 'Matt Cavotta').
card_number('warren weirding'/'MOR', '82').
card_flavor_text('warren weirding'/'MOR', '\"And that\'s when it was discovered that boggarts have just half a brain.\"\n—The Book of Other Folk').
card_multiverse_id('warren weirding'/'MOR', '152732').

card_in_set('waterspout weavers', 'MOR').
card_original_type('waterspout weavers'/'MOR', 'Creature — Merfolk Wizard').
card_original_text('waterspout weavers'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Waterspout Weavers, you may reveal it. If you do, each creature you control gains flying until end of turn.').
card_first_print('waterspout weavers', 'MOR').
card_image_name('waterspout weavers'/'MOR', 'waterspout weavers').
card_uid('waterspout weavers'/'MOR', 'MOR:Waterspout Weavers:waterspout weavers').
card_rarity('waterspout weavers'/'MOR', 'Uncommon').
card_artist('waterspout weavers'/'MOR', 'Eric Fortune').
card_number('waterspout weavers'/'MOR', '56').
card_multiverse_id('waterspout weavers'/'MOR', '152656').

card_in_set('weed-pruner poplar', 'MOR').
card_original_type('weed-pruner poplar'/'MOR', 'Creature — Treefolk Assassin').
card_original_text('weed-pruner poplar'/'MOR', 'At the beginning of your upkeep, target creature other than Weed-Pruner Poplar gets -1/-1 until end of turn.').
card_first_print('weed-pruner poplar', 'MOR').
card_image_name('weed-pruner poplar'/'MOR', 'weed-pruner poplar').
card_uid('weed-pruner poplar'/'MOR', 'MOR:Weed-Pruner Poplar:weed-pruner poplar').
card_rarity('weed-pruner poplar'/'MOR', 'Common').
card_artist('weed-pruner poplar'/'MOR', 'Jeff Miracola').
card_number('weed-pruner poplar'/'MOR', '83').
card_flavor_text('weed-pruner poplar'/'MOR', '\"When Byoog reached down to pull up a bit of root for the stew, he didn\'t expect the root to pull back.\"\n—A tale of Auntie Wort').
card_multiverse_id('weed-pruner poplar'/'MOR', '152741').

card_in_set('weight of conscience', 'MOR').
card_original_type('weight of conscience'/'MOR', 'Enchantment — Aura').
card_original_text('weight of conscience'/'MOR', 'Enchant creature\nEnchanted creature can\'t attack.\nTap two untapped creatures you control that share a creature type: Remove enchanted creature from the game.').
card_first_print('weight of conscience', 'MOR').
card_image_name('weight of conscience'/'MOR', 'weight of conscience').
card_uid('weight of conscience'/'MOR', 'MOR:Weight of Conscience:weight of conscience').
card_rarity('weight of conscience'/'MOR', 'Common').
card_artist('weight of conscience'/'MOR', 'Heather Hudson').
card_number('weight of conscience'/'MOR', '28').
card_flavor_text('weight of conscience'/'MOR', 'Sometimes the weight of the world on your shoulders is a literal one.').
card_multiverse_id('weight of conscience'/'MOR', '152737').

card_in_set('weirding shaman', 'MOR').
card_original_type('weirding shaman'/'MOR', 'Creature — Goblin Shaman').
card_original_text('weirding shaman'/'MOR', '{3}{B}, Sacrifice a Goblin: Put two 1/1 black Goblin Rogue creature tokens into play.').
card_first_print('weirding shaman', 'MOR').
card_image_name('weirding shaman'/'MOR', 'weirding shaman').
card_uid('weirding shaman'/'MOR', 'MOR:Weirding Shaman:weirding shaman').
card_rarity('weirding shaman'/'MOR', 'Rare').
card_artist('weirding shaman'/'MOR', 'Matt Cavotta').
card_number('weirding shaman'/'MOR', '84').
card_flavor_text('weirding shaman'/'MOR', 'All boggarts have an auntie, but they don\'t all have a mum.').
card_multiverse_id('weirding shaman'/'MOR', '153011').

card_in_set('winnower patrol', 'MOR').
card_original_type('winnower patrol'/'MOR', 'Creature — Elf Warrior').
card_original_text('winnower patrol'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Winnower Patrol, you may reveal it. If you do, put a +1/+1 counter on Winnower Patrol.').
card_first_print('winnower patrol', 'MOR').
card_image_name('winnower patrol'/'MOR', 'winnower patrol').
card_uid('winnower patrol'/'MOR', 'MOR:Winnower Patrol:winnower patrol').
card_rarity('winnower patrol'/'MOR', 'Common').
card_artist('winnower patrol'/'MOR', 'Eric Fortune').
card_number('winnower patrol'/'MOR', '139').
card_multiverse_id('winnower patrol'/'MOR', '152669').

card_in_set('wolf-skull shaman', 'MOR').
card_original_type('wolf-skull shaman'/'MOR', 'Creature — Elf Shaman').
card_original_text('wolf-skull shaman'/'MOR', 'Kinship At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Wolf-Skull Shaman, you may reveal it. If you do, put a 2/2 green Wolf creature token into play.').
card_first_print('wolf-skull shaman', 'MOR').
card_image_name('wolf-skull shaman'/'MOR', 'wolf-skull shaman').
card_uid('wolf-skull shaman'/'MOR', 'MOR:Wolf-Skull Shaman:wolf-skull shaman').
card_rarity('wolf-skull shaman'/'MOR', 'Uncommon').
card_artist('wolf-skull shaman'/'MOR', 'Jim Murray').
card_number('wolf-skull shaman'/'MOR', '140').
card_flavor_text('wolf-skull shaman'/'MOR', 'A chorus of howls answers his call.').
card_multiverse_id('wolf-skull shaman'/'MOR', '152728').
