% Judge Gift Program

set('pJGP').
set_name('pJGP', 'Judge Gift Program').
set_release_date('pJGP', '1998-06-01').
set_border('pJGP', 'black').
set_type('pJGP', 'promo').

card_in_set('argothian enchantress', 'pJGP').
card_original_type('argothian enchantress'/'pJGP', 'Creature — Human Druid').
card_original_text('argothian enchantress'/'pJGP', '').
card_first_print('argothian enchantress', 'pJGP').
card_image_name('argothian enchantress'/'pJGP', 'argothian enchantress').
card_uid('argothian enchantress'/'pJGP', 'pJGP:Argothian Enchantress:argothian enchantress').
card_rarity('argothian enchantress'/'pJGP', 'Special').
card_artist('argothian enchantress'/'pJGP', 'Daren Bader').
card_number('argothian enchantress'/'pJGP', '12').

card_in_set('armageddon', 'pJGP').
card_original_type('armageddon'/'pJGP', 'Sorcery').
card_original_text('armageddon'/'pJGP', '').
card_image_name('armageddon'/'pJGP', 'armageddon').
card_uid('armageddon'/'pJGP', 'pJGP:Armageddon:armageddon').
card_rarity('armageddon'/'pJGP', 'Special').
card_artist('armageddon'/'pJGP', 'John Avon').
card_number('armageddon'/'pJGP', '14').
card_flavor_text('armageddon'/'pJGP', '\"\'O miserable of happy! Is this the end/ Of this new glorious world... ?\'\"\n—John Milton, Paradise Lost').

card_in_set('balance', 'pJGP').
card_original_type('balance'/'pJGP', 'Sorcery').
card_original_text('balance'/'pJGP', '').
card_image_name('balance'/'pJGP', 'balance').
card_uid('balance'/'pJGP', 'pJGP:Balance:balance').
card_rarity('balance'/'pJGP', 'Special').
card_artist('balance'/'pJGP', 'Kev Walker').
card_number('balance'/'pJGP', '15').

card_in_set('ball lightning', 'pJGP').
card_original_type('ball lightning'/'pJGP', 'Creature — Elemental').
card_original_text('ball lightning'/'pJGP', '').
card_image_name('ball lightning'/'pJGP', 'ball lightning').
card_uid('ball lightning'/'pJGP', 'pJGP:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'pJGP', 'Special').
card_artist('ball lightning'/'pJGP', 'Quinton Hoover').
card_number('ball lightning'/'pJGP', '7').
card_flavor_text('ball lightning'/'pJGP', '\"Life, struck sharp on death, Makes awful lightning.\" - Elizabeth Barrett Browning, \"Aurora Leigh\"').

card_in_set('bitterblossom', 'pJGP').
card_original_type('bitterblossom'/'pJGP', 'Tribal Enchantment — Faerie').
card_original_text('bitterblossom'/'pJGP', '').
card_first_print('bitterblossom', 'pJGP').
card_image_name('bitterblossom'/'pJGP', 'bitterblossom').
card_uid('bitterblossom'/'pJGP', 'pJGP:Bitterblossom:bitterblossom').
card_rarity('bitterblossom'/'pJGP', 'Special').
card_artist('bitterblossom'/'pJGP', 'Nils Hamm').
card_number('bitterblossom'/'pJGP', '59').
card_flavor_text('bitterblossom'/'pJGP', 'In Lorwyn\'s brief evenings, the sun pauses at the horizon long enough for a certain species of violet to bloom with the fragrance of mischief.').

card_in_set('bloodstained mire', 'pJGP').
card_original_type('bloodstained mire'/'pJGP', 'Land').
card_original_text('bloodstained mire'/'pJGP', '').
card_first_print('bloodstained mire', 'pJGP').
card_image_name('bloodstained mire'/'pJGP', 'bloodstained mire').
card_uid('bloodstained mire'/'pJGP', 'pJGP:Bloodstained Mire:bloodstained mire').
card_rarity('bloodstained mire'/'pJGP', 'Special').
card_artist('bloodstained mire'/'pJGP', 'Rob Alexander').
card_number('bloodstained mire'/'pJGP', '43').

card_in_set('bribery', 'pJGP').
card_original_type('bribery'/'pJGP', 'Sorcery').
card_original_text('bribery'/'pJGP', '').
card_first_print('bribery', 'pJGP').
card_image_name('bribery'/'pJGP', 'bribery').
card_uid('bribery'/'pJGP', 'pJGP:Bribery:bribery').
card_rarity('bribery'/'pJGP', 'Special').
card_artist('bribery'/'pJGP', 'Cynthia Sheppard').
card_number('bribery'/'pJGP', '73').

card_in_set('burning wish', 'pJGP').
card_original_type('burning wish'/'pJGP', 'Sorcery').
card_original_text('burning wish'/'pJGP', '').
card_first_print('burning wish', 'pJGP').
card_image_name('burning wish'/'pJGP', 'burning wish').
card_uid('burning wish'/'pJGP', 'pJGP:Burning Wish:burning wish').
card_rarity('burning wish'/'pJGP', 'Special').
card_artist('burning wish'/'pJGP', 'Ariel Olivetti').
card_number('burning wish'/'pJGP', '42').

card_in_set('command tower', 'pJGP').
card_original_type('command tower'/'pJGP', 'Land').
card_original_text('command tower'/'pJGP', '').
card_first_print('command tower', 'pJGP').
card_image_name('command tower'/'pJGP', 'command tower').
card_uid('command tower'/'pJGP', 'pJGP:Command Tower:command tower').
card_rarity('command tower'/'pJGP', 'Special').
card_artist('command tower'/'pJGP', 'Ryan Yee').
card_number('command tower'/'pJGP', '71').
card_flavor_text('command tower'/'pJGP', 'When defeat is near and guidance is scarce, all eyes look in one direction.').

card_in_set('counterspell', 'pJGP').
card_original_type('counterspell'/'pJGP', 'Instant').
card_original_text('counterspell'/'pJGP', '').
card_image_name('counterspell'/'pJGP', 'counterspell').
card_uid('counterspell'/'pJGP', 'pJGP:Counterspell:counterspell').
card_rarity('counterspell'/'pJGP', 'Special').
card_artist('counterspell'/'pJGP', 'Dom!').
card_number('counterspell'/'pJGP', '5').

card_in_set('crucible of worlds', 'pJGP').
card_original_type('crucible of worlds'/'pJGP', 'Artifact').
card_original_text('crucible of worlds'/'pJGP', '').
card_first_print('crucible of worlds', 'pJGP').
card_image_name('crucible of worlds'/'pJGP', 'crucible of worlds').
card_uid('crucible of worlds'/'pJGP', 'pJGP:Crucible of Worlds:crucible of worlds').
card_rarity('crucible of worlds'/'pJGP', 'Special').
card_artist('crucible of worlds'/'pJGP', 'Ron Spencer').
card_number('crucible of worlds'/'pJGP', '75').
card_flavor_text('crucible of worlds'/'pJGP', 'Amidst the darkest ashes grow the strongest seeds.').

card_in_set('cunning wish', 'pJGP').
card_original_type('cunning wish'/'pJGP', 'Instant').
card_original_text('cunning wish'/'pJGP', '').
card_first_print('cunning wish', 'pJGP').
card_image_name('cunning wish'/'pJGP', 'cunning wish').
card_uid('cunning wish'/'pJGP', 'pJGP:Cunning Wish:cunning wish').
card_rarity('cunning wish'/'pJGP', 'Special').
card_artist('cunning wish'/'pJGP', 'Zoltan Boros & Gabor Szikszai').
card_number('cunning wish'/'pJGP', '29').
card_flavor_text('cunning wish'/'pJGP', 'The foolish wish for that which they desire. The cunning wish for that which they require.').

card_in_set('dark confidant', 'pJGP').
card_original_type('dark confidant'/'pJGP', 'Creature — Human Wizard').
card_original_text('dark confidant'/'pJGP', '').
card_first_print('dark confidant', 'pJGP').
card_image_name('dark confidant'/'pJGP', 'dark confidant').
card_uid('dark confidant'/'pJGP', 'pJGP:Dark Confidant:dark confidant').
card_rarity('dark confidant'/'pJGP', 'Special').
card_artist('dark confidant'/'pJGP', 'Ron Spears').
card_number('dark confidant'/'pJGP', '61').
card_flavor_text('dark confidant'/'pJGP', 'Greatness, at any cost.').

card_in_set('dark ritual', 'pJGP').
card_original_type('dark ritual'/'pJGP', 'Instant').
card_original_text('dark ritual'/'pJGP', '').
card_image_name('dark ritual'/'pJGP', 'dark ritual').
card_uid('dark ritual'/'pJGP', 'pJGP:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'pJGP', 'Special').
card_artist('dark ritual'/'pJGP', 'Mark Hyzer').
card_number('dark ritual'/'pJGP', '38').

card_in_set('decree of justice', 'pJGP').
card_original_type('decree of justice'/'pJGP', 'Sorcery').
card_original_text('decree of justice'/'pJGP', '').
card_first_print('decree of justice', 'pJGP').
card_image_name('decree of justice'/'pJGP', 'decree of justice').
card_uid('decree of justice'/'pJGP', 'pJGP:Decree of Justice:decree of justice').
card_rarity('decree of justice'/'pJGP', 'Special').
card_artist('decree of justice'/'pJGP', 'Alan Pollack').
card_number('decree of justice'/'pJGP', '32').

card_in_set('demonic tutor', 'pJGP').
card_original_type('demonic tutor'/'pJGP', 'Sorcery').
card_original_text('demonic tutor'/'pJGP', '').
card_image_name('demonic tutor'/'pJGP', 'demonic tutor').
card_uid('demonic tutor'/'pJGP', 'pJGP:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'pJGP', 'Special').
card_artist('demonic tutor'/'pJGP', 'Daarken').
card_number('demonic tutor'/'pJGP', '35').

card_in_set('deranged hermit', 'pJGP').
card_original_type('deranged hermit'/'pJGP', 'Creature — Elf').
card_original_text('deranged hermit'/'pJGP', '').
card_first_print('deranged hermit', 'pJGP').
card_image_name('deranged hermit'/'pJGP', 'deranged hermit').
card_uid('deranged hermit'/'pJGP', 'pJGP:Deranged Hermit:deranged hermit').
card_rarity('deranged hermit'/'pJGP', 'Special').
card_artist('deranged hermit'/'pJGP', 'Kev Walker').
card_number('deranged hermit'/'pJGP', '18').

card_in_set('doubling season', 'pJGP').
card_original_type('doubling season'/'pJGP', 'Enchantment').
card_original_text('doubling season'/'pJGP', '').
card_first_print('doubling season', 'pJGP').
card_image_name('doubling season'/'pJGP', 'doubling season').
card_uid('doubling season'/'pJGP', 'pJGP:Doubling Season:doubling season').
card_rarity('doubling season'/'pJGP', 'Special').
card_artist('doubling season'/'pJGP', 'Chuck Lukacs').
card_number('doubling season'/'pJGP', '62').

card_in_set('elesh norn, grand cenobite', 'pJGP').
card_original_type('elesh norn, grand cenobite'/'pJGP', 'Legendary Creature — Praetor').
card_original_text('elesh norn, grand cenobite'/'pJGP', '').
card_first_print('elesh norn, grand cenobite', 'pJGP').
card_image_name('elesh norn, grand cenobite'/'pJGP', 'elesh norn, grand cenobite').
card_uid('elesh norn, grand cenobite'/'pJGP', 'pJGP:Elesh Norn, Grand Cenobite:elesh norn, grand cenobite').
card_rarity('elesh norn, grand cenobite'/'pJGP', 'Special').
card_artist('elesh norn, grand cenobite'/'pJGP', 'Igor Kieryluk').
card_number('elesh norn, grand cenobite'/'pJGP', '87').
card_flavor_text('elesh norn, grand cenobite'/'pJGP', '\"The Gitaxians whisper among themselves of other worlds. If they exist, we must bring Phyrexia\'s magnificence to them.\"').

card_in_set('entomb', 'pJGP').
card_original_type('entomb'/'pJGP', 'Instant').
card_original_text('entomb'/'pJGP', '').
card_first_print('entomb', 'pJGP').
card_image_name('entomb'/'pJGP', 'entomb').
card_uid('entomb'/'pJGP', 'pJGP:Entomb:entomb').
card_rarity('entomb'/'pJGP', 'Special').
card_artist('entomb'/'pJGP', 'Ron Spears').
card_number('entomb'/'pJGP', '56').
card_flavor_text('entomb'/'pJGP', 'A grave is the safest place to store ill-gotten treasures.').

card_in_set('exalted angel', 'pJGP').
card_original_type('exalted angel'/'pJGP', 'Creature — Angel').
card_original_text('exalted angel'/'pJGP', '').
card_first_print('exalted angel', 'pJGP').
card_image_name('exalted angel'/'pJGP', 'exalted angel').
card_uid('exalted angel'/'pJGP', 'pJGP:Exalted Angel:exalted angel').
card_rarity('exalted angel'/'pJGP', 'Special').
card_artist('exalted angel'/'pJGP', 'Rob Alexander').
card_number('exalted angel'/'pJGP', '24').
card_flavor_text('exalted angel'/'pJGP', '\"She is a theme of honor and reknown, ...Whose present courage may beat down our foes.\"\n—William Shakespeare, Troilus and Cressida').

card_in_set('flooded strand', 'pJGP').
card_original_type('flooded strand'/'pJGP', 'Land').
card_original_text('flooded strand'/'pJGP', '').
card_first_print('flooded strand', 'pJGP').
card_image_name('flooded strand'/'pJGP', 'flooded strand').
card_uid('flooded strand'/'pJGP', 'pJGP:Flooded Strand:flooded strand').
card_rarity('flooded strand'/'pJGP', 'Special').
card_artist('flooded strand'/'pJGP', 'Rob Alexander').
card_number('flooded strand'/'pJGP', '44').

card_in_set('flusterstorm', 'pJGP').
card_original_type('flusterstorm'/'pJGP', 'Instant').
card_original_text('flusterstorm'/'pJGP', '').
card_first_print('flusterstorm', 'pJGP').
card_image_name('flusterstorm'/'pJGP', 'flusterstorm').
card_uid('flusterstorm'/'pJGP', 'pJGP:Flusterstorm:flusterstorm').
card_rarity('flusterstorm'/'pJGP', 'Special').
card_artist('flusterstorm'/'pJGP', 'Erica Yang').
card_number('flusterstorm'/'pJGP', '65').

card_in_set('force of will', 'pJGP').
card_original_type('force of will'/'pJGP', 'Instant').
card_original_text('force of will'/'pJGP', '').
card_image_name('force of will'/'pJGP', 'force of will').
card_uid('force of will'/'pJGP', 'pJGP:Force of Will:force of will').
card_rarity('force of will'/'pJGP', 'Special').
card_artist('force of will'/'pJGP', 'Matt Stewart').
card_number('force of will'/'pJGP', '83').

card_in_set('forest', 'pJGP').
card_original_type('forest'/'pJGP', 'Basic Land — Forest').
card_original_text('forest'/'pJGP', '').
card_image_name('forest'/'pJGP', 'forest').
card_uid('forest'/'pJGP', 'pJGP:Forest:forest').
card_rarity('forest'/'pJGP', 'Basic Land').
card_artist('forest'/'pJGP', 'Terese Nielsen').
card_number('forest'/'pJGP', '93').

card_in_set('gaea\'s cradle', 'pJGP').
card_original_type('gaea\'s cradle'/'pJGP', 'Legendary Land').
card_original_text('gaea\'s cradle'/'pJGP', '').
card_first_print('gaea\'s cradle', 'pJGP').
card_image_name('gaea\'s cradle'/'pJGP', 'gaea\'s cradle').
card_uid('gaea\'s cradle'/'pJGP', 'pJGP:Gaea\'s Cradle:gaea\'s cradle').
card_rarity('gaea\'s cradle'/'pJGP', 'Special').
card_artist('gaea\'s cradle'/'pJGP', 'Mark Zug').
card_number('gaea\'s cradle'/'pJGP', '3').
card_flavor_text('gaea\'s cradle'/'pJGP', '\"Here sprouted the first seedling of Argoth. Here the last tree will fall.\"\n—Gamelon, Citanul elder').

card_in_set('gemstone mine', 'pJGP').
card_original_type('gemstone mine'/'pJGP', 'Land').
card_original_text('gemstone mine'/'pJGP', '').
card_image_name('gemstone mine'/'pJGP', 'gemstone mine').
card_uid('gemstone mine'/'pJGP', 'pJGP:Gemstone Mine:gemstone mine').
card_rarity('gemstone mine'/'pJGP', 'Special').
card_artist('gemstone mine'/'pJGP', 'Brom').
card_number('gemstone mine'/'pJGP', '20').

card_in_set('genesis', 'pJGP').
card_original_type('genesis'/'pJGP', 'Creature — Incarnation').
card_original_text('genesis'/'pJGP', '').
card_first_print('genesis', 'pJGP').
card_image_name('genesis'/'pJGP', 'genesis').
card_uid('genesis'/'pJGP', 'pJGP:Genesis:genesis').
card_rarity('genesis'/'pJGP', 'Special').
card_artist('genesis'/'pJGP', 'Mark Zug').
card_number('genesis'/'pJGP', '79').
card_flavor_text('genesis'/'pJGP', '\"First through the Riftstone was Genesis—and the world was lifeless no more.\"\n—Scroll of Beginnings').

card_in_set('goblin piledriver', 'pJGP').
card_original_type('goblin piledriver'/'pJGP', 'Creature — Goblin Warrior').
card_original_text('goblin piledriver'/'pJGP', '').
card_first_print('goblin piledriver', 'pJGP').
card_image_name('goblin piledriver'/'pJGP', 'goblin piledriver').
card_uid('goblin piledriver'/'pJGP', 'pJGP:Goblin Piledriver:goblin piledriver').
card_rarity('goblin piledriver'/'pJGP', 'Special').
card_artist('goblin piledriver'/'pJGP', 'Dave Kendall').
card_number('goblin piledriver'/'pJGP', '36').

card_in_set('goblin welder', 'pJGP').
card_original_type('goblin welder'/'pJGP', 'Creature — Goblin Artificer').
card_original_text('goblin welder'/'pJGP', '').
card_first_print('goblin welder', 'pJGP').
card_image_name('goblin welder'/'pJGP', 'goblin welder').
card_uid('goblin welder'/'pJGP', 'pJGP:Goblin Welder:goblin welder').
card_rarity('goblin welder'/'pJGP', 'Special').
card_artist('goblin welder'/'pJGP', 'Scott M. Fischer').
card_number('goblin welder'/'pJGP', '63').
card_flavor_text('goblin welder'/'pJGP', '\"I wrecked your metal guy, boss. But look! I made you an ashtray.\"').

card_in_set('greater good', 'pJGP').
card_original_type('greater good'/'pJGP', 'Enchantment').
card_original_text('greater good'/'pJGP', '').
card_first_print('greater good', 'pJGP').
card_image_name('greater good'/'pJGP', 'greater good').
card_uid('greater good'/'pJGP', 'pJGP:Greater Good:greater good').
card_rarity('greater good'/'pJGP', 'Special').
card_artist('greater good'/'pJGP', 'Mathias Kollros').
card_number('greater good'/'pJGP', '81').
card_flavor_text('greater good'/'pJGP', '\"To examine the causes of life, we must first have recourse to death.\"\n—Mary Shelley, Frankenstein').

card_in_set('grim lavamancer', 'pJGP').
card_original_type('grim lavamancer'/'pJGP', 'Creature — Human Wizard').
card_original_text('grim lavamancer'/'pJGP', '').
card_first_print('grim lavamancer', 'pJGP').
card_image_name('grim lavamancer'/'pJGP', 'grim lavamancer').
card_uid('grim lavamancer'/'pJGP', 'pJGP:Grim Lavamancer:grim lavamancer').
card_rarity('grim lavamancer'/'pJGP', 'Special').
card_artist('grim lavamancer'/'pJGP', 'Michael Sutfin').
card_number('grim lavamancer'/'pJGP', '25').
card_flavor_text('grim lavamancer'/'pJGP', '\"Fools dig for water, corpses, or gold. The earth\'s real treasure is far deeper.\"').

card_in_set('hammer of bogardan', 'pJGP').
card_original_type('hammer of bogardan'/'pJGP', 'Sorcery').
card_original_text('hammer of bogardan'/'pJGP', '').
card_image_name('hammer of bogardan'/'pJGP', 'hammer of bogardan').
card_uid('hammer of bogardan'/'pJGP', 'pJGP:Hammer of Bogardan:hammer of bogardan').
card_rarity('hammer of bogardan'/'pJGP', 'Special').
card_artist('hammer of bogardan'/'pJGP', 'Ron Spencer').
card_number('hammer of bogardan'/'pJGP', '9').

card_in_set('hanna, ship\'s navigator', 'pJGP').
card_original_type('hanna, ship\'s navigator'/'pJGP', 'Legendary Creature — Human Artificer').
card_original_text('hanna, ship\'s navigator'/'pJGP', '').
card_first_print('hanna, ship\'s navigator', 'pJGP').
card_image_name('hanna, ship\'s navigator'/'pJGP', 'hanna, ship\'s navigator').
card_uid('hanna, ship\'s navigator'/'pJGP', 'pJGP:Hanna, Ship\'s Navigator:hanna, ship\'s navigator').
card_rarity('hanna, ship\'s navigator'/'pJGP', 'Special').
card_artist('hanna, ship\'s navigator'/'pJGP', 'Terese Nielsen').
card_number('hanna, ship\'s navigator'/'pJGP', '84').
card_flavor_text('hanna, ship\'s navigator'/'pJGP', '\"I never thought I\'d spend my life fighting. I\'m a maker, not a destroyer.\"').

card_in_set('hermit druid', 'pJGP').
card_original_type('hermit druid'/'pJGP', 'Creature — Human Druid').
card_original_text('hermit druid'/'pJGP', '').
card_image_name('hermit druid'/'pJGP', 'hermit druid').
card_uid('hermit druid'/'pJGP', 'pJGP:Hermit Druid:hermit druid').
card_rarity('hermit druid'/'pJGP', 'Special').
card_artist('hermit druid'/'pJGP', 'Heather Hudson').
card_number('hermit druid'/'pJGP', '19').
card_flavor_text('hermit druid'/'pJGP', 'Seeking the company of plants ensures that your wits will go to seed.').

card_in_set('imperial recruiter', 'pJGP').
card_original_type('imperial recruiter'/'pJGP', 'Creature — Human Advisor').
card_original_text('imperial recruiter'/'pJGP', '').
card_first_print('imperial recruiter', 'pJGP').
card_image_name('imperial recruiter'/'pJGP', 'imperial recruiter').
card_uid('imperial recruiter'/'pJGP', 'pJGP:Imperial Recruiter:imperial recruiter').
card_rarity('imperial recruiter'/'pJGP', 'Special').
card_artist('imperial recruiter'/'pJGP', 'Mitsuaki Sagiri').
card_number('imperial recruiter'/'pJGP', '74').

card_in_set('intuition', 'pJGP').
card_original_type('intuition'/'pJGP', 'Instant').
card_original_text('intuition'/'pJGP', '').
card_image_name('intuition'/'pJGP', 'intuition').
card_uid('intuition'/'pJGP', 'pJGP:Intuition:intuition').
card_rarity('intuition'/'pJGP', 'Special').
card_artist('intuition'/'pJGP', 'April Lee').
card_number('intuition'/'pJGP', '11').

card_in_set('island', 'pJGP').
card_original_type('island'/'pJGP', 'Basic Land — Island').
card_original_text('island'/'pJGP', '').
card_image_name('island'/'pJGP', 'island').
card_uid('island'/'pJGP', 'pJGP:Island:island').
card_rarity('island'/'pJGP', 'Basic Land').
card_artist('island'/'pJGP', 'Terese Nielsen').
card_number('island'/'pJGP', '90').

card_in_set('karador, ghost chieftain', 'pJGP').
card_original_type('karador, ghost chieftain'/'pJGP', 'Legendary Creature — Centaur Spirit').
card_original_text('karador, ghost chieftain'/'pJGP', '').
card_first_print('karador, ghost chieftain', 'pJGP').
card_image_name('karador, ghost chieftain'/'pJGP', 'karador, ghost chieftain').
card_uid('karador, ghost chieftain'/'pJGP', 'pJGP:Karador, Ghost Chieftain:karador, ghost chieftain').
card_rarity('karador, ghost chieftain'/'pJGP', 'Special').
card_artist('karador, ghost chieftain'/'pJGP', 'Todd Lockwood').
card_number('karador, ghost chieftain'/'pJGP', '80').
card_flavor_text('karador, ghost chieftain'/'pJGP', '\"Death tried to uncrown me. But now I return, king of a greater realm.\"').

card_in_set('karakas', 'pJGP').
card_original_type('karakas'/'pJGP', 'Legendary Land').
card_original_text('karakas'/'pJGP', '').
card_image_name('karakas'/'pJGP', 'karakas').
card_uid('karakas'/'pJGP', 'pJGP:Karakas:karakas').
card_rarity('karakas'/'pJGP', 'Special').
card_artist('karakas'/'pJGP', 'Drew Baker').
card_number('karakas'/'pJGP', '69').

card_in_set('karmic guide', 'pJGP').
card_original_type('karmic guide'/'pJGP', 'Creature — Angel Spirit').
card_original_text('karmic guide'/'pJGP', '').
card_first_print('karmic guide', 'pJGP').
card_image_name('karmic guide'/'pJGP', 'karmic guide').
card_uid('karmic guide'/'pJGP', 'pJGP:Karmic Guide:karmic guide').
card_rarity('karmic guide'/'pJGP', 'Special').
card_artist('karmic guide'/'pJGP', 'Allen Williams').
card_number('karmic guide'/'pJGP', '67').

card_in_set('land tax', 'pJGP').
card_original_type('land tax'/'pJGP', 'Enchantment').
card_original_text('land tax'/'pJGP', '').
card_image_name('land tax'/'pJGP', 'land tax').
card_uid('land tax'/'pJGP', 'pJGP:Land Tax:land tax').
card_rarity('land tax'/'pJGP', 'Special').
card_artist('land tax'/'pJGP', 'Chuck Lukacs').
card_number('land tax'/'pJGP', '52').

card_in_set('lightning bolt', 'pJGP').
card_original_type('lightning bolt'/'pJGP', 'Instant').
card_original_text('lightning bolt'/'pJGP', '').
card_image_name('lightning bolt'/'pJGP', 'lightning bolt').
card_uid('lightning bolt'/'pJGP', 'pJGP:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'pJGP', 'Special').
card_artist('lightning bolt'/'pJGP', 'Christopher Rush').
card_number('lightning bolt'/'pJGP', '1').

card_in_set('living death', 'pJGP').
card_original_type('living death'/'pJGP', 'Sorcery').
card_original_text('living death'/'pJGP', '').
card_image_name('living death'/'pJGP', 'living death').
card_uid('living death'/'pJGP', 'pJGP:Living Death:living death').
card_rarity('living death'/'pJGP', 'Special').
card_artist('living death'/'pJGP', 'Charles Gillespie').
card_number('living death'/'pJGP', '13').

card_in_set('living wish', 'pJGP').
card_original_type('living wish'/'pJGP', 'Sorcery').
card_original_text('living wish'/'pJGP', '').
card_first_print('living wish', 'pJGP').
card_image_name('living wish'/'pJGP', 'living wish').
card_uid('living wish'/'pJGP', 'pJGP:Living Wish:living wish').
card_rarity('living wish'/'pJGP', 'Special').
card_artist('living wish'/'pJGP', 'Hideaki Takamura').
card_number('living wish'/'pJGP', '37').

card_in_set('mana crypt', 'pJGP').
card_original_type('mana crypt'/'pJGP', 'Artifact').
card_original_text('mana crypt'/'pJGP', '').
card_image_name('mana crypt'/'pJGP', 'mana crypt').
card_uid('mana crypt'/'pJGP', 'pJGP:Mana Crypt:mana crypt').
card_rarity('mana crypt'/'pJGP', 'Special').
card_artist('mana crypt'/'pJGP', 'Matt Stewart').
card_number('mana crypt'/'pJGP', '60').

card_in_set('maze of ith', 'pJGP').
card_original_type('maze of ith'/'pJGP', 'Land').
card_original_text('maze of ith'/'pJGP', '').
card_image_name('maze of ith'/'pJGP', 'maze of ith').
card_uid('maze of ith'/'pJGP', 'pJGP:Maze of Ith:maze of ith').
card_rarity('maze of ith'/'pJGP', 'Special').
card_artist('maze of ith'/'pJGP', 'Zoltan Boros & Gabor Szikszai').
card_number('maze of ith'/'pJGP', '39').

card_in_set('meddling mage', 'pJGP').
card_original_type('meddling mage'/'pJGP', 'Creature — Human Wizard').
card_original_text('meddling mage'/'pJGP', '').
card_first_print('meddling mage', 'pJGP').
card_image_name('meddling mage'/'pJGP', 'meddling mage').
card_uid('meddling mage'/'pJGP', 'pJGP:Meddling Mage:meddling mage').
card_rarity('meddling mage'/'pJGP', 'Special').
card_artist('meddling mage'/'pJGP', 'Nottsuo').
card_number('meddling mage'/'pJGP', '26').
card_flavor_text('meddling mage'/'pJGP', 'Meddling mages chant so loudly that no one can get a spell in edgewise.').

card_in_set('memory lapse', 'pJGP').
card_original_type('memory lapse'/'pJGP', 'Instant').
card_original_text('memory lapse'/'pJGP', '').
card_image_name('memory lapse'/'pJGP', 'memory lapse').
card_uid('memory lapse'/'pJGP', 'pJGP:Memory Lapse:memory lapse').
card_rarity('memory lapse'/'pJGP', 'Special').
card_artist('memory lapse'/'pJGP', 'Mark Tedin').
card_number('memory lapse'/'pJGP', '4').
card_flavor_text('memory lapse'/'pJGP', '\"Um...oh...what was I saying?\"\n—Reveka, wizard savant').

card_in_set('mind\'s desire', 'pJGP').
card_original_type('mind\'s desire'/'pJGP', 'Sorcery').
card_original_text('mind\'s desire'/'pJGP', '').
card_first_print('mind\'s desire', 'pJGP').
card_image_name('mind\'s desire'/'pJGP', 'mind\'s desire').
card_uid('mind\'s desire'/'pJGP', 'pJGP:Mind\'s Desire:mind\'s desire').
card_rarity('mind\'s desire'/'pJGP', 'Special').
card_artist('mind\'s desire'/'pJGP', 'Anthony Francisco').
card_number('mind\'s desire'/'pJGP', '34').

card_in_set('mishra\'s factory', 'pJGP').
card_original_type('mishra\'s factory'/'pJGP', 'Land').
card_original_text('mishra\'s factory'/'pJGP', '').
card_image_name('mishra\'s factory'/'pJGP', 'mishra\'s factory').
card_uid('mishra\'s factory'/'pJGP', 'pJGP:Mishra\'s Factory:mishra\'s factory').
card_rarity('mishra\'s factory'/'pJGP', 'Special').
card_artist('mishra\'s factory'/'pJGP', 'Kaja & Phil Foglio').
card_number('mishra\'s factory'/'pJGP', '23').

card_in_set('morphling', 'pJGP').
card_original_type('morphling'/'pJGP', 'Creature — Shapeshifter').
card_original_text('morphling'/'pJGP', '').
card_first_print('morphling', 'pJGP').
card_image_name('morphling'/'pJGP', 'morphling').
card_uid('morphling'/'pJGP', 'pJGP:Morphling:morphling').
card_rarity('morphling'/'pJGP', 'Special').
card_artist('morphling'/'pJGP', 'rk post').
card_number('morphling'/'pJGP', '53').

card_in_set('mountain', 'pJGP').
card_original_type('mountain'/'pJGP', 'Basic Land — Mountain').
card_original_text('mountain'/'pJGP', '').
card_image_name('mountain'/'pJGP', 'mountain').
card_uid('mountain'/'pJGP', 'pJGP:Mountain:mountain').
card_rarity('mountain'/'pJGP', 'Basic Land').
card_artist('mountain'/'pJGP', 'Terese Nielsen').
card_number('mountain'/'pJGP', '92').

card_in_set('natural order', 'pJGP').
card_original_type('natural order'/'pJGP', 'Sorcery').
card_original_text('natural order'/'pJGP', '').
card_image_name('natural order'/'pJGP', 'natural order').
card_uid('natural order'/'pJGP', 'pJGP:Natural Order:natural order').
card_rarity('natural order'/'pJGP', 'Special').
card_artist('natural order'/'pJGP', 'Terese Nielsen').
card_number('natural order'/'pJGP', '49').
card_flavor_text('natural order'/'pJGP', '...but the price of Mangaras\'s freedom was Asmira\'s life.').

card_in_set('nekusar, the mindrazer', 'pJGP').
card_original_type('nekusar, the mindrazer'/'pJGP', 'Legendary Creature — Zombie Wizard').
card_original_text('nekusar, the mindrazer'/'pJGP', '').
card_first_print('nekusar, the mindrazer', 'pJGP').
card_image_name('nekusar, the mindrazer'/'pJGP', 'nekusar, the mindrazer').
card_uid('nekusar, the mindrazer'/'pJGP', 'pJGP:Nekusar, the Mindrazer:nekusar, the mindrazer').
card_rarity('nekusar, the mindrazer'/'pJGP', 'Special').
card_artist('nekusar, the mindrazer'/'pJGP', 'Mark Winters').
card_number('nekusar, the mindrazer'/'pJGP', '86').
card_flavor_text('nekusar, the mindrazer'/'pJGP', 'His enemies wondered if the lich king\'s brutal death and unnatural rebirth had been his plan all along.').

card_in_set('noble hierarch', 'pJGP').
card_original_type('noble hierarch'/'pJGP', 'Creature — Human Druid').
card_original_text('noble hierarch'/'pJGP', '').
card_first_print('noble hierarch', 'pJGP').
card_image_name('noble hierarch'/'pJGP', 'noble hierarch').
card_uid('noble hierarch'/'pJGP', 'pJGP:Noble Hierarch:noble hierarch').
card_rarity('noble hierarch'/'pJGP', 'Special').
card_artist('noble hierarch'/'pJGP', 'Mark Zug').
card_number('noble hierarch'/'pJGP', '66').
card_flavor_text('noble hierarch'/'pJGP', 'She protects the sacred groves from blight, drought, and the Unbeholden.').

card_in_set('oath of druids', 'pJGP').
card_original_type('oath of druids'/'pJGP', 'Enchantment').
card_original_text('oath of druids'/'pJGP', '').
card_first_print('oath of druids', 'pJGP').
card_image_name('oath of druids'/'pJGP', 'oath of druids').
card_uid('oath of druids'/'pJGP', 'pJGP:Oath of Druids:oath of druids').
card_rarity('oath of druids'/'pJGP', 'Special').
card_artist('oath of druids'/'pJGP', 'Daren Bader').
card_number('oath of druids'/'pJGP', '8').

card_in_set('oloro, ageless ascetic', 'pJGP').
card_original_type('oloro, ageless ascetic'/'pJGP', 'Legendary Creature — Giant Soldier').
card_original_text('oloro, ageless ascetic'/'pJGP', '').
card_first_print('oloro, ageless ascetic', 'pJGP').
card_image_name('oloro, ageless ascetic'/'pJGP', 'oloro, ageless ascetic').
card_uid('oloro, ageless ascetic'/'pJGP', 'pJGP:Oloro, Ageless Ascetic:oloro, ageless ascetic').
card_rarity('oloro, ageless ascetic'/'pJGP', 'Special').
card_artist('oloro, ageless ascetic'/'pJGP', 'Eric Deschamps').
card_number('oloro, ageless ascetic'/'pJGP', '88').

card_in_set('orim\'s chant', 'pJGP').
card_original_type('orim\'s chant'/'pJGP', 'Instant').
card_original_text('orim\'s chant'/'pJGP', '').
card_first_print('orim\'s chant', 'pJGP').
card_image_name('orim\'s chant'/'pJGP', 'orim\'s chant').
card_uid('orim\'s chant'/'pJGP', 'pJGP:Orim\'s Chant:orim\'s chant').
card_rarity('orim\'s chant'/'pJGP', 'Special').
card_artist('orim\'s chant'/'pJGP', 'Chippy').
card_number('orim\'s chant'/'pJGP', '33').

card_in_set('overwhelming forces', 'pJGP').
card_original_type('overwhelming forces'/'pJGP', 'Sorcery').
card_original_text('overwhelming forces'/'pJGP', '').
card_first_print('overwhelming forces', 'pJGP').
card_image_name('overwhelming forces'/'pJGP', 'overwhelming forces').
card_uid('overwhelming forces'/'pJGP', 'pJGP:Overwhelming Forces:overwhelming forces').
card_rarity('overwhelming forces'/'pJGP', 'Special').
card_artist('overwhelming forces'/'pJGP', 'Gao Yan').
card_number('overwhelming forces'/'pJGP', '76').
card_flavor_text('overwhelming forces'/'pJGP', 'By the year 208, Cao Cao commanded more than 1,000 experienced generals and a million infantry, cavalry, and naval troops.').

card_in_set('pernicious deed', 'pJGP').
card_original_type('pernicious deed'/'pJGP', 'Enchantment').
card_original_text('pernicious deed'/'pJGP', '').
card_first_print('pernicious deed', 'pJGP').
card_image_name('pernicious deed'/'pJGP', 'pernicious deed').
card_uid('pernicious deed'/'pJGP', 'pJGP:Pernicious Deed:pernicious deed').
card_rarity('pernicious deed'/'pJGP', 'Special').
card_artist('pernicious deed'/'pJGP', 'Ittoku').
card_number('pernicious deed'/'pJGP', '27').
card_flavor_text('pernicious deed'/'pJGP', '\"The tyrannous and bloody deed is done, The most arch deed of piteous massacre That ever yet this land was guilty of.\"\n—William Shakespeare, Richard III').

card_in_set('phyrexian dreadnought', 'pJGP').
card_original_type('phyrexian dreadnought'/'pJGP', 'Artifact Creature — Dreadnought').
card_original_text('phyrexian dreadnought'/'pJGP', '').
card_image_name('phyrexian dreadnought'/'pJGP', 'phyrexian dreadnought').
card_uid('phyrexian dreadnought'/'pJGP', 'pJGP:Phyrexian Dreadnought:phyrexian dreadnought').
card_rarity('phyrexian dreadnought'/'pJGP', 'Special').
card_artist('phyrexian dreadnought'/'pJGP', 'Pete Venters').
card_number('phyrexian dreadnought'/'pJGP', '50').

card_in_set('phyrexian negator', 'pJGP').
card_original_type('phyrexian negator'/'pJGP', 'Creature — Horror').
card_original_text('phyrexian negator'/'pJGP', '').
card_first_print('phyrexian negator', 'pJGP').
card_image_name('phyrexian negator'/'pJGP', 'phyrexian negator').
card_uid('phyrexian negator'/'pJGP', 'pJGP:Phyrexian Negator:phyrexian negator').
card_rarity('phyrexian negator'/'pJGP', 'Special').
card_artist('phyrexian negator'/'pJGP', 'John Zeleznik').
card_number('phyrexian negator'/'pJGP', '17').
card_flavor_text('phyrexian negator'/'pJGP', 'They exist to cease.').

card_in_set('plains', 'pJGP').
card_original_type('plains'/'pJGP', 'Basic Land — Plains').
card_original_text('plains'/'pJGP', '').
card_image_name('plains'/'pJGP', 'plains').
card_uid('plains'/'pJGP', 'pJGP:Plains:plains').
card_rarity('plains'/'pJGP', 'Basic Land').
card_artist('plains'/'pJGP', 'Terese Nielsen').
card_number('plains'/'pJGP', '89').

card_in_set('polluted delta', 'pJGP').
card_original_type('polluted delta'/'pJGP', 'Land').
card_original_text('polluted delta'/'pJGP', '').
card_first_print('polluted delta', 'pJGP').
card_image_name('polluted delta'/'pJGP', 'polluted delta').
card_uid('polluted delta'/'pJGP', 'pJGP:Polluted Delta:polluted delta').
card_rarity('polluted delta'/'pJGP', 'Special').
card_artist('polluted delta'/'pJGP', 'Rob Alexander').
card_number('polluted delta'/'pJGP', '45').

card_in_set('ravenous baloth', 'pJGP').
card_original_type('ravenous baloth'/'pJGP', 'Creature — Beast').
card_original_text('ravenous baloth'/'pJGP', '').
card_first_print('ravenous baloth', 'pJGP').
card_image_name('ravenous baloth'/'pJGP', 'ravenous baloth').
card_uid('ravenous baloth'/'pJGP', 'pJGP:Ravenous Baloth:ravenous baloth').
card_rarity('ravenous baloth'/'pJGP', 'Special').
card_artist('ravenous baloth'/'pJGP', 'Todd Lockwood').
card_number('ravenous baloth'/'pJGP', '28').
card_flavor_text('ravenous baloth'/'pJGP', '\"All we know about the Krosan Forest we have learned from those few who made it out alive.\"\n—Elvish refugee').

card_in_set('regrowth', 'pJGP').
card_original_type('regrowth'/'pJGP', 'Sorcery').
card_original_text('regrowth'/'pJGP', '').
card_image_name('regrowth'/'pJGP', 'regrowth').
card_uid('regrowth'/'pJGP', 'pJGP:Regrowth:regrowth').
card_rarity('regrowth'/'pJGP', 'Special').
card_artist('regrowth'/'pJGP', 'Dameon Willich').
card_number('regrowth'/'pJGP', '21').

card_in_set('riku of two reflections', 'pJGP').
card_original_type('riku of two reflections'/'pJGP', 'Legendary Creature — Human Wizard').
card_original_text('riku of two reflections'/'pJGP', '').
card_first_print('riku of two reflections', 'pJGP').
card_image_name('riku of two reflections'/'pJGP', 'riku of two reflections').
card_uid('riku of two reflections'/'pJGP', 'pJGP:Riku of Two Reflections:riku of two reflections').
card_rarity('riku of two reflections'/'pJGP', 'Special').
card_artist('riku of two reflections'/'pJGP', 'Izzy').
card_number('riku of two reflections'/'pJGP', '82').

card_in_set('show and tell', 'pJGP').
card_original_type('show and tell'/'pJGP', 'Sorcery').
card_original_text('show and tell'/'pJGP', '').
card_first_print('show and tell', 'pJGP').
card_image_name('show and tell'/'pJGP', 'show and tell').
card_uid('show and tell'/'pJGP', 'pJGP:Show and Tell:show and tell').
card_rarity('show and tell'/'pJGP', 'Special').
card_artist('show and tell'/'pJGP', 'Zack Stella').
card_number('show and tell'/'pJGP', '77').
card_flavor_text('show and tell'/'pJGP', 'At the academy, \"show and tell\" too often becomes \"run and hide.\"').

card_in_set('sinkhole', 'pJGP').
card_original_type('sinkhole'/'pJGP', 'Sorcery').
card_original_text('sinkhole'/'pJGP', '').
card_image_name('sinkhole'/'pJGP', 'sinkhole').
card_uid('sinkhole'/'pJGP', 'pJGP:Sinkhole:sinkhole').
card_rarity('sinkhole'/'pJGP', 'Special').
card_artist('sinkhole'/'pJGP', 'Dave Allsop').
card_number('sinkhole'/'pJGP', '48').

card_in_set('sneak attack', 'pJGP').
card_original_type('sneak attack'/'pJGP', 'Enchantment').
card_original_text('sneak attack'/'pJGP', '').
card_first_print('sneak attack', 'pJGP').
card_image_name('sneak attack'/'pJGP', 'sneak attack').
card_uid('sneak attack'/'pJGP', 'pJGP:Sneak Attack:sneak attack').
card_rarity('sneak attack'/'pJGP', 'Special').
card_artist('sneak attack'/'pJGP', 'Tyler Jacobson').
card_number('sneak attack'/'pJGP', '68').

card_in_set('sol ring', 'pJGP').
card_original_type('sol ring'/'pJGP', 'Artifact').
card_original_text('sol ring'/'pJGP', '').
card_image_name('sol ring'/'pJGP', 'sol ring').
card_uid('sol ring'/'pJGP', 'pJGP:Sol Ring:sol ring').
card_rarity('sol ring'/'pJGP', 'Special').
card_artist('sol ring'/'pJGP', 'Mark Tedin').
card_number('sol ring'/'pJGP', '22').

card_in_set('stifle', 'pJGP').
card_original_type('stifle'/'pJGP', 'Instant').
card_original_text('stifle'/'pJGP', '').
card_first_print('stifle', 'pJGP').
card_image_name('stifle'/'pJGP', 'stifle').
card_uid('stifle'/'pJGP', 'pJGP:Stifle:stifle').
card_rarity('stifle'/'pJGP', 'Special').
card_artist('stifle'/'pJGP', 'Eric Fortune').
card_number('stifle'/'pJGP', '40').

card_in_set('stroke of genius', 'pJGP').
card_original_type('stroke of genius'/'pJGP', 'Instant').
card_original_text('stroke of genius'/'pJGP', '').
card_first_print('stroke of genius', 'pJGP').
card_image_name('stroke of genius'/'pJGP', 'stroke of genius').
card_uid('stroke of genius'/'pJGP', 'pJGP:Stroke of Genius:stroke of genius').
card_rarity('stroke of genius'/'pJGP', 'Special').
card_artist('stroke of genius'/'pJGP', 'Stephen Daniele').
card_number('stroke of genius'/'pJGP', '2').
card_flavor_text('stroke of genius'/'pJGP', 'After a hundred failed experiments, Urza was stunned to find that common silver passed through the portal undamaged. He immediately designed a golem made of the metal.').

card_in_set('survival of the fittest', 'pJGP').
card_original_type('survival of the fittest'/'pJGP', 'Enchantment').
card_original_text('survival of the fittest'/'pJGP', '').
card_first_print('survival of the fittest', 'pJGP').
card_image_name('survival of the fittest'/'pJGP', 'survival of the fittest').
card_uid('survival of the fittest'/'pJGP', 'pJGP:Survival of the Fittest:survival of the fittest').
card_rarity('survival of the fittest'/'pJGP', 'Special').
card_artist('survival of the fittest'/'pJGP', 'Shelly Wan').
card_number('survival of the fittest'/'pJGP', '41').

card_in_set('swamp', 'pJGP').
card_original_type('swamp'/'pJGP', 'Basic Land — Swamp').
card_original_text('swamp'/'pJGP', '').
card_image_name('swamp'/'pJGP', 'swamp').
card_uid('swamp'/'pJGP', 'pJGP:Swamp:swamp').
card_rarity('swamp'/'pJGP', 'Basic Land').
card_artist('swamp'/'pJGP', 'Terese Nielsen').
card_number('swamp'/'pJGP', '91').

card_in_set('sword of feast and famine', 'pJGP').
card_original_type('sword of feast and famine'/'pJGP', 'Artifact — Equipment').
card_original_text('sword of feast and famine'/'pJGP', '').
card_first_print('sword of feast and famine', 'pJGP').
card_image_name('sword of feast and famine'/'pJGP', 'sword of feast and famine').
card_uid('sword of feast and famine'/'pJGP', 'pJGP:Sword of Feast and Famine:sword of feast and famine').
card_rarity('sword of feast and famine'/'pJGP', 'Special').
card_artist('sword of feast and famine'/'pJGP', 'Chris Rahn').
card_number('sword of feast and famine'/'pJGP', '85').

card_in_set('sword of fire and ice', 'pJGP').
card_original_type('sword of fire and ice'/'pJGP', 'Artifact — Equipment').
card_original_text('sword of fire and ice'/'pJGP', '').
card_first_print('sword of fire and ice', 'pJGP').
card_image_name('sword of fire and ice'/'pJGP', 'sword of fire and ice').
card_uid('sword of fire and ice'/'pJGP', 'pJGP:Sword of Fire and Ice:sword of fire and ice').
card_rarity('sword of fire and ice'/'pJGP', 'Special').
card_artist('sword of fire and ice'/'pJGP', 'Mark Zug').
card_number('sword of fire and ice'/'pJGP', '57').

card_in_set('sword of light and shadow', 'pJGP').
card_original_type('sword of light and shadow'/'pJGP', 'Artifact — Equipment').
card_original_text('sword of light and shadow'/'pJGP', '').
card_first_print('sword of light and shadow', 'pJGP').
card_image_name('sword of light and shadow'/'pJGP', 'sword of light and shadow').
card_uid('sword of light and shadow'/'pJGP', 'pJGP:Sword of Light and Shadow:sword of light and shadow').
card_rarity('sword of light and shadow'/'pJGP', 'Special').
card_artist('sword of light and shadow'/'pJGP', 'Mark Zug').
card_number('sword of light and shadow'/'pJGP', '70').

card_in_set('swords to plowshares', 'pJGP').
card_original_type('swords to plowshares'/'pJGP', 'Instant').
card_original_text('swords to plowshares'/'pJGP', '').
card_image_name('swords to plowshares'/'pJGP', 'swords to plowshares').
card_uid('swords to plowshares'/'pJGP', 'pJGP:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'pJGP', 'Special').
card_artist('swords to plowshares'/'pJGP', 'Sam Wolfe Connelly').
card_number('swords to plowshares'/'pJGP', '72').
card_flavor_text('swords to plowshares'/'pJGP', 'The smallest seed of regret can bloom into redemption.').

card_in_set('thawing glaciers', 'pJGP').
card_original_type('thawing glaciers'/'pJGP', 'Land').
card_original_text('thawing glaciers'/'pJGP', '').
card_image_name('thawing glaciers'/'pJGP', 'thawing glaciers').
card_uid('thawing glaciers'/'pJGP', 'pJGP:Thawing Glaciers:thawing glaciers').
card_rarity('thawing glaciers'/'pJGP', 'Special').
card_artist('thawing glaciers'/'pJGP', 'Jim Nelson').
card_number('thawing glaciers'/'pJGP', '51').

card_in_set('time warp', 'pJGP').
card_original_type('time warp'/'pJGP', 'Sorcery').
card_original_text('time warp'/'pJGP', '').
card_image_name('time warp'/'pJGP', 'time warp').
card_uid('time warp'/'pJGP', 'pJGP:Time Warp:time warp').
card_rarity('time warp'/'pJGP', 'Special').
card_artist('time warp'/'pJGP', 'Pete Venters').
card_number('time warp'/'pJGP', '16').
card_flavor_text('time warp'/'pJGP', '\"Let\'s do it again!\"\n—Squee, goblin cabin hand').

card_in_set('tradewind rider', 'pJGP').
card_original_type('tradewind rider'/'pJGP', 'Creature — Spirit').
card_original_text('tradewind rider'/'pJGP', '').
card_image_name('tradewind rider'/'pJGP', 'tradewind rider').
card_uid('tradewind rider'/'pJGP', 'pJGP:Tradewind Rider:tradewind rider').
card_rarity('tradewind rider'/'pJGP', 'Special').
card_artist('tradewind rider'/'pJGP', 'John Matson').
card_number('tradewind rider'/'pJGP', '10').
card_flavor_text('tradewind rider'/'pJGP', '\"\'Tis a shame, in such a tempest, to have but one anchor.\"\n—Laurence Sterne, Tristram Shandy').

card_in_set('vampiric tutor', 'pJGP').
card_original_type('vampiric tutor'/'pJGP', 'Instant').
card_original_text('vampiric tutor'/'pJGP', '').
card_image_name('vampiric tutor'/'pJGP', 'vampiric tutor').
card_uid('vampiric tutor'/'pJGP', 'pJGP:Vampiric Tutor:vampiric tutor').
card_rarity('vampiric tutor'/'pJGP', 'Special').
card_artist('vampiric tutor'/'pJGP', 'Gary Leach').
card_number('vampiric tutor'/'pJGP', '6').

card_in_set('vendilion clique', 'pJGP').
card_original_type('vendilion clique'/'pJGP', 'Legendary Creature — Faerie Wizard').
card_original_text('vendilion clique'/'pJGP', '').
card_first_print('vendilion clique', 'pJGP').
card_image_name('vendilion clique'/'pJGP', 'vendilion clique').
card_uid('vendilion clique'/'pJGP', 'pJGP:Vendilion Clique:vendilion clique').
card_rarity('vendilion clique'/'pJGP', 'Special').
card_artist('vendilion clique'/'pJGP', 'Jesper Ejsing').
card_number('vendilion clique'/'pJGP', '58').

card_in_set('vindicate', 'pJGP').
card_original_type('vindicate'/'pJGP', 'Sorcery').
card_original_text('vindicate'/'pJGP', '').
card_first_print('vindicate', 'pJGP').
card_image_name('vindicate'/'pJGP', 'vindicate1').
card_uid('vindicate'/'pJGP', 'pJGP:Vindicate:vindicate1').
card_rarity('vindicate'/'pJGP', 'Special').
card_artist('vindicate'/'pJGP', 'Mark Zug').
card_number('vindicate'/'pJGP', '31').
card_flavor_text('vindicate'/'pJGP', 'Some convictions are so strong that the world must break to accommodate them.').

card_in_set('vindicate', 'pJGP').
card_original_type('vindicate'/'pJGP', 'Sorcery').
card_original_text('vindicate'/'pJGP', '').
card_image_name('vindicate'/'pJGP', 'vindicate2').
card_uid('vindicate'/'pJGP', 'pJGP:Vindicate:vindicate2').
card_rarity('vindicate'/'pJGP', 'Special').
card_artist('vindicate'/'pJGP', 'Karla Ortiz').
card_number('vindicate'/'pJGP', '78').
card_flavor_text('vindicate'/'pJGP', '\"I have seen entire civilizations rise and fall. You mortals are but dust to me.\"\n—Sorin Markov').

card_in_set('wasteland', 'pJGP').
card_original_type('wasteland'/'pJGP', 'Land').
card_original_text('wasteland'/'pJGP', '').
card_image_name('wasteland'/'pJGP', 'wasteland').
card_uid('wasteland'/'pJGP', 'pJGP:Wasteland:wasteland').
card_rarity('wasteland'/'pJGP', 'Special').
card_artist('wasteland'/'pJGP', 'Carl Critchlow').
card_number('wasteland'/'pJGP', '55').
card_flavor_text('wasteland'/'pJGP', '\"The land promises nothing and keeps its promise.\" -Oracle en-Vec').

card_in_set('wheel of fortune', 'pJGP').
card_original_type('wheel of fortune'/'pJGP', 'Sorcery').
card_original_text('wheel of fortune'/'pJGP', '').
card_image_name('wheel of fortune'/'pJGP', 'wheel of fortune').
card_uid('wheel of fortune'/'pJGP', 'pJGP:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'pJGP', 'Special').
card_artist('wheel of fortune'/'pJGP', 'John Matson').
card_number('wheel of fortune'/'pJGP', '54').

card_in_set('windswept heath', 'pJGP').
card_original_type('windswept heath'/'pJGP', 'Land').
card_original_text('windswept heath'/'pJGP', '').
card_first_print('windswept heath', 'pJGP').
card_image_name('windswept heath'/'pJGP', 'windswept heath').
card_uid('windswept heath'/'pJGP', 'pJGP:Windswept Heath:windswept heath').
card_rarity('windswept heath'/'pJGP', 'Special').
card_artist('windswept heath'/'pJGP', 'Anthony S. Waters').
card_number('windswept heath'/'pJGP', '46').

card_in_set('wooded foothills', 'pJGP').
card_original_type('wooded foothills'/'pJGP', 'Land').
card_original_text('wooded foothills'/'pJGP', '').
card_first_print('wooded foothills', 'pJGP').
card_image_name('wooded foothills'/'pJGP', 'wooded foothills').
card_uid('wooded foothills'/'pJGP', 'pJGP:Wooded Foothills:wooded foothills').
card_rarity('wooded foothills'/'pJGP', 'Special').
card_artist('wooded foothills'/'pJGP', 'Rob Alexander').
card_number('wooded foothills'/'pJGP', '47').

card_in_set('xiahou dun, the one-eyed', 'pJGP').
card_original_type('xiahou dun, the one-eyed'/'pJGP', 'Legendary Creature — Human Soldier').
card_original_text('xiahou dun, the one-eyed'/'pJGP', '').
card_first_print('xiahou dun, the one-eyed', 'pJGP').
card_image_name('xiahou dun, the one-eyed'/'pJGP', 'xiahou dun, the one-eyed').
card_uid('xiahou dun, the one-eyed'/'pJGP', 'pJGP:Xiahou Dun, the One-Eyed:xiahou dun, the one-eyed').
card_rarity('xiahou dun, the one-eyed'/'pJGP', 'Special').
card_artist('xiahou dun, the one-eyed'/'pJGP', 'Junko Taguchi').
card_number('xiahou dun, the one-eyed'/'pJGP', '64').

card_in_set('yawgmoth\'s will', 'pJGP').
card_original_type('yawgmoth\'s will'/'pJGP', 'Sorcery').
card_original_text('yawgmoth\'s will'/'pJGP', '').
card_first_print('yawgmoth\'s will', 'pJGP').
card_image_name('yawgmoth\'s will'/'pJGP', 'yawgmoth\'s will').
card_uid('yawgmoth\'s will'/'pJGP', 'pJGP:Yawgmoth\'s Will:yawgmoth\'s will').
card_rarity('yawgmoth\'s will'/'pJGP', 'Special').
card_artist('yawgmoth\'s will'/'pJGP', 'Ron Spencer').
card_number('yawgmoth\'s will'/'pJGP', '30').
