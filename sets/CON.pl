% Conflux

set('CON').
set_name('CON', 'Conflux').
set_release_date('CON', '2009-02-06').
set_border('CON', 'black').
set_type('CON', 'expansion').
set_block('CON', 'Alara').

card_in_set('absorb vis', 'CON').
card_original_type('absorb vis'/'CON', 'Sorcery').
card_original_text('absorb vis'/'CON', 'Target player loses 4 life and you gain 4 life.\nBasic landcycling {1}{B} ({1}{B}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('absorb vis', 'CON').
card_image_name('absorb vis'/'CON', 'absorb vis').
card_uid('absorb vis'/'CON', 'CON:Absorb Vis:absorb vis').
card_rarity('absorb vis'/'CON', 'Common').
card_artist('absorb vis'/'CON', 'Brandon Kitkouski').
card_number('absorb vis'/'CON', '40').
card_multiverse_id('absorb vis'/'CON', '179513').

card_in_set('aerie mystics', 'CON').
card_original_type('aerie mystics'/'CON', 'Creature — Bird Wizard').
card_original_text('aerie mystics'/'CON', 'Flying\n{1}{G}{U}: Creatures you control gain shroud until end of turn.').
card_first_print('aerie mystics', 'CON').
card_image_name('aerie mystics'/'CON', 'aerie mystics').
card_uid('aerie mystics'/'CON', 'CON:Aerie Mystics:aerie mystics').
card_rarity('aerie mystics'/'CON', 'Uncommon').
card_artist('aerie mystics'/'CON', 'Mark Zug').
card_number('aerie mystics'/'CON', '1').
card_flavor_text('aerie mystics'/'CON', 'They are cautious with their body language and facial expressions. Any stray movement could betray the positions of the troops they protect and cost many lives.').
card_multiverse_id('aerie mystics'/'CON', '153946').

card_in_set('ancient ziggurat', 'CON').
card_original_type('ancient ziggurat'/'CON', 'Land').
card_original_text('ancient ziggurat'/'CON', '{T}: Add one mana of any color to your mana pool. Spend this mana only to play creature spells.').
card_image_name('ancient ziggurat'/'CON', 'ancient ziggurat').
card_uid('ancient ziggurat'/'CON', 'CON:Ancient Ziggurat:ancient ziggurat').
card_rarity('ancient ziggurat'/'CON', 'Uncommon').
card_artist('ancient ziggurat'/'CON', 'John Avon').
card_number('ancient ziggurat'/'CON', '141').
card_flavor_text('ancient ziggurat'/'CON', 'Built in honor of Alara\'s creatures, the ziggurat vanished long ago. When Progenitus awakened, the temple emerged again.').
card_multiverse_id('ancient ziggurat'/'CON', '189271').

card_in_set('apocalypse hydra', 'CON').
card_original_type('apocalypse hydra'/'CON', 'Creature — Hydra').
card_original_text('apocalypse hydra'/'CON', 'Apocalypse Hydra comes into play with X +1/+1 counters on it. If X is 5 or more, it comes into play with an additional X +1/+1 counters on it.\n{1}{R}, Remove a +1/+1 counter from Apocalypse Hydra: Apocalypse Hydra deals 1 damage to target creature or player.').
card_first_print('apocalypse hydra', 'CON').
card_image_name('apocalypse hydra'/'CON', 'apocalypse hydra').
card_uid('apocalypse hydra'/'CON', 'CON:Apocalypse Hydra:apocalypse hydra').
card_rarity('apocalypse hydra'/'CON', 'Mythic Rare').
card_artist('apocalypse hydra'/'CON', 'Jason Chan').
card_number('apocalypse hydra'/'CON', '98').
card_multiverse_id('apocalypse hydra'/'CON', '183068').

card_in_set('armillary sphere', 'CON').
card_original_type('armillary sphere'/'CON', 'Artifact').
card_original_text('armillary sphere'/'CON', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('armillary sphere', 'CON').
card_image_name('armillary sphere'/'CON', 'armillary sphere').
card_uid('armillary sphere'/'CON', 'CON:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'CON', 'Common').
card_artist('armillary sphere'/'CON', 'Franz Vohwinkel').
card_number('armillary sphere'/'CON', '134').
card_flavor_text('armillary sphere'/'CON', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'CON', '137936').

card_in_set('asha\'s favor', 'CON').
card_original_type('asha\'s favor'/'CON', 'Enchantment — Aura').
card_original_text('asha\'s favor'/'CON', 'Enchant creature\nEnchanted creature has flying, first strike, and vigilance.').
card_first_print('asha\'s favor', 'CON').
card_image_name('asha\'s favor'/'CON', 'asha\'s favor').
card_uid('asha\'s favor'/'CON', 'CON:Asha\'s Favor:asha\'s favor').
card_rarity('asha\'s favor'/'CON', 'Common').
card_artist('asha\'s favor'/'CON', 'Donato Giancola').
card_number('asha\'s favor'/'CON', '2').
card_flavor_text('asha\'s favor'/'CON', 'As his new wings lifted him high above Bant, Taric felt his earthly aspirations transform into heavenly resolve.').
card_multiverse_id('asha\'s favor'/'CON', '150826').

card_in_set('aven squire', 'CON').
card_original_type('aven squire'/'CON', 'Creature — Bird Soldier').
card_original_text('aven squire'/'CON', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('aven squire', 'CON').
card_image_name('aven squire'/'CON', 'aven squire').
card_uid('aven squire'/'CON', 'CON:Aven Squire:aven squire').
card_rarity('aven squire'/'CON', 'Common').
card_artist('aven squire'/'CON', 'David Palumbo').
card_number('aven squire'/'CON', '3').
card_flavor_text('aven squire'/'CON', 'When the meek charge into battle, courage becomes infectious.').
card_multiverse_id('aven squire'/'CON', '184992').

card_in_set('aven trailblazer', 'CON').
card_original_type('aven trailblazer'/'CON', 'Creature — Bird Soldier').
card_original_text('aven trailblazer'/'CON', 'Flying\nDomain Aven Trailblazer\'s toughness is equal to the number of basic land types among lands you control.').
card_first_print('aven trailblazer', 'CON').
card_image_name('aven trailblazer'/'CON', 'aven trailblazer').
card_uid('aven trailblazer'/'CON', 'CON:Aven Trailblazer:aven trailblazer').
card_rarity('aven trailblazer'/'CON', 'Common').
card_artist('aven trailblazer'/'CON', 'Chris Rahn').
card_number('aven trailblazer'/'CON', '4').
card_flavor_text('aven trailblazer'/'CON', '\"The bird wore the form of a man, bereft of filigree. Why do the Texts not speak of it?\"\n—Belator of Esper').
card_multiverse_id('aven trailblazer'/'CON', '180278').

card_in_set('banefire', 'CON').
card_original_type('banefire'/'CON', 'Sorcery').
card_original_text('banefire'/'CON', 'Banefire deals X damage to target creature or player.\nIf X is 5 or more, Banefire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_first_print('banefire', 'CON').
card_image_name('banefire'/'CON', 'banefire').
card_uid('banefire'/'CON', 'CON:Banefire:banefire').
card_rarity('banefire'/'CON', 'Rare').
card_artist('banefire'/'CON', 'Raymond Swanland').
card_number('banefire'/'CON', '58').
card_flavor_text('banefire'/'CON', 'For Sarkhan Vol, the dragon is the purest expression of life\'s savage splendor.').
card_multiverse_id('banefire'/'CON', '186613').

card_in_set('beacon behemoth', 'CON').
card_original_type('beacon behemoth'/'CON', 'Creature — Beast').
card_original_text('beacon behemoth'/'CON', '{1}: Target creature with power 5 or greater gains vigilance until end of turn.').
card_first_print('beacon behemoth', 'CON').
card_image_name('beacon behemoth'/'CON', 'beacon behemoth').
card_uid('beacon behemoth'/'CON', 'CON:Beacon Behemoth:beacon behemoth').
card_rarity('beacon behemoth'/'CON', 'Common').
card_artist('beacon behemoth'/'CON', 'Jesper Ejsing').
card_number('beacon behemoth'/'CON', '78').
card_flavor_text('beacon behemoth'/'CON', 'When its smoky plumes light Naya\'s sky, every creature from the smallest pip fawn to the largest rannet heeds the warning.').
card_multiverse_id('beacon behemoth'/'CON', '177931').

card_in_set('blood tyrant', 'CON').
card_original_type('blood tyrant'/'CON', 'Creature — Vampire').
card_original_text('blood tyrant'/'CON', 'Flying, trample\nAt the beginning of your upkeep, each player loses 1 life. Put a +1/+1 counter on Blood Tyrant for each 1 life lost this way.\nWhenever a player loses the game, put five +1/+1 counters on Blood Tyrant.').
card_first_print('blood tyrant', 'CON').
card_image_name('blood tyrant'/'CON', 'blood tyrant').
card_uid('blood tyrant'/'CON', 'CON:Blood Tyrant:blood tyrant').
card_rarity('blood tyrant'/'CON', 'Rare').
card_artist('blood tyrant'/'CON', 'Karl Kopinski').
card_number('blood tyrant'/'CON', '99').
card_multiverse_id('blood tyrant'/'CON', '180267').

card_in_set('bloodhall ooze', 'CON').
card_original_type('bloodhall ooze'/'CON', 'Creature — Ooze').
card_original_text('bloodhall ooze'/'CON', 'At the beginning of your upkeep, if you control a black permanent, you may put a +1/+1 counter on Bloodhall Ooze.\nAt the beginning of your upkeep, if you control a green permanent, you may put a +1/+1 counter on Bloodhall Ooze.').
card_first_print('bloodhall ooze', 'CON').
card_image_name('bloodhall ooze'/'CON', 'bloodhall ooze').
card_uid('bloodhall ooze'/'CON', 'CON:Bloodhall Ooze:bloodhall ooze').
card_rarity('bloodhall ooze'/'CON', 'Rare').
card_artist('bloodhall ooze'/'CON', 'Jaime Jones').
card_number('bloodhall ooze'/'CON', '59').
card_flavor_text('bloodhall ooze'/'CON', 'A drop of blood spilled from the first dragon.').
card_multiverse_id('bloodhall ooze'/'CON', '159610').

card_in_set('bone saw', 'CON').
card_original_type('bone saw'/'CON', 'Artifact — Equipment').
card_original_text('bone saw'/'CON', 'Equipped creature gets +1/+0.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('bone saw', 'CON').
card_image_name('bone saw'/'CON', 'bone saw').
card_uid('bone saw'/'CON', 'CON:Bone Saw:bone saw').
card_rarity('bone saw'/'CON', 'Common').
card_artist('bone saw'/'CON', 'Pete Venters').
card_number('bone saw'/'CON', '135').
card_flavor_text('bone saw'/'CON', 'In a world where death is always violent, cruel weapons are as common as rocks.').
card_multiverse_id('bone saw'/'CON', '189270').

card_in_set('brackwater elemental', 'CON').
card_original_type('brackwater elemental'/'CON', 'Creature — Elemental').
card_original_text('brackwater elemental'/'CON', 'When Brackwater Elemental attacks or blocks, sacrifice it at end of turn.\nUnearth {2}{U} ({2}{U}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('brackwater elemental', 'CON').
card_image_name('brackwater elemental'/'CON', 'brackwater elemental').
card_uid('brackwater elemental'/'CON', 'CON:Brackwater Elemental:brackwater elemental').
card_rarity('brackwater elemental'/'CON', 'Common').
card_artist('brackwater elemental'/'CON', 'Thomas M. Baxa').
card_number('brackwater elemental'/'CON', '21').
card_multiverse_id('brackwater elemental'/'CON', '180147').

card_in_set('canyon minotaur', 'CON').
card_original_type('canyon minotaur'/'CON', 'Creature — Minotaur Warrior').
card_original_text('canyon minotaur'/'CON', '').
card_first_print('canyon minotaur', 'CON').
card_image_name('canyon minotaur'/'CON', 'canyon minotaur').
card_uid('canyon minotaur'/'CON', 'CON:Canyon Minotaur:canyon minotaur').
card_rarity('canyon minotaur'/'CON', 'Common').
card_artist('canyon minotaur'/'CON', 'Steve Prescott').
card_number('canyon minotaur'/'CON', '60').
card_flavor_text('canyon minotaur'/'CON', 'On Jund, the deep canyons were the best places to hide. When the goblins wandered into Naya, they found that was not so true.').
card_multiverse_id('canyon minotaur'/'CON', '184994').

card_in_set('celestial purge', 'CON').
card_original_type('celestial purge'/'CON', 'Instant').
card_original_text('celestial purge'/'CON', 'Remove target black or red permanent from the game.').
card_image_name('celestial purge'/'CON', 'celestial purge').
card_uid('celestial purge'/'CON', 'CON:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'CON', 'Uncommon').
card_artist('celestial purge'/'CON', 'David Palumbo').
card_number('celestial purge'/'CON', '5').
card_flavor_text('celestial purge'/'CON', '\"This new chaos confounds us. We must fling it into our winds, our storms. These we can control.\"\n—Bezzit Plar, Esper stormcaller').
card_multiverse_id('celestial purge'/'CON', '183055').

card_in_set('charnelhoard wurm', 'CON').
card_original_type('charnelhoard wurm'/'CON', 'Creature — Wurm').
card_original_text('charnelhoard wurm'/'CON', 'Trample\nWhenever Charnelhoard Wurm deals damage to an opponent, you may return target card from your graveyard to your hand.').
card_first_print('charnelhoard wurm', 'CON').
card_image_name('charnelhoard wurm'/'CON', 'charnelhoard wurm').
card_uid('charnelhoard wurm'/'CON', 'CON:Charnelhoard Wurm:charnelhoard wurm').
card_rarity('charnelhoard wurm'/'CON', 'Rare').
card_artist('charnelhoard wurm'/'CON', 'Lars Grant-West').
card_number('charnelhoard wurm'/'CON', '100').
card_flavor_text('charnelhoard wurm'/'CON', 'Jund\'s dragons hoard only sangrite crystals. Its wurms aren\'t so picky.').
card_multiverse_id('charnelhoard wurm'/'CON', '179438').

card_in_set('child of alara', 'CON').
card_original_type('child of alara'/'CON', 'Legendary Creature — Avatar').
card_original_text('child of alara'/'CON', 'Trample\nWhen Child of Alara is put into a graveyard from play, destroy all nonland permanents. They can\'t be regenerated.').
card_first_print('child of alara', 'CON').
card_image_name('child of alara'/'CON', 'child of alara').
card_uid('child of alara'/'CON', 'CON:Child of Alara:child of alara').
card_rarity('child of alara'/'CON', 'Mythic Rare').
card_artist('child of alara'/'CON', 'Steve Argyle').
card_number('child of alara'/'CON', '101').
card_flavor_text('child of alara'/'CON', 'The progeny of the Maelstrom shows no allegiance—and no mercy—to any of the five shards.').
card_multiverse_id('child of alara'/'CON', '180516').

card_in_set('cliffrunner behemoth', 'CON').
card_original_type('cliffrunner behemoth'/'CON', 'Creature — Rhino Beast').
card_original_text('cliffrunner behemoth'/'CON', 'Cliffrunner Behemoth has haste as long as you control a red permanent.\nCliffrunner Behemoth has lifelink as long as you control a white permanent.').
card_first_print('cliffrunner behemoth', 'CON').
card_image_name('cliffrunner behemoth'/'CON', 'cliffrunner behemoth').
card_uid('cliffrunner behemoth'/'CON', 'CON:Cliffrunner Behemoth:cliffrunner behemoth').
card_rarity('cliffrunner behemoth'/'CON', 'Rare').
card_artist('cliffrunner behemoth'/'CON', 'Wayne Reynolds').
card_number('cliffrunner behemoth'/'CON', '79').
card_flavor_text('cliffrunner behemoth'/'CON', 'It\'s revered for its power, celebrated for its grace, and feared for the avalanches triggered by its thunderous feet.').
card_multiverse_id('cliffrunner behemoth'/'CON', '159612').

card_in_set('conflux', 'CON').
card_original_type('conflux'/'CON', 'Sorcery').
card_original_text('conflux'/'CON', 'Search your library for a white card, a blue card, a black card, a red card, and a green card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_first_print('conflux', 'CON').
card_image_name('conflux'/'CON', 'conflux').
card_uid('conflux'/'CON', 'CON:Conflux:conflux').
card_rarity('conflux'/'CON', 'Mythic Rare').
card_artist('conflux'/'CON', 'Karl Kopinski').
card_number('conflux'/'CON', '102').
card_flavor_text('conflux'/'CON', 'After years of world-bending machinations, Bolas\'s triumph is at hand.').
card_multiverse_id('conflux'/'CON', '179437').

card_in_set('constricting tendrils', 'CON').
card_original_type('constricting tendrils'/'CON', 'Instant').
card_original_text('constricting tendrils'/'CON', 'Target creature gets -3/-0 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('constricting tendrils', 'CON').
card_image_name('constricting tendrils'/'CON', 'constricting tendrils').
card_uid('constricting tendrils'/'CON', 'CON:Constricting Tendrils:constricting tendrils').
card_rarity('constricting tendrils'/'CON', 'Common').
card_artist('constricting tendrils'/'CON', 'David Palumbo').
card_number('constricting tendrils'/'CON', '22').
card_flavor_text('constricting tendrils'/'CON', 'Priests of Bant protect their temples with traps more elaborate than any mosaic floor.').
card_multiverse_id('constricting tendrils'/'CON', '180272').

card_in_set('controlled instincts', 'CON').
card_original_type('controlled instincts'/'CON', 'Enchantment — Aura').
card_original_text('controlled instincts'/'CON', 'Enchant red or green creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('controlled instincts', 'CON').
card_image_name('controlled instincts'/'CON', 'controlled instincts').
card_uid('controlled instincts'/'CON', 'CON:Controlled Instincts:controlled instincts').
card_rarity('controlled instincts'/'CON', 'Uncommon').
card_artist('controlled instincts'/'CON', 'Ralph Horsley').
card_number('controlled instincts'/'CON', '23').
card_flavor_text('controlled instincts'/'CON', 'Togk Manytooth struggled helplessly as the kathari circled above him. For a warrior, immobilization is a fate worse than death.').
card_multiverse_id('controlled instincts'/'CON', '183053').

card_in_set('corrupted roots', 'CON').
card_original_type('corrupted roots'/'CON', 'Enchantment — Aura').
card_original_text('corrupted roots'/'CON', 'Enchant Forest or Plains\nWhenever enchanted land becomes tapped, its controller loses 2 life.').
card_first_print('corrupted roots', 'CON').
card_image_name('corrupted roots'/'CON', 'corrupted roots').
card_uid('corrupted roots'/'CON', 'CON:Corrupted Roots:corrupted roots').
card_rarity('corrupted roots'/'CON', 'Uncommon').
card_artist('corrupted roots'/'CON', 'Mark Hyzer').
card_number('corrupted roots'/'CON', '41').
card_flavor_text('corrupted roots'/'CON', 'The bones of Jund\'s dead resurfaced in Naya, poisoning the jungle and killing the sunseeders\' crops.').
card_multiverse_id('corrupted roots'/'CON', '183054').

card_in_set('countersquall', 'CON').
card_original_type('countersquall'/'CON', 'Instant').
card_original_text('countersquall'/'CON', 'Counter target noncreature spell. Its controller loses 2 life.').
card_first_print('countersquall', 'CON').
card_image_name('countersquall'/'CON', 'countersquall').
card_uid('countersquall'/'CON', 'CON:Countersquall:countersquall').
card_rarity('countersquall'/'CON', 'Uncommon').
card_artist('countersquall'/'CON', 'Anthony Francisco').
card_number('countersquall'/'CON', '103').
card_flavor_text('countersquall'/'CON', 'Each of the twenty-three winds of Esper is named and chronicled, and every possible interaction with the flow of magic is exhaustively detailed.').
card_multiverse_id('countersquall'/'CON', '186327').

card_in_set('court homunculus', 'CON').
card_original_type('court homunculus'/'CON', 'Artifact Creature — Homunculus').
card_original_text('court homunculus'/'CON', 'Court Homunculus gets +1/+1 as long as you control another artifact.').
card_first_print('court homunculus', 'CON').
card_image_name('court homunculus'/'CON', 'court homunculus').
card_uid('court homunculus'/'CON', 'CON:Court Homunculus:court homunculus').
card_rarity('court homunculus'/'CON', 'Common').
card_artist('court homunculus'/'CON', 'Matt Cavotta').
card_number('court homunculus'/'CON', '6').
card_flavor_text('court homunculus'/'CON', 'Mages of Esper measure their wealth and status by the number of servants in their retinues.').
card_multiverse_id('court homunculus'/'CON', '182607').

card_in_set('cumber stone', 'CON').
card_original_type('cumber stone'/'CON', 'Artifact').
card_original_text('cumber stone'/'CON', 'Creatures your opponents control get -1/-0.').
card_first_print('cumber stone', 'CON').
card_image_name('cumber stone'/'CON', 'cumber stone').
card_uid('cumber stone'/'CON', 'CON:Cumber Stone:cumber stone').
card_rarity('cumber stone'/'CON', 'Uncommon').
card_artist('cumber stone'/'CON', 'Warren Mahy').
card_number('cumber stone'/'CON', '24').
card_flavor_text('cumber stone'/'CON', '\"The stone is more potent than ever. The flesh that creeps into our land will become even more slovenly and weak.\"\n—Niclavs, archmage of Esper').
card_multiverse_id('cumber stone'/'CON', '180322').

card_in_set('cylian sunsinger', 'CON').
card_original_type('cylian sunsinger'/'CON', 'Creature — Elf Shaman').
card_original_text('cylian sunsinger'/'CON', '{R}{G}{W}: Cylian Sunsinger and each other creature with the same name as it get +3/+3 until end of turn.').
card_first_print('cylian sunsinger', 'CON').
card_image_name('cylian sunsinger'/'CON', 'cylian sunsinger').
card_uid('cylian sunsinger'/'CON', 'CON:Cylian Sunsinger:cylian sunsinger').
card_rarity('cylian sunsinger'/'CON', 'Rare').
card_artist('cylian sunsinger'/'CON', 'Jesper Ejsing').
card_number('cylian sunsinger'/'CON', '80').
card_flavor_text('cylian sunsinger'/'CON', '\"The sun shines bright upon the strong.\"').
card_multiverse_id('cylian sunsinger'/'CON', '189081').

card_in_set('dark temper', 'CON').
card_original_type('dark temper'/'CON', 'Instant').
card_original_text('dark temper'/'CON', 'Dark Temper deals 2 damage to target creature. If you control a black permanent, destroy the creature instead.').
card_first_print('dark temper', 'CON').
card_image_name('dark temper'/'CON', 'dark temper').
card_uid('dark temper'/'CON', 'CON:Dark Temper:dark temper').
card_rarity('dark temper'/'CON', 'Common').
card_artist('dark temper'/'CON', 'Jaime Jones').
card_number('dark temper'/'CON', '61').
card_flavor_text('dark temper'/'CON', 'When you\'ve got the temperament of a dragon, every argument is one you\'ll win.').
card_multiverse_id('dark temper'/'CON', '185143').

card_in_set('darklit gargoyle', 'CON').
card_original_type('darklit gargoyle'/'CON', 'Artifact Creature — Gargoyle').
card_original_text('darklit gargoyle'/'CON', 'Flying\n{B}: Darklit Gargoyle gets +2/-1 until end of turn.').
card_first_print('darklit gargoyle', 'CON').
card_image_name('darklit gargoyle'/'CON', 'darklit gargoyle').
card_uid('darklit gargoyle'/'CON', 'CON:Darklit Gargoyle:darklit gargoyle').
card_rarity('darklit gargoyle'/'CON', 'Common').
card_artist('darklit gargoyle'/'CON', 'Howard Lyon').
card_number('darklit gargoyle'/'CON', '7').
card_flavor_text('darklit gargoyle'/'CON', 'It shines in the darkness of its master\'s ambitions.').
card_multiverse_id('darklit gargoyle'/'CON', '180334').

card_in_set('drag down', 'CON').
card_original_type('drag down'/'CON', 'Instant').
card_original_text('drag down'/'CON', 'Domain Target creature gets -1/-1 until end of turn for each basic land type among lands you control.').
card_first_print('drag down', 'CON').
card_image_name('drag down'/'CON', 'drag down').
card_uid('drag down'/'CON', 'CON:Drag Down:drag down').
card_rarity('drag down'/'CON', 'Common').
card_artist('drag down'/'CON', 'Trevor Claxton').
card_number('drag down'/'CON', '42').
card_flavor_text('drag down'/'CON', 'The barbarians of Jund believe the bottomless tar pits extend forever into other, darker worlds.').
card_multiverse_id('drag down'/'CON', '179239').

card_in_set('dragonsoul knight', 'CON').
card_original_type('dragonsoul knight'/'CON', 'Creature — Human Knight').
card_original_text('dragonsoul knight'/'CON', 'First strike\n{W}{U}{B}{R}{G}: Until end of turn, Dragonsoul Knight becomes a Dragon, gets +5/+3, and gains flying and trample.').
card_first_print('dragonsoul knight', 'CON').
card_image_name('dragonsoul knight'/'CON', 'dragonsoul knight').
card_uid('dragonsoul knight'/'CON', 'CON:Dragonsoul Knight:dragonsoul knight').
card_rarity('dragonsoul knight'/'CON', 'Uncommon').
card_artist('dragonsoul knight'/'CON', 'Justin Sweet').
card_number('dragonsoul knight'/'CON', '62').
card_flavor_text('dragonsoul knight'/'CON', 'The farther he roamed from Jund and its dragons, the more he felt their essence in himself.').
card_multiverse_id('dragonsoul knight'/'CON', '186392').

card_in_set('dreadwing', 'CON').
card_original_type('dreadwing'/'CON', 'Creature — Zombie').
card_original_text('dreadwing'/'CON', '{1}{U}{R}: Dreadwing gets +3/+0 and gains flying until end of turn.').
card_first_print('dreadwing', 'CON').
card_image_name('dreadwing'/'CON', 'dreadwing').
card_uid('dreadwing'/'CON', 'CON:Dreadwing:dreadwing').
card_rarity('dreadwing'/'CON', 'Uncommon').
card_artist('dreadwing'/'CON', 'Mark Hyzer').
card_number('dreadwing'/'CON', '43').
card_flavor_text('dreadwing'/'CON', 'Dreadwings spring from lofty perches to surprise kathari in midflight. They smother their prey and then consume it as they glide gently toward the ground.').
card_multiverse_id('dreadwing'/'CON', '159629').

card_in_set('elder mastery', 'CON').
card_original_type('elder mastery'/'CON', 'Enchantment — Aura').
card_original_text('elder mastery'/'CON', 'Enchant creature\nEnchanted creature gets +3/+3 and has flying.\nWhenever enchanted creature deals damage to a player, that player discards two cards.').
card_first_print('elder mastery', 'CON').
card_image_name('elder mastery'/'CON', 'elder mastery').
card_uid('elder mastery'/'CON', 'CON:Elder Mastery:elder mastery').
card_rarity('elder mastery'/'CON', 'Uncommon').
card_artist('elder mastery'/'CON', 'Dave Allsop').
card_number('elder mastery'/'CON', '104').
card_flavor_text('elder mastery'/'CON', 'Taste his power, hunger for his command.').
card_multiverse_id('elder mastery'/'CON', '183024').

card_in_set('ember weaver', 'CON').
card_original_type('ember weaver'/'CON', 'Creature — Spider').
card_original_text('ember weaver'/'CON', 'Reach (This can block creatures with flying.)\nAs long as you control a red permanent, Ember Weaver gets +1/+0 and has first strike.').
card_first_print('ember weaver', 'CON').
card_image_name('ember weaver'/'CON', 'ember weaver').
card_uid('ember weaver'/'CON', 'CON:Ember Weaver:ember weaver').
card_rarity('ember weaver'/'CON', 'Common').
card_artist('ember weaver'/'CON', 'Steve Prescott').
card_number('ember weaver'/'CON', '81').
card_flavor_text('ember weaver'/'CON', '\"Each night, the sun unravels and blows away. Each day, the spiders set a new one in the sky.\"\n—Sunseeder myth').
card_multiverse_id('ember weaver'/'CON', '184990').

card_in_set('esper cormorants', 'CON').
card_original_type('esper cormorants'/'CON', 'Artifact Creature — Bird').
card_original_text('esper cormorants'/'CON', 'Flying').
card_first_print('esper cormorants', 'CON').
card_image_name('esper cormorants'/'CON', 'esper cormorants').
card_uid('esper cormorants'/'CON', 'CON:Esper Cormorants:esper cormorants').
card_rarity('esper cormorants'/'CON', 'Common').
card_artist('esper cormorants'/'CON', 'Warren Mahy').
card_number('esper cormorants'/'CON', '105').
card_flavor_text('esper cormorants'/'CON', '\"The smiths of this land must be mad to reach so far and so high for another creature to decorate.\"\n—Cagen Vargan, Jhessian sea scout').
card_multiverse_id('esper cormorants'/'CON', '184983').

card_in_set('esperzoa', 'CON').
card_original_type('esperzoa'/'CON', 'Artifact Creature — Jellyfish').
card_original_text('esperzoa'/'CON', 'Flying\nAt the beginning of your upkeep, return an artifact you control to its owner\'s hand.').
card_first_print('esperzoa', 'CON').
card_image_name('esperzoa'/'CON', 'esperzoa').
card_uid('esperzoa'/'CON', 'CON:Esperzoa:esperzoa').
card_rarity('esperzoa'/'CON', 'Uncommon').
card_artist('esperzoa'/'CON', 'Warren Mahy').
card_number('esperzoa'/'CON', '25').
card_flavor_text('esperzoa'/'CON', 'The more metal it digests, the more its jelly will fetch on the alchemists\' market.').
card_multiverse_id('esperzoa'/'CON', '180158').

card_in_set('ethersworn adjudicator', 'CON').
card_original_type('ethersworn adjudicator'/'CON', 'Artifact Creature — Vedalken Knight').
card_original_text('ethersworn adjudicator'/'CON', 'Flying\n{1}{W}{B}, {T}: Destroy target creature or enchantment.\n{2}{U}: Untap Ethersworn Adjudicator.').
card_first_print('ethersworn adjudicator', 'CON').
card_image_name('ethersworn adjudicator'/'CON', 'ethersworn adjudicator').
card_uid('ethersworn adjudicator'/'CON', 'CON:Ethersworn Adjudicator:ethersworn adjudicator').
card_rarity('ethersworn adjudicator'/'CON', 'Mythic Rare').
card_artist('ethersworn adjudicator'/'CON', 'Dan Scott').
card_number('ethersworn adjudicator'/'CON', '26').
card_flavor_text('ethersworn adjudicator'/'CON', 'Esper mages devised their weapons to be so devastating that war seemed unnecessary.').
card_multiverse_id('ethersworn adjudicator'/'CON', '175035').

card_in_set('exotic orchard', 'CON').
card_original_type('exotic orchard'/'CON', 'Land').
card_original_text('exotic orchard'/'CON', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_first_print('exotic orchard', 'CON').
card_image_name('exotic orchard'/'CON', 'exotic orchard').
card_uid('exotic orchard'/'CON', 'CON:Exotic Orchard:exotic orchard').
card_rarity('exotic orchard'/'CON', 'Rare').
card_artist('exotic orchard'/'CON', 'Steven Belledin').
card_number('exotic orchard'/'CON', '142').
card_flavor_text('exotic orchard'/'CON', '\"It was a strange morning. When we awoke, we found our trees transformed. We didn\'t know whether to water them or polish them.\"\n—Pulan, Bant orchardist').
card_multiverse_id('exotic orchard'/'CON', '189273').

card_in_set('exploding borders', 'CON').
card_original_type('exploding borders'/'CON', 'Sorcery').
card_original_text('exploding borders'/'CON', 'Domain Search your library for a basic land card, put that card into play tapped, then shuffle your library. Exploding Borders deals X damage to target player, where X is the number of basic land types among lands you control.').
card_first_print('exploding borders', 'CON').
card_image_name('exploding borders'/'CON', 'exploding borders').
card_uid('exploding borders'/'CON', 'CON:Exploding Borders:exploding borders').
card_rarity('exploding borders'/'CON', 'Common').
card_artist('exploding borders'/'CON', 'Zoltan Boros & Gabor Szikszai').
card_number('exploding borders'/'CON', '106').
card_flavor_text('exploding borders'/'CON', 'Reuniting a world is not a gentle process.').
card_multiverse_id('exploding borders'/'CON', '189378').

card_in_set('extractor demon', 'CON').
card_original_type('extractor demon'/'CON', 'Creature — Demon').
card_original_text('extractor demon'/'CON', 'Flying\nWhenever another creature leaves play, you may have target player put the top two cards of his or her library into his or her graveyard.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('extractor demon', 'CON').
card_image_name('extractor demon'/'CON', 'extractor demon').
card_uid('extractor demon'/'CON', 'CON:Extractor Demon:extractor demon').
card_rarity('extractor demon'/'CON', 'Rare').
card_artist('extractor demon'/'CON', 'Carl Critchlow').
card_number('extractor demon'/'CON', '44').
card_multiverse_id('extractor demon'/'CON', '189082').

card_in_set('faerie mechanist', 'CON').
card_original_type('faerie mechanist'/'CON', 'Artifact Creature — Faerie Artificer').
card_original_text('faerie mechanist'/'CON', 'Flying\nWhen Faerie Mechanist comes into play, look at the top three cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('faerie mechanist', 'CON').
card_image_name('faerie mechanist'/'CON', 'faerie mechanist').
card_uid('faerie mechanist'/'CON', 'CON:Faerie Mechanist:faerie mechanist').
card_rarity('faerie mechanist'/'CON', 'Common').
card_artist('faerie mechanist'/'CON', 'Matt Cavotta').
card_number('faerie mechanist'/'CON', '27').
card_multiverse_id('faerie mechanist'/'CON', '182608').

card_in_set('fiery fall', 'CON').
card_original_type('fiery fall'/'CON', 'Instant').
card_original_text('fiery fall'/'CON', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('fiery fall', 'CON').
card_image_name('fiery fall'/'CON', 'fiery fall').
card_uid('fiery fall'/'CON', 'CON:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'CON', 'Common').
card_artist('fiery fall'/'CON', 'Daarken').
card_number('fiery fall'/'CON', '63').
card_flavor_text('fiery fall'/'CON', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'CON', '179519').

card_in_set('filigree fracture', 'CON').
card_original_type('filigree fracture'/'CON', 'Instant').
card_original_text('filigree fracture'/'CON', 'Destroy target artifact or enchantment. If that permanent was blue or black, draw a card.').
card_first_print('filigree fracture', 'CON').
card_image_name('filigree fracture'/'CON', 'filigree fracture').
card_uid('filigree fracture'/'CON', 'CON:Filigree Fracture:filigree fracture').
card_rarity('filigree fracture'/'CON', 'Uncommon').
card_artist('filigree fracture'/'CON', 'Howard Lyon').
card_number('filigree fracture'/'CON', '82').
card_flavor_text('filigree fracture'/'CON', 'The sphinx tyrannized Bant until a rhox mage tested the hardiness of her shiny parts.').
card_multiverse_id('filigree fracture'/'CON', '179439').

card_in_set('fleshformer', 'CON').
card_original_type('fleshformer'/'CON', 'Creature — Human Wizard').
card_original_text('fleshformer'/'CON', '{W}{U}{B}{R}{G}: Fleshformer gets +2/+2 and gains fear until end of turn. Target creature gets -2/-2 until end of turn. Play this ability only during your turn.').
card_first_print('fleshformer', 'CON').
card_image_name('fleshformer'/'CON', 'fleshformer').
card_uid('fleshformer'/'CON', 'CON:Fleshformer:fleshformer').
card_rarity('fleshformer'/'CON', 'Uncommon').
card_artist('fleshformer'/'CON', 'Dave Kendall').
card_number('fleshformer'/'CON', '45').
card_flavor_text('fleshformer'/'CON', 'Necromancers who discovered the new sources of mana were quick to dream up new nightmares with them.').
card_multiverse_id('fleshformer'/'CON', '179884').

card_in_set('font of mythos', 'CON').
card_original_type('font of mythos'/'CON', 'Artifact').
card_original_text('font of mythos'/'CON', 'At the beginning of each player\'s draw step, that player draws two additional cards.').
card_first_print('font of mythos', 'CON').
card_image_name('font of mythos'/'CON', 'font of mythos').
card_uid('font of mythos'/'CON', 'CON:Font of Mythos:font of mythos').
card_rarity('font of mythos'/'CON', 'Rare').
card_artist('font of mythos'/'CON', 'Dave Allsop').
card_number('font of mythos'/'CON', '136').
card_flavor_text('font of mythos'/'CON', 'Those who drink from Malfegor\'s cup are tainted with hunger and stained with lies.').
card_multiverse_id('font of mythos'/'CON', '189147').

card_in_set('frontline sage', 'CON').
card_original_type('frontline sage'/'CON', 'Creature — Human Wizard').
card_original_text('frontline sage'/'CON', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{U}, {T}: Draw a card, then discard a card.').
card_first_print('frontline sage', 'CON').
card_image_name('frontline sage'/'CON', 'frontline sage').
card_uid('frontline sage'/'CON', 'CON:Frontline Sage:frontline sage').
card_rarity('frontline sage'/'CON', 'Common').
card_artist('frontline sage'/'CON', 'Volkan Baga').
card_number('frontline sage'/'CON', '28').
card_flavor_text('frontline sage'/'CON', 'The battle was won in the commander\'s head before the first weapon was drawn.').
card_multiverse_id('frontline sage'/'CON', '184993').

card_in_set('fusion elemental', 'CON').
card_original_type('fusion elemental'/'CON', 'Creature — Elemental').
card_original_text('fusion elemental'/'CON', '').
card_first_print('fusion elemental', 'CON').
card_image_name('fusion elemental'/'CON', 'fusion elemental').
card_uid('fusion elemental'/'CON', 'CON:Fusion Elemental:fusion elemental').
card_rarity('fusion elemental'/'CON', 'Uncommon').
card_artist('fusion elemental'/'CON', 'Michael Komarck').
card_number('fusion elemental'/'CON', '107').
card_flavor_text('fusion elemental'/'CON', 'As the shards merged into the Maelstrom, their mana energies fused into new monstrosities.').
card_multiverse_id('fusion elemental'/'CON', '179443').

card_in_set('giltspire avenger', 'CON').
card_original_type('giltspire avenger'/'CON', 'Creature — Human Soldier').
card_original_text('giltspire avenger'/'CON', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Destroy target creature that dealt damage to you this turn.').
card_first_print('giltspire avenger', 'CON').
card_image_name('giltspire avenger'/'CON', 'giltspire avenger').
card_uid('giltspire avenger'/'CON', 'CON:Giltspire Avenger:giltspire avenger').
card_rarity('giltspire avenger'/'CON', 'Rare').
card_artist('giltspire avenger'/'CON', 'Chris Rahn').
card_number('giltspire avenger'/'CON', '108').
card_flavor_text('giltspire avenger'/'CON', '\"To trespass so far from home is to ask for a lonesome grave.\"').
card_multiverse_id('giltspire avenger'/'CON', '186614').

card_in_set('gleam of resistance', 'CON').
card_original_type('gleam of resistance'/'CON', 'Instant').
card_original_text('gleam of resistance'/'CON', 'Creatures you control get +1/+2 until end of turn. Untap those creatures.\nBasic landcycling {1}{W} ({1}{W}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('gleam of resistance', 'CON').
card_image_name('gleam of resistance'/'CON', 'gleam of resistance').
card_uid('gleam of resistance'/'CON', 'CON:Gleam of Resistance:gleam of resistance').
card_rarity('gleam of resistance'/'CON', 'Common').
card_artist('gleam of resistance'/'CON', 'Matt Stewart').
card_number('gleam of resistance'/'CON', '8').
card_multiverse_id('gleam of resistance'/'CON', '179505').

card_in_set('gluttonous slime', 'CON').
card_original_type('gluttonous slime'/'CON', 'Creature — Ooze').
card_original_text('gluttonous slime'/'CON', 'Flash\nDevour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)').
card_first_print('gluttonous slime', 'CON').
card_image_name('gluttonous slime'/'CON', 'gluttonous slime').
card_uid('gluttonous slime'/'CON', 'CON:Gluttonous Slime:gluttonous slime').
card_rarity('gluttonous slime'/'CON', 'Uncommon').
card_artist('gluttonous slime'/'CON', 'Trevor Claxton').
card_number('gluttonous slime'/'CON', '83').
card_flavor_text('gluttonous slime'/'CON', 'On Jund, everything eventually ends up in something else\'s stomach.').
card_multiverse_id('gluttonous slime'/'CON', '179512').

card_in_set('goblin outlander', 'CON').
card_original_type('goblin outlander'/'CON', 'Creature — Goblin Scout').
card_original_text('goblin outlander'/'CON', 'Protection from white').
card_first_print('goblin outlander', 'CON').
card_image_name('goblin outlander'/'CON', 'goblin outlander').
card_uid('goblin outlander'/'CON', 'CON:Goblin Outlander:goblin outlander').
card_rarity('goblin outlander'/'CON', 'Common').
card_artist('goblin outlander'/'CON', 'Trevor Claxton').
card_number('goblin outlander'/'CON', '109').
card_flavor_text('goblin outlander'/'CON', 'Egbol stared in wonder at Naya\'s landscape. So much to eat. So much to steal.').
card_multiverse_id('goblin outlander'/'CON', '185142').

card_in_set('goblin razerunners', 'CON').
card_original_type('goblin razerunners'/'CON', 'Creature — Goblin Warrior').
card_original_text('goblin razerunners'/'CON', '{1}{R}, Sacrifice a land: Put a +1/+1 counter on Goblin Razerunners.\nAt the end of your turn, you may have Goblin Razerunners deal damage equal to the number of +1/+1 counters on it to target player.').
card_first_print('goblin razerunners', 'CON').
card_image_name('goblin razerunners'/'CON', 'goblin razerunners').
card_uid('goblin razerunners'/'CON', 'CON:Goblin Razerunners:goblin razerunners').
card_rarity('goblin razerunners'/'CON', 'Rare').
card_artist('goblin razerunners'/'CON', 'Raymond Swanland').
card_number('goblin razerunners'/'CON', '64').
card_flavor_text('goblin razerunners'/'CON', 'Finding themselves in a new and unexplored world, they immediately set it on fire.').
card_multiverse_id('goblin razerunners'/'CON', '179487').

card_in_set('grixis illusionist', 'CON').
card_original_type('grixis illusionist'/'CON', 'Creature — Human Wizard').
card_original_text('grixis illusionist'/'CON', '{T}: Target land you control becomes the basic land type of your choice until end of turn.').
card_first_print('grixis illusionist', 'CON').
card_image_name('grixis illusionist'/'CON', 'grixis illusionist').
card_uid('grixis illusionist'/'CON', 'CON:Grixis Illusionist:grixis illusionist').
card_rarity('grixis illusionist'/'CON', 'Common').
card_artist('grixis illusionist'/'CON', 'Mark Tedin').
card_number('grixis illusionist'/'CON', '29').
card_flavor_text('grixis illusionist'/'CON', '\"It\'s simple, really. If they can\'t find us, they can\'t kill us.\"').
card_multiverse_id('grixis illusionist'/'CON', '177927').

card_in_set('grixis slavedriver', 'CON').
card_original_type('grixis slavedriver'/'CON', 'Creature — Zombie Giant').
card_original_text('grixis slavedriver'/'CON', 'When Grixis Slavedriver leaves play, put a 2/2 black Zombie creature token into play.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('grixis slavedriver', 'CON').
card_image_name('grixis slavedriver'/'CON', 'grixis slavedriver').
card_uid('grixis slavedriver'/'CON', 'CON:Grixis Slavedriver:grixis slavedriver').
card_rarity('grixis slavedriver'/'CON', 'Uncommon').
card_artist('grixis slavedriver'/'CON', 'Dave Kendall').
card_number('grixis slavedriver'/'CON', '46').
card_multiverse_id('grixis slavedriver'/'CON', '179804').

card_in_set('gwafa hazid, profiteer', 'CON').
card_original_type('gwafa hazid, profiteer'/'CON', 'Legendary Creature — Human Rogue').
card_original_text('gwafa hazid, profiteer'/'CON', '{W}{U}, {T}: Put a bribery counter on target creature you don\'t control. Its controller draws a card.\nCreatures with bribery counters on them can\'t attack or block.').
card_first_print('gwafa hazid, profiteer', 'CON').
card_image_name('gwafa hazid, profiteer'/'CON', 'gwafa hazid, profiteer').
card_uid('gwafa hazid, profiteer'/'CON', 'CON:Gwafa Hazid, Profiteer:gwafa hazid, profiteer').
card_rarity('gwafa hazid, profiteer'/'CON', 'Rare').
card_artist('gwafa hazid, profiteer'/'CON', 'Todd Lockwood').
card_number('gwafa hazid, profiteer'/'CON', '110').
card_flavor_text('gwafa hazid, profiteer'/'CON', '\"Everyone has a price.\"').
card_multiverse_id('gwafa hazid, profiteer'/'CON', '185811').

card_in_set('hellkite hatchling', 'CON').
card_original_type('hellkite hatchling'/'CON', 'Creature — Dragon').
card_original_text('hellkite hatchling'/'CON', 'Devour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)\nHellkite Hatchling has flying and trample if it devoured a creature.').
card_first_print('hellkite hatchling', 'CON').
card_image_name('hellkite hatchling'/'CON', 'hellkite hatchling').
card_uid('hellkite hatchling'/'CON', 'CON:Hellkite Hatchling:hellkite hatchling').
card_rarity('hellkite hatchling'/'CON', 'Uncommon').
card_artist('hellkite hatchling'/'CON', 'Daarken').
card_number('hellkite hatchling'/'CON', '111').
card_flavor_text('hellkite hatchling'/'CON', 'A killing machine from birth.').
card_multiverse_id('hellkite hatchling'/'CON', '189146').

card_in_set('hellspark elemental', 'CON').
card_original_type('hellspark elemental'/'CON', 'Creature — Elemental').
card_original_text('hellspark elemental'/'CON', 'Trample, haste\nAt end of turn, sacrifice Hellspark Elemental.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_image_name('hellspark elemental'/'CON', 'hellspark elemental').
card_uid('hellspark elemental'/'CON', 'CON:Hellspark Elemental:hellspark elemental').
card_rarity('hellspark elemental'/'CON', 'Uncommon').
card_artist('hellspark elemental'/'CON', 'Justin Sweet').
card_number('hellspark elemental'/'CON', '65').
card_multiverse_id('hellspark elemental'/'CON', '174799').

card_in_set('ignite disorder', 'CON').
card_original_type('ignite disorder'/'CON', 'Instant').
card_original_text('ignite disorder'/'CON', 'Ignite Disorder deals 3 damage divided as you choose among any number of target white and/or blue creatures.').
card_first_print('ignite disorder', 'CON').
card_image_name('ignite disorder'/'CON', 'ignite disorder').
card_uid('ignite disorder'/'CON', 'CON:Ignite Disorder:ignite disorder').
card_rarity('ignite disorder'/'CON', 'Uncommon').
card_artist('ignite disorder'/'CON', 'Zoltan Boros & Gabor Szikszai').
card_number('ignite disorder'/'CON', '66').
card_flavor_text('ignite disorder'/'CON', '\"Bant is a world imprisoned by polished stone and tyrannical rule. It yearns to strike back against those who restrain it.\"').
card_multiverse_id('ignite disorder'/'CON', '183056').

card_in_set('infectious horror', 'CON').
card_original_type('infectious horror'/'CON', 'Creature — Zombie Horror').
card_original_text('infectious horror'/'CON', 'Whenever Infectious Horror attacks, each opponent loses 2 life.').
card_first_print('infectious horror', 'CON').
card_image_name('infectious horror'/'CON', 'infectious horror').
card_uid('infectious horror'/'CON', 'CON:Infectious Horror:infectious horror').
card_rarity('infectious horror'/'CON', 'Common').
card_artist('infectious horror'/'CON', 'Pete Venters').
card_number('infectious horror'/'CON', '47').
card_flavor_text('infectious horror'/'CON', 'Not once in the history of Grixis has anyone died of old age.').
card_multiverse_id('infectious horror'/'CON', '157732').

card_in_set('inkwell leviathan', 'CON').
card_original_type('inkwell leviathan'/'CON', 'Artifact Creature — Leviathan').
card_original_text('inkwell leviathan'/'CON', 'Islandwalk, trample, shroud').
card_first_print('inkwell leviathan', 'CON').
card_image_name('inkwell leviathan'/'CON', 'inkwell leviathan').
card_uid('inkwell leviathan'/'CON', 'CON:Inkwell Leviathan:inkwell leviathan').
card_rarity('inkwell leviathan'/'CON', 'Rare').
card_artist('inkwell leviathan'/'CON', 'Anthony Francisco').
card_number('inkwell leviathan'/'CON', '30').
card_flavor_text('inkwell leviathan'/'CON', '\"Into its maw went the seventh sea, never to be seen again while the world remains.\"\n—Esper fable').
card_multiverse_id('inkwell leviathan'/'CON', '154081').

card_in_set('jhessian balmgiver', 'CON').
card_original_type('jhessian balmgiver'/'CON', 'Creature — Human Cleric').
card_original_text('jhessian balmgiver'/'CON', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Target creature is unblockable this turn.').
card_first_print('jhessian balmgiver', 'CON').
card_image_name('jhessian balmgiver'/'CON', 'jhessian balmgiver').
card_uid('jhessian balmgiver'/'CON', 'CON:Jhessian Balmgiver:jhessian balmgiver').
card_rarity('jhessian balmgiver'/'CON', 'Uncommon').
card_artist('jhessian balmgiver'/'CON', 'David Palumbo').
card_number('jhessian balmgiver'/'CON', '112').
card_flavor_text('jhessian balmgiver'/'CON', '\"You have two choices: heed my advice now or need my healing later.\"').
card_multiverse_id('jhessian balmgiver'/'CON', '174791').

card_in_set('kaleidostone', 'CON').
card_original_type('kaleidostone'/'CON', 'Artifact').
card_original_text('kaleidostone'/'CON', 'When Kaleidostone comes into play, draw a card.\n{5}, {T}, Sacrifice Kaleidostone: Add {W}{U}{B}{R}{G} to your mana pool.').
card_first_print('kaleidostone', 'CON').
card_image_name('kaleidostone'/'CON', 'kaleidostone').
card_uid('kaleidostone'/'CON', 'CON:Kaleidostone:kaleidostone').
card_rarity('kaleidostone'/'CON', 'Common').
card_artist('kaleidostone'/'CON', 'Chippy').
card_number('kaleidostone'/'CON', '137').
card_flavor_text('kaleidostone'/'CON', 'Once broken, who knows what worlds might grow from its shards?').
card_multiverse_id('kaleidostone'/'CON', '189272').

card_in_set('kederekt parasite', 'CON').
card_original_type('kederekt parasite'/'CON', 'Creature — Horror').
card_original_text('kederekt parasite'/'CON', 'Whenever an opponent draws a card, if you control a red permanent, you may have Kederekt Parasite deal 1 damage to that player.').
card_first_print('kederekt parasite', 'CON').
card_image_name('kederekt parasite'/'CON', 'kederekt parasite').
card_uid('kederekt parasite'/'CON', 'CON:Kederekt Parasite:kederekt parasite').
card_rarity('kederekt parasite'/'CON', 'Rare').
card_artist('kederekt parasite'/'CON', 'Dan Scott').
card_number('kederekt parasite'/'CON', '48').
card_flavor_text('kederekt parasite'/'CON', 'When the smell of passing thoughts piques its hunger, its maw becomes primed with acid and a taste for brains.').
card_multiverse_id('kederekt parasite'/'CON', '189080').

card_in_set('knight of the reliquary', 'CON').
card_original_type('knight of the reliquary'/'CON', 'Creature — Human Knight').
card_original_text('knight of the reliquary'/'CON', 'Knight of the Reliquary gets +1/+1 for each land card in your graveyard.\n{T}, Sacrifice a Forest or Plains: Search your library for a land card, put it into play, then shuffle your library.').
card_first_print('knight of the reliquary', 'CON').
card_image_name('knight of the reliquary'/'CON', 'knight of the reliquary').
card_uid('knight of the reliquary'/'CON', 'CON:Knight of the Reliquary:knight of the reliquary').
card_rarity('knight of the reliquary'/'CON', 'Rare').
card_artist('knight of the reliquary'/'CON', 'Michael Komarck').
card_number('knight of the reliquary'/'CON', '113').
card_flavor_text('knight of the reliquary'/'CON', '\"Knowledge of Bant\'s landscape and ruins is a weapon that the invaders can\'t comprehend.\"\n—Elspeth').
card_multiverse_id('knight of the reliquary'/'CON', '189145').

card_in_set('knotvine mystic', 'CON').
card_original_type('knotvine mystic'/'CON', 'Creature — Elf Druid').
card_original_text('knotvine mystic'/'CON', '{1}, {T}: Add {R}{G}{W} to your mana pool.').
card_first_print('knotvine mystic', 'CON').
card_image_name('knotvine mystic'/'CON', 'knotvine mystic').
card_uid('knotvine mystic'/'CON', 'CON:Knotvine Mystic:knotvine mystic').
card_rarity('knotvine mystic'/'CON', 'Uncommon').
card_artist('knotvine mystic'/'CON', 'Ariel Olivetti').
card_number('knotvine mystic'/'CON', '114').
card_flavor_text('knotvine mystic'/'CON', 'The elves of Naya were the last to acknowledge the massive shift of mana in their world.').
card_multiverse_id('knotvine mystic'/'CON', '180265').

card_in_set('kranioceros', 'CON').
card_original_type('kranioceros'/'CON', 'Creature — Beast').
card_original_text('kranioceros'/'CON', '{1}{W}: Kranioceros gets +0/+3 until end of turn.').
card_first_print('kranioceros', 'CON').
card_image_name('kranioceros'/'CON', 'kranioceros').
card_uid('kranioceros'/'CON', 'CON:Kranioceros:kranioceros').
card_rarity('kranioceros'/'CON', 'Common').
card_artist('kranioceros'/'CON', 'Steve Argyle').
card_number('kranioceros'/'CON', '67').
card_flavor_text('kranioceros'/'CON', '\"A surly beast, the kranioceros will raise its defenses at the smallest threat. Stay out of sight and downwind, or you\'ll disrupt its natural migrations.\"\n—Ebrel, godtoucher mentor').
card_multiverse_id('kranioceros'/'CON', '177940').

card_in_set('lapse of certainty', 'CON').
card_original_type('lapse of certainty'/'CON', 'Instant').
card_original_text('lapse of certainty'/'CON', 'Counter target spell. If that spell is countered this way, put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_first_print('lapse of certainty', 'CON').
card_image_name('lapse of certainty'/'CON', 'lapse of certainty').
card_uid('lapse of certainty'/'CON', 'CON:Lapse of Certainty:lapse of certainty').
card_rarity('lapse of certainty'/'CON', 'Common').
card_artist('lapse of certainty'/'CON', 'Anthony Francisco').
card_number('lapse of certainty'/'CON', '9').
card_flavor_text('lapse of certainty'/'CON', 'Without a connection of mind and body, magic is just an idea stuck in the head, a word on the tip of a tongue.').
card_multiverse_id('lapse of certainty'/'CON', '175026').

card_in_set('maelstrom archangel', 'CON').
card_original_type('maelstrom archangel'/'CON', 'Creature — Angel').
card_original_text('maelstrom archangel'/'CON', 'Flying\nWhenever Maelstrom Archangel deals combat damage to a player, you may play a nonland card from your hand without paying its mana cost.').
card_first_print('maelstrom archangel', 'CON').
card_image_name('maelstrom archangel'/'CON', 'maelstrom archangel').
card_uid('maelstrom archangel'/'CON', 'CON:Maelstrom Archangel:maelstrom archangel').
card_rarity('maelstrom archangel'/'CON', 'Mythic Rare').
card_artist('maelstrom archangel'/'CON', 'Cyril Van Der Haegen').
card_number('maelstrom archangel'/'CON', '115').
card_flavor_text('maelstrom archangel'/'CON', 'There is no world where angels fear to tread.').
card_multiverse_id('maelstrom archangel'/'CON', '185136').

card_in_set('magister sphinx', 'CON').
card_original_type('magister sphinx'/'CON', 'Artifact Creature — Sphinx').
card_original_text('magister sphinx'/'CON', 'Flying\nWhen Magister Sphinx comes into play, target player\'s life total becomes 10.').
card_first_print('magister sphinx', 'CON').
card_image_name('magister sphinx'/'CON', 'magister sphinx').
card_uid('magister sphinx'/'CON', 'CON:Magister Sphinx:magister sphinx').
card_rarity('magister sphinx'/'CON', 'Rare').
card_artist('magister sphinx'/'CON', 'Steven Belledin').
card_number('magister sphinx'/'CON', '116').
card_flavor_text('magister sphinx'/'CON', '\"These benighted worlds are thick with ignorance. I will educate them. They will listen, or they will die.\"').
card_multiverse_id('magister sphinx'/'CON', '179892').

card_in_set('malfegor', 'CON').
card_original_type('malfegor'/'CON', 'Legendary Creature — Demon Dragon').
card_original_text('malfegor'/'CON', 'Flying\nWhen Malfegor comes into play, discard your hand. Each opponent sacrifices a creature for each card discarded this way.').
card_image_name('malfegor'/'CON', 'malfegor').
card_uid('malfegor'/'CON', 'CON:Malfegor:malfegor').
card_rarity('malfegor'/'CON', 'Mythic Rare').
card_artist('malfegor'/'CON', 'Jason Chan').
card_number('malfegor'/'CON', '117').
card_flavor_text('malfegor'/'CON', 'A demon cannot be trusted, and a dragon will not be ruled.').
card_multiverse_id('malfegor'/'CON', '185812').

card_in_set('mana cylix', 'CON').
card_original_type('mana cylix'/'CON', 'Artifact').
card_original_text('mana cylix'/'CON', '{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('mana cylix'/'CON', 'mana cylix').
card_uid('mana cylix'/'CON', 'CON:Mana Cylix:mana cylix').
card_rarity('mana cylix'/'CON', 'Common').
card_artist('mana cylix'/'CON', 'Howard Lyon').
card_number('mana cylix'/'CON', '138').
card_flavor_text('mana cylix'/'CON', 'Those who dismiss the cylix as an ordinary wooden bowl are blind to the true measure of its worth.').
card_multiverse_id('mana cylix'/'CON', '189269').

card_in_set('manaforce mace', 'CON').
card_original_type('manaforce mace'/'CON', 'Artifact — Equipment').
card_original_text('manaforce mace'/'CON', 'Domain Equipped creature gets +1/+1 for each basic land type among lands you control.\nEquip {3}').
card_first_print('manaforce mace', 'CON').
card_image_name('manaforce mace'/'CON', 'manaforce mace').
card_uid('manaforce mace'/'CON', 'CON:Manaforce Mace:manaforce mace').
card_rarity('manaforce mace'/'CON', 'Uncommon').
card_artist('manaforce mace'/'CON', 'Jeremy Jarvis').
card_number('manaforce mace'/'CON', '139').
card_flavor_text('manaforce mace'/'CON', 'As the shards merged, relics once thought mundane regained forgotten powers.').
card_multiverse_id('manaforce mace'/'CON', '180159').

card_in_set('maniacal rage', 'CON').
card_original_type('maniacal rage'/'CON', 'Enchantment — Aura').
card_original_text('maniacal rage'/'CON', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t block.').
card_image_name('maniacal rage'/'CON', 'maniacal rage').
card_uid('maniacal rage'/'CON', 'CON:Maniacal Rage:maniacal rage').
card_rarity('maniacal rage'/'CON', 'Common').
card_artist('maniacal rage'/'CON', 'Brandon Kitkouski').
card_number('maniacal rage'/'CON', '68').
card_flavor_text('maniacal rage'/'CON', 'The spikes of sangrite made the goblin stronger, but they didn\'t make him any smarter.').
card_multiverse_id('maniacal rage'/'CON', '182609').

card_in_set('mark of asylum', 'CON').
card_original_type('mark of asylum'/'CON', 'Enchantment').
card_original_text('mark of asylum'/'CON', 'Prevent all noncombat damage that would be dealt to creatures you control.').
card_first_print('mark of asylum', 'CON').
card_image_name('mark of asylum'/'CON', 'mark of asylum').
card_uid('mark of asylum'/'CON', 'CON:Mark of Asylum:mark of asylum').
card_rarity('mark of asylum'/'CON', 'Rare').
card_artist('mark of asylum'/'CON', 'Sal Villagran').
card_number('mark of asylum'/'CON', '10').
card_flavor_text('mark of asylum'/'CON', 'A paragon of honor, Galo Aher fell to a blast of fire. The Order of the White Orchid was so saddened that they forged a new sigil in his likeness.').
card_multiverse_id('mark of asylum'/'CON', '186615').

card_in_set('martial coup', 'CON').
card_original_type('martial coup'/'CON', 'Sorcery').
card_original_text('martial coup'/'CON', 'Put X 1/1 white Soldier creature tokens into play. If X is 5 or more, destroy all other creatures.').
card_first_print('martial coup', 'CON').
card_image_name('martial coup'/'CON', 'martial coup').
card_uid('martial coup'/'CON', 'CON:Martial Coup:martial coup').
card_rarity('martial coup'/'CON', 'Rare').
card_artist('martial coup'/'CON', 'Greg Staples').
card_number('martial coup'/'CON', '11').
card_flavor_text('martial coup'/'CON', 'Their war forgotten, the nations of Bant stood united in the face of a common threat.').
card_multiverse_id('martial coup'/'CON', '178560').

card_in_set('master transmuter', 'CON').
card_original_type('master transmuter'/'CON', 'Artifact Creature — Human Artificer').
card_original_text('master transmuter'/'CON', '{U}, {T}, Return an artifact you control to its owner\'s hand: You may put an artifact card from your hand into play.').
card_first_print('master transmuter', 'CON').
card_image_name('master transmuter'/'CON', 'master transmuter').
card_uid('master transmuter'/'CON', 'CON:Master Transmuter:master transmuter').
card_rarity('master transmuter'/'CON', 'Rare').
card_artist('master transmuter'/'CON', 'Chippy').
card_number('master transmuter'/'CON', '31').
card_flavor_text('master transmuter'/'CON', '\"Wasted potential surrounds us. Lend me that bauble, and let me see what it can be made to be.\"').
card_multiverse_id('master transmuter'/'CON', '179252').

card_in_set('matca rioters', 'CON').
card_original_type('matca rioters'/'CON', 'Creature — Human Warrior').
card_original_text('matca rioters'/'CON', 'Domain Matca Rioters\'s power and toughness are each equal to the number of basic land types among lands you control.').
card_first_print('matca rioters', 'CON').
card_image_name('matca rioters'/'CON', 'matca rioters').
card_uid('matca rioters'/'CON', 'CON:Matca Rioters:matca rioters').
card_rarity('matca rioters'/'CON', 'Common').
card_artist('matca rioters'/'CON', 'Steve Argyle').
card_number('matca rioters'/'CON', '84').
card_flavor_text('matca rioters'/'CON', 'When outsiders interrupted the matca championship, things got ugly.').
card_multiverse_id('matca rioters'/'CON', '179248').

card_in_set('meglonoth', 'CON').
card_original_type('meglonoth'/'CON', 'Creature — Beast').
card_original_text('meglonoth'/'CON', 'Vigilance, trample\nWhenever Meglonoth blocks a creature, Meglonoth deals damage to that creature\'s controller equal to Meglonoth\'s power.').
card_first_print('meglonoth', 'CON').
card_image_name('meglonoth'/'CON', 'meglonoth').
card_uid('meglonoth'/'CON', 'CON:Meglonoth:meglonoth').
card_rarity('meglonoth'/'CON', 'Rare').
card_artist('meglonoth'/'CON', 'Steve Prescott').
card_number('meglonoth'/'CON', '118').
card_flavor_text('meglonoth'/'CON', 'When the shards merged, Mayael found herself the general of Naya\'s mightiest army.').
card_multiverse_id('meglonoth'/'CON', '179886').

card_in_set('might of alara', 'CON').
card_original_type('might of alara'/'CON', 'Instant').
card_original_text('might of alara'/'CON', 'Domain Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_first_print('might of alara', 'CON').
card_image_name('might of alara'/'CON', 'might of alara').
card_uid('might of alara'/'CON', 'CON:Might of Alara:might of alara').
card_rarity('might of alara'/'CON', 'Common').
card_artist('might of alara'/'CON', 'Steve Prescott').
card_number('might of alara'/'CON', '85').
card_flavor_text('might of alara'/'CON', 'The combined strength of all five planes spawned creations none had seen before.').
card_multiverse_id('might of alara'/'CON', '150810').

card_in_set('mirror-sigil sergeant', 'CON').
card_original_type('mirror-sigil sergeant'/'CON', 'Creature — Rhino Soldier').
card_original_text('mirror-sigil sergeant'/'CON', 'Trample\nAt the beginning of your upkeep, if you control a blue permanent, you may put a token into play that\'s a copy of Mirror-Sigil Sergeant.').
card_first_print('mirror-sigil sergeant', 'CON').
card_image_name('mirror-sigil sergeant'/'CON', 'mirror-sigil sergeant').
card_uid('mirror-sigil sergeant'/'CON', 'CON:Mirror-Sigil Sergeant:mirror-sigil sergeant').
card_rarity('mirror-sigil sergeant'/'CON', 'Mythic Rare').
card_artist('mirror-sigil sergeant'/'CON', 'Chris Rahn').
card_number('mirror-sigil sergeant'/'CON', '12').
card_flavor_text('mirror-sigil sergeant'/'CON', '\"If I had many lives, I would give them all for Bant.\"').
card_multiverse_id('mirror-sigil sergeant'/'CON', '158598').

card_in_set('molten frame', 'CON').
card_original_type('molten frame'/'CON', 'Instant').
card_original_text('molten frame'/'CON', 'Destroy target artifact creature.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('molten frame', 'CON').
card_image_name('molten frame'/'CON', 'molten frame').
card_uid('molten frame'/'CON', 'CON:Molten Frame:molten frame').
card_rarity('molten frame'/'CON', 'Common').
card_artist('molten frame'/'CON', 'Izzy').
card_number('molten frame'/'CON', '69').
card_flavor_text('molten frame'/'CON', 'The metal filigree in his body glowed red-hot, and his flesh soon followed.').
card_multiverse_id('molten frame'/'CON', '184988').

card_in_set('nacatl hunt-pride', 'CON').
card_original_type('nacatl hunt-pride'/'CON', 'Creature — Cat Warrior').
card_original_text('nacatl hunt-pride'/'CON', 'Vigilance\n{R}, {T}: Target creature can\'t block this turn.\n{G}, {T}: Target creature blocks this turn if able.').
card_first_print('nacatl hunt-pride', 'CON').
card_image_name('nacatl hunt-pride'/'CON', 'nacatl hunt-pride').
card_uid('nacatl hunt-pride'/'CON', 'CON:Nacatl Hunt-Pride:nacatl hunt-pride').
card_rarity('nacatl hunt-pride'/'CON', 'Uncommon').
card_artist('nacatl hunt-pride'/'CON', 'Steve Prescott').
card_number('nacatl hunt-pride'/'CON', '13').
card_flavor_text('nacatl hunt-pride'/'CON', '\"We must hunt and kill the dragon before it can return to raze our ancient city.\"').
card_multiverse_id('nacatl hunt-pride'/'CON', '138571').

card_in_set('nacatl outlander', 'CON').
card_original_type('nacatl outlander'/'CON', 'Creature — Cat Scout').
card_original_text('nacatl outlander'/'CON', 'Protection from blue').
card_first_print('nacatl outlander', 'CON').
card_image_name('nacatl outlander'/'CON', 'nacatl outlander').
card_uid('nacatl outlander'/'CON', 'CON:Nacatl Outlander:nacatl outlander').
card_rarity('nacatl outlander'/'CON', 'Common').
card_artist('nacatl outlander'/'CON', 'Zoltan Boros & Gabor Szikszai').
card_number('nacatl outlander'/'CON', '119').
card_flavor_text('nacatl outlander'/'CON', 'Survival in the wilds of Naya left Tiyan well equipped to win the civilized battles of Bant.').
card_multiverse_id('nacatl outlander'/'CON', '185140').

card_in_set('nacatl savage', 'CON').
card_original_type('nacatl savage'/'CON', 'Creature — Cat Warrior').
card_original_text('nacatl savage'/'CON', 'Protection from artifacts').
card_first_print('nacatl savage', 'CON').
card_image_name('nacatl savage'/'CON', 'nacatl savage').
card_uid('nacatl savage'/'CON', 'CON:Nacatl Savage:nacatl savage').
card_rarity('nacatl savage'/'CON', 'Common').
card_artist('nacatl savage'/'CON', 'Paolo Parente').
card_number('nacatl savage'/'CON', '86').
card_flavor_text('nacatl savage'/'CON', '\"Blades dull and armor dents. Marisi taught us that instinct is the only thing a true warrior needs.\"\n—Ajani').
card_multiverse_id('nacatl savage'/'CON', '183022').

card_in_set('nicol bolas, planeswalker', 'CON').
card_original_type('nicol bolas, planeswalker'/'CON', 'Planeswalker — Bolas').
card_original_text('nicol bolas, planeswalker'/'CON', '+3: Destroy target noncreature permanent.\n-2: Gain control of target creature.\n-9: Nicol Bolas, Planeswalker deals 7 damage to target player. That player discards seven cards, then sacrifices seven permanents.').
card_first_print('nicol bolas, planeswalker', 'CON').
card_image_name('nicol bolas, planeswalker'/'CON', 'nicol bolas, planeswalker').
card_uid('nicol bolas, planeswalker'/'CON', 'CON:Nicol Bolas, Planeswalker:nicol bolas, planeswalker').
card_rarity('nicol bolas, planeswalker'/'CON', 'Mythic Rare').
card_artist('nicol bolas, planeswalker'/'CON', 'D. Alexander Gregory').
card_number('nicol bolas, planeswalker'/'CON', '120').
card_multiverse_id('nicol bolas, planeswalker'/'CON', '179441').

card_in_set('noble hierarch', 'CON').
card_original_type('noble hierarch'/'CON', 'Creature — Human Druid').
card_original_text('noble hierarch'/'CON', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_image_name('noble hierarch'/'CON', 'noble hierarch').
card_uid('noble hierarch'/'CON', 'CON:Noble Hierarch:noble hierarch').
card_rarity('noble hierarch'/'CON', 'Rare').
card_artist('noble hierarch'/'CON', 'Mark Zug').
card_number('noble hierarch'/'CON', '87').
card_flavor_text('noble hierarch'/'CON', 'She protects the sacred groves from blight, drought, and the Unbeholden.').
card_multiverse_id('noble hierarch'/'CON', '179434').

card_in_set('nyxathid', 'CON').
card_original_type('nyxathid'/'CON', 'Creature — Elemental').
card_original_text('nyxathid'/'CON', 'As Nyxathid comes into play, choose an opponent.\nNyxathid gets -1/-1 for each card in the chosen player\'s hand.').
card_first_print('nyxathid', 'CON').
card_image_name('nyxathid'/'CON', 'nyxathid').
card_uid('nyxathid'/'CON', 'CON:Nyxathid:nyxathid').
card_rarity('nyxathid'/'CON', 'Rare').
card_artist('nyxathid'/'CON', 'Raymond Swanland').
card_number('nyxathid'/'CON', '49').
card_flavor_text('nyxathid'/'CON', 'Born of volcanic forces, it thrives on the absolute panic it inspires.').
card_multiverse_id('nyxathid'/'CON', '186616').

card_in_set('obelisk of alara', 'CON').
card_original_type('obelisk of alara'/'CON', 'Artifact').
card_original_text('obelisk of alara'/'CON', '{1}{W}, {T}: You gain 5 life.\n{1}{U}, {T}: Draw a card, then discard a card.\n{1}{B}, {T}: Target creature gets -2/-2 until end of turn.\n{1}{R}, {T}: Obelisk of Alara deals 3 damage to target player.\n{1}{G}, {T}: Target creature gets +4/+4 until end of turn.').
card_image_name('obelisk of alara'/'CON', 'obelisk of alara').
card_uid('obelisk of alara'/'CON', 'CON:Obelisk of Alara:obelisk of alara').
card_rarity('obelisk of alara'/'CON', 'Rare').
card_artist('obelisk of alara'/'CON', 'Jeremy Jarvis').
card_number('obelisk of alara'/'CON', '140').
card_multiverse_id('obelisk of alara'/'CON', '183018').

card_in_set('paleoloth', 'CON').
card_original_type('paleoloth'/'CON', 'Creature — Beast').
card_original_text('paleoloth'/'CON', 'Whenever another creature with power 5 or greater comes into play under your control, you may return target creature card from your graveyard to your hand.').
card_first_print('paleoloth', 'CON').
card_image_name('paleoloth'/'CON', 'paleoloth').
card_uid('paleoloth'/'CON', 'CON:Paleoloth:paleoloth').
card_rarity('paleoloth'/'CON', 'Rare').
card_artist('paleoloth'/'CON', 'Christopher Moeller').
card_number('paleoloth'/'CON', '88').
card_flavor_text('paleoloth'/'CON', '\"Gods do not sleep soundly in the earth\'s embrace.\"\n—Mayael the Anima').
card_multiverse_id('paleoloth'/'CON', '189873').

card_in_set('paragon of the amesha', 'CON').
card_original_type('paragon of the amesha'/'CON', 'Creature — Human Knight').
card_original_text('paragon of the amesha'/'CON', 'First strike\n{W}{U}{B}{R}{G}: Until end of turn, Paragon of the Amesha becomes an Angel, gets +3/+3, and gains flying and lifelink.').
card_first_print('paragon of the amesha', 'CON').
card_image_name('paragon of the amesha'/'CON', 'paragon of the amesha').
card_uid('paragon of the amesha'/'CON', 'CON:Paragon of the Amesha:paragon of the amesha').
card_rarity('paragon of the amesha'/'CON', 'Uncommon').
card_artist('paragon of the amesha'/'CON', 'Chris Rahn').
card_number('paragon of the amesha'/'CON', '14').
card_flavor_text('paragon of the amesha'/'CON', '\"Let my lance sing with the voices of angels and the heathen cower before my shield, for I will teach this land the meaning of honor.\"').
card_multiverse_id('paragon of the amesha'/'CON', '179878').

card_in_set('parasitic strix', 'CON').
card_original_type('parasitic strix'/'CON', 'Artifact Creature — Bird').
card_original_text('parasitic strix'/'CON', 'Flying\nWhen Parasitic Strix comes into play, if you control a black permanent, target player loses 2 life and you gain 2 life.').
card_first_print('parasitic strix', 'CON').
card_image_name('parasitic strix'/'CON', 'parasitic strix').
card_uid('parasitic strix'/'CON', 'CON:Parasitic Strix:parasitic strix').
card_rarity('parasitic strix'/'CON', 'Common').
card_artist('parasitic strix'/'CON', 'Steven Belledin').
card_number('parasitic strix'/'CON', '32').
card_flavor_text('parasitic strix'/'CON', 'After finding no sustenance on the edges of Grixis, it turned to the skies of Bant.').
card_multiverse_id('parasitic strix'/'CON', '175021').

card_in_set('path to exile', 'CON').
card_original_type('path to exile'/'CON', 'Instant').
card_original_text('path to exile'/'CON', 'Remove target creature from the game. Its controller may search his or her library for a basic land card, put that card into play tapped, then shuffle his or her library.').
card_image_name('path to exile'/'CON', 'path to exile').
card_uid('path to exile'/'CON', 'CON:Path to Exile:path to exile').
card_rarity('path to exile'/'CON', 'Uncommon').
card_artist('path to exile'/'CON', 'Todd Lockwood').
card_number('path to exile'/'CON', '15').
card_multiverse_id('path to exile'/'CON', '179235').

card_in_set('pestilent kathari', 'CON').
card_original_type('pestilent kathari'/'CON', 'Creature — Bird Warrior').
card_original_text('pestilent kathari'/'CON', 'Flying\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)\n{2}{R}: Pestilent Kathari gains first strike until end of turn.').
card_first_print('pestilent kathari', 'CON').
card_image_name('pestilent kathari'/'CON', 'pestilent kathari').
card_uid('pestilent kathari'/'CON', 'CON:Pestilent Kathari:pestilent kathari').
card_rarity('pestilent kathari'/'CON', 'Common').
card_artist('pestilent kathari'/'CON', 'Dave Kendall').
card_number('pestilent kathari'/'CON', '50').
card_multiverse_id('pestilent kathari'/'CON', '179518').

card_in_set('progenitus', 'CON').
card_original_type('progenitus'/'CON', 'Legendary Creature — Hydra Avatar').
card_original_text('progenitus'/'CON', 'Protection from everything\nIf Progenitus would be put into a graveyard from anywhere, reveal Progenitus and shuffle it into its owner\'s library instead.').
card_first_print('progenitus', 'CON').
card_image_name('progenitus'/'CON', 'progenitus').
card_uid('progenitus'/'CON', 'CON:Progenitus:progenitus').
card_rarity('progenitus'/'CON', 'Mythic Rare').
card_artist('progenitus'/'CON', 'Jaime Jones').
card_number('progenitus'/'CON', '121').
card_flavor_text('progenitus'/'CON', 'The Soul of the World has returned.').
card_multiverse_id('progenitus'/'CON', '179496').

card_in_set('quenchable fire', 'CON').
card_original_type('quenchable fire'/'CON', 'Sorcery').
card_original_text('quenchable fire'/'CON', 'Quenchable Fire deals 3 damage to target player. It deals an additional 3 damage to that player at the beginning of your next upkeep step unless he or she pays {U} before that step.').
card_first_print('quenchable fire', 'CON').
card_image_name('quenchable fire'/'CON', 'quenchable fire').
card_uid('quenchable fire'/'CON', 'CON:Quenchable Fire:quenchable fire').
card_rarity('quenchable fire'/'CON', 'Common').
card_artist('quenchable fire'/'CON', 'Jean-Sébastien Rossbach').
card_number('quenchable fire'/'CON', '70').
card_flavor_text('quenchable fire'/'CON', 'You\'d better know how to pray for rain.').
card_multiverse_id('quenchable fire'/'CON', '179440').

card_in_set('rakka mar', 'CON').
card_original_type('rakka mar'/'CON', 'Legendary Creature — Human Shaman').
card_original_text('rakka mar'/'CON', 'Haste\n{R}, {T}: Put a 3/1 red Elemental creature token with haste into play.').
card_first_print('rakka mar', 'CON').
card_image_name('rakka mar'/'CON', 'rakka mar').
card_uid('rakka mar'/'CON', 'CON:Rakka Mar:rakka mar').
card_rarity('rakka mar'/'CON', 'Rare').
card_artist('rakka mar'/'CON', 'Jason Chan').
card_number('rakka mar'/'CON', '71').
card_flavor_text('rakka mar'/'CON', '\"The finest pawns are those with pawns of their own.\"\n—Nicol Bolas').
card_multiverse_id('rakka mar'/'CON', '185810').

card_in_set('reliquary tower', 'CON').
card_original_type('reliquary tower'/'CON', 'Land').
card_original_text('reliquary tower'/'CON', 'You have no maximum hand size.\n{T}: Add {1} to your mana pool.').
card_image_name('reliquary tower'/'CON', 'reliquary tower').
card_uid('reliquary tower'/'CON', 'CON:Reliquary Tower:reliquary tower').
card_rarity('reliquary tower'/'CON', 'Uncommon').
card_artist('reliquary tower'/'CON', 'Jesper Ejsing').
card_number('reliquary tower'/'CON', '143').
card_flavor_text('reliquary tower'/'CON', 'When the aven scouts located the tower in Esper, the Knights of the Reliquary set off on a righteous crusade to recover their lost treasures.').
card_multiverse_id('reliquary tower'/'CON', '150807').

card_in_set('rhox bodyguard', 'CON').
card_original_type('rhox bodyguard'/'CON', 'Creature — Rhino Monk Soldier').
card_original_text('rhox bodyguard'/'CON', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhen Rhox Bodyguard comes into play, you gain 3 life.').
card_first_print('rhox bodyguard', 'CON').
card_image_name('rhox bodyguard'/'CON', 'rhox bodyguard').
card_uid('rhox bodyguard'/'CON', 'CON:Rhox Bodyguard:rhox bodyguard').
card_rarity('rhox bodyguard'/'CON', 'Common').
card_artist('rhox bodyguard'/'CON', 'Sal Villagran').
card_number('rhox bodyguard'/'CON', '122').
card_flavor_text('rhox bodyguard'/'CON', 'An enlightened soul caged in a body made for battle.').
card_multiverse_id('rhox bodyguard'/'CON', '177924').

card_in_set('rhox meditant', 'CON').
card_original_type('rhox meditant'/'CON', 'Creature — Rhino Monk').
card_original_text('rhox meditant'/'CON', 'When Rhox Meditant comes into play, if you control a green permanent, draw a card.').
card_first_print('rhox meditant', 'CON').
card_image_name('rhox meditant'/'CON', 'rhox meditant').
card_uid('rhox meditant'/'CON', 'CON:Rhox Meditant:rhox meditant').
card_rarity('rhox meditant'/'CON', 'Common').
card_artist('rhox meditant'/'CON', 'Donato Giancola').
card_number('rhox meditant'/'CON', '16').
card_flavor_text('rhox meditant'/'CON', 'The weight of her conviction balances on the harmony of her soul.').
card_multiverse_id('rhox meditant'/'CON', '159634').

card_in_set('rotting rats', 'CON').
card_original_type('rotting rats'/'CON', 'Creature — Zombie Rat').
card_original_text('rotting rats'/'CON', 'When Rotting Rats comes into play, each player discards a card.\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('rotting rats', 'CON').
card_image_name('rotting rats'/'CON', 'rotting rats').
card_uid('rotting rats'/'CON', 'CON:Rotting Rats:rotting rats').
card_rarity('rotting rats'/'CON', 'Common').
card_artist('rotting rats'/'CON', 'Dave Allsop').
card_number('rotting rats'/'CON', '51').
card_multiverse_id('rotting rats'/'CON', '150833').

card_in_set('rupture spire', 'CON').
card_original_type('rupture spire'/'CON', 'Land').
card_original_text('rupture spire'/'CON', 'Rupture Spire comes into play tapped.\nWhen Rupture Spire comes into play, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('rupture spire', 'CON').
card_image_name('rupture spire'/'CON', 'rupture spire').
card_uid('rupture spire'/'CON', 'CON:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'CON', 'Common').
card_artist('rupture spire'/'CON', 'Jaime Jones').
card_number('rupture spire'/'CON', '144').
card_multiverse_id('rupture spire'/'CON', '142301').

card_in_set('sacellum archers', 'CON').
card_original_type('sacellum archers'/'CON', 'Creature — Elf Archer').
card_original_text('sacellum archers'/'CON', '{R}{W}, {T}: Sacellum Archers deals 2 damage to target attacking or blocking creature.').
card_first_print('sacellum archers', 'CON').
card_image_name('sacellum archers'/'CON', 'sacellum archers').
card_uid('sacellum archers'/'CON', 'CON:Sacellum Archers:sacellum archers').
card_rarity('sacellum archers'/'CON', 'Uncommon').
card_artist('sacellum archers'/'CON', 'Kev Walker').
card_number('sacellum archers'/'CON', '89').
card_flavor_text('sacellum archers'/'CON', '\"Our arrows are aimed not at the sacred behemoths but at those who dare to dream of such a trophy.\"').
card_multiverse_id('sacellum archers'/'CON', '179488').

card_in_set('salvage slasher', 'CON').
card_original_type('salvage slasher'/'CON', 'Artifact Creature — Human Rogue').
card_original_text('salvage slasher'/'CON', 'Salvage Slasher gets +1/+0 for each artifact card in your graveyard.').
card_first_print('salvage slasher', 'CON').
card_image_name('salvage slasher'/'CON', 'salvage slasher').
card_uid('salvage slasher'/'CON', 'CON:Salvage Slasher:salvage slasher').
card_rarity('salvage slasher'/'CON', 'Common').
card_artist('salvage slasher'/'CON', 'Anthony Francisco').
card_number('salvage slasher'/'CON', '52').
card_flavor_text('salvage slasher'/'CON', 'Esper artificers never imagined that one day their scraps would be pressed against their soft and fragile throats.').
card_multiverse_id('salvage slasher'/'CON', '179254').

card_in_set('scarland thrinax', 'CON').
card_original_type('scarland thrinax'/'CON', 'Creature — Lizard').
card_original_text('scarland thrinax'/'CON', 'Sacrifice a creature: Put a +1/+1 counter on Scarland Thrinax.').
card_first_print('scarland thrinax', 'CON').
card_image_name('scarland thrinax'/'CON', 'scarland thrinax').
card_uid('scarland thrinax'/'CON', 'CON:Scarland Thrinax:scarland thrinax').
card_rarity('scarland thrinax'/'CON', 'Uncommon').
card_artist('scarland thrinax'/'CON', 'Daarken').
card_number('scarland thrinax'/'CON', '123').
card_flavor_text('scarland thrinax'/'CON', '\"There is only one way of life in Jund: feed on the weak until you are cut down by something stronger.\"\n—Jorshu of Clan Nel Toth').
card_multiverse_id('scarland thrinax'/'CON', '183025').

card_in_set('scattershot archer', 'CON').
card_original_type('scattershot archer'/'CON', 'Creature — Elf Archer').
card_original_text('scattershot archer'/'CON', '{T}: Scattershot Archer deals 1 damage to each creature with flying.').
card_first_print('scattershot archer', 'CON').
card_image_name('scattershot archer'/'CON', 'scattershot archer').
card_uid('scattershot archer'/'CON', 'CON:Scattershot Archer:scattershot archer').
card_rarity('scattershot archer'/'CON', 'Common').
card_artist('scattershot archer'/'CON', 'Steve Argyle').
card_number('scattershot archer'/'CON', '90').
card_flavor_text('scattershot archer'/'CON', 'To train her elves for war, Mayael would drop a sackful of acorns from the tree canopy. Each archer tried to split as many as possible before the acorns hit the forest floor below.').
card_multiverse_id('scattershot archer'/'CON', '185828').

card_in_set('scepter of dominance', 'CON').
card_original_type('scepter of dominance'/'CON', 'Artifact').
card_original_text('scepter of dominance'/'CON', '{W}, {T}: Tap target permanent.').
card_first_print('scepter of dominance', 'CON').
card_image_name('scepter of dominance'/'CON', 'scepter of dominance').
card_uid('scepter of dominance'/'CON', 'CON:Scepter of Dominance:scepter of dominance').
card_rarity('scepter of dominance'/'CON', 'Rare').
card_artist('scepter of dominance'/'CON', 'Howard Lyon').
card_number('scepter of dominance'/'CON', '17').
card_flavor_text('scepter of dominance'/'CON', '\"Whether or not you will bow to me is not open to debate. The question is, will I ever let you rise?\"\n—Fridius, telemin master').
card_multiverse_id('scepter of dominance'/'CON', '174792').

card_in_set('scepter of fugue', 'CON').
card_original_type('scepter of fugue'/'CON', 'Artifact').
card_original_text('scepter of fugue'/'CON', '{1}{B}, {T}: Target player discards a card. Play this ability only during your turn.').
card_first_print('scepter of fugue', 'CON').
card_image_name('scepter of fugue'/'CON', 'scepter of fugue').
card_uid('scepter of fugue'/'CON', 'CON:Scepter of Fugue:scepter of fugue').
card_rarity('scepter of fugue'/'CON', 'Rare').
card_artist('scepter of fugue'/'CON', 'Franz Vohwinkel').
card_number('scepter of fugue'/'CON', '53').
card_flavor_text('scepter of fugue'/'CON', 'One goes to Tidehollow either to forget or to be forgotten. Either way, the scullers will oblige.').
card_multiverse_id('scepter of fugue'/'CON', '189084').

card_in_set('scepter of insight', 'CON').
card_original_type('scepter of insight'/'CON', 'Artifact').
card_original_text('scepter of insight'/'CON', '{3}{U}, {T}: Draw a card.').
card_first_print('scepter of insight', 'CON').
card_image_name('scepter of insight'/'CON', 'scepter of insight').
card_uid('scepter of insight'/'CON', 'CON:Scepter of Insight:scepter of insight').
card_rarity('scepter of insight'/'CON', 'Rare').
card_artist('scepter of insight'/'CON', 'Steven Belledin').
card_number('scepter of insight'/'CON', '33').
card_flavor_text('scepter of insight'/'CON', '\"The road to truth has many branches, and so must the cane with which I walk it.\"\n—Voln the Elder').
card_multiverse_id('scepter of insight'/'CON', '184989').

card_in_set('scornful æther-lich', 'CON').
card_original_type('scornful æther-lich'/'CON', 'Artifact Creature — Zombie Wizard').
card_original_text('scornful æther-lich'/'CON', '{W}{B}: Scornful Æther-Lich gains fear and vigilance until end of turn.').
card_first_print('scornful æther-lich', 'CON').
card_image_name('scornful æther-lich'/'CON', 'scornful aether-lich').
card_uid('scornful æther-lich'/'CON', 'CON:Scornful Æther-Lich:scornful aether-lich').
card_rarity('scornful æther-lich'/'CON', 'Uncommon').
card_artist('scornful æther-lich'/'CON', 'Steven Belledin').
card_number('scornful æther-lich'/'CON', '34').
card_flavor_text('scornful æther-lich'/'CON', '\"With no flesh, there is no pain, no hesitation, no emotion of any kind. He is crafted perfection.\"\n—Tezzeret').
card_multiverse_id('scornful æther-lich'/'CON', '186328').

card_in_set('sedraxis alchemist', 'CON').
card_original_type('sedraxis alchemist'/'CON', 'Creature — Zombie Wizard').
card_original_text('sedraxis alchemist'/'CON', 'When Sedraxis Alchemist comes into play, if you control a blue permanent, return target nonland permanent to its owner\'s hand.').
card_first_print('sedraxis alchemist', 'CON').
card_image_name('sedraxis alchemist'/'CON', 'sedraxis alchemist').
card_uid('sedraxis alchemist'/'CON', 'CON:Sedraxis Alchemist:sedraxis alchemist').
card_rarity('sedraxis alchemist'/'CON', 'Common').
card_artist('sedraxis alchemist'/'CON', 'Karl Kopinski').
card_number('sedraxis alchemist'/'CON', '54').
card_flavor_text('sedraxis alchemist'/'CON', 'The problem with a liquid that can dissolve anything is finding something to carry it in.').
card_multiverse_id('sedraxis alchemist'/'CON', '182306').

card_in_set('shambling remains', 'CON').
card_original_type('shambling remains'/'CON', 'Creature — Zombie Horror').
card_original_text('shambling remains'/'CON', 'Shambling Remains can\'t block.\nUnearth {B}{R} ({B}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('shambling remains', 'CON').
card_image_name('shambling remains'/'CON', 'shambling remains').
card_uid('shambling remains'/'CON', 'CON:Shambling Remains:shambling remains').
card_rarity('shambling remains'/'CON', 'Uncommon').
card_artist('shambling remains'/'CON', 'Nils Hamm').
card_number('shambling remains'/'CON', '124').
card_multiverse_id('shambling remains'/'CON', '184991').

card_in_set('shard convergence', 'CON').
card_original_type('shard convergence'/'CON', 'Sorcery').
card_original_text('shard convergence'/'CON', 'Search your library for a Plains card, an Island card, a Swamp card, and a Mountain card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_first_print('shard convergence', 'CON').
card_image_name('shard convergence'/'CON', 'shard convergence').
card_uid('shard convergence'/'CON', 'CON:Shard Convergence:shard convergence').
card_rarity('shard convergence'/'CON', 'Uncommon').
card_artist('shard convergence'/'CON', 'Vance Kovacs').
card_number('shard convergence'/'CON', '91').
card_flavor_text('shard convergence'/'CON', 'Alara is broken no more.').
card_multiverse_id('shard convergence'/'CON', '175034').

card_in_set('sigil of the empty throne', 'CON').
card_original_type('sigil of the empty throne'/'CON', 'Enchantment').
card_original_text('sigil of the empty throne'/'CON', 'Whenever you play an enchantment spell, put a 4/4 white Angel creature token with flying into play.').
card_first_print('sigil of the empty throne', 'CON').
card_image_name('sigil of the empty throne'/'CON', 'sigil of the empty throne').
card_uid('sigil of the empty throne'/'CON', 'CON:Sigil of the Empty Throne:sigil of the empty throne').
card_rarity('sigil of the empty throne'/'CON', 'Rare').
card_artist('sigil of the empty throne'/'CON', 'Cyril Van Der Haegen').
card_number('sigil of the empty throne'/'CON', '18').
card_flavor_text('sigil of the empty throne'/'CON', 'When Asha left Bant, she ensured that the world would have protection and order in her absence.').
card_multiverse_id('sigil of the empty throne'/'CON', '180271').

card_in_set('skyward eye prophets', 'CON').
card_original_type('skyward eye prophets'/'CON', 'Creature — Human Wizard').
card_original_text('skyward eye prophets'/'CON', 'Vigilance\n{T}: Reveal the top card of your library. If it\'s a land card, put it into play. Otherwise, put it into your hand.').
card_first_print('skyward eye prophets', 'CON').
card_image_name('skyward eye prophets'/'CON', 'skyward eye prophets').
card_uid('skyward eye prophets'/'CON', 'CON:Skyward Eye Prophets:skyward eye prophets').
card_rarity('skyward eye prophets'/'CON', 'Uncommon').
card_artist('skyward eye prophets'/'CON', 'Matt Stewart').
card_number('skyward eye prophets'/'CON', '125').
card_flavor_text('skyward eye prophets'/'CON', 'They lament the doom that is coming to Bant without realizing the part their own leaders have played in it.').
card_multiverse_id('skyward eye prophets'/'CON', '174953').

card_in_set('sludge strider', 'CON').
card_original_type('sludge strider'/'CON', 'Artifact Creature — Insect').
card_original_text('sludge strider'/'CON', 'Whenever another artifact comes into play under your control or another artifact you control leaves play, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.').
card_first_print('sludge strider', 'CON').
card_image_name('sludge strider'/'CON', 'sludge strider').
card_uid('sludge strider'/'CON', 'CON:Sludge Strider:sludge strider').
card_rarity('sludge strider'/'CON', 'Uncommon').
card_artist('sludge strider'/'CON', 'Franz Vohwinkel').
card_number('sludge strider'/'CON', '126').
card_flavor_text('sludge strider'/'CON', 'Underneath the cities of Esper are cycles of life unseen by those who feed them.').
card_multiverse_id('sludge strider'/'CON', '183019').

card_in_set('soul\'s majesty', 'CON').
card_original_type('soul\'s majesty'/'CON', 'Sorcery').
card_original_text('soul\'s majesty'/'CON', 'Draw cards equal to the power of target creature you control.').
card_first_print('soul\'s majesty', 'CON').
card_image_name('soul\'s majesty'/'CON', 'soul\'s majesty').
card_uid('soul\'s majesty'/'CON', 'CON:Soul\'s Majesty:soul\'s majesty').
card_rarity('soul\'s majesty'/'CON', 'Rare').
card_artist('soul\'s majesty'/'CON', 'Jesper Ejsing').
card_number('soul\'s majesty'/'CON', '92').
card_flavor_text('soul\'s majesty'/'CON', 'An avatar he sculpts of wisdom and strength.').
card_multiverse_id('soul\'s majesty'/'CON', '185137').

card_in_set('sphinx summoner', 'CON').
card_original_type('sphinx summoner'/'CON', 'Artifact Creature — Sphinx').
card_original_text('sphinx summoner'/'CON', 'Flying\nWhen Sphinx Summoner comes into play, you may search your library for an artifact creature card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('sphinx summoner', 'CON').
card_image_name('sphinx summoner'/'CON', 'sphinx summoner').
card_uid('sphinx summoner'/'CON', 'CON:Sphinx Summoner:sphinx summoner').
card_rarity('sphinx summoner'/'CON', 'Rare').
card_artist('sphinx summoner'/'CON', 'Jaime Jones').
card_number('sphinx summoner'/'CON', '127').
card_multiverse_id('sphinx summoner'/'CON', '189079').

card_in_set('spore burst', 'CON').
card_original_type('spore burst'/'CON', 'Sorcery').
card_original_text('spore burst'/'CON', 'Domain Put a 1/1 green Saproling creature token into play for each basic land type among lands you control.').
card_first_print('spore burst', 'CON').
card_image_name('spore burst'/'CON', 'spore burst').
card_uid('spore burst'/'CON', 'CON:Spore Burst:spore burst').
card_rarity('spore burst'/'CON', 'Uncommon').
card_artist('spore burst'/'CON', 'Daarken').
card_number('spore burst'/'CON', '93').
card_flavor_text('spore burst'/'CON', 'Seeds from Jund don\'t drift gently on the wind. They get up and stampede.').
card_multiverse_id('spore burst'/'CON', '185829').

card_in_set('suicidal charge', 'CON').
card_original_type('suicidal charge'/'CON', 'Enchantment').
card_original_text('suicidal charge'/'CON', 'Sacrifice Suicidal Charge: Creatures your opponents control get -1/-1 until end of turn. Those creatures attack this turn if able.').
card_first_print('suicidal charge', 'CON').
card_image_name('suicidal charge'/'CON', 'suicidal charge').
card_uid('suicidal charge'/'CON', 'CON:Suicidal Charge:suicidal charge').
card_rarity('suicidal charge'/'CON', 'Common').
card_artist('suicidal charge'/'CON', 'Daarken').
card_number('suicidal charge'/'CON', '128').
card_flavor_text('suicidal charge'/'CON', '\"They think they\'re winning. But they\'re just lining up to be dinner.\"\n—Rakka Mar').
card_multiverse_id('suicidal charge'/'CON', '180345').

card_in_set('sylvan bounty', 'CON').
card_original_type('sylvan bounty'/'CON', 'Instant').
card_original_text('sylvan bounty'/'CON', 'Target player gains 8 life.\nBasic landcycling {1}{G} ({1}{G}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('sylvan bounty', 'CON').
card_image_name('sylvan bounty'/'CON', 'sylvan bounty').
card_uid('sylvan bounty'/'CON', 'CON:Sylvan Bounty:sylvan bounty').
card_rarity('sylvan bounty'/'CON', 'Common').
card_artist('sylvan bounty'/'CON', 'Chris Rahn').
card_number('sylvan bounty'/'CON', '94').
card_flavor_text('sylvan bounty'/'CON', 'Some who scouted new lands chose to stay.').
card_multiverse_id('sylvan bounty'/'CON', '179523').

card_in_set('telemin performance', 'CON').
card_original_type('telemin performance'/'CON', 'Sorcery').
card_original_text('telemin performance'/'CON', 'Target opponent reveals cards from the top of his or her library until he or she reveals a creature card. That player puts all noncreature cards revealed this way into his or her graveyard, then you put the creature card into play under your control.').
card_first_print('telemin performance', 'CON').
card_image_name('telemin performance'/'CON', 'telemin performance').
card_uid('telemin performance'/'CON', 'CON:Telemin Performance:telemin performance').
card_rarity('telemin performance'/'CON', 'Rare').
card_artist('telemin performance'/'CON', 'Izzy').
card_number('telemin performance'/'CON', '35').
card_multiverse_id('telemin performance'/'CON', '189085').

card_in_set('thornling', 'CON').
card_original_type('thornling'/'CON', 'Creature — Elemental Shapeshifter').
card_original_text('thornling'/'CON', '{G}: Thornling gains haste until end of turn.\n{G}: Thornling gains trample until end of turn.\n{G}: Thornling is indestructible this turn.\n{1}: Thornling gets +1/-1 until end of turn.\n{1}: Thornling gets -1/+1 until end of turn.').
card_first_print('thornling', 'CON').
card_image_name('thornling'/'CON', 'thornling').
card_uid('thornling'/'CON', 'CON:Thornling:thornling').
card_rarity('thornling'/'CON', 'Mythic Rare').
card_artist('thornling'/'CON', 'Kev Walker').
card_number('thornling'/'CON', '95').
card_multiverse_id('thornling'/'CON', '180341').

card_in_set('toxic iguanar', 'CON').
card_original_type('toxic iguanar'/'CON', 'Creature — Lizard').
card_original_text('toxic iguanar'/'CON', 'Toxic Iguanar has deathtouch as long as you control a green permanent. (Whenever it deals damage to a creature, destroy that creature.)').
card_first_print('toxic iguanar', 'CON').
card_image_name('toxic iguanar'/'CON', 'toxic iguanar').
card_uid('toxic iguanar'/'CON', 'CON:Toxic Iguanar:toxic iguanar').
card_rarity('toxic iguanar'/'CON', 'Common').
card_artist('toxic iguanar'/'CON', 'Brandon Kitkouski').
card_number('toxic iguanar'/'CON', '72').
card_flavor_text('toxic iguanar'/'CON', 'There are no \"weak\" creatures on Jund. Even the smallest can strike a deadly blow.').
card_multiverse_id('toxic iguanar'/'CON', '170956').

card_in_set('traumatic visions', 'CON').
card_original_type('traumatic visions'/'CON', 'Instant').
card_original_text('traumatic visions'/'CON', 'Counter target spell.\nBasic landcycling {1}{U} ({1}{U}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('traumatic visions', 'CON').
card_image_name('traumatic visions'/'CON', 'traumatic visions').
card_uid('traumatic visions'/'CON', 'CON:Traumatic Visions:traumatic visions').
card_rarity('traumatic visions'/'CON', 'Common').
card_artist('traumatic visions'/'CON', 'Cyril Van Der Haegen').
card_number('traumatic visions'/'CON', '36').
card_multiverse_id('traumatic visions'/'CON', '179508').

card_in_set('tukatongue thallid', 'CON').
card_original_type('tukatongue thallid'/'CON', 'Creature — Fungus').
card_original_text('tukatongue thallid'/'CON', 'When Tukatongue Thallid is put into a graveyard from play, put a 1/1 green Saproling creature token into play.').
card_first_print('tukatongue thallid', 'CON').
card_image_name('tukatongue thallid'/'CON', 'tukatongue thallid').
card_uid('tukatongue thallid'/'CON', 'CON:Tukatongue Thallid:tukatongue thallid').
card_rarity('tukatongue thallid'/'CON', 'Common').
card_artist('tukatongue thallid'/'CON', 'Vance Kovacs').
card_number('tukatongue thallid'/'CON', '96').
card_flavor_text('tukatongue thallid'/'CON', 'Jund\'s thallids tried to disguise their deliciousness by covering themselves in spines harvested from the tukatongue tree.').
card_multiverse_id('tukatongue thallid'/'CON', '184995').

card_in_set('unstable frontier', 'CON').
card_original_type('unstable frontier'/'CON', 'Land').
card_original_text('unstable frontier'/'CON', '{T}: Add {1} to your mana pool.\n{T}: Target land you control becomes the basic land type of your choice until end of turn.').
card_first_print('unstable frontier', 'CON').
card_image_name('unstable frontier'/'CON', 'unstable frontier').
card_uid('unstable frontier'/'CON', 'CON:Unstable Frontier:unstable frontier').
card_rarity('unstable frontier'/'CON', 'Uncommon').
card_artist('unstable frontier'/'CON', 'John Avon').
card_number('unstable frontier'/'CON', '145').
card_flavor_text('unstable frontier'/'CON', 'The goblins went to sleep in Jund and awoke in Grixis. They\'d never seen a zombie before. They would never see one again.').
card_multiverse_id('unstable frontier'/'CON', '189078').

card_in_set('unsummon', 'CON').
card_original_type('unsummon'/'CON', 'Instant').
card_original_text('unsummon'/'CON', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'CON', 'unsummon').
card_uid('unsummon'/'CON', 'CON:Unsummon:unsummon').
card_rarity('unsummon'/'CON', 'Common').
card_artist('unsummon'/'CON', 'Izzy').
card_number('unsummon'/'CON', '37').
card_flavor_text('unsummon'/'CON', '\"Send it back to its own strange world so we can avoid another inglorious combat with these wicked beings.\"\n—Captain Valek of Jhess').
card_multiverse_id('unsummon'/'CON', '177932').

card_in_set('vagrant plowbeasts', 'CON').
card_original_type('vagrant plowbeasts'/'CON', 'Creature — Beast').
card_original_text('vagrant plowbeasts'/'CON', '{1}: Regenerate target creature with power 5 or greater.').
card_first_print('vagrant plowbeasts', 'CON').
card_image_name('vagrant plowbeasts'/'CON', 'vagrant plowbeasts').
card_uid('vagrant plowbeasts'/'CON', 'CON:Vagrant Plowbeasts:vagrant plowbeasts').
card_rarity('vagrant plowbeasts'/'CON', 'Uncommon').
card_artist('vagrant plowbeasts'/'CON', 'Zoltan Boros & Gabor Szikszai').
card_number('vagrant plowbeasts'/'CON', '129').
card_flavor_text('vagrant plowbeasts'/'CON', 'Plowbeasts of Naya escaped their harnesses in droves, content to snack on the conveniently cultivated fields of Eos and Valeron.').
card_multiverse_id('vagrant plowbeasts'/'CON', '180344').

card_in_set('valeron outlander', 'CON').
card_original_type('valeron outlander'/'CON', 'Creature — Human Scout').
card_original_text('valeron outlander'/'CON', 'Protection from black').
card_first_print('valeron outlander', 'CON').
card_image_name('valeron outlander'/'CON', 'valeron outlander').
card_uid('valeron outlander'/'CON', 'CON:Valeron Outlander:valeron outlander').
card_rarity('valeron outlander'/'CON', 'Common').
card_artist('valeron outlander'/'CON', 'Matt Stewart').
card_number('valeron outlander'/'CON', '130').
card_flavor_text('valeron outlander'/'CON', 'After years of honing her philosophy in debate with stubborn rhoxes, Niella was ready to convert any heathen.').
card_multiverse_id('valeron outlander'/'CON', '185144').

card_in_set('valiant guard', 'CON').
card_original_type('valiant guard'/'CON', 'Creature — Human Soldier').
card_original_text('valiant guard'/'CON', '').
card_first_print('valiant guard', 'CON').
card_image_name('valiant guard'/'CON', 'valiant guard').
card_uid('valiant guard'/'CON', 'CON:Valiant Guard:valiant guard').
card_rarity('valiant guard'/'CON', 'Common').
card_artist('valiant guard'/'CON', 'Chris Rahn').
card_number('valiant guard'/'CON', '19').
card_flavor_text('valiant guard'/'CON', 'As the outsiders invaded Bant, soldiers who once saw sigils as the highest marks of glory began to see the scars of battle as tokens of equal worth.').
card_multiverse_id('valiant guard'/'CON', '184984').

card_in_set('vectis agents', 'CON').
card_original_type('vectis agents'/'CON', 'Artifact Creature — Human Rogue').
card_original_text('vectis agents'/'CON', '{U}{B}: Vectis Agents gets -2/-0 until end of turn and is unblockable this turn.').
card_first_print('vectis agents', 'CON').
card_image_name('vectis agents'/'CON', 'vectis agents').
card_uid('vectis agents'/'CON', 'CON:Vectis Agents:vectis agents').
card_rarity('vectis agents'/'CON', 'Common').
card_artist('vectis agents'/'CON', 'Chippy').
card_number('vectis agents'/'CON', '131').
card_flavor_text('vectis agents'/'CON', 'With their life energy suppressed under their etherium enhancements, thieves from Esper found it surprisingly easy to explore the ruins of Grixis.').
card_multiverse_id('vectis agents'/'CON', '183063').

card_in_set('vedalken outlander', 'CON').
card_original_type('vedalken outlander'/'CON', 'Artifact Creature — Vedalken Scout').
card_original_text('vedalken outlander'/'CON', 'Protection from red').
card_first_print('vedalken outlander', 'CON').
card_image_name('vedalken outlander'/'CON', 'vedalken outlander').
card_uid('vedalken outlander'/'CON', 'CON:Vedalken Outlander:vedalken outlander').
card_rarity('vedalken outlander'/'CON', 'Common').
card_artist('vedalken outlander'/'CON', 'Izzy').
card_number('vedalken outlander'/'CON', '132').
card_flavor_text('vedalken outlander'/'CON', 'The Seekers of Carmot searched across the unknown lands for the mystical red stone that could reforge Esper in ethereal perfection.').
card_multiverse_id('vedalken outlander'/'CON', '185141').

card_in_set('viashino slaughtermaster', 'CON').
card_original_type('viashino slaughtermaster'/'CON', 'Creature — Viashino Warrior').
card_original_text('viashino slaughtermaster'/'CON', 'Double strike\n{B}{G}: Viashino Slaughtermaster gets +1/+1 until end of turn. Play this ability only once each turn.').
card_first_print('viashino slaughtermaster', 'CON').
card_image_name('viashino slaughtermaster'/'CON', 'viashino slaughtermaster').
card_uid('viashino slaughtermaster'/'CON', 'CON:Viashino Slaughtermaster:viashino slaughtermaster').
card_rarity('viashino slaughtermaster'/'CON', 'Uncommon').
card_artist('viashino slaughtermaster'/'CON', 'Raymond Swanland').
card_number('viashino slaughtermaster'/'CON', '73').
card_flavor_text('viashino slaughtermaster'/'CON', '\"I\'ll fight two at once, and then lick their guts from my blades.\"').
card_multiverse_id('viashino slaughtermaster'/'CON', '179245').

card_in_set('view from above', 'CON').
card_original_type('view from above'/'CON', 'Instant').
card_original_text('view from above'/'CON', 'Target creature gains flying until end of turn. If you control a white permanent, return View from Above to its owner\'s hand.').
card_first_print('view from above', 'CON').
card_image_name('view from above'/'CON', 'view from above').
card_uid('view from above'/'CON', 'CON:View from Above:view from above').
card_rarity('view from above'/'CON', 'Uncommon').
card_artist('view from above'/'CON', 'Howard Lyon').
card_number('view from above'/'CON', '38').
card_flavor_text('view from above'/'CON', '\"This air feels so heavy and thick. There are no winds to speak of. I fear the knowledge that comes from a place like this.\"').
card_multiverse_id('view from above'/'CON', '183058').

card_in_set('voices from the void', 'CON').
card_original_type('voices from the void'/'CON', 'Sorcery').
card_original_text('voices from the void'/'CON', 'Domain Target player discards a card for each basic land type among lands you control.').
card_first_print('voices from the void', 'CON').
card_image_name('voices from the void'/'CON', 'voices from the void').
card_uid('voices from the void'/'CON', 'CON:Voices from the Void:voices from the void').
card_rarity('voices from the void'/'CON', 'Uncommon').
card_artist('voices from the void'/'CON', 'rk post').
card_number('voices from the void'/'CON', '55').
card_flavor_text('voices from the void'/'CON', 'As Grixis collided with the rest of Alara, the worlds began to hear the hateful whispers of the forgotten dead.').
card_multiverse_id('voices from the void'/'CON', '150847').

card_in_set('volcanic fallout', 'CON').
card_original_type('volcanic fallout'/'CON', 'Instant').
card_original_text('volcanic fallout'/'CON', 'Volcanic Fallout can\'t be countered.\nVolcanic Fallout deals 2 damage to each creature and each player.').
card_image_name('volcanic fallout'/'CON', 'volcanic fallout').
card_uid('volcanic fallout'/'CON', 'CON:Volcanic Fallout:volcanic fallout').
card_rarity('volcanic fallout'/'CON', 'Uncommon').
card_artist('volcanic fallout'/'CON', 'Zoltan Boros & Gabor Szikszai').
card_number('volcanic fallout'/'CON', '74').
card_flavor_text('volcanic fallout'/'CON', '\"How can we outrun the sky?\"\n—Hadran, sunseeder of Naya').
card_multiverse_id('volcanic fallout'/'CON', '180274').

card_in_set('voracious dragon', 'CON').
card_original_type('voracious dragon'/'CON', 'Creature — Dragon').
card_original_text('voracious dragon'/'CON', 'Flying\nDevour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)\nWhen Voracious Dragon comes into play, it deals damage to target creature or player equal to twice the number of Goblins it devoured.').
card_first_print('voracious dragon', 'CON').
card_image_name('voracious dragon'/'CON', 'voracious dragon').
card_uid('voracious dragon'/'CON', 'CON:Voracious Dragon:voracious dragon').
card_rarity('voracious dragon'/'CON', 'Rare').
card_artist('voracious dragon'/'CON', 'Dominick Domingo').
card_number('voracious dragon'/'CON', '75').
card_multiverse_id('voracious dragon'/'CON', '186329').

card_in_set('wall of reverence', 'CON').
card_original_type('wall of reverence'/'CON', 'Creature — Spirit Wall').
card_original_text('wall of reverence'/'CON', 'Defender, flying\nAt the end of your turn, you may gain life equal to the power of target creature you control.').
card_first_print('wall of reverence', 'CON').
card_image_name('wall of reverence'/'CON', 'wall of reverence').
card_uid('wall of reverence'/'CON', 'CON:Wall of Reverence:wall of reverence').
card_rarity('wall of reverence'/'CON', 'Rare').
card_artist('wall of reverence'/'CON', 'Wayne Reynolds').
card_number('wall of reverence'/'CON', '20').
card_flavor_text('wall of reverence'/'CON', 'The lives of elves are long, but their memories are longer. Even after death, they do not desert their homes.').
card_multiverse_id('wall of reverence'/'CON', '189874').

card_in_set('wandering goblins', 'CON').
card_original_type('wandering goblins'/'CON', 'Creature — Goblin Warrior').
card_original_text('wandering goblins'/'CON', 'Domain {3}: Wandering Goblins gets +1/+0 until end of turn for each basic land type among lands you control.').
card_first_print('wandering goblins', 'CON').
card_image_name('wandering goblins'/'CON', 'wandering goblins').
card_uid('wandering goblins'/'CON', 'CON:Wandering Goblins:wandering goblins').
card_rarity('wandering goblins'/'CON', 'Common').
card_artist('wandering goblins'/'CON', 'Karl Kopinski').
card_number('wandering goblins'/'CON', '76').
card_flavor_text('wandering goblins'/'CON', 'Tired of waiting for a dragon to eat them, some hardy goblins struck out to become meals for the unknown.').
card_multiverse_id('wandering goblins'/'CON', '180279').

card_in_set('wild leotau', 'CON').
card_original_type('wild leotau'/'CON', 'Creature — Cat').
card_original_text('wild leotau'/'CON', 'At the beginning of your upkeep, sacrifice Wild Leotau unless you pay {G}.').
card_first_print('wild leotau', 'CON').
card_image_name('wild leotau'/'CON', 'wild leotau').
card_uid('wild leotau'/'CON', 'CON:Wild Leotau:wild leotau').
card_rarity('wild leotau'/'CON', 'Common').
card_artist('wild leotau'/'CON', 'Michael Komarck').
card_number('wild leotau'/'CON', '97').
card_flavor_text('wild leotau'/'CON', '\"Leotau that were born wild make the best mounts. It\'s like riding a thunderstorm.\"\n—Rafiq of the Many').
card_multiverse_id('wild leotau'/'CON', '179489').

card_in_set('worldheart phoenix', 'CON').
card_original_type('worldheart phoenix'/'CON', 'Creature — Phoenix').
card_original_text('worldheart phoenix'/'CON', 'Flying\nYou may play Worldheart Phoenix from your graveyard by paying {W}{U}{B}{R}{G} rather than paying its mana cost. If you do, it comes into play with two +1/+1 counters on it.').
card_first_print('worldheart phoenix', 'CON').
card_image_name('worldheart phoenix'/'CON', 'worldheart phoenix').
card_uid('worldheart phoenix'/'CON', 'CON:Worldheart Phoenix:worldheart phoenix').
card_rarity('worldheart phoenix'/'CON', 'Rare').
card_artist('worldheart phoenix'/'CON', 'Aleksi Briclot').
card_number('worldheart phoenix'/'CON', '77').
card_multiverse_id('worldheart phoenix'/'CON', '175401').

card_in_set('worldly counsel', 'CON').
card_original_type('worldly counsel'/'CON', 'Instant').
card_original_text('worldly counsel'/'CON', 'Domain Look at the top X cards of your library, where X is the number of basic land types among lands you control. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_image_name('worldly counsel'/'CON', 'worldly counsel').
card_uid('worldly counsel'/'CON', 'CON:Worldly Counsel:worldly counsel').
card_rarity('worldly counsel'/'CON', 'Common').
card_artist('worldly counsel'/'CON', 'Matt Cavotta').
card_number('worldly counsel'/'CON', '39').
card_flavor_text('worldly counsel'/'CON', 'Every horizon hides a new possibility.').
card_multiverse_id('worldly counsel'/'CON', '142298').

card_in_set('wretched banquet', 'CON').
card_original_type('wretched banquet'/'CON', 'Sorcery').
card_original_text('wretched banquet'/'CON', 'Destroy target creature if it has the least power or is tied for least power among creatures in play.').
card_first_print('wretched banquet', 'CON').
card_image_name('wretched banquet'/'CON', 'wretched banquet').
card_uid('wretched banquet'/'CON', 'CON:Wretched Banquet:wretched banquet').
card_rarity('wretched banquet'/'CON', 'Common').
card_artist('wretched banquet'/'CON', 'Nils Hamm').
card_number('wretched banquet'/'CON', '56').
card_flavor_text('wretched banquet'/'CON', '\"The meek inherit nothing.\"\n—Sedris, the Traitor King').
card_multiverse_id('wretched banquet'/'CON', '174907').

card_in_set('yoke of the damned', 'CON').
card_original_type('yoke of the damned'/'CON', 'Enchantment — Aura').
card_original_text('yoke of the damned'/'CON', 'Enchant creature\nWhen a creature is put into a graveyard from play, destroy enchanted creature.').
card_first_print('yoke of the damned', 'CON').
card_image_name('yoke of the damned'/'CON', 'yoke of the damned').
card_uid('yoke of the damned'/'CON', 'CON:Yoke of the Damned:yoke of the damned').
card_rarity('yoke of the damned'/'CON', 'Common').
card_artist('yoke of the damned'/'CON', 'Paul Bonner').
card_number('yoke of the damned'/'CON', '57').
card_flavor_text('yoke of the damned'/'CON', 'The demon\'s yoke is part leash, part noose.').
card_multiverse_id('yoke of the damned'/'CON', '179891').

card_in_set('zombie outlander', 'CON').
card_original_type('zombie outlander'/'CON', 'Creature — Zombie Scout').
card_original_text('zombie outlander'/'CON', 'Protection from green').
card_first_print('zombie outlander', 'CON').
card_image_name('zombie outlander'/'CON', 'zombie outlander').
card_uid('zombie outlander'/'CON', 'CON:Zombie Outlander:zombie outlander').
card_rarity('zombie outlander'/'CON', 'Common').
card_artist('zombie outlander'/'CON', 'Nils Hamm').
card_number('zombie outlander'/'CON', '133').
card_flavor_text('zombie outlander'/'CON', 'The ripe smell of life drifted into Grixis. The dead caught the scent and with reckless hunger followed it back into Jund.').
card_multiverse_id('zombie outlander'/'CON', '185138').
