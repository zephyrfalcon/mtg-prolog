% Celebration

set('pCEL').
set_name('pCEL', 'Celebration').
set_release_date('pCEL', '1996-08-14').
set_border('pCEL', 'black').
set_type('pCEL', 'promo').

card_in_set('1996 world champion', 'pCEL').
card_original_type('1996 world champion'/'pCEL', 'Summon — Legend').
card_original_text('1996 world champion'/'pCEL', '').
card_first_print('1996 world champion', 'pCEL').
card_image_name('1996 world champion'/'pCEL', '1996 world champion').
card_uid('1996 world champion'/'pCEL', 'pCEL:1996 World Champion:1996 world champion').
card_rarity('1996 world champion'/'pCEL', 'Special').
card_artist('1996 world champion'/'pCEL', 'Christopher Rush').
card_number('1996 world champion'/'pCEL', '1').
card_flavor_text('1996 world champion'/'pCEL', 'It takes great sacrifice to make it to the top.').

card_in_set('fraternal exaltation', 'pCEL').
card_original_type('fraternal exaltation'/'pCEL', 'Sorcery').
card_original_text('fraternal exaltation'/'pCEL', '').
card_first_print('fraternal exaltation', 'pCEL').
card_image_name('fraternal exaltation'/'pCEL', 'fraternal exaltation').
card_uid('fraternal exaltation'/'pCEL', 'pCEL:Fraternal Exaltation:fraternal exaltation').
card_rarity('fraternal exaltation'/'pCEL', 'Special').
card_artist('fraternal exaltation'/'pCEL', 'Susan Garfield').
card_number('fraternal exaltation'/'pCEL', '5').
card_flavor_text('fraternal exaltation'/'pCEL', '... and when you want to go explore, the number you should have is four. —Sandra Boynton').

card_in_set('proposal', 'pCEL').
card_original_type('proposal'/'pCEL', 'Sorcery').
card_original_text('proposal'/'pCEL', '').
card_first_print('proposal', 'pCEL').
card_image_name('proposal'/'pCEL', 'proposal').
card_uid('proposal'/'pCEL', 'pCEL:Proposal:proposal').
card_rarity('proposal'/'pCEL', 'Special').
card_artist('proposal'/'pCEL', 'Quinton Hoover').
card_number('proposal'/'pCEL', '3').

card_in_set('robot chicken', 'pCEL').
card_original_type('robot chicken'/'pCEL', 'Artifact Creature — Chicken Construct').
card_original_text('robot chicken'/'pCEL', '').
card_first_print('robot chicken', 'pCEL').
card_image_name('robot chicken'/'pCEL', 'robot chicken').
card_uid('robot chicken'/'pCEL', 'pCEL:Robot Chicken:robot chicken').
card_rarity('robot chicken'/'pCEL', 'Special').
card_artist('robot chicken'/'pCEL', 'Robot Chicken').
card_number('robot chicken'/'pCEL', '6').
card_flavor_text('robot chicken'/'pCEL', 'Why did the chicken cross the road? To die in the name of science.').

card_in_set('shichifukujin dragon', 'pCEL').
card_original_type('shichifukujin dragon'/'pCEL', 'Summon — Dragon').
card_original_text('shichifukujin dragon'/'pCEL', '').
card_first_print('shichifukujin dragon', 'pCEL').
card_image_name('shichifukujin dragon'/'pCEL', 'shichifukujin dragon').
card_uid('shichifukujin dragon'/'pCEL', 'pCEL:Shichifukujin Dragon:shichifukujin dragon').
card_rarity('shichifukujin dragon'/'pCEL', 'Special').
card_artist('shichifukujin dragon'/'pCEL', 'Christopher Rush').
card_number('shichifukujin dragon'/'pCEL', '2').

card_in_set('splendid genesis', 'pCEL').
card_original_type('splendid genesis'/'pCEL', 'Sorcery').
card_original_text('splendid genesis'/'pCEL', '').
card_first_print('splendid genesis', 'pCEL').
card_image_name('splendid genesis'/'pCEL', 'splendid genesis').
card_uid('splendid genesis'/'pCEL', 'pCEL:Splendid Genesis:splendid genesis').
card_rarity('splendid genesis'/'pCEL', 'Special').
card_artist('splendid genesis'/'pCEL', 'Monique Thirifay').
card_number('splendid genesis'/'pCEL', '4').
card_flavor_text('splendid genesis'/'pCEL', 'She discovered with great delight that one does not love one\'s children because they are one\'s children but because of the friendship formed while raising them. Gabriel Garciá Márquez').
