% Launch Parties

set('pLPA').
set_name('pLPA', 'Launch Parties').
set_release_date('pLPA', '2008-02-01').
set_border('pLPA', 'black').
set_type('pLPA', 'promo').

card_in_set('ajani vengeant', 'pLPA').
card_original_type('ajani vengeant'/'pLPA', 'Planeswalker — Ajani').
card_original_text('ajani vengeant'/'pLPA', '').
card_image_name('ajani vengeant'/'pLPA', 'ajani vengeant').
card_uid('ajani vengeant'/'pLPA', 'pLPA:Ajani Vengeant:ajani vengeant').
card_rarity('ajani vengeant'/'pLPA', 'Special').
card_artist('ajani vengeant'/'pLPA', 'Jason Chan').
card_number('ajani vengeant'/'pLPA', '4').

card_in_set('ancient hellkite', 'pLPA').
card_original_type('ancient hellkite'/'pLPA', 'Creature — Dragon').
card_original_text('ancient hellkite'/'pLPA', '').
card_first_print('ancient hellkite', 'pLPA').
card_image_name('ancient hellkite'/'pLPA', 'ancient hellkite').
card_uid('ancient hellkite'/'pLPA', 'pLPA:Ancient Hellkite:ancient hellkite').
card_rarity('ancient hellkite'/'pLPA', 'Special').
card_artist('ancient hellkite'/'pLPA', 'Wayne Reynolds').
card_number('ancient hellkite'/'pLPA', '11').
card_flavor_text('ancient hellkite'/'pLPA', 'When a dragon attacks, there are no bystanders.').

card_in_set('ant queen', 'pLPA').
card_original_type('ant queen'/'pLPA', 'Creature — Insect').
card_original_text('ant queen'/'pLPA', '').
card_first_print('ant queen', 'pLPA').
card_image_name('ant queen'/'pLPA', 'ant queen').
card_uid('ant queen'/'pLPA', 'pLPA:Ant Queen:ant queen').
card_rarity('ant queen'/'pLPA', 'Special').
card_artist('ant queen'/'pLPA', 'Ron Spencer').
card_number('ant queen'/'pLPA', '7').
card_flavor_text('ant queen'/'pLPA', '\"Kill the queen first, or we\'ll be fighting her drones forever. It is not in a queen\'s nature to have enough servants.\"\n—Borzard, exterminator captain').

card_in_set('bident of thassa', 'pLPA').
card_original_type('bident of thassa'/'pLPA', 'Legendary Enchantment Artifact').
card_original_text('bident of thassa'/'pLPA', '').
card_first_print('bident of thassa', 'pLPA').
card_image_name('bident of thassa'/'pLPA', 'bident of thassa').
card_uid('bident of thassa'/'pLPA', 'pLPA:Bident of Thassa:bident of thassa').
card_rarity('bident of thassa'/'pLPA', 'Special').
card_artist('bident of thassa'/'pLPA', 'Ryan Yee').
card_number('bident of thassa'/'pLPA', '24').
card_flavor_text('bident of thassa'/'pLPA', 'The wills of mortals shift as the tide ebbs and flows.').

card_in_set('breaking', 'pLPA').
card_original_type('breaking'/'pLPA', 'Sorcery').
card_original_text('breaking'/'pLPA', '').
card_first_print('breaking', 'pLPA').
card_image_name('breaking'/'pLPA', 'breakingentering').
card_uid('breaking'/'pLPA', 'pLPA:Breaking:breakingentering').
card_rarity('breaking'/'pLPA', 'Special').
card_artist('breaking'/'pLPA', 'Igor Kieryluk').
card_number('breaking'/'pLPA', '22a').

card_in_set('colossal whale', 'pLPA').
card_original_type('colossal whale'/'pLPA', 'Creature — Whale').
card_original_text('colossal whale'/'pLPA', '').
card_first_print('colossal whale', 'pLPA').
card_image_name('colossal whale'/'pLPA', 'colossal whale').
card_uid('colossal whale'/'pLPA', 'pLPA:Colossal Whale:colossal whale').
card_rarity('colossal whale'/'pLPA', 'Special').
card_artist('colossal whale'/'pLPA', 'Erica Yang').
card_number('colossal whale'/'pLPA', '23').

card_in_set('deadbridge goliath', 'pLPA').
card_original_type('deadbridge goliath'/'pLPA', 'Creature — Insect').
card_original_text('deadbridge goliath'/'pLPA', '').
card_first_print('deadbridge goliath', 'pLPA').
card_image_name('deadbridge goliath'/'pLPA', 'deadbridge goliath').
card_uid('deadbridge goliath'/'pLPA', 'pLPA:Deadbridge Goliath:deadbridge goliath').
card_rarity('deadbridge goliath'/'pLPA', 'Special').
card_artist('deadbridge goliath'/'pLPA', 'Nils Hamm').
card_number('deadbridge goliath'/'pLPA', '20').
card_flavor_text('deadbridge goliath'/'pLPA', 'Some Golgari insects live for centuries—and they never stop growing.').

card_in_set('deathbringer regent', 'pLPA').
card_original_type('deathbringer regent'/'pLPA', 'Creature — Dragon').
card_original_text('deathbringer regent'/'pLPA', '').
card_first_print('deathbringer regent', 'pLPA').
card_image_name('deathbringer regent'/'pLPA', 'deathbringer regent').
card_uid('deathbringer regent'/'pLPA', 'pLPA:Deathbringer Regent:deathbringer regent').
card_rarity('deathbringer regent'/'pLPA', 'Special').
card_artist('deathbringer regent'/'pLPA', 'Chris Rallis').
card_number('deathbringer regent'/'pLPA', '31').

card_in_set('dictate of the twin gods', 'pLPA').
card_original_type('dictate of the twin gods'/'pLPA', 'Enchantment').
card_original_text('dictate of the twin gods'/'pLPA', '').
card_first_print('dictate of the twin gods', 'pLPA').
card_image_name('dictate of the twin gods'/'pLPA', 'dictate of the twin gods').
card_uid('dictate of the twin gods'/'pLPA', 'pLPA:Dictate of the Twin Gods:dictate of the twin gods').
card_rarity('dictate of the twin gods'/'pLPA', 'Special').
card_artist('dictate of the twin gods'/'pLPA', 'Chase Stone').
card_number('dictate of the twin gods'/'pLPA', '26').
card_flavor_text('dictate of the twin gods'/'pLPA', 'Iroas and Mogis are as different in appearance as they are in personality.').

card_in_set('dragon throne of tarkir', 'pLPA').
card_original_type('dragon throne of tarkir'/'pLPA', 'Legendary Artifact — Equipment').
card_original_text('dragon throne of tarkir'/'pLPA', '').
card_first_print('dragon throne of tarkir', 'pLPA').
card_image_name('dragon throne of tarkir'/'pLPA', 'dragon throne of tarkir').
card_uid('dragon throne of tarkir'/'pLPA', 'pLPA:Dragon Throne of Tarkir:dragon throne of tarkir').
card_rarity('dragon throne of tarkir'/'pLPA', 'Special').
card_artist('dragon throne of tarkir'/'pLPA', 'Daarken').
card_number('dragon throne of tarkir'/'pLPA', '27').
card_flavor_text('dragon throne of tarkir'/'pLPA', 'What once soared high above Tarkir is now reduced to a seat.').

card_in_set('earwig squad', 'pLPA').
card_original_type('earwig squad'/'pLPA', 'Creature — Goblin Rogue').
card_original_text('earwig squad'/'pLPA', '').
card_first_print('earwig squad', 'pLPA').
card_image_name('earwig squad'/'pLPA', 'earwig squad').
card_uid('earwig squad'/'pLPA', 'pLPA:Earwig Squad:earwig squad').
card_rarity('earwig squad'/'pLPA', 'Special').
card_artist('earwig squad'/'pLPA', 'Warren Mahy').
card_number('earwig squad'/'pLPA', '1').

card_in_set('entering', 'pLPA').
card_original_type('entering'/'pLPA', 'Sorcery').
card_original_text('entering'/'pLPA', '').
card_first_print('entering', 'pLPA').
card_image_name('entering'/'pLPA', 'breakingentering').
card_uid('entering'/'pLPA', 'pLPA:Entering:breakingentering').
card_rarity('entering'/'pLPA', 'Special').
card_artist('entering'/'pLPA', 'Igor Kieryluk').
card_number('entering'/'pLPA', '22b').

card_in_set('figure of destiny', 'pLPA').
card_original_type('figure of destiny'/'pLPA', 'Creature — Kithkin').
card_original_text('figure of destiny'/'pLPA', '').
card_first_print('figure of destiny', 'pLPA').
card_image_name('figure of destiny'/'pLPA', 'figure of destiny').
card_uid('figure of destiny'/'pLPA', 'pLPA:Figure of Destiny:figure of destiny').
card_rarity('figure of destiny'/'pLPA', 'Special').
card_artist('figure of destiny'/'pLPA', 'Scott M. Fischer').
card_number('figure of destiny'/'pLPA', '3').

card_in_set('garruk\'s horde', 'pLPA').
card_original_type('garruk\'s horde'/'pLPA', 'Creature — Beast').
card_original_text('garruk\'s horde'/'pLPA', '').
card_first_print('garruk\'s horde', 'pLPA').
card_image_name('garruk\'s horde'/'pLPA', 'garruk\'s horde').
card_uid('garruk\'s horde'/'pLPA', 'pLPA:Garruk\'s Horde:garruk\'s horde').
card_rarity('garruk\'s horde'/'pLPA', 'Special').
card_artist('garruk\'s horde'/'pLPA', 'Kev Walker').
card_number('garruk\'s horde'/'pLPA', '15').

card_in_set('in garruk\'s wake', 'pLPA').
card_original_type('in garruk\'s wake'/'pLPA', 'Sorcery').
card_original_text('in garruk\'s wake'/'pLPA', '').
card_first_print('in garruk\'s wake', 'pLPA').
card_image_name('in garruk\'s wake'/'pLPA', 'in garruk\'s wake').
card_uid('in garruk\'s wake'/'pLPA', 'pLPA:In Garruk\'s Wake:in garruk\'s wake').
card_rarity('in garruk\'s wake'/'pLPA', 'Special').
card_artist('in garruk\'s wake'/'pLPA', 'Brad Rigney').
card_number('in garruk\'s wake'/'pLPA', '28').
card_flavor_text('in garruk\'s wake'/'pLPA', 'Beyond pain, beyond obsession and wild despair, there lies a place of twisted power only the most tormented souls can reach.').

card_in_set('joraga warcaller', 'pLPA').
card_original_type('joraga warcaller'/'pLPA', 'Creature — Elf Warrior').
card_original_text('joraga warcaller'/'pLPA', '').
card_first_print('joraga warcaller', 'pLPA').
card_image_name('joraga warcaller'/'pLPA', 'joraga warcaller').
card_uid('joraga warcaller'/'pLPA', 'pLPA:Joraga Warcaller:joraga warcaller').
card_rarity('joraga warcaller'/'pLPA', 'Special').
card_artist('joraga warcaller'/'pLPA', 'Kev Walker').
card_number('joraga warcaller'/'pLPA', '9').

card_in_set('knight of new alara', 'pLPA').
card_original_type('knight of new alara'/'pLPA', 'Creature — Human Knight').
card_original_text('knight of new alara'/'pLPA', '').
card_first_print('knight of new alara', 'pLPA').
card_image_name('knight of new alara'/'pLPA', 'knight of new alara').
card_uid('knight of new alara'/'pLPA', 'pLPA:Knight of New Alara:knight of new alara').
card_rarity('knight of new alara'/'pLPA', 'Special').
card_artist('knight of new alara'/'pLPA', 'David Palumbo').
card_number('knight of new alara'/'pLPA', '6').
card_flavor_text('knight of new alara'/'pLPA', '\"I embrace all of Alara, Grixis as much as Bant. Let my former home call me traitor. I do this not for accolades, but because it is right.\"').

card_in_set('lord of shatterskull pass', 'pLPA').
card_original_type('lord of shatterskull pass'/'pLPA', 'Creature — Minotaur Shaman').
card_original_text('lord of shatterskull pass'/'pLPA', '').
card_first_print('lord of shatterskull pass', 'pLPA').
card_image_name('lord of shatterskull pass'/'pLPA', 'lord of shatterskull pass').
card_uid('lord of shatterskull pass'/'pLPA', 'pLPA:Lord of Shatterskull Pass:lord of shatterskull pass').
card_rarity('lord of shatterskull pass'/'pLPA', 'Special').
card_artist('lord of shatterskull pass'/'pLPA', 'Igor Kieryluk').
card_number('lord of shatterskull pass'/'pLPA', '10').

card_in_set('ludevic\'s abomination', 'pLPA').
card_original_type('ludevic\'s abomination'/'pLPA', 'Creature — Lizard Horror').
card_original_text('ludevic\'s abomination'/'pLPA', '').
card_first_print('ludevic\'s abomination', 'pLPA').
card_image_name('ludevic\'s abomination'/'pLPA', 'ludevic\'s abomination').
card_uid('ludevic\'s abomination'/'pLPA', 'pLPA:Ludevic\'s Abomination:ludevic\'s abomination').
card_rarity('ludevic\'s abomination'/'pLPA', 'Special').
card_artist('ludevic\'s abomination'/'pLPA', 'Karl Kopinski').
card_number('ludevic\'s abomination'/'pLPA', '16b').
card_flavor_text('ludevic\'s abomination'/'pLPA', 'After several frustrating experiments, the visionary Ludevic realized he needed to create a monster that fed on torch-wielding mobs.').

card_in_set('ludevic\'s test subject', 'pLPA').
card_original_type('ludevic\'s test subject'/'pLPA', 'Creature — Lizard').
card_original_text('ludevic\'s test subject'/'pLPA', '').
card_first_print('ludevic\'s test subject', 'pLPA').
card_image_name('ludevic\'s test subject'/'pLPA', 'ludevic\'s test subject').
card_uid('ludevic\'s test subject'/'pLPA', 'pLPA:Ludevic\'s Test Subject:ludevic\'s test subject').
card_rarity('ludevic\'s test subject'/'pLPA', 'Special').
card_artist('ludevic\'s test subject'/'pLPA', 'Karl Kopinski').
card_number('ludevic\'s test subject'/'pLPA', '16a').

card_in_set('mondronen shaman', 'pLPA').
card_original_type('mondronen shaman'/'pLPA', 'Creature — Human Werewolf Shaman').
card_original_text('mondronen shaman'/'pLPA', '').
card_first_print('mondronen shaman', 'pLPA').
card_image_name('mondronen shaman'/'pLPA', 'mondronen shaman').
card_uid('mondronen shaman'/'pLPA', 'pLPA:Mondronen Shaman:mondronen shaman').
card_rarity('mondronen shaman'/'pLPA', 'Special').
card_artist('mondronen shaman'/'pLPA', 'Anthony Palumbo').
card_number('mondronen shaman'/'pLPA', '17a').
card_flavor_text('mondronen shaman'/'pLPA', '\"Tovolar, my master. Gather the howlpack, for the Hunter\'s Moon is nigh and Thraben\'s walls grow weak.\"').

card_in_set('obelisk of alara', 'pLPA').
card_original_type('obelisk of alara'/'pLPA', 'Artifact').
card_original_text('obelisk of alara'/'pLPA', '').
card_first_print('obelisk of alara', 'pLPA').
card_image_name('obelisk of alara'/'pLPA', 'obelisk of alara').
card_uid('obelisk of alara'/'pLPA', 'pLPA:Obelisk of Alara:obelisk of alara').
card_rarity('obelisk of alara'/'pLPA', 'Special').
card_artist('obelisk of alara'/'pLPA', 'John Avon').
card_number('obelisk of alara'/'pLPA', '5').

card_in_set('phyrexian metamorph', 'pLPA').
card_original_type('phyrexian metamorph'/'pLPA', 'Artifact Creature — Shapeshifter').
card_original_text('phyrexian metamorph'/'pLPA', '').
card_first_print('phyrexian metamorph', 'pLPA').
card_image_name('phyrexian metamorph'/'pLPA', 'phyrexian metamorph').
card_uid('phyrexian metamorph'/'pLPA', 'pLPA:Phyrexian Metamorph:phyrexian metamorph').
card_rarity('phyrexian metamorph'/'pLPA', 'Special').
card_artist('phyrexian metamorph'/'pLPA', 'Jung Park').
card_number('phyrexian metamorph'/'pLPA', '14').

card_in_set('restoration angel', 'pLPA').
card_original_type('restoration angel'/'pLPA', 'Creature — Angel').
card_original_text('restoration angel'/'pLPA', '').
card_first_print('restoration angel', 'pLPA').
card_image_name('restoration angel'/'pLPA', 'restoration angel').
card_uid('restoration angel'/'pLPA', 'pLPA:Restoration Angel:restoration angel').
card_rarity('restoration angel'/'pLPA', 'Special').
card_artist('restoration angel'/'pLPA', 'Wesley Burt').
card_number('restoration angel'/'pLPA', '18').

card_in_set('sandsteppe mastodon', 'pLPA').
card_original_type('sandsteppe mastodon'/'pLPA', 'Creature — Elephant').
card_original_text('sandsteppe mastodon'/'pLPA', '').
card_first_print('sandsteppe mastodon', 'pLPA').
card_image_name('sandsteppe mastodon'/'pLPA', 'sandsteppe mastodon').
card_uid('sandsteppe mastodon'/'pLPA', 'pLPA:Sandsteppe Mastodon:sandsteppe mastodon').
card_rarity('sandsteppe mastodon'/'pLPA', 'Special').
card_artist('sandsteppe mastodon'/'pLPA', 'Kev Walker').
card_number('sandsteppe mastodon'/'pLPA', '29').
card_flavor_text('sandsteppe mastodon'/'pLPA', 'A mastodon\'s path always leads to an oasis.').

card_in_set('skarrg goliath', 'pLPA').
card_original_type('skarrg goliath'/'pLPA', 'Creature — Beast').
card_original_text('skarrg goliath'/'pLPA', '').
card_first_print('skarrg goliath', 'pLPA').
card_image_name('skarrg goliath'/'pLPA', 'skarrg goliath').
card_uid('skarrg goliath'/'pLPA', 'pLPA:Skarrg Goliath:skarrg goliath').
card_rarity('skarrg goliath'/'pLPA', 'Special').
card_artist('skarrg goliath'/'pLPA', 'Karl Kopinski').
card_number('skarrg goliath'/'pLPA', '21').
card_flavor_text('skarrg goliath'/'pLPA', '\"They bind us with their laws. We free ourselves with nature\'s fist.\"\n—Nikya of the Old Ways').

card_in_set('staff of nin', 'pLPA').
card_original_type('staff of nin'/'pLPA', 'Artifact').
card_original_text('staff of nin'/'pLPA', '').
card_first_print('staff of nin', 'pLPA').
card_image_name('staff of nin'/'pLPA', 'staff of nin').
card_uid('staff of nin'/'pLPA', 'pLPA:Staff of Nin:staff of nin').
card_rarity('staff of nin'/'pLPA', 'Special').
card_artist('staff of nin'/'pLPA', 'Brad Rigney').
card_number('staff of nin'/'pLPA', '19').
card_flavor_text('staff of nin'/'pLPA', '\"I have attuned the staff to your screams so that only I may benefit from your pain.\"\n—Volux, disciple of Nin').

card_in_set('steel hellkite', 'pLPA').
card_original_type('steel hellkite'/'pLPA', 'Artifact Creature — Dragon').
card_original_text('steel hellkite'/'pLPA', '').
card_first_print('steel hellkite', 'pLPA').
card_image_name('steel hellkite'/'pLPA', 'steel hellkite').
card_uid('steel hellkite'/'pLPA', 'pLPA:Steel Hellkite:steel hellkite').
card_rarity('steel hellkite'/'pLPA', 'Special').
card_artist('steel hellkite'/'pLPA', 'Jaime Jones').
card_number('steel hellkite'/'pLPA', '12').

card_in_set('thopter assembly', 'pLPA').
card_original_type('thopter assembly'/'pLPA', 'Artifact Creature — Thopter').
card_original_text('thopter assembly'/'pLPA', '').
card_first_print('thopter assembly', 'pLPA').
card_image_name('thopter assembly'/'pLPA', 'thopter assembly').
card_uid('thopter assembly'/'pLPA', 'pLPA:Thopter Assembly:thopter assembly').
card_rarity('thopter assembly'/'pLPA', 'Special').
card_artist('thopter assembly'/'pLPA', 'Volkan Baga').
card_number('thopter assembly'/'pLPA', '13').

card_in_set('tovolar\'s magehunter', 'pLPA').
card_original_type('tovolar\'s magehunter'/'pLPA', 'Creature — Werewolf').
card_original_text('tovolar\'s magehunter'/'pLPA', '').
card_first_print('tovolar\'s magehunter', 'pLPA').
card_image_name('tovolar\'s magehunter'/'pLPA', 'tovolar\'s magehunter').
card_uid('tovolar\'s magehunter'/'pLPA', 'pLPA:Tovolar\'s Magehunter:tovolar\'s magehunter').
card_rarity('tovolar\'s magehunter'/'pLPA', 'Special').
card_artist('tovolar\'s magehunter'/'pLPA', 'Anthony Palumbo').
card_number('tovolar\'s magehunter'/'pLPA', '17b').

card_in_set('tromokratis', 'pLPA').
card_original_type('tromokratis'/'pLPA', 'Legendary Creature — Kraken').
card_original_text('tromokratis'/'pLPA', '').
card_first_print('tromokratis', 'pLPA').
card_image_name('tromokratis'/'pLPA', 'tromokratis').
card_uid('tromokratis'/'pLPA', 'pLPA:Tromokratis:tromokratis').
card_rarity('tromokratis'/'pLPA', 'Special').
card_artist('tromokratis'/'pLPA', 'Jaime Jones').
card_number('tromokratis'/'pLPA', '25').

card_in_set('valakut, the molten pinnacle', 'pLPA').
card_original_type('valakut, the molten pinnacle'/'pLPA', 'Land').
card_original_text('valakut, the molten pinnacle'/'pLPA', '').
card_first_print('valakut, the molten pinnacle', 'pLPA').
card_image_name('valakut, the molten pinnacle'/'pLPA', 'valakut, the molten pinnacle').
card_uid('valakut, the molten pinnacle'/'pLPA', 'pLPA:Valakut, the Molten Pinnacle:valakut, the molten pinnacle').
card_rarity('valakut, the molten pinnacle'/'pLPA', 'Special').
card_artist('valakut, the molten pinnacle'/'pLPA', 'James Paick').
card_number('valakut, the molten pinnacle'/'pLPA', '8').

card_in_set('vexing shusher', 'pLPA').
card_original_type('vexing shusher'/'pLPA', 'Creature — Goblin Shaman').
card_original_text('vexing shusher'/'pLPA', '').
card_first_print('vexing shusher', 'pLPA').
card_image_name('vexing shusher'/'pLPA', 'vexing shusher').
card_uid('vexing shusher'/'pLPA', 'pLPA:Vexing Shusher:vexing shusher').
card_rarity('vexing shusher'/'pLPA', 'Special').
card_artist('vexing shusher'/'pLPA', 'Cyril Van Der Haegen').
card_number('vexing shusher'/'pLPA', '2').
card_flavor_text('vexing shusher'/'pLPA', 'The stench of bloodcap mushrooms on the breath is enough to ward off even the most potent magics, especially when combined with a special chant: \"Hushhh.\"').
