% Masters Edition

set('MED').
set_name('MED', 'Masters Edition').
set_release_date('MED', '2007-09-10').
set_border('MED', 'black').
set_type('MED', 'masters').

card_in_set('adun oakenshield', 'MED').
card_original_type('adun oakenshield'/'MED', 'Legendary Creature — Human Knight').
card_original_text('adun oakenshield'/'MED', '{B}{R}{G}, {T}: Return target creature card from your graveyard to your hand.').
card_image_name('adun oakenshield'/'MED', 'adun oakenshield').
card_uid('adun oakenshield'/'MED', 'MED:Adun Oakenshield:adun oakenshield').
card_rarity('adun oakenshield'/'MED', 'Rare').
card_artist('adun oakenshield'/'MED', 'Jeff A. Menges').
card_number('adun oakenshield'/'MED', '141').
card_flavor_text('adun oakenshield'/'MED', '\". . . And at his passing, the bodies of the world\'s great warriors shall rise from their graves and follow him to battle.\"\n—The Anvilonian Grimoire').
card_multiverse_id('adun oakenshield'/'MED', '159294').

card_in_set('amnesia', 'MED').
card_original_type('amnesia'/'MED', 'Sorcery').
card_original_text('amnesia'/'MED', 'Target player reveals his or her hand and discards all nonland cards.').
card_image_name('amnesia'/'MED', 'amnesia').
card_uid('amnesia'/'MED', 'MED:Amnesia:amnesia').
card_rarity('amnesia'/'MED', 'Rare').
card_artist('amnesia'/'MED', 'Mark Poole').
card_number('amnesia'/'MED', '29').
card_flavor_text('amnesia'/'MED', '\"When one has witnessed the unspeakable, \'tis sometimes better to forget.\"\n—Vervamon the Elder').
card_multiverse_id('amnesia'/'MED', '159828').

card_in_set('angry mob', 'MED').
card_original_type('angry mob'/'MED', 'Creature — Human').
card_original_text('angry mob'/'MED', 'Trample\nAs long as it\'s your turn, Angry Mob\'s power and toughness are each equal to 2 plus the number of Swamps your opponents control. As long as it\'s not your turn, Angry Mob\'s power and toughness are each 2.').
card_image_name('angry mob'/'MED', 'angry mob').
card_uid('angry mob'/'MED', 'MED:Angry Mob:angry mob').
card_rarity('angry mob'/'MED', 'Uncommon').
card_artist('angry mob'/'MED', 'Drew Tucker').
card_number('angry mob'/'MED', '1').
card_multiverse_id('angry mob'/'MED', '159321').

card_in_set('animate dead', 'MED').
card_original_type('animate dead'/'MED', 'Enchantment — Aura').
card_original_text('animate dead'/'MED', 'Enchant creature card in a graveyard\nWhen Animate Dead comes into play, if it\'s in play, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put into play with Animate Dead.\" Return enchanted creature card to play under your control and attach Animate Dead to it. When Animate Dead leaves play, that creature\'s controller sacrifices it.\nEnchanted creature gets -1/-0.').
card_image_name('animate dead'/'MED', 'animate dead').
card_uid('animate dead'/'MED', 'MED:Animate Dead:animate dead').
card_rarity('animate dead'/'MED', 'Uncommon').
card_artist('animate dead'/'MED', 'Anson Maddocks').
card_number('animate dead'/'MED', '57').
card_multiverse_id('animate dead'/'MED', '159249').

card_in_set('animate wall', 'MED').
card_original_type('animate wall'/'MED', 'Enchantment — Aura').
card_original_text('animate wall'/'MED', 'Enchant Wall\nEnchanted Wall can attack as though it didn\'t have defender.').
card_image_name('animate wall'/'MED', 'animate wall').
card_uid('animate wall'/'MED', 'MED:Animate Wall:animate wall').
card_rarity('animate wall'/'MED', 'Uncommon').
card_artist('animate wall'/'MED', 'Dan Frazier').
card_number('animate wall'/'MED', '2').
card_multiverse_id('animate wall'/'MED', '159250').

card_in_set('ankh of mishra', 'MED').
card_original_type('ankh of mishra'/'MED', 'Artifact').
card_original_text('ankh of mishra'/'MED', 'Whenever a land comes into play, Ankh of Mishra deals 2 damage to that land\'s controller.').
card_image_name('ankh of mishra'/'MED', 'ankh of mishra').
card_uid('ankh of mishra'/'MED', 'MED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'MED', 'Rare').
card_artist('ankh of mishra'/'MED', 'Amy Weber').
card_number('ankh of mishra'/'MED', '151').
card_multiverse_id('ankh of mishra'/'MED', '159251').

card_in_set('apprentice wizard', 'MED').
card_original_type('apprentice wizard'/'MED', 'Creature — Human Wizard').
card_original_text('apprentice wizard'/'MED', '{U}, {T}: Add {3} to your mana pool.').
card_image_name('apprentice wizard'/'MED', 'apprentice wizard').
card_uid('apprentice wizard'/'MED', 'MED:Apprentice Wizard:apprentice wizard').
card_rarity('apprentice wizard'/'MED', 'Common').
card_artist('apprentice wizard'/'MED', 'Dan Frazier').
card_number('apprentice wizard'/'MED', '30').
card_multiverse_id('apprentice wizard'/'MED', '159087').

card_in_set('arcane denial', 'MED').
card_original_type('arcane denial'/'MED', 'Instant').
card_original_text('arcane denial'/'MED', 'Counter target spell. Its controller may draw up to two cards at the beginning of the next turn\'s upkeep.\nYou draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('arcane denial'/'MED', 'arcane denial').
card_uid('arcane denial'/'MED', 'MED:Arcane Denial:arcane denial').
card_rarity('arcane denial'/'MED', 'Common').
card_artist('arcane denial'/'MED', 'Richard Kane Ferguson').
card_number('arcane denial'/'MED', '31').
card_multiverse_id('arcane denial'/'MED', '159084').

card_in_set('argivian archaeologist', 'MED').
card_original_type('argivian archaeologist'/'MED', 'Creature — Human Artificer').
card_original_text('argivian archaeologist'/'MED', '{W}{W}, {T}: Return target artifact card from your graveyard to your hand.').
card_image_name('argivian archaeologist'/'MED', 'argivian archaeologist').
card_uid('argivian archaeologist'/'MED', 'MED:Argivian Archaeologist:argivian archaeologist').
card_rarity('argivian archaeologist'/'MED', 'Rare').
card_artist('argivian archaeologist'/'MED', 'Amy Weber').
card_number('argivian archaeologist'/'MED', '3').
card_flavor_text('argivian archaeologist'/'MED', 'Fascinated by the lore of ancient struggles, the archaeologist searches incessantly for remnants of an earlier, more powerful era.').
card_multiverse_id('argivian archaeologist'/'MED', '159743').

card_in_set('armageddon', 'MED').
card_original_type('armageddon'/'MED', 'Sorcery').
card_original_text('armageddon'/'MED', 'Destroy all lands.').
card_image_name('armageddon'/'MED', 'armageddon').
card_uid('armageddon'/'MED', 'MED:Armageddon:armageddon').
card_rarity('armageddon'/'MED', 'Rare').
card_artist('armageddon'/'MED', 'Jesper Myrfors').
card_number('armageddon'/'MED', '4').
card_multiverse_id('armageddon'/'MED', '159252').

card_in_set('artifact blast', 'MED').
card_original_type('artifact blast'/'MED', 'Instant').
card_original_text('artifact blast'/'MED', 'Counter target artifact spell.').
card_image_name('artifact blast'/'MED', 'artifact blast').
card_uid('artifact blast'/'MED', 'MED:Artifact Blast:artifact blast').
card_rarity('artifact blast'/'MED', 'Common').
card_artist('artifact blast'/'MED', 'Mark Poole').
card_number('artifact blast'/'MED', '85').
card_flavor_text('artifact blast'/'MED', 'The first line of defense against Urza and Mishra, the artifact blast achieved widespread fame until an unlucky mage discovered it was useless on the devices the brothers had already created.').
card_multiverse_id('artifact blast'/'MED', '159744').

card_in_set('ashnod\'s transmogrant', 'MED').
card_original_type('ashnod\'s transmogrant'/'MED', 'Artifact').
card_original_text('ashnod\'s transmogrant'/'MED', '{T}, Sacrifice Ashnod\'s Transmogrant: Put a +1/+1 counter on target nonartifact creature. That creature becomes an artifact in addition to its other types.').
card_image_name('ashnod\'s transmogrant'/'MED', 'ashnod\'s transmogrant').
card_uid('ashnod\'s transmogrant'/'MED', 'MED:Ashnod\'s Transmogrant:ashnod\'s transmogrant').
card_rarity('ashnod\'s transmogrant'/'MED', 'Common').
card_artist('ashnod\'s transmogrant'/'MED', 'Mark Tedin').
card_number('ashnod\'s transmogrant'/'MED', '152').
card_flavor_text('ashnod\'s transmogrant'/'MED', 'Ashnod found few willing to trade their humanity for the power she offered them.').
card_multiverse_id('ashnod\'s transmogrant'/'MED', '159110').

card_in_set('autumn willow', 'MED').
card_original_type('autumn willow'/'MED', 'Legendary Creature — Avatar').
card_original_text('autumn willow'/'MED', 'Shroud\n{G}: Until end of turn, Autumn Willow can be the target of spells and abilities controlled by target player as though it didn\'t have shroud.').
card_image_name('autumn willow'/'MED', 'autumn willow').
card_uid('autumn willow'/'MED', 'MED:Autumn Willow:autumn willow').
card_rarity('autumn willow'/'MED', 'Rare').
card_artist('autumn willow'/'MED', 'Margaret Organ-Kean').
card_number('autumn willow'/'MED', '113').
card_flavor_text('autumn willow'/'MED', '\"We must shake her limbs and rattle her brains.\"\n—Grandmother Sengir').
card_multiverse_id('autumn willow'/'MED', '159205').

card_in_set('balduvian horde', 'MED').
card_original_type('balduvian horde'/'MED', 'Creature — Human Barbarian').
card_original_text('balduvian horde'/'MED', 'When Balduvian Horde comes into play, sacrifice it unless you discard a card at random.').
card_image_name('balduvian horde'/'MED', 'balduvian horde').
card_uid('balduvian horde'/'MED', 'MED:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'MED', 'Rare').
card_artist('balduvian horde'/'MED', 'Brian Snõddy').
card_number('balduvian horde'/'MED', '86').
card_flavor_text('balduvian horde'/'MED', '\"Peace will come only when we have taken Varchild\'s head.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('balduvian horde'/'MED', '159086').

card_in_set('ball lightning', 'MED').
card_original_type('ball lightning'/'MED', 'Creature — Elemental').
card_original_text('ball lightning'/'MED', 'Trample, haste\nAt end of turn, sacrifice Ball Lightning.').
card_image_name('ball lightning'/'MED', 'ball lightning').
card_uid('ball lightning'/'MED', 'MED:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'MED', 'Rare').
card_artist('ball lightning'/'MED', 'Quinton Hoover').
card_number('ball lightning'/'MED', '87').
card_multiverse_id('ball lightning'/'MED', '159139').

card_in_set('baron sengir', 'MED').
card_original_type('baron sengir'/'MED', 'Legendary Creature — Vampire').
card_original_text('baron sengir'/'MED', 'Flying\nWhenever a creature dealt damage by Baron Sengir this turn is put into a graveyard, put a +2/+2 counter on Baron Sengir.\n{T}: Regenerate another target Vampire.').
card_image_name('baron sengir'/'MED', 'baron sengir').
card_uid('baron sengir'/'MED', 'MED:Baron Sengir:baron sengir').
card_rarity('baron sengir'/'MED', 'Rare').
card_artist('baron sengir'/'MED', 'Pete Venters').
card_number('baron sengir'/'MED', '58').
card_flavor_text('baron sengir'/'MED', '\"Beast. Defiler. The source of all my pain.\"\n—Ihsan\'s Shade').
card_multiverse_id('baron sengir'/'MED', '159208').

card_in_set('basal thrull', 'MED').
card_original_type('basal thrull'/'MED', 'Creature — Thrull').
card_original_text('basal thrull'/'MED', '{T}, Sacrifice Basal Thrull: Add {B}{B} to your mana pool.').
card_image_name('basal thrull'/'MED', 'basal thrull').
card_uid('basal thrull'/'MED', 'MED:Basal Thrull:basal thrull').
card_rarity('basal thrull'/'MED', 'Common').
card_artist('basal thrull'/'MED', 'Kaja Foglio').
card_number('basal thrull'/'MED', '59').
card_flavor_text('basal thrull'/'MED', 'Initially bred for sacrifice, the thrulls eventually turned on their masters, the Order of the Ebon Hand, with gruesome results.\n—Sarpadian Empires, vol. II').
card_multiverse_id('basal thrull'/'MED', '159728').

card_in_set('benalish hero', 'MED').
card_original_type('benalish hero'/'MED', 'Creature — Human Soldier').
card_original_text('benalish hero'/'MED', 'Banding').
card_image_name('benalish hero'/'MED', 'benalish hero').
card_uid('benalish hero'/'MED', 'MED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'MED', 'Common').
card_artist('benalish hero'/'MED', 'Douglas Shuler').
card_number('benalish hero'/'MED', '5').
card_flavor_text('benalish hero'/'MED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').
card_multiverse_id('benalish hero'/'MED', '159253').

card_in_set('berserk', 'MED').
card_original_type('berserk'/'MED', 'Instant').
card_original_text('berserk'/'MED', 'Play Berserk only before the combat damage step.\nTarget creature gains trample and gets +X/+0 until end of turn, where X is its power. At end of turn, destroy that creature if it attacked this turn.').
card_image_name('berserk'/'MED', 'berserk').
card_uid('berserk'/'MED', 'MED:Berserk:berserk').
card_rarity('berserk'/'MED', 'Rare').
card_artist('berserk'/'MED', 'Dan Frazier').
card_number('berserk'/'MED', '114').
card_multiverse_id('berserk'/'MED', '159254').

card_in_set('bestial fury', 'MED').
card_original_type('bestial fury'/'MED', 'Enchantment — Aura').
card_original_text('bestial fury'/'MED', 'Enchant creature\nWhen Bestial Fury comes into play, draw a card at the beginning of the next turn\'s upkeep.\nWhenever enchanted creature becomes blocked, it gets +4/+0 and gains trample until end of turn.').
card_image_name('bestial fury'/'MED', 'bestial fury').
card_uid('bestial fury'/'MED', 'MED:Bestial Fury:bestial fury').
card_rarity('bestial fury'/'MED', 'Common').
card_artist('bestial fury'/'MED', 'Mike Raabe').
card_number('bestial fury'/'MED', '88').
card_multiverse_id('bestial fury'/'MED', '159736').

card_in_set('black knight', 'MED').
card_original_type('black knight'/'MED', 'Creature — Human Knight').
card_original_text('black knight'/'MED', 'First strike, protection from white').
card_image_name('black knight'/'MED', 'black knight').
card_uid('black knight'/'MED', 'MED:Black Knight:black knight').
card_rarity('black knight'/'MED', 'Uncommon').
card_artist('black knight'/'MED', 'Jeff A. Menges').
card_number('black knight'/'MED', '60').
card_flavor_text('black knight'/'MED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'MED', '159817').

card_in_set('blight', 'MED').
card_original_type('blight'/'MED', 'Enchantment — Aura').
card_original_text('blight'/'MED', 'Enchant land\nWhen enchanted land becomes tapped, destroy it.').
card_image_name('blight'/'MED', 'blight').
card_uid('blight'/'MED', 'MED:Blight:blight').
card_rarity('blight'/'MED', 'Uncommon').
card_artist('blight'/'MED', 'Pete Venters').
card_number('blight'/'MED', '61').
card_multiverse_id('blight'/'MED', '159769').

card_in_set('breeding pit', 'MED').
card_original_type('breeding pit'/'MED', 'Enchantment').
card_original_text('breeding pit'/'MED', 'At the beginning of your upkeep, sacrifice Breeding Pit unless you pay {B}{B}. \nAt the end of your turn, put a 0/1 black Thrull creature token into play.').
card_image_name('breeding pit'/'MED', 'breeding pit').
card_uid('breeding pit'/'MED', 'MED:Breeding Pit:breeding pit').
card_rarity('breeding pit'/'MED', 'Uncommon').
card_artist('breeding pit'/'MED', 'Anson Maddocks').
card_number('breeding pit'/'MED', '62').
card_flavor_text('breeding pit'/'MED', 'The thrulls bred at a terrifying pace. In the end, they overwhelmed the Order of the Ebon Hand.').
card_multiverse_id('breeding pit'/'MED', '159166').

card_in_set('brothers of fire', 'MED').
card_original_type('brothers of fire'/'MED', 'Creature — Human Shaman').
card_original_text('brothers of fire'/'MED', '{1}{R}{R}: Brothers of Fire deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('brothers of fire'/'MED', 'brothers of fire').
card_uid('brothers of fire'/'MED', 'MED:Brothers of Fire:brothers of fire').
card_rarity('brothers of fire'/'MED', 'Common').
card_artist('brothers of fire'/'MED', 'Mark Tedin').
card_number('brothers of fire'/'MED', '89').
card_flavor_text('brothers of fire'/'MED', 'Fire is never a gentle master.').
card_multiverse_id('brothers of fire'/'MED', '159141').

card_in_set('carnivorous plant', 'MED').
card_original_type('carnivorous plant'/'MED', 'Creature — Plant Wall').
card_original_text('carnivorous plant'/'MED', 'Defender').
card_image_name('carnivorous plant'/'MED', 'carnivorous plant').
card_uid('carnivorous plant'/'MED', 'MED:Carnivorous Plant:carnivorous plant').
card_rarity('carnivorous plant'/'MED', 'Uncommon').
card_artist('carnivorous plant'/'MED', 'Quinton Hoover').
card_number('carnivorous plant'/'MED', '115').
card_flavor_text('carnivorous plant'/'MED', '\"It had a mouth like that of a great beast, and gnashed its teeth as it strained to reach us. I am thankful it possessed no means of locomotion.\"\n—Vervamon the Elder').
card_multiverse_id('carnivorous plant'/'MED', '159142').

card_in_set('centaur archer', 'MED').
card_original_type('centaur archer'/'MED', 'Creature — Centaur Archer').
card_original_text('centaur archer'/'MED', '{T}: Centaur Archer deals 1 damage to target creature with flying.').
card_image_name('centaur archer'/'MED', 'centaur archer').
card_uid('centaur archer'/'MED', 'MED:Centaur Archer:centaur archer').
card_rarity('centaur archer'/'MED', 'Uncommon').
card_artist('centaur archer'/'MED', 'Melissa A. Benson').
card_number('centaur archer'/'MED', '142').
card_flavor_text('centaur archer'/'MED', '\"Centaurs will kill our aesthir if they can; they\'ve always been enemies. Destroy the horse-people on sight.\"\n—Arna Kennerüd, Skyknight').
card_multiverse_id('centaur archer'/'MED', '159834').

card_in_set('chains of mephistopheles', 'MED').
card_original_type('chains of mephistopheles'/'MED', 'Enchantment').
card_original_text('chains of mephistopheles'/'MED', 'If a player would draw a card except the first one he or she draws in his or her draw step each turn, that player discards a card instead. If the player discards a card this way, he or she draws a card. If the player doesn\'t discard a card this way, he or she puts the top card of his or her library into his or her graveyard.').
card_image_name('chains of mephistopheles'/'MED', 'chains of mephistopheles').
card_uid('chains of mephistopheles'/'MED', 'MED:Chains of Mephistopheles:chains of mephistopheles').
card_rarity('chains of mephistopheles'/'MED', 'Rare').
card_artist('chains of mephistopheles'/'MED', 'Heather Hudson').
card_number('chains of mephistopheles'/'MED', '63').
card_multiverse_id('chains of mephistopheles'/'MED', '159823').

card_in_set('chub toad', 'MED').
card_original_type('chub toad'/'MED', 'Creature — Frog').
card_original_text('chub toad'/'MED', 'Whenever Chub Toad blocks or becomes blocked, it gets +2/+2 until end of turn.').
card_image_name('chub toad'/'MED', 'chub toad').
card_uid('chub toad'/'MED', 'MED:Chub Toad:chub toad').
card_rarity('chub toad'/'MED', 'Common').
card_artist('chub toad'/'MED', 'Daniel Gelon').
card_number('chub toad'/'MED', '116').
card_flavor_text('chub toad'/'MED', '\"Chub toad, chub toad\nAt the door.\nRun away quick\nOr you\'ll run no more.\"\n—Traditional children\'s rhyme').
card_multiverse_id('chub toad'/'MED', '159226').

card_in_set('clockwork beast', 'MED').
card_original_type('clockwork beast'/'MED', 'Artifact Creature — Beast').
card_original_text('clockwork beast'/'MED', 'Clockwork Beast comes into play with seven +1/+0 counters on it.\nAt end of combat, if Clockwork Beast attacked or blocked this turn, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Beast. This ability can\'t cause the total number of +1/+0 counters on Clockwork Beast to be greater than seven. Play this ability only during your upkeep.').
card_image_name('clockwork beast'/'MED', 'clockwork beast').
card_uid('clockwork beast'/'MED', 'MED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'MED', 'Uncommon').
card_artist('clockwork beast'/'MED', 'Drew Tucker').
card_number('clockwork beast'/'MED', '153').
card_multiverse_id('clockwork beast'/'MED', '159256').

card_in_set('contagion', 'MED').
card_original_type('contagion'/'MED', 'Instant').
card_original_text('contagion'/'MED', 'You may pay 1 life and remove a black card in your hand from the game rather than pay Contagion\'s mana cost.\nDistribute two -2/-1 counters among one or two target creatures.').
card_image_name('contagion'/'MED', 'contagion').
card_uid('contagion'/'MED', 'MED:Contagion:contagion').
card_rarity('contagion'/'MED', 'Rare').
card_artist('contagion'/'MED', 'Mike Raabe').
card_number('contagion'/'MED', '64').
card_multiverse_id('contagion'/'MED', '159089').

card_in_set('copper tablet', 'MED').
card_original_type('copper tablet'/'MED', 'Artifact').
card_original_text('copper tablet'/'MED', 'At the beginning of each player\'s upkeep, Copper Tablet deals 1 damage to that player.').
card_image_name('copper tablet'/'MED', 'copper tablet').
card_uid('copper tablet'/'MED', 'MED:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'MED', 'Uncommon').
card_artist('copper tablet'/'MED', 'Amy Weber').
card_number('copper tablet'/'MED', '154').
card_multiverse_id('copper tablet'/'MED', '159257').

card_in_set('crookshank kobolds', 'MED').
card_original_type('crookshank kobolds'/'MED', 'Creature — Kobold').
card_original_text('crookshank kobolds'/'MED', 'Crookshank Kobolds is red.').
card_image_name('crookshank kobolds'/'MED', 'crookshank kobolds').
card_uid('crookshank kobolds'/'MED', 'MED:Crookshank Kobolds:crookshank kobolds').
card_rarity('crookshank kobolds'/'MED', 'Common').
card_artist('crookshank kobolds'/'MED', 'Christopher Rush').
card_number('crookshank kobolds'/'MED', '90').
card_flavor_text('crookshank kobolds'/'MED', 'The Crookshank military boasts a standing army of nearly twenty-four million, give or take twenty-two million.').
card_multiverse_id('crookshank kobolds'/'MED', '159770').

card_in_set('crusade', 'MED').
card_original_type('crusade'/'MED', 'Enchantment').
card_original_text('crusade'/'MED', 'White creatures get +1/+1.').
card_image_name('crusade'/'MED', 'crusade').
card_uid('crusade'/'MED', 'MED:Crusade:crusade').
card_rarity('crusade'/'MED', 'Rare').
card_artist('crusade'/'MED', 'Mark Poole').
card_number('crusade'/'MED', '6').
card_multiverse_id('crusade'/'MED', '159818').

card_in_set('cuombajj witches', 'MED').
card_original_type('cuombajj witches'/'MED', 'Creature — Human Wizard').
card_original_text('cuombajj witches'/'MED', '{T}: Cuombajj Witches deals 1 damage to target creature or player and 1 damage to target creature or player of an opponent\'s choice.').
card_image_name('cuombajj witches'/'MED', 'cuombajj witches').
card_uid('cuombajj witches'/'MED', 'MED:Cuombajj Witches:cuombajj witches').
card_rarity('cuombajj witches'/'MED', 'Common').
card_artist('cuombajj witches'/'MED', 'Kaja Foglio').
card_number('cuombajj witches'/'MED', '65').
card_multiverse_id('cuombajj witches'/'MED', '159123').

card_in_set('cursed rack', 'MED').
card_original_type('cursed rack'/'MED', 'Artifact').
card_original_text('cursed rack'/'MED', 'As Cursed Rack comes into play, choose an opponent.\nThe chosen player\'s maximum hand size is four.').
card_image_name('cursed rack'/'MED', 'cursed rack').
card_uid('cursed rack'/'MED', 'MED:Cursed Rack:cursed rack').
card_rarity('cursed rack'/'MED', 'Uncommon').
card_artist('cursed rack'/'MED', 'Richard Thomas').
card_number('cursed rack'/'MED', '155').
card_flavor_text('cursed rack'/'MED', 'Ashnod invented several torture techniques that could make victims even miles away beg for mercy as if the End had come.').
card_multiverse_id('cursed rack'/'MED', '159840').

card_in_set('dakkon blackblade', 'MED').
card_original_type('dakkon blackblade'/'MED', 'Legendary Creature — Human Warrior').
card_original_text('dakkon blackblade'/'MED', 'Dakkon Blackblade\'s power and toughness are each equal to the number of lands you control.').
card_image_name('dakkon blackblade'/'MED', 'dakkon blackblade').
card_uid('dakkon blackblade'/'MED', 'MED:Dakkon Blackblade:dakkon blackblade').
card_rarity('dakkon blackblade'/'MED', 'Rare').
card_artist('dakkon blackblade'/'MED', 'Richard Kane Ferguson').
card_number('dakkon blackblade'/'MED', '143').
card_flavor_text('dakkon blackblade'/'MED', '\"My power is as vast as the plains, my strength is that of mountains. Each wave that crashes upon the shore thunders like blood in my veins.\"\n—Dakkon Blackblade, Memoirs').
card_multiverse_id('dakkon blackblade'/'MED', '159298').

card_in_set('death speakers', 'MED').
card_original_type('death speakers'/'MED', 'Creature — Human Cleric').
card_original_text('death speakers'/'MED', 'Protection from black').
card_image_name('death speakers'/'MED', 'death speakers').
card_uid('death speakers'/'MED', 'MED:Death Speakers:death speakers').
card_rarity('death speakers'/'MED', 'Common').
card_artist('death speakers'/'MED', 'Douglas Shuler').
card_number('death speakers'/'MED', '7').
card_flavor_text('death speakers'/'MED', '\"Such innocent little birds. They sing a sweet song, sitting on their fragile branch.\"\n—Grandmother Sengir').
card_multiverse_id('death speakers'/'MED', '159816').

card_in_set('death ward', 'MED').
card_original_type('death ward'/'MED', 'Instant').
card_original_text('death ward'/'MED', 'Regenerate target creature.').
card_image_name('death ward'/'MED', 'death ward').
card_uid('death ward'/'MED', 'MED:Death Ward:death ward').
card_rarity('death ward'/'MED', 'Common').
card_artist('death ward'/'MED', 'Mark Poole').
card_number('death ward'/'MED', '8').
card_multiverse_id('death ward'/'MED', '159754').

card_in_set('derelor', 'MED').
card_original_type('derelor'/'MED', 'Creature — Thrull').
card_original_text('derelor'/'MED', 'Black spells you play cost {B} more to play.').
card_image_name('derelor'/'MED', 'derelor').
card_uid('derelor'/'MED', 'MED:Derelor:derelor').
card_rarity('derelor'/'MED', 'Uncommon').
card_artist('derelor'/'MED', 'Anson Maddocks').
card_number('derelor'/'MED', '66').
card_flavor_text('derelor'/'MED', '\"Strength it has, but at the cost of a continuous supply of energy. Such failure can bear only one result.\"\n—Execution order of Endrek Sahr,\nmaster breeder').
card_multiverse_id('derelor'/'MED', '159171').

card_in_set('diamond valley', 'MED').
card_original_type('diamond valley'/'MED', 'Land').
card_original_text('diamond valley'/'MED', '{T}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_image_name('diamond valley'/'MED', 'diamond valley').
card_uid('diamond valley'/'MED', 'MED:Diamond Valley:diamond valley').
card_rarity('diamond valley'/'MED', 'Rare').
card_artist('diamond valley'/'MED', 'Brian Snõddy').
card_number('diamond valley'/'MED', '175').
card_multiverse_id('diamond valley'/'MED', '159124').

card_in_set('diminishing returns', 'MED').
card_original_type('diminishing returns'/'MED', 'Sorcery').
card_original_text('diminishing returns'/'MED', 'Each player shuffles his or her hand and graveyard into his or her library. You remove the top ten cards of your library from the game. Then each player draws up to seven cards.').
card_image_name('diminishing returns'/'MED', 'diminishing returns').
card_uid('diminishing returns'/'MED', 'MED:Diminishing Returns:diminishing returns').
card_rarity('diminishing returns'/'MED', 'Rare').
card_artist('diminishing returns'/'MED', 'L. A. Williams').
card_number('diminishing returns'/'MED', '32').
card_multiverse_id('diminishing returns'/'MED', '159090').

card_in_set('divine transformation', 'MED').
card_original_type('divine transformation'/'MED', 'Enchantment — Aura').
card_original_text('divine transformation'/'MED', 'Enchant creature\nEnchanted creature gets +3/+3.').
card_image_name('divine transformation'/'MED', 'divine transformation').
card_uid('divine transformation'/'MED', 'MED:Divine Transformation:divine transformation').
card_rarity('divine transformation'/'MED', 'Uncommon').
card_artist('divine transformation'/'MED', 'NéNé Thomas').
card_number('divine transformation'/'MED', '9').
card_flavor_text('divine transformation'/'MED', 'Glory surged through her and radiance surrounded her. All things were possible with the blessing of the Divine.').
card_multiverse_id('divine transformation'/'MED', '159299').

card_in_set('dragon engine', 'MED').
card_original_type('dragon engine'/'MED', 'Artifact Creature — Construct').
card_original_text('dragon engine'/'MED', '{2}: Dragon Engine gets +1/+0 until end of turn.').
card_image_name('dragon engine'/'MED', 'dragon engine').
card_uid('dragon engine'/'MED', 'MED:Dragon Engine:dragon engine').
card_rarity('dragon engine'/'MED', 'Common').
card_artist('dragon engine'/'MED', 'Anson Maddocks').
card_number('dragon engine'/'MED', '156').
card_flavor_text('dragon engine'/'MED', 'Those who believed the city of Kroog would never fall to Mishra\'s forces severely underestimated the might of his war machines.').
card_multiverse_id('dragon engine'/'MED', '159841').

card_in_set('dust to dust', 'MED').
card_original_type('dust to dust'/'MED', 'Sorcery').
card_original_text('dust to dust'/'MED', 'Remove two target artifacts from the game.').
card_image_name('dust to dust'/'MED', 'dust to dust').
card_uid('dust to dust'/'MED', 'MED:Dust to Dust:dust to dust').
card_rarity('dust to dust'/'MED', 'Common').
card_artist('dust to dust'/'MED', 'Drew Tucker').
card_number('dust to dust'/'MED', '10').
card_flavor_text('dust to dust'/'MED', 'Tervish never noticed that the amulet had vanished. It had disappeared not only from his possession, but from his memory as well.').
card_multiverse_id('dust to dust'/'MED', '159144').

card_in_set('dwarven catapult', 'MED').
card_original_type('dwarven catapult'/'MED', 'Instant').
card_original_text('dwarven catapult'/'MED', 'Dwarven Catapult deals X damage divided evenly, rounded down, among all creatures target opponent controls.').
card_image_name('dwarven catapult'/'MED', 'dwarven catapult').
card_uid('dwarven catapult'/'MED', 'MED:Dwarven Catapult:dwarven catapult').
card_rarity('dwarven catapult'/'MED', 'Uncommon').
card_artist('dwarven catapult'/'MED', 'Jeff A. Menges').
card_number('dwarven catapult'/'MED', '91').
card_flavor_text('dwarven catapult'/'MED', '\"Often greatly outnumbered in battle, dwarves relied on catapults as one means of damaging a large army.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('dwarven catapult'/'MED', '159729').

card_in_set('dwarven soldier', 'MED').
card_original_type('dwarven soldier'/'MED', 'Creature — Dwarf Soldier').
card_original_text('dwarven soldier'/'MED', 'Whenever Dwarven Soldier blocks or becomes blocked by one or more Orcs, Dwarven Soldier gets +0/+2 until end of turn.').
card_image_name('dwarven soldier'/'MED', 'dwarven soldier').
card_uid('dwarven soldier'/'MED', 'MED:Dwarven Soldier:dwarven soldier').
card_rarity('dwarven soldier'/'MED', 'Common').
card_artist('dwarven soldier'/'MED', 'Rob Alexander').
card_number('dwarven soldier'/'MED', '92').
card_flavor_text('dwarven soldier'/'MED', '\"Let no one say we did not fight until the last . . . .\"\n—Headstone fragment from a mass grave found in the Crimson Peaks').
card_multiverse_id('dwarven soldier'/'MED', '159173').

card_in_set('eater of the dead', 'MED').
card_original_type('eater of the dead'/'MED', 'Creature — Horror').
card_original_text('eater of the dead'/'MED', 'Untap Eater of the Dead: Remove target creature card in a graveyard from the game.').
card_image_name('eater of the dead'/'MED', 'eater of the dead').
card_uid('eater of the dead'/'MED', 'MED:Eater of the Dead:eater of the dead').
card_rarity('eater of the dead'/'MED', 'Uncommon').
card_artist('eater of the dead'/'MED', 'Jesper Myrfors').
card_number('eater of the dead'/'MED', '67').
card_flavor_text('eater of the dead'/'MED', 'Even the putrid muscles of the dead can provide strength to those loathsome enough to consume them.').
card_multiverse_id('eater of the dead'/'MED', '159145').

card_in_set('elder land wurm', 'MED').
card_original_type('elder land wurm'/'MED', 'Creature — Dragon Wurm').
card_original_text('elder land wurm'/'MED', 'Defender, trample\nWhen Elder Land Wurm blocks, it loses defender.').
card_image_name('elder land wurm'/'MED', 'elder land wurm').
card_uid('elder land wurm'/'MED', 'MED:Elder Land Wurm:elder land wurm').
card_rarity('elder land wurm'/'MED', 'Uncommon').
card_artist('elder land wurm'/'MED', 'Quinton Hoover').
card_number('elder land wurm'/'MED', '11').
card_flavor_text('elder land wurm'/'MED', 'Sometimes it\'s best to let sleeping dragons lie.').
card_multiverse_id('elder land wurm'/'MED', '159824').

card_in_set('energy arc', 'MED').
card_original_type('energy arc'/'MED', 'Instant').
card_original_text('energy arc'/'MED', 'Untap any number of target creatures. Prevent all combat damage that would be dealt to and dealt by those creatures this turn.').
card_image_name('energy arc'/'MED', 'energy arc').
card_uid('energy arc'/'MED', 'MED:Energy Arc:energy arc').
card_rarity('energy arc'/'MED', 'Uncommon').
card_artist('energy arc'/'MED', 'Terese Nielsen').
card_number('energy arc'/'MED', '144').
card_flavor_text('energy arc'/'MED', '\"Relent, and you may transcend your situation.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('energy arc'/'MED', '159830').

card_in_set('erg raiders', 'MED').
card_original_type('erg raiders'/'MED', 'Creature — Human Warrior').
card_original_text('erg raiders'/'MED', 'At the end of your turn, if Erg Raiders didn\'t attack this turn, Erg Raiders deals 2 damage to you unless it came under your control this turn.').
card_image_name('erg raiders'/'MED', 'erg raiders').
card_uid('erg raiders'/'MED', 'MED:Erg Raiders:erg raiders').
card_rarity('erg raiders'/'MED', 'Common').
card_artist('erg raiders'/'MED', 'Dameon Willich').
card_number('erg raiders'/'MED', '68').
card_multiverse_id('erg raiders'/'MED', '159126').

card_in_set('eureka', 'MED').
card_original_type('eureka'/'MED', 'Sorcery').
card_original_text('eureka'/'MED', 'Starting with you, each player may put a permanent card from his or her hand into play. Repeat this process until no one puts a card into play.').
card_image_name('eureka'/'MED', 'eureka').
card_uid('eureka'/'MED', 'MED:Eureka:eureka').
card_rarity('eureka'/'MED', 'Rare').
card_artist('eureka'/'MED', 'Kaja Foglio').
card_number('eureka'/'MED', '117').
card_multiverse_id('eureka'/'MED', '159300').

card_in_set('exile', 'MED').
card_original_type('exile'/'MED', 'Instant').
card_original_text('exile'/'MED', 'Remove target nonwhite attacking creature from the game. You gain life equal to its toughness.').
card_image_name('exile'/'MED', 'exile').
card_uid('exile'/'MED', 'MED:Exile:exile').
card_rarity('exile'/'MED', 'Common').
card_artist('exile'/'MED', 'Rob Alexander').
card_number('exile'/'MED', '12').
card_multiverse_id('exile'/'MED', '159091').

card_in_set('feast or famine', 'MED').
card_original_type('feast or famine'/'MED', 'Instant').
card_original_text('feast or famine'/'MED', 'Choose one Put a 2/2 black Zombie creature token into play; or destroy target nonblack, nonartifact creature and it can\'t be regenerated.').
card_image_name('feast or famine'/'MED', 'feast or famine').
card_uid('feast or famine'/'MED', 'MED:Feast or Famine:feast or famine').
card_rarity('feast or famine'/'MED', 'Common').
card_artist('feast or famine'/'MED', 'Pete Venters').
card_number('feast or famine'/'MED', '70').
card_flavor_text('feast or famine'/'MED', '\"We are not yet free of Lim-Dûl\'s terrors.\"\n—Halvor Arensson, Kjeldoran priest').
card_multiverse_id('feast or famine'/'MED', '159739').

card_in_set('fire covenant', 'MED').
card_original_type('fire covenant'/'MED', 'Instant').
card_original_text('fire covenant'/'MED', 'As an additional cost to play Fire Covenant, pay X life.\nFire Covenant deals X damage divided as you choose among any number of target creatures.').
card_image_name('fire covenant'/'MED', 'fire covenant').
card_uid('fire covenant'/'MED', 'MED:Fire Covenant:fire covenant').
card_rarity('fire covenant'/'MED', 'Uncommon').
card_artist('fire covenant'/'MED', 'Dan Frazier').
card_number('fire covenant'/'MED', '145').
card_multiverse_id('fire covenant'/'MED', '159836').

card_in_set('fissure', 'MED').
card_original_type('fissure'/'MED', 'Instant').
card_original_text('fissure'/'MED', 'Destroy target creature or land. It can\'t be regenerated.').
card_image_name('fissure'/'MED', 'fissure').
card_uid('fissure'/'MED', 'MED:Fissure:fissure').
card_rarity('fissure'/'MED', 'Common').
card_artist('fissure'/'MED', 'Douglas Shuler').
card_number('fissure'/'MED', '93').
card_flavor_text('fissure'/'MED', '\"Must not all things at the last be swallowed up in death?\"\n—Plato, Phædo, trans. Jowett').
card_multiverse_id('fissure'/'MED', '159147').

card_in_set('force of will', 'MED').
card_original_type('force of will'/'MED', 'Instant').
card_original_text('force of will'/'MED', 'You may pay 1 life and remove a blue card in your hand from the game rather than pay Force of Will\'s mana cost.\nCounter target spell.').
card_image_name('force of will'/'MED', 'force of will').
card_uid('force of will'/'MED', 'MED:Force of Will:force of will').
card_rarity('force of will'/'MED', 'Rare').
card_artist('force of will'/'MED', 'Terese Nielsen').
card_number('force of will'/'MED', '33').
card_multiverse_id('force of will'/'MED', '159092').

card_in_set('forcefield', 'MED').
card_original_type('forcefield'/'MED', 'Artifact').
card_original_text('forcefield'/'MED', '{1}: The next time target unblocked creature would deal combat damage to you this turn, prevent all but 1 of that damage.').
card_image_name('forcefield'/'MED', 'forcefield').
card_uid('forcefield'/'MED', 'MED:Forcefield:forcefield').
card_rarity('forcefield'/'MED', 'Rare').
card_artist('forcefield'/'MED', 'Dan Frazier').
card_number('forcefield'/'MED', '157').
card_multiverse_id('forcefield'/'MED', '159755').

card_in_set('forest', 'MED').
card_original_type('forest'/'MED', 'Basic Land — Forest').
card_original_text('forest'/'MED', '').
card_image_name('forest'/'MED', 'forest1').
card_uid('forest'/'MED', 'MED:Forest:forest1').
card_rarity('forest'/'MED', 'Basic Land').
card_artist('forest'/'MED', 'Christopher Rush').
card_number('forest'/'MED', '193').
card_multiverse_id('forest'/'MED', '159278').

card_in_set('forest', 'MED').
card_original_type('forest'/'MED', 'Basic Land — Forest').
card_original_text('forest'/'MED', '').
card_image_name('forest'/'MED', 'forest2').
card_uid('forest'/'MED', 'MED:Forest:forest2').
card_rarity('forest'/'MED', 'Basic Land').
card_artist('forest'/'MED', 'Christopher Rush').
card_number('forest'/'MED', '194').
card_multiverse_id('forest'/'MED', '159279').

card_in_set('forest', 'MED').
card_original_type('forest'/'MED', 'Basic Land — Forest').
card_original_text('forest'/'MED', '').
card_image_name('forest'/'MED', 'forest3').
card_uid('forest'/'MED', 'MED:Forest:forest3').
card_rarity('forest'/'MED', 'Basic Land').
card_artist('forest'/'MED', 'Christopher Rush').
card_number('forest'/'MED', '195').
card_multiverse_id('forest'/'MED', '159280').

card_in_set('fyndhorn elves', 'MED').
card_original_type('fyndhorn elves'/'MED', 'Creature — Elf Druid').
card_original_text('fyndhorn elves'/'MED', '{T}: Add {G} to your mana pool.').
card_image_name('fyndhorn elves'/'MED', 'fyndhorn elves').
card_uid('fyndhorn elves'/'MED', 'MED:Fyndhorn Elves:fyndhorn elves').
card_rarity('fyndhorn elves'/'MED', 'Common').
card_artist('fyndhorn elves'/'MED', 'Justin Hampton').
card_number('fyndhorn elves'/'MED', '118').
card_flavor_text('fyndhorn elves'/'MED', '\"Living side by side with the elves for so long leaves me with no doubt that we serve the same goddess.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('fyndhorn elves'/'MED', '159228').

card_in_set('gargantuan gorilla', 'MED').
card_original_type('gargantuan gorilla'/'MED', 'Creature — Ape').
card_original_text('gargantuan gorilla'/'MED', 'At the beginning of your upkeep, sacrifice Gargantuan Gorilla unless you sacrifice a Forest. If you sacrifice Gargantuan Gorilla this way, it deals 7 damage to you. If you sacrifice a snow Forest this way, Gargantuan Gorilla gains trample until end of turn.\n{T}: Gargantuan Gorilla deals damage equal to its power to target creature. That creature deals damage equal to its power to Gargantuan Gorilla.').
card_image_name('gargantuan gorilla'/'MED', 'gargantuan gorilla').
card_uid('gargantuan gorilla'/'MED', 'MED:Gargantuan Gorilla:gargantuan gorilla').
card_rarity('gargantuan gorilla'/'MED', 'Rare').
card_artist('gargantuan gorilla'/'MED', 'Greg Simanson').
card_number('gargantuan gorilla'/'MED', '119').
card_multiverse_id('gargantuan gorilla'/'MED', '159093').

card_in_set('ghazbán ogre', 'MED').
card_original_type('ghazbán ogre'/'MED', 'Creature — Ogre').
card_original_text('ghazbán ogre'/'MED', 'At the beginning of your upkeep, if a player has more life than any other, that player gains control of Ghazbán Ogre.').
card_image_name('ghazbán ogre'/'MED', 'ghazban ogre').
card_uid('ghazbán ogre'/'MED', 'MED:Ghazbán Ogre:ghazban ogre').
card_rarity('ghazbán ogre'/'MED', 'Common').
card_artist('ghazbán ogre'/'MED', 'Jesper Myrfors').
card_number('ghazbán ogre'/'MED', '120').
card_multiverse_id('ghazbán ogre'/'MED', '159127').

card_in_set('giant tortoise', 'MED').
card_original_type('giant tortoise'/'MED', 'Creature — Turtle').
card_original_text('giant tortoise'/'MED', 'Giant Tortoise gets +0/+3 as long as it\'s untapped.').
card_image_name('giant tortoise'/'MED', 'giant tortoise').
card_uid('giant tortoise'/'MED', 'MED:Giant Tortoise:giant tortoise').
card_rarity('giant tortoise'/'MED', 'Common').
card_artist('giant tortoise'/'MED', 'Kaja Foglio').
card_number('giant tortoise'/'MED', '34').
card_multiverse_id('giant tortoise'/'MED', '159129').

card_in_set('goblin chirurgeon', 'MED').
card_original_type('goblin chirurgeon'/'MED', 'Creature — Goblin Shaman').
card_original_text('goblin chirurgeon'/'MED', 'Sacrifice a Goblin: Regenerate target creature.').
card_image_name('goblin chirurgeon'/'MED', 'goblin chirurgeon').
card_uid('goblin chirurgeon'/'MED', 'MED:Goblin Chirurgeon:goblin chirurgeon').
card_rarity('goblin chirurgeon'/'MED', 'Common').
card_artist('goblin chirurgeon'/'MED', 'Dan Frazier').
card_number('goblin chirurgeon'/'MED', '94').
card_flavor_text('goblin chirurgeon'/'MED', '\"Perhaps goblins are good for something after all.\"\n—General Khurzog').
card_multiverse_id('goblin chirurgeon'/'MED', '159730').

card_in_set('goblin grenade', 'MED').
card_original_type('goblin grenade'/'MED', 'Sorcery').
card_original_text('goblin grenade'/'MED', 'As an additional cost to play Goblin Grenade, sacrifice a Goblin.\nGoblin Grenade deals 5 damage to target creature or player.').
card_image_name('goblin grenade'/'MED', 'goblin grenade').
card_uid('goblin grenade'/'MED', 'MED:Goblin Grenade:goblin grenade').
card_rarity('goblin grenade'/'MED', 'Uncommon').
card_artist('goblin grenade'/'MED', 'Ron Spencer').
card_number('goblin grenade'/'MED', '95').
card_flavor_text('goblin grenade'/'MED', '\"According to accepted theory, the grenade held some kind of flammable mixture and was carried to its target by a hapless goblin.\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('goblin grenade'/'MED', '159731').

card_in_set('goblin mutant', 'MED').
card_original_type('goblin mutant'/'MED', 'Creature — Goblin Mutant').
card_original_text('goblin mutant'/'MED', 'Trample\nGoblin Mutant can\'t attack if defending player controls an untapped creature with power 3 or greater.\nGoblin Mutant can\'t block creatures with power 3 or greater.').
card_image_name('goblin mutant'/'MED', 'goblin mutant').
card_uid('goblin mutant'/'MED', 'MED:Goblin Mutant:goblin mutant').
card_rarity('goblin mutant'/'MED', 'Uncommon').
card_artist('goblin mutant'/'MED', 'Daniel Gelon').
card_number('goblin mutant'/'MED', '96').
card_flavor_text('goblin mutant'/'MED', 'If only it had three brains, too.').
card_multiverse_id('goblin mutant'/'MED', '159229').

card_in_set('goblin wizard', 'MED').
card_original_type('goblin wizard'/'MED', 'Creature — Goblin Wizard').
card_original_text('goblin wizard'/'MED', '{T}: You may put a Goblin permanent card from your hand into play.\n{R}: Target Goblin gains protection from white until end of turn.').
card_image_name('goblin wizard'/'MED', 'goblin wizard').
card_uid('goblin wizard'/'MED', 'MED:Goblin Wizard:goblin wizard').
card_rarity('goblin wizard'/'MED', 'Rare').
card_artist('goblin wizard'/'MED', 'Daniel Gelon').
card_number('goblin wizard'/'MED', '97').
card_multiverse_id('goblin wizard'/'MED', '159758').

card_in_set('goblins of the flarg', 'MED').
card_original_type('goblins of the flarg'/'MED', 'Creature — Goblin Warrior').
card_original_text('goblins of the flarg'/'MED', 'Mountainwalk\nWhen you control a Dwarf, sacrifice Goblins of the Flarg.').
card_image_name('goblins of the flarg'/'MED', 'goblins of the flarg').
card_uid('goblins of the flarg'/'MED', 'MED:Goblins of the Flarg:goblins of the flarg').
card_rarity('goblins of the flarg'/'MED', 'Common').
card_artist('goblins of the flarg'/'MED', 'Tom Wänerstrand').
card_number('goblins of the flarg'/'MED', '98').
card_multiverse_id('goblins of the flarg'/'MED', '159759').

card_in_set('granite gargoyle', 'MED').
card_original_type('granite gargoyle'/'MED', 'Creature — Gargoyle').
card_original_text('granite gargoyle'/'MED', 'Flying\n{R}: Granite Gargoyle gets +0/+1 until end of turn.').
card_image_name('granite gargoyle'/'MED', 'granite gargoyle').
card_uid('granite gargoyle'/'MED', 'MED:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'MED', 'Uncommon').
card_artist('granite gargoyle'/'MED', 'Christopher Rush').
card_number('granite gargoyle'/'MED', '99').
card_flavor_text('granite gargoyle'/'MED', '\"While most overworlders fortunately don\'t realize this, gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—Asmoranomardicadaistinaculdacar,\nThe Underworld Cookbook').
card_multiverse_id('granite gargoyle'/'MED', '159259').

card_in_set('greater realm of preservation', 'MED').
card_original_type('greater realm of preservation'/'MED', 'Enchantment').
card_original_text('greater realm of preservation'/'MED', '{1}{W}: The next time a black or red source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('greater realm of preservation'/'MED', 'greater realm of preservation').
card_uid('greater realm of preservation'/'MED', 'MED:Greater Realm of Preservation:greater realm of preservation').
card_rarity('greater realm of preservation'/'MED', 'Uncommon').
card_artist('greater realm of preservation'/'MED', 'NéNé Thomas').
card_number('greater realm of preservation'/'MED', '13').
card_multiverse_id('greater realm of preservation'/'MED', '159825').

card_in_set('hallowed ground', 'MED').
card_original_type('hallowed ground'/'MED', 'Enchantment').
card_original_text('hallowed ground'/'MED', '{W}{W}: Return target nonsnow land you control to its owner\'s hand.').
card_image_name('hallowed ground'/'MED', 'hallowed ground').
card_uid('hallowed ground'/'MED', 'MED:Hallowed Ground:hallowed ground').
card_rarity('hallowed ground'/'MED', 'Uncommon').
card_artist('hallowed ground'/'MED', 'Douglas Shuler').
card_number('hallowed ground'/'MED', '14').
card_flavor_text('hallowed ground'/'MED', '\"On this site where Kjeld\'s blood was spilled, let none raise a fist or deny a beggar.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('hallowed ground'/'MED', '159230').

card_in_set('hand of justice', 'MED').
card_original_type('hand of justice'/'MED', 'Creature — Avatar').
card_original_text('hand of justice'/'MED', '{T}, Tap three untapped white creatures you control: Destroy target creature.').
card_image_name('hand of justice'/'MED', 'hand of justice').
card_uid('hand of justice'/'MED', 'MED:Hand of Justice:hand of justice').
card_rarity('hand of justice'/'MED', 'Rare').
card_artist('hand of justice'/'MED', 'Melissa A. Benson').
card_number('hand of justice'/'MED', '15').
card_flavor_text('hand of justice'/'MED', '\"The Hand of Justice will come to cleanse the world if we are true.\"\n—Oliver Farrel').
card_multiverse_id('hand of justice'/'MED', '159175').

card_in_set('hecatomb', 'MED').
card_original_type('hecatomb'/'MED', 'Enchantment').
card_original_text('hecatomb'/'MED', 'When Hecatomb comes into play, sacrifice Hecatomb unless you sacrifice four creatures.\nTap an untapped Swamp you control: Hecatomb deals 1 damage to target creature or player.').
card_image_name('hecatomb'/'MED', 'hecatomb').
card_uid('hecatomb'/'MED', 'MED:Hecatomb:hecatomb').
card_rarity('hecatomb'/'MED', 'Rare').
card_artist('hecatomb'/'MED', 'NéNé Thomas').
card_number('hecatomb'/'MED', '71').
card_multiverse_id('hecatomb'/'MED', '159748').

card_in_set('high tide', 'MED').
card_original_type('high tide'/'MED', 'Instant').
card_original_text('high tide'/'MED', 'Until end of turn, whenever a player taps an Island for mana, that player adds {U} to his or her mana pool.').
card_image_name('high tide'/'MED', 'high tide').
card_uid('high tide'/'MED', 'MED:High Tide:high tide').
card_rarity('high tide'/'MED', 'Uncommon').
card_artist('high tide'/'MED', 'Anson Maddocks').
card_number('high tide'/'MED', '35').
card_flavor_text('high tide'/'MED', '\"May Svyelun and her tides favor you.\"\n—Traditional merfolk blessing').
card_multiverse_id('high tide'/'MED', '159177').

card_in_set('holy light', 'MED').
card_original_type('holy light'/'MED', 'Instant').
card_original_text('holy light'/'MED', 'Nonwhite creatures get -1/-1 until end of turn.').
card_image_name('holy light'/'MED', 'holy light').
card_uid('holy light'/'MED', 'MED:Holy Light:holy light').
card_rarity('holy light'/'MED', 'Common').
card_artist('holy light'/'MED', 'Drew Tucker').
card_number('holy light'/'MED', '16').
card_flavor_text('holy light'/'MED', '\"Bathed in hallowed light, the infidels looked upon the impurities of their souls and despaired.\"\n—The Book of Tal').
card_multiverse_id('holy light'/'MED', '159148').

card_in_set('homarid spawning bed', 'MED').
card_original_type('homarid spawning bed'/'MED', 'Enchantment').
card_original_text('homarid spawning bed'/'MED', '{1}{U}{U}, Sacrifice a blue creature: Put X 1/1 blue Camarid creature tokens into play, where X is the sacrificed creature\'s converted mana cost.').
card_image_name('homarid spawning bed'/'MED', 'homarid spawning bed').
card_uid('homarid spawning bed'/'MED', 'MED:Homarid Spawning Bed:homarid spawning bed').
card_rarity('homarid spawning bed'/'MED', 'Uncommon').
card_artist('homarid spawning bed'/'MED', 'Douglas Shuler').
card_number('homarid spawning bed'/'MED', '36').
card_multiverse_id('homarid spawning bed'/'MED', '159179').

card_in_set('hungry mist', 'MED').
card_original_type('hungry mist'/'MED', 'Creature — Elemental').
card_original_text('hungry mist'/'MED', 'At the beginning of your upkeep, sacrifice Hungry Mist unless you pay {G}{G}.').
card_image_name('hungry mist'/'MED', 'hungry mist').
card_uid('hungry mist'/'MED', 'MED:Hungry Mist:hungry mist').
card_rarity('hungry mist'/'MED', 'Common').
card_artist('hungry mist'/'MED', 'Heather Hudson').
card_number('hungry mist'/'MED', '121').
card_flavor_text('hungry mist'/'MED', '\"If the air must feed, let it take the self-righteous.\"\n—Murat, death speaker').
card_multiverse_id('hungry mist'/'MED', '159212').

card_in_set('hyalopterous lemure', 'MED').
card_original_type('hyalopterous lemure'/'MED', 'Creature — Spirit').
card_original_text('hyalopterous lemure'/'MED', '{0}: Hyalopterous Lemure gets -1/-0 and gains flying until end of turn.').
card_image_name('hyalopterous lemure'/'MED', 'hyalopterous lemure').
card_uid('hyalopterous lemure'/'MED', 'MED:Hyalopterous Lemure:hyalopterous lemure').
card_rarity('hyalopterous lemure'/'MED', 'Common').
card_artist('hyalopterous lemure'/'MED', 'Richard Thomas').
card_number('hyalopterous lemure'/'MED', '72').
card_flavor_text('hyalopterous lemure'/'MED', '\"The lemures looked harmless, until they descended on my troops. Within moments, only bones remained.\"\n—Lucilde Fiksdotter, Leader of the Order of the White Shield').
card_multiverse_id('hyalopterous lemure'/'MED', '159837').

card_in_set('hydroblast', 'MED').
card_original_type('hydroblast'/'MED', 'Instant').
card_original_text('hydroblast'/'MED', 'Choose one Counter target spell if it\'s red; or destroy target permanent if it\'s red.').
card_image_name('hydroblast'/'MED', 'hydroblast').
card_uid('hydroblast'/'MED', 'MED:Hydroblast:hydroblast').
card_rarity('hydroblast'/'MED', 'Common').
card_artist('hydroblast'/'MED', 'Kaja Foglio').
card_number('hydroblast'/'MED', '37').
card_flavor_text('hydroblast'/'MED', '\"Heed the lessons of our time: the forms of water may move the land itself and hold captive the fires within.\"\n—Gustha Ebbasdotter,\nKjeldoran royal mage').
card_multiverse_id('hydroblast'/'MED', '159231').

card_in_set('hymn of rebirth', 'MED').
card_original_type('hymn of rebirth'/'MED', 'Sorcery').
card_original_text('hymn of rebirth'/'MED', 'Put target creature card in a graveyard into play under your control.').
card_image_name('hymn of rebirth'/'MED', 'hymn of rebirth').
card_uid('hymn of rebirth'/'MED', 'MED:Hymn of Rebirth:hymn of rebirth').
card_rarity('hymn of rebirth'/'MED', 'Uncommon').
card_artist('hymn of rebirth'/'MED', 'Richard Kane Ferguson').
card_number('hymn of rebirth'/'MED', '146').
card_flavor_text('hymn of rebirth'/'MED', '\"There will come soft rains, and spring shall be amongst us, a welcome friend.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('hymn of rebirth'/'MED', '159838').

card_in_set('hymn to tourach', 'MED').
card_original_type('hymn to tourach'/'MED', 'Sorcery').
card_original_text('hymn to tourach'/'MED', 'Target player discards two cards at random.').
card_image_name('hymn to tourach'/'MED', 'hymn to tourach').
card_uid('hymn to tourach'/'MED', 'MED:Hymn to Tourach:hymn to tourach').
card_rarity('hymn to tourach'/'MED', 'Uncommon').
card_artist('hymn to tourach'/'MED', 'Quinton Hoover').
card_number('hymn to tourach'/'MED', '73').
card_flavor_text('hymn to tourach'/'MED', 'Knowing the hymn\'s power, the followers of Leitbur carefully guarded their pillaged transciptions.').
card_multiverse_id('hymn to tourach'/'MED', '159180').

card_in_set('icatian lieutenant', 'MED').
card_original_type('icatian lieutenant'/'MED', 'Creature — Human Soldier').
card_original_text('icatian lieutenant'/'MED', '{1}{W}: Target Soldier creature gets +1/+0 until end of turn.').
card_image_name('icatian lieutenant'/'MED', 'icatian lieutenant').
card_uid('icatian lieutenant'/'MED', 'MED:Icatian Lieutenant:icatian lieutenant').
card_rarity('icatian lieutenant'/'MED', 'Common').
card_artist('icatian lieutenant'/'MED', 'Pete Venters').
card_number('icatian lieutenant'/'MED', '17').
card_flavor_text('icatian lieutenant'/'MED', 'To become an officer, an Icatian soldier had to pass a series of tests. These evaluated not only fighting and leadership skills, but also integrity, honor, and moral strength.').
card_multiverse_id('icatian lieutenant'/'MED', '159846').

card_in_set('icatian town', 'MED').
card_original_type('icatian town'/'MED', 'Sorcery').
card_original_text('icatian town'/'MED', 'Put four 1/1 white Citizen creature tokens into play.').
card_image_name('icatian town'/'MED', 'icatian town').
card_uid('icatian town'/'MED', 'MED:Icatian Town:icatian town').
card_rarity('icatian town'/'MED', 'Uncommon').
card_artist('icatian town'/'MED', 'Tom Wänerstrand').
card_number('icatian town'/'MED', '18').
card_flavor_text('icatian town'/'MED', 'Icatia\'s once peaceful towns faced increasing attacks from orcs and goblins as the climate cooled. By the time the empire fell, they were little more than armed camps.').
card_multiverse_id('icatian town'/'MED', '159843').

card_in_set('ice storm', 'MED').
card_original_type('ice storm'/'MED', 'Sorcery').
card_original_text('ice storm'/'MED', 'Destroy target land.').
card_image_name('ice storm'/'MED', 'ice storm').
card_uid('ice storm'/'MED', 'MED:Ice Storm:ice storm').
card_rarity('ice storm'/'MED', 'Uncommon').
card_artist('ice storm'/'MED', 'Dan Frazier').
card_number('ice storm'/'MED', '122').
card_multiverse_id('ice storm'/'MED', '159260').

card_in_set('ifh-bíff efreet', 'MED').
card_original_type('ifh-bíff efreet'/'MED', 'Creature — Efreet').
card_original_text('ifh-bíff efreet'/'MED', 'Flying\n{G}: Ifh-Bíff Efreet deals 1 damage to each creature with flying and each player. Any player may play this ability.').
card_image_name('ifh-bíff efreet'/'MED', 'ifh-biff efreet').
card_uid('ifh-bíff efreet'/'MED', 'MED:Ifh-Bíff Efreet:ifh-biff efreet').
card_rarity('ifh-bíff efreet'/'MED', 'Rare').
card_artist('ifh-bíff efreet'/'MED', 'Jesper Myrfors').
card_number('ifh-bíff efreet'/'MED', '123').
card_multiverse_id('ifh-bíff efreet'/'MED', '159130').

card_in_set('illusionary forces', 'MED').
card_original_type('illusionary forces'/'MED', 'Creature — Illusion').
card_original_text('illusionary forces'/'MED', 'Flying\nCumulative upkeep {U}').
card_image_name('illusionary forces'/'MED', 'illusionary forces').
card_uid('illusionary forces'/'MED', 'MED:Illusionary Forces:illusionary forces').
card_rarity('illusionary forces'/'MED', 'Uncommon').
card_artist('illusionary forces'/'MED', 'Justin Hampton').
card_number('illusionary forces'/'MED', '38').
card_flavor_text('illusionary forces'/'MED', '\"This school was founded in secret, operates in secret, and exists for the teaching of secrets. Those who would alter reality must first escape it.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('illusionary forces'/'MED', '159232').

card_in_set('illusionary wall', 'MED').
card_original_type('illusionary wall'/'MED', 'Creature — Illusion Wall').
card_original_text('illusionary wall'/'MED', 'Defender, flying, first strike\nCumulative upkeep {U}').
card_image_name('illusionary wall'/'MED', 'illusionary wall').
card_uid('illusionary wall'/'MED', 'MED:Illusionary Wall:illusionary wall').
card_rarity('illusionary wall'/'MED', 'Common').
card_artist('illusionary wall'/'MED', 'Mark Poole').
card_number('illusionary wall'/'MED', '39').
card_flavor_text('illusionary wall'/'MED', '\"Let them see what is not there and feel what does not touch them. When they no longer trust their senses, that is the time to strike.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('illusionary wall'/'MED', '159233').

card_in_set('illusions of grandeur', 'MED').
card_original_type('illusions of grandeur'/'MED', 'Enchantment').
card_original_text('illusions of grandeur'/'MED', 'Cumulative upkeep {2}\nWhen Illusions of Grandeur comes into play, you gain 20 life.\nWhen Illusions of Grandeur leaves play, you lose 20 life.').
card_image_name('illusions of grandeur'/'MED', 'illusions of grandeur').
card_uid('illusions of grandeur'/'MED', 'MED:Illusions of Grandeur:illusions of grandeur').
card_rarity('illusions of grandeur'/'MED', 'Rare').
card_artist('illusions of grandeur'/'MED', 'Quinton Hoover').
card_number('illusions of grandeur'/'MED', '40').
card_multiverse_id('illusions of grandeur'/'MED', '159749').

card_in_set('island', 'MED').
card_original_type('island'/'MED', 'Basic Land — Island').
card_original_text('island'/'MED', '').
card_image_name('island'/'MED', 'island1').
card_uid('island'/'MED', 'MED:Island:island1').
card_rarity('island'/'MED', 'Basic Land').
card_artist('island'/'MED', 'Mark Poole').
card_number('island'/'MED', '184').
card_multiverse_id('island'/'MED', '159281').

card_in_set('island', 'MED').
card_original_type('island'/'MED', 'Basic Land — Island').
card_original_text('island'/'MED', '').
card_image_name('island'/'MED', 'island2').
card_uid('island'/'MED', 'MED:Island:island2').
card_rarity('island'/'MED', 'Basic Land').
card_artist('island'/'MED', 'Mark Poole').
card_number('island'/'MED', '185').
card_multiverse_id('island'/'MED', '159282').

card_in_set('island', 'MED').
card_original_type('island'/'MED', 'Basic Land — Island').
card_original_text('island'/'MED', '').
card_image_name('island'/'MED', 'island3').
card_uid('island'/'MED', 'MED:Island:island3').
card_rarity('island'/'MED', 'Basic Land').
card_artist('island'/'MED', 'Mark Poole').
card_number('island'/'MED', '186').
card_multiverse_id('island'/'MED', '159283').

card_in_set('island of wak-wak', 'MED').
card_original_type('island of wak-wak'/'MED', 'Land').
card_original_text('island of wak-wak'/'MED', '{T}: The power of target creature with flying becomes 0 until end of turn.').
card_image_name('island of wak-wak'/'MED', 'island of wak-wak').
card_uid('island of wak-wak'/'MED', 'MED:Island of Wak-Wak:island of wak-wak').
card_rarity('island of wak-wak'/'MED', 'Rare').
card_artist('island of wak-wak'/'MED', 'Douglas Shuler').
card_number('island of wak-wak'/'MED', '176').
card_flavor_text('island of wak-wak'/'MED', 'The Isle of Wak-Wak, home to a tribe of winged folk, is named for a peculiar fruit that grows there. The fruit looks like a woman\'s head, and when ripe speaks the word \"Wak.\"').
card_multiverse_id('island of wak-wak'/'MED', '159763').

card_in_set('ivory tower', 'MED').
card_original_type('ivory tower'/'MED', 'Artifact').
card_original_text('ivory tower'/'MED', 'At the beginning of your upkeep, you gain X life, where X is the number of cards in your hand minus four.').
card_image_name('ivory tower'/'MED', 'ivory tower').
card_uid('ivory tower'/'MED', 'MED:Ivory Tower:ivory tower').
card_rarity('ivory tower'/'MED', 'Rare').
card_artist('ivory tower'/'MED', 'Margaret Organ-Kean').
card_number('ivory tower'/'MED', '158').
card_flavor_text('ivory tower'/'MED', 'Valuing scholarship above all else, the inhabitants of the Ivory Tower reward those who sacrifice power for knowledge.').
card_multiverse_id('ivory tower'/'MED', '159111').

card_in_set('jacques le vert', 'MED').
card_original_type('jacques le vert'/'MED', 'Legendary Creature — Human Warrior').
card_original_text('jacques le vert'/'MED', 'Green creatures you control get +0/+2.').
card_image_name('jacques le vert'/'MED', 'jacques le vert').
card_uid('jacques le vert'/'MED', 'MED:Jacques le Vert:jacques le vert').
card_rarity('jacques le vert'/'MED', 'Rare').
card_artist('jacques le vert'/'MED', 'Andi Rusu').
card_number('jacques le vert'/'MED', '147').
card_flavor_text('jacques le vert'/'MED', 'Abandoning his sword to return to the lush forest of Pendelhaven, Jacques le Vert devoted his life to protecting the creatures of his homeland.').
card_multiverse_id('jacques le vert'/'MED', '159301').

card_in_set('jokulhaups', 'MED').
card_original_type('jokulhaups'/'MED', 'Sorcery').
card_original_text('jokulhaups'/'MED', 'Destroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_image_name('jokulhaups'/'MED', 'jokulhaups').
card_uid('jokulhaups'/'MED', 'MED:Jokulhaups:jokulhaups').
card_rarity('jokulhaups'/'MED', 'Rare').
card_artist('jokulhaups'/'MED', 'Richard Thomas').
card_number('jokulhaups'/'MED', '100').
card_flavor_text('jokulhaups'/'MED', '\"I was shocked when I first saw the aftermath of the Yavimaya Valley disaster. The raging waters had swept away trees, bridges, and even houses. My healers had much work to do.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('jokulhaups'/'MED', '159235').

card_in_set('juxtapose', 'MED').
card_original_type('juxtapose'/'MED', 'Sorcery').
card_original_text('juxtapose'/'MED', 'You and target player exchange control of the creature you each control with the highest converted mana cost. Then exchange control of artifacts the same way. If two or more permanents a player controls are tied for highest cost, their controller chooses one of them.').
card_image_name('juxtapose'/'MED', 'juxtapose').
card_uid('juxtapose'/'MED', 'MED:Juxtapose:juxtapose').
card_rarity('juxtapose'/'MED', 'Uncommon').
card_artist('juxtapose'/'MED', 'Justin Hampton').
card_number('juxtapose'/'MED', '41').
card_multiverse_id('juxtapose'/'MED', '159772').

card_in_set('juzám djinn', 'MED').
card_original_type('juzám djinn'/'MED', 'Creature — Djinn').
card_original_text('juzám djinn'/'MED', 'At the beginning of your upkeep, Juzám Djinn deals 1 damage to you.').
card_image_name('juzám djinn'/'MED', 'juzam djinn').
card_uid('juzám djinn'/'MED', 'MED:Juzám Djinn:juzam djinn').
card_rarity('juzám djinn'/'MED', 'Rare').
card_artist('juzám djinn'/'MED', 'Mark Tedin').
card_number('juzám djinn'/'MED', '74').
card_flavor_text('juzám djinn'/'MED', '\"Expect my visit when the darkness comes. The night I think is best for hiding all.\"\n—Ouallada').
card_multiverse_id('juzám djinn'/'MED', '159132').

card_in_set('keldon warlord', 'MED').
card_original_type('keldon warlord'/'MED', 'Creature — Human Barbarian').
card_original_text('keldon warlord'/'MED', 'Keldon Warlord\'s power and toughness are each equal to the number of non-Wall creatures you control.').
card_image_name('keldon warlord'/'MED', 'keldon warlord').
card_uid('keldon warlord'/'MED', 'MED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'MED', 'Uncommon').
card_artist('keldon warlord'/'MED', 'Kev Brockschmidt').
card_number('keldon warlord'/'MED', '101').
card_multiverse_id('keldon warlord'/'MED', '159262').

card_in_set('khabál ghoul', 'MED').
card_original_type('khabál ghoul'/'MED', 'Creature — Zombie').
card_original_text('khabál ghoul'/'MED', 'At end of turn, put a +1/+1 counter on Khabál Ghoul for each creature put into a graveyard from play this turn.').
card_image_name('khabál ghoul'/'MED', 'khabal ghoul').
card_uid('khabál ghoul'/'MED', 'MED:Khabál Ghoul:khabal ghoul').
card_rarity('khabál ghoul'/'MED', 'Rare').
card_artist('khabál ghoul'/'MED', 'Douglas Shuler').
card_number('khabál ghoul'/'MED', '75').
card_multiverse_id('khabál ghoul'/'MED', '159764').

card_in_set('knights of thorn', 'MED').
card_original_type('knights of thorn'/'MED', 'Creature — Human Knight').
card_original_text('knights of thorn'/'MED', 'Banding, protection from red').
card_image_name('knights of thorn'/'MED', 'knights of thorn').
card_uid('knights of thorn'/'MED', 'MED:Knights of Thorn:knights of thorn').
card_rarity('knights of thorn'/'MED', 'Common').
card_artist('knights of thorn'/'MED', 'Christopher Rush').
card_number('knights of thorn'/'MED', '19').
card_flavor_text('knights of thorn'/'MED', '\"With a great cry, the goblin host broke and ran as the first wave of knights penetrated its ranks.\"\n—Tivadar of Thorn,\nHistory of the Goblin Wars').
card_multiverse_id('knights of thorn'/'MED', '159149').

card_in_set('lake of the dead', 'MED').
card_original_type('lake of the dead'/'MED', 'Land').
card_original_text('lake of the dead'/'MED', 'If Lake of the Dead would come into play, sacrifice a Swamp instead. If you do, put Lake of the Dead into play. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice a Swamp: Add {B}{B}{B}{B} to your mana pool.').
card_image_name('lake of the dead'/'MED', 'lake of the dead').
card_uid('lake of the dead'/'MED', 'MED:Lake of the Dead:lake of the dead').
card_rarity('lake of the dead'/'MED', 'Rare').
card_artist('lake of the dead'/'MED', 'Pete Venters').
card_number('lake of the dead'/'MED', '177').
card_multiverse_id('lake of the dead'/'MED', '159095').

card_in_set('lightning bolt', 'MED').
card_original_type('lightning bolt'/'MED', 'Instant').
card_original_text('lightning bolt'/'MED', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'MED', 'lightning bolt').
card_uid('lightning bolt'/'MED', 'MED:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'MED', 'Common').
card_artist('lightning bolt'/'MED', 'Christopher Rush').
card_number('lightning bolt'/'MED', '102').
card_multiverse_id('lightning bolt'/'MED', '159263').

card_in_set('lim-dûl\'s vault', 'MED').
card_original_type('lim-dûl\'s vault'/'MED', 'Instant').
card_original_text('lim-dûl\'s vault'/'MED', 'Look at the top five cards of your library. As many times as you choose, you may pay 1 life, put those cards on the bottom of your library, then look at the top five cards of your library. Then shuffle your library and put the last cards you looked at this way on top of it in any order.').
card_image_name('lim-dûl\'s vault'/'MED', 'lim-dul\'s vault').
card_uid('lim-dûl\'s vault'/'MED', 'MED:Lim-Dûl\'s Vault:lim-dul\'s vault').
card_rarity('lim-dûl\'s vault'/'MED', 'Uncommon').
card_artist('lim-dûl\'s vault'/'MED', 'Rob Alexander').
card_number('lim-dûl\'s vault'/'MED', '148').
card_multiverse_id('lim-dûl\'s vault'/'MED', '159832').

card_in_set('lord of tresserhorn', 'MED').
card_original_type('lord of tresserhorn'/'MED', 'Legendary Creature — Zombie').
card_original_text('lord of tresserhorn'/'MED', 'When Lord of Tresserhorn comes into play, you lose 2 life and sacrifice two creatures, and an opponent draws two cards.\n{B}: Regenerate Lord of Tresserhorn.').
card_image_name('lord of tresserhorn'/'MED', 'lord of tresserhorn').
card_uid('lord of tresserhorn'/'MED', 'MED:Lord of Tresserhorn:lord of tresserhorn').
card_rarity('lord of tresserhorn'/'MED', 'Rare').
card_artist('lord of tresserhorn'/'MED', 'Anson Maddocks').
card_number('lord of tresserhorn'/'MED', '149').
card_multiverse_id('lord of tresserhorn'/'MED', '159096').

card_in_set('mana flare', 'MED').
card_original_type('mana flare'/'MED', 'Enchantment').
card_original_text('mana flare'/'MED', 'Whenever a player taps a land for mana, that player adds one mana of that type to his or her mana pool.').
card_image_name('mana flare'/'MED', 'mana flare').
card_uid('mana flare'/'MED', 'MED:Mana Flare:mana flare').
card_rarity('mana flare'/'MED', 'Rare').
card_artist('mana flare'/'MED', 'Christopher Rush').
card_number('mana flare'/'MED', '103').
card_multiverse_id('mana flare'/'MED', '159264').

card_in_set('márton stromgald', 'MED').
card_original_type('márton stromgald'/'MED', 'Legendary Creature — Human Knight').
card_original_text('márton stromgald'/'MED', 'Whenever Márton Stromgald attacks, other attacking creatures get +1/+1 until end of turn for each attacking creature other than Márton Stromgald.\nWhenever Márton Stromgald blocks, other blocking creatures get +1/+1 until end of turn for each blocking creature other than Márton Stromgald.').
card_image_name('márton stromgald'/'MED', 'marton stromgald').
card_uid('márton stromgald'/'MED', 'MED:Márton Stromgald:marton stromgald').
card_rarity('márton stromgald'/'MED', 'Rare').
card_artist('márton stromgald'/'MED', 'Mark Poole').
card_number('márton stromgald'/'MED', '104').
card_multiverse_id('márton stromgald'/'MED', '159237').

card_in_set('mesa pegasus', 'MED').
card_original_type('mesa pegasus'/'MED', 'Creature — Pegasus').
card_original_text('mesa pegasus'/'MED', 'Flying, banding').
card_image_name('mesa pegasus'/'MED', 'mesa pegasus').
card_uid('mesa pegasus'/'MED', 'MED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'MED', 'Common').
card_artist('mesa pegasus'/'MED', 'Melissa A. Benson').
card_number('mesa pegasus'/'MED', '20').
card_flavor_text('mesa pegasus'/'MED', 'Before a woman marries in the village of Sursi, she must visit the land of the mesa pegasus. Legend has it that if the woman is pure of heart and her love is true, a mesa pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'MED', '159819').

card_in_set('mindstab thrull', 'MED').
card_original_type('mindstab thrull'/'MED', 'Creature — Thrull').
card_original_text('mindstab thrull'/'MED', 'Whenever Mindstab Thrull attacks and isn\'t blocked, you may sacrifice it. If you do, defending player discards three cards.').
card_image_name('mindstab thrull'/'MED', 'mindstab thrull').
card_uid('mindstab thrull'/'MED', 'MED:Mindstab Thrull:mindstab thrull').
card_rarity('mindstab thrull'/'MED', 'Common').
card_artist('mindstab thrull'/'MED', 'Mark Tedin').
card_number('mindstab thrull'/'MED', '76').
card_multiverse_id('mindstab thrull'/'MED', '159821').

card_in_set('mirror universe', 'MED').
card_original_type('mirror universe'/'MED', 'Artifact').
card_original_text('mirror universe'/'MED', '{T}, Sacrifice Mirror Universe: Exchange life totals with target opponent. Play this ability only during your upkeep.').
card_image_name('mirror universe'/'MED', 'mirror universe').
card_uid('mirror universe'/'MED', 'MED:Mirror Universe:mirror universe').
card_rarity('mirror universe'/'MED', 'Rare').
card_artist('mirror universe'/'MED', 'Phil Foglio').
card_number('mirror universe'/'MED', '159').
card_multiverse_id('mirror universe'/'MED', '159307').

card_in_set('mishra\'s factory', 'MED').
card_original_type('mishra\'s factory'/'MED', 'Land').
card_original_text('mishra\'s factory'/'MED', '{T}: Add {1} to your mana pool.\n{1}: Mishra\'s Factory becomes a 2/2 Assembly-Worker artifact creature until end of turn. It\'s still a land.\n{T}: Target Assembly-Worker creature gets +1/+1 until end of turn.').
card_image_name('mishra\'s factory'/'MED', 'mishra\'s factory').
card_uid('mishra\'s factory'/'MED', 'MED:Mishra\'s Factory:mishra\'s factory').
card_rarity('mishra\'s factory'/'MED', 'Uncommon').
card_artist('mishra\'s factory'/'MED', 'Kaja & Phil Foglio').
card_number('mishra\'s factory'/'MED', '178').
card_multiverse_id('mishra\'s factory'/'MED', '159114').

card_in_set('moat', 'MED').
card_original_type('moat'/'MED', 'Enchantment').
card_original_text('moat'/'MED', 'Creatures without flying can\'t attack.').
card_image_name('moat'/'MED', 'moat').
card_uid('moat'/'MED', 'MED:Moat:moat').
card_rarity('moat'/'MED', 'Rare').
card_artist('moat'/'MED', 'Jeff A. Menges').
card_number('moat'/'MED', '21').
card_flavor_text('moat'/'MED', 'The purpose of any moat is to impede attack. Some are filled with water, some with thistles. Some are filled with things best left unseen.').
card_multiverse_id('moat'/'MED', '159308').

card_in_set('mountain', 'MED').
card_original_type('mountain'/'MED', 'Basic Land — Mountain').
card_original_text('mountain'/'MED', '').
card_image_name('mountain'/'MED', 'mountain1').
card_uid('mountain'/'MED', 'MED:Mountain:mountain1').
card_rarity('mountain'/'MED', 'Basic Land').
card_artist('mountain'/'MED', 'Douglas Shuler').
card_number('mountain'/'MED', '190').
card_multiverse_id('mountain'/'MED', '159284').

card_in_set('mountain', 'MED').
card_original_type('mountain'/'MED', 'Basic Land — Mountain').
card_original_text('mountain'/'MED', '').
card_image_name('mountain'/'MED', 'mountain2').
card_uid('mountain'/'MED', 'MED:Mountain:mountain2').
card_rarity('mountain'/'MED', 'Basic Land').
card_artist('mountain'/'MED', 'Douglas Shuler').
card_number('mountain'/'MED', '191').
card_multiverse_id('mountain'/'MED', '159285').

card_in_set('mountain', 'MED').
card_original_type('mountain'/'MED', 'Basic Land — Mountain').
card_original_text('mountain'/'MED', '').
card_image_name('mountain'/'MED', 'mountain3').
card_uid('mountain'/'MED', 'MED:Mountain:mountain3').
card_rarity('mountain'/'MED', 'Basic Land').
card_artist('mountain'/'MED', 'Douglas Shuler').
card_number('mountain'/'MED', '192').
card_multiverse_id('mountain'/'MED', '159286').

card_in_set('mountain yeti', 'MED').
card_original_type('mountain yeti'/'MED', 'Creature — Yeti').
card_original_text('mountain yeti'/'MED', 'Mountainwalk, protection from white').
card_image_name('mountain yeti'/'MED', 'mountain yeti').
card_uid('mountain yeti'/'MED', 'MED:Mountain Yeti:mountain yeti').
card_rarity('mountain yeti'/'MED', 'Common').
card_artist('mountain yeti'/'MED', 'Dan Frazier').
card_number('mountain yeti'/'MED', '105').
card_flavor_text('mountain yeti'/'MED', 'The Yeti\'s single greatest asset is its unnerving ability to blend in with its surroundings.').
card_multiverse_id('mountain yeti'/'MED', '159773').

card_in_set('mystic remora', 'MED').
card_original_type('mystic remora'/'MED', 'Enchantment').
card_original_text('mystic remora'/'MED', 'Cumulative upkeep {1}\nWhenever an opponent plays a noncreature spell, you may draw a card unless that player pays {4}.').
card_image_name('mystic remora'/'MED', 'mystic remora').
card_uid('mystic remora'/'MED', 'MED:Mystic Remora:mystic remora').
card_rarity('mystic remora'/'MED', 'Uncommon').
card_artist('mystic remora'/'MED', 'Ken Meyer, Jr.').
card_number('mystic remora'/'MED', '42').
card_multiverse_id('mystic remora'/'MED', '159831').

card_in_set('nature\'s lore', 'MED').
card_original_type('nature\'s lore'/'MED', 'Sorcery').
card_original_text('nature\'s lore'/'MED', 'Search your library for a Forest card and put that card into play. Then shuffle your library.').
card_image_name('nature\'s lore'/'MED', 'nature\'s lore').
card_uid('nature\'s lore'/'MED', 'MED:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'MED', 'Common').
card_artist('nature\'s lore'/'MED', 'Rick Emond').
card_number('nature\'s lore'/'MED', '124').
card_flavor_text('nature\'s lore'/'MED', '\"Fyndhorn is our home.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('nature\'s lore'/'MED', '159238').

card_in_set('nether shadow', 'MED').
card_original_type('nether shadow'/'MED', 'Creature — Spirit').
card_original_text('nether shadow'/'MED', 'Haste\nAt the beginning of your upkeep, if Nether Shadow is in your graveyard with three or more creature cards above it, you may put Nether Shadow into play.').
card_image_name('nether shadow'/'MED', 'nether shadow').
card_uid('nether shadow'/'MED', 'MED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'MED', 'Uncommon').
card_artist('nether shadow'/'MED', 'Christopher Rush').
card_number('nether shadow'/'MED', '77').
card_multiverse_id('nether shadow'/'MED', '159265').

card_in_set('nevinyrral\'s disk', 'MED').
card_original_type('nevinyrral\'s disk'/'MED', 'Artifact').
card_original_text('nevinyrral\'s disk'/'MED', 'Nevinyrral\'s Disk comes into play tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'MED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'MED', 'MED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'MED', 'Rare').
card_artist('nevinyrral\'s disk'/'MED', 'Mark Tedin').
card_number('nevinyrral\'s disk'/'MED', '160').
card_multiverse_id('nevinyrral\'s disk'/'MED', '159266').

card_in_set('onulet', 'MED').
card_original_type('onulet'/'MED', 'Artifact Creature — Construct').
card_original_text('onulet'/'MED', 'When Onulet is put into a graveyard from play, you gain 2 life.').
card_image_name('onulet'/'MED', 'onulet').
card_uid('onulet'/'MED', 'MED:Onulet:onulet').
card_rarity('onulet'/'MED', 'Common').
card_artist('onulet'/'MED', 'Anson Maddocks').
card_number('onulet'/'MED', '161').
card_flavor_text('onulet'/'MED', 'An early inspiration for Urza, Tocasia\'s onulets contained magical essences that could be cannibalized after they stopped functioning.').
card_multiverse_id('onulet'/'MED', '159116').

card_in_set('orcish mechanics', 'MED').
card_original_type('orcish mechanics'/'MED', 'Creature — Orc').
card_original_text('orcish mechanics'/'MED', '{T}, Sacrifice an artifact: Orcish Mechanics deals 2 damage to target creature or player.').
card_image_name('orcish mechanics'/'MED', 'orcish mechanics').
card_uid('orcish mechanics'/'MED', 'MED:Orcish Mechanics:orcish mechanics').
card_rarity('orcish mechanics'/'MED', 'Uncommon').
card_artist('orcish mechanics'/'MED', 'Pete Venters').
card_number('orcish mechanics'/'MED', '106').
card_multiverse_id('orcish mechanics'/'MED', '159117').

card_in_set('order of leitbur', 'MED').
card_original_type('order of leitbur'/'MED', 'Creature — Human Cleric Knight').
card_original_text('order of leitbur'/'MED', 'Protection from black\n{W}{W}: Order of Leitbur gets +1/+0 until end of turn.\n{W}: Order of Leitbur gains first strike until end of turn.').
card_image_name('order of leitbur'/'MED', 'order of leitbur').
card_uid('order of leitbur'/'MED', 'MED:Order of Leitbur:order of leitbur').
card_rarity('order of leitbur'/'MED', 'Common').
card_artist('order of leitbur'/'MED', 'Randy Asplund-Faith').
card_number('order of leitbur'/'MED', '22').
card_flavor_text('order of leitbur'/'MED', '\"Trained to battle the followers of Tourach, the Order of Leitbur was not as successful in later conflicts with orcish and goblin raiders.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('order of leitbur'/'MED', '159188').

card_in_set('order of the ebon hand', 'MED').
card_original_type('order of the ebon hand'/'MED', 'Creature — Cleric Knight').
card_original_text('order of the ebon hand'/'MED', 'Protection from white\n{B}{B}: Order of the Ebon Hand gets +1/+0 until end of turn.\n{B}: Order of the Ebon Hand gains first strike until end of turn.').
card_image_name('order of the ebon hand'/'MED', 'order of the ebon hand').
card_uid('order of the ebon hand'/'MED', 'MED:Order of the Ebon Hand:order of the ebon hand').
card_rarity('order of the ebon hand'/'MED', 'Common').
card_artist('order of the ebon hand'/'MED', 'Ron Spencer').
card_number('order of the ebon hand'/'MED', '78').
card_multiverse_id('order of the ebon hand'/'MED', '159193').

card_in_set('oubliette', 'MED').
card_original_type('oubliette'/'MED', 'Enchantment').
card_original_text('oubliette'/'MED', 'When Oubliette comes into play, remove target creature and all Auras attached to it from the game. Note the number and kind of counters that were on that creature.\nWhen Oubliette leaves play, return the removed card to play under its owner\'s control tapped with the noted number and kind of counters on it. If you do, return the removed Aura cards to play under their owner\'s control attached to that permanent.').
card_image_name('oubliette'/'MED', 'oubliette').
card_uid('oubliette'/'MED', 'MED:Oubliette:oubliette').
card_rarity('oubliette'/'MED', 'Common').
card_artist('oubliette'/'MED', 'Douglas Shuler').
card_number('oubliette'/'MED', '79').
card_multiverse_id('oubliette'/'MED', '159135').

card_in_set('paralyze', 'MED').
card_original_type('paralyze'/'MED', 'Enchantment — Aura').
card_original_text('paralyze'/'MED', 'Enchant creature\nWhen Paralyze comes into play, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {4}. If he or she does, untap the creature.').
card_image_name('paralyze'/'MED', 'paralyze').
card_uid('paralyze'/'MED', 'MED:Paralyze:paralyze').
card_rarity('paralyze'/'MED', 'Common').
card_artist('paralyze'/'MED', 'Anson Maddocks').
card_number('paralyze'/'MED', '80').
card_multiverse_id('paralyze'/'MED', '159267').

card_in_set('petra sphinx', 'MED').
card_original_type('petra sphinx'/'MED', 'Creature — Sphinx').
card_original_text('petra sphinx'/'MED', '{T}: Target player names a card, then reveals the top card of his or her library. If that card is the named card, that player puts it into his or her hand. If it isn\'t, the player puts it into his or her graveyard.').
card_image_name('petra sphinx'/'MED', 'petra sphinx').
card_uid('petra sphinx'/'MED', 'MED:Petra Sphinx:petra sphinx').
card_rarity('petra sphinx'/'MED', 'Rare').
card_artist('petra sphinx'/'MED', 'Sandra Everingham').
card_number('petra sphinx'/'MED', '23').
card_flavor_text('petra sphinx'/'MED', 'What walks on four legs in the morning, two legs in the afternoon, and three legs in the evening?').
card_multiverse_id('petra sphinx'/'MED', '159309').

card_in_set('phantom monster', 'MED').
card_original_type('phantom monster'/'MED', 'Creature — Illusion').
card_original_text('phantom monster'/'MED', 'Flying').
card_image_name('phantom monster'/'MED', 'phantom monster').
card_uid('phantom monster'/'MED', 'MED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'MED', 'Common').
card_artist('phantom monster'/'MED', 'Jesper Myrfors').
card_number('phantom monster'/'MED', '43').
card_flavor_text('phantom monster'/'MED', '\"While, like a ghastly rapid river,\nThrough the pale door\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'MED', '159268').

card_in_set('phelddagrif', 'MED').
card_original_type('phelddagrif'/'MED', 'Legendary Creature — Phelddagrif').
card_original_text('phelddagrif'/'MED', '{G}: Phelddagrif gains trample until end of turn. Target opponent puts a 1/1 green Hippo creature token into play.\n{W}: Phelddagrif gains flying until end of turn. Target opponent gains 2 life.\n{U}: Return Phelddagrif to its owner\'s hand. Target opponent may draw a card.').
card_image_name('phelddagrif'/'MED', 'phelddagrif').
card_uid('phelddagrif'/'MED', 'MED:Phelddagrif:phelddagrif').
card_rarity('phelddagrif'/'MED', 'Rare').
card_artist('phelddagrif'/'MED', 'Amy Weber').
card_number('phelddagrif'/'MED', '150').
card_multiverse_id('phelddagrif'/'MED', '159097').

card_in_set('phyrexian boon', 'MED').
card_original_type('phyrexian boon'/'MED', 'Enchantment — Aura').
card_original_text('phyrexian boon'/'MED', 'Enchant creature\nEnchanted creature gets +2/+1 as long as it\'s black. Otherwise, it gets -1/-2.').
card_image_name('phyrexian boon'/'MED', 'phyrexian boon').
card_uid('phyrexian boon'/'MED', 'MED:Phyrexian Boon:phyrexian boon').
card_rarity('phyrexian boon'/'MED', 'Common').
card_artist('phyrexian boon'/'MED', 'Mark Tedin').
card_number('phyrexian boon'/'MED', '81').
card_flavor_text('phyrexian boon'/'MED', '\"Phyrexia\'s touch is painful to all but the blackest of hearts.\"\n—Gerda Äagesdotter,\nArchmage of the Unseen').
card_multiverse_id('phyrexian boon'/'MED', '159741').

card_in_set('phyrexian war beast', 'MED').
card_original_type('phyrexian war beast'/'MED', 'Artifact Creature — Beast').
card_original_text('phyrexian war beast'/'MED', 'When Phyrexian War Beast leaves play, sacrifice a land and Phyrexian War Beast deals 1 damage to you.').
card_image_name('phyrexian war beast'/'MED', 'phyrexian war beast').
card_uid('phyrexian war beast'/'MED', 'MED:Phyrexian War Beast:phyrexian war beast').
card_rarity('phyrexian war beast'/'MED', 'Uncommon').
card_artist('phyrexian war beast'/'MED', 'Bill Sienkiewicz').
card_number('phyrexian war beast'/'MED', '162').
card_flavor_text('phyrexian war beast'/'MED', '\"Knowing its origins, how could they have thought they could control it?\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('phyrexian war beast'/'MED', '159098').

card_in_set('plains', 'MED').
card_original_type('plains'/'MED', 'Basic Land — Plains').
card_original_text('plains'/'MED', '').
card_image_name('plains'/'MED', 'plains1').
card_uid('plains'/'MED', 'MED:Plains:plains1').
card_rarity('plains'/'MED', 'Basic Land').
card_artist('plains'/'MED', 'Jesper Myrfors').
card_number('plains'/'MED', '181').
card_multiverse_id('plains'/'MED', '159287').

card_in_set('plains', 'MED').
card_original_type('plains'/'MED', 'Basic Land — Plains').
card_original_text('plains'/'MED', '').
card_image_name('plains'/'MED', 'plains2').
card_uid('plains'/'MED', 'MED:Plains:plains2').
card_rarity('plains'/'MED', 'Basic Land').
card_artist('plains'/'MED', 'Jesper Myrfors').
card_number('plains'/'MED', '182').
card_multiverse_id('plains'/'MED', '159288').

card_in_set('plains', 'MED').
card_original_type('plains'/'MED', 'Basic Land — Plains').
card_original_text('plains'/'MED', '').
card_image_name('plains'/'MED', 'plains3').
card_uid('plains'/'MED', 'MED:Plains:plains3').
card_rarity('plains'/'MED', 'Basic Land').
card_artist('plains'/'MED', 'Jesper Myrfors').
card_number('plains'/'MED', '183').
card_multiverse_id('plains'/'MED', '159289').

card_in_set('polar kraken', 'MED').
card_original_type('polar kraken'/'MED', 'Creature — Kraken').
card_original_text('polar kraken'/'MED', 'Trample\nPolar Kraken comes into play tapped.\nCumulative upkeep—Sacrifice a land.').
card_image_name('polar kraken'/'MED', 'polar kraken').
card_uid('polar kraken'/'MED', 'MED:Polar Kraken:polar kraken').
card_rarity('polar kraken'/'MED', 'Rare').
card_artist('polar kraken'/'MED', 'Mark Tedin').
card_number('polar kraken'/'MED', '44').
card_flavor_text('polar kraken'/'MED', '\"It was big. Really, really, big. No, bigger than that. It was big!\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('polar kraken'/'MED', '159241').

card_in_set('pox', 'MED').
card_original_type('pox'/'MED', 'Sorcery').
card_original_text('pox'/'MED', 'Each player loses a third of his or her life, then discards a third of the cards in his or her hand, then sacrifices a third of the creatures he or she controls, then sacrifices a third of the lands he or she controls. Round up each time.').
card_image_name('pox'/'MED', 'pox').
card_uid('pox'/'MED', 'MED:Pox:pox').
card_rarity('pox'/'MED', 'Rare').
card_artist('pox'/'MED', 'Cornelius Brudi').
card_number('pox'/'MED', '82').
card_multiverse_id('pox'/'MED', '159242').

card_in_set('preacher', 'MED').
card_original_type('preacher'/'MED', 'Creature — Human Cleric').
card_original_text('preacher'/'MED', 'You may choose not to untap Preacher during your untap step.\n{T}: Gain control of target creature of an opponent\'s choice that he or she controls as long as Preacher remains tapped.').
card_image_name('preacher'/'MED', 'preacher').
card_uid('preacher'/'MED', 'MED:Preacher:preacher').
card_rarity('preacher'/'MED', 'Rare').
card_artist('preacher'/'MED', 'Quinton Hoover').
card_number('preacher'/'MED', '24').
card_multiverse_id('preacher'/'MED', '159153').

card_in_set('primal order', 'MED').
card_original_type('primal order'/'MED', 'Enchantment').
card_original_text('primal order'/'MED', 'At the beginning of each player\'s upkeep, Primal Order deals damage to that player equal to the number of nonbasic lands he or she controls.').
card_image_name('primal order'/'MED', 'primal order').
card_uid('primal order'/'MED', 'MED:Primal Order:primal order').
card_rarity('primal order'/'MED', 'Rare').
card_artist('primal order'/'MED', 'Rob Alexander').
card_number('primal order'/'MED', '125').
card_flavor_text('primal order'/'MED', '\"It\'s strange, but I can feel it when the land is corrupted. I understand why the Autumn Willow is so protective.\"\n—Daria').
card_multiverse_id('primal order'/'MED', '159215').

card_in_set('psychic purge', 'MED').
card_original_type('psychic purge'/'MED', 'Sorcery').
card_original_text('psychic purge'/'MED', 'Psychic Purge deals 1 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Psychic Purge, that player loses 5 life.').
card_image_name('psychic purge'/'MED', 'psychic purge').
card_uid('psychic purge'/'MED', 'MED:Psychic Purge:psychic purge').
card_rarity('psychic purge'/'MED', 'Uncommon').
card_artist('psychic purge'/'MED', 'Susan Van Camp').
card_number('psychic purge'/'MED', '45').
card_multiverse_id('psychic purge'/'MED', '159311').

card_in_set('psychic venom', 'MED').
card_original_type('psychic venom'/'MED', 'Enchantment — Aura').
card_original_text('psychic venom'/'MED', 'Enchant land\nWhenever enchanted land becomes tapped, Psychic Venom deals 2 damage to that land\'s controller.').
card_image_name('psychic venom'/'MED', 'psychic venom').
card_uid('psychic venom'/'MED', 'MED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'MED', 'Common').
card_artist('psychic venom'/'MED', 'Brian Snõddy').
card_number('psychic venom'/'MED', '46').
card_multiverse_id('psychic venom'/'MED', '159269').

card_in_set('pyroblast', 'MED').
card_original_type('pyroblast'/'MED', 'Instant').
card_original_text('pyroblast'/'MED', 'Choose one Counter target spell if it\'s blue; or destroy target permanent if it\'s blue.').
card_image_name('pyroblast'/'MED', 'pyroblast').
card_uid('pyroblast'/'MED', 'MED:Pyroblast:pyroblast').
card_rarity('pyroblast'/'MED', 'Common').
card_artist('pyroblast'/'MED', 'Kaja Foglio').
card_number('pyroblast'/'MED', '107').
card_flavor_text('pyroblast'/'MED', '\"Just the thing for those pesky water mages.\"\n—Jaya Ballard, task mage').
card_multiverse_id('pyroblast'/'MED', '159243').

card_in_set('rabid wombat', 'MED').
card_original_type('rabid wombat'/'MED', 'Creature — Wombat').
card_original_text('rabid wombat'/'MED', 'Vigilance\nRabid Wombat gets +2/+2 for each Aura attached to it.').
card_image_name('rabid wombat'/'MED', 'rabid wombat').
card_uid('rabid wombat'/'MED', 'MED:Rabid Wombat:rabid wombat').
card_rarity('rabid wombat'/'MED', 'Uncommon').
card_artist('rabid wombat'/'MED', 'Kaja Foglio').
card_number('rabid wombat'/'MED', '126').
card_multiverse_id('rabid wombat'/'MED', '159312').

card_in_set('rainbow vale', 'MED').
card_original_type('rainbow vale'/'MED', 'Land').
card_original_text('rainbow vale'/'MED', '{T}: Add one mana of any color to your mana pool. An opponent gains control of Rainbow Vale at end of turn.').
card_image_name('rainbow vale'/'MED', 'rainbow vale').
card_uid('rainbow vale'/'MED', 'MED:Rainbow Vale:rainbow vale').
card_rarity('rainbow vale'/'MED', 'Rare').
card_artist('rainbow vale'/'MED', 'Kaja Foglio').
card_number('rainbow vale'/'MED', '179').
card_flavor_text('rainbow vale'/'MED', 'In the feudal days of Icatia, finding the Rainbow Vale was often the goal of knights\' quests.').
card_multiverse_id('rainbow vale'/'MED', '159734').

card_in_set('righteous avengers', 'MED').
card_original_type('righteous avengers'/'MED', 'Creature — Human Soldier').
card_original_text('righteous avengers'/'MED', 'Plainswalk').
card_image_name('righteous avengers'/'MED', 'righteous avengers').
card_uid('righteous avengers'/'MED', 'MED:Righteous Avengers:righteous avengers').
card_rarity('righteous avengers'/'MED', 'Common').
card_artist('righteous avengers'/'MED', 'Heather Hudson').
card_number('righteous avengers'/'MED', '25').
card_flavor_text('righteous avengers'/'MED', 'Few can withstand the wrath of the righteous.').
card_multiverse_id('righteous avengers'/'MED', '159314').

card_in_set('ring of ma\'rûf', 'MED').
card_original_type('ring of ma\'rûf'/'MED', 'Artifact').
card_original_text('ring of ma\'rûf'/'MED', '{5}, {T}, Remove Ring of Ma\'rûf from the game: The next time you would draw a card this turn, instead choose a card you own from outside the game and put it into your hand.').
card_image_name('ring of ma\'rûf'/'MED', 'ring of ma\'ruf').
card_uid('ring of ma\'rûf'/'MED', 'MED:Ring of Ma\'rûf:ring of ma\'ruf').
card_rarity('ring of ma\'rûf'/'MED', 'Rare').
card_artist('ring of ma\'rûf'/'MED', 'Dan Frazier').
card_number('ring of ma\'rûf'/'MED', '163').
card_multiverse_id('ring of ma\'rûf'/'MED', '159136').

card_in_set('river merfolk', 'MED').
card_original_type('river merfolk'/'MED', 'Creature — Merfolk').
card_original_text('river merfolk'/'MED', '{U}: River Merfolk gains mountainwalk until end of turn.').
card_image_name('river merfolk'/'MED', 'river merfolk').
card_uid('river merfolk'/'MED', 'MED:River Merfolk:river merfolk').
card_rarity('river merfolk'/'MED', 'Common').
card_artist('river merfolk'/'MED', 'Douglas Shuler').
card_number('river merfolk'/'MED', '47').
card_flavor_text('river merfolk'/'MED', '\"Dwelling in icy mountain streams near goblin and orcish foes, the river merfolk were known for their stoicism.\"\n—Sarpadian Empires, vol. V').
card_multiverse_id('river merfolk'/'MED', '159822').

card_in_set('roots', 'MED').
card_original_type('roots'/'MED', 'Enchantment — Aura').
card_original_text('roots'/'MED', 'Enchant creature without flying\nWhen Roots comes into play, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('roots'/'MED', 'roots').
card_uid('roots'/'MED', 'MED:Roots:roots').
card_rarity('roots'/'MED', 'Common').
card_artist('roots'/'MED', 'Nicola Leonard').
card_number('roots'/'MED', '127').
card_flavor_text('roots'/'MED', '\"That which nourishes can also bind.\"\n—Gemma, Willow Priestess').
card_multiverse_id('roots'/'MED', '159218').

card_in_set('scryb sprites', 'MED').
card_original_type('scryb sprites'/'MED', 'Creature — Faerie').
card_original_text('scryb sprites'/'MED', 'Flying').
card_image_name('scryb sprites'/'MED', 'scryb sprites').
card_uid('scryb sprites'/'MED', 'MED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'MED', 'Common').
card_artist('scryb sprites'/'MED', 'Amy Weber').
card_number('scryb sprites'/'MED', '128').
card_flavor_text('scryb sprites'/'MED', 'The only sound was the gentle clicking of the faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'MED', '159270').

card_in_set('sea sprite', 'MED').
card_original_type('sea sprite'/'MED', 'Creature — Faerie').
card_original_text('sea sprite'/'MED', 'Flying, protection from red').
card_image_name('sea sprite'/'MED', 'sea sprite').
card_uid('sea sprite'/'MED', 'MED:Sea Sprite:sea sprite').
card_rarity('sea sprite'/'MED', 'Common').
card_artist('sea sprite'/'MED', 'Susan Van Camp').
card_number('sea sprite'/'MED', '48').
card_flavor_text('sea sprite'/'MED', '\"No one can catch what won\'t be caught.\"\n—Kakra, sea troll').
card_multiverse_id('sea sprite'/'MED', '159219').

card_in_set('seasinger', 'MED').
card_original_type('seasinger'/'MED', 'Creature — Merfolk').
card_original_text('seasinger'/'MED', 'When you control no Islands, sacrifice Seasinger.\nYou may choose not to untap Seasinger during your untap step.\n{T}: Gain control of target creature whose controller controls an Island as long as you control Seasinger and as long as Seasinger remains tapped.').
card_image_name('seasinger'/'MED', 'seasinger').
card_uid('seasinger'/'MED', 'MED:Seasinger:seasinger').
card_rarity('seasinger'/'MED', 'Uncommon').
card_artist('seasinger'/'MED', 'Amy Weber').
card_number('seasinger'/'MED', '49').
card_multiverse_id('seasinger'/'MED', '159845').

card_in_set('seraph', 'MED').
card_original_type('seraph'/'MED', 'Creature — Angel').
card_original_text('seraph'/'MED', 'Flying\nWhenever a creature dealt damage by Seraph this turn is put into a graveyard, put that card into play under your control at end of turn. Sacrifice the creature when you lose control of Seraph.').
card_image_name('seraph'/'MED', 'seraph').
card_uid('seraph'/'MED', 'MED:Seraph:seraph').
card_rarity('seraph'/'MED', 'Rare').
card_artist('seraph'/'MED', 'Christopher Rush').
card_number('seraph'/'MED', '26').
card_multiverse_id('seraph'/'MED', '159245').

card_in_set('serendib efreet', 'MED').
card_original_type('serendib efreet'/'MED', 'Creature — Efreet').
card_original_text('serendib efreet'/'MED', 'Flying\nAt the beginning of your upkeep, Serendib Efreet deals 1 damage to you.').
card_image_name('serendib efreet'/'MED', 'serendib efreet').
card_uid('serendib efreet'/'MED', 'MED:Serendib Efreet:serendib efreet').
card_rarity('serendib efreet'/'MED', 'Rare').
card_artist('serendib efreet'/'MED', 'Anson Maddocks').
card_number('serendib efreet'/'MED', '50').
card_multiverse_id('serendib efreet'/'MED', '159137').

card_in_set('serpent generator', 'MED').
card_original_type('serpent generator'/'MED', 'Artifact').
card_original_text('serpent generator'/'MED', '{4}, {T}: Put a 1/1 Snake artifact creature token into play. This creature has \"Whenever this creature deals damage to a player, that player gets a poison counter.\" (A player with ten or more poison counters loses the game.)').
card_image_name('serpent generator'/'MED', 'serpent generator').
card_uid('serpent generator'/'MED', 'MED:Serpent Generator:serpent generator').
card_rarity('serpent generator'/'MED', 'Rare').
card_artist('serpent generator'/'MED', 'Mark Tedin').
card_number('serpent generator'/'MED', '164').
card_multiverse_id('serpent generator'/'MED', '159826').

card_in_set('shambling strider', 'MED').
card_original_type('shambling strider'/'MED', 'Creature — Yeti').
card_original_text('shambling strider'/'MED', '{R}{G}: Shambling Strider gets +1/-1 until end of turn.').
card_image_name('shambling strider'/'MED', 'shambling strider').
card_uid('shambling strider'/'MED', 'MED:Shambling Strider:shambling strider').
card_rarity('shambling strider'/'MED', 'Common').
card_artist('shambling strider'/'MED', 'Douglas Shuler').
card_number('shambling strider'/'MED', '129').
card_flavor_text('shambling strider'/'MED', 'Freyalise forbid that any stranger should wander into the striders\' territory.').
card_multiverse_id('shambling strider'/'MED', '159246').

card_in_set('shield of the ages', 'MED').
card_original_type('shield of the ages'/'MED', 'Artifact').
card_original_text('shield of the ages'/'MED', '{2}: Prevent the next 1 damage that would be dealt to you this turn.').
card_image_name('shield of the ages'/'MED', 'shield of the ages').
card_uid('shield of the ages'/'MED', 'MED:Shield of the Ages:shield of the ages').
card_rarity('shield of the ages'/'MED', 'Uncommon').
card_artist('shield of the ages'/'MED', 'Anson Maddocks').
card_number('shield of the ages'/'MED', '165').
card_flavor_text('shield of the ages'/'MED', '\"This shield is a true rarity: an artifact whose purpose is obvious.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('shield of the ages'/'MED', '159839').

card_in_set('shield sphere', 'MED').
card_original_type('shield sphere'/'MED', 'Artifact Creature — Wall').
card_original_text('shield sphere'/'MED', 'Defender\nWhenever Shield Sphere blocks, put a -0/-1 counter on it.').
card_image_name('shield sphere'/'MED', 'shield sphere').
card_uid('shield sphere'/'MED', 'MED:Shield Sphere:shield sphere').
card_rarity('shield sphere'/'MED', 'Common').
card_artist('shield sphere'/'MED', 'Alan Rabinowitz').
card_number('shield sphere'/'MED', '166').
card_flavor_text('shield sphere'/'MED', '\"My soldiers know that they need never fear for their protection.\"\n—King Darien of Kjeldor').
card_multiverse_id('shield sphere'/'MED', '159103').

card_in_set('singing tree', 'MED').
card_original_type('singing tree'/'MED', 'Creature — Plant').
card_original_text('singing tree'/'MED', '{T}: Target attacking creature\'s power becomes 0 until end of turn.').
card_image_name('singing tree'/'MED', 'singing tree').
card_uid('singing tree'/'MED', 'MED:Singing Tree:singing tree').
card_rarity('singing tree'/'MED', 'Uncommon').
card_artist('singing tree'/'MED', 'Rob Alexander').
card_number('singing tree'/'MED', '130').
card_multiverse_id('singing tree'/'MED', '159765').

card_in_set('spectral bears', 'MED').
card_original_type('spectral bears'/'MED', 'Creature — Bear Spirit').
card_original_text('spectral bears'/'MED', 'Whenever Spectral Bears attacks, if defending player controls no black nontoken permanents, it doesn\'t untap during your next untap step.').
card_image_name('spectral bears'/'MED', 'spectral bears').
card_uid('spectral bears'/'MED', 'MED:Spectral Bears:spectral bears').
card_rarity('spectral bears'/'MED', 'Uncommon').
card_artist('spectral bears'/'MED', 'Pat Morrissey').
card_number('spectral bears'/'MED', '131').
card_flavor_text('spectral bears'/'MED', '\"I hear there are bears—or spirits—that guard caravans passing through the forest.\"\n—Gulsen, abbey matron').
card_multiverse_id('spectral bears'/'MED', '159224').

card_in_set('spinal villain', 'MED').
card_original_type('spinal villain'/'MED', 'Creature — Beast').
card_original_text('spinal villain'/'MED', '{T}: Destroy target blue creature.').
card_image_name('spinal villain'/'MED', 'spinal villain').
card_uid('spinal villain'/'MED', 'MED:Spinal Villain:spinal villain').
card_rarity('spinal villain'/'MED', 'Uncommon').
card_artist('spinal villain'/'MED', 'Anson Maddocks').
card_number('spinal villain'/'MED', '108').
card_flavor_text('spinal villain'/'MED', '\"Striking silent as a dream,\nCutting short the strangled scream . . .\"\n—Tobrian, \"Watchdragon\"').
card_multiverse_id('spinal villain'/'MED', '159315').

card_in_set('stone calendar', 'MED').
card_original_type('stone calendar'/'MED', 'Artifact').
card_original_text('stone calendar'/'MED', 'Spells you play cost up to {1} less to play.').
card_image_name('stone calendar'/'MED', 'stone calendar').
card_uid('stone calendar'/'MED', 'MED:Stone Calendar:stone calendar').
card_rarity('stone calendar'/'MED', 'Uncommon').
card_artist('stone calendar'/'MED', 'Amy Weber').
card_number('stone calendar'/'MED', '167').
card_flavor_text('stone calendar'/'MED', 'The Pretender Mairsil ordered a great calendar drawn up to show when the paths to the Dark Lands were strongest.').
card_multiverse_id('stone calendar'/'MED', '159154').

card_in_set('stone giant', 'MED').
card_original_type('stone giant'/'MED', 'Creature — Giant').
card_original_text('stone giant'/'MED', '{T}: Target creature you control with toughness less than Stone Giant\'s power gains flying until end of turn. Destroy that creature at end of turn.').
card_image_name('stone giant'/'MED', 'stone giant').
card_uid('stone giant'/'MED', 'MED:Stone Giant:stone giant').
card_rarity('stone giant'/'MED', 'Uncommon').
card_artist('stone giant'/'MED', 'Dameon Willich').
card_number('stone giant'/'MED', '109').
card_flavor_text('stone giant'/'MED', 'What goes up, must come down.').
card_multiverse_id('stone giant'/'MED', '159274').

card_in_set('storm seeker', 'MED').
card_original_type('storm seeker'/'MED', 'Instant').
card_original_text('storm seeker'/'MED', 'Storm Seeker deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('storm seeker'/'MED', 'storm seeker').
card_uid('storm seeker'/'MED', 'MED:Storm Seeker:storm seeker').
card_rarity('storm seeker'/'MED', 'Uncommon').
card_artist('storm seeker'/'MED', 'Mark Poole').
card_number('storm seeker'/'MED', '132').
card_multiverse_id('storm seeker'/'MED', '159316').

card_in_set('su-chi', 'MED').
card_original_type('su-chi'/'MED', 'Artifact Creature — Construct').
card_original_text('su-chi'/'MED', 'When Su-Chi is put into a graveyard from play, add {4} to your mana pool.').
card_image_name('su-chi'/'MED', 'su-chi').
card_uid('su-chi'/'MED', 'MED:Su-Chi:su-chi').
card_rarity('su-chi'/'MED', 'Rare').
card_artist('su-chi'/'MED', 'Christopher Rush').
card_number('su-chi'/'MED', '168').
card_flavor_text('su-chi'/'MED', 'Flawed copies of relics from the Thran Empire, the su-chi were inherently unstable but provided useful knowledge for Tocasia\'s students.').
card_multiverse_id('su-chi'/'MED', '159119').

card_in_set('sunken city', 'MED').
card_original_type('sunken city'/'MED', 'Enchantment').
card_original_text('sunken city'/'MED', 'At the beginning of your upkeep, sacrifice Sunken City unless you pay {U}{U}.\nBlue creatures get +1/+1.').
card_image_name('sunken city'/'MED', 'sunken city').
card_uid('sunken city'/'MED', 'MED:Sunken City:sunken city').
card_rarity('sunken city'/'MED', 'Uncommon').
card_artist('sunken city'/'MED', 'Jesper Myrfors').
card_number('sunken city'/'MED', '51').
card_multiverse_id('sunken city'/'MED', '159155').

card_in_set('swamp', 'MED').
card_original_type('swamp'/'MED', 'Basic Land — Swamp').
card_original_text('swamp'/'MED', '').
card_image_name('swamp'/'MED', 'swamp1').
card_uid('swamp'/'MED', 'MED:Swamp:swamp1').
card_rarity('swamp'/'MED', 'Basic Land').
card_artist('swamp'/'MED', 'Dan Frazier').
card_number('swamp'/'MED', '187').
card_multiverse_id('swamp'/'MED', '159290').

card_in_set('swamp', 'MED').
card_original_type('swamp'/'MED', 'Basic Land — Swamp').
card_original_text('swamp'/'MED', '').
card_image_name('swamp'/'MED', 'swamp2').
card_uid('swamp'/'MED', 'MED:Swamp:swamp2').
card_rarity('swamp'/'MED', 'Basic Land').
card_artist('swamp'/'MED', 'Dan Frazier').
card_number('swamp'/'MED', '188').
card_multiverse_id('swamp'/'MED', '159291').

card_in_set('swamp', 'MED').
card_original_type('swamp'/'MED', 'Basic Land — Swamp').
card_original_text('swamp'/'MED', '').
card_image_name('swamp'/'MED', 'swamp3').
card_uid('swamp'/'MED', 'MED:Swamp:swamp3').
card_rarity('swamp'/'MED', 'Basic Land').
card_artist('swamp'/'MED', 'Dan Frazier').
card_number('swamp'/'MED', '189').
card_multiverse_id('swamp'/'MED', '159292').

card_in_set('sylvan library', 'MED').
card_original_type('sylvan library'/'MED', 'Enchantment').
card_original_text('sylvan library'/'MED', 'At the beginning of your draw step, you may draw two cards. If you do, choose two cards in your hand drawn this turn. For each of those cards, pay 4 life or put the card on top of your library.').
card_image_name('sylvan library'/'MED', 'sylvan library').
card_uid('sylvan library'/'MED', 'MED:Sylvan Library:sylvan library').
card_rarity('sylvan library'/'MED', 'Rare').
card_artist('sylvan library'/'MED', 'Harold McNeill').
card_number('sylvan library'/'MED', '133').
card_multiverse_id('sylvan library'/'MED', '159317').

card_in_set('tawnos\'s coffin', 'MED').
card_original_type('tawnos\'s coffin'/'MED', 'Artifact').
card_original_text('tawnos\'s coffin'/'MED', 'You may choose not to untap Tawnos\'s Coffin during your untap step.\n{3}, {T}: Remove target creature and all Auras attached to it from the game. Note the number and kind of counters that were on that creature. When Tawnos\'s Coffin leaves play or becomes untapped, return the removed card to play under its owner\'s control tapped with the noted number and kind of counters on it, and if you do, return the removed Aura cards to play under their owner\'s control attached to that permanent.').
card_image_name('tawnos\'s coffin'/'MED', 'tawnos\'s coffin').
card_uid('tawnos\'s coffin'/'MED', 'MED:Tawnos\'s Coffin:tawnos\'s coffin').
card_rarity('tawnos\'s coffin'/'MED', 'Rare').
card_artist('tawnos\'s coffin'/'MED', 'Christopher Rush').
card_number('tawnos\'s coffin'/'MED', '169').
card_multiverse_id('tawnos\'s coffin'/'MED', '159120').

card_in_set('telekinesis', 'MED').
card_original_type('telekinesis'/'MED', 'Instant').
card_original_text('telekinesis'/'MED', 'Tap target creature. Prevent all combat damage that would be dealt by that creature this turn. It doesn\'t untap during its controller\'s next two untap steps.').
card_image_name('telekinesis'/'MED', 'telekinesis').
card_uid('telekinesis'/'MED', 'MED:Telekinesis:telekinesis').
card_rarity('telekinesis'/'MED', 'Common').
card_artist('telekinesis'/'MED', 'Daniel Gelon').
card_number('telekinesis'/'MED', '52').
card_multiverse_id('telekinesis'/'MED', '159774').

card_in_set('thawing glaciers', 'MED').
card_original_type('thawing glaciers'/'MED', 'Land').
card_original_text('thawing glaciers'/'MED', 'Thawing Glaciers comes into play tapped.\n{1}, {T}: Search your library for a basic land card, put that card into play tapped, then shuffle your library. Thawing Glaciers gains substance until end of turn. Return Thawing Glaciers to its owner\'s hand when it loses substance.').
card_image_name('thawing glaciers'/'MED', 'thawing glaciers').
card_uid('thawing glaciers'/'MED', 'MED:Thawing Glaciers:thawing glaciers').
card_rarity('thawing glaciers'/'MED', 'Rare').
card_artist('thawing glaciers'/'MED', 'Jeff A. Menges').
card_number('thawing glaciers'/'MED', '180').
card_multiverse_id('thawing glaciers'/'MED', '159106').

card_in_set('the fallen', 'MED').
card_original_type('the fallen'/'MED', 'Creature — Zombie').
card_original_text('the fallen'/'MED', 'At the beginning of your upkeep, The Fallen deals 1 damage to each opponent it has dealt damage to this game.').
card_image_name('the fallen'/'MED', 'the fallen').
card_uid('the fallen'/'MED', 'MED:The Fallen:the fallen').
card_rarity('the fallen'/'MED', 'Uncommon').
card_artist('the fallen'/'MED', 'Jesper Myrfors').
card_number('the fallen'/'MED', '69').
card_flavor_text('the fallen'/'MED', 'Magic often masters those who cannot master it.').
card_multiverse_id('the fallen'/'MED', '159156').

card_in_set('thicket basilisk', 'MED').
card_original_type('thicket basilisk'/'MED', 'Creature — Basilisk').
card_original_text('thicket basilisk'/'MED', 'Whenever Thicket Basilisk blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_image_name('thicket basilisk'/'MED', 'thicket basilisk').
card_uid('thicket basilisk'/'MED', 'MED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'MED', 'Uncommon').
card_artist('thicket basilisk'/'MED', 'Dan Frazier').
card_number('thicket basilisk'/'MED', '134').
card_flavor_text('thicket basilisk'/'MED', 'Moss-covered statues littered the area, a macabre monument to the basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'MED', '159275').

card_in_set('thorn thallid', 'MED').
card_original_type('thorn thallid'/'MED', 'Creature — Fungus').
card_original_text('thorn thallid'/'MED', 'At the beginning of your upkeep, put a spore counter on Thorn Thallid.\nRemove three spore counters from Thorn Thallid: Thorn Thallid deals 1 damage to target creature or player.').
card_image_name('thorn thallid'/'MED', 'thorn thallid').
card_uid('thorn thallid'/'MED', 'MED:Thorn Thallid:thorn thallid').
card_rarity('thorn thallid'/'MED', 'Common').
card_artist('thorn thallid'/'MED', 'Mark Tedin').
card_number('thorn thallid'/'MED', '135').
card_flavor_text('thorn thallid'/'MED', '\"The danger in cultivating massive plants caught the elves by surprise.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('thorn thallid'/'MED', '159196').

card_in_set('thrull champion', 'MED').
card_original_type('thrull champion'/'MED', 'Creature — Thrull').
card_original_text('thrull champion'/'MED', 'Thrull creatures get +1/+1.\n{T}: Gain control of target Thrull as long as you control Thrull Champion.').
card_image_name('thrull champion'/'MED', 'thrull champion').
card_uid('thrull champion'/'MED', 'MED:Thrull Champion:thrull champion').
card_rarity('thrull champion'/'MED', 'Rare').
card_artist('thrull champion'/'MED', 'Daniel Gelon').
card_number('thrull champion'/'MED', '83').
card_flavor_text('thrull champion'/'MED', '\"Those idiots should never have bred thrulls for combat!\"\n—Jherana Rure').
card_multiverse_id('thrull champion'/'MED', '159200').

card_in_set('thrull retainer', 'MED').
card_original_type('thrull retainer'/'MED', 'Enchantment — Aura').
card_original_text('thrull retainer'/'MED', 'Enchant creature\nEnchanted creature gets +1/+1.\nSacrifice Thrull Retainer: Regenerate enchanted creature.').
card_image_name('thrull retainer'/'MED', 'thrull retainer').
card_uid('thrull retainer'/'MED', 'MED:Thrull Retainer:thrull retainer').
card_rarity('thrull retainer'/'MED', 'Common').
card_artist('thrull retainer'/'MED', 'Ron Spencer').
card_number('thrull retainer'/'MED', '84').
card_flavor_text('thrull retainer'/'MED', '\"Until the Rebellion, thrulls served their masters faithfully—even at the cost of their own lives.\"\n—Sarpadian Empires, vol. II').
card_multiverse_id('thrull retainer'/'MED', '159201').

card_in_set('thunder spirit', 'MED').
card_original_type('thunder spirit'/'MED', 'Creature — Elemental Spirit').
card_original_text('thunder spirit'/'MED', 'Flying, first strike').
card_image_name('thunder spirit'/'MED', 'thunder spirit').
card_uid('thunder spirit'/'MED', 'MED:Thunder Spirit:thunder spirit').
card_rarity('thunder spirit'/'MED', 'Uncommon').
card_artist('thunder spirit'/'MED', 'Randy Asplund-Faith').
card_number('thunder spirit'/'MED', '27').
card_multiverse_id('thunder spirit'/'MED', '159318').

card_in_set('time elemental', 'MED').
card_original_type('time elemental'/'MED', 'Creature — Elemental').
card_original_text('time elemental'/'MED', 'When Time Elemental attacks or blocks, at end of combat, sacrifice it and it deals 5 damage to you.\n{2}{U}{U}, {T}: Return target permanent that isn\'t enchanted to its owner\'s hand.').
card_image_name('time elemental'/'MED', 'time elemental').
card_uid('time elemental'/'MED', 'MED:Time Elemental:time elemental').
card_rarity('time elemental'/'MED', 'Rare').
card_artist('time elemental'/'MED', 'Amy Weber').
card_number('time elemental'/'MED', '53').
card_multiverse_id('time elemental'/'MED', '159775').

card_in_set('tivadar\'s crusade', 'MED').
card_original_type('tivadar\'s crusade'/'MED', 'Sorcery').
card_original_text('tivadar\'s crusade'/'MED', 'Destroy all Goblins.').
card_image_name('tivadar\'s crusade'/'MED', 'tivadar\'s crusade').
card_uid('tivadar\'s crusade'/'MED', 'MED:Tivadar\'s Crusade:tivadar\'s crusade').
card_rarity('tivadar\'s crusade'/'MED', 'Uncommon').
card_artist('tivadar\'s crusade'/'MED', 'Dennis Detwiller').
card_number('tivadar\'s crusade'/'MED', '28').
card_multiverse_id('tivadar\'s crusade'/'MED', '159829').

card_in_set('tornado', 'MED').
card_original_type('tornado'/'MED', 'Enchantment').
card_original_text('tornado'/'MED', 'Cumulative upkeep {G}\n{2}{G}, Pay 3 life for each velocity counter on Tornado: Destroy target permanent and put a velocity counter on Tornado. Play this ability only once each turn.').
card_image_name('tornado'/'MED', 'tornado').
card_uid('tornado'/'MED', 'MED:Tornado:tornado').
card_rarity('tornado'/'MED', 'Rare').
card_artist('tornado'/'MED', 'Susan Van Camp').
card_number('tornado'/'MED', '136').
card_multiverse_id('tornado'/'MED', '159833').

card_in_set('urza\'s bauble', 'MED').
card_original_type('urza\'s bauble'/'MED', 'Artifact').
card_original_text('urza\'s bauble'/'MED', '{T}, Sacrifice Urza\'s Bauble: Look at a card at random in target player\'s hand. You draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('urza\'s bauble'/'MED', 'urza\'s bauble').
card_uid('urza\'s bauble'/'MED', 'MED:Urza\'s Bauble:urza\'s bauble').
card_rarity('urza\'s bauble'/'MED', 'Uncommon').
card_artist('urza\'s bauble'/'MED', 'Christopher Rush').
card_number('urza\'s bauble'/'MED', '170').
card_multiverse_id('urza\'s bauble'/'MED', '159247').

card_in_set('urza\'s chalice', 'MED').
card_original_type('urza\'s chalice'/'MED', 'Artifact').
card_original_text('urza\'s chalice'/'MED', 'Whenever a player plays an artifact spell, you may pay {1}. If you do, you gain 1 life.').
card_image_name('urza\'s chalice'/'MED', 'urza\'s chalice').
card_uid('urza\'s chalice'/'MED', 'MED:Urza\'s Chalice:urza\'s chalice').
card_rarity('urza\'s chalice'/'MED', 'Common').
card_artist('urza\'s chalice'/'MED', 'Jeff A. Menges').
card_number('urza\'s chalice'/'MED', '171').
card_flavor_text('urza\'s chalice'/'MED', 'When sorely wounded or tired, Urza would often retreat to the workshops of his apprentices. They were greatly amazed at how much better he looked each time he took a sip of water.').
card_multiverse_id('urza\'s chalice'/'MED', '159842').

card_in_set('varchild\'s war-riders', 'MED').
card_original_type('varchild\'s war-riders'/'MED', 'Creature — Human Warrior').
card_original_text('varchild\'s war-riders'/'MED', 'Cumulative upkeep—Put a 1/1 red Survivor creature token into play under an opponent\'s control.\nTrample, rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_image_name('varchild\'s war-riders'/'MED', 'varchild\'s war-riders').
card_uid('varchild\'s war-riders'/'MED', 'MED:Varchild\'s War-Riders:varchild\'s war-riders').
card_rarity('varchild\'s war-riders'/'MED', 'Rare').
card_artist('varchild\'s war-riders'/'MED', 'Susan Van Camp').
card_number('varchild\'s war-riders'/'MED', '110').
card_flavor_text('varchild\'s war-riders'/'MED', '\"What tries to crush our spirit only strengthens our resolve.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('varchild\'s war-riders'/'MED', '159742').

card_in_set('vesuvan doppelganger', 'MED').
card_original_type('vesuvan doppelganger'/'MED', 'Creature — Shapeshifter').
card_original_text('vesuvan doppelganger'/'MED', 'As Vesuvan Doppelganger comes into play, you may choose a creature in play. If you do, Vesuvan Doppelganger comes into play as a copy of that creature except for its color and gains \"At the beginning of your upkeep, you may have this creature become a copy of target creature except for its color. If you do, this creature gains this ability.\"').
card_image_name('vesuvan doppelganger'/'MED', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'MED', 'MED:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'MED', 'Rare').
card_artist('vesuvan doppelganger'/'MED', 'Quinton Hoover').
card_number('vesuvan doppelganger'/'MED', '54').
card_multiverse_id('vesuvan doppelganger'/'MED', '159820').

card_in_set('vodalian knights', 'MED').
card_original_type('vodalian knights'/'MED', 'Creature — Merfolk Knight').
card_original_text('vodalian knights'/'MED', 'First strike\nVodalian Knights can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Vodalian Knights.\n{U}: Vodalian Knights gains flying until end of turn.').
card_image_name('vodalian knights'/'MED', 'vodalian knights').
card_uid('vodalian knights'/'MED', 'MED:Vodalian Knights:vodalian knights').
card_rarity('vodalian knights'/'MED', 'Uncommon').
card_artist('vodalian knights'/'MED', 'Susan Van Camp').
card_number('vodalian knights'/'MED', '55').
card_flavor_text('vodalian knights'/'MED', 'Fear the knight leaping from the water into the air, weapon ready.').
card_multiverse_id('vodalian knights'/'MED', '159202').

card_in_set('walking wall', 'MED').
card_original_type('walking wall'/'MED', 'Artifact Creature — Wall').
card_original_text('walking wall'/'MED', 'Defender\n{3}: Walking Wall gets +3/-1 until end of turn and can attack this turn as though it didn\'t have defender. Play this ability only once each turn.').
card_image_name('walking wall'/'MED', 'walking wall').
card_uid('walking wall'/'MED', 'MED:Walking Wall:walking wall').
card_rarity('walking wall'/'MED', 'Uncommon').
card_artist('walking wall'/'MED', 'Anthony Waters').
card_number('walking wall'/'MED', '172').
card_flavor_text('walking wall'/'MED', '\"The fortress is not what it seems.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('walking wall'/'MED', '159248').

card_in_set('wanderlust', 'MED').
card_original_type('wanderlust'/'MED', 'Enchantment — Aura').
card_original_text('wanderlust'/'MED', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, Wanderlust deals 1 damage to that player.').
card_image_name('wanderlust'/'MED', 'wanderlust').
card_uid('wanderlust'/'MED', 'MED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'MED', 'Common').
card_artist('wanderlust'/'MED', 'Cornelius Brudi').
card_number('wanderlust'/'MED', '137').
card_multiverse_id('wanderlust'/'MED', '159757').

card_in_set('winds of change', 'MED').
card_original_type('winds of change'/'MED', 'Sorcery').
card_original_text('winds of change'/'MED', 'Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.').
card_image_name('winds of change'/'MED', 'winds of change').
card_uid('winds of change'/'MED', 'MED:Winds of Change:winds of change').
card_rarity('winds of change'/'MED', 'Uncommon').
card_artist('winds of change'/'MED', 'Justin Hampton').
card_number('winds of change'/'MED', '111').
card_flavor_text('winds of change'/'MED', '\"\'Tis the set of sails and not the gales\nWhich tells us the way to go.\"\n—Ella Wheeler Wilcox, \"Winds of Fate\"').
card_multiverse_id('winds of change'/'MED', '159320').

card_in_set('winter blast', 'MED').
card_original_type('winter blast'/'MED', 'Sorcery').
card_original_text('winter blast'/'MED', 'Tap X target creatures. Winter Blast deals 2 damage to each of those creatures with flying.').
card_image_name('winter blast'/'MED', 'winter blast').
card_uid('winter blast'/'MED', 'MED:Winter Blast:winter blast').
card_rarity('winter blast'/'MED', 'Uncommon').
card_artist('winter blast'/'MED', 'Kaja Foglio').
card_number('winter blast'/'MED', '138').
card_flavor_text('winter blast'/'MED', '\"Blow, winds, and crack your cheeks! rage! blow!\"\n—William Shakespeare, King Lear').
card_multiverse_id('winter blast'/'MED', '159827').

card_in_set('winter orb', 'MED').
card_original_type('winter orb'/'MED', 'Artifact').
card_original_text('winter orb'/'MED', 'As long as Winter Orb is untapped, players can\'t untap more than one land during their untap steps.').
card_image_name('winter orb'/'MED', 'winter orb').
card_uid('winter orb'/'MED', 'MED:Winter Orb:winter orb').
card_rarity('winter orb'/'MED', 'Rare').
card_artist('winter orb'/'MED', 'Mark Tedin').
card_number('winter orb'/'MED', '173').
card_multiverse_id('winter orb'/'MED', '159277').

card_in_set('word of undoing', 'MED').
card_original_type('word of undoing'/'MED', 'Instant').
card_original_text('word of undoing'/'MED', 'Return target creature and all white Auras you own attached to it to their owners\' hands.').
card_image_name('word of undoing'/'MED', 'word of undoing').
card_uid('word of undoing'/'MED', 'MED:Word of Undoing:word of undoing').
card_rarity('word of undoing'/'MED', 'Common').
card_artist('word of undoing'/'MED', 'Christopher Rush').
card_number('word of undoing'/'MED', '56').
card_flavor_text('word of undoing'/'MED', '\"It was in Urza\'s journals that I finally found the secret at the core of the summonings.\"\n—Journal, author unknown').
card_multiverse_id('word of undoing'/'MED', '159751').

card_in_set('wyluli wolf', 'MED').
card_original_type('wyluli wolf'/'MED', 'Creature — Wolf').
card_original_text('wyluli wolf'/'MED', '{T}: Target creature gets +1/+1 until end of turn.').
card_image_name('wyluli wolf'/'MED', 'wyluli wolf').
card_uid('wyluli wolf'/'MED', 'MED:Wyluli Wolf:wyluli wolf').
card_rarity('wyluli wolf'/'MED', 'Common').
card_artist('wyluli wolf'/'MED', 'Susan Van Camp').
card_number('wyluli wolf'/'MED', '139').
card_flavor_text('wyluli wolf'/'MED', '\"When one wolf calls, others follow. Who wants to fight creatures that eat scorpions?\"\n—Maimun al-Wyluli, diary').
card_multiverse_id('wyluli wolf'/'MED', '159766').

card_in_set('yavimaya ants', 'MED').
card_original_type('yavimaya ants'/'MED', 'Creature — Insect').
card_original_text('yavimaya ants'/'MED', 'Trample, haste\nCumulative upkeep {G}{G}').
card_image_name('yavimaya ants'/'MED', 'yavimaya ants').
card_uid('yavimaya ants'/'MED', 'MED:Yavimaya Ants:yavimaya ants').
card_rarity('yavimaya ants'/'MED', 'Uncommon').
card_artist('yavimaya ants'/'MED', 'Pat Morrissey').
card_number('yavimaya ants'/'MED', '140').
card_flavor_text('yavimaya ants'/'MED', '\"Few natural forces are as devastating as hunger.\"\n—Kaysa, elder druid of the Juniper Order').
card_multiverse_id('yavimaya ants'/'MED', '159109').

card_in_set('ydwen efreet', 'MED').
card_original_type('ydwen efreet'/'MED', 'Creature — Efreet').
card_original_text('ydwen efreet'/'MED', 'Whenever you\'re attacked, flip a coin. If you lose the flip, Ydwen Efreet can\'t block this turn.').
card_image_name('ydwen efreet'/'MED', 'ydwen efreet').
card_uid('ydwen efreet'/'MED', 'MED:Ydwen Efreet:ydwen efreet').
card_rarity('ydwen efreet'/'MED', 'Rare').
card_artist('ydwen efreet'/'MED', 'Drew Tucker').
card_number('ydwen efreet'/'MED', '112').
card_multiverse_id('ydwen efreet'/'MED', '159844').

card_in_set('zuran orb', 'MED').
card_original_type('zuran orb'/'MED', 'Artifact').
card_original_text('zuran orb'/'MED', 'Sacrifice a land: You gain 2 life.').
card_image_name('zuran orb'/'MED', 'zuran orb').
card_uid('zuran orb'/'MED', 'MED:Zuran Orb:zuran orb').
card_rarity('zuran orb'/'MED', 'Uncommon').
card_artist('zuran orb'/'MED', 'Sandra Everingham').
card_number('zuran orb'/'MED', '174').
card_flavor_text('zuran orb'/'MED', '\"I will go to any length to achieve my goal. Eternal life is worth any sacrifice.\"\n—Zur the Enchanter').
card_multiverse_id('zuran orb'/'MED', '159752').
