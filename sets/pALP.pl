% Asia Pacific Land Program

set('pALP').
set_name('pALP', 'Asia Pacific Land Program').
set_release_date('pALP', '1998-09-01').
set_border('pALP', 'black').
set_type('pALP', 'promo').

card_in_set('forest', 'pALP').
card_original_type('forest'/'pALP', 'Basic Land — Forest').
card_original_text('forest'/'pALP', '').
card_image_name('forest'/'pALP', 'forest1').
card_uid('forest'/'pALP', 'pALP:Forest:forest1').
card_rarity('forest'/'pALP', 'Basic Land').
card_artist('forest'/'pALP', 'Pete Venters').
card_number('forest'/'pALP', '1').

card_in_set('forest', 'pALP').
card_original_type('forest'/'pALP', 'Basic Land — Forest').
card_original_text('forest'/'pALP', '').
card_image_name('forest'/'pALP', 'forest2').
card_uid('forest'/'pALP', 'pALP:Forest:forest2').
card_rarity('forest'/'pALP', 'Basic Land').
card_artist('forest'/'pALP', 'Edward P. Beard, Jr.').
card_number('forest'/'pALP', '11').

card_in_set('forest', 'pALP').
card_original_type('forest'/'pALP', 'Basic Land — Forest').
card_original_text('forest'/'pALP', '').
card_image_name('forest'/'pALP', 'forest3').
card_uid('forest'/'pALP', 'pALP:Forest:forest3').
card_rarity('forest'/'pALP', 'Basic Land').
card_artist('forest'/'pALP', 'Christopher Rush').
card_number('forest'/'pALP', '6').

card_in_set('island', 'pALP').
card_original_type('island'/'pALP', 'Basic Land — Island').
card_original_text('island'/'pALP', '').
card_image_name('island'/'pALP', 'island1').
card_uid('island'/'pALP', 'pALP:Island:island1').
card_rarity('island'/'pALP', 'Basic Land').
card_artist('island'/'pALP', 'Rob Alexander').
card_number('island'/'pALP', '12').

card_in_set('island', 'pALP').
card_original_type('island'/'pALP', 'Basic Land — Island').
card_original_text('island'/'pALP', '').
card_image_name('island'/'pALP', 'island2').
card_uid('island'/'pALP', 'pALP:Island:island2').
card_rarity('island'/'pALP', 'Basic Land').
card_artist('island'/'pALP', 'Edward P. Beard, Jr.').
card_number('island'/'pALP', '2').

card_in_set('island', 'pALP').
card_original_type('island'/'pALP', 'Basic Land — Island').
card_original_text('island'/'pALP', '').
card_image_name('island'/'pALP', 'island3').
card_uid('island'/'pALP', 'pALP:Island:island3').
card_rarity('island'/'pALP', 'Basic Land').
card_artist('island'/'pALP', 'Bob Eggleton').
card_number('island'/'pALP', '7').

card_in_set('mountain', 'pALP').
card_original_type('mountain'/'pALP', 'Basic Land — Mountain').
card_original_text('mountain'/'pALP', '').
card_image_name('mountain'/'pALP', 'mountain1').
card_uid('mountain'/'pALP', 'pALP:Mountain:mountain1').
card_rarity('mountain'/'pALP', 'Basic Land').
card_artist('mountain'/'pALP', 'Edward P. Beard, Jr.').
card_number('mountain'/'pALP', '13').

card_in_set('mountain', 'pALP').
card_original_type('mountain'/'pALP', 'Basic Land — Mountain').
card_original_text('mountain'/'pALP', '').
card_image_name('mountain'/'pALP', 'mountain2').
card_uid('mountain'/'pALP', 'pALP:Mountain:mountain2').
card_rarity('mountain'/'pALP', 'Basic Land').
card_artist('mountain'/'pALP', 'Heather Hudson').
card_number('mountain'/'pALP', '3').

card_in_set('mountain', 'pALP').
card_original_type('mountain'/'pALP', 'Basic Land — Mountain').
card_original_text('mountain'/'pALP', '').
card_image_name('mountain'/'pALP', 'mountain3').
card_uid('mountain'/'pALP', 'pALP:Mountain:mountain3').
card_rarity('mountain'/'pALP', 'Basic Land').
card_artist('mountain'/'pALP', 'Rebecca Guay').
card_number('mountain'/'pALP', '8').

card_in_set('plains', 'pALP').
card_original_type('plains'/'pALP', 'Basic Land — Plains').
card_original_text('plains'/'pALP', '').
card_image_name('plains'/'pALP', 'plains1').
card_uid('plains'/'pALP', 'pALP:Plains:plains1').
card_rarity('plains'/'pALP', 'Basic Land').
card_artist('plains'/'pALP', 'Rebecca Guay').
card_number('plains'/'pALP', '14').

card_in_set('plains', 'pALP').
card_original_type('plains'/'pALP', 'Basic Land — Plains').
card_original_text('plains'/'pALP', '').
card_image_name('plains'/'pALP', 'plains2').
card_uid('plains'/'pALP', 'pALP:Plains:plains2').
card_rarity('plains'/'pALP', 'Basic Land').
card_artist('plains'/'pALP', 'Ron Spears').
card_number('plains'/'pALP', '4').

card_in_set('plains', 'pALP').
card_original_type('plains'/'pALP', 'Basic Land — Plains').
card_original_text('plains'/'pALP', '').
card_image_name('plains'/'pALP', 'plains3').
card_uid('plains'/'pALP', 'pALP:Plains:plains3').
card_rarity('plains'/'pALP', 'Basic Land').
card_artist('plains'/'pALP', 'Colin MacNeil').
card_number('plains'/'pALP', '9').

card_in_set('swamp', 'pALP').
card_original_type('swamp'/'pALP', 'Basic Land — Swamp').
card_original_text('swamp'/'pALP', '').
card_image_name('swamp'/'pALP', 'swamp1').
card_uid('swamp'/'pALP', 'pALP:Swamp:swamp1').
card_rarity('swamp'/'pALP', 'Basic Land').
card_artist('swamp'/'pALP', 'Ron Spears').
card_number('swamp'/'pALP', '10').

card_in_set('swamp', 'pALP').
card_original_type('swamp'/'pALP', 'Basic Land — Swamp').
card_original_text('swamp'/'pALP', '').
card_image_name('swamp'/'pALP', 'swamp2').
card_uid('swamp'/'pALP', 'pALP:Swamp:swamp2').
card_rarity('swamp'/'pALP', 'Basic Land').
card_artist('swamp'/'pALP', 'Ron Spears').
card_number('swamp'/'pALP', '15').

card_in_set('swamp', 'pALP').
card_original_type('swamp'/'pALP', 'Basic Land — Swamp').
card_original_text('swamp'/'pALP', '').
card_image_name('swamp'/'pALP', 'swamp3').
card_uid('swamp'/'pALP', 'pALP:Swamp:swamp3').
card_rarity('swamp'/'pALP', 'Basic Land').
card_artist('swamp'/'pALP', 'Edward P. Beard, Jr.').
card_number('swamp'/'pALP', '5').
