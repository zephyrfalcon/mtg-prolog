% Pro Tour

set('pPRO').
set_name('pPRO', 'Pro Tour').
set_release_date('pPRO', '2007-02-09').
set_border('pPRO', 'black').
set_type('pPRO', 'promo').

card_in_set('ajani goldmane', 'pPRO').
card_original_type('ajani goldmane'/'pPRO', 'Planeswalker — Ajani').
card_original_text('ajani goldmane'/'pPRO', '').
card_first_print('ajani goldmane', 'pPRO').
card_image_name('ajani goldmane'/'pPRO', 'ajani goldmane').
card_uid('ajani goldmane'/'pPRO', 'pPRO:Ajani Goldmane:ajani goldmane').
card_rarity('ajani goldmane'/'pPRO', 'Special').
card_artist('ajani goldmane'/'pPRO', 'Christopher Moeller').
card_number('ajani goldmane'/'pPRO', '5').

card_in_set('avatar of woe', 'pPRO').
card_original_type('avatar of woe'/'pPRO', 'Creature — Avatar').
card_original_text('avatar of woe'/'pPRO', '').
card_image_name('avatar of woe'/'pPRO', 'avatar of woe').
card_uid('avatar of woe'/'pPRO', 'pPRO:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'pPRO', 'Special').
card_artist('avatar of woe'/'pPRO', 'rk post').
card_number('avatar of woe'/'pPRO', '4').

card_in_set('eternal dragon', 'pPRO').
card_original_type('eternal dragon'/'pPRO', 'Creature — Dragon Spirit').
card_original_text('eternal dragon'/'pPRO', '').
card_image_name('eternal dragon'/'pPRO', 'eternal dragon').
card_uid('eternal dragon'/'pPRO', 'pPRO:Eternal Dragon:eternal dragon').
card_rarity('eternal dragon'/'pPRO', 'Special').
card_artist('eternal dragon'/'pPRO', 'Adam Rex').
card_number('eternal dragon'/'pPRO', '1').

card_in_set('mirari\'s wake', 'pPRO').
card_original_type('mirari\'s wake'/'pPRO', 'Enchantment').
card_original_text('mirari\'s wake'/'pPRO', '').
card_image_name('mirari\'s wake'/'pPRO', 'mirari\'s wake').
card_uid('mirari\'s wake'/'pPRO', 'pPRO:Mirari\'s Wake:mirari\'s wake').
card_rarity('mirari\'s wake'/'pPRO', 'Special').
card_artist('mirari\'s wake'/'pPRO', 'Volkan Baga').
card_number('mirari\'s wake'/'pPRO', '2').
card_flavor_text('mirari\'s wake'/'pPRO', 'Even after a false god tore magic from Dominaria, power still radiated from the Mirari sword that slew her.').

card_in_set('treva, the renewer', 'pPRO').
card_original_type('treva, the renewer'/'pPRO', 'Legendary Creature — Dragon').
card_original_text('treva, the renewer'/'pPRO', '').
card_image_name('treva, the renewer'/'pPRO', 'treva, the renewer').
card_uid('treva, the renewer'/'pPRO', 'pPRO:Treva, the Renewer:treva, the renewer').
card_rarity('treva, the renewer'/'pPRO', 'Special').
card_artist('treva, the renewer'/'pPRO', 'Jean-Sébastien Rossbach').
card_number('treva, the renewer'/'pPRO', '3').
