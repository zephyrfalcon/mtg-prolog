% Magic: The Gathering—Conspiracy

set('CNS').
set_name('CNS', 'Magic: The Gathering—Conspiracy').
set_release_date('CNS', '2014-06-06').
set_border('CNS', 'black').
set_type('CNS', 'conspiracy').

card_in_set('academy elite', 'CNS').
card_original_type('academy elite'/'CNS', 'Creature — Human Wizard').
card_original_text('academy elite'/'CNS', 'Academy Elite enters the battlefield with X +1/+1 counters on it, where X is the number of instant and sorcery cards in all graveyards.\n{2}{U}, Remove a +1/+1 counter from Academy Elite: Draw a card, then discard a card.').
card_first_print('academy elite', 'CNS').
card_image_name('academy elite'/'CNS', 'academy elite').
card_uid('academy elite'/'CNS', 'CNS:Academy Elite:academy elite').
card_rarity('academy elite'/'CNS', 'Rare').
card_artist('academy elite'/'CNS', 'Volkan Baga').
card_number('academy elite'/'CNS', '20').
card_multiverse_id('academy elite'/'CNS', '382205').

card_in_set('advantageous proclamation', 'CNS').
card_original_type('advantageous proclamation'/'CNS', 'Conspiracy').
card_original_text('advantageous proclamation'/'CNS', '(Start the game with this conspiracy face up in the command zone.)\nYour minimum deck size is reduced by five.').
card_first_print('advantageous proclamation', 'CNS').
card_image_name('advantageous proclamation'/'CNS', 'advantageous proclamation').
card_uid('advantageous proclamation'/'CNS', 'CNS:Advantageous Proclamation:advantageous proclamation').
card_rarity('advantageous proclamation'/'CNS', 'Uncommon').
card_artist('advantageous proclamation'/'CNS', 'Izzy').
card_number('advantageous proclamation'/'CNS', '1').
card_flavor_text('advantageous proclamation'/'CNS', '\"The beneficent council deems you worthy of favor. They hope this doesn\'t provoke envy from your peers.\"').
card_multiverse_id('advantageous proclamation'/'CNS', '382206').

card_in_set('æther searcher', 'CNS').
card_original_type('æther searcher'/'CNS', 'Artifact Creature — Construct').
card_original_text('æther searcher'/'CNS', 'Reveal Æther Searcher as you draft it. Reveal the next card you draft and note its name. \nWhen Æther Searcher enters the battlefield, you may search your hand and/or library for a card with a name noted as you drafted cards named Æther Searcher. You may cast it without paying its mana cost. If you searched your library this way, shuffle it.').
card_first_print('æther searcher', 'CNS').
card_image_name('æther searcher'/'CNS', 'aether searcher').
card_uid('æther searcher'/'CNS', 'CNS:Æther Searcher:aether searcher').
card_rarity('æther searcher'/'CNS', 'Rare').
card_artist('æther searcher'/'CNS', 'James Paick').
card_number('æther searcher'/'CNS', '53').
card_multiverse_id('æther searcher'/'CNS', '382207').

card_in_set('æther tradewinds', 'CNS').
card_original_type('æther tradewinds'/'CNS', 'Instant').
card_original_text('æther tradewinds'/'CNS', 'Return target permanent you control and target permanent you don\'t control to their owners\' hands.').
card_image_name('æther tradewinds'/'CNS', 'aether tradewinds').
card_uid('æther tradewinds'/'CNS', 'CNS:Æther Tradewinds:aether tradewinds').
card_rarity('æther tradewinds'/'CNS', 'Common').
card_artist('æther tradewinds'/'CNS', 'Kieran Yanner').
card_number('æther tradewinds'/'CNS', '89').
card_flavor_text('æther tradewinds'/'CNS', '\"The wind smells of misfortune. Check your knots.\"\n—Anitan, Ondu cleric').
card_multiverse_id('æther tradewinds'/'CNS', '382208').

card_in_set('agent of acquisitions', 'CNS').
card_original_type('agent of acquisitions'/'CNS', 'Artifact Creature — Construct').
card_original_text('agent of acquisitions'/'CNS', 'Draft Agent of Acquisitions face up.\nInstead of drafting a card from a booster pack, you may draft each card in that booster pack, one at a time. If you do, turn Agent of Acquisitions face down and you can\'t draft cards for the rest of this draft round. (You may look at booster packs passed to you.)').
card_first_print('agent of acquisitions', 'CNS').
card_image_name('agent of acquisitions'/'CNS', 'agent of acquisitions').
card_uid('agent of acquisitions'/'CNS', 'CNS:Agent of Acquisitions:agent of acquisitions').
card_rarity('agent of acquisitions'/'CNS', 'Uncommon').
card_artist('agent of acquisitions'/'CNS', 'Eytan Zana').
card_number('agent of acquisitions'/'CNS', '54').
card_multiverse_id('agent of acquisitions'/'CNS', '382209').

card_in_set('air servant', 'CNS').
card_original_type('air servant'/'CNS', 'Creature — Elemental').
card_original_text('air servant'/'CNS', 'Flying\n{2}{U}: Tap target creature with flying.').
card_image_name('air servant'/'CNS', 'air servant').
card_uid('air servant'/'CNS', 'CNS:Air Servant:air servant').
card_rarity('air servant'/'CNS', 'Uncommon').
card_artist('air servant'/'CNS', 'Lars Grant-West').
card_number('air servant'/'CNS', '90').
card_flavor_text('air servant'/'CNS', '\"Wind is forceful, yet ephemeral. It can knock a dragon out of the sky, yet pass through the smallest crack unhindered.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('air servant'/'CNS', '382210').

card_in_set('ajani\'s sunstriker', 'CNS').
card_original_type('ajani\'s sunstriker'/'CNS', 'Creature — Cat Cleric').
card_original_text('ajani\'s sunstriker'/'CNS', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('ajani\'s sunstriker'/'CNS', 'ajani\'s sunstriker').
card_uid('ajani\'s sunstriker'/'CNS', 'CNS:Ajani\'s Sunstriker:ajani\'s sunstriker').
card_rarity('ajani\'s sunstriker'/'CNS', 'Common').
card_artist('ajani\'s sunstriker'/'CNS', 'Matt Stewart').
card_number('ajani\'s sunstriker'/'CNS', '66').
card_flavor_text('ajani\'s sunstriker'/'CNS', '\"Ajani goes where he is needed most and in his name we keep his lands safe.\"').
card_multiverse_id('ajani\'s sunstriker'/'CNS', '382211').

card_in_set('altar of dementia', 'CNS').
card_original_type('altar of dementia'/'CNS', 'Artifact').
card_original_text('altar of dementia'/'CNS', 'Sacrifice a creature: Target player puts a number of cards equal to the sacrificed creature\'s power from the top of his or her library into his or her graveyard.').
card_image_name('altar of dementia'/'CNS', 'altar of dementia').
card_uid('altar of dementia'/'CNS', 'CNS:Altar of Dementia:altar of dementia').
card_rarity('altar of dementia'/'CNS', 'Rare').
card_artist('altar of dementia'/'CNS', 'Brom').
card_number('altar of dementia'/'CNS', '196').
card_flavor_text('altar of dementia'/'CNS', '\"It is not that you will go mad. It is that you will beg for madness.\"\n—Volrath').
card_multiverse_id('altar of dementia'/'CNS', '382212').

card_in_set('altar\'s reap', 'CNS').
card_original_type('altar\'s reap'/'CNS', 'Instant').
card_original_text('altar\'s reap'/'CNS', 'As an additional cost to cast Altar\'s Reap, sacrifice a creature.\nDraw two cards.').
card_image_name('altar\'s reap'/'CNS', 'altar\'s reap').
card_uid('altar\'s reap'/'CNS', 'CNS:Altar\'s Reap:altar\'s reap').
card_rarity('altar\'s reap'/'CNS', 'Common').
card_artist('altar\'s reap'/'CNS', 'Donato Giancola').
card_number('altar\'s reap'/'CNS', '112').
card_flavor_text('altar\'s reap'/'CNS', '\"Don\'t worry, your death will be as informative as possible.\"\n—Gorghul, augur of skulls').
card_multiverse_id('altar\'s reap'/'CNS', '382213').

card_in_set('apex hawks', 'CNS').
card_original_type('apex hawks'/'CNS', 'Creature — Bird').
card_original_text('apex hawks'/'CNS', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nFlying\nApex Hawks enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('apex hawks'/'CNS', 'apex hawks').
card_uid('apex hawks'/'CNS', 'CNS:Apex Hawks:apex hawks').
card_rarity('apex hawks'/'CNS', 'Common').
card_artist('apex hawks'/'CNS', 'David Palumbo').
card_number('apex hawks'/'CNS', '67').
card_multiverse_id('apex hawks'/'CNS', '382214').

card_in_set('assassinate', 'CNS').
card_original_type('assassinate'/'CNS', 'Sorcery').
card_original_text('assassinate'/'CNS', 'Destroy target tapped creature.').
card_image_name('assassinate'/'CNS', 'assassinate').
card_uid('assassinate'/'CNS', 'CNS:Assassinate:assassinate').
card_rarity('assassinate'/'CNS', 'Common').
card_artist('assassinate'/'CNS', 'Kev Walker').
card_number('assassinate'/'CNS', '113').
card_flavor_text('assassinate'/'CNS', '\"This is how wars are won—not with armies of soldiers but with a single knife blade, artfully placed.\"\n—Yurin, royal assassin').
card_multiverse_id('assassinate'/'CNS', '382215').

card_in_set('backup plan', 'CNS').
card_original_type('backup plan'/'CNS', 'Conspiracy').
card_original_text('backup plan'/'CNS', '(Start the game with this conspiracy face up in the command zone.)\nDraw an additional hand of seven cards as the game begins. Before taking mulligans, shuffle all but one of your hands into your library.').
card_first_print('backup plan', 'CNS').
card_image_name('backup plan'/'CNS', 'backup plan').
card_uid('backup plan'/'CNS', 'CNS:Backup Plan:backup plan').
card_rarity('backup plan'/'CNS', 'Rare').
card_artist('backup plan'/'CNS', 'David Palumbo').
card_number('backup plan'/'CNS', '2').
card_flavor_text('backup plan'/'CNS', '\"In the market for an alternative, friend?\"').
card_multiverse_id('backup plan'/'CNS', '382216').

card_in_set('barbed shocker', 'CNS').
card_original_type('barbed shocker'/'CNS', 'Creature — Insect').
card_original_text('barbed shocker'/'CNS', 'Trample, haste\nWhenever Barbed Shocker deals damage to a player, that player discards all the cards in his or her hand, then draws that many cards.').
card_image_name('barbed shocker'/'CNS', 'barbed shocker').
card_uid('barbed shocker'/'CNS', 'CNS:Barbed Shocker:barbed shocker').
card_rarity('barbed shocker'/'CNS', 'Uncommon').
card_artist('barbed shocker'/'CNS', 'Tony Szczudlo').
card_number('barbed shocker'/'CNS', '136').
card_flavor_text('barbed shocker'/'CNS', 'Fervid shamans willingly submit to shockers in hopes of glimpsing the fortunes of the future.').
card_multiverse_id('barbed shocker'/'CNS', '382217').

card_in_set('basandra, battle seraph', 'CNS').
card_original_type('basandra, battle seraph'/'CNS', 'Legendary Creature — Angel').
card_original_text('basandra, battle seraph'/'CNS', 'Flying\nPlayers can\'t cast spells during combat.\n{R}: Target creature attacks this turn if able.').
card_image_name('basandra, battle seraph'/'CNS', 'basandra, battle seraph').
card_uid('basandra, battle seraph'/'CNS', 'CNS:Basandra, Battle Seraph:basandra, battle seraph').
card_rarity('basandra, battle seraph'/'CNS', 'Rare').
card_artist('basandra, battle seraph'/'CNS', 'Terese Nielsen').
card_number('basandra, battle seraph'/'CNS', '184').
card_flavor_text('basandra, battle seraph'/'CNS', '\"Why listen to the mumblings of wizards when the lash speaks true?\"').
card_multiverse_id('basandra, battle seraph'/'CNS', '382218').

card_in_set('bite of the black rose', 'CNS').
card_original_type('bite of the black rose'/'CNS', 'Sorcery').
card_original_text('bite of the black rose'/'CNS', 'Will of the council — Starting with you, each player votes for sickness or psychosis. If sickness gets more votes, creatures your opponents control get -2/-2 until end of turn. If psychosis gets more votes or the vote is tied, each opponent discards two cards.').
card_first_print('bite of the black rose', 'CNS').
card_image_name('bite of the black rose'/'CNS', 'bite of the black rose').
card_uid('bite of the black rose'/'CNS', 'CNS:Bite of the Black Rose:bite of the black rose').
card_rarity('bite of the black rose'/'CNS', 'Uncommon').
card_artist('bite of the black rose'/'CNS', 'Franz Vohwinkel').
card_number('bite of the black rose'/'CNS', '26').
card_multiverse_id('bite of the black rose'/'CNS', '382219').

card_in_set('boldwyr intimidator', 'CNS').
card_original_type('boldwyr intimidator'/'CNS', 'Creature — Giant Warrior').
card_original_text('boldwyr intimidator'/'CNS', 'Cowards can\'t block Warriors.\n{R}: Target creature becomes a Coward until end of turn.\n{2}{R}: Target creature becomes a Warrior until end of turn.').
card_image_name('boldwyr intimidator'/'CNS', 'boldwyr intimidator').
card_uid('boldwyr intimidator'/'CNS', 'CNS:Boldwyr Intimidator:boldwyr intimidator').
card_rarity('boldwyr intimidator'/'CNS', 'Uncommon').
card_artist('boldwyr intimidator'/'CNS', 'Esad Ribic').
card_number('boldwyr intimidator'/'CNS', '137').
card_flavor_text('boldwyr intimidator'/'CNS', '\"Now everyone knows what you are.\"').
card_multiverse_id('boldwyr intimidator'/'CNS', '382220').

card_in_set('brago\'s favor', 'CNS').
card_original_type('brago\'s favor'/'CNS', 'Conspiracy').
card_original_text('brago\'s favor'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nSpells with the chosen name you cast cost {1} less to cast.').
card_first_print('brago\'s favor', 'CNS').
card_image_name('brago\'s favor'/'CNS', 'brago\'s favor').
card_uid('brago\'s favor'/'CNS', 'CNS:Brago\'s Favor:brago\'s favor').
card_rarity('brago\'s favor'/'CNS', 'Common').
card_artist('brago\'s favor'/'CNS', 'Karla Ortiz').
card_number('brago\'s favor'/'CNS', '3').
card_multiverse_id('brago\'s favor'/'CNS', '382222').

card_in_set('brago\'s representative', 'CNS').
card_original_type('brago\'s representative'/'CNS', 'Creature — Human Advisor').
card_original_text('brago\'s representative'/'CNS', 'While voting, you get an additional vote. (The votes can be for different choices or for the same choice.)').
card_first_print('brago\'s representative', 'CNS').
card_image_name('brago\'s representative'/'CNS', 'brago\'s representative').
card_uid('brago\'s representative'/'CNS', 'CNS:Brago\'s Representative:brago\'s representative').
card_rarity('brago\'s representative'/'CNS', 'Common').
card_artist('brago\'s representative'/'CNS', 'Anthony Palumbo').
card_number('brago\'s representative'/'CNS', '14').
card_flavor_text('brago\'s representative'/'CNS', 'On Fiora, no matter is settled until King Brago offers an opinion.').
card_multiverse_id('brago\'s representative'/'CNS', '382223').

card_in_set('brago, king eternal', 'CNS').
card_original_type('brago, king eternal'/'CNS', 'Legendary Creature — Spirit').
card_original_text('brago, king eternal'/'CNS', 'Flying\nWhen Brago, King Eternal deals combat damage to a player, exile any number of target nonland permanents you control, then return those cards to the battlefield under their owner\'s control.').
card_first_print('brago, king eternal', 'CNS').
card_image_name('brago, king eternal'/'CNS', 'brago, king eternal').
card_uid('brago, king eternal'/'CNS', 'CNS:Brago, King Eternal:brago, king eternal').
card_rarity('brago, king eternal'/'CNS', 'Rare').
card_artist('brago, king eternal'/'CNS', 'Karla Ortiz').
card_number('brago, king eternal'/'CNS', '41').
card_flavor_text('brago, king eternal'/'CNS', '\"My rule persists beyond death itself.\"').
card_multiverse_id('brago, king eternal'/'CNS', '382221').

card_in_set('brainstorm', 'CNS').
card_original_type('brainstorm'/'CNS', 'Instant').
card_original_text('brainstorm'/'CNS', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'CNS', 'brainstorm').
card_uid('brainstorm'/'CNS', 'CNS:Brainstorm:brainstorm').
card_rarity('brainstorm'/'CNS', 'Common').
card_artist('brainstorm'/'CNS', 'Willian Murai').
card_number('brainstorm'/'CNS', '91').
card_flavor_text('brainstorm'/'CNS', 'The mizzium-sphere array drove her mind deep into the thought field, where only the rarest motes of genius may be plucked.').
card_multiverse_id('brainstorm'/'CNS', '382224').

card_in_set('breakthrough', 'CNS').
card_original_type('breakthrough'/'CNS', 'Sorcery').
card_original_text('breakthrough'/'CNS', 'Draw four cards, then choose X cards in your hand and discard the rest.').
card_image_name('breakthrough'/'CNS', 'breakthrough').
card_uid('breakthrough'/'CNS', 'CNS:Breakthrough:breakthrough').
card_rarity('breakthrough'/'CNS', 'Uncommon').
card_artist('breakthrough'/'CNS', 'Raymond Swanland').
card_number('breakthrough'/'CNS', '92').
card_flavor_text('breakthrough'/'CNS', 'The ideas came flooding in so fast that they couldn\'t all be contained.').
card_multiverse_id('breakthrough'/'CNS', '382225').

card_in_set('brimstone volley', 'CNS').
card_original_type('brimstone volley'/'CNS', 'Instant').
card_original_text('brimstone volley'/'CNS', 'Brimstone Volley deals 3 damage to target creature or player.\nMorbid — Brimstone Volley deals 5 damage to that creature or player instead if a creature died this turn.').
card_image_name('brimstone volley'/'CNS', 'brimstone volley').
card_uid('brimstone volley'/'CNS', 'CNS:Brimstone Volley:brimstone volley').
card_rarity('brimstone volley'/'CNS', 'Common').
card_artist('brimstone volley'/'CNS', 'Eytan Zana').
card_number('brimstone volley'/'CNS', '138').
card_multiverse_id('brimstone volley'/'CNS', '382226').

card_in_set('canal dredger', 'CNS').
card_original_type('canal dredger'/'CNS', 'Artifact Creature — Construct').
card_original_text('canal dredger'/'CNS', 'Draft Canal Dredger face up.\nEach player passes the last card from each booster pack to a player who drafted a card named Canal Dredger.\n{T}: Put target card from your graveyard on the bottom of your library.').
card_first_print('canal dredger', 'CNS').
card_image_name('canal dredger'/'CNS', 'canal dredger').
card_uid('canal dredger'/'CNS', 'CNS:Canal Dredger:canal dredger').
card_rarity('canal dredger'/'CNS', 'Rare').
card_artist('canal dredger'/'CNS', 'John Avon').
card_number('canal dredger'/'CNS', '55').
card_multiverse_id('canal dredger'/'CNS', '382227').

card_in_set('charging rhino', 'CNS').
card_original_type('charging rhino'/'CNS', 'Creature — Rhino').
card_original_text('charging rhino'/'CNS', 'Charging Rhino can\'t be blocked by more than one creature.').
card_image_name('charging rhino'/'CNS', 'charging rhino').
card_uid('charging rhino'/'CNS', 'CNS:Charging Rhino:charging rhino').
card_rarity('charging rhino'/'CNS', 'Common').
card_artist('charging rhino'/'CNS', 'Daren Bader').
card_number('charging rhino'/'CNS', '159').
card_flavor_text('charging rhino'/'CNS', '\"Is it wrong to go straight for what you want? Sneaking is for cats and goblins.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('charging rhino'/'CNS', '382228').

card_in_set('chartooth cougar', 'CNS').
card_original_type('chartooth cougar'/'CNS', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'CNS', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'CNS', 'chartooth cougar').
card_uid('chartooth cougar'/'CNS', 'CNS:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'CNS', 'Common').
card_artist('chartooth cougar'/'CNS', 'Chase Stone').
card_number('chartooth cougar'/'CNS', '139').
card_multiverse_id('chartooth cougar'/'CNS', '382229').

card_in_set('cinder wall', 'CNS').
card_original_type('cinder wall'/'CNS', 'Creature — Wall').
card_original_text('cinder wall'/'CNS', 'Defender\nWhen Cinder Wall blocks, destroy it at end of combat.').
card_image_name('cinder wall'/'CNS', 'cinder wall').
card_uid('cinder wall'/'CNS', 'CNS:Cinder Wall:cinder wall').
card_rarity('cinder wall'/'CNS', 'Common').
card_artist('cinder wall'/'CNS', 'Anthony S. Waters').
card_number('cinder wall'/'CNS', '140').
card_flavor_text('cinder wall'/'CNS', '\"The smell of roasted flesh hung over the valley for hours.\"\n—Onean sergeant').
card_multiverse_id('cinder wall'/'CNS', '382230').

card_in_set('coercive portal', 'CNS').
card_original_type('coercive portal'/'CNS', 'Artifact').
card_original_text('coercive portal'/'CNS', 'Will of the council — At the beginning of your upkeep, starting with you, each player votes for carnage or homage. If carnage gets more votes, sacrifice Coercive Portal and destroy all nonland permanents. If homage gets more votes or the vote is tied, draw a card.').
card_first_print('coercive portal', 'CNS').
card_image_name('coercive portal'/'CNS', 'coercive portal').
card_uid('coercive portal'/'CNS', 'CNS:Coercive Portal:coercive portal').
card_rarity('coercive portal'/'CNS', 'Mythic Rare').
card_artist('coercive portal'/'CNS', 'Yeong-Hao Han').
card_number('coercive portal'/'CNS', '56').
card_multiverse_id('coercive portal'/'CNS', '382231').

card_in_set('cogwork grinder', 'CNS').
card_original_type('cogwork grinder'/'CNS', 'Artifact Creature — Construct').
card_original_text('cogwork grinder'/'CNS', 'Draft Cogwork Grinder face up.\nAs you draft a card, you may remove it from the draft face down. (Those cards aren\'t in your card pool.)\nCogwork Grinder enters the battlefield with X +1/+1 counters on it, where X is the number of cards you removed from the draft with cards named Cogwork Grinder.').
card_first_print('cogwork grinder', 'CNS').
card_image_name('cogwork grinder'/'CNS', 'cogwork grinder').
card_uid('cogwork grinder'/'CNS', 'CNS:Cogwork Grinder:cogwork grinder').
card_rarity('cogwork grinder'/'CNS', 'Rare').
card_artist('cogwork grinder'/'CNS', 'Jasper Sandner').
card_number('cogwork grinder'/'CNS', '57').
card_multiverse_id('cogwork grinder'/'CNS', '382232').

card_in_set('cogwork librarian', 'CNS').
card_original_type('cogwork librarian'/'CNS', 'Artifact Creature — Construct').
card_original_text('cogwork librarian'/'CNS', 'Draft Cogwork Librarian face up.\nAs you draft a card, you may draft an additional card from that booster pack. If you do, put Cogwork Librarian into that booster pack.').
card_first_print('cogwork librarian', 'CNS').
card_image_name('cogwork librarian'/'CNS', 'cogwork librarian').
card_uid('cogwork librarian'/'CNS', 'CNS:Cogwork Librarian:cogwork librarian').
card_rarity('cogwork librarian'/'CNS', 'Common').
card_artist('cogwork librarian'/'CNS', 'Dan Scott').
card_number('cogwork librarian'/'CNS', '58').
card_flavor_text('cogwork librarian'/'CNS', 'Why wait for answers?').
card_multiverse_id('cogwork librarian'/'CNS', '382233').

card_in_set('cogwork spy', 'CNS').
card_original_type('cogwork spy'/'CNS', 'Artifact Creature — Bird Construct').
card_original_text('cogwork spy'/'CNS', 'Reveal Cogwork Spy as you draft it. You may look at the next card drafted from this booster pack.\nFlying').
card_first_print('cogwork spy', 'CNS').
card_image_name('cogwork spy'/'CNS', 'cogwork spy').
card_uid('cogwork spy'/'CNS', 'CNS:Cogwork Spy:cogwork spy').
card_rarity('cogwork spy'/'CNS', 'Common').
card_artist('cogwork spy'/'CNS', 'Tomasz Jedruszek').
card_number('cogwork spy'/'CNS', '59').
card_flavor_text('cogwork spy'/'CNS', 'Paliano\'s creations provide ample opportunities to plot the downfall of a neighbor.').
card_multiverse_id('cogwork spy'/'CNS', '382234').

card_in_set('cogwork tracker', 'CNS').
card_original_type('cogwork tracker'/'CNS', 'Artifact Creature — Hound Construct').
card_original_text('cogwork tracker'/'CNS', 'Reveal Cogwork Tracker as you draft it and note the player who passed it to you.\nCogwork Tracker attacks each turn if able.\nCogwork Tracker attacks a player you noted for cards named Cogwork Tracker each turn if able.').
card_first_print('cogwork tracker', 'CNS').
card_image_name('cogwork tracker'/'CNS', 'cogwork tracker').
card_uid('cogwork tracker'/'CNS', 'CNS:Cogwork Tracker:cogwork tracker').
card_rarity('cogwork tracker'/'CNS', 'Uncommon').
card_artist('cogwork tracker'/'CNS', 'Alex Horley-Orlandelli').
card_number('cogwork tracker'/'CNS', '60').
card_multiverse_id('cogwork tracker'/'CNS', '382235').

card_in_set('compulsive research', 'CNS').
card_original_type('compulsive research'/'CNS', 'Sorcery').
card_original_text('compulsive research'/'CNS', 'Target player draws three cards. Then that player discards two cards unless he or she discards a land card.').
card_image_name('compulsive research'/'CNS', 'compulsive research').
card_uid('compulsive research'/'CNS', 'CNS:Compulsive Research:compulsive research').
card_rarity('compulsive research'/'CNS', 'Common').
card_artist('compulsive research'/'CNS', 'Michael Sutfin').
card_number('compulsive research'/'CNS', '93').
card_flavor_text('compulsive research'/'CNS', '\"Four parts molten bronze, yes . . . one part frozen mercury, yes, yes . . . but then what?\"').
card_multiverse_id('compulsive research'/'CNS', '382236').

card_in_set('copperhorn scout', 'CNS').
card_original_type('copperhorn scout'/'CNS', 'Creature — Elf Scout').
card_original_text('copperhorn scout'/'CNS', 'Whenever Copperhorn Scout attacks, untap each other creature you control.').
card_image_name('copperhorn scout'/'CNS', 'copperhorn scout').
card_uid('copperhorn scout'/'CNS', 'CNS:Copperhorn Scout:copperhorn scout').
card_rarity('copperhorn scout'/'CNS', 'Common').
card_artist('copperhorn scout'/'CNS', 'Shelly Wan').
card_number('copperhorn scout'/'CNS', '160').
card_flavor_text('copperhorn scout'/'CNS', 'Fangren spur may sound louder, but Viridian resurgents claim that the horn of the vorrac creates a truer, more inspiring note.').
card_multiverse_id('copperhorn scout'/'CNS', '382237').

card_in_set('council guardian', 'CNS').
card_original_type('council guardian'/'CNS', 'Creature — Giant Soldier').
card_original_text('council guardian'/'CNS', 'Will of the council — When Council Guardian enters the battlefield, starting with you, each player votes for blue, black, red, or green. Council Guardian gains protection from each color with the most votes or tied for most votes.').
card_first_print('council guardian', 'CNS').
card_image_name('council guardian'/'CNS', 'council guardian').
card_uid('council guardian'/'CNS', 'CNS:Council Guardian:council guardian').
card_rarity('council guardian'/'CNS', 'Uncommon').
card_artist('council guardian'/'CNS', 'Volkan Baga').
card_number('council guardian'/'CNS', '15').
card_multiverse_id('council guardian'/'CNS', '382238').

card_in_set('council\'s judgment', 'CNS').
card_original_type('council\'s judgment'/'CNS', 'Sorcery').
card_original_text('council\'s judgment'/'CNS', 'Will of the council — Starting with you, each player votes for a nonland permanent you don\'t control. Exile each permanent with the most votes or tied for most votes.').
card_first_print('council\'s judgment', 'CNS').
card_image_name('council\'s judgment'/'CNS', 'council\'s judgment').
card_uid('council\'s judgment'/'CNS', 'CNS:Council\'s Judgment:council\'s judgment').
card_rarity('council\'s judgment'/'CNS', 'Rare').
card_artist('council\'s judgment'/'CNS', 'Kev Walker').
card_number('council\'s judgment'/'CNS', '16').
card_flavor_text('council\'s judgment'/'CNS', 'When its power is threatened, the council speaks with a unified voice.').
card_multiverse_id('council\'s judgment'/'CNS', '382239').

card_in_set('courier hawk', 'CNS').
card_original_type('courier hawk'/'CNS', 'Creature — Bird').
card_original_text('courier hawk'/'CNS', 'Flying, vigilance').
card_image_name('courier hawk'/'CNS', 'courier hawk').
card_uid('courier hawk'/'CNS', 'CNS:Courier Hawk:courier hawk').
card_rarity('courier hawk'/'CNS', 'Common').
card_artist('courier hawk'/'CNS', 'Mark Poole').
card_number('courier hawk'/'CNS', '68').
card_flavor_text('courier hawk'/'CNS', 'The Orzhov started using hawks as messengers in order to bypass the many toll roads and bridges that dot Ravnica. Soon the service became one of their most profitable businesses.').
card_multiverse_id('courier hawk'/'CNS', '382240').

card_in_set('crookclaw transmuter', 'CNS').
card_original_type('crookclaw transmuter'/'CNS', 'Creature — Bird Wizard').
card_original_text('crookclaw transmuter'/'CNS', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Crookclaw Transmuter enters the battlefield, switch target creature\'s power and toughness until end of turn.').
card_image_name('crookclaw transmuter'/'CNS', 'crookclaw transmuter').
card_uid('crookclaw transmuter'/'CNS', 'CNS:Crookclaw Transmuter:crookclaw transmuter').
card_rarity('crookclaw transmuter'/'CNS', 'Common').
card_artist('crookclaw transmuter'/'CNS', 'Ron Spencer').
card_number('crookclaw transmuter'/'CNS', '94').
card_multiverse_id('crookclaw transmuter'/'CNS', '382241').

card_in_set('custodi soulbinders', 'CNS').
card_original_type('custodi soulbinders'/'CNS', 'Creature — Human Cleric').
card_original_text('custodi soulbinders'/'CNS', 'Custodi Soulbinders enters the battlefield with X +1/+1 counters on it, where X is the number of other creatures on the battlefield.\n{2}{W}, Remove a +1/+1 counter from Custodi Soulbinders: Put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('custodi soulbinders', 'CNS').
card_image_name('custodi soulbinders'/'CNS', 'custodi soulbinders').
card_uid('custodi soulbinders'/'CNS', 'CNS:Custodi Soulbinders:custodi soulbinders').
card_rarity('custodi soulbinders'/'CNS', 'Rare').
card_artist('custodi soulbinders'/'CNS', 'Karla Ortiz').
card_number('custodi soulbinders'/'CNS', '17').
card_multiverse_id('custodi soulbinders'/'CNS', '382242').

card_in_set('custodi squire', 'CNS').
card_original_type('custodi squire'/'CNS', 'Creature — Spirit Cleric').
card_original_text('custodi squire'/'CNS', 'Flying\nWill of the council — When Custodi Squire enters the battlefield, starting with you, each player votes for an artifact, creature, or enchantment card in your graveyard. Return each card with the most votes or tied for most votes to your hand.').
card_first_print('custodi squire', 'CNS').
card_image_name('custodi squire'/'CNS', 'custodi squire').
card_uid('custodi squire'/'CNS', 'CNS:Custodi Squire:custodi squire').
card_rarity('custodi squire'/'CNS', 'Common').
card_artist('custodi squire'/'CNS', 'Alex Horley-Orlandelli').
card_number('custodi squire'/'CNS', '18').
card_multiverse_id('custodi squire'/'CNS', '382243').

card_in_set('dack fayden', 'CNS').
card_original_type('dack fayden'/'CNS', 'Planeswalker — Dack').
card_original_text('dack fayden'/'CNS', '+1: Target player draws two cards, then discards two cards.\n-2: Gain control of target artifact.\n-6: You get an emblem with \"Whenever you cast a spell that targets one or more permanents, gain control of those permanents.\"').
card_first_print('dack fayden', 'CNS').
card_image_name('dack fayden'/'CNS', 'dack fayden').
card_uid('dack fayden'/'CNS', 'CNS:Dack Fayden:dack fayden').
card_rarity('dack fayden'/'CNS', 'Mythic Rare').
card_artist('dack fayden'/'CNS', 'Eric Deschamps').
card_number('dack fayden'/'CNS', '42').
card_multiverse_id('dack fayden'/'CNS', '382244').

card_in_set('dack\'s duplicate', 'CNS').
card_original_type('dack\'s duplicate'/'CNS', 'Creature — Shapeshifter').
card_original_text('dack\'s duplicate'/'CNS', 'You may have Dack\'s Duplicate enter the battlefield as a copy of any creature on the battlefield except it gains haste and dethrone. (Whenever it attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_first_print('dack\'s duplicate', 'CNS').
card_image_name('dack\'s duplicate'/'CNS', 'dack\'s duplicate').
card_uid('dack\'s duplicate'/'CNS', 'CNS:Dack\'s Duplicate:dack\'s duplicate').
card_rarity('dack\'s duplicate'/'CNS', 'Rare').
card_artist('dack\'s duplicate'/'CNS', 'Karl Kopinski').
card_number('dack\'s duplicate'/'CNS', '43').
card_multiverse_id('dack\'s duplicate'/'CNS', '382245').

card_in_set('deal broker', 'CNS').
card_original_type('deal broker'/'CNS', 'Artifact Creature — Construct').
card_original_text('deal broker'/'CNS', 'Draft Deal Broker face up.\nImmediately after the draft, you may reveal a card in your card pool. Each other player may offer you one card in his or her card pool in exchange. You may accept any one offer.\n{T}: Draw a card, then discard a card.').
card_first_print('deal broker', 'CNS').
card_image_name('deal broker'/'CNS', 'deal broker').
card_uid('deal broker'/'CNS', 'CNS:Deal Broker:deal broker').
card_rarity('deal broker'/'CNS', 'Rare').
card_artist('deal broker'/'CNS', 'Cliff Childs').
card_number('deal broker'/'CNS', '61').
card_multiverse_id('deal broker'/'CNS', '382246').

card_in_set('deathforge shaman', 'CNS').
card_original_type('deathforge shaman'/'CNS', 'Creature — Ogre Shaman').
card_original_text('deathforge shaman'/'CNS', 'Multikicker {R} (You may pay an additional {R} any number of times as you cast this spell.)\nWhen Deathforge Shaman enters the battlefield, it deals damage to target player equal to twice the number of times it was kicked.').
card_image_name('deathforge shaman'/'CNS', 'deathforge shaman').
card_uid('deathforge shaman'/'CNS', 'CNS:Deathforge Shaman:deathforge shaman').
card_rarity('deathforge shaman'/'CNS', 'Uncommon').
card_artist('deathforge shaman'/'CNS', 'Dave Kendall').
card_number('deathforge shaman'/'CNS', '141').
card_multiverse_id('deathforge shaman'/'CNS', '382247').

card_in_set('deathreap ritual', 'CNS').
card_original_type('deathreap ritual'/'CNS', 'Enchantment').
card_original_text('deathreap ritual'/'CNS', 'Morbid — At the beginning of each end step, if a creature died this turn, you may draw a card.').
card_first_print('deathreap ritual', 'CNS').
card_image_name('deathreap ritual'/'CNS', 'deathreap ritual').
card_uid('deathreap ritual'/'CNS', 'CNS:Deathreap Ritual:deathreap ritual').
card_rarity('deathreap ritual'/'CNS', 'Uncommon').
card_artist('deathreap ritual'/'CNS', 'Steve Argyle').
card_number('deathreap ritual'/'CNS', '44').
card_flavor_text('deathreap ritual'/'CNS', '\"All who set foot in Paliano are pawns in someone\'s play for power.\"\n—Marchesa, the Black Rose').
card_multiverse_id('deathreap ritual'/'CNS', '382248').

card_in_set('deathrender', 'CNS').
card_original_type('deathrender'/'CNS', 'Artifact — Equipment').
card_original_text('deathrender'/'CNS', 'Equipped creature gets +2/+2.\nWhenever equipped creature dies, you may put a creature card from your hand onto the battlefield and attach Deathrender to it.\nEquip {2}').
card_image_name('deathrender'/'CNS', 'deathrender').
card_uid('deathrender'/'CNS', 'CNS:Deathrender:deathrender').
card_rarity('deathrender'/'CNS', 'Rare').
card_artist('deathrender'/'CNS', 'Martina Pilcerova').
card_number('deathrender'/'CNS', '197').
card_multiverse_id('deathrender'/'CNS', '382249').

card_in_set('decimate', 'CNS').
card_original_type('decimate'/'CNS', 'Sorcery').
card_original_text('decimate'/'CNS', 'Destroy target artifact, target creature, target enchantment, and target land.').
card_image_name('decimate'/'CNS', 'decimate').
card_uid('decimate'/'CNS', 'CNS:Decimate:decimate').
card_rarity('decimate'/'CNS', 'Rare').
card_artist('decimate'/'CNS', 'Zoltan Boros').
card_number('decimate'/'CNS', '185').
card_flavor_text('decimate'/'CNS', 'Anarchy comes in many forms: social, individual, Gruul . . .').
card_multiverse_id('decimate'/'CNS', '382250').

card_in_set('dimir doppelganger', 'CNS').
card_original_type('dimir doppelganger'/'CNS', 'Creature — Shapeshifter').
card_original_text('dimir doppelganger'/'CNS', '{1}{U}{B}: Exile target creature card from a graveyard. Dimir Doppelganger becomes a copy of that card and gains this ability.').
card_image_name('dimir doppelganger'/'CNS', 'dimir doppelganger').
card_uid('dimir doppelganger'/'CNS', 'CNS:Dimir Doppelganger:dimir doppelganger').
card_rarity('dimir doppelganger'/'CNS', 'Rare').
card_artist('dimir doppelganger'/'CNS', 'Jim Murray').
card_number('dimir doppelganger'/'CNS', '186').
card_flavor_text('dimir doppelganger'/'CNS', '\"Fear not. Your life will not go unlived.\"').
card_multiverse_id('dimir doppelganger'/'CNS', '382251').

card_in_set('doomed traveler', 'CNS').
card_original_type('doomed traveler'/'CNS', 'Creature — Human Soldier').
card_original_text('doomed traveler'/'CNS', 'When Doomed Traveler dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('doomed traveler'/'CNS', 'doomed traveler').
card_uid('doomed traveler'/'CNS', 'CNS:Doomed Traveler:doomed traveler').
card_rarity('doomed traveler'/'CNS', 'Common').
card_artist('doomed traveler'/'CNS', 'Lars Grant-West').
card_number('doomed traveler'/'CNS', '69').
card_flavor_text('doomed traveler'/'CNS', 'He vowed he would never rest until he reached his destination. He doesn\'t know how right he was.').
card_multiverse_id('doomed traveler'/'CNS', '382252').

card_in_set('double stroke', 'CNS').
card_original_type('double stroke'/'CNS', 'Conspiracy').
card_original_text('double stroke'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nWhenever you cast an instant or sorcery spell with the chosen name, you may copy it. You may choose new targets for the copy.').
card_first_print('double stroke', 'CNS').
card_image_name('double stroke'/'CNS', 'double stroke').
card_uid('double stroke'/'CNS', 'CNS:Double Stroke:double stroke').
card_rarity('double stroke'/'CNS', 'Uncommon').
card_artist('double stroke'/'CNS', 'Christopher Moeller').
card_number('double stroke'/'CNS', '4').
card_multiverse_id('double stroke'/'CNS', '382253').

card_in_set('drakestown forgotten', 'CNS').
card_original_type('drakestown forgotten'/'CNS', 'Creature — Zombie').
card_original_text('drakestown forgotten'/'CNS', 'Drakestown Forgotten enters the battlefield with X +1/+1 counters on it, where X is the number of creature cards in all graveyards.\n{2}{B}, Remove a +1/+1 counter from Drakestown Forgotten: Target creature gets -1/-1 until end of turn.').
card_first_print('drakestown forgotten', 'CNS').
card_image_name('drakestown forgotten'/'CNS', 'drakestown forgotten').
card_uid('drakestown forgotten'/'CNS', 'CNS:Drakestown Forgotten:drakestown forgotten').
card_rarity('drakestown forgotten'/'CNS', 'Rare').
card_artist('drakestown forgotten'/'CNS', 'Steve Prescott').
card_number('drakestown forgotten'/'CNS', '27').
card_multiverse_id('drakestown forgotten'/'CNS', '382254').

card_in_set('dream fracture', 'CNS').
card_original_type('dream fracture'/'CNS', 'Instant').
card_original_text('dream fracture'/'CNS', 'Counter target spell. Its controller draws a card.\nDraw a card.').
card_image_name('dream fracture'/'CNS', 'dream fracture').
card_uid('dream fracture'/'CNS', 'CNS:Dream Fracture:dream fracture').
card_rarity('dream fracture'/'CNS', 'Common').
card_artist('dream fracture'/'CNS', 'Howard Lyon').
card_number('dream fracture'/'CNS', '95').
card_flavor_text('dream fracture'/'CNS', '\"Creation is a paradox. It hatches from its opposite.\"\n—Olka, Mistmeadow witch').
card_multiverse_id('dream fracture'/'CNS', '382255').

card_in_set('echoing courage', 'CNS').
card_original_type('echoing courage'/'CNS', 'Instant').
card_original_text('echoing courage'/'CNS', 'Target creature and all other creatures with the same name as that creature get +2/+2 until end of turn.').
card_image_name('echoing courage'/'CNS', 'echoing courage').
card_uid('echoing courage'/'CNS', 'CNS:Echoing Courage:echoing courage').
card_rarity('echoing courage'/'CNS', 'Common').
card_artist('echoing courage'/'CNS', 'Greg Staples').
card_number('echoing courage'/'CNS', '161').
card_flavor_text('echoing courage'/'CNS', 'A single seed unleashes a flurry of growth.').
card_multiverse_id('echoing courage'/'CNS', '382256').

card_in_set('edric, spymaster of trest', 'CNS').
card_original_type('edric, spymaster of trest'/'CNS', 'Legendary Creature — Elf Rogue').
card_original_text('edric, spymaster of trest'/'CNS', 'Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.').
card_image_name('edric, spymaster of trest'/'CNS', 'edric, spymaster of trest').
card_uid('edric, spymaster of trest'/'CNS', 'CNS:Edric, Spymaster of Trest:edric, spymaster of trest').
card_rarity('edric, spymaster of trest'/'CNS', 'Rare').
card_artist('edric, spymaster of trest'/'CNS', 'Volkan Baga').
card_number('edric, spymaster of trest'/'CNS', '187').
card_flavor_text('edric, spymaster of trest'/'CNS', '\"I am not at liberty to reveal my sources, but I can assure you, the price on your head is high.\"').
card_multiverse_id('edric, spymaster of trest'/'CNS', '382257').

card_in_set('elephant guide', 'CNS').
card_original_type('elephant guide'/'CNS', 'Enchantment — Aura').
card_original_text('elephant guide'/'CNS', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature dies, put a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('elephant guide'/'CNS', 'elephant guide').
card_uid('elephant guide'/'CNS', 'CNS:Elephant Guide:elephant guide').
card_rarity('elephant guide'/'CNS', 'Uncommon').
card_artist('elephant guide'/'CNS', 'Tomasz Jedruszek').
card_number('elephant guide'/'CNS', '162').
card_flavor_text('elephant guide'/'CNS', 'Nature\'s strength outlives the strong.').
card_multiverse_id('elephant guide'/'CNS', '382258').

card_in_set('elvish aberration', 'CNS').
card_original_type('elvish aberration'/'CNS', 'Creature — Elf Mutant').
card_original_text('elvish aberration'/'CNS', '{T}: Add {G}{G}{G} to your mana pool.\nForestcycling {2} ({2}, Discard this card: Search your library for a Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('elvish aberration'/'CNS', 'elvish aberration').
card_uid('elvish aberration'/'CNS', 'CNS:Elvish Aberration:elvish aberration').
card_rarity('elvish aberration'/'CNS', 'Common').
card_artist('elvish aberration'/'CNS', 'Matt Cavotta').
card_number('elvish aberration'/'CNS', '163').
card_multiverse_id('elvish aberration'/'CNS', '382259').

card_in_set('enclave elite', 'CNS').
card_original_type('enclave elite'/'CNS', 'Creature — Merfolk Soldier').
card_original_text('enclave elite'/'CNS', 'Multikicker {1}{U} (You may pay an additional {1}{U} any number of times as you cast this spell.)\nIslandwalk\nEnclave Elite enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('enclave elite'/'CNS', 'enclave elite').
card_uid('enclave elite'/'CNS', 'CNS:Enclave Elite:enclave elite').
card_rarity('enclave elite'/'CNS', 'Common').
card_artist('enclave elite'/'CNS', 'Igor Kieryluk').
card_number('enclave elite'/'CNS', '96').
card_multiverse_id('enclave elite'/'CNS', '382260').

card_in_set('enraged revolutionary', 'CNS').
card_original_type('enraged revolutionary'/'CNS', 'Creature — Human Warrior').
card_original_text('enraged revolutionary'/'CNS', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_first_print('enraged revolutionary', 'CNS').
card_image_name('enraged revolutionary'/'CNS', 'enraged revolutionary').
card_uid('enraged revolutionary'/'CNS', 'CNS:Enraged Revolutionary:enraged revolutionary').
card_rarity('enraged revolutionary'/'CNS', 'Common').
card_artist('enraged revolutionary'/'CNS', 'Drew Baker').
card_number('enraged revolutionary'/'CNS', '31').
card_flavor_text('enraged revolutionary'/'CNS', 'When every right is voted away, only rage remains.').
card_multiverse_id('enraged revolutionary'/'CNS', '382261').

card_in_set('exploration', 'CNS').
card_original_type('exploration'/'CNS', 'Enchantment').
card_original_text('exploration'/'CNS', 'You may play an additional land on each of your turns.').
card_image_name('exploration'/'CNS', 'exploration').
card_uid('exploration'/'CNS', 'CNS:Exploration:exploration').
card_rarity('exploration'/'CNS', 'Rare').
card_artist('exploration'/'CNS', 'Florian de Gesincourt').
card_number('exploration'/'CNS', '164').
card_flavor_text('exploration'/'CNS', '\"Only when we journey into the unknown can we find the impossible.\"\n—Cyrul the Wanderer').
card_multiverse_id('exploration'/'CNS', '382262').

card_in_set('explorer\'s scope', 'CNS').
card_original_type('explorer\'s scope'/'CNS', 'Artifact — Equipment').
card_original_text('explorer\'s scope'/'CNS', 'Whenever equipped creature attacks, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield tapped.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('explorer\'s scope'/'CNS', 'explorer\'s scope').
card_uid('explorer\'s scope'/'CNS', 'CNS:Explorer\'s Scope:explorer\'s scope').
card_rarity('explorer\'s scope'/'CNS', 'Uncommon').
card_artist('explorer\'s scope'/'CNS', 'Vincent Proce').
card_number('explorer\'s scope'/'CNS', '198').
card_multiverse_id('explorer\'s scope'/'CNS', '382263').

card_in_set('extract from darkness', 'CNS').
card_original_type('extract from darkness'/'CNS', 'Sorcery').
card_original_text('extract from darkness'/'CNS', 'Each player puts the top two cards of his or her library into his or her graveyard. Then put a creature card from a graveyard onto the battlefield under your control.').
card_first_print('extract from darkness', 'CNS').
card_image_name('extract from darkness'/'CNS', 'extract from darkness').
card_uid('extract from darkness'/'CNS', 'CNS:Extract from Darkness:extract from darkness').
card_rarity('extract from darkness'/'CNS', 'Uncommon').
card_artist('extract from darkness'/'CNS', 'Dallas Williams').
card_number('extract from darkness'/'CNS', '45').
card_flavor_text('extract from darkness'/'CNS', 'If you want to wreak havoc above, sometimes it\'s best to look below.').
card_multiverse_id('extract from darkness'/'CNS', '382264').

card_in_set('fact or fiction', 'CNS').
card_original_type('fact or fiction'/'CNS', 'Instant').
card_original_text('fact or fiction'/'CNS', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'CNS', 'fact or fiction').
card_uid('fact or fiction'/'CNS', 'CNS:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'CNS', 'Uncommon').
card_artist('fact or fiction'/'CNS', 'Matt Cavotta').
card_number('fact or fiction'/'CNS', '97').
card_flavor_text('fact or fiction'/'CNS', '\"Try to pretend like you understand what\'s important.\"').
card_multiverse_id('fact or fiction'/'CNS', '382265').

card_in_set('favorable winds', 'CNS').
card_original_type('favorable winds'/'CNS', 'Enchantment').
card_original_text('favorable winds'/'CNS', 'Creatures you control with flying get +1/+1.').
card_image_name('favorable winds'/'CNS', 'favorable winds').
card_uid('favorable winds'/'CNS', 'CNS:Favorable Winds:favorable winds').
card_rarity('favorable winds'/'CNS', 'Uncommon').
card_artist('favorable winds'/'CNS', 'Winona Nelson').
card_number('favorable winds'/'CNS', '98').
card_flavor_text('favorable winds'/'CNS', 'Long thought to be extinct, flocks of gryffs reappeared with Avacyn\'s return.').
card_multiverse_id('favorable winds'/'CNS', '382266').

card_in_set('fires of yavimaya', 'CNS').
card_original_type('fires of yavimaya'/'CNS', 'Enchantment').
card_original_text('fires of yavimaya'/'CNS', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'CNS', 'fires of yavimaya').
card_uid('fires of yavimaya'/'CNS', 'CNS:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'CNS', 'Uncommon').
card_artist('fires of yavimaya'/'CNS', 'Izzy').
card_number('fires of yavimaya'/'CNS', '188').
card_flavor_text('fires of yavimaya'/'CNS', 'Yavimaya lights the quickest path to battle.').
card_multiverse_id('fires of yavimaya'/'CNS', '382267').

card_in_set('fireshrieker', 'CNS').
card_original_type('fireshrieker'/'CNS', 'Artifact — Equipment').
card_original_text('fireshrieker'/'CNS', 'Equipped creature has double strike. (It deals both first-strike and regular combat damage.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('fireshrieker'/'CNS', 'fireshrieker').
card_uid('fireshrieker'/'CNS', 'CNS:Fireshrieker:fireshrieker').
card_rarity('fireshrieker'/'CNS', 'Uncommon').
card_artist('fireshrieker'/'CNS', 'Christopher Moeller').
card_number('fireshrieker'/'CNS', '199').
card_multiverse_id('fireshrieker'/'CNS', '382268').

card_in_set('flamewright', 'CNS').
card_original_type('flamewright'/'CNS', 'Creature — Human Artificer').
card_original_text('flamewright'/'CNS', '{1}, {T}: Put a 1/1 colorless Construct artifact creature token with defender onto the battlefield.\n{T}, Sacrifice a creature with defender: Flamewright deals 1 damage to target creature or player.').
card_first_print('flamewright', 'CNS').
card_image_name('flamewright'/'CNS', 'flamewright').
card_uid('flamewright'/'CNS', 'CNS:Flamewright:flamewright').
card_rarity('flamewright'/'CNS', 'Uncommon').
card_artist('flamewright'/'CNS', 'Mathias Kollros').
card_number('flamewright'/'CNS', '46').
card_flavor_text('flamewright'/'CNS', 'Those who can\'t conspire, create.').
card_multiverse_id('flamewright'/'CNS', '382269').

card_in_set('flaring flame-kin', 'CNS').
card_original_type('flaring flame-kin'/'CNS', 'Creature — Elemental Warrior').
card_original_text('flaring flame-kin'/'CNS', 'As long as Flaring Flame-Kin is enchanted, it gets +2/+2, has trample, and has \"{R}: Flaring Flame-Kin gets +1/+0 until end of turn.\"').
card_image_name('flaring flame-kin'/'CNS', 'flaring flame-kin').
card_uid('flaring flame-kin'/'CNS', 'CNS:Flaring Flame-Kin:flaring flame-kin').
card_rarity('flaring flame-kin'/'CNS', 'Uncommon').
card_artist('flaring flame-kin'/'CNS', 'Brian Hagan').
card_number('flaring flame-kin'/'CNS', '142').
card_flavor_text('flaring flame-kin'/'CNS', 'A flame-kin is always formidable, but feeding its fire with a sorcerous gift grants it the power to devastate armies.').
card_multiverse_id('flaring flame-kin'/'CNS', '382270').

card_in_set('flowstone blade', 'CNS').
card_original_type('flowstone blade'/'CNS', 'Enchantment — Aura').
card_original_text('flowstone blade'/'CNS', 'Enchant creature\n{R}: Enchanted creature gets +1/-1 until end of turn.').
card_image_name('flowstone blade'/'CNS', 'flowstone blade').
card_uid('flowstone blade'/'CNS', 'CNS:Flowstone Blade:flowstone blade').
card_rarity('flowstone blade'/'CNS', 'Common').
card_artist('flowstone blade'/'CNS', 'Allen Williams').
card_number('flowstone blade'/'CNS', '143').
card_flavor_text('flowstone blade'/'CNS', '\"Remember the fable of the elf who came upon a cave of gold. In trying to free the largest piece, she was crushed by its weight.\"\n—Karn').
card_multiverse_id('flowstone blade'/'CNS', '382271').

card_in_set('galvanic juggernaut', 'CNS').
card_original_type('galvanic juggernaut'/'CNS', 'Artifact Creature — Juggernaut').
card_original_text('galvanic juggernaut'/'CNS', 'Galvanic Juggernaut attacks each turn if able.\nGalvanic Juggernaut doesn\'t untap during your untap step.\nWhenever another creature dies, untap Galvanic Juggernaut.').
card_image_name('galvanic juggernaut'/'CNS', 'galvanic juggernaut').
card_uid('galvanic juggernaut'/'CNS', 'CNS:Galvanic Juggernaut:galvanic juggernaut').
card_rarity('galvanic juggernaut'/'CNS', 'Uncommon').
card_artist('galvanic juggernaut'/'CNS', 'Lucas Graciano').
card_number('galvanic juggernaut'/'CNS', '200').
card_multiverse_id('galvanic juggernaut'/'CNS', '382272').

card_in_set('gamekeeper', 'CNS').
card_original_type('gamekeeper'/'CNS', 'Creature — Elf').
card_original_text('gamekeeper'/'CNS', 'When Gamekeeper dies, you may exile it. If you do, reveal cards from the top of your library until you reveal a creature card. Put that card onto the battlefield and put all other cards revealed this way into your graveyard.').
card_image_name('gamekeeper'/'CNS', 'gamekeeper').
card_uid('gamekeeper'/'CNS', 'CNS:Gamekeeper:gamekeeper').
card_rarity('gamekeeper'/'CNS', 'Uncommon').
card_artist('gamekeeper'/'CNS', 'Scott Hampton').
card_number('gamekeeper'/'CNS', '165').
card_multiverse_id('gamekeeper'/'CNS', '382273').

card_in_set('glimmerpoint stag', 'CNS').
card_original_type('glimmerpoint stag'/'CNS', 'Creature — Elk').
card_original_text('glimmerpoint stag'/'CNS', 'Vigilance\nWhen Glimmerpoint Stag enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('glimmerpoint stag'/'CNS', 'glimmerpoint stag').
card_uid('glimmerpoint stag'/'CNS', 'CNS:Glimmerpoint Stag:glimmerpoint stag').
card_rarity('glimmerpoint stag'/'CNS', 'Uncommon').
card_artist('glimmerpoint stag'/'CNS', 'Ryan Pancoast').
card_number('glimmerpoint stag'/'CNS', '70').
card_multiverse_id('glimmerpoint stag'/'CNS', '382274').

card_in_set('gnarlid pack', 'CNS').
card_original_type('gnarlid pack'/'CNS', 'Creature — Beast').
card_original_text('gnarlid pack'/'CNS', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nGnarlid Pack enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('gnarlid pack'/'CNS', 'gnarlid pack').
card_uid('gnarlid pack'/'CNS', 'CNS:Gnarlid Pack:gnarlid pack').
card_rarity('gnarlid pack'/'CNS', 'Common').
card_artist('gnarlid pack'/'CNS', 'Johann Bodin').
card_number('gnarlid pack'/'CNS', '166').
card_multiverse_id('gnarlid pack'/'CNS', '382275').

card_in_set('grenzo\'s cutthroat', 'CNS').
card_original_type('grenzo\'s cutthroat'/'CNS', 'Creature — Goblin Rogue').
card_original_text('grenzo\'s cutthroat'/'CNS', 'First strike\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_first_print('grenzo\'s cutthroat', 'CNS').
card_image_name('grenzo\'s cutthroat'/'CNS', 'grenzo\'s cutthroat').
card_uid('grenzo\'s cutthroat'/'CNS', 'CNS:Grenzo\'s Cutthroat:grenzo\'s cutthroat').
card_rarity('grenzo\'s cutthroat'/'CNS', 'Common').
card_artist('grenzo\'s cutthroat'/'CNS', 'Svetlin Velinov').
card_number('grenzo\'s cutthroat'/'CNS', '32').
card_flavor_text('grenzo\'s cutthroat'/'CNS', '\"Their guards are soft, ready to welcome a sharp dagger.\"').
card_multiverse_id('grenzo\'s cutthroat'/'CNS', '382277').

card_in_set('grenzo\'s rebuttal', 'CNS').
card_original_type('grenzo\'s rebuttal'/'CNS', 'Sorcery').
card_original_text('grenzo\'s rebuttal'/'CNS', 'Put a 4/4 red Ogre creature token onto the battlefield. Starting with you, each player chooses an artifact, a creature, and a land from among the permanents controlled by the player to his or her left. Destroy each permanent chosen this way.').
card_first_print('grenzo\'s rebuttal', 'CNS').
card_image_name('grenzo\'s rebuttal'/'CNS', 'grenzo\'s rebuttal').
card_uid('grenzo\'s rebuttal'/'CNS', 'CNS:Grenzo\'s Rebuttal:grenzo\'s rebuttal').
card_rarity('grenzo\'s rebuttal'/'CNS', 'Rare').
card_artist('grenzo\'s rebuttal'/'CNS', 'Dave Kendall').
card_number('grenzo\'s rebuttal'/'CNS', '33').
card_multiverse_id('grenzo\'s rebuttal'/'CNS', '382278').

card_in_set('grenzo, dungeon warden', 'CNS').
card_original_type('grenzo, dungeon warden'/'CNS', 'Legendary Creature — Goblin Rogue').
card_original_text('grenzo, dungeon warden'/'CNS', 'Grenzo, Dungeon Warden enters the battlefield with X +1/+1 counters on it.\n{2}: Put the bottom card of your library into your graveyard. If it\'s a creature card with power less than or equal to Grenzo\'s power, put it onto the battlefield.').
card_first_print('grenzo, dungeon warden', 'CNS').
card_image_name('grenzo, dungeon warden'/'CNS', 'grenzo, dungeon warden').
card_uid('grenzo, dungeon warden'/'CNS', 'CNS:Grenzo, Dungeon Warden:grenzo, dungeon warden').
card_rarity('grenzo, dungeon warden'/'CNS', 'Rare').
card_artist('grenzo, dungeon warden'/'CNS', 'Lucas Graciano').
card_number('grenzo, dungeon warden'/'CNS', '47').
card_multiverse_id('grenzo, dungeon warden'/'CNS', '382276').

card_in_set('grixis illusionist', 'CNS').
card_original_type('grixis illusionist'/'CNS', 'Creature — Human Wizard').
card_original_text('grixis illusionist'/'CNS', '{T}: Target land you control becomes the basic land type of your choice until end of turn.').
card_image_name('grixis illusionist'/'CNS', 'grixis illusionist').
card_uid('grixis illusionist'/'CNS', 'CNS:Grixis Illusionist:grixis illusionist').
card_rarity('grixis illusionist'/'CNS', 'Common').
card_artist('grixis illusionist'/'CNS', 'Mark Tedin').
card_number('grixis illusionist'/'CNS', '99').
card_flavor_text('grixis illusionist'/'CNS', '\"It\'s simple, really. If they can\'t find us, they can\'t kill us.\"').
card_multiverse_id('grixis illusionist'/'CNS', '382279').

card_in_set('grudge keeper', 'CNS').
card_original_type('grudge keeper'/'CNS', 'Creature — Zombie Wizard').
card_original_text('grudge keeper'/'CNS', 'Whenever players finish voting, each opponent who voted for a choice you didn\'t vote for loses 2 life.').
card_first_print('grudge keeper', 'CNS').
card_image_name('grudge keeper'/'CNS', 'grudge keeper').
card_uid('grudge keeper'/'CNS', 'CNS:Grudge Keeper:grudge keeper').
card_rarity('grudge keeper'/'CNS', 'Common').
card_artist('grudge keeper'/'CNS', 'Ryan Barger').
card_number('grudge keeper'/'CNS', '28').
card_flavor_text('grudge keeper'/'CNS', '\"Your defiance is noted.\"').
card_multiverse_id('grudge keeper'/'CNS', '382280').

card_in_set('guardian zendikon', 'CNS').
card_original_type('guardian zendikon'/'CNS', 'Enchantment — Aura').
card_original_text('guardian zendikon'/'CNS', 'Enchant land\nEnchanted land is a 2/6 white Wall creature with defender. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_image_name('guardian zendikon'/'CNS', 'guardian zendikon').
card_uid('guardian zendikon'/'CNS', 'CNS:Guardian Zendikon:guardian zendikon').
card_rarity('guardian zendikon'/'CNS', 'Common').
card_artist('guardian zendikon'/'CNS', 'John Avon').
card_number('guardian zendikon'/'CNS', '71').
card_multiverse_id('guardian zendikon'/'CNS', '382281').

card_in_set('heartless hidetsugu', 'CNS').
card_original_type('heartless hidetsugu'/'CNS', 'Legendary Creature — Ogre Shaman').
card_original_text('heartless hidetsugu'/'CNS', '{T}: Heartless Hidetsugu deals damage to each player equal to half that player\'s life total, rounded down.').
card_image_name('heartless hidetsugu'/'CNS', 'heartless hidetsugu').
card_uid('heartless hidetsugu'/'CNS', 'CNS:Heartless Hidetsugu:heartless hidetsugu').
card_rarity('heartless hidetsugu'/'CNS', 'Rare').
card_artist('heartless hidetsugu'/'CNS', 'Carl Critchlow').
card_number('heartless hidetsugu'/'CNS', '144').
card_flavor_text('heartless hidetsugu'/'CNS', 'Hidetsugu held over a dozen oni in blood oath. At his touch, rocks scorched. At his word, cities burned.').
card_multiverse_id('heartless hidetsugu'/'CNS', '382282').

card_in_set('heckling fiends', 'CNS').
card_original_type('heckling fiends'/'CNS', 'Creature — Devil').
card_original_text('heckling fiends'/'CNS', '{2}{R}: Target creature attacks this turn if able.').
card_image_name('heckling fiends'/'CNS', 'heckling fiends').
card_uid('heckling fiends'/'CNS', 'CNS:Heckling Fiends:heckling fiends').
card_rarity('heckling fiends'/'CNS', 'Uncommon').
card_artist('heckling fiends'/'CNS', 'Clint Cearley').
card_number('heckling fiends'/'CNS', '145').
card_flavor_text('heckling fiends'/'CNS', 'Their language may be a cacophonous agglomeration of chittering snorts and mad barking, but the message they send is all too clear.').
card_multiverse_id('heckling fiends'/'CNS', '382283').

card_in_set('howling wolf', 'CNS').
card_original_type('howling wolf'/'CNS', 'Creature — Wolf').
card_original_text('howling wolf'/'CNS', 'When Howling Wolf enters the battlefield, you may search your library for up to three cards named Howling Wolf, reveal them, and put them into your hand. If you do, shuffle your library.').
card_image_name('howling wolf'/'CNS', 'howling wolf').
card_uid('howling wolf'/'CNS', 'CNS:Howling Wolf:howling wolf').
card_rarity('howling wolf'/'CNS', 'Common').
card_artist('howling wolf'/'CNS', 'Nils Hamm').
card_number('howling wolf'/'CNS', '167').
card_multiverse_id('howling wolf'/'CNS', '382284').

card_in_set('hunger of the howlpack', 'CNS').
card_original_type('hunger of the howlpack'/'CNS', 'Instant').
card_original_text('hunger of the howlpack'/'CNS', 'Put a +1/+1 counter on target creature.\nMorbid — Put three +1/+1 counters on that creature instead if a creature died this turn.').
card_image_name('hunger of the howlpack'/'CNS', 'hunger of the howlpack').
card_uid('hunger of the howlpack'/'CNS', 'CNS:Hunger of the Howlpack:hunger of the howlpack').
card_rarity('hunger of the howlpack'/'CNS', 'Common').
card_artist('hunger of the howlpack'/'CNS', 'Nils Hamm').
card_number('hunger of the howlpack'/'CNS', '168').
card_flavor_text('hunger of the howlpack'/'CNS', '\"Werewolves are an unholy mix of a predator\'s instinct and a human\'s hatred.\"\n—Alena, trapper of Kessig').
card_multiverse_id('hunger of the howlpack'/'CNS', '382285').

card_in_set('hydra omnivore', 'CNS').
card_original_type('hydra omnivore'/'CNS', 'Creature — Hydra').
card_original_text('hydra omnivore'/'CNS', 'Whenever Hydra Omnivore deals combat damage to an opponent, it deals that much damage to each other opponent.').
card_image_name('hydra omnivore'/'CNS', 'hydra omnivore').
card_uid('hydra omnivore'/'CNS', 'CNS:Hydra Omnivore:hydra omnivore').
card_rarity('hydra omnivore'/'CNS', 'Mythic Rare').
card_artist('hydra omnivore'/'CNS', 'Svetlin Velinov').
card_number('hydra omnivore'/'CNS', '169').
card_flavor_text('hydra omnivore'/'CNS', '\"We should learn from the hydra. It never plays favorites; it devours everyone equally.\"\n—Thrass, elder druid').
card_multiverse_id('hydra omnivore'/'CNS', '382286').

card_in_set('ignition team', 'CNS').
card_original_type('ignition team'/'CNS', 'Creature — Goblin Warrior').
card_original_text('ignition team'/'CNS', 'Ignition Team enters the battlefield with X +1/+1 counters on it, where X is the number of tapped lands on the battlefield.\n{2}{R}, Remove a +1/+1 counter from Ignition Team: Target land becomes a 4/4 red Elemental creature until end of turn. It\'s still a land.').
card_first_print('ignition team', 'CNS').
card_image_name('ignition team'/'CNS', 'ignition team').
card_uid('ignition team'/'CNS', 'CNS:Ignition Team:ignition team').
card_rarity('ignition team'/'CNS', 'Rare').
card_artist('ignition team'/'CNS', 'Karl Kopinski').
card_number('ignition team'/'CNS', '34').
card_multiverse_id('ignition team'/'CNS', '382287').

card_in_set('ill-gotten gains', 'CNS').
card_original_type('ill-gotten gains'/'CNS', 'Sorcery').
card_original_text('ill-gotten gains'/'CNS', 'Exile Ill-Gotten Gains. Each player discards his or her hand, then returns up to three cards from his or her graveyard to his or her hand.').
card_image_name('ill-gotten gains'/'CNS', 'ill-gotten gains').
card_uid('ill-gotten gains'/'CNS', 'CNS:Ill-Gotten Gains:ill-gotten gains').
card_rarity('ill-gotten gains'/'CNS', 'Rare').
card_artist('ill-gotten gains'/'CNS', 'Greg Staples').
card_number('ill-gotten gains'/'CNS', '114').
card_flavor_text('ill-gotten gains'/'CNS', 'Urza thought it a crusade. Xantcha knew it was a robbery.').
card_multiverse_id('ill-gotten gains'/'CNS', '382288').

card_in_set('immediate action', 'CNS').
card_original_type('immediate action'/'CNS', 'Conspiracy').
card_original_text('immediate action'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nCreatures you control with the chosen name have haste.').
card_first_print('immediate action', 'CNS').
card_image_name('immediate action'/'CNS', 'immediate action').
card_uid('immediate action'/'CNS', 'CNS:Immediate Action:immediate action').
card_rarity('immediate action'/'CNS', 'Common').
card_artist('immediate action'/'CNS', 'Karl Kopinski').
card_number('immediate action'/'CNS', '5').
card_multiverse_id('immediate action'/'CNS', '382289').

card_in_set('infectious horror', 'CNS').
card_original_type('infectious horror'/'CNS', 'Creature — Zombie Horror').
card_original_text('infectious horror'/'CNS', 'Whenever Infectious Horror attacks, each opponent loses 2 life.').
card_image_name('infectious horror'/'CNS', 'infectious horror').
card_uid('infectious horror'/'CNS', 'CNS:Infectious Horror:infectious horror').
card_rarity('infectious horror'/'CNS', 'Common').
card_artist('infectious horror'/'CNS', 'Pete Venters').
card_number('infectious horror'/'CNS', '115').
card_flavor_text('infectious horror'/'CNS', 'Not once in the history of Grixis has anyone died of old age.').
card_multiverse_id('infectious horror'/'CNS', '382290').

card_in_set('intangible virtue', 'CNS').
card_original_type('intangible virtue'/'CNS', 'Enchantment').
card_original_text('intangible virtue'/'CNS', 'Creature tokens you control get +1/+1 and have vigilance.').
card_image_name('intangible virtue'/'CNS', 'intangible virtue').
card_uid('intangible virtue'/'CNS', 'CNS:Intangible Virtue:intangible virtue').
card_rarity('intangible virtue'/'CNS', 'Uncommon').
card_artist('intangible virtue'/'CNS', 'Clint Cearley').
card_number('intangible virtue'/'CNS', '72').
card_flavor_text('intangible virtue'/'CNS', 'In life, they were a motley crew: farmers, lords, cutpurses, priests. In death, they are united in singular, benevolent purpose.').
card_multiverse_id('intangible virtue'/'CNS', '382291').

card_in_set('iterative analysis', 'CNS').
card_original_type('iterative analysis'/'CNS', 'Conspiracy').
card_original_text('iterative analysis'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nWhenever you cast an instant or sorcery spell with the chosen name, you may draw a card.').
card_first_print('iterative analysis', 'CNS').
card_image_name('iterative analysis'/'CNS', 'iterative analysis').
card_uid('iterative analysis'/'CNS', 'CNS:Iterative Analysis:iterative analysis').
card_rarity('iterative analysis'/'CNS', 'Uncommon').
card_artist('iterative analysis'/'CNS', 'Winona Nelson').
card_number('iterative analysis'/'CNS', '6').
card_multiverse_id('iterative analysis'/'CNS', '382292').

card_in_set('jetting glasskite', 'CNS').
card_original_type('jetting glasskite'/'CNS', 'Creature — Spirit').
card_original_text('jetting glasskite'/'CNS', 'Flying\nWhenever Jetting Glasskite becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.').
card_image_name('jetting glasskite'/'CNS', 'jetting glasskite').
card_uid('jetting glasskite'/'CNS', 'CNS:Jetting Glasskite:jetting glasskite').
card_rarity('jetting glasskite'/'CNS', 'Uncommon').
card_artist('jetting glasskite'/'CNS', 'Shishizaru').
card_number('jetting glasskite'/'CNS', '100').
card_flavor_text('jetting glasskite'/'CNS', 'The bolt struck with a flash, and there was a terrible sound, as of glass shattering, but the creature was unharmed.').
card_multiverse_id('jetting glasskite'/'CNS', '382293').

card_in_set('kor chant', 'CNS').
card_original_type('kor chant'/'CNS', 'Instant').
card_original_text('kor chant'/'CNS', 'All damage that would be dealt this turn to target creature you control by a source of your choice is dealt to another target creature instead.').
card_image_name('kor chant'/'CNS', 'kor chant').
card_uid('kor chant'/'CNS', 'CNS:Kor Chant:kor chant').
card_rarity('kor chant'/'CNS', 'Common').
card_artist('kor chant'/'CNS', 'Yohann Schepacz').
card_number('kor chant'/'CNS', '73').
card_flavor_text('kor chant'/'CNS', 'Sacred rites repurposed to fight an ancient threat.').
card_multiverse_id('kor chant'/'CNS', '382294').

card_in_set('lead the stampede', 'CNS').
card_original_type('lead the stampede'/'CNS', 'Sorcery').
card_original_text('lead the stampede'/'CNS', 'Look at the top five cards of your library. You may reveal any number of creature cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('lead the stampede'/'CNS', 'lead the stampede').
card_uid('lead the stampede'/'CNS', 'CNS:Lead the Stampede:lead the stampede').
card_rarity('lead the stampede'/'CNS', 'Uncommon').
card_artist('lead the stampede'/'CNS', 'Efrem Palacios').
card_number('lead the stampede'/'CNS', '170').
card_multiverse_id('lead the stampede'/'CNS', '382295').

card_in_set('liliana\'s specter', 'CNS').
card_original_type('liliana\'s specter'/'CNS', 'Creature — Specter').
card_original_text('liliana\'s specter'/'CNS', 'Flying\nWhen Liliana\'s Specter enters the battlefield, each opponent discards a card.').
card_image_name('liliana\'s specter'/'CNS', 'liliana\'s specter').
card_uid('liliana\'s specter'/'CNS', 'CNS:Liliana\'s Specter:liliana\'s specter').
card_rarity('liliana\'s specter'/'CNS', 'Common').
card_artist('liliana\'s specter'/'CNS', 'Vance Kovacs').
card_number('liliana\'s specter'/'CNS', '116').
card_flavor_text('liliana\'s specter'/'CNS', '\"The finest minions know what I need without me ever saying a thing.\"\n—Liliana Vess').
card_multiverse_id('liliana\'s specter'/'CNS', '382296').

card_in_set('lizard warrior', 'CNS').
card_original_type('lizard warrior'/'CNS', 'Creature — Lizard Warrior').
card_original_text('lizard warrior'/'CNS', '').
card_image_name('lizard warrior'/'CNS', 'lizard warrior').
card_uid('lizard warrior'/'CNS', 'CNS:Lizard Warrior:lizard warrior').
card_rarity('lizard warrior'/'CNS', 'Common').
card_artist('lizard warrior'/'CNS', 'Roger Raupp').
card_number('lizard warrior'/'CNS', '146').
card_flavor_text('lizard warrior'/'CNS', 'Don\'t let its appearance frighten you. Let its claws and teeth do that.').
card_multiverse_id('lizard warrior'/'CNS', '382297').

card_in_set('lore seeker', 'CNS').
card_original_type('lore seeker'/'CNS', 'Artifact Creature — Construct').
card_original_text('lore seeker'/'CNS', 'Reveal Lore Seeker as you draft it. After you draft Lore Seeker, you may add a booster pack to the draft. (Your next pick is from that booster pack. Pass it to the next player and it\'s drafted this draft round.)').
card_first_print('lore seeker', 'CNS').
card_image_name('lore seeker'/'CNS', 'lore seeker').
card_uid('lore seeker'/'CNS', 'CNS:Lore Seeker:lore seeker').
card_rarity('lore seeker'/'CNS', 'Rare').
card_artist('lore seeker'/'CNS', 'Jason Felix').
card_number('lore seeker'/'CNS', '62').
card_flavor_text('lore seeker'/'CNS', 'Each volume contains wonders.').
card_multiverse_id('lore seeker'/'CNS', '382298').

card_in_set('lurking automaton', 'CNS').
card_original_type('lurking automaton'/'CNS', 'Artifact Creature — Construct').
card_original_text('lurking automaton'/'CNS', 'Reveal Lurking Automaton as you draft it and note how many cards you\'ve drafted this draft round, including Lurking Automaton.\nLurking Automaton enters the battlefield with X +1/+1 counters on it, where X is the highest number you noted for cards named Lurking Automaton.').
card_first_print('lurking automaton', 'CNS').
card_image_name('lurking automaton'/'CNS', 'lurking automaton').
card_uid('lurking automaton'/'CNS', 'CNS:Lurking Automaton:lurking automaton').
card_rarity('lurking automaton'/'CNS', 'Common').
card_artist('lurking automaton'/'CNS', 'Yeong-Hao Han').
card_number('lurking automaton'/'CNS', '63').
card_multiverse_id('lurking automaton'/'CNS', '382299').

card_in_set('magister of worth', 'CNS').
card_original_type('magister of worth'/'CNS', 'Creature — Angel').
card_original_text('magister of worth'/'CNS', 'Flying\nWill of the council — When Magister of Worth enters the battlefield, starting with you, each player votes for grace or condemnation. If grace gets more votes, each player returns each creature card from his or her graveyard to the battlefield. If condemnation gets more votes or the vote is tied, destroy all creatures other than Magister of Worth.').
card_image_name('magister of worth'/'CNS', 'magister of worth').
card_uid('magister of worth'/'CNS', 'CNS:Magister of Worth:magister of worth').
card_rarity('magister of worth'/'CNS', 'Rare').
card_artist('magister of worth'/'CNS', 'John Stanko').
card_number('magister of worth'/'CNS', '48').
card_multiverse_id('magister of worth'/'CNS', '382300').

card_in_set('magus of the mirror', 'CNS').
card_original_type('magus of the mirror'/'CNS', 'Creature — Human Wizard').
card_original_text('magus of the mirror'/'CNS', '{T}, Sacrifice Magus of the Mirror: Exchange life totals with target opponent. Activate this ability only during your upkeep.').
card_image_name('magus of the mirror'/'CNS', 'magus of the mirror').
card_uid('magus of the mirror'/'CNS', 'CNS:Magus of the Mirror:magus of the mirror').
card_rarity('magus of the mirror'/'CNS', 'Rare').
card_artist('magus of the mirror'/'CNS', 'Christopher Moeller').
card_number('magus of the mirror'/'CNS', '117').
card_flavor_text('magus of the mirror'/'CNS', '\"Behold! The image of the enemy and all that she has. Trust your envy, and take it.\"').
card_multiverse_id('magus of the mirror'/'CNS', '382301').

card_in_set('mana geyser', 'CNS').
card_original_type('mana geyser'/'CNS', 'Sorcery').
card_original_text('mana geyser'/'CNS', 'Add {R} to your mana pool for each tapped land your opponents control.').
card_image_name('mana geyser'/'CNS', 'mana geyser').
card_uid('mana geyser'/'CNS', 'CNS:Mana Geyser:mana geyser').
card_rarity('mana geyser'/'CNS', 'Common').
card_artist('mana geyser'/'CNS', 'Martina Pilcerova').
card_number('mana geyser'/'CNS', '147').
card_flavor_text('mana geyser'/'CNS', 'The Quicksilver Sea hissed and bubbled at the indignity. The Vulshok shaman just smiled.').
card_multiverse_id('mana geyser'/'CNS', '382302').

card_in_set('marchesa\'s emissary', 'CNS').
card_original_type('marchesa\'s emissary'/'CNS', 'Creature — Human Rogue').
card_original_text('marchesa\'s emissary'/'CNS', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_first_print('marchesa\'s emissary', 'CNS').
card_image_name('marchesa\'s emissary'/'CNS', 'marchesa\'s emissary').
card_uid('marchesa\'s emissary'/'CNS', 'CNS:Marchesa\'s Emissary:marchesa\'s emissary').
card_rarity('marchesa\'s emissary'/'CNS', 'Common').
card_artist('marchesa\'s emissary'/'CNS', 'Tyler Jacobson').
card_number('marchesa\'s emissary'/'CNS', '21').
card_flavor_text('marchesa\'s emissary'/'CNS', '\"The Black Rose doesn\'t tolerate weeds.\"').
card_multiverse_id('marchesa\'s emissary'/'CNS', '382304').

card_in_set('marchesa\'s infiltrator', 'CNS').
card_original_type('marchesa\'s infiltrator'/'CNS', 'Creature — Human Rogue').
card_original_text('marchesa\'s infiltrator'/'CNS', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nWhenever Marchesa\'s Infiltrator deals combat damage to a player, draw a card.').
card_first_print('marchesa\'s infiltrator', 'CNS').
card_image_name('marchesa\'s infiltrator'/'CNS', 'marchesa\'s infiltrator').
card_uid('marchesa\'s infiltrator'/'CNS', 'CNS:Marchesa\'s Infiltrator:marchesa\'s infiltrator').
card_rarity('marchesa\'s infiltrator'/'CNS', 'Uncommon').
card_artist('marchesa\'s infiltrator'/'CNS', 'Lucas Graciano').
card_number('marchesa\'s infiltrator'/'CNS', '22').
card_flavor_text('marchesa\'s infiltrator'/'CNS', '\"Secrets, like coins, are easy to steal.\"').
card_multiverse_id('marchesa\'s infiltrator'/'CNS', '382305').

card_in_set('marchesa\'s smuggler', 'CNS').
card_original_type('marchesa\'s smuggler'/'CNS', 'Creature — Human Rogue').
card_original_text('marchesa\'s smuggler'/'CNS', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\n{1}{U}{R}: Target creature you control gains haste until end of turn and can\'t be blocked this turn.').
card_first_print('marchesa\'s smuggler', 'CNS').
card_image_name('marchesa\'s smuggler'/'CNS', 'marchesa\'s smuggler').
card_uid('marchesa\'s smuggler'/'CNS', 'CNS:Marchesa\'s Smuggler:marchesa\'s smuggler').
card_rarity('marchesa\'s smuggler'/'CNS', 'Uncommon').
card_artist('marchesa\'s smuggler'/'CNS', 'Dan Scott').
card_number('marchesa\'s smuggler'/'CNS', '50').
card_flavor_text('marchesa\'s smuggler'/'CNS', '\"Watch your head . . . and your back.\"').
card_multiverse_id('marchesa\'s smuggler'/'CNS', '382306').

card_in_set('marchesa, the black rose', 'CNS').
card_original_type('marchesa, the black rose'/'CNS', 'Legendary Creature — Human Wizard').
card_original_text('marchesa, the black rose'/'CNS', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nOther creatures you control have dethrone.\nWhenever a creature you control with a +1/+1 counter on it dies, return that card to the battlefield under your control at the beginning of the next end step.').
card_first_print('marchesa, the black rose', 'CNS').
card_image_name('marchesa, the black rose'/'CNS', 'marchesa, the black rose').
card_uid('marchesa, the black rose'/'CNS', 'CNS:Marchesa, the Black Rose:marchesa, the black rose').
card_rarity('marchesa, the black rose'/'CNS', 'Mythic Rare').
card_artist('marchesa, the black rose'/'CNS', 'Matt Stewart').
card_number('marchesa, the black rose'/'CNS', '49').
card_multiverse_id('marchesa, the black rose'/'CNS', '382303').

card_in_set('minamo scrollkeeper', 'CNS').
card_original_type('minamo scrollkeeper'/'CNS', 'Creature — Human Wizard').
card_original_text('minamo scrollkeeper'/'CNS', 'Defender\nYour maximum hand size is increased by one.').
card_image_name('minamo scrollkeeper'/'CNS', 'minamo scrollkeeper').
card_uid('minamo scrollkeeper'/'CNS', 'CNS:Minamo Scrollkeeper:minamo scrollkeeper').
card_rarity('minamo scrollkeeper'/'CNS', 'Common').
card_artist('minamo scrollkeeper'/'CNS', 'Paolo Parente').
card_number('minamo scrollkeeper'/'CNS', '101').
card_flavor_text('minamo scrollkeeper'/'CNS', 'The scrollkeepers never stepped an inch away from the doors of the Great Library, not even when the ogres and their oni masters swept through Minamo.').
card_multiverse_id('minamo scrollkeeper'/'CNS', '382307').

card_in_set('mirari\'s wake', 'CNS').
card_original_type('mirari\'s wake'/'CNS', 'Enchantment').
card_original_text('mirari\'s wake'/'CNS', 'Creatures you control get +1/+1.\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.').
card_image_name('mirari\'s wake'/'CNS', 'mirari\'s wake').
card_uid('mirari\'s wake'/'CNS', 'CNS:Mirari\'s Wake:mirari\'s wake').
card_rarity('mirari\'s wake'/'CNS', 'Mythic Rare').
card_artist('mirari\'s wake'/'CNS', 'Volkan Baga').
card_number('mirari\'s wake'/'CNS', '189').
card_flavor_text('mirari\'s wake'/'CNS', 'The land drank power from the Mirari as though it had thirsted forever.').
card_multiverse_id('mirari\'s wake'/'CNS', '382308').

card_in_set('mirrodin\'s core', 'CNS').
card_original_type('mirrodin\'s core'/'CNS', 'Land').
card_original_text('mirrodin\'s core'/'CNS', '{T}: Add {1} to your mana pool.\n{T}: Put a charge counter on Mirrodin\'s Core.\n{T}, Remove a charge counter from Mirrodin\'s Core: Add one mana of any color to your mana pool.').
card_image_name('mirrodin\'s core'/'CNS', 'mirrodin\'s core').
card_uid('mirrodin\'s core'/'CNS', 'CNS:Mirrodin\'s Core:mirrodin\'s core').
card_rarity('mirrodin\'s core'/'CNS', 'Uncommon').
card_artist('mirrodin\'s core'/'CNS', 'Greg Staples').
card_number('mirrodin\'s core'/'CNS', '208').
card_multiverse_id('mirrodin\'s core'/'CNS', '382309').

card_in_set('misdirection', 'CNS').
card_original_type('misdirection'/'CNS', 'Instant').
card_original_text('misdirection'/'CNS', 'You may exile a blue card from your hand rather than pay Misdirection\'s mana cost.\nChange the target of target spell with a single target.').
card_image_name('misdirection'/'CNS', 'misdirection').
card_uid('misdirection'/'CNS', 'CNS:Misdirection:misdirection').
card_rarity('misdirection'/'CNS', 'Rare').
card_artist('misdirection'/'CNS', 'Mathias Kollros').
card_number('misdirection'/'CNS', '102').
card_multiverse_id('misdirection'/'CNS', '382310').

card_in_set('moment of heroism', 'CNS').
card_original_type('moment of heroism'/'CNS', 'Instant').
card_original_text('moment of heroism'/'CNS', 'Target creature gets +2/+2 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('moment of heroism'/'CNS', 'moment of heroism').
card_uid('moment of heroism'/'CNS', 'CNS:Moment of Heroism:moment of heroism').
card_rarity('moment of heroism'/'CNS', 'Common').
card_artist('moment of heroism'/'CNS', 'Christopher Moeller').
card_number('moment of heroism'/'CNS', '74').
card_flavor_text('moment of heroism'/'CNS', '\"My faith is stronger than fang, claw, or mindless hunger.\"').
card_multiverse_id('moment of heroism'/'CNS', '382311').

card_in_set('morkrut banshee', 'CNS').
card_original_type('morkrut banshee'/'CNS', 'Creature — Spirit').
card_original_text('morkrut banshee'/'CNS', 'Morbid — When Morkrut Banshee enters the battlefield, if a creature died this turn, target creature gets -4/-4 until end of turn.').
card_image_name('morkrut banshee'/'CNS', 'morkrut banshee').
card_uid('morkrut banshee'/'CNS', 'CNS:Morkrut Banshee:morkrut banshee').
card_rarity('morkrut banshee'/'CNS', 'Uncommon').
card_artist('morkrut banshee'/'CNS', 'Svetlin Velinov').
card_number('morkrut banshee'/'CNS', '118').
card_flavor_text('morkrut banshee'/'CNS', '\"Let go your grudges, or risk wandering the bogs forever in a murderous rage.\"\n—Hildin, priest of Avacyn').
card_multiverse_id('morkrut banshee'/'CNS', '382312').

card_in_set('mortify', 'CNS').
card_original_type('mortify'/'CNS', 'Instant').
card_original_text('mortify'/'CNS', 'Destroy target creature or enchantment.').
card_image_name('mortify'/'CNS', 'mortify').
card_uid('mortify'/'CNS', 'CNS:Mortify:mortify').
card_rarity('mortify'/'CNS', 'Uncommon').
card_artist('mortify'/'CNS', 'Nils Hamm').
card_number('mortify'/'CNS', '190').
card_flavor_text('mortify'/'CNS', 'Many who cross Sorin\'s path come down with a sudden and fatal case of being-in-the-way-of-a-millennia-old-vampire.').
card_multiverse_id('mortify'/'CNS', '382313').

card_in_set('muzzio\'s preparations', 'CNS').
card_original_type('muzzio\'s preparations'/'CNS', 'Conspiracy').
card_original_text('muzzio\'s preparations'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nEach creature you control with the chosen name enters the battlefield with an additional +1/+1 counter on it.').
card_first_print('muzzio\'s preparations', 'CNS').
card_image_name('muzzio\'s preparations'/'CNS', 'muzzio\'s preparations').
card_uid('muzzio\'s preparations'/'CNS', 'CNS:Muzzio\'s Preparations:muzzio\'s preparations').
card_rarity('muzzio\'s preparations'/'CNS', 'Common').
card_artist('muzzio\'s preparations'/'CNS', 'Karl Kopinski').
card_number('muzzio\'s preparations'/'CNS', '7').
card_multiverse_id('muzzio\'s preparations'/'CNS', '382315').

card_in_set('muzzio, visionary architect', 'CNS').
card_original_type('muzzio, visionary architect'/'CNS', 'Legendary Creature — Human Artificer').
card_original_text('muzzio, visionary architect'/'CNS', '{3}{U}, {T}: Look at the top X cards of your library, where X is the highest converted mana cost among artifacts you control. You may reveal an artifact card from among them and put it onto the battlefield. Put the rest on the bottom of your library in any order.').
card_first_print('muzzio, visionary architect', 'CNS').
card_image_name('muzzio, visionary architect'/'CNS', 'muzzio, visionary architect').
card_uid('muzzio, visionary architect'/'CNS', 'CNS:Muzzio, Visionary Architect:muzzio, visionary architect').
card_rarity('muzzio, visionary architect'/'CNS', 'Mythic Rare').
card_artist('muzzio, visionary architect'/'CNS', 'Volkan Baga').
card_number('muzzio, visionary architect'/'CNS', '23').
card_multiverse_id('muzzio, visionary architect'/'CNS', '382314').

card_in_set('nature\'s claim', 'CNS').
card_original_type('nature\'s claim'/'CNS', 'Instant').
card_original_text('nature\'s claim'/'CNS', 'Destroy target artifact or enchantment. Its controller gains 4 life.').
card_image_name('nature\'s claim'/'CNS', 'nature\'s claim').
card_uid('nature\'s claim'/'CNS', 'CNS:Nature\'s Claim:nature\'s claim').
card_rarity('nature\'s claim'/'CNS', 'Common').
card_artist('nature\'s claim'/'CNS', 'Raoul Vitale').
card_number('nature\'s claim'/'CNS', '171').
card_flavor_text('nature\'s claim'/'CNS', '\"The timeless, tireless jaws of nature shall one day devour us all.\"\n—Sildark, artificer of Omu').
card_multiverse_id('nature\'s claim'/'CNS', '382316').

card_in_set('necromantic thirst', 'CNS').
card_original_type('necromantic thirst'/'CNS', 'Enchantment — Aura').
card_original_text('necromantic thirst'/'CNS', 'Enchant creature\nWhenever enchanted creature deals combat damage to a player, you may return target creature card from your graveyard to your hand.').
card_image_name('necromantic thirst'/'CNS', 'necromantic thirst').
card_uid('necromantic thirst'/'CNS', 'CNS:Necromantic Thirst:necromantic thirst').
card_rarity('necromantic thirst'/'CNS', 'Common').
card_artist('necromantic thirst'/'CNS', 'Brandon Kitkouski').
card_number('necromantic thirst'/'CNS', '119').
card_multiverse_id('necromantic thirst'/'CNS', '382317').

card_in_set('noble templar', 'CNS').
card_original_type('noble templar'/'CNS', 'Creature — Human Cleric Soldier').
card_original_text('noble templar'/'CNS', 'Vigilance\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('noble templar'/'CNS', 'noble templar').
card_uid('noble templar'/'CNS', 'CNS:Noble Templar:noble templar').
card_rarity('noble templar'/'CNS', 'Common').
card_artist('noble templar'/'CNS', 'Alex Horley-Orlandelli').
card_number('noble templar'/'CNS', '75').
card_multiverse_id('noble templar'/'CNS', '382318').

card_in_set('orcish cannonade', 'CNS').
card_original_type('orcish cannonade'/'CNS', 'Instant').
card_original_text('orcish cannonade'/'CNS', 'Orcish Cannonade deals 2 damage to target creature or player and 3 damage to you.\nDraw a card.').
card_image_name('orcish cannonade'/'CNS', 'orcish cannonade').
card_uid('orcish cannonade'/'CNS', 'CNS:Orcish Cannonade:orcish cannonade').
card_rarity('orcish cannonade'/'CNS', 'Common').
card_artist('orcish cannonade'/'CNS', 'Pete Venters').
card_number('orcish cannonade'/'CNS', '148').
card_flavor_text('orcish cannonade'/'CNS', '\"Crispy! Scarback! Load another volcano-ball.\"\n—Stumphobbler Thuj, orcish captain').
card_multiverse_id('orcish cannonade'/'CNS', '382319').

card_in_set('paliano, the high city', 'CNS').
card_original_type('paliano, the high city'/'CNS', 'Legendary Land').
card_original_text('paliano, the high city'/'CNS', 'Reveal Paliano, the High City as you draft it. The player to your right chooses a color, you choose another color, then the player to your left chooses a third color.\n{T}: Add one mana to your mana pool of any color chosen as you drafted cards named Paliano, the High City.').
card_first_print('paliano, the high city', 'CNS').
card_image_name('paliano, the high city'/'CNS', 'paliano, the high city').
card_uid('paliano, the high city'/'CNS', 'CNS:Paliano, the High City:paliano, the high city').
card_rarity('paliano, the high city'/'CNS', 'Rare').
card_artist('paliano, the high city'/'CNS', 'Adam Paquette').
card_number('paliano, the high city'/'CNS', '65').
card_multiverse_id('paliano, the high city'/'CNS', '382320').

card_in_set('peace strider', 'CNS').
card_original_type('peace strider'/'CNS', 'Artifact Creature — Construct').
card_original_text('peace strider'/'CNS', 'When Peace Strider enters the battlefield, you gain 3 life.').
card_image_name('peace strider'/'CNS', 'peace strider').
card_uid('peace strider'/'CNS', 'CNS:Peace Strider:peace strider').
card_rarity('peace strider'/'CNS', 'Uncommon').
card_artist('peace strider'/'CNS', 'Igor Kieryluk').
card_number('peace strider'/'CNS', '201').
card_flavor_text('peace strider'/'CNS', '\"The Vanished must have sent it from beyond to aid us in this struggle.\"\n—Kessla, Sylvok shaman').
card_multiverse_id('peace strider'/'CNS', '382321').

card_in_set('pelakka wurm', 'CNS').
card_original_type('pelakka wurm'/'CNS', 'Creature — Wurm').
card_original_text('pelakka wurm'/'CNS', 'Trample\nWhen Pelakka Wurm enters the battlefield, you gain 7 life.\nWhen Pelakka Wurm dies, draw a card.').
card_image_name('pelakka wurm'/'CNS', 'pelakka wurm').
card_uid('pelakka wurm'/'CNS', 'CNS:Pelakka Wurm:pelakka wurm').
card_rarity('pelakka wurm'/'CNS', 'Uncommon').
card_artist('pelakka wurm'/'CNS', 'Daniel Ljunggren').
card_number('pelakka wurm'/'CNS', '172').
card_flavor_text('pelakka wurm'/'CNS', 'It eats what it wants to eat—which is anything that moves.').
card_multiverse_id('pelakka wurm'/'CNS', '382322').

card_in_set('pernicious deed', 'CNS').
card_original_type('pernicious deed'/'CNS', 'Enchantment').
card_original_text('pernicious deed'/'CNS', '{X}, Sacrifice Pernicious Deed: Destroy each artifact, creature, and enchantment with converted mana cost X or less.').
card_image_name('pernicious deed'/'CNS', 'pernicious deed').
card_uid('pernicious deed'/'CNS', 'CNS:Pernicious Deed:pernicious deed').
card_rarity('pernicious deed'/'CNS', 'Mythic Rare').
card_artist('pernicious deed'/'CNS', 'Christopher Moeller').
card_number('pernicious deed'/'CNS', '191').
card_flavor_text('pernicious deed'/'CNS', '\"Yawgmoth,\" Freyalise whispered as she set the bomb, \"now you will pay for your treachery.\"').
card_multiverse_id('pernicious deed'/'CNS', '382323').

card_in_set('phage the untouchable', 'CNS').
card_original_type('phage the untouchable'/'CNS', 'Legendary Creature — Avatar Minion').
card_original_text('phage the untouchable'/'CNS', 'When Phage the Untouchable enters the battlefield, if you didn\'t cast it from your hand, you lose the game.\nWhenever Phage deals combat damage to a creature, destroy that creature. It can\'t be regenerated.\nWhenever Phage deals combat damage to a player, that player loses the game.').
card_image_name('phage the untouchable'/'CNS', 'phage the untouchable').
card_uid('phage the untouchable'/'CNS', 'CNS:Phage the Untouchable:phage the untouchable').
card_rarity('phage the untouchable'/'CNS', 'Mythic Rare').
card_artist('phage the untouchable'/'CNS', 'Ron Spears').
card_number('phage the untouchable'/'CNS', '120').
card_multiverse_id('phage the untouchable'/'CNS', '382324').

card_in_set('pillarfield ox', 'CNS').
card_original_type('pillarfield ox'/'CNS', 'Creature — Ox').
card_original_text('pillarfield ox'/'CNS', '').
card_image_name('pillarfield ox'/'CNS', 'pillarfield ox').
card_uid('pillarfield ox'/'CNS', 'CNS:Pillarfield Ox:pillarfield ox').
card_rarity('pillarfield ox'/'CNS', 'Common').
card_artist('pillarfield ox'/'CNS', 'Andrew Robinson').
card_number('pillarfield ox'/'CNS', '76').
card_flavor_text('pillarfield ox'/'CNS', '\"May starving fleas birth a thousand generations on your stubborn hide, cow!\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('pillarfield ox'/'CNS', '382325').

card_in_set('pitchburn devils', 'CNS').
card_original_type('pitchburn devils'/'CNS', 'Creature — Devil').
card_original_text('pitchburn devils'/'CNS', 'When Pitchburn Devils dies, it deals 3 damage to target creature or player.').
card_image_name('pitchburn devils'/'CNS', 'pitchburn devils').
card_uid('pitchburn devils'/'CNS', 'CNS:Pitchburn Devils:pitchburn devils').
card_rarity('pitchburn devils'/'CNS', 'Common').
card_artist('pitchburn devils'/'CNS', 'Johann Bodin').
card_number('pitchburn devils'/'CNS', '149').
card_flavor_text('pitchburn devils'/'CNS', 'The ingenuity of goblins, the depravity of demons, and the smarts of sheep.').
card_multiverse_id('pitchburn devils'/'CNS', '382326').

card_in_set('plagued rusalka', 'CNS').
card_original_type('plagued rusalka'/'CNS', 'Creature — Spirit').
card_original_text('plagued rusalka'/'CNS', '{B}, Sacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_image_name('plagued rusalka'/'CNS', 'plagued rusalka').
card_uid('plagued rusalka'/'CNS', 'CNS:Plagued Rusalka:plagued rusalka').
card_rarity('plagued rusalka'/'CNS', 'Uncommon').
card_artist('plagued rusalka'/'CNS', 'Alex Horley-Orlandelli').
card_number('plagued rusalka'/'CNS', '121').
card_flavor_text('plagued rusalka'/'CNS', '\"Look at her, once filled with innocence. Death has a way of wringing away such . . . deficiencies.\"\n—Savra').
card_multiverse_id('plagued rusalka'/'CNS', '382327').

card_in_set('plated seastrider', 'CNS').
card_original_type('plated seastrider'/'CNS', 'Creature — Beast').
card_original_text('plated seastrider'/'CNS', '').
card_image_name('plated seastrider'/'CNS', 'plated seastrider').
card_uid('plated seastrider'/'CNS', 'CNS:Plated Seastrider:plated seastrider').
card_rarity('plated seastrider'/'CNS', 'Common').
card_artist('plated seastrider'/'CNS', 'Izzy').
card_number('plated seastrider'/'CNS', '103').
card_flavor_text('plated seastrider'/'CNS', 'The Neurok buried entangling cables just under the Quicksilver Sea. One seastrider harvest can provide an army\'s worth of armor and shields.').
card_multiverse_id('plated seastrider'/'CNS', '382328').

card_in_set('plea for power', 'CNS').
card_original_type('plea for power'/'CNS', 'Sorcery').
card_original_text('plea for power'/'CNS', 'Will of the council — Starting with you, each player votes for time or knowledge. If time gets more votes, take an extra turn after this one. If knowledge gets more votes or the vote is tied, draw three cards.').
card_first_print('plea for power', 'CNS').
card_image_name('plea for power'/'CNS', 'plea for power').
card_uid('plea for power'/'CNS', 'CNS:Plea for Power:plea for power').
card_rarity('plea for power'/'CNS', 'Rare').
card_artist('plea for power'/'CNS', 'John Severin Brassell').
card_number('plea for power'/'CNS', '24').
card_multiverse_id('plea for power'/'CNS', '382329').

card_in_set('plummet', 'CNS').
card_original_type('plummet'/'CNS', 'Instant').
card_original_text('plummet'/'CNS', 'Destroy target creature with flying.').
card_image_name('plummet'/'CNS', 'plummet').
card_uid('plummet'/'CNS', 'CNS:Plummet:plummet').
card_rarity('plummet'/'CNS', 'Common').
card_artist('plummet'/'CNS', 'Pete Venters').
card_number('plummet'/'CNS', '173').
card_flavor_text('plummet'/'CNS', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'CNS', '382330').

card_in_set('power of fire', 'CNS').
card_original_type('power of fire'/'CNS', 'Enchantment — Aura').
card_original_text('power of fire'/'CNS', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_image_name('power of fire'/'CNS', 'power of fire').
card_uid('power of fire'/'CNS', 'CNS:Power of Fire:power of fire').
card_rarity('power of fire'/'CNS', 'Common').
card_artist('power of fire'/'CNS', 'Trevor Hairsine').
card_number('power of fire'/'CNS', '150').
card_flavor_text('power of fire'/'CNS', 'The cinders believe their flame was stolen by one they call the Extinguisher. All who now wield fire draw their desperate wrath.').
card_multiverse_id('power of fire'/'CNS', '382331').

card_in_set('power play', 'CNS').
card_original_type('power play'/'CNS', 'Conspiracy').
card_original_text('power play'/'CNS', '(Start the game with this conspiracy face up in the command zone.)\nYou are the starting player. If multiple players would be the starting player, one of those players is chosen at random.').
card_first_print('power play', 'CNS').
card_image_name('power play'/'CNS', 'power play').
card_uid('power play'/'CNS', 'CNS:Power Play:power play').
card_rarity('power play'/'CNS', 'Uncommon').
card_artist('power play'/'CNS', 'Matt Stewart').
card_number('power play'/'CNS', '8').
card_multiverse_id('power play'/'CNS', '382332').

card_in_set('predator\'s howl', 'CNS').
card_original_type('predator\'s howl'/'CNS', 'Instant').
card_original_text('predator\'s howl'/'CNS', 'Put a 2/2 green Wolf creature token onto the battlefield.\nMorbid — Put three 2/2 green Wolf creature tokens onto the battlefield instead if a creature died this turn.').
card_first_print('predator\'s howl', 'CNS').
card_image_name('predator\'s howl'/'CNS', 'predator\'s howl').
card_uid('predator\'s howl'/'CNS', 'CNS:Predator\'s Howl:predator\'s howl').
card_rarity('predator\'s howl'/'CNS', 'Uncommon').
card_artist('predator\'s howl'/'CNS', 'Ralph Horsley').
card_number('predator\'s howl'/'CNS', '37').
card_flavor_text('predator\'s howl'/'CNS', '\"And Muzzio says my arguments have no teeth.\"\n—Selvala, ranger of the Lowlands').
card_multiverse_id('predator\'s howl'/'CNS', '382333').

card_in_set('pride guardian', 'CNS').
card_original_type('pride guardian'/'CNS', 'Creature — Cat Monk').
card_original_text('pride guardian'/'CNS', 'Defender\nWhenever Pride Guardian blocks, you gain 3 life.').
card_image_name('pride guardian'/'CNS', 'pride guardian').
card_uid('pride guardian'/'CNS', 'CNS:Pride Guardian:pride guardian').
card_rarity('pride guardian'/'CNS', 'Common').
card_artist('pride guardian'/'CNS', 'Chris Rahn').
card_number('pride guardian'/'CNS', '77').
card_flavor_text('pride guardian'/'CNS', '\"Any hotheaded cub can raise a sword in anger.\"').
card_multiverse_id('pride guardian'/'CNS', '382334').

card_in_set('pristine angel', 'CNS').
card_original_type('pristine angel'/'CNS', 'Creature — Angel').
card_original_text('pristine angel'/'CNS', 'Flying\nAs long as Pristine Angel is untapped, it has protection from artifacts and from all colors.\nWhenever you cast a spell, you may untap Pristine Angel.').
card_image_name('pristine angel'/'CNS', 'pristine angel').
card_uid('pristine angel'/'CNS', 'CNS:Pristine Angel:pristine angel').
card_rarity('pristine angel'/'CNS', 'Mythic Rare').
card_artist('pristine angel'/'CNS', 'Scott M. Fischer').
card_number('pristine angel'/'CNS', '78').
card_multiverse_id('pristine angel'/'CNS', '382335').

card_in_set('provoke', 'CNS').
card_original_type('provoke'/'CNS', 'Instant').
card_original_text('provoke'/'CNS', 'Untap target creature you don\'t control. That creature blocks this turn if able.\nDraw a card.').
card_image_name('provoke'/'CNS', 'provoke').
card_uid('provoke'/'CNS', 'CNS:Provoke:provoke').
card_rarity('provoke'/'CNS', 'Common').
card_artist('provoke'/'CNS', 'Kev Walker').
card_number('provoke'/'CNS', '174').
card_flavor_text('provoke'/'CNS', '\"Never start a fight you can\'t finish.\"\n—Padril, council guardian').
card_multiverse_id('provoke'/'CNS', '382336').

card_in_set('quag vampires', 'CNS').
card_original_type('quag vampires'/'CNS', 'Creature — Vampire Rogue').
card_original_text('quag vampires'/'CNS', 'Multikicker {1}{B} (You may pay an additional {1}{B} any number of times as you cast this spell.)\nSwampwalk\nQuag Vampires enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('quag vampires'/'CNS', 'quag vampires').
card_uid('quag vampires'/'CNS', 'CNS:Quag Vampires:quag vampires').
card_rarity('quag vampires'/'CNS', 'Common').
card_artist('quag vampires'/'CNS', 'Jason Felix').
card_number('quag vampires'/'CNS', '122').
card_multiverse_id('quag vampires'/'CNS', '382337').

card_in_set('quicksand', 'CNS').
card_original_type('quicksand'/'CNS', 'Land').
card_original_text('quicksand'/'CNS', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_image_name('quicksand'/'CNS', 'quicksand').
card_uid('quicksand'/'CNS', 'CNS:Quicksand:quicksand').
card_rarity('quicksand'/'CNS', 'Uncommon').
card_artist('quicksand'/'CNS', 'Matt Stewart').
card_number('quicksand'/'CNS', '209').
card_flavor_text('quicksand'/'CNS', 'Not all deaths are etched with mythic meaning and iconic glory.').
card_multiverse_id('quicksand'/'CNS', '382338').

card_in_set('realm seekers', 'CNS').
card_original_type('realm seekers'/'CNS', 'Creature — Elf Scout').
card_original_text('realm seekers'/'CNS', 'Realm Seekers enters the battlefield with X +1/+1 counters on it, where X is the total number of cards in all players\' hands.\n{2}{G}, Remove a +1/+1 counter from Realm Seekers: Search your library for a land card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('realm seekers', 'CNS').
card_image_name('realm seekers'/'CNS', 'realm seekers').
card_uid('realm seekers'/'CNS', 'CNS:Realm Seekers:realm seekers').
card_rarity('realm seekers'/'CNS', 'Rare').
card_artist('realm seekers'/'CNS', 'Mike Sass').
card_number('realm seekers'/'CNS', '38').
card_multiverse_id('realm seekers'/'CNS', '382339').

card_in_set('reckless scholar', 'CNS').
card_original_type('reckless scholar'/'CNS', 'Creature — Human Wizard').
card_original_text('reckless scholar'/'CNS', '{T}: Target player draws a card, then discards a card.').
card_image_name('reckless scholar'/'CNS', 'reckless scholar').
card_uid('reckless scholar'/'CNS', 'CNS:Reckless Scholar:reckless scholar').
card_rarity('reckless scholar'/'CNS', 'Common').
card_artist('reckless scholar'/'CNS', 'Steve Prescott').
card_number('reckless scholar'/'CNS', '104').
card_flavor_text('reckless scholar'/'CNS', '\"Any good prospector must sift the gold from the sand.\"').
card_multiverse_id('reckless scholar'/'CNS', '382340').

card_in_set('reckless spite', 'CNS').
card_original_type('reckless spite'/'CNS', 'Instant').
card_original_text('reckless spite'/'CNS', 'Destroy two target nonblack creatures. You lose 5 life.').
card_image_name('reckless spite'/'CNS', 'reckless spite').
card_uid('reckless spite'/'CNS', 'CNS:Reckless Spite:reckless spite').
card_rarity('reckless spite'/'CNS', 'Uncommon').
card_artist('reckless spite'/'CNS', 'Karl Kopinski').
card_number('reckless spite'/'CNS', '123').
card_flavor_text('reckless spite'/'CNS', '\"The end is justified in being mean. Or something like that.\"').
card_multiverse_id('reckless spite'/'CNS', '382341').

card_in_set('reflecting pool', 'CNS').
card_original_type('reflecting pool'/'CNS', 'Land').
card_original_text('reflecting pool'/'CNS', '{T}: Add to your mana pool one mana of any type that a land you control could produce.').
card_image_name('reflecting pool'/'CNS', 'reflecting pool').
card_uid('reflecting pool'/'CNS', 'CNS:Reflecting Pool:reflecting pool').
card_rarity('reflecting pool'/'CNS', 'Rare').
card_artist('reflecting pool'/'CNS', 'Fred Fields').
card_number('reflecting pool'/'CNS', '210').
card_flavor_text('reflecting pool'/'CNS', 'Does it reflect the future that once was or the past that can never be?').
card_multiverse_id('reflecting pool'/'CNS', '382342').

card_in_set('reign of the pit', 'CNS').
card_original_type('reign of the pit'/'CNS', 'Sorcery').
card_original_text('reign of the pit'/'CNS', 'Each player sacrifices a creature. Put an X/X black Demon creature token with flying onto the battlefield, where X is the total power of the creatures sacrificed this way.').
card_first_print('reign of the pit', 'CNS').
card_image_name('reign of the pit'/'CNS', 'reign of the pit').
card_uid('reign of the pit'/'CNS', 'CNS:Reign of the Pit:reign of the pit').
card_rarity('reign of the pit'/'CNS', 'Rare').
card_artist('reign of the pit'/'CNS', 'Evan Shipard').
card_number('reign of the pit'/'CNS', '29').
card_multiverse_id('reign of the pit'/'CNS', '382343').

card_in_set('reito lantern', 'CNS').
card_original_type('reito lantern'/'CNS', 'Artifact').
card_original_text('reito lantern'/'CNS', '{3}: Put target card from a graveyard on the bottom of its owner\'s library.').
card_image_name('reito lantern'/'CNS', 'reito lantern').
card_uid('reito lantern'/'CNS', 'CNS:Reito Lantern:reito lantern').
card_rarity('reito lantern'/'CNS', 'Uncommon').
card_artist('reito lantern'/'CNS', 'Greg Hildebrandt').
card_number('reito lantern'/'CNS', '202').
card_flavor_text('reito lantern'/'CNS', 'Lanterns carved from the mystic stones of the Reito Mines were said to light the way of lost souls.').
card_multiverse_id('reito lantern'/'CNS', '382344').

card_in_set('relic crush', 'CNS').
card_original_type('relic crush'/'CNS', 'Instant').
card_original_text('relic crush'/'CNS', 'Destroy target artifact or enchantment and up to one other target artifact or enchantment.').
card_image_name('relic crush'/'CNS', 'relic crush').
card_uid('relic crush'/'CNS', 'CNS:Relic Crush:relic crush').
card_rarity('relic crush'/'CNS', 'Uncommon').
card_artist('relic crush'/'CNS', 'Steven Belledin').
card_number('relic crush'/'CNS', '175').
card_flavor_text('relic crush'/'CNS', 'There are many ruins, but there used to be many more.').
card_multiverse_id('relic crush'/'CNS', '382345').

card_in_set('respite', 'CNS').
card_original_type('respite'/'CNS', 'Instant').
card_original_text('respite'/'CNS', 'Prevent all combat damage that would be dealt this turn. You gain 1 life for each attacking creature.').
card_image_name('respite'/'CNS', 'respite').
card_uid('respite'/'CNS', 'CNS:Respite:respite').
card_rarity('respite'/'CNS', 'Common').
card_artist('respite'/'CNS', 'Rebecca Guay').
card_number('respite'/'CNS', '176').
card_flavor_text('respite'/'CNS', '\"If they board us we\'re finished,\" warned Orim. Crovax nodded. \"And if they don\'t . . . what then?\"').
card_multiverse_id('respite'/'CNS', '382346').

card_in_set('reya dawnbringer', 'CNS').
card_original_type('reya dawnbringer'/'CNS', 'Legendary Creature — Angel').
card_original_text('reya dawnbringer'/'CNS', 'Flying\nAt the beginning of your upkeep, you may return target creature card from your graveyard to the battlefield.').
card_image_name('reya dawnbringer'/'CNS', 'reya dawnbringer').
card_uid('reya dawnbringer'/'CNS', 'CNS:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'CNS', 'Rare').
card_artist('reya dawnbringer'/'CNS', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'CNS', '79').
card_flavor_text('reya dawnbringer'/'CNS', '\"You have not died until I consent.\"').
card_multiverse_id('reya dawnbringer'/'CNS', '382347').

card_in_set('rousing of souls', 'CNS').
card_original_type('rousing of souls'/'CNS', 'Sorcery').
card_original_text('rousing of souls'/'CNS', 'Parley — Each player reveals the top card of his or her library. For each nonland card revealed this way, you put a 1/1 white Spirit creature token with flying onto the battlefield. Then each player draws a card.').
card_first_print('rousing of souls', 'CNS').
card_image_name('rousing of souls'/'CNS', 'rousing of souls').
card_uid('rousing of souls'/'CNS', 'CNS:Rousing of Souls:rousing of souls').
card_rarity('rousing of souls'/'CNS', 'Common').
card_artist('rousing of souls'/'CNS', 'Jeff Simpson').
card_number('rousing of souls'/'CNS', '19').
card_flavor_text('rousing of souls'/'CNS', '\"We remember. We rise.\"').
card_multiverse_id('rousing of souls'/'CNS', '382348').

card_in_set('rout', 'CNS').
card_original_type('rout'/'CNS', 'Sorcery').
card_original_text('rout'/'CNS', 'You may cast Rout any time you could cast an instant if you pay {2} more to cast it.\nDestroy all creatures. They can\'t be regenerated.').
card_image_name('rout'/'CNS', 'rout').
card_uid('rout'/'CNS', 'CNS:Rout:rout').
card_rarity('rout'/'CNS', 'Rare').
card_artist('rout'/'CNS', 'Igor Kieryluk').
card_number('rout'/'CNS', '80').
card_flavor_text('rout'/'CNS', '\"End this. What I seek is far greater.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('rout'/'CNS', '382349').

card_in_set('runed servitor', 'CNS').
card_original_type('runed servitor'/'CNS', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'CNS', 'When Runed Servitor dies, each player draws a card.').
card_image_name('runed servitor'/'CNS', 'runed servitor').
card_uid('runed servitor'/'CNS', 'CNS:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'CNS', 'Uncommon').
card_artist('runed servitor'/'CNS', 'Mike Bierek').
card_number('runed servitor'/'CNS', '203').
card_flavor_text('runed servitor'/'CNS', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'CNS', '382350').

card_in_set('sakura-tribe elder', 'CNS').
card_original_type('sakura-tribe elder'/'CNS', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'CNS', 'Sacrifice Sakura-Tribe Elder: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_image_name('sakura-tribe elder'/'CNS', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'CNS', 'CNS:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'CNS', 'Common').
card_artist('sakura-tribe elder'/'CNS', 'Carl Critchlow').
card_number('sakura-tribe elder'/'CNS', '177').
card_flavor_text('sakura-tribe elder'/'CNS', 'Slain warriors were buried with a tree sapling, so they would become a part of the forest after death.').
card_multiverse_id('sakura-tribe elder'/'CNS', '382351').

card_in_set('scaled wurm', 'CNS').
card_original_type('scaled wurm'/'CNS', 'Creature — Wurm').
card_original_text('scaled wurm'/'CNS', '').
card_image_name('scaled wurm'/'CNS', 'scaled wurm').
card_uid('scaled wurm'/'CNS', 'CNS:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'CNS', 'Common').
card_artist('scaled wurm'/'CNS', 'Wayne England').
card_number('scaled wurm'/'CNS', '178').
card_flavor_text('scaled wurm'/'CNS', 'The wurms become so big, a single scale can be used as a roof for a human dwelling.').
card_multiverse_id('scaled wurm'/'CNS', '382352').

card_in_set('scourge of the throne', 'CNS').
card_original_type('scourge of the throne'/'CNS', 'Creature — Dragon').
card_original_text('scourge of the throne'/'CNS', 'Flying\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nWhenever Scourge of the Throne attacks for the first time each turn, if it\'s attacking the player with the most life or tied for most life, untap all attacking creatures. After this phase, there is an additional combat phase.').
card_first_print('scourge of the throne', 'CNS').
card_image_name('scourge of the throne'/'CNS', 'scourge of the throne').
card_uid('scourge of the throne'/'CNS', 'CNS:Scourge of the Throne:scourge of the throne').
card_rarity('scourge of the throne'/'CNS', 'Mythic Rare').
card_artist('scourge of the throne'/'CNS', 'Michael Komarck').
card_number('scourge of the throne'/'CNS', '35').
card_multiverse_id('scourge of the throne'/'CNS', '382353').

card_in_set('screaming seahawk', 'CNS').
card_original_type('screaming seahawk'/'CNS', 'Creature — Bird').
card_original_text('screaming seahawk'/'CNS', 'Flying\nWhen Screaming Seahawk enters the battlefield, you may search your library for a card named Screaming Seahawk, reveal it, and put it into your hand. If you do, shuffle your library.').
card_image_name('screaming seahawk'/'CNS', 'screaming seahawk').
card_uid('screaming seahawk'/'CNS', 'CNS:Screaming Seahawk:screaming seahawk').
card_rarity('screaming seahawk'/'CNS', 'Common').
card_artist('screaming seahawk'/'CNS', 'Heather Hudson').
card_number('screaming seahawk'/'CNS', '105').
card_multiverse_id('screaming seahawk'/'CNS', '382354').

card_in_set('secret summoning', 'CNS').
card_original_type('secret summoning'/'CNS', 'Conspiracy').
card_original_text('secret summoning'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nWhenever a creature with the chosen name enters the battlefield under your control, you may search your library for any number of cards with that name, reveal them, put them into your hand, then shuffle your library.').
card_first_print('secret summoning', 'CNS').
card_image_name('secret summoning'/'CNS', 'secret summoning').
card_uid('secret summoning'/'CNS', 'CNS:Secret Summoning:secret summoning').
card_rarity('secret summoning'/'CNS', 'Uncommon').
card_artist('secret summoning'/'CNS', 'Lucas Graciano').
card_number('secret summoning'/'CNS', '9').
card_multiverse_id('secret summoning'/'CNS', '382355').

card_in_set('secrets of paradise', 'CNS').
card_original_type('secrets of paradise'/'CNS', 'Conspiracy').
card_original_text('secrets of paradise'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nCreatures you control with the chosen name have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('secrets of paradise', 'CNS').
card_image_name('secrets of paradise'/'CNS', 'secrets of paradise').
card_uid('secrets of paradise'/'CNS', 'CNS:Secrets of Paradise:secrets of paradise').
card_rarity('secrets of paradise'/'CNS', 'Common').
card_artist('secrets of paradise'/'CNS', 'Tyler Jacobson').
card_number('secrets of paradise'/'CNS', '10').
card_multiverse_id('secrets of paradise'/'CNS', '382356').

card_in_set('selvala\'s charge', 'CNS').
card_original_type('selvala\'s charge'/'CNS', 'Sorcery').
card_original_text('selvala\'s charge'/'CNS', 'Parley — Each player reveals the top card of his or her library. For each nonland card revealed this way, you put a 3/3 green Elephant creature token onto the battlefield. Then each player draws a card.').
card_first_print('selvala\'s charge', 'CNS').
card_image_name('selvala\'s charge'/'CNS', 'selvala\'s charge').
card_uid('selvala\'s charge'/'CNS', 'CNS:Selvala\'s Charge:selvala\'s charge').
card_rarity('selvala\'s charge'/'CNS', 'Uncommon').
card_artist('selvala\'s charge'/'CNS', 'Dan Scott').
card_number('selvala\'s charge'/'CNS', '39').
card_flavor_text('selvala\'s charge'/'CNS', 'Selvala\'s arrival never goes unnoticed.').
card_multiverse_id('selvala\'s charge'/'CNS', '382358').

card_in_set('selvala\'s enforcer', 'CNS').
card_original_type('selvala\'s enforcer'/'CNS', 'Creature — Elf Warrior').
card_original_text('selvala\'s enforcer'/'CNS', 'Parley — When Selvala\'s Enforcer enters the battlefield, each player reveals the top card of his or her library. For each nonland card revealed this way, put a +1/+1 counter on Selvala\'s Enforcer. Then each player draws a card.').
card_first_print('selvala\'s enforcer', 'CNS').
card_image_name('selvala\'s enforcer'/'CNS', 'selvala\'s enforcer').
card_uid('selvala\'s enforcer'/'CNS', 'CNS:Selvala\'s Enforcer:selvala\'s enforcer').
card_rarity('selvala\'s enforcer'/'CNS', 'Common').
card_artist('selvala\'s enforcer'/'CNS', 'Jesper Ejsing').
card_number('selvala\'s enforcer'/'CNS', '40').
card_multiverse_id('selvala\'s enforcer'/'CNS', '382359').

card_in_set('selvala, explorer returned', 'CNS').
card_original_type('selvala, explorer returned'/'CNS', 'Legendary Creature — Elf Scout').
card_original_text('selvala, explorer returned'/'CNS', 'Parley — {T}: Each player reveals the top card of his or her library. For each nonland card revealed this way, add {G} to your mana pool and you gain 1 life. Then each player draws a card.').
card_first_print('selvala, explorer returned', 'CNS').
card_image_name('selvala, explorer returned'/'CNS', 'selvala, explorer returned').
card_uid('selvala, explorer returned'/'CNS', 'CNS:Selvala, Explorer Returned:selvala, explorer returned').
card_rarity('selvala, explorer returned'/'CNS', 'Rare').
card_artist('selvala, explorer returned'/'CNS', 'Tyler Jacobson').
card_number('selvala, explorer returned'/'CNS', '51').
card_flavor_text('selvala, explorer returned'/'CNS', '\"The Lowlands refuse to suffer at the whims of the High City.\"').
card_multiverse_id('selvala, explorer returned'/'CNS', '382357').

card_in_set('sentinel dispatch', 'CNS').
card_original_type('sentinel dispatch'/'CNS', 'Conspiracy').
card_original_text('sentinel dispatch'/'CNS', '(Start the game with this conspiracy face up in the command zone.)\nAt the beginning of the first upkeep, put a 1/1 colorless Construct artifact creature token with defender onto the battlefield.').
card_first_print('sentinel dispatch', 'CNS').
card_image_name('sentinel dispatch'/'CNS', 'sentinel dispatch').
card_uid('sentinel dispatch'/'CNS', 'CNS:Sentinel Dispatch:sentinel dispatch').
card_rarity('sentinel dispatch'/'CNS', 'Common').
card_artist('sentinel dispatch'/'CNS', 'Adam Paquette').
card_number('sentinel dispatch'/'CNS', '11').
card_flavor_text('sentinel dispatch'/'CNS', '\"Unusual. Security also worth stealing.\"\n—Dack Fayden').
card_multiverse_id('sentinel dispatch'/'CNS', '382360').

card_in_set('shoreline ranger', 'CNS').
card_original_type('shoreline ranger'/'CNS', 'Creature — Bird Soldier').
card_original_text('shoreline ranger'/'CNS', 'Flying\nIslandcycling {2} ({2}, Discard this card: Search your library for an Island card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('shoreline ranger'/'CNS', 'shoreline ranger').
card_uid('shoreline ranger'/'CNS', 'CNS:Shoreline Ranger:shoreline ranger').
card_rarity('shoreline ranger'/'CNS', 'Common').
card_artist('shoreline ranger'/'CNS', 'Michael Sutfin').
card_number('shoreline ranger'/'CNS', '106').
card_multiverse_id('shoreline ranger'/'CNS', '382361').

card_in_set('silent arbiter', 'CNS').
card_original_type('silent arbiter'/'CNS', 'Artifact Creature — Construct').
card_original_text('silent arbiter'/'CNS', 'No more than one creature can attack each combat.\nNo more than one creature can block each combat.').
card_image_name('silent arbiter'/'CNS', 'silent arbiter').
card_uid('silent arbiter'/'CNS', 'CNS:Silent Arbiter:silent arbiter').
card_rarity('silent arbiter'/'CNS', 'Rare').
card_artist('silent arbiter'/'CNS', 'Mark Zug').
card_number('silent arbiter'/'CNS', '204').
card_flavor_text('silent arbiter'/'CNS', 'Scholars say that arbiters exist on every world, created by an unknown hand to enforce justice.').
card_multiverse_id('silent arbiter'/'CNS', '382362').

card_in_set('silverchase fox', 'CNS').
card_original_type('silverchase fox'/'CNS', 'Creature — Fox').
card_original_text('silverchase fox'/'CNS', '{1}{W}, Sacrifice Silverchase Fox: Exile target enchantment.').
card_image_name('silverchase fox'/'CNS', 'silverchase fox').
card_uid('silverchase fox'/'CNS', 'CNS:Silverchase Fox:silverchase fox').
card_rarity('silverchase fox'/'CNS', 'Common').
card_artist('silverchase fox'/'CNS', 'Howard Lyon').
card_number('silverchase fox'/'CNS', '81').
card_flavor_text('silverchase fox'/'CNS', '\"Be careful in your hunt. The clever little creature can steal your wards and lead you right into the werewolf\'s den.\"\n—Britta, midwife of Gatstaf').
card_multiverse_id('silverchase fox'/'CNS', '382363').

card_in_set('skeletal scrying', 'CNS').
card_original_type('skeletal scrying'/'CNS', 'Instant').
card_original_text('skeletal scrying'/'CNS', 'As an additional cost to cast Skeletal Scrying, exile X cards from your graveyard.\nYou draw X cards and you lose X life.').
card_image_name('skeletal scrying'/'CNS', 'skeletal scrying').
card_uid('skeletal scrying'/'CNS', 'CNS:Skeletal Scrying:skeletal scrying').
card_rarity('skeletal scrying'/'CNS', 'Uncommon').
card_artist('skeletal scrying'/'CNS', 'Bob Petillo').
card_number('skeletal scrying'/'CNS', '124').
card_flavor_text('skeletal scrying'/'CNS', 'His family was a small price to pay for the dark gifts that called to him.').
card_multiverse_id('skeletal scrying'/'CNS', '382364').

card_in_set('skitter of lizards', 'CNS').
card_original_type('skitter of lizards'/'CNS', 'Creature — Lizard').
card_original_text('skitter of lizards'/'CNS', 'Multikicker {1}{R} (You may pay an additional {1}{R} any number of times as you cast this spell.)\nHaste\nSkitter of Lizards enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('skitter of lizards'/'CNS', 'skitter of lizards').
card_uid('skitter of lizards'/'CNS', 'CNS:Skitter of Lizards:skitter of lizards').
card_rarity('skitter of lizards'/'CNS', 'Common').
card_artist('skitter of lizards'/'CNS', 'Warren Mahy').
card_number('skitter of lizards'/'CNS', '151').
card_multiverse_id('skitter of lizards'/'CNS', '382365').

card_in_set('sky spirit', 'CNS').
card_original_type('sky spirit'/'CNS', 'Creature — Spirit').
card_original_text('sky spirit'/'CNS', 'Flying, first strike').
card_image_name('sky spirit'/'CNS', 'sky spirit').
card_uid('sky spirit'/'CNS', 'CNS:Sky Spirit:sky spirit').
card_rarity('sky spirit'/'CNS', 'Uncommon').
card_artist('sky spirit'/'CNS', 'Rebecca Guay').
card_number('sky spirit'/'CNS', '192').
card_flavor_text('sky spirit'/'CNS', '\"Like a strain of music: easy to remember but impossible to catch.\"\n—Mirri of the Weatherlight').
card_multiverse_id('sky spirit'/'CNS', '382366').

card_in_set('smallpox', 'CNS').
card_original_type('smallpox'/'CNS', 'Sorcery').
card_original_text('smallpox'/'CNS', 'Each player loses 1 life, discards a card, sacrifices a creature, then sacrifices a land.').
card_image_name('smallpox'/'CNS', 'smallpox').
card_uid('smallpox'/'CNS', 'CNS:Smallpox:smallpox').
card_rarity('smallpox'/'CNS', 'Uncommon').
card_artist('smallpox'/'CNS', 'Ryan Pancoast').
card_number('smallpox'/'CNS', '125').
card_flavor_text('smallpox'/'CNS', 'Take away a few things, and a rebellion may ensue. Take away everything, and the oppression will be accepted as fate.').
card_multiverse_id('smallpox'/'CNS', '382367').

card_in_set('soulcatcher', 'CNS').
card_original_type('soulcatcher'/'CNS', 'Creature — Bird Soldier').
card_original_text('soulcatcher'/'CNS', 'Flying\nWhenever a creature with flying dies, put a +1/+1 counter on Soulcatcher.').
card_image_name('soulcatcher'/'CNS', 'soulcatcher').
card_uid('soulcatcher'/'CNS', 'CNS:Soulcatcher:soulcatcher').
card_rarity('soulcatcher'/'CNS', 'Uncommon').
card_artist('soulcatcher'/'CNS', 'Julie Dillon').
card_number('soulcatcher'/'CNS', '82').
card_flavor_text('soulcatcher'/'CNS', '\"Their spirits walk the winds and fill my soul.\"').
card_multiverse_id('soulcatcher'/'CNS', '382368').

card_in_set('spectral searchlight', 'CNS').
card_original_type('spectral searchlight'/'CNS', 'Artifact').
card_original_text('spectral searchlight'/'CNS', '{T}: Choose a player. That player adds one mana of any color he or she chooses to his or her mana pool.').
card_image_name('spectral searchlight'/'CNS', 'spectral searchlight').
card_uid('spectral searchlight'/'CNS', 'CNS:Spectral Searchlight:spectral searchlight').
card_rarity('spectral searchlight'/'CNS', 'Uncommon').
card_artist('spectral searchlight'/'CNS', 'Martina Pilcerova').
card_number('spectral searchlight'/'CNS', '205').
card_flavor_text('spectral searchlight'/'CNS', 'The first searchlights were given as gifts, symbols of cooperation, to the emissaries present at the signing of the Guildpact.').
card_multiverse_id('spectral searchlight'/'CNS', '382369').

card_in_set('spiritmonger', 'CNS').
card_original_type('spiritmonger'/'CNS', 'Creature — Beast').
card_original_text('spiritmonger'/'CNS', 'Whenever Spiritmonger deals damage to a creature, put a +1/+1 counter on Spiritmonger.\n{B}: Regenerate Spiritmonger.\n{G}: Spiritmonger becomes the color of your choice until end of turn.').
card_image_name('spiritmonger'/'CNS', 'spiritmonger').
card_uid('spiritmonger'/'CNS', 'CNS:Spiritmonger:spiritmonger').
card_rarity('spiritmonger'/'CNS', 'Rare').
card_artist('spiritmonger'/'CNS', 'Kev Walker').
card_number('spiritmonger'/'CNS', '193').
card_multiverse_id('spiritmonger'/'CNS', '382370').

card_in_set('split decision', 'CNS').
card_original_type('split decision'/'CNS', 'Instant').
card_original_text('split decision'/'CNS', 'Will of the council — Choose target instant or sorcery spell. Starting with you, each player votes for denial or duplication. If denial gets more votes, counter the spell. If duplication gets more votes or the vote is tied, copy the spell. You may choose new targets for the copy.').
card_first_print('split decision', 'CNS').
card_image_name('split decision'/'CNS', 'split decision').
card_uid('split decision'/'CNS', 'CNS:Split Decision:split decision').
card_rarity('split decision'/'CNS', 'Uncommon').
card_artist('split decision'/'CNS', 'Robbie Trevino').
card_number('split decision'/'CNS', '25').
card_multiverse_id('split decision'/'CNS', '382371').

card_in_set('spontaneous combustion', 'CNS').
card_original_type('spontaneous combustion'/'CNS', 'Instant').
card_original_text('spontaneous combustion'/'CNS', 'As an additional cost to cast Spontaneous Combustion, sacrifice a creature.\nSpontaneous Combustion deals 3 damage to each creature.').
card_image_name('spontaneous combustion'/'CNS', 'spontaneous combustion').
card_uid('spontaneous combustion'/'CNS', 'CNS:Spontaneous Combustion:spontaneous combustion').
card_rarity('spontaneous combustion'/'CNS', 'Uncommon').
card_artist('spontaneous combustion'/'CNS', 'Jason Rainville').
card_number('spontaneous combustion'/'CNS', '194').
card_flavor_text('spontaneous combustion'/'CNS', '\"Heat of battle\" is usually a metaphor.').
card_multiverse_id('spontaneous combustion'/'CNS', '382372').

card_in_set('sporecap spider', 'CNS').
card_original_type('sporecap spider'/'CNS', 'Creature — Spider').
card_original_text('sporecap spider'/'CNS', 'Reach (This creature can block creatures with flying.)').
card_image_name('sporecap spider'/'CNS', 'sporecap spider').
card_uid('sporecap spider'/'CNS', 'CNS:Sporecap Spider:sporecap spider').
card_rarity('sporecap spider'/'CNS', 'Common').
card_artist('sporecap spider'/'CNS', 'Lars Grant-West').
card_number('sporecap spider'/'CNS', '179').
card_flavor_text('sporecap spider'/'CNS', '\"They don\'t move much, but then again, if you get caught in its web, it has all the time in the world to get to you.\"\n—Saidah, Joraga hunter').
card_multiverse_id('sporecap spider'/'CNS', '382373').

card_in_set('squirrel nest', 'CNS').
card_original_type('squirrel nest'/'CNS', 'Enchantment — Aura').
card_original_text('squirrel nest'/'CNS', 'Enchant land\nEnchanted land has \"{T}: Put a 1/1 green Squirrel creature token onto the battlefield.\"').
card_image_name('squirrel nest'/'CNS', 'squirrel nest').
card_uid('squirrel nest'/'CNS', 'CNS:Squirrel Nest:squirrel nest').
card_rarity('squirrel nest'/'CNS', 'Uncommon').
card_artist('squirrel nest'/'CNS', 'Daniel Ljunggren').
card_number('squirrel nest'/'CNS', '180').
card_flavor_text('squirrel nest'/'CNS', 'In the treetops, unseen by many, lurks chittering, skittering death.').
card_multiverse_id('squirrel nest'/'CNS', '382374').

card_in_set('stasis cell', 'CNS').
card_original_type('stasis cell'/'CNS', 'Enchantment — Aura').
card_original_text('stasis cell'/'CNS', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\n{3}{U}: Attach Stasis Cell to target creature.').
card_image_name('stasis cell'/'CNS', 'stasis cell').
card_uid('stasis cell'/'CNS', 'CNS:Stasis Cell:stasis cell').
card_rarity('stasis cell'/'CNS', 'Common').
card_artist('stasis cell'/'CNS', 'Mark A. Nelson').
card_number('stasis cell'/'CNS', '107').
card_flavor_text('stasis cell'/'CNS', 'The Simic created the cells to preserve their experiments. The Azorius put the cells to use on the guilty.').
card_multiverse_id('stasis cell'/'CNS', '382375').

card_in_set('stave off', 'CNS').
card_original_type('stave off'/'CNS', 'Instant').
card_original_text('stave off'/'CNS', 'Target creature gains protection from the color of your choice until end of turn.').
card_image_name('stave off'/'CNS', 'stave off').
card_uid('stave off'/'CNS', 'CNS:Stave Off:stave off').
card_rarity('stave off'/'CNS', 'Common').
card_artist('stave off'/'CNS', 'Mark Zug').
card_number('stave off'/'CNS', '83').
card_flavor_text('stave off'/'CNS', '\"Just a moment of perfect grace and sanctity is enough.\"\n—Kal Lior, leonin elder').
card_multiverse_id('stave off'/'CNS', '382376').

card_in_set('stifle', 'CNS').
card_original_type('stifle'/'CNS', 'Instant').
card_original_text('stifle'/'CNS', 'Counter target activated or triggered ability. (Mana abilities can\'t be targeted.)').
card_image_name('stifle'/'CNS', 'stifle').
card_uid('stifle'/'CNS', 'CNS:Stifle:stifle').
card_rarity('stifle'/'CNS', 'Rare').
card_artist('stifle'/'CNS', 'Eric Fortune').
card_number('stifle'/'CNS', '108').
card_flavor_text('stifle'/'CNS', '\"Superior force meets woefully inadequate object. What paradox were you expecting?\"\n—Azguri, archmage of Evos Isle').
card_multiverse_id('stifle'/'CNS', '382377').

card_in_set('stronghold discipline', 'CNS').
card_original_type('stronghold discipline'/'CNS', 'Sorcery').
card_original_text('stronghold discipline'/'CNS', 'Each player loses 1 life for each creature he or she controls.').
card_image_name('stronghold discipline'/'CNS', 'stronghold discipline').
card_uid('stronghold discipline'/'CNS', 'CNS:Stronghold Discipline:stronghold discipline').
card_rarity('stronghold discipline'/'CNS', 'Common').
card_artist('stronghold discipline'/'CNS', 'Daarken').
card_number('stronghold discipline'/'CNS', '126').
card_flavor_text('stronghold discipline'/'CNS', 'In Urborg, the instigators of each new rebellion are soaked in the blood of the last.').
card_multiverse_id('stronghold discipline'/'CNS', '382378').

card_in_set('sulfuric vortex', 'CNS').
card_original_type('sulfuric vortex'/'CNS', 'Enchantment').
card_original_text('sulfuric vortex'/'CNS', 'At the beginning of each player\'s upkeep, Sulfuric Vortex deals 2 damage to that player.\nIf a player would gain life, that player gains no life instead.').
card_image_name('sulfuric vortex'/'CNS', 'sulfuric vortex').
card_uid('sulfuric vortex'/'CNS', 'CNS:Sulfuric Vortex:sulfuric vortex').
card_rarity('sulfuric vortex'/'CNS', 'Rare').
card_artist('sulfuric vortex'/'CNS', 'Greg Staples').
card_number('sulfuric vortex'/'CNS', '152').
card_multiverse_id('sulfuric vortex'/'CNS', '382379').

card_in_set('swords to plowshares', 'CNS').
card_original_type('swords to plowshares'/'CNS', 'Instant').
card_original_text('swords to plowshares'/'CNS', 'Exile target creature. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'CNS', 'swords to plowshares').
card_uid('swords to plowshares'/'CNS', 'CNS:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'CNS', 'Uncommon').
card_artist('swords to plowshares'/'CNS', 'Terese Nielsen').
card_number('swords to plowshares'/'CNS', '84').
card_flavor_text('swords to plowshares'/'CNS', 'The smallest seed of regret can bloom into redemption.').
card_multiverse_id('swords to plowshares'/'CNS', '382380').

card_in_set('syphon soul', 'CNS').
card_original_type('syphon soul'/'CNS', 'Sorcery').
card_original_text('syphon soul'/'CNS', 'Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.').
card_image_name('syphon soul'/'CNS', 'syphon soul').
card_uid('syphon soul'/'CNS', 'CNS:Syphon Soul:syphon soul').
card_rarity('syphon soul'/'CNS', 'Common').
card_artist('syphon soul'/'CNS', 'Ron Spears').
card_number('syphon soul'/'CNS', '127').
card_flavor_text('syphon soul'/'CNS', 'As Phage drank their energy, a vague memory of Jeska stirred. Then she lost herself again in the joy of her victims\' suffering.').
card_multiverse_id('syphon soul'/'CNS', '382381').

card_in_set('terastodon', 'CNS').
card_original_type('terastodon'/'CNS', 'Creature — Elephant').
card_original_text('terastodon'/'CNS', 'When Terastodon enters the battlefield, you may destroy up to three target noncreature permanents. For each permanent put into a graveyard this way, its controller puts a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('terastodon'/'CNS', 'terastodon').
card_uid('terastodon'/'CNS', 'CNS:Terastodon:terastodon').
card_rarity('terastodon'/'CNS', 'Rare').
card_artist('terastodon'/'CNS', 'Lars Grant-West').
card_number('terastodon'/'CNS', '181').
card_multiverse_id('terastodon'/'CNS', '382382').

card_in_set('torch fiend', 'CNS').
card_original_type('torch fiend'/'CNS', 'Creature — Devil').
card_original_text('torch fiend'/'CNS', '{R}, Sacrifice Torch Fiend: Destroy target artifact.').
card_image_name('torch fiend'/'CNS', 'torch fiend').
card_uid('torch fiend'/'CNS', 'CNS:Torch Fiend:torch fiend').
card_rarity('torch fiend'/'CNS', 'Common').
card_artist('torch fiend'/'CNS', 'Winona Nelson').
card_number('torch fiend'/'CNS', '153').
card_flavor_text('torch fiend'/'CNS', 'Devils redecorate every room with fire.').
card_multiverse_id('torch fiend'/'CNS', '382383').

card_in_set('tragic slip', 'CNS').
card_original_type('tragic slip'/'CNS', 'Instant').
card_original_text('tragic slip'/'CNS', 'Target creature gets -1/-1 until end of turn.\nMorbid — That creature gets -13/-13 until end of turn instead if a creature died this turn.').
card_image_name('tragic slip'/'CNS', 'tragic slip').
card_uid('tragic slip'/'CNS', 'CNS:Tragic Slip:tragic slip').
card_rarity('tragic slip'/'CNS', 'Common').
card_artist('tragic slip'/'CNS', 'Christopher Moeller').
card_number('tragic slip'/'CNS', '128').
card_flavor_text('tragic slip'/'CNS', 'Linger on death\'s door and risk being invited in.').
card_multiverse_id('tragic slip'/'CNS', '382384').

card_in_set('traveler\'s cloak', 'CNS').
card_original_type('traveler\'s cloak'/'CNS', 'Enchantment — Aura').
card_original_text('traveler\'s cloak'/'CNS', 'Enchant creature\nAs Traveler\'s Cloak enters the battlefield, choose a land type.\nWhen Traveler\'s Cloak enters the battlefield, draw a card.\nEnchanted creature has landwalk of the chosen type.').
card_image_name('traveler\'s cloak'/'CNS', 'traveler\'s cloak').
card_uid('traveler\'s cloak'/'CNS', 'CNS:Traveler\'s Cloak:traveler\'s cloak').
card_rarity('traveler\'s cloak'/'CNS', 'Common').
card_artist('traveler\'s cloak'/'CNS', 'Rebecca Guay').
card_number('traveler\'s cloak'/'CNS', '109').
card_multiverse_id('traveler\'s cloak'/'CNS', '382385').

card_in_set('treasonous ogre', 'CNS').
card_original_type('treasonous ogre'/'CNS', 'Creature — Ogre Shaman').
card_original_text('treasonous ogre'/'CNS', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nPay 3 life: Add {R} to your mana pool.').
card_first_print('treasonous ogre', 'CNS').
card_image_name('treasonous ogre'/'CNS', 'treasonous ogre').
card_uid('treasonous ogre'/'CNS', 'CNS:Treasonous Ogre:treasonous ogre').
card_rarity('treasonous ogre'/'CNS', 'Uncommon').
card_artist('treasonous ogre'/'CNS', 'Randy Gallegos').
card_number('treasonous ogre'/'CNS', '36').
card_flavor_text('treasonous ogre'/'CNS', '\"Everything comes with a price. Especially revolution.\"\n—Grenzo, dungeon warden').
card_multiverse_id('treasonous ogre'/'CNS', '382386').

card_in_set('trumpet blast', 'CNS').
card_original_type('trumpet blast'/'CNS', 'Instant').
card_original_text('trumpet blast'/'CNS', 'Attacking creatures get +2/+0 until end of turn.').
card_image_name('trumpet blast'/'CNS', 'trumpet blast').
card_uid('trumpet blast'/'CNS', 'CNS:Trumpet Blast:trumpet blast').
card_rarity('trumpet blast'/'CNS', 'Common').
card_artist('trumpet blast'/'CNS', 'Carl Critchlow').
card_number('trumpet blast'/'CNS', '154').
card_flavor_text('trumpet blast'/'CNS', 'Keldon warriors don\'t need signals to tell them when to attack. They need signals to tell them when to stop.').
card_multiverse_id('trumpet blast'/'CNS', '382387').

card_in_set('turn the tide', 'CNS').
card_original_type('turn the tide'/'CNS', 'Instant').
card_original_text('turn the tide'/'CNS', 'Creatures your opponents control get -2/-0 until end of turn.').
card_image_name('turn the tide'/'CNS', 'turn the tide').
card_uid('turn the tide'/'CNS', 'CNS:Turn the Tide:turn the tide').
card_rarity('turn the tide'/'CNS', 'Common').
card_artist('turn the tide'/'CNS', 'Jason Felix').
card_number('turn the tide'/'CNS', '110').
card_flavor_text('turn the tide'/'CNS', '\"Let their mindless armies come and face the might of genius.\"\n—Varil, Neurok partisan').
card_multiverse_id('turn the tide'/'CNS', '382388').

card_in_set('twisted abomination', 'CNS').
card_original_type('twisted abomination'/'CNS', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'CNS', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'CNS', 'twisted abomination').
card_uid('twisted abomination'/'CNS', 'CNS:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'CNS', 'Common').
card_artist('twisted abomination'/'CNS', 'Daren Bader').
card_number('twisted abomination'/'CNS', '129').
card_multiverse_id('twisted abomination'/'CNS', '382389').

card_in_set('typhoid rats', 'CNS').
card_original_type('typhoid rats'/'CNS', 'Creature — Rat').
card_original_text('typhoid rats'/'CNS', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('typhoid rats'/'CNS', 'typhoid rats').
card_uid('typhoid rats'/'CNS', 'CNS:Typhoid Rats:typhoid rats').
card_rarity('typhoid rats'/'CNS', 'Common').
card_artist('typhoid rats'/'CNS', 'Kev Walker').
card_number('typhoid rats'/'CNS', '130').
card_flavor_text('typhoid rats'/'CNS', 'Kidnappers caught in Havengul are given two choices: languish in prison or become rat catchers. The smart ones go to prison.').
card_multiverse_id('typhoid rats'/'CNS', '382390').

card_in_set('tyrant\'s choice', 'CNS').
card_original_type('tyrant\'s choice'/'CNS', 'Sorcery').
card_original_text('tyrant\'s choice'/'CNS', 'Will of the council — Starting with you, each player votes for death or torture. If death gets more votes, each opponent sacrifices a creature. If torture gets more votes or the vote is tied, each opponent loses 4 life.').
card_first_print('tyrant\'s choice', 'CNS').
card_image_name('tyrant\'s choice'/'CNS', 'tyrant\'s choice').
card_uid('tyrant\'s choice'/'CNS', 'CNS:Tyrant\'s Choice:tyrant\'s choice').
card_rarity('tyrant\'s choice'/'CNS', 'Common').
card_artist('tyrant\'s choice'/'CNS', 'Daarken').
card_number('tyrant\'s choice'/'CNS', '30').
card_multiverse_id('tyrant\'s choice'/'CNS', '382391').

card_in_set('uncontrollable anger', 'CNS').
card_original_type('uncontrollable anger'/'CNS', 'Enchantment — Aura').
card_original_text('uncontrollable anger'/'CNS', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_image_name('uncontrollable anger'/'CNS', 'uncontrollable anger').
card_uid('uncontrollable anger'/'CNS', 'CNS:Uncontrollable Anger:uncontrollable anger').
card_rarity('uncontrollable anger'/'CNS', 'Uncommon').
card_artist('uncontrollable anger'/'CNS', 'Kev Walker').
card_number('uncontrollable anger'/'CNS', '155').
card_multiverse_id('uncontrollable anger'/'CNS', '382392').

card_in_set('unexpected potential', 'CNS').
card_original_type('unexpected potential'/'CNS', 'Conspiracy').
card_original_text('unexpected potential'/'CNS', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nYou may spend mana as though it were mana of any color to cast spells with the chosen name.').
card_first_print('unexpected potential', 'CNS').
card_image_name('unexpected potential'/'CNS', 'unexpected potential').
card_uid('unexpected potential'/'CNS', 'CNS:Unexpected Potential:unexpected potential').
card_rarity('unexpected potential'/'CNS', 'Uncommon').
card_artist('unexpected potential'/'CNS', 'Izzy').
card_number('unexpected potential'/'CNS', '12').
card_multiverse_id('unexpected potential'/'CNS', '382393').

card_in_set('unhallowed pact', 'CNS').
card_original_type('unhallowed pact'/'CNS', 'Enchantment — Aura').
card_original_text('unhallowed pact'/'CNS', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.').
card_image_name('unhallowed pact'/'CNS', 'unhallowed pact').
card_uid('unhallowed pact'/'CNS', 'CNS:Unhallowed Pact:unhallowed pact').
card_rarity('unhallowed pact'/'CNS', 'Common').
card_artist('unhallowed pact'/'CNS', 'Volkan Baga').
card_number('unhallowed pact'/'CNS', '131').
card_flavor_text('unhallowed pact'/'CNS', 'Agreements with demons seldom end at the grave.').
card_multiverse_id('unhallowed pact'/'CNS', '382394').

card_in_set('unquestioned authority', 'CNS').
card_original_type('unquestioned authority'/'CNS', 'Enchantment — Aura').
card_original_text('unquestioned authority'/'CNS', 'Enchant creature\nWhen Unquestioned Authority enters the battlefield, draw a card.\nEnchanted creature has protection from creatures.').
card_image_name('unquestioned authority'/'CNS', 'unquestioned authority').
card_uid('unquestioned authority'/'CNS', 'CNS:Unquestioned Authority:unquestioned authority').
card_rarity('unquestioned authority'/'CNS', 'Uncommon').
card_artist('unquestioned authority'/'CNS', 'Zoltan Boros').
card_number('unquestioned authority'/'CNS', '85').
card_flavor_text('unquestioned authority'/'CNS', 'After Gideon cleansed the Ninth District, none of the ganglords challenged the Boros.').
card_multiverse_id('unquestioned authority'/'CNS', '382395').

card_in_set('valor made real', 'CNS').
card_original_type('valor made real'/'CNS', 'Instant').
card_original_text('valor made real'/'CNS', 'Target creature can block any number of creatures this turn.').
card_image_name('valor made real'/'CNS', 'valor made real').
card_uid('valor made real'/'CNS', 'CNS:Valor Made Real:valor made real').
card_rarity('valor made real'/'CNS', 'Common').
card_artist('valor made real'/'CNS', 'Jeff Miracola').
card_number('valor made real'/'CNS', '86').
card_flavor_text('valor made real'/'CNS', '\"As my father taught, ‘Training will raise your shield to the blow, but courage fills the gaps the shield leaves open.\'\"').
card_multiverse_id('valor made real'/'CNS', '382396').

card_in_set('vampire hexmage', 'CNS').
card_original_type('vampire hexmage'/'CNS', 'Creature — Vampire Shaman').
card_original_text('vampire hexmage'/'CNS', 'First strike\nSacrifice Vampire Hexmage: Remove all counters from target permanent.').
card_image_name('vampire hexmage'/'CNS', 'vampire hexmage').
card_uid('vampire hexmage'/'CNS', 'CNS:Vampire Hexmage:vampire hexmage').
card_rarity('vampire hexmage'/'CNS', 'Uncommon').
card_artist('vampire hexmage'/'CNS', 'Eric Deschamps').
card_number('vampire hexmage'/'CNS', '132').
card_flavor_text('vampire hexmage'/'CNS', 'When the blood hunt lost its thrill, she looked for less tangible means of domination.').
card_multiverse_id('vampire hexmage'/'CNS', '382397').

card_in_set('vedalken orrery', 'CNS').
card_original_type('vedalken orrery'/'CNS', 'Artifact').
card_original_text('vedalken orrery'/'CNS', 'You may cast nonland cards as though they had flash.').
card_image_name('vedalken orrery'/'CNS', 'vedalken orrery').
card_uid('vedalken orrery'/'CNS', 'CNS:Vedalken Orrery:vedalken orrery').
card_rarity('vedalken orrery'/'CNS', 'Rare').
card_artist('vedalken orrery'/'CNS', 'John Avon').
card_number('vedalken orrery'/'CNS', '206').
card_flavor_text('vedalken orrery'/'CNS', 'The model incorporated the fifth sun effortlessly, as if it had always known.').
card_multiverse_id('vedalken orrery'/'CNS', '382398').

card_in_set('vent sentinel', 'CNS').
card_original_type('vent sentinel'/'CNS', 'Creature — Elemental').
card_original_text('vent sentinel'/'CNS', 'Defender\n{1}{R}, {T}: Vent Sentinel deals damage to target player equal to the number of creatures with defender you control.').
card_image_name('vent sentinel'/'CNS', 'vent sentinel').
card_uid('vent sentinel'/'CNS', 'CNS:Vent Sentinel:vent sentinel').
card_rarity('vent sentinel'/'CNS', 'Common').
card_artist('vent sentinel'/'CNS', 'Chris Rahn').
card_number('vent sentinel'/'CNS', '156').
card_flavor_text('vent sentinel'/'CNS', '\"Zendikar fortifies itself against the Great Threat, making sentries of sky, sea, and stone.\"\n—The War Diaries').
card_multiverse_id('vent sentinel'/'CNS', '382399').

card_in_set('victimize', 'CNS').
card_original_type('victimize'/'CNS', 'Sorcery').
card_original_text('victimize'/'CNS', 'Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, return the chosen cards to the battlefield tapped.').
card_image_name('victimize'/'CNS', 'victimize').
card_uid('victimize'/'CNS', 'CNS:Victimize:victimize').
card_rarity('victimize'/'CNS', 'Uncommon').
card_artist('victimize'/'CNS', 'Craig J Spearing').
card_number('victimize'/'CNS', '133').
card_flavor_text('victimize'/'CNS', '\"Die. But die knowing that your pathetic life will yield a great bounty for me.\"\n—Zul Ashur, lich lord').
card_multiverse_id('victimize'/'CNS', '382400').

card_in_set('volcanic fallout', 'CNS').
card_original_type('volcanic fallout'/'CNS', 'Instant').
card_original_text('volcanic fallout'/'CNS', 'Volcanic Fallout can\'t be countered.\nVolcanic Fallout deals 2 damage to each creature and each player.').
card_image_name('volcanic fallout'/'CNS', 'volcanic fallout').
card_uid('volcanic fallout'/'CNS', 'CNS:Volcanic Fallout:volcanic fallout').
card_rarity('volcanic fallout'/'CNS', 'Uncommon').
card_artist('volcanic fallout'/'CNS', 'Zoltan Boros & Gabor Szikszai').
card_number('volcanic fallout'/'CNS', '157').
card_flavor_text('volcanic fallout'/'CNS', '\"How can we outrun the sky?\"\n—Hadran, sunseeder of Naya').
card_multiverse_id('volcanic fallout'/'CNS', '382401').

card_in_set('vow of duty', 'CNS').
card_original_type('vow of duty'/'CNS', 'Enchantment — Aura').
card_original_text('vow of duty'/'CNS', 'Enchant creature\nEnchanted creature gets +2/+2, has vigilance, and can\'t attack you or a planeswalker you control.').
card_image_name('vow of duty'/'CNS', 'vow of duty').
card_uid('vow of duty'/'CNS', 'CNS:Vow of Duty:vow of duty').
card_rarity('vow of duty'/'CNS', 'Uncommon').
card_artist('vow of duty'/'CNS', 'Wayne Reynolds').
card_number('vow of duty'/'CNS', '87').
card_flavor_text('vow of duty'/'CNS', '\"I prefer loyalty to be a matter of respect, not of magic. But one does what one must.\"\n—Kaalia of the Vast').
card_multiverse_id('vow of duty'/'CNS', '382402').

card_in_set('wakedancer', 'CNS').
card_original_type('wakedancer'/'CNS', 'Creature — Human Shaman').
card_original_text('wakedancer'/'CNS', 'Morbid — When Wakedancer enters the battlefield, if a creature died this turn, put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('wakedancer'/'CNS', 'wakedancer').
card_uid('wakedancer'/'CNS', 'CNS:Wakedancer:wakedancer').
card_rarity('wakedancer'/'CNS', 'Common').
card_artist('wakedancer'/'CNS', 'Austin Hsu').
card_number('wakedancer'/'CNS', '134').
card_flavor_text('wakedancer'/'CNS', 'Hers is an ancient form of necromancy, steeped in shamanic trance and ritual that few skaberen or ghoulcallers comprehend.').
card_multiverse_id('wakedancer'/'CNS', '382403').

card_in_set('wakestone gargoyle', 'CNS').
card_original_type('wakestone gargoyle'/'CNS', 'Creature — Gargoyle').
card_original_text('wakestone gargoyle'/'CNS', 'Defender, flying\n{1}{W}: Creatures you control with defender can attack this turn as though they didn\'t have defender.').
card_image_name('wakestone gargoyle'/'CNS', 'wakestone gargoyle').
card_uid('wakestone gargoyle'/'CNS', 'CNS:Wakestone Gargoyle:wakestone gargoyle').
card_rarity('wakestone gargoyle'/'CNS', 'Uncommon').
card_artist('wakestone gargoyle'/'CNS', 'Jim Murray').
card_number('wakestone gargoyle'/'CNS', '88').
card_flavor_text('wakestone gargoyle'/'CNS', 'Its pulsating cry shatters bonds of iron, granite, and servitude.').
card_multiverse_id('wakestone gargoyle'/'CNS', '382404').

card_in_set('warmonger\'s chariot', 'CNS').
card_original_type('warmonger\'s chariot'/'CNS', 'Artifact — Equipment').
card_original_text('warmonger\'s chariot'/'CNS', 'Equipped creature gets +2/+2.\nAs long as equipped creature has defender, it can attack as though it didn\'t have defender.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('warmonger\'s chariot'/'CNS', 'warmonger\'s chariot').
card_uid('warmonger\'s chariot'/'CNS', 'CNS:Warmonger\'s Chariot:warmonger\'s chariot').
card_rarity('warmonger\'s chariot'/'CNS', 'Uncommon').
card_artist('warmonger\'s chariot'/'CNS', 'Warren Mahy').
card_number('warmonger\'s chariot'/'CNS', '207').
card_flavor_text('warmonger\'s chariot'/'CNS', 'The best defense is a good offense.').
card_multiverse_id('warmonger\'s chariot'/'CNS', '382405').

card_in_set('whispergear sneak', 'CNS').
card_original_type('whispergear sneak'/'CNS', 'Artifact Creature — Construct').
card_original_text('whispergear sneak'/'CNS', 'Draft Whispergear Sneak face up.\nDuring the draft, you may turn Whispergear Sneak face down. If you do, look at any unopened booster pack in the draft or any booster pack not being looked at by another player.').
card_first_print('whispergear sneak', 'CNS').
card_image_name('whispergear sneak'/'CNS', 'whispergear sneak').
card_uid('whispergear sneak'/'CNS', 'CNS:Whispergear Sneak:whispergear sneak').
card_rarity('whispergear sneak'/'CNS', 'Common').
card_artist('whispergear sneak'/'CNS', 'Jesper Ejsing').
card_number('whispergear sneak'/'CNS', '64').
card_flavor_text('whispergear sneak'/'CNS', 'Spy for today. Plan for tomorrow.').
card_multiverse_id('whispergear sneak'/'CNS', '382406').

card_in_set('wind dancer', 'CNS').
card_original_type('wind dancer'/'CNS', 'Creature — Faerie').
card_original_text('wind dancer'/'CNS', 'Flying\n{T}: Target creature gains flying until end of turn.').
card_image_name('wind dancer'/'CNS', 'wind dancer').
card_uid('wind dancer'/'CNS', 'CNS:Wind Dancer:wind dancer').
card_rarity('wind dancer'/'CNS', 'Uncommon').
card_artist('wind dancer'/'CNS', 'Cynthia Sheppard').
card_number('wind dancer'/'CNS', '111').
card_flavor_text('wind dancer'/'CNS', '\"Take my hand and dance to the music of the winds.\"').
card_multiverse_id('wind dancer'/'CNS', '382407').

card_in_set('wolfbriar elemental', 'CNS').
card_original_type('wolfbriar elemental'/'CNS', 'Creature — Elemental').
card_original_text('wolfbriar elemental'/'CNS', 'Multikicker {G} (You may pay an additional {G} any number of times as you cast this spell.)\nWhen Wolfbriar Elemental enters the battlefield, put a 2/2 green Wolf creature token onto the battlefield for each time it was kicked.').
card_image_name('wolfbriar elemental'/'CNS', 'wolfbriar elemental').
card_uid('wolfbriar elemental'/'CNS', 'CNS:Wolfbriar Elemental:wolfbriar elemental').
card_rarity('wolfbriar elemental'/'CNS', 'Rare').
card_artist('wolfbriar elemental'/'CNS', 'Chippy').
card_number('wolfbriar elemental'/'CNS', '182').
card_multiverse_id('wolfbriar elemental'/'CNS', '382408').

card_in_set('wood sage', 'CNS').
card_original_type('wood sage'/'CNS', 'Creature — Human Druid').
card_original_text('wood sage'/'CNS', '{T}: Name a creature card. Reveal the top four cards of your library and put all of them with that name into your hand. Put the rest into your graveyard.').
card_image_name('wood sage'/'CNS', 'wood sage').
card_uid('wood sage'/'CNS', 'CNS:Wood Sage:wood sage').
card_rarity('wood sage'/'CNS', 'Uncommon').
card_artist('wood sage'/'CNS', 'Paolo Parente').
card_number('wood sage'/'CNS', '195').
card_multiverse_id('wood sage'/'CNS', '382409').

card_in_set('woodvine elemental', 'CNS').
card_original_type('woodvine elemental'/'CNS', 'Creature — Elemental').
card_original_text('woodvine elemental'/'CNS', 'Trample\nParley — Whenever Woodvine Elemental attacks, each player reveals the top card of his or her library. For each nonland card revealed this way, attacking creatures you control get +1/+1 until end of turn. Then each player draws a card.').
card_first_print('woodvine elemental', 'CNS').
card_image_name('woodvine elemental'/'CNS', 'woodvine elemental').
card_uid('woodvine elemental'/'CNS', 'CNS:Woodvine Elemental:woodvine elemental').
card_rarity('woodvine elemental'/'CNS', 'Uncommon').
card_artist('woodvine elemental'/'CNS', 'Mike Bierek').
card_number('woodvine elemental'/'CNS', '52').
card_multiverse_id('woodvine elemental'/'CNS', '382410').

card_in_set('worldknit', 'CNS').
card_original_type('worldknit'/'CNS', 'Conspiracy').
card_original_text('worldknit'/'CNS', '(Start the game with this conspiracy face up in the command zone.)\nAs long as every card in your card pool started the game in your library or in the command zone, lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('worldknit', 'CNS').
card_image_name('worldknit'/'CNS', 'worldknit').
card_uid('worldknit'/'CNS', 'CNS:Worldknit:worldknit').
card_rarity('worldknit'/'CNS', 'Rare').
card_artist('worldknit'/'CNS', 'Adam Paquette').
card_number('worldknit'/'CNS', '13').
card_multiverse_id('worldknit'/'CNS', '382411').

card_in_set('wrap in flames', 'CNS').
card_original_type('wrap in flames'/'CNS', 'Sorcery').
card_original_text('wrap in flames'/'CNS', 'Wrap in Flames deals 1 damage to each of up to three target creatures. Those creatures can\'t block this turn.').
card_image_name('wrap in flames'/'CNS', 'wrap in flames').
card_uid('wrap in flames'/'CNS', 'CNS:Wrap in Flames:wrap in flames').
card_rarity('wrap in flames'/'CNS', 'Common').
card_artist('wrap in flames'/'CNS', 'Véronique Meignaud').
card_number('wrap in flames'/'CNS', '158').
card_flavor_text('wrap in flames'/'CNS', '\"Our pyromancer\'s flames may not have killed them, but they bought us the time we needed.\"\n—The War Diaries').
card_multiverse_id('wrap in flames'/'CNS', '382412').

card_in_set('wrap in vigor', 'CNS').
card_original_type('wrap in vigor'/'CNS', 'Instant').
card_original_text('wrap in vigor'/'CNS', 'Regenerate each creature you control.').
card_image_name('wrap in vigor'/'CNS', 'wrap in vigor').
card_uid('wrap in vigor'/'CNS', 'CNS:Wrap in Vigor:wrap in vigor').
card_rarity('wrap in vigor'/'CNS', 'Common').
card_artist('wrap in vigor'/'CNS', 'John Donahue').
card_number('wrap in vigor'/'CNS', '183').
card_flavor_text('wrap in vigor'/'CNS', 'Some nature mages unknowingly took advantage of the temporal energies still swirling on Dominaria. What they mistook for healing magic was in fact the manipulation of time.').
card_multiverse_id('wrap in vigor'/'CNS', '382413').

card_in_set('zombie goliath', 'CNS').
card_original_type('zombie goliath'/'CNS', 'Creature — Zombie Giant').
card_original_text('zombie goliath'/'CNS', '').
card_image_name('zombie goliath'/'CNS', 'zombie goliath').
card_uid('zombie goliath'/'CNS', 'CNS:Zombie Goliath:zombie goliath').
card_rarity('zombie goliath'/'CNS', 'Common').
card_artist('zombie goliath'/'CNS', 'E. M. Gist').
card_number('zombie goliath'/'CNS', '135').
card_flavor_text('zombie goliath'/'CNS', '\"Removing the encumbrance of useless brain tissue grants several advantages: a slight increase in mobility, a response of revulsion and horror from the enemy, and, in the case of large specimens, room for passengers.\"\n—Zul Ashur, lich lord').
card_multiverse_id('zombie goliath'/'CNS', '382414').
