% Mercadian Masques

set('MMQ').
set_name('MMQ', 'Mercadian Masques').
set_release_date('MMQ', '1999-10-04').
set_border('MMQ', 'black').
set_type('MMQ', 'expansion').
set_block('MMQ', 'Masques').

card_in_set('aerial caravan', 'MMQ').
card_original_type('aerial caravan'/'MMQ', 'Creature — Soldier').
card_original_text('aerial caravan'/'MMQ', 'Flying\n{1}{U}{U}: Remove the top card of your library from the game. Until end of turn, you may play that card as though it were in your hand. (Reveal the card as you remove it from the game.)').
card_first_print('aerial caravan', 'MMQ').
card_image_name('aerial caravan'/'MMQ', 'aerial caravan').
card_uid('aerial caravan'/'MMQ', 'MMQ:Aerial Caravan:aerial caravan').
card_rarity('aerial caravan'/'MMQ', 'Rare').
card_artist('aerial caravan'/'MMQ', 'DiTerlizzi').
card_number('aerial caravan'/'MMQ', '58').
card_flavor_text('aerial caravan'/'MMQ', 'Successful delivery is not guaranteed.').
card_multiverse_id('aerial caravan'/'MMQ', '19667').

card_in_set('afterlife', 'MMQ').
card_original_type('afterlife'/'MMQ', 'Instant').
card_original_text('afterlife'/'MMQ', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 1/1 white Spirit creature token with flying into play.').
card_image_name('afterlife'/'MMQ', 'afterlife').
card_uid('afterlife'/'MMQ', 'MMQ:Afterlife:afterlife').
card_rarity('afterlife'/'MMQ', 'Uncommon').
card_artist('afterlife'/'MMQ', 'Brian Snõddy').
card_number('afterlife'/'MMQ', '1').
card_multiverse_id('afterlife'/'MMQ', '19549').

card_in_set('alabaster wall', 'MMQ').
card_original_type('alabaster wall'/'MMQ', 'Creature — Wall').
card_original_text('alabaster wall'/'MMQ', '(Walls can\'t attack.)\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('alabaster wall', 'MMQ').
card_image_name('alabaster wall'/'MMQ', 'alabaster wall').
card_uid('alabaster wall'/'MMQ', 'MMQ:Alabaster Wall:alabaster wall').
card_rarity('alabaster wall'/'MMQ', 'Common').
card_artist('alabaster wall'/'MMQ', 'Randy Gallegos').
card_number('alabaster wall'/'MMQ', '2').
card_flavor_text('alabaster wall'/'MMQ', 'Its mortar is mixed with waters straight from the Fountain of Cho.').
card_multiverse_id('alabaster wall'/'MMQ', '19541').

card_in_set('alley grifters', 'MMQ').
card_original_type('alley grifters'/'MMQ', 'Creature — Mercenary').
card_original_text('alley grifters'/'MMQ', 'Whenever Alley Grifters becomes blocked, defending player discards a card from his or her hand.').
card_first_print('alley grifters', 'MMQ').
card_image_name('alley grifters'/'MMQ', 'alley grifters').
card_uid('alley grifters'/'MMQ', 'MMQ:Alley Grifters:alley grifters').
card_rarity('alley grifters'/'MMQ', 'Common').
card_artist('alley grifters'/'MMQ', 'Paolo Parente').
card_number('alley grifters'/'MMQ', '115').
card_flavor_text('alley grifters'/'MMQ', 'One distracts the victim, the other extracts the valuables.').
card_multiverse_id('alley grifters'/'MMQ', '19584').

card_in_set('ancestral mask', 'MMQ').
card_original_type('ancestral mask'/'MMQ', 'Enchant Creature').
card_original_text('ancestral mask'/'MMQ', 'Enchanted creature gets +2/+2 for each other enchantment in play.').
card_first_print('ancestral mask', 'MMQ').
card_image_name('ancestral mask'/'MMQ', 'ancestral mask').
card_uid('ancestral mask'/'MMQ', 'MMQ:Ancestral Mask:ancestral mask').
card_rarity('ancestral mask'/'MMQ', 'Common').
card_artist('ancestral mask'/'MMQ', 'Massimilano Frezzato').
card_number('ancestral mask'/'MMQ', '229').
card_flavor_text('ancestral mask'/'MMQ', 'The masks, like the ancient trees they\'re grown from, lend strength to the dryads.').
card_multiverse_id('ancestral mask'/'MMQ', '19634').

card_in_set('armistice', 'MMQ').
card_original_type('armistice'/'MMQ', 'Enchantment').
card_original_text('armistice'/'MMQ', '{3}{W}{W} You draw a card and target opponent gains 3 life.').
card_first_print('armistice', 'MMQ').
card_image_name('armistice'/'MMQ', 'armistice').
card_uid('armistice'/'MMQ', 'MMQ:Armistice:armistice').
card_rarity('armistice'/'MMQ', 'Rare').
card_artist('armistice'/'MMQ', 'Dan Frazier').
card_number('armistice'/'MMQ', '3').
card_flavor_text('armistice'/'MMQ', 'We all must profit for peace to last.\n—Mercadian saying').
card_multiverse_id('armistice'/'MMQ', '19661').

card_in_set('arms dealer', 'MMQ').
card_original_type('arms dealer'/'MMQ', 'Creature — Goblin').
card_original_text('arms dealer'/'MMQ', '{1}{R}, Sacrifice a Goblin: Arms Dealer deals 4 damage to target creature.').
card_first_print('arms dealer', 'MMQ').
card_image_name('arms dealer'/'MMQ', 'arms dealer').
card_uid('arms dealer'/'MMQ', 'MMQ:Arms Dealer:arms dealer').
card_rarity('arms dealer'/'MMQ', 'Uncommon').
card_artist('arms dealer'/'MMQ', 'Luca Zontini').
card_number('arms dealer'/'MMQ', '172').
card_flavor_text('arms dealer'/'MMQ', 'He has the weapons, he has the connections, and he knows the hangar\'s dark secret.').
card_multiverse_id('arms dealer'/'MMQ', '19861').

card_in_set('arrest', 'MMQ').
card_original_type('arrest'/'MMQ', 'Enchant Creature').
card_original_text('arrest'/'MMQ', 'Enchanted creature can\'t attack or block, and its activated abilities can\'t be played.').
card_image_name('arrest'/'MMQ', 'arrest').
card_uid('arrest'/'MMQ', 'MMQ:Arrest:arrest').
card_rarity('arrest'/'MMQ', 'Uncommon').
card_artist('arrest'/'MMQ', 'Dan Frazier').
card_number('arrest'/'MMQ', '4').
card_flavor_text('arrest'/'MMQ', 'Orim had no memory of the previous night—the first thing she could remember was waking up with the dead guard\'s blood on her hands.').
card_multiverse_id('arrest'/'MMQ', '19789').

card_in_set('assembly hall', 'MMQ').
card_original_type('assembly hall'/'MMQ', 'Artifact').
card_original_text('assembly hall'/'MMQ', '{4}, {T}: Reveal a creature card in your hand, search your library for a copy of that card, and put the card into your hand. Then shuffle your library.').
card_first_print('assembly hall', 'MMQ').
card_image_name('assembly hall'/'MMQ', 'assembly hall').
card_uid('assembly hall'/'MMQ', 'MMQ:Assembly Hall:assembly hall').
card_rarity('assembly hall'/'MMQ', 'Rare').
card_artist('assembly hall'/'MMQ', 'Val Mayerik').
card_number('assembly hall'/'MMQ', '286').
card_multiverse_id('assembly hall'/'MMQ', '19761').

card_in_set('ballista squad', 'MMQ').
card_original_type('ballista squad'/'MMQ', 'Creature — Rebel').
card_original_text('ballista squad'/'MMQ', '{X}{W}, {T}: Ballista Squad deals X damage to target attacking or blocking creature.').
card_first_print('ballista squad', 'MMQ').
card_image_name('ballista squad'/'MMQ', 'ballista squad').
card_uid('ballista squad'/'MMQ', 'MMQ:Ballista Squad:ballista squad').
card_rarity('ballista squad'/'MMQ', 'Uncommon').
card_artist('ballista squad'/'MMQ', 'Matthew D. Wilson').
card_number('ballista squad'/'MMQ', '5').
card_flavor_text('ballista squad'/'MMQ', 'The perfect antidote for a tightly packed formation.').
card_multiverse_id('ballista squad'/'MMQ', '19781').

card_in_set('balloon peddler', 'MMQ').
card_original_type('balloon peddler'/'MMQ', 'Creature — Spellshaper').
card_original_text('balloon peddler'/'MMQ', '{U}, {T}, Discard a card from your hand: Target creature gains flying until end of turn.').
card_first_print('balloon peddler', 'MMQ').
card_image_name('balloon peddler'/'MMQ', 'balloon peddler').
card_uid('balloon peddler'/'MMQ', 'MMQ:Balloon Peddler:balloon peddler').
card_rarity('balloon peddler'/'MMQ', 'Common').
card_artist('balloon peddler'/'MMQ', 'Paolo Parente').
card_number('balloon peddler'/'MMQ', '59').
card_flavor_text('balloon peddler'/'MMQ', 'The market festival turned out to be the high point of Jaffy\'s visit.').
card_multiverse_id('balloon peddler'/'MMQ', '19558').

card_in_set('barbed wire', 'MMQ').
card_original_type('barbed wire'/'MMQ', 'Artifact').
card_original_text('barbed wire'/'MMQ', 'At the beginning of each player\'s upkeep, Barbed Wire deals 1 damage to that player.\n{2}: Prevent the next 1 damage that would be dealt by Barbed Wire this turn.').
card_first_print('barbed wire', 'MMQ').
card_image_name('barbed wire'/'MMQ', 'barbed wire').
card_uid('barbed wire'/'MMQ', 'MMQ:Barbed Wire:barbed wire').
card_rarity('barbed wire'/'MMQ', 'Uncommon').
card_artist('barbed wire'/'MMQ', 'Ron Spencer').
card_number('barbed wire'/'MMQ', '287').
card_flavor_text('barbed wire'/'MMQ', 'A steel snake with ten thousand teeth.').
card_multiverse_id('barbed wire'/'MMQ', '19883').

card_in_set('bargaining table', 'MMQ').
card_original_type('bargaining table'/'MMQ', 'Artifact').
card_original_text('bargaining table'/'MMQ', '{X}, {T}: Draw a card. X is the number of cards in an opponent\'s hand.').
card_first_print('bargaining table', 'MMQ').
card_image_name('bargaining table'/'MMQ', 'bargaining table').
card_uid('bargaining table'/'MMQ', 'MMQ:Bargaining Table:bargaining table').
card_rarity('bargaining table'/'MMQ', 'Rare').
card_artist('bargaining table'/'MMQ', 'Scott M. Fischer').
card_number('bargaining table'/'MMQ', '288').
card_flavor_text('bargaining table'/'MMQ', 'Although it looks like a game, business around the table is deadly serious.').
card_multiverse_id('bargaining table'/'MMQ', '19749').

card_in_set('battle rampart', 'MMQ').
card_original_type('battle rampart'/'MMQ', 'Creature — Wall').
card_original_text('battle rampart'/'MMQ', '(Walls can\'t attack.)\n{T}: Target creature gains haste until end of turn. (That creature may attack and {T} the turn it comes under your control.)').
card_first_print('battle rampart', 'MMQ').
card_image_name('battle rampart'/'MMQ', 'battle rampart').
card_uid('battle rampart'/'MMQ', 'MMQ:Battle Rampart:battle rampart').
card_rarity('battle rampart'/'MMQ', 'Common').
card_artist('battle rampart'/'MMQ', 'Ron Spencer').
card_number('battle rampart'/'MMQ', '173').
card_multiverse_id('battle rampart'/'MMQ', '19607').

card_in_set('battle squadron', 'MMQ').
card_original_type('battle squadron'/'MMQ', 'Creature — Ship').
card_original_text('battle squadron'/'MMQ', 'Flying\nBattle Squadron\'s power and toughness are each equal to the number of creatures you control.').
card_first_print('battle squadron', 'MMQ').
card_image_name('battle squadron'/'MMQ', 'battle squadron').
card_uid('battle squadron'/'MMQ', 'MMQ:Battle Squadron:battle squadron').
card_rarity('battle squadron'/'MMQ', 'Rare').
card_artist('battle squadron'/'MMQ', 'Mark Tedin').
card_number('battle squadron'/'MMQ', '174').
card_flavor_text('battle squadron'/'MMQ', 'The goblins made an unruly pile with military precision.').
card_multiverse_id('battle squadron'/'MMQ', '19712').

card_in_set('bifurcate', 'MMQ').
card_original_type('bifurcate'/'MMQ', 'Sorcery').
card_original_text('bifurcate'/'MMQ', 'Search your library for a copy of target creature card in play and put that card into play. Then shuffle your library.').
card_first_print('bifurcate', 'MMQ').
card_image_name('bifurcate'/'MMQ', 'bifurcate').
card_uid('bifurcate'/'MMQ', 'MMQ:Bifurcate:bifurcate').
card_rarity('bifurcate'/'MMQ', 'Rare').
card_artist('bifurcate'/'MMQ', 'John Matson').
card_number('bifurcate'/'MMQ', '230').
card_multiverse_id('bifurcate'/'MMQ', '19741').

card_in_set('black market', 'MMQ').
card_original_type('black market'/'MMQ', 'Enchantment').
card_original_text('black market'/'MMQ', 'Whenever a creature is put into a graveyard from play, put a charge counter on Black Market.\nAt the beginning of your precombat main phase, add {B} to your mana pool for each charge counter on Black Market.').
card_first_print('black market', 'MMQ').
card_image_name('black market'/'MMQ', 'black market').
card_uid('black market'/'MMQ', 'MMQ:Black Market:black market').
card_rarity('black market'/'MMQ', 'Rare').
card_artist('black market'/'MMQ', 'Jeff Easley').
card_number('black market'/'MMQ', '116').
card_multiverse_id('black market'/'MMQ', '19831').

card_in_set('blaster mage', 'MMQ').
card_original_type('blaster mage'/'MMQ', 'Creature — Spellshaper').
card_original_text('blaster mage'/'MMQ', '{R}, {T}, Discard a card from your hand: Destroy target Wall.').
card_first_print('blaster mage', 'MMQ').
card_image_name('blaster mage'/'MMQ', 'blaster mage').
card_uid('blaster mage'/'MMQ', 'MMQ:Blaster Mage:blaster mage').
card_rarity('blaster mage'/'MMQ', 'Common').
card_artist('blaster mage'/'MMQ', 'George Pratt').
card_number('blaster mage'/'MMQ', '175').
card_flavor_text('blaster mage'/'MMQ', '\"Don\'t get up. I\'ll show myself out.\"').
card_multiverse_id('blaster mage'/'MMQ', '21332').

card_in_set('blockade runner', 'MMQ').
card_original_type('blockade runner'/'MMQ', 'Creature — Merfolk').
card_original_text('blockade runner'/'MMQ', '{U} Blockade Runner is unblockable this turn.').
card_first_print('blockade runner', 'MMQ').
card_image_name('blockade runner'/'MMQ', 'blockade runner').
card_uid('blockade runner'/'MMQ', 'MMQ:Blockade Runner:blockade runner').
card_rarity('blockade runner'/'MMQ', 'Common').
card_artist('blockade runner'/'MMQ', 'Carl Critchlow').
card_number('blockade runner'/'MMQ', '60').
card_flavor_text('blockade runner'/'MMQ', '\"I need to get back to Mercadia City,\" said Sisay. \"We can get you there,\" the vizier answered. \"Easily.\"').
card_multiverse_id('blockade runner'/'MMQ', '19567').

card_in_set('blood hound', 'MMQ').
card_original_type('blood hound'/'MMQ', 'Creature — Hound').
card_original_text('blood hound'/'MMQ', 'Whenever you\'re dealt damage, you may put that many +1/+1 counters on Blood Hound.\nAt the end of your turn, remove all +1/+1 counters from Blood Hound.').
card_first_print('blood hound', 'MMQ').
card_image_name('blood hound'/'MMQ', 'blood hound').
card_uid('blood hound'/'MMQ', 'MMQ:Blood Hound:blood hound').
card_rarity('blood hound'/'MMQ', 'Rare').
card_artist('blood hound'/'MMQ', 'Bradley Williams').
card_number('blood hound'/'MMQ', '176').
card_multiverse_id('blood hound'/'MMQ', '19714').

card_in_set('blood oath', 'MMQ').
card_original_type('blood oath'/'MMQ', 'Instant').
card_original_text('blood oath'/'MMQ', 'Choose a card type. Target opponent reveals his or her hand. Blood Oath deals 3 damage to that player for each card of the chosen type revealed this way. (The card types are artifact, creature, enchantment, instant, land, and sorcery.)').
card_first_print('blood oath', 'MMQ').
card_image_name('blood oath'/'MMQ', 'blood oath').
card_uid('blood oath'/'MMQ', 'MMQ:Blood Oath:blood oath').
card_rarity('blood oath'/'MMQ', 'Rare').
card_artist('blood oath'/'MMQ', 'Mike Ploog').
card_number('blood oath'/'MMQ', '177').
card_multiverse_id('blood oath'/'MMQ', '19726').

card_in_set('boa constrictor', 'MMQ').
card_original_type('boa constrictor'/'MMQ', 'Creature — Snake').
card_original_text('boa constrictor'/'MMQ', '{T}: Boa Constrictor gets +3/+3 until end of turn.').
card_first_print('boa constrictor', 'MMQ').
card_image_name('boa constrictor'/'MMQ', 'boa constrictor').
card_uid('boa constrictor'/'MMQ', 'MMQ:Boa Constrictor:boa constrictor').
card_rarity('boa constrictor'/'MMQ', 'Uncommon').
card_artist('boa constrictor'/'MMQ', 'Carl Critchlow').
card_number('boa constrictor'/'MMQ', '231').
card_flavor_text('boa constrictor'/'MMQ', 'Its eyes are bigger than its stomach, but its mouth is larger still.').
card_multiverse_id('boa constrictor'/'MMQ', '19859').

card_in_set('bog smugglers', 'MMQ').
card_original_type('bog smugglers'/'MMQ', 'Creature — Mercenary').
card_original_text('bog smugglers'/'MMQ', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)').
card_first_print('bog smugglers', 'MMQ').
card_image_name('bog smugglers'/'MMQ', 'bog smugglers').
card_uid('bog smugglers'/'MMQ', 'MMQ:Bog Smugglers:bog smugglers').
card_rarity('bog smugglers'/'MMQ', 'Common').
card_artist('bog smugglers'/'MMQ', 'Mike Ploog').
card_number('bog smugglers'/'MMQ', '117').
card_flavor_text('bog smugglers'/'MMQ', 'They slide over the bog like oil across glass.').
card_multiverse_id('bog smugglers'/'MMQ', '19598').

card_in_set('bog witch', 'MMQ').
card_original_type('bog witch'/'MMQ', 'Creature — Spellshaper').
card_original_text('bog witch'/'MMQ', '{B}, {T}, Discard a card from your hand: Add {B}{B}{B} to your mana pool.').
card_first_print('bog witch', 'MMQ').
card_image_name('bog witch'/'MMQ', 'bog witch').
card_uid('bog witch'/'MMQ', 'MMQ:Bog Witch:bog witch').
card_rarity('bog witch'/'MMQ', 'Common').
card_artist('bog witch'/'MMQ', 'Gao Yan').
card_number('bog witch'/'MMQ', '118').
card_flavor_text('bog witch'/'MMQ', 'The world is the body. The mana is the blood. The witch is the surgeon.').
card_multiverse_id('bog witch'/'MMQ', '19578').

card_in_set('brainstorm', 'MMQ').
card_original_type('brainstorm'/'MMQ', 'Instant').
card_original_text('brainstorm'/'MMQ', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'MMQ', 'brainstorm').
card_uid('brainstorm'/'MMQ', 'MMQ:Brainstorm:brainstorm').
card_rarity('brainstorm'/'MMQ', 'Common').
card_artist('brainstorm'/'MMQ', 'DiTerlizzi').
card_number('brainstorm'/'MMQ', '61').
card_flavor_text('brainstorm'/'MMQ', '\"I see more than others do because I know where to look.\"\n—Saprazzan vizier').
card_multiverse_id('brainstorm'/'MMQ', '19571').

card_in_set('brawl', 'MMQ').
card_original_type('brawl'/'MMQ', 'Instant').
card_original_text('brawl'/'MMQ', 'Until end of turn, all creatures gain \"{T}: This creature deals damage equal to its power to target creature.\"').
card_first_print('brawl', 'MMQ').
card_image_name('brawl'/'MMQ', 'brawl').
card_uid('brawl'/'MMQ', 'MMQ:Brawl:brawl').
card_rarity('brawl'/'MMQ', 'Rare').
card_artist('brawl'/'MMQ', 'Edward P. Beard, Jr.').
card_number('brawl'/'MMQ', '178').
card_flavor_text('brawl'/'MMQ', 'Anything can happen when patience is scarce and wine is abundant.').
card_multiverse_id('brawl'/'MMQ', '19723').

card_in_set('briar patch', 'MMQ').
card_original_type('briar patch'/'MMQ', 'Enchantment').
card_original_text('briar patch'/'MMQ', 'Whenever a creature attacks you, it gets -1/-0 until end of turn.').
card_first_print('briar patch', 'MMQ').
card_image_name('briar patch'/'MMQ', 'briar patch').
card_uid('briar patch'/'MMQ', 'MMQ:Briar Patch:briar patch').
card_rarity('briar patch'/'MMQ', 'Uncommon').
card_artist('briar patch'/'MMQ', 'Rebecca Guay').
card_number('briar patch'/'MMQ', '232').
card_flavor_text('briar patch'/'MMQ', 'As easily as the dryads could close a fist, so could the branches and underbrush entangle.').
card_multiverse_id('briar patch'/'MMQ', '19745').

card_in_set('bribery', 'MMQ').
card_original_type('bribery'/'MMQ', 'Sorcery').
card_original_text('bribery'/'MMQ', 'Search target opponent\'s library for a creature card and put that card into play under your control. That player then shuffles his or her library.').
card_image_name('bribery'/'MMQ', 'bribery').
card_uid('bribery'/'MMQ', 'MMQ:Bribery:bribery').
card_rarity('bribery'/'MMQ', 'Rare').
card_artist('bribery'/'MMQ', 'Andrew Robinson').
card_number('bribery'/'MMQ', '62').
card_flavor_text('bribery'/'MMQ', 'In Mercadia City, loyalties extend only as far as your purse can stretch.').
card_multiverse_id('bribery'/'MMQ', '21300').

card_in_set('buoyancy', 'MMQ').
card_original_type('buoyancy'/'MMQ', 'Enchant Creature').
card_original_text('buoyancy'/'MMQ', 'You may play Buoyancy any time you could play an instant.\nEnchanted creature has flying.').
card_first_print('buoyancy', 'MMQ').
card_image_name('buoyancy'/'MMQ', 'buoyancy').
card_uid('buoyancy'/'MMQ', 'MMQ:Buoyancy:buoyancy').
card_rarity('buoyancy'/'MMQ', 'Common').
card_artist('buoyancy'/'MMQ', 'Jeff Miracola').
card_number('buoyancy'/'MMQ', '63').
card_flavor_text('buoyancy'/'MMQ', 'Saprazzan merfolk become legged on land, but some find it quicker to bring the water with them.').
card_multiverse_id('buoyancy'/'MMQ', '19574').

card_in_set('cackling witch', 'MMQ').
card_original_type('cackling witch'/'MMQ', 'Creature — Spellshaper').
card_original_text('cackling witch'/'MMQ', '{X}{B}, {T}, Discard a card from your hand: Target creature gets +X/+0 until end of turn.').
card_first_print('cackling witch', 'MMQ').
card_image_name('cackling witch'/'MMQ', 'cackling witch').
card_uid('cackling witch'/'MMQ', 'MMQ:Cackling Witch:cackling witch').
card_rarity('cackling witch'/'MMQ', 'Uncommon').
card_artist('cackling witch'/'MMQ', 'Brian Despain').
card_number('cackling witch'/'MMQ', '119').
card_flavor_text('cackling witch'/'MMQ', 'The touch of his madness can drive anyone into a killing frenzy.').
card_multiverse_id('cackling witch'/'MMQ', '19815').

card_in_set('caller of the hunt', 'MMQ').
card_original_type('caller of the hunt'/'MMQ', 'Creature — Lord').
card_original_text('caller of the hunt'/'MMQ', 'As you play Caller of the Hunt, choose a creature type.\nCaller of the Hunt\'s power and toughness are each equal to the number of creatures in play of the chosen type.').
card_first_print('caller of the hunt', 'MMQ').
card_image_name('caller of the hunt'/'MMQ', 'caller of the hunt').
card_uid('caller of the hunt'/'MMQ', 'MMQ:Caller of the Hunt:caller of the hunt').
card_rarity('caller of the hunt'/'MMQ', 'Rare').
card_artist('caller of the hunt'/'MMQ', 'Clyde Caldwell').
card_number('caller of the hunt'/'MMQ', '233').
card_multiverse_id('caller of the hunt'/'MMQ', '19730').

card_in_set('cateran brute', 'MMQ').
card_original_type('cateran brute'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran brute'/'MMQ', '{2}, {T}: Search your library for a Mercenary card with converted mana cost 2 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran brute', 'MMQ').
card_image_name('cateran brute'/'MMQ', 'cateran brute').
card_uid('cateran brute'/'MMQ', 'MMQ:Cateran Brute:cateran brute').
card_rarity('cateran brute'/'MMQ', 'Common').
card_artist('cateran brute'/'MMQ', 'Edward P. Beard, Jr.').
card_number('cateran brute'/'MMQ', '120').
card_flavor_text('cateran brute'/'MMQ', 'The Cateran guild prefers to call them \"public relations.\"').
card_multiverse_id('cateran brute'/'MMQ', '19582').

card_in_set('cateran enforcer', 'MMQ').
card_original_type('cateran enforcer'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran enforcer'/'MMQ', 'Cateran Enforcer can\'t be blocked except by artifact creatures and black creatures.\n{4}, {T}: Search your library for a Mercenary card with converted mana cost 4 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran enforcer', 'MMQ').
card_image_name('cateran enforcer'/'MMQ', 'cateran enforcer').
card_uid('cateran enforcer'/'MMQ', 'MMQ:Cateran Enforcer:cateran enforcer').
card_rarity('cateran enforcer'/'MMQ', 'Uncommon').
card_artist('cateran enforcer'/'MMQ', 'Mike Ploog').
card_number('cateran enforcer'/'MMQ', '121').
card_multiverse_id('cateran enforcer'/'MMQ', '19690').

card_in_set('cateran kidnappers', 'MMQ').
card_original_type('cateran kidnappers'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran kidnappers'/'MMQ', '{3}, {T}: Search your library for a Mercenary card with converted mana cost 3 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran kidnappers', 'MMQ').
card_image_name('cateran kidnappers'/'MMQ', 'cateran kidnappers').
card_uid('cateran kidnappers'/'MMQ', 'MMQ:Cateran Kidnappers:cateran kidnappers').
card_rarity('cateran kidnappers'/'MMQ', 'Uncommon').
card_artist('cateran kidnappers'/'MMQ', 'Carl Critchlow').
card_number('cateran kidnappers'/'MMQ', '122').
card_flavor_text('cateran kidnappers'/'MMQ', 'Recruits aren\'t always volunteers.').
card_multiverse_id('cateran kidnappers'/'MMQ', '19819').

card_in_set('cateran overlord', 'MMQ').
card_original_type('cateran overlord'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran overlord'/'MMQ', 'Sacrifice a creature: Regenerate Cateran Overlord.\n{6}, {T}: Search your library for a Mercenary card with converted mana cost 6 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran overlord', 'MMQ').
card_image_name('cateran overlord'/'MMQ', 'cateran overlord').
card_uid('cateran overlord'/'MMQ', 'MMQ:Cateran Overlord:cateran overlord').
card_rarity('cateran overlord'/'MMQ', 'Rare').
card_artist('cateran overlord'/'MMQ', 'Michael Sutfin').
card_number('cateran overlord'/'MMQ', '123').
card_multiverse_id('cateran overlord'/'MMQ', '19698').

card_in_set('cateran persuader', 'MMQ').
card_original_type('cateran persuader'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran persuader'/'MMQ', '{1}, {T}: Search your library for a Mercenary card with converted mana cost 1 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran persuader', 'MMQ').
card_image_name('cateran persuader'/'MMQ', 'cateran persuader').
card_uid('cateran persuader'/'MMQ', 'MMQ:Cateran Persuader:cateran persuader').
card_rarity('cateran persuader'/'MMQ', 'Common').
card_artist('cateran persuader'/'MMQ', 'Carl Critchlow').
card_number('cateran persuader'/'MMQ', '124').
card_multiverse_id('cateran persuader'/'MMQ', '19580').

card_in_set('cateran slaver', 'MMQ').
card_original_type('cateran slaver'/'MMQ', 'Creature — Mercenary').
card_original_text('cateran slaver'/'MMQ', 'Swampwalk (This creature is unblockable as long as defending player controls a swamp.)\n{5}, {T}: Search your library for a Mercenary card with converted mana cost 5 or less and put that card into play. Then shuffle your library.').
card_first_print('cateran slaver', 'MMQ').
card_image_name('cateran slaver'/'MMQ', 'cateran slaver').
card_uid('cateran slaver'/'MMQ', 'MMQ:Cateran Slaver:cateran slaver').
card_rarity('cateran slaver'/'MMQ', 'Rare').
card_artist('cateran slaver'/'MMQ', 'Carl Critchlow').
card_number('cateran slaver'/'MMQ', '125').
card_multiverse_id('cateran slaver'/'MMQ', '19691').

card_in_set('cateran summons', 'MMQ').
card_original_type('cateran summons'/'MMQ', 'Sorcery').
card_original_text('cateran summons'/'MMQ', 'Search your library for a Mercenary card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('cateran summons', 'MMQ').
card_image_name('cateran summons'/'MMQ', 'cateran summons').
card_uid('cateran summons'/'MMQ', 'MMQ:Cateran Summons:cateran summons').
card_rarity('cateran summons'/'MMQ', 'Uncommon').
card_artist('cateran summons'/'MMQ', 'Alan Pollack').
card_number('cateran summons'/'MMQ', '126').
card_flavor_text('cateran summons'/'MMQ', 'Even when recruitment is slow, the guild can always cook something up.').
card_multiverse_id('cateran summons'/'MMQ', '19832').

card_in_set('caustic wasps', 'MMQ').
card_original_type('caustic wasps'/'MMQ', 'Creature — Insect').
card_original_text('caustic wasps'/'MMQ', 'Flying\nWhenever Caustic Wasps deals combat damage to a player, you may destroy target artifact that player controls.').
card_first_print('caustic wasps', 'MMQ').
card_image_name('caustic wasps'/'MMQ', 'caustic wasps').
card_uid('caustic wasps'/'MMQ', 'MMQ:Caustic Wasps:caustic wasps').
card_rarity('caustic wasps'/'MMQ', 'Uncommon').
card_artist('caustic wasps'/'MMQ', 'Glen Angus').
card_number('caustic wasps'/'MMQ', '234').
card_flavor_text('caustic wasps'/'MMQ', 'Even the wasp knows how to pay the magistrate his due respect.').
card_multiverse_id('caustic wasps'/'MMQ', '19865').

card_in_set('cave sense', 'MMQ').
card_original_type('cave sense'/'MMQ', 'Enchant Creature').
card_original_text('cave sense'/'MMQ', 'Enchanted creature gets +1/+1 and has mountainwalk. (It\'s unblockable as long as defending player controls a mountain.)').
card_first_print('cave sense', 'MMQ').
card_image_name('cave sense'/'MMQ', 'cave sense').
card_uid('cave sense'/'MMQ', 'MMQ:Cave Sense:cave sense').
card_rarity('cave sense'/'MMQ', 'Common').
card_artist('cave sense'/'MMQ', 'Mark Romanoski').
card_number('cave sense'/'MMQ', '179').
card_multiverse_id('cave sense'/'MMQ', '19620').

card_in_set('cave-in', 'MMQ').
card_original_type('cave-in'/'MMQ', 'Sorcery').
card_original_text('cave-in'/'MMQ', 'You may remove a red card in your hand from the game instead of paying Cave-In\'s mana cost.\nCave-In deals 2 damage to each creature and each player.').
card_first_print('cave-in', 'MMQ').
card_image_name('cave-in'/'MMQ', 'cave-in').
card_uid('cave-in'/'MMQ', 'MMQ:Cave-In:cave-in').
card_rarity('cave-in'/'MMQ', 'Rare').
card_artist('cave-in'/'MMQ', 'Mark Tedin').
card_number('cave-in'/'MMQ', '180').
card_flavor_text('cave-in'/'MMQ', 'One fleet was gone; others were poised.').
card_multiverse_id('cave-in'/'MMQ', '19725').

card_in_set('cavern crawler', 'MMQ').
card_original_type('cavern crawler'/'MMQ', 'Creature — Insect').
card_original_text('cavern crawler'/'MMQ', 'Mountainwalk (This creature is unblockable as long as defending player controls a mountain.)\n{R} Cavern Crawler gets +1/-1 until end of turn.').
card_first_print('cavern crawler', 'MMQ').
card_image_name('cavern crawler'/'MMQ', 'cavern crawler').
card_uid('cavern crawler'/'MMQ', 'MMQ:Cavern Crawler:cavern crawler').
card_rarity('cavern crawler'/'MMQ', 'Common').
card_artist('cavern crawler'/'MMQ', 'Pete Venters').
card_number('cavern crawler'/'MMQ', '181').
card_multiverse_id('cavern crawler'/'MMQ', '19610').

card_in_set('ceremonial guard', 'MMQ').
card_original_type('ceremonial guard'/'MMQ', 'Creature — Soldier').
card_original_text('ceremonial guard'/'MMQ', 'When Ceremonial Guard attacks or blocks, destroy it at end of combat.').
card_first_print('ceremonial guard', 'MMQ').
card_image_name('ceremonial guard'/'MMQ', 'ceremonial guard').
card_uid('ceremonial guard'/'MMQ', 'MMQ:Ceremonial Guard:ceremonial guard').
card_rarity('ceremonial guard'/'MMQ', 'Common').
card_artist('ceremonial guard'/'MMQ', 'Daren Bader').
card_number('ceremonial guard'/'MMQ', '182').
card_flavor_text('ceremonial guard'/'MMQ', '\"These guards are the most disciplined military unit in the city. They couldn\'t fight their way out of a broom closet, but they\'ve got discipline.\"\n—Gerrard, to Sisay').
card_multiverse_id('ceremonial guard'/'MMQ', '19603').

card_in_set('chambered nautilus', 'MMQ').
card_original_type('chambered nautilus'/'MMQ', 'Creature — Beast').
card_original_text('chambered nautilus'/'MMQ', 'Whenever Chambered Nautilus becomes blocked, you may draw a card.').
card_first_print('chambered nautilus', 'MMQ').
card_image_name('chambered nautilus'/'MMQ', 'chambered nautilus').
card_uid('chambered nautilus'/'MMQ', 'MMQ:Chambered Nautilus:chambered nautilus').
card_rarity('chambered nautilus'/'MMQ', 'Uncommon').
card_artist('chambered nautilus'/'MMQ', 'John Matson').
card_number('chambered nautilus'/'MMQ', '64').
card_flavor_text('chambered nautilus'/'MMQ', 'What\'s merely a home for the nautilus can become exquisite jewelry in the hands of Saprazzan artisans.').
card_multiverse_id('chambered nautilus'/'MMQ', '19798').

card_in_set('chameleon spirit', 'MMQ').
card_original_type('chameleon spirit'/'MMQ', 'Creature — Illusion').
card_original_text('chameleon spirit'/'MMQ', 'As Chameleon Spirit comes into play, choose a color.\nChameleon Spirit\'s power and toughness are each equal to the number of permanents of the chosen color your opponents control.').
card_first_print('chameleon spirit', 'MMQ').
card_image_name('chameleon spirit'/'MMQ', 'chameleon spirit').
card_uid('chameleon spirit'/'MMQ', 'MMQ:Chameleon Spirit:chameleon spirit').
card_rarity('chameleon spirit'/'MMQ', 'Uncommon').
card_artist('chameleon spirit'/'MMQ', 'Bradley Williams').
card_number('chameleon spirit'/'MMQ', '65').
card_multiverse_id('chameleon spirit'/'MMQ', '19800').

card_in_set('charisma', 'MMQ').
card_original_type('charisma'/'MMQ', 'Enchant Creature').
card_original_text('charisma'/'MMQ', 'Whenever enchanted creature deals damage to a creature, you control that creature as long as Charisma remains in play.').
card_first_print('charisma', 'MMQ').
card_image_name('charisma'/'MMQ', 'charisma').
card_uid('charisma'/'MMQ', 'MMQ:Charisma:charisma').
card_rarity('charisma'/'MMQ', 'Rare').
card_artist('charisma'/'MMQ', 'Terese Nielsen').
card_number('charisma'/'MMQ', '66').
card_flavor_text('charisma'/'MMQ', 'It was Gerrard\'s nature to lead and the Mercadians\' to follow.').
card_multiverse_id('charisma'/'MMQ', '20456').

card_in_set('charm peddler', 'MMQ').
card_original_type('charm peddler'/'MMQ', 'Creature — Spellshaper').
card_original_text('charm peddler'/'MMQ', '{W}, {T}, Discard a card from your hand: The next time a source of your choice would deal damage to target creature this turn, prevent that damage.').
card_first_print('charm peddler', 'MMQ').
card_image_name('charm peddler'/'MMQ', 'charm peddler').
card_uid('charm peddler'/'MMQ', 'MMQ:Charm Peddler:charm peddler').
card_rarity('charm peddler'/'MMQ', 'Common').
card_artist('charm peddler'/'MMQ', 'John Matson').
card_number('charm peddler'/'MMQ', '6').
card_multiverse_id('charm peddler'/'MMQ', '19535').

card_in_set('charmed griffin', 'MMQ').
card_original_type('charmed griffin'/'MMQ', 'Creature — Griffin').
card_original_text('charmed griffin'/'MMQ', 'Flying\nWhen Charmed Griffin comes into play, each other player may put an artifact or enchantment card into play from his or her hand.').
card_first_print('charmed griffin', 'MMQ').
card_image_name('charmed griffin'/'MMQ', 'charmed griffin').
card_uid('charmed griffin'/'MMQ', 'MMQ:Charmed Griffin:charmed griffin').
card_rarity('charmed griffin'/'MMQ', 'Uncommon').
card_artist('charmed griffin'/'MMQ', 'Ray Lago').
card_number('charmed griffin'/'MMQ', '7').
card_flavor_text('charmed griffin'/'MMQ', 'Are you sure you can afford such a luxury?').
card_multiverse_id('charmed griffin'/'MMQ', '19777').

card_in_set('cho-arrim alchemist', 'MMQ').
card_original_type('cho-arrim alchemist'/'MMQ', 'Creature — Spellshaper').
card_original_text('cho-arrim alchemist'/'MMQ', '{1}{W}{W}, {T}, Discard a card from your hand: The next time a source of your choice would deal damage to you this turn, prevent that damage and gain that much life.').
card_first_print('cho-arrim alchemist', 'MMQ').
card_image_name('cho-arrim alchemist'/'MMQ', 'cho-arrim alchemist').
card_uid('cho-arrim alchemist'/'MMQ', 'MMQ:Cho-Arrim Alchemist:cho-arrim alchemist').
card_rarity('cho-arrim alchemist'/'MMQ', 'Rare').
card_artist('cho-arrim alchemist'/'MMQ', 'Scott M. Fischer').
card_number('cho-arrim alchemist'/'MMQ', '8').
card_multiverse_id('cho-arrim alchemist'/'MMQ', '19647').

card_in_set('cho-arrim bruiser', 'MMQ').
card_original_type('cho-arrim bruiser'/'MMQ', 'Creature — Rebel').
card_original_text('cho-arrim bruiser'/'MMQ', 'Whenever Cho-Arrim Bruiser attacks, you may tap up to two target creatures.').
card_first_print('cho-arrim bruiser', 'MMQ').
card_image_name('cho-arrim bruiser'/'MMQ', 'cho-arrim bruiser').
card_uid('cho-arrim bruiser'/'MMQ', 'MMQ:Cho-Arrim Bruiser:cho-arrim bruiser').
card_rarity('cho-arrim bruiser'/'MMQ', 'Rare').
card_artist('cho-arrim bruiser'/'MMQ', 'Paolo Parente').
card_number('cho-arrim bruiser'/'MMQ', '9').
card_flavor_text('cho-arrim bruiser'/'MMQ', 'He doesn\'t know why the Cho-Arrim fight the Mercadians, but he\'s happy to bash heads for them anyway.').
card_multiverse_id('cho-arrim bruiser'/'MMQ', '19650').

card_in_set('cho-arrim legate', 'MMQ').
card_original_type('cho-arrim legate'/'MMQ', 'Creature — Soldier').
card_original_text('cho-arrim legate'/'MMQ', 'Protection from black\nIf an opponent controls a swamp and you control a plains, you may play Cho-Arrim Legate without paying its mana cost.').
card_first_print('cho-arrim legate', 'MMQ').
card_image_name('cho-arrim legate'/'MMQ', 'cho-arrim legate').
card_uid('cho-arrim legate'/'MMQ', 'MMQ:Cho-Arrim Legate:cho-arrim legate').
card_rarity('cho-arrim legate'/'MMQ', 'Uncommon').
card_artist('cho-arrim legate'/'MMQ', 'rk post').
card_number('cho-arrim legate'/'MMQ', '10').
card_multiverse_id('cho-arrim legate'/'MMQ', '19779').

card_in_set('cho-manno\'s blessing', 'MMQ').
card_original_type('cho-manno\'s blessing'/'MMQ', 'Enchant Creature').
card_original_text('cho-manno\'s blessing'/'MMQ', 'You may play Cho-Manno\'s Blessing any time you could play an instant.\nAs Cho-Manno\'s Blessing comes into play, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Cho-Manno\'s Blessing.').
card_first_print('cho-manno\'s blessing', 'MMQ').
card_image_name('cho-manno\'s blessing'/'MMQ', 'cho-manno\'s blessing').
card_uid('cho-manno\'s blessing'/'MMQ', 'MMQ:Cho-Manno\'s Blessing:cho-manno\'s blessing').
card_rarity('cho-manno\'s blessing'/'MMQ', 'Common').
card_artist('cho-manno\'s blessing'/'MMQ', 'John Matson').
card_number('cho-manno\'s blessing'/'MMQ', '12').
card_multiverse_id('cho-manno\'s blessing'/'MMQ', '19551').

card_in_set('cho-manno, revolutionary', 'MMQ').
card_original_type('cho-manno, revolutionary'/'MMQ', 'Creature — Rebel Legend').
card_original_text('cho-manno, revolutionary'/'MMQ', 'Prevent all damage that would be dealt to Cho-Manno, Revolutionary.').
card_first_print('cho-manno, revolutionary', 'MMQ').
card_image_name('cho-manno, revolutionary'/'MMQ', 'cho-manno, revolutionary').
card_uid('cho-manno, revolutionary'/'MMQ', 'MMQ:Cho-Manno, Revolutionary:cho-manno, revolutionary').
card_rarity('cho-manno, revolutionary'/'MMQ', 'Rare').
card_artist('cho-manno, revolutionary'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('cho-manno, revolutionary'/'MMQ', '11').
card_flavor_text('cho-manno, revolutionary'/'MMQ', '\"Mercadia\'s masks can no longer hide the truth. Our day has come at last.\"\n—Cho-Manno').
card_multiverse_id('cho-manno, revolutionary'/'MMQ', '20411').

card_in_set('cinder elemental', 'MMQ').
card_original_type('cinder elemental'/'MMQ', 'Creature — Elemental').
card_original_text('cinder elemental'/'MMQ', '{X}{R}, {T}, Sacrifice Cinder Elemental: Cinder Elemental deals X damage to target creature or player.').
card_first_print('cinder elemental', 'MMQ').
card_image_name('cinder elemental'/'MMQ', 'cinder elemental').
card_uid('cinder elemental'/'MMQ', 'MMQ:Cinder Elemental:cinder elemental').
card_rarity('cinder elemental'/'MMQ', 'Uncommon').
card_artist('cinder elemental'/'MMQ', 'Greg Staples').
card_number('cinder elemental'/'MMQ', '183').
card_flavor_text('cinder elemental'/'MMQ', 'Their rage can grow to such proportions that they explode in a cloud of fire.').
card_multiverse_id('cinder elemental'/'MMQ', '19608').

card_in_set('clear the land', 'MMQ').
card_original_type('clear the land'/'MMQ', 'Sorcery').
card_original_text('clear the land'/'MMQ', 'Each player reveals the top five cards of his or her library, puts into play tapped all land cards revealed this way, and removes the rest from the game.').
card_first_print('clear the land', 'MMQ').
card_image_name('clear the land'/'MMQ', 'clear the land').
card_uid('clear the land'/'MMQ', 'MMQ:Clear the Land:clear the land').
card_rarity('clear the land'/'MMQ', 'Rare').
card_artist('clear the land'/'MMQ', 'Bradley Williams').
card_number('clear the land'/'MMQ', '235').
card_multiverse_id('clear the land'/'MMQ', '19873').

card_in_set('close quarters', 'MMQ').
card_original_type('close quarters'/'MMQ', 'Enchantment').
card_original_text('close quarters'/'MMQ', 'Whenever a creature you control becomes blocked, Close Quarters deals 1 damage to target creature or player.').
card_first_print('close quarters', 'MMQ').
card_image_name('close quarters'/'MMQ', 'close quarters').
card_uid('close quarters'/'MMQ', 'MMQ:Close Quarters:close quarters').
card_rarity('close quarters'/'MMQ', 'Uncommon').
card_artist('close quarters'/'MMQ', 'Ron Spencer').
card_number('close quarters'/'MMQ', '184').
card_flavor_text('close quarters'/'MMQ', 'The Mercadians\' ineptitude in close combat sometimes accidentally pays off.').
card_multiverse_id('close quarters'/'MMQ', '19847').

card_in_set('cloud sprite', 'MMQ').
card_original_type('cloud sprite'/'MMQ', 'Creature — Faerie').
card_original_text('cloud sprite'/'MMQ', 'Flying\nCloud Sprite may block only creatures with flying.').
card_first_print('cloud sprite', 'MMQ').
card_image_name('cloud sprite'/'MMQ', 'cloud sprite').
card_uid('cloud sprite'/'MMQ', 'MMQ:Cloud Sprite:cloud sprite').
card_rarity('cloud sprite'/'MMQ', 'Common').
card_artist('cloud sprite'/'MMQ', 'Mark Zug').
card_number('cloud sprite'/'MMQ', '67').
card_flavor_text('cloud sprite'/'MMQ', 'The delicate sprites carry messages for Saprazzans, but they refuse to land in Mercadia City\'s filthy markets.').
card_multiverse_id('cloud sprite'/'MMQ', '19563').

card_in_set('coastal piracy', 'MMQ').
card_original_type('coastal piracy'/'MMQ', 'Enchantment').
card_original_text('coastal piracy'/'MMQ', 'Whenever a creature you control deals combat damage to an opponent, you may draw a card.').
card_first_print('coastal piracy', 'MMQ').
card_image_name('coastal piracy'/'MMQ', 'coastal piracy').
card_uid('coastal piracy'/'MMQ', 'MMQ:Coastal Piracy:coastal piracy').
card_rarity('coastal piracy'/'MMQ', 'Uncommon').
card_artist('coastal piracy'/'MMQ', 'Matthew D. Wilson').
card_number('coastal piracy'/'MMQ', '68').
card_flavor_text('coastal piracy'/'MMQ', '\"I don\'t like to think of myself as a pirate. I\'m more like a stimulator of the local economy.\"').
card_multiverse_id('coastal piracy'/'MMQ', '19805').

card_in_set('collective unconscious', 'MMQ').
card_original_type('collective unconscious'/'MMQ', 'Sorcery').
card_original_text('collective unconscious'/'MMQ', 'Draw a card for each creature you control.').
card_first_print('collective unconscious', 'MMQ').
card_image_name('collective unconscious'/'MMQ', 'collective unconscious').
card_uid('collective unconscious'/'MMQ', 'MMQ:Collective Unconscious:collective unconscious').
card_rarity('collective unconscious'/'MMQ', 'Rare').
card_artist('collective unconscious'/'MMQ', 'Andrew Goldhawk').
card_number('collective unconscious'/'MMQ', '236').
card_flavor_text('collective unconscious'/'MMQ', 'Gerrard knew little of Ramos, the sky god they worshiped. But he felt the power of the chant.').
card_multiverse_id('collective unconscious'/'MMQ', '19739').

card_in_set('common cause', 'MMQ').
card_original_type('common cause'/'MMQ', 'Enchantment').
card_original_text('common cause'/'MMQ', 'Nonartifact creatures get +2/+2 as long as they all share a color.').
card_first_print('common cause', 'MMQ').
card_image_name('common cause'/'MMQ', 'common cause').
card_uid('common cause'/'MMQ', 'MMQ:Common Cause:common cause').
card_rarity('common cause'/'MMQ', 'Rare').
card_artist('common cause'/'MMQ', 'John Matson').
card_number('common cause'/'MMQ', '13').
card_flavor_text('common cause'/'MMQ', 'Mercadia City\'s troops are at their best when they have no enemy to fight.').
card_multiverse_id('common cause'/'MMQ', '19665').

card_in_set('conspiracy', 'MMQ').
card_original_type('conspiracy'/'MMQ', 'Enchantment').
card_original_text('conspiracy'/'MMQ', 'As Conspiracy comes into play, choose a creature type.\nCreatures you control and creature cards in your graveyard, hand, and library are of the chosen type.').
card_first_print('conspiracy', 'MMQ').
card_image_name('conspiracy'/'MMQ', 'conspiracy').
card_uid('conspiracy'/'MMQ', 'MMQ:Conspiracy:conspiracy').
card_rarity('conspiracy'/'MMQ', 'Rare').
card_artist('conspiracy'/'MMQ', 'Jeff Easley').
card_number('conspiracy'/'MMQ', '127').
card_multiverse_id('conspiracy'/'MMQ', '19702').

card_in_set('cornered market', 'MMQ').
card_original_type('cornered market'/'MMQ', 'Enchantment').
card_original_text('cornered market'/'MMQ', 'Players can\'t play spells or nonbasic lands with the same name as a card in play.').
card_first_print('cornered market', 'MMQ').
card_image_name('cornered market'/'MMQ', 'cornered market').
card_uid('cornered market'/'MMQ', 'MMQ:Cornered Market:cornered market').
card_rarity('cornered market'/'MMQ', 'Rare').
card_artist('cornered market'/'MMQ', 'Edward P. Beard, Jr.').
card_number('cornered market'/'MMQ', '14').
card_flavor_text('cornered market'/'MMQ', 'Don\'t step on the livelihood of other vendors, or they may step on you.\n—Mercadian saying').
card_multiverse_id('cornered market'/'MMQ', '19663').

card_in_set('corrupt official', 'MMQ').
card_original_type('corrupt official'/'MMQ', 'Creature — Minion').
card_original_text('corrupt official'/'MMQ', '{2}{B} Regenerate Corrupt Official.\nWhenever Corrupt Official becomes blocked, defending player discards a card at random from his or her hand.').
card_first_print('corrupt official', 'MMQ').
card_image_name('corrupt official'/'MMQ', 'corrupt official').
card_uid('corrupt official'/'MMQ', 'MMQ:Corrupt Official:corrupt official').
card_rarity('corrupt official'/'MMQ', 'Rare').
card_artist('corrupt official'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('corrupt official'/'MMQ', '128').
card_flavor_text('corrupt official'/'MMQ', 'His favor comes at a high price.').
card_multiverse_id('corrupt official'/'MMQ', '19715').

card_in_set('counterspell', 'MMQ').
card_original_type('counterspell'/'MMQ', 'Instant').
card_original_text('counterspell'/'MMQ', 'Counter target spell.').
card_image_name('counterspell'/'MMQ', 'counterspell').
card_uid('counterspell'/'MMQ', 'MMQ:Counterspell:counterspell').
card_rarity('counterspell'/'MMQ', 'Common').
card_artist('counterspell'/'MMQ', 'Gao Yan').
card_number('counterspell'/'MMQ', '69').
card_flavor_text('counterspell'/'MMQ', '\"Your attack has been rendered harmless. It is, however, quite pretty.\"\n—Saprazzan vizier').
card_multiverse_id('counterspell'/'MMQ', '19570').

card_in_set('cowardice', 'MMQ').
card_original_type('cowardice'/'MMQ', 'Enchantment').
card_original_text('cowardice'/'MMQ', 'Whenever a creature becomes the target of a spell or ability, return that creature to its owner\'s hand.').
card_first_print('cowardice', 'MMQ').
card_image_name('cowardice'/'MMQ', 'cowardice').
card_uid('cowardice'/'MMQ', 'MMQ:Cowardice:cowardice').
card_rarity('cowardice'/'MMQ', 'Rare').
card_artist('cowardice'/'MMQ', 'Scott M. Fischer').
card_number('cowardice'/'MMQ', '70').
card_flavor_text('cowardice'/'MMQ', 'Gerrard was the Mercadian soldiers\' greatest hero; they followed him blindly. When he left, they stumbled around blindly.').
card_multiverse_id('cowardice'/'MMQ', '19683').

card_in_set('crackdown', 'MMQ').
card_original_type('crackdown'/'MMQ', 'Enchantment').
card_original_text('crackdown'/'MMQ', 'Nonwhite creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_first_print('crackdown', 'MMQ').
card_image_name('crackdown'/'MMQ', 'crackdown').
card_uid('crackdown'/'MMQ', 'MMQ:Crackdown:crackdown').
card_rarity('crackdown'/'MMQ', 'Rare').
card_artist('crackdown'/'MMQ', 'Rebecca Guay').
card_number('crackdown'/'MMQ', '15').
card_flavor_text('crackdown'/'MMQ', '\"There is no place for mercy in defense of our lives.\"\n—Ta-Spon, Cho-Arrim executioner').
card_multiverse_id('crackdown'/'MMQ', '19664').

card_in_set('crag saurian', 'MMQ').
card_original_type('crag saurian'/'MMQ', 'Creature — Lizard').
card_original_text('crag saurian'/'MMQ', 'Whenever Crag Saurian is dealt damage, the controller of that damage\'s source gains control of Crag Saurian.').
card_first_print('crag saurian', 'MMQ').
card_image_name('crag saurian'/'MMQ', 'crag saurian').
card_uid('crag saurian'/'MMQ', 'MMQ:Crag Saurian:crag saurian').
card_rarity('crag saurian'/'MMQ', 'Rare').
card_artist('crag saurian'/'MMQ', 'Matthew D. Wilson').
card_number('crag saurian'/'MMQ', '185').
card_flavor_text('crag saurian'/'MMQ', 'It follows the strongest leader—the one with the longest whip.').
card_multiverse_id('crag saurian'/'MMQ', '19713').

card_in_set('crash', 'MMQ').
card_original_type('crash'/'MMQ', 'Instant').
card_original_text('crash'/'MMQ', 'You may sacrifice a mountain instead of paying Crash\'s mana cost.\nDestroy target artifact.').
card_first_print('crash', 'MMQ').
card_image_name('crash'/'MMQ', 'crash').
card_uid('crash'/'MMQ', 'MMQ:Crash:crash').
card_rarity('crash'/'MMQ', 'Common').
card_artist('crash'/'MMQ', 'Doug Chaffee').
card_number('crash'/'MMQ', '186').
card_flavor_text('crash'/'MMQ', 'Gerrard shook his head as the ground rushed to meet him. \"This,\" he thought, \"is not the start I was hoping for.\"').
card_multiverse_id('crash'/'MMQ', '19616').

card_in_set('credit voucher', 'MMQ').
card_original_type('credit voucher'/'MMQ', 'Artifact').
card_original_text('credit voucher'/'MMQ', '{2}, {T}, Sacrifice Credit Voucher: Shuffle any number of cards from your hand into your library, then draw that many cards.').
card_first_print('credit voucher', 'MMQ').
card_image_name('credit voucher'/'MMQ', 'credit voucher').
card_uid('credit voucher'/'MMQ', 'MMQ:Credit Voucher:credit voucher').
card_rarity('credit voucher'/'MMQ', 'Uncommon').
card_artist('credit voucher'/'MMQ', 'D. Alexander Gregory').
card_number('credit voucher'/'MMQ', '289').
card_multiverse_id('credit voucher'/'MMQ', '19885').

card_in_set('crenellated wall', 'MMQ').
card_original_type('crenellated wall'/'MMQ', 'Artifact Creature — Wall').
card_original_text('crenellated wall'/'MMQ', '(Walls can\'t attack.)\n{T}: Target creature gets +0/+4 until end of turn.').
card_first_print('crenellated wall', 'MMQ').
card_image_name('crenellated wall'/'MMQ', 'crenellated wall').
card_uid('crenellated wall'/'MMQ', 'MMQ:Crenellated Wall:crenellated wall').
card_rarity('crenellated wall'/'MMQ', 'Uncommon').
card_artist('crenellated wall'/'MMQ', 'Arnie Swekel').
card_number('crenellated wall'/'MMQ', '290').
card_flavor_text('crenellated wall'/'MMQ', 'Mercadian soldiers excel at finding things to stand behind.').
card_multiverse_id('crenellated wall'/'MMQ', '19880').

card_in_set('crooked scales', 'MMQ').
card_original_type('crooked scales'/'MMQ', 'Artifact').
card_original_text('crooked scales'/'MMQ', '{4}, {T}: Choose target creature you control and target creature an opponent controls. Flip a coin. If you win the flip, destroy the creature the opponent controls. If you lose the flip, destroy the creature you control unless you pay {3} and reflip the coin.').
card_first_print('crooked scales', 'MMQ').
card_image_name('crooked scales'/'MMQ', 'crooked scales').
card_uid('crooked scales'/'MMQ', 'MMQ:Crooked Scales:crooked scales').
card_rarity('crooked scales'/'MMQ', 'Rare').
card_artist('crooked scales'/'MMQ', 'Ron Spears').
card_number('crooked scales'/'MMQ', '291').
card_multiverse_id('crooked scales'/'MMQ', '19752').

card_in_set('crossbow infantry', 'MMQ').
card_original_type('crossbow infantry'/'MMQ', 'Creature — Soldier').
card_original_text('crossbow infantry'/'MMQ', '{T}: Crossbow Infantry deals 1 damage to target attacking or blocking creature.').
card_first_print('crossbow infantry', 'MMQ').
card_image_name('crossbow infantry'/'MMQ', 'crossbow infantry').
card_uid('crossbow infantry'/'MMQ', 'MMQ:Crossbow Infantry:crossbow infantry').
card_rarity('crossbow infantry'/'MMQ', 'Common').
card_artist('crossbow infantry'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('crossbow infantry'/'MMQ', '16').
card_flavor_text('crossbow infantry'/'MMQ', 'The crossbow is the ideal weapon for the lazy Mercadians: just point and shoot.').
card_multiverse_id('crossbow infantry'/'MMQ', '19540').

card_in_set('crumbling sanctuary', 'MMQ').
card_original_type('crumbling sanctuary'/'MMQ', 'Artifact').
card_original_text('crumbling sanctuary'/'MMQ', 'For each 1 damage that would be dealt to a player, that player removes the top card of his or her library from the game instead.').
card_first_print('crumbling sanctuary', 'MMQ').
card_image_name('crumbling sanctuary'/'MMQ', 'crumbling sanctuary').
card_uid('crumbling sanctuary'/'MMQ', 'MMQ:Crumbling Sanctuary:crumbling sanctuary').
card_rarity('crumbling sanctuary'/'MMQ', 'Rare').
card_artist('crumbling sanctuary'/'MMQ', 'Randy Gallegos').
card_number('crumbling sanctuary'/'MMQ', '292').
card_multiverse_id('crumbling sanctuary'/'MMQ', '19746').

card_in_set('customs depot', 'MMQ').
card_original_type('customs depot'/'MMQ', 'Enchantment').
card_original_text('customs depot'/'MMQ', 'Whenever you play a creature spell, you may pay {1}. If you do, draw a card, then discard a card from your hand.').
card_first_print('customs depot', 'MMQ').
card_image_name('customs depot'/'MMQ', 'customs depot').
card_uid('customs depot'/'MMQ', 'MMQ:Customs Depot:customs depot').
card_rarity('customs depot'/'MMQ', 'Uncommon').
card_artist('customs depot'/'MMQ', 'Scott M. Fischer').
card_number('customs depot'/'MMQ', '71').
card_flavor_text('customs depot'/'MMQ', 'A bribe is always faster than filling out paperwork.').
card_multiverse_id('customs depot'/'MMQ', '19575').

card_in_set('dark ritual', 'MMQ').
card_original_type('dark ritual'/'MMQ', 'Instant').
card_original_text('dark ritual'/'MMQ', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'MMQ', 'dark ritual').
card_uid('dark ritual'/'MMQ', 'MMQ:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'MMQ', 'Common').
card_artist('dark ritual'/'MMQ', 'Rebecca Guay').
card_number('dark ritual'/'MMQ', '129').
card_flavor_text('dark ritual'/'MMQ', '\"Eye of twilight, give us sight.\nTake our offer, give us might.\nMake our power tears of night.\"').
card_multiverse_id('dark ritual'/'MMQ', '19592').

card_in_set('darting merfolk', 'MMQ').
card_original_type('darting merfolk'/'MMQ', 'Creature — Merfolk').
card_original_text('darting merfolk'/'MMQ', '{U} Return Darting Merfolk to its owner\'s hand.').
card_first_print('darting merfolk', 'MMQ').
card_image_name('darting merfolk'/'MMQ', 'darting merfolk').
card_uid('darting merfolk'/'MMQ', 'MMQ:Darting Merfolk:darting merfolk').
card_rarity('darting merfolk'/'MMQ', 'Common').
card_artist('darting merfolk'/'MMQ', 'Sam Wood').
card_number('darting merfolk'/'MMQ', '72').
card_flavor_text('darting merfolk'/'MMQ', 'You might as well try to catch water with a net.').
card_multiverse_id('darting merfolk'/'MMQ', '19559').

card_in_set('dawnstrider', 'MMQ').
card_original_type('dawnstrider'/'MMQ', 'Creature — Spellshaper').
card_original_text('dawnstrider'/'MMQ', '{G}, {T}, Discard a card from your hand: Prevent all combat damage that would be dealt this turn.').
card_first_print('dawnstrider', 'MMQ').
card_image_name('dawnstrider'/'MMQ', 'dawnstrider').
card_uid('dawnstrider'/'MMQ', 'MMQ:Dawnstrider:dawnstrider').
card_rarity('dawnstrider'/'MMQ', 'Rare').
card_artist('dawnstrider'/'MMQ', 'rk post').
card_number('dawnstrider'/'MMQ', '237').
card_flavor_text('dawnstrider'/'MMQ', 'Morning\'s mists can hide many things.').
card_multiverse_id('dawnstrider'/'MMQ', '19855').

card_in_set('deadly insect', 'MMQ').
card_original_type('deadly insect'/'MMQ', 'Creature — Insect').
card_original_text('deadly insect'/'MMQ', 'Deadly Insect can\'t be the target of spells or abilities.').
card_image_name('deadly insect'/'MMQ', 'deadly insect').
card_uid('deadly insect'/'MMQ', 'MMQ:Deadly Insect:deadly insect').
card_rarity('deadly insect'/'MMQ', 'Common').
card_artist('deadly insect'/'MMQ', 'Randy Gallegos').
card_number('deadly insect'/'MMQ', '238').
card_flavor_text('deadly insect'/'MMQ', 'With his newfound sense of self-importance, Squee set his sights on ever-bigger bugs.').
card_multiverse_id('deadly insect'/'MMQ', '19628').

card_in_set('deathgazer', 'MMQ').
card_original_type('deathgazer'/'MMQ', 'Creature — Lizard').
card_original_text('deathgazer'/'MMQ', 'Whenever Deathgazer blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.').
card_first_print('deathgazer', 'MMQ').
card_image_name('deathgazer'/'MMQ', 'deathgazer').
card_uid('deathgazer'/'MMQ', 'MMQ:Deathgazer:deathgazer').
card_rarity('deathgazer'/'MMQ', 'Uncommon').
card_artist('deathgazer'/'MMQ', 'Donato Giancola').
card_number('deathgazer'/'MMQ', '130').
card_multiverse_id('deathgazer'/'MMQ', '19820').

card_in_set('deepwood drummer', 'MMQ').
card_original_type('deepwood drummer'/'MMQ', 'Creature — Spellshaper').
card_original_text('deepwood drummer'/'MMQ', '{G}, {T}, Discard a card from your hand: Target creature gets +2/+2 until end of turn.').
card_first_print('deepwood drummer', 'MMQ').
card_image_name('deepwood drummer'/'MMQ', 'deepwood drummer').
card_uid('deepwood drummer'/'MMQ', 'MMQ:Deepwood Drummer:deepwood drummer').
card_rarity('deepwood drummer'/'MMQ', 'Common').
card_artist('deepwood drummer'/'MMQ', 'Ron Spears').
card_number('deepwood drummer'/'MMQ', '239').
card_flavor_text('deepwood drummer'/'MMQ', 'His drums echo Deepwood\'s heartbeat.').
card_multiverse_id('deepwood drummer'/'MMQ', '19621').

card_in_set('deepwood elder', 'MMQ').
card_original_type('deepwood elder'/'MMQ', 'Creature — Spellshaper').
card_original_text('deepwood elder'/'MMQ', '{X}{G}{G}, {T}, Discard a card from your hand: X target lands become forests until end of turn.').
card_first_print('deepwood elder', 'MMQ').
card_image_name('deepwood elder'/'MMQ', 'deepwood elder').
card_uid('deepwood elder'/'MMQ', 'MMQ:Deepwood Elder:deepwood elder').
card_rarity('deepwood elder'/'MMQ', 'Rare').
card_artist('deepwood elder'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('deepwood elder'/'MMQ', '240').
card_flavor_text('deepwood elder'/'MMQ', 'They\'d guarded the Henge so long that they were as attuned to the powerstones as they were to the trees.').
card_multiverse_id('deepwood elder'/'MMQ', '22895').

card_in_set('deepwood ghoul', 'MMQ').
card_original_type('deepwood ghoul'/'MMQ', 'Creature — Zombie').
card_original_text('deepwood ghoul'/'MMQ', 'Pay 2 life: Regenerate Deepwood Ghoul.').
card_first_print('deepwood ghoul', 'MMQ').
card_image_name('deepwood ghoul'/'MMQ', 'deepwood ghoul').
card_uid('deepwood ghoul'/'MMQ', 'MMQ:Deepwood Ghoul:deepwood ghoul').
card_rarity('deepwood ghoul'/'MMQ', 'Common').
card_artist('deepwood ghoul'/'MMQ', 'Alan Pollack').
card_number('deepwood ghoul'/'MMQ', '131').
card_flavor_text('deepwood ghoul'/'MMQ', 'They feed on the living to stay undead.').
card_multiverse_id('deepwood ghoul'/'MMQ', '19587').

card_in_set('deepwood legate', 'MMQ').
card_original_type('deepwood legate'/'MMQ', 'Creature — Shade').
card_original_text('deepwood legate'/'MMQ', 'If an opponent controls a forest and you control a swamp, you may play Deepwood Legate without paying its mana cost.\n{B} Deepwood Legate gets +1/+1 until end of turn.').
card_first_print('deepwood legate', 'MMQ').
card_image_name('deepwood legate'/'MMQ', 'deepwood legate').
card_uid('deepwood legate'/'MMQ', 'MMQ:Deepwood Legate:deepwood legate').
card_rarity('deepwood legate'/'MMQ', 'Uncommon').
card_artist('deepwood legate'/'MMQ', 'Pete Venters').
card_number('deepwood legate'/'MMQ', '132').
card_multiverse_id('deepwood legate'/'MMQ', '19821').

card_in_set('deepwood tantiv', 'MMQ').
card_original_type('deepwood tantiv'/'MMQ', 'Creature — Beast').
card_original_text('deepwood tantiv'/'MMQ', 'Whenever Deepwood Tantiv becomes blocked, you gain 2 life.').
card_first_print('deepwood tantiv', 'MMQ').
card_image_name('deepwood tantiv'/'MMQ', 'deepwood tantiv').
card_uid('deepwood tantiv'/'MMQ', 'MMQ:Deepwood Tantiv:deepwood tantiv').
card_rarity('deepwood tantiv'/'MMQ', 'Uncommon').
card_artist('deepwood tantiv'/'MMQ', 'Joel Biske').
card_number('deepwood tantiv'/'MMQ', '241').
card_flavor_text('deepwood tantiv'/'MMQ', 'A single tantiv is just as dangerous as a herd.').
card_multiverse_id('deepwood tantiv'/'MMQ', '19866').

card_in_set('deepwood wolverine', 'MMQ').
card_original_type('deepwood wolverine'/'MMQ', 'Creature — Wolverine').
card_original_text('deepwood wolverine'/'MMQ', 'Whenever Deepwood Wolverine becomes blocked, it gets +2/+0 until end of turn.').
card_first_print('deepwood wolverine', 'MMQ').
card_image_name('deepwood wolverine'/'MMQ', 'deepwood wolverine').
card_uid('deepwood wolverine'/'MMQ', 'MMQ:Deepwood Wolverine:deepwood wolverine').
card_rarity('deepwood wolverine'/'MMQ', 'Common').
card_artist('deepwood wolverine'/'MMQ', 'Ray Lago').
card_number('deepwood wolverine'/'MMQ', '242').
card_flavor_text('deepwood wolverine'/'MMQ', 'The jhovalls are depleting its food sources, the Mercadians are eroding its home, and you\'re wondering why it\'s angry?').
card_multiverse_id('deepwood wolverine'/'MMQ', '19623').

card_in_set('dehydration', 'MMQ').
card_original_type('dehydration'/'MMQ', 'Enchant Creature').
card_original_text('dehydration'/'MMQ', 'Enchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('dehydration', 'MMQ').
card_image_name('dehydration'/'MMQ', 'dehydration').
card_uid('dehydration'/'MMQ', 'MMQ:Dehydration:dehydration').
card_rarity('dehydration'/'MMQ', 'Common').
card_artist('dehydration'/'MMQ', 'Val Mayerik').
card_number('dehydration'/'MMQ', '73').
card_flavor_text('dehydration'/'MMQ', '\"You say Saprazzo is a city built on water? I hope they have enough to wash the dust from my throat.\"\n—Sisay').
card_multiverse_id('dehydration'/'MMQ', '19577').

card_in_set('delraich', 'MMQ').
card_original_type('delraich'/'MMQ', 'Creature — Horror').
card_original_text('delraich'/'MMQ', 'Trample\nYou may sacrifice three black creatures instead of paying Delraich\'s mana cost.').
card_first_print('delraich', 'MMQ').
card_image_name('delraich'/'MMQ', 'delraich').
card_uid('delraich'/'MMQ', 'MMQ:Delraich:delraich').
card_rarity('delraich'/'MMQ', 'Rare').
card_artist('delraich'/'MMQ', 'Todd Lockwood').
card_number('delraich'/'MMQ', '133').
card_flavor_text('delraich'/'MMQ', 'It wears the souls of its victims like jewels on a chain.').
card_multiverse_id('delraich'/'MMQ', '19697').

card_in_set('desert twister', 'MMQ').
card_original_type('desert twister'/'MMQ', 'Sorcery').
card_original_text('desert twister'/'MMQ', 'Destroy target permanent.').
card_image_name('desert twister'/'MMQ', 'desert twister').
card_uid('desert twister'/'MMQ', 'MMQ:Desert Twister:desert twister').
card_rarity('desert twister'/'MMQ', 'Uncommon').
card_artist('desert twister'/'MMQ', 'Kevin Murphy').
card_number('desert twister'/'MMQ', '243').
card_flavor_text('desert twister'/'MMQ', 'The magical dust clouds Mercadian mages use to traverse the plane generate storms that devastate the surrounding terrain. But as long as the storms don\'t touch the mountain, the nobles simply don\'t care.').
card_multiverse_id('desert twister'/'MMQ', '19638').

card_in_set('devout witness', 'MMQ').
card_original_type('devout witness'/'MMQ', 'Creature — Spellshaper').
card_original_text('devout witness'/'MMQ', '{1}{W}, {T}, Discard a card from your hand: Destroy target artifact or enchantment.').
card_first_print('devout witness', 'MMQ').
card_image_name('devout witness'/'MMQ', 'devout witness').
card_uid('devout witness'/'MMQ', 'MMQ:Devout Witness:devout witness').
card_rarity('devout witness'/'MMQ', 'Common').
card_artist('devout witness'/'MMQ', 'Don Hazeltine').
card_number('devout witness'/'MMQ', '17').
card_flavor_text('devout witness'/'MMQ', 'The Cho-Arrim fought Mercadia\'s decadence with more than just swords.').
card_multiverse_id('devout witness'/'MMQ', '19536').

card_in_set('diplomatic escort', 'MMQ').
card_original_type('diplomatic escort'/'MMQ', 'Creature — Spellshaper').
card_original_text('diplomatic escort'/'MMQ', '{U}, {T}, Discard a card from your hand: Counter target spell or ability that targets a creature.').
card_first_print('diplomatic escort', 'MMQ').
card_image_name('diplomatic escort'/'MMQ', 'diplomatic escort').
card_uid('diplomatic escort'/'MMQ', 'MMQ:Diplomatic Escort:diplomatic escort').
card_rarity('diplomatic escort'/'MMQ', 'Uncommon').
card_artist('diplomatic escort'/'MMQ', 'Rebecca Guay').
card_number('diplomatic escort'/'MMQ', '74').
card_multiverse_id('diplomatic escort'/'MMQ', '19794').

card_in_set('diplomatic immunity', 'MMQ').
card_original_type('diplomatic immunity'/'MMQ', 'Enchant Creature').
card_original_text('diplomatic immunity'/'MMQ', 'Enchanted creature can\'t be the target of spells or abilities.\nDiplomatic Immunity can\'t be the target of spells or abilities.').
card_first_print('diplomatic immunity', 'MMQ').
card_image_name('diplomatic immunity'/'MMQ', 'diplomatic immunity').
card_uid('diplomatic immunity'/'MMQ', 'MMQ:Diplomatic Immunity:diplomatic immunity').
card_rarity('diplomatic immunity'/'MMQ', 'Common').
card_artist('diplomatic immunity'/'MMQ', 'Terese Nielsen').
card_number('diplomatic immunity'/'MMQ', '75').
card_multiverse_id('diplomatic immunity'/'MMQ', '19576').

card_in_set('disenchant', 'MMQ').
card_original_type('disenchant'/'MMQ', 'Instant').
card_original_text('disenchant'/'MMQ', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'MMQ', 'disenchant').
card_uid('disenchant'/'MMQ', 'MMQ:Disenchant:disenchant').
card_rarity('disenchant'/'MMQ', 'Common').
card_artist('disenchant'/'MMQ', 'Adam Rex').
card_number('disenchant'/'MMQ', '18').
card_flavor_text('disenchant'/'MMQ', 'The scepter of power is fragile in a calloused hand.\n—Cho-Arrim saying').
card_multiverse_id('disenchant'/'MMQ', '19548').

card_in_set('distorting lens', 'MMQ').
card_original_type('distorting lens'/'MMQ', 'Artifact').
card_original_text('distorting lens'/'MMQ', '{T}: Target permanent becomes the color of your choice until end of turn.').
card_first_print('distorting lens', 'MMQ').
card_image_name('distorting lens'/'MMQ', 'distorting lens').
card_uid('distorting lens'/'MMQ', 'MMQ:Distorting Lens:distorting lens').
card_rarity('distorting lens'/'MMQ', 'Rare').
card_artist('distorting lens'/'MMQ', 'Glen Angus').
card_number('distorting lens'/'MMQ', '293').
card_flavor_text('distorting lens'/'MMQ', 'The magistrate seems to prefer a distorted perspective of reality.').
card_multiverse_id('distorting lens'/'MMQ', '19758').

card_in_set('drake hatchling', 'MMQ').
card_original_type('drake hatchling'/'MMQ', 'Creature — Drake').
card_original_text('drake hatchling'/'MMQ', 'Flying\n{U} Drake Hatchling gets +1/+0 until end of turn. Play this ability only once each turn.').
card_first_print('drake hatchling', 'MMQ').
card_image_name('drake hatchling'/'MMQ', 'drake hatchling').
card_uid('drake hatchling'/'MMQ', 'MMQ:Drake Hatchling:drake hatchling').
card_rarity('drake hatchling'/'MMQ', 'Common').
card_artist('drake hatchling'/'MMQ', 'Bradley Williams').
card_number('drake hatchling'/'MMQ', '76').
card_flavor_text('drake hatchling'/'MMQ', 'There is beauty in the space between learning to fly and taking it for granted.').
card_multiverse_id('drake hatchling'/'MMQ', '19565').

card_in_set('dust bowl', 'MMQ').
card_original_type('dust bowl'/'MMQ', 'Land').
card_original_text('dust bowl'/'MMQ', '{T}: Add one colorless mana to your mana pool.\n{3}, {T}, Sacrifice a land: Destroy target nonbasic land.').
card_first_print('dust bowl', 'MMQ').
card_image_name('dust bowl'/'MMQ', 'dust bowl').
card_uid('dust bowl'/'MMQ', 'MMQ:Dust Bowl:dust bowl').
card_rarity('dust bowl'/'MMQ', 'Rare').
card_artist('dust bowl'/'MMQ', 'Ben Thompson').
card_number('dust bowl'/'MMQ', '316').
card_multiverse_id('dust bowl'/'MMQ', '19772').

card_in_set('embargo', 'MMQ').
card_original_type('embargo'/'MMQ', 'Enchantment').
card_original_text('embargo'/'MMQ', 'Nonland permanents don\'t untap during their controllers\' untap steps.\nAt the beginning of your upkeep, you lose 2 life.').
card_first_print('embargo', 'MMQ').
card_image_name('embargo'/'MMQ', 'embargo').
card_uid('embargo'/'MMQ', 'MMQ:Embargo:embargo').
card_rarity('embargo'/'MMQ', 'Rare').
card_artist('embargo'/'MMQ', 'Nelson DeCastro').
card_number('embargo'/'MMQ', '77').
card_multiverse_id('embargo'/'MMQ', '19679').

card_in_set('energy flux', 'MMQ').
card_original_type('energy flux'/'MMQ', 'Enchantment').
card_original_text('energy flux'/'MMQ', 'All artifacts gain \"At the beginning of your upkeep, sacrifice this artifact unless you pay {2}.\"').
card_image_name('energy flux'/'MMQ', 'energy flux').
card_uid('energy flux'/'MMQ', 'MMQ:Energy Flux:energy flux').
card_rarity('energy flux'/'MMQ', 'Uncommon').
card_artist('energy flux'/'MMQ', 'Qiao Dafu').
card_number('energy flux'/'MMQ', '78').
card_flavor_text('energy flux'/'MMQ', 'Karn said nothing—he just grasped the powerstone and squeezed tightly. The wild flashing ceased.').
card_multiverse_id('energy flux'/'MMQ', '19806').

card_in_set('enslaved horror', 'MMQ').
card_original_type('enslaved horror'/'MMQ', 'Creature — Horror').
card_original_text('enslaved horror'/'MMQ', 'When Enslaved Horror comes into play, each other player may return a creature card from his or her graveyard to play.').
card_first_print('enslaved horror', 'MMQ').
card_image_name('enslaved horror'/'MMQ', 'enslaved horror').
card_uid('enslaved horror'/'MMQ', 'MMQ:Enslaved Horror:enslaved horror').
card_rarity('enslaved horror'/'MMQ', 'Uncommon').
card_artist('enslaved horror'/'MMQ', 'Mike Ploog').
card_number('enslaved horror'/'MMQ', '134').
card_multiverse_id('enslaved horror'/'MMQ', '19817').

card_in_set('erithizon', 'MMQ').
card_original_type('erithizon'/'MMQ', 'Creature — Beast').
card_original_text('erithizon'/'MMQ', 'Whenever Erithizon attacks, put a +1/+1 counter on target creature of defending player\'s choice.').
card_first_print('erithizon', 'MMQ').
card_image_name('erithizon'/'MMQ', 'erithizon').
card_uid('erithizon'/'MMQ', 'MMQ:Erithizon:erithizon').
card_rarity('erithizon'/'MMQ', 'Rare').
card_artist('erithizon'/'MMQ', 'Scott M. Fischer').
card_number('erithizon'/'MMQ', '244').
card_flavor_text('erithizon'/'MMQ', 'Unlike scorpions, the erithizon isn\'t immune to its own weaponry.').
card_multiverse_id('erithizon'/'MMQ', '19734').

card_in_set('extortion', 'MMQ').
card_original_type('extortion'/'MMQ', 'Sorcery').
card_original_text('extortion'/'MMQ', 'Look at target player\'s hand and choose up to two cards from it. That player discards those cards.').
card_first_print('extortion', 'MMQ').
card_image_name('extortion'/'MMQ', 'extortion').
card_uid('extortion'/'MMQ', 'MMQ:Extortion:extortion').
card_rarity('extortion'/'MMQ', 'Rare').
card_artist('extortion'/'MMQ', 'Pete Venters').
card_number('extortion'/'MMQ', '135').
card_flavor_text('extortion'/'MMQ', 'The enforcers make no effort to distinguish gold from flesh.').
card_multiverse_id('extortion'/'MMQ', '19703').

card_in_set('extravagant spirit', 'MMQ').
card_original_type('extravagant spirit'/'MMQ', 'Creature — Spirit').
card_original_text('extravagant spirit'/'MMQ', 'Flying\nAt the beginning of your upkeep, sacrifice Extravagant Spirit unless you pay {1} for each card in your hand.').
card_first_print('extravagant spirit', 'MMQ').
card_image_name('extravagant spirit'/'MMQ', 'extravagant spirit').
card_uid('extravagant spirit'/'MMQ', 'MMQ:Extravagant Spirit:extravagant spirit').
card_rarity('extravagant spirit'/'MMQ', 'Rare').
card_artist('extravagant spirit'/'MMQ', 'Edward P. Beard, Jr.').
card_number('extravagant spirit'/'MMQ', '79').
card_flavor_text('extravagant spirit'/'MMQ', 'The fewer your possessions, the fewer your worries.').
card_multiverse_id('extravagant spirit'/'MMQ', '19673').

card_in_set('eye of ramos', 'MMQ').
card_original_type('eye of ramos'/'MMQ', 'Artifact').
card_original_text('eye of ramos'/'MMQ', '{T}: Add one blue mana to your mana pool. \nSacrifice Eye of Ramos: Add one blue mana to your mana pool.').
card_first_print('eye of ramos', 'MMQ').
card_image_name('eye of ramos'/'MMQ', 'eye of ramos').
card_uid('eye of ramos'/'MMQ', 'MMQ:Eye of Ramos:eye of ramos').
card_rarity('eye of ramos'/'MMQ', 'Rare').
card_artist('eye of ramos'/'MMQ', 'David Martin').
card_number('eye of ramos'/'MMQ', '294').
card_flavor_text('eye of ramos'/'MMQ', 'Ramos wept, and there were seas.').
card_multiverse_id('eye of ramos'/'MMQ', '19765').

card_in_set('false demise', 'MMQ').
card_original_type('false demise'/'MMQ', 'Enchant Creature').
card_original_text('false demise'/'MMQ', 'When enchanted creature is put into a graveyard, return that creature to play under your control.').
card_image_name('false demise'/'MMQ', 'false demise').
card_uid('false demise'/'MMQ', 'MMQ:False Demise:false demise').
card_rarity('false demise'/'MMQ', 'Uncommon').
card_artist('false demise'/'MMQ', 'Pat Morrissey').
card_number('false demise'/'MMQ', '80').
card_flavor_text('false demise'/'MMQ', '\"Go down there and fish him out. Maybe his hide will fetch a price.\"').
card_multiverse_id('false demise'/'MMQ', '19812').

card_in_set('ferocity', 'MMQ').
card_original_type('ferocity'/'MMQ', 'Enchant Creature').
card_original_text('ferocity'/'MMQ', 'Whenever enchanted creature blocks or becomes blocked, you may put a +1/+1 counter on it.').
card_first_print('ferocity', 'MMQ').
card_image_name('ferocity'/'MMQ', 'ferocity').
card_uid('ferocity'/'MMQ', 'MMQ:Ferocity:ferocity').
card_rarity('ferocity'/'MMQ', 'Common').
card_artist('ferocity'/'MMQ', 'Pete Venters').
card_number('ferocity'/'MMQ', '245').
card_flavor_text('ferocity'/'MMQ', '\"You lot go on ahead. I\'ll keep killing them until it sticks.\"\n—Tahngarth').
card_multiverse_id('ferocity'/'MMQ', '19639').

card_in_set('flailing manticore', 'MMQ').
card_original_type('flailing manticore'/'MMQ', 'Creature — Monster').
card_original_text('flailing manticore'/'MMQ', 'Flying, first strike\n{1}: Flailing Manticore gets +1/+1 until end of turn. Any player may play this ability.\n{1}: Flailing Manticore gets -1/-1 until end of turn. Any player may play this ability.').
card_first_print('flailing manticore', 'MMQ').
card_image_name('flailing manticore'/'MMQ', 'flailing manticore').
card_uid('flailing manticore'/'MMQ', 'MMQ:Flailing Manticore:flailing manticore').
card_rarity('flailing manticore'/'MMQ', 'Rare').
card_artist('flailing manticore'/'MMQ', 'Roger Raupp').
card_number('flailing manticore'/'MMQ', '187').
card_multiverse_id('flailing manticore'/'MMQ', '19710').

card_in_set('flailing ogre', 'MMQ').
card_original_type('flailing ogre'/'MMQ', 'Creature — Ogre').
card_original_text('flailing ogre'/'MMQ', '{1}: Flailing Ogre gets +1/+1 until end of turn. Any player may play this ability.\n{1}: Flailing Ogre gets -1/-1 until end of turn. Any player may play this ability.').
card_first_print('flailing ogre', 'MMQ').
card_image_name('flailing ogre'/'MMQ', 'flailing ogre').
card_uid('flailing ogre'/'MMQ', 'MMQ:Flailing Ogre:flailing ogre').
card_rarity('flailing ogre'/'MMQ', 'Uncommon').
card_artist('flailing ogre'/'MMQ', 'Daniel R. Horne').
card_number('flailing ogre'/'MMQ', '188').
card_multiverse_id('flailing ogre'/'MMQ', '19839').

card_in_set('flailing soldier', 'MMQ').
card_original_type('flailing soldier'/'MMQ', 'Creature — Soldier').
card_original_text('flailing soldier'/'MMQ', '{1}: Flailing Soldier gets +1/+1 until end of turn. Any player may play this ability.\n{1}: Flailing Soldier gets -1/-1 until end of turn. Any player may play this ability.').
card_first_print('flailing soldier', 'MMQ').
card_image_name('flailing soldier'/'MMQ', 'flailing soldier').
card_uid('flailing soldier'/'MMQ', 'MMQ:Flailing Soldier:flailing soldier').
card_rarity('flailing soldier'/'MMQ', 'Common').
card_artist('flailing soldier'/'MMQ', 'Orizio Daniele').
card_number('flailing soldier'/'MMQ', '189').
card_multiverse_id('flailing soldier'/'MMQ', '19602').

card_in_set('flaming sword', 'MMQ').
card_original_type('flaming sword'/'MMQ', 'Enchant Creature').
card_original_text('flaming sword'/'MMQ', 'You may play Flaming Sword any time you could play an instant.\nTarget creature gets +1/+0 and has first strike.').
card_first_print('flaming sword', 'MMQ').
card_image_name('flaming sword'/'MMQ', 'flaming sword').
card_uid('flaming sword'/'MMQ', 'MMQ:Flaming Sword:flaming sword').
card_rarity('flaming sword'/'MMQ', 'Common').
card_artist('flaming sword'/'MMQ', 'Randy Gallegos').
card_number('flaming sword'/'MMQ', '190').
card_flavor_text('flaming sword'/'MMQ', '\"It\'s not Talruum crystal, but I must admit—it gets the job done.\"\n—Tahngarth').
card_multiverse_id('flaming sword'/'MMQ', '19613').

card_in_set('food chain', 'MMQ').
card_original_type('food chain'/'MMQ', 'Enchantment').
card_original_text('food chain'/'MMQ', 'Remove a creature you control from the game: Add X mana of any color to your mana pool, where X is the removed creature\'s converted mana cost plus one. This mana may be spent only to play creature spells.').
card_first_print('food chain', 'MMQ').
card_image_name('food chain'/'MMQ', 'food chain').
card_uid('food chain'/'MMQ', 'MMQ:Food Chain:food chain').
card_rarity('food chain'/'MMQ', 'Rare').
card_artist('food chain'/'MMQ', 'Val Mayerik').
card_number('food chain'/'MMQ', '246').
card_multiverse_id('food chain'/'MMQ', '19737').

card_in_set('forced march', 'MMQ').
card_original_type('forced march'/'MMQ', 'Sorcery').
card_original_text('forced march'/'MMQ', 'Destroy all creatures with converted mana cost X or less.').
card_first_print('forced march', 'MMQ').
card_image_name('forced march'/'MMQ', 'forced march').
card_uid('forced march'/'MMQ', 'MMQ:Forced March:forced march').
card_rarity('forced march'/'MMQ', 'Rare').
card_artist('forced march'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('forced march'/'MMQ', '136').
card_flavor_text('forced march'/'MMQ', 'The Caterans call it a screening process. The dead are in no condition to argue.').
card_multiverse_id('forced march'/'MMQ', '19826').

card_in_set('forest', 'MMQ').
card_original_type('forest'/'MMQ', 'Land').
card_original_text('forest'/'MMQ', 'G').
card_image_name('forest'/'MMQ', 'forest1').
card_uid('forest'/'MMQ', 'MMQ:Forest:forest1').
card_rarity('forest'/'MMQ', 'Basic Land').
card_artist('forest'/'MMQ', 'Donato Giancola').
card_number('forest'/'MMQ', '347').
card_multiverse_id('forest'/'MMQ', '20896').

card_in_set('forest', 'MMQ').
card_original_type('forest'/'MMQ', 'Land').
card_original_text('forest'/'MMQ', 'G').
card_image_name('forest'/'MMQ', 'forest2').
card_uid('forest'/'MMQ', 'MMQ:Forest:forest2').
card_rarity('forest'/'MMQ', 'Basic Land').
card_artist('forest'/'MMQ', 'Rob Alexander').
card_number('forest'/'MMQ', '348').
card_multiverse_id('forest'/'MMQ', '20897').

card_in_set('forest', 'MMQ').
card_original_type('forest'/'MMQ', 'Land').
card_original_text('forest'/'MMQ', 'G').
card_image_name('forest'/'MMQ', 'forest3').
card_uid('forest'/'MMQ', 'MMQ:Forest:forest3').
card_rarity('forest'/'MMQ', 'Basic Land').
card_artist('forest'/'MMQ', 'Rob Alexander').
card_number('forest'/'MMQ', '349').
card_multiverse_id('forest'/'MMQ', '20898').

card_in_set('forest', 'MMQ').
card_original_type('forest'/'MMQ', 'Land').
card_original_text('forest'/'MMQ', 'G').
card_image_name('forest'/'MMQ', 'forest4').
card_uid('forest'/'MMQ', 'MMQ:Forest:forest4').
card_rarity('forest'/'MMQ', 'Basic Land').
card_artist('forest'/'MMQ', 'Terry Springer').
card_number('forest'/'MMQ', '350').
card_multiverse_id('forest'/'MMQ', '20899').

card_in_set('foster', 'MMQ').
card_original_type('foster'/'MMQ', 'Enchantment').
card_original_text('foster'/'MMQ', 'Whenever a creature you control is put into a graveyard, you may pay {1}. If you do, reveal cards from the top of your library until you reveal a creature card. Put that card into your hand and the rest into your graveyard.').
card_first_print('foster', 'MMQ').
card_image_name('foster'/'MMQ', 'foster').
card_uid('foster'/'MMQ', 'MMQ:Foster:foster').
card_rarity('foster'/'MMQ', 'Rare').
card_artist('foster'/'MMQ', 'Carl Critchlow').
card_number('foster'/'MMQ', '247').
card_multiverse_id('foster'/'MMQ', '19740').

card_in_set('fountain of cho', 'MMQ').
card_original_type('fountain of cho'/'MMQ', 'Land').
card_original_text('fountain of cho'/'MMQ', 'Fountain of Cho comes into play tapped.\n{T}: Put a storage counter on Fountain of Cho.\n{T}, Remove any number of storage counters from Fountain of Cho: Add one white mana to your mana pool for each storage counter removed this way.').
card_first_print('fountain of cho', 'MMQ').
card_image_name('fountain of cho'/'MMQ', 'fountain of cho').
card_uid('fountain of cho'/'MMQ', 'MMQ:Fountain of Cho:fountain of cho').
card_rarity('fountain of cho'/'MMQ', 'Uncommon').
card_artist('fountain of cho'/'MMQ', 'Scott Hampton').
card_number('fountain of cho'/'MMQ', '317').
card_multiverse_id('fountain of cho'/'MMQ', '19893').

card_in_set('fountain watch', 'MMQ').
card_original_type('fountain watch'/'MMQ', 'Creature — Guardian').
card_original_text('fountain watch'/'MMQ', 'Artifacts and enchantments you control can\'t be the target of spells or abilities.').
card_first_print('fountain watch', 'MMQ').
card_image_name('fountain watch'/'MMQ', 'fountain watch').
card_uid('fountain watch'/'MMQ', 'MMQ:Fountain Watch:fountain watch').
card_rarity('fountain watch'/'MMQ', 'Rare').
card_artist('fountain watch'/'MMQ', 'Jeff Miracola').
card_number('fountain watch'/'MMQ', '19').
card_flavor_text('fountain watch'/'MMQ', 'The Cho-Arrim didn\'t rely only on a secret location to protect their Fountain.').
card_multiverse_id('fountain watch'/'MMQ', '19784').

card_in_set('fresh volunteers', 'MMQ').
card_original_type('fresh volunteers'/'MMQ', 'Creature — Rebel').
card_original_text('fresh volunteers'/'MMQ', '').
card_first_print('fresh volunteers', 'MMQ').
card_image_name('fresh volunteers'/'MMQ', 'fresh volunteers').
card_uid('fresh volunteers'/'MMQ', 'MMQ:Fresh Volunteers:fresh volunteers').
card_rarity('fresh volunteers'/'MMQ', 'Common').
card_artist('fresh volunteers'/'MMQ', 'Jeff Miracola').
card_number('fresh volunteers'/'MMQ', '20').
card_flavor_text('fresh volunteers'/'MMQ', 'Every Cho-Arrim villager is a potential warrior; when they are called, they abandon their peaceful way of life and take up arms to defend it.').
card_multiverse_id('fresh volunteers'/'MMQ', '19542').

card_in_set('furious assault', 'MMQ').
card_original_type('furious assault'/'MMQ', 'Enchantment').
card_original_text('furious assault'/'MMQ', 'Whenever you play a creature spell, Furious Assault deals 1 damage to target player.').
card_first_print('furious assault', 'MMQ').
card_image_name('furious assault'/'MMQ', 'furious assault').
card_uid('furious assault'/'MMQ', 'MMQ:Furious Assault:furious assault').
card_rarity('furious assault'/'MMQ', 'Common').
card_artist('furious assault'/'MMQ', 'Greg Staples').
card_number('furious assault'/'MMQ', '191').
card_flavor_text('furious assault'/'MMQ', '\"Does it burn, Saprazzan? Do you wish you had never raised arms against us?\"\n—Kyren overseer').
card_multiverse_id('furious assault'/'MMQ', '19848').

card_in_set('game preserve', 'MMQ').
card_original_type('game preserve'/'MMQ', 'Enchantment').
card_original_text('game preserve'/'MMQ', 'At the beginning of your upkeep, each player reveals the top card of his or her library. If all cards revealed this way are creature cards, put those cards into play under their owners\' control. (Otherwise, put them back face-down on top of their owners\' libraries.)').
card_first_print('game preserve', 'MMQ').
card_image_name('game preserve'/'MMQ', 'game preserve').
card_uid('game preserve'/'MMQ', 'MMQ:Game Preserve:game preserve').
card_rarity('game preserve'/'MMQ', 'Rare').
card_artist('game preserve'/'MMQ', 'Luca Zontini').
card_number('game preserve'/'MMQ', '248').
card_multiverse_id('game preserve'/'MMQ', '19742').

card_in_set('general\'s regalia', 'MMQ').
card_original_type('general\'s regalia'/'MMQ', 'Artifact').
card_original_text('general\'s regalia'/'MMQ', '{3}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to target creature you control instead.').
card_first_print('general\'s regalia', 'MMQ').
card_image_name('general\'s regalia'/'MMQ', 'general\'s regalia').
card_uid('general\'s regalia'/'MMQ', 'MMQ:General\'s Regalia:general\'s regalia').
card_rarity('general\'s regalia'/'MMQ', 'Rare').
card_artist('general\'s regalia'/'MMQ', 'David Monette').
card_number('general\'s regalia'/'MMQ', '295').
card_flavor_text('general\'s regalia'/'MMQ', 'A uniform fit for a goblin.').
card_multiverse_id('general\'s regalia'/'MMQ', '19753').

card_in_set('gerrard\'s irregulars', 'MMQ').
card_original_type('gerrard\'s irregulars'/'MMQ', 'Creature — Soldier').
card_original_text('gerrard\'s irregulars'/'MMQ', 'Trample; haste (This creature may attack and {T} the turn it comes under your control.)').
card_first_print('gerrard\'s irregulars', 'MMQ').
card_image_name('gerrard\'s irregulars'/'MMQ', 'gerrard\'s irregulars').
card_uid('gerrard\'s irregulars'/'MMQ', 'MMQ:Gerrard\'s Irregulars:gerrard\'s irregulars').
card_rarity('gerrard\'s irregulars'/'MMQ', 'Common').
card_artist('gerrard\'s irregulars'/'MMQ', 'Eric Peterson').
card_number('gerrard\'s irregulars'/'MMQ', '192').
card_flavor_text('gerrard\'s irregulars'/'MMQ', 'Had Gerrard realized he\'d end up fighting against them, he might not have trained them so well.').
card_multiverse_id('gerrard\'s irregulars'/'MMQ', '19842').

card_in_set('ghoul\'s feast', 'MMQ').
card_original_type('ghoul\'s feast'/'MMQ', 'Instant').
card_original_text('ghoul\'s feast'/'MMQ', 'Target creature gets +X/+0 until end of turn, where X is the number of creature cards in your graveyard.').
card_first_print('ghoul\'s feast', 'MMQ').
card_image_name('ghoul\'s feast'/'MMQ', 'ghoul\'s feast').
card_uid('ghoul\'s feast'/'MMQ', 'MMQ:Ghoul\'s Feast:ghoul\'s feast').
card_rarity('ghoul\'s feast'/'MMQ', 'Uncommon').
card_artist('ghoul\'s feast'/'MMQ', 'Alan Pollack').
card_number('ghoul\'s feast'/'MMQ', '137').
card_flavor_text('ghoul\'s feast'/'MMQ', 'Mercadians not wealthy enough to buy a tomb are thrown into a bog called \"the Ghoul\'s Larder.\"').
card_multiverse_id('ghoul\'s feast'/'MMQ', '19596').

card_in_set('giant caterpillar', 'MMQ').
card_original_type('giant caterpillar'/'MMQ', 'Creature — Insect').
card_original_text('giant caterpillar'/'MMQ', '{G}, Sacrifice Giant Caterpillar: Put a 1/1 green Butterfly creature token with flying into play at end of turn.').
card_image_name('giant caterpillar'/'MMQ', 'giant caterpillar').
card_uid('giant caterpillar'/'MMQ', 'MMQ:Giant Caterpillar:giant caterpillar').
card_rarity('giant caterpillar'/'MMQ', 'Common').
card_artist('giant caterpillar'/'MMQ', 'Arnie Swekel').
card_number('giant caterpillar'/'MMQ', '249').
card_multiverse_id('giant caterpillar'/'MMQ', '20847').

card_in_set('glowing anemone', 'MMQ').
card_original_type('glowing anemone'/'MMQ', 'Creature — Beast').
card_original_text('glowing anemone'/'MMQ', 'When Glowing Anemone comes into play, you may return target land to its owner\'s hand.').
card_first_print('glowing anemone', 'MMQ').
card_image_name('glowing anemone'/'MMQ', 'glowing anemone').
card_uid('glowing anemone'/'MMQ', 'MMQ:Glowing Anemone:glowing anemone').
card_rarity('glowing anemone'/'MMQ', 'Uncommon').
card_artist('glowing anemone'/'MMQ', 'Pete Venters').
card_number('glowing anemone'/'MMQ', '81').
card_flavor_text('glowing anemone'/'MMQ', 'Beautiful to behold, terrible to be held.').
card_multiverse_id('glowing anemone'/'MMQ', '19803').

card_in_set('groundskeeper', 'MMQ').
card_original_type('groundskeeper'/'MMQ', 'Creature — Druid').
card_original_text('groundskeeper'/'MMQ', '{1}{G} Return target basic land card from your graveyard to your hand.').
card_first_print('groundskeeper', 'MMQ').
card_image_name('groundskeeper'/'MMQ', 'groundskeeper').
card_uid('groundskeeper'/'MMQ', 'MMQ:Groundskeeper:groundskeeper').
card_rarity('groundskeeper'/'MMQ', 'Uncommon').
card_artist('groundskeeper'/'MMQ', 'Alan Rabinowitz').
card_number('groundskeeper'/'MMQ', '250').
card_flavor_text('groundskeeper'/'MMQ', 'Whereas the rebels fight to defend the land, they work to renew it.').
card_multiverse_id('groundskeeper'/'MMQ', '19863').

card_in_set('gush', 'MMQ').
card_original_type('gush'/'MMQ', 'Instant').
card_original_text('gush'/'MMQ', 'You may return two islands you control to their owner\'s hand instead of paying Gush\'s mana cost.\nDraw two cards.').
card_first_print('gush', 'MMQ').
card_image_name('gush'/'MMQ', 'gush').
card_uid('gush'/'MMQ', 'MMQ:Gush:gush').
card_rarity('gush'/'MMQ', 'Common').
card_artist('gush'/'MMQ', 'Kev Walker').
card_number('gush'/'MMQ', '82').
card_flavor_text('gush'/'MMQ', 'Don\'t trust your secrets to the sea.').
card_multiverse_id('gush'/'MMQ', '20404').

card_in_set('hammer mage', 'MMQ').
card_original_type('hammer mage'/'MMQ', 'Creature — Spellshaper').
card_original_text('hammer mage'/'MMQ', '{X}{R}, {T}, Discard a card from your hand: Destroy all artifacts with converted mana cost X or less.').
card_first_print('hammer mage', 'MMQ').
card_image_name('hammer mage'/'MMQ', 'hammer mage').
card_uid('hammer mage'/'MMQ', 'MMQ:Hammer Mage:hammer mage').
card_rarity('hammer mage'/'MMQ', 'Uncommon').
card_artist('hammer mage'/'MMQ', 'Rebecca Guay').
card_number('hammer mage'/'MMQ', '193').
card_flavor_text('hammer mage'/'MMQ', 'When he\'s holding the hammers, everything looks like a nail.').
card_multiverse_id('hammer mage'/'MMQ', '19708').

card_in_set('haunted crossroads', 'MMQ').
card_original_type('haunted crossroads'/'MMQ', 'Enchantment').
card_original_text('haunted crossroads'/'MMQ', '{B} Put target creature card from your graveyard on top of your library.').
card_first_print('haunted crossroads', 'MMQ').
card_image_name('haunted crossroads'/'MMQ', 'haunted crossroads').
card_uid('haunted crossroads'/'MMQ', 'MMQ:Haunted Crossroads:haunted crossroads').
card_rarity('haunted crossroads'/'MMQ', 'Uncommon').
card_artist('haunted crossroads'/'MMQ', 'Carl Critchlow').
card_number('haunted crossroads'/'MMQ', '138').
card_flavor_text('haunted crossroads'/'MMQ', 'It seemed as if no one wanted them to reach the Henge of Ramos—not even the dead.').
card_multiverse_id('haunted crossroads'/'MMQ', '19704').

card_in_set('heart of ramos', 'MMQ').
card_original_type('heart of ramos'/'MMQ', 'Artifact').
card_original_text('heart of ramos'/'MMQ', '{T}: Add one red mana to your mana pool. \nSacrifice Heart of Ramos: Add one red mana to your mana pool.').
card_first_print('heart of ramos', 'MMQ').
card_image_name('heart of ramos'/'MMQ', 'heart of ramos').
card_uid('heart of ramos'/'MMQ', 'MMQ:Heart of Ramos:heart of ramos').
card_rarity('heart of ramos'/'MMQ', 'Rare').
card_artist('heart of ramos'/'MMQ', 'David Martin').
card_number('heart of ramos'/'MMQ', '296').
card_flavor_text('heart of ramos'/'MMQ', 'Ramos bled, and there was fire.').
card_multiverse_id('heart of ramos'/'MMQ', '19766').

card_in_set('henge guardian', 'MMQ').
card_original_type('henge guardian'/'MMQ', 'Artifact Creature').
card_original_text('henge guardian'/'MMQ', '{2}: Henge Guardian gains trample until end of turn.').
card_first_print('henge guardian', 'MMQ').
card_image_name('henge guardian'/'MMQ', 'henge guardian').
card_uid('henge guardian'/'MMQ', 'MMQ:Henge Guardian:henge guardian').
card_rarity('henge guardian'/'MMQ', 'Uncommon').
card_artist('henge guardian'/'MMQ', 'Chippy').
card_number('henge guardian'/'MMQ', '297').
card_flavor_text('henge guardian'/'MMQ', 'Like so many Thran relics, the wurm engine kept operating long after its creators were gone.').
card_multiverse_id('henge guardian'/'MMQ', '19770').

card_in_set('henge of ramos', 'MMQ').
card_original_type('henge of ramos'/'MMQ', 'Land').
card_original_text('henge of ramos'/'MMQ', '{T}: Add one colorless mana to your mana pool.\n{2}, {T}: Add one mana of any color to your mana pool.').
card_first_print('henge of ramos', 'MMQ').
card_image_name('henge of ramos'/'MMQ', 'henge of ramos').
card_uid('henge of ramos'/'MMQ', 'MMQ:Henge of Ramos:henge of ramos').
card_rarity('henge of ramos'/'MMQ', 'Uncommon').
card_artist('henge of ramos'/'MMQ', 'Edward P. Beard, Jr.').
card_number('henge of ramos'/'MMQ', '318').
card_flavor_text('henge of ramos'/'MMQ', 'It is the hub. We are the wheel.\n—Dryad saying').
card_multiverse_id('henge of ramos'/'MMQ', '19768').

card_in_set('hickory woodlot', 'MMQ').
card_original_type('hickory woodlot'/'MMQ', 'Land').
card_original_text('hickory woodlot'/'MMQ', 'Hickory Woodlot comes into play tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Hickory Woodlot: Add two green mana to your mana pool. If there are no depletion counters on Hickory Woodlot, sacrifice it.').
card_first_print('hickory woodlot', 'MMQ').
card_image_name('hickory woodlot'/'MMQ', 'hickory woodlot').
card_uid('hickory woodlot'/'MMQ', 'MMQ:Hickory Woodlot:hickory woodlot').
card_rarity('hickory woodlot'/'MMQ', 'Common').
card_artist('hickory woodlot'/'MMQ', 'Sean McConnell').
card_number('hickory woodlot'/'MMQ', '319').
card_multiverse_id('hickory woodlot'/'MMQ', '19646').

card_in_set('high market', 'MMQ').
card_original_type('high market'/'MMQ', 'Land').
card_original_text('high market'/'MMQ', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice a creature: You gain 1 life.').
card_first_print('high market', 'MMQ').
card_image_name('high market'/'MMQ', 'high market').
card_uid('high market'/'MMQ', 'MMQ:High Market:high market').
card_rarity('high market'/'MMQ', 'Rare').
card_artist('high market'/'MMQ', 'Carl Critchlow').
card_number('high market'/'MMQ', '320').
card_flavor_text('high market'/'MMQ', 'If it can\'t be had here, it can\'t be had on any world.').
card_multiverse_id('high market'/'MMQ', '19890').

card_in_set('high seas', 'MMQ').
card_original_type('high seas'/'MMQ', 'Enchantment').
card_original_text('high seas'/'MMQ', 'Red creature spells and green creature spells cost {1} more to play.').
card_first_print('high seas', 'MMQ').
card_image_name('high seas'/'MMQ', 'high seas').
card_uid('high seas'/'MMQ', 'MMQ:High Seas:high seas').
card_rarity('high seas'/'MMQ', 'Uncommon').
card_artist('high seas'/'MMQ', 'Massimilano Frezzato').
card_number('high seas'/'MMQ', '83').
card_flavor_text('high seas'/'MMQ', 'Bend as the wave breaks or it will break you as well.\n—Saprazzan saying').
card_multiverse_id('high seas'/'MMQ', '19814').

card_in_set('highway robber', 'MMQ').
card_original_type('highway robber'/'MMQ', 'Creature — Mercenary').
card_original_text('highway robber'/'MMQ', 'When Highway Robber comes into play, you gain 2 life and target opponent loses 2 life.').
card_first_print('highway robber', 'MMQ').
card_image_name('highway robber'/'MMQ', 'highway robber').
card_uid('highway robber'/'MMQ', 'MMQ:Highway Robber:highway robber').
card_rarity('highway robber'/'MMQ', 'Common').
card_artist('highway robber'/'MMQ', 'Kev Walker').
card_number('highway robber'/'MMQ', '139').
card_flavor_text('highway robber'/'MMQ', '\"Tonight, madam, it\'s your money and your life.\"').
card_multiverse_id('highway robber'/'MMQ', '19585').

card_in_set('hired giant', 'MMQ').
card_original_type('hired giant'/'MMQ', 'Creature — Giant').
card_original_text('hired giant'/'MMQ', 'When Hired Giant comes into play, each other player may search his or her library for a land card, put that card into play, then shuffle that library.').
card_first_print('hired giant', 'MMQ').
card_image_name('hired giant'/'MMQ', 'hired giant').
card_uid('hired giant'/'MMQ', 'MMQ:Hired Giant:hired giant').
card_rarity('hired giant'/'MMQ', 'Uncommon').
card_artist('hired giant'/'MMQ', 'Ben Thompson').
card_number('hired giant'/'MMQ', '194').
card_multiverse_id('hired giant'/'MMQ', '19838').

card_in_set('honor the fallen', 'MMQ').
card_original_type('honor the fallen'/'MMQ', 'Instant').
card_original_text('honor the fallen'/'MMQ', 'Remove all creature cards in all graveyards from the game. You gain 1 life for each card removed this way.').
card_first_print('honor the fallen', 'MMQ').
card_image_name('honor the fallen'/'MMQ', 'honor the fallen').
card_uid('honor the fallen'/'MMQ', 'MMQ:Honor the Fallen:honor the fallen').
card_rarity('honor the fallen'/'MMQ', 'Rare').
card_artist('honor the fallen'/'MMQ', 'Terese Nielsen').
card_number('honor the fallen'/'MMQ', '21').
card_flavor_text('honor the fallen'/'MMQ', '\"May the River claim Cho-Manno\'s body, wherever it lies.\"\n—Ta-Karnst, Cho-Arrim healer').
card_multiverse_id('honor the fallen'/'MMQ', '19787').

card_in_set('hoodwink', 'MMQ').
card_original_type('hoodwink'/'MMQ', 'Instant').
card_original_text('hoodwink'/'MMQ', 'Return target artifact, enchantment, or land to its owner\'s hand.').
card_first_print('hoodwink', 'MMQ').
card_image_name('hoodwink'/'MMQ', 'hoodwink').
card_uid('hoodwink'/'MMQ', 'MMQ:Hoodwink:hoodwink').
card_rarity('hoodwink'/'MMQ', 'Common').
card_artist('hoodwink'/'MMQ', 'Arnie Swekel').
card_number('hoodwink'/'MMQ', '84').
card_flavor_text('hoodwink'/'MMQ', 'The youths of Rishada\'s marketplace are always ready for a quick footrace.').
card_multiverse_id('hoodwink'/'MMQ', '20454').

card_in_set('horn of plenty', 'MMQ').
card_original_type('horn of plenty'/'MMQ', 'Artifact').
card_original_text('horn of plenty'/'MMQ', 'Whenever a player plays a spell, he or she may pay {1}. If that player does, he or she draws a card at end of turn.').
card_first_print('horn of plenty', 'MMQ').
card_image_name('horn of plenty'/'MMQ', 'horn of plenty').
card_uid('horn of plenty'/'MMQ', 'MMQ:Horn of Plenty:horn of plenty').
card_rarity('horn of plenty'/'MMQ', 'Rare').
card_artist('horn of plenty'/'MMQ', 'Brian Despain').
card_number('horn of plenty'/'MMQ', '298').
card_flavor_text('horn of plenty'/'MMQ', '\"This horn is an exact replica of the fabled Horn of Ramos, except mine is more lavish.\"\n—Mercadian magistrate').
card_multiverse_id('horn of plenty'/'MMQ', '19755').

card_in_set('horn of ramos', 'MMQ').
card_original_type('horn of ramos'/'MMQ', 'Artifact').
card_original_text('horn of ramos'/'MMQ', '{T}: Add one green mana to your mana pool.\nSacrifice Horn of Ramos: Add one green mana to your mana pool.').
card_first_print('horn of ramos', 'MMQ').
card_image_name('horn of ramos'/'MMQ', 'horn of ramos').
card_uid('horn of ramos'/'MMQ', 'MMQ:Horn of Ramos:horn of ramos').
card_rarity('horn of ramos'/'MMQ', 'Rare').
card_artist('horn of ramos'/'MMQ', 'David Martin').
card_number('horn of ramos'/'MMQ', '299').
card_flavor_text('horn of ramos'/'MMQ', 'Ramos touched, and there was life.').
card_multiverse_id('horn of ramos'/'MMQ', '19763').

card_in_set('horned troll', 'MMQ').
card_original_type('horned troll'/'MMQ', 'Creature — Troll').
card_original_text('horned troll'/'MMQ', '{G} Regenerate Horned Troll.').
card_first_print('horned troll', 'MMQ').
card_image_name('horned troll'/'MMQ', 'horned troll').
card_uid('horned troll'/'MMQ', 'MMQ:Horned Troll:horned troll').
card_rarity('horned troll'/'MMQ', 'Common').
card_artist('horned troll'/'MMQ', 'Heather Hudson').
card_number('horned troll'/'MMQ', '251').
card_flavor_text('horned troll'/'MMQ', '\"If I says it\'s a troll, it\'s a troll. Goblins is always right in Mercadia.\"\n—Squee').
card_multiverse_id('horned troll'/'MMQ', '19630').

card_in_set('howling wolf', 'MMQ').
card_original_type('howling wolf'/'MMQ', 'Creature — Wolf').
card_original_text('howling wolf'/'MMQ', 'When Howling Wolf comes into play, you may search your library for up to three Howling Wolf cards, reveal them, and put them into your hand. If you do, shuffle your library.').
card_first_print('howling wolf', 'MMQ').
card_image_name('howling wolf'/'MMQ', 'howling wolf').
card_uid('howling wolf'/'MMQ', 'MMQ:Howling Wolf:howling wolf').
card_rarity('howling wolf'/'MMQ', 'Common').
card_artist('howling wolf'/'MMQ', 'Heather Hudson').
card_number('howling wolf'/'MMQ', '252').
card_multiverse_id('howling wolf'/'MMQ', '19632').

card_in_set('hunted wumpus', 'MMQ').
card_original_type('hunted wumpus'/'MMQ', 'Creature — Beast').
card_original_text('hunted wumpus'/'MMQ', 'When Hunted Wumpus comes into play, each other player may put a creature card from his or her hand into play under his or her control.').
card_first_print('hunted wumpus', 'MMQ').
card_image_name('hunted wumpus'/'MMQ', 'hunted wumpus').
card_uid('hunted wumpus'/'MMQ', 'MMQ:Hunted Wumpus:hunted wumpus').
card_rarity('hunted wumpus'/'MMQ', 'Uncommon').
card_artist('hunted wumpus'/'MMQ', 'Brian Snõddy').
card_number('hunted wumpus'/'MMQ', '253').
card_flavor_text('hunted wumpus'/'MMQ', 'Just one can feed a dozen people for a month.').
card_multiverse_id('hunted wumpus'/'MMQ', '19857').

card_in_set('ignoble soldier', 'MMQ').
card_original_type('ignoble soldier'/'MMQ', 'Creature — Soldier').
card_original_text('ignoble soldier'/'MMQ', 'Whenever Ignoble Soldier becomes blocked, prevent all combat damage that would be dealt by it this turn.').
card_first_print('ignoble soldier', 'MMQ').
card_image_name('ignoble soldier'/'MMQ', 'ignoble soldier').
card_uid('ignoble soldier'/'MMQ', 'MMQ:Ignoble Soldier:ignoble soldier').
card_rarity('ignoble soldier'/'MMQ', 'Uncommon').
card_artist('ignoble soldier'/'MMQ', 'Mark Romanoski').
card_number('ignoble soldier'/'MMQ', '22').
card_flavor_text('ignoble soldier'/'MMQ', 'Cowardice is a term invented by those with no cunning.').
card_multiverse_id('ignoble soldier'/'MMQ', '19778').

card_in_set('indentured djinn', 'MMQ').
card_original_type('indentured djinn'/'MMQ', 'Creature — Djinn').
card_original_text('indentured djinn'/'MMQ', 'Flying\nWhen Indentured Djinn comes into play, each other player may draw up to three cards.').
card_first_print('indentured djinn', 'MMQ').
card_image_name('indentured djinn'/'MMQ', 'indentured djinn').
card_uid('indentured djinn'/'MMQ', 'MMQ:Indentured Djinn:indentured djinn').
card_rarity('indentured djinn'/'MMQ', 'Uncommon').
card_artist('indentured djinn'/'MMQ', 'Val Mayerik').
card_number('indentured djinn'/'MMQ', '85').
card_flavor_text('indentured djinn'/'MMQ', 'It\'s not always clear who\'s the master and who\'s the servant.').
card_multiverse_id('indentured djinn'/'MMQ', '19797').

card_in_set('instigator', 'MMQ').
card_original_type('instigator'/'MMQ', 'Creature — Spellshaper').
card_original_text('instigator'/'MMQ', '{1}{B}{B}, {T}, Discard a card from your hand: Creatures target player controls attack this turn if able.').
card_first_print('instigator', 'MMQ').
card_image_name('instigator'/'MMQ', 'instigator').
card_uid('instigator'/'MMQ', 'MMQ:Instigator:instigator').
card_rarity('instigator'/'MMQ', 'Rare').
card_artist('instigator'/'MMQ', 'Fred Fields').
card_number('instigator'/'MMQ', '140').
card_flavor_text('instigator'/'MMQ', 'The desecration of their dead makes even the Saprazzans careless.').
card_multiverse_id('instigator'/'MMQ', '19699').

card_in_set('insubordination', 'MMQ').
card_original_type('insubordination'/'MMQ', 'Enchant Creature').
card_original_text('insubordination'/'MMQ', 'At the end of the turn of enchanted creature\'s controller, Insubordination deals 2 damage to that player unless enchanted creature attacked this turn.').
card_first_print('insubordination', 'MMQ').
card_image_name('insubordination'/'MMQ', 'insubordination').
card_uid('insubordination'/'MMQ', 'MMQ:Insubordination:insubordination').
card_rarity('insubordination'/'MMQ', 'Common').
card_artist('insubordination'/'MMQ', 'Andrew Goldhawk').
card_number('insubordination'/'MMQ', '141').
card_multiverse_id('insubordination'/'MMQ', '19590').

card_in_set('intimidation', 'MMQ').
card_original_type('intimidation'/'MMQ', 'Enchantment').
card_original_text('intimidation'/'MMQ', 'Creatures you control can\'t be blocked except by artifact creatures and black creatures.').
card_first_print('intimidation', 'MMQ').
card_image_name('intimidation'/'MMQ', 'intimidation').
card_uid('intimidation'/'MMQ', 'MMQ:Intimidation:intimidation').
card_rarity('intimidation'/'MMQ', 'Uncommon').
card_artist('intimidation'/'MMQ', 'Terese Nielsen').
card_number('intimidation'/'MMQ', '142').
card_flavor_text('intimidation'/'MMQ', '\"If they move, kill them. In fact, kill one now to make sure the other understands.\"').
card_multiverse_id('intimidation'/'MMQ', '19833').

card_in_set('invigorate', 'MMQ').
card_original_type('invigorate'/'MMQ', 'Instant').
card_original_text('invigorate'/'MMQ', 'If you control a forest, you may have an opponent gain 3 life instead of paying Invigorate\'s mana cost.\nTarget creature gets +4/+4 until end of turn.').
card_first_print('invigorate', 'MMQ').
card_image_name('invigorate'/'MMQ', 'invigorate').
card_uid('invigorate'/'MMQ', 'MMQ:Invigorate:invigorate').
card_rarity('invigorate'/'MMQ', 'Common').
card_artist('invigorate'/'MMQ', 'Dan Frazier').
card_number('invigorate'/'MMQ', '254').
card_multiverse_id('invigorate'/'MMQ', '19640').

card_in_set('inviolability', 'MMQ').
card_original_type('inviolability'/'MMQ', 'Enchant Creature').
card_original_text('inviolability'/'MMQ', 'Prevent all damage that would be dealt to enchanted creature.').
card_first_print('inviolability', 'MMQ').
card_image_name('inviolability'/'MMQ', 'inviolability').
card_uid('inviolability'/'MMQ', 'MMQ:Inviolability:inviolability').
card_rarity('inviolability'/'MMQ', 'Common').
card_artist('inviolability'/'MMQ', 'DiTerlizzi').
card_number('inviolability'/'MMQ', '23').
card_flavor_text('inviolability'/'MMQ', 'A soldier\'s faith is magic strong enough to protect against any blade.').
card_multiverse_id('inviolability'/'MMQ', '19546').

card_in_set('iron lance', 'MMQ').
card_original_type('iron lance'/'MMQ', 'Artifact').
card_original_text('iron lance'/'MMQ', '{3}, {T}: Target creature gains first strike until end of turn.').
card_first_print('iron lance', 'MMQ').
card_image_name('iron lance'/'MMQ', 'iron lance').
card_uid('iron lance'/'MMQ', 'MMQ:Iron Lance:iron lance').
card_rarity('iron lance'/'MMQ', 'Uncommon').
card_artist('iron lance'/'MMQ', 'Scott M. Fischer').
card_number('iron lance'/'MMQ', '300').
card_flavor_text('iron lance'/'MMQ', '\"The only way to get Mercadians to fight on the front lines is to give them really long weapons.\"\n—Gerrard').
card_multiverse_id('iron lance'/'MMQ', '19882').

card_in_set('island', 'MMQ').
card_original_type('island'/'MMQ', 'Land').
card_original_text('island'/'MMQ', 'U').
card_image_name('island'/'MMQ', 'island1').
card_uid('island'/'MMQ', 'MMQ:Island:island1').
card_rarity('island'/'MMQ', 'Basic Land').
card_artist('island'/'MMQ', 'Terry Springer').
card_number('island'/'MMQ', '335').
card_multiverse_id('island'/'MMQ', '20884').

card_in_set('island', 'MMQ').
card_original_type('island'/'MMQ', 'Land').
card_original_text('island'/'MMQ', 'U').
card_image_name('island'/'MMQ', 'island2').
card_uid('island'/'MMQ', 'MMQ:Island:island2').
card_rarity('island'/'MMQ', 'Basic Land').
card_artist('island'/'MMQ', 'Scott Bailey').
card_number('island'/'MMQ', '336').
card_multiverse_id('island'/'MMQ', '20885').

card_in_set('island', 'MMQ').
card_original_type('island'/'MMQ', 'Land').
card_original_text('island'/'MMQ', 'U').
card_image_name('island'/'MMQ', 'island3').
card_uid('island'/'MMQ', 'MMQ:Island:island3').
card_rarity('island'/'MMQ', 'Basic Land').
card_artist('island'/'MMQ', 'Scott Bailey').
card_number('island'/'MMQ', '337').
card_multiverse_id('island'/'MMQ', '20886').

card_in_set('island', 'MMQ').
card_original_type('island'/'MMQ', 'Land').
card_original_text('island'/'MMQ', 'U').
card_image_name('island'/'MMQ', 'island4').
card_uid('island'/'MMQ', 'MMQ:Island:island4').
card_rarity('island'/'MMQ', 'Basic Land').
card_artist('island'/'MMQ', 'Tony Szczudlo').
card_number('island'/'MMQ', '338').
card_multiverse_id('island'/'MMQ', '20887').

card_in_set('ivory mask', 'MMQ').
card_original_type('ivory mask'/'MMQ', 'Enchantment').
card_original_text('ivory mask'/'MMQ', 'You can\'t be the target of spells or abilities.').
card_first_print('ivory mask', 'MMQ').
card_image_name('ivory mask'/'MMQ', 'ivory mask').
card_uid('ivory mask'/'MMQ', 'MMQ:Ivory Mask:ivory mask').
card_rarity('ivory mask'/'MMQ', 'Rare').
card_artist('ivory mask'/'MMQ', 'Glen Angus').
card_number('ivory mask'/'MMQ', '24').
card_flavor_text('ivory mask'/'MMQ', '\"I did not choose my role, Orim,\" said Cho-Manno patiently, \"and I suspect Gerrard did not choose his.\"').
card_multiverse_id('ivory mask'/'MMQ', '19656').

card_in_set('jeweled torque', 'MMQ').
card_original_type('jeweled torque'/'MMQ', 'Artifact').
card_original_text('jeweled torque'/'MMQ', 'As Jeweled Torque comes into play, choose a color.\nWhenever a player plays a spell of the chosen color, you may pay {2}. If you do, you gain 2 life.').
card_first_print('jeweled torque', 'MMQ').
card_image_name('jeweled torque'/'MMQ', 'jeweled torque').
card_uid('jeweled torque'/'MMQ', 'MMQ:Jeweled Torque:jeweled torque').
card_rarity('jeweled torque'/'MMQ', 'Uncommon').
card_artist('jeweled torque'/'MMQ', 'Mark Zug').
card_number('jeweled torque'/'MMQ', '301').
card_multiverse_id('jeweled torque'/'MMQ', '19884').

card_in_set('jhovall queen', 'MMQ').
card_original_type('jhovall queen'/'MMQ', 'Creature — Rebel').
card_original_text('jhovall queen'/'MMQ', 'Attacking doesn\'t cause Jhovall Queen to tap.').
card_first_print('jhovall queen', 'MMQ').
card_image_name('jhovall queen'/'MMQ', 'jhovall queen').
card_uid('jhovall queen'/'MMQ', 'MMQ:Jhovall Queen:jhovall queen').
card_rarity('jhovall queen'/'MMQ', 'Rare').
card_artist('jhovall queen'/'MMQ', 'Michael Sutfin').
card_number('jhovall queen'/'MMQ', '25').
card_flavor_text('jhovall queen'/'MMQ', 'War-trained jhovalls eat twice their weight in war-trained soldiers daily.').
card_multiverse_id('jhovall queen'/'MMQ', '19654').

card_in_set('jhovall rider', 'MMQ').
card_original_type('jhovall rider'/'MMQ', 'Creature — Rebel').
card_original_text('jhovall rider'/'MMQ', 'Trample').
card_first_print('jhovall rider', 'MMQ').
card_image_name('jhovall rider'/'MMQ', 'jhovall rider').
card_uid('jhovall rider'/'MMQ', 'MMQ:Jhovall Rider:jhovall rider').
card_rarity('jhovall rider'/'MMQ', 'Uncommon').
card_artist('jhovall rider'/'MMQ', 'Scott M. Fischer').
card_number('jhovall rider'/'MMQ', '26').
card_flavor_text('jhovall rider'/'MMQ', 'Don\'t be fooled by the riders\' fluid grace—it takes years of practice to ride these beasts.').
card_multiverse_id('jhovall rider'/'MMQ', '20172').

card_in_set('karn\'s touch', 'MMQ').
card_original_type('karn\'s touch'/'MMQ', 'Instant').
card_original_text('karn\'s touch'/'MMQ', 'Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn. (It retains its abilities.)').
card_first_print('karn\'s touch', 'MMQ').
card_image_name('karn\'s touch'/'MMQ', 'karn\'s touch').
card_uid('karn\'s touch'/'MMQ', 'MMQ:Karn\'s Touch:karn\'s touch').
card_rarity('karn\'s touch'/'MMQ', 'Rare').
card_artist('karn\'s touch'/'MMQ', 'Alan Pollack').
card_number('karn\'s touch'/'MMQ', '86').
card_multiverse_id('karn\'s touch'/'MMQ', '19573').

card_in_set('kris mage', 'MMQ').
card_original_type('kris mage'/'MMQ', 'Creature — Spellshaper').
card_original_text('kris mage'/'MMQ', '{R}, {T}, Discard a card from your hand: Kris Mage deals 1 damage to target creature or player.').
card_first_print('kris mage', 'MMQ').
card_image_name('kris mage'/'MMQ', 'kris mage').
card_uid('kris mage'/'MMQ', 'MMQ:Kris Mage:kris mage').
card_rarity('kris mage'/'MMQ', 'Common').
card_artist('kris mage'/'MMQ', 'Matthew D. Wilson').
card_number('kris mage'/'MMQ', '195').
card_flavor_text('kris mage'/'MMQ', 'Her blade draws blood without ever touching its target.').
card_multiverse_id('kris mage'/'MMQ', '19836').

card_in_set('kyren archive', 'MMQ').
card_original_type('kyren archive'/'MMQ', 'Artifact').
card_original_text('kyren archive'/'MMQ', 'At the beginning of your upkeep, you may remove the top card of your library from the game face down.\n{5}, Discard your hand, Sacrifice Kyren Archive: Put all cards removed from the game with Kyren Archive into their owner\'s hand.').
card_first_print('kyren archive', 'MMQ').
card_image_name('kyren archive'/'MMQ', 'kyren archive').
card_uid('kyren archive'/'MMQ', 'MMQ:Kyren Archive:kyren archive').
card_rarity('kyren archive'/'MMQ', 'Rare').
card_artist('kyren archive'/'MMQ', 'Roger Raupp').
card_number('kyren archive'/'MMQ', '302').
card_multiverse_id('kyren archive'/'MMQ', '19756').

card_in_set('kyren glider', 'MMQ').
card_original_type('kyren glider'/'MMQ', 'Creature — Goblin').
card_original_text('kyren glider'/'MMQ', 'Flying\nKyren Glider can\'t block.').
card_first_print('kyren glider', 'MMQ').
card_image_name('kyren glider'/'MMQ', 'kyren glider').
card_uid('kyren glider'/'MMQ', 'MMQ:Kyren Glider:kyren glider').
card_rarity('kyren glider'/'MMQ', 'Common').
card_artist('kyren glider'/'MMQ', 'Daren Bader').
card_number('kyren glider'/'MMQ', '196').
card_flavor_text('kyren glider'/'MMQ', 'Mercadia\'s Kyren goblins are the opposite of Dominarian goblins: they\'re smart and cowardly.').
card_multiverse_id('kyren glider'/'MMQ', '19604').

card_in_set('kyren legate', 'MMQ').
card_original_type('kyren legate'/'MMQ', 'Creature — Goblin').
card_original_text('kyren legate'/'MMQ', 'If an opponent controls a plains and you control a mountain, you may play Kyren Legate without paying its mana cost.\nHaste (This creature may attack and {T} the turn it comes under your control.)').
card_first_print('kyren legate', 'MMQ').
card_image_name('kyren legate'/'MMQ', 'kyren legate').
card_uid('kyren legate'/'MMQ', 'MMQ:Kyren Legate:kyren legate').
card_rarity('kyren legate'/'MMQ', 'Uncommon').
card_artist('kyren legate'/'MMQ', 'Dave Dorman').
card_number('kyren legate'/'MMQ', '197').
card_multiverse_id('kyren legate'/'MMQ', '19841').

card_in_set('kyren negotiations', 'MMQ').
card_original_type('kyren negotiations'/'MMQ', 'Enchantment').
card_original_text('kyren negotiations'/'MMQ', 'Tap an untapped creature you control: Kyren Negotiations deals 1 damage to target player.').
card_first_print('kyren negotiations', 'MMQ').
card_image_name('kyren negotiations'/'MMQ', 'kyren negotiations').
card_uid('kyren negotiations'/'MMQ', 'MMQ:Kyren Negotiations:kyren negotiations').
card_rarity('kyren negotiations'/'MMQ', 'Uncommon').
card_artist('kyren negotiations'/'MMQ', 'Scott Hampton').
card_number('kyren negotiations'/'MMQ', '198').
card_flavor_text('kyren negotiations'/'MMQ', 'Kyren goblins always speak in questions and never allow wrong answers.').
card_multiverse_id('kyren negotiations'/'MMQ', '19846').

card_in_set('kyren sniper', 'MMQ').
card_original_type('kyren sniper'/'MMQ', 'Creature — Goblin').
card_original_text('kyren sniper'/'MMQ', 'At the beginning of your upkeep, you may have Kyren Sniper deal 1 damage to target player.').
card_first_print('kyren sniper', 'MMQ').
card_image_name('kyren sniper'/'MMQ', 'kyren sniper').
card_uid('kyren sniper'/'MMQ', 'MMQ:Kyren Sniper:kyren sniper').
card_rarity('kyren sniper'/'MMQ', 'Common').
card_artist('kyren sniper'/'MMQ', 'Carl Critchlow').
card_number('kyren sniper'/'MMQ', '199').
card_flavor_text('kyren sniper'/'MMQ', 'Kyren goblins always act behind the scenes—even in battle.').
card_multiverse_id('kyren sniper'/'MMQ', '19606').

card_in_set('kyren toy', 'MMQ').
card_original_type('kyren toy'/'MMQ', 'Artifact').
card_original_text('kyren toy'/'MMQ', '{1}, {T}: Put a charge counter on Kyren Toy.\n{T}, Remove X charge counters from Kyren Toy: Add X plus one colorless mana to your mana pool.').
card_first_print('kyren toy', 'MMQ').
card_image_name('kyren toy'/'MMQ', 'kyren toy').
card_uid('kyren toy'/'MMQ', 'MMQ:Kyren Toy:kyren toy').
card_rarity('kyren toy'/'MMQ', 'Rare').
card_artist('kyren toy'/'MMQ', 'Arnie Swekel').
card_number('kyren toy'/'MMQ', '303').
card_multiverse_id('kyren toy'/'MMQ', '19759').

card_in_set('land grant', 'MMQ').
card_original_type('land grant'/'MMQ', 'Sorcery').
card_original_text('land grant'/'MMQ', 'If you have no land cards in hand, you may reveal your hand instead of paying Land Grant\'s mana cost.\nSearch your library for a forest card and put that card into your hand. Then shuffle your library.').
card_first_print('land grant', 'MMQ').
card_image_name('land grant'/'MMQ', 'land grant').
card_uid('land grant'/'MMQ', 'MMQ:Land Grant:land grant').
card_rarity('land grant'/'MMQ', 'Common').
card_artist('land grant'/'MMQ', 'D. Alexander Gregory').
card_number('land grant'/'MMQ', '255').
card_multiverse_id('land grant'/'MMQ', '19633').

card_in_set('larceny', 'MMQ').
card_original_type('larceny'/'MMQ', 'Enchantment').
card_original_text('larceny'/'MMQ', 'Whenever a creature you control deals combat damage to a player, that player discards a card from his or her hand.').
card_first_print('larceny', 'MMQ').
card_image_name('larceny'/'MMQ', 'larceny').
card_uid('larceny'/'MMQ', 'MMQ:Larceny:larceny').
card_rarity('larceny'/'MMQ', 'Uncommon').
card_artist('larceny'/'MMQ', 'Dave Dorman').
card_number('larceny'/'MMQ', '143').
card_multiverse_id('larceny'/'MMQ', '19825').

card_in_set('last breath', 'MMQ').
card_original_type('last breath'/'MMQ', 'Instant').
card_original_text('last breath'/'MMQ', 'Remove target creature with power 2 or less from the game. Its controller gains 4 life.').
card_first_print('last breath', 'MMQ').
card_image_name('last breath'/'MMQ', 'last breath').
card_uid('last breath'/'MMQ', 'MMQ:Last Breath:last breath').
card_rarity('last breath'/'MMQ', 'Uncommon').
card_artist('last breath'/'MMQ', 'DiTerlizzi').
card_number('last breath'/'MMQ', '27').
card_flavor_text('last breath'/'MMQ', '\"The River flows. My soul returns.\"').
card_multiverse_id('last breath'/'MMQ', '136513').

card_in_set('lava runner', 'MMQ').
card_original_type('lava runner'/'MMQ', 'Creature — Lizard').
card_original_text('lava runner'/'MMQ', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nWhenever Lava Runner becomes the target of a spell or ability, that spell or ability\'s controller sacrifices a land.').
card_first_print('lava runner', 'MMQ').
card_image_name('lava runner'/'MMQ', 'lava runner').
card_uid('lava runner'/'MMQ', 'MMQ:Lava Runner:lava runner').
card_rarity('lava runner'/'MMQ', 'Rare').
card_artist('lava runner'/'MMQ', 'Donato Giancola').
card_number('lava runner'/'MMQ', '200').
card_multiverse_id('lava runner'/'MMQ', '19845').

card_in_set('ley line', 'MMQ').
card_original_type('ley line'/'MMQ', 'Enchantment').
card_original_text('ley line'/'MMQ', 'At the beginning of each player\'s upkeep, that player may put a +1/+1 counter on target creature.').
card_first_print('ley line', 'MMQ').
card_image_name('ley line'/'MMQ', 'ley line').
card_uid('ley line'/'MMQ', 'MMQ:Ley Line:ley line').
card_rarity('ley line'/'MMQ', 'Uncommon').
card_artist('ley line'/'MMQ', 'Terese Nielsen').
card_number('ley line'/'MMQ', '256').
card_flavor_text('ley line'/'MMQ', '\"I can feel the strength of the entire forest.\"\n—Gerrard').
card_multiverse_id('ley line'/'MMQ', '19867').

card_in_set('liability', 'MMQ').
card_original_type('liability'/'MMQ', 'Enchantment').
card_original_text('liability'/'MMQ', 'Whenever a card is put into a player\'s graveyard from play, that player loses 1 life.').
card_first_print('liability', 'MMQ').
card_image_name('liability'/'MMQ', 'liability').
card_uid('liability'/'MMQ', 'MMQ:Liability:liability').
card_rarity('liability'/'MMQ', 'Rare').
card_artist('liability'/'MMQ', 'Christopher Moeller').
card_number('liability'/'MMQ', '144').
card_flavor_text('liability'/'MMQ', 'Gerrard realized that the helpless woman they\'d rescued from Rath was nothing of the sort.').
card_multiverse_id('liability'/'MMQ', '19706').

card_in_set('lightning hounds', 'MMQ').
card_original_type('lightning hounds'/'MMQ', 'Creature — Hound').
card_original_text('lightning hounds'/'MMQ', 'First strike').
card_image_name('lightning hounds'/'MMQ', 'lightning hounds').
card_uid('lightning hounds'/'MMQ', 'MMQ:Lightning Hounds:lightning hounds').
card_rarity('lightning hounds'/'MMQ', 'Common').
card_artist('lightning hounds'/'MMQ', 'Andrew Robinson').
card_number('lightning hounds'/'MMQ', '201').
card_flavor_text('lightning hounds'/'MMQ', 'Quick enough to avoid jhovalls when alone and fierce enough to attack them in packs, the hounds were as at home in the mountains as the Mercadians were atop theirs.').
card_multiverse_id('lightning hounds'/'MMQ', '19618').

card_in_set('lithophage', 'MMQ').
card_original_type('lithophage'/'MMQ', 'Creature — Insect').
card_original_text('lithophage'/'MMQ', 'At the beginning of your upkeep, sacrifice Lithophage unless you sacrifice a mountain.').
card_first_print('lithophage', 'MMQ').
card_image_name('lithophage'/'MMQ', 'lithophage').
card_uid('lithophage'/'MMQ', 'MMQ:Lithophage:lithophage').
card_rarity('lithophage'/'MMQ', 'Rare').
card_artist('lithophage'/'MMQ', 'Mike Ploog').
card_number('lithophage'/'MMQ', '202').
card_flavor_text('lithophage'/'MMQ', 'It\'s viciously territorial, destroying every creature on the mountain it has chosen to consume.').
card_multiverse_id('lithophage'/'MMQ', '19731').

card_in_set('lumbering satyr', 'MMQ').
card_original_type('lumbering satyr'/'MMQ', 'Creature — Beast').
card_original_text('lumbering satyr'/'MMQ', 'All creatures gain forestwalk. (They\'re unblockable as long as defending player controls a forest.)').
card_first_print('lumbering satyr', 'MMQ').
card_image_name('lumbering satyr'/'MMQ', 'lumbering satyr').
card_uid('lumbering satyr'/'MMQ', 'MMQ:Lumbering Satyr:lumbering satyr').
card_rarity('lumbering satyr'/'MMQ', 'Uncommon').
card_artist('lumbering satyr'/'MMQ', 'Alan Pollack').
card_number('lumbering satyr'/'MMQ', '257').
card_flavor_text('lumbering satyr'/'MMQ', 'The satyr carves the path that all of Rushwood follows.\n—Cho-Arrim saying').
card_multiverse_id('lumbering satyr'/'MMQ', '19860').

card_in_set('lunge', 'MMQ').
card_original_type('lunge'/'MMQ', 'Instant').
card_original_text('lunge'/'MMQ', 'Lunge deals 2 damage to target creature and 2 damage to target player.').
card_first_print('lunge', 'MMQ').
card_image_name('lunge'/'MMQ', 'lunge').
card_uid('lunge'/'MMQ', 'MMQ:Lunge:lunge').
card_rarity('lunge'/'MMQ', 'Common').
card_artist('lunge'/'MMQ', 'Dan Frazier').
card_number('lunge'/'MMQ', '203').
card_flavor_text('lunge'/'MMQ', 'Cho-Arrim righteousness was no match for Benalish combat training.').
card_multiverse_id('lunge'/'MMQ', '19617').

card_in_set('lure', 'MMQ').
card_original_type('lure'/'MMQ', 'Enchant Creature').
card_original_text('lure'/'MMQ', 'All creatures able to block enchanted creature do so if able.').
card_image_name('lure'/'MMQ', 'lure').
card_uid('lure'/'MMQ', 'MMQ:Lure:lure').
card_rarity('lure'/'MMQ', 'Uncommon').
card_artist('lure'/'MMQ', 'DiTerlizzi').
card_number('lure'/'MMQ', '258').
card_flavor_text('lure'/'MMQ', 'Deepwood\'s reach is not long, so it invites its enemies in.').
card_multiverse_id('lure'/'MMQ', '22053').

card_in_set('maggot therapy', 'MMQ').
card_original_type('maggot therapy'/'MMQ', 'Enchant Creature').
card_original_text('maggot therapy'/'MMQ', 'You may play Maggot Therapy any time you could play an instant.\nEnchanted creature gets +2/-2.').
card_first_print('maggot therapy', 'MMQ').
card_image_name('maggot therapy'/'MMQ', 'maggot therapy').
card_uid('maggot therapy'/'MMQ', 'MMQ:Maggot Therapy:maggot therapy').
card_rarity('maggot therapy'/'MMQ', 'Common').
card_artist('maggot therapy'/'MMQ', 'Jeff Easley').
card_number('maggot therapy'/'MMQ', '145').
card_flavor_text('maggot therapy'/'MMQ', '\"If this is the cure, I\'d hate to see the disease.\"\n—Orim').
card_multiverse_id('maggot therapy'/'MMQ', '19594').

card_in_set('magistrate\'s scepter', 'MMQ').
card_original_type('magistrate\'s scepter'/'MMQ', 'Artifact').
card_original_text('magistrate\'s scepter'/'MMQ', '{4}, {T}: Put a charge counter on Magistrate\'s Scepter.\n{T}, Remove three charge counters from Magistrate\'s Scepter: Take another turn after this one.').
card_first_print('magistrate\'s scepter', 'MMQ').
card_image_name('magistrate\'s scepter'/'MMQ', 'magistrate\'s scepter').
card_uid('magistrate\'s scepter'/'MMQ', 'MMQ:Magistrate\'s Scepter:magistrate\'s scepter').
card_rarity('magistrate\'s scepter'/'MMQ', 'Rare').
card_artist('magistrate\'s scepter'/'MMQ', 'Adam Rex').
card_number('magistrate\'s scepter'/'MMQ', '304').
card_multiverse_id('magistrate\'s scepter'/'MMQ', '19748').

card_in_set('magistrate\'s veto', 'MMQ').
card_original_type('magistrate\'s veto'/'MMQ', 'Enchantment').
card_original_text('magistrate\'s veto'/'MMQ', 'White creatures and blue creatures can\'t block.').
card_first_print('magistrate\'s veto', 'MMQ').
card_image_name('magistrate\'s veto'/'MMQ', 'magistrate\'s veto').
card_uid('magistrate\'s veto'/'MMQ', 'MMQ:Magistrate\'s Veto:magistrate\'s veto').
card_rarity('magistrate\'s veto'/'MMQ', 'Uncommon').
card_artist('magistrate\'s veto'/'MMQ', 'Brian Snõddy').
card_number('magistrate\'s veto'/'MMQ', '204').
card_flavor_text('magistrate\'s veto'/'MMQ', 'The magistrate had been the goblins\' puppet for so long that he made the right decisions with little prompting.').
card_multiverse_id('magistrate\'s veto'/'MMQ', '19854').

card_in_set('megatherium', 'MMQ').
card_original_type('megatherium'/'MMQ', 'Creature — Beast').
card_original_text('megatherium'/'MMQ', 'Trample\nWhen Megatherium comes into play, sacrifice it unless you pay {1} for each card in your hand.').
card_first_print('megatherium', 'MMQ').
card_image_name('megatherium'/'MMQ', 'megatherium').
card_uid('megatherium'/'MMQ', 'MMQ:Megatherium:megatherium').
card_rarity('megatherium'/'MMQ', 'Rare').
card_artist('megatherium'/'MMQ', 'Paolo Parente').
card_number('megatherium'/'MMQ', '259').
card_multiverse_id('megatherium'/'MMQ', '19733').

card_in_set('mercadia\'s downfall', 'MMQ').
card_original_type('mercadia\'s downfall'/'MMQ', 'Instant').
card_original_text('mercadia\'s downfall'/'MMQ', 'Attacking creatures get +X/+0 until end of turn, where X is the number of nonbasic lands defending player controls.').
card_first_print('mercadia\'s downfall', 'MMQ').
card_image_name('mercadia\'s downfall'/'MMQ', 'mercadia\'s downfall').
card_uid('mercadia\'s downfall'/'MMQ', 'MMQ:Mercadia\'s Downfall:mercadia\'s downfall').
card_rarity('mercadia\'s downfall'/'MMQ', 'Uncommon').
card_artist('mercadia\'s downfall'/'MMQ', 'Pete Venters').
card_number('mercadia\'s downfall'/'MMQ', '205').
card_flavor_text('mercadia\'s downfall'/'MMQ', 'With an explosion that shook the foundation of the city, the Weatherlight burst from the underground hangar.').
card_multiverse_id('mercadia\'s downfall'/'MMQ', '19850').

card_in_set('mercadian atlas', 'MMQ').
card_original_type('mercadian atlas'/'MMQ', 'Artifact').
card_original_text('mercadian atlas'/'MMQ', 'At the end of your turn, if you didn\'t play a land this turn, you may draw a card.').
card_first_print('mercadian atlas', 'MMQ').
card_image_name('mercadian atlas'/'MMQ', 'mercadian atlas').
card_uid('mercadian atlas'/'MMQ', 'MMQ:Mercadian Atlas:mercadian atlas').
card_rarity('mercadian atlas'/'MMQ', 'Rare').
card_artist('mercadian atlas'/'MMQ', 'Dan Frazier').
card_number('mercadian atlas'/'MMQ', '305').
card_flavor_text('mercadian atlas'/'MMQ', 'One may as well chart the clouds as try to map Mercadia.').
card_multiverse_id('mercadian atlas'/'MMQ', '20844').

card_in_set('mercadian bazaar', 'MMQ').
card_original_type('mercadian bazaar'/'MMQ', 'Land').
card_original_text('mercadian bazaar'/'MMQ', 'Mercadian Bazaar comes into play tapped.\n{T}: Put a storage counter on Mercadian Bazaar.\n{T}, Remove any number of storage counters from Mercadian Bazaar: Add one red mana to your mana pool for each storage counter removed this way.').
card_first_print('mercadian bazaar', 'MMQ').
card_image_name('mercadian bazaar'/'MMQ', 'mercadian bazaar').
card_uid('mercadian bazaar'/'MMQ', 'MMQ:Mercadian Bazaar:mercadian bazaar').
card_rarity('mercadian bazaar'/'MMQ', 'Uncommon').
card_artist('mercadian bazaar'/'MMQ', 'Terese Nielsen').
card_number('mercadian bazaar'/'MMQ', '321').
card_multiverse_id('mercadian bazaar'/'MMQ', '19894').

card_in_set('mercadian lift', 'MMQ').
card_original_type('mercadian lift'/'MMQ', 'Artifact').
card_original_text('mercadian lift'/'MMQ', '{1}, {T}: Put a winch counter on Mercadian Lift.\n{T}, Remove X winch counters from Mercadian Lift: Put a creature card with converted mana cost X from your hand into play.').
card_first_print('mercadian lift', 'MMQ').
card_image_name('mercadian lift'/'MMQ', 'mercadian lift').
card_uid('mercadian lift'/'MMQ', 'MMQ:Mercadian Lift:mercadian lift').
card_rarity('mercadian lift'/'MMQ', 'Rare').
card_artist('mercadian lift'/'MMQ', 'Gary Ruddell').
card_number('mercadian lift'/'MMQ', '306').
card_multiverse_id('mercadian lift'/'MMQ', '19754').

card_in_set('midnight ritual', 'MMQ').
card_original_type('midnight ritual'/'MMQ', 'Sorcery').
card_original_text('midnight ritual'/'MMQ', 'Remove X target creature cards in your graveyard from the game. For each creature card removed this way, put a black 2/2 Zombie creature token into play.').
card_first_print('midnight ritual', 'MMQ').
card_image_name('midnight ritual'/'MMQ', 'midnight ritual').
card_uid('midnight ritual'/'MMQ', 'MMQ:Midnight Ritual:midnight ritual').
card_rarity('midnight ritual'/'MMQ', 'Rare').
card_artist('midnight ritual'/'MMQ', 'Jeff Easley').
card_number('midnight ritual'/'MMQ', '146').
card_flavor_text('midnight ritual'/'MMQ', 'Bury old friends deeply, lest they return bearing worms.').
card_multiverse_id('midnight ritual'/'MMQ', '19707').

card_in_set('misdirection', 'MMQ').
card_original_type('misdirection'/'MMQ', 'Instant').
card_original_text('misdirection'/'MMQ', 'You may remove a blue card in your hand from the game instead of paying Misdirection\'s mana cost.\nTarget spell with a single target targets another target instead.').
card_first_print('misdirection', 'MMQ').
card_image_name('misdirection'/'MMQ', 'misdirection').
card_uid('misdirection'/'MMQ', 'MMQ:Misdirection:misdirection').
card_rarity('misdirection'/'MMQ', 'Rare').
card_artist('misdirection'/'MMQ', 'Paolo Parente').
card_number('misdirection'/'MMQ', '87').
card_multiverse_id('misdirection'/'MMQ', '19681').

card_in_set('misshapen fiend', 'MMQ').
card_original_type('misshapen fiend'/'MMQ', 'Creature — Mercenary').
card_original_text('misshapen fiend'/'MMQ', 'Flying').
card_first_print('misshapen fiend', 'MMQ').
card_image_name('misshapen fiend'/'MMQ', 'misshapen fiend').
card_uid('misshapen fiend'/'MMQ', 'MMQ:Misshapen Fiend:misshapen fiend').
card_rarity('misshapen fiend'/'MMQ', 'Common').
card_artist('misshapen fiend'/'MMQ', 'Adam Rex').
card_number('misshapen fiend'/'MMQ', '147').
card_flavor_text('misshapen fiend'/'MMQ', 'You\'d scarcely believe they could fly until they drop out of the sky onto you.').
card_multiverse_id('misshapen fiend'/'MMQ', '19581').

card_in_set('misstep', 'MMQ').
card_original_type('misstep'/'MMQ', 'Sorcery').
card_original_text('misstep'/'MMQ', 'Creatures target player controls don\'t untap during that player\'s next untap step.').
card_first_print('misstep', 'MMQ').
card_image_name('misstep'/'MMQ', 'misstep').
card_uid('misstep'/'MMQ', 'MMQ:Misstep:misstep').
card_rarity('misstep'/'MMQ', 'Common').
card_artist('misstep'/'MMQ', 'Kev Walker').
card_number('misstep'/'MMQ', '88').
card_flavor_text('misstep'/'MMQ', 'The feet have no eyes.\n—Saprazzan saying').
card_multiverse_id('misstep'/'MMQ', '19568').

card_in_set('molting harpy', 'MMQ').
card_original_type('molting harpy'/'MMQ', 'Creature — Mercenary').
card_original_text('molting harpy'/'MMQ', 'Flying\nAt the beginning of your upkeep, sacrifice Molting Harpy unless you pay {2}.').
card_first_print('molting harpy', 'MMQ').
card_image_name('molting harpy'/'MMQ', 'molting harpy').
card_uid('molting harpy'/'MMQ', 'MMQ:Molting Harpy:molting harpy').
card_rarity('molting harpy'/'MMQ', 'Uncommon').
card_artist('molting harpy'/'MMQ', 'Jeff Laubenstein').
card_number('molting harpy'/'MMQ', '148').
card_flavor_text('molting harpy'/'MMQ', 'Shed harpy feathers leave painful wounds—which explains the harpies\' mood.').
card_multiverse_id('molting harpy'/'MMQ', '19818').

card_in_set('moment of silence', 'MMQ').
card_original_type('moment of silence'/'MMQ', 'Instant').
card_original_text('moment of silence'/'MMQ', 'Target player skips his or her combat phase this turn.').
card_first_print('moment of silence', 'MMQ').
card_image_name('moment of silence'/'MMQ', 'moment of silence').
card_uid('moment of silence'/'MMQ', 'MMQ:Moment of Silence:moment of silence').
card_rarity('moment of silence'/'MMQ', 'Common').
card_artist('moment of silence'/'MMQ', 'Christopher Moeller').
card_number('moment of silence'/'MMQ', '28').
card_flavor_text('moment of silence'/'MMQ', 'The silence commemorates not those who\'ve died, but those about to.').
card_multiverse_id('moment of silence'/'MMQ', '19554').

card_in_set('monkey cage', 'MMQ').
card_original_type('monkey cage'/'MMQ', 'Artifact').
card_original_text('monkey cage'/'MMQ', 'When a creature comes into play, sacrifice Monkey Cage and put into play a number of 2/2 green Ape creature tokens equal to that creature\'s converted mana cost.').
card_first_print('monkey cage', 'MMQ').
card_image_name('monkey cage'/'MMQ', 'monkey cage').
card_uid('monkey cage'/'MMQ', 'MMQ:Monkey Cage:monkey cage').
card_rarity('monkey cage'/'MMQ', 'Rare').
card_artist('monkey cage'/'MMQ', 'Carl Critchlow').
card_number('monkey cage'/'MMQ', '307').
card_multiverse_id('monkey cage'/'MMQ', '19878').

card_in_set('moonlit wake', 'MMQ').
card_original_type('moonlit wake'/'MMQ', 'Enchantment').
card_original_text('moonlit wake'/'MMQ', 'Whenever a creature is put into a graveyard from play, you gain 1 life.').
card_first_print('moonlit wake', 'MMQ').
card_image_name('moonlit wake'/'MMQ', 'moonlit wake').
card_uid('moonlit wake'/'MMQ', 'MMQ:Moonlit Wake:moonlit wake').
card_rarity('moonlit wake'/'MMQ', 'Uncommon').
card_artist('moonlit wake'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('moonlit wake'/'MMQ', '29').
card_flavor_text('moonlit wake'/'MMQ', '\"As long as Rushwood glows, no life is ever truly lost.\"\n—Ta-Karnst, Cho-Arrim healer').
card_multiverse_id('moonlit wake'/'MMQ', '136514').

card_in_set('mountain', 'MMQ').
card_original_type('mountain'/'MMQ', 'Land').
card_original_text('mountain'/'MMQ', 'R').
card_image_name('mountain'/'MMQ', 'mountain1').
card_uid('mountain'/'MMQ', 'MMQ:Mountain:mountain1').
card_rarity('mountain'/'MMQ', 'Basic Land').
card_artist('mountain'/'MMQ', 'Terry Springer').
card_number('mountain'/'MMQ', '343').
card_multiverse_id('mountain'/'MMQ', '20892').

card_in_set('mountain', 'MMQ').
card_original_type('mountain'/'MMQ', 'Land').
card_original_text('mountain'/'MMQ', 'R').
card_image_name('mountain'/'MMQ', 'mountain2').
card_uid('mountain'/'MMQ', 'MMQ:Mountain:mountain2').
card_rarity('mountain'/'MMQ', 'Basic Land').
card_artist('mountain'/'MMQ', 'Scott Bailey').
card_number('mountain'/'MMQ', '344').
card_multiverse_id('mountain'/'MMQ', '20893').

card_in_set('mountain', 'MMQ').
card_original_type('mountain'/'MMQ', 'Land').
card_original_text('mountain'/'MMQ', 'R').
card_image_name('mountain'/'MMQ', 'mountain3').
card_uid('mountain'/'MMQ', 'MMQ:Mountain:mountain3').
card_rarity('mountain'/'MMQ', 'Basic Land').
card_artist('mountain'/'MMQ', 'Dana Knutson').
card_number('mountain'/'MMQ', '345').
card_multiverse_id('mountain'/'MMQ', '20894').

card_in_set('mountain', 'MMQ').
card_original_type('mountain'/'MMQ', 'Land').
card_original_text('mountain'/'MMQ', 'R').
card_image_name('mountain'/'MMQ', 'mountain4').
card_uid('mountain'/'MMQ', 'MMQ:Mountain:mountain4').
card_rarity('mountain'/'MMQ', 'Basic Land').
card_artist('mountain'/'MMQ', 'Rob Alexander').
card_number('mountain'/'MMQ', '346').
card_multiverse_id('mountain'/'MMQ', '20895').

card_in_set('muzzle', 'MMQ').
card_original_type('muzzle'/'MMQ', 'Enchant Creature').
card_original_text('muzzle'/'MMQ', 'Prevent all damage that would be dealt by enchanted creature.').
card_first_print('muzzle', 'MMQ').
card_image_name('muzzle'/'MMQ', 'muzzle').
card_uid('muzzle'/'MMQ', 'MMQ:Muzzle:muzzle').
card_rarity('muzzle'/'MMQ', 'Common').
card_artist('muzzle'/'MMQ', 'Matt Cavotta').
card_number('muzzle'/'MMQ', '30').
card_flavor_text('muzzle'/'MMQ', 'Clenched teeth do not bite.\n—Cho-Arrim saying').
card_multiverse_id('muzzle'/'MMQ', '20410').

card_in_set('natural affinity', 'MMQ').
card_original_type('natural affinity'/'MMQ', 'Instant').
card_original_text('natural affinity'/'MMQ', 'All lands become 2/2 creatures until end of turn. They still count as lands.').
card_first_print('natural affinity', 'MMQ').
card_image_name('natural affinity'/'MMQ', 'natural affinity').
card_uid('natural affinity'/'MMQ', 'MMQ:Natural Affinity:natural affinity').
card_rarity('natural affinity'/'MMQ', 'Rare').
card_artist('natural affinity'/'MMQ', 'Pete Venters').
card_number('natural affinity'/'MMQ', '260').
card_flavor_text('natural affinity'/'MMQ', 'As long as the dryads are a part of the forest, the forest is a part of the army.').
card_multiverse_id('natural affinity'/'MMQ', '19875').

card_in_set('nether spirit', 'MMQ').
card_original_type('nether spirit'/'MMQ', 'Creature — Spirit').
card_original_text('nether spirit'/'MMQ', 'At the beginning of your upkeep, if Nether Spirit is the only creature card in your graveyard, you may return Nether Spirit to play.').
card_first_print('nether spirit', 'MMQ').
card_image_name('nether spirit'/'MMQ', 'nether spirit').
card_uid('nether spirit'/'MMQ', 'MMQ:Nether Spirit:nether spirit').
card_rarity('nether spirit'/'MMQ', 'Rare').
card_artist('nether spirit'/'MMQ', 'Alan Pollack').
card_number('nether spirit'/'MMQ', '149').
card_flavor_text('nether spirit'/'MMQ', 'It roams in both the worlds of the living and the dead but belongs to neither.').
card_multiverse_id('nether spirit'/'MMQ', '19693').

card_in_set('nightwind glider', 'MMQ').
card_original_type('nightwind glider'/'MMQ', 'Creature — Rebel').
card_original_text('nightwind glider'/'MMQ', 'Flying, protection from black').
card_first_print('nightwind glider', 'MMQ').
card_image_name('nightwind glider'/'MMQ', 'nightwind glider').
card_uid('nightwind glider'/'MMQ', 'MMQ:Nightwind Glider:nightwind glider').
card_rarity('nightwind glider'/'MMQ', 'Common').
card_artist('nightwind glider'/'MMQ', 'Randy Gallegos').
card_number('nightwind glider'/'MMQ', '31').
card_flavor_text('nightwind glider'/'MMQ', 'Once you learn to float on the shadows, you\'ll never fear them again.').
card_multiverse_id('nightwind glider'/'MMQ', '19544').

card_in_set('noble purpose', 'MMQ').
card_original_type('noble purpose'/'MMQ', 'Enchantment').
card_original_text('noble purpose'/'MMQ', 'Whenever a creature you control deals combat damage, you gain that much life.').
card_first_print('noble purpose', 'MMQ').
card_image_name('noble purpose'/'MMQ', 'noble purpose').
card_uid('noble purpose'/'MMQ', 'MMQ:Noble Purpose:noble purpose').
card_rarity('noble purpose'/'MMQ', 'Uncommon').
card_artist('noble purpose'/'MMQ', 'Kev Walker').
card_number('noble purpose'/'MMQ', '32').
card_flavor_text('noble purpose'/'MMQ', 'Fighting for the right reason is itself a source of strength.').
card_multiverse_id('noble purpose'/'MMQ', '19786').

card_in_set('notorious assassin', 'MMQ').
card_original_type('notorious assassin'/'MMQ', 'Creature — Spellshaper').
card_original_text('notorious assassin'/'MMQ', '{2}{B}, {T}, Discard a card from your hand: Destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('notorious assassin', 'MMQ').
card_image_name('notorious assassin'/'MMQ', 'notorious assassin').
card_uid('notorious assassin'/'MMQ', 'MMQ:Notorious Assassin:notorious assassin').
card_rarity('notorious assassin'/'MMQ', 'Rare').
card_artist('notorious assassin'/'MMQ', 'Heather Hudson').
card_number('notorious assassin'/'MMQ', '150').
card_flavor_text('notorious assassin'/'MMQ', 'He wears his infamy like a fine silk shirt.').
card_multiverse_id('notorious assassin'/'MMQ', '19695').

card_in_set('ogre taskmaster', 'MMQ').
card_original_type('ogre taskmaster'/'MMQ', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'MMQ', 'Ogre Taskmaster can\'t block.').
card_image_name('ogre taskmaster'/'MMQ', 'ogre taskmaster').
card_uid('ogre taskmaster'/'MMQ', 'MMQ:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'MMQ', 'Uncommon').
card_artist('ogre taskmaster'/'MMQ', 'Orizio Daniele').
card_number('ogre taskmaster'/'MMQ', '206').
card_flavor_text('ogre taskmaster'/'MMQ', 'He doesn\'t have to be smart—his masters only ever give him one command.').
card_multiverse_id('ogre taskmaster'/'MMQ', '19844').

card_in_set('orim\'s cure', 'MMQ').
card_original_type('orim\'s cure'/'MMQ', 'Instant').
card_original_text('orim\'s cure'/'MMQ', 'If you control a plains, you may tap an untapped creature you control instead of paying the mana cost of Orim\'s Cure.\nPrevent the next 4 damage that would be dealt to target creature or player this turn.').
card_first_print('orim\'s cure', 'MMQ').
card_image_name('orim\'s cure'/'MMQ', 'orim\'s cure').
card_uid('orim\'s cure'/'MMQ', 'MMQ:Orim\'s Cure:orim\'s cure').
card_rarity('orim\'s cure'/'MMQ', 'Common').
card_artist('orim\'s cure'/'MMQ', 'Don Hazeltine').
card_number('orim\'s cure'/'MMQ', '33').
card_multiverse_id('orim\'s cure'/'MMQ', '19547').

card_in_set('overtaker', 'MMQ').
card_original_type('overtaker'/'MMQ', 'Creature — Spellshaper').
card_original_text('overtaker'/'MMQ', '{3}{U}, {T}, Discard a card from your hand: Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn. (It may attack and {T} the turn it comes under your control.)').
card_image_name('overtaker'/'MMQ', 'overtaker').
card_uid('overtaker'/'MMQ', 'MMQ:Overtaker:overtaker').
card_rarity('overtaker'/'MMQ', 'Rare').
card_artist('overtaker'/'MMQ', 'Clyde Caldwell').
card_number('overtaker'/'MMQ', '89').
card_flavor_text('overtaker'/'MMQ', '\"It\'s no challenge to master a Mercadian.\"').
card_multiverse_id('overtaker'/'MMQ', '19666').

card_in_set('panacea', 'MMQ').
card_original_type('panacea'/'MMQ', 'Artifact').
card_original_text('panacea'/'MMQ', '{X}{X}, {T}: Prevent the next X damage that would be dealt to target creature or player this turn.').
card_first_print('panacea', 'MMQ').
card_image_name('panacea'/'MMQ', 'panacea').
card_uid('panacea'/'MMQ', 'MMQ:Panacea:panacea').
card_rarity('panacea'/'MMQ', 'Uncommon').
card_artist('panacea'/'MMQ', 'Donato Giancola').
card_number('panacea'/'MMQ', '308').
card_flavor_text('panacea'/'MMQ', 'A drop numbs the tongue. A sip sends the heart racing. A draught reforges the body.').
card_multiverse_id('panacea'/'MMQ', '19886').

card_in_set('pangosaur', 'MMQ').
card_original_type('pangosaur'/'MMQ', 'Creature — Lizard').
card_original_text('pangosaur'/'MMQ', 'Whenever a player plays a land, return Pangosaur to its owner\'s hand.').
card_first_print('pangosaur', 'MMQ').
card_image_name('pangosaur'/'MMQ', 'pangosaur').
card_uid('pangosaur'/'MMQ', 'MMQ:Pangosaur:pangosaur').
card_rarity('pangosaur'/'MMQ', 'Rare').
card_artist('pangosaur'/'MMQ', 'Mark Tedin').
card_number('pangosaur'/'MMQ', '261').
card_flavor_text('pangosaur'/'MMQ', 'Only Rushwood is vast enough to swallow this creature whole.').
card_multiverse_id('pangosaur'/'MMQ', '20788').

card_in_set('peat bog', 'MMQ').
card_original_type('peat bog'/'MMQ', 'Land').
card_original_text('peat bog'/'MMQ', 'Peat Bog comes into play tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Peat Bog: Add two black mana to your mana pool. If there are no depletion counters on Peat Bog, sacrifice it.').
card_first_print('peat bog', 'MMQ').
card_image_name('peat bog'/'MMQ', 'peat bog').
card_uid('peat bog'/'MMQ', 'MMQ:Peat Bog:peat bog').
card_rarity('peat bog'/'MMQ', 'Common').
card_artist('peat bog'/'MMQ', 'Val Mayerik').
card_number('peat bog'/'MMQ', '322').
card_multiverse_id('peat bog'/'MMQ', '19644').

card_in_set('pious warrior', 'MMQ').
card_original_type('pious warrior'/'MMQ', 'Creature — Rebel').
card_original_text('pious warrior'/'MMQ', 'Whenever Pious Warrior is dealt combat damage, you gain that much life.').
card_first_print('pious warrior', 'MMQ').
card_image_name('pious warrior'/'MMQ', 'pious warrior').
card_uid('pious warrior'/'MMQ', 'MMQ:Pious Warrior:pious warrior').
card_rarity('pious warrior'/'MMQ', 'Common').
card_artist('pious warrior'/'MMQ', 'Jeff Miracola').
card_number('pious warrior'/'MMQ', '34').
card_flavor_text('pious warrior'/'MMQ', 'The Cho-Arrim make no distinction between healers and soldiers.').
card_multiverse_id('pious warrior'/'MMQ', '19552').

card_in_set('plains', 'MMQ').
card_original_type('plains'/'MMQ', 'Land').
card_original_text('plains'/'MMQ', 'W').
card_image_name('plains'/'MMQ', 'plains1').
card_uid('plains'/'MMQ', 'MMQ:Plains:plains1').
card_rarity('plains'/'MMQ', 'Basic Land').
card_artist('plains'/'MMQ', 'Terry Springer').
card_number('plains'/'MMQ', '331').
card_multiverse_id('plains'/'MMQ', '20880').

card_in_set('plains', 'MMQ').
card_original_type('plains'/'MMQ', 'Land').
card_original_text('plains'/'MMQ', 'W').
card_image_name('plains'/'MMQ', 'plains2').
card_uid('plains'/'MMQ', 'MMQ:Plains:plains2').
card_rarity('plains'/'MMQ', 'Basic Land').
card_artist('plains'/'MMQ', 'Scott Bailey').
card_number('plains'/'MMQ', '332').
card_multiverse_id('plains'/'MMQ', '20881').

card_in_set('plains', 'MMQ').
card_original_type('plains'/'MMQ', 'Land').
card_original_text('plains'/'MMQ', 'W').
card_image_name('plains'/'MMQ', 'plains3').
card_uid('plains'/'MMQ', 'MMQ:Plains:plains3').
card_rarity('plains'/'MMQ', 'Basic Land').
card_artist('plains'/'MMQ', 'Dana Knutson').
card_number('plains'/'MMQ', '333').
card_multiverse_id('plains'/'MMQ', '20882').

card_in_set('plains', 'MMQ').
card_original_type('plains'/'MMQ', 'Land').
card_original_text('plains'/'MMQ', 'W').
card_image_name('plains'/'MMQ', 'plains4').
card_uid('plains'/'MMQ', 'MMQ:Plains:plains4').
card_rarity('plains'/'MMQ', 'Basic Land').
card_artist('plains'/'MMQ', 'Edward P. Beard, Jr.').
card_number('plains'/'MMQ', '334').
card_multiverse_id('plains'/'MMQ', '20883').

card_in_set('port inspector', 'MMQ').
card_original_type('port inspector'/'MMQ', 'Creature — Townsfolk').
card_original_text('port inspector'/'MMQ', 'Whenever Port Inspector becomes blocked, you may look at defending player\'s hand.').
card_first_print('port inspector', 'MMQ').
card_image_name('port inspector'/'MMQ', 'port inspector').
card_uid('port inspector'/'MMQ', 'MMQ:Port Inspector:port inspector').
card_rarity('port inspector'/'MMQ', 'Common').
card_artist('port inspector'/'MMQ', 'Dan Frazier').
card_number('port inspector'/'MMQ', '90').
card_flavor_text('port inspector'/'MMQ', '\"There may be a few minor additional fees, levies, duties, surtaxes, tariffs, imposts, tolls . . . .\"').
card_multiverse_id('port inspector'/'MMQ', '19562').

card_in_set('power matrix', 'MMQ').
card_original_type('power matrix'/'MMQ', 'Artifact').
card_original_text('power matrix'/'MMQ', '{T}: Target creature gets +1/+1 and gains flying, first strike, and trample until end of turn.').
card_first_print('power matrix', 'MMQ').
card_image_name('power matrix'/'MMQ', 'power matrix').
card_uid('power matrix'/'MMQ', 'MMQ:Power Matrix:power matrix').
card_rarity('power matrix'/'MMQ', 'Rare').
card_artist('power matrix'/'MMQ', 'Alan Pollack').
card_number('power matrix'/'MMQ', '309').
card_flavor_text('power matrix'/'MMQ', 'As he repaired the ship\'s planeswalking engine, Karn saw for the first time how he could catalyze the Weatherlight\'s evolution.').
card_multiverse_id('power matrix'/'MMQ', '19888').

card_in_set('pretender\'s claim', 'MMQ').
card_original_type('pretender\'s claim'/'MMQ', 'Enchant Creature').
card_original_text('pretender\'s claim'/'MMQ', 'Whenever enchanted creature becomes blocked, tap all lands defending player controls.').
card_first_print('pretender\'s claim', 'MMQ').
card_image_name('pretender\'s claim'/'MMQ', 'pretender\'s claim').
card_uid('pretender\'s claim'/'MMQ', 'MMQ:Pretender\'s Claim:pretender\'s claim').
card_rarity('pretender\'s claim'/'MMQ', 'Uncommon').
card_artist('pretender\'s claim'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('pretender\'s claim'/'MMQ', '151').
card_flavor_text('pretender\'s claim'/'MMQ', 'The surest way to lose your holdings is to plead for them in Mercadia City\'s courts.').
card_multiverse_id('pretender\'s claim'/'MMQ', '19827').

card_in_set('primeval shambler', 'MMQ').
card_original_type('primeval shambler'/'MMQ', 'Creature — Mercenary').
card_original_text('primeval shambler'/'MMQ', '{B} Primeval Shambler gets +1/+1 until end of turn.').
card_first_print('primeval shambler', 'MMQ').
card_image_name('primeval shambler'/'MMQ', 'primeval shambler').
card_uid('primeval shambler'/'MMQ', 'MMQ:Primeval Shambler:primeval shambler').
card_rarity('primeval shambler'/'MMQ', 'Uncommon').
card_artist('primeval shambler'/'MMQ', 'Chippy').
card_number('primeval shambler'/'MMQ', '152').
card_flavor_text('primeval shambler'/'MMQ', 'His mind, like his body, is made up of the swamp\'s flotsam.').
card_multiverse_id('primeval shambler'/'MMQ', '19824').

card_in_set('puffer extract', 'MMQ').
card_original_type('puffer extract'/'MMQ', 'Artifact').
card_original_text('puffer extract'/'MMQ', '{X}, {T}: Target creature you control gets +X/+X until end of turn. Destroy it at end of turn.').
card_first_print('puffer extract', 'MMQ').
card_image_name('puffer extract'/'MMQ', 'puffer extract').
card_uid('puffer extract'/'MMQ', 'MMQ:Puffer Extract:puffer extract').
card_rarity('puffer extract'/'MMQ', 'Uncommon').
card_artist('puffer extract'/'MMQ', 'Heather Hudson').
card_number('puffer extract'/'MMQ', '310').
card_flavor_text('puffer extract'/'MMQ', 'The puffer\'s flesh is the deadliest in the sea—and therefore the most useful.').
card_multiverse_id('puffer extract'/'MMQ', '19881').

card_in_set('pulverize', 'MMQ').
card_original_type('pulverize'/'MMQ', 'Sorcery').
card_original_text('pulverize'/'MMQ', 'You may sacrifice two mountains instead of paying Pulverize\'s mana cost.\nDestroy all artifacts.').
card_first_print('pulverize', 'MMQ').
card_image_name('pulverize'/'MMQ', 'pulverize').
card_uid('pulverize'/'MMQ', 'MMQ:Pulverize:pulverize').
card_rarity('pulverize'/'MMQ', 'Rare').
card_artist('pulverize'/'MMQ', 'Scott M. Fischer').
card_number('pulverize'/'MMQ', '207').
card_multiverse_id('pulverize'/'MMQ', '19724').

card_in_set('puppet\'s verdict', 'MMQ').
card_original_type('puppet\'s verdict'/'MMQ', 'Instant').
card_original_text('puppet\'s verdict'/'MMQ', 'Flip a coin. If you win the flip, destroy all creatures with power 2 or less. If you lose the flip, destroy all creatures with power 3 or greater.').
card_first_print('puppet\'s verdict', 'MMQ').
card_image_name('puppet\'s verdict'/'MMQ', 'puppet\'s verdict').
card_uid('puppet\'s verdict'/'MMQ', 'MMQ:Puppet\'s Verdict:puppet\'s verdict').
card_rarity('puppet\'s verdict'/'MMQ', 'Rare').
card_artist('puppet\'s verdict'/'MMQ', 'Edward P. Beard, Jr.').
card_number('puppet\'s verdict'/'MMQ', '208').
card_flavor_text('puppet\'s verdict'/'MMQ', '\"What was heads again?\"\n—Mercadian magistrate').
card_multiverse_id('puppet\'s verdict'/'MMQ', '19721').

card_in_set('putrefaction', 'MMQ').
card_original_type('putrefaction'/'MMQ', 'Enchantment').
card_original_text('putrefaction'/'MMQ', 'Whenever a player plays a white spell or green spell, that player discards a card from his or her hand.').
card_first_print('putrefaction', 'MMQ').
card_image_name('putrefaction'/'MMQ', 'putrefaction').
card_uid('putrefaction'/'MMQ', 'MMQ:Putrefaction:putrefaction').
card_rarity('putrefaction'/'MMQ', 'Uncommon').
card_artist('putrefaction'/'MMQ', 'DiTerlizzi').
card_number('putrefaction'/'MMQ', '153').
card_flavor_text('putrefaction'/'MMQ', '\"Do you want to be alive or whole?\"\n—Ta-Karnst, Cho-Arrim healer').
card_multiverse_id('putrefaction'/'MMQ', '19834').

card_in_set('quagmire lamprey', 'MMQ').
card_original_type('quagmire lamprey'/'MMQ', 'Creature — Fish').
card_original_text('quagmire lamprey'/'MMQ', 'Whenever Quagmire Lamprey becomes blocked by a creature, put a -1/-1 counter on that creature.').
card_first_print('quagmire lamprey', 'MMQ').
card_image_name('quagmire lamprey'/'MMQ', 'quagmire lamprey').
card_uid('quagmire lamprey'/'MMQ', 'MMQ:Quagmire Lamprey:quagmire lamprey').
card_rarity('quagmire lamprey'/'MMQ', 'Uncommon').
card_artist('quagmire lamprey'/'MMQ', 'Glen Angus').
card_number('quagmire lamprey'/'MMQ', '154').
card_multiverse_id('quagmire lamprey'/'MMQ', '19822').

card_in_set('rain of tears', 'MMQ').
card_original_type('rain of tears'/'MMQ', 'Sorcery').
card_original_text('rain of tears'/'MMQ', 'Destroy target land.').
card_image_name('rain of tears'/'MMQ', 'rain of tears').
card_uid('rain of tears'/'MMQ', 'MMQ:Rain of Tears:rain of tears').
card_rarity('rain of tears'/'MMQ', 'Uncommon').
card_artist('rain of tears'/'MMQ', 'Edward P. Beard, Jr.').
card_number('rain of tears'/'MMQ', '155').
card_flavor_text('rain of tears'/'MMQ', 'Mercadian elitism is reinforced daily by physical position: their neighbors are literally beneath them and are treated accordingly.').
card_multiverse_id('rain of tears'/'MMQ', '19828').

card_in_set('ramosian captain', 'MMQ').
card_original_type('ramosian captain'/'MMQ', 'Creature — Rebel').
card_original_text('ramosian captain'/'MMQ', 'First strike\n{5}, {T}: Search your library for a Rebel card with converted mana cost 4 or less and put that card into play. Then shuffle your library.').
card_first_print('ramosian captain', 'MMQ').
card_image_name('ramosian captain'/'MMQ', 'ramosian captain').
card_uid('ramosian captain'/'MMQ', 'MMQ:Ramosian Captain:ramosian captain').
card_rarity('ramosian captain'/'MMQ', 'Uncommon').
card_artist('ramosian captain'/'MMQ', 'Matthew D. Wilson').
card_number('ramosian captain'/'MMQ', '35').
card_flavor_text('ramosian captain'/'MMQ', 'The Cho-Arrim believe in leading by example.').
card_multiverse_id('ramosian captain'/'MMQ', '19782').

card_in_set('ramosian commander', 'MMQ').
card_original_type('ramosian commander'/'MMQ', 'Creature — Rebel').
card_original_text('ramosian commander'/'MMQ', '{6}, {T}: Search your library for a Rebel card with converted mana cost 5 or less and put that card into play. Then shuffle your library.').
card_first_print('ramosian commander', 'MMQ').
card_image_name('ramosian commander'/'MMQ', 'ramosian commander').
card_uid('ramosian commander'/'MMQ', 'MMQ:Ramosian Commander:ramosian commander').
card_rarity('ramosian commander'/'MMQ', 'Uncommon').
card_artist('ramosian commander'/'MMQ', 'Scott Hampton').
card_number('ramosian commander'/'MMQ', '36').
card_flavor_text('ramosian commander'/'MMQ', '\"Cho-Manno guides your spirit. I guide your sword.\"').
card_multiverse_id('ramosian commander'/'MMQ', '20848').

card_in_set('ramosian lieutenant', 'MMQ').
card_original_type('ramosian lieutenant'/'MMQ', 'Creature — Rebel').
card_original_text('ramosian lieutenant'/'MMQ', '{4}, {T}: Search your library for a Rebel card with converted mana cost 3 or less and put that card into play. Then shuffle your library.').
card_first_print('ramosian lieutenant', 'MMQ').
card_image_name('ramosian lieutenant'/'MMQ', 'ramosian lieutenant').
card_uid('ramosian lieutenant'/'MMQ', 'MMQ:Ramosian Lieutenant:ramosian lieutenant').
card_rarity('ramosian lieutenant'/'MMQ', 'Common').
card_artist('ramosian lieutenant'/'MMQ', 'Alan Pollack').
card_number('ramosian lieutenant'/'MMQ', '37').
card_flavor_text('ramosian lieutenant'/'MMQ', '\"Give me your hand and I will give you victory.\"').
card_multiverse_id('ramosian lieutenant'/'MMQ', '19543').

card_in_set('ramosian rally', 'MMQ').
card_original_type('ramosian rally'/'MMQ', 'Instant').
card_original_text('ramosian rally'/'MMQ', 'If you control a plains, you may tap an untapped creature you control instead of paying Ramosian Rally\'s mana cost.\nCreatures you control get +1/+1 until end of turn.').
card_first_print('ramosian rally', 'MMQ').
card_image_name('ramosian rally'/'MMQ', 'ramosian rally').
card_uid('ramosian rally'/'MMQ', 'MMQ:Ramosian Rally:ramosian rally').
card_rarity('ramosian rally'/'MMQ', 'Common').
card_artist('ramosian rally'/'MMQ', 'Christopher Moeller').
card_number('ramosian rally'/'MMQ', '38').
card_multiverse_id('ramosian rally'/'MMQ', '20409').

card_in_set('ramosian sergeant', 'MMQ').
card_original_type('ramosian sergeant'/'MMQ', 'Creature — Rebel').
card_original_text('ramosian sergeant'/'MMQ', '{3}, {T}: Search your library for a Rebel card with converted mana cost 2 or less and put that card into play. Then shuffle your library.').
card_first_print('ramosian sergeant', 'MMQ').
card_image_name('ramosian sergeant'/'MMQ', 'ramosian sergeant').
card_uid('ramosian sergeant'/'MMQ', 'MMQ:Ramosian Sergeant:ramosian sergeant').
card_rarity('ramosian sergeant'/'MMQ', 'Common').
card_artist('ramosian sergeant'/'MMQ', 'Don Hazeltine').
card_number('ramosian sergeant'/'MMQ', '39').
card_flavor_text('ramosian sergeant'/'MMQ', 'Her commands are part rallying cry, part sermon, and wholly undeniable.').
card_multiverse_id('ramosian sergeant'/'MMQ', '19539').

card_in_set('ramosian sky marshal', 'MMQ').
card_original_type('ramosian sky marshal'/'MMQ', 'Creature — Rebel').
card_original_text('ramosian sky marshal'/'MMQ', 'Flying\n{7}, {T}: Search your library for a Rebel card with converted mana cost 6 or less and put that card into play. Then shuffle your library.').
card_first_print('ramosian sky marshal', 'MMQ').
card_image_name('ramosian sky marshal'/'MMQ', 'ramosian sky marshal').
card_uid('ramosian sky marshal'/'MMQ', 'MMQ:Ramosian Sky Marshal:ramosian sky marshal').
card_rarity('ramosian sky marshal'/'MMQ', 'Rare').
card_artist('ramosian sky marshal'/'MMQ', 'Matt Cavotta').
card_number('ramosian sky marshal'/'MMQ', '40').
card_flavor_text('ramosian sky marshal'/'MMQ', 'The Cho-Arrim fell from the sky onto Mercadia City like a vengeful rain.').
card_multiverse_id('ramosian sky marshal'/'MMQ', '19648').

card_in_set('rampart crawler', 'MMQ').
card_original_type('rampart crawler'/'MMQ', 'Creature — Mercenary').
card_original_text('rampart crawler'/'MMQ', 'Rampart Crawler can\'t be blocked by Walls.').
card_first_print('rampart crawler', 'MMQ').
card_image_name('rampart crawler'/'MMQ', 'rampart crawler').
card_uid('rampart crawler'/'MMQ', 'MMQ:Rampart Crawler:rampart crawler').
card_rarity('rampart crawler'/'MMQ', 'Common').
card_artist('rampart crawler'/'MMQ', 'Pete Venters').
card_number('rampart crawler'/'MMQ', '156').
card_flavor_text('rampart crawler'/'MMQ', '\"Not even the magistrate in his lofty tower is out of our reach.\"\n—Cateran overlord').
card_multiverse_id('rampart crawler'/'MMQ', '19586').

card_in_set('rappelling scouts', 'MMQ').
card_original_type('rappelling scouts'/'MMQ', 'Creature — Rebel').
card_original_text('rappelling scouts'/'MMQ', 'Flying\n{2}{W} Rappelling Scouts gains protection from the color of your choice until end of turn.').
card_first_print('rappelling scouts', 'MMQ').
card_image_name('rappelling scouts'/'MMQ', 'rappelling scouts').
card_uid('rappelling scouts'/'MMQ', 'MMQ:Rappelling Scouts:rappelling scouts').
card_rarity('rappelling scouts'/'MMQ', 'Rare').
card_artist('rappelling scouts'/'MMQ', 'Nelson DeCastro').
card_number('rappelling scouts'/'MMQ', '41').
card_multiverse_id('rappelling scouts'/'MMQ', '20412').

card_in_set('remote farm', 'MMQ').
card_original_type('remote farm'/'MMQ', 'Land').
card_original_text('remote farm'/'MMQ', 'Remote Farm comes into play tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Remote Farm: Add two white mana to your mana pool. If there are no depletion counters on Remote Farm, sacrifice it.').
card_first_print('remote farm', 'MMQ').
card_image_name('remote farm'/'MMQ', 'remote farm').
card_uid('remote farm'/'MMQ', 'MMQ:Remote Farm:remote farm').
card_rarity('remote farm'/'MMQ', 'Common').
card_artist('remote farm'/'MMQ', 'Rob Alexander').
card_number('remote farm'/'MMQ', '323').
card_multiverse_id('remote farm'/'MMQ', '19642').

card_in_set('renounce', 'MMQ').
card_original_type('renounce'/'MMQ', 'Instant').
card_original_text('renounce'/'MMQ', 'Sacrifice any number of permanents. You gain 2 life for each one sacrificed this way.').
card_first_print('renounce', 'MMQ').
card_image_name('renounce'/'MMQ', 'renounce').
card_uid('renounce'/'MMQ', 'MMQ:Renounce:renounce').
card_rarity('renounce'/'MMQ', 'Uncommon').
card_artist('renounce'/'MMQ', 'Carl Critchlow').
card_number('renounce'/'MMQ', '42').
card_flavor_text('renounce'/'MMQ', 'Gerrard offered no defense to Orim\'s condemnation; the mission was under his command, and he was responsible.').
card_multiverse_id('renounce'/'MMQ', '136515').

card_in_set('revered elder', 'MMQ').
card_original_type('revered elder'/'MMQ', 'Creature — Cleric').
card_original_text('revered elder'/'MMQ', '{1}: Prevent the next 1 damage that would be dealt to Revered Elder this turn.').
card_first_print('revered elder', 'MMQ').
card_image_name('revered elder'/'MMQ', 'revered elder').
card_uid('revered elder'/'MMQ', 'MMQ:Revered Elder:revered elder').
card_rarity('revered elder'/'MMQ', 'Common').
card_artist('revered elder'/'MMQ', 'Donato Giancola').
card_number('revered elder'/'MMQ', '43').
card_flavor_text('revered elder'/'MMQ', 'The Cho-Arrim worship life and revere those who\'ve experienced it longest.').
card_multiverse_id('revered elder'/'MMQ', '19538').

card_in_set('reverent mantra', 'MMQ').
card_original_type('reverent mantra'/'MMQ', 'Instant').
card_original_text('reverent mantra'/'MMQ', 'You may remove a white card in your hand from the game instead of paying Reverent Mantra\'s mana cost.\nAll creatures gain protection from the color of your choice until end of turn.').
card_first_print('reverent mantra', 'MMQ').
card_image_name('reverent mantra'/'MMQ', 'reverent mantra').
card_uid('reverent mantra'/'MMQ', 'MMQ:Reverent Mantra:reverent mantra').
card_rarity('reverent mantra'/'MMQ', 'Rare').
card_artist('reverent mantra'/'MMQ', 'Rebecca Guay').
card_number('reverent mantra'/'MMQ', '44').
card_multiverse_id('reverent mantra'/'MMQ', '19653').

card_in_set('revive', 'MMQ').
card_original_type('revive'/'MMQ', 'Sorcery').
card_original_text('revive'/'MMQ', 'Return target green card from your graveyard to your hand.').
card_first_print('revive', 'MMQ').
card_image_name('revive'/'MMQ', 'revive').
card_uid('revive'/'MMQ', 'MMQ:Revive:revive').
card_rarity('revive'/'MMQ', 'Uncommon').
card_artist('revive'/'MMQ', 'Matthew D. Wilson').
card_number('revive'/'MMQ', '262').
card_flavor_text('revive'/'MMQ', 'Gerrard\'s heart ached, recalling the living wreath Mirri laid for her dead kin. No such memorial was left for her in Rath.').
card_multiverse_id('revive'/'MMQ', '19641').

card_in_set('righteous aura', 'MMQ').
card_original_type('righteous aura'/'MMQ', 'Enchantment').
card_original_text('righteous aura'/'MMQ', '{W}, Pay 2 life: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('righteous aura'/'MMQ', 'righteous aura').
card_uid('righteous aura'/'MMQ', 'MMQ:Righteous Aura:righteous aura').
card_rarity('righteous aura'/'MMQ', 'Uncommon').
card_artist('righteous aura'/'MMQ', 'Pete Venters').
card_number('righteous aura'/'MMQ', '45').
card_flavor_text('righteous aura'/'MMQ', '\"There is a great difference between pain and injury.\"\n—Cho-Manno').
card_multiverse_id('righteous aura'/'MMQ', '19555').

card_in_set('righteous indignation', 'MMQ').
card_original_type('righteous indignation'/'MMQ', 'Enchantment').
card_original_text('righteous indignation'/'MMQ', 'Whenever a creature blocks a black or red creature, the blocking creature gets +1/+1 until end of turn.').
card_first_print('righteous indignation', 'MMQ').
card_image_name('righteous indignation'/'MMQ', 'righteous indignation').
card_uid('righteous indignation'/'MMQ', 'MMQ:Righteous Indignation:righteous indignation').
card_rarity('righteous indignation'/'MMQ', 'Uncommon').
card_artist('righteous indignation'/'MMQ', 'Val Mayerik').
card_number('righteous indignation'/'MMQ', '46').
card_flavor_text('righteous indignation'/'MMQ', '\"You are the disease that plagues Mercadia. I am the healer\'s knife that will cut you out.\"').
card_multiverse_id('righteous indignation'/'MMQ', '19793').

card_in_set('rishadan airship', 'MMQ').
card_original_type('rishadan airship'/'MMQ', 'Creature — Pirate').
card_original_text('rishadan airship'/'MMQ', 'Flying\nRishadan Airship may block only creatures with flying.').
card_first_print('rishadan airship', 'MMQ').
card_image_name('rishadan airship'/'MMQ', 'rishadan airship').
card_uid('rishadan airship'/'MMQ', 'MMQ:Rishadan Airship:rishadan airship').
card_rarity('rishadan airship'/'MMQ', 'Common').
card_artist('rishadan airship'/'MMQ', 'Kev Walker').
card_number('rishadan airship'/'MMQ', '91').
card_flavor_text('rishadan airship'/'MMQ', 'The view is truly spectacular—as long as you don\'t look at Rishada itself.').
card_multiverse_id('rishadan airship'/'MMQ', '19802').

card_in_set('rishadan brigand', 'MMQ').
card_original_type('rishadan brigand'/'MMQ', 'Creature — Pirate').
card_original_text('rishadan brigand'/'MMQ', 'Flying\nWhen Rishadan Brigand comes into play, each opponent sacrifices a permanent unless he or she pays {3}.\nRishadan Brigand may block only creatures with flying.').
card_first_print('rishadan brigand', 'MMQ').
card_image_name('rishadan brigand'/'MMQ', 'rishadan brigand').
card_uid('rishadan brigand'/'MMQ', 'MMQ:Rishadan Brigand:rishadan brigand').
card_rarity('rishadan brigand'/'MMQ', 'Rare').
card_artist('rishadan brigand'/'MMQ', 'Scott Hampton').
card_number('rishadan brigand'/'MMQ', '92').
card_multiverse_id('rishadan brigand'/'MMQ', '19670').

card_in_set('rishadan cutpurse', 'MMQ').
card_original_type('rishadan cutpurse'/'MMQ', 'Creature — Pirate').
card_original_text('rishadan cutpurse'/'MMQ', 'When Rishadan Cutpurse comes into play, each opponent sacrifices a permanent unless he or she pays {1}.').
card_first_print('rishadan cutpurse', 'MMQ').
card_image_name('rishadan cutpurse'/'MMQ', 'rishadan cutpurse').
card_uid('rishadan cutpurse'/'MMQ', 'MMQ:Rishadan Cutpurse:rishadan cutpurse').
card_rarity('rishadan cutpurse'/'MMQ', 'Common').
card_artist('rishadan cutpurse'/'MMQ', 'Christopher Moeller').
card_number('rishadan cutpurse'/'MMQ', '93').
card_flavor_text('rishadan cutpurse'/'MMQ', 'The bigger the purse, the greater the fool.\n—Rishadan saying').
card_multiverse_id('rishadan cutpurse'/'MMQ', '19566').

card_in_set('rishadan footpad', 'MMQ').
card_original_type('rishadan footpad'/'MMQ', 'Creature — Pirate').
card_original_text('rishadan footpad'/'MMQ', 'When Rishadan Footpad comes into play, each opponent sacrifices a permanent unless he or she pays {2}.').
card_first_print('rishadan footpad', 'MMQ').
card_image_name('rishadan footpad'/'MMQ', 'rishadan footpad').
card_uid('rishadan footpad'/'MMQ', 'MMQ:Rishadan Footpad:rishadan footpad').
card_rarity('rishadan footpad'/'MMQ', 'Uncommon').
card_artist('rishadan footpad'/'MMQ', 'Adam Rex').
card_number('rishadan footpad'/'MMQ', '94').
card_flavor_text('rishadan footpad'/'MMQ', '\"The best things in life are free. Money, for instance.\"').
card_multiverse_id('rishadan footpad'/'MMQ', '19810').

card_in_set('rishadan pawnshop', 'MMQ').
card_original_type('rishadan pawnshop'/'MMQ', 'Artifact').
card_original_text('rishadan pawnshop'/'MMQ', '{2}, {T}: Shuffle target card in play you control into its owner\'s library.').
card_first_print('rishadan pawnshop', 'MMQ').
card_image_name('rishadan pawnshop'/'MMQ', 'rishadan pawnshop').
card_uid('rishadan pawnshop'/'MMQ', 'MMQ:Rishadan Pawnshop:rishadan pawnshop').
card_rarity('rishadan pawnshop'/'MMQ', 'Rare').
card_artist('rishadan pawnshop'/'MMQ', 'Joel Biske').
card_number('rishadan pawnshop'/'MMQ', '311').
card_flavor_text('rishadan pawnshop'/'MMQ', 'Everything comes to the waterfront eventually, on the tide or on credit.').
card_multiverse_id('rishadan pawnshop'/'MMQ', '19757').

card_in_set('rishadan port', 'MMQ').
card_original_type('rishadan port'/'MMQ', 'Land').
card_original_text('rishadan port'/'MMQ', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Tap target land.').
card_first_print('rishadan port', 'MMQ').
card_image_name('rishadan port'/'MMQ', 'rishadan port').
card_uid('rishadan port'/'MMQ', 'MMQ:Rishadan Port:rishadan port').
card_rarity('rishadan port'/'MMQ', 'Rare').
card_artist('rishadan port'/'MMQ', 'Jerry Tiritilli').
card_number('rishadan port'/'MMQ', '324').
card_flavor_text('rishadan port'/'MMQ', 'Rishada is the gateway to free trade—but the key will cost you.').
card_multiverse_id('rishadan port'/'MMQ', '19767').

card_in_set('robber fly', 'MMQ').
card_original_type('robber fly'/'MMQ', 'Creature — Insect').
card_original_text('robber fly'/'MMQ', 'Flying\nWhenever Robber Fly becomes blocked, defending player discards his or her hand, then draws that many cards.').
card_first_print('robber fly', 'MMQ').
card_image_name('robber fly'/'MMQ', 'robber fly').
card_uid('robber fly'/'MMQ', 'MMQ:Robber Fly:robber fly').
card_rarity('robber fly'/'MMQ', 'Uncommon').
card_artist('robber fly'/'MMQ', 'John Matson').
card_number('robber fly'/'MMQ', '209').
card_flavor_text('robber fly'/'MMQ', 'In pursuit of the juicy fly, Squee stumbled on the hidden hangar.').
card_multiverse_id('robber fly'/'MMQ', '19719').

card_in_set('rock badger', 'MMQ').
card_original_type('rock badger'/'MMQ', 'Creature — Beast').
card_original_text('rock badger'/'MMQ', 'Mountainwalk (This creature is unblockable as long as defending player controls a mountain.)').
card_first_print('rock badger', 'MMQ').
card_image_name('rock badger'/'MMQ', 'rock badger').
card_uid('rock badger'/'MMQ', 'MMQ:Rock Badger:rock badger').
card_rarity('rock badger'/'MMQ', 'Uncommon').
card_artist('rock badger'/'MMQ', 'Heather Hudson').
card_number('rock badger'/'MMQ', '210').
card_flavor_text('rock badger'/'MMQ', 'The Kyren goblins created most of the inverted mountain\'s tunnels simply by driving the beast before them.').
card_multiverse_id('rock badger'/'MMQ', '19840').

card_in_set('rouse', 'MMQ').
card_original_type('rouse'/'MMQ', 'Instant').
card_original_text('rouse'/'MMQ', 'If you control a swamp, you may pay 2 life instead of paying Rouse\'s mana cost.\nTarget creature gets +2/+0 until end of turn.').
card_first_print('rouse', 'MMQ').
card_image_name('rouse'/'MMQ', 'rouse').
card_uid('rouse'/'MMQ', 'MMQ:Rouse:rouse').
card_rarity('rouse'/'MMQ', 'Common').
card_artist('rouse'/'MMQ', 'Dave Dorman').
card_number('rouse'/'MMQ', '157').
card_multiverse_id('rouse'/'MMQ', '19700').

card_in_set('rushwood dryad', 'MMQ').
card_original_type('rushwood dryad'/'MMQ', 'Creature — Dryad').
card_original_text('rushwood dryad'/'MMQ', 'Forestwalk (This creature is unblockable as long as defending player controls a forest.)').
card_first_print('rushwood dryad', 'MMQ').
card_image_name('rushwood dryad'/'MMQ', 'rushwood dryad').
card_uid('rushwood dryad'/'MMQ', 'MMQ:Rushwood Dryad:rushwood dryad').
card_rarity('rushwood dryad'/'MMQ', 'Common').
card_artist('rushwood dryad'/'MMQ', 'Todd Lockwood').
card_number('rushwood dryad'/'MMQ', '263').
card_flavor_text('rushwood dryad'/'MMQ', 'Rushwood is a living jewel that reflects its dwellers\' hearts.').
card_multiverse_id('rushwood dryad'/'MMQ', '19627').

card_in_set('rushwood elemental', 'MMQ').
card_original_type('rushwood elemental'/'MMQ', 'Creature — Elemental').
card_original_text('rushwood elemental'/'MMQ', 'Trample\nAt the beginning of your upkeep, you may put a +1/+1 counter on Rushwood Elemental.').
card_first_print('rushwood elemental', 'MMQ').
card_image_name('rushwood elemental'/'MMQ', 'rushwood elemental').
card_uid('rushwood elemental'/'MMQ', 'MMQ:Rushwood Elemental:rushwood elemental').
card_rarity('rushwood elemental'/'MMQ', 'Rare').
card_artist('rushwood elemental'/'MMQ', 'Hannibal King').
card_number('rushwood elemental'/'MMQ', '264').
card_multiverse_id('rushwood elemental'/'MMQ', '19869').

card_in_set('rushwood grove', 'MMQ').
card_original_type('rushwood grove'/'MMQ', 'Land').
card_original_text('rushwood grove'/'MMQ', 'Rushwood Grove comes into play tapped.\n{T}: Put a storage counter on Rushwood Grove.\n{T}, Remove any number of storage counters from Rushwood Grove: Add one green mana to your mana pool for each storage counter removed this way.').
card_first_print('rushwood grove', 'MMQ').
card_image_name('rushwood grove'/'MMQ', 'rushwood grove').
card_uid('rushwood grove'/'MMQ', 'MMQ:Rushwood Grove:rushwood grove').
card_rarity('rushwood grove'/'MMQ', 'Uncommon').
card_artist('rushwood grove'/'MMQ', 'George Pratt').
card_number('rushwood grove'/'MMQ', '325').
card_multiverse_id('rushwood grove'/'MMQ', '19895').

card_in_set('rushwood herbalist', 'MMQ').
card_original_type('rushwood herbalist'/'MMQ', 'Creature — Spellshaper').
card_original_text('rushwood herbalist'/'MMQ', '{G}, {T}, Discard a card from your hand: Regenerate target creature.').
card_first_print('rushwood herbalist', 'MMQ').
card_image_name('rushwood herbalist'/'MMQ', 'rushwood herbalist').
card_uid('rushwood herbalist'/'MMQ', 'MMQ:Rushwood Herbalist:rushwood herbalist').
card_rarity('rushwood herbalist'/'MMQ', 'Common').
card_artist('rushwood herbalist'/'MMQ', 'Terese Nielsen').
card_number('rushwood herbalist'/'MMQ', '265').
card_flavor_text('rushwood herbalist'/'MMQ', 'When generals gather their armies, healers gather their herbs.').
card_multiverse_id('rushwood herbalist'/'MMQ', '19622').

card_in_set('rushwood legate', 'MMQ').
card_original_type('rushwood legate'/'MMQ', 'Creature — Dryad').
card_original_text('rushwood legate'/'MMQ', 'If an opponent controls an island and you control a forest, you may play Rushwood Legate without paying its mana cost.').
card_first_print('rushwood legate', 'MMQ').
card_image_name('rushwood legate'/'MMQ', 'rushwood legate').
card_uid('rushwood legate'/'MMQ', 'MMQ:Rushwood Legate:rushwood legate').
card_rarity('rushwood legate'/'MMQ', 'Uncommon').
card_artist('rushwood legate'/'MMQ', 'Mark Romanoski').
card_number('rushwood legate'/'MMQ', '266').
card_multiverse_id('rushwood legate'/'MMQ', '19864').

card_in_set('saber ants', 'MMQ').
card_original_type('saber ants'/'MMQ', 'Creature — Insect').
card_original_text('saber ants'/'MMQ', 'Whenever Saber Ants is dealt damage, you may put that many 1/1 green Insect creature tokens into play.').
card_first_print('saber ants', 'MMQ').
card_image_name('saber ants'/'MMQ', 'saber ants').
card_uid('saber ants'/'MMQ', 'MMQ:Saber Ants:saber ants').
card_rarity('saber ants'/'MMQ', 'Uncommon').
card_artist('saber ants'/'MMQ', 'Greg Staples').
card_number('saber ants'/'MMQ', '267').
card_flavor_text('saber ants'/'MMQ', 'They stay on the battlefield only long enough to consume the losers.').
card_multiverse_id('saber ants'/'MMQ', '19862').

card_in_set('sacred prey', 'MMQ').
card_original_type('sacred prey'/'MMQ', 'Creature — Beast').
card_original_text('sacred prey'/'MMQ', 'When Sacred Prey becomes blocked, you gain 1 life.').
card_first_print('sacred prey', 'MMQ').
card_image_name('sacred prey'/'MMQ', 'sacred prey').
card_uid('sacred prey'/'MMQ', 'MMQ:Sacred Prey:sacred prey').
card_rarity('sacred prey'/'MMQ', 'Common').
card_artist('sacred prey'/'MMQ', 'Rebecca Guay').
card_number('sacred prey'/'MMQ', '268').
card_flavor_text('sacred prey'/'MMQ', 'To see one is a good omen to the Cho-Arrim.').
card_multiverse_id('sacred prey'/'MMQ', '19738').

card_in_set('sailmonger', 'MMQ').
card_original_type('sailmonger'/'MMQ', 'Creature — Monger').
card_original_text('sailmonger'/'MMQ', '{2}: Target creature gains flying until end of turn. Any player may play this ability.').
card_first_print('sailmonger', 'MMQ').
card_image_name('sailmonger'/'MMQ', 'sailmonger').
card_uid('sailmonger'/'MMQ', 'MMQ:Sailmonger:sailmonger').
card_rarity('sailmonger'/'MMQ', 'Uncommon').
card_artist('sailmonger'/'MMQ', 'Michael Sutfin').
card_number('sailmonger'/'MMQ', '95').
card_flavor_text('sailmonger'/'MMQ', 'Clients who cheat the sailmonger make an impact on the rest.').
card_multiverse_id('sailmonger'/'MMQ', '19796').

card_in_set('sand squid', 'MMQ').
card_original_type('sand squid'/'MMQ', 'Creature — Beast').
card_original_text('sand squid'/'MMQ', 'Islandwalk (This creature is unblockable as long as defending player controls an island.)\nYou may choose not to untap Sand Squid during your untap step.\n{T}: Tap target creature. That creature does not untap during its controller\'s untap step as long as Sand Squid remains tapped.').
card_first_print('sand squid', 'MMQ').
card_image_name('sand squid'/'MMQ', 'sand squid').
card_uid('sand squid'/'MMQ', 'MMQ:Sand Squid:sand squid').
card_rarity('sand squid'/'MMQ', 'Rare').
card_artist('sand squid'/'MMQ', 'Kev Walker').
card_number('sand squid'/'MMQ', '96').
card_multiverse_id('sand squid'/'MMQ', '19668').

card_in_set('sandstone needle', 'MMQ').
card_original_type('sandstone needle'/'MMQ', 'Land').
card_original_text('sandstone needle'/'MMQ', 'Sandstone Needle comes into play tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Sandstone Needle: Add two red mana to your mana pool. If there are no depletion counters on Sandstone Needle, sacrifice it.').
card_first_print('sandstone needle', 'MMQ').
card_image_name('sandstone needle'/'MMQ', 'sandstone needle').
card_uid('sandstone needle'/'MMQ', 'MMQ:Sandstone Needle:sandstone needle').
card_rarity('sandstone needle'/'MMQ', 'Common').
card_artist('sandstone needle'/'MMQ', 'Alan Rabinowitz').
card_number('sandstone needle'/'MMQ', '326').
card_multiverse_id('sandstone needle'/'MMQ', '19645').

card_in_set('saprazzan bailiff', 'MMQ').
card_original_type('saprazzan bailiff'/'MMQ', 'Creature — Merfolk').
card_original_text('saprazzan bailiff'/'MMQ', 'When Saprazzan Bailiff comes into play, remove all artifact and enchantment cards in all graveyards from the game.\nWhen Saprazzan Bailiff leaves play, return all artifact and enchantment cards from all graveyards to their owners\' hands.').
card_first_print('saprazzan bailiff', 'MMQ').
card_image_name('saprazzan bailiff'/'MMQ', 'saprazzan bailiff').
card_uid('saprazzan bailiff'/'MMQ', 'MMQ:Saprazzan Bailiff:saprazzan bailiff').
card_rarity('saprazzan bailiff'/'MMQ', 'Rare').
card_artist('saprazzan bailiff'/'MMQ', 'Ron Spencer').
card_number('saprazzan bailiff'/'MMQ', '97').
card_multiverse_id('saprazzan bailiff'/'MMQ', '19669').

card_in_set('saprazzan breaker', 'MMQ').
card_original_type('saprazzan breaker'/'MMQ', 'Creature — Beast').
card_original_text('saprazzan breaker'/'MMQ', '{U} Put the top card of your library into your graveyard. If that card is a land card, Saprazzan Breaker is unblockable this turn.').
card_first_print('saprazzan breaker', 'MMQ').
card_image_name('saprazzan breaker'/'MMQ', 'saprazzan breaker').
card_uid('saprazzan breaker'/'MMQ', 'MMQ:Saprazzan Breaker:saprazzan breaker').
card_rarity('saprazzan breaker'/'MMQ', 'Uncommon').
card_artist('saprazzan breaker'/'MMQ', 'Pete Venters').
card_number('saprazzan breaker'/'MMQ', '98').
card_multiverse_id('saprazzan breaker'/'MMQ', '19671').

card_in_set('saprazzan cove', 'MMQ').
card_original_type('saprazzan cove'/'MMQ', 'Land').
card_original_text('saprazzan cove'/'MMQ', 'Saprazzan Cove comes into play tapped.\n{T}: Put a storage counter on Saprazzan Cove.\n{T}, Remove any number of storage counters from Saprazzan Cove: Add one blue mana to your mana pool for each storage counter removed this way.').
card_first_print('saprazzan cove', 'MMQ').
card_image_name('saprazzan cove'/'MMQ', 'saprazzan cove').
card_uid('saprazzan cove'/'MMQ', 'MMQ:Saprazzan Cove:saprazzan cove').
card_rarity('saprazzan cove'/'MMQ', 'Uncommon').
card_artist('saprazzan cove'/'MMQ', 'Rebecca Guay').
card_number('saprazzan cove'/'MMQ', '327').
card_multiverse_id('saprazzan cove'/'MMQ', '19891').

card_in_set('saprazzan heir', 'MMQ').
card_original_type('saprazzan heir'/'MMQ', 'Creature — Merfolk').
card_original_text('saprazzan heir'/'MMQ', 'Whenever Saprazzan Heir becomes blocked, you may draw three cards.').
card_first_print('saprazzan heir', 'MMQ').
card_image_name('saprazzan heir'/'MMQ', 'saprazzan heir').
card_uid('saprazzan heir'/'MMQ', 'MMQ:Saprazzan Heir:saprazzan heir').
card_rarity('saprazzan heir'/'MMQ', 'Rare').
card_artist('saprazzan heir'/'MMQ', 'Terese Nielsen').
card_number('saprazzan heir'/'MMQ', '99').
card_flavor_text('saprazzan heir'/'MMQ', 'Within the walls of the floating city, Saprazzan wealth is like Saprazzan water: abundant, free-flowing, and accessible to all.').
card_multiverse_id('saprazzan heir'/'MMQ', '19672').

card_in_set('saprazzan legate', 'MMQ').
card_original_type('saprazzan legate'/'MMQ', 'Creature — Soldier').
card_original_text('saprazzan legate'/'MMQ', 'Flying\nIf an opponent controls a mountain and you control an island, you may play Saprazzan Legate without paying its mana cost.').
card_first_print('saprazzan legate', 'MMQ').
card_image_name('saprazzan legate'/'MMQ', 'saprazzan legate').
card_uid('saprazzan legate'/'MMQ', 'MMQ:Saprazzan Legate:saprazzan legate').
card_rarity('saprazzan legate'/'MMQ', 'Uncommon').
card_artist('saprazzan legate'/'MMQ', 'Andrew Goldhawk').
card_number('saprazzan legate'/'MMQ', '100').
card_multiverse_id('saprazzan legate'/'MMQ', '19799').

card_in_set('saprazzan outrigger', 'MMQ').
card_original_type('saprazzan outrigger'/'MMQ', 'Creature — Ship').
card_original_text('saprazzan outrigger'/'MMQ', 'When Saprazzan Outrigger attacks or blocks, put it on top of its owner\'s library at end of combat.').
card_first_print('saprazzan outrigger', 'MMQ').
card_image_name('saprazzan outrigger'/'MMQ', 'saprazzan outrigger').
card_uid('saprazzan outrigger'/'MMQ', 'MMQ:Saprazzan Outrigger:saprazzan outrigger').
card_rarity('saprazzan outrigger'/'MMQ', 'Common').
card_artist('saprazzan outrigger'/'MMQ', 'Doug Chaffee').
card_number('saprazzan outrigger'/'MMQ', '101').
card_flavor_text('saprazzan outrigger'/'MMQ', 'The Saprazzans excel at both tactics: hit and run.').
card_multiverse_id('saprazzan outrigger'/'MMQ', '21773').

card_in_set('saprazzan raider', 'MMQ').
card_original_type('saprazzan raider'/'MMQ', 'Creature — Merfolk').
card_original_text('saprazzan raider'/'MMQ', 'When Saprazzan Raider becomes blocked, return it to its owner\'s hand.').
card_first_print('saprazzan raider', 'MMQ').
card_image_name('saprazzan raider'/'MMQ', 'saprazzan raider').
card_uid('saprazzan raider'/'MMQ', 'MMQ:Saprazzan Raider:saprazzan raider').
card_rarity('saprazzan raider'/'MMQ', 'Common').
card_artist('saprazzan raider'/'MMQ', 'Jeff Miracola').
card_number('saprazzan raider'/'MMQ', '102').
card_flavor_text('saprazzan raider'/'MMQ', 'Some quick dagger work and then back to the sea—that\'s the Saprazzan way.').
card_multiverse_id('saprazzan raider'/'MMQ', '19564').

card_in_set('saprazzan skerry', 'MMQ').
card_original_type('saprazzan skerry'/'MMQ', 'Land').
card_original_text('saprazzan skerry'/'MMQ', 'Saprazzan Skerry comes into play tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Saprazzan Skerry: Add two blue mana to your mana pool. If there are no depletion counters on Saprazzan Skerry, sacrifice it.').
card_first_print('saprazzan skerry', 'MMQ').
card_image_name('saprazzan skerry'/'MMQ', 'saprazzan skerry').
card_uid('saprazzan skerry'/'MMQ', 'MMQ:Saprazzan Skerry:saprazzan skerry').
card_rarity('saprazzan skerry'/'MMQ', 'Common').
card_artist('saprazzan skerry'/'MMQ', 'Pat Morrissey').
card_number('saprazzan skerry'/'MMQ', '328').
card_multiverse_id('saprazzan skerry'/'MMQ', '19643').

card_in_set('scandalmonger', 'MMQ').
card_original_type('scandalmonger'/'MMQ', 'Creature — Monger').
card_original_text('scandalmonger'/'MMQ', '{2}: Target player discards a card from his or her hand. Any player may play this ability but only if he or she could play a sorcery.').
card_first_print('scandalmonger', 'MMQ').
card_image_name('scandalmonger'/'MMQ', 'scandalmonger').
card_uid('scandalmonger'/'MMQ', 'MMQ:Scandalmonger:scandalmonger').
card_rarity('scandalmonger'/'MMQ', 'Uncommon').
card_artist('scandalmonger'/'MMQ', 'Matt Cavotta').
card_number('scandalmonger'/'MMQ', '158').
card_flavor_text('scandalmonger'/'MMQ', 'Silence costs extra. Breaking it costs extra still.').
card_multiverse_id('scandalmonger'/'MMQ', '19816').

card_in_set('security detail', 'MMQ').
card_original_type('security detail'/'MMQ', 'Enchantment').
card_original_text('security detail'/'MMQ', '{W}{W} Put a 1/1 white Soldier creature token into play. Play this ability only if you control no creatures and only once each turn.').
card_first_print('security detail', 'MMQ').
card_image_name('security detail'/'MMQ', 'security detail').
card_uid('security detail'/'MMQ', 'MMQ:Security Detail:security detail').
card_rarity('security detail'/'MMQ', 'Rare').
card_artist('security detail'/'MMQ', 'Val Mayerik').
card_number('security detail'/'MMQ', '47').
card_multiverse_id('security detail'/'MMQ', '19655').

card_in_set('seismic mage', 'MMQ').
card_original_type('seismic mage'/'MMQ', 'Creature — Spellshaper').
card_original_text('seismic mage'/'MMQ', '{2}{R}, {T}, Discard a card from your hand: Destroy target land.').
card_first_print('seismic mage', 'MMQ').
card_image_name('seismic mage'/'MMQ', 'seismic mage').
card_uid('seismic mage'/'MMQ', 'MMQ:Seismic Mage:seismic mage').
card_rarity('seismic mage'/'MMQ', 'Rare').
card_artist('seismic mage'/'MMQ', 'Pete Venters').
card_number('seismic mage'/'MMQ', '211').
card_flavor_text('seismic mage'/'MMQ', 'The ground shakes when he walks. His customers shake if they\'re late with his fee.').
card_multiverse_id('seismic mage'/'MMQ', '19835').

card_in_set('sever soul', 'MMQ').
card_original_type('sever soul'/'MMQ', 'Sorcery').
card_original_text('sever soul'/'MMQ', 'Destroy target nonblack creature. It can\'t be regenerated. You gain life equal to its toughness.').
card_first_print('sever soul', 'MMQ').
card_image_name('sever soul'/'MMQ', 'sever soul').
card_uid('sever soul'/'MMQ', 'MMQ:Sever Soul:sever soul').
card_rarity('sever soul'/'MMQ', 'Common').
card_artist('sever soul'/'MMQ', 'Jeff Easley').
card_number('sever soul'/'MMQ', '159').
card_flavor_text('sever soul'/'MMQ', '\"With the proper tools, even the Great River can be dammed.\"\n—Ta-Spon, Cho-Arrim executioner').
card_multiverse_id('sever soul'/'MMQ', '19591').

card_in_set('shock troops', 'MMQ').
card_original_type('shock troops'/'MMQ', 'Creature — Soldier').
card_original_text('shock troops'/'MMQ', 'Sacrifice Shock Troops: Shock Troops deals 2 damage to target creature or player.').
card_first_print('shock troops', 'MMQ').
card_image_name('shock troops'/'MMQ', 'shock troops').
card_uid('shock troops'/'MMQ', 'MMQ:Shock Troops:shock troops').
card_rarity('shock troops'/'MMQ', 'Common').
card_artist('shock troops'/'MMQ', 'Jeff Miracola').
card_number('shock troops'/'MMQ', '212').
card_flavor_text('shock troops'/'MMQ', 'When goblins blow themselves up, it\'s called stupidity. When humans do, it\'s called heroism.').
card_multiverse_id('shock troops'/'MMQ', '19609').

card_in_set('shoving match', 'MMQ').
card_original_type('shoving match'/'MMQ', 'Instant').
card_original_text('shoving match'/'MMQ', 'Until end of turn, all creatures gain \"{T}: Tap target creature.\"').
card_first_print('shoving match', 'MMQ').
card_image_name('shoving match'/'MMQ', 'shoving match').
card_uid('shoving match'/'MMQ', 'MMQ:Shoving Match:shoving match').
card_rarity('shoving match'/'MMQ', 'Uncommon').
card_artist('shoving match'/'MMQ', 'Dave Dorman').
card_number('shoving match'/'MMQ', '103').
card_flavor_text('shoving match'/'MMQ', 'Rishadan barkeeps know something the Mercadians have yet to learn: if you keep pushing, someone will eventually push back.').
card_multiverse_id('shoving match'/'MMQ', '19677').

card_in_set('silent assassin', 'MMQ').
card_original_type('silent assassin'/'MMQ', 'Creature — Mercenary').
card_original_text('silent assassin'/'MMQ', '{3}{B}: Destroy target blocking creature at end of combat.').
card_first_print('silent assassin', 'MMQ').
card_image_name('silent assassin'/'MMQ', 'silent assassin').
card_uid('silent assassin'/'MMQ', 'MMQ:Silent Assassin:silent assassin').
card_rarity('silent assassin'/'MMQ', 'Rare').
card_artist('silent assassin'/'MMQ', 'rk post').
card_number('silent assassin'/'MMQ', '160').
card_flavor_text('silent assassin'/'MMQ', 'She feels no need to disturb her victim\'s slumber.').
card_multiverse_id('silent assassin'/'MMQ', '19692').

card_in_set('silverglade elemental', 'MMQ').
card_original_type('silverglade elemental'/'MMQ', 'Creature — Elemental').
card_original_text('silverglade elemental'/'MMQ', 'When Silverglade Elemental comes into play, you may search your library for a forest card and put that card into play. If you do, shuffle your library.').
card_first_print('silverglade elemental', 'MMQ').
card_image_name('silverglade elemental'/'MMQ', 'silverglade elemental').
card_uid('silverglade elemental'/'MMQ', 'MMQ:Silverglade Elemental:silverglade elemental').
card_rarity('silverglade elemental'/'MMQ', 'Common').
card_artist('silverglade elemental'/'MMQ', 'Chippy').
card_number('silverglade elemental'/'MMQ', '269').
card_multiverse_id('silverglade elemental'/'MMQ', '19631').

card_in_set('silverglade pathfinder', 'MMQ').
card_original_type('silverglade pathfinder'/'MMQ', 'Creature — Spellshaper').
card_original_text('silverglade pathfinder'/'MMQ', '{1}{G}, {T}, Discard a card from your hand: Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('silverglade pathfinder', 'MMQ').
card_image_name('silverglade pathfinder'/'MMQ', 'silverglade pathfinder').
card_uid('silverglade pathfinder'/'MMQ', 'MMQ:Silverglade Pathfinder:silverglade pathfinder').
card_rarity('silverglade pathfinder'/'MMQ', 'Uncommon').
card_artist('silverglade pathfinder'/'MMQ', 'rk post').
card_number('silverglade pathfinder'/'MMQ', '270').
card_multiverse_id('silverglade pathfinder'/'MMQ', '19728').

card_in_set('sizzle', 'MMQ').
card_original_type('sizzle'/'MMQ', 'Sorcery').
card_original_text('sizzle'/'MMQ', 'Sizzle deals 3 damage to each opponent.').
card_first_print('sizzle', 'MMQ').
card_image_name('sizzle'/'MMQ', 'sizzle').
card_uid('sizzle'/'MMQ', 'MMQ:Sizzle:sizzle').
card_rarity('sizzle'/'MMQ', 'Common').
card_artist('sizzle'/'MMQ', 'Brian Snõddy').
card_number('sizzle'/'MMQ', '213').
card_flavor_text('sizzle'/'MMQ', 'Explosions ripped through the ship overhead, and those unfortunate enough to be directly below scrambled to find cover.').
card_multiverse_id('sizzle'/'MMQ', '19611').

card_in_set('skulking fugitive', 'MMQ').
card_original_type('skulking fugitive'/'MMQ', 'Creature — Mercenary').
card_original_text('skulking fugitive'/'MMQ', 'When Skulking Fugitive becomes the target of a spell or ability, sacrifice Skulking Fugitive.').
card_first_print('skulking fugitive', 'MMQ').
card_image_name('skulking fugitive'/'MMQ', 'skulking fugitive').
card_uid('skulking fugitive'/'MMQ', 'MMQ:Skulking Fugitive:skulking fugitive').
card_rarity('skulking fugitive'/'MMQ', 'Common').
card_artist('skulking fugitive'/'MMQ', 'Scott M. Fischer').
card_number('skulking fugitive'/'MMQ', '161').
card_flavor_text('skulking fugitive'/'MMQ', 'In the Cateran guild, being caught is a capital offense.').
card_multiverse_id('skulking fugitive'/'MMQ', '19588').

card_in_set('skull of ramos', 'MMQ').
card_original_type('skull of ramos'/'MMQ', 'Artifact').
card_original_text('skull of ramos'/'MMQ', '{T}: Add one black mana to your mana pool. \nSacrifice Skull of Ramos: Add one black mana to your mana pool.').
card_first_print('skull of ramos', 'MMQ').
card_image_name('skull of ramos'/'MMQ', 'skull of ramos').
card_uid('skull of ramos'/'MMQ', 'MMQ:Skull of Ramos:skull of ramos').
card_rarity('skull of ramos'/'MMQ', 'Rare').
card_artist('skull of ramos'/'MMQ', 'David Martin').
card_number('skull of ramos'/'MMQ', '312').
card_flavor_text('skull of ramos'/'MMQ', 'Ramos fell, and there was night.').
card_multiverse_id('skull of ramos'/'MMQ', '19764').

card_in_set('snake pit', 'MMQ').
card_original_type('snake pit'/'MMQ', 'Enchantment').
card_original_text('snake pit'/'MMQ', 'Whenever an opponent plays a blue or black spell, you may put a 1/1 green Snake creature token into play.').
card_first_print('snake pit', 'MMQ').
card_image_name('snake pit'/'MMQ', 'snake pit').
card_uid('snake pit'/'MMQ', 'MMQ:Snake Pit:snake pit').
card_rarity('snake pit'/'MMQ', 'Uncommon').
card_artist('snake pit'/'MMQ', 'Carl Critchlow').
card_number('snake pit'/'MMQ', '271').
card_flavor_text('snake pit'/'MMQ', 'If the ground is smooth, the way will be rough.\n—Deepwood saying').
card_multiverse_id('snake pit'/'MMQ', '19876').

card_in_set('snorting gahr', 'MMQ').
card_original_type('snorting gahr'/'MMQ', 'Creature — Beast').
card_original_text('snorting gahr'/'MMQ', 'Whenever Snorting Gahr becomes blocked, it gets +2/+2 until end of turn.').
card_first_print('snorting gahr', 'MMQ').
card_image_name('snorting gahr'/'MMQ', 'snorting gahr').
card_uid('snorting gahr'/'MMQ', 'MMQ:Snorting Gahr:snorting gahr').
card_rarity('snorting gahr'/'MMQ', 'Common').
card_artist('snorting gahr'/'MMQ', 'Andrew Goldhawk').
card_number('snorting gahr'/'MMQ', '272').
card_flavor_text('snorting gahr'/'MMQ', 'There\'s little advantage to surprising the gahr.').
card_multiverse_id('snorting gahr'/'MMQ', '19626').

card_in_set('snuff out', 'MMQ').
card_original_type('snuff out'/'MMQ', 'Instant').
card_original_text('snuff out'/'MMQ', 'If you control a swamp, you may pay 4 life instead of paying Snuff Out\'s mana cost.\nDestroy target nonblack creature. It can\'t be regenerated.').
card_first_print('snuff out', 'MMQ').
card_image_name('snuff out'/'MMQ', 'snuff out').
card_uid('snuff out'/'MMQ', 'MMQ:Snuff Out:snuff out').
card_rarity('snuff out'/'MMQ', 'Common').
card_artist('snuff out'/'MMQ', 'Mike Ploog').
card_number('snuff out'/'MMQ', '162').
card_flavor_text('snuff out'/'MMQ', 'Squee watched his Kyren cousins fall with a mixture of sympathy and relief.').
card_multiverse_id('snuff out'/'MMQ', '19595').

card_in_set('soothing balm', 'MMQ').
card_original_type('soothing balm'/'MMQ', 'Instant').
card_original_text('soothing balm'/'MMQ', 'Target player gains 5 life.').
card_first_print('soothing balm', 'MMQ').
card_image_name('soothing balm'/'MMQ', 'soothing balm').
card_uid('soothing balm'/'MMQ', 'MMQ:Soothing Balm:soothing balm').
card_rarity('soothing balm'/'MMQ', 'Common').
card_artist('soothing balm'/'MMQ', 'Scott M. Fischer').
card_number('soothing balm'/'MMQ', '48').
card_flavor_text('soothing balm'/'MMQ', 'Orim taught Ta-Karnst and the other Cho-Arrim healers a far less invasive method of healing.').
card_multiverse_id('soothing balm'/'MMQ', '19550').

card_in_set('soothsaying', 'MMQ').
card_original_type('soothsaying'/'MMQ', 'Enchantment').
card_original_text('soothsaying'/'MMQ', '{3}{U}{U} Shuffle your library.\n{X} Look at the top X cards of your library and put them back in any order.').
card_first_print('soothsaying', 'MMQ').
card_image_name('soothsaying'/'MMQ', 'soothsaying').
card_uid('soothsaying'/'MMQ', 'MMQ:Soothsaying:soothsaying').
card_rarity('soothsaying'/'MMQ', 'Uncommon').
card_artist('soothsaying'/'MMQ', 'Pat Morrissey').
card_number('soothsaying'/'MMQ', '104').
card_flavor_text('soothsaying'/'MMQ', 'The vizier\'s wisdom proved invaluable in untangling the Mercadian knot of lies.').
card_multiverse_id('soothsaying'/'MMQ', '19807').

card_in_set('soul channeling', 'MMQ').
card_original_type('soul channeling'/'MMQ', 'Enchant Creature').
card_original_text('soul channeling'/'MMQ', 'Pay 2 life: Regenerate enchanted creature.').
card_first_print('soul channeling', 'MMQ').
card_image_name('soul channeling'/'MMQ', 'soul channeling').
card_uid('soul channeling'/'MMQ', 'MMQ:Soul Channeling:soul channeling').
card_rarity('soul channeling'/'MMQ', 'Common').
card_artist('soul channeling'/'MMQ', 'DiTerlizzi').
card_number('soul channeling'/'MMQ', '163').
card_flavor_text('soul channeling'/'MMQ', 'Driven by masters more terrible than the Mercadians could imagine, the dark overseers would pay any price to keep their shipwrights working.').
card_multiverse_id('soul channeling'/'MMQ', '19593').

card_in_set('specter\'s wail', 'MMQ').
card_original_type('specter\'s wail'/'MMQ', 'Sorcery').
card_original_text('specter\'s wail'/'MMQ', 'Target player discards a card at random from his or her hand.').
card_first_print('specter\'s wail', 'MMQ').
card_image_name('specter\'s wail'/'MMQ', 'specter\'s wail').
card_uid('specter\'s wail'/'MMQ', 'MMQ:Specter\'s Wail:specter\'s wail').
card_rarity('specter\'s wail'/'MMQ', 'Common').
card_artist('specter\'s wail'/'MMQ', 'Randy Gallegos').
card_number('specter\'s wail'/'MMQ', '164').
card_flavor_text('specter\'s wail'/'MMQ', 'As he gritted his teeth to focus through the pain, Tahngarth glanced at Takara. She seemed completely unaffected.').
card_multiverse_id('specter\'s wail'/'MMQ', '19597').

card_in_set('spidersilk armor', 'MMQ').
card_original_type('spidersilk armor'/'MMQ', 'Enchantment').
card_original_text('spidersilk armor'/'MMQ', 'Creatures you control get +0/+1 and may block as though they had flying.').
card_first_print('spidersilk armor', 'MMQ').
card_image_name('spidersilk armor'/'MMQ', 'spidersilk armor').
card_uid('spidersilk armor'/'MMQ', 'MMQ:Spidersilk Armor:spidersilk armor').
card_rarity('spidersilk armor'/'MMQ', 'Common').
card_artist('spidersilk armor'/'MMQ', 'Scott Hampton').
card_number('spidersilk armor'/'MMQ', '273').
card_flavor_text('spidersilk armor'/'MMQ', 'It hardly weighs anything, but it takes all day to remove.').
card_multiverse_id('spidersilk armor'/'MMQ', '19870').

card_in_set('spiritual focus', 'MMQ').
card_original_type('spiritual focus'/'MMQ', 'Enchantment').
card_original_text('spiritual focus'/'MMQ', 'Whenever a spell or ability an opponent controls causes you to discard a card, you gain 2 life and you may draw a card.').
card_first_print('spiritual focus', 'MMQ').
card_image_name('spiritual focus'/'MMQ', 'spiritual focus').
card_uid('spiritual focus'/'MMQ', 'MMQ:Spiritual Focus:spiritual focus').
card_rarity('spiritual focus'/'MMQ', 'Rare').
card_artist('spiritual focus'/'MMQ', 'Andrew Goldhawk').
card_number('spiritual focus'/'MMQ', '49').
card_multiverse_id('spiritual focus'/'MMQ', '19657').

card_in_set('spontaneous generation', 'MMQ').
card_original_type('spontaneous generation'/'MMQ', 'Sorcery').
card_original_text('spontaneous generation'/'MMQ', 'Put a 1/1 green Saproling creature token into play for each card in your hand.').
card_first_print('spontaneous generation', 'MMQ').
card_image_name('spontaneous generation'/'MMQ', 'spontaneous generation').
card_uid('spontaneous generation'/'MMQ', 'MMQ:Spontaneous Generation:spontaneous generation').
card_rarity('spontaneous generation'/'MMQ', 'Rare').
card_artist('spontaneous generation'/'MMQ', 'Alan Pollack').
card_number('spontaneous generation'/'MMQ', '274').
card_flavor_text('spontaneous generation'/'MMQ', 'Saprolings always thrive on the ruins of others, and Mercadia ruins more than most.').
card_multiverse_id('spontaneous generation'/'MMQ', '19871').

card_in_set('squall', 'MMQ').
card_original_type('squall'/'MMQ', 'Sorcery').
card_original_text('squall'/'MMQ', 'Squall deals 2 damage to each creature with flying.').
card_image_name('squall'/'MMQ', 'squall').
card_uid('squall'/'MMQ', 'MMQ:Squall:squall').
card_rarity('squall'/'MMQ', 'Common').
card_artist('squall'/'MMQ', 'Val Mayerik').
card_number('squall'/'MMQ', '275').
card_flavor_text('squall'/'MMQ', 'The air that holds them aloft can just as easily put them down.').
card_multiverse_id('squall'/'MMQ', '19636').

card_in_set('squallmonger', 'MMQ').
card_original_type('squallmonger'/'MMQ', 'Creature — Monger').
card_original_text('squallmonger'/'MMQ', '{2}: Squallmonger deals 1 damage to each creature with flying and each player. Any player may play this ability.').
card_first_print('squallmonger', 'MMQ').
card_image_name('squallmonger'/'MMQ', 'squallmonger').
card_uid('squallmonger'/'MMQ', 'MMQ:Squallmonger:squallmonger').
card_rarity('squallmonger'/'MMQ', 'Uncommon').
card_artist('squallmonger'/'MMQ', 'Heather Hudson').
card_number('squallmonger'/'MMQ', '276').
card_flavor_text('squallmonger'/'MMQ', '\"Squall, gale, hurricane—it\'s all a matter of size . . . and price.\"').
card_multiverse_id('squallmonger'/'MMQ', '19856').

card_in_set('squee, goblin nabob', 'MMQ').
card_original_type('squee, goblin nabob'/'MMQ', 'Creature — Goblin Legend').
card_original_text('squee, goblin nabob'/'MMQ', 'At the beginning of your upkeep, if Squee, Goblin Nabob is in your graveyard, you may return Squee to your hand.').
card_first_print('squee, goblin nabob', 'MMQ').
card_image_name('squee, goblin nabob'/'MMQ', 'squee, goblin nabob').
card_uid('squee, goblin nabob'/'MMQ', 'MMQ:Squee, Goblin Nabob:squee, goblin nabob').
card_rarity('squee, goblin nabob'/'MMQ', 'Rare').
card_artist('squee, goblin nabob'/'MMQ', 'David Monette').
card_number('squee, goblin nabob'/'MMQ', '214').
card_flavor_text('squee, goblin nabob'/'MMQ', '\"General?!\" Tahngarth roared. \"General nuisance, maybe.\"').
card_multiverse_id('squee, goblin nabob'/'MMQ', '19716').

card_in_set('squeeze', 'MMQ').
card_original_type('squeeze'/'MMQ', 'Enchantment').
card_original_text('squeeze'/'MMQ', 'Sorcery spells cost {3} more to play.').
card_first_print('squeeze', 'MMQ').
card_image_name('squeeze'/'MMQ', 'squeeze').
card_uid('squeeze'/'MMQ', 'MMQ:Squeeze:squeeze').
card_rarity('squeeze'/'MMQ', 'Rare').
card_artist('squeeze'/'MMQ', 'DiTerlizzi').
card_number('squeeze'/'MMQ', '105').
card_flavor_text('squeeze'/'MMQ', 'Any pirate would prefer Rishada\'s swift and cruel justice to Saprazzo\'s patient punishments.').
card_multiverse_id('squeeze'/'MMQ', '19685').

card_in_set('stamina', 'MMQ').
card_original_type('stamina'/'MMQ', 'Enchant Creature').
card_original_text('stamina'/'MMQ', 'Attacking doesn\'t cause enchanted creature to tap.\nSacrifice Stamina: Regenerate enchanted creature.').
card_first_print('stamina', 'MMQ').
card_image_name('stamina'/'MMQ', 'stamina').
card_uid('stamina'/'MMQ', 'MMQ:Stamina:stamina').
card_rarity('stamina'/'MMQ', 'Uncommon').
card_artist('stamina'/'MMQ', 'Paolo Parente').
card_number('stamina'/'MMQ', '277').
card_multiverse_id('stamina'/'MMQ', '19868').

card_in_set('statecraft', 'MMQ').
card_original_type('statecraft'/'MMQ', 'Enchantment').
card_original_text('statecraft'/'MMQ', 'Prevent all combat damage that would be dealt to and dealt by creatures you control.').
card_first_print('statecraft', 'MMQ').
card_image_name('statecraft'/'MMQ', 'statecraft').
card_uid('statecraft'/'MMQ', 'MMQ:Statecraft:statecraft').
card_rarity('statecraft'/'MMQ', 'Rare').
card_artist('statecraft'/'MMQ', 'Mike Ploog').
card_number('statecraft'/'MMQ', '106').
card_flavor_text('statecraft'/'MMQ', '\"Inside these walls, peace; outside, sharks. You choose.\"\n—Saprazzan vizier').
card_multiverse_id('statecraft'/'MMQ', '19676').

card_in_set('steadfast guard', 'MMQ').
card_original_type('steadfast guard'/'MMQ', 'Creature — Rebel').
card_original_text('steadfast guard'/'MMQ', 'Attacking doesn\'t cause Steadfast Guard to tap.').
card_first_print('steadfast guard', 'MMQ').
card_image_name('steadfast guard'/'MMQ', 'steadfast guard').
card_uid('steadfast guard'/'MMQ', 'MMQ:Steadfast Guard:steadfast guard').
card_rarity('steadfast guard'/'MMQ', 'Common').
card_artist('steadfast guard'/'MMQ', 'Adam Rex').
card_number('steadfast guard'/'MMQ', '50').
card_flavor_text('steadfast guard'/'MMQ', 'The Mercadians were prepared for the guard, but the guard was even more prepared for the Mercadians.').
card_multiverse_id('steadfast guard'/'MMQ', '19780').

card_in_set('stinging barrier', 'MMQ').
card_original_type('stinging barrier'/'MMQ', 'Creature — Wall').
card_original_text('stinging barrier'/'MMQ', '(Walls can\'t attack.)\n{U}, {T}: Stinging Barrier deals 1 damage to target creature or player.').
card_first_print('stinging barrier', 'MMQ').
card_image_name('stinging barrier'/'MMQ', 'stinging barrier').
card_uid('stinging barrier'/'MMQ', 'MMQ:Stinging Barrier:stinging barrier').
card_rarity('stinging barrier'/'MMQ', 'Common').
card_artist('stinging barrier'/'MMQ', 'Pat Morrissey').
card_number('stinging barrier'/'MMQ', '107').
card_multiverse_id('stinging barrier'/'MMQ', '19561').

card_in_set('stone rain', 'MMQ').
card_original_type('stone rain'/'MMQ', 'Sorcery').
card_original_text('stone rain'/'MMQ', 'Destroy target land.').
card_image_name('stone rain'/'MMQ', 'stone rain').
card_uid('stone rain'/'MMQ', 'MMQ:Stone Rain:stone rain').
card_rarity('stone rain'/'MMQ', 'Common').
card_artist('stone rain'/'MMQ', 'Ben Thompson').
card_number('stone rain'/'MMQ', '215').
card_flavor_text('stone rain'/'MMQ', 'The return of Ramos was foretold by devastating natural disasters and unnatural storms.').
card_multiverse_id('stone rain'/'MMQ', '19612').

card_in_set('story circle', 'MMQ').
card_original_type('story circle'/'MMQ', 'Enchantment').
card_original_text('story circle'/'MMQ', 'As Story Circle comes into play, choose a color.\n{W} The next time a source of your choice of the chosen color would deal damage to you this turn, prevent that damage.').
card_first_print('story circle', 'MMQ').
card_image_name('story circle'/'MMQ', 'story circle').
card_uid('story circle'/'MMQ', 'MMQ:Story Circle:story circle').
card_rarity('story circle'/'MMQ', 'Uncommon').
card_artist('story circle'/'MMQ', 'Bradley Williams').
card_number('story circle'/'MMQ', '51').
card_multiverse_id('story circle'/'MMQ', '20173').

card_in_set('strongarm thug', 'MMQ').
card_original_type('strongarm thug'/'MMQ', 'Creature — Mercenary').
card_original_text('strongarm thug'/'MMQ', 'When Strongarm Thug comes into play, you may return a Mercenary card from your graveyard to your hand.').
card_first_print('strongarm thug', 'MMQ').
card_image_name('strongarm thug'/'MMQ', 'strongarm thug').
card_uid('strongarm thug'/'MMQ', 'MMQ:Strongarm Thug:strongarm thug').
card_rarity('strongarm thug'/'MMQ', 'Uncommon').
card_artist('strongarm thug'/'MMQ', 'Rebecca Guay').
card_number('strongarm thug'/'MMQ', '165').
card_flavor_text('strongarm thug'/'MMQ', '\"No, I insist—let me carry all that heavy jewelry.\"').
card_multiverse_id('strongarm thug'/'MMQ', '19689').

card_in_set('subterranean hangar', 'MMQ').
card_original_type('subterranean hangar'/'MMQ', 'Land').
card_original_text('subterranean hangar'/'MMQ', 'Subterranean Hangar comes into play tapped.\n{T}: Put a storage counter on Subterranean Hangar.\n{T}, Remove any number of storage counters from Subterranean Hangar: Add one black mana to your mana pool for each storage counter removed this way.').
card_first_print('subterranean hangar', 'MMQ').
card_image_name('subterranean hangar'/'MMQ', 'subterranean hangar').
card_uid('subterranean hangar'/'MMQ', 'MMQ:Subterranean Hangar:subterranean hangar').
card_rarity('subterranean hangar'/'MMQ', 'Uncommon').
card_artist('subterranean hangar'/'MMQ', 'Matt Cavotta').
card_number('subterranean hangar'/'MMQ', '329').
card_multiverse_id('subterranean hangar'/'MMQ', '19892').

card_in_set('sustenance', 'MMQ').
card_original_type('sustenance'/'MMQ', 'Enchantment').
card_original_text('sustenance'/'MMQ', '{1}, Sacrifice a land: Target creature gets +1/+1 until end of turn.').
card_first_print('sustenance', 'MMQ').
card_image_name('sustenance'/'MMQ', 'sustenance').
card_uid('sustenance'/'MMQ', 'MMQ:Sustenance:sustenance').
card_rarity('sustenance'/'MMQ', 'Uncommon').
card_artist('sustenance'/'MMQ', 'Qiao Dafu').
card_number('sustenance'/'MMQ', '278').
card_flavor_text('sustenance'/'MMQ', 'Like the dryads, the forest itself willingly gives up some of its life for the sake of the future.').
card_multiverse_id('sustenance'/'MMQ', '19849').

card_in_set('swamp', 'MMQ').
card_original_type('swamp'/'MMQ', 'Land').
card_original_text('swamp'/'MMQ', 'B').
card_image_name('swamp'/'MMQ', 'swamp1').
card_uid('swamp'/'MMQ', 'MMQ:Swamp:swamp1').
card_rarity('swamp'/'MMQ', 'Basic Land').
card_artist('swamp'/'MMQ', 'Jeff Easley').
card_number('swamp'/'MMQ', '339').
card_multiverse_id('swamp'/'MMQ', '20888').

card_in_set('swamp', 'MMQ').
card_original_type('swamp'/'MMQ', 'Land').
card_original_text('swamp'/'MMQ', 'B').
card_image_name('swamp'/'MMQ', 'swamp2').
card_uid('swamp'/'MMQ', 'MMQ:Swamp:swamp2').
card_rarity('swamp'/'MMQ', 'Basic Land').
card_artist('swamp'/'MMQ', 'Rob Alexander').
card_number('swamp'/'MMQ', '340').
card_multiverse_id('swamp'/'MMQ', '20889').

card_in_set('swamp', 'MMQ').
card_original_type('swamp'/'MMQ', 'Land').
card_original_text('swamp'/'MMQ', 'B').
card_image_name('swamp'/'MMQ', 'swamp3').
card_uid('swamp'/'MMQ', 'MMQ:Swamp:swamp3').
card_rarity('swamp'/'MMQ', 'Basic Land').
card_artist('swamp'/'MMQ', 'Rob Alexander').
card_number('swamp'/'MMQ', '341').
card_multiverse_id('swamp'/'MMQ', '20890').

card_in_set('swamp', 'MMQ').
card_original_type('swamp'/'MMQ', 'Land').
card_original_text('swamp'/'MMQ', 'B').
card_image_name('swamp'/'MMQ', 'swamp4').
card_uid('swamp'/'MMQ', 'MMQ:Swamp:swamp4').
card_rarity('swamp'/'MMQ', 'Basic Land').
card_artist('swamp'/'MMQ', 'Terry Springer').
card_number('swamp'/'MMQ', '342').
card_multiverse_id('swamp'/'MMQ', '20891').

card_in_set('task force', 'MMQ').
card_original_type('task force'/'MMQ', 'Creature — Rebel').
card_original_text('task force'/'MMQ', 'Whenever Task Force becomes the target of a spell or ability, it gets +0/+3 until end of turn.').
card_first_print('task force', 'MMQ').
card_image_name('task force'/'MMQ', 'task force').
card_uid('task force'/'MMQ', 'MMQ:Task Force:task force').
card_rarity('task force'/'MMQ', 'Common').
card_artist('task force'/'MMQ', 'Gary Ruddell').
card_number('task force'/'MMQ', '52').
card_flavor_text('task force'/'MMQ', 'They are the reflection of Rushwood\'s glow on the edge of a wooden sword.').
card_multiverse_id('task force'/'MMQ', '19545').

card_in_set('tectonic break', 'MMQ').
card_original_type('tectonic break'/'MMQ', 'Sorcery').
card_original_text('tectonic break'/'MMQ', 'Each player sacrifices X lands.').
card_first_print('tectonic break', 'MMQ').
card_image_name('tectonic break'/'MMQ', 'tectonic break').
card_uid('tectonic break'/'MMQ', 'MMQ:Tectonic Break:tectonic break').
card_rarity('tectonic break'/'MMQ', 'Rare').
card_artist('tectonic break'/'MMQ', 'Rebecca Guay').
card_number('tectonic break'/'MMQ', '216').
card_flavor_text('tectonic break'/'MMQ', 'Only the foolish and the rich build their houses where the land is thin and the ocean is near.').
card_multiverse_id('tectonic break'/'MMQ', '19720').

card_in_set('territorial dispute', 'MMQ').
card_original_type('territorial dispute'/'MMQ', 'Enchantment').
card_original_text('territorial dispute'/'MMQ', 'Players can\'t play lands.\nAt the beginning of your upkeep, sacrifice Territorial Dispute unless you sacrifice a land.').
card_first_print('territorial dispute', 'MMQ').
card_image_name('territorial dispute'/'MMQ', 'territorial dispute').
card_uid('territorial dispute'/'MMQ', 'MMQ:Territorial Dispute:territorial dispute').
card_rarity('territorial dispute'/'MMQ', 'Rare').
card_artist('territorial dispute'/'MMQ', 'Mike Ploog').
card_number('territorial dispute'/'MMQ', '217').
card_flavor_text('territorial dispute'/'MMQ', 'Bit by bit, Mercadia City had forced the Cho-Arrim from their ancestral lands. They would be pushed no further.').
card_multiverse_id('territorial dispute'/'MMQ', '19718').

card_in_set('thermal glider', 'MMQ').
card_original_type('thermal glider'/'MMQ', 'Creature — Rebel').
card_original_text('thermal glider'/'MMQ', 'Flying, protection from red').
card_first_print('thermal glider', 'MMQ').
card_image_name('thermal glider'/'MMQ', 'thermal glider').
card_uid('thermal glider'/'MMQ', 'MMQ:Thermal Glider:thermal glider').
card_rarity('thermal glider'/'MMQ', 'Common').
card_artist('thermal glider'/'MMQ', 'Mark Zug').
card_number('thermal glider'/'MMQ', '53').
card_flavor_text('thermal glider'/'MMQ', '\"The Mercadians are too busy looking down on us to see us coming.\"\n—Cho-Arrim rebel').
card_multiverse_id('thermal glider'/'MMQ', '19537').

card_in_set('thieves\' auction', 'MMQ').
card_original_type('thieves\' auction'/'MMQ', 'Sorcery').
card_original_text('thieves\' auction'/'MMQ', 'Set aside all permanents. You choose one of those cards and put it into play tapped under your control. Then your opponent chooses one and puts it into play tapped under his or her control. Repeat this process until all cards set aside this way have been chosen. (Local enchantments with no permanent to enchant remain removed from the game.)').
card_first_print('thieves\' auction', 'MMQ').
card_image_name('thieves\' auction'/'MMQ', 'thieves\' auction').
card_uid('thieves\' auction'/'MMQ', 'MMQ:Thieves\' Auction:thieves\' auction').
card_rarity('thieves\' auction'/'MMQ', 'Rare').
card_artist('thieves\' auction'/'MMQ', 'Kevin Murphy').
card_number('thieves\' auction'/'MMQ', '218').
card_multiverse_id('thieves\' auction'/'MMQ', '19722').

card_in_set('thrashing wumpus', 'MMQ').
card_original_type('thrashing wumpus'/'MMQ', 'Creature — Beast').
card_original_text('thrashing wumpus'/'MMQ', '{B} Thrashing Wumpus deals 1 damage to each creature and each player.').
card_first_print('thrashing wumpus', 'MMQ').
card_image_name('thrashing wumpus'/'MMQ', 'thrashing wumpus').
card_uid('thrashing wumpus'/'MMQ', 'MMQ:Thrashing Wumpus:thrashing wumpus').
card_rarity('thrashing wumpus'/'MMQ', 'Rare').
card_artist('thrashing wumpus'/'MMQ', 'Jeff Miracola').
card_number('thrashing wumpus'/'MMQ', '166').
card_flavor_text('thrashing wumpus'/'MMQ', 'Young wumpuses are malevolent and vicious—but they grow out of it.').
card_multiverse_id('thrashing wumpus'/'MMQ', '19823').

card_in_set('thunderclap', 'MMQ').
card_original_type('thunderclap'/'MMQ', 'Instant').
card_original_text('thunderclap'/'MMQ', 'You may sacrifice a mountain instead of paying Thunderclap\'s mana cost.\nThunderclap deals 3 damage to target creature.').
card_first_print('thunderclap', 'MMQ').
card_image_name('thunderclap'/'MMQ', 'thunderclap').
card_uid('thunderclap'/'MMQ', 'MMQ:Thunderclap:thunderclap').
card_rarity('thunderclap'/'MMQ', 'Common').
card_artist('thunderclap'/'MMQ', 'Tom Wänerstrand').
card_number('thunderclap'/'MMQ', '219').
card_multiverse_id('thunderclap'/'MMQ', '19615').

card_in_set('thwart', 'MMQ').
card_original_type('thwart'/'MMQ', 'Instant').
card_original_text('thwart'/'MMQ', 'You may return three islands you control to their owner\'s hand instead of paying Thwart\'s mana cost.\nCounter target spell.').
card_first_print('thwart', 'MMQ').
card_image_name('thwart'/'MMQ', 'thwart').
card_uid('thwart'/'MMQ', 'MMQ:Thwart:thwart').
card_rarity('thwart'/'MMQ', 'Uncommon').
card_artist('thwart'/'MMQ', 'Christopher Moeller').
card_number('thwart'/'MMQ', '108').
card_multiverse_id('thwart'/'MMQ', '19811').

card_in_set('tidal bore', 'MMQ').
card_original_type('tidal bore'/'MMQ', 'Instant').
card_original_text('tidal bore'/'MMQ', 'You may return an island you control to its owner\'s hand instead of paying Tidal Bore\'s mana cost.\nTap or untap target creature.').
card_first_print('tidal bore', 'MMQ').
card_image_name('tidal bore'/'MMQ', 'tidal bore').
card_uid('tidal bore'/'MMQ', 'MMQ:Tidal Bore:tidal bore').
card_rarity('tidal bore'/'MMQ', 'Common').
card_artist('tidal bore'/'MMQ', 'Frank Kelly Freas').
card_number('tidal bore'/'MMQ', '109').
card_flavor_text('tidal bore'/'MMQ', 'The dry riverbed became a raging torrent, and the Cho-Arrim claimed their prize.').
card_multiverse_id('tidal bore'/'MMQ', '19569').

card_in_set('tidal kraken', 'MMQ').
card_original_type('tidal kraken'/'MMQ', 'Creature — Monster').
card_original_text('tidal kraken'/'MMQ', 'Tidal Kraken is unblockable.').
card_first_print('tidal kraken', 'MMQ').
card_image_name('tidal kraken'/'MMQ', 'tidal kraken').
card_uid('tidal kraken'/'MMQ', 'MMQ:Tidal Kraken:tidal kraken').
card_rarity('tidal kraken'/'MMQ', 'Rare').
card_artist('tidal kraken'/'MMQ', 'Christopher Moeller').
card_number('tidal kraken'/'MMQ', '110').
card_flavor_text('tidal kraken'/'MMQ', 'To merfolk, pirates are a nuisance. To pirates, merfolk are a threat. To the kraken, they\'re both appetizers.').
card_multiverse_id('tidal kraken'/'MMQ', '19674').

card_in_set('tiger claws', 'MMQ').
card_original_type('tiger claws'/'MMQ', 'Enchant Creature').
card_original_text('tiger claws'/'MMQ', 'You may play Tiger Claws any time you could play an instant.\nEnchanted creature gets +1/+1 and has trample.').
card_first_print('tiger claws', 'MMQ').
card_image_name('tiger claws'/'MMQ', 'tiger claws').
card_uid('tiger claws'/'MMQ', 'MMQ:Tiger Claws:tiger claws').
card_rarity('tiger claws'/'MMQ', 'Common').
card_artist('tiger claws'/'MMQ', 'Adam Rex').
card_number('tiger claws'/'MMQ', '279').
card_flavor_text('tiger claws'/'MMQ', 'Cho-Arrim martial artists emulate the beasts of their home.').
card_multiverse_id('tiger claws'/'MMQ', '19635').

card_in_set('timid drake', 'MMQ').
card_original_type('timid drake'/'MMQ', 'Creature — Drake').
card_original_text('timid drake'/'MMQ', 'Flying\nWhenever another creature comes into play, return Timid Drake to its owner\'s hand.').
card_image_name('timid drake'/'MMQ', 'timid drake').
card_uid('timid drake'/'MMQ', 'MMQ:Timid Drake:timid drake').
card_rarity('timid drake'/'MMQ', 'Uncommon').
card_artist('timid drake'/'MMQ', 'Edward P. Beard, Jr.').
card_number('timid drake'/'MMQ', '111').
card_flavor_text('timid drake'/'MMQ', 'It flees from anything that isn\'t shaped like a cloud.').
card_multiverse_id('timid drake'/'MMQ', '19560').

card_in_set('tonic peddler', 'MMQ').
card_original_type('tonic peddler'/'MMQ', 'Creature — Spellshaper').
card_original_text('tonic peddler'/'MMQ', '{W}, {T}, Discard a card from your hand: Target player gains 3 life.').
card_first_print('tonic peddler', 'MMQ').
card_image_name('tonic peddler'/'MMQ', 'tonic peddler').
card_uid('tonic peddler'/'MMQ', 'MMQ:Tonic Peddler:tonic peddler').
card_rarity('tonic peddler'/'MMQ', 'Uncommon').
card_artist('tonic peddler'/'MMQ', 'Adam Rex').
card_number('tonic peddler'/'MMQ', '54').
card_flavor_text('tonic peddler'/'MMQ', '\"The price is written at the bottom of the cup.\"').
card_multiverse_id('tonic peddler'/'MMQ', '136516').

card_in_set('tooth of ramos', 'MMQ').
card_original_type('tooth of ramos'/'MMQ', 'Artifact').
card_original_text('tooth of ramos'/'MMQ', '{T}: Add one white mana to your mana pool. \nSacrifice Tooth of Ramos: Add one white mana to your mana pool.').
card_first_print('tooth of ramos', 'MMQ').
card_image_name('tooth of ramos'/'MMQ', 'tooth of ramos').
card_uid('tooth of ramos'/'MMQ', 'MMQ:Tooth of Ramos:tooth of ramos').
card_rarity('tooth of ramos'/'MMQ', 'Rare').
card_artist('tooth of ramos'/'MMQ', 'David Martin').
card_number('tooth of ramos'/'MMQ', '313').
card_flavor_text('tooth of ramos'/'MMQ', 'Ramos smiled, and there was day.').
card_multiverse_id('tooth of ramos'/'MMQ', '19762').

card_in_set('tower of the magistrate', 'MMQ').
card_original_type('tower of the magistrate'/'MMQ', 'Land').
card_original_text('tower of the magistrate'/'MMQ', '{T}: Add one colorless mana to your mana pool.\n{1}, {T}: Target creature gains protection from artifacts until end of turn.').
card_first_print('tower of the magistrate', 'MMQ').
card_image_name('tower of the magistrate'/'MMQ', 'tower of the magistrate').
card_uid('tower of the magistrate'/'MMQ', 'MMQ:Tower of the Magistrate:tower of the magistrate').
card_rarity('tower of the magistrate'/'MMQ', 'Rare').
card_artist('tower of the magistrate'/'MMQ', 'Thomas Gianni').
card_number('tower of the magistrate'/'MMQ', '330').
card_flavor_text('tower of the magistrate'/'MMQ', 'Like the city, weakest at its foundation.').
card_multiverse_id('tower of the magistrate'/'MMQ', '19769').

card_in_set('toymaker', 'MMQ').
card_original_type('toymaker'/'MMQ', 'Artifact Creature — Spellshaper').
card_original_text('toymaker'/'MMQ', '{1}, {T}, Discard a card from your hand: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn. (It retains its abilities.)').
card_first_print('toymaker', 'MMQ').
card_image_name('toymaker'/'MMQ', 'toymaker').
card_uid('toymaker'/'MMQ', 'MMQ:Toymaker:toymaker').
card_rarity('toymaker'/'MMQ', 'Uncommon').
card_artist('toymaker'/'MMQ', 'Frank Kelly Freas').
card_number('toymaker'/'MMQ', '314').
card_multiverse_id('toymaker'/'MMQ', '19877').

card_in_set('trade routes', 'MMQ').
card_original_type('trade routes'/'MMQ', 'Enchantment').
card_original_text('trade routes'/'MMQ', '{1}: Return target land you control to its owner\'s hand.\n{1}, Discard a land card from your hand: Draw a card.').
card_first_print('trade routes', 'MMQ').
card_image_name('trade routes'/'MMQ', 'trade routes').
card_uid('trade routes'/'MMQ', 'MMQ:Trade Routes:trade routes').
card_rarity('trade routes'/'MMQ', 'Rare').
card_artist('trade routes'/'MMQ', 'Matt Cavotta').
card_number('trade routes'/'MMQ', '112').
card_flavor_text('trade routes'/'MMQ', 'Like the price of goods, the value of the routes was renegotiated daily.').
card_multiverse_id('trade routes'/'MMQ', '19675').

card_in_set('tranquility', 'MMQ').
card_original_type('tranquility'/'MMQ', 'Sorcery').
card_original_text('tranquility'/'MMQ', 'Destroy all enchantments.').
card_image_name('tranquility'/'MMQ', 'tranquility').
card_uid('tranquility'/'MMQ', 'MMQ:Tranquility:tranquility').
card_rarity('tranquility'/'MMQ', 'Common').
card_artist('tranquility'/'MMQ', 'Heather Hudson').
card_number('tranquility'/'MMQ', '280').
card_flavor_text('tranquility'/'MMQ', 'Sisay had been restored to her place on the ship, the Legacy had been recovered, and the Weatherlight was sailing home to Dominaria. Gerrard focused on the debts he\'d settled . . . and on those he\'d soon collect.').
card_multiverse_id('tranquility'/'MMQ', '19637').

card_in_set('trap runner', 'MMQ').
card_original_type('trap runner'/'MMQ', 'Creature — Soldier').
card_original_text('trap runner'/'MMQ', '{T}: Target attacking unblocked creature becomes blocked. (This ability works on unblockable creatures.)').
card_first_print('trap runner', 'MMQ').
card_image_name('trap runner'/'MMQ', 'trap runner').
card_uid('trap runner'/'MMQ', 'MMQ:Trap Runner:trap runner').
card_rarity('trap runner'/'MMQ', 'Uncommon').
card_artist('trap runner'/'MMQ', 'Ron Spencer').
card_number('trap runner'/'MMQ', '55').
card_flavor_text('trap runner'/'MMQ', '\"How could I catch her when the trees conspired to stop me?\"').
card_multiverse_id('trap runner'/'MMQ', '22288').

card_in_set('tremor', 'MMQ').
card_original_type('tremor'/'MMQ', 'Sorcery').
card_original_text('tremor'/'MMQ', 'Tremor deals 1 damage to each creature without flying.').
card_image_name('tremor'/'MMQ', 'tremor').
card_uid('tremor'/'MMQ', 'MMQ:Tremor:tremor').
card_rarity('tremor'/'MMQ', 'Common').
card_artist('tremor'/'MMQ', 'Mark Romanoski').
card_number('tremor'/'MMQ', '220').
card_flavor_text('tremor'/'MMQ', 'Without a word, Gerrard and Sisay ran deeper into the mountain. Neither of them intended to leave Mercadia without the Weatherlight and its crew.').
card_multiverse_id('tremor'/'MMQ', '19614').

card_in_set('two-headed dragon', 'MMQ').
card_original_type('two-headed dragon'/'MMQ', 'Creature — Dragon').
card_original_text('two-headed dragon'/'MMQ', 'Flying\n{1}{R} Two-Headed Dragon gets +2/+0 until end of turn.\nTwo-Headed Dragon can\'t be blocked except by two or more creatures. It may block one additional creature. (All blocks must be legal.)').
card_first_print('two-headed dragon', 'MMQ').
card_image_name('two-headed dragon'/'MMQ', 'two-headed dragon').
card_uid('two-headed dragon'/'MMQ', 'MMQ:Two-Headed Dragon:two-headed dragon').
card_rarity('two-headed dragon'/'MMQ', 'Rare').
card_artist('two-headed dragon'/'MMQ', 'Sam Wood').
card_number('two-headed dragon'/'MMQ', '221').
card_multiverse_id('two-headed dragon'/'MMQ', '19711').

card_in_set('undertaker', 'MMQ').
card_original_type('undertaker'/'MMQ', 'Creature — Spellshaper').
card_original_text('undertaker'/'MMQ', '{B}, {T}, Discard a card from your hand: Return target creature card from your graveyard to your hand.').
card_first_print('undertaker', 'MMQ').
card_image_name('undertaker'/'MMQ', 'undertaker').
card_uid('undertaker'/'MMQ', 'MMQ:Undertaker:undertaker').
card_rarity('undertaker'/'MMQ', 'Common').
card_artist('undertaker'/'MMQ', 'Jeff Easley').
card_number('undertaker'/'MMQ', '167').
card_flavor_text('undertaker'/'MMQ', 'The weight of death is heavy but not immovable.').
card_multiverse_id('undertaker'/'MMQ', '21306').

card_in_set('unmask', 'MMQ').
card_original_type('unmask'/'MMQ', 'Sorcery').
card_original_text('unmask'/'MMQ', 'You may remove a black card in your hand from the game instead of paying Unmask\'s mana cost.\nLook at target player\'s hand and choose a nonland card from it. That player discards that card.').
card_first_print('unmask', 'MMQ').
card_image_name('unmask'/'MMQ', 'unmask').
card_uid('unmask'/'MMQ', 'MMQ:Unmask:unmask').
card_rarity('unmask'/'MMQ', 'Rare').
card_artist('unmask'/'MMQ', 'rk post').
card_number('unmask'/'MMQ', '168').
card_multiverse_id('unmask'/'MMQ', '19829').

card_in_set('unnatural hunger', 'MMQ').
card_original_type('unnatural hunger'/'MMQ', 'Enchant Creature').
card_original_text('unnatural hunger'/'MMQ', 'At the beginning of the upkeep of enchanted creature\'s controller, Unnatural Hunger deals to that player damage equal to enchanted creature\'s power unless he or she sacrifices another creature.').
card_first_print('unnatural hunger', 'MMQ').
card_image_name('unnatural hunger'/'MMQ', 'unnatural hunger').
card_uid('unnatural hunger'/'MMQ', 'MMQ:Unnatural Hunger:unnatural hunger').
card_rarity('unnatural hunger'/'MMQ', 'Rare').
card_artist('unnatural hunger'/'MMQ', 'Jeff Miracola').
card_number('unnatural hunger'/'MMQ', '169').
card_multiverse_id('unnatural hunger'/'MMQ', '19705').

card_in_set('uphill battle', 'MMQ').
card_original_type('uphill battle'/'MMQ', 'Enchantment').
card_original_text('uphill battle'/'MMQ', 'Creatures your opponents play come into play tapped.').
card_first_print('uphill battle', 'MMQ').
card_image_name('uphill battle'/'MMQ', 'uphill battle').
card_uid('uphill battle'/'MMQ', 'MMQ:Uphill Battle:uphill battle').
card_rarity('uphill battle'/'MMQ', 'Uncommon').
card_artist('uphill battle'/'MMQ', 'Pete Venters').
card_number('uphill battle'/'MMQ', '222').
card_flavor_text('uphill battle'/'MMQ', 'Cho-Manno knew he had no choice but to ally with Gerrard. If that\'s what it took to free his people, he\'d pay the price.').
card_multiverse_id('uphill battle'/'MMQ', '20786').

card_in_set('vendetta', 'MMQ').
card_original_type('vendetta'/'MMQ', 'Instant').
card_original_text('vendetta'/'MMQ', 'Destroy target nonblack creature. It can\'t be regenerated. You lose life equal to that creature\'s toughness.').
card_first_print('vendetta', 'MMQ').
card_image_name('vendetta'/'MMQ', 'vendetta').
card_uid('vendetta'/'MMQ', 'MMQ:Vendetta:vendetta').
card_rarity('vendetta'/'MMQ', 'Common').
card_artist('vendetta'/'MMQ', 'Dan Frazier').
card_number('vendetta'/'MMQ', '170').
card_flavor_text('vendetta'/'MMQ', 'Starke knew the voice was Takara\'s, but the venom was Volrath\'s.').
card_multiverse_id('vendetta'/'MMQ', '19589').

card_in_set('venomous breath', 'MMQ').
card_original_type('venomous breath'/'MMQ', 'Instant').
card_original_text('venomous breath'/'MMQ', 'At end of combat, destroy all creatures that blocked or were blocked by target creature this turn.').
card_image_name('venomous breath'/'MMQ', 'venomous breath').
card_uid('venomous breath'/'MMQ', 'MMQ:Venomous Breath:venomous breath').
card_rarity('venomous breath'/'MMQ', 'Uncommon').
card_artist('venomous breath'/'MMQ', 'DiTerlizzi').
card_number('venomous breath'/'MMQ', '281').
card_flavor_text('venomous breath'/'MMQ', 'The dryads need neither size nor strength to defeat their enemies in battle.').
card_multiverse_id('venomous breath'/'MMQ', '19744').

card_in_set('venomous dragonfly', 'MMQ').
card_original_type('venomous dragonfly'/'MMQ', 'Creature — Insect').
card_original_text('venomous dragonfly'/'MMQ', 'Flying\nWhenever Venomous Dragonfly blocks or becomes blocked by a creature, destroy that creature at end of combat.').
card_first_print('venomous dragonfly', 'MMQ').
card_image_name('venomous dragonfly'/'MMQ', 'venomous dragonfly').
card_uid('venomous dragonfly'/'MMQ', 'MMQ:Venomous Dragonfly:venomous dragonfly').
card_rarity('venomous dragonfly'/'MMQ', 'Common').
card_artist('venomous dragonfly'/'MMQ', 'Tom Wänerstrand').
card_number('venomous dragonfly'/'MMQ', '282').
card_flavor_text('venomous dragonfly'/'MMQ', 'A pretty little death is still death.').
card_multiverse_id('venomous dragonfly'/'MMQ', '19625').

card_in_set('vernal equinox', 'MMQ').
card_original_type('vernal equinox'/'MMQ', 'Enchantment').
card_original_text('vernal equinox'/'MMQ', 'Any player may play creature and enchantment spells any time he or she could play an instant.').
card_first_print('vernal equinox', 'MMQ').
card_image_name('vernal equinox'/'MMQ', 'vernal equinox').
card_uid('vernal equinox'/'MMQ', 'MMQ:Vernal Equinox:vernal equinox').
card_rarity('vernal equinox'/'MMQ', 'Rare').
card_artist('vernal equinox'/'MMQ', 'Rebecca Guay').
card_number('vernal equinox'/'MMQ', '283').
card_multiverse_id('vernal equinox'/'MMQ', '19736').

card_in_set('vine dryad', 'MMQ').
card_original_type('vine dryad'/'MMQ', 'Creature — Dryad').
card_original_text('vine dryad'/'MMQ', 'Forestwalk (This creature is unblockable as long as defending player controls a forest.)\nYou may play Vine Dryad any time you could play an instant.\nYou may remove a green card in your hand from the game instead of paying Vine Dryad\'s mana cost.').
card_first_print('vine dryad', 'MMQ').
card_image_name('vine dryad'/'MMQ', 'vine dryad').
card_uid('vine dryad'/'MMQ', 'MMQ:Vine Dryad:vine dryad').
card_rarity('vine dryad'/'MMQ', 'Rare').
card_artist('vine dryad'/'MMQ', 'Jeff Laubenstein').
card_number('vine dryad'/'MMQ', '284').
card_multiverse_id('vine dryad'/'MMQ', '19735').

card_in_set('vine trellis', 'MMQ').
card_original_type('vine trellis'/'MMQ', 'Creature — Wall').
card_original_text('vine trellis'/'MMQ', '(Walls can\'t attack.)\n{T}: Add one green mana to your mana pool.').
card_first_print('vine trellis', 'MMQ').
card_image_name('vine trellis'/'MMQ', 'vine trellis').
card_uid('vine trellis'/'MMQ', 'MMQ:Vine Trellis:vine trellis').
card_rarity('vine trellis'/'MMQ', 'Common').
card_artist('vine trellis'/'MMQ', 'DiTerlizzi').
card_number('vine trellis'/'MMQ', '285').
card_flavor_text('vine trellis'/'MMQ', '\"We protect our own with our own.\"').
card_multiverse_id('vine trellis'/'MMQ', '19624').

card_in_set('volcanic wind', 'MMQ').
card_original_type('volcanic wind'/'MMQ', 'Sorcery').
card_original_text('volcanic wind'/'MMQ', 'Volcanic Wind deals X damage divided as you choose among any number of target creatures, where X is the number of creatures in play.').
card_first_print('volcanic wind', 'MMQ').
card_image_name('volcanic wind'/'MMQ', 'volcanic wind').
card_uid('volcanic wind'/'MMQ', 'MMQ:Volcanic Wind:volcanic wind').
card_rarity('volcanic wind'/'MMQ', 'Uncommon').
card_artist('volcanic wind'/'MMQ', 'Rebecca Guay').
card_number('volcanic wind'/'MMQ', '223').
card_flavor_text('volcanic wind'/'MMQ', 'The Cho-Arrim call it \"the fury of Ramos.\"').
card_multiverse_id('volcanic wind'/'MMQ', '19853').

card_in_set('wall of distortion', 'MMQ').
card_original_type('wall of distortion'/'MMQ', 'Creature — Wall').
card_original_text('wall of distortion'/'MMQ', '(Walls can\'t attack.)\n{2}{B}, {T}: Target player discards a card from his or her hand. Play this ability only if you could play a sorcery.').
card_first_print('wall of distortion', 'MMQ').
card_image_name('wall of distortion'/'MMQ', 'wall of distortion').
card_uid('wall of distortion'/'MMQ', 'MMQ:Wall of Distortion:wall of distortion').
card_rarity('wall of distortion'/'MMQ', 'Common').
card_artist('wall of distortion'/'MMQ', 'Mark Tedin').
card_number('wall of distortion'/'MMQ', '171').
card_flavor_text('wall of distortion'/'MMQ', 'It reflects only nightmares.').
card_multiverse_id('wall of distortion'/'MMQ', '19583').

card_in_set('war cadence', 'MMQ').
card_original_type('war cadence'/'MMQ', 'Enchantment').
card_original_text('war cadence'/'MMQ', '{X}{R} Creatures can\'t block this turn unless their controller pays {X} for each blocking creature.').
card_first_print('war cadence', 'MMQ').
card_image_name('war cadence'/'MMQ', 'war cadence').
card_uid('war cadence'/'MMQ', 'MMQ:War Cadence:war cadence').
card_rarity('war cadence'/'MMQ', 'Uncommon').
card_artist('war cadence'/'MMQ', 'John Matson').
card_number('war cadence'/'MMQ', '224').
card_flavor_text('war cadence'/'MMQ', 'The rhythm isn\'t a call to arms, but a signal that the goblins have made a new enemy.').
card_multiverse_id('war cadence'/'MMQ', '19619').

card_in_set('war tax', 'MMQ').
card_original_type('war tax'/'MMQ', 'Enchantment').
card_original_text('war tax'/'MMQ', '{X}{U} Creatures can\'t attack this turn unless their controller pays {X} for each attacking creature.').
card_first_print('war tax', 'MMQ').
card_image_name('war tax'/'MMQ', 'war tax').
card_uid('war tax'/'MMQ', 'MMQ:War Tax:war tax').
card_rarity('war tax'/'MMQ', 'Uncommon').
card_artist('war tax'/'MMQ', 'Greg & Tim Hildebrandt').
card_number('war tax'/'MMQ', '113').
card_flavor_text('war tax'/'MMQ', 'The worst thing about being a depraved pirate is having to work with a bunch of depraved pirates.').
card_multiverse_id('war tax'/'MMQ', '19809').

card_in_set('warmonger', 'MMQ').
card_original_type('warmonger'/'MMQ', 'Creature — Monger').
card_original_text('warmonger'/'MMQ', '{2}: Warmonger deals 1 damage to each creature without flying and each player. Any player may play this ability.').
card_image_name('warmonger'/'MMQ', 'warmonger').
card_uid('warmonger'/'MMQ', 'MMQ:Warmonger:warmonger').
card_rarity('warmonger'/'MMQ', 'Uncommon').
card_artist('warmonger'/'MMQ', 'Heather Hudson').
card_number('warmonger'/'MMQ', '225').
card_flavor_text('warmonger'/'MMQ', 'Funny how money and war travel so well together.').
card_multiverse_id('warmonger'/'MMQ', '19837').

card_in_set('warpath', 'MMQ').
card_original_type('warpath'/'MMQ', 'Instant').
card_original_text('warpath'/'MMQ', 'Warpath deals 3 damage to each blocking creature and each blocked creature.').
card_first_print('warpath', 'MMQ').
card_image_name('warpath'/'MMQ', 'warpath').
card_uid('warpath'/'MMQ', 'MMQ:Warpath:warpath').
card_rarity('warpath'/'MMQ', 'Uncommon').
card_artist('warpath'/'MMQ', 'Paolo Parente').
card_number('warpath'/'MMQ', '226').
card_flavor_text('warpath'/'MMQ', 'Cho-Manno had never seen Mercadia City\'s forces fight so well. The magistrate must have hired a new hero.').
card_multiverse_id('warpath'/'MMQ', '19851').

card_in_set('waterfront bouncer', 'MMQ').
card_original_type('waterfront bouncer'/'MMQ', 'Creature — Spellshaper').
card_original_text('waterfront bouncer'/'MMQ', '{U}, {T}, Discard a card from your hand: Return target creature to its owner\'s hand.').
card_first_print('waterfront bouncer', 'MMQ').
card_image_name('waterfront bouncer'/'MMQ', 'waterfront bouncer').
card_uid('waterfront bouncer'/'MMQ', 'MMQ:Waterfront Bouncer:waterfront bouncer').
card_rarity('waterfront bouncer'/'MMQ', 'Common').
card_artist('waterfront bouncer'/'MMQ', 'Paolo Parente').
card_number('waterfront bouncer'/'MMQ', '114').
card_flavor_text('waterfront bouncer'/'MMQ', 'Closing time comes earlier to some than to others.').
card_multiverse_id('waterfront bouncer'/'MMQ', '19557').

card_in_set('wave of reckoning', 'MMQ').
card_original_type('wave of reckoning'/'MMQ', 'Sorcery').
card_original_text('wave of reckoning'/'MMQ', 'Each creature deals to itself damage equal to its power.').
card_first_print('wave of reckoning', 'MMQ').
card_image_name('wave of reckoning'/'MMQ', 'wave of reckoning').
card_uid('wave of reckoning'/'MMQ', 'MMQ:Wave of Reckoning:wave of reckoning').
card_rarity('wave of reckoning'/'MMQ', 'Rare').
card_artist('wave of reckoning'/'MMQ', 'Bradley Williams').
card_number('wave of reckoning'/'MMQ', '56').
card_flavor_text('wave of reckoning'/'MMQ', 'Inner conflict can defeat a soldier more quickly than any army.').
card_multiverse_id('wave of reckoning'/'MMQ', '19659').

card_in_set('wild jhovall', 'MMQ').
card_original_type('wild jhovall'/'MMQ', 'Creature — Cat').
card_original_text('wild jhovall'/'MMQ', '').
card_first_print('wild jhovall', 'MMQ').
card_image_name('wild jhovall'/'MMQ', 'wild jhovall').
card_uid('wild jhovall'/'MMQ', 'MMQ:Wild Jhovall:wild jhovall').
card_rarity('wild jhovall'/'MMQ', 'Common').
card_artist('wild jhovall'/'MMQ', 'Daren Bader').
card_number('wild jhovall'/'MMQ', '227').
card_flavor_text('wild jhovall'/'MMQ', 'Jhovalls sharpen their claws on trees—or on Mercadians, if no trees are handy.').
card_multiverse_id('wild jhovall'/'MMQ', '19605').

card_in_set('wishmonger', 'MMQ').
card_original_type('wishmonger'/'MMQ', 'Creature — Monger').
card_original_text('wishmonger'/'MMQ', '{2}: Target creature gains protection from the color of its controller\'s choice until end of turn. Any player may play this ability.').
card_first_print('wishmonger', 'MMQ').
card_image_name('wishmonger'/'MMQ', 'wishmonger').
card_uid('wishmonger'/'MMQ', 'MMQ:Wishmonger:wishmonger').
card_rarity('wishmonger'/'MMQ', 'Uncommon').
card_artist('wishmonger'/'MMQ', 'Heather Hudson').
card_number('wishmonger'/'MMQ', '57').
card_flavor_text('wishmonger'/'MMQ', '\"It\'s not what you ask for, but how you ask for it.\"').
card_multiverse_id('wishmonger'/'MMQ', '19776').

card_in_set('word of blasting', 'MMQ').
card_original_type('word of blasting'/'MMQ', 'Instant').
card_original_text('word of blasting'/'MMQ', 'Destroy target Wall. It can\'t be regenerated. Word of Blasting deals damage equal to that Wall\'s converted mana cost to the Wall\'s controller.').
card_image_name('word of blasting'/'MMQ', 'word of blasting').
card_uid('word of blasting'/'MMQ', 'MMQ:Word of Blasting:word of blasting').
card_rarity('word of blasting'/'MMQ', 'Uncommon').
card_artist('word of blasting'/'MMQ', 'Eric Peterson').
card_number('word of blasting'/'MMQ', '228').
card_multiverse_id('word of blasting'/'MMQ', '19852').

card_in_set('worry beads', 'MMQ').
card_original_type('worry beads'/'MMQ', 'Artifact').
card_original_text('worry beads'/'MMQ', 'At the beginning of each player\'s upkeep, that player puts the top card of his or her library into his or her graveyard.').
card_first_print('worry beads', 'MMQ').
card_image_name('worry beads'/'MMQ', 'worry beads').
card_uid('worry beads'/'MMQ', 'MMQ:Worry Beads:worry beads').
card_rarity('worry beads'/'MMQ', 'Rare').
card_artist('worry beads'/'MMQ', 'rk post').
card_number('worry beads'/'MMQ', '315').
card_multiverse_id('worry beads'/'MMQ', '19747').
