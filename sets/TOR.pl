% Torment

set('TOR').
set_name('TOR', 'Torment').
set_release_date('TOR', '2002-02-04').
set_border('TOR', 'black').
set_type('TOR', 'expansion').
set_block('TOR', 'Odyssey').

card_in_set('accelerate', 'TOR').
card_original_type('accelerate'/'TOR', 'Instant').
card_original_text('accelerate'/'TOR', 'Target creature gains haste until end of turn.\nDraw a card.').
card_first_print('accelerate', 'TOR').
card_image_name('accelerate'/'TOR', 'accelerate').
card_uid('accelerate'/'TOR', 'TOR:Accelerate:accelerate').
card_rarity('accelerate'/'TOR', 'Common').
card_artist('accelerate'/'TOR', 'Gary Ruddell').
card_number('accelerate'/'TOR', '90').
card_flavor_text('accelerate'/'TOR', '\"I\'ve seen lightning move slower.\"\n—Nomad sentry').
card_multiverse_id('accelerate'/'TOR', '29857').

card_in_set('acorn harvest', 'TOR').
card_original_type('acorn harvest'/'TOR', 'Sorcery').
card_original_text('acorn harvest'/'TOR', 'Put two 1/1 green Squirrel creature tokens into play.\nFlashback—{1}{G}, Pay 3 life. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('acorn harvest', 'TOR').
card_image_name('acorn harvest'/'TOR', 'acorn harvest').
card_uid('acorn harvest'/'TOR', 'TOR:Acorn Harvest:acorn harvest').
card_rarity('acorn harvest'/'TOR', 'Common').
card_artist('acorn harvest'/'TOR', 'Edward P. Beard, Jr.').
card_number('acorn harvest'/'TOR', '118').
card_multiverse_id('acorn harvest'/'TOR', '29873').

card_in_set('alter reality', 'TOR').
card_original_type('alter reality'/'TOR', 'Instant').
card_original_text('alter reality'/'TOR', 'Change the text of target permanent or spell by replacing all instances of one color word with another. (This effect doesn\'t end at end of turn.)\nFlashback {1}{U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('alter reality', 'TOR').
card_image_name('alter reality'/'TOR', 'alter reality').
card_uid('alter reality'/'TOR', 'TOR:Alter Reality:alter reality').
card_rarity('alter reality'/'TOR', 'Rare').
card_artist('alter reality'/'TOR', 'Justin Sweet').
card_number('alter reality'/'TOR', '22').
card_multiverse_id('alter reality'/'TOR', '34832').

card_in_set('ambassador laquatus', 'TOR').
card_original_type('ambassador laquatus'/'TOR', 'Creature — Merfolk Legend').
card_original_text('ambassador laquatus'/'TOR', '{3}: Target player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('ambassador laquatus', 'TOR').
card_image_name('ambassador laquatus'/'TOR', 'ambassador laquatus').
card_uid('ambassador laquatus'/'TOR', 'TOR:Ambassador Laquatus:ambassador laquatus').
card_rarity('ambassador laquatus'/'TOR', 'Rare').
card_artist('ambassador laquatus'/'TOR', 'Eric Peterson').
card_number('ambassador laquatus'/'TOR', '23').
card_flavor_text('ambassador laquatus'/'TOR', '\"He smiles with poisoned lips.\"\n—Empress Llawan').
card_multiverse_id('ambassador laquatus'/'TOR', '34378').

card_in_set('angel of retribution', 'TOR').
card_original_type('angel of retribution'/'TOR', 'Creature — Angel').
card_original_text('angel of retribution'/'TOR', 'Flying, first strike').
card_first_print('angel of retribution', 'TOR').
card_image_name('angel of retribution'/'TOR', 'angel of retribution').
card_uid('angel of retribution'/'TOR', 'TOR:Angel of Retribution:angel of retribution').
card_rarity('angel of retribution'/'TOR', 'Rare').
card_artist('angel of retribution'/'TOR', 'rk post').
card_number('angel of retribution'/'TOR', '1').
card_flavor_text('angel of retribution'/'TOR', 'Bitter vengeance never glowed so bright nor sang so sweet.').
card_multiverse_id('angel of retribution'/'TOR', '32233').

card_in_set('anurid scavenger', 'TOR').
card_original_type('anurid scavenger'/'TOR', 'Creature — Beast').
card_original_text('anurid scavenger'/'TOR', 'Protection from black\nAt the beginning of your upkeep, sacrifice Anurid Scavenger unless you put a card from your graveyard on the bottom of your library.').
card_first_print('anurid scavenger', 'TOR').
card_image_name('anurid scavenger'/'TOR', 'anurid scavenger').
card_uid('anurid scavenger'/'TOR', 'TOR:Anurid Scavenger:anurid scavenger').
card_rarity('anurid scavenger'/'TOR', 'Uncommon').
card_artist('anurid scavenger'/'TOR', 'Bob Petillo').
card_number('anurid scavenger'/'TOR', '119').
card_flavor_text('anurid scavenger'/'TOR', 'Krosa\'s topmost bottom feeder.').
card_multiverse_id('anurid scavenger'/'TOR', '33687').

card_in_set('aquamoeba', 'TOR').
card_original_type('aquamoeba'/'TOR', 'Creature — Beast').
card_original_text('aquamoeba'/'TOR', 'Discard a card from your hand: Switch Aquamoeba\'s power and toughness until end of turn.').
card_first_print('aquamoeba', 'TOR').
card_image_name('aquamoeba'/'TOR', 'aquamoeba').
card_uid('aquamoeba'/'TOR', 'TOR:Aquamoeba:aquamoeba').
card_rarity('aquamoeba'/'TOR', 'Common').
card_artist('aquamoeba'/'TOR', 'Arnie Swekel').
card_number('aquamoeba'/'TOR', '24').
card_flavor_text('aquamoeba'/'TOR', 'Some tides need no moon.').
card_multiverse_id('aquamoeba'/'TOR', '34465').

card_in_set('arrogant wurm', 'TOR').
card_original_type('arrogant wurm'/'TOR', 'Creature — Wurm').
card_original_text('arrogant wurm'/'TOR', 'Trample\nMadness {2}{G} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_image_name('arrogant wurm'/'TOR', 'arrogant wurm').
card_uid('arrogant wurm'/'TOR', 'TOR:Arrogant Wurm:arrogant wurm').
card_rarity('arrogant wurm'/'TOR', 'Uncommon').
card_artist('arrogant wurm'/'TOR', 'John Avon').
card_number('arrogant wurm'/'TOR', '120').
card_flavor_text('arrogant wurm'/'TOR', 'It\'s hard to be humble when the whole world is bite size.').
card_multiverse_id('arrogant wurm'/'TOR', '12623').

card_in_set('aven trooper', 'TOR').
card_original_type('aven trooper'/'TOR', 'Creature — Bird Soldier').
card_original_text('aven trooper'/'TOR', 'Flying\n{2}{W}, Discard a card from your hand: Aven Trooper gets +1/+2 until end of turn.').
card_first_print('aven trooper', 'TOR').
card_image_name('aven trooper'/'TOR', 'aven trooper').
card_uid('aven trooper'/'TOR', 'TOR:Aven Trooper:aven trooper').
card_rarity('aven trooper'/'TOR', 'Common').
card_artist('aven trooper'/'TOR', 'Greg Staples').
card_number('aven trooper'/'TOR', '2').
card_flavor_text('aven trooper'/'TOR', 'The very skies seethe with the aven\'s hatred for the Cabal.').
card_multiverse_id('aven trooper'/'TOR', '5584').

card_in_set('balshan collaborator', 'TOR').
card_original_type('balshan collaborator'/'TOR', 'Creature — Bird Soldier').
card_original_text('balshan collaborator'/'TOR', 'Flying\n{B}: Balshan Collaborator gets +1/+1 until end of turn.').
card_first_print('balshan collaborator', 'TOR').
card_image_name('balshan collaborator'/'TOR', 'balshan collaborator').
card_uid('balshan collaborator'/'TOR', 'TOR:Balshan Collaborator:balshan collaborator').
card_rarity('balshan collaborator'/'TOR', 'Uncommon').
card_artist('balshan collaborator'/'TOR', 'DiTerlizzi').
card_number('balshan collaborator'/'TOR', '25').
card_flavor_text('balshan collaborator'/'TOR', '\"Power, gold, crackers—every bird has its price.\"\n—Chainer, dementia master').
card_multiverse_id('balshan collaborator'/'TOR', '32915').

card_in_set('balthor the stout', 'TOR').
card_original_type('balthor the stout'/'TOR', 'Creature — Dwarf Legend').
card_original_text('balthor the stout'/'TOR', 'All Barbarians get +1/+1.\n{R}: Target Barbarian gets +1/+0 until end of turn.').
card_first_print('balthor the stout', 'TOR').
card_image_name('balthor the stout'/'TOR', 'balthor the stout').
card_uid('balthor the stout'/'TOR', 'TOR:Balthor the Stout:balthor the stout').
card_rarity('balthor the stout'/'TOR', 'Rare').
card_artist('balthor the stout'/'TOR', 'Ron Spears').
card_number('balthor the stout'/'TOR', '91').
card_flavor_text('balthor the stout'/'TOR', '\"I like to think of him as concentrated barbarian.\"\n—Kamahl, pit fighter').
card_multiverse_id('balthor the stout'/'TOR', '19553').

card_in_set('barbarian outcast', 'TOR').
card_original_type('barbarian outcast'/'TOR', 'Creature — Barbarian Beast').
card_original_text('barbarian outcast'/'TOR', 'When you control no swamps, sacrifice Barbarian Outcast.').
card_first_print('barbarian outcast', 'TOR').
card_image_name('barbarian outcast'/'TOR', 'barbarian outcast').
card_uid('barbarian outcast'/'TOR', 'TOR:Barbarian Outcast:barbarian outcast').
card_rarity('barbarian outcast'/'TOR', 'Common').
card_artist('barbarian outcast'/'TOR', 'Mark Tedin').
card_number('barbarian outcast'/'TOR', '92').
card_flavor_text('barbarian outcast'/'TOR', '\"We offer power to anyone willing to take it. Sadly, few are unburdened enough by their prejudices to accept.\"\n—Cabal surgeon').
card_multiverse_id('barbarian outcast'/'TOR', '26726').

card_in_set('basking rootwalla', 'TOR').
card_original_type('basking rootwalla'/'TOR', 'Creature — Lizard').
card_original_text('basking rootwalla'/'TOR', '{1}{G}: Basking Rootwalla gets +2/+2 until end of turn. Play this ability only once each turn.\nMadness {0} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_image_name('basking rootwalla'/'TOR', 'basking rootwalla').
card_uid('basking rootwalla'/'TOR', 'TOR:Basking Rootwalla:basking rootwalla').
card_rarity('basking rootwalla'/'TOR', 'Common').
card_artist('basking rootwalla'/'TOR', 'Heather Hudson').
card_number('basking rootwalla'/'TOR', '121').
card_multiverse_id('basking rootwalla'/'TOR', '35072').

card_in_set('boneshard slasher', 'TOR').
card_original_type('boneshard slasher'/'TOR', 'Creature — Horror').
card_original_text('boneshard slasher'/'TOR', 'Flying\nThreshold Boneshard Slasher gets +2/+2 and has \"When Boneshard Slasher becomes the target of a spell or ability, sacrifice it.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('boneshard slasher', 'TOR').
card_image_name('boneshard slasher'/'TOR', 'boneshard slasher').
card_uid('boneshard slasher'/'TOR', 'TOR:Boneshard Slasher:boneshard slasher').
card_rarity('boneshard slasher'/'TOR', 'Uncommon').
card_artist('boneshard slasher'/'TOR', 'Ron Spencer').
card_number('boneshard slasher'/'TOR', '50').
card_multiverse_id('boneshard slasher'/'TOR', '32225').

card_in_set('breakthrough', 'TOR').
card_original_type('breakthrough'/'TOR', 'Sorcery').
card_original_text('breakthrough'/'TOR', 'Draw four cards, then choose X cards in your hand and discard the rest from it.').
card_first_print('breakthrough', 'TOR').
card_image_name('breakthrough'/'TOR', 'breakthrough').
card_uid('breakthrough'/'TOR', 'TOR:Breakthrough:breakthrough').
card_rarity('breakthrough'/'TOR', 'Uncommon').
card_artist('breakthrough'/'TOR', 'Gary Ruddell').
card_number('breakthrough'/'TOR', '26').
card_flavor_text('breakthrough'/'TOR', 'The ideas came flooding in so fast that they couldn\'t all be contained.').
card_multiverse_id('breakthrough'/'TOR', '29927').

card_in_set('cabal coffers', 'TOR').
card_original_type('cabal coffers'/'TOR', 'Land').
card_original_text('cabal coffers'/'TOR', '{2}, {T}: Add {B} to your mana pool for each swamp you control.').
card_image_name('cabal coffers'/'TOR', 'cabal coffers').
card_uid('cabal coffers'/'TOR', 'TOR:Cabal Coffers:cabal coffers').
card_rarity('cabal coffers'/'TOR', 'Uncommon').
card_artist('cabal coffers'/'TOR', 'Don Hazeltine').
card_number('cabal coffers'/'TOR', '139').
card_flavor_text('cabal coffers'/'TOR', 'Deep within the Cabal\'s vault, the Mirari pulsed like a dead sun—and its darkness radiated across Otaria.').
card_multiverse_id('cabal coffers'/'TOR', '29896').

card_in_set('cabal ritual', 'TOR').
card_original_type('cabal ritual'/'TOR', 'Instant').
card_original_text('cabal ritual'/'TOR', 'Add {B}{B}{B} to your mana pool.\nThreshold Instead add {B}{B}{B}{B}{B} to your mana pool. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('cabal ritual', 'TOR').
card_image_name('cabal ritual'/'TOR', 'cabal ritual').
card_uid('cabal ritual'/'TOR', 'TOR:Cabal Ritual:cabal ritual').
card_rarity('cabal ritual'/'TOR', 'Common').
card_artist('cabal ritual'/'TOR', 'Greg & Tim Hildebrandt').
card_number('cabal ritual'/'TOR', '51').
card_flavor_text('cabal ritual'/'TOR', '\"Each syllable chills your veins. Each word rattles your mind.\"\n—Cabal Patriarch').
card_multiverse_id('cabal ritual'/'TOR', '30564').

card_in_set('cabal surgeon', 'TOR').
card_original_type('cabal surgeon'/'TOR', 'Creature — Minion').
card_original_text('cabal surgeon'/'TOR', '{2}{B}{B}, {T}, Remove two cards in your graveyard from the game: Return target creature card from your graveyard to your hand.').
card_first_print('cabal surgeon', 'TOR').
card_image_name('cabal surgeon'/'TOR', 'cabal surgeon').
card_uid('cabal surgeon'/'TOR', 'TOR:Cabal Surgeon:cabal surgeon').
card_rarity('cabal surgeon'/'TOR', 'Common').
card_artist('cabal surgeon'/'TOR', 'Donato Giancola').
card_number('cabal surgeon'/'TOR', '52').
card_flavor_text('cabal surgeon'/'TOR', '\"Clerics give up when their patients die. Can I help it if I\'m more compassionate?\"').
card_multiverse_id('cabal surgeon'/'TOR', '32224').

card_in_set('cabal torturer', 'TOR').
card_original_type('cabal torturer'/'TOR', 'Creature — Minion').
card_original_text('cabal torturer'/'TOR', '{B}, {T}: Target creature gets -1/-1 until end of turn.\nThreshold {3}{B}{B}, {T}: Target creature gets -2/-2 until end of turn. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('cabal torturer', 'TOR').
card_image_name('cabal torturer'/'TOR', 'cabal torturer').
card_uid('cabal torturer'/'TOR', 'TOR:Cabal Torturer:cabal torturer').
card_rarity('cabal torturer'/'TOR', 'Common').
card_artist('cabal torturer'/'TOR', 'Pete Venters').
card_number('cabal torturer'/'TOR', '53').
card_multiverse_id('cabal torturer'/'TOR', '32226').

card_in_set('carrion rats', 'TOR').
card_original_type('carrion rats'/'TOR', 'Creature — Rat').
card_original_text('carrion rats'/'TOR', 'Whenever Carrion Rats attacks or blocks, any player may remove a card in his or her graveyard from the game. If a player does, Carrion Rats deals no combat damage this turn.').
card_first_print('carrion rats', 'TOR').
card_image_name('carrion rats'/'TOR', 'carrion rats').
card_uid('carrion rats'/'TOR', 'TOR:Carrion Rats:carrion rats').
card_rarity('carrion rats'/'TOR', 'Common').
card_artist('carrion rats'/'TOR', 'Edward P. Beard, Jr.').
card_number('carrion rats'/'TOR', '54').
card_flavor_text('carrion rats'/'TOR', '\"Just what I need—more competition.\"\n—Cabal grave robber').
card_multiverse_id('carrion rats'/'TOR', '26279').

card_in_set('carrion wurm', 'TOR').
card_original_type('carrion wurm'/'TOR', 'Creature — Zombie Wurm').
card_original_text('carrion wurm'/'TOR', 'Whenever Carrion Wurm attacks or blocks, any player may remove three cards in his or her graveyard from the game. If a player does, Carrion Wurm deals no combat damage this turn.').
card_first_print('carrion wurm', 'TOR').
card_image_name('carrion wurm'/'TOR', 'carrion wurm').
card_uid('carrion wurm'/'TOR', 'TOR:Carrion Wurm:carrion wurm').
card_rarity('carrion wurm'/'TOR', 'Uncommon').
card_artist('carrion wurm'/'TOR', 'Glen Angus').
card_number('carrion wurm'/'TOR', '55').
card_flavor_text('carrion wurm'/'TOR', 'Since it eats only carrion, sometimes it needs to make some from scratch.').
card_multiverse_id('carrion wurm'/'TOR', '26449').

card_in_set('centaur chieftain', 'TOR').
card_original_type('centaur chieftain'/'TOR', 'Creature — Centaur').
card_original_text('centaur chieftain'/'TOR', 'Haste\nThreshold When Centaur Chieftain comes into play, creatures you control get +1/+1 and gain trample until end of turn. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('centaur chieftain', 'TOR').
card_image_name('centaur chieftain'/'TOR', 'centaur chieftain').
card_uid('centaur chieftain'/'TOR', 'TOR:Centaur Chieftain:centaur chieftain').
card_rarity('centaur chieftain'/'TOR', 'Uncommon').
card_artist('centaur chieftain'/'TOR', 'Justin Sweet').
card_number('centaur chieftain'/'TOR', '122').
card_multiverse_id('centaur chieftain'/'TOR', '32218').

card_in_set('centaur veteran', 'TOR').
card_original_type('centaur veteran'/'TOR', 'Creature — Centaur').
card_original_text('centaur veteran'/'TOR', 'Trample\n{G}, Discard a card from your hand: Regenerate Centaur Veteran.').
card_first_print('centaur veteran', 'TOR').
card_image_name('centaur veteran'/'TOR', 'centaur veteran').
card_uid('centaur veteran'/'TOR', 'TOR:Centaur Veteran:centaur veteran').
card_rarity('centaur veteran'/'TOR', 'Common').
card_artist('centaur veteran'/'TOR', 'Mark Zug').
card_number('centaur veteran'/'TOR', '123').
card_flavor_text('centaur veteran'/'TOR', 'He wears his scars like badges of honor, warning his foes that he\'s not easily defeated.').
card_multiverse_id('centaur veteran'/'TOR', '29800').

card_in_set('cephalid aristocrat', 'TOR').
card_original_type('cephalid aristocrat'/'TOR', 'Creature — Cephalid').
card_original_text('cephalid aristocrat'/'TOR', 'Whenever Cephalid Aristocrat becomes the target of a spell or ability, put the top two cards of your library into your graveyard.').
card_first_print('cephalid aristocrat', 'TOR').
card_image_name('cephalid aristocrat'/'TOR', 'cephalid aristocrat').
card_uid('cephalid aristocrat'/'TOR', 'TOR:Cephalid Aristocrat:cephalid aristocrat').
card_rarity('cephalid aristocrat'/'TOR', 'Common').
card_artist('cephalid aristocrat'/'TOR', 'Rob Alexander').
card_number('cephalid aristocrat'/'TOR', '27').
card_flavor_text('cephalid aristocrat'/'TOR', 'It\'s easy to tell the difference between the devious cephalids and the trustworthy ones. The trustworthy ones are dead.').
card_multiverse_id('cephalid aristocrat'/'TOR', '29708').

card_in_set('cephalid illusionist', 'TOR').
card_original_type('cephalid illusionist'/'TOR', 'Creature — Cephalid Wizard').
card_original_text('cephalid illusionist'/'TOR', 'Whenever Cephalid Illusionist becomes the target of a spell or ability, put the top three cards of your library into your graveyard.\n{2}{U}, {T}: This turn prevent all combat damage that would be dealt to and dealt by target creature you control.').
card_first_print('cephalid illusionist', 'TOR').
card_image_name('cephalid illusionist'/'TOR', 'cephalid illusionist').
card_uid('cephalid illusionist'/'TOR', 'TOR:Cephalid Illusionist:cephalid illusionist').
card_rarity('cephalid illusionist'/'TOR', 'Uncommon').
card_artist('cephalid illusionist'/'TOR', 'Pete Venters').
card_number('cephalid illusionist'/'TOR', '28').
card_multiverse_id('cephalid illusionist'/'TOR', '34776').

card_in_set('cephalid sage', 'TOR').
card_original_type('cephalid sage'/'TOR', 'Creature — Cephalid').
card_original_text('cephalid sage'/'TOR', 'Threshold When Cephalid Sage comes into play, draw three cards, then discard two cards from your hand. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('cephalid sage', 'TOR').
card_image_name('cephalid sage'/'TOR', 'cephalid sage').
card_uid('cephalid sage'/'TOR', 'TOR:Cephalid Sage:cephalid sage').
card_rarity('cephalid sage'/'TOR', 'Uncommon').
card_artist('cephalid sage'/'TOR', 'Keith Garletts').
card_number('cephalid sage'/'TOR', '29').
card_multiverse_id('cephalid sage'/'TOR', '33690').

card_in_set('cephalid snitch', 'TOR').
card_original_type('cephalid snitch'/'TOR', 'Creature — Cephalid Wizard').
card_original_text('cephalid snitch'/'TOR', 'Sacrifice Cephalid Snitch: Target creature loses protection from black until end of turn.').
card_first_print('cephalid snitch', 'TOR').
card_image_name('cephalid snitch'/'TOR', 'cephalid snitch').
card_uid('cephalid snitch'/'TOR', 'TOR:Cephalid Snitch:cephalid snitch').
card_rarity('cephalid snitch'/'TOR', 'Common').
card_artist('cephalid snitch'/'TOR', 'Jerry Tiritilli').
card_number('cephalid snitch'/'TOR', '30').
card_flavor_text('cephalid snitch'/'TOR', 'The cautious are wary of their enemies. The wise are also wary of their friends.').
card_multiverse_id('cephalid snitch'/'TOR', '31840').

card_in_set('cephalid vandal', 'TOR').
card_original_type('cephalid vandal'/'TOR', 'Creature — Cephalid').
card_original_text('cephalid vandal'/'TOR', 'At the beginning of your upkeep, put a shred counter on Cephalid Vandal. Then put the top card of your library into your graveyard for each shred counter on Cephalid Vandal.').
card_first_print('cephalid vandal', 'TOR').
card_image_name('cephalid vandal'/'TOR', 'cephalid vandal').
card_uid('cephalid vandal'/'TOR', 'TOR:Cephalid Vandal:cephalid vandal').
card_rarity('cephalid vandal'/'TOR', 'Rare').
card_artist('cephalid vandal'/'TOR', 'Alex Horley-Orlandelli').
card_number('cephalid vandal'/'TOR', '31').
card_multiverse_id('cephalid vandal'/'TOR', '36034').

card_in_set('chainer\'s edict', 'TOR').
card_original_type('chainer\'s edict'/'TOR', 'Sorcery').
card_original_text('chainer\'s edict'/'TOR', 'Target player sacrifices a creature.\nFlashback {5}{B}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('chainer\'s edict'/'TOR', 'chainer\'s edict').
card_uid('chainer\'s edict'/'TOR', 'TOR:Chainer\'s Edict:chainer\'s edict').
card_rarity('chainer\'s edict'/'TOR', 'Uncommon').
card_artist('chainer\'s edict'/'TOR', 'Ben Thompson').
card_number('chainer\'s edict'/'TOR', '57').
card_flavor_text('chainer\'s edict'/'TOR', 'The pits have their own form of mercy.').
card_multiverse_id('chainer\'s edict'/'TOR', '29603').

card_in_set('chainer, dementia master', 'TOR').
card_original_type('chainer, dementia master'/'TOR', 'Creature — Minion Legend').
card_original_text('chainer, dementia master'/'TOR', 'All Nightmares get +1/+1.\n{B}{B}{B}, Pay 3 life: Put target creature card from a graveyard into play under your control. That creature is black and is a Nightmare in addition to its creature types.\nWhen Chainer, Dementia Master leaves play, remove all Nightmares from the game.').
card_first_print('chainer, dementia master', 'TOR').
card_image_name('chainer, dementia master'/'TOR', 'chainer, dementia master').
card_uid('chainer, dementia master'/'TOR', 'TOR:Chainer, Dementia Master:chainer, dementia master').
card_rarity('chainer, dementia master'/'TOR', 'Rare').
card_artist('chainer, dementia master'/'TOR', 'Mark Zug').
card_number('chainer, dementia master'/'TOR', '56').
card_multiverse_id('chainer, dementia master'/'TOR', '32916').

card_in_set('churning eddy', 'TOR').
card_original_type('churning eddy'/'TOR', 'Sorcery').
card_original_text('churning eddy'/'TOR', 'Return target creature and target land to their owners\' hands.').
card_first_print('churning eddy', 'TOR').
card_image_name('churning eddy'/'TOR', 'churning eddy').
card_uid('churning eddy'/'TOR', 'TOR:Churning Eddy:churning eddy').
card_rarity('churning eddy'/'TOR', 'Common').
card_artist('churning eddy'/'TOR', 'Thomas M. Baxa').
card_number('churning eddy'/'TOR', '32').
card_flavor_text('churning eddy'/'TOR', '\"Magic is like the tide—both ebb, both flow, and both serve my whims.\"\n—Empress Llawan').
card_multiverse_id('churning eddy'/'TOR', '29720').

card_in_set('circular logic', 'TOR').
card_original_type('circular logic'/'TOR', 'Instant').
card_original_text('circular logic'/'TOR', 'Counter target spell unless its controller pays {1} for each card in your graveyard.\nMadness {U} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_image_name('circular logic'/'TOR', 'circular logic').
card_uid('circular logic'/'TOR', 'TOR:Circular Logic:circular logic').
card_rarity('circular logic'/'TOR', 'Uncommon').
card_artist('circular logic'/'TOR', 'Anthony S. Waters').
card_number('circular logic'/'TOR', '33').
card_multiverse_id('circular logic'/'TOR', '34368').

card_in_set('cleansing meditation', 'TOR').
card_original_type('cleansing meditation'/'TOR', 'Sorcery').
card_original_text('cleansing meditation'/'TOR', 'Destroy all enchantments.\nThreshold Instead destroy all enchantments, then return to play all cards in your graveyard destroyed this way. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('cleansing meditation', 'TOR').
card_image_name('cleansing meditation'/'TOR', 'cleansing meditation').
card_uid('cleansing meditation'/'TOR', 'TOR:Cleansing Meditation:cleansing meditation').
card_rarity('cleansing meditation'/'TOR', 'Uncommon').
card_artist('cleansing meditation'/'TOR', 'Ron Spears').
card_number('cleansing meditation'/'TOR', '3').
card_multiverse_id('cleansing meditation'/'TOR', '29883').

card_in_set('compulsion', 'TOR').
card_original_type('compulsion'/'TOR', 'Enchantment').
card_original_text('compulsion'/'TOR', '{1}{U}, Discard a card from your hand: Draw a card. \n{1}{U}, Sacrifice Compulsion: Draw a card.').
card_first_print('compulsion', 'TOR').
card_image_name('compulsion'/'TOR', 'compulsion').
card_uid('compulsion'/'TOR', 'TOR:Compulsion:compulsion').
card_rarity('compulsion'/'TOR', 'Uncommon').
card_artist('compulsion'/'TOR', 'Christopher Moeller').
card_number('compulsion'/'TOR', '34').
card_multiverse_id('compulsion'/'TOR', '34473').

card_in_set('coral net', 'TOR').
card_original_type('coral net'/'TOR', 'Enchant Creature').
card_original_text('coral net'/'TOR', 'Coral Net can enchant only a green or white creature.\nEnchanted creature has \"At the beginning of your upkeep, sacrifice this creature unless you discard a card from your hand.\"').
card_first_print('coral net', 'TOR').
card_image_name('coral net'/'TOR', 'coral net').
card_uid('coral net'/'TOR', 'TOR:Coral Net:coral net').
card_rarity('coral net'/'TOR', 'Common').
card_artist('coral net'/'TOR', 'Roger Raupp').
card_number('coral net'/'TOR', '35').
card_multiverse_id('coral net'/'TOR', '19696').

card_in_set('crackling club', 'TOR').
card_original_type('crackling club'/'TOR', 'Enchant Creature').
card_original_text('crackling club'/'TOR', 'Enchanted creature gets +1/+0.\nSacrifice Crackling Club: Crackling Club deals 1 damage to target creature.').
card_first_print('crackling club', 'TOR').
card_image_name('crackling club'/'TOR', 'crackling club').
card_uid('crackling club'/'TOR', 'TOR:Crackling Club:crackling club').
card_rarity('crackling club'/'TOR', 'Common').
card_artist('crackling club'/'TOR', 'Mike Ploog').
card_number('crackling club'/'TOR', '93').
card_flavor_text('crackling club'/'TOR', 'It adds injury to injury.').
card_multiverse_id('crackling club'/'TOR', '34278').

card_in_set('crazed firecat', 'TOR').
card_original_type('crazed firecat'/'TOR', 'Creature — Cat').
card_original_text('crazed firecat'/'TOR', 'When Crazed Firecat comes into play, flip a coin until you lose a flip. Put a +1/+1 counter on Crazed Firecat for each flip you win.').
card_first_print('crazed firecat', 'TOR').
card_image_name('crazed firecat'/'TOR', 'crazed firecat').
card_uid('crazed firecat'/'TOR', 'TOR:Crazed Firecat:crazed firecat').
card_rarity('crazed firecat'/'TOR', 'Uncommon').
card_artist('crazed firecat'/'TOR', 'Ron Spears').
card_number('crazed firecat'/'TOR', '94').
card_flavor_text('crazed firecat'/'TOR', '\"The longer it\'s caged, the madder it gets.\"\n—Firecat handler').
card_multiverse_id('crazed firecat'/'TOR', '29754').

card_in_set('crippling fatigue', 'TOR').
card_original_type('crippling fatigue'/'TOR', 'Sorcery').
card_original_text('crippling fatigue'/'TOR', 'Target creature gets -2/-2 until end of turn.\nFlashback—{1}{B}, Pay 3 life. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('crippling fatigue', 'TOR').
card_image_name('crippling fatigue'/'TOR', 'crippling fatigue').
card_uid('crippling fatigue'/'TOR', 'TOR:Crippling Fatigue:crippling fatigue').
card_rarity('crippling fatigue'/'TOR', 'Common').
card_artist('crippling fatigue'/'TOR', 'Heather Hudson').
card_number('crippling fatigue'/'TOR', '58').
card_multiverse_id('crippling fatigue'/'TOR', '34225').

card_in_set('dawn of the dead', 'TOR').
card_original_type('dawn of the dead'/'TOR', 'Enchantment').
card_original_text('dawn of the dead'/'TOR', 'At the beginning of your upkeep, you lose 1 life.\nAt the beginning of your upkeep, you may return target creature card from your graveyard to play. That creature gains haste until end of turn. Remove it from the game at end of turn.').
card_first_print('dawn of the dead', 'TOR').
card_image_name('dawn of the dead'/'TOR', 'dawn of the dead').
card_uid('dawn of the dead'/'TOR', 'TOR:Dawn of the Dead:dawn of the dead').
card_rarity('dawn of the dead'/'TOR', 'Rare').
card_artist('dawn of the dead'/'TOR', 'Pete Venters').
card_number('dawn of the dead'/'TOR', '59').
card_multiverse_id('dawn of the dead'/'TOR', '35924').

card_in_set('deep analysis', 'TOR').
card_original_type('deep analysis'/'TOR', 'Sorcery').
card_original_text('deep analysis'/'TOR', 'Target player draws two cards.\nFlashback—{1}{U}, Pay 3 life. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('deep analysis'/'TOR', 'deep analysis').
card_uid('deep analysis'/'TOR', 'TOR:Deep Analysis:deep analysis').
card_rarity('deep analysis'/'TOR', 'Common').
card_artist('deep analysis'/'TOR', 'Daren Bader').
card_number('deep analysis'/'TOR', '36').
card_flavor_text('deep analysis'/'TOR', '\"The specimen seems to be broken.\"').
card_multiverse_id('deep analysis'/'TOR', '32237').

card_in_set('devastating dreams', 'TOR').
card_original_type('devastating dreams'/'TOR', 'Sorcery').
card_original_text('devastating dreams'/'TOR', 'As an additional cost to play Devastating Dreams, discard X cards at random from your hand.\nEach player sacrifices X lands. Devastating Dreams deals X damage to each creature.').
card_first_print('devastating dreams', 'TOR').
card_image_name('devastating dreams'/'TOR', 'devastating dreams').
card_uid('devastating dreams'/'TOR', 'TOR:Devastating Dreams:devastating dreams').
card_rarity('devastating dreams'/'TOR', 'Rare').
card_artist('devastating dreams'/'TOR', 'Tony Szczudlo').
card_number('devastating dreams'/'TOR', '95').
card_flavor_text('devastating dreams'/'TOR', 'Kamahl dreams of storms of fire.').
card_multiverse_id('devastating dreams'/'TOR', '30561').

card_in_set('dwell on the past', 'TOR').
card_original_type('dwell on the past'/'TOR', 'Sorcery').
card_original_text('dwell on the past'/'TOR', 'Target player shuffles up to four target cards from his or her graveyard into his or her library.').
card_first_print('dwell on the past', 'TOR').
card_image_name('dwell on the past'/'TOR', 'dwell on the past').
card_uid('dwell on the past'/'TOR', 'TOR:Dwell on the Past:dwell on the past').
card_rarity('dwell on the past'/'TOR', 'Uncommon').
card_artist('dwell on the past'/'TOR', 'Rebecca Guay').
card_number('dwell on the past'/'TOR', '124').
card_flavor_text('dwell on the past'/'TOR', 'None can find what hasn\'t been lost.\n—Nantuko teaching').
card_multiverse_id('dwell on the past'/'TOR', '26404').

card_in_set('enslaved dwarf', 'TOR').
card_original_type('enslaved dwarf'/'TOR', 'Creature — Dwarf').
card_original_text('enslaved dwarf'/'TOR', '{R}, Sacrifice Enslaved Dwarf: Target black creature gets +1/+0 and gains first strike until end of turn.').
card_first_print('enslaved dwarf', 'TOR').
card_image_name('enslaved dwarf'/'TOR', 'enslaved dwarf').
card_uid('enslaved dwarf'/'TOR', 'TOR:Enslaved Dwarf:enslaved dwarf').
card_rarity('enslaved dwarf'/'TOR', 'Common').
card_artist('enslaved dwarf'/'TOR', 'Terese Nielsen').
card_number('enslaved dwarf'/'TOR', '96').
card_flavor_text('enslaved dwarf'/'TOR', 'Captive dwarves always regain their freedom, either through escape or through a desperate, fiery death.').
card_multiverse_id('enslaved dwarf'/'TOR', '32917').

card_in_set('equal treatment', 'TOR').
card_original_type('equal treatment'/'TOR', 'Instant').
card_original_text('equal treatment'/'TOR', 'If any source would deal 1 or more damage to a creature or player this turn, it deals 2 damage to that creature or player instead.\nDraw a card.').
card_first_print('equal treatment', 'TOR').
card_image_name('equal treatment'/'TOR', 'equal treatment').
card_uid('equal treatment'/'TOR', 'TOR:Equal Treatment:equal treatment').
card_rarity('equal treatment'/'TOR', 'Uncommon').
card_artist('equal treatment'/'TOR', 'Greg & Tim Hildebrandt').
card_number('equal treatment'/'TOR', '4').
card_multiverse_id('equal treatment'/'TOR', '29571').

card_in_set('faceless butcher', 'TOR').
card_original_type('faceless butcher'/'TOR', 'Creature — Nightmare Horror').
card_original_text('faceless butcher'/'TOR', 'When Faceless Butcher comes into play, remove target creature other than Faceless Butcher from the game.\nWhen Faceless Butcher leaves play, return the removed card to play under its owner\'s control.').
card_first_print('faceless butcher', 'TOR').
card_image_name('faceless butcher'/'TOR', 'faceless butcher').
card_uid('faceless butcher'/'TOR', 'TOR:Faceless Butcher:faceless butcher').
card_rarity('faceless butcher'/'TOR', 'Common').
card_artist('faceless butcher'/'TOR', 'Daren Bader').
card_number('faceless butcher'/'TOR', '60').
card_multiverse_id('faceless butcher'/'TOR', '29739').

card_in_set('false memories', 'TOR').
card_original_type('false memories'/'TOR', 'Instant').
card_original_text('false memories'/'TOR', 'Put the top seven cards of your library into your graveyard. At end of turn, remove seven cards in your graveyard from the game.').
card_first_print('false memories', 'TOR').
card_image_name('false memories'/'TOR', 'false memories').
card_uid('false memories'/'TOR', 'TOR:False Memories:false memories').
card_rarity('false memories'/'TOR', 'Rare').
card_artist('false memories'/'TOR', 'Ron Spencer').
card_number('false memories'/'TOR', '37').
card_flavor_text('false memories'/'TOR', '\"My enemies will forget everything but their anguish.\"\n—Ambassador Laquatus').
card_multiverse_id('false memories'/'TOR', '5598').

card_in_set('far wanderings', 'TOR').
card_original_type('far wanderings'/'TOR', 'Sorcery').
card_original_text('far wanderings'/'TOR', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.\nThreshold Instead search your library for three basic land cards and put them into play tapped. Then shuffle your library. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('far wanderings', 'TOR').
card_image_name('far wanderings'/'TOR', 'far wanderings').
card_uid('far wanderings'/'TOR', 'TOR:Far Wanderings:far wanderings').
card_rarity('far wanderings'/'TOR', 'Common').
card_artist('far wanderings'/'TOR', 'Darrell Riche').
card_number('far wanderings'/'TOR', '125').
card_multiverse_id('far wanderings'/'TOR', '15229').

card_in_set('fiery temper', 'TOR').
card_original_type('fiery temper'/'TOR', 'Instant').
card_original_text('fiery temper'/'TOR', 'Fiery Temper deals 3 damage to target creature or player.\nMadness {R} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('fiery temper', 'TOR').
card_image_name('fiery temper'/'TOR', 'fiery temper').
card_uid('fiery temper'/'TOR', 'TOR:Fiery Temper:fiery temper').
card_rarity('fiery temper'/'TOR', 'Common').
card_artist('fiery temper'/'TOR', 'Greg & Tim Hildebrandt').
card_number('fiery temper'/'TOR', '97').
card_multiverse_id('fiery temper'/'TOR', '32918').

card_in_set('flaming gambit', 'TOR').
card_original_type('flaming gambit'/'TOR', 'Instant').
card_original_text('flaming gambit'/'TOR', 'Flaming Gambit deals X damage to target player. That player may choose a creature he or she controls and have Flaming Gambit deal that damage to it instead.\nFlashback {X}{R}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('flaming gambit', 'TOR').
card_image_name('flaming gambit'/'TOR', 'flaming gambit').
card_uid('flaming gambit'/'TOR', 'TOR:Flaming Gambit:flaming gambit').
card_rarity('flaming gambit'/'TOR', 'Uncommon').
card_artist('flaming gambit'/'TOR', 'Donato Giancola').
card_number('flaming gambit'/'TOR', '98').
card_multiverse_id('flaming gambit'/'TOR', '32921').

card_in_set('flash of defiance', 'TOR').
card_original_type('flash of defiance'/'TOR', 'Sorcery').
card_original_text('flash of defiance'/'TOR', 'Players can\'t block with green and/or white creatures this turn.\nFlashback—{1}{R}, Pay 3 life. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('flash of defiance', 'TOR').
card_image_name('flash of defiance'/'TOR', 'flash of defiance').
card_uid('flash of defiance'/'TOR', 'TOR:Flash of Defiance:flash of defiance').
card_rarity('flash of defiance'/'TOR', 'Common').
card_artist('flash of defiance'/'TOR', 'Carl Critchlow').
card_number('flash of defiance'/'TOR', '99').
card_multiverse_id('flash of defiance'/'TOR', '29863').

card_in_set('floating shield', 'TOR').
card_original_type('floating shield'/'TOR', 'Enchant Creature').
card_original_text('floating shield'/'TOR', 'As Floating Shield comes into play, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Floating Shield.\nSacrifice Floating Shield: Target creature gains protection from the chosen color until end of turn.').
card_first_print('floating shield', 'TOR').
card_image_name('floating shield'/'TOR', 'floating shield').
card_uid('floating shield'/'TOR', 'TOR:Floating Shield:floating shield').
card_rarity('floating shield'/'TOR', 'Common').
card_artist('floating shield'/'TOR', 'Keith Garletts').
card_number('floating shield'/'TOR', '5').
card_multiverse_id('floating shield'/'TOR', '32396').

card_in_set('frantic purification', 'TOR').
card_original_type('frantic purification'/'TOR', 'Instant').
card_original_text('frantic purification'/'TOR', 'Destroy target enchantment.\nMadness {W} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('frantic purification', 'TOR').
card_image_name('frantic purification'/'TOR', 'frantic purification').
card_uid('frantic purification'/'TOR', 'TOR:Frantic Purification:frantic purification').
card_rarity('frantic purification'/'TOR', 'Common').
card_artist('frantic purification'/'TOR', 'Mark Brill').
card_number('frantic purification'/'TOR', '6').
card_flavor_text('frantic purification'/'TOR', 'By definition, madness ends in one of two ways: clarity . . . or death.').
card_multiverse_id('frantic purification'/'TOR', '29841').

card_in_set('ghostly wings', 'TOR').
card_original_type('ghostly wings'/'TOR', 'Enchant Creature').
card_original_text('ghostly wings'/'TOR', 'Enchanted creature gets +1/+1 and has flying.\nDiscard a card from your hand: Return enchanted creature to its owner\'s hand.').
card_first_print('ghostly wings', 'TOR').
card_image_name('ghostly wings'/'TOR', 'ghostly wings').
card_uid('ghostly wings'/'TOR', 'TOR:Ghostly Wings:ghostly wings').
card_rarity('ghostly wings'/'TOR', 'Common').
card_artist('ghostly wings'/'TOR', 'David Martin').
card_number('ghostly wings'/'TOR', '38').
card_multiverse_id('ghostly wings'/'TOR', '32235').

card_in_set('gloomdrifter', 'TOR').
card_original_type('gloomdrifter'/'TOR', 'Creature — Minion').
card_original_text('gloomdrifter'/'TOR', 'Flying\nThreshold When Gloomdrifter comes into play, nonblack creatures get -2/-2 until end of turn. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('gloomdrifter', 'TOR').
card_image_name('gloomdrifter'/'TOR', 'gloomdrifter').
card_uid('gloomdrifter'/'TOR', 'TOR:Gloomdrifter:gloomdrifter').
card_rarity('gloomdrifter'/'TOR', 'Uncommon').
card_artist('gloomdrifter'/'TOR', 'Adam Rex').
card_number('gloomdrifter'/'TOR', '61').
card_multiverse_id('gloomdrifter'/'TOR', '12405').

card_in_set('gravegouger', 'TOR').
card_original_type('gravegouger'/'TOR', 'Creature — Nightmare Horror').
card_original_text('gravegouger'/'TOR', 'When Gravegouger comes into play, remove up to two target cards in a single graveyard from the game.\nWhen Gravegouger leaves play, return the removed cards to their owner\'s graveyard.').
card_first_print('gravegouger', 'TOR').
card_image_name('gravegouger'/'TOR', 'gravegouger').
card_uid('gravegouger'/'TOR', 'TOR:Gravegouger:gravegouger').
card_rarity('gravegouger'/'TOR', 'Common').
card_artist('gravegouger'/'TOR', 'Daren Bader').
card_number('gravegouger'/'TOR', '62').
card_multiverse_id('gravegouger'/'TOR', '32210').

card_in_set('grim lavamancer', 'TOR').
card_original_type('grim lavamancer'/'TOR', 'Creature — Wizard').
card_original_text('grim lavamancer'/'TOR', '{R}, {T}, Remove two cards in your graveyard from the game: Grim Lavamancer deals 2 damage to target creature or player.').
card_image_name('grim lavamancer'/'TOR', 'grim lavamancer').
card_uid('grim lavamancer'/'TOR', 'TOR:Grim Lavamancer:grim lavamancer').
card_rarity('grim lavamancer'/'TOR', 'Rare').
card_artist('grim lavamancer'/'TOR', 'Jim Nelson').
card_number('grim lavamancer'/'TOR', '100').
card_flavor_text('grim lavamancer'/'TOR', '\"Fools dig for water, corpses, or gold. The earth\'s real treasure is far deeper.\"').
card_multiverse_id('grim lavamancer'/'TOR', '36111').

card_in_set('grotesque hybrid', 'TOR').
card_original_type('grotesque hybrid'/'TOR', 'Creature — Zombie').
card_original_text('grotesque hybrid'/'TOR', 'Whenever Grotesque Hybrid deals combat damage to a creature, destroy that creature. It can\'t be regenerated.\nDiscard a card from your hand: Grotesque Hybrid gains flying and protection from green and from white until end of turn.').
card_first_print('grotesque hybrid', 'TOR').
card_image_name('grotesque hybrid'/'TOR', 'grotesque hybrid').
card_uid('grotesque hybrid'/'TOR', 'TOR:Grotesque Hybrid:grotesque hybrid').
card_rarity('grotesque hybrid'/'TOR', 'Uncommon').
card_artist('grotesque hybrid'/'TOR', 'Terese Nielsen').
card_number('grotesque hybrid'/'TOR', '63').
card_multiverse_id('grotesque hybrid'/'TOR', '32222').

card_in_set('gurzigost', 'TOR').
card_original_type('gurzigost'/'TOR', 'Creature — Beast').
card_original_text('gurzigost'/'TOR', 'At the beginning of your upkeep, sacrifice Gurzigost unless you put two cards from your graveyard on the bottom of your library.\n{G}{G}, Discard a card from your hand: You may have Gurzigost deal its combat damage to defending player this turn as though it weren\'t blocked.').
card_first_print('gurzigost', 'TOR').
card_image_name('gurzigost'/'TOR', 'gurzigost').
card_uid('gurzigost'/'TOR', 'TOR:Gurzigost:gurzigost').
card_rarity('gurzigost'/'TOR', 'Rare').
card_artist('gurzigost'/'TOR', 'Scott M. Fischer').
card_number('gurzigost'/'TOR', '126').
card_multiverse_id('gurzigost'/'TOR', '32221').

card_in_set('hell-bent raider', 'TOR').
card_original_type('hell-bent raider'/'TOR', 'Creature — Barbarian').
card_original_text('hell-bent raider'/'TOR', 'First strike, haste\nDiscard a card at random from your hand: Hell-Bent Raider gains protection from white until end of turn.').
card_first_print('hell-bent raider', 'TOR').
card_image_name('hell-bent raider'/'TOR', 'hell-bent raider').
card_uid('hell-bent raider'/'TOR', 'TOR:Hell-Bent Raider:hell-bent raider').
card_rarity('hell-bent raider'/'TOR', 'Rare').
card_artist('hell-bent raider'/'TOR', 'Mike Ploog').
card_number('hell-bent raider'/'TOR', '101').
card_flavor_text('hell-bent raider'/'TOR', 'He doesn\'t slow until his spear is weighted with corpses.').
card_multiverse_id('hell-bent raider'/'TOR', '34384').

card_in_set('hydromorph guardian', 'TOR').
card_original_type('hydromorph guardian'/'TOR', 'Creature — Guardian').
card_original_text('hydromorph guardian'/'TOR', '{U}, Sacrifice Hydromorph Guardian: Counter target spell that targets one or more creatures you control.').
card_first_print('hydromorph guardian', 'TOR').
card_image_name('hydromorph guardian'/'TOR', 'hydromorph guardian').
card_uid('hydromorph guardian'/'TOR', 'TOR:Hydromorph Guardian:hydromorph guardian').
card_rarity('hydromorph guardian'/'TOR', 'Common').
card_artist('hydromorph guardian'/'TOR', 'Glen Angus').
card_number('hydromorph guardian'/'TOR', '39').
card_flavor_text('hydromorph guardian'/'TOR', 'In front of every strong leader is a pool of loyal bodyguards.').
card_multiverse_id('hydromorph guardian'/'TOR', '33627').

card_in_set('hydromorph gull', 'TOR').
card_original_type('hydromorph gull'/'TOR', 'Creature — Bird Guardian').
card_original_text('hydromorph gull'/'TOR', 'Flying\n{U}, Sacrifice Hydromorph Gull: Counter target spell that targets one or more creatures you control.').
card_first_print('hydromorph gull', 'TOR').
card_image_name('hydromorph gull'/'TOR', 'hydromorph gull').
card_uid('hydromorph gull'/'TOR', 'TOR:Hydromorph Gull:hydromorph gull').
card_rarity('hydromorph gull'/'TOR', 'Uncommon').
card_artist('hydromorph gull'/'TOR', 'Arnie Swekel').
card_number('hydromorph gull'/'TOR', '40').
card_flavor_text('hydromorph gull'/'TOR', '\"The only kind of water that should fly through the air is rain.\"\n—Kamahl, pit fighter').
card_multiverse_id('hydromorph gull'/'TOR', '29719').

card_in_set('hypnox', 'TOR').
card_original_type('hypnox'/'TOR', 'Creature — Nightmare Horror').
card_original_text('hypnox'/'TOR', 'Flying\nWhen Hypnox comes into play, if you played it from your hand, remove all cards in target opponent\'s hand from the game.\nWhen Hypnox leaves play, return the removed cards to their owner\'s hand.').
card_first_print('hypnox', 'TOR').
card_image_name('hypnox'/'TOR', 'hypnox').
card_uid('hypnox'/'TOR', 'TOR:Hypnox:hypnox').
card_rarity('hypnox'/'TOR', 'Rare').
card_artist('hypnox'/'TOR', 'Greg Staples').
card_number('hypnox'/'TOR', '64').
card_multiverse_id('hypnox'/'TOR', '31831').

card_in_set('hypochondria', 'TOR').
card_original_type('hypochondria'/'TOR', 'Enchantment').
card_original_text('hypochondria'/'TOR', '{W}, Discard a card from your hand: Prevent the next 3 damage that would be dealt to target creature or player this turn.\n{W}, Sacrifice Hypochondria: Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_first_print('hypochondria', 'TOR').
card_image_name('hypochondria'/'TOR', 'hypochondria').
card_uid('hypochondria'/'TOR', 'TOR:Hypochondria:hypochondria').
card_rarity('hypochondria'/'TOR', 'Uncommon').
card_artist('hypochondria'/'TOR', 'Christopher Moeller').
card_number('hypochondria'/'TOR', '7').
card_multiverse_id('hypochondria'/'TOR', '29705').

card_in_set('ichorid', 'TOR').
card_original_type('ichorid'/'TOR', 'Creature — Horror').
card_original_text('ichorid'/'TOR', 'Haste\nAt end of turn, sacrifice Ichorid.\nAt the beginning of your upkeep, if Ichorid is in your graveyard, you may remove a black creature card in your graveyard other than Ichorid from the game. If you do, return Ichorid to play.').
card_first_print('ichorid', 'TOR').
card_image_name('ichorid'/'TOR', 'ichorid').
card_uid('ichorid'/'TOR', 'TOR:Ichorid:ichorid').
card_rarity('ichorid'/'TOR', 'Rare').
card_artist('ichorid'/'TOR', 'rk post').
card_number('ichorid'/'TOR', '65').
card_multiverse_id('ichorid'/'TOR', '35923').

card_in_set('insidious dreams', 'TOR').
card_original_type('insidious dreams'/'TOR', 'Instant').
card_original_text('insidious dreams'/'TOR', 'As an additional cost to play Insidious Dreams, discard X cards from your hand.\nSearch your library for X cards. Then shuffle your library and put those cards on top of it in any order.').
card_first_print('insidious dreams', 'TOR').
card_image_name('insidious dreams'/'TOR', 'insidious dreams').
card_uid('insidious dreams'/'TOR', 'TOR:Insidious Dreams:insidious dreams').
card_rarity('insidious dreams'/'TOR', 'Rare').
card_artist('insidious dreams'/'TOR', 'John Avon').
card_number('insidious dreams'/'TOR', '66').
card_flavor_text('insidious dreams'/'TOR', 'Chainer dreams of ultimate knowledge.').
card_multiverse_id('insidious dreams'/'TOR', '29825').

card_in_set('insist', 'TOR').
card_original_type('insist'/'TOR', 'Sorcery').
card_original_text('insist'/'TOR', 'The next creature spell you play this turn can\'t be countered by spells or abilities.\nDraw a card.').
card_first_print('insist', 'TOR').
card_image_name('insist'/'TOR', 'insist').
card_uid('insist'/'TOR', 'TOR:Insist:insist').
card_rarity('insist'/'TOR', 'Rare').
card_artist('insist'/'TOR', 'Franz Vohwinkel').
card_number('insist'/'TOR', '127').
card_flavor_text('insist'/'TOR', '\"Finesse is no match for brute force.\"\n—Seton, centaur druid').
card_multiverse_id('insist'/'TOR', '29886').

card_in_set('invigorating falls', 'TOR').
card_original_type('invigorating falls'/'TOR', 'Sorcery').
card_original_text('invigorating falls'/'TOR', 'You gain life equal to the number of creature cards in all graveyards.').
card_first_print('invigorating falls', 'TOR').
card_image_name('invigorating falls'/'TOR', 'invigorating falls').
card_uid('invigorating falls'/'TOR', 'TOR:Invigorating Falls:invigorating falls').
card_rarity('invigorating falls'/'TOR', 'Common').
card_artist('invigorating falls'/'TOR', 'Rebecca Guay').
card_number('invigorating falls'/'TOR', '128').
card_flavor_text('invigorating falls'/'TOR', 'Krosan druids do not fear death, for they know that nature will only prosper from their passing.').
card_multiverse_id('invigorating falls'/'TOR', '29907').

card_in_set('kamahl\'s sledge', 'TOR').
card_original_type('kamahl\'s sledge'/'TOR', 'Sorcery').
card_original_text('kamahl\'s sledge'/'TOR', 'Kamahl\'s Sledge deals 4 damage to target creature.\nThreshold Instead Kamahl\'s Sledge deals 4 damage to that creature and 4 damage to that creature\'s controller. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('kamahl\'s sledge', 'TOR').
card_image_name('kamahl\'s sledge'/'TOR', 'kamahl\'s sledge').
card_uid('kamahl\'s sledge'/'TOR', 'TOR:Kamahl\'s Sledge:kamahl\'s sledge').
card_rarity('kamahl\'s sledge'/'TOR', 'Common').
card_artist('kamahl\'s sledge'/'TOR', 'Don Hazeltine').
card_number('kamahl\'s sledge'/'TOR', '102').
card_multiverse_id('kamahl\'s sledge'/'TOR', '32229').

card_in_set('krosan constrictor', 'TOR').
card_original_type('krosan constrictor'/'TOR', 'Creature — Snake').
card_original_text('krosan constrictor'/'TOR', 'Swampwalk\n{T}: Target black creature gets -2/-0 until end of turn.').
card_first_print('krosan constrictor', 'TOR').
card_image_name('krosan constrictor'/'TOR', 'krosan constrictor').
card_uid('krosan constrictor'/'TOR', 'TOR:Krosan Constrictor:krosan constrictor').
card_rarity('krosan constrictor'/'TOR', 'Common').
card_artist('krosan constrictor'/'TOR', 'Jim Nelson').
card_number('krosan constrictor'/'TOR', '129').
card_flavor_text('krosan constrictor'/'TOR', '\"It\'s a slithering, coldblooded menace. I\'ve gotta respect that.\"\n—Braids, dementia summoner').
card_multiverse_id('krosan constrictor'/'TOR', '29715').

card_in_set('krosan restorer', 'TOR').
card_original_type('krosan restorer'/'TOR', 'Creature — Druid').
card_original_text('krosan restorer'/'TOR', '{T}: Untap target land.\nThreshold {T}: Untap up to three target lands. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('krosan restorer', 'TOR').
card_image_name('krosan restorer'/'TOR', 'krosan restorer').
card_uid('krosan restorer'/'TOR', 'TOR:Krosan Restorer:krosan restorer').
card_rarity('krosan restorer'/'TOR', 'Common').
card_artist('krosan restorer'/'TOR', 'Clyde Caldwell').
card_number('krosan restorer'/'TOR', '130').
card_multiverse_id('krosan restorer'/'TOR', '29872').

card_in_set('laquatus\'s champion', 'TOR').
card_original_type('laquatus\'s champion'/'TOR', 'Creature — Nightmare Horror').
card_original_text('laquatus\'s champion'/'TOR', 'When Laquatus\'s Champion comes into play, target player loses 6 life. \nWhen Laquatus\'s Champion leaves play, that player gains 6 life.\n{B}: Regenerate Laquatus\'s Champion.').
card_image_name('laquatus\'s champion'/'TOR', 'laquatus\'s champion').
card_uid('laquatus\'s champion'/'TOR', 'TOR:Laquatus\'s Champion:laquatus\'s champion').
card_rarity('laquatus\'s champion'/'TOR', 'Rare').
card_artist('laquatus\'s champion'/'TOR', 'Greg Staples').
card_number('laquatus\'s champion'/'TOR', '67').
card_flavor_text('laquatus\'s champion'/'TOR', 'Chainer\'s dark gift to a darker soul.').
card_multiverse_id('laquatus\'s champion'/'TOR', '31869').

card_in_set('last laugh', 'TOR').
card_original_type('last laugh'/'TOR', 'Enchantment').
card_original_text('last laugh'/'TOR', 'Whenever a permanent other than Last Laugh is put into a graveyard from play, Last Laugh deals 1 damage to each creature and each player.\nWhen no creatures are in play, sacrifice Last Laugh.').
card_first_print('last laugh', 'TOR').
card_image_name('last laugh'/'TOR', 'last laugh').
card_uid('last laugh'/'TOR', 'TOR:Last Laugh:last laugh').
card_rarity('last laugh'/'TOR', 'Rare').
card_artist('last laugh'/'TOR', 'John Matson').
card_number('last laugh'/'TOR', '68').
card_multiverse_id('last laugh'/'TOR', '34467').

card_in_set('liquify', 'TOR').
card_original_type('liquify'/'TOR', 'Instant').
card_original_text('liquify'/'TOR', 'Counter target spell with converted mana cost 3 or less. If it\'s countered this way, remove it from the game instead of putting it into its owner\'s graveyard.').
card_first_print('liquify', 'TOR').
card_image_name('liquify'/'TOR', 'liquify').
card_uid('liquify'/'TOR', 'TOR:Liquify:liquify').
card_rarity('liquify'/'TOR', 'Common').
card_artist('liquify'/'TOR', 'Ron Spencer').
card_number('liquify'/'TOR', '41').
card_multiverse_id('liquify'/'TOR', '34756').

card_in_set('llawan, cephalid empress', 'TOR').
card_original_type('llawan, cephalid empress'/'TOR', 'Creature — Cephalid Legend').
card_original_text('llawan, cephalid empress'/'TOR', 'When Llawan, Cephalid Empress comes into play, return all blue creatures your opponents control to their owners\' hands.\nYour opponents can\'t play blue creature spells.').
card_first_print('llawan, cephalid empress', 'TOR').
card_image_name('llawan, cephalid empress'/'TOR', 'llawan, cephalid empress').
card_uid('llawan, cephalid empress'/'TOR', 'TOR:Llawan, Cephalid Empress:llawan, cephalid empress').
card_rarity('llawan, cephalid empress'/'TOR', 'Rare').
card_artist('llawan, cephalid empress'/'TOR', 'Mark Zug').
card_number('llawan, cephalid empress'/'TOR', '42').
card_multiverse_id('llawan, cephalid empress'/'TOR', '27175').

card_in_set('longhorn firebeast', 'TOR').
card_original_type('longhorn firebeast'/'TOR', 'Creature — Beast').
card_original_text('longhorn firebeast'/'TOR', 'When Longhorn Firebeast comes into play, any opponent may have it deal 5 damage to him or her. If a player does, sacrifice Longhorn Firebeast.').
card_first_print('longhorn firebeast', 'TOR').
card_image_name('longhorn firebeast'/'TOR', 'longhorn firebeast').
card_uid('longhorn firebeast'/'TOR', 'TOR:Longhorn Firebeast:longhorn firebeast').
card_rarity('longhorn firebeast'/'TOR', 'Common').
card_artist('longhorn firebeast'/'TOR', 'Glen Angus').
card_number('longhorn firebeast'/'TOR', '103').
card_flavor_text('longhorn firebeast'/'TOR', 'Its blast can fuse armor to bone, leaving skeletons that glint brightly in the sun.').
card_multiverse_id('longhorn firebeast'/'TOR', '29770').

card_in_set('major teroh', 'TOR').
card_original_type('major teroh'/'TOR', 'Creature — Bird Soldier Legend').
card_original_text('major teroh'/'TOR', 'Flying\n{3}{W}{W}, Sacrifice Major Teroh: Remove all black creatures from the game.').
card_first_print('major teroh', 'TOR').
card_image_name('major teroh'/'TOR', 'major teroh').
card_uid('major teroh'/'TOR', 'TOR:Major Teroh:major teroh').
card_rarity('major teroh'/'TOR', 'Rare').
card_artist('major teroh'/'TOR', 'Daren Bader').
card_number('major teroh'/'TOR', '8').
card_flavor_text('major teroh'/'TOR', '\"I am the holy wind that shall avenge the Cabal\'s victims.\"').
card_multiverse_id('major teroh'/'TOR', '29912').

card_in_set('mesmeric fiend', 'TOR').
card_original_type('mesmeric fiend'/'TOR', 'Creature — Nightmare Horror').
card_original_text('mesmeric fiend'/'TOR', 'When Mesmeric Fiend comes into play, target opponent reveals his or her hand and you choose a nonland card from it. Remove that card from the game.\nWhen Mesmeric Fiend leaves play, return the removed card to its owner\'s hand.').
card_first_print('mesmeric fiend', 'TOR').
card_image_name('mesmeric fiend'/'TOR', 'mesmeric fiend').
card_uid('mesmeric fiend'/'TOR', 'TOR:Mesmeric Fiend:mesmeric fiend').
card_rarity('mesmeric fiend'/'TOR', 'Common').
card_artist('mesmeric fiend'/'TOR', 'Dana Knutson').
card_number('mesmeric fiend'/'TOR', '69').
card_multiverse_id('mesmeric fiend'/'TOR', '32211').

card_in_set('militant monk', 'TOR').
card_original_type('militant monk'/'TOR', 'Creature — Cleric').
card_original_text('militant monk'/'TOR', 'Attacking doesn\'t cause Militant Monk to tap.\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('militant monk', 'TOR').
card_image_name('militant monk'/'TOR', 'militant monk').
card_uid('militant monk'/'TOR', 'TOR:Militant Monk:militant monk').
card_rarity('militant monk'/'TOR', 'Common').
card_artist('militant monk'/'TOR', 'Mark Brill').
card_number('militant monk'/'TOR', '9').
card_multiverse_id('militant monk'/'TOR', '12363').

card_in_set('mind sludge', 'TOR').
card_original_type('mind sludge'/'TOR', 'Sorcery').
card_original_text('mind sludge'/'TOR', 'Target player discards a card from his or her hand for each swamp you control.').
card_first_print('mind sludge', 'TOR').
card_image_name('mind sludge'/'TOR', 'mind sludge').
card_uid('mind sludge'/'TOR', 'TOR:Mind Sludge:mind sludge').
card_rarity('mind sludge'/'TOR', 'Uncommon').
card_artist('mind sludge'/'TOR', 'Eric Peterson').
card_number('mind sludge'/'TOR', '70').
card_flavor_text('mind sludge'/'TOR', 'When you get into the swamp, the swamp gets into you.').
card_multiverse_id('mind sludge'/'TOR', '26432').

card_in_set('morningtide', 'TOR').
card_original_type('morningtide'/'TOR', 'Sorcery').
card_original_text('morningtide'/'TOR', 'Remove all cards in all graveyards from the game.').
card_first_print('morningtide', 'TOR').
card_image_name('morningtide'/'TOR', 'morningtide').
card_uid('morningtide'/'TOR', 'TOR:Morningtide:morningtide').
card_rarity('morningtide'/'TOR', 'Rare').
card_artist('morningtide'/'TOR', 'Tony Szczudlo').
card_number('morningtide'/'TOR', '10').
card_flavor_text('morningtide'/'TOR', '\"The spirits of the righteous shall rise into the sky. Even dirtwalkers will fly like aven.\"\n—Major Teroh').
card_multiverse_id('morningtide'/'TOR', '34234').

card_in_set('mortal combat', 'TOR').
card_original_type('mortal combat'/'TOR', 'Enchantment').
card_original_text('mortal combat'/'TOR', 'At the beginning of your upkeep, if twenty or more creature cards are in your graveyard, you win the game.').
card_first_print('mortal combat', 'TOR').
card_image_name('mortal combat'/'TOR', 'mortal combat').
card_uid('mortal combat'/'TOR', 'TOR:Mortal Combat:mortal combat').
card_rarity('mortal combat'/'TOR', 'Rare').
card_artist('mortal combat'/'TOR', 'Mike Ploog').
card_number('mortal combat'/'TOR', '71').
card_flavor_text('mortal combat'/'TOR', 'The crowd roared, the fighters bled, and the dead piled high in the pits. Only the Cabal could win.').
card_multiverse_id('mortal combat'/'TOR', '29960').

card_in_set('mortiphobia', 'TOR').
card_original_type('mortiphobia'/'TOR', 'Enchantment').
card_original_text('mortiphobia'/'TOR', '{1}{B}, Discard a card from your hand: Remove target card in a graveyard from the game.\n{1}{B}, Sacrifice Mortiphobia: Remove target card in a graveyard from the game.').
card_first_print('mortiphobia', 'TOR').
card_image_name('mortiphobia'/'TOR', 'mortiphobia').
card_uid('mortiphobia'/'TOR', 'TOR:Mortiphobia:mortiphobia').
card_rarity('mortiphobia'/'TOR', 'Uncommon').
card_artist('mortiphobia'/'TOR', 'Christopher Moeller').
card_number('mortiphobia'/'TOR', '72').
card_multiverse_id('mortiphobia'/'TOR', '36433').

card_in_set('mutilate', 'TOR').
card_original_type('mutilate'/'TOR', 'Sorcery').
card_original_text('mutilate'/'TOR', 'All creatures get -1/-1 until end of turn for each swamp you control.').
card_first_print('mutilate', 'TOR').
card_image_name('mutilate'/'TOR', 'mutilate').
card_uid('mutilate'/'TOR', 'TOR:Mutilate:mutilate').
card_rarity('mutilate'/'TOR', 'Rare').
card_artist('mutilate'/'TOR', 'Eric Peterson').
card_number('mutilate'/'TOR', '73').
card_flavor_text('mutilate'/'TOR', '\"They appreciate my handiwork. I hear their joy in every scream.\"\n—Chainer, dementia master').
card_multiverse_id('mutilate'/'TOR', '35194').

card_in_set('mystic familiar', 'TOR').
card_original_type('mystic familiar'/'TOR', 'Creature — Bird').
card_original_text('mystic familiar'/'TOR', 'Flying\nThreshold Mystic Familiar gets +1/+1 and has protection from black. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('mystic familiar', 'TOR').
card_image_name('mystic familiar'/'TOR', 'mystic familiar').
card_uid('mystic familiar'/'TOR', 'TOR:Mystic Familiar:mystic familiar').
card_rarity('mystic familiar'/'TOR', 'Common').
card_artist('mystic familiar'/'TOR', 'Edward P. Beard, Jr.').
card_number('mystic familiar'/'TOR', '11').
card_flavor_text('mystic familiar'/'TOR', 'They soar, and the mystics follow.').
card_multiverse_id('mystic familiar'/'TOR', '30607').

card_in_set('nantuko blightcutter', 'TOR').
card_original_type('nantuko blightcutter'/'TOR', 'Creature — Insect Druid').
card_original_text('nantuko blightcutter'/'TOR', 'Protection from black\nThreshold Nantuko Blightcutter gets +1/+1 for each black permanent your opponents control. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('nantuko blightcutter', 'TOR').
card_image_name('nantuko blightcutter'/'TOR', 'nantuko blightcutter').
card_uid('nantuko blightcutter'/'TOR', 'TOR:Nantuko Blightcutter:nantuko blightcutter').
card_rarity('nantuko blightcutter'/'TOR', 'Rare').
card_artist('nantuko blightcutter'/'TOR', 'Matt Cavotta').
card_number('nantuko blightcutter'/'TOR', '131').
card_multiverse_id('nantuko blightcutter'/'TOR', '32243').

card_in_set('nantuko calmer', 'TOR').
card_original_type('nantuko calmer'/'TOR', 'Creature — Insect Druid').
card_original_text('nantuko calmer'/'TOR', '{G}, {T}, Sacrifice Nantuko Calmer: Destroy target enchantment.\nThreshold Nantuko Calmer gets +1/+1. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('nantuko calmer', 'TOR').
card_image_name('nantuko calmer'/'TOR', 'nantuko calmer').
card_uid('nantuko calmer'/'TOR', 'TOR:Nantuko Calmer:nantuko calmer').
card_rarity('nantuko calmer'/'TOR', 'Common').
card_artist('nantuko calmer'/'TOR', 'Mark Romanoski').
card_number('nantuko calmer'/'TOR', '132').
card_multiverse_id('nantuko calmer'/'TOR', '31851').

card_in_set('nantuko cultivator', 'TOR').
card_original_type('nantuko cultivator'/'TOR', 'Creature — Insect Druid').
card_original_text('nantuko cultivator'/'TOR', 'When Nantuko Cultivator comes into play, you may discard any number of land cards from your hand. Put that many +1/+1 counters on Nantuko Cultivator and draw that many cards.').
card_first_print('nantuko cultivator', 'TOR').
card_image_name('nantuko cultivator'/'TOR', 'nantuko cultivator').
card_uid('nantuko cultivator'/'TOR', 'TOR:Nantuko Cultivator:nantuko cultivator').
card_rarity('nantuko cultivator'/'TOR', 'Rare').
card_artist('nantuko cultivator'/'TOR', 'Darrell Riche').
card_number('nantuko cultivator'/'TOR', '133').
card_multiverse_id('nantuko cultivator'/'TOR', '34229').

card_in_set('nantuko shade', 'TOR').
card_original_type('nantuko shade'/'TOR', 'Creature — Insect Shade').
card_original_text('nantuko shade'/'TOR', '{B}: Nantuko Shade gets +1/+1 until end of turn.').
card_first_print('nantuko shade', 'TOR').
card_image_name('nantuko shade'/'TOR', 'nantuko shade').
card_uid('nantuko shade'/'TOR', 'TOR:Nantuko Shade:nantuko shade').
card_rarity('nantuko shade'/'TOR', 'Rare').
card_artist('nantuko shade'/'TOR', 'Brian Snõddy').
card_number('nantuko shade'/'TOR', '74').
card_flavor_text('nantuko shade'/'TOR', '\"If the Nantuko only knew what awaits them beyond death, they would abandon all they hold dear.\"\n—Cabal Patriarch').
card_multiverse_id('nantuko shade'/'TOR', '35053').

card_in_set('narcissism', 'TOR').
card_original_type('narcissism'/'TOR', 'Enchantment').
card_original_text('narcissism'/'TOR', '{G}, Discard a card from your hand: Target creature gets +2/+2 until end of turn.\n{G}, Sacrifice Narcissism: Target creature gets +2/+2 until end of turn.').
card_first_print('narcissism', 'TOR').
card_image_name('narcissism'/'TOR', 'narcissism').
card_uid('narcissism'/'TOR', 'TOR:Narcissism:narcissism').
card_rarity('narcissism'/'TOR', 'Uncommon').
card_artist('narcissism'/'TOR', 'Christopher Moeller').
card_number('narcissism'/'TOR', '134').
card_multiverse_id('narcissism'/'TOR', '5663').

card_in_set('nostalgic dreams', 'TOR').
card_original_type('nostalgic dreams'/'TOR', 'Sorcery').
card_original_text('nostalgic dreams'/'TOR', 'As an additional cost to play Nostalgic Dreams, discard X cards from your hand.\nReturn X target cards from your graveyard to your hand. Remove Nostalgic Dreams from the game.').
card_first_print('nostalgic dreams', 'TOR').
card_image_name('nostalgic dreams'/'TOR', 'nostalgic dreams').
card_uid('nostalgic dreams'/'TOR', 'TOR:Nostalgic Dreams:nostalgic dreams').
card_rarity('nostalgic dreams'/'TOR', 'Rare').
card_artist('nostalgic dreams'/'TOR', 'Darrell Riche').
card_number('nostalgic dreams'/'TOR', '135').
card_flavor_text('nostalgic dreams'/'TOR', 'Seton dreams of life renewed.').
card_multiverse_id('nostalgic dreams'/'TOR', '32208').

card_in_set('obsessive search', 'TOR').
card_original_type('obsessive search'/'TOR', 'Instant').
card_original_text('obsessive search'/'TOR', 'Draw a card.\nMadness {U} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('obsessive search', 'TOR').
card_image_name('obsessive search'/'TOR', 'obsessive search').
card_uid('obsessive search'/'TOR', 'TOR:Obsessive Search:obsessive search').
card_rarity('obsessive search'/'TOR', 'Common').
card_artist('obsessive search'/'TOR', 'Jim Nelson').
card_number('obsessive search'/'TOR', '43').
card_flavor_text('obsessive search'/'TOR', 'The question strained his sanity. The answer snapped it in half.').
card_multiverse_id('obsessive search'/'TOR', '29721').

card_in_set('organ grinder', 'TOR').
card_original_type('organ grinder'/'TOR', 'Creature — Zombie').
card_original_text('organ grinder'/'TOR', '{T}, Remove three cards in your graveyard from the game: Target player loses 3 life.').
card_first_print('organ grinder', 'TOR').
card_image_name('organ grinder'/'TOR', 'organ grinder').
card_uid('organ grinder'/'TOR', 'TOR:Organ Grinder:organ grinder').
card_rarity('organ grinder'/'TOR', 'Common').
card_artist('organ grinder'/'TOR', 'Adam Rex').
card_number('organ grinder'/'TOR', '75').
card_flavor_text('organ grinder'/'TOR', 'It knows what makes you tick. It knows how to make the ticking stop.').
card_multiverse_id('organ grinder'/'TOR', '32238').

card_in_set('overmaster', 'TOR').
card_original_type('overmaster'/'TOR', 'Sorcery').
card_original_text('overmaster'/'TOR', 'The next instant or sorcery spell you play this turn can\'t be countered by spells or abilities.\nDraw a card.').
card_first_print('overmaster', 'TOR').
card_image_name('overmaster'/'TOR', 'overmaster').
card_uid('overmaster'/'TOR', 'TOR:Overmaster:overmaster').
card_rarity('overmaster'/'TOR', 'Rare').
card_artist('overmaster'/'TOR', 'Anthony S. Waters').
card_number('overmaster'/'TOR', '104').
card_flavor_text('overmaster'/'TOR', '\"Trickery can\'t defeat raw power.\"\n—Matoc, lavamancer').
card_multiverse_id('overmaster'/'TOR', '29868').

card_in_set('parallel evolution', 'TOR').
card_original_type('parallel evolution'/'TOR', 'Sorcery').
card_original_text('parallel evolution'/'TOR', 'For each creature token in play, its controller puts a creature token into play that\'s a copy of that creature.\nFlashback {4}{G}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('parallel evolution', 'TOR').
card_image_name('parallel evolution'/'TOR', 'parallel evolution').
card_uid('parallel evolution'/'TOR', 'TOR:Parallel Evolution:parallel evolution').
card_rarity('parallel evolution'/'TOR', 'Rare').
card_artist('parallel evolution'/'TOR', 'Matt Cavotta').
card_number('parallel evolution'/'TOR', '136').
card_multiverse_id('parallel evolution'/'TOR', '29992').

card_in_set('pardic arsonist', 'TOR').
card_original_type('pardic arsonist'/'TOR', 'Creature — Barbarian').
card_original_text('pardic arsonist'/'TOR', 'Threshold When Pardic Arsonist comes into play, it deals 3 damage to target creature or player. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('pardic arsonist', 'TOR').
card_image_name('pardic arsonist'/'TOR', 'pardic arsonist').
card_uid('pardic arsonist'/'TOR', 'TOR:Pardic Arsonist:pardic arsonist').
card_rarity('pardic arsonist'/'TOR', 'Uncommon').
card_artist('pardic arsonist'/'TOR', 'rk post').
card_number('pardic arsonist'/'TOR', '105').
card_flavor_text('pardic arsonist'/'TOR', 'Otaria\'s epidemic of insanity didn\'t affect certain barbarians—but no one noticed.').
card_multiverse_id('pardic arsonist'/'TOR', '32213').

card_in_set('pardic collaborator', 'TOR').
card_original_type('pardic collaborator'/'TOR', 'Creature — Barbarian').
card_original_text('pardic collaborator'/'TOR', 'First strike\n{B}: Pardic Collaborator gets +1/+1 until end of turn.').
card_first_print('pardic collaborator', 'TOR').
card_image_name('pardic collaborator'/'TOR', 'pardic collaborator').
card_uid('pardic collaborator'/'TOR', 'TOR:Pardic Collaborator:pardic collaborator').
card_rarity('pardic collaborator'/'TOR', 'Uncommon').
card_artist('pardic collaborator'/'TOR', 'Pete Venters').
card_number('pardic collaborator'/'TOR', '106').
card_flavor_text('pardic collaborator'/'TOR', '\"We\'ve been taught that strength is everything. So how can an alliance with the strongest be a betrayal?\"').
card_multiverse_id('pardic collaborator'/'TOR', '35054').

card_in_set('pardic lancer', 'TOR').
card_original_type('pardic lancer'/'TOR', 'Creature — Barbarian').
card_original_text('pardic lancer'/'TOR', 'Discard a card at random from your hand: Pardic Lancer gets +1/+0 and gains first strike until end of turn.').
card_first_print('pardic lancer', 'TOR').
card_image_name('pardic lancer'/'TOR', 'pardic lancer').
card_uid('pardic lancer'/'TOR', 'TOR:Pardic Lancer:pardic lancer').
card_rarity('pardic lancer'/'TOR', 'Common').
card_artist('pardic lancer'/'TOR', 'Justin Sweet').
card_number('pardic lancer'/'TOR', '107').
card_flavor_text('pardic lancer'/'TOR', 'He gets to the point right away.').
card_multiverse_id('pardic lancer'/'TOR', '32240').

card_in_set('pay no heed', 'TOR').
card_original_type('pay no heed'/'TOR', 'Instant').
card_original_text('pay no heed'/'TOR', 'Prevent all damage a source of your choice would deal this turn.').
card_first_print('pay no heed', 'TOR').
card_image_name('pay no heed'/'TOR', 'pay no heed').
card_uid('pay no heed'/'TOR', 'TOR:Pay No Heed:pay no heed').
card_rarity('pay no heed'/'TOR', 'Common').
card_artist('pay no heed'/'TOR', 'Adam Rex').
card_number('pay no heed'/'TOR', '12').
card_flavor_text('pay no heed'/'TOR', '\"Inhale life. Exhale pain.\"\n—Mystic elder').
card_multiverse_id('pay no heed'/'TOR', '32232').

card_in_set('petradon', 'TOR').
card_original_type('petradon'/'TOR', 'Creature — Nightmare Beast').
card_original_text('petradon'/'TOR', 'When Petradon comes into play, remove two target lands from the game.\nWhen Petradon leaves play, return the removed cards to play under their owners\' control.\n{R}: Petradon gets +1/+0 until end of turn.').
card_first_print('petradon', 'TOR').
card_image_name('petradon'/'TOR', 'petradon').
card_uid('petradon'/'TOR', 'TOR:Petradon:petradon').
card_rarity('petradon'/'TOR', 'Rare').
card_artist('petradon'/'TOR', 'Jim Nelson').
card_number('petradon'/'TOR', '108').
card_multiverse_id('petradon'/'TOR', '29799').

card_in_set('petravark', 'TOR').
card_original_type('petravark'/'TOR', 'Creature — Nightmare Beast').
card_original_text('petravark'/'TOR', 'When Petravark comes into play, remove target land from the game.\nWhen Petravark leaves play, return the removed card to play under its owner\'s control.').
card_first_print('petravark', 'TOR').
card_image_name('petravark'/'TOR', 'petravark').
card_uid('petravark'/'TOR', 'TOR:Petravark:petravark').
card_rarity('petravark'/'TOR', 'Common').
card_artist('petravark'/'TOR', 'Wayne England').
card_number('petravark'/'TOR', '109').
card_multiverse_id('petravark'/'TOR', '31805').

card_in_set('pitchstone wall', 'TOR').
card_original_type('pitchstone wall'/'TOR', 'Creature — Wall').
card_original_text('pitchstone wall'/'TOR', '(Walls can\'t attack.)\nWhenever you discard a card from your hand, you may sacrifice Pitchstone Wall. If you do, return the discarded card from your graveyard to your hand.').
card_first_print('pitchstone wall', 'TOR').
card_image_name('pitchstone wall'/'TOR', 'pitchstone wall').
card_uid('pitchstone wall'/'TOR', 'TOR:Pitchstone Wall:pitchstone wall').
card_rarity('pitchstone wall'/'TOR', 'Uncommon').
card_artist('pitchstone wall'/'TOR', 'David Martin').
card_number('pitchstone wall'/'TOR', '110').
card_flavor_text('pitchstone wall'/'TOR', 'Its material may not be precious, but the minds it protects are.').
card_multiverse_id('pitchstone wall'/'TOR', '36432').

card_in_set('plagiarize', 'TOR').
card_original_type('plagiarize'/'TOR', 'Instant').
card_original_text('plagiarize'/'TOR', 'Until end of turn, if target player would draw a card, instead that player skips that draw and you draw a card.').
card_first_print('plagiarize', 'TOR').
card_image_name('plagiarize'/'TOR', 'plagiarize').
card_uid('plagiarize'/'TOR', 'TOR:Plagiarize:plagiarize').
card_rarity('plagiarize'/'TOR', 'Rare').
card_artist('plagiarize'/'TOR', 'Ben Thompson').
card_number('plagiarize'/'TOR', '44').
card_flavor_text('plagiarize'/'TOR', '\"Are you thinking what I\'m thinking?\"').
card_multiverse_id('plagiarize'/'TOR', '29824').

card_in_set('possessed aven', 'TOR').
card_original_type('possessed aven'/'TOR', 'Creature — Bird Soldier Horror').
card_original_text('possessed aven'/'TOR', 'Flying\nThreshold Possessed Aven gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target blue creature.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('possessed aven', 'TOR').
card_image_name('possessed aven'/'TOR', 'possessed aven').
card_uid('possessed aven'/'TOR', 'TOR:Possessed Aven:possessed aven').
card_rarity('possessed aven'/'TOR', 'Rare').
card_artist('possessed aven'/'TOR', 'Scott M. Fischer').
card_number('possessed aven'/'TOR', '45').
card_multiverse_id('possessed aven'/'TOR', '30658').

card_in_set('possessed barbarian', 'TOR').
card_original_type('possessed barbarian'/'TOR', 'Creature — Barbarian Horror').
card_original_text('possessed barbarian'/'TOR', 'First strike\nThreshold Possessed Barbarian gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target red creature.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('possessed barbarian', 'TOR').
card_image_name('possessed barbarian'/'TOR', 'possessed barbarian').
card_uid('possessed barbarian'/'TOR', 'TOR:Possessed Barbarian:possessed barbarian').
card_rarity('possessed barbarian'/'TOR', 'Rare').
card_artist('possessed barbarian'/'TOR', 'Scott M. Fischer').
card_number('possessed barbarian'/'TOR', '111').
card_multiverse_id('possessed barbarian'/'TOR', '32231').

card_in_set('possessed centaur', 'TOR').
card_original_type('possessed centaur'/'TOR', 'Creature — Centaur Horror').
card_original_text('possessed centaur'/'TOR', 'Trample\nThreshold Possessed Centaur gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target green creature.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('possessed centaur', 'TOR').
card_image_name('possessed centaur'/'TOR', 'possessed centaur').
card_uid('possessed centaur'/'TOR', 'TOR:Possessed Centaur:possessed centaur').
card_rarity('possessed centaur'/'TOR', 'Rare').
card_artist('possessed centaur'/'TOR', 'Alex Horley-Orlandelli').
card_number('possessed centaur'/'TOR', '137').
card_multiverse_id('possessed centaur'/'TOR', '29787').

card_in_set('possessed nomad', 'TOR').
card_original_type('possessed nomad'/'TOR', 'Creature — Nomad Horror').
card_original_text('possessed nomad'/'TOR', 'Attacking doesn\'t cause Possessed Nomad to tap.\nThreshold Possessed Nomad gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target white creature.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('possessed nomad', 'TOR').
card_image_name('possessed nomad'/'TOR', 'possessed nomad').
card_uid('possessed nomad'/'TOR', 'TOR:Possessed Nomad:possessed nomad').
card_rarity('possessed nomad'/'TOR', 'Rare').
card_artist('possessed nomad'/'TOR', 'Eric Peterson').
card_number('possessed nomad'/'TOR', '13').
card_multiverse_id('possessed nomad'/'TOR', '29816').

card_in_set('psychotic haze', 'TOR').
card_original_type('psychotic haze'/'TOR', 'Instant').
card_original_text('psychotic haze'/'TOR', 'Psychotic Haze deals 1 damage to each creature and each player.\nMadness {1}{B} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('psychotic haze', 'TOR').
card_image_name('psychotic haze'/'TOR', 'psychotic haze').
card_uid('psychotic haze'/'TOR', 'TOR:Psychotic Haze:psychotic haze').
card_rarity('psychotic haze'/'TOR', 'Common').
card_artist('psychotic haze'/'TOR', 'Alex Horley-Orlandelli').
card_number('psychotic haze'/'TOR', '76').
card_multiverse_id('psychotic haze'/'TOR', '33691').

card_in_set('putrid imp', 'TOR').
card_original_type('putrid imp'/'TOR', 'Creature — Zombie Imp').
card_original_text('putrid imp'/'TOR', 'Discard a card from your hand: Putrid Imp gains flying until end of turn.\nThreshold Putrid Imp gets +1/+1 and can\'t block. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('putrid imp', 'TOR').
card_image_name('putrid imp'/'TOR', 'putrid imp').
card_uid('putrid imp'/'TOR', 'TOR:Putrid Imp:putrid imp').
card_rarity('putrid imp'/'TOR', 'Common').
card_artist('putrid imp'/'TOR', 'Wayne England').
card_number('putrid imp'/'TOR', '77').
card_multiverse_id('putrid imp'/'TOR', '34470').

card_in_set('pyromania', 'TOR').
card_original_type('pyromania'/'TOR', 'Enchantment').
card_original_text('pyromania'/'TOR', '{1}{R}, Discard a card at random from your hand: Pyromania deals 1 damage to target creature or player.\n{1}{R}, Sacrifice Pyromania: Pyromania deals 1 damage to target creature or player.').
card_first_print('pyromania', 'TOR').
card_image_name('pyromania'/'TOR', 'pyromania').
card_uid('pyromania'/'TOR', 'TOR:Pyromania:pyromania').
card_rarity('pyromania'/'TOR', 'Uncommon').
card_artist('pyromania'/'TOR', 'Christopher Moeller').
card_number('pyromania'/'TOR', '112').
card_multiverse_id('pyromania'/'TOR', '34390').

card_in_set('radiate', 'TOR').
card_original_type('radiate'/'TOR', 'Instant').
card_original_text('radiate'/'TOR', 'Choose target instant or sorcery spell that targets only a single permanent or player. For each other permanent or player that spell could target, put a copy of the spell onto the stack. Each copy targets a different one of those permanents and players.').
card_first_print('radiate', 'TOR').
card_image_name('radiate'/'TOR', 'radiate').
card_uid('radiate'/'TOR', 'TOR:Radiate:radiate').
card_rarity('radiate'/'TOR', 'Rare').
card_artist('radiate'/'TOR', 'Carl Critchlow').
card_number('radiate'/'TOR', '113').
card_multiverse_id('radiate'/'TOR', '34250').

card_in_set('rancid earth', 'TOR').
card_original_type('rancid earth'/'TOR', 'Sorcery').
card_original_text('rancid earth'/'TOR', 'Destroy target land.\nThreshold Instead destroy that land and Rancid Earth deals 1 damage to each creature and each player. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('rancid earth', 'TOR').
card_image_name('rancid earth'/'TOR', 'rancid earth').
card_uid('rancid earth'/'TOR', 'TOR:Rancid Earth:rancid earth').
card_rarity('rancid earth'/'TOR', 'Common').
card_artist('rancid earth'/'TOR', 'Ciruelo').
card_number('rancid earth'/'TOR', '78').
card_multiverse_id('rancid earth'/'TOR', '31598').

card_in_set('reborn hero', 'TOR').
card_original_type('reborn hero'/'TOR', 'Creature — Soldier').
card_original_text('reborn hero'/'TOR', 'Attacking doesn\'t cause Reborn Hero to tap.\nThreshold When Reborn Hero is put into a graveyard from play, you may pay {W}{W}. If you do, return Reborn Hero to play under your control. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('reborn hero', 'TOR').
card_image_name('reborn hero'/'TOR', 'reborn hero').
card_uid('reborn hero'/'TOR', 'TOR:Reborn Hero:reborn hero').
card_rarity('reborn hero'/'TOR', 'Rare').
card_artist('reborn hero'/'TOR', 'Gary Ruddell').
card_number('reborn hero'/'TOR', '14').
card_multiverse_id('reborn hero'/'TOR', '34247').

card_in_set('restless dreams', 'TOR').
card_original_type('restless dreams'/'TOR', 'Sorcery').
card_original_text('restless dreams'/'TOR', 'As an additional cost to play Restless Dreams, discard X cards from your hand.\nReturn X target creature cards from your graveyard to your hand.').
card_first_print('restless dreams', 'TOR').
card_image_name('restless dreams'/'TOR', 'restless dreams').
card_uid('restless dreams'/'TOR', 'TOR:Restless Dreams:restless dreams').
card_rarity('restless dreams'/'TOR', 'Common').
card_artist('restless dreams'/'TOR', 'John Matson').
card_number('restless dreams'/'TOR', '79').
card_flavor_text('restless dreams'/'TOR', 'Braids dreams of dark rebirth.').
card_multiverse_id('restless dreams'/'TOR', '29908').

card_in_set('retraced image', 'TOR').
card_original_type('retraced image'/'TOR', 'Sorcery').
card_original_text('retraced image'/'TOR', 'Reveal a card in your hand, then put that card into play if it has the same name as a permanent in play.').
card_first_print('retraced image', 'TOR').
card_image_name('retraced image'/'TOR', 'retraced image').
card_uid('retraced image'/'TOR', 'TOR:Retraced Image:retraced image').
card_rarity('retraced image'/'TOR', 'Rare').
card_artist('retraced image'/'TOR', 'Greg Staples').
card_number('retraced image'/'TOR', '46').
card_flavor_text('retraced image'/'TOR', 'An army from a soldier, a flock from a bird, a school from a fish . . . a profit from an atrocity.').
card_multiverse_id('retraced image'/'TOR', '33698').

card_in_set('sengir vampire', 'TOR').
card_original_type('sengir vampire'/'TOR', 'Creature — Vampire').
card_original_text('sengir vampire'/'TOR', 'Flying\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'TOR', 'sengir vampire').
card_uid('sengir vampire'/'TOR', 'TOR:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'TOR', 'Rare').
card_artist('sengir vampire'/'TOR', 'Kev Walker').
card_number('sengir vampire'/'TOR', '80').
card_flavor_text('sengir vampire'/'TOR', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'TOR', '35086').

card_in_set('seton\'s scout', 'TOR').
card_original_type('seton\'s scout'/'TOR', 'Creature — Centaur Druid').
card_original_text('seton\'s scout'/'TOR', 'Seton\'s Scout may block as though it had flying.\nThreshold Seton\'s Scout gets +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('seton\'s scout', 'TOR').
card_image_name('seton\'s scout'/'TOR', 'seton\'s scout').
card_uid('seton\'s scout'/'TOR', 'TOR:Seton\'s Scout:seton\'s scout').
card_rarity('seton\'s scout'/'TOR', 'Uncommon').
card_artist('seton\'s scout'/'TOR', 'Mark Romanoski').
card_number('seton\'s scout'/'TOR', '138').
card_multiverse_id('seton\'s scout'/'TOR', '32244').

card_in_set('shade\'s form', 'TOR').
card_original_type('shade\'s form'/'TOR', 'Enchant Creature').
card_original_text('shade\'s form'/'TOR', 'Enchanted creature has \"{B}: This creature gets +1/+1 until end of turn.\"\nWhen enchanted creature is put into a graveyard, return that creature to play under your control.').
card_first_print('shade\'s form', 'TOR').
card_image_name('shade\'s form'/'TOR', 'shade\'s form').
card_uid('shade\'s form'/'TOR', 'TOR:Shade\'s Form:shade\'s form').
card_rarity('shade\'s form'/'TOR', 'Common').
card_artist('shade\'s form'/'TOR', 'Clyde Caldwell').
card_number('shade\'s form'/'TOR', '81').
card_multiverse_id('shade\'s form'/'TOR', '23043').

card_in_set('shambling swarm', 'TOR').
card_original_type('shambling swarm'/'TOR', 'Creature — Horror').
card_original_text('shambling swarm'/'TOR', 'When Shambling Swarm is put into a graveyard from play, distribute three -1/-1 counters among one, two, or three target creatures. Remove those counters at end of turn.').
card_first_print('shambling swarm', 'TOR').
card_image_name('shambling swarm'/'TOR', 'shambling swarm').
card_uid('shambling swarm'/'TOR', 'TOR:Shambling Swarm:shambling swarm').
card_rarity('shambling swarm'/'TOR', 'Rare').
card_artist('shambling swarm'/'TOR', 'Arnie Swekel').
card_number('shambling swarm'/'TOR', '82').
card_flavor_text('shambling swarm'/'TOR', 'Chainer\'s madness personified, it exists only to slaughter the innocent.').
card_multiverse_id('shambling swarm'/'TOR', '31822').

card_in_set('sickening dreams', 'TOR').
card_original_type('sickening dreams'/'TOR', 'Sorcery').
card_original_text('sickening dreams'/'TOR', 'As an additional cost to play Sickening Dreams, discard X cards from your hand.\nSickening Dreams deals X damage to each creature and each player.').
card_first_print('sickening dreams', 'TOR').
card_image_name('sickening dreams'/'TOR', 'sickening dreams').
card_uid('sickening dreams'/'TOR', 'TOR:Sickening Dreams:sickening dreams').
card_rarity('sickening dreams'/'TOR', 'Uncommon').
card_artist('sickening dreams'/'TOR', 'Scott M. Fischer').
card_number('sickening dreams'/'TOR', '83').
card_flavor_text('sickening dreams'/'TOR', 'The Patriarch dreams of vile plague.').
card_multiverse_id('sickening dreams'/'TOR', '35063').

card_in_set('skullscorch', 'TOR').
card_original_type('skullscorch'/'TOR', 'Sorcery').
card_original_text('skullscorch'/'TOR', 'Target player discards two cards at random from his or her hand unless that player has Skullscorch deal 4 damage to him or her.').
card_first_print('skullscorch', 'TOR').
card_image_name('skullscorch'/'TOR', 'skullscorch').
card_uid('skullscorch'/'TOR', 'TOR:Skullscorch:skullscorch').
card_rarity('skullscorch'/'TOR', 'Rare').
card_artist('skullscorch'/'TOR', 'Bradley Williams').
card_number('skullscorch'/'TOR', '114').
card_flavor_text('skullscorch'/'TOR', '\"A good whack on the head usually has the same effect.\"\n—Kamahl, pit fighter').
card_multiverse_id('skullscorch'/'TOR', '34382').

card_in_set('skywing aven', 'TOR').
card_original_type('skywing aven'/'TOR', 'Creature — Bird Soldier').
card_original_text('skywing aven'/'TOR', 'Flying\nDiscard a card from your hand: Return Skywing Aven to its owner\'s hand.').
card_first_print('skywing aven', 'TOR').
card_image_name('skywing aven'/'TOR', 'skywing aven').
card_uid('skywing aven'/'TOR', 'TOR:Skywing Aven:skywing aven').
card_rarity('skywing aven'/'TOR', 'Common').
card_artist('skywing aven'/'TOR', 'Matt Cavotta').
card_number('skywing aven'/'TOR', '47').
card_flavor_text('skywing aven'/'TOR', '\"I am as the wind that bears me: harsh yet gentle, fleeting yet ever-present. Together we fly beyond imagination.\"').
card_multiverse_id('skywing aven'/'TOR', '29713').

card_in_set('slithery stalker', 'TOR').
card_original_type('slithery stalker'/'TOR', 'Creature — Nightmare Horror').
card_original_text('slithery stalker'/'TOR', 'Swampwalk\nWhen Slithery Stalker comes into play, remove target green or white creature an opponent controls from the game.\nWhen Slithery Stalker leaves play, return the removed card to play under its owner\'s control.').
card_first_print('slithery stalker', 'TOR').
card_image_name('slithery stalker'/'TOR', 'slithery stalker').
card_uid('slithery stalker'/'TOR', 'TOR:Slithery Stalker:slithery stalker').
card_rarity('slithery stalker'/'TOR', 'Uncommon').
card_artist('slithery stalker'/'TOR', 'John Avon').
card_number('slithery stalker'/'TOR', '84').
card_multiverse_id('slithery stalker'/'TOR', '31833').

card_in_set('sonic seizure', 'TOR').
card_original_type('sonic seizure'/'TOR', 'Instant').
card_original_text('sonic seizure'/'TOR', 'As an additional cost to play Sonic Seizure, discard a card at random from your hand.\nSonic Seizure deals 3 damage to target creature or player.').
card_first_print('sonic seizure', 'TOR').
card_image_name('sonic seizure'/'TOR', 'sonic seizure').
card_uid('sonic seizure'/'TOR', 'TOR:Sonic Seizure:sonic seizure').
card_rarity('sonic seizure'/'TOR', 'Common').
card_artist('sonic seizure'/'TOR', 'Terese Nielsen').
card_number('sonic seizure'/'TOR', '115').
card_multiverse_id('sonic seizure'/'TOR', '35165').

card_in_set('soul scourge', 'TOR').
card_original_type('soul scourge'/'TOR', 'Creature — Nightmare Horror').
card_original_text('soul scourge'/'TOR', 'Flying\nWhen Soul Scourge comes into play, target player loses 3 life.\nWhen Soul Scourge leaves play, that player gains 3 life.').
card_first_print('soul scourge', 'TOR').
card_image_name('soul scourge'/'TOR', 'soul scourge').
card_uid('soul scourge'/'TOR', 'TOR:Soul Scourge:soul scourge').
card_rarity('soul scourge'/'TOR', 'Common').
card_artist('soul scourge'/'TOR', 'Carl Critchlow').
card_number('soul scourge'/'TOR', '85').
card_multiverse_id('soul scourge'/'TOR', '29731').

card_in_set('spirit flare', 'TOR').
card_original_type('spirit flare'/'TOR', 'Instant').
card_original_text('spirit flare'/'TOR', 'Tap target untapped creature you control. If you do, it deals damage equal to its power to target attacking or blocking creature an opponent controls.\nFlashback—{1}{W}, Pay 3 life. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('spirit flare', 'TOR').
card_image_name('spirit flare'/'TOR', 'spirit flare').
card_uid('spirit flare'/'TOR', 'TOR:Spirit Flare:spirit flare').
card_rarity('spirit flare'/'TOR', 'Common').
card_artist('spirit flare'/'TOR', 'rk post').
card_number('spirit flare'/'TOR', '15').
card_multiverse_id('spirit flare'/'TOR', '26361').

card_in_set('stern judge', 'TOR').
card_original_type('stern judge'/'TOR', 'Creature — Cleric').
card_original_text('stern judge'/'TOR', '{T}: Each player loses 1 life for each swamp he or she controls.').
card_first_print('stern judge', 'TOR').
card_image_name('stern judge'/'TOR', 'stern judge').
card_uid('stern judge'/'TOR', 'TOR:Stern Judge:stern judge').
card_rarity('stern judge'/'TOR', 'Uncommon').
card_artist('stern judge'/'TOR', 'Matt Cavotta').
card_number('stern judge'/'TOR', '16').
card_flavor_text('stern judge'/'TOR', '\"How to punish the guilty is up to the Ancestor. Deciding who merits such punishment is up to me.\"').
card_multiverse_id('stern judge'/'TOR', '35082').

card_in_set('strength of isolation', 'TOR').
card_original_type('strength of isolation'/'TOR', 'Enchant Creature').
card_original_text('strength of isolation'/'TOR', 'Enchanted creature gets +1/+2 and has protection from black.\nMadness {W} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('strength of isolation', 'TOR').
card_image_name('strength of isolation'/'TOR', 'strength of isolation').
card_uid('strength of isolation'/'TOR', 'TOR:Strength of Isolation:strength of isolation').
card_rarity('strength of isolation'/'TOR', 'Uncommon').
card_artist('strength of isolation'/'TOR', 'Jerry Tiritilli').
card_number('strength of isolation'/'TOR', '17').
card_flavor_text('strength of isolation'/'TOR', 'Don\'t mistake isolation for safety.').
card_multiverse_id('strength of isolation'/'TOR', '34782').

card_in_set('strength of lunacy', 'TOR').
card_original_type('strength of lunacy'/'TOR', 'Enchant Creature').
card_original_text('strength of lunacy'/'TOR', 'Enchanted creature gets +2/+1 and has protection from white.\nMadness {B} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('strength of lunacy', 'TOR').
card_image_name('strength of lunacy'/'TOR', 'strength of lunacy').
card_uid('strength of lunacy'/'TOR', 'TOR:Strength of Lunacy:strength of lunacy').
card_rarity('strength of lunacy'/'TOR', 'Uncommon').
card_artist('strength of lunacy'/'TOR', 'Greg & Tim Hildebrandt').
card_number('strength of lunacy'/'TOR', '86').
card_flavor_text('strength of lunacy'/'TOR', 'Don\'t confuse lunacy with courage.').
card_multiverse_id('strength of lunacy'/'TOR', '34222').

card_in_set('stupefying touch', 'TOR').
card_original_type('stupefying touch'/'TOR', 'Enchant Creature').
card_original_text('stupefying touch'/'TOR', 'When Stupefying Touch comes into play, draw a card.\nEnchanted creature\'s activated abilities can\'t be played.').
card_first_print('stupefying touch', 'TOR').
card_image_name('stupefying touch'/'TOR', 'stupefying touch').
card_uid('stupefying touch'/'TOR', 'TOR:Stupefying Touch:stupefying touch').
card_rarity('stupefying touch'/'TOR', 'Uncommon').
card_artist('stupefying touch'/'TOR', 'Bradley Williams').
card_number('stupefying touch'/'TOR', '48').
card_flavor_text('stupefying touch'/'TOR', 'Just because your eyes are open doesn\'t mean you\'re awake.').
card_multiverse_id('stupefying touch'/'TOR', '36415').

card_in_set('tainted field', 'TOR').
card_original_type('tainted field'/'TOR', 'Land').
card_original_text('tainted field'/'TOR', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Play this ability only if you control a swamp.').
card_first_print('tainted field', 'TOR').
card_image_name('tainted field'/'TOR', 'tainted field').
card_uid('tainted field'/'TOR', 'TOR:Tainted Field:tainted field').
card_rarity('tainted field'/'TOR', 'Uncommon').
card_artist('tainted field'/'TOR', 'Don Hazeltine').
card_number('tainted field'/'TOR', '140').
card_multiverse_id('tainted field'/'TOR', '31757').

card_in_set('tainted isle', 'TOR').
card_original_type('tainted isle'/'TOR', 'Land').
card_original_text('tainted isle'/'TOR', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Play this ability only if you control a swamp.').
card_first_print('tainted isle', 'TOR').
card_image_name('tainted isle'/'TOR', 'tainted isle').
card_uid('tainted isle'/'TOR', 'TOR:Tainted Isle:tainted isle').
card_rarity('tainted isle'/'TOR', 'Uncommon').
card_artist('tainted isle'/'TOR', 'Alan Pollack').
card_number('tainted isle'/'TOR', '141').
card_multiverse_id('tainted isle'/'TOR', '31758').

card_in_set('tainted peak', 'TOR').
card_original_type('tainted peak'/'TOR', 'Land').
card_original_text('tainted peak'/'TOR', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Play this ability only if you control a swamp.').
card_first_print('tainted peak', 'TOR').
card_image_name('tainted peak'/'TOR', 'tainted peak').
card_uid('tainted peak'/'TOR', 'TOR:Tainted Peak:tainted peak').
card_rarity('tainted peak'/'TOR', 'Uncommon').
card_artist('tainted peak'/'TOR', 'Tony Szczudlo').
card_number('tainted peak'/'TOR', '142').
card_multiverse_id('tainted peak'/'TOR', '31759').

card_in_set('tainted wood', 'TOR').
card_original_type('tainted wood'/'TOR', 'Land').
card_original_text('tainted wood'/'TOR', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Play this ability only if you control a swamp.').
card_first_print('tainted wood', 'TOR').
card_image_name('tainted wood'/'TOR', 'tainted wood').
card_uid('tainted wood'/'TOR', 'TOR:Tainted Wood:tainted wood').
card_rarity('tainted wood'/'TOR', 'Uncommon').
card_artist('tainted wood'/'TOR', 'Rob Alexander').
card_number('tainted wood'/'TOR', '143').
card_multiverse_id('tainted wood'/'TOR', '31760').

card_in_set('temporary insanity', 'TOR').
card_original_type('temporary insanity'/'TOR', 'Instant').
card_original_text('temporary insanity'/'TOR', 'Untap target creature with power less than the number of cards in your graveyard and gain control of it until end of turn. That creature gains haste until end of turn.').
card_first_print('temporary insanity', 'TOR').
card_image_name('temporary insanity'/'TOR', 'temporary insanity').
card_uid('temporary insanity'/'TOR', 'TOR:Temporary Insanity:temporary insanity').
card_rarity('temporary insanity'/'TOR', 'Uncommon').
card_artist('temporary insanity'/'TOR', 'Mark Romanoski').
card_number('temporary insanity'/'TOR', '116').
card_multiverse_id('temporary insanity'/'TOR', '32236').

card_in_set('teroh\'s faithful', 'TOR').
card_original_type('teroh\'s faithful'/'TOR', 'Creature — Cleric').
card_original_text('teroh\'s faithful'/'TOR', 'When Teroh\'s Faithful comes into play, you gain 4 life.').
card_first_print('teroh\'s faithful', 'TOR').
card_image_name('teroh\'s faithful'/'TOR', 'teroh\'s faithful').
card_uid('teroh\'s faithful'/'TOR', 'TOR:Teroh\'s Faithful:teroh\'s faithful').
card_rarity('teroh\'s faithful'/'TOR', 'Common').
card_artist('teroh\'s faithful'/'TOR', 'Greg & Tim Hildebrandt').
card_number('teroh\'s faithful'/'TOR', '18').
card_flavor_text('teroh\'s faithful'/'TOR', 'The light of reason follows them even into battle.').
card_multiverse_id('teroh\'s faithful'/'TOR', '12345').

card_in_set('teroh\'s vanguard', 'TOR').
card_original_type('teroh\'s vanguard'/'TOR', 'Creature — Nomad').
card_original_text('teroh\'s vanguard'/'TOR', 'You may play Teroh\'s Vanguard any time you could play an instant.\nThreshold When Teroh\'s Vanguard comes into play, creatures you control gain protection from black until end of turn. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('teroh\'s vanguard', 'TOR').
card_image_name('teroh\'s vanguard'/'TOR', 'teroh\'s vanguard').
card_uid('teroh\'s vanguard'/'TOR', 'TOR:Teroh\'s Vanguard:teroh\'s vanguard').
card_rarity('teroh\'s vanguard'/'TOR', 'Uncommon').
card_artist('teroh\'s vanguard'/'TOR', 'Greg & Tim Hildebrandt').
card_number('teroh\'s vanguard'/'TOR', '19').
card_multiverse_id('teroh\'s vanguard'/'TOR', '32205').

card_in_set('transcendence', 'TOR').
card_original_type('transcendence'/'TOR', 'Enchantment').
card_original_text('transcendence'/'TOR', 'You don\'t lose the game for having 0 or less life.\nWhen you have 20 or more life, you lose the game.\nWhenever you lose life, you gain 2 life for each 1 life you lost. (Damage dealt to you causes you to lose life.)').
card_first_print('transcendence', 'TOR').
card_image_name('transcendence'/'TOR', 'transcendence').
card_uid('transcendence'/'TOR', 'TOR:Transcendence:transcendence').
card_rarity('transcendence'/'TOR', 'Rare').
card_artist('transcendence'/'TOR', 'Rebecca Guay').
card_number('transcendence'/'TOR', '20').
card_multiverse_id('transcendence'/'TOR', '8875').

card_in_set('turbulent dreams', 'TOR').
card_original_type('turbulent dreams'/'TOR', 'Sorcery').
card_original_text('turbulent dreams'/'TOR', 'As an additional cost to play Turbulent Dreams, discard X cards from your hand.\nReturn X target nonland permanents to their owners\' hands.').
card_first_print('turbulent dreams', 'TOR').
card_image_name('turbulent dreams'/'TOR', 'turbulent dreams').
card_uid('turbulent dreams'/'TOR', 'TOR:Turbulent Dreams:turbulent dreams').
card_rarity('turbulent dreams'/'TOR', 'Rare').
card_artist('turbulent dreams'/'TOR', 'Wayne England').
card_number('turbulent dreams'/'TOR', '49').
card_flavor_text('turbulent dreams'/'TOR', 'Laquatus dreams of seizing control.').
card_multiverse_id('turbulent dreams'/'TOR', '29827').

card_in_set('unhinge', 'TOR').
card_original_type('unhinge'/'TOR', 'Sorcery').
card_original_text('unhinge'/'TOR', 'Target player discards a card from his or her hand.\nDraw a card.').
card_first_print('unhinge', 'TOR').
card_image_name('unhinge'/'TOR', 'unhinge').
card_uid('unhinge'/'TOR', 'TOR:Unhinge:unhinge').
card_rarity('unhinge'/'TOR', 'Common').
card_artist('unhinge'/'TOR', 'Keith Garletts').
card_number('unhinge'/'TOR', '87').
card_flavor_text('unhinge'/'TOR', '\"Don\'t let your mind wander—it might not come back.\"\n—Braids, dementia summoner').
card_multiverse_id('unhinge'/'TOR', '35922').

card_in_set('vengeful dreams', 'TOR').
card_original_type('vengeful dreams'/'TOR', 'Instant').
card_original_text('vengeful dreams'/'TOR', 'As an additional cost to play Vengeful Dreams, discard X cards from your hand.\nRemove X target attacking creatures from the game.').
card_first_print('vengeful dreams', 'TOR').
card_image_name('vengeful dreams'/'TOR', 'vengeful dreams').
card_uid('vengeful dreams'/'TOR', 'TOR:Vengeful Dreams:vengeful dreams').
card_rarity('vengeful dreams'/'TOR', 'Rare').
card_artist('vengeful dreams'/'TOR', 'Mark Tedin').
card_number('vengeful dreams'/'TOR', '21').
card_flavor_text('vengeful dreams'/'TOR', 'Teroh dreams of his enemies\' doom.').
card_multiverse_id('vengeful dreams'/'TOR', '29698').

card_in_set('violent eruption', 'TOR').
card_original_type('violent eruption'/'TOR', 'Instant').
card_original_text('violent eruption'/'TOR', 'Violent Eruption deals 4 damage divided as you choose among any number of target creatures and/or players.\nMadness {1}{R}{R} (You may play this card for its madness cost at the time you discard it from your hand.)').
card_first_print('violent eruption', 'TOR').
card_image_name('violent eruption'/'TOR', 'violent eruption').
card_uid('violent eruption'/'TOR', 'TOR:Violent Eruption:violent eruption').
card_rarity('violent eruption'/'TOR', 'Uncommon').
card_artist('violent eruption'/'TOR', 'Bob Petillo').
card_number('violent eruption'/'TOR', '117').
card_multiverse_id('violent eruption'/'TOR', '34253').

card_in_set('waste away', 'TOR').
card_original_type('waste away'/'TOR', 'Instant').
card_original_text('waste away'/'TOR', 'As an additional cost to play Waste Away, discard a card from your hand.\nTarget creature gets -5/-5 until end of turn.').
card_first_print('waste away', 'TOR').
card_image_name('waste away'/'TOR', 'waste away').
card_uid('waste away'/'TOR', 'TOR:Waste Away:waste away').
card_rarity('waste away'/'TOR', 'Common').
card_artist('waste away'/'TOR', 'Alan Pollack').
card_number('waste away'/'TOR', '88').
card_flavor_text('waste away'/'TOR', 'Chainer\'s insanity touched nearly every living thing—including viruses.').
card_multiverse_id('waste away'/'TOR', '32133').

card_in_set('zombie trailblazer', 'TOR').
card_original_type('zombie trailblazer'/'TOR', 'Creature — Zombie').
card_original_text('zombie trailblazer'/'TOR', 'Tap an untapped Zombie you control: Target land becomes a swamp until end of turn.\nTap an untapped Zombie you control: Target creature gains swampwalk until end of turn.').
card_first_print('zombie trailblazer', 'TOR').
card_image_name('zombie trailblazer'/'TOR', 'zombie trailblazer').
card_uid('zombie trailblazer'/'TOR', 'TOR:Zombie Trailblazer:zombie trailblazer').
card_rarity('zombie trailblazer'/'TOR', 'Uncommon').
card_artist('zombie trailblazer'/'TOR', 'Brian Snõddy').
card_number('zombie trailblazer'/'TOR', '89').
card_flavor_text('zombie trailblazer'/'TOR', 'Some zombies are natural-reborn leaders.').
card_multiverse_id('zombie trailblazer'/'TOR', '34819').
