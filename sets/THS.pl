% Theros

set('THS').
set_name('THS', 'Theros').
set_release_date('THS', '2013-09-27').
set_border('THS', 'black').
set_type('THS', 'expansion').
set_block('THS', 'Theros').

card_in_set('abhorrent overlord', 'THS').
card_original_type('abhorrent overlord'/'THS', 'Creature — Demon').
card_original_text('abhorrent overlord'/'THS', 'Flying\nWhen Abhorrent Overlord enters the battlefield, put a number of 1/1 black Harpy creature tokens with flying onto the battlefield equal to your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)\nAt the beginning of your upkeep, sacrifice a creature.').
card_image_name('abhorrent overlord'/'THS', 'abhorrent overlord').
card_uid('abhorrent overlord'/'THS', 'THS:Abhorrent Overlord:abhorrent overlord').
card_rarity('abhorrent overlord'/'THS', 'Rare').
card_artist('abhorrent overlord'/'THS', 'Slawomir Maniak').
card_number('abhorrent overlord'/'THS', '75').
card_multiverse_id('abhorrent overlord'/'THS', '373661').

card_in_set('agent of horizons', 'THS').
card_original_type('agent of horizons'/'THS', 'Creature — Human Rogue').
card_original_text('agent of horizons'/'THS', '{2}{U}: Agent of Horizons can\'t be blocked this turn.').
card_first_print('agent of horizons', 'THS').
card_image_name('agent of horizons'/'THS', 'agent of horizons').
card_uid('agent of horizons'/'THS', 'THS:Agent of Horizons:agent of horizons').
card_rarity('agent of horizons'/'THS', 'Common').
card_artist('agent of horizons'/'THS', 'Clint Cearley').
card_number('agent of horizons'/'THS', '148').
card_flavor_text('agent of horizons'/'THS', 'The light in the woods just before dawn reveals a glimmering network of branches, roots, and spiderwebs. The acolytes of Kruphix walk this lattice unseen.').
card_multiverse_id('agent of horizons'/'THS', '373712').

card_in_set('agent of the fates', 'THS').
card_original_type('agent of the fates'/'THS', 'Creature — Human Assassin').
card_original_text('agent of the fates'/'THS', 'Deathtouch\nHeroic — Whenever you cast a spell that targets Agent of the Fates, each opponent sacrifices a creature.').
card_first_print('agent of the fates', 'THS').
card_image_name('agent of the fates'/'THS', 'agent of the fates').
card_uid('agent of the fates'/'THS', 'THS:Agent of the Fates:agent of the fates').
card_rarity('agent of the fates'/'THS', 'Rare').
card_artist('agent of the fates'/'THS', 'Matt Stewart').
card_number('agent of the fates'/'THS', '76').
card_flavor_text('agent of the fates'/'THS', '\"You are breathing borrowed air.\"').
card_multiverse_id('agent of the fates'/'THS', '373543').

card_in_set('akroan crusader', 'THS').
card_original_type('akroan crusader'/'THS', 'Creature — Human Soldier').
card_original_text('akroan crusader'/'THS', 'Heroic — Whenever you cast a spell that targets Akroan Crusader, put a 1/1 red Soldier creature token with haste onto the battlefield.').
card_first_print('akroan crusader', 'THS').
card_image_name('akroan crusader'/'THS', 'akroan crusader').
card_uid('akroan crusader'/'THS', 'THS:Akroan Crusader:akroan crusader').
card_rarity('akroan crusader'/'THS', 'Common').
card_artist('akroan crusader'/'THS', 'Johann Bodin').
card_number('akroan crusader'/'THS', '111').
card_flavor_text('akroan crusader'/'THS', 'An Akroan soldier\'s worth is measured by the number of swords raised by his battle cry.').
card_multiverse_id('akroan crusader'/'THS', '373578').

card_in_set('akroan hoplite', 'THS').
card_original_type('akroan hoplite'/'THS', 'Creature — Human Soldier').
card_original_text('akroan hoplite'/'THS', 'Whenever Akroan Hoplite attacks, it gets +X/+0 until end of turn, where X is the number of attacking creatures you control.').
card_first_print('akroan hoplite', 'THS').
card_image_name('akroan hoplite'/'THS', 'akroan hoplite').
card_uid('akroan hoplite'/'THS', 'THS:Akroan Hoplite:akroan hoplite').
card_rarity('akroan hoplite'/'THS', 'Uncommon').
card_artist('akroan hoplite'/'THS', 'Igor Kieryluk').
card_number('akroan hoplite'/'THS', '185').
card_flavor_text('akroan hoplite'/'THS', '\"Fair fight? How could it be a fair fight? We\'re Akroans. They\'re not.\"').
card_multiverse_id('akroan hoplite'/'THS', '373590').

card_in_set('akroan horse', 'THS').
card_original_type('akroan horse'/'THS', 'Artifact Creature — Horse').
card_original_text('akroan horse'/'THS', 'Defender\nWhen Akroan Horse enters the battlefield, an opponent gains control of it.\nAt the beginning of your upkeep, each opponent puts a 1/1 white Soldier creature token onto the battlefield.').
card_first_print('akroan horse', 'THS').
card_image_name('akroan horse'/'THS', 'akroan horse').
card_uid('akroan horse'/'THS', 'THS:Akroan Horse:akroan horse').
card_rarity('akroan horse'/'THS', 'Rare').
card_artist('akroan horse'/'THS', 'Seb McKinnon').
card_number('akroan horse'/'THS', '210').
card_multiverse_id('akroan horse'/'THS', '373550').

card_in_set('anax and cymede', 'THS').
card_original_type('anax and cymede'/'THS', 'Legendary Creature — Human Soldier').
card_original_text('anax and cymede'/'THS', 'First strike, vigilance\nHeroic — Whenever you cast a spell that targets Anax and Cymede, creatures you control get +1/+1 and gain trample until end of turn.').
card_image_name('anax and cymede'/'THS', 'anax and cymede').
card_uid('anax and cymede'/'THS', 'THS:Anax and Cymede:anax and cymede').
card_rarity('anax and cymede'/'THS', 'Rare').
card_artist('anax and cymede'/'THS', 'Willian Murai').
card_number('anax and cymede'/'THS', '186').
card_flavor_text('anax and cymede'/'THS', 'Akros\'s greatest heroes are also its royalty.').
card_multiverse_id('anax and cymede'/'THS', '373530').

card_in_set('anger of the gods', 'THS').
card_original_type('anger of the gods'/'THS', 'Sorcery').
card_original_text('anger of the gods'/'THS', 'Anger of the Gods deals 3 damage to each creature. If a creature dealt damage this way would die this turn, exile it instead.').
card_first_print('anger of the gods', 'THS').
card_image_name('anger of the gods'/'THS', 'anger of the gods').
card_uid('anger of the gods'/'THS', 'THS:Anger of the Gods:anger of the gods').
card_rarity('anger of the gods'/'THS', 'Rare').
card_artist('anger of the gods'/'THS', 'Noah Bradley').
card_number('anger of the gods'/'THS', '112').
card_flavor_text('anger of the gods'/'THS', 'There was no reason to pray. This was already an act of the gods.').
card_multiverse_id('anger of the gods'/'THS', '373604').

card_in_set('annul', 'THS').
card_original_type('annul'/'THS', 'Instant').
card_original_text('annul'/'THS', 'Counter target artifact or enchantment spell.').
card_image_name('annul'/'THS', 'annul').
card_uid('annul'/'THS', 'THS:Annul:annul').
card_rarity('annul'/'THS', 'Common').
card_artist('annul'/'THS', 'Christine Choi').
card_number('annul'/'THS', '38').
card_flavor_text('annul'/'THS', '\"Why pray to the gods, who feed on your worship? Dissolve your illusions and see the true nature of things.\"\n—Xenagos, the Reveler').
card_multiverse_id('annul'/'THS', '373584').

card_in_set('anthousa, setessan hero', 'THS').
card_original_type('anthousa, setessan hero'/'THS', 'Legendary Creature — Human Warrior').
card_original_text('anthousa, setessan hero'/'THS', 'Heroic — Whenever you cast a spell that targets Anthousa, Setessan Hero, up to three target lands you control each become 2/2 Warrior creatures until end of turn. They\'re still lands.').
card_image_name('anthousa, setessan hero'/'THS', 'anthousa, setessan hero').
card_uid('anthousa, setessan hero'/'THS', 'THS:Anthousa, Setessan Hero:anthousa, setessan hero').
card_rarity('anthousa, setessan hero'/'THS', 'Rare').
card_artist('anthousa, setessan hero'/'THS', 'Howard Lyon').
card_number('anthousa, setessan hero'/'THS', '149').
card_flavor_text('anthousa, setessan hero'/'THS', 'The warriors she leads can rarely keep pace with her, and neither can the tales.').
card_multiverse_id('anthousa, setessan hero'/'THS', '373671').

card_in_set('anvilwrought raptor', 'THS').
card_original_type('anvilwrought raptor'/'THS', 'Artifact Creature — Bird').
card_original_text('anvilwrought raptor'/'THS', 'Flying, first strike').
card_first_print('anvilwrought raptor', 'THS').
card_image_name('anvilwrought raptor'/'THS', 'anvilwrought raptor').
card_uid('anvilwrought raptor'/'THS', 'THS:Anvilwrought Raptor:anvilwrought raptor').
card_rarity('anvilwrought raptor'/'THS', 'Uncommon').
card_artist('anvilwrought raptor'/'THS', 'James Zapata').
card_number('anvilwrought raptor'/'THS', '211').
card_flavor_text('anvilwrought raptor'/'THS', '\"I know its lightness, for I have seen it fly. I know its weight, for I have seen it strike.\"\n—Brigone, soldier of Meletis').
card_multiverse_id('anvilwrought raptor'/'THS', '373677').

card_in_set('aqueous form', 'THS').
card_original_type('aqueous form'/'THS', 'Enchantment — Aura').
card_original_text('aqueous form'/'THS', 'Enchant creature\nEnchanted creature can\'t be blocked.\nWhenever enchanted creature attacks, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('aqueous form', 'THS').
card_image_name('aqueous form'/'THS', 'aqueous form').
card_uid('aqueous form'/'THS', 'THS:Aqueous Form:aqueous form').
card_rarity('aqueous form'/'THS', 'Common').
card_artist('aqueous form'/'THS', 'Slawomir Maniak').
card_number('aqueous form'/'THS', '39').
card_multiverse_id('aqueous form'/'THS', '373715').

card_in_set('arbor colossus', 'THS').
card_original_type('arbor colossus'/'THS', 'Creature — Giant').
card_original_text('arbor colossus'/'THS', 'Reach\n{3}{G}{G}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Arbor Colossus becomes monstrous, destroy target creature with flying an opponent controls.').
card_first_print('arbor colossus', 'THS').
card_image_name('arbor colossus'/'THS', 'arbor colossus').
card_uid('arbor colossus'/'THS', 'THS:Arbor Colossus:arbor colossus').
card_rarity('arbor colossus'/'THS', 'Rare').
card_artist('arbor colossus'/'THS', 'Jaime Jones').
card_number('arbor colossus'/'THS', '150').
card_multiverse_id('arbor colossus'/'THS', '373740').

card_in_set('arena athlete', 'THS').
card_original_type('arena athlete'/'THS', 'Creature — Human').
card_original_text('arena athlete'/'THS', 'Heroic — Whenever you cast a spell that targets Arena Athlete, target creature an opponent controls can\'t block this turn.').
card_first_print('arena athlete', 'THS').
card_image_name('arena athlete'/'THS', 'arena athlete').
card_uid('arena athlete'/'THS', 'THS:Arena Athlete:arena athlete').
card_rarity('arena athlete'/'THS', 'Uncommon').
card_artist('arena athlete'/'THS', 'Jason Chan').
card_number('arena athlete'/'THS', '113').
card_flavor_text('arena athlete'/'THS', 'The Iroan Games award no medals. Athletes vie for a visit from Iroas, god of victory.').
card_multiverse_id('arena athlete'/'THS', '373585').

card_in_set('artisan of forms', 'THS').
card_original_type('artisan of forms'/'THS', 'Creature — Human Wizard').
card_original_text('artisan of forms'/'THS', 'Heroic — Whenever you cast a spell that targets Artisan of Forms, you may have Artisan of Forms become a copy of target creature and gain this ability.').
card_first_print('artisan of forms', 'THS').
card_image_name('artisan of forms'/'THS', 'artisan of forms').
card_uid('artisan of forms'/'THS', 'THS:Artisan of Forms:artisan of forms').
card_rarity('artisan of forms'/'THS', 'Rare').
card_artist('artisan of forms'/'THS', 'Min Yum').
card_number('artisan of forms'/'THS', '40').
card_multiverse_id('artisan of forms'/'THS', '373742').

card_in_set('artisan\'s sorrow', 'THS').
card_original_type('artisan\'s sorrow'/'THS', 'Instant').
card_original_text('artisan\'s sorrow'/'THS', 'Destroy target artifact or enchantment. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('artisan\'s sorrow', 'THS').
card_image_name('artisan\'s sorrow'/'THS', 'artisan\'s sorrow').
card_uid('artisan\'s sorrow'/'THS', 'THS:Artisan\'s Sorrow:artisan\'s sorrow').
card_rarity('artisan\'s sorrow'/'THS', 'Uncommon').
card_artist('artisan\'s sorrow'/'THS', 'Jung Park').
card_number('artisan\'s sorrow'/'THS', '151').
card_flavor_text('artisan\'s sorrow'/'THS', 'Some seers read bones or entrails. Others just like to break things.').
card_multiverse_id('artisan\'s sorrow'/'THS', '373506').

card_in_set('ashen rider', 'THS').
card_original_type('ashen rider'/'THS', 'Creature — Archon').
card_original_text('ashen rider'/'THS', 'Flying\nWhen Ashen Rider enters the battlefield or dies, exile target permanent.').
card_first_print('ashen rider', 'THS').
card_image_name('ashen rider'/'THS', 'ashen rider').
card_uid('ashen rider'/'THS', 'THS:Ashen Rider:ashen rider').
card_rarity('ashen rider'/'THS', 'Mythic Rare').
card_artist('ashen rider'/'THS', 'Chris Rahn').
card_number('ashen rider'/'THS', '187').
card_flavor_text('ashen rider'/'THS', 'One offering to appease her on her arrival. Another to celebrate her departure.').
card_multiverse_id('ashen rider'/'THS', '373689').

card_in_set('ashiok, nightmare weaver', 'THS').
card_original_type('ashiok, nightmare weaver'/'THS', 'Planeswalker — Ashiok').
card_original_text('ashiok, nightmare weaver'/'THS', '+2: Exile the top three cards of target opponent\'s library.\n-X: Put a creature card with converted mana cost X exiled with Ashiok, Nightmare Weaver onto the battlefield under your control. That creature is a Nightmare in addition to its other types.\n-10: Exile all cards from all opponents\' hands and graveyards.').
card_first_print('ashiok, nightmare weaver', 'THS').
card_image_name('ashiok, nightmare weaver'/'THS', 'ashiok, nightmare weaver').
card_uid('ashiok, nightmare weaver'/'THS', 'THS:Ashiok, Nightmare Weaver:ashiok, nightmare weaver').
card_rarity('ashiok, nightmare weaver'/'THS', 'Mythic Rare').
card_artist('ashiok, nightmare weaver'/'THS', 'Karla Ortiz').
card_number('ashiok, nightmare weaver'/'THS', '188').
card_multiverse_id('ashiok, nightmare weaver'/'THS', '373500').

card_in_set('asphodel wanderer', 'THS').
card_original_type('asphodel wanderer'/'THS', 'Creature — Skeleton Soldier').
card_original_text('asphodel wanderer'/'THS', '{2}{B}: Regenerate Asphodel Wanderer.').
card_first_print('asphodel wanderer', 'THS').
card_image_name('asphodel wanderer'/'THS', 'asphodel wanderer').
card_uid('asphodel wanderer'/'THS', 'THS:Asphodel Wanderer:asphodel wanderer').
card_rarity('asphodel wanderer'/'THS', 'Common').
card_artist('asphodel wanderer'/'THS', 'Scott Chou').
card_number('asphodel wanderer'/'THS', '77').
card_flavor_text('asphodel wanderer'/'THS', 'He killed out of hate, so now only hate sustains him. He sought immortality, so the gods gave it to him.').
card_multiverse_id('asphodel wanderer'/'THS', '373631').

card_in_set('baleful eidolon', 'THS').
card_original_type('baleful eidolon'/'THS', 'Enchantment Creature — Spirit').
card_original_text('baleful eidolon'/'THS', 'Bestow {4}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.) \nEnchanted creature gets +1/+1 and has deathtouch.').
card_first_print('baleful eidolon', 'THS').
card_image_name('baleful eidolon'/'THS', 'baleful eidolon').
card_uid('baleful eidolon'/'THS', 'THS:Baleful Eidolon:baleful eidolon').
card_rarity('baleful eidolon'/'THS', 'Common').
card_artist('baleful eidolon'/'THS', 'Min Yum').
card_number('baleful eidolon'/'THS', '78').
card_multiverse_id('baleful eidolon'/'THS', '373720').

card_in_set('battlewise hoplite', 'THS').
card_original_type('battlewise hoplite'/'THS', 'Creature — Human Soldier').
card_original_text('battlewise hoplite'/'THS', 'Heroic — Whenever you cast a spell that targets Battlewise Hoplite, put a +1/+1 counter on Battlewise Hoplite, then scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('battlewise hoplite', 'THS').
card_image_name('battlewise hoplite'/'THS', 'battlewise hoplite').
card_uid('battlewise hoplite'/'THS', 'THS:Battlewise Hoplite:battlewise hoplite').
card_rarity('battlewise hoplite'/'THS', 'Uncommon').
card_artist('battlewise hoplite'/'THS', 'Willian Murai').
card_number('battlewise hoplite'/'THS', '189').
card_multiverse_id('battlewise hoplite'/'THS', '373612').

card_in_set('battlewise valor', 'THS').
card_original_type('battlewise valor'/'THS', 'Instant').
card_original_text('battlewise valor'/'THS', 'Target creature gets +2/+2 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('battlewise valor', 'THS').
card_image_name('battlewise valor'/'THS', 'battlewise valor').
card_uid('battlewise valor'/'THS', 'THS:Battlewise Valor:battlewise valor').
card_rarity('battlewise valor'/'THS', 'Common').
card_artist('battlewise valor'/'THS', 'Zack Stella').
card_number('battlewise valor'/'THS', '1').
card_flavor_text('battlewise valor'/'THS', 'It\'s never good to walk into an ambush, but with the right spell you might walk out again.').
card_multiverse_id('battlewise valor'/'THS', '373627').

card_in_set('benthic giant', 'THS').
card_original_type('benthic giant'/'THS', 'Creature — Giant').
card_original_text('benthic giant'/'THS', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('benthic giant', 'THS').
card_image_name('benthic giant'/'THS', 'benthic giant').
card_uid('benthic giant'/'THS', 'THS:Benthic Giant:benthic giant').
card_rarity('benthic giant'/'THS', 'Common').
card_artist('benthic giant'/'THS', 'Jaime Jones').
card_number('benthic giant'/'THS', '41').
card_flavor_text('benthic giant'/'THS', '\"Some fates you can see coming for you, plain as day, and there\'s nothing you can do about them.\"\n—Callaphe the mariner').
card_multiverse_id('benthic giant'/'THS', '373583').

card_in_set('bident of thassa', 'THS').
card_original_type('bident of thassa'/'THS', 'Legendary Enchantment Artifact').
card_original_text('bident of thassa'/'THS', 'Whenever a creature you control deals combat damage to a player, you may draw a card.\n{1}{U}, {T}: Creatures your opponents control attack this turn if able.').
card_image_name('bident of thassa'/'THS', 'bident of thassa').
card_uid('bident of thassa'/'THS', 'THS:Bident of Thassa:bident of thassa').
card_rarity('bident of thassa'/'THS', 'Rare').
card_artist('bident of thassa'/'THS', 'Yeong-Hao Han').
card_number('bident of thassa'/'THS', '42').
card_flavor_text('bident of thassa'/'THS', 'The wills of mortals shift as the tide ebbs and flows.').
card_multiverse_id('bident of thassa'/'THS', '373544').

card_in_set('blood-toll harpy', 'THS').
card_original_type('blood-toll harpy'/'THS', 'Creature — Harpy').
card_original_text('blood-toll harpy'/'THS', 'Flying\nWhen Blood-Toll Harpy enters the battlefield, each player loses 1 life.').
card_first_print('blood-toll harpy', 'THS').
card_image_name('blood-toll harpy'/'THS', 'blood-toll harpy').
card_uid('blood-toll harpy'/'THS', 'THS:Blood-Toll Harpy:blood-toll harpy').
card_rarity('blood-toll harpy'/'THS', 'Common').
card_artist('blood-toll harpy'/'THS', 'Kev Walker').
card_number('blood-toll harpy'/'THS', '79').
card_flavor_text('blood-toll harpy'/'THS', 'When harpies demand a toll to cross through their territory, consider yourself lucky if they permit payment in coin.').
card_multiverse_id('blood-toll harpy'/'THS', '373638').

card_in_set('boon of erebos', 'THS').
card_original_type('boon of erebos'/'THS', 'Instant').
card_original_text('boon of erebos'/'THS', 'Target creature gets +2/+0 until end of turn. Regenerate it. You lose 2 life.').
card_first_print('boon of erebos', 'THS').
card_image_name('boon of erebos'/'THS', 'boon of erebos').
card_uid('boon of erebos'/'THS', 'THS:Boon of Erebos:boon of erebos').
card_rarity('boon of erebos'/'THS', 'Common').
card_artist('boon of erebos'/'THS', 'James Ryman').
card_number('boon of erebos'/'THS', '80').
card_flavor_text('boon of erebos'/'THS', '\"Death is not a certainty. Erebos determines when a mortal\'s time is up.\"\n—Iadorna, death priest of Erebos').
card_multiverse_id('boon of erebos'/'THS', '373642').

card_in_set('boon satyr', 'THS').
card_original_type('boon satyr'/'THS', 'Enchantment Creature — Satyr').
card_original_text('boon satyr'/'THS', 'Flash\nBestow {3}{G}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +4/+2.').
card_first_print('boon satyr', 'THS').
card_image_name('boon satyr'/'THS', 'boon satyr').
card_uid('boon satyr'/'THS', 'THS:Boon Satyr:boon satyr').
card_rarity('boon satyr'/'THS', 'Rare').
card_artist('boon satyr'/'THS', 'Wesley Burt').
card_number('boon satyr'/'THS', '152').
card_multiverse_id('boon satyr'/'THS', '373509').

card_in_set('borderland minotaur', 'THS').
card_original_type('borderland minotaur'/'THS', 'Creature — Minotaur Warrior').
card_original_text('borderland minotaur'/'THS', '').
card_first_print('borderland minotaur', 'THS').
card_image_name('borderland minotaur'/'THS', 'borderland minotaur').
card_uid('borderland minotaur'/'THS', 'THS:Borderland Minotaur:borderland minotaur').
card_rarity('borderland minotaur'/'THS', 'Common').
card_artist('borderland minotaur'/'THS', 'Greg Staples').
card_number('borderland minotaur'/'THS', '114').
card_flavor_text('borderland minotaur'/'THS', '\"You have led us to triumph over the forces of Mogis!\" said Brygus the Brave, clapping the Champion on the back.\nThe Champion wiped the sweat and blood from her brow.\n\"I count eight graves,\" she said. \"Too many to call this a victory.\"\n—The Theriad').
card_multiverse_id('borderland minotaur'/'THS', '373659').

card_in_set('boulderfall', 'THS').
card_original_type('boulderfall'/'THS', 'Instant').
card_original_text('boulderfall'/'THS', 'Boulderfall deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_first_print('boulderfall', 'THS').
card_image_name('boulderfall'/'THS', 'boulderfall').
card_uid('boulderfall'/'THS', 'THS:Boulderfall:boulderfall').
card_rarity('boulderfall'/'THS', 'Common').
card_artist('boulderfall'/'THS', 'Ralph Horsley').
card_number('boulderfall'/'THS', '115').
card_flavor_text('boulderfall'/'THS', '\"Defeating the Meletians was not so difficult. I needed only to move a mountain.\"\n—Eocles, oracle of Purphoros').
card_multiverse_id('boulderfall'/'THS', '373714').

card_in_set('bow of nylea', 'THS').
card_original_type('bow of nylea'/'THS', 'Legendary Enchantment Artifact').
card_original_text('bow of nylea'/'THS', 'Attacking creatures you control have deathtouch.\n{1}{G}, {T}: Choose one — Put a +1/+1 counter on target creature; or Bow of Nylea deals 2 damage to target creature with flying; or you gain 3 life; or put up to four target cards from your graveyard on the bottom of your library in any order.').
card_first_print('bow of nylea', 'THS').
card_image_name('bow of nylea'/'THS', 'bow of nylea').
card_uid('bow of nylea'/'THS', 'THS:Bow of Nylea:bow of nylea').
card_rarity('bow of nylea'/'THS', 'Rare').
card_artist('bow of nylea'/'THS', 'Yeong-Hao Han').
card_number('bow of nylea'/'THS', '153').
card_multiverse_id('bow of nylea'/'THS', '373603').

card_in_set('breaching hippocamp', 'THS').
card_original_type('breaching hippocamp'/'THS', 'Creature — Horse Fish').
card_original_text('breaching hippocamp'/'THS', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Breaching Hippocamp enters the battlefield, untap another target creature you control.').
card_first_print('breaching hippocamp', 'THS').
card_image_name('breaching hippocamp'/'THS', 'breaching hippocamp').
card_uid('breaching hippocamp'/'THS', 'THS:Breaching Hippocamp:breaching hippocamp').
card_rarity('breaching hippocamp'/'THS', 'Common').
card_artist('breaching hippocamp'/'THS', 'Christopher Burdett').
card_number('breaching hippocamp'/'THS', '43').
card_flavor_text('breaching hippocamp'/'THS', 'Don\'t try to ride this steed unless you\'ve got gills too.').
card_multiverse_id('breaching hippocamp'/'THS', '373636').

card_in_set('bronze sable', 'THS').
card_original_type('bronze sable'/'THS', 'Artifact Creature — Sable').
card_original_text('bronze sable'/'THS', '').
card_first_print('bronze sable', 'THS').
card_image_name('bronze sable'/'THS', 'bronze sable').
card_uid('bronze sable'/'THS', 'THS:Bronze Sable:bronze sable').
card_rarity('bronze sable'/'THS', 'Common').
card_artist('bronze sable'/'THS', 'Jasper Sandner').
card_number('bronze sable'/'THS', '212').
card_flavor_text('bronze sable'/'THS', 'The Champion stood alone between the horde of the Returned and the shrine to Karametra, cutting down scores among hundreds. She would have been overcome if not for the aid of the temple guardians whom Karametra awakened.\n—The Theriad').
card_multiverse_id('bronze sable'/'THS', '373730').

card_in_set('burnished hart', 'THS').
card_original_type('burnished hart'/'THS', 'Artifact Creature — Elk').
card_original_text('burnished hart'/'THS', '{3}, Sacrifice Burnished Hart: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('burnished hart', 'THS').
card_image_name('burnished hart'/'THS', 'burnished hart').
card_uid('burnished hart'/'THS', 'THS:Burnished Hart:burnished hart').
card_rarity('burnished hart'/'THS', 'Uncommon').
card_artist('burnished hart'/'THS', 'Yeong-Hao Han').
card_number('burnished hart'/'THS', '213').
card_flavor_text('burnished hart'/'THS', 'Forged by divine hands to wander mortal realms.').
card_multiverse_id('burnished hart'/'THS', '373620').

card_in_set('cavalry pegasus', 'THS').
card_original_type('cavalry pegasus'/'THS', 'Creature — Pegasus').
card_original_text('cavalry pegasus'/'THS', 'Flying\nWhenever Cavalry Pegasus attacks, each attacking Human gains flying until end of turn.').
card_image_name('cavalry pegasus'/'THS', 'cavalry pegasus').
card_uid('cavalry pegasus'/'THS', 'THS:Cavalry Pegasus:cavalry pegasus').
card_rarity('cavalry pegasus'/'THS', 'Common').
card_artist('cavalry pegasus'/'THS', 'Kev Walker').
card_number('cavalry pegasus'/'THS', '2').
card_flavor_text('cavalry pegasus'/'THS', '\"It is hope, hooved and winged.\"\n—Cymede, queen of Akros').
card_multiverse_id('cavalry pegasus'/'THS', '373684').

card_in_set('cavern lampad', 'THS').
card_original_type('cavern lampad'/'THS', 'Enchantment Creature — Nymph').
card_original_text('cavern lampad'/'THS', 'Bestow {5}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nIntimidate\nEnchanted creature gets +2/+2 and has intimidate.').
card_first_print('cavern lampad', 'THS').
card_image_name('cavern lampad'/'THS', 'cavern lampad').
card_uid('cavern lampad'/'THS', 'THS:Cavern Lampad:cavern lampad').
card_rarity('cavern lampad'/'THS', 'Common').
card_artist('cavern lampad'/'THS', 'Volkan Baga').
card_number('cavern lampad'/'THS', '81').
card_multiverse_id('cavern lampad'/'THS', '373726').

card_in_set('celestial archon', 'THS').
card_original_type('celestial archon'/'THS', 'Enchantment Creature — Archon').
card_original_text('celestial archon'/'THS', 'Bestow {5}{W}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying, first strike\nEnchanted creature gets +4/+4 and has flying and first strike.').
card_image_name('celestial archon'/'THS', 'celestial archon').
card_uid('celestial archon'/'THS', 'THS:Celestial Archon:celestial archon').
card_rarity('celestial archon'/'THS', 'Rare').
card_artist('celestial archon'/'THS', 'Matt Stewart').
card_number('celestial archon'/'THS', '3').
card_multiverse_id('celestial archon'/'THS', '373503').

card_in_set('centaur battlemaster', 'THS').
card_original_type('centaur battlemaster'/'THS', 'Creature — Centaur Warrior').
card_original_text('centaur battlemaster'/'THS', 'Heroic — Whenever you cast a spell that targets Centaur Battlemaster, put three +1/+1 counters on Centaur Battlemaster.').
card_first_print('centaur battlemaster', 'THS').
card_image_name('centaur battlemaster'/'THS', 'centaur battlemaster').
card_uid('centaur battlemaster'/'THS', 'THS:Centaur Battlemaster:centaur battlemaster').
card_rarity('centaur battlemaster'/'THS', 'Uncommon').
card_artist('centaur battlemaster'/'THS', 'Kev Walker').
card_number('centaur battlemaster'/'THS', '154').
card_flavor_text('centaur battlemaster'/'THS', '\"A herd is only as strong as its weakest. Our weakest just killed his third minotaur raider—today.\"\n—Braulios of Pheres Band').
card_multiverse_id('centaur battlemaster'/'THS', '373594').

card_in_set('chained to the rocks', 'THS').
card_original_type('chained to the rocks'/'THS', 'Enchantment — Aura').
card_original_text('chained to the rocks'/'THS', 'Enchant Mountain you control\nWhen Chained to the Rocks enters the battlefield, exile target creature an opponent controls until Chained to the Rocks leaves the battlefield. (That creature returns under its owner\'s control.)').
card_first_print('chained to the rocks', 'THS').
card_image_name('chained to the rocks'/'THS', 'chained to the rocks').
card_uid('chained to the rocks'/'THS', 'THS:Chained to the Rocks:chained to the rocks').
card_rarity('chained to the rocks'/'THS', 'Rare').
card_artist('chained to the rocks'/'THS', 'Aaron Miller').
card_number('chained to the rocks'/'THS', '4').
card_multiverse_id('chained to the rocks'/'THS', '373521').

card_in_set('chosen by heliod', 'THS').
card_original_type('chosen by heliod'/'THS', 'Enchantment — Aura').
card_original_text('chosen by heliod'/'THS', 'Enchant creature\nWhen Chosen by Heliod enters the battlefield, draw a card.\nEnchanted creature gets +0/+2.').
card_first_print('chosen by heliod', 'THS').
card_image_name('chosen by heliod'/'THS', 'chosen by heliod').
card_uid('chosen by heliod'/'THS', 'THS:Chosen by Heliod:chosen by heliod').
card_rarity('chosen by heliod'/'THS', 'Common').
card_artist('chosen by heliod'/'THS', 'Zack Stella').
card_number('chosen by heliod'/'THS', '5').
card_flavor_text('chosen by heliod'/'THS', '\"Training and studies aid a soldier in meager amounts. The gods do the rest.\"\n—Brigone, soldier of Meletis').
card_multiverse_id('chosen by heliod'/'THS', '373561').

card_in_set('chronicler of heroes', 'THS').
card_original_type('chronicler of heroes'/'THS', 'Creature — Centaur Wizard').
card_original_text('chronicler of heroes'/'THS', 'When Chronicler of Heroes enters the battlefield, draw a card if you control a creature with a +1/+1 counter on it.').
card_first_print('chronicler of heroes', 'THS').
card_image_name('chronicler of heroes'/'THS', 'chronicler of heroes').
card_uid('chronicler of heroes'/'THS', 'THS:Chronicler of Heroes:chronicler of heroes').
card_rarity('chronicler of heroes'/'THS', 'Uncommon').
card_artist('chronicler of heroes'/'THS', 'John Stanko').
card_number('chronicler of heroes'/'THS', '190').
card_flavor_text('chronicler of heroes'/'THS', 'She paints pictures with words, though not all pictures show the truth.').
card_multiverse_id('chronicler of heroes'/'THS', '373621').

card_in_set('coastline chimera', 'THS').
card_original_type('coastline chimera'/'THS', 'Creature — Chimera').
card_original_text('coastline chimera'/'THS', 'Flying\n{1}{W}: Coastline Chimera can block an additional creature this turn.').
card_first_print('coastline chimera', 'THS').
card_image_name('coastline chimera'/'THS', 'coastline chimera').
card_uid('coastline chimera'/'THS', 'THS:Coastline Chimera:coastline chimera').
card_rarity('coastline chimera'/'THS', 'Common').
card_artist('coastline chimera'/'THS', 'Dan Scott').
card_number('coastline chimera'/'THS', '44').
card_flavor_text('coastline chimera'/'THS', 'Seeing a chimera overhead foretells good fortune, but only because seeing one any closer foretells dismemberment.').
card_multiverse_id('coastline chimera'/'THS', '373707').

card_in_set('colossus of akros', 'THS').
card_original_type('colossus of akros'/'THS', 'Artifact Creature — Golem').
card_original_text('colossus of akros'/'THS', 'Defender, indestructible\n{1}0: Monstrosity 10. (If this creature isn\'t monstrous, put ten +1/+1 counters on it and it becomes monstrous.)\nAs long as Colossus of Akros is monstrous, it has trample and can attack as though it didn\'t have defender.').
card_first_print('colossus of akros', 'THS').
card_image_name('colossus of akros'/'THS', 'colossus of akros').
card_uid('colossus of akros'/'THS', 'THS:Colossus of Akros:colossus of akros').
card_rarity('colossus of akros'/'THS', 'Rare').
card_artist('colossus of akros'/'THS', 'Zack Stella').
card_number('colossus of akros'/'THS', '214').
card_multiverse_id('colossus of akros'/'THS', '373555').

card_in_set('commune with the gods', 'THS').
card_original_type('commune with the gods'/'THS', 'Sorcery').
card_original_text('commune with the gods'/'THS', 'Reveal the top five cards of your library. You may put a creature or enchantment card from among them into your hand. Put the rest into your graveyard.').
card_first_print('commune with the gods', 'THS').
card_image_name('commune with the gods'/'THS', 'commune with the gods').
card_uid('commune with the gods'/'THS', 'THS:Commune with the Gods:commune with the gods').
card_rarity('commune with the gods'/'THS', 'Common').
card_artist('commune with the gods'/'THS', 'Aleksi Briclot').
card_number('commune with the gods'/'THS', '155').
card_flavor_text('commune with the gods'/'THS', 'For the first time in many years, Elspeth asked for help.').
card_multiverse_id('commune with the gods'/'THS', '373656').

card_in_set('coordinated assault', 'THS').
card_original_type('coordinated assault'/'THS', 'Instant').
card_original_text('coordinated assault'/'THS', 'Up to two target creatures each get +1/+0 and gain first strike until end of turn.').
card_first_print('coordinated assault', 'THS').
card_image_name('coordinated assault'/'THS', 'coordinated assault').
card_uid('coordinated assault'/'THS', 'THS:Coordinated Assault:coordinated assault').
card_rarity('coordinated assault'/'THS', 'Uncommon').
card_artist('coordinated assault'/'THS', 'John Severin Brassell').
card_number('coordinated assault'/'THS', '116').
card_flavor_text('coordinated assault'/'THS', 'It\'s hard to shout \"Shields up!\" with a javelin in your chest.').
card_multiverse_id('coordinated assault'/'THS', '373513').

card_in_set('crackling triton', 'THS').
card_original_type('crackling triton'/'THS', 'Creature — Merfolk Wizard').
card_original_text('crackling triton'/'THS', '{2}{R}, Sacrifice Crackling Triton: Crackling Triton deals 2 damage to target creature or player.').
card_first_print('crackling triton', 'THS').
card_image_name('crackling triton'/'THS', 'crackling triton').
card_uid('crackling triton'/'THS', 'THS:Crackling Triton:crackling triton').
card_rarity('crackling triton'/'THS', 'Common').
card_artist('crackling triton'/'THS', 'Greg Staples').
card_number('crackling triton'/'THS', '45').
card_flavor_text('crackling triton'/'THS', 'He calls upon both the currents in the sea and the current in the clouds.').
card_multiverse_id('crackling triton'/'THS', '373588').

card_in_set('curse of the swine', 'THS').
card_original_type('curse of the swine'/'THS', 'Sorcery').
card_original_text('curse of the swine'/'THS', 'Exile X target creatures. For each creature exiled this way, its controller puts a 2/2 green Boar creature token onto the battlefield.').
card_first_print('curse of the swine', 'THS').
card_image_name('curse of the swine'/'THS', 'curse of the swine').
card_uid('curse of the swine'/'THS', 'THS:Curse of the Swine:curse of the swine').
card_rarity('curse of the swine'/'THS', 'Rare').
card_artist('curse of the swine'/'THS', 'James Ryman').
card_number('curse of the swine'/'THS', '46').
card_flavor_text('curse of the swine'/'THS', 'Another imminent battle subsided in busy snuffling and carefree rooting.').
card_multiverse_id('curse of the swine'/'THS', '373542').

card_in_set('cutthroat maneuver', 'THS').
card_original_type('cutthroat maneuver'/'THS', 'Instant').
card_original_text('cutthroat maneuver'/'THS', 'Up to two target creatures each get +1/+1 and gain lifelink until end of turn.').
card_first_print('cutthroat maneuver', 'THS').
card_image_name('cutthroat maneuver'/'THS', 'cutthroat maneuver').
card_uid('cutthroat maneuver'/'THS', 'THS:Cutthroat Maneuver:cutthroat maneuver').
card_rarity('cutthroat maneuver'/'THS', 'Uncommon').
card_artist('cutthroat maneuver'/'THS', 'Brad Rigney').
card_number('cutthroat maneuver'/'THS', '82').
card_flavor_text('cutthroat maneuver'/'THS', '\"Our ambition drives us forward. Together we will claim what is ours, no matter who holds it.\"').
card_multiverse_id('cutthroat maneuver'/'THS', '373660').

card_in_set('dark betrayal', 'THS').
card_original_type('dark betrayal'/'THS', 'Instant').
card_original_text('dark betrayal'/'THS', 'Destroy target black creature.').
card_first_print('dark betrayal', 'THS').
card_image_name('dark betrayal'/'THS', 'dark betrayal').
card_uid('dark betrayal'/'THS', 'THS:Dark Betrayal:dark betrayal').
card_rarity('dark betrayal'/'THS', 'Uncommon').
card_artist('dark betrayal'/'THS', 'Nils Hamm').
card_number('dark betrayal'/'THS', '83').
card_flavor_text('dark betrayal'/'THS', '\"You\'re just like me: ruthless, cunning, and ambitious. Obviously you\'re a threat.\"\n—Basarios the Blade').
card_multiverse_id('dark betrayal'/'THS', '373504').

card_in_set('dauntless onslaught', 'THS').
card_original_type('dauntless onslaught'/'THS', 'Instant').
card_original_text('dauntless onslaught'/'THS', 'Up to two target creatures each get +2/+2 until end of turn.').
card_first_print('dauntless onslaught', 'THS').
card_image_name('dauntless onslaught'/'THS', 'dauntless onslaught').
card_uid('dauntless onslaught'/'THS', 'THS:Dauntless Onslaught:dauntless onslaught').
card_rarity('dauntless onslaught'/'THS', 'Uncommon').
card_artist('dauntless onslaught'/'THS', 'Peter Mohrbacher').
card_number('dauntless onslaught'/'THS', '6').
card_flavor_text('dauntless onslaught'/'THS', '\"The people of Akros must learn from our leonin adversaries. If we match their staunch ferocity with our superior faith, we cannot fail.\"\n—Cymede, queen of Akros').
card_multiverse_id('dauntless onslaught'/'THS', '373525').

card_in_set('daxos of meletis', 'THS').
card_original_type('daxos of meletis'/'THS', 'Legendary Creature — Human Soldier').
card_original_text('daxos of meletis'/'THS', 'Daxos of Meletis can\'t be blocked by creatures with power 3 or greater.\nWhenever Daxos of Meletis deals combat damage to a player, exile the top card of that player\'s library. You gain life equal to that card\'s converted mana cost. Until end of turn, you may cast that card and you may spend mana as though it were mana of any color to cast it.').
card_first_print('daxos of meletis', 'THS').
card_image_name('daxos of meletis'/'THS', 'daxos of meletis').
card_uid('daxos of meletis'/'THS', 'THS:Daxos of Meletis:daxos of meletis').
card_rarity('daxos of meletis'/'THS', 'Rare').
card_artist('daxos of meletis'/'THS', 'Karla Ortiz').
card_number('daxos of meletis'/'THS', '191').
card_multiverse_id('daxos of meletis'/'THS', '373664').

card_in_set('deathbellow raider', 'THS').
card_original_type('deathbellow raider'/'THS', 'Creature — Minotaur Berserker').
card_original_text('deathbellow raider'/'THS', 'Deathbellow Raider attacks each turn if able.\n{2}{B}: Regenerate Deathbellow Raider.').
card_first_print('deathbellow raider', 'THS').
card_image_name('deathbellow raider'/'THS', 'deathbellow raider').
card_uid('deathbellow raider'/'THS', 'THS:Deathbellow Raider:deathbellow raider').
card_rarity('deathbellow raider'/'THS', 'Common').
card_artist('deathbellow raider'/'THS', 'Wayne Reynolds').
card_number('deathbellow raider'/'THS', '117').
card_flavor_text('deathbellow raider'/'THS', '\"The temple has been rededicated. It belongs to Mogis now.\"\n—Rastos, disciple of Mogis').
card_multiverse_id('deathbellow raider'/'THS', '373593').

card_in_set('decorated griffin', 'THS').
card_original_type('decorated griffin'/'THS', 'Creature — Griffin').
card_original_text('decorated griffin'/'THS', 'Flying\n{1}{W}: Prevent the next 1 combat damage that would be dealt to you this turn.').
card_first_print('decorated griffin', 'THS').
card_image_name('decorated griffin'/'THS', 'decorated griffin').
card_uid('decorated griffin'/'THS', 'THS:Decorated Griffin:decorated griffin').
card_rarity('decorated griffin'/'THS', 'Uncommon').
card_artist('decorated griffin'/'THS', 'Phill Simmer').
card_number('decorated griffin'/'THS', '7').
card_flavor_text('decorated griffin'/'THS', 'The awards and medals of polis-dwellers mean nothing to griffins, but they repay acts of generosity.').
card_multiverse_id('decorated griffin'/'THS', '373577').

card_in_set('defend the hearth', 'THS').
card_original_type('defend the hearth'/'THS', 'Instant').
card_original_text('defend the hearth'/'THS', 'Prevent all combat damage that would be dealt to players this turn.').
card_first_print('defend the hearth', 'THS').
card_image_name('defend the hearth'/'THS', 'defend the hearth').
card_uid('defend the hearth'/'THS', 'THS:Defend the Hearth:defend the hearth').
card_rarity('defend the hearth'/'THS', 'Common').
card_artist('defend the hearth'/'THS', 'Raymond Swanland').
card_number('defend the hearth'/'THS', '156').
card_flavor_text('defend the hearth'/'THS', 'The defenders said not a word. Their shields, spears, and stances were warning enough.').
card_multiverse_id('defend the hearth'/'THS', '373623').

card_in_set('demolish', 'THS').
card_original_type('demolish'/'THS', 'Sorcery').
card_original_text('demolish'/'THS', 'Destroy target artifact or land.').
card_image_name('demolish'/'THS', 'demolish').
card_uid('demolish'/'THS', 'THS:Demolish:demolish').
card_rarity('demolish'/'THS', 'Common').
card_artist('demolish'/'THS', 'Volkan Baga').
card_number('demolish'/'THS', '118').
card_flavor_text('demolish'/'THS', '\"When fire is shackled to candle and torch, subjected to insignificance, it finds a way to lash out at its masters.\"\n—Vala, disciple of Purphoros').
card_multiverse_id('demolish'/'THS', '373537').

card_in_set('destructive revelry', 'THS').
card_original_type('destructive revelry'/'THS', 'Instant').
card_original_text('destructive revelry'/'THS', 'Destroy target artifact or enchantment. Destructive Revelry deals 2 damage to that permanent\'s controller.').
card_image_name('destructive revelry'/'THS', 'destructive revelry').
card_uid('destructive revelry'/'THS', 'THS:Destructive Revelry:destructive revelry').
card_rarity('destructive revelry'/'THS', 'Uncommon').
card_artist('destructive revelry'/'THS', 'Kev Walker').
card_number('destructive revelry'/'THS', '192').
card_flavor_text('destructive revelry'/'THS', '\"Stoke a fire hot enough and you\'ll never run out of things to burn.\"\n—Xenagos, the Reveler').
card_multiverse_id('destructive revelry'/'THS', '373678').

card_in_set('disciple of phenax', 'THS').
card_original_type('disciple of phenax'/'THS', 'Creature — Human Cleric').
card_original_text('disciple of phenax'/'THS', 'When Disciple of Phenax enters the battlefield, target player reveals a number of cards from his or her hand equal to your devotion to black. You choose one of them. That player discards that card. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_first_print('disciple of phenax', 'THS').
card_image_name('disciple of phenax'/'THS', 'disciple of phenax').
card_uid('disciple of phenax'/'THS', 'THS:Disciple of Phenax:disciple of phenax').
card_rarity('disciple of phenax'/'THS', 'Common').
card_artist('disciple of phenax'/'THS', 'John Severin Brassell').
card_number('disciple of phenax'/'THS', '84').
card_multiverse_id('disciple of phenax'/'THS', '373520').

card_in_set('dissolve', 'THS').
card_original_type('dissolve'/'THS', 'Instant').
card_original_text('dissolve'/'THS', 'Counter target spell. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_image_name('dissolve'/'THS', 'dissolve').
card_uid('dissolve'/'THS', 'THS:Dissolve:dissolve').
card_rarity('dissolve'/'THS', 'Uncommon').
card_artist('dissolve'/'THS', 'Wesley Burt').
card_number('dissolve'/'THS', '47').
card_flavor_text('dissolve'/'THS', '\"You thought only the gods could stop you?\"').
card_multiverse_id('dissolve'/'THS', '373557').

card_in_set('divine verdict', 'THS').
card_original_type('divine verdict'/'THS', 'Instant').
card_original_text('divine verdict'/'THS', 'Destroy target attacking or blocking creature.').
card_image_name('divine verdict'/'THS', 'divine verdict').
card_uid('divine verdict'/'THS', 'THS:Divine Verdict:divine verdict').
card_rarity('divine verdict'/'THS', 'Common').
card_artist('divine verdict'/'THS', 'Raymond Swanland').
card_number('divine verdict'/'THS', '8').
card_flavor_text('divine verdict'/'THS', 'The last thing to go through the cyclops\'s mind was a twelve-ton block of marble.').
card_multiverse_id('divine verdict'/'THS', '373648').

card_in_set('dragon mantle', 'THS').
card_original_type('dragon mantle'/'THS', 'Enchantment — Aura').
card_original_text('dragon mantle'/'THS', 'Enchant creature\nWhen Dragon Mantle enters the battlefield, draw a card.\nEnchanted creature has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('dragon mantle', 'THS').
card_image_name('dragon mantle'/'THS', 'dragon mantle').
card_uid('dragon mantle'/'THS', 'THS:Dragon Mantle:dragon mantle').
card_rarity('dragon mantle'/'THS', 'Common').
card_artist('dragon mantle'/'THS', 'Anthony Palumbo').
card_number('dragon mantle'/'THS', '119').
card_multiverse_id('dragon mantle'/'THS', '373634').

card_in_set('elspeth, sun\'s champion', 'THS').
card_original_type('elspeth, sun\'s champion'/'THS', 'Planeswalker — Elspeth').
card_original_text('elspeth, sun\'s champion'/'THS', '+1: Put three 1/1 white Soldier creature tokens onto the battlefield.\n-3: Destroy all creatures with power 4 or greater.\n-7: You get an emblem with \"Creatures you control get +2/+2 and have flying.\"').
card_first_print('elspeth, sun\'s champion', 'THS').
card_image_name('elspeth, sun\'s champion'/'THS', 'elspeth, sun\'s champion').
card_uid('elspeth, sun\'s champion'/'THS', 'THS:Elspeth, Sun\'s Champion:elspeth, sun\'s champion').
card_rarity('elspeth, sun\'s champion'/'THS', 'Mythic Rare').
card_artist('elspeth, sun\'s champion'/'THS', 'Eric Deschamps').
card_number('elspeth, sun\'s champion'/'THS', '9').
card_multiverse_id('elspeth, sun\'s champion'/'THS', '373649').

card_in_set('ember swallower', 'THS').
card_original_type('ember swallower'/'THS', 'Creature — Elemental').
card_original_text('ember swallower'/'THS', '{5}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Ember Swallower becomes monstrous, each player sacrifices three lands.').
card_image_name('ember swallower'/'THS', 'ember swallower').
card_uid('ember swallower'/'THS', 'THS:Ember Swallower:ember swallower').
card_rarity('ember swallower'/'THS', 'Rare').
card_artist('ember swallower'/'THS', 'Slawomir Maniak').
card_number('ember swallower'/'THS', '120').
card_multiverse_id('ember swallower'/'THS', '373597').

card_in_set('ephara\'s warden', 'THS').
card_original_type('ephara\'s warden'/'THS', 'Creature — Human Cleric').
card_original_text('ephara\'s warden'/'THS', '{T}: Tap target creature with power 3 or less.').
card_first_print('ephara\'s warden', 'THS').
card_image_name('ephara\'s warden'/'THS', 'ephara\'s warden').
card_uid('ephara\'s warden'/'THS', 'THS:Ephara\'s Warden:ephara\'s warden').
card_rarity('ephara\'s warden'/'THS', 'Common').
card_artist('ephara\'s warden'/'THS', 'Zack Stella').
card_number('ephara\'s warden'/'THS', '10').
card_flavor_text('ephara\'s warden'/'THS', '\"When you threaten the sanctity of the polis, you insult Ephara herself. If she doesn\'t smite you, I will.\"').
card_multiverse_id('ephara\'s warden'/'THS', '373724').

card_in_set('erebos\'s emissary', 'THS').
card_original_type('erebos\'s emissary'/'THS', 'Enchantment Creature — Snake').
card_original_text('erebos\'s emissary'/'THS', 'Bestow {5}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDiscard a creature card: Erebos\'s Emissary gets +2/+2 until end of turn. If Erebos\'s Emissary is an Aura, enchanted creature gets +2/+2 until end of turn instead.\nEnchanted creature gets +3/+3.').
card_first_print('erebos\'s emissary', 'THS').
card_image_name('erebos\'s emissary'/'THS', 'erebos\'s emissary').
card_uid('erebos\'s emissary'/'THS', 'THS:Erebos\'s Emissary:erebos\'s emissary').
card_rarity('erebos\'s emissary'/'THS', 'Uncommon').
card_artist('erebos\'s emissary'/'THS', 'Sam Burley').
card_number('erebos\'s emissary'/'THS', '86').
card_multiverse_id('erebos\'s emissary'/'THS', '373745').

card_in_set('erebos, god of the dead', 'THS').
card_original_type('erebos, god of the dead'/'THS', 'Legendary Enchantment Creature — God').
card_original_text('erebos, god of the dead'/'THS', 'Indestructible\nAs long as your devotion to black is less than five, Erebos isn\'t a creature. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)\nYour opponents can\'t gain life.\n{1}{B}, Pay 2 life: Draw a card.').
card_first_print('erebos, god of the dead', 'THS').
card_image_name('erebos, god of the dead'/'THS', 'erebos, god of the dead').
card_uid('erebos, god of the dead'/'THS', 'THS:Erebos, God of the Dead:erebos, god of the dead').
card_rarity('erebos, god of the dead'/'THS', 'Mythic Rare').
card_artist('erebos, god of the dead'/'THS', 'Peter Mohrbacher').
card_number('erebos, god of the dead'/'THS', '85').
card_multiverse_id('erebos, god of the dead'/'THS', '373589').

card_in_set('evangel of heliod', 'THS').
card_original_type('evangel of heliod'/'THS', 'Creature — Human Cleric').
card_original_text('evangel of heliod'/'THS', 'When Evangel of Heliod enters the battlefield, put a number of 1/1 white Soldier creature tokens onto the battlefield equal to your devotion to white. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)').
card_first_print('evangel of heliod', 'THS').
card_image_name('evangel of heliod'/'THS', 'evangel of heliod').
card_uid('evangel of heliod'/'THS', 'THS:Evangel of Heliod:evangel of heliod').
card_rarity('evangel of heliod'/'THS', 'Uncommon').
card_artist('evangel of heliod'/'THS', 'Nils Hamm').
card_number('evangel of heliod'/'THS', '11').
card_multiverse_id('evangel of heliod'/'THS', '373641').

card_in_set('fabled hero', 'THS').
card_original_type('fabled hero'/'THS', 'Creature — Human Soldier').
card_original_text('fabled hero'/'THS', 'Double strike\nHeroic — Whenever you cast a spell that targets Fabled Hero, put a +1/+1 counter on Fabled Hero.').
card_first_print('fabled hero', 'THS').
card_image_name('fabled hero'/'THS', 'fabled hero').
card_uid('fabled hero'/'THS', 'THS:Fabled Hero:fabled hero').
card_rarity('fabled hero'/'THS', 'Rare').
card_artist('fabled hero'/'THS', 'Aaron Miller').
card_number('fabled hero'/'THS', '12').
card_flavor_text('fabled hero'/'THS', '\"You. Poet. Be sure to write this down.\"').
card_multiverse_id('fabled hero'/'THS', '373606').

card_in_set('fade into antiquity', 'THS').
card_original_type('fade into antiquity'/'THS', 'Sorcery').
card_original_text('fade into antiquity'/'THS', 'Exile target artifact or enchantment.').
card_first_print('fade into antiquity', 'THS').
card_image_name('fade into antiquity'/'THS', 'fade into antiquity').
card_uid('fade into antiquity'/'THS', 'THS:Fade into Antiquity:fade into antiquity').
card_rarity('fade into antiquity'/'THS', 'Common').
card_artist('fade into antiquity'/'THS', 'Noah Bradley').
card_number('fade into antiquity'/'THS', '157').
card_flavor_text('fade into antiquity'/'THS', '\"Are the gods angry at our discontent with what they give us, or jealous that we made a thing they cannot?\"\n—Kleon the Iron-Booted').
card_multiverse_id('fade into antiquity'/'THS', '373576').

card_in_set('fanatic of mogis', 'THS').
card_original_type('fanatic of mogis'/'THS', 'Creature — Minotaur Shaman').
card_original_text('fanatic of mogis'/'THS', 'When Fanatic of Mogis enters the battlefield, it deals damage to each opponent equal to your devotion to red. (Each {R} in the mana costs of permanents you control counts toward your devotion to red.)').
card_first_print('fanatic of mogis', 'THS').
card_image_name('fanatic of mogis'/'THS', 'fanatic of mogis').
card_uid('fanatic of mogis'/'THS', 'THS:Fanatic of Mogis:fanatic of mogis').
card_rarity('fanatic of mogis'/'THS', 'Uncommon').
card_artist('fanatic of mogis'/'THS', 'Matt Stewart').
card_number('fanatic of mogis'/'THS', '121').
card_multiverse_id('fanatic of mogis'/'THS', '373511').

card_in_set('fate foretold', 'THS').
card_original_type('fate foretold'/'THS', 'Enchantment — Aura').
card_original_text('fate foretold'/'THS', 'Enchant creature\nWhen Fate Foretold enters the battlefield, draw a card.\nWhen enchanted creature dies, its controller draws a card.').
card_first_print('fate foretold', 'THS').
card_image_name('fate foretold'/'THS', 'fate foretold').
card_uid('fate foretold'/'THS', 'THS:Fate Foretold:fate foretold').
card_rarity('fate foretold'/'THS', 'Common').
card_artist('fate foretold'/'THS', 'Dan Scott').
card_number('fate foretold'/'THS', '48').
card_flavor_text('fate foretold'/'THS', 'The tale of her life was already written, but that didn\'t mean she could predict the ending.').
card_multiverse_id('fate foretold'/'THS', '373686').

card_in_set('favored hoplite', 'THS').
card_original_type('favored hoplite'/'THS', 'Creature — Human Soldier').
card_original_text('favored hoplite'/'THS', 'Heroic — Whenever you cast a spell that targets Favored Hoplite, put a +1/+1 counter on Favored Hoplite and prevent all damage that would be dealt to it this turn.').
card_first_print('favored hoplite', 'THS').
card_image_name('favored hoplite'/'THS', 'favored hoplite').
card_uid('favored hoplite'/'THS', 'THS:Favored Hoplite:favored hoplite').
card_rarity('favored hoplite'/'THS', 'Uncommon').
card_artist('favored hoplite'/'THS', 'Winona Nelson').
card_number('favored hoplite'/'THS', '13').
card_multiverse_id('favored hoplite'/'THS', '373596').

card_in_set('felhide minotaur', 'THS').
card_original_type('felhide minotaur'/'THS', 'Creature — Minotaur').
card_original_text('felhide minotaur'/'THS', '').
card_first_print('felhide minotaur', 'THS').
card_image_name('felhide minotaur'/'THS', 'felhide minotaur').
card_uid('felhide minotaur'/'THS', 'THS:Felhide Minotaur:felhide minotaur').
card_rarity('felhide minotaur'/'THS', 'Common').
card_artist('felhide minotaur'/'THS', 'Kev Walker').
card_number('felhide minotaur'/'THS', '87').
card_flavor_text('felhide minotaur'/'THS', 'With spear held high, the Champion came to meet Thyrogog of the Ashlands, who wore the old king\'s skin as a cloak and fed on the flesh of innocents. The foul minotaur raised the great axe called Goremaster and charged.\n—The Theriad').
card_multiverse_id('felhide minotaur'/'THS', '373674').

card_in_set('feral invocation', 'THS').
card_original_type('feral invocation'/'THS', 'Enchantment — Aura').
card_original_text('feral invocation'/'THS', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+2.').
card_first_print('feral invocation', 'THS').
card_image_name('feral invocation'/'THS', 'feral invocation').
card_uid('feral invocation'/'THS', 'THS:Feral Invocation:feral invocation').
card_rarity('feral invocation'/'THS', 'Common').
card_artist('feral invocation'/'THS', 'Mathias Kollros').
card_number('feral invocation'/'THS', '158').
card_flavor_text('feral invocation'/'THS', 'Nylea\'s sacred lynx guards those who honor the Nessian Wood and hunts those who don\'t.').
card_multiverse_id('feral invocation'/'THS', '373703').

card_in_set('firedrinker satyr', 'THS').
card_original_type('firedrinker satyr'/'THS', 'Creature — Satyr Shaman').
card_original_text('firedrinker satyr'/'THS', 'Whenever Firedrinker Satyr is dealt damage, it deals that much damage to you.\n{1}{R}: Firedrinker Satyr gets +1/+0 until end of turn and deals 1 damage to you.').
card_first_print('firedrinker satyr', 'THS').
card_image_name('firedrinker satyr'/'THS', 'firedrinker satyr').
card_uid('firedrinker satyr'/'THS', 'THS:Firedrinker Satyr:firedrinker satyr').
card_rarity('firedrinker satyr'/'THS', 'Rare').
card_artist('firedrinker satyr'/'THS', 'Anthony Palumbo').
card_number('firedrinker satyr'/'THS', '122').
card_flavor_text('firedrinker satyr'/'THS', 'Attending a satyr revel requires a high tolerance for pain.').
card_multiverse_id('firedrinker satyr'/'THS', '373552').

card_in_set('flamecast wheel', 'THS').
card_original_type('flamecast wheel'/'THS', 'Artifact').
card_original_text('flamecast wheel'/'THS', '{5}, {T}, Sacrifice Flamecast Wheel: Flamecast Wheel deals 3 damage to target creature.').
card_first_print('flamecast wheel', 'THS').
card_image_name('flamecast wheel'/'THS', 'flamecast wheel').
card_uid('flamecast wheel'/'THS', 'THS:Flamecast Wheel:flamecast wheel').
card_rarity('flamecast wheel'/'THS', 'Uncommon').
card_artist('flamecast wheel'/'THS', 'Jasper Sandner').
card_number('flamecast wheel'/'THS', '215').
card_flavor_text('flamecast wheel'/'THS', 'Beware the gifts of an ill-tempered forge god.').
card_multiverse_id('flamecast wheel'/'THS', '373514').

card_in_set('flamespeaker adept', 'THS').
card_original_type('flamespeaker adept'/'THS', 'Creature — Human Shaman').
card_original_text('flamespeaker adept'/'THS', 'Whenever you scry, Flamespeaker Adept gets +2/+0 and gains first strike until end of turn.').
card_first_print('flamespeaker adept', 'THS').
card_image_name('flamespeaker adept'/'THS', 'flamespeaker adept').
card_uid('flamespeaker adept'/'THS', 'THS:Flamespeaker Adept:flamespeaker adept').
card_rarity('flamespeaker adept'/'THS', 'Uncommon').
card_artist('flamespeaker adept'/'THS', 'Lucas Graciano').
card_number('flamespeaker adept'/'THS', '123').
card_flavor_text('flamespeaker adept'/'THS', '\"I see your future, mantled in ash.\"').
card_multiverse_id('flamespeaker adept'/'THS', '373705').

card_in_set('fleecemane lion', 'THS').
card_original_type('fleecemane lion'/'THS', 'Creature — Cat').
card_original_text('fleecemane lion'/'THS', '{3}{G}{W}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nAs long as Fleecemane Lion is monstrous, it has hexproof and indestructible.').
card_first_print('fleecemane lion', 'THS').
card_image_name('fleecemane lion'/'THS', 'fleecemane lion').
card_uid('fleecemane lion'/'THS', 'THS:Fleecemane Lion:fleecemane lion').
card_rarity('fleecemane lion'/'THS', 'Rare').
card_artist('fleecemane lion'/'THS', 'Slawomir Maniak').
card_number('fleecemane lion'/'THS', '193').
card_multiverse_id('fleecemane lion'/'THS', '373562').

card_in_set('fleetfeather sandals', 'THS').
card_original_type('fleetfeather sandals'/'THS', 'Artifact — Equipment').
card_original_text('fleetfeather sandals'/'THS', 'Equipped creature has flying and haste.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('fleetfeather sandals', 'THS').
card_image_name('fleetfeather sandals'/'THS', 'fleetfeather sandals').
card_uid('fleetfeather sandals'/'THS', 'THS:Fleetfeather Sandals:fleetfeather sandals').
card_rarity('fleetfeather sandals'/'THS', 'Common').
card_artist('fleetfeather sandals'/'THS', 'Steve Prescott').
card_number('fleetfeather sandals'/'THS', '216').
card_flavor_text('fleetfeather sandals'/'THS', '\"The gods gave us no wings to fly, but they gave us an even greater gift: imagination.\"\n—Daxos of Meletis').
card_multiverse_id('fleetfeather sandals'/'THS', '373501').

card_in_set('fleshmad steed', 'THS').
card_original_type('fleshmad steed'/'THS', 'Creature — Horse').
card_original_text('fleshmad steed'/'THS', 'Whenever another creature dies, tap Fleshmad Steed.').
card_first_print('fleshmad steed', 'THS').
card_image_name('fleshmad steed'/'THS', 'fleshmad steed').
card_uid('fleshmad steed'/'THS', 'THS:Fleshmad Steed:fleshmad steed').
card_rarity('fleshmad steed'/'THS', 'Common').
card_artist('fleshmad steed'/'THS', 'Robbie Trevino').
card_number('fleshmad steed'/'THS', '88').
card_flavor_text('fleshmad steed'/'THS', 'More disturbing than the unknown is a distortion of the familiar.').
card_multiverse_id('fleshmad steed'/'THS', '373528').

card_in_set('forest', 'THS').
card_original_type('forest'/'THS', 'Basic Land — Forest').
card_original_text('forest'/'THS', 'G').
card_image_name('forest'/'THS', 'forest1').
card_uid('forest'/'THS', 'THS:Forest:forest1').
card_rarity('forest'/'THS', 'Basic Land').
card_artist('forest'/'THS', 'Rob Alexander').
card_number('forest'/'THS', '246').
card_multiverse_id('forest'/'THS', '373568').

card_in_set('forest', 'THS').
card_original_type('forest'/'THS', 'Basic Land — Forest').
card_original_text('forest'/'THS', 'G').
card_image_name('forest'/'THS', 'forest2').
card_uid('forest'/'THS', 'THS:Forest:forest2').
card_rarity('forest'/'THS', 'Basic Land').
card_artist('forest'/'THS', 'Steven Belledin').
card_number('forest'/'THS', '247').
card_multiverse_id('forest'/'THS', '373625').

card_in_set('forest', 'THS').
card_original_type('forest'/'THS', 'Basic Land — Forest').
card_original_text('forest'/'THS', 'G').
card_image_name('forest'/'THS', 'forest3').
card_uid('forest'/'THS', 'THS:Forest:forest3').
card_rarity('forest'/'THS', 'Basic Land').
card_artist('forest'/'THS', 'Adam Paquette').
card_number('forest'/'THS', '248').
card_multiverse_id('forest'/'THS', '373615').

card_in_set('forest', 'THS').
card_original_type('forest'/'THS', 'Basic Land — Forest').
card_original_text('forest'/'THS', 'G').
card_image_name('forest'/'THS', 'forest4').
card_uid('forest'/'THS', 'THS:Forest:forest4').
card_rarity('forest'/'THS', 'Basic Land').
card_artist('forest'/'THS', 'Raoul Vitale').
card_number('forest'/'THS', '249').
card_multiverse_id('forest'/'THS', '373688').

card_in_set('gainsay', 'THS').
card_original_type('gainsay'/'THS', 'Instant').
card_original_text('gainsay'/'THS', 'Counter target blue spell.').
card_image_name('gainsay'/'THS', 'gainsay').
card_uid('gainsay'/'THS', 'THS:Gainsay:gainsay').
card_rarity('gainsay'/'THS', 'Uncommon').
card_artist('gainsay'/'THS', 'Clint Cearley').
card_number('gainsay'/'THS', '49').
card_flavor_text('gainsay'/'THS', '\"You dryfolk must take the air for granted. You constantly insist on wasting it.\"\n—Kenessos, priest of Thassa').
card_multiverse_id('gainsay'/'THS', '373682').

card_in_set('gift of immortality', 'THS').
card_original_type('gift of immortality'/'THS', 'Enchantment — Aura').
card_original_text('gift of immortality'/'THS', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under its owner\'s control. Return Gift of Immortality to the battlefield attached to that creature at the beginning of the next end step.').
card_first_print('gift of immortality', 'THS').
card_image_name('gift of immortality'/'THS', 'gift of immortality').
card_uid('gift of immortality'/'THS', 'THS:Gift of Immortality:gift of immortality').
card_rarity('gift of immortality'/'THS', 'Rare').
card_artist('gift of immortality'/'THS', 'Matt Stewart').
card_number('gift of immortality'/'THS', '14').
card_multiverse_id('gift of immortality'/'THS', '373566').

card_in_set('glare of heresy', 'THS').
card_original_type('glare of heresy'/'THS', 'Sorcery').
card_original_text('glare of heresy'/'THS', 'Exile target white permanent.').
card_first_print('glare of heresy', 'THS').
card_image_name('glare of heresy'/'THS', 'glare of heresy').
card_uid('glare of heresy'/'THS', 'THS:Glare of Heresy:glare of heresy').
card_rarity('glare of heresy'/'THS', 'Uncommon').
card_artist('glare of heresy'/'THS', 'Raymond Swanland').
card_number('glare of heresy'/'THS', '15').
card_flavor_text('glare of heresy'/'THS', 'No foe is more hated than the former friend.').
card_multiverse_id('glare of heresy'/'THS', '373691').

card_in_set('gods willing', 'THS').
card_original_type('gods willing'/'THS', 'Instant').
card_original_text('gods willing'/'THS', 'Target creature you control gains protection from the color of your choice until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('gods willing', 'THS').
card_image_name('gods willing'/'THS', 'gods willing').
card_uid('gods willing'/'THS', 'THS:Gods Willing:gods willing').
card_rarity('gods willing'/'THS', 'Common').
card_artist('gods willing'/'THS', 'Mark Winters').
card_number('gods willing'/'THS', '16').
card_flavor_text('gods willing'/'THS', 'Honor the gods of Theros, and they will return the favor.').
card_multiverse_id('gods willing'/'THS', '373516').

card_in_set('gray merchant of asphodel', 'THS').
card_original_type('gray merchant of asphodel'/'THS', 'Creature — Zombie').
card_original_text('gray merchant of asphodel'/'THS', 'When Gray Merchant of Asphodel enters the battlefield, each opponent loses X life, where X is your devotion to black. You gain life equal to the life lost this way. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_first_print('gray merchant of asphodel', 'THS').
card_image_name('gray merchant of asphodel'/'THS', 'gray merchant of asphodel').
card_uid('gray merchant of asphodel'/'THS', 'THS:Gray Merchant of Asphodel:gray merchant of asphodel').
card_rarity('gray merchant of asphodel'/'THS', 'Common').
card_artist('gray merchant of asphodel'/'THS', 'Robbie Trevino').
card_number('gray merchant of asphodel'/'THS', '89').
card_multiverse_id('gray merchant of asphodel'/'THS', '373645').

card_in_set('griptide', 'THS').
card_original_type('griptide'/'THS', 'Instant').
card_original_text('griptide'/'THS', 'Put target creature on top of its owner\'s library.').
card_image_name('griptide'/'THS', 'griptide').
card_uid('griptide'/'THS', 'THS:Griptide:griptide').
card_rarity('griptide'/'THS', 'Common').
card_artist('griptide'/'THS', 'Adam Paquette').
card_number('griptide'/'THS', '50').
card_flavor_text('griptide'/'THS', 'When the sea god Thassa wishes to speak with a mortal, she does not issue a summons or grant a vision requesting a visit. The sea simply brings her guest before her.').
card_multiverse_id('griptide'/'THS', '373519').

card_in_set('guardians of meletis', 'THS').
card_original_type('guardians of meletis'/'THS', 'Artifact Creature — Golem').
card_original_text('guardians of meletis'/'THS', 'Defender').
card_first_print('guardians of meletis', 'THS').
card_image_name('guardians of meletis'/'THS', 'guardians of meletis').
card_uid('guardians of meletis'/'THS', 'THS:Guardians of Meletis:guardians of meletis').
card_rarity('guardians of meletis'/'THS', 'Common').
card_artist('guardians of meletis'/'THS', 'Magali Villeneuve').
card_number('guardians of meletis'/'THS', '217').
card_flavor_text('guardians of meletis'/'THS', 'The histories speak of two feuding rulers whose deaths were celebrated and whose monuments symbolized the end of their wars. In truth they were peaceful lovers, their story lost to the ages.').
card_multiverse_id('guardians of meletis'/'THS', '373605').

card_in_set('hammer of purphoros', 'THS').
card_original_type('hammer of purphoros'/'THS', 'Legendary Enchantment Artifact').
card_original_text('hammer of purphoros'/'THS', 'Creatures you control have haste.\n{2}{R}, {T}, Sacrifice a land: Put a 3/3 colorless Golem enchantment artifact creature token onto the battlefield.').
card_first_print('hammer of purphoros', 'THS').
card_image_name('hammer of purphoros'/'THS', 'hammer of purphoros').
card_uid('hammer of purphoros'/'THS', 'THS:Hammer of Purphoros:hammer of purphoros').
card_rarity('hammer of purphoros'/'THS', 'Rare').
card_artist('hammer of purphoros'/'THS', 'Yeong-Hao Han').
card_number('hammer of purphoros'/'THS', '124').
card_flavor_text('hammer of purphoros'/'THS', 'All the world is Purphoros\'s anvil.').
card_multiverse_id('hammer of purphoros'/'THS', '373587').

card_in_set('heliod\'s emissary', 'THS').
card_original_type('heliod\'s emissary'/'THS', 'Enchantment Creature — Elk').
card_original_text('heliod\'s emissary'/'THS', 'Bestow {6}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhenever Heliod\'s Emissary or enchanted creature attacks, tap target creature an opponent controls.\nEnchanted creature gets +3/+3.').
card_first_print('heliod\'s emissary', 'THS').
card_image_name('heliod\'s emissary'/'THS', 'heliod\'s emissary').
card_uid('heliod\'s emissary'/'THS', 'THS:Heliod\'s Emissary:heliod\'s emissary').
card_rarity('heliod\'s emissary'/'THS', 'Uncommon').
card_artist('heliod\'s emissary'/'THS', 'Sam Burley').
card_number('heliod\'s emissary'/'THS', '18').
card_multiverse_id('heliod\'s emissary'/'THS', '373729').

card_in_set('heliod, god of the sun', 'THS').
card_original_type('heliod, god of the sun'/'THS', 'Legendary Enchantment Creature — God').
card_original_text('heliod, god of the sun'/'THS', 'Indestructible\nAs long as your devotion to white is less than five, Heliod isn\'t a creature. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)\nOther creatures you control have vigilance.\n{2}{W}{W}: Put a 2/1 white Cleric enchantment creature token onto the battlefield.').
card_first_print('heliod, god of the sun', 'THS').
card_image_name('heliod, god of the sun'/'THS', 'heliod, god of the sun').
card_uid('heliod, god of the sun'/'THS', 'THS:Heliod, God of the Sun:heliod, god of the sun').
card_rarity('heliod, god of the sun'/'THS', 'Mythic Rare').
card_artist('heliod, god of the sun'/'THS', 'Jaime Jones').
card_number('heliod, god of the sun'/'THS', '17').
card_multiverse_id('heliod, god of the sun'/'THS', '373524').

card_in_set('hero\'s downfall', 'THS').
card_original_type('hero\'s downfall'/'THS', 'Instant').
card_original_text('hero\'s downfall'/'THS', 'Destroy target creature or planeswalker.').
card_first_print('hero\'s downfall', 'THS').
card_image_name('hero\'s downfall'/'THS', 'hero\'s downfall').
card_uid('hero\'s downfall'/'THS', 'THS:Hero\'s Downfall:hero\'s downfall').
card_rarity('hero\'s downfall'/'THS', 'Rare').
card_artist('hero\'s downfall'/'THS', 'Ryan Pancoast').
card_number('hero\'s downfall'/'THS', '90').
card_flavor_text('hero\'s downfall'/'THS', 'Destiny exalts a chosen few, but even heroes break.').
card_multiverse_id('hero\'s downfall'/'THS', '373575').

card_in_set('hopeful eidolon', 'THS').
card_original_type('hopeful eidolon'/'THS', 'Enchantment Creature — Spirit').
card_original_text('hopeful eidolon'/'THS', 'Bestow {3}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)\nEnchanted creature gets +1/+1 and has lifelink.').
card_first_print('hopeful eidolon', 'THS').
card_image_name('hopeful eidolon'/'THS', 'hopeful eidolon').
card_uid('hopeful eidolon'/'THS', 'THS:Hopeful Eidolon:hopeful eidolon').
card_rarity('hopeful eidolon'/'THS', 'Common').
card_artist('hopeful eidolon'/'THS', 'Min Yum').
card_number('hopeful eidolon'/'THS', '19').
card_multiverse_id('hopeful eidolon'/'THS', '373616').

card_in_set('horizon chimera', 'THS').
card_original_type('horizon chimera'/'THS', 'Creature — Chimera').
card_original_text('horizon chimera'/'THS', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying, trample\nWhenever you draw a card, you gain 1 life.').
card_first_print('horizon chimera', 'THS').
card_image_name('horizon chimera'/'THS', 'horizon chimera').
card_uid('horizon chimera'/'THS', 'THS:Horizon Chimera:horizon chimera').
card_rarity('horizon chimera'/'THS', 'Uncommon').
card_artist('horizon chimera'/'THS', 'Sam Burley').
card_number('horizon chimera'/'THS', '194').
card_multiverse_id('horizon chimera'/'THS', '373738').

card_in_set('horizon scholar', 'THS').
card_original_type('horizon scholar'/'THS', 'Creature — Sphinx').
card_original_text('horizon scholar'/'THS', 'Flying\nWhen Horizon Scholar enters the battlefield, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('horizon scholar', 'THS').
card_image_name('horizon scholar'/'THS', 'horizon scholar').
card_uid('horizon scholar'/'THS', 'THS:Horizon Scholar:horizon scholar').
card_rarity('horizon scholar'/'THS', 'Uncommon').
card_artist('horizon scholar'/'THS', 'Karl Kopinski').
card_number('horizon scholar'/'THS', '51').
card_multiverse_id('horizon scholar'/'THS', '373628').

card_in_set('hundred-handed one', 'THS').
card_original_type('hundred-handed one'/'THS', 'Creature — Giant').
card_original_text('hundred-handed one'/'THS', 'Vigilance \n{3}{W}{W}{W}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nAs long as Hundred-Handed One is monstrous, it has reach and can block an additional ninety-nine creatures each combat.').
card_first_print('hundred-handed one', 'THS').
card_image_name('hundred-handed one'/'THS', 'hundred-handed one').
card_uid('hundred-handed one'/'THS', 'THS:Hundred-Handed One:hundred-handed one').
card_rarity('hundred-handed one'/'THS', 'Rare').
card_artist('hundred-handed one'/'THS', 'Brad Rigney').
card_number('hundred-handed one'/'THS', '20').
card_multiverse_id('hundred-handed one'/'THS', '373708').

card_in_set('hunt the hunter', 'THS').
card_original_type('hunt the hunter'/'THS', 'Sorcery').
card_original_text('hunt the hunter'/'THS', 'Target green creature you control gets +2/+2 until end of turn. It fights target green creature an opponent controls.').
card_first_print('hunt the hunter', 'THS').
card_image_name('hunt the hunter'/'THS', 'hunt the hunter').
card_uid('hunt the hunter'/'THS', 'THS:Hunt the Hunter:hunt the hunter').
card_rarity('hunt the hunter'/'THS', 'Uncommon').
card_artist('hunt the hunter'/'THS', 'Ryan Barger').
card_number('hunt the hunter'/'THS', '159').
card_flavor_text('hunt the hunter'/'THS', '\"A grudge is a tattoo worn on the inside.\"\n—Setessan warrior saying').
card_multiverse_id('hunt the hunter'/'THS', '373668').

card_in_set('hythonia the cruel', 'THS').
card_original_type('hythonia the cruel'/'THS', 'Legendary Creature — Gorgon').
card_original_text('hythonia the cruel'/'THS', 'Deathtouch\n{6}{B}{B}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Hythonia the Cruel becomes monstrous, destroy all non-Gorgon creatures.').
card_first_print('hythonia the cruel', 'THS').
card_image_name('hythonia the cruel'/'THS', 'hythonia the cruel').
card_uid('hythonia the cruel'/'THS', 'THS:Hythonia the Cruel:hythonia the cruel').
card_rarity('hythonia the cruel'/'THS', 'Mythic Rare').
card_artist('hythonia the cruel'/'THS', 'Chris Rahn').
card_number('hythonia the cruel'/'THS', '91').
card_multiverse_id('hythonia the cruel'/'THS', '373673').

card_in_set('ill-tempered cyclops', 'THS').
card_original_type('ill-tempered cyclops'/'THS', 'Creature — Cyclops').
card_original_text('ill-tempered cyclops'/'THS', 'Trample\n{5}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_first_print('ill-tempered cyclops', 'THS').
card_image_name('ill-tempered cyclops'/'THS', 'ill-tempered cyclops').
card_uid('ill-tempered cyclops'/'THS', 'THS:Ill-Tempered Cyclops:ill-tempered cyclops').
card_rarity('ill-tempered cyclops'/'THS', 'Common').
card_artist('ill-tempered cyclops'/'THS', 'Peter Mohrbacher').
card_number('ill-tempered cyclops'/'THS', '125').
card_flavor_text('ill-tempered cyclops'/'THS', 'A cyclops has two moods: angry and asleep.').
card_multiverse_id('ill-tempered cyclops'/'THS', '373545').

card_in_set('insatiable harpy', 'THS').
card_original_type('insatiable harpy'/'THS', 'Creature — Harpy').
card_original_text('insatiable harpy'/'THS', 'Flying, lifelink').
card_first_print('insatiable harpy', 'THS').
card_image_name('insatiable harpy'/'THS', 'insatiable harpy').
card_uid('insatiable harpy'/'THS', 'THS:Insatiable Harpy:insatiable harpy').
card_rarity('insatiable harpy'/'THS', 'Uncommon').
card_artist('insatiable harpy'/'THS', 'Matt Stewart').
card_number('insatiable harpy'/'THS', '92').
card_flavor_text('insatiable harpy'/'THS', 'Gold coin, battered helmet, broken wrist bone—all have the same value in the eyes of a harpy.').
card_multiverse_id('insatiable harpy'/'THS', '373670').

card_in_set('island', 'THS').
card_original_type('island'/'THS', 'Basic Land — Island').
card_original_text('island'/'THS', 'U').
card_image_name('island'/'THS', 'island1').
card_uid('island'/'THS', 'THS:Island:island1').
card_rarity('island'/'THS', 'Basic Land').
card_artist('island'/'THS', 'Rob Alexander').
card_number('island'/'THS', '234').
card_multiverse_id('island'/'THS', '373558').

card_in_set('island', 'THS').
card_original_type('island'/'THS', 'Basic Land — Island').
card_original_text('island'/'THS', 'U').
card_image_name('island'/'THS', 'island2').
card_uid('island'/'THS', 'THS:Island:island2').
card_rarity('island'/'THS', 'Basic Land').
card_artist('island'/'THS', 'Steven Belledin').
card_number('island'/'THS', '235').
card_multiverse_id('island'/'THS', '373595').

card_in_set('island', 'THS').
card_original_type('island'/'THS', 'Basic Land — Island').
card_original_text('island'/'THS', 'U').
card_image_name('island'/'THS', 'island3').
card_uid('island'/'THS', 'THS:Island:island3').
card_rarity('island'/'THS', 'Basic Land').
card_artist('island'/'THS', 'Adam Paquette').
card_number('island'/'THS', '236').
card_multiverse_id('island'/'THS', '373723').

card_in_set('island', 'THS').
card_original_type('island'/'THS', 'Basic Land — Island').
card_original_text('island'/'THS', 'U').
card_image_name('island'/'THS', 'island4').
card_uid('island'/'THS', 'THS:Island:island4').
card_rarity('island'/'THS', 'Basic Land').
card_artist('island'/'THS', 'Raoul Vitale').
card_number('island'/'THS', '237').
card_multiverse_id('island'/'THS', '373736').

card_in_set('karametra\'s acolyte', 'THS').
card_original_type('karametra\'s acolyte'/'THS', 'Creature — Human Druid').
card_original_text('karametra\'s acolyte'/'THS', '{T}: Add an amount of {G} to your mana pool equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_image_name('karametra\'s acolyte'/'THS', 'karametra\'s acolyte').
card_uid('karametra\'s acolyte'/'THS', 'THS:Karametra\'s Acolyte:karametra\'s acolyte').
card_rarity('karametra\'s acolyte'/'THS', 'Uncommon').
card_artist('karametra\'s acolyte'/'THS', 'Chase Stone').
card_number('karametra\'s acolyte'/'THS', '160').
card_flavor_text('karametra\'s acolyte'/'THS', '\"The wilds are a garden tended by divine hands.\"').
card_multiverse_id('karametra\'s acolyte'/'THS', '373538').

card_in_set('keepsake gorgon', 'THS').
card_original_type('keepsake gorgon'/'THS', 'Creature — Gorgon').
card_original_text('keepsake gorgon'/'THS', 'Deathtouch\n{5}{B}{B}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nWhen Keepsake Gorgon becomes monstrous, destroy target non-Gorgon creature an opponent controls.').
card_first_print('keepsake gorgon', 'THS').
card_image_name('keepsake gorgon'/'THS', 'keepsake gorgon').
card_uid('keepsake gorgon'/'THS', 'THS:Keepsake Gorgon:keepsake gorgon').
card_rarity('keepsake gorgon'/'THS', 'Uncommon').
card_artist('keepsake gorgon'/'THS', 'Aaron Miller').
card_number('keepsake gorgon'/'THS', '93').
card_multiverse_id('keepsake gorgon'/'THS', '373548').

card_in_set('kragma warcaller', 'THS').
card_original_type('kragma warcaller'/'THS', 'Creature — Minotaur Warrior').
card_original_text('kragma warcaller'/'THS', 'Minotaur creatures you control have haste.\nWhenever a Minotaur you control attacks, it gets +2/+0 until end of turn.').
card_first_print('kragma warcaller', 'THS').
card_image_name('kragma warcaller'/'THS', 'kragma warcaller').
card_uid('kragma warcaller'/'THS', 'THS:Kragma Warcaller:kragma warcaller').
card_rarity('kragma warcaller'/'THS', 'Uncommon').
card_artist('kragma warcaller'/'THS', 'Gabor Szikszai').
card_number('kragma warcaller'/'THS', '195').
card_flavor_text('kragma warcaller'/'THS', 'A warcaller merely brings the herd together. After that, the meat-hunger is all the encouragement they need.').
card_multiverse_id('kragma warcaller'/'THS', '373710').

card_in_set('labyrinth champion', 'THS').
card_original_type('labyrinth champion'/'THS', 'Creature — Human Warrior').
card_original_text('labyrinth champion'/'THS', 'Heroic — Whenever you cast a spell that targets Labyrinth Champion, Labyrinth Champion deals 2 damage to target creature or player.').
card_first_print('labyrinth champion', 'THS').
card_image_name('labyrinth champion'/'THS', 'labyrinth champion').
card_uid('labyrinth champion'/'THS', 'THS:Labyrinth Champion:labyrinth champion').
card_rarity('labyrinth champion'/'THS', 'Rare').
card_artist('labyrinth champion'/'THS', 'Chase Stone').
card_number('labyrinth champion'/'THS', '126').
card_flavor_text('labyrinth champion'/'THS', '\"It used to be a lair. Now it\'s just a tunnel.\"').
card_multiverse_id('labyrinth champion'/'THS', '373586').

card_in_set('lagonna-band elder', 'THS').
card_original_type('lagonna-band elder'/'THS', 'Creature — Centaur Advisor').
card_original_text('lagonna-band elder'/'THS', 'When Lagonna-Band Elder enters the battlefield, if you control an enchantment, you gain 3 life.').
card_first_print('lagonna-band elder', 'THS').
card_image_name('lagonna-band elder'/'THS', 'lagonna-band elder').
card_uid('lagonna-band elder'/'THS', 'THS:Lagonna-Band Elder:lagonna-band elder').
card_rarity('lagonna-band elder'/'THS', 'Common').
card_artist('lagonna-band elder'/'THS', 'Min Yum').
card_number('lagonna-band elder'/'THS', '21').
card_flavor_text('lagonna-band elder'/'THS', '\"The best lessons are not the ones I teach. They are the ones the pupils realize for themselves.\"').
card_multiverse_id('lagonna-band elder'/'THS', '373599').

card_in_set('lash of the whip', 'THS').
card_original_type('lash of the whip'/'THS', 'Instant').
card_original_text('lash of the whip'/'THS', 'Target creature gets -4/-4 until end of turn.').
card_first_print('lash of the whip', 'THS').
card_image_name('lash of the whip'/'THS', 'lash of the whip').
card_uid('lash of the whip'/'THS', 'THS:Lash of the Whip:lash of the whip').
card_rarity('lash of the whip'/'THS', 'Common').
card_artist('lash of the whip'/'THS', 'Dan Scott').
card_number('lash of the whip'/'THS', '94').
card_flavor_text('lash of the whip'/'THS', '\"No matter who their fickle hearts worship, all mortals belong to one god in the end.\"\n—Iadorna, death priest of Erebos').
card_multiverse_id('lash of the whip'/'THS', '373610').

card_in_set('last breath', 'THS').
card_original_type('last breath'/'THS', 'Instant').
card_original_text('last breath'/'THS', 'Exile target creature with power 2 or less. Its controller gains 4 life.').
card_image_name('last breath'/'THS', 'last breath').
card_uid('last breath'/'THS', 'THS:Last Breath:last breath').
card_rarity('last breath'/'THS', 'Common').
card_artist('last breath'/'THS', 'Nils Hamm').
card_number('last breath'/'THS', '22').
card_flavor_text('last breath'/'THS', 'In time, all things turn to dust. Some things just take less time.').
card_multiverse_id('last breath'/'THS', '373680').

card_in_set('leafcrown dryad', 'THS').
card_original_type('leafcrown dryad'/'THS', 'Enchantment Creature — Nymph Dryad').
card_original_text('leafcrown dryad'/'THS', 'Bestow {3}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nReach\nEnchanted creature gets +2/+2 and has reach.').
card_first_print('leafcrown dryad', 'THS').
card_image_name('leafcrown dryad'/'THS', 'leafcrown dryad').
card_uid('leafcrown dryad'/'THS', 'THS:Leafcrown Dryad:leafcrown dryad').
card_rarity('leafcrown dryad'/'THS', 'Common').
card_artist('leafcrown dryad'/'THS', 'Volkan Baga').
card_number('leafcrown dryad'/'THS', '161').
card_multiverse_id('leafcrown dryad'/'THS', '373523').

card_in_set('leonin snarecaster', 'THS').
card_original_type('leonin snarecaster'/'THS', 'Creature — Cat Soldier').
card_original_text('leonin snarecaster'/'THS', 'When Leonin Snarecaster enters the battlefield, you may tap target creature.').
card_first_print('leonin snarecaster', 'THS').
card_image_name('leonin snarecaster'/'THS', 'leonin snarecaster').
card_uid('leonin snarecaster'/'THS', 'THS:Leonin Snarecaster:leonin snarecaster').
card_rarity('leonin snarecaster'/'THS', 'Common').
card_artist('leonin snarecaster'/'THS', 'Kev Walker').
card_number('leonin snarecaster'/'THS', '23').
card_flavor_text('leonin snarecaster'/'THS', 'Formerly oppressed by the polis of Meletis, leonin occasionally \"mistake\" their old enemies for game.').
card_multiverse_id('leonin snarecaster'/'THS', '373600').

card_in_set('lightning strike', 'THS').
card_original_type('lightning strike'/'THS', 'Instant').
card_original_text('lightning strike'/'THS', 'Lightning Strike deals 3 damage to target creature or player.').
card_first_print('lightning strike', 'THS').
card_image_name('lightning strike'/'THS', 'lightning strike').
card_uid('lightning strike'/'THS', 'THS:Lightning Strike:lightning strike').
card_rarity('lightning strike'/'THS', 'Common').
card_artist('lightning strike'/'THS', 'Adam Paquette').
card_number('lightning strike'/'THS', '127').
card_flavor_text('lightning strike'/'THS', '\"The hand of Keranos can be seen in every rumbling storm cloud. Best not to stand where he points.\"\n—Rakleia of Shrine Peak').
card_multiverse_id('lightning strike'/'THS', '373651').

card_in_set('loathsome catoblepas', 'THS').
card_original_type('loathsome catoblepas'/'THS', 'Creature — Beast').
card_original_text('loathsome catoblepas'/'THS', '{2}{G}: Loathsome Catoblepas must be blocked this turn if able.\nWhen Loathsome Catoblepas dies, target creature an opponent controls gets -3/-3 until end of turn.').
card_first_print('loathsome catoblepas', 'THS').
card_image_name('loathsome catoblepas'/'THS', 'loathsome catoblepas').
card_uid('loathsome catoblepas'/'THS', 'THS:Loathsome Catoblepas:loathsome catoblepas').
card_rarity('loathsome catoblepas'/'THS', 'Common').
card_artist('loathsome catoblepas'/'THS', 'Christopher Burdett').
card_number('loathsome catoblepas'/'THS', '95').
card_multiverse_id('loathsome catoblepas'/'THS', '373731').

card_in_set('lost in a labyrinth', 'THS').
card_original_type('lost in a labyrinth'/'THS', 'Instant').
card_original_text('lost in a labyrinth'/'THS', 'Target creature gets -3/-0 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('lost in a labyrinth', 'THS').
card_image_name('lost in a labyrinth'/'THS', 'lost in a labyrinth').
card_uid('lost in a labyrinth'/'THS', 'THS:Lost in a Labyrinth:lost in a labyrinth').
card_rarity('lost in a labyrinth'/'THS', 'Common').
card_artist('lost in a labyrinth'/'THS', 'Winona Nelson').
card_number('lost in a labyrinth'/'THS', '52').
card_flavor_text('lost in a labyrinth'/'THS', 'Even those who leave the labyrinth never escape it, forever dreaming of their time trapped within.').
card_multiverse_id('lost in a labyrinth'/'THS', '373629').

card_in_set('magma jet', 'THS').
card_original_type('magma jet'/'THS', 'Instant').
card_original_text('magma jet'/'THS', 'Magma Jet deals 2 damage to target creature or player. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('magma jet'/'THS', 'magma jet').
card_uid('magma jet'/'THS', 'THS:Magma Jet:magma jet').
card_rarity('magma jet'/'THS', 'Uncommon').
card_artist('magma jet'/'THS', 'Maciej Kuciara').
card_number('magma jet'/'THS', '128').
card_multiverse_id('magma jet'/'THS', '373704').

card_in_set('march of the returned', 'THS').
card_original_type('march of the returned'/'THS', 'Sorcery').
card_original_text('march of the returned'/'THS', 'Return up to two target creature cards from your graveyard to your hand.').
card_first_print('march of the returned', 'THS').
card_image_name('march of the returned'/'THS', 'march of the returned').
card_uid('march of the returned'/'THS', 'THS:March of the Returned:march of the returned').
card_rarity('march of the returned'/'THS', 'Common').
card_artist('march of the returned'/'THS', 'Mark Zug').
card_number('march of the returned'/'THS', '96').
card_flavor_text('march of the returned'/'THS', 'The Returned have no memory of the Underworld or of their former lives. The golden masks they wear are the last gifts of the selves they left behind.').
card_multiverse_id('march of the returned'/'THS', '373579').

card_in_set('master of waves', 'THS').
card_original_type('master of waves'/'THS', 'Creature — Merfolk Wizard').
card_original_text('master of waves'/'THS', 'Protection from red\nElemental creatures you control get +1/+1.\nWhen Master of Waves enters the battlefield, put a number of 1/0 blue Elemental creature tokens onto the battlefield equal to your devotion to blue. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)').
card_first_print('master of waves', 'THS').
card_image_name('master of waves'/'THS', 'master of waves').
card_uid('master of waves'/'THS', 'THS:Master of Waves:master of waves').
card_rarity('master of waves'/'THS', 'Mythic Rare').
card_artist('master of waves'/'THS', 'Karl Kopinski').
card_number('master of waves'/'THS', '53').
card_multiverse_id('master of waves'/'THS', '373536').

card_in_set('medomai the ageless', 'THS').
card_original_type('medomai the ageless'/'THS', 'Legendary Creature — Sphinx').
card_original_text('medomai the ageless'/'THS', 'Flying\nWhenever Medomai the Ageless deals combat damage to a player, take an extra turn after this one.\nMedomai the Ageless can\'t attack during extra turns.').
card_first_print('medomai the ageless', 'THS').
card_image_name('medomai the ageless'/'THS', 'medomai the ageless').
card_uid('medomai the ageless'/'THS', 'THS:Medomai the Ageless:medomai the ageless').
card_rarity('medomai the ageless'/'THS', 'Mythic Rare').
card_artist('medomai the ageless'/'THS', 'David Palumbo').
card_number('medomai the ageless'/'THS', '196').
card_multiverse_id('medomai the ageless'/'THS', '373675').

card_in_set('meletis charlatan', 'THS').
card_original_type('meletis charlatan'/'THS', 'Creature — Human Wizard').
card_original_text('meletis charlatan'/'THS', '{2}{U}, {T}: The controller of target instant or sorcery spell copies it. That player may choose new targets for the copy.').
card_first_print('meletis charlatan', 'THS').
card_image_name('meletis charlatan'/'THS', 'meletis charlatan').
card_uid('meletis charlatan'/'THS', 'THS:Meletis Charlatan:meletis charlatan').
card_rarity('meletis charlatan'/'THS', 'Rare').
card_artist('meletis charlatan'/'THS', 'Jason A. Engle').
card_number('meletis charlatan'/'THS', '54').
card_flavor_text('meletis charlatan'/'THS', '\"Every object has an echo in the Æther beyond the world. Every idea has a shadow that can be brought to light.\"').
card_multiverse_id('meletis charlatan'/'THS', '373741').

card_in_set('messenger\'s speed', 'THS').
card_original_type('messenger\'s speed'/'THS', 'Enchantment — Aura').
card_original_text('messenger\'s speed'/'THS', 'Enchant creature\nEnchanted creature has trample and haste.').
card_first_print('messenger\'s speed', 'THS').
card_image_name('messenger\'s speed'/'THS', 'messenger\'s speed').
card_uid('messenger\'s speed'/'THS', 'THS:Messenger\'s Speed:messenger\'s speed').
card_rarity('messenger\'s speed'/'THS', 'Common').
card_artist('messenger\'s speed'/'THS', 'Clint Cearley').
card_number('messenger\'s speed'/'THS', '129').
card_flavor_text('messenger\'s speed'/'THS', '\"He outran arrows. He outran even the archers\' insults.\"\n—Bayma, storyteller of Lagonna Band').
card_multiverse_id('messenger\'s speed'/'THS', '373699').

card_in_set('minotaur skullcleaver', 'THS').
card_original_type('minotaur skullcleaver'/'THS', 'Creature — Minotaur Berserker').
card_original_text('minotaur skullcleaver'/'THS', 'Haste\nWhen Minotaur Skullcleaver enters the battlefield, it gets +2/+0 until end of turn.').
card_first_print('minotaur skullcleaver', 'THS').
card_image_name('minotaur skullcleaver'/'THS', 'minotaur skullcleaver').
card_uid('minotaur skullcleaver'/'THS', 'THS:Minotaur Skullcleaver:minotaur skullcleaver').
card_rarity('minotaur skullcleaver'/'THS', 'Common').
card_artist('minotaur skullcleaver'/'THS', 'Phill Simmer').
card_number('minotaur skullcleaver'/'THS', '130').
card_flavor_text('minotaur skullcleaver'/'THS', '\"Their only dreams are of full stomachs.\"\n—Kleon the Iron-Booted').
card_multiverse_id('minotaur skullcleaver'/'THS', '373698').

card_in_set('mistcutter hydra', 'THS').
card_original_type('mistcutter hydra'/'THS', 'Creature — Hydra').
card_original_text('mistcutter hydra'/'THS', 'Mistcutter Hydra can\'t be countered.\nHaste, protection from blue\nMistcutter Hydra enters the battlefield with X +1/+1 counters on it.').
card_first_print('mistcutter hydra', 'THS').
card_image_name('mistcutter hydra'/'THS', 'mistcutter hydra').
card_uid('mistcutter hydra'/'THS', 'THS:Mistcutter Hydra:mistcutter hydra').
card_rarity('mistcutter hydra'/'THS', 'Rare').
card_artist('mistcutter hydra'/'THS', 'Ryan Pancoast').
card_number('mistcutter hydra'/'THS', '162').
card_multiverse_id('mistcutter hydra'/'THS', '373727').

card_in_set('mnemonic wall', 'THS').
card_original_type('mnemonic wall'/'THS', 'Creature — Wall').
card_original_text('mnemonic wall'/'THS', 'Defender\nWhen Mnemonic Wall enters the battlefield, you may return target instant or sorcery card from your graveyard to your hand.').
card_image_name('mnemonic wall'/'THS', 'mnemonic wall').
card_uid('mnemonic wall'/'THS', 'THS:Mnemonic Wall:mnemonic wall').
card_rarity('mnemonic wall'/'THS', 'Common').
card_artist('mnemonic wall'/'THS', 'Trevor Claxton').
card_number('mnemonic wall'/'THS', '55').
card_flavor_text('mnemonic wall'/'THS', '\"It augments anamnesis. What is so confusing about that?\"\n—Perisophia the philosopher').
card_multiverse_id('mnemonic wall'/'THS', '373658').

card_in_set('mogis\'s marauder', 'THS').
card_original_type('mogis\'s marauder'/'THS', 'Creature — Human Berserker').
card_original_text('mogis\'s marauder'/'THS', 'When Mogis\'s Marauder enters the battlefield, up to X target creatures each gain intimidate and haste until end of turn, where X is your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_first_print('mogis\'s marauder', 'THS').
card_image_name('mogis\'s marauder'/'THS', 'mogis\'s marauder').
card_uid('mogis\'s marauder'/'THS', 'THS:Mogis\'s Marauder:mogis\'s marauder').
card_rarity('mogis\'s marauder'/'THS', 'Uncommon').
card_artist('mogis\'s marauder'/'THS', 'Chase Stone').
card_number('mogis\'s marauder'/'THS', '97').
card_multiverse_id('mogis\'s marauder'/'THS', '373565').

card_in_set('mountain', 'THS').
card_original_type('mountain'/'THS', 'Basic Land — Mountain').
card_original_text('mountain'/'THS', 'R').
card_image_name('mountain'/'THS', 'mountain1').
card_uid('mountain'/'THS', 'THS:Mountain:mountain1').
card_rarity('mountain'/'THS', 'Basic Land').
card_artist('mountain'/'THS', 'Rob Alexander').
card_number('mountain'/'THS', '242').
card_multiverse_id('mountain'/'THS', '373609').

card_in_set('mountain', 'THS').
card_original_type('mountain'/'THS', 'Basic Land — Mountain').
card_original_text('mountain'/'THS', 'R').
card_image_name('mountain'/'THS', 'mountain2').
card_uid('mountain'/'THS', 'THS:Mountain:mountain2').
card_rarity('mountain'/'THS', 'Basic Land').
card_artist('mountain'/'THS', 'Steven Belledin').
card_number('mountain'/'THS', '243').
card_multiverse_id('mountain'/'THS', '373746').

card_in_set('mountain', 'THS').
card_original_type('mountain'/'THS', 'Basic Land — Mountain').
card_original_text('mountain'/'THS', 'R').
card_image_name('mountain'/'THS', 'mountain3').
card_uid('mountain'/'THS', 'THS:Mountain:mountain3').
card_rarity('mountain'/'THS', 'Basic Land').
card_artist('mountain'/'THS', 'Adam Paquette').
card_number('mountain'/'THS', '244').
card_multiverse_id('mountain'/'THS', '373683').

card_in_set('mountain', 'THS').
card_original_type('mountain'/'THS', 'Basic Land — Mountain').
card_original_text('mountain'/'THS', 'R').
card_image_name('mountain'/'THS', 'mountain4').
card_uid('mountain'/'THS', 'THS:Mountain:mountain4').
card_rarity('mountain'/'THS', 'Basic Land').
card_artist('mountain'/'THS', 'Raoul Vitale').
card_number('mountain'/'THS', '245').
card_multiverse_id('mountain'/'THS', '373546').

card_in_set('nemesis of mortals', 'THS').
card_original_type('nemesis of mortals'/'THS', 'Creature — Snake').
card_original_text('nemesis of mortals'/'THS', 'Nemesis of Mortals costs {1} less to cast for each creature card in your graveyard.\n{7}{G}{G}: Monstrosity 5. This ability costs {1} less to activate for each creature card in your graveyard. (If this creature isn\'t monstrous, put five +1/+1 counters on it and it becomes monstrous.)').
card_first_print('nemesis of mortals', 'THS').
card_image_name('nemesis of mortals'/'THS', 'nemesis of mortals').
card_uid('nemesis of mortals'/'THS', 'THS:Nemesis of Mortals:nemesis of mortals').
card_rarity('nemesis of mortals'/'THS', 'Uncommon').
card_artist('nemesis of mortals'/'THS', 'Mathias Kollros').
card_number('nemesis of mortals'/'THS', '163').
card_multiverse_id('nemesis of mortals'/'THS', '373694').

card_in_set('nessian asp', 'THS').
card_original_type('nessian asp'/'THS', 'Creature — Snake').
card_original_text('nessian asp'/'THS', 'Reach\n{6}{G}: Monstrosity 4. (If this creature isn\'t monstrous, put four +1/+1 counters on it and it becomes monstrous.)').
card_first_print('nessian asp', 'THS').
card_image_name('nessian asp'/'THS', 'nessian asp').
card_uid('nessian asp'/'THS', 'THS:Nessian Asp:nessian asp').
card_rarity('nessian asp'/'THS', 'Common').
card_artist('nessian asp'/'THS', 'Alex Horley-Orlandelli').
card_number('nessian asp'/'THS', '164').
card_flavor_text('nessian asp'/'THS', 'It\'s not the two heads you should fear. It\'s the four fangs.').
card_multiverse_id('nessian asp'/'THS', '373650').

card_in_set('nessian courser', 'THS').
card_original_type('nessian courser'/'THS', 'Creature — Centaur Warrior').
card_original_text('nessian courser'/'THS', '').
card_image_name('nessian courser'/'THS', 'nessian courser').
card_uid('nessian courser'/'THS', 'THS:Nessian Courser:nessian courser').
card_rarity('nessian courser'/'THS', 'Common').
card_artist('nessian courser'/'THS', 'Steve Prescott').
card_number('nessian courser'/'THS', '165').
card_flavor_text('nessian courser'/'THS', 'Khestes the Adamant, the Champion\'s closest ally among the centaurs, took one stone to his shoulder and another to his flank. He held his stride and his aim, and let fly the arrow that killed the giant Grinthax.\n—The Theriad').
card_multiverse_id('nessian courser'/'THS', '373581').

card_in_set('nighthowler', 'THS').
card_original_type('nighthowler'/'THS', 'Enchantment Creature — Horror').
card_original_text('nighthowler'/'THS', 'Bestow {2}{B}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nNighthowler and enchanted creature each get +X/+X, where X is the number of creature cards in all graveyards.').
card_image_name('nighthowler'/'THS', 'nighthowler').
card_uid('nighthowler'/'THS', 'THS:Nighthowler:nighthowler').
card_rarity('nighthowler'/'THS', 'Rare').
card_artist('nighthowler'/'THS', 'Nils Hamm').
card_number('nighthowler'/'THS', '98').
card_multiverse_id('nighthowler'/'THS', '373564').

card_in_set('nimbus naiad', 'THS').
card_original_type('nimbus naiad'/'THS', 'Enchantment Creature — Nymph').
card_original_text('nimbus naiad'/'THS', 'Bestow {4}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nEnchanted creature gets +2/+2 and has flying.').
card_first_print('nimbus naiad', 'THS').
card_image_name('nimbus naiad'/'THS', 'nimbus naiad').
card_uid('nimbus naiad'/'THS', 'THS:Nimbus Naiad:nimbus naiad').
card_rarity('nimbus naiad'/'THS', 'Common').
card_artist('nimbus naiad'/'THS', 'David Palumbo').
card_number('nimbus naiad'/'THS', '56').
card_multiverse_id('nimbus naiad'/'THS', '373719').

card_in_set('nykthos, shrine to nyx', 'THS').
card_original_type('nykthos, shrine to nyx'/'THS', 'Legendary Land').
card_original_text('nykthos, shrine to nyx'/'THS', '{T}: Add {1} to your mana pool.\n{2}, {T}: Choose a color. Add to your mana pool an amount of mana of that color equal to your devotion to that color. (Your devotion to a color is the number of mana symbols of that color in the mana costs of permanents you control.)').
card_first_print('nykthos, shrine to nyx', 'THS').
card_image_name('nykthos, shrine to nyx'/'THS', 'nykthos, shrine to nyx').
card_uid('nykthos, shrine to nyx'/'THS', 'THS:Nykthos, Shrine to Nyx:nykthos, shrine to nyx').
card_rarity('nykthos, shrine to nyx'/'THS', 'Rare').
card_artist('nykthos, shrine to nyx'/'THS', 'Jung Park').
card_number('nykthos, shrine to nyx'/'THS', '223').
card_multiverse_id('nykthos, shrine to nyx'/'THS', '373713').

card_in_set('nylea\'s disciple', 'THS').
card_original_type('nylea\'s disciple'/'THS', 'Creature — Centaur Archer').
card_original_text('nylea\'s disciple'/'THS', 'When Nylea\'s Disciple enters the battlefield, you gain life equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_first_print('nylea\'s disciple', 'THS').
card_image_name('nylea\'s disciple'/'THS', 'nylea\'s disciple').
card_uid('nylea\'s disciple'/'THS', 'THS:Nylea\'s Disciple:nylea\'s disciple').
card_rarity('nylea\'s disciple'/'THS', 'Common').
card_artist('nylea\'s disciple'/'THS', 'Trevor Claxton').
card_number('nylea\'s disciple'/'THS', '167').
card_multiverse_id('nylea\'s disciple'/'THS', '373498').

card_in_set('nylea\'s emissary', 'THS').
card_original_type('nylea\'s emissary'/'THS', 'Enchantment Creature — Cat').
card_original_text('nylea\'s emissary'/'THS', 'Bestow {5}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nTrample\nEnchanted creature gets +3/+3 and has trample.').
card_first_print('nylea\'s emissary', 'THS').
card_image_name('nylea\'s emissary'/'THS', 'nylea\'s emissary').
card_uid('nylea\'s emissary'/'THS', 'THS:Nylea\'s Emissary:nylea\'s emissary').
card_rarity('nylea\'s emissary'/'THS', 'Uncommon').
card_artist('nylea\'s emissary'/'THS', 'Sam Burley').
card_number('nylea\'s emissary'/'THS', '168').
card_multiverse_id('nylea\'s emissary'/'THS', '373510').

card_in_set('nylea\'s presence', 'THS').
card_original_type('nylea\'s presence'/'THS', 'Enchantment — Aura').
card_original_text('nylea\'s presence'/'THS', 'Enchant land\nWhen Nylea\'s Presence enters the battlefield, draw a card.\nEnchanted land is every basic land type in addition to its other types.').
card_first_print('nylea\'s presence', 'THS').
card_image_name('nylea\'s presence'/'THS', 'nylea\'s presence').
card_uid('nylea\'s presence'/'THS', 'THS:Nylea\'s Presence:nylea\'s presence').
card_rarity('nylea\'s presence'/'THS', 'Common').
card_artist('nylea\'s presence'/'THS', 'Ralph Horsley').
card_number('nylea\'s presence'/'THS', '169').
card_multiverse_id('nylea\'s presence'/'THS', '373580').

card_in_set('nylea, god of the hunt', 'THS').
card_original_type('nylea, god of the hunt'/'THS', 'Legendary Enchantment Creature — God').
card_original_text('nylea, god of the hunt'/'THS', 'Indestructible\nAs long as your devotion to green is less than five, Nylea isn\'t a creature. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)\nOther creatures you control have trample.\n{3}{G}: Target creature gets +2/+2 until end of turn.').
card_first_print('nylea, god of the hunt', 'THS').
card_image_name('nylea, god of the hunt'/'THS', 'nylea, god of the hunt').
card_uid('nylea, god of the hunt'/'THS', 'THS:Nylea, God of the Hunt:nylea, god of the hunt').
card_rarity('nylea, god of the hunt'/'THS', 'Mythic Rare').
card_artist('nylea, god of the hunt'/'THS', 'Chris Rahn').
card_number('nylea, god of the hunt'/'THS', '166').
card_multiverse_id('nylea, god of the hunt'/'THS', '373559').

card_in_set('observant alseid', 'THS').
card_original_type('observant alseid'/'THS', 'Enchantment Creature — Nymph').
card_original_text('observant alseid'/'THS', 'Bestow {4}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nVigilance\nEnchanted creature gets +2/+2 and has vigilance.').
card_first_print('observant alseid', 'THS').
card_image_name('observant alseid'/'THS', 'observant alseid').
card_uid('observant alseid'/'THS', 'THS:Observant Alseid:observant alseid').
card_rarity('observant alseid'/'THS', 'Common').
card_artist('observant alseid'/'THS', 'Todd Lockwood').
card_number('observant alseid'/'THS', '24').
card_multiverse_id('observant alseid'/'THS', '373733').

card_in_set('omenspeaker', 'THS').
card_original_type('omenspeaker'/'THS', 'Creature — Human Wizard').
card_original_text('omenspeaker'/'THS', 'When Omenspeaker enters the battlefield, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('omenspeaker', 'THS').
card_image_name('omenspeaker'/'THS', 'omenspeaker').
card_uid('omenspeaker'/'THS', 'THS:Omenspeaker:omenspeaker').
card_rarity('omenspeaker'/'THS', 'Common').
card_artist('omenspeaker'/'THS', 'Dallas Williams').
card_number('omenspeaker'/'THS', '57').
card_flavor_text('omenspeaker'/'THS', 'Her prophecies amaze her even as she speaks them.').
card_multiverse_id('omenspeaker'/'THS', '373693').

card_in_set('opaline unicorn', 'THS').
card_original_type('opaline unicorn'/'THS', 'Artifact Creature — Unicorn').
card_original_text('opaline unicorn'/'THS', '{T}: Add one mana of any color to your mana pool.').
card_first_print('opaline unicorn', 'THS').
card_image_name('opaline unicorn'/'THS', 'opaline unicorn').
card_uid('opaline unicorn'/'THS', 'THS:Opaline Unicorn:opaline unicorn').
card_rarity('opaline unicorn'/'THS', 'Common').
card_artist('opaline unicorn'/'THS', 'Christine Choi').
card_number('opaline unicorn'/'THS', '218').
card_flavor_text('opaline unicorn'/'THS', 'Purphoros once loved Nylea, the god of the hunt. His passion inspired his most astounding works of art.').
card_multiverse_id('opaline unicorn'/'THS', '373611').

card_in_set('ordeal of erebos', 'THS').
card_original_type('ordeal of erebos'/'THS', 'Enchantment — Aura').
card_original_text('ordeal of erebos'/'THS', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Erebos.\nWhen you sacrifice Ordeal of Erebos, target player discards two cards.').
card_first_print('ordeal of erebos', 'THS').
card_image_name('ordeal of erebos'/'THS', 'ordeal of erebos').
card_uid('ordeal of erebos'/'THS', 'THS:Ordeal of Erebos:ordeal of erebos').
card_rarity('ordeal of erebos'/'THS', 'Uncommon').
card_artist('ordeal of erebos'/'THS', 'Tyler Jacobson').
card_number('ordeal of erebos'/'THS', '99').
card_multiverse_id('ordeal of erebos'/'THS', '373646').

card_in_set('ordeal of heliod', 'THS').
card_original_type('ordeal of heliod'/'THS', 'Enchantment — Aura').
card_original_text('ordeal of heliod'/'THS', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Heliod.\nWhen you sacrifice Ordeal of Heliod, you gain 10 life.').
card_first_print('ordeal of heliod', 'THS').
card_image_name('ordeal of heliod'/'THS', 'ordeal of heliod').
card_uid('ordeal of heliod'/'THS', 'THS:Ordeal of Heliod:ordeal of heliod').
card_rarity('ordeal of heliod'/'THS', 'Uncommon').
card_artist('ordeal of heliod'/'THS', 'Lucas Graciano').
card_number('ordeal of heliod'/'THS', '25').
card_multiverse_id('ordeal of heliod'/'THS', '373619').

card_in_set('ordeal of nylea', 'THS').
card_original_type('ordeal of nylea'/'THS', 'Enchantment — Aura').
card_original_text('ordeal of nylea'/'THS', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Nylea.\nWhen you sacrifice Ordeal of Nylea, search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('ordeal of nylea', 'THS').
card_image_name('ordeal of nylea'/'THS', 'ordeal of nylea').
card_uid('ordeal of nylea'/'THS', 'THS:Ordeal of Nylea:ordeal of nylea').
card_rarity('ordeal of nylea'/'THS', 'Uncommon').
card_artist('ordeal of nylea'/'THS', 'David Palumbo').
card_number('ordeal of nylea'/'THS', '170').
card_multiverse_id('ordeal of nylea'/'THS', '373553').

card_in_set('ordeal of purphoros', 'THS').
card_original_type('ordeal of purphoros'/'THS', 'Enchantment — Aura').
card_original_text('ordeal of purphoros'/'THS', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Purphoros.\nWhen you sacrifice Ordeal of Purphoros, it deals 3 damage to target creature or player.').
card_image_name('ordeal of purphoros'/'THS', 'ordeal of purphoros').
card_uid('ordeal of purphoros'/'THS', 'THS:Ordeal of Purphoros:ordeal of purphoros').
card_rarity('ordeal of purphoros'/'THS', 'Uncommon').
card_artist('ordeal of purphoros'/'THS', 'Maciej Kuciara').
card_number('ordeal of purphoros'/'THS', '131').
card_multiverse_id('ordeal of purphoros'/'THS', '373647').

card_in_set('ordeal of thassa', 'THS').
card_original_type('ordeal of thassa'/'THS', 'Enchantment — Aura').
card_original_text('ordeal of thassa'/'THS', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Thassa.\nWhen you sacrifice Ordeal of Thassa, draw two cards.').
card_first_print('ordeal of thassa', 'THS').
card_image_name('ordeal of thassa'/'THS', 'ordeal of thassa').
card_uid('ordeal of thassa'/'THS', 'THS:Ordeal of Thassa:ordeal of thassa').
card_rarity('ordeal of thassa'/'THS', 'Uncommon').
card_artist('ordeal of thassa'/'THS', 'Howard Lyon').
card_number('ordeal of thassa'/'THS', '58').
card_multiverse_id('ordeal of thassa'/'THS', '373696').

card_in_set('peak eruption', 'THS').
card_original_type('peak eruption'/'THS', 'Sorcery').
card_original_text('peak eruption'/'THS', 'Destroy target Mountain. Peak Eruption deals 3 damage to that land\'s controller.').
card_first_print('peak eruption', 'THS').
card_image_name('peak eruption'/'THS', 'peak eruption').
card_uid('peak eruption'/'THS', 'THS:Peak Eruption:peak eruption').
card_rarity('peak eruption'/'THS', 'Uncommon').
card_artist('peak eruption'/'THS', 'Adam Paquette').
card_number('peak eruption'/'THS', '132').
card_flavor_text('peak eruption'/'THS', '\"Early olive buds? A good omen. Flock of crows? Not so good. Mountain exploding? Bad. Very, very bad.\"\n—Hira, street oracle').
card_multiverse_id('peak eruption'/'THS', '373507').

card_in_set('phalanx leader', 'THS').
card_original_type('phalanx leader'/'THS', 'Creature — Human Soldier').
card_original_text('phalanx leader'/'THS', 'Heroic — Whenever you cast a spell that targets Phalanx Leader, put a +1/+1 counter on each creature you control.').
card_image_name('phalanx leader'/'THS', 'phalanx leader').
card_uid('phalanx leader'/'THS', 'THS:Phalanx Leader:phalanx leader').
card_rarity('phalanx leader'/'THS', 'Uncommon').
card_artist('phalanx leader'/'THS', 'David Palumbo').
card_number('phalanx leader'/'THS', '26').
card_flavor_text('phalanx leader'/'THS', 'His soldiers etch his words on the insides of their shields, their inspiration always in sight during battle.').
card_multiverse_id('phalanx leader'/'THS', '373592').

card_in_set('pharika\'s cure', 'THS').
card_original_type('pharika\'s cure'/'THS', 'Instant').
card_original_text('pharika\'s cure'/'THS', 'Pharika\'s Cure deals 2 damage to target creature and you gain 2 life.').
card_first_print('pharika\'s cure', 'THS').
card_image_name('pharika\'s cure'/'THS', 'pharika\'s cure').
card_uid('pharika\'s cure'/'THS', 'THS:Pharika\'s Cure:pharika\'s cure').
card_rarity('pharika\'s cure'/'THS', 'Common').
card_artist('pharika\'s cure'/'THS', 'Igor Kieryluk').
card_number('pharika\'s cure'/'THS', '100').
card_flavor_text('pharika\'s cure'/'THS', '\"The venom cleanses the sickness from your body, but it will not be pleasant, and you may not survive. Pharika\'s blessings are fickle.\"\n—Solon, acolyte of Pharika').
card_multiverse_id('pharika\'s cure'/'THS', '373722').

card_in_set('pharika\'s mender', 'THS').
card_original_type('pharika\'s mender'/'THS', 'Creature — Gorgon').
card_original_text('pharika\'s mender'/'THS', 'When Pharika\'s Mender enters the battlefield, you may return target creature or enchantment card from your graveyard to your hand.').
card_first_print('pharika\'s mender', 'THS').
card_image_name('pharika\'s mender'/'THS', 'pharika\'s mender').
card_uid('pharika\'s mender'/'THS', 'THS:Pharika\'s Mender:pharika\'s mender').
card_rarity('pharika\'s mender'/'THS', 'Uncommon').
card_artist('pharika\'s mender'/'THS', 'Peter Mohrbacher').
card_number('pharika\'s mender'/'THS', '197').
card_flavor_text('pharika\'s mender'/'THS', '\"The direst venom becomes a panacea under Pharika\'s guidance. I bring it to the worthy, clinging at the edge of the abyss.\"').
card_multiverse_id('pharika\'s mender'/'THS', '373569').

card_in_set('pheres-band centaurs', 'THS').
card_original_type('pheres-band centaurs'/'THS', 'Creature — Centaur Warrior').
card_original_text('pheres-band centaurs'/'THS', '').
card_first_print('pheres-band centaurs', 'THS').
card_image_name('pheres-band centaurs'/'THS', 'pheres-band centaurs').
card_uid('pheres-band centaurs'/'THS', 'THS:Pheres-Band Centaurs:pheres-band centaurs').
card_rarity('pheres-band centaurs'/'THS', 'Common').
card_artist('pheres-band centaurs'/'THS', 'Mark Winters').
card_number('pheres-band centaurs'/'THS', '171').
card_flavor_text('pheres-band centaurs'/'THS', '\"Poets speak of your unrivaled speed,\" the Champion said to the assembled centaurs, \"but it is plain to see that your true strength lies in your unwavering loyalty to one another.\"\n—The Theriad').
card_multiverse_id('pheres-band centaurs'/'THS', '373657').

card_in_set('plains', 'THS').
card_original_type('plains'/'THS', 'Basic Land — Plains').
card_original_text('plains'/'THS', 'W').
card_image_name('plains'/'THS', 'plains1').
card_uid('plains'/'THS', 'THS:Plains:plains1').
card_rarity('plains'/'THS', 'Basic Land').
card_artist('plains'/'THS', 'Rob Alexander').
card_number('plains'/'THS', '230').
card_multiverse_id('plains'/'THS', '373654').

card_in_set('plains', 'THS').
card_original_type('plains'/'THS', 'Basic Land — Plains').
card_original_text('plains'/'THS', 'W').
card_image_name('plains'/'THS', 'plains2').
card_uid('plains'/'THS', 'THS:Plains:plains2').
card_rarity('plains'/'THS', 'Basic Land').
card_artist('plains'/'THS', 'Steven Belledin').
card_number('plains'/'THS', '231').
card_multiverse_id('plains'/'THS', '373533').

card_in_set('plains', 'THS').
card_original_type('plains'/'THS', 'Basic Land — Plains').
card_original_text('plains'/'THS', 'W').
card_image_name('plains'/'THS', 'plains3').
card_uid('plains'/'THS', 'THS:Plains:plains3').
card_rarity('plains'/'THS', 'Basic Land').
card_artist('plains'/'THS', 'Adam Paquette').
card_number('plains'/'THS', '232').
card_multiverse_id('plains'/'THS', '373700').

card_in_set('plains', 'THS').
card_original_type('plains'/'THS', 'Basic Land — Plains').
card_original_text('plains'/'THS', 'W').
card_image_name('plains'/'THS', 'plains4').
card_uid('plains'/'THS', 'THS:Plains:plains4').
card_rarity('plains'/'THS', 'Basic Land').
card_artist('plains'/'THS', 'Raoul Vitale').
card_number('plains'/'THS', '233').
card_multiverse_id('plains'/'THS', '373582').

card_in_set('polis crusher', 'THS').
card_original_type('polis crusher'/'THS', 'Creature — Cyclops').
card_original_text('polis crusher'/'THS', 'Trample, protection from enchantments\n{4}{R}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhenever Polis Crusher deals combat damage to a player, if Polis Crusher is monstrous, destroy target enchantment that player controls.').
card_first_print('polis crusher', 'THS').
card_image_name('polis crusher'/'THS', 'polis crusher').
card_uid('polis crusher'/'THS', 'THS:Polis Crusher:polis crusher').
card_rarity('polis crusher'/'THS', 'Rare').
card_artist('polis crusher'/'THS', 'Chase Stone').
card_number('polis crusher'/'THS', '198').
card_multiverse_id('polis crusher'/'THS', '373716').

card_in_set('polukranos, world eater', 'THS').
card_original_type('polukranos, world eater'/'THS', 'Legendary Creature — Hydra').
card_original_text('polukranos, world eater'/'THS', '{X}{X}{G}: Monstrosity X. (If this creature isn\'t monstrous, put X +1/+1 counters on it and it becomes monstrous.)\nWhen Polukranos, World Eater becomes monstrous, it deals X damage divided as you choose among any number of target creatures your opponents control. Each of those creatures deals damage equal to its power to Polukranos.').
card_image_name('polukranos, world eater'/'THS', 'polukranos, world eater').
card_uid('polukranos, world eater'/'THS', 'THS:Polukranos, World Eater:polukranos, world eater').
card_rarity('polukranos, world eater'/'THS', 'Mythic Rare').
card_artist('polukranos, world eater'/'THS', 'Johann Bodin').
card_number('polukranos, world eater'/'THS', '172').
card_multiverse_id('polukranos, world eater'/'THS', '373549').

card_in_set('portent of betrayal', 'THS').
card_original_type('portent of betrayal'/'THS', 'Sorcery').
card_original_text('portent of betrayal'/'THS', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('portent of betrayal', 'THS').
card_image_name('portent of betrayal'/'THS', 'portent of betrayal').
card_uid('portent of betrayal'/'THS', 'THS:Portent of Betrayal:portent of betrayal').
card_rarity('portent of betrayal'/'THS', 'Common').
card_artist('portent of betrayal'/'THS', 'Daarken').
card_number('portent of betrayal'/'THS', '133').
card_multiverse_id('portent of betrayal'/'THS', '373667').

card_in_set('prescient chimera', 'THS').
card_original_type('prescient chimera'/'THS', 'Creature — Chimera').
card_original_text('prescient chimera'/'THS', 'Flying\nWhenever you cast an instant or sorcery spell, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('prescient chimera', 'THS').
card_image_name('prescient chimera'/'THS', 'prescient chimera').
card_uid('prescient chimera'/'THS', 'THS:Prescient Chimera:prescient chimera').
card_rarity('prescient chimera'/'THS', 'Common').
card_artist('prescient chimera'/'THS', 'Daarken').
card_number('prescient chimera'/'THS', '59').
card_multiverse_id('prescient chimera'/'THS', '373643').

card_in_set('priest of iroas', 'THS').
card_original_type('priest of iroas'/'THS', 'Creature — Human Cleric').
card_original_text('priest of iroas'/'THS', '{3}{W}, Sacrifice Priest of Iroas: Destroy target enchantment.').
card_first_print('priest of iroas', 'THS').
card_image_name('priest of iroas'/'THS', 'priest of iroas').
card_uid('priest of iroas'/'THS', 'THS:Priest of Iroas:priest of iroas').
card_rarity('priest of iroas'/'THS', 'Common').
card_artist('priest of iroas'/'THS', 'Clint Cearley').
card_number('priest of iroas'/'THS', '134').
card_flavor_text('priest of iroas'/'THS', '\"Even my last breath will be a blow struck for Iroas.\"').
card_multiverse_id('priest of iroas'/'THS', '373614').

card_in_set('prognostic sphinx', 'THS').
card_original_type('prognostic sphinx'/'THS', 'Creature — Sphinx').
card_original_text('prognostic sphinx'/'THS', 'Flying\nDiscard a card: Prognostic Sphinx gains hexproof until end of turn. Tap it.\nWhenever Prognostic Sphinx attacks, scry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('prognostic sphinx', 'THS').
card_image_name('prognostic sphinx'/'THS', 'prognostic sphinx').
card_uid('prognostic sphinx'/'THS', 'THS:Prognostic Sphinx:prognostic sphinx').
card_rarity('prognostic sphinx'/'THS', 'Rare').
card_artist('prognostic sphinx'/'THS', 'Steve Prescott').
card_number('prognostic sphinx'/'THS', '60').
card_multiverse_id('prognostic sphinx'/'THS', '373617').

card_in_set('prophet of kruphix', 'THS').
card_original_type('prophet of kruphix'/'THS', 'Creature — Human Wizard').
card_original_text('prophet of kruphix'/'THS', 'Untap all creatures and lands you control during each other player\'s untap step.\nYou may cast creature cards as though they had flash.').
card_first_print('prophet of kruphix', 'THS').
card_image_name('prophet of kruphix'/'THS', 'prophet of kruphix').
card_uid('prophet of kruphix'/'THS', 'THS:Prophet of Kruphix:prophet of kruphix').
card_rarity('prophet of kruphix'/'THS', 'Rare').
card_artist('prophet of kruphix'/'THS', 'Winona Nelson').
card_number('prophet of kruphix'/'THS', '199').
card_flavor_text('prophet of kruphix'/'THS', '\"Time is fluid as a dance, and truth as fleeting.\"').
card_multiverse_id('prophet of kruphix'/'THS', '373635').

card_in_set('prowler\'s helm', 'THS').
card_original_type('prowler\'s helm'/'THS', 'Artifact — Equipment').
card_original_text('prowler\'s helm'/'THS', 'Equipped creature can\'t be blocked except by Walls.\nEquip {2}').
card_first_print('prowler\'s helm', 'THS').
card_image_name('prowler\'s helm'/'THS', 'prowler\'s helm').
card_uid('prowler\'s helm'/'THS', 'THS:Prowler\'s Helm:prowler\'s helm').
card_rarity('prowler\'s helm'/'THS', 'Uncommon').
card_artist('prowler\'s helm'/'THS', 'Igor Kieryluk').
card_number('prowler\'s helm'/'THS', '219').
card_flavor_text('prowler\'s helm'/'THS', '\"The youths prattle on about heroic deeds, but avoiding the noose is a feat more daring than their entire careers.\"\n—Basarios the Blade').
card_multiverse_id('prowler\'s helm'/'THS', '373626').

card_in_set('psychic intrusion', 'THS').
card_original_type('psychic intrusion'/'THS', 'Sorcery').
card_original_text('psychic intrusion'/'THS', 'Target opponent reveals his or her hand. You choose a nonland card from that player\'s graveyard or hand and exile it. You may cast that card for as long as it remains exiled, and you may spend mana as though it were mana of any color to cast that spell.').
card_first_print('psychic intrusion', 'THS').
card_image_name('psychic intrusion'/'THS', 'psychic intrusion').
card_uid('psychic intrusion'/'THS', 'THS:Psychic Intrusion:psychic intrusion').
card_rarity('psychic intrusion'/'THS', 'Rare').
card_artist('psychic intrusion'/'THS', 'Jaime Jones').
card_number('psychic intrusion'/'THS', '200').
card_multiverse_id('psychic intrusion'/'THS', '373695').

card_in_set('purphoros\'s emissary', 'THS').
card_original_type('purphoros\'s emissary'/'THS', 'Enchantment Creature — Ox').
card_original_text('purphoros\'s emissary'/'THS', 'Bestow {6}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nPurphoros\'s Emissary can\'t be blocked except by two or more creatures.\nEnchanted creature gets +3/+3 and can\'t be blocked except by two or more creatures.').
card_first_print('purphoros\'s emissary', 'THS').
card_image_name('purphoros\'s emissary'/'THS', 'purphoros\'s emissary').
card_uid('purphoros\'s emissary'/'THS', 'THS:Purphoros\'s Emissary:purphoros\'s emissary').
card_rarity('purphoros\'s emissary'/'THS', 'Uncommon').
card_artist('purphoros\'s emissary'/'THS', 'Sam Burley').
card_number('purphoros\'s emissary'/'THS', '136').
card_multiverse_id('purphoros\'s emissary'/'THS', '373505').

card_in_set('purphoros, god of the forge', 'THS').
card_original_type('purphoros, god of the forge'/'THS', 'Legendary Enchantment Creature — God').
card_original_text('purphoros, god of the forge'/'THS', 'Indestructible\nAs long as your devotion to red is less than five, Purphoros isn\'t a creature.\nWhenever another creature enters the battlefield under your control, Purphoros deals 2 damage to each opponent.\n{2}{R}: Creatures you control get +1/+0 until end of turn.').
card_first_print('purphoros, god of the forge', 'THS').
card_image_name('purphoros, god of the forge'/'THS', 'purphoros, god of the forge').
card_uid('purphoros, god of the forge'/'THS', 'THS:Purphoros, God of the Forge:purphoros, god of the forge').
card_rarity('purphoros, god of the forge'/'THS', 'Mythic Rare').
card_artist('purphoros, god of the forge'/'THS', 'Eric Deschamps').
card_number('purphoros, god of the forge'/'THS', '135').
card_multiverse_id('purphoros, god of the forge'/'THS', '373556').

card_in_set('pyxis of pandemonium', 'THS').
card_original_type('pyxis of pandemonium'/'THS', 'Artifact').
card_original_text('pyxis of pandemonium'/'THS', '{T}: Each player exiles the top card of his or her library face down.\n{7}, {T}, Sacrifice Pyxis of Pandemonium: Each player turns face up all cards he or she owns exiled with Pyxis of Pandemonium, then puts all permanent cards among them onto the battlefield.').
card_first_print('pyxis of pandemonium', 'THS').
card_image_name('pyxis of pandemonium'/'THS', 'pyxis of pandemonium').
card_uid('pyxis of pandemonium'/'THS', 'THS:Pyxis of Pandemonium:pyxis of pandemonium').
card_rarity('pyxis of pandemonium'/'THS', 'Rare').
card_artist('pyxis of pandemonium'/'THS', 'David Palumbo').
card_number('pyxis of pandemonium'/'THS', '220').
card_multiverse_id('pyxis of pandemonium'/'THS', '373669').

card_in_set('rage of purphoros', 'THS').
card_original_type('rage of purphoros'/'THS', 'Sorcery').
card_original_text('rage of purphoros'/'THS', 'Rage of Purphoros deals 4 damage to target creature. It can\'t be regenerated this turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('rage of purphoros', 'THS').
card_image_name('rage of purphoros'/'THS', 'rage of purphoros').
card_uid('rage of purphoros'/'THS', 'THS:Rage of Purphoros:rage of purphoros').
card_rarity('rage of purphoros'/'THS', 'Common').
card_artist('rage of purphoros'/'THS', 'Mathias Kollros').
card_number('rage of purphoros'/'THS', '137').
card_multiverse_id('rage of purphoros'/'THS', '373663').

card_in_set('rageblood shaman', 'THS').
card_original_type('rageblood shaman'/'THS', 'Creature — Minotaur Shaman').
card_original_text('rageblood shaman'/'THS', 'Trample\nOther Minotaur creatures you control get +1/+1 and have trample.').
card_first_print('rageblood shaman', 'THS').
card_image_name('rageblood shaman'/'THS', 'rageblood shaman').
card_uid('rageblood shaman'/'THS', 'THS:Rageblood Shaman:rageblood shaman').
card_rarity('rageblood shaman'/'THS', 'Rare').
card_artist('rageblood shaman'/'THS', 'Mike Bierek').
card_number('rageblood shaman'/'THS', '138').
card_flavor_text('rageblood shaman'/'THS', '\"I see a spark of pure rage. Soon that spark will spread from the depths of Kragma. Soon its fire will engulf the polis.\"\n—Hira, street oracle').
card_multiverse_id('rageblood shaman'/'THS', '373672').

card_in_set('ray of dissolution', 'THS').
card_original_type('ray of dissolution'/'THS', 'Instant').
card_original_text('ray of dissolution'/'THS', 'Destroy target enchantment. You gain 3 life.').
card_first_print('ray of dissolution', 'THS').
card_image_name('ray of dissolution'/'THS', 'ray of dissolution').
card_uid('ray of dissolution'/'THS', 'THS:Ray of Dissolution:ray of dissolution').
card_rarity('ray of dissolution'/'THS', 'Common').
card_artist('ray of dissolution'/'THS', 'Terese Nielsen').
card_number('ray of dissolution'/'THS', '27').
card_flavor_text('ray of dissolution'/'THS', 'The works of one god last only as long as the patience of another.').
card_multiverse_id('ray of dissolution'/'THS', '373739').

card_in_set('read the bones', 'THS').
card_original_type('read the bones'/'THS', 'Sorcery').
card_original_text('read the bones'/'THS', 'Scry 2, then draw two cards. You lose 2 life. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('read the bones', 'THS').
card_image_name('read the bones'/'THS', 'read the bones').
card_uid('read the bones'/'THS', 'THS:Read the Bones:read the bones').
card_rarity('read the bones'/'THS', 'Common').
card_artist('read the bones'/'THS', 'Lars Grant-West').
card_number('read the bones'/'THS', '101').
card_flavor_text('read the bones'/'THS', 'The dead know lessons the living haven\'t learned.').
card_multiverse_id('read the bones'/'THS', '373725').

card_in_set('reaper of the wilds', 'THS').
card_original_type('reaper of the wilds'/'THS', 'Creature — Gorgon').
card_original_text('reaper of the wilds'/'THS', 'Whenever another creature dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{B}: Reaper of the Wilds gains deathtouch until end of turn.\n{1}{G}: Reaper of the Wilds gains hexproof until end of turn.').
card_first_print('reaper of the wilds', 'THS').
card_image_name('reaper of the wilds'/'THS', 'reaper of the wilds').
card_uid('reaper of the wilds'/'THS', 'THS:Reaper of the Wilds:reaper of the wilds').
card_rarity('reaper of the wilds'/'THS', 'Rare').
card_artist('reaper of the wilds'/'THS', 'Karl Kopinski').
card_number('reaper of the wilds'/'THS', '201').
card_multiverse_id('reaper of the wilds'/'THS', '373570').

card_in_set('rescue from the underworld', 'THS').
card_original_type('rescue from the underworld'/'THS', 'Instant').
card_original_text('rescue from the underworld'/'THS', 'As an additional cost to cast Rescue from the Underworld, sacrifice a creature. \nChoose target creature card in your graveyard. Return that card and the sacrificed card to the battlefield under your control at the beginning of your next upkeep. Exile Rescue from the Underworld.').
card_first_print('rescue from the underworld', 'THS').
card_image_name('rescue from the underworld'/'THS', 'rescue from the underworld').
card_uid('rescue from the underworld'/'THS', 'THS:Rescue from the Underworld:rescue from the underworld').
card_rarity('rescue from the underworld'/'THS', 'Uncommon').
card_artist('rescue from the underworld'/'THS', 'Raymond Swanland').
card_number('rescue from the underworld'/'THS', '102').
card_multiverse_id('rescue from the underworld'/'THS', '373532').

card_in_set('returned centaur', 'THS').
card_original_type('returned centaur'/'THS', 'Creature — Zombie Centaur').
card_original_text('returned centaur'/'THS', 'When Returned Centaur enters the battlefield, target player puts the top four cards of his or her library into his or her graveyard.').
card_first_print('returned centaur', 'THS').
card_image_name('returned centaur'/'THS', 'returned centaur').
card_uid('returned centaur'/'THS', 'THS:Returned Centaur:returned centaur').
card_rarity('returned centaur'/'THS', 'Common').
card_artist('returned centaur'/'THS', 'Lucas Graciano').
card_number('returned centaur'/'THS', '103').
card_flavor_text('returned centaur'/'THS', 'Driven away by his living kin, he wanders mourning through the wilderness, seeking the dead city of Asphodel.').
card_multiverse_id('returned centaur'/'THS', '373644').

card_in_set('returned phalanx', 'THS').
card_original_type('returned phalanx'/'THS', 'Creature — Zombie Soldier').
card_original_text('returned phalanx'/'THS', 'Defender\n{1}{U}: Returned Phalanx can attack this turn as though it didn\'t have defender.').
card_first_print('returned phalanx', 'THS').
card_image_name('returned phalanx'/'THS', 'returned phalanx').
card_uid('returned phalanx'/'THS', 'THS:Returned Phalanx:returned phalanx').
card_rarity('returned phalanx'/'THS', 'Common').
card_artist('returned phalanx'/'THS', 'Seb McKinnon').
card_number('returned phalanx'/'THS', '104').
card_flavor_text('returned phalanx'/'THS', 'They lived in different nations and fought in different eras, but as the Returned, they link arms as one.').
card_multiverse_id('returned phalanx'/'THS', '373508').

card_in_set('reverent hunter', 'THS').
card_original_type('reverent hunter'/'THS', 'Creature — Human Archer').
card_original_text('reverent hunter'/'THS', 'When Reverent Hunter enters the battlefield, put a number of +1/+1 counters on it equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_first_print('reverent hunter', 'THS').
card_image_name('reverent hunter'/'THS', 'reverent hunter').
card_uid('reverent hunter'/'THS', 'THS:Reverent Hunter:reverent hunter').
card_rarity('reverent hunter'/'THS', 'Rare').
card_artist('reverent hunter'/'THS', 'Wesley Burt').
card_number('reverent hunter'/'THS', '173').
card_multiverse_id('reverent hunter'/'THS', '373601').

card_in_set('satyr hedonist', 'THS').
card_original_type('satyr hedonist'/'THS', 'Creature — Satyr').
card_original_text('satyr hedonist'/'THS', '{R}, Sacrifice Satyr Hedonist: Add {R}{R}{R} to your mana pool.').
card_image_name('satyr hedonist'/'THS', 'satyr hedonist').
card_uid('satyr hedonist'/'THS', 'THS:Satyr Hedonist:satyr hedonist').
card_rarity('satyr hedonist'/'THS', 'Common').
card_artist('satyr hedonist'/'THS', 'Chase Stone').
card_number('satyr hedonist'/'THS', '174').
card_flavor_text('satyr hedonist'/'THS', '\"Any festival you can walk away from wasn\'t worth attending in the first place.\"').
card_multiverse_id('satyr hedonist'/'THS', '373744').

card_in_set('satyr piper', 'THS').
card_original_type('satyr piper'/'THS', 'Creature — Satyr Rogue').
card_original_text('satyr piper'/'THS', '{3}{G}: Target creature must be blocked this turn if able.').
card_first_print('satyr piper', 'THS').
card_image_name('satyr piper'/'THS', 'satyr piper').
card_uid('satyr piper'/'THS', 'THS:Satyr Piper:satyr piper').
card_rarity('satyr piper'/'THS', 'Uncommon').
card_artist('satyr piper'/'THS', 'Christopher Moeller').
card_number('satyr piper'/'THS', '175').
card_flavor_text('satyr piper'/'THS', '\"When I asked my commander the reward for killing that prancing nuisance, he told me, ‘None! I want to kill him myself!\'\"\n—Phrogas, soldier of Akros').
card_multiverse_id('satyr piper'/'THS', '373728').

card_in_set('satyr rambler', 'THS').
card_original_type('satyr rambler'/'THS', 'Creature — Satyr').
card_original_text('satyr rambler'/'THS', 'Trample').
card_first_print('satyr rambler', 'THS').
card_image_name('satyr rambler'/'THS', 'satyr rambler').
card_uid('satyr rambler'/'THS', 'THS:Satyr Rambler:satyr rambler').
card_rarity('satyr rambler'/'THS', 'Common').
card_artist('satyr rambler'/'THS', 'John Stanko').
card_number('satyr rambler'/'THS', '139').
card_flavor_text('satyr rambler'/'THS', 'A satyr is bound by nothing—not home, not family, not loyalty.').
card_multiverse_id('satyr rambler'/'THS', '373737').

card_in_set('savage surge', 'THS').
card_original_type('savage surge'/'THS', 'Instant').
card_original_text('savage surge'/'THS', 'Target creature gets +2/+2 until end of turn. Untap that creature.').
card_image_name('savage surge'/'THS', 'savage surge').
card_uid('savage surge'/'THS', 'THS:Savage Surge:savage surge').
card_rarity('savage surge'/'THS', 'Common').
card_artist('savage surge'/'THS', 'Jasper Sandner').
card_number('savage surge'/'THS', '176').
card_flavor_text('savage surge'/'THS', '\"I thought its back was turned!\"\n\"A centaur has two backs!\"').
card_multiverse_id('savage surge'/'THS', '373602').

card_in_set('scholar of athreos', 'THS').
card_original_type('scholar of athreos'/'THS', 'Creature — Human Cleric').
card_original_text('scholar of athreos'/'THS', '{2}{B}: Each opponent loses 1 life. You gain life equal to the life lost this way.').
card_first_print('scholar of athreos', 'THS').
card_image_name('scholar of athreos'/'THS', 'scholar of athreos').
card_uid('scholar of athreos'/'THS', 'THS:Scholar of Athreos:scholar of athreos').
card_rarity('scholar of athreos'/'THS', 'Common').
card_artist('scholar of athreos'/'THS', 'Cynthia Sheppard').
card_number('scholar of athreos'/'THS', '28').
card_flavor_text('scholar of athreos'/'THS', 'She asks pointed questions of the dead who wait for Athreos, learning of life from those who are about to leave it.').
card_multiverse_id('scholar of athreos'/'THS', '373692').

card_in_set('scourgemark', 'THS').
card_original_type('scourgemark'/'THS', 'Enchantment — Aura').
card_original_text('scourgemark'/'THS', 'Enchant creature\nWhen Scourgemark enters the battlefield, draw a card.\nEnchanted creature gets +1/+0.').
card_first_print('scourgemark', 'THS').
card_image_name('scourgemark'/'THS', 'scourgemark').
card_uid('scourgemark'/'THS', 'THS:Scourgemark:scourgemark').
card_rarity('scourgemark'/'THS', 'Common').
card_artist('scourgemark'/'THS', 'Franz Vohwinkel').
card_number('scourgemark'/'THS', '105').
card_flavor_text('scourgemark'/'THS', 'To members of the cult of Erebos, gold-infused tattoos symbolize the inevitable grasp of the god of death.').
card_multiverse_id('scourgemark'/'THS', '373652').

card_in_set('sea god\'s revenge', 'THS').
card_original_type('sea god\'s revenge'/'THS', 'Sorcery').
card_original_text('sea god\'s revenge'/'THS', 'Return up to three target creatures your opponents control to their owners\' hands. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('sea god\'s revenge', 'THS').
card_image_name('sea god\'s revenge'/'THS', 'sea god\'s revenge').
card_uid('sea god\'s revenge'/'THS', 'THS:Sea God\'s Revenge:sea god\'s revenge').
card_rarity('sea god\'s revenge'/'THS', 'Uncommon').
card_artist('sea god\'s revenge'/'THS', 'Eric Velhagen').
card_number('sea god\'s revenge'/'THS', '61').
card_flavor_text('sea god\'s revenge'/'THS', '\"What has neither mouth nor throat, yet swallows captain, crew, and boat?\"\n—Sphinx\'s riddle').
card_multiverse_id('sea god\'s revenge'/'THS', '373517').

card_in_set('sealock monster', 'THS').
card_original_type('sealock monster'/'THS', 'Creature — Octopus').
card_original_text('sealock monster'/'THS', 'Sealock Monster can\'t attack unless defending player controls an Island.\n{5}{U}{U}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Sealock Monster becomes monstrous, target land becomes an Island in addition to its other types.').
card_first_print('sealock monster', 'THS').
card_image_name('sealock monster'/'THS', 'sealock monster').
card_uid('sealock monster'/'THS', 'THS:Sealock Monster:sealock monster').
card_rarity('sealock monster'/'THS', 'Uncommon').
card_artist('sealock monster'/'THS', 'Adam Paquette').
card_number('sealock monster'/'THS', '62').
card_multiverse_id('sealock monster'/'THS', '373653').

card_in_set('sedge scorpion', 'THS').
card_original_type('sedge scorpion'/'THS', 'Creature — Scorpion').
card_original_text('sedge scorpion'/'THS', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('sedge scorpion', 'THS').
card_image_name('sedge scorpion'/'THS', 'sedge scorpion').
card_uid('sedge scorpion'/'THS', 'THS:Sedge Scorpion:sedge scorpion').
card_rarity('sedge scorpion'/'THS', 'Common').
card_artist('sedge scorpion'/'THS', 'John Stanko').
card_number('sedge scorpion'/'THS', '177').
card_flavor_text('sedge scorpion'/'THS', 'Thakolides the Mighty\nSlayer of minotaurs\nVanquisher of giants\nKilled by a scorpion\n—Inscription on an Akroan grave').
card_multiverse_id('sedge scorpion'/'THS', '373718').

card_in_set('sentry of the underworld', 'THS').
card_original_type('sentry of the underworld'/'THS', 'Creature — Griffin Skeleton').
card_original_text('sentry of the underworld'/'THS', 'Flying, vigilance\n{W}{B}, Pay 3 life: Regenerate Sentry of the Underworld.').
card_first_print('sentry of the underworld', 'THS').
card_image_name('sentry of the underworld'/'THS', 'sentry of the underworld').
card_uid('sentry of the underworld'/'THS', 'THS:Sentry of the Underworld:sentry of the underworld').
card_rarity('sentry of the underworld'/'THS', 'Uncommon').
card_artist('sentry of the underworld'/'THS', 'Dave Kendall').
card_number('sentry of the underworld'/'THS', '202').
card_flavor_text('sentry of the underworld'/'THS', 'When Athreos gathers the newly dead to be ferried across the Five Rivers That Ring the World, he sends skeletal griffins to fetch those who stray.').
card_multiverse_id('sentry of the underworld'/'THS', '373697').

card_in_set('setessan battle priest', 'THS').
card_original_type('setessan battle priest'/'THS', 'Creature — Human Cleric').
card_original_text('setessan battle priest'/'THS', 'Heroic — Whenever you cast a spell that targets Setessan Battle Priest, you gain 2 life.').
card_first_print('setessan battle priest', 'THS').
card_image_name('setessan battle priest'/'THS', 'setessan battle priest').
card_uid('setessan battle priest'/'THS', 'THS:Setessan Battle Priest:setessan battle priest').
card_rarity('setessan battle priest'/'THS', 'Common').
card_artist('setessan battle priest'/'THS', 'Wesley Burt').
card_number('setessan battle priest'/'THS', '29').
card_flavor_text('setessan battle priest'/'THS', '\"Your god teaches you only how to kill. Karametra teaches me to defend what I hold dear. That is why I will prevail.\"').
card_multiverse_id('setessan battle priest'/'THS', '373515').

card_in_set('setessan griffin', 'THS').
card_original_type('setessan griffin'/'THS', 'Creature — Griffin').
card_original_text('setessan griffin'/'THS', 'Flying\n{2}{G}{G}: Setessan Griffin gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_first_print('setessan griffin', 'THS').
card_image_name('setessan griffin'/'THS', 'setessan griffin').
card_uid('setessan griffin'/'THS', 'THS:Setessan Griffin:setessan griffin').
card_rarity('setessan griffin'/'THS', 'Common').
card_artist('setessan griffin'/'THS', 'Greg Staples').
card_number('setessan griffin'/'THS', '30').
card_flavor_text('setessan griffin'/'THS', 'Most griffins must be caught and broken into the service of the polis. Not so in Setessa, where they volunteer.').
card_multiverse_id('setessan griffin'/'THS', '373574').

card_in_set('shipbreaker kraken', 'THS').
card_original_type('shipbreaker kraken'/'THS', 'Creature — Kraken').
card_original_text('shipbreaker kraken'/'THS', '{6}{U}{U}: Monstrosity 4. (If this creature isn\'t monstrous, put four +1/+1 counters on it and it becomes monstrous.)\nWhen Shipbreaker Kraken becomes monstrous, tap up to four target creatures. Those creatures don\'t untap during their controllers\' untap steps for as long as you control Shipbreaker Kraken.').
card_image_name('shipbreaker kraken'/'THS', 'shipbreaker kraken').
card_uid('shipbreaker kraken'/'THS', 'THS:Shipbreaker Kraken:shipbreaker kraken').
card_rarity('shipbreaker kraken'/'THS', 'Rare').
card_artist('shipbreaker kraken'/'THS', 'Jack Wang').
card_number('shipbreaker kraken'/'THS', '63').
card_multiverse_id('shipbreaker kraken'/'THS', '373637').

card_in_set('shipwreck singer', 'THS').
card_original_type('shipwreck singer'/'THS', 'Creature — Siren').
card_original_text('shipwreck singer'/'THS', 'Flying\n{1}{U}: Target creature an opponent controls attacks this turn if able.\n{1}{B}, {T}: Attacking creatures get -1/-1 until end of turn.').
card_first_print('shipwreck singer', 'THS').
card_image_name('shipwreck singer'/'THS', 'shipwreck singer').
card_uid('shipwreck singer'/'THS', 'THS:Shipwreck Singer:shipwreck singer').
card_rarity('shipwreck singer'/'THS', 'Uncommon').
card_artist('shipwreck singer'/'THS', 'Daarken').
card_number('shipwreck singer'/'THS', '203').
card_flavor_text('shipwreck singer'/'THS', 'Her melody melds death and beauty with such artistry that even the gods weep to hear it.').
card_multiverse_id('shipwreck singer'/'THS', '373702').

card_in_set('shredding winds', 'THS').
card_original_type('shredding winds'/'THS', 'Instant').
card_original_text('shredding winds'/'THS', 'Shredding Winds deals 7 damage to target creature with flying.').
card_first_print('shredding winds', 'THS').
card_image_name('shredding winds'/'THS', 'shredding winds').
card_uid('shredding winds'/'THS', 'THS:Shredding Winds:shredding winds').
card_rarity('shredding winds'/'THS', 'Common').
card_artist('shredding winds'/'THS', 'Christopher Moeller').
card_number('shredding winds'/'THS', '178').
card_flavor_text('shredding winds'/'THS', '\"Enemies of the wood! Your presence here is a slap in Nylea\'s face. Do not be surprised if she slaps back.\"\n—Telphe, druid of Nylea').
card_multiverse_id('shredding winds'/'THS', '373676').

card_in_set('silent artisan', 'THS').
card_original_type('silent artisan'/'THS', 'Creature — Giant').
card_original_text('silent artisan'/'THS', '').
card_first_print('silent artisan', 'THS').
card_image_name('silent artisan'/'THS', 'silent artisan').
card_uid('silent artisan'/'THS', 'THS:Silent Artisan:silent artisan').
card_rarity('silent artisan'/'THS', 'Common').
card_artist('silent artisan'/'THS', 'Anthony Palumbo').
card_number('silent artisan'/'THS', '31').
card_flavor_text('silent artisan'/'THS', 'On the fourth day they passed through a forest of immense stacked stones. Althemone, youngest of the companions, called these pillars the work of a god, but the Champion knew better. She quickened her pace.\n—The Theriad').
card_multiverse_id('silent artisan'/'THS', '373573').

card_in_set('sip of hemlock', 'THS').
card_original_type('sip of hemlock'/'THS', 'Sorcery').
card_original_text('sip of hemlock'/'THS', 'Destroy target creature. Its controller loses 2 life.').
card_first_print('sip of hemlock', 'THS').
card_image_name('sip of hemlock'/'THS', 'sip of hemlock').
card_uid('sip of hemlock'/'THS', 'THS:Sip of Hemlock:sip of hemlock').
card_rarity('sip of hemlock'/'THS', 'Common').
card_artist('sip of hemlock'/'THS', 'Nils Hamm').
card_number('sip of hemlock'/'THS', '106').
card_flavor_text('sip of hemlock'/'THS', 'Conspirators poisoned the oracle not because her visions were wrong, but because they were right.').
card_multiverse_id('sip of hemlock'/'THS', '373598').

card_in_set('soldier of the pantheon', 'THS').
card_original_type('soldier of the pantheon'/'THS', 'Creature — Human Soldier').
card_original_text('soldier of the pantheon'/'THS', 'Protection from multicolored\nWhenever an opponent casts a multicolored spell, you gain 1 life.').
card_first_print('soldier of the pantheon', 'THS').
card_image_name('soldier of the pantheon'/'THS', 'soldier of the pantheon').
card_uid('soldier of the pantheon'/'THS', 'THS:Soldier of the Pantheon:soldier of the pantheon').
card_rarity('soldier of the pantheon'/'THS', 'Rare').
card_artist('soldier of the pantheon'/'THS', 'Eric Deschamps').
card_number('soldier of the pantheon'/'THS', '32').
card_flavor_text('soldier of the pantheon'/'THS', '\"I hear the gods\' voices in my dreams each night, and I offer bloody trophies on their altars each day.\"').
card_multiverse_id('soldier of the pantheon'/'THS', '373529').

card_in_set('spark jolt', 'THS').
card_original_type('spark jolt'/'THS', 'Instant').
card_original_text('spark jolt'/'THS', 'Spark Jolt deals 1 damage to target creature or player. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('spark jolt', 'THS').
card_image_name('spark jolt'/'THS', 'spark jolt').
card_uid('spark jolt'/'THS', 'THS:Spark Jolt:spark jolt').
card_rarity('spark jolt'/'THS', 'Common').
card_artist('spark jolt'/'THS', 'Mike Bierek').
card_number('spark jolt'/'THS', '140').
card_flavor_text('spark jolt'/'THS', 'Acolytes of Purphoros hammer the world until they see the sparks of change.').
card_multiverse_id('spark jolt'/'THS', '373618').

card_in_set('spear of heliod', 'THS').
card_original_type('spear of heliod'/'THS', 'Legendary Enchantment Artifact').
card_original_text('spear of heliod'/'THS', 'Creatures you control get +1/+1.\n{1}{W}{W}, {T}: Destroy target creature that dealt damage to you this turn.').
card_first_print('spear of heliod', 'THS').
card_image_name('spear of heliod'/'THS', 'spear of heliod').
card_uid('spear of heliod'/'THS', 'THS:Spear of Heliod:spear of heliod').
card_rarity('spear of heliod'/'THS', 'Rare').
card_artist('spear of heliod'/'THS', 'Yeong-Hao Han').
card_number('spear of heliod'/'THS', '33').
card_flavor_text('spear of heliod'/'THS', 'Legend speaks of the Sun Spear, the mighty weapon that can strike any point in Theros, even the depths of the Underworld.').
card_multiverse_id('spear of heliod'/'THS', '373717').

card_in_set('spearpoint oread', 'THS').
card_original_type('spearpoint oread'/'THS', 'Enchantment Creature — Nymph').
card_original_text('spearpoint oread'/'THS', 'Bestow {5}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFirst strike\nEnchanted creature gets +2/+2 and has first strike.').
card_first_print('spearpoint oread', 'THS').
card_image_name('spearpoint oread'/'THS', 'spearpoint oread').
card_uid('spearpoint oread'/'THS', 'THS:Spearpoint Oread:spearpoint oread').
card_rarity('spearpoint oread'/'THS', 'Common').
card_artist('spearpoint oread'/'THS', 'Todd Lockwood').
card_number('spearpoint oread'/'THS', '141').
card_multiverse_id('spearpoint oread'/'THS', '373732').

card_in_set('spellheart chimera', 'THS').
card_original_type('spellheart chimera'/'THS', 'Creature — Chimera').
card_original_text('spellheart chimera'/'THS', 'Flying, trample\nSpellheart Chimera\'s power is equal to the number of instant and sorcery cards in your graveyard.').
card_first_print('spellheart chimera', 'THS').
card_image_name('spellheart chimera'/'THS', 'spellheart chimera').
card_uid('spellheart chimera'/'THS', 'THS:Spellheart Chimera:spellheart chimera').
card_rarity('spellheart chimera'/'THS', 'Uncommon').
card_artist('spellheart chimera'/'THS', 'Svetlin Velinov').
card_number('spellheart chimera'/'THS', '204').
card_flavor_text('spellheart chimera'/'THS', 'Thaumaturges remain silent around chimeras, lest their words conjure even stranger beasts.').
card_multiverse_id('spellheart chimera'/'THS', '373554').

card_in_set('staunch-hearted warrior', 'THS').
card_original_type('staunch-hearted warrior'/'THS', 'Creature — Human Warrior').
card_original_text('staunch-hearted warrior'/'THS', 'Heroic — Whenever you cast a spell that targets Staunch-Hearted Warrior, put two +1/+1 counters on Staunch-Hearted Warrior.').
card_first_print('staunch-hearted warrior', 'THS').
card_image_name('staunch-hearted warrior'/'THS', 'staunch-hearted warrior').
card_uid('staunch-hearted warrior'/'THS', 'THS:Staunch-Hearted Warrior:staunch-hearted warrior').
card_rarity('staunch-hearted warrior'/'THS', 'Common').
card_artist('staunch-hearted warrior'/'THS', 'Greg Staples').
card_number('staunch-hearted warrior'/'THS', '179').
card_flavor_text('staunch-hearted warrior'/'THS', 'As soon as she faces a monster, she begins composing its epitaph.').
card_multiverse_id('staunch-hearted warrior'/'THS', '373591').

card_in_set('steam augury', 'THS').
card_original_type('steam augury'/'THS', 'Instant').
card_original_text('steam augury'/'THS', 'Reveal the top five cards of your library and separate them into two piles. An opponent chooses one of those piles. Put that pile into your hand and the other into your graveyard.').
card_first_print('steam augury', 'THS').
card_image_name('steam augury'/'THS', 'steam augury').
card_uid('steam augury'/'THS', 'THS:Steam Augury:steam augury').
card_rarity('steam augury'/'THS', 'Rare').
card_artist('steam augury'/'THS', 'Dave Kendall').
card_number('steam augury'/'THS', '205').
card_flavor_text('steam augury'/'THS', 'Keranos is a fickle god, delivering punishment as readily as prophecy.').
card_multiverse_id('steam augury'/'THS', '373539').

card_in_set('stoneshock giant', 'THS').
card_original_type('stoneshock giant'/'THS', 'Creature — Giant').
card_original_text('stoneshock giant'/'THS', '{6}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Stoneshock Giant becomes monstrous, creatures without flying your opponents control can\'t block this turn.').
card_first_print('stoneshock giant', 'THS').
card_image_name('stoneshock giant'/'THS', 'stoneshock giant').
card_uid('stoneshock giant'/'THS', 'THS:Stoneshock Giant:stoneshock giant').
card_rarity('stoneshock giant'/'THS', 'Uncommon').
card_artist('stoneshock giant'/'THS', 'Lars Grant-West').
card_number('stoneshock giant'/'THS', '142').
card_multiverse_id('stoneshock giant'/'THS', '373687').

card_in_set('stormbreath dragon', 'THS').
card_original_type('stormbreath dragon'/'THS', 'Creature — Dragon').
card_original_text('stormbreath dragon'/'THS', 'Flying, haste, protection from white\n{5}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Stormbreath Dragon becomes monstrous, it deals damage to each opponent equal to the number of cards in that player\'s hand.').
card_first_print('stormbreath dragon', 'THS').
card_image_name('stormbreath dragon'/'THS', 'stormbreath dragon').
card_uid('stormbreath dragon'/'THS', 'THS:Stormbreath Dragon:stormbreath dragon').
card_rarity('stormbreath dragon'/'THS', 'Mythic Rare').
card_artist('stormbreath dragon'/'THS', 'Slawomir Maniak').
card_number('stormbreath dragon'/'THS', '143').
card_multiverse_id('stormbreath dragon'/'THS', '373679').

card_in_set('stymied hopes', 'THS').
card_original_type('stymied hopes'/'THS', 'Instant').
card_original_text('stymied hopes'/'THS', 'Counter target spell unless its controller pays {1}. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('stymied hopes', 'THS').
card_image_name('stymied hopes'/'THS', 'stymied hopes').
card_uid('stymied hopes'/'THS', 'THS:Stymied Hopes:stymied hopes').
card_rarity('stymied hopes'/'THS', 'Common').
card_artist('stymied hopes'/'THS', 'Peter Mohrbacher').
card_number('stymied hopes'/'THS', '64').
card_flavor_text('stymied hopes'/'THS', 'When a god\'s will is against you, the day never ends well.').
card_multiverse_id('stymied hopes'/'THS', '373721').

card_in_set('swamp', 'THS').
card_original_type('swamp'/'THS', 'Basic Land — Swamp').
card_original_text('swamp'/'THS', 'B').
card_image_name('swamp'/'THS', 'swamp1').
card_uid('swamp'/'THS', 'THS:Swamp:swamp1').
card_rarity('swamp'/'THS', 'Basic Land').
card_artist('swamp'/'THS', 'Rob Alexander').
card_number('swamp'/'THS', '238').
card_multiverse_id('swamp'/'THS', '373608').

card_in_set('swamp', 'THS').
card_original_type('swamp'/'THS', 'Basic Land — Swamp').
card_original_text('swamp'/'THS', 'B').
card_image_name('swamp'/'THS', 'swamp2').
card_uid('swamp'/'THS', 'THS:Swamp:swamp2').
card_rarity('swamp'/'THS', 'Basic Land').
card_artist('swamp'/'THS', 'Steven Belledin').
card_number('swamp'/'THS', '239').
card_multiverse_id('swamp'/'THS', '373567').

card_in_set('swamp', 'THS').
card_original_type('swamp'/'THS', 'Basic Land — Swamp').
card_original_text('swamp'/'THS', 'B').
card_image_name('swamp'/'THS', 'swamp3').
card_uid('swamp'/'THS', 'THS:Swamp:swamp3').
card_rarity('swamp'/'THS', 'Basic Land').
card_artist('swamp'/'THS', 'Adam Paquette').
card_number('swamp'/'THS', '240').
card_multiverse_id('swamp'/'THS', '373706').

card_in_set('swamp', 'THS').
card_original_type('swamp'/'THS', 'Basic Land — Swamp').
card_original_text('swamp'/'THS', 'B').
card_image_name('swamp'/'THS', 'swamp4').
card_uid('swamp'/'THS', 'THS:Swamp:swamp4').
card_rarity('swamp'/'THS', 'Basic Land').
card_artist('swamp'/'THS', 'Raoul Vitale').
card_number('swamp'/'THS', '241').
card_multiverse_id('swamp'/'THS', '373681').

card_in_set('swan song', 'THS').
card_original_type('swan song'/'THS', 'Instant').
card_original_text('swan song'/'THS', 'Counter target enchantment, instant, or sorcery spell. Its controller puts a 2/2 blue Bird creature token with flying onto the battlefield.').
card_first_print('swan song', 'THS').
card_image_name('swan song'/'THS', 'swan song').
card_uid('swan song'/'THS', 'THS:Swan Song:swan song').
card_rarity('swan song'/'THS', 'Rare').
card_artist('swan song'/'THS', 'Peter Mohrbacher').
card_number('swan song'/'THS', '65').
card_flavor_text('swan song'/'THS', '\"The most enlightened mages create beauty from violence.\"\n—Medomai the Ageless').
card_multiverse_id('swan song'/'THS', '373701').

card_in_set('sylvan caryatid', 'THS').
card_original_type('sylvan caryatid'/'THS', 'Creature — Plant').
card_original_text('sylvan caryatid'/'THS', 'Defender, hexproof\n{T}: Add one mana of any color to your mana pool.').
card_image_name('sylvan caryatid'/'THS', 'sylvan caryatid').
card_uid('sylvan caryatid'/'THS', 'THS:Sylvan Caryatid:sylvan caryatid').
card_rarity('sylvan caryatid'/'THS', 'Rare').
card_artist('sylvan caryatid'/'THS', 'Chase Stone').
card_number('sylvan caryatid'/'THS', '180').
card_flavor_text('sylvan caryatid'/'THS', 'Those who enter the copse never leave. They find peace there and take root, becoming part of the ever-growing grove.').
card_multiverse_id('sylvan caryatid'/'THS', '373624').

card_in_set('temple of abandon', 'THS').
card_original_type('temple of abandon'/'THS', 'Land').
card_original_text('temple of abandon'/'THS', 'Temple of Abandon enters the battlefield tapped.\nWhen Temple of Abandon enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('temple of abandon', 'THS').
card_image_name('temple of abandon'/'THS', 'temple of abandon').
card_uid('temple of abandon'/'THS', 'THS:Temple of Abandon:temple of abandon').
card_rarity('temple of abandon'/'THS', 'Rare').
card_artist('temple of abandon'/'THS', 'Mike Bierek').
card_number('temple of abandon'/'THS', '224').
card_multiverse_id('temple of abandon'/'THS', '373711').

card_in_set('temple of deceit', 'THS').
card_original_type('temple of deceit'/'THS', 'Land').
card_original_text('temple of deceit'/'THS', 'Temple of Deceit enters the battlefield tapped.\nWhen Temple of Deceit enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('temple of deceit', 'THS').
card_image_name('temple of deceit'/'THS', 'temple of deceit').
card_uid('temple of deceit'/'THS', 'THS:Temple of Deceit:temple of deceit').
card_rarity('temple of deceit'/'THS', 'Rare').
card_artist('temple of deceit'/'THS', 'Raymond Swanland').
card_number('temple of deceit'/'THS', '225').
card_multiverse_id('temple of deceit'/'THS', '373734').

card_in_set('temple of mystery', 'THS').
card_original_type('temple of mystery'/'THS', 'Land').
card_original_text('temple of mystery'/'THS', 'Temple of Mystery enters the battlefield tapped.\nWhen Temple of Mystery enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {G} or {U} to your mana pool.').
card_first_print('temple of mystery', 'THS').
card_image_name('temple of mystery'/'THS', 'temple of mystery').
card_uid('temple of mystery'/'THS', 'THS:Temple of Mystery:temple of mystery').
card_rarity('temple of mystery'/'THS', 'Rare').
card_artist('temple of mystery'/'THS', 'Noah Bradley').
card_number('temple of mystery'/'THS', '226').
card_multiverse_id('temple of mystery'/'THS', '373571').

card_in_set('temple of silence', 'THS').
card_original_type('temple of silence'/'THS', 'Land').
card_original_text('temple of silence'/'THS', 'Temple of Silence enters the battlefield tapped.\nWhen Temple of Silence enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {W} or {B} to your mana pool.').
card_first_print('temple of silence', 'THS').
card_image_name('temple of silence'/'THS', 'temple of silence').
card_uid('temple of silence'/'THS', 'THS:Temple of Silence:temple of silence').
card_rarity('temple of silence'/'THS', 'Rare').
card_artist('temple of silence'/'THS', 'Karl Kopinski').
card_number('temple of silence'/'THS', '227').
card_multiverse_id('temple of silence'/'THS', '373522').

card_in_set('temple of triumph', 'THS').
card_original_type('temple of triumph'/'THS', 'Land').
card_original_text('temple of triumph'/'THS', 'Temple of Triumph enters the battlefield tapped.\nWhen Temple of Triumph enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {R} or {W} to your mana pool.').
card_first_print('temple of triumph', 'THS').
card_image_name('temple of triumph'/'THS', 'temple of triumph').
card_uid('temple of triumph'/'THS', 'THS:Temple of Triumph:temple of triumph').
card_rarity('temple of triumph'/'THS', 'Rare').
card_artist('temple of triumph'/'THS', 'Jason Felix').
card_number('temple of triumph'/'THS', '228').
card_multiverse_id('temple of triumph'/'THS', '373560').

card_in_set('thassa\'s bounty', 'THS').
card_original_type('thassa\'s bounty'/'THS', 'Sorcery').
card_original_text('thassa\'s bounty'/'THS', 'Draw three cards. Target player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('thassa\'s bounty', 'THS').
card_image_name('thassa\'s bounty'/'THS', 'thassa\'s bounty').
card_uid('thassa\'s bounty'/'THS', 'THS:Thassa\'s Bounty:thassa\'s bounty').
card_rarity('thassa\'s bounty'/'THS', 'Common').
card_artist('thassa\'s bounty'/'THS', 'Ryan Yee').
card_number('thassa\'s bounty'/'THS', '67').
card_flavor_text('thassa\'s bounty'/'THS', '\"Was this gift cast adrift for any to find, or did Thassa guide the currents to bring it to me alone?\"\n—Kenessos, priest of Thassa').
card_multiverse_id('thassa\'s bounty'/'THS', '373662').

card_in_set('thassa\'s emissary', 'THS').
card_original_type('thassa\'s emissary'/'THS', 'Enchantment Creature — Crab').
card_original_text('thassa\'s emissary'/'THS', 'Bestow {5}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhenever Thassa\'s Emissary or enchanted creature deals combat damage to a player, draw a card.\nEnchanted creature gets +3/+3.').
card_first_print('thassa\'s emissary', 'THS').
card_image_name('thassa\'s emissary'/'THS', 'thassa\'s emissary').
card_uid('thassa\'s emissary'/'THS', 'THS:Thassa\'s Emissary:thassa\'s emissary').
card_rarity('thassa\'s emissary'/'THS', 'Uncommon').
card_artist('thassa\'s emissary'/'THS', 'Sam Burley').
card_number('thassa\'s emissary'/'THS', '68').
card_multiverse_id('thassa\'s emissary'/'THS', '373735').

card_in_set('thassa, god of the sea', 'THS').
card_original_type('thassa, god of the sea'/'THS', 'Legendary Enchantment Creature — God').
card_original_text('thassa, god of the sea'/'THS', 'Indestructible\nAs long as your devotion to blue is less than five, Thassa isn\'t a creature. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)\nAt the beginning of your upkeep, scry 1.\n{1}{U}: Target creature you control can\'t be blocked this turn.').
card_first_print('thassa, god of the sea', 'THS').
card_image_name('thassa, god of the sea'/'THS', 'thassa, god of the sea').
card_uid('thassa, god of the sea'/'THS', 'THS:Thassa, God of the Sea:thassa, god of the sea').
card_rarity('thassa, god of the sea'/'THS', 'Mythic Rare').
card_artist('thassa, god of the sea'/'THS', 'Jason Chan').
card_number('thassa, god of the sea'/'THS', '66').
card_multiverse_id('thassa, god of the sea'/'THS', '373535').

card_in_set('thoughtseize', 'THS').
card_original_type('thoughtseize'/'THS', 'Sorcery').
card_original_text('thoughtseize'/'THS', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card. You lose 2 life.').
card_image_name('thoughtseize'/'THS', 'thoughtseize').
card_uid('thoughtseize'/'THS', 'THS:Thoughtseize:thoughtseize').
card_rarity('thoughtseize'/'THS', 'Rare').
card_artist('thoughtseize'/'THS', 'Lucas Graciano').
card_number('thoughtseize'/'THS', '107').
card_flavor_text('thoughtseize'/'THS', '\"Knowledge is such a burden. Release it. Release all your fears to me.\"\n—Ashiok, Nightmare Weaver').
card_multiverse_id('thoughtseize'/'THS', '373632').

card_in_set('time to feed', 'THS').
card_original_type('time to feed'/'THS', 'Sorcery').
card_original_text('time to feed'/'THS', 'Choose target creature an opponent controls. When that creature dies this turn, you gain 3 life. Target creature you control fights that\ncreature. (Each deals damage equal to its power to the other.)').
card_first_print('time to feed', 'THS').
card_image_name('time to feed'/'THS', 'time to feed').
card_uid('time to feed'/'THS', 'THS:Time to Feed:time to feed').
card_rarity('time to feed'/'THS', 'Common').
card_artist('time to feed'/'THS', 'Wayne Reynolds').
card_number('time to feed'/'THS', '181').
card_multiverse_id('time to feed'/'THS', '373633').

card_in_set('titan of eternal fire', 'THS').
card_original_type('titan of eternal fire'/'THS', 'Creature — Giant').
card_original_text('titan of eternal fire'/'THS', 'Each Human creature you control has \"{R}, {T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('titan of eternal fire', 'THS').
card_image_name('titan of eternal fire'/'THS', 'titan of eternal fire').
card_uid('titan of eternal fire'/'THS', 'THS:Titan of Eternal Fire:titan of eternal fire').
card_rarity('titan of eternal fire'/'THS', 'Rare').
card_artist('titan of eternal fire'/'THS', 'Aleksi Briclot').
card_number('titan of eternal fire'/'THS', '144').
card_flavor_text('titan of eternal fire'/'THS', 'There is no gift more precious or more perilous than fire.').
card_multiverse_id('titan of eternal fire'/'THS', '373630').

card_in_set('titan\'s strength', 'THS').
card_original_type('titan\'s strength'/'THS', 'Instant').
card_original_text('titan\'s strength'/'THS', 'Target creature gets +3/+1 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('titan\'s strength', 'THS').
card_image_name('titan\'s strength'/'THS', 'titan\'s strength').
card_uid('titan\'s strength'/'THS', 'THS:Titan\'s Strength:titan\'s strength').
card_rarity('titan\'s strength'/'THS', 'Common').
card_artist('titan\'s strength'/'THS', 'Karl Kopinski').
card_number('titan\'s strength'/'THS', '145').
card_multiverse_id('titan\'s strength'/'THS', '373607').

card_in_set('tormented hero', 'THS').
card_original_type('tormented hero'/'THS', 'Creature — Human Warrior').
card_original_text('tormented hero'/'THS', 'Tormented Hero enters the battlefield tapped.\nHeroic — Whenever you cast a spell that targets Tormented Hero, each opponent loses 1 life. You gain life equal to the life lost this way.').
card_image_name('tormented hero'/'THS', 'tormented hero').
card_uid('tormented hero'/'THS', 'THS:Tormented Hero:tormented hero').
card_rarity('tormented hero'/'THS', 'Uncommon').
card_artist('tormented hero'/'THS', 'Winona Nelson').
card_number('tormented hero'/'THS', '108').
card_multiverse_id('tormented hero'/'THS', '373541').

card_in_set('traveler\'s amulet', 'THS').
card_original_type('traveler\'s amulet'/'THS', 'Artifact').
card_original_text('traveler\'s amulet'/'THS', '{1}, Sacrifice Traveler\'s Amulet: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('traveler\'s amulet'/'THS', 'traveler\'s amulet').
card_uid('traveler\'s amulet'/'THS', 'THS:Traveler\'s Amulet:traveler\'s amulet').
card_rarity('traveler\'s amulet'/'THS', 'Common').
card_artist('traveler\'s amulet'/'THS', 'Franz Vohwinkel').
card_number('traveler\'s amulet'/'THS', '221').
card_flavor_text('traveler\'s amulet'/'THS', '\"What we call magic is nothing more than hope crystallized into a destination.\"\n—Perisophia the philosopher').
card_multiverse_id('traveler\'s amulet'/'THS', '373690').

card_in_set('traveling philosopher', 'THS').
card_original_type('traveling philosopher'/'THS', 'Creature — Human Advisor').
card_original_text('traveling philosopher'/'THS', '').
card_first_print('traveling philosopher', 'THS').
card_image_name('traveling philosopher'/'THS', 'traveling philosopher').
card_uid('traveling philosopher'/'THS', 'THS:Traveling Philosopher:traveling philosopher').
card_rarity('traveling philosopher'/'THS', 'Common').
card_artist('traveling philosopher'/'THS', 'James Ryman').
card_number('traveling philosopher'/'THS', '34').
card_flavor_text('traveling philosopher'/'THS', 'The Champion and the philosopher Olexa returned from the opposing camp at dusk. Behind them, the enemy raised sail and departed, breaking the siege. When asked what the two had done, the Champion replied, \"We spoke to them.\"\n—The Theriad').
card_multiverse_id('traveling philosopher'/'THS', '373512').

card_in_set('triad of fates', 'THS').
card_original_type('triad of fates'/'THS', 'Legendary Creature — Human Wizard').
card_original_text('triad of fates'/'THS', '{1}, {T}: Put a fate counter on another target creature.\n{W}, {T}: Exile target creature that has a fate counter on it, then return it to the battlefield under its owner\'s control.\n{B}, {T}: Exile target creature that has a fate counter on it. Its controller draws two cards.').
card_first_print('triad of fates', 'THS').
card_image_name('triad of fates'/'THS', 'triad of fates').
card_uid('triad of fates'/'THS', 'THS:Triad of Fates:triad of fates').
card_rarity('triad of fates'/'THS', 'Rare').
card_artist('triad of fates'/'THS', 'Daarken').
card_number('triad of fates'/'THS', '206').
card_multiverse_id('triad of fates'/'THS', '373685').

card_in_set('triton fortune hunter', 'THS').
card_original_type('triton fortune hunter'/'THS', 'Creature — Merfolk Soldier').
card_original_text('triton fortune hunter'/'THS', 'Heroic — Whenever you cast a spell that targets Triton Fortune Hunter, draw a card.').
card_first_print('triton fortune hunter', 'THS').
card_image_name('triton fortune hunter'/'THS', 'triton fortune hunter').
card_uid('triton fortune hunter'/'THS', 'THS:Triton Fortune Hunter:triton fortune hunter').
card_rarity('triton fortune hunter'/'THS', 'Uncommon').
card_artist('triton fortune hunter'/'THS', 'Clint Cearley').
card_number('triton fortune hunter'/'THS', '69').
card_flavor_text('triton fortune hunter'/'THS', '\"Thassa has blessed me with power and insight. I am careful not to disappoint her.\"').
card_multiverse_id('triton fortune hunter'/'THS', '373531').

card_in_set('triton shorethief', 'THS').
card_original_type('triton shorethief'/'THS', 'Creature — Merfolk Rogue').
card_original_text('triton shorethief'/'THS', '').
card_first_print('triton shorethief', 'THS').
card_image_name('triton shorethief'/'THS', 'triton shorethief').
card_uid('triton shorethief'/'THS', 'THS:Triton Shorethief:triton shorethief').
card_rarity('triton shorethief'/'THS', 'Common').
card_artist('triton shorethief'/'THS', 'Howard Lyon').
card_number('triton shorethief'/'THS', '70').
card_flavor_text('triton shorethief'/'THS', 'At sunrise, the Champion and her companions awoke to find their supplies gone and Brygus, their sentry, dead. Carefully arranged piles of ornamental shells gave a clear warning: go no further.\n—The Theriad').
card_multiverse_id('triton shorethief'/'THS', '373551').

card_in_set('triton tactics', 'THS').
card_original_type('triton tactics'/'THS', 'Instant').
card_original_text('triton tactics'/'THS', 'Up to two target creatures each get +0/+3 until end of turn. Untap those creatures. At this turn\'s next end of combat, tap each creature that was blocked by one of those creatures this turn and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('triton tactics', 'THS').
card_image_name('triton tactics'/'THS', 'triton tactics').
card_uid('triton tactics'/'THS', 'THS:Triton Tactics:triton tactics').
card_rarity('triton tactics'/'THS', 'Uncommon').
card_artist('triton tactics'/'THS', 'Jack Wang').
card_number('triton tactics'/'THS', '71').
card_multiverse_id('triton tactics'/'THS', '373639').

card_in_set('two-headed cerberus', 'THS').
card_original_type('two-headed cerberus'/'THS', 'Creature — Hound').
card_original_text('two-headed cerberus'/'THS', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_first_print('two-headed cerberus', 'THS').
card_image_name('two-headed cerberus'/'THS', 'two-headed cerberus').
card_uid('two-headed cerberus'/'THS', 'THS:Two-Headed Cerberus:two-headed cerberus').
card_rarity('two-headed cerberus'/'THS', 'Common').
card_artist('two-headed cerberus'/'THS', 'Karl Kopinski').
card_number('two-headed cerberus'/'THS', '146').
card_flavor_text('two-headed cerberus'/'THS', 'The left head keeps the right head starved as motivation to track new prey.').
card_multiverse_id('two-headed cerberus'/'THS', '373540').

card_in_set('tymaret, the murder king', 'THS').
card_original_type('tymaret, the murder king'/'THS', 'Legendary Creature — Zombie Warrior').
card_original_text('tymaret, the murder king'/'THS', '{1}{R}, Sacrifice another creature: Tymaret, the Murder King deals 2 damage to target player.\n{1}{B}, Sacrifice a creature: Return Tymaret from your graveyard to your hand.').
card_first_print('tymaret, the murder king', 'THS').
card_image_name('tymaret, the murder king'/'THS', 'tymaret, the murder king').
card_uid('tymaret, the murder king'/'THS', 'THS:Tymaret, the Murder King:tymaret, the murder king').
card_rarity('tymaret, the murder king'/'THS', 'Rare').
card_artist('tymaret, the murder king'/'THS', 'Volkan Baga').
card_number('tymaret, the murder king'/'THS', '207').
card_flavor_text('tymaret, the murder king'/'THS', 'His memories remained in the Underworld, but his cruelty crossed the Rivers with him.').
card_multiverse_id('tymaret, the murder king'/'THS', '373665').

card_in_set('underworld cerberus', 'THS').
card_original_type('underworld cerberus'/'THS', 'Creature — Hound').
card_original_text('underworld cerberus'/'THS', 'Underworld Cerberus can\'t be blocked except by three or more creatures.\nCards in graveyards can\'t be the targets of spells or abilities.\nWhen Underworld Cerberus dies, exile it and each player returns all creature cards from his or her graveyard to his or her hand.').
card_first_print('underworld cerberus', 'THS').
card_image_name('underworld cerberus'/'THS', 'underworld cerberus').
card_uid('underworld cerberus'/'THS', 'THS:Underworld Cerberus:underworld cerberus').
card_rarity('underworld cerberus'/'THS', 'Mythic Rare').
card_artist('underworld cerberus'/'THS', 'Svetlin Velinov').
card_number('underworld cerberus'/'THS', '208').
card_multiverse_id('underworld cerberus'/'THS', '373666').

card_in_set('unknown shores', 'THS').
card_original_type('unknown shores'/'THS', 'Land').
card_original_text('unknown shores'/'THS', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('unknown shores', 'THS').
card_image_name('unknown shores'/'THS', 'unknown shores').
card_uid('unknown shores'/'THS', 'THS:Unknown Shores:unknown shores').
card_rarity('unknown shores'/'THS', 'Common').
card_artist('unknown shores'/'THS', 'Seb McKinnon').
card_number('unknown shores'/'THS', '229').
card_flavor_text('unknown shores'/'THS', 'Philosophers speak of a place where myths wash like tides upon the shores of the real.').
card_multiverse_id('unknown shores'/'THS', '373743').

card_in_set('vanquish the foul', 'THS').
card_original_type('vanquish the foul'/'THS', 'Sorcery').
card_original_text('vanquish the foul'/'THS', 'Destroy target creature with power 4 or greater. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('vanquish the foul', 'THS').
card_image_name('vanquish the foul'/'THS', 'vanquish the foul').
card_uid('vanquish the foul'/'THS', 'THS:Vanquish the Foul:vanquish the foul').
card_rarity('vanquish the foul'/'THS', 'Uncommon').
card_artist('vanquish the foul'/'THS', 'Eric Deschamps').
card_number('vanquish the foul'/'THS', '35').
card_multiverse_id('vanquish the foul'/'THS', '373640').

card_in_set('vaporkin', 'THS').
card_original_type('vaporkin'/'THS', 'Creature — Elemental').
card_original_text('vaporkin'/'THS', 'Flying\nVaporkin can block only creatures with flying.').
card_first_print('vaporkin', 'THS').
card_image_name('vaporkin'/'THS', 'vaporkin').
card_uid('vaporkin'/'THS', 'THS:Vaporkin:vaporkin').
card_rarity('vaporkin'/'THS', 'Common').
card_artist('vaporkin'/'THS', 'Seb McKinnon').
card_number('vaporkin'/'THS', '72').
card_flavor_text('vaporkin'/'THS', '\"Mists are carefree. They drift where they will, unencumbered by rocks and river beds.\"\n—Thrasios, triton hero').
card_multiverse_id('vaporkin'/'THS', '373547').

card_in_set('viper\'s kiss', 'THS').
card_original_type('viper\'s kiss'/'THS', 'Enchantment — Aura').
card_original_text('viper\'s kiss'/'THS', 'Enchant creature\nEnchanted creature gets -1/-1, and its activated abilities can\'t be activated.').
card_first_print('viper\'s kiss', 'THS').
card_image_name('viper\'s kiss'/'THS', 'viper\'s kiss').
card_uid('viper\'s kiss'/'THS', 'THS:Viper\'s Kiss:viper\'s kiss').
card_rarity('viper\'s kiss'/'THS', 'Common').
card_artist('viper\'s kiss'/'THS', 'Svetlin Velinov').
card_number('viper\'s kiss'/'THS', '109').
card_flavor_text('viper\'s kiss'/'THS', '\"I could heal a snakebite. Looks like you got bitten by a constellation.\"\n—Aescalos, healer of Pharika').
card_multiverse_id('viper\'s kiss'/'THS', '373499').

card_in_set('voyage\'s end', 'THS').
card_original_type('voyage\'s end'/'THS', 'Instant').
card_original_text('voyage\'s end'/'THS', 'Return target creature to its owner\'s hand. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('voyage\'s end', 'THS').
card_image_name('voyage\'s end'/'THS', 'voyage\'s end').
card_uid('voyage\'s end'/'THS', 'THS:Voyage\'s End:voyage\'s end').
card_rarity('voyage\'s end'/'THS', 'Common').
card_artist('voyage\'s end'/'THS', 'Chris Rahn').
card_number('voyage\'s end'/'THS', '73').
card_flavor_text('voyage\'s end'/'THS', 'Philosophers say those lost at sea ascended to a more perfect realm. Sailors say they drowned.').
card_multiverse_id('voyage\'s end'/'THS', '373527').

card_in_set('voyaging satyr', 'THS').
card_original_type('voyaging satyr'/'THS', 'Creature — Satyr Druid').
card_original_text('voyaging satyr'/'THS', '{T}: Untap target land.').
card_first_print('voyaging satyr', 'THS').
card_image_name('voyaging satyr'/'THS', 'voyaging satyr').
card_uid('voyaging satyr'/'THS', 'THS:Voyaging Satyr:voyaging satyr').
card_rarity('voyaging satyr'/'THS', 'Common').
card_artist('voyaging satyr'/'THS', 'Tyler Jacobson').
card_number('voyaging satyr'/'THS', '182').
card_flavor_text('voyaging satyr'/'THS', '\"None can own the land\'s bounty. The gods made this world for all to share its riches. And I\'m not just saying that because you caught me stealing your fruit.\"').
card_multiverse_id('voyaging satyr'/'THS', '373518').

card_in_set('vulpine goliath', 'THS').
card_original_type('vulpine goliath'/'THS', 'Creature — Fox').
card_original_text('vulpine goliath'/'THS', 'Trample').
card_first_print('vulpine goliath', 'THS').
card_image_name('vulpine goliath'/'THS', 'vulpine goliath').
card_uid('vulpine goliath'/'THS', 'THS:Vulpine Goliath:vulpine goliath').
card_rarity('vulpine goliath'/'THS', 'Common').
card_artist('vulpine goliath'/'THS', 'Adam Paquette').
card_number('vulpine goliath'/'THS', '183').
card_flavor_text('vulpine goliath'/'THS', '\"With a diet of hydras, giants, and massive serpents, anything would get that big.\"\n—Corisande, Setessan hunter').
card_multiverse_id('vulpine goliath'/'THS', '373655').

card_in_set('warriors\' lesson', 'THS').
card_original_type('warriors\' lesson'/'THS', 'Instant').
card_original_text('warriors\' lesson'/'THS', 'Until end of turn, up to two target creatures you control each gain \"Whenever this creature deals combat damage to a player, draw a card.\"').
card_first_print('warriors\' lesson', 'THS').
card_image_name('warriors\' lesson'/'THS', 'warriors\' lesson').
card_uid('warriors\' lesson'/'THS', 'THS:Warriors\' Lesson:warriors\' lesson').
card_rarity('warriors\' lesson'/'THS', 'Uncommon').
card_artist('warriors\' lesson'/'THS', 'Steve Prescott').
card_number('warriors\' lesson'/'THS', '184').
card_flavor_text('warriors\' lesson'/'THS', '\"Let each challenge make you a better warrior.\"\n—Anthousa of Setessa').
card_multiverse_id('warriors\' lesson'/'THS', '373613').

card_in_set('wavecrash triton', 'THS').
card_original_type('wavecrash triton'/'THS', 'Creature — Merfolk Wizard').
card_original_text('wavecrash triton'/'THS', 'Heroic — Whenever you cast a spell that targets Wavecrash Triton, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_first_print('wavecrash triton', 'THS').
card_image_name('wavecrash triton'/'THS', 'wavecrash triton').
card_uid('wavecrash triton'/'THS', 'THS:Wavecrash Triton:wavecrash triton').
card_rarity('wavecrash triton'/'THS', 'Common').
card_artist('wavecrash triton'/'THS', 'Ryan Barger').
card_number('wavecrash triton'/'THS', '74').
card_multiverse_id('wavecrash triton'/'THS', '373534').

card_in_set('whip of erebos', 'THS').
card_original_type('whip of erebos'/'THS', 'Legendary Enchantment Artifact').
card_original_text('whip of erebos'/'THS', 'Creatures you control have lifelink.\n{2}{B}{B}, {T}: Return target creature card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step. If it would leave the battlefield, exile it instead of putting it anywhere else. Activate this ability only any time you could cast a sorcery.').
card_first_print('whip of erebos', 'THS').
card_image_name('whip of erebos'/'THS', 'whip of erebos').
card_uid('whip of erebos'/'THS', 'THS:Whip of Erebos:whip of erebos').
card_rarity('whip of erebos'/'THS', 'Rare').
card_artist('whip of erebos'/'THS', 'Yeong-Hao Han').
card_number('whip of erebos'/'THS', '110').
card_multiverse_id('whip of erebos'/'THS', '373709').

card_in_set('wild celebrants', 'THS').
card_original_type('wild celebrants'/'THS', 'Creature — Satyr').
card_original_text('wild celebrants'/'THS', 'When Wild Celebrants enters the battlefield, you may destroy target artifact.').
card_first_print('wild celebrants', 'THS').
card_image_name('wild celebrants'/'THS', 'wild celebrants').
card_uid('wild celebrants'/'THS', 'THS:Wild Celebrants:wild celebrants').
card_rarity('wild celebrants'/'THS', 'Common').
card_artist('wild celebrants'/'THS', 'Igor Kieryluk').
card_number('wild celebrants'/'THS', '147').
card_flavor_text('wild celebrants'/'THS', '\"You can tell something\'s really valuable by the sound it makes when you slam your staff into it.\"').
card_multiverse_id('wild celebrants'/'THS', '373526').

card_in_set('wingsteed rider', 'THS').
card_original_type('wingsteed rider'/'THS', 'Creature — Human Knight').
card_original_text('wingsteed rider'/'THS', 'Flying\nHeroic — Whenever you cast a spell that targets Wingsteed Rider, put a +1/+1 counter on Wingsteed Rider.').
card_first_print('wingsteed rider', 'THS').
card_image_name('wingsteed rider'/'THS', 'wingsteed rider').
card_uid('wingsteed rider'/'THS', 'THS:Wingsteed Rider:wingsteed rider').
card_rarity('wingsteed rider'/'THS', 'Common').
card_artist('wingsteed rider'/'THS', 'Cynthia Sheppard').
card_number('wingsteed rider'/'THS', '36').
card_flavor_text('wingsteed rider'/'THS', 'Trust is the only bridle a pegasus will accept.').
card_multiverse_id('wingsteed rider'/'THS', '373563').

card_in_set('witches\' eye', 'THS').
card_original_type('witches\' eye'/'THS', 'Artifact — Equipment').
card_original_text('witches\' eye'/'THS', 'Equipped creature has \"{1}, {T}: Scry 1.\" (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)\nEquip {1}').
card_first_print('witches\' eye', 'THS').
card_image_name('witches\' eye'/'THS', 'witches\' eye').
card_uid('witches\' eye'/'THS', 'THS:Witches\' Eye:witches\' eye').
card_rarity('witches\' eye'/'THS', 'Uncommon').
card_artist('witches\' eye'/'THS', 'Daniel Ljunggren').
card_number('witches\' eye'/'THS', '222').
card_flavor_text('witches\' eye'/'THS', 'The price of prophecy is a vacant eye socket.').
card_multiverse_id('witches\' eye'/'THS', '373622').

card_in_set('xenagos, the reveler', 'THS').
card_original_type('xenagos, the reveler'/'THS', 'Planeswalker — Xenagos').
card_original_text('xenagos, the reveler'/'THS', '+1: Add X mana in any combination of {R} and/or {G} to your mana pool, where X is the number of creatures you control.\n0: Put a 2/2 red and green Satyr creature token with haste onto the battlefield.\n-6: Exile the top seven cards of your library. You may put any number of creature and/or land cards from among them onto the battlefield.').
card_first_print('xenagos, the reveler', 'THS').
card_image_name('xenagos, the reveler'/'THS', 'xenagos, the reveler').
card_uid('xenagos, the reveler'/'THS', 'THS:Xenagos, the Reveler:xenagos, the reveler').
card_rarity('xenagos, the reveler'/'THS', 'Mythic Rare').
card_artist('xenagos, the reveler'/'THS', 'Jason Chan').
card_number('xenagos, the reveler'/'THS', '209').
card_multiverse_id('xenagos, the reveler'/'THS', '373502').

card_in_set('yoked ox', 'THS').
card_original_type('yoked ox'/'THS', 'Creature — Ox').
card_original_text('yoked ox'/'THS', '').
card_first_print('yoked ox', 'THS').
card_image_name('yoked ox'/'THS', 'yoked ox').
card_uid('yoked ox'/'THS', 'THS:Yoked Ox:yoked ox').
card_rarity('yoked ox'/'THS', 'Common').
card_artist('yoked ox'/'THS', 'Ryan Yee').
card_number('yoked ox'/'THS', '37').
card_flavor_text('yoked ox'/'THS', 'It was in fields of grain, not fields of battle, that the Champion learned to bear the yoke of duty to the gods. She worked the land long before she was called on to defend it.\n—The Theriad').
card_multiverse_id('yoked ox'/'THS', '373572').
