% Arena League

set('pARL').
set_name('pARL', 'Arena League').
set_release_date('pARL', '1996-08-02').
set_border('pARL', 'black').
set_type('pARL', 'promo').

card_in_set('arc lightning', 'pARL').
card_original_type('arc lightning'/'pARL', 'Sorcery').
card_original_text('arc lightning'/'pARL', '').
card_first_print('arc lightning', 'pARL').
card_image_name('arc lightning'/'pARL', 'arc lightning').
card_uid('arc lightning'/'pARL', 'pARL:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'pARL', 'Special').
card_artist('arc lightning'/'pARL', 'Andrew Goldhawk').
card_number('arc lightning'/'pARL', '42').
card_flavor_text('arc lightning'/'pARL', '\"It is vain to look for a defense against lightning.\"\n—Publius Syrus, Maxim 835, trans. Lyman').

card_in_set('ashnod\'s coupon', 'pARL').
card_original_type('ashnod\'s coupon'/'pARL', 'Artifact').
card_original_text('ashnod\'s coupon'/'pARL', '').
card_first_print('ashnod\'s coupon', 'pARL').
card_border('ashnod\'s coupon'/'pARL', 'silver').
card_image_name('ashnod\'s coupon'/'pARL', 'ashnod\'s coupon').
card_uid('ashnod\'s coupon'/'pARL', 'pARL:Ashnod\'s Coupon:ashnod\'s coupon').
card_rarity('ashnod\'s coupon'/'pARL', 'Special').
card_artist('ashnod\'s coupon'/'pARL', 'Edward P. Beard, Jr.').
card_number('ashnod\'s coupon'/'pARL', '66').
card_flavor_text('ashnod\'s coupon'/'pARL', 'Limited time offer. Void where prohibited. Limit one per purchase. Valid only in participating duels. This coupon is nontransferable and invalid if shattered, crumbled, detonated, pillaged, or otherwise disenchanted. Cash value less than 1/20 of a cent. Offer not valid in Quebec, Rhode Island, or where prohibited by law or the DCI.').

card_in_set('bonesplitter', 'pARL').
card_original_type('bonesplitter'/'pARL', 'Artifact — Equipment').
card_original_text('bonesplitter'/'pARL', '').
card_first_print('bonesplitter', 'pARL').
card_image_name('bonesplitter'/'pARL', 'bonesplitter').
card_uid('bonesplitter'/'pARL', 'pARL:Bonesplitter:bonesplitter').
card_rarity('bonesplitter'/'pARL', 'Special').
card_artist('bonesplitter'/'pARL', 'Darrell Riche').
card_number('bonesplitter'/'pARL', '52').

card_in_set('booster tutor', 'pARL').
card_original_type('booster tutor'/'pARL', 'Instant').
card_original_text('booster tutor'/'pARL', '').
card_first_print('booster tutor', 'pARL').
card_border('booster tutor'/'pARL', 'silver').
card_image_name('booster tutor'/'pARL', 'booster tutor').
card_uid('booster tutor'/'pARL', 'pARL:Booster Tutor:booster tutor').
card_rarity('booster tutor'/'pARL', 'Special').
card_artist('booster tutor'/'pARL', 'Heather Hudson').
card_number('booster tutor'/'pARL', '63').
card_flavor_text('booster tutor'/'pARL', 'Make your judgment wisely: The right pack will let you defeat your nemesis, but a poor choice can only end in torment. And Homelands.').

card_in_set('castigate', 'pARL').
card_original_type('castigate'/'pARL', 'Sorcery').
card_original_text('castigate'/'pARL', '').
card_first_print('castigate', 'pARL').
card_image_name('castigate'/'pARL', 'castigate').
card_uid('castigate'/'pARL', 'pARL:Castigate:castigate').
card_rarity('castigate'/'pARL', 'Special').
card_artist('castigate'/'pARL', 'Darrell Riche').
card_number('castigate'/'pARL', '80').
card_flavor_text('castigate'/'pARL', '\"We have no need for military might. We wield two of the sharpest swords ever forged: Faith in our left hand, Wealth in our right.\"\n—Vuliev of the Ghost Council').

card_in_set('chill', 'pARL').
card_original_type('chill'/'pARL', 'Enchantment').
card_original_text('chill'/'pARL', '').
card_first_print('chill', 'pARL').
card_image_name('chill'/'pARL', 'chill').
card_uid('chill'/'pARL', 'pARL:Chill:chill').
card_rarity('chill'/'pARL', 'Special').
card_artist('chill'/'pARL', 'Greg Simanson').
card_number('chill'/'pARL', '19').
card_flavor_text('chill'/'pARL', '\"Temper, temper.\"\n—Ertai, wizard adept').

card_in_set('circle of protection: art', 'pARL').
card_original_type('circle of protection: art'/'pARL', 'Enchantment').
card_original_text('circle of protection: art'/'pARL', '').
card_first_print('circle of protection: art', 'pARL').
card_border('circle of protection: art'/'pARL', 'silver').
card_image_name('circle of protection: art'/'pARL', 'circle of protection art').
card_uid('circle of protection: art'/'pARL', 'pARL:Circle of Protection: Art:circle of protection art').
card_rarity('circle of protection: art'/'pARL', 'Special').
card_artist('circle of protection: art'/'pARL', 'Mark “It’s teh-DEEN” Tedin').
card_number('circle of protection: art'/'pARL', '61').

card_in_set('coiling oracle', 'pARL').
card_original_type('coiling oracle'/'pARL', 'Creature — Snake Elf Druid').
card_original_text('coiling oracle'/'pARL', '').
card_first_print('coiling oracle', 'pARL').
card_image_name('coiling oracle'/'pARL', 'coiling oracle').
card_uid('coiling oracle'/'pARL', 'pARL:Coiling Oracle:coiling oracle').
card_rarity('coiling oracle'/'pARL', 'Special').
card_artist('coiling oracle'/'pARL', 'Mark Zug').
card_number('coiling oracle'/'pARL', '82').
card_flavor_text('coiling oracle'/'pARL', 'Snaking remnants of nature directed by a body of thought and progress, the oracles embody all that is Simic.').

card_in_set('creeping mold', 'pARL').
card_original_type('creeping mold'/'pARL', 'Sorcery').
card_original_text('creeping mold'/'pARL', '').
card_first_print('creeping mold', 'pARL').
card_image_name('creeping mold'/'pARL', 'creeping mold').
card_uid('creeping mold'/'pARL', 'pARL:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'pARL', 'Special').
card_artist('creeping mold'/'pARL', 'Dave Seeley').
card_number('creeping mold'/'pARL', '28').
card_flavor_text('creeping mold'/'pARL', '\"Mold could catch you.\"\n—Suq\'Ata insult').

card_in_set('darksteel ingot', 'pARL').
card_original_type('darksteel ingot'/'pARL', 'Artifact').
card_original_text('darksteel ingot'/'pARL', '').
card_first_print('darksteel ingot', 'pARL').
card_image_name('darksteel ingot'/'pARL', 'darksteel ingot').
card_uid('darksteel ingot'/'pARL', 'pARL:Darksteel Ingot:darksteel ingot').
card_rarity('darksteel ingot'/'pARL', 'Special').
card_artist('darksteel ingot'/'pARL', 'Martina Pilcerova').
card_number('darksteel ingot'/'pARL', '58').

card_in_set('dauthi slayer', 'pARL').
card_original_type('dauthi slayer'/'pARL', 'Creature — Dauthi Soldier').
card_original_text('dauthi slayer'/'pARL', '').
card_first_print('dauthi slayer', 'pARL').
card_image_name('dauthi slayer'/'pARL', 'dauthi slayer').
card_uid('dauthi slayer'/'pARL', 'pARL:Dauthi Slayer:dauthi slayer').
card_rarity('dauthi slayer'/'pARL', 'Special').
card_artist('dauthi slayer'/'pARL', 'Dermot Power').
card_number('dauthi slayer'/'pARL', '43').
card_flavor_text('dauthi slayer'/'pARL', '\"A wisp of life remains in the undergloom of Death; a visible form, though no heart beats within it.\"\n—Homer, The Iliad, trans. Fitzgerald').

card_in_set('diabolic edict', 'pARL').
card_original_type('diabolic edict'/'pARL', 'Instant').
card_original_text('diabolic edict'/'pARL', '').
card_first_print('diabolic edict', 'pARL').
card_image_name('diabolic edict'/'pARL', 'diabolic edict').
card_uid('diabolic edict'/'pARL', 'pARL:Diabolic Edict:diabolic edict').
card_rarity('diabolic edict'/'pARL', 'Special').
card_artist('diabolic edict'/'pARL', 'Ron Spencer').
card_number('diabolic edict'/'pARL', '37').
card_flavor_text('diabolic edict'/'pARL', 'Greven il-Vec lifted Vhati off his feet. \"The fall will give you time to think on your failure.\"').

card_in_set('disenchant', 'pARL').
card_original_type('disenchant'/'pARL', 'Instant').
card_original_text('disenchant'/'pARL', '').
card_image_name('disenchant'/'pARL', 'disenchant').
card_uid('disenchant'/'pARL', 'pARL:Disenchant:disenchant').
card_rarity('disenchant'/'pARL', 'Special').
card_artist('disenchant'/'pARL', 'Kevin McCann').
card_number('disenchant'/'pARL', '6').

card_in_set('dismiss', 'pARL').
card_original_type('dismiss'/'pARL', 'Instant').
card_original_text('dismiss'/'pARL', '').
card_first_print('dismiss', 'pARL').
card_image_name('dismiss'/'pARL', 'dismiss').
card_uid('dismiss'/'pARL', 'pARL:Dismiss:dismiss').
card_rarity('dismiss'/'pARL', 'Special').
card_artist('dismiss'/'pARL', 'Donato Giancola').
card_number('dismiss'/'pARL', '29').
card_flavor_text('dismiss'/'pARL', '\"There is nothing you can do that I cannot simply deny.\"\n—Ertai, wizard adept').

card_in_set('duress', 'pARL').
card_original_type('duress'/'pARL', 'Sorcery').
card_original_text('duress'/'pARL', '').
card_image_name('duress'/'pARL', 'duress').
card_uid('duress'/'pARL', 'pARL:Duress:duress').
card_rarity('duress'/'pARL', 'Special').
card_artist('duress'/'pARL', 'Lawrence Snelly').
card_number('duress'/'pARL', '17').
card_flavor_text('duress'/'pARL', '\"We decide who is worthy of our works.\"\n—Gix, Yawgmoth praetor').

card_in_set('elvish aberration', 'pARL').
card_original_type('elvish aberration'/'pARL', 'Creature — Elf Mutant').
card_original_text('elvish aberration'/'pARL', '').
card_first_print('elvish aberration', 'pARL').
card_image_name('elvish aberration'/'pARL', 'elvish aberration').
card_uid('elvish aberration'/'pARL', 'pARL:Elvish Aberration:elvish aberration').
card_rarity('elvish aberration'/'pARL', 'Special').
card_artist('elvish aberration'/'pARL', 'Matt Cavotta').
card_number('elvish aberration'/'pARL', '51').

card_in_set('empyrial armor', 'pARL').
card_original_type('empyrial armor'/'pARL', 'Enchantment — Aura').
card_original_text('empyrial armor'/'pARL', '').
card_first_print('empyrial armor', 'pARL').
card_image_name('empyrial armor'/'pARL', 'empyrial armor').
card_uid('empyrial armor'/'pARL', 'pARL:Empyrial Armor:empyrial armor').
card_rarity('empyrial armor'/'pARL', 'Special').
card_artist('empyrial armor'/'pARL', 'D. Alexander Gregory').
card_number('empyrial armor'/'pARL', '31').
card_flavor_text('empyrial armor'/'pARL', '\"An angel appeared in the smoldering skies above the fray, her clothes as flames, her armor as fire.\" -\"Hymn of Angelfire\"').

card_in_set('enlightened tutor', 'pARL').
card_original_type('enlightened tutor'/'pARL', 'Instant').
card_original_text('enlightened tutor'/'pARL', '').
card_first_print('enlightened tutor', 'pARL').
card_image_name('enlightened tutor'/'pARL', 'enlightened tutor').
card_uid('enlightened tutor'/'pARL', 'pARL:Enlightened Tutor:enlightened tutor').
card_rarity('enlightened tutor'/'pARL', 'Special').
card_artist('enlightened tutor'/'pARL', 'Dan Frazier').
card_number('enlightened tutor'/'pARL', '21').
card_flavor_text('enlightened tutor'/'pARL', '\"I do not teach. I simply reveal.\"\n—Daudi, Femeref tutor').

card_in_set('fireball', 'pARL').
card_original_type('fireball'/'pARL', 'Sorcery').
card_original_text('fireball'/'pARL', '').
card_image_name('fireball'/'pARL', 'fireball').
card_uid('fireball'/'pARL', 'pARL:Fireball:fireball').
card_rarity('fireball'/'pARL', 'Special').
card_artist('fireball'/'pARL', 'Jock').
card_number('fireball'/'pARL', '7').

card_in_set('fling', 'pARL').
card_original_type('fling'/'pARL', 'Instant').
card_original_text('fling'/'pARL', '').
card_first_print('fling', 'pARL').
card_image_name('fling'/'pARL', 'fling').
card_uid('fling'/'pARL', 'pARL:Fling:fling').
card_rarity('fling'/'pARL', 'Special').
card_artist('fling'/'pARL', 'Paolo Parente').
card_number('fling'/'pARL', '30').
card_flavor_text('fling'/'pARL', 'It\'s raining rats and moggs.').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest1').
card_uid('forest'/'pARL', 'pARL:Forest:forest1').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Anthony S. Waters').
card_number('forest'/'pARL', '12').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest2').
card_uid('forest'/'pARL', 'pARL:Forest:forest2').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Rob Alexander').
card_number('forest'/'pARL', '27').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest3').
card_uid('forest'/'pARL', 'pARL:Forest:forest3').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Pat Morrissey').
card_number('forest'/'pARL', '36').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest4').
card_uid('forest'/'pARL', 'pARL:Forest:forest4').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Christopher Rush').
card_number('forest'/'pARL', '40').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest5').
card_uid('forest'/'pARL', 'pARL:Forest:forest5').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Rob Alexander').
card_number('forest'/'pARL', '49').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest6').
card_uid('forest'/'pARL', 'pARL:Forest:forest6').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Tony Roberts').
card_number('forest'/'pARL', '5').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest7').
card_uid('forest'/'pARL', 'pARL:Forest:forest7').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'John Avon').
card_number('forest'/'pARL', '57').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest8').
card_uid('forest'/'pARL', 'pARL:Forest:forest8').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'Don Thompson').
card_number('forest'/'pARL', '71').

card_in_set('forest', 'pARL').
card_original_type('forest'/'pARL', 'Basic Land — Forest').
card_original_text('forest'/'pARL', '').
card_image_name('forest'/'pARL', 'forest9').
card_uid('forest'/'pARL', 'pARL:Forest:forest9').
card_rarity('forest'/'pARL', 'Basic Land').
card_artist('forest'/'pARL', 'John Avon').
card_number('forest'/'pARL', '79').

card_in_set('gaea\'s blessing', 'pARL').
card_original_type('gaea\'s blessing'/'pARL', 'Sorcery').
card_original_text('gaea\'s blessing'/'pARL', '').
card_first_print('gaea\'s blessing', 'pARL').
card_image_name('gaea\'s blessing'/'pARL', 'gaea\'s blessing').
card_uid('gaea\'s blessing'/'pARL', 'pARL:Gaea\'s Blessing:gaea\'s blessing').
card_rarity('gaea\'s blessing'/'pARL', 'Special').
card_artist('gaea\'s blessing'/'pARL', 'Rebecca Guay').
card_number('gaea\'s blessing'/'pARL', '38').

card_in_set('genju of the spires', 'pARL').
card_original_type('genju of the spires'/'pARL', 'Enchantment — Aura').
card_original_text('genju of the spires'/'pARL', '').
card_first_print('genju of the spires', 'pARL').
card_image_name('genju of the spires'/'pARL', 'genju of the spires').
card_uid('genju of the spires'/'pARL', 'pARL:Genju of the Spires:genju of the spires').
card_rarity('genju of the spires'/'pARL', 'Special').
card_artist('genju of the spires'/'pARL', 'Joel Thomas').
card_number('genju of the spires'/'pARL', '72').

card_in_set('glacial ray', 'pARL').
card_original_type('glacial ray'/'pARL', 'Instant — Arcane').
card_original_text('glacial ray'/'pARL', '').
card_first_print('glacial ray', 'pARL').
card_image_name('glacial ray'/'pARL', 'glacial ray').
card_uid('glacial ray'/'pARL', 'pARL:Glacial Ray:glacial ray').
card_rarity('glacial ray'/'pARL', 'Special').
card_artist('glacial ray'/'pARL', 'Jim Murray').
card_number('glacial ray'/'pARL', '60').

card_in_set('goblin mime', 'pARL').
card_original_type('goblin mime'/'pARL', 'Creature — Goblin Mime').
card_original_text('goblin mime'/'pARL', '').
card_first_print('goblin mime', 'pARL').
card_border('goblin mime'/'pARL', 'silver').
card_image_name('goblin mime'/'pARL', 'goblin mime').
card_uid('goblin mime'/'pARL', 'pARL:Goblin Mime:goblin mime').
card_rarity('goblin mime'/'pARL', 'Special').
card_artist('goblin mime'/'pARL', 'Dan Frazier').
card_number('goblin mime'/'pARL', '64').
card_flavor_text('goblin mime'/'pARL', 'Mung noticed a few clouds gathering in the sky. Perhaps today he could perform his \"walking against the wind\" routine.').

card_in_set('granny\'s payback', 'pARL').
card_original_type('granny\'s payback'/'pARL', 'Sorcery').
card_original_text('granny\'s payback'/'pARL', '').
card_first_print('granny\'s payback', 'pARL').
card_border('granny\'s payback'/'pARL', 'silver').
card_image_name('granny\'s payback'/'pARL', 'granny\'s payback').
card_uid('granny\'s payback'/'pARL', 'pARL:Granny\'s Payback:granny\'s payback').
card_rarity('granny\'s payback'/'pARL', 'Special').
card_artist('granny\'s payback'/'pARL', 'Kaja Foglio').
card_number('granny\'s payback'/'pARL', '65').
card_flavor_text('granny\'s payback'/'pARL', 'Few remember the exploits of Dakkon\'s grandmother, Eunice Blackblade.').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island1').
card_uid('island'/'pARL', 'pARL:Island:island1').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Tony Roberts').
card_number('island'/'pARL', '2').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island2').
card_uid('island'/'pARL', 'pARL:Island:island2').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Tony Szczudlo').
card_number('island'/'pARL', '24').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island3').
card_uid('island'/'pARL', 'pARL:Island:island3').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Anson Maddocks').
card_number('island'/'pARL', '33').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island4').
card_uid('island'/'pARL', 'pARL:Island:island4').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Mark Poole').
card_number('island'/'pARL', '39').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island5').
card_uid('island'/'pARL', 'pARL:Island:island5').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Rob Alexander').
card_number('island'/'pARL', '46').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island6').
card_uid('island'/'pARL', 'pARL:Island:island6').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'John Avon').
card_number('island'/'pARL', '54').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island7').
card_uid('island'/'pARL', 'pARL:Island:island7').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Don Thompson').
card_number('island'/'pARL', '68').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island8').
card_uid('island'/'pARL', 'pARL:Island:island8').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'John Avon').
card_number('island'/'pARL', '76').

card_in_set('island', 'pARL').
card_original_type('island'/'pARL', 'Basic Land — Island').
card_original_text('island'/'pARL', '').
card_image_name('island'/'pARL', 'island9').
card_uid('island'/'pARL', 'pARL:Island:island9').
card_rarity('island'/'pARL', 'Basic Land').
card_artist('island'/'pARL', 'Donato Giancola').
card_number('island'/'pARL', '9').

card_in_set('karn, silver golem', 'pARL').
card_original_type('karn, silver golem'/'pARL', 'Legendary Artifact Creature — Golem').
card_original_text('karn, silver golem'/'pARL', '').
card_first_print('karn, silver golem', 'pARL').
card_image_name('karn, silver golem'/'pARL', 'karn, silver golem').
card_uid('karn, silver golem'/'pARL', 'pARL:Karn, Silver Golem:karn, silver golem').
card_rarity('karn, silver golem'/'pARL', 'Special').
card_artist('karn, silver golem'/'pARL', 'Mark Zug').
card_number('karn, silver golem'/'pARL', '16').

card_in_set('man-o\'-war', 'pARL').
card_original_type('man-o\'-war'/'pARL', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'pARL', '').
card_first_print('man-o\'-war', 'pARL').
card_image_name('man-o\'-war'/'pARL', 'man-o\'-war').
card_uid('man-o\'-war'/'pARL', 'pARL:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'pARL', 'Special').
card_artist('man-o\'-war'/'pARL', 'Jon J. Muth').
card_number('man-o\'-war'/'pARL', '41').
card_flavor_text('man-o\'-war'/'pARL', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philospher').

card_in_set('mana leak', 'pARL').
card_original_type('mana leak'/'pARL', 'Instant').
card_original_text('mana leak'/'pARL', '').
card_first_print('mana leak', 'pARL').
card_image_name('mana leak'/'pARL', 'mana leak').
card_uid('mana leak'/'pARL', 'pARL:Mana Leak:mana leak').
card_rarity('mana leak'/'pARL', 'Special').
card_artist('mana leak'/'pARL', 'Christopher Rush').
card_number('mana leak'/'pARL', '44').
card_flavor_text('mana leak'/'pARL', '\"If a man will begin with certainties, he shall end in doubts.\"\n—Sir Francis Bacon, The Advancement of Learning').

card_in_set('mise', 'pARL').
card_original_type('mise'/'pARL', 'Instant').
card_original_text('mise'/'pARL', '').
card_first_print('mise', 'pARL').
card_border('mise'/'pARL', 'silver').
card_image_name('mise'/'pARL', 'mise').
card_uid('mise'/'pARL', 'pARL:Mise:mise').
card_rarity('mise'/'pARL', 'Special').
card_artist('mise'/'pARL', 'Mark Zug').
card_number('mise'/'pARL', '62').
card_flavor_text('mise'/'pARL', 'Statistically mind-staggering as it might seem, the term \"mise\" was in fact coined simultaneously by over one thousand Magic players.').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain1').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain1').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'John Avon').
card_number('mountain'/'pARL', '11').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain2').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain2').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'Rob Alexander').
card_number('mountain'/'pARL', '26').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain3').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain3').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'Tom Wänerstrand').
card_number('mountain'/'pARL', '35').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain4').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain4').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'Tony Roberts').
card_number('mountain'/'pARL', '4').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain5').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain5').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'Rob Alexander').
card_number('mountain'/'pARL', '48').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain6').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain6').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'John Avon').
card_number('mountain'/'pARL', '56').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain7').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain7').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'Don Thompson').
card_number('mountain'/'pARL', '70').

card_in_set('mountain', 'pARL').
card_original_type('mountain'/'pARL', 'Basic Land — Mountain').
card_original_text('mountain'/'pARL', '').
card_image_name('mountain'/'pARL', 'mountain8').
card_uid('mountain'/'pARL', 'pARL:Mountain:mountain8').
card_rarity('mountain'/'pARL', 'Basic Land').
card_artist('mountain'/'pARL', 'John Avon').
card_number('mountain'/'pARL', '78').

card_in_set('okina nightwatch', 'pARL').
card_original_type('okina nightwatch'/'pARL', 'Creature — Human Monk').
card_original_text('okina nightwatch'/'pARL', '').
card_first_print('okina nightwatch', 'pARL').
card_image_name('okina nightwatch'/'pARL', 'okina nightwatch').
card_uid('okina nightwatch'/'pARL', 'pARL:Okina Nightwatch:okina nightwatch').
card_rarity('okina nightwatch'/'pARL', 'Special').
card_artist('okina nightwatch'/'pARL', 'Heather Hudson').
card_number('okina nightwatch'/'pARL', '73').
card_flavor_text('okina nightwatch'/'pARL', 'Only the most skilled budoka could protect the Okina Shrine. Only the best of these protectors took the night watch.').

card_in_set('pillage', 'pARL').
card_original_type('pillage'/'pARL', 'Sorcery').
card_original_text('pillage'/'pARL', '').
card_image_name('pillage'/'pARL', 'pillage').
card_uid('pillage'/'pARL', 'pARL:Pillage:pillage').
card_rarity('pillage'/'pARL', 'Special').
card_artist('pillage'/'pARL', 'Richard Kane Ferguson').
card_number('pillage'/'pARL', '20').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains1').
card_uid('plains'/'pARL', 'pARL:Plains:plains1').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Tony Roberts').
card_number('plains'/'pARL', '1').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains2').
card_uid('plains'/'pARL', 'pARL:Plains:plains2').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Edward P. Beard, Jr.').
card_number('plains'/'pARL', '23').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains3').
card_uid('plains'/'pARL', 'pARL:Plains:plains3').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Christopher Rush').
card_number('plains'/'pARL', '32').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains4').
card_uid('plains'/'pARL', 'pARL:Plains:plains4').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Rob Alexander').
card_number('plains'/'pARL', '45').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains5').
card_uid('plains'/'pARL', 'pARL:Plains:plains5').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'John Avon').
card_number('plains'/'pARL', '53').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains6').
card_uid('plains'/'pARL', 'pARL:Plains:plains6').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Don Thompson').
card_number('plains'/'pARL', '67').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains7').
card_uid('plains'/'pARL', 'pARL:Plains:plains7').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'John Avon').
card_number('plains'/'pARL', '75').

card_in_set('plains', 'pARL').
card_original_type('plains'/'pARL', 'Basic Land — Plains').
card_original_text('plains'/'pARL', '').
card_image_name('plains'/'pARL', 'plains8').
card_uid('plains'/'pARL', 'pARL:Plains:plains8').
card_rarity('plains'/'pARL', 'Basic Land').
card_artist('plains'/'pARL', 'Rob Alexander').
card_number('plains'/'pARL', '8').

card_in_set('pouncing jaguar', 'pARL').
card_original_type('pouncing jaguar'/'pARL', 'Creature — Cat').
card_original_text('pouncing jaguar'/'pARL', '').
card_first_print('pouncing jaguar', 'pARL').
card_image_name('pouncing jaguar'/'pARL', 'pouncing jaguar').
card_uid('pouncing jaguar'/'pARL', 'pARL:Pouncing Jaguar:pouncing jaguar').
card_rarity('pouncing jaguar'/'pARL', 'Special').
card_artist('pouncing jaguar'/'pARL', 'Daren Bader').
card_number('pouncing jaguar'/'pARL', '13').
card_flavor_text('pouncing jaguar'/'pARL', 'One pounce, she\'s hungry—you die quickly. Two, she\'s teaching her cubs—you\'re in for a long day.').

card_in_set('rewind', 'pARL').
card_original_type('rewind'/'pARL', 'Instant').
card_original_text('rewind'/'pARL', '').
card_first_print('rewind', 'pARL').
card_image_name('rewind'/'pARL', 'rewind').
card_uid('rewind'/'pARL', 'pARL:Rewind:rewind').
card_rarity('rewind'/'pARL', 'Special').
card_artist('rewind'/'pARL', 'Dermot Power').
card_number('rewind'/'pARL', '15').
card_flavor_text('rewind'/'pARL', '\"Time flows like a river. In Tolaria we practice the art of building dams.\"\n—Barrin, master wizard').

card_in_set('serum visions', 'pARL').
card_original_type('serum visions'/'pARL', 'Sorcery').
card_original_text('serum visions'/'pARL', '').
card_first_print('serum visions', 'pARL').
card_image_name('serum visions'/'pARL', 'serum visions').
card_uid('serum visions'/'pARL', 'pARL:Serum Visions:serum visions').
card_rarity('serum visions'/'pARL', 'Special').
card_artist('serum visions'/'pARL', 'Ben Thompson').
card_number('serum visions'/'pARL', '59').

card_in_set('skirk marauder', 'pARL').
card_original_type('skirk marauder'/'pARL', 'Creature — Goblin').
card_original_text('skirk marauder'/'pARL', '').
card_first_print('skirk marauder', 'pARL').
card_image_name('skirk marauder'/'pARL', 'skirk marauder').
card_uid('skirk marauder'/'pARL', 'pARL:Skirk Marauder:skirk marauder').
card_rarity('skirk marauder'/'pARL', 'Special').
card_artist('skirk marauder'/'pARL', 'Pete Venters').
card_number('skirk marauder'/'pARL', '50').

card_in_set('skittering skirge', 'pARL').
card_original_type('skittering skirge'/'pARL', 'Creature — Imp').
card_original_text('skittering skirge'/'pARL', '').
card_first_print('skittering skirge', 'pARL').
card_image_name('skittering skirge'/'pARL', 'skittering skirge').
card_uid('skittering skirge'/'pARL', 'pARL:Skittering Skirge:skittering skirge').
card_rarity('skittering skirge'/'pARL', 'Special').
card_artist('skittering skirge'/'pARL', 'Ron Spencer').
card_number('skittering skirge'/'pARL', '14').
card_flavor_text('skittering skirge'/'pARL', 'The imps\' warbling cries echo through Phyrexia\'s towers like those of mourning doves in a cathedral.').

card_in_set('skyknight legionnaire', 'pARL').
card_original_type('skyknight legionnaire'/'pARL', 'Creature — Human Knight').
card_original_text('skyknight legionnaire'/'pARL', '').
card_first_print('skyknight legionnaire', 'pARL').
card_image_name('skyknight legionnaire'/'pARL', 'skyknight legionnaire').
card_uid('skyknight legionnaire'/'pARL', 'pARL:Skyknight Legionnaire:skyknight legionnaire').
card_rarity('skyknight legionnaire'/'pARL', 'Special').
card_artist('skyknight legionnaire'/'pARL', 'Jim Murray').
card_number('skyknight legionnaire'/'pARL', '74').
card_flavor_text('skyknight legionnaire'/'pARL', 'I do not know which gives me more pride: the legions of soldiers marching in thunderous lockstep, or the cry of the skyknights riding their rocs like jagged lightning overhead.\n—Razia').

card_in_set('stupor', 'pARL').
card_original_type('stupor'/'pARL', 'Sorcery').
card_original_text('stupor'/'pARL', '').
card_first_print('stupor', 'pARL').
card_image_name('stupor'/'pARL', 'stupor').
card_uid('stupor'/'pARL', 'pARL:Stupor:stupor').
card_rarity('stupor'/'pARL', 'Special').
card_artist('stupor'/'pARL', 'Mike Kimble').
card_number('stupor'/'pARL', '22').
card_flavor_text('stupor'/'pARL', 'There are medicines for all afflictions but idleness. —Suq\'Ata saying').

card_in_set('surging flame', 'pARL').
card_original_type('surging flame'/'pARL', 'Instant').
card_original_text('surging flame'/'pARL', '').
card_first_print('surging flame', 'pARL').
card_image_name('surging flame'/'pARL', 'surging flame').
card_uid('surging flame'/'pARL', 'pARL:Surging Flame:surging flame').
card_rarity('surging flame'/'pARL', 'Special').
card_artist('surging flame'/'pARL', 'Ron Spencer').
card_number('surging flame'/'pARL', '83').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp1').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp1').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'John Avon').
card_number('swamp'/'pARL', '10').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp2').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp2').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'Rob Alexander').
card_number('swamp'/'pARL', '25').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp3').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp3').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'Tony Roberts').
card_number('swamp'/'pARL', '3').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp4').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp4').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'Douglas Shuler').
card_number('swamp'/'pARL', '34').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp5').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp5').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'Rob Alexander').
card_number('swamp'/'pARL', '47').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp6').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp6').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'John Avon').
card_number('swamp'/'pARL', '55').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp7').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp7').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'Don Thompson').
card_number('swamp'/'pARL', '69').

card_in_set('swamp', 'pARL').
card_original_type('swamp'/'pARL', 'Basic Land — Swamp').
card_original_text('swamp'/'pARL', '').
card_image_name('swamp'/'pARL', 'swamp8').
card_uid('swamp'/'pARL', 'pARL:Swamp:swamp8').
card_rarity('swamp'/'pARL', 'Basic Land').
card_artist('swamp'/'pARL', 'John Avon').
card_number('swamp'/'pARL', '77').

card_in_set('uktabi orangutan', 'pARL').
card_original_type('uktabi orangutan'/'pARL', 'Creature — Ape').
card_original_text('uktabi orangutan'/'pARL', '').
card_first_print('uktabi orangutan', 'pARL').
card_image_name('uktabi orangutan'/'pARL', 'uktabi orangutan').
card_uid('uktabi orangutan'/'pARL', 'pARL:Uktabi Orangutan:uktabi orangutan').
card_rarity('uktabi orangutan'/'pARL', 'Special').
card_artist('uktabi orangutan'/'pARL', 'Una Fricker').
card_number('uktabi orangutan'/'pARL', '18').
card_flavor_text('uktabi orangutan'/'pARL', '\"Is it true that the apes wear furs of gold when they marry?\"\n—Rana, Suq\'Ata market fool').

card_in_set('wee dragonauts', 'pARL').
card_original_type('wee dragonauts'/'pARL', 'Creature — Faerie Wizard').
card_original_text('wee dragonauts'/'pARL', '').
card_first_print('wee dragonauts', 'pARL').
card_image_name('wee dragonauts'/'pARL', 'wee dragonauts').
card_uid('wee dragonauts'/'pARL', 'pARL:Wee Dragonauts:wee dragonauts').
card_rarity('wee dragonauts'/'pARL', 'Special').
card_artist('wee dragonauts'/'pARL', 'Greg Staples').
card_number('wee dragonauts'/'pARL', '81').
card_flavor_text('wee dragonauts'/'pARL', 'The blazekite is a simple concept, really - just a vehicular application of dragscoop ionics and electropropulsion magnetronics.\n—Juzba, Izzet tinker').
