% Urza's Saga

set('USG').
set_name('USG', 'Urza\'s Saga').
set_release_date('USG', '1998-10-12').
set_border('USG', 'black').
set_type('USG', 'expansion').
set_block('USG', 'Urza\'s').

card_in_set('absolute grace', 'USG').
card_original_type('absolute grace'/'USG', 'Enchantment').
card_original_text('absolute grace'/'USG', 'All creatures gain protection from black.').
card_first_print('absolute grace', 'USG').
card_image_name('absolute grace'/'USG', 'absolute grace').
card_uid('absolute grace'/'USG', 'USG:Absolute Grace:absolute grace').
card_rarity('absolute grace'/'USG', 'Uncommon').
card_artist('absolute grace'/'USG', 'Jeff Miracola').
card_number('absolute grace'/'USG', '1').
card_flavor_text('absolute grace'/'USG', 'In pursuit of Urza, the Phyrexians sent countless foul legions into Serra\'s realm. Though beaten back, they left it tainted with uncleansable evil.').
card_multiverse_id('absolute grace'/'USG', '5716').

card_in_set('absolute law', 'USG').
card_original_type('absolute law'/'USG', 'Enchantment').
card_original_text('absolute law'/'USG', 'All creatures gain protection from red.').
card_first_print('absolute law', 'USG').
card_image_name('absolute law'/'USG', 'absolute law').
card_uid('absolute law'/'USG', 'USG:Absolute Law:absolute law').
card_rarity('absolute law'/'USG', 'Uncommon').
card_artist('absolute law'/'USG', 'Mark Zug').
card_number('absolute law'/'USG', '2').
card_flavor_text('absolute law'/'USG', 'The strength of law is unwavering. It is an iron bar in a world of water.').
card_multiverse_id('absolute law'/'USG', '8437').

card_in_set('abundance', 'USG').
card_original_type('abundance'/'USG', 'Enchantment').
card_original_text('abundance'/'USG', 'Instead of drawing a card, you may choose land or nonland and reveal cards from your library until you reveal a card of the chosen kind. Put that card into your hand and put all other revealed cards on the bottom of your library in any order.').
card_first_print('abundance', 'USG').
card_image_name('abundance'/'USG', 'abundance').
card_uid('abundance'/'USG', 'USG:Abundance:abundance').
card_rarity('abundance'/'USG', 'Rare').
card_artist('abundance'/'USG', 'Rebecca Guay').
card_number('abundance'/'USG', '229').
card_multiverse_id('abundance'/'USG', '5551').

card_in_set('abyssal horror', 'USG').
card_original_type('abyssal horror'/'USG', 'Summon — Horror').
card_original_text('abyssal horror'/'USG', 'Flying\nWhen Abyssal Horror comes into play, target player chooses and discards two cards.').
card_first_print('abyssal horror', 'USG').
card_image_name('abyssal horror'/'USG', 'abyssal horror').
card_uid('abyssal horror'/'USG', 'USG:Abyssal Horror:abyssal horror').
card_rarity('abyssal horror'/'USG', 'Rare').
card_artist('abyssal horror'/'USG', 'rk post').
card_number('abyssal horror'/'USG', '115').
card_flavor_text('abyssal horror'/'USG', 'It has no face of its own—it wears that of its latest victim.').
card_multiverse_id('abyssal horror'/'USG', '5636').

card_in_set('academy researchers', 'USG').
card_original_type('academy researchers'/'USG', 'Summon — Wizards').
card_original_text('academy researchers'/'USG', 'When Academy Researchers comes into play, you may choose an enchant creature card in your hand and put that enchantment into play on Academy Researchers.').
card_first_print('academy researchers', 'USG').
card_image_name('academy researchers'/'USG', 'academy researchers').
card_uid('academy researchers'/'USG', 'USG:Academy Researchers:academy researchers').
card_rarity('academy researchers'/'USG', 'Uncommon').
card_artist('academy researchers'/'USG', 'Stephen Daniele').
card_number('academy researchers'/'USG', '58').
card_multiverse_id('academy researchers'/'USG', '9713').

card_in_set('acidic soil', 'USG').
card_original_type('acidic soil'/'USG', 'Sorcery').
card_original_text('acidic soil'/'USG', 'Acidic Soil deals 1 damage to each player for each land he or she controls.').
card_first_print('acidic soil', 'USG').
card_image_name('acidic soil'/'USG', 'acidic soil').
card_uid('acidic soil'/'USG', 'USG:Acidic Soil:acidic soil').
card_rarity('acidic soil'/'USG', 'Uncommon').
card_artist('acidic soil'/'USG', 'Scott M. Fischer').
card_number('acidic soil'/'USG', '172').
card_flavor_text('acidic soil'/'USG', 'Phyrexia had tried to take Urza\'s soul. He was relieved that Shiv tried to claim only his soles.').
card_multiverse_id('acidic soil'/'USG', '5722').

card_in_set('acridian', 'USG').
card_original_type('acridian'/'USG', 'Summon — Insect').
card_original_text('acridian'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_first_print('acridian', 'USG').
card_image_name('acridian'/'USG', 'acridian').
card_uid('acridian'/'USG', 'USG:Acridian:acridian').
card_rarity('acridian'/'USG', 'Common').
card_artist('acridian'/'USG', 'rk post').
card_number('acridian'/'USG', '230').
card_flavor_text('acridian'/'USG', 'The elves of Argoth were trained to ride these creatures, even when their mounts traveled upside-down.').
card_multiverse_id('acridian'/'USG', '5865').

card_in_set('albino troll', 'USG').
card_original_type('albino troll'/'USG', 'Summon — Troll').
card_original_text('albino troll'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\n{1}{G} Regenerate Albino Troll.').
card_first_print('albino troll', 'USG').
card_image_name('albino troll'/'USG', 'albino troll').
card_uid('albino troll'/'USG', 'USG:Albino Troll:albino troll').
card_rarity('albino troll'/'USG', 'Uncommon').
card_artist('albino troll'/'USG', 'Paolo Parente').
card_number('albino troll'/'USG', '231').
card_multiverse_id('albino troll'/'USG', '5867').

card_in_set('anaconda', 'USG').
card_original_type('anaconda'/'USG', 'Summon — Snake').
card_original_text('anaconda'/'USG', 'Swampwalk (If defending player controls a swamp, this creature is unblockable.)').
card_image_name('anaconda'/'USG', 'anaconda').
card_uid('anaconda'/'USG', 'USG:Anaconda:anaconda').
card_rarity('anaconda'/'USG', 'Uncommon').
card_artist('anaconda'/'USG', 'Stephen Daniele').
card_number('anaconda'/'USG', '232').
card_flavor_text('anaconda'/'USG', 'If you\'re smaller than the anaconda, it considers you food. If you\'re larger than the anaconda, it considers you a lot of food.').
card_multiverse_id('anaconda'/'USG', '5603').

card_in_set('angelic chorus', 'USG').
card_original_type('angelic chorus'/'USG', 'Enchantment').
card_original_text('angelic chorus'/'USG', 'Whenever a creature comes into play under your control, gain life equal to that creature\'s toughness.').
card_first_print('angelic chorus', 'USG').
card_image_name('angelic chorus'/'USG', 'angelic chorus').
card_uid('angelic chorus'/'USG', 'USG:Angelic Chorus:angelic chorus').
card_rarity('angelic chorus'/'USG', 'Rare').
card_artist('angelic chorus'/'USG', 'Ron Spencer').
card_number('angelic chorus'/'USG', '3').
card_flavor_text('angelic chorus'/'USG', 'The very young and the very old know best the song the angels sing.').
card_multiverse_id('angelic chorus'/'USG', '5714').

card_in_set('angelic page', 'USG').
card_original_type('angelic page'/'USG', 'Summon — Spirit').
card_original_text('angelic page'/'USG', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_first_print('angelic page', 'USG').
card_image_name('angelic page'/'USG', 'angelic page').
card_uid('angelic page'/'USG', 'USG:Angelic Page:angelic page').
card_rarity('angelic page'/'USG', 'Common').
card_artist('angelic page'/'USG', 'Rebecca Guay').
card_number('angelic page'/'USG', '4').
card_flavor_text('angelic page'/'USG', 'If only every message were as perfect as its bearers.').
card_multiverse_id('angelic page'/'USG', '5781').

card_in_set('annul', 'USG').
card_original_type('annul'/'USG', 'Interrupt').
card_original_text('annul'/'USG', 'Counter target artifact or enchantment spell.').
card_first_print('annul', 'USG').
card_image_name('annul'/'USG', 'annul').
card_uid('annul'/'USG', 'USG:Annul:annul').
card_rarity('annul'/'USG', 'Common').
card_artist('annul'/'USG', 'Greg Simanson').
card_number('annul'/'USG', '59').
card_flavor_text('annul'/'USG', 'The most effective way to destroy a spell is to ensure it was never cast in the first place.').
card_multiverse_id('annul'/'USG', '10420').

card_in_set('antagonism', 'USG').
card_original_type('antagonism'/'USG', 'Enchantment').
card_original_text('antagonism'/'USG', 'During each player\'s discard phase, Antagonism deals 2 damage to that player unless one of his or her opponents was successfully dealt damage that turn.').
card_first_print('antagonism', 'USG').
card_image_name('antagonism'/'USG', 'antagonism').
card_uid('antagonism'/'USG', 'USG:Antagonism:antagonism').
card_rarity('antagonism'/'USG', 'Rare').
card_artist('antagonism'/'USG', 'Donato Giancola').
card_number('antagonism'/'USG', '173').
card_multiverse_id('antagonism'/'USG', '5572').

card_in_set('arc lightning', 'USG').
card_original_type('arc lightning'/'USG', 'Sorcery').
card_original_text('arc lightning'/'USG', 'Arc Lightning deals 3 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('arc lightning'/'USG', 'arc lightning').
card_uid('arc lightning'/'USG', 'USG:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'USG', 'Common').
card_artist('arc lightning'/'USG', 'Andrew Goldhawk').
card_number('arc lightning'/'USG', '174').
card_flavor_text('arc lightning'/'USG', 'Rainclouds don\'t last long in Shiv, but that doesn\'t stop the lightning.').
card_multiverse_id('arc lightning'/'USG', '5733').

card_in_set('arcane laboratory', 'USG').
card_original_type('arcane laboratory'/'USG', 'Enchantment').
card_original_text('arcane laboratory'/'USG', 'Each player cannot play more than one spell each turn.').
card_first_print('arcane laboratory', 'USG').
card_image_name('arcane laboratory'/'USG', 'arcane laboratory').
card_uid('arcane laboratory'/'USG', 'USG:Arcane Laboratory:arcane laboratory').
card_rarity('arcane laboratory'/'USG', 'Uncommon').
card_artist('arcane laboratory'/'USG', 'Stephen Daniele').
card_number('arcane laboratory'/'USG', '60').
card_flavor_text('arcane laboratory'/'USG', 'It soon became obvious that some experiments were best overseen by fireproof teachers.').
card_multiverse_id('arcane laboratory'/'USG', '5764').

card_in_set('argothian elder', 'USG').
card_original_type('argothian elder'/'USG', 'Summon — Elf').
card_original_text('argothian elder'/'USG', '{T}: Untap two target lands.').
card_first_print('argothian elder', 'USG').
card_image_name('argothian elder'/'USG', 'argothian elder').
card_uid('argothian elder'/'USG', 'USG:Argothian Elder:argothian elder').
card_rarity('argothian elder'/'USG', 'Uncommon').
card_artist('argothian elder'/'USG', 'DiTerlizzi').
card_number('argothian elder'/'USG', '233').
card_flavor_text('argothian elder'/'USG', 'Sharpen your ears\n—Elvish expression meaning \"grow wiser\"').
card_multiverse_id('argothian elder'/'USG', '5736').

card_in_set('argothian enchantress', 'USG').
card_original_type('argothian enchantress'/'USG', 'Summon — Enchantress').
card_original_text('argothian enchantress'/'USG', 'Argothian Enchantress cannot be the target of spells or abilities.\nWhenever you successfully cast an enchantment spell, draw a card.').
card_image_name('argothian enchantress'/'USG', 'argothian enchantress').
card_uid('argothian enchantress'/'USG', 'USG:Argothian Enchantress:argothian enchantress').
card_rarity('argothian enchantress'/'USG', 'Rare').
card_artist('argothian enchantress'/'USG', 'Daren Bader').
card_number('argothian enchantress'/'USG', '234').
card_multiverse_id('argothian enchantress'/'USG', '5686').

card_in_set('argothian swine', 'USG').
card_original_type('argothian swine'/'USG', 'Summon — Boars').
card_original_text('argothian swine'/'USG', 'Trample').
card_first_print('argothian swine', 'USG').
card_image_name('argothian swine'/'USG', 'argothian swine').
card_uid('argothian swine'/'USG', 'USG:Argothian Swine:argothian swine').
card_rarity('argothian swine'/'USG', 'Common').
card_artist('argothian swine'/'USG', 'Randy Elliott').
card_number('argothian swine'/'USG', '235').
card_flavor_text('argothian swine'/'USG', 'In Argoth, the shortest path between two points is the one the swine make.').
card_multiverse_id('argothian swine'/'USG', '5862').

card_in_set('argothian wurm', 'USG').
card_original_type('argothian wurm'/'USG', 'Summon — Wurm').
card_original_text('argothian wurm'/'USG', 'Trample\nWhen Argothian Wurm comes into play, any player may sacrifice a land to put Argothian Wurm on top of owner\'s library.').
card_first_print('argothian wurm', 'USG').
card_image_name('argothian wurm'/'USG', 'argothian wurm').
card_uid('argothian wurm'/'USG', 'USG:Argothian Wurm:argothian wurm').
card_rarity('argothian wurm'/'USG', 'Rare').
card_artist('argothian wurm'/'USG', 'Kev Walker').
card_number('argothian wurm'/'USG', '236').
card_multiverse_id('argothian wurm'/'USG', '5769').

card_in_set('attunement', 'USG').
card_original_type('attunement'/'USG', 'Enchantment').
card_original_text('attunement'/'USG', 'Return Attunement to owner\'s hand: Draw three cards, then choose and discard four cards.').
card_first_print('attunement', 'USG').
card_image_name('attunement'/'USG', 'attunement').
card_uid('attunement'/'USG', 'USG:Attunement:attunement').
card_rarity('attunement'/'USG', 'Rare').
card_artist('attunement'/'USG', 'Randy Gallegos').
card_number('attunement'/'USG', '61').
card_flavor_text('attunement'/'USG', '\"The solution can hide for only so long.\"\n—Urza').
card_multiverse_id('attunement'/'USG', '5596').

card_in_set('back to basics', 'USG').
card_original_type('back to basics'/'USG', 'Enchantment').
card_original_text('back to basics'/'USG', 'Nonbasic lands do not untap during their controllers\' untap phases.').
card_first_print('back to basics', 'USG').
card_image_name('back to basics'/'USG', 'back to basics').
card_uid('back to basics'/'USG', 'USG:Back to Basics:back to basics').
card_rarity('back to basics'/'USG', 'Rare').
card_artist('back to basics'/'USG', 'Andrew Robinson').
card_number('back to basics'/'USG', '62').
card_flavor_text('back to basics'/'USG', '\"A ruler wears a crown while the rest of us wear hats, but which would you rather have when it\'s raining?\"\n—Barrin, Principia').
card_multiverse_id('back to basics'/'USG', '5711').

card_in_set('barrin\'s codex', 'USG').
card_original_type('barrin\'s codex'/'USG', 'Artifact').
card_original_text('barrin\'s codex'/'USG', 'During your upkeep, you may put a page counter on Barrin\'s Codex.\n{4}, {T}, Sacrifice Barrin\'s Codex: Draw X cards, where X is the number of page counters on Barrin\'s Codex.').
card_first_print('barrin\'s codex', 'USG').
card_image_name('barrin\'s codex'/'USG', 'barrin\'s codex').
card_uid('barrin\'s codex'/'USG', 'USG:Barrin\'s Codex:barrin\'s codex').
card_rarity('barrin\'s codex'/'USG', 'Rare').
card_artist('barrin\'s codex'/'USG', 'DiTerlizzi').
card_number('barrin\'s codex'/'USG', '286').
card_multiverse_id('barrin\'s codex'/'USG', '5735').

card_in_set('barrin, master wizard', 'USG').
card_original_type('barrin, master wizard'/'USG', 'Summon — Legend').
card_original_text('barrin, master wizard'/'USG', 'Barrin, Master Wizard counts as a Wizard.\n{2}, Sacrifice a permanent: Return target creature to owner\'s hand.').
card_first_print('barrin, master wizard', 'USG').
card_image_name('barrin, master wizard'/'USG', 'barrin, master wizard').
card_uid('barrin, master wizard'/'USG', 'USG:Barrin, Master Wizard:barrin, master wizard').
card_rarity('barrin, master wizard'/'USG', 'Rare').
card_artist('barrin, master wizard'/'USG', 'Michael Sutfin').
card_number('barrin, master wizard'/'USG', '63').
card_flavor_text('barrin, master wizard'/'USG', '\"Knowledge is no more expensive than ignorance, and at least as satisfying.\"\n—Barrin, master wizard').
card_multiverse_id('barrin, master wizard'/'USG', '9856').

card_in_set('bedlam', 'USG').
card_original_type('bedlam'/'USG', 'Enchantment').
card_original_text('bedlam'/'USG', 'Creatures cannot block.').
card_first_print('bedlam', 'USG').
card_image_name('bedlam'/'USG', 'bedlam').
card_uid('bedlam'/'USG', 'USG:Bedlam:bedlam').
card_rarity('bedlam'/'USG', 'Rare').
card_artist('bedlam'/'USG', 'Mike Raabe').
card_number('bedlam'/'USG', '175').
card_flavor_text('bedlam'/'USG', 'Sometimes quantity, in the absence of quality, is good enough.').
card_multiverse_id('bedlam'/'USG', '5727').

card_in_set('befoul', 'USG').
card_original_type('befoul'/'USG', 'Sorcery').
card_original_text('befoul'/'USG', 'Destroy target land or nonblack creature. A creature destroyed this way cannot be regenerated this turn.').
card_first_print('befoul', 'USG').
card_image_name('befoul'/'USG', 'befoul').
card_uid('befoul'/'USG', 'USG:Befoul:befoul').
card_rarity('befoul'/'USG', 'Common').
card_artist('befoul'/'USG', 'Pete Venters').
card_number('befoul'/'USG', '116').
card_flavor_text('befoul'/'USG', '\"The land putrefied at its touch, turned into an oily bile in seconds.\"\n—Radiant, archangel').
card_multiverse_id('befoul'/'USG', '5605').

card_in_set('bereavement', 'USG').
card_original_type('bereavement'/'USG', 'Enchantment').
card_original_text('bereavement'/'USG', 'Whenever a green creature is put into a graveyard from play, its controller chooses and discards a card.').
card_first_print('bereavement', 'USG').
card_image_name('bereavement'/'USG', 'bereavement').
card_uid('bereavement'/'USG', 'USG:Bereavement:bereavement').
card_rarity('bereavement'/'USG', 'Uncommon').
card_artist('bereavement'/'USG', 'Andrew Goldhawk').
card_number('bereavement'/'USG', '117').
card_flavor_text('bereavement'/'USG', '\"Grief is as useless as love.\"\n—Gix, Yawgmoth praetor').
card_multiverse_id('bereavement'/'USG', '5617').

card_in_set('blanchwood armor', 'USG').
card_original_type('blanchwood armor'/'USG', 'Enchant Creature').
card_original_text('blanchwood armor'/'USG', 'Enchanted creature gets +X/+X, where X is the number of forests you control.').
card_first_print('blanchwood armor', 'USG').
card_image_name('blanchwood armor'/'USG', 'blanchwood armor').
card_uid('blanchwood armor'/'USG', 'USG:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'USG', 'Uncommon').
card_artist('blanchwood armor'/'USG', 'Paolo Parente').
card_number('blanchwood armor'/'USG', '237').
card_flavor_text('blanchwood armor'/'USG', '\"Braid her branches, life fulfilling\nGoddess loving, Goddess killing.\"\n—Citanul chant').
card_multiverse_id('blanchwood armor'/'USG', '5623').

card_in_set('blanchwood treefolk', 'USG').
card_original_type('blanchwood treefolk'/'USG', 'Summon — Treefolk').
card_original_text('blanchwood treefolk'/'USG', '').
card_first_print('blanchwood treefolk', 'USG').
card_image_name('blanchwood treefolk'/'USG', 'blanchwood treefolk').
card_uid('blanchwood treefolk'/'USG', 'USG:Blanchwood Treefolk:blanchwood treefolk').
card_rarity('blanchwood treefolk'/'USG', 'Common').
card_artist('blanchwood treefolk'/'USG', 'DiTerlizzi').
card_number('blanchwood treefolk'/'USG', '238').
card_flavor_text('blanchwood treefolk'/'USG', 'The massive Argivian attack on their rooted kindred was a declaration of war to the treefolk.').
card_multiverse_id('blanchwood treefolk'/'USG', '5747').

card_in_set('blasted landscape', 'USG').
card_original_type('blasted landscape'/'USG', 'Land').
card_original_text('blasted landscape'/'USG', '{T}: Add one colorless mana to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('blasted landscape', 'USG').
card_image_name('blasted landscape'/'USG', 'blasted landscape').
card_uid('blasted landscape'/'USG', 'USG:Blasted Landscape:blasted landscape').
card_rarity('blasted landscape'/'USG', 'Uncommon').
card_artist('blasted landscape'/'USG', 'Ciruelo').
card_number('blasted landscape'/'USG', '319').
card_multiverse_id('blasted landscape'/'USG', '5612').

card_in_set('blood vassal', 'USG').
card_original_type('blood vassal'/'USG', 'Summon — Thrull').
card_original_text('blood vassal'/'USG', 'Sacrifice Blood Vassal: Add {B}{B} to your mana pool. Play this ability as a mana source.').
card_first_print('blood vassal', 'USG').
card_image_name('blood vassal'/'USG', 'blood vassal').
card_uid('blood vassal'/'USG', 'USG:Blood Vassal:blood vassal').
card_rarity('blood vassal'/'USG', 'Common').
card_artist('blood vassal'/'USG', 'Chippy').
card_number('blood vassal'/'USG', '118').
card_flavor_text('blood vassal'/'USG', '\"They are bred to suffer and born to die. Much like humans.\"\n—Gix, Yawgmoth praetor').
card_multiverse_id('blood vassal'/'USG', '5737').

card_in_set('bog raiders', 'USG').
card_original_type('bog raiders'/'USG', 'Summon — Zombies').
card_original_text('bog raiders'/'USG', 'Swampwalk (If defending player controls a swamp, this creature is unblockable.)').
card_image_name('bog raiders'/'USG', 'bog raiders').
card_uid('bog raiders'/'USG', 'USG:Bog Raiders:bog raiders').
card_rarity('bog raiders'/'USG', 'Common').
card_artist('bog raiders'/'USG', 'Carl Critchlow').
card_number('bog raiders'/'USG', '119').
card_flavor_text('bog raiders'/'USG', '\"Let weak feed on weak, that we may divine the nature of strength.\"\n—Phyrexian Scriptures').
card_multiverse_id('bog raiders'/'USG', '5597').

card_in_set('brand', 'USG').
card_original_type('brand'/'USG', 'Instant').
card_original_text('brand'/'USG', 'Gain control of all permanents you own.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('brand', 'USG').
card_image_name('brand'/'USG', 'brand').
card_uid('brand'/'USG', 'USG:Brand:brand').
card_rarity('brand'/'USG', 'Rare').
card_artist('brand'/'USG', 'Donato Giancola').
card_number('brand'/'USG', '176').
card_flavor_text('brand'/'USG', '\"By this glyph I affirm your role.\"\n—Urza, to Karn').
card_multiverse_id('brand'/'USG', '5564').

card_in_set('bravado', 'USG').
card_original_type('bravado'/'USG', 'Enchant Creature').
card_original_text('bravado'/'USG', 'Enchanted creature gets +1/+1 for each other creature you control.').
card_first_print('bravado', 'USG').
card_image_name('bravado'/'USG', 'bravado').
card_uid('bravado'/'USG', 'USG:Bravado:bravado').
card_rarity('bravado'/'USG', 'Common').
card_artist('bravado'/'USG', 'Jerry Tiritilli').
card_number('bravado'/'USG', '177').
card_flavor_text('bravado'/'USG', '\"We drive the dragons from our home. Why should we fear you?\"\n—Fire Eye, viashino bey').
card_multiverse_id('bravado'/'USG', '5848').

card_in_set('breach', 'USG').
card_original_type('breach'/'USG', 'Instant').
card_original_text('breach'/'USG', 'Target creature gets +2/+0 until end of turn. That creature cannot be blocked except by artifact creatures and black creatures this turn.').
card_first_print('breach', 'USG').
card_image_name('breach'/'USG', 'breach').
card_uid('breach'/'USG', 'USG:Breach:breach').
card_rarity('breach'/'USG', 'Common').
card_artist('breach'/'USG', 'Greg Staples').
card_number('breach'/'USG', '120').
card_multiverse_id('breach'/'USG', '5633').

card_in_set('brilliant halo', 'USG').
card_original_type('brilliant halo'/'USG', 'Enchant Creature').
card_original_text('brilliant halo'/'USG', 'Enchanted creature gets +1/+2.\nWhen Brilliant Halo is put into a graveyard from play, return Brilliant Halo to owner\'s hand.').
card_first_print('brilliant halo', 'USG').
card_image_name('brilliant halo'/'USG', 'brilliant halo').
card_uid('brilliant halo'/'USG', 'USG:Brilliant Halo:brilliant halo').
card_rarity('brilliant halo'/'USG', 'Common').
card_artist('brilliant halo'/'USG', 'Randy Gallegos').
card_number('brilliant halo'/'USG', '5').
card_multiverse_id('brilliant halo'/'USG', '9890').

card_in_set('bull hippo', 'USG').
card_original_type('bull hippo'/'USG', 'Summon — Hippo').
card_original_text('bull hippo'/'USG', 'Islandwalk (If defending player controls an island, this creature is unblockable.)').
card_image_name('bull hippo'/'USG', 'bull hippo').
card_uid('bull hippo'/'USG', 'USG:Bull Hippo:bull hippo').
card_rarity('bull hippo'/'USG', 'Uncommon').
card_artist('bull hippo'/'USG', 'Daren Bader').
card_number('bull hippo'/'USG', '239').
card_flavor_text('bull hippo'/'USG', '\"How could you not hear it approach? It\'s a hippo!\"\n—Argivian commander').
card_multiverse_id('bull hippo'/'USG', '5577').

card_in_set('bulwark', 'USG').
card_original_type('bulwark'/'USG', 'Enchantment').
card_original_text('bulwark'/'USG', 'During your upkeep, Bulwark deals 1 damage to target opponent for each card in your hand greater than the number of cards in that player\'s hand.').
card_first_print('bulwark', 'USG').
card_image_name('bulwark'/'USG', 'bulwark').
card_uid('bulwark'/'USG', 'USG:Bulwark:bulwark').
card_rarity('bulwark'/'USG', 'Rare').
card_artist('bulwark'/'USG', 'Brian Snõddy').
card_number('bulwark'/'USG', '178').
card_flavor_text('bulwark'/'USG', '\"It will be the goblin\'s first bath, and its last.\" —Fire Eye, viashino bey').
card_multiverse_id('bulwark'/'USG', '8886').

card_in_set('cackling fiend', 'USG').
card_original_type('cackling fiend'/'USG', 'Summon — Zombie').
card_original_text('cackling fiend'/'USG', 'When Cackling Fiend comes into play, each of your opponents chooses and discards a card.').
card_first_print('cackling fiend', 'USG').
card_image_name('cackling fiend'/'USG', 'cackling fiend').
card_uid('cackling fiend'/'USG', 'USG:Cackling Fiend:cackling fiend').
card_rarity('cackling fiend'/'USG', 'Common').
card_artist('cackling fiend'/'USG', 'Brian Despain').
card_number('cackling fiend'/'USG', '121').
card_flavor_text('cackling fiend'/'USG', 'Its windpipe is only the first to amplify its maddening laughter.').
card_multiverse_id('cackling fiend'/'USG', '5567').

card_in_set('carpet of flowers', 'USG').
card_original_type('carpet of flowers'/'USG', 'Enchantment').
card_original_text('carpet of flowers'/'USG', 'During your main phase, you may add up to X mana of one color to your mana pool, where X is the number of islands target opponent controls.').
card_first_print('carpet of flowers', 'USG').
card_image_name('carpet of flowers'/'USG', 'carpet of flowers').
card_uid('carpet of flowers'/'USG', 'USG:Carpet of Flowers:carpet of flowers').
card_rarity('carpet of flowers'/'USG', 'Uncommon').
card_artist('carpet of flowers'/'USG', 'Rebecca Guay').
card_number('carpet of flowers'/'USG', '240').
card_multiverse_id('carpet of flowers'/'USG', '5858').

card_in_set('carrion beetles', 'USG').
card_original_type('carrion beetles'/'USG', 'Summon — Insects').
card_original_text('carrion beetles'/'USG', '{2}{B}, {T}: Remove from the game up to three target cards in one graveyard.').
card_first_print('carrion beetles', 'USG').
card_image_name('carrion beetles'/'USG', 'carrion beetles').
card_uid('carrion beetles'/'USG', 'USG:Carrion Beetles:carrion beetles').
card_rarity('carrion beetles'/'USG', 'Common').
card_artist('carrion beetles'/'USG', 'Ron Spencer').
card_number('carrion beetles'/'USG', '122').
card_flavor_text('carrion beetles'/'USG', 'It\'s all fun and games until someone loses an eye.').
card_multiverse_id('carrion beetles'/'USG', '5620').

card_in_set('catalog', 'USG').
card_original_type('catalog'/'USG', 'Instant').
card_original_text('catalog'/'USG', 'Draw two cards, then choose and discard a card.').
card_first_print('catalog', 'USG').
card_image_name('catalog'/'USG', 'catalog').
card_uid('catalog'/'USG', 'USG:Catalog:catalog').
card_rarity('catalog'/'USG', 'Common').
card_artist('catalog'/'USG', 'Berry').
card_number('catalog'/'USG', '64').
card_flavor_text('catalog'/'USG', '\"Without order comes errors, and errors kill on Tolaria.\"\n—Barrin, master wizard').
card_multiverse_id('catalog'/'USG', '9852').

card_in_set('catastrophe', 'USG').
card_original_type('catastrophe'/'USG', 'Sorcery').
card_original_text('catastrophe'/'USG', 'Destroy all lands or all creatures. Creatures destroyed this way cannot regenerate this turn.').
card_first_print('catastrophe', 'USG').
card_image_name('catastrophe'/'USG', 'catastrophe').
card_uid('catastrophe'/'USG', 'USG:Catastrophe:catastrophe').
card_rarity('catastrophe'/'USG', 'Rare').
card_artist('catastrophe'/'USG', 'Andrew Robinson').
card_number('catastrophe'/'USG', '6').
card_flavor_text('catastrophe'/'USG', 'Radiant\'s eyes flashed. \"Go, then,\" the angel spat at Serra, \"and leave this world to those who truly care.\"').
card_multiverse_id('catastrophe'/'USG', '5637').

card_in_set('cathodion', 'USG').
card_original_type('cathodion'/'USG', 'Artifact Creature').
card_original_text('cathodion'/'USG', 'When Cathodion is put into a graveyard from play, add three colorless mana to your mana pool.').
card_first_print('cathodion', 'USG').
card_image_name('cathodion'/'USG', 'cathodion').
card_uid('cathodion'/'USG', 'USG:Cathodion:cathodion').
card_rarity('cathodion'/'USG', 'Uncommon').
card_artist('cathodion'/'USG', 'Henry G. Higgenbotham').
card_number('cathodion'/'USG', '287').
card_flavor_text('cathodion'/'USG', 'Instead of creating a tool that would be damaged by heat, the Thran built one that was charged by it.').
card_multiverse_id('cathodion'/'USG', '10659').

card_in_set('cave tiger', 'USG').
card_original_type('cave tiger'/'USG', 'Summon — Cat').
card_original_text('cave tiger'/'USG', 'Whenever a creature blocks it, Cave Tiger gets +1/+1 until end of turn.').
card_first_print('cave tiger', 'USG').
card_image_name('cave tiger'/'USG', 'cave tiger').
card_uid('cave tiger'/'USG', 'USG:Cave Tiger:cave tiger').
card_rarity('cave tiger'/'USG', 'Common').
card_artist('cave tiger'/'USG', 'Hannibal King').
card_number('cave tiger'/'USG', '241').
card_flavor_text('cave tiger'/'USG', 'The druids found a haven in the cool limestone tunnels beneath Argoth. The invaders found only tigers.').
card_multiverse_id('cave tiger'/'USG', '5762').

card_in_set('child of gaea', 'USG').
card_original_type('child of gaea'/'USG', 'Summon — Elemental').
card_original_text('child of gaea'/'USG', 'Trample\nDuring your upkeep, pay {G}{G} or sacrifice Child of Gaea.\n{1}{G} Regenerate Child of Gaea.').
card_first_print('child of gaea', 'USG').
card_image_name('child of gaea'/'USG', 'child of gaea').
card_uid('child of gaea'/'USG', 'USG:Child of Gaea:child of gaea').
card_rarity('child of gaea'/'USG', 'Rare').
card_artist('child of gaea'/'USG', 'Paolo Parente').
card_number('child of gaea'/'USG', '242').
card_multiverse_id('child of gaea'/'USG', '9678').

card_in_set('chimeric staff', 'USG').
card_original_type('chimeric staff'/'USG', 'Artifact').
card_original_text('chimeric staff'/'USG', '{X} Chimeric Staff is an artifact creature with power and toughness each equal to X until end of turn.').
card_first_print('chimeric staff', 'USG').
card_image_name('chimeric staff'/'USG', 'chimeric staff').
card_uid('chimeric staff'/'USG', 'USG:Chimeric Staff:chimeric staff').
card_rarity('chimeric staff'/'USG', 'Rare').
card_artist('chimeric staff'/'USG', 'Michael Sutfin').
card_number('chimeric staff'/'USG', '288').
card_flavor_text('chimeric staff'/'USG', 'A snake in the grasp.').
card_multiverse_id('chimeric staff'/'USG', '8858').

card_in_set('citanul centaurs', 'USG').
card_original_type('citanul centaurs'/'USG', 'Summon — Centaurs').
card_original_text('citanul centaurs'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nCitanul Centaurs cannot be the target of spells or abilities.').
card_first_print('citanul centaurs', 'USG').
card_image_name('citanul centaurs'/'USG', 'citanul centaurs').
card_uid('citanul centaurs'/'USG', 'USG:Citanul Centaurs:citanul centaurs').
card_rarity('citanul centaurs'/'USG', 'Rare').
card_artist('citanul centaurs'/'USG', 'Val Mayerik').
card_number('citanul centaurs'/'USG', '243').
card_multiverse_id('citanul centaurs'/'USG', '5552').

card_in_set('citanul flute', 'USG').
card_original_type('citanul flute'/'USG', 'Artifact').
card_original_text('citanul flute'/'USG', '{X}, {T}: Search your library for a creature card with total casting cost no greater than X. Reveal that card and put it into your hand. Shuffle your library afterward.').
card_first_print('citanul flute', 'USG').
card_image_name('citanul flute'/'USG', 'citanul flute').
card_uid('citanul flute'/'USG', 'USG:Citanul Flute:citanul flute').
card_rarity('citanul flute'/'USG', 'Rare').
card_artist('citanul flute'/'USG', 'Berry').
card_number('citanul flute'/'USG', '289').
card_multiverse_id('citanul flute'/'USG', '8868').

card_in_set('citanul hierophants', 'USG').
card_original_type('citanul hierophants'/'USG', 'Summon — Druids').
card_original_text('citanul hierophants'/'USG', 'Each creature you control gains \"{T}: Add {G} to your mana pool. Play this ability as a mana source.\"').
card_first_print('citanul hierophants', 'USG').
card_image_name('citanul hierophants'/'USG', 'citanul hierophants').
card_uid('citanul hierophants'/'USG', 'USG:Citanul Hierophants:citanul hierophants').
card_rarity('citanul hierophants'/'USG', 'Rare').
card_artist('citanul hierophants'/'USG', 'Vincent Evans').
card_number('citanul hierophants'/'USG', '244').
card_flavor_text('citanul hierophants'/'USG', 'From deep in the caves beneath the forest, the hierophants planned the druids\' raids against the enemy.').
card_multiverse_id('citanul hierophants'/'USG', '5724').

card_in_set('claws of gix', 'USG').
card_original_type('claws of gix'/'USG', 'Artifact').
card_original_text('claws of gix'/'USG', '{1}, Sacrifice a permanent: Gain 1 life.').
card_first_print('claws of gix', 'USG').
card_image_name('claws of gix'/'USG', 'claws of gix').
card_uid('claws of gix'/'USG', 'USG:Claws of Gix:claws of gix').
card_rarity('claws of gix'/'USG', 'Uncommon').
card_artist('claws of gix'/'USG', 'Henry G. Higgenbotham').
card_number('claws of gix'/'USG', '290').
card_flavor_text('claws of gix'/'USG', 'When the Brotherhood of Gix dug out the cave of Koilos they found their master\'s severed hand. They enshrined it, hoping that one day it would point the way to Phyrexia.').
card_multiverse_id('claws of gix'/'USG', '8890').

card_in_set('clear', 'USG').
card_original_type('clear'/'USG', 'Instant').
card_original_text('clear'/'USG', 'Destroy target enchantment.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('clear', 'USG').
card_image_name('clear'/'USG', 'clear').
card_uid('clear'/'USG', 'USG:Clear:clear').
card_rarity('clear'/'USG', 'Uncommon').
card_artist('clear'/'USG', 'Andrew Robinson').
card_number('clear'/'USG', '7').
card_multiverse_id('clear'/'USG', '5692').

card_in_set('cloak of mists', 'USG').
card_original_type('cloak of mists'/'USG', 'Enchant Creature').
card_original_text('cloak of mists'/'USG', 'Enchanted creature is unblockable.').
card_first_print('cloak of mists', 'USG').
card_image_name('cloak of mists'/'USG', 'cloak of mists').
card_uid('cloak of mists'/'USG', 'USG:Cloak of Mists:cloak of mists').
card_rarity('cloak of mists'/'USG', 'Common').
card_artist('cloak of mists'/'USG', 'John Matson').
card_number('cloak of mists'/'USG', '65').
card_flavor_text('cloak of mists'/'USG', '\"All we could lose, we did. All we could keep, we do. And both are shrouded by mists.\"\n—Barrin, master wizard').
card_multiverse_id('cloak of mists'/'USG', '5718').

card_in_set('confiscate', 'USG').
card_original_type('confiscate'/'USG', 'Enchant Permanent').
card_original_text('confiscate'/'USG', 'You control enchanted permanent.').
card_first_print('confiscate', 'USG').
card_image_name('confiscate'/'USG', 'confiscate').
card_uid('confiscate'/'USG', 'USG:Confiscate:confiscate').
card_rarity('confiscate'/'USG', 'Uncommon').
card_artist('confiscate'/'USG', 'Adam Rex').
card_number('confiscate'/'USG', '66').
card_flavor_text('confiscate'/'USG', '\"I don\'t understand why he works so hard on a device to duplicate a sound so easily made with hand and armpit.\"\n—Barrin, progress report').
card_multiverse_id('confiscate'/'USG', '10426').

card_in_set('congregate', 'USG').
card_original_type('congregate'/'USG', 'Instant').
card_original_text('congregate'/'USG', 'Target player gains 2 life for each creature in play.').
card_first_print('congregate', 'USG').
card_image_name('congregate'/'USG', 'congregate').
card_uid('congregate'/'USG', 'USG:Congregate:congregate').
card_rarity('congregate'/'USG', 'Common').
card_artist('congregate'/'USG', 'Mark Zug').
card_number('congregate'/'USG', '8').
card_flavor_text('congregate'/'USG', '\"In the gathering there is strength for all who founder, renewal for all who languish, love for all who sing.\"\n—Song of All, canto 642').
card_multiverse_id('congregate'/'USG', '9702').

card_in_set('contamination', 'USG').
card_original_type('contamination'/'USG', 'Enchantment').
card_original_text('contamination'/'USG', 'During your upkeep, sacrifice a creature or sacrifice Contamination.\nWhenever a land is tapped for mana, it produces {B} instead of its normal type and amount.').
card_first_print('contamination', 'USG').
card_image_name('contamination'/'USG', 'contamination').
card_uid('contamination'/'USG', 'USG:Contamination:contamination').
card_rarity('contamination'/'USG', 'Rare').
card_artist('contamination'/'USG', 'Stephen Daniele').
card_number('contamination'/'USG', '123').
card_multiverse_id('contamination'/'USG', '5768').

card_in_set('copper gnomes', 'USG').
card_original_type('copper gnomes'/'USG', 'Artifact Creature').
card_original_text('copper gnomes'/'USG', '{4}, Sacrifice Copper Gnomes: Choose an artifact card in your hand and put that artifact into play.').
card_first_print('copper gnomes', 'USG').
card_image_name('copper gnomes'/'USG', 'copper gnomes').
card_uid('copper gnomes'/'USG', 'USG:Copper Gnomes:copper gnomes').
card_rarity('copper gnomes'/'USG', 'Rare').
card_artist('copper gnomes'/'USG', 'Jeff Laubenstein').
card_number('copper gnomes'/'USG', '291').
card_flavor_text('copper gnomes'/'USG', 'Start with eleven gnomes and a room of parts, and come morning you\'ll have ten and a monster the likes of which you\'ve never seen.').
card_multiverse_id('copper gnomes'/'USG', '8815').

card_in_set('coral merfolk', 'USG').
card_original_type('coral merfolk'/'USG', 'Summon — Merfolk').
card_original_text('coral merfolk'/'USG', '').
card_first_print('coral merfolk', 'USG').
card_image_name('coral merfolk'/'USG', 'coral merfolk').
card_uid('coral merfolk'/'USG', 'USG:Coral Merfolk:coral merfolk').
card_rarity('coral merfolk'/'USG', 'Common').
card_artist('coral merfolk'/'USG', 'rk post').
card_number('coral merfolk'/'USG', '67').
card_flavor_text('coral merfolk'/'USG', 'It is not unusual for a single family of coral merfolk to spend centuries carefully guiding the growth of the reefs where they make their home.').
card_multiverse_id('coral merfolk'/'USG', '5613').

card_in_set('corrupt', 'USG').
card_original_type('corrupt'/'USG', 'Sorcery').
card_original_text('corrupt'/'USG', 'Corrupt deals 1 damage to target creature or player for each swamp you control. When Corrupt successfully deals damage to a creature or player, gain life equal to that damage.').
card_image_name('corrupt'/'USG', 'corrupt').
card_uid('corrupt'/'USG', 'USG:Corrupt:corrupt').
card_rarity('corrupt'/'USG', 'Common').
card_artist('corrupt'/'USG', 'Vincent Evans').
card_number('corrupt'/'USG', '124').
card_flavor_text('corrupt'/'USG', 'Yawgmoth brushed Urza\'s mind, and Urza\'s world convulsed.').
card_multiverse_id('corrupt'/'USG', '5853').

card_in_set('cradle guard', 'USG').
card_original_type('cradle guard'/'USG', 'Summon — Treefolk').
card_original_text('cradle guard'/'USG', 'Trample; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_first_print('cradle guard', 'USG').
card_image_name('cradle guard'/'USG', 'cradle guard').
card_uid('cradle guard'/'USG', 'USG:Cradle Guard:cradle guard').
card_rarity('cradle guard'/'USG', 'Uncommon').
card_artist('cradle guard'/'USG', 'Mark Zug').
card_number('cradle guard'/'USG', '245').
card_flavor_text('cradle guard'/'USG', 'Mother, sleep / Dream our lives\nOur roots your soul / Our leaves your bed.').
card_multiverse_id('cradle guard'/'USG', '5665').

card_in_set('crater hellion', 'USG').
card_original_type('crater hellion'/'USG', 'Summon — Beast').
card_original_text('crater hellion'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nWhen Crater Hellion comes into play, it deals 4 damage to each other creature.').
card_first_print('crater hellion', 'USG').
card_image_name('crater hellion'/'USG', 'crater hellion').
card_uid('crater hellion'/'USG', 'USG:Crater Hellion:crater hellion').
card_rarity('crater hellion'/'USG', 'Rare').
card_artist('crater hellion'/'USG', 'Daren Bader').
card_number('crater hellion'/'USG', '179').
card_multiverse_id('crater hellion'/'USG', '5546').

card_in_set('crazed skirge', 'USG').
card_original_type('crazed skirge'/'USG', 'Summon — Imp').
card_original_text('crazed skirge'/'USG', 'Flying\nCrazed Skirge is unaffected by summoning sickness.').
card_first_print('crazed skirge', 'USG').
card_image_name('crazed skirge'/'USG', 'crazed skirge').
card_uid('crazed skirge'/'USG', 'USG:Crazed Skirge:crazed skirge').
card_rarity('crazed skirge'/'USG', 'Uncommon').
card_artist('crazed skirge'/'USG', 'Ron Spencer').
card_number('crazed skirge'/'USG', '125').
card_flavor_text('crazed skirge'/'USG', 'They are Phyrexia\'s couriers; the messages they carry are inscribed on their slick hides.').
card_multiverse_id('crazed skirge'/'USG', '5555').

card_in_set('crosswinds', 'USG').
card_original_type('crosswinds'/'USG', 'Enchantment').
card_original_text('crosswinds'/'USG', 'All creatures with flying get -2/-0.').
card_first_print('crosswinds', 'USG').
card_image_name('crosswinds'/'USG', 'crosswinds').
card_uid('crosswinds'/'USG', 'USG:Crosswinds:crosswinds').
card_rarity('crosswinds'/'USG', 'Uncommon').
card_artist('crosswinds'/'USG', 'Randy Elliott').
card_number('crosswinds'/'USG', '246').
card_flavor_text('crosswinds'/'USG', 'Harbin\'s ornithopter had been trapped for two days within the currents of the storm. When the skies cleared, all he could see was a horizon of trees.').
card_multiverse_id('crosswinds'/'USG', '5668').

card_in_set('crystal chimes', 'USG').
card_original_type('crystal chimes'/'USG', 'Artifact').
card_original_text('crystal chimes'/'USG', '{3}, {T}, Sacrifice Crystal Chimes: Return all enchantment cards from your graveyard to your hand.').
card_first_print('crystal chimes', 'USG').
card_image_name('crystal chimes'/'USG', 'crystal chimes').
card_uid('crystal chimes'/'USG', 'USG:Crystal Chimes:crystal chimes').
card_rarity('crystal chimes'/'USG', 'Uncommon').
card_artist('crystal chimes'/'USG', 'Donato Giancola').
card_number('crystal chimes'/'USG', '292').
card_flavor_text('crystal chimes'/'USG', 'As Serra was to learn, the peace and sanctity of her realm were as fragile as glass.').
card_multiverse_id('crystal chimes'/'USG', '5742').

card_in_set('curfew', 'USG').
card_original_type('curfew'/'USG', 'Instant').
card_original_text('curfew'/'USG', 'Each player chooses a creature he or she controls and returns it to owner\'s hand.').
card_first_print('curfew', 'USG').
card_image_name('curfew'/'USG', 'curfew').
card_uid('curfew'/'USG', 'USG:Curfew:curfew').
card_rarity('curfew'/'USG', 'Common').
card_artist('curfew'/'USG', 'Randy Gallegos').
card_number('curfew'/'USG', '68').
card_flavor_text('curfew'/'USG', '\". . . But I\'m not tired!\"').
card_multiverse_id('curfew'/'USG', '7817').

card_in_set('dark hatchling', 'USG').
card_original_type('dark hatchling'/'USG', 'Summon — Horror').
card_original_text('dark hatchling'/'USG', 'Flying\nWhen Dark Hatchling comes into play, destroy target nonblack creature. That creature cannot be regenerated this turn.').
card_first_print('dark hatchling', 'USG').
card_image_name('dark hatchling'/'USG', 'dark hatchling').
card_uid('dark hatchling'/'USG', 'USG:Dark Hatchling:dark hatchling').
card_rarity('dark hatchling'/'USG', 'Rare').
card_artist('dark hatchling'/'USG', 'Mark A. Nelson').
card_number('dark hatchling'/'USG', '126').
card_multiverse_id('dark hatchling'/'USG', '5852').

card_in_set('dark ritual', 'USG').
card_original_type('dark ritual'/'USG', 'Mana Source').
card_original_text('dark ritual'/'USG', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'USG', 'dark ritual').
card_uid('dark ritual'/'USG', 'USG:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'USG', 'Common').
card_artist('dark ritual'/'USG', 'Tom Fleming').
card_number('dark ritual'/'USG', '127').
card_flavor_text('dark ritual'/'USG', '\"From void evolved Phyrexia. Great Yawgmoth, Father of Machines, saw its perfection. Thus The Grand Evolution began.\"\n—Phyrexian Scriptures').
card_multiverse_id('dark ritual'/'USG', '5626').

card_in_set('darkest hour', 'USG').
card_original_type('darkest hour'/'USG', 'Enchantment').
card_original_text('darkest hour'/'USG', 'All creatures are black.').
card_first_print('darkest hour', 'USG').
card_image_name('darkest hour'/'USG', 'darkest hour').
card_uid('darkest hour'/'USG', 'USG:Darkest Hour:darkest hour').
card_rarity('darkest hour'/'USG', 'Rare').
card_artist('darkest hour'/'USG', 'Heather Hudson').
card_number('darkest hour'/'USG', '128').
card_flavor_text('darkest hour'/'USG', '\"Yawgmoth spent eons wrapping Phyrexians in human skin. They are the sleeper agents, and they are everywhere.\"\n—Xantcha, to Urza').
card_multiverse_id('darkest hour'/'USG', '5622').

card_in_set('defensive formation', 'USG').
card_original_type('defensive formation'/'USG', 'Enchantment').
card_original_text('defensive formation'/'USG', 'Instead of the attacking player, you choose how creatures attacking you deal combat damage.').
card_first_print('defensive formation', 'USG').
card_image_name('defensive formation'/'USG', 'defensive formation').
card_uid('defensive formation'/'USG', 'USG:Defensive Formation:defensive formation').
card_rarity('defensive formation'/'USG', 'Uncommon').
card_artist('defensive formation'/'USG', 'Greg Staples').
card_number('defensive formation'/'USG', '9').
card_flavor_text('defensive formation'/'USG', '\"Your enemies will pound upon the door of your defenses, but only you shall have the key, and it is the key of life.\"\n—Song of All, canto 873').
card_multiverse_id('defensive formation'/'USG', '5588').

card_in_set('despondency', 'USG').
card_original_type('despondency'/'USG', 'Enchant Creature').
card_original_text('despondency'/'USG', 'Enchanted creature gets -2/-0.\nWhen Despondency is put into a graveyard from play, return Despondency to owner\'s hand.').
card_first_print('despondency', 'USG').
card_image_name('despondency'/'USG', 'despondency').
card_uid('despondency'/'USG', 'USG:Despondency:despondency').
card_rarity('despondency'/'USG', 'Common').
card_artist('despondency'/'USG', 'D. Alexander Gregory').
card_number('despondency'/'USG', '129').
card_multiverse_id('despondency'/'USG', '10656').

card_in_set('destructive urge', 'USG').
card_original_type('destructive urge'/'USG', 'Enchant Creature').
card_original_text('destructive urge'/'USG', 'Whenever enchanted creature successfully deals combat damage to a player, that player sacrifices a land.').
card_first_print('destructive urge', 'USG').
card_image_name('destructive urge'/'USG', 'destructive urge').
card_uid('destructive urge'/'USG', 'USG:Destructive Urge:destructive urge').
card_rarity('destructive urge'/'USG', 'Uncommon').
card_artist('destructive urge'/'USG', 'Andrew Robinson').
card_number('destructive urge'/'USG', '180').
card_flavor_text('destructive urge'/'USG', 'Red sky at night, dragon\'s delight.').
card_multiverse_id('destructive urge'/'USG', '10658').

card_in_set('diabolic servitude', 'USG').
card_original_type('diabolic servitude'/'USG', 'Enchantment').
card_original_text('diabolic servitude'/'USG', 'When Diabolic Servitude comes into play, choose target creature card in your graveyard and put that creature into play. \nWhen the chosen creature is put into a graveyard, remove the creature from the game and return Diabolic Servitude to owner\'s hand.\nWhen Diabolic Servitude leaves play, remove the chosen creature from the game.').
card_first_print('diabolic servitude', 'USG').
card_image_name('diabolic servitude'/'USG', 'diabolic servitude').
card_uid('diabolic servitude'/'USG', 'USG:Diabolic Servitude:diabolic servitude').
card_rarity('diabolic servitude'/'USG', 'Uncommon').
card_artist('diabolic servitude'/'USG', 'Scott M. Fischer').
card_number('diabolic servitude'/'USG', '130').
card_multiverse_id('diabolic servitude'/'USG', '10427').

card_in_set('disciple of grace', 'USG').
card_original_type('disciple of grace'/'USG', 'Summon — Cleric').
card_original_text('disciple of grace'/'USG', 'Protection from black\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('disciple of grace', 'USG').
card_image_name('disciple of grace'/'USG', 'disciple of grace').
card_uid('disciple of grace'/'USG', 'USG:Disciple of Grace:disciple of grace').
card_rarity('disciple of grace'/'USG', 'Common').
card_artist('disciple of grace'/'USG', 'Robh Ruppel').
card_number('disciple of grace'/'USG', '10').
card_flavor_text('disciple of grace'/'USG', 'Beauty is beyond law.').
card_multiverse_id('disciple of grace'/'USG', '5634').

card_in_set('disciple of law', 'USG').
card_original_type('disciple of law'/'USG', 'Summon — Cleric').
card_original_text('disciple of law'/'USG', 'Protection from red\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('disciple of law', 'USG').
card_image_name('disciple of law'/'USG', 'disciple of law').
card_uid('disciple of law'/'USG', 'USG:Disciple of Law:disciple of law').
card_rarity('disciple of law'/'USG', 'Common').
card_artist('disciple of law'/'USG', 'Matthew D. Wilson').
card_number('disciple of law'/'USG', '11').
card_flavor_text('disciple of law'/'USG', 'A religious order for religious order.').
card_multiverse_id('disciple of law'/'USG', '5672').

card_in_set('discordant dirge', 'USG').
card_original_type('discordant dirge'/'USG', 'Enchantment').
card_original_text('discordant dirge'/'USG', 'During your upkeep, you may put a verse counter on Discordant Dirge.\n{B}, Sacrifice Discordant Dirge: Look at target opponent\'s hand and choose up to X of those cards, where X is the number of verse counters on Discordant Dirge. That player discards those cards.').
card_first_print('discordant dirge', 'USG').
card_image_name('discordant dirge'/'USG', 'discordant dirge').
card_uid('discordant dirge'/'USG', 'USG:Discordant Dirge:discordant dirge').
card_rarity('discordant dirge'/'USG', 'Rare').
card_artist('discordant dirge'/'USG', 'Carl Critchlow').
card_number('discordant dirge'/'USG', '131').
card_multiverse_id('discordant dirge'/'USG', '8446').

card_in_set('disenchant', 'USG').
card_original_type('disenchant'/'USG', 'Instant').
card_original_text('disenchant'/'USG', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'USG', 'disenchant').
card_uid('disenchant'/'USG', 'USG:Disenchant:disenchant').
card_rarity('disenchant'/'USG', 'Common').
card_artist('disenchant'/'USG', 'Donato Giancola').
card_number('disenchant'/'USG', '12').
card_flavor_text('disenchant'/'USG', '\"Let Phyrexia breed evil in the darkness; my holy light will reveal its taint.\"\n—Serra').
card_multiverse_id('disenchant'/'USG', '5630').

card_in_set('disorder', 'USG').
card_original_type('disorder'/'USG', 'Sorcery').
card_original_text('disorder'/'USG', 'Disorder deals 2 damage to each white creature and each player who controls a white creature.').
card_first_print('disorder', 'USG').
card_image_name('disorder'/'USG', 'disorder').
card_uid('disorder'/'USG', 'USG:Disorder:disorder').
card_rarity('disorder'/'USG', 'Uncommon').
card_artist('disorder'/'USG', 'Terese Nielsen').
card_number('disorder'/'USG', '181').
card_flavor_text('disorder'/'USG', '\"Then, just when the other guys were winnin\', the sky threw up.\"\n—Jula, goblin raider').
card_multiverse_id('disorder'/'USG', '5854').

card_in_set('disruptive student', 'USG').
card_original_type('disruptive student'/'USG', 'Summon — Wizard').
card_original_text('disruptive student'/'USG', '{T}: Counter target spell unless its caster pays an additional {1}. Play this ability as an interrupt.').
card_first_print('disruptive student', 'USG').
card_image_name('disruptive student'/'USG', 'disruptive student').
card_uid('disruptive student'/'USG', 'USG:Disruptive Student:disruptive student').
card_rarity('disruptive student'/'USG', 'Common').
card_artist('disruptive student'/'USG', 'Randy Gallegos').
card_number('disruptive student'/'USG', '69').
card_flavor_text('disruptive student'/'USG', '\"Teferi is a problem student. Always late for class. No appreciation for constructive use of time.\"\n—Barrin, progress report').
card_multiverse_id('disruptive student'/'USG', '10695').

card_in_set('douse', 'USG').
card_original_type('douse'/'USG', 'Enchantment').
card_original_text('douse'/'USG', '{1}{U} Counter target red spell. Play this ability as an interrupt.').
card_first_print('douse', 'USG').
card_image_name('douse'/'USG', 'douse').
card_uid('douse'/'USG', 'USG:Douse:douse').
card_rarity('douse'/'USG', 'Uncommon').
card_artist('douse'/'USG', 'Val Mayerik').
card_number('douse'/'USG', '70').
card_flavor_text('douse'/'USG', 'The academy\'s libraries were protected by fire-prevention spells. Even after the disaster, the books were intact—though forever sealed in time.').
card_multiverse_id('douse'/'USG', '5723').

card_in_set('dragon blood', 'USG').
card_original_type('dragon blood'/'USG', 'Artifact').
card_original_text('dragon blood'/'USG', '{3}, {T}: Put a +1/+1 counter on target creature.').
card_first_print('dragon blood', 'USG').
card_image_name('dragon blood'/'USG', 'dragon blood').
card_uid('dragon blood'/'USG', 'USG:Dragon Blood:dragon blood').
card_rarity('dragon blood'/'USG', 'Uncommon').
card_artist('dragon blood'/'USG', 'Greg Simanson').
card_number('dragon blood'/'USG', '293').
card_flavor_text('dragon blood'/'USG', 'Fire in the blood, fire in the belly.').
card_multiverse_id('dragon blood'/'USG', '8870').

card_in_set('drifting djinn', 'USG').
card_original_type('drifting djinn'/'USG', 'Summon — Djinn').
card_original_text('drifting djinn'/'USG', 'Flying\nDuring your upkeep, pay {1}{U} or sacrifice Drifting Djinn.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('drifting djinn', 'USG').
card_image_name('drifting djinn'/'USG', 'drifting djinn').
card_uid('drifting djinn'/'USG', 'USG:Drifting Djinn:drifting djinn').
card_rarity('drifting djinn'/'USG', 'Rare').
card_artist('drifting djinn'/'USG', 'Carl Critchlow').
card_number('drifting djinn'/'USG', '71').
card_multiverse_id('drifting djinn'/'USG', '5719').

card_in_set('drifting meadow', 'USG').
card_original_type('drifting meadow'/'USG', 'Land').
card_original_text('drifting meadow'/'USG', 'Drifting Meadow comes into play tapped.\n{T}: Add {W} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('drifting meadow', 'USG').
card_image_name('drifting meadow'/'USG', 'drifting meadow').
card_uid('drifting meadow'/'USG', 'USG:Drifting Meadow:drifting meadow').
card_rarity('drifting meadow'/'USG', 'Common').
card_artist('drifting meadow'/'USG', 'Bob Eggleton').
card_number('drifting meadow'/'USG', '320').
card_multiverse_id('drifting meadow'/'USG', '5805').

card_in_set('dromosaur', 'USG').
card_original_type('dromosaur'/'USG', 'Summon — Lizard').
card_original_text('dromosaur'/'USG', 'Whenever Dromosaur blocks or becomes blocked, it gets +2/-2 until end of turn.').
card_first_print('dromosaur', 'USG').
card_image_name('dromosaur'/'USG', 'dromosaur').
card_uid('dromosaur'/'USG', 'USG:Dromosaur:dromosaur').
card_rarity('dromosaur'/'USG', 'Common').
card_artist('dromosaur'/'USG', 'Ciruelo').
card_number('dromosaur'/'USG', '182').
card_flavor_text('dromosaur'/'USG', 'They say dromosaurs are frightened of dogs, even little ones. There are no dogs in Shiv. Not even little ones.').
card_multiverse_id('dromosaur'/'USG', '5734').

card_in_set('duress', 'USG').
card_original_type('duress'/'USG', 'Sorcery').
card_original_text('duress'/'USG', 'Look at target opponent\'s hand and choose a noncreature, nonland card there. That player discards that card.').
card_image_name('duress'/'USG', 'duress').
card_uid('duress'/'USG', 'USG:Duress:duress').
card_rarity('duress'/'USG', 'Common').
card_artist('duress'/'USG', 'Lawrence Snelly').
card_number('duress'/'USG', '132').
card_flavor_text('duress'/'USG', '\"We decide who is worthy of our works.\"\n—Gix, Yawgmoth praetor').
card_multiverse_id('duress'/'USG', '5557').

card_in_set('eastern paladin', 'USG').
card_original_type('eastern paladin'/'USG', 'Summon — Knight').
card_original_text('eastern paladin'/'USG', '{B}{B}, {T}: Destroy target green creature.').
card_first_print('eastern paladin', 'USG').
card_image_name('eastern paladin'/'USG', 'eastern paladin').
card_uid('eastern paladin'/'USG', 'USG:Eastern Paladin:eastern paladin').
card_rarity('eastern paladin'/'USG', 'Rare').
card_artist('eastern paladin'/'USG', 'Carl Critchlow').
card_number('eastern paladin'/'USG', '133').
card_flavor_text('eastern paladin'/'USG', '\"Their fragile world. Their futile lives. They obstruct the Grand Evolution. In Yawgmoth\'s name, we shall excise them.\"\n—Oath of the East').
card_multiverse_id('eastern paladin'/'USG', '5860').

card_in_set('electryte', 'USG').
card_original_type('electryte'/'USG', 'Summon — Beast').
card_original_text('electryte'/'USG', 'Whenever Electryte successfully deals combat damage to defending player, Electryte deals damage equal to its power to each blocking creature.').
card_first_print('electryte', 'USG').
card_image_name('electryte'/'USG', 'electryte').
card_uid('electryte'/'USG', 'USG:Electryte:electryte').
card_rarity('electryte'/'USG', 'Rare').
card_artist('electryte'/'USG', 'Thomas M. Baxa').
card_number('electryte'/'USG', '183').
card_flavor_text('electryte'/'USG', 'Shivan inhabitants are hardened to fire, so their predators have developed alternative weaponry.').
card_multiverse_id('electryte'/'USG', '9700').

card_in_set('elite archers', 'USG').
card_original_type('elite archers'/'USG', 'Summon — Soldiers').
card_original_text('elite archers'/'USG', '{T}: Elite Archers deals 3 damage to target attacking or blocking creature.').
card_first_print('elite archers', 'USG').
card_image_name('elite archers'/'USG', 'elite archers').
card_uid('elite archers'/'USG', 'USG:Elite Archers:elite archers').
card_rarity('elite archers'/'USG', 'Rare').
card_artist('elite archers'/'USG', 'Greg Staples').
card_number('elite archers'/'USG', '13').
card_flavor_text('elite archers'/'USG', 'Arrows fletched with the feathers of angels seldom miss their mark.').
card_multiverse_id('elite archers'/'USG', '5771').

card_in_set('elvish herder', 'USG').
card_original_type('elvish herder'/'USG', 'Summon — Elf').
card_original_text('elvish herder'/'USG', '{G} Target creature gains trample until end of turn.').
card_first_print('elvish herder', 'USG').
card_image_name('elvish herder'/'USG', 'elvish herder').
card_uid('elvish herder'/'USG', 'USG:Elvish Herder:elvish herder').
card_rarity('elvish herder'/'USG', 'Common').
card_artist('elvish herder'/'USG', 'Tom Fleming').
card_number('elvish herder'/'USG', '247').
card_flavor_text('elvish herder'/'USG', 'Before Urza and Mishra came to Argoth, the herders prevented their creatures from stampeding. During the war, they encouraged it.').
card_multiverse_id('elvish herder'/'USG', '5738').

card_in_set('elvish lyrist', 'USG').
card_original_type('elvish lyrist'/'USG', 'Summon — Elf').
card_original_text('elvish lyrist'/'USG', '{G}, {T}, Sacrifice Elvish Lyrist: Destroy target enchantment.').
card_first_print('elvish lyrist', 'USG').
card_image_name('elvish lyrist'/'USG', 'elvish lyrist').
card_uid('elvish lyrist'/'USG', 'USG:Elvish Lyrist:elvish lyrist').
card_rarity('elvish lyrist'/'USG', 'Common').
card_artist('elvish lyrist'/'USG', 'Rebecca Guay').
card_number('elvish lyrist'/'USG', '248').
card_flavor_text('elvish lyrist'/'USG', 'Bring the spear of ancient briar;\nBring the torch to light the pyre.\nBring the one who trod our ground;\nBring the spade to dig his mound.').
card_multiverse_id('elvish lyrist'/'USG', '5774').

card_in_set('enchantment alteration', 'USG').
card_original_type('enchantment alteration'/'USG', 'Instant').
card_original_text('enchantment alteration'/'USG', 'Move target enchantment from one creature to another or from one land to another. (The enchantment\'s new target must be legal.)').
card_image_name('enchantment alteration'/'USG', 'enchantment alteration').
card_uid('enchantment alteration'/'USG', 'USG:Enchantment Alteration:enchantment alteration').
card_rarity('enchantment alteration'/'USG', 'Uncommon').
card_artist('enchantment alteration'/'USG', 'D. Alexander Gregory').
card_number('enchantment alteration'/'USG', '72').
card_multiverse_id('enchantment alteration'/'USG', '8825').

card_in_set('endless wurm', 'USG').
card_original_type('endless wurm'/'USG', 'Summon — Wurm').
card_original_text('endless wurm'/'USG', 'Trample\nDuring your upkeep, sacrifice an enchantment or sacrifice Endless Wurm.').
card_first_print('endless wurm', 'USG').
card_image_name('endless wurm'/'USG', 'endless wurm').
card_uid('endless wurm'/'USG', 'USG:Endless Wurm:endless wurm').
card_rarity('endless wurm'/'USG', 'Rare').
card_artist('endless wurm'/'USG', 'DiTerlizzi').
card_number('endless wurm'/'USG', '249').
card_flavor_text('endless wurm'/'USG', 'Ages ago, a party of elves took cover to let one pass. They\'re still waiting.').
card_multiverse_id('endless wurm'/'USG', '8820').

card_in_set('endoskeleton', 'USG').
card_original_type('endoskeleton'/'USG', 'Artifact').
card_original_text('endoskeleton'/'USG', 'You may choose not to untap Endoskeleton during your untap phase.\n{2}, {T}: Target creature gets +0/+3 as long as Endoskeleton remains tapped.').
card_first_print('endoskeleton', 'USG').
card_image_name('endoskeleton'/'USG', 'endoskeleton').
card_uid('endoskeleton'/'USG', 'USG:Endoskeleton:endoskeleton').
card_rarity('endoskeleton'/'USG', 'Uncommon').
card_artist('endoskeleton'/'USG', 'Mark Tedin').
card_number('endoskeleton'/'USG', '294').
card_multiverse_id('endoskeleton'/'USG', '8846').

card_in_set('energy field', 'USG').
card_original_type('energy field'/'USG', 'Enchantment').
card_original_text('energy field'/'USG', 'Prevent all damage dealt to you from sources you do not control. \nWhen a card is put into your graveyard, sacrifice Energy Field.').
card_first_print('energy field', 'USG').
card_image_name('energy field'/'USG', 'energy field').
card_uid('energy field'/'USG', 'USG:Energy Field:energy field').
card_rarity('energy field'/'USG', 'Rare').
card_artist('energy field'/'USG', 'John Matson').
card_number('energy field'/'USG', '73').
card_multiverse_id('energy field'/'USG', '10421').

card_in_set('exhaustion', 'USG').
card_original_type('exhaustion'/'USG', 'Sorcery').
card_original_text('exhaustion'/'USG', 'Creatures and lands target opponent controls do not untap during his or her next untap phase.').
card_image_name('exhaustion'/'USG', 'exhaustion').
card_uid('exhaustion'/'USG', 'USG:Exhaustion:exhaustion').
card_rarity('exhaustion'/'USG', 'Uncommon').
card_artist('exhaustion'/'USG', 'Paolo Parente').
card_number('exhaustion'/'USG', '74').
card_flavor_text('exhaustion'/'USG', 'The mage felt as though he\'d been in the stasis suit for days. Upon his return, he found it was months.').
card_multiverse_id('exhaustion'/'USG', '5650').

card_in_set('exhume', 'USG').
card_original_type('exhume'/'USG', 'Sorcery').
card_original_text('exhume'/'USG', 'Each player chooses a creature card in his or her graveyard and puts that creature into play.').
card_first_print('exhume', 'USG').
card_image_name('exhume'/'USG', 'exhume').
card_uid('exhume'/'USG', 'USG:Exhume:exhume').
card_rarity('exhume'/'USG', 'Common').
card_artist('exhume'/'USG', 'Carl Critchlow').
card_number('exhume'/'USG', '134').
card_flavor_text('exhume'/'USG', '\"Death—an outmoded concept. We sleep, and we change.\"\n—Sitrik, birth priest').
card_multiverse_id('exhume'/'USG', '5556').

card_in_set('exploration', 'USG').
card_original_type('exploration'/'USG', 'Enchantment').
card_original_text('exploration'/'USG', 'You may play an additional land each turn.').
card_first_print('exploration', 'USG').
card_image_name('exploration'/'USG', 'exploration').
card_uid('exploration'/'USG', 'USG:Exploration:exploration').
card_rarity('exploration'/'USG', 'Rare').
card_artist('exploration'/'USG', 'Brian Snõddy').
card_number('exploration'/'USG', '250').
card_flavor_text('exploration'/'USG', 'The first explorers found Argoth a storehouse of natural wealth—towering forests grown over rich veins of ore.').
card_multiverse_id('exploration'/'USG', '8823').

card_in_set('expunge', 'USG').
card_original_type('expunge'/'USG', 'Instant').
card_original_text('expunge'/'USG', 'Destroy target nonartifact, nonblack creature. That creature cannot be regenerated this turn.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('expunge', 'USG').
card_image_name('expunge'/'USG', 'expunge').
card_uid('expunge'/'USG', 'USG:Expunge:expunge').
card_rarity('expunge'/'USG', 'Common').
card_artist('expunge'/'USG', 'Christopher Moeller').
card_number('expunge'/'USG', '135').
card_multiverse_id('expunge'/'USG', '5819').

card_in_set('faith healer', 'USG').
card_original_type('faith healer'/'USG', 'Summon — Cleric').
card_original_text('faith healer'/'USG', 'Sacrifice an enchantment: Gain life equal to the sacrificed enchantment\'s total casting cost.').
card_first_print('faith healer', 'USG').
card_image_name('faith healer'/'USG', 'faith healer').
card_uid('faith healer'/'USG', 'USG:Faith Healer:faith healer').
card_rarity('faith healer'/'USG', 'Rare').
card_artist('faith healer'/'USG', 'Randy Gallegos').
card_number('faith healer'/'USG', '14').
card_flavor_text('faith healer'/'USG', 'The power of faith is quiet. It is the leaf unmoved by the hurricane.').
card_multiverse_id('faith healer'/'USG', '5829').

card_in_set('falter', 'USG').
card_original_type('falter'/'USG', 'Instant').
card_original_text('falter'/'USG', 'Creatures without flying cannot block this turn.').
card_first_print('falter', 'USG').
card_image_name('falter'/'USG', 'falter').
card_uid('falter'/'USG', 'USG:Falter:falter').
card_rarity('falter'/'USG', 'Common').
card_artist('falter'/'USG', 'Mike Raabe').
card_number('falter'/'USG', '184').
card_flavor_text('falter'/'USG', 'Like a sleeping dragon, Shiv stirs and groans at times.').
card_multiverse_id('falter'/'USG', '5542').

card_in_set('fault line', 'USG').
card_original_type('fault line'/'USG', 'Instant').
card_original_text('fault line'/'USG', 'Fault Line deals X damage to each creature without flying and each player.').
card_first_print('fault line', 'USG').
card_image_name('fault line'/'USG', 'fault line').
card_uid('fault line'/'USG', 'USG:Fault Line:fault line').
card_rarity('fault line'/'USG', 'Rare').
card_artist('fault line'/'USG', 'Ron Spencer').
card_number('fault line'/'USG', '185').
card_flavor_text('fault line'/'USG', 'We live on the serpent\'s back.\n—Viashino saying').
card_multiverse_id('fault line'/'USG', '8867').

card_in_set('fecundity', 'USG').
card_original_type('fecundity'/'USG', 'Enchantment').
card_original_text('fecundity'/'USG', 'Whenever a creature is put into a graveyard from play, that creature\'s controller may draw a card.').
card_first_print('fecundity', 'USG').
card_image_name('fecundity'/'USG', 'fecundity').
card_uid('fecundity'/'USG', 'USG:Fecundity:fecundity').
card_rarity('fecundity'/'USG', 'Uncommon').
card_artist('fecundity'/'USG', 'Rebecca Guay').
card_number('fecundity'/'USG', '251').
card_flavor_text('fecundity'/'USG', 'Life is eternal. A lifetime is ephemeral.').
card_multiverse_id('fecundity'/'USG', '5604').

card_in_set('fertile ground', 'USG').
card_original_type('fertile ground'/'USG', 'Enchant Land').
card_original_text('fertile ground'/'USG', 'Whenever enchanted land is tapped for mana, it produces an additional one mana of any color.').
card_first_print('fertile ground', 'USG').
card_image_name('fertile ground'/'USG', 'fertile ground').
card_uid('fertile ground'/'USG', 'USG:Fertile Ground:fertile ground').
card_rarity('fertile ground'/'USG', 'Common').
card_artist('fertile ground'/'USG', 'Heather Hudson').
card_number('fertile ground'/'USG', '252').
card_flavor_text('fertile ground'/'USG', 'The forest was too lush for the brothers to despoil—almost.').
card_multiverse_id('fertile ground'/'USG', '5655').

card_in_set('fiery mantle', 'USG').
card_original_type('fiery mantle'/'USG', 'Enchant Creature').
card_original_text('fiery mantle'/'USG', 'When Fiery Mantle is put into a graveyard from play, return Fiery Mantle to owner\'s hand.\n{R} Enchanted creature gets +1/+0 until end of turn.').
card_first_print('fiery mantle', 'USG').
card_image_name('fiery mantle'/'USG', 'fiery mantle').
card_uid('fiery mantle'/'USG', 'USG:Fiery Mantle:fiery mantle').
card_rarity('fiery mantle'/'USG', 'Common').
card_artist('fiery mantle'/'USG', 'Bob Eggleton').
card_number('fiery mantle'/'USG', '186').
card_multiverse_id('fiery mantle'/'USG', '10425').

card_in_set('fire ants', 'USG').
card_original_type('fire ants'/'USG', 'Summon — Insects').
card_original_text('fire ants'/'USG', '{T}: Fire Ants deals 1 damage to each other creature without flying.').
card_first_print('fire ants', 'USG').
card_image_name('fire ants'/'USG', 'fire ants').
card_uid('fire ants'/'USG', 'USG:Fire Ants:fire ants').
card_rarity('fire ants'/'USG', 'Uncommon').
card_artist('fire ants'/'USG', 'Tom Fleming').
card_number('fire ants'/'USG', '187').
card_flavor_text('fire ants'/'USG', 'Visitors to Shiv fear the dragons, the goblins, or the viashino. Natives fear the ants.').
card_multiverse_id('fire ants'/'USG', '5568').

card_in_set('flesh reaver', 'USG').
card_original_type('flesh reaver'/'USG', 'Summon — Horror').
card_original_text('flesh reaver'/'USG', 'Whenever Flesh Reaver successfully deals damage to a creature or opponent, Flesh Reaver deals an equal amount of damage to you.').
card_first_print('flesh reaver', 'USG').
card_image_name('flesh reaver'/'USG', 'flesh reaver').
card_uid('flesh reaver'/'USG', 'USG:Flesh Reaver:flesh reaver').
card_rarity('flesh reaver'/'USG', 'Uncommon').
card_artist('flesh reaver'/'USG', 'Pete Venters').
card_number('flesh reaver'/'USG', '136').
card_flavor_text('flesh reaver'/'USG', 'Though the reaver is horrifyingly effective, its dorsal vents spit a highly corrosive cloud of filth.').
card_multiverse_id('flesh reaver'/'USG', '5654').

card_in_set('fluctuator', 'USG').
card_original_type('fluctuator'/'USG', 'Artifact').
card_original_text('fluctuator'/'USG', 'Cycling costs you up to {2} less to play.').
card_first_print('fluctuator', 'USG').
card_image_name('fluctuator'/'USG', 'fluctuator').
card_uid('fluctuator'/'USG', 'USG:Fluctuator:fluctuator').
card_rarity('fluctuator'/'USG', 'Rare').
card_artist('fluctuator'/'USG', 'John Matson').
card_number('fluctuator'/'USG', '295').
card_flavor_text('fluctuator'/'USG', 'Fiko summoned only atogs for three straight sessions. The tutor couldn\'t decide whether to punish his failure or praise his consistency.').
card_multiverse_id('fluctuator'/'USG', '5806').

card_in_set('fog bank', 'USG').
card_original_type('fog bank'/'USG', 'Summon — Wall').
card_original_text('fog bank'/'USG', '(Walls cannot attack.)\nFlying\nFog Bank does not deal or receive combat damage.').
card_first_print('fog bank', 'USG').
card_image_name('fog bank'/'USG', 'fog bank').
card_uid('fog bank'/'USG', 'USG:Fog Bank:fog bank').
card_rarity('fog bank'/'USG', 'Uncommon').
card_artist('fog bank'/'USG', 'Scott Kirschner').
card_number('fog bank'/'USG', '75').
card_multiverse_id('fog bank'/'USG', '5753').

card_in_set('forest', 'USG').
card_original_type('forest'/'USG', 'Land').
card_original_text('forest'/'USG', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'USG', 'forest1').
card_uid('forest'/'USG', 'USG:Forest:forest1').
card_rarity('forest'/'USG', 'Basic Land').
card_artist('forest'/'USG', 'Anthony S. Waters').
card_number('forest'/'USG', '347').
card_multiverse_id('forest'/'USG', '8338').

card_in_set('forest', 'USG').
card_original_type('forest'/'USG', 'Land').
card_original_text('forest'/'USG', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'USG', 'forest2').
card_uid('forest'/'USG', 'USG:Forest:forest2').
card_rarity('forest'/'USG', 'Basic Land').
card_artist('forest'/'USG', 'Anthony S. Waters').
card_number('forest'/'USG', '348').
card_multiverse_id('forest'/'USG', '8339').

card_in_set('forest', 'USG').
card_original_type('forest'/'USG', 'Land').
card_original_text('forest'/'USG', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'USG', 'forest3').
card_uid('forest'/'USG', 'USG:Forest:forest3').
card_rarity('forest'/'USG', 'Basic Land').
card_artist('forest'/'USG', 'Anthony S. Waters').
card_number('forest'/'USG', '349').
card_multiverse_id('forest'/'USG', '8340').

card_in_set('forest', 'USG').
card_original_type('forest'/'USG', 'Land').
card_original_text('forest'/'USG', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'USG', 'forest4').
card_uid('forest'/'USG', 'USG:Forest:forest4').
card_rarity('forest'/'USG', 'Basic Land').
card_artist('forest'/'USG', 'Anthony S. Waters').
card_number('forest'/'USG', '350').
card_multiverse_id('forest'/'USG', '8341').

card_in_set('fortitude', 'USG').
card_original_type('fortitude'/'USG', 'Enchant Creature').
card_original_text('fortitude'/'USG', 'When Fortitude is put into a graveyard from play, return Fortitude to owner\'s hand.\nSacrifice a forest: Regenerate enchanted creature.').
card_first_print('fortitude', 'USG').
card_image_name('fortitude'/'USG', 'fortitude').
card_uid('fortitude'/'USG', 'USG:Fortitude:fortitude').
card_rarity('fortitude'/'USG', 'Common').
card_artist('fortitude'/'USG', 'Daren Bader').
card_number('fortitude'/'USG', '253').
card_multiverse_id('fortitude'/'USG', '9850').

card_in_set('gaea\'s bounty', 'USG').
card_original_type('gaea\'s bounty'/'USG', 'Sorcery').
card_original_text('gaea\'s bounty'/'USG', 'Search your library for up to two forest cards, reveal them, and put them into your hand. Shuffle your library afterward.').
card_first_print('gaea\'s bounty', 'USG').
card_image_name('gaea\'s bounty'/'USG', 'gaea\'s bounty').
card_uid('gaea\'s bounty'/'USG', 'USG:Gaea\'s Bounty:gaea\'s bounty').
card_rarity('gaea\'s bounty'/'USG', 'Common').
card_artist('gaea\'s bounty'/'USG', 'Stephen Daniele').
card_number('gaea\'s bounty'/'USG', '254').
card_flavor_text('gaea\'s bounty'/'USG', 'The forest grew back so quickly that lumbering machines were suspended in the treetops.').
card_multiverse_id('gaea\'s bounty'/'USG', '5710').

card_in_set('gaea\'s cradle', 'USG').
card_original_type('gaea\'s cradle'/'USG', 'Legendary Land').
card_original_text('gaea\'s cradle'/'USG', '{T}: Add {G} to your mana pool for each creature you control.').
card_image_name('gaea\'s cradle'/'USG', 'gaea\'s cradle').
card_uid('gaea\'s cradle'/'USG', 'USG:Gaea\'s Cradle:gaea\'s cradle').
card_rarity('gaea\'s cradle'/'USG', 'Rare').
card_artist('gaea\'s cradle'/'USG', 'Mark Zug').
card_number('gaea\'s cradle'/'USG', '321').
card_flavor_text('gaea\'s cradle'/'USG', '\"Here sprouted the first seedling of Argoth. Here the last tree will fall.\"\n—Gamelen, Citanul elder').
card_multiverse_id('gaea\'s cradle'/'USG', '10422').

card_in_set('gaea\'s embrace', 'USG').
card_original_type('gaea\'s embrace'/'USG', 'Enchant Creature').
card_original_text('gaea\'s embrace'/'USG', 'Enchanted creature gets +3/+3 and gains trample.\n{G} Regenerate enchanted creature.').
card_first_print('gaea\'s embrace', 'USG').
card_image_name('gaea\'s embrace'/'USG', 'gaea\'s embrace').
card_uid('gaea\'s embrace'/'USG', 'USG:Gaea\'s Embrace:gaea\'s embrace').
card_rarity('gaea\'s embrace'/'USG', 'Uncommon').
card_artist('gaea\'s embrace'/'USG', 'Paolo Parente').
card_number('gaea\'s embrace'/'USG', '255').
card_flavor_text('gaea\'s embrace'/'USG', 'The forest rose to the battle, not to save the people but to save itself.').
card_multiverse_id('gaea\'s embrace'/'USG', '5720').

card_in_set('gamble', 'USG').
card_original_type('gamble'/'USG', 'Sorcery').
card_original_text('gamble'/'USG', 'Search your library for a card, put that card into your hand, then discard a card at random. Shuffle your library afterward.').
card_first_print('gamble', 'USG').
card_image_name('gamble'/'USG', 'gamble').
card_uid('gamble'/'USG', 'USG:Gamble:gamble').
card_rarity('gamble'/'USG', 'Rare').
card_artist('gamble'/'USG', 'Andrew Goldhawk').
card_number('gamble'/'USG', '188').
card_flavor_text('gamble'/'USG', 'When you\'ve got nothing, you might as well trade it for something else.').
card_multiverse_id('gamble'/'USG', '10654').

card_in_set('gilded drake', 'USG').
card_original_type('gilded drake'/'USG', 'Summon — Drake').
card_original_text('gilded drake'/'USG', 'Flying\nWhen Gilded Drake comes into play, exchange control of Gilded Drake for target creature one of your opponents controls or sacrifice Gilded Drake.').
card_first_print('gilded drake', 'USG').
card_image_name('gilded drake'/'USG', 'gilded drake').
card_uid('gilded drake'/'USG', 'USG:Gilded Drake:gilded drake').
card_rarity('gilded drake'/'USG', 'Rare').
card_artist('gilded drake'/'USG', 'Bob Eggleton').
card_number('gilded drake'/'USG', '76').
card_flavor_text('gilded drake'/'USG', 'Buyer beware.').
card_multiverse_id('gilded drake'/'USG', '5837').

card_in_set('glorious anthem', 'USG').
card_original_type('glorious anthem'/'USG', 'Enchantment').
card_original_text('glorious anthem'/'USG', 'All creatures you control get +1/+1.').
card_first_print('glorious anthem', 'USG').
card_image_name('glorious anthem'/'USG', 'glorious anthem').
card_uid('glorious anthem'/'USG', 'USG:Glorious Anthem:glorious anthem').
card_rarity('glorious anthem'/'USG', 'Rare').
card_artist('glorious anthem'/'USG', 'Kev Walker').
card_number('glorious anthem'/'USG', '15').
card_flavor_text('glorious anthem'/'USG', 'Once heard, the battle song of an angel becomes part of the listener forever.').
card_multiverse_id('glorious anthem'/'USG', '5835').

card_in_set('goblin cadets', 'USG').
card_original_type('goblin cadets'/'USG', 'Summon — Goblins').
card_original_text('goblin cadets'/'USG', 'Whenever Goblin Cadets blocks or becomes blocked, target opponent gains control of it. (This removes Goblin Cadets from combat.)').
card_first_print('goblin cadets', 'USG').
card_image_name('goblin cadets'/'USG', 'goblin cadets').
card_uid('goblin cadets'/'USG', 'USG:Goblin Cadets:goblin cadets').
card_rarity('goblin cadets'/'USG', 'Uncommon').
card_artist('goblin cadets'/'USG', 'Jerry Tiritilli').
card_number('goblin cadets'/'USG', '189').
card_flavor_text('goblin cadets'/'USG', '\"If you kids don\'t stop that racket, I\'m turning this expedition around right now!\"').
card_multiverse_id('goblin cadets'/'USG', '5541').

card_in_set('goblin lackey', 'USG').
card_original_type('goblin lackey'/'USG', 'Summon — Goblin').
card_original_text('goblin lackey'/'USG', 'Whenever Goblin Lackey successfully deals damage to a player, you may choose a Goblin card in your hand and put that Goblin into play.').
card_first_print('goblin lackey', 'USG').
card_image_name('goblin lackey'/'USG', 'goblin lackey').
card_uid('goblin lackey'/'USG', 'USG:Goblin Lackey:goblin lackey').
card_rarity('goblin lackey'/'USG', 'Uncommon').
card_artist('goblin lackey'/'USG', 'Jerry Tiritilli').
card_number('goblin lackey'/'USG', '190').
card_flavor_text('goblin lackey'/'USG', 'All bark, someone else\'s bite.').
card_multiverse_id('goblin lackey'/'USG', '9851').

card_in_set('goblin matron', 'USG').
card_original_type('goblin matron'/'USG', 'Summon — Goblin').
card_original_text('goblin matron'/'USG', 'When Goblin Matron comes into play, you may search your library for a Goblin card. If you do, reveal that card, put it into your hand, and shuffle your library afterward.').
card_image_name('goblin matron'/'USG', 'goblin matron').
card_uid('goblin matron'/'USG', 'USG:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'USG', 'Common').
card_artist('goblin matron'/'USG', 'DiTerlizzi').
card_number('goblin matron'/'USG', '191').
card_flavor_text('goblin matron'/'USG', 'There\'s always room for one more.').
card_multiverse_id('goblin matron'/'USG', '8824').

card_in_set('goblin offensive', 'USG').
card_original_type('goblin offensive'/'USG', 'Sorcery').
card_original_text('goblin offensive'/'USG', 'Put X Goblin tokens into play. Treat these tokens as 1/1 red creatures.').
card_first_print('goblin offensive', 'USG').
card_image_name('goblin offensive'/'USG', 'goblin offensive').
card_uid('goblin offensive'/'USG', 'USG:Goblin Offensive:goblin offensive').
card_rarity('goblin offensive'/'USG', 'Uncommon').
card_artist('goblin offensive'/'USG', 'Carl Critchlow').
card_number('goblin offensive'/'USG', '192').
card_flavor_text('goblin offensive'/'USG', 'They certainly are.').
card_multiverse_id('goblin offensive'/'USG', '8818').

card_in_set('goblin patrol', 'USG').
card_original_type('goblin patrol'/'USG', 'Summon — Goblins').
card_original_text('goblin patrol'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_first_print('goblin patrol', 'USG').
card_image_name('goblin patrol'/'USG', 'goblin patrol').
card_uid('goblin patrol'/'USG', 'USG:Goblin Patrol:goblin patrol').
card_rarity('goblin patrol'/'USG', 'Common').
card_artist('goblin patrol'/'USG', 'Greg Staples').
card_number('goblin patrol'/'USG', '193').
card_flavor_text('goblin patrol'/'USG', '\"Take the sharp metal stick and make a lotta holes.\"\n—Jula, goblin raider').
card_multiverse_id('goblin patrol'/'USG', '5653').

card_in_set('goblin raider', 'USG').
card_original_type('goblin raider'/'USG', 'Summon — Goblin').
card_original_text('goblin raider'/'USG', 'Goblin Raider cannot block.').
card_image_name('goblin raider'/'USG', 'goblin raider').
card_uid('goblin raider'/'USG', 'USG:Goblin Raider:goblin raider').
card_rarity('goblin raider'/'USG', 'Common').
card_artist('goblin raider'/'USG', 'Greg Staples').
card_number('goblin raider'/'USG', '194').
card_flavor_text('goblin raider'/'USG', 'He was proud to wear the lizard skin around his waist, just for the fun of annoying the enemy.').
card_multiverse_id('goblin raider'/'USG', '5701').

card_in_set('goblin spelunkers', 'USG').
card_original_type('goblin spelunkers'/'USG', 'Summon — Goblins').
card_original_text('goblin spelunkers'/'USG', 'Mountainwalk (If defending player controls a mountain, this creature is unblockable.)').
card_first_print('goblin spelunkers', 'USG').
card_image_name('goblin spelunkers'/'USG', 'goblin spelunkers').
card_uid('goblin spelunkers'/'USG', 'USG:Goblin Spelunkers:goblin spelunkers').
card_rarity('goblin spelunkers'/'USG', 'Common').
card_artist('goblin spelunkers'/'USG', 'DiTerlizzi').
card_number('goblin spelunkers'/'USG', '195').
card_flavor_text('goblin spelunkers'/'USG', '\"It only short jump. You go first.\"\n\"AIIIEEEE!\"\n\"Hmm . . . we go different way now.\"').
card_multiverse_id('goblin spelunkers'/'USG', '5543').

card_in_set('goblin war buggy', 'USG').
card_original_type('goblin war buggy'/'USG', 'Summon — Goblin').
card_original_text('goblin war buggy'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nGoblin War Buggy is unaffected by summoning sickness.').
card_first_print('goblin war buggy', 'USG').
card_image_name('goblin war buggy'/'USG', 'goblin war buggy').
card_uid('goblin war buggy'/'USG', 'USG:Goblin War Buggy:goblin war buggy').
card_rarity('goblin war buggy'/'USG', 'Common').
card_artist('goblin war buggy'/'USG', 'DiTerlizzi').
card_number('goblin war buggy'/'USG', '196').
card_multiverse_id('goblin war buggy'/'USG', '5760').

card_in_set('gorilla warrior', 'USG').
card_original_type('gorilla warrior'/'USG', 'Summon — Ape').
card_original_text('gorilla warrior'/'USG', '').
card_image_name('gorilla warrior'/'USG', 'gorilla warrior').
card_uid('gorilla warrior'/'USG', 'USG:Gorilla Warrior:gorilla warrior').
card_rarity('gorilla warrior'/'USG', 'Common').
card_artist('gorilla warrior'/'USG', 'Steve White').
card_number('gorilla warrior'/'USG', '256').
card_flavor_text('gorilla warrior'/'USG', 'The gorilla beat its chest and threw great handfuls of leaves into the air. It howled challenge and showed its teeth. The mechanical soldier, not understanding, simply killed it.').
card_multiverse_id('gorilla warrior'/'USG', '5683').

card_in_set('grafted skullcap', 'USG').
card_original_type('grafted skullcap'/'USG', 'Artifact').
card_original_text('grafted skullcap'/'USG', 'During your draw phase, draw an additional card.\nAt the end of each of your turns, discard your hand.').
card_first_print('grafted skullcap', 'USG').
card_image_name('grafted skullcap'/'USG', 'grafted skullcap').
card_uid('grafted skullcap'/'USG', 'USG:Grafted Skullcap:grafted skullcap').
card_rarity('grafted skullcap'/'USG', 'Rare').
card_artist('grafted skullcap'/'USG', 'Brian Despain').
card_number('grafted skullcap'/'USG', '296').
card_flavor_text('grafted skullcap'/'USG', '\"Let go your mind. Mine is fitter.\"\n—Gix, Yawgmoth praetor').
card_multiverse_id('grafted skullcap'/'USG', '7169').

card_in_set('great whale', 'USG').
card_original_type('great whale'/'USG', 'Summon — Whale').
card_original_text('great whale'/'USG', 'When Great Whale comes into play, untap up to seven lands.').
card_first_print('great whale', 'USG').
card_image_name('great whale'/'USG', 'great whale').
card_uid('great whale'/'USG', 'USG:Great Whale:great whale').
card_rarity('great whale'/'USG', 'Rare').
card_artist('great whale'/'USG', 'Bob Eggleton').
card_number('great whale'/'USG', '77').
card_flavor_text('great whale'/'USG', '\"As a great whale dies, it flips onto its back. And so an island is born.\"\n—Mariners\' legend').
card_multiverse_id('great whale'/'USG', '10682').

card_in_set('greater good', 'USG').
card_original_type('greater good'/'USG', 'Enchantment').
card_original_text('greater good'/'USG', 'Sacrifice a creature: Draw cards equal to the sacrificed creature\'s power, then choose and discard three cards.').
card_image_name('greater good'/'USG', 'greater good').
card_uid('greater good'/'USG', 'USG:Greater Good:greater good').
card_rarity('greater good'/'USG', 'Rare').
card_artist('greater good'/'USG', 'Pete Venters').
card_number('greater good'/'USG', '257').
card_flavor_text('greater good'/'USG', '\"We have more sprouts than they have hands.\"\n—Gamelen, Citanul elder').
card_multiverse_id('greater good'/'USG', '10467').

card_in_set('greener pastures', 'USG').
card_original_type('greener pastures'/'USG', 'Enchantment').
card_original_text('greener pastures'/'USG', 'During each player\'s upkeep, if that player controls more lands than any other, the player puts a Saproling token into play under his or her control. Treat this token as a 1/1 green creature.').
card_first_print('greener pastures', 'USG').
card_image_name('greener pastures'/'USG', 'greener pastures').
card_uid('greener pastures'/'USG', 'USG:Greener Pastures:greener pastures').
card_rarity('greener pastures'/'USG', 'Rare').
card_artist('greener pastures'/'USG', 'Heather Hudson').
card_number('greener pastures'/'USG', '258').
card_multiverse_id('greener pastures'/'USG', '9704').

card_in_set('guma', 'USG').
card_original_type('guma'/'USG', 'Summon — Cat').
card_original_text('guma'/'USG', 'Protection from blue').
card_first_print('guma', 'USG').
card_image_name('guma'/'USG', 'guma').
card_uid('guma'/'USG', 'USG:Guma:guma').
card_rarity('guma'/'USG', 'Uncommon').
card_artist('guma'/'USG', 'Daren Bader').
card_number('guma'/'USG', '197').
card_flavor_text('guma'/'USG', 'When the giant returned for the night, he found a dead merfolk on his pillow. Although he praised the little guma, he inwardly wondered where she had hid the head.').
card_multiverse_id('guma'/'USG', '8806').

card_in_set('hawkeater moth', 'USG').
card_original_type('hawkeater moth'/'USG', 'Summon — Insect').
card_original_text('hawkeater moth'/'USG', 'Flying\nHawkeater Moth cannot be the target of spells or abilities.').
card_first_print('hawkeater moth', 'USG').
card_image_name('hawkeater moth'/'USG', 'hawkeater moth').
card_uid('hawkeater moth'/'USG', 'USG:Hawkeater Moth:hawkeater moth').
card_rarity('hawkeater moth'/'USG', 'Uncommon').
card_artist('hawkeater moth'/'USG', 'Heather Hudson').
card_number('hawkeater moth'/'USG', '259').
card_flavor_text('hawkeater moth'/'USG', 'Each day at dusk the birds above the canopy grow silent.').
card_multiverse_id('hawkeater moth'/'USG', '9710').

card_in_set('headlong rush', 'USG').
card_original_type('headlong rush'/'USG', 'Instant').
card_original_text('headlong rush'/'USG', 'All attacking creatures gain first strike until end of turn.').
card_first_print('headlong rush', 'USG').
card_image_name('headlong rush'/'USG', 'headlong rush').
card_uid('headlong rush'/'USG', 'USG:Headlong Rush:headlong rush').
card_rarity('headlong rush'/'USG', 'Common').
card_artist('headlong rush'/'USG', 'Dermot Power').
card_number('headlong rush'/'USG', '198').
card_flavor_text('headlong rush'/'USG', 'A landslide of goblins poured toward the defenders—tumbling, rolling, and bouncing their way down the steep hillside.').
card_multiverse_id('headlong rush'/'USG', '8805').

card_in_set('healing salve', 'USG').
card_original_type('healing salve'/'USG', 'Instant').
card_original_text('healing salve'/'USG', 'Choose one Target player gains 3 life; or prevent up to 3 damage to a creature or player.').
card_image_name('healing salve'/'USG', 'healing salve').
card_uid('healing salve'/'USG', 'USG:Healing Salve:healing salve').
card_rarity('healing salve'/'USG', 'Common').
card_artist('healing salve'/'USG', 'Heather Hudson').
card_number('healing salve'/'USG', '16').
card_flavor_text('healing salve'/'USG', '\"Xantcha is recovering. The medicine is slow, but my magic would have killed her.\"\n—Serra, to Urza').
card_multiverse_id('healing salve'/'USG', '8813').

card_in_set('heat ray', 'USG').
card_original_type('heat ray'/'USG', 'Instant').
card_original_text('heat ray'/'USG', 'Heat Ray deals X damage to target creature.').
card_first_print('heat ray', 'USG').
card_image_name('heat ray'/'USG', 'heat ray').
card_uid('heat ray'/'USG', 'USG:Heat Ray:heat ray').
card_rarity('heat ray'/'USG', 'Common').
card_artist('heat ray'/'USG', 'Brian Snõddy').
card_number('heat ray'/'USG', '199').
card_flavor_text('heat ray'/'USG', 'It\'s not known whether the Thran built the device to forge their wonders or to defend them.').
card_multiverse_id('heat ray'/'USG', '10464').

card_in_set('herald of serra', 'USG').
card_original_type('herald of serra'/'USG', 'Summon — Angel').
card_original_text('herald of serra'/'USG', 'Flying; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nAttacking does not cause Herald of Serra to tap.').
card_first_print('herald of serra', 'USG').
card_image_name('herald of serra'/'USG', 'herald of serra').
card_uid('herald of serra'/'USG', 'USG:Herald of Serra:herald of serra').
card_rarity('herald of serra'/'USG', 'Rare').
card_artist('herald of serra'/'USG', 'Matthew D. Wilson').
card_number('herald of serra'/'USG', '17').
card_multiverse_id('herald of serra'/'USG', '5578').

card_in_set('hermetic study', 'USG').
card_original_type('hermetic study'/'USG', 'Enchant Creature').
card_original_text('hermetic study'/'USG', 'Enchanted creature gains \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('hermetic study', 'USG').
card_image_name('hermetic study'/'USG', 'hermetic study').
card_uid('hermetic study'/'USG', 'USG:Hermetic Study:hermetic study').
card_rarity('hermetic study'/'USG', 'Common').
card_artist('hermetic study'/'USG', 'Ron Spencer').
card_number('hermetic study'/'USG', '78').
card_flavor_text('hermetic study'/'USG', '\"Books can be replaced; a prize student cannot. Be patient.\"\n—Urza, to Barrin').
card_multiverse_id('hermetic study'/'USG', '8784').

card_in_set('hibernation', 'USG').
card_original_type('hibernation'/'USG', 'Instant').
card_original_text('hibernation'/'USG', 'Return all green permanents to owners\' hands.').
card_first_print('hibernation', 'USG').
card_image_name('hibernation'/'USG', 'hibernation').
card_uid('hibernation'/'USG', 'USG:Hibernation:hibernation').
card_rarity('hibernation'/'USG', 'Uncommon').
card_artist('hibernation'/'USG', 'Scott Kirschner').
card_number('hibernation'/'USG', '79').
card_flavor_text('hibernation'/'USG', 'On its way to the cave, the armadillo brushed by a sapling. It awoke to find a full-grown tree blocking its path.').
card_multiverse_id('hibernation'/'USG', '5674').

card_in_set('hidden ancients', 'USG').
card_original_type('hidden ancients'/'USG', 'Enchantment').
card_original_text('hidden ancients'/'USG', 'When one of your opponents successfully casts an enchantment spell, if Hidden Ancients is an enchantment, Hidden Ancients becomes a 5/5 creature that counts as a Treefolk.').
card_first_print('hidden ancients', 'USG').
card_image_name('hidden ancients'/'USG', 'hidden ancients').
card_uid('hidden ancients'/'USG', 'USG:Hidden Ancients:hidden ancients').
card_rarity('hidden ancients'/'USG', 'Uncommon').
card_artist('hidden ancients'/'USG', 'Daren Bader').
card_number('hidden ancients'/'USG', '260').
card_flavor_text('hidden ancients'/'USG', 'The only alert the invaders had was the rustling of leaves on a day without wind.').
card_multiverse_id('hidden ancients'/'USG', '5785').

card_in_set('hidden guerrillas', 'USG').
card_original_type('hidden guerrillas'/'USG', 'Enchantment').
card_original_text('hidden guerrillas'/'USG', 'When one of your opponents successfully casts an artifact spell, if Hidden Guerrillas is an enchantment, Hidden Guerrillas becomes a 5/3 creature with trample and that counts as a Soldier.').
card_first_print('hidden guerrillas', 'USG').
card_image_name('hidden guerrillas'/'USG', 'hidden guerrillas').
card_uid('hidden guerrillas'/'USG', 'USG:Hidden Guerrillas:hidden guerrillas').
card_rarity('hidden guerrillas'/'USG', 'Uncommon').
card_artist('hidden guerrillas'/'USG', 'Christopher Moeller').
card_number('hidden guerrillas'/'USG', '261').
card_multiverse_id('hidden guerrillas'/'USG', '5796').

card_in_set('hidden herd', 'USG').
card_original_type('hidden herd'/'USG', 'Enchantment').
card_original_text('hidden herd'/'USG', 'When one of your opponents plays a nonbasic land, if Hidden Herd is an enchantment, Hidden Herd becomes a 3/3 creature that counts as a Beast.').
card_first_print('hidden herd', 'USG').
card_image_name('hidden herd'/'USG', 'hidden herd').
card_uid('hidden herd'/'USG', 'USG:Hidden Herd:hidden herd').
card_rarity('hidden herd'/'USG', 'Rare').
card_artist('hidden herd'/'USG', 'Andrew Robinson').
card_number('hidden herd'/'USG', '262').
card_multiverse_id('hidden herd'/'USG', '5783').

card_in_set('hidden predators', 'USG').
card_original_type('hidden predators'/'USG', 'Enchantment').
card_original_text('hidden predators'/'USG', 'When one of your opponents controls a creature with power 4 or greater, if Hidden Predators is an enchantment, Hidden Predators becomes a 4/4 creature that counts as a Beast.').
card_first_print('hidden predators', 'USG').
card_image_name('hidden predators'/'USG', 'hidden predators').
card_uid('hidden predators'/'USG', 'USG:Hidden Predators:hidden predators').
card_rarity('hidden predators'/'USG', 'Rare').
card_artist('hidden predators'/'USG', 'John Matson').
card_number('hidden predators'/'USG', '263').
card_multiverse_id('hidden predators'/'USG', '5786').

card_in_set('hidden spider', 'USG').
card_original_type('hidden spider'/'USG', 'Enchantment').
card_original_text('hidden spider'/'USG', 'When one of your opponents successfully casts a creature with flying, if Hidden Spider is an enchantment, Hidden Spider becomes a 3/5 creature that can block creatures with flying and that counts as a Spider.').
card_first_print('hidden spider', 'USG').
card_image_name('hidden spider'/'USG', 'hidden spider').
card_uid('hidden spider'/'USG', 'USG:Hidden Spider:hidden spider').
card_rarity('hidden spider'/'USG', 'Common').
card_artist('hidden spider'/'USG', 'Thomas M. Baxa').
card_number('hidden spider'/'USG', '264').
card_flavor_text('hidden spider'/'USG', 'It wants only to dress you in silk.').
card_multiverse_id('hidden spider'/'USG', '5798').

card_in_set('hidden stag', 'USG').
card_original_type('hidden stag'/'USG', 'Enchantment').
card_original_text('hidden stag'/'USG', 'Whenever one of your opponents plays a land, if Hidden Stag is an enchantment, Hidden Stag becomes a 3/2 creature that counts as a Beast.\nWhenever you play a land, if Hidden Stag is a creature, Hidden Stag becomes an enchantment.').
card_first_print('hidden stag', 'USG').
card_image_name('hidden stag'/'USG', 'hidden stag').
card_uid('hidden stag'/'USG', 'USG:Hidden Stag:hidden stag').
card_rarity('hidden stag'/'USG', 'Rare').
card_artist('hidden stag'/'USG', 'Berry').
card_number('hidden stag'/'USG', '265').
card_multiverse_id('hidden stag'/'USG', '5790').

card_in_set('hollow dogs', 'USG').
card_original_type('hollow dogs'/'USG', 'Summon — Hounds').
card_original_text('hollow dogs'/'USG', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_first_print('hollow dogs', 'USG').
card_image_name('hollow dogs'/'USG', 'hollow dogs').
card_uid('hollow dogs'/'USG', 'USG:Hollow Dogs:hollow dogs').
card_rarity('hollow dogs'/'USG', 'Common').
card_artist('hollow dogs'/'USG', 'Jeff Miracola').
card_number('hollow dogs'/'USG', '137').
card_flavor_text('hollow dogs'/'USG', 'A hollow dog is never empty. It is filled with thirst for the hunt.').
card_multiverse_id('hollow dogs'/'USG', '5606').

card_in_set('hopping automaton', 'USG').
card_original_type('hopping automaton'/'USG', 'Artifact Creature').
card_original_text('hopping automaton'/'USG', '{0}: Hopping Automaton gets -1/-1 and gains flying until end of turn.').
card_first_print('hopping automaton', 'USG').
card_image_name('hopping automaton'/'USG', 'hopping automaton').
card_uid('hopping automaton'/'USG', 'USG:Hopping Automaton:hopping automaton').
card_rarity('hopping automaton'/'USG', 'Uncommon').
card_artist('hopping automaton'/'USG', 'Val Mayerik').
card_number('hopping automaton'/'USG', '297').
card_flavor_text('hopping automaton'/'USG', 'Designed to carry equipment across rivers, the hopping automaton was soon pressed into service in the infantry.').
card_multiverse_id('hopping automaton'/'USG', '5700').

card_in_set('horseshoe crab', 'USG').
card_original_type('horseshoe crab'/'USG', 'Summon — Crab').
card_original_text('horseshoe crab'/'USG', '{U} Untap Horseshoe Crab.').
card_first_print('horseshoe crab', 'USG').
card_image_name('horseshoe crab'/'USG', 'horseshoe crab').
card_uid('horseshoe crab'/'USG', 'USG:Horseshoe Crab:horseshoe crab').
card_rarity('horseshoe crab'/'USG', 'Common').
card_artist('horseshoe crab'/'USG', 'Scott Kirschner').
card_number('horseshoe crab'/'USG', '80').
card_flavor_text('horseshoe crab'/'USG', 'In the final days before the disaster, all the crabs on Tolaria migrated from inlets, streams, and ponds back to the sea. No one took note.').
card_multiverse_id('horseshoe crab'/'USG', '9849').

card_in_set('humble', 'USG').
card_original_type('humble'/'USG', 'Instant').
card_original_text('humble'/'USG', 'Target creature loses all abilities and is a 0/1 creature until end of turn.').
card_first_print('humble', 'USG').
card_image_name('humble'/'USG', 'humble').
card_uid('humble'/'USG', 'USG:Humble:humble').
card_rarity('humble'/'USG', 'Uncommon').
card_artist('humble'/'USG', 'Val Mayerik').
card_number('humble'/'USG', '18').
card_flavor_text('humble'/'USG', '\"It is not your place to rule, Radiant. It may not even be mine.\"\n—Serra').
card_multiverse_id('humble'/'USG', '5702').

card_in_set('hush', 'USG').
card_original_type('hush'/'USG', 'Sorcery').
card_original_text('hush'/'USG', 'Destroy all enchantments.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('hush', 'USG').
card_image_name('hush'/'USG', 'hush').
card_uid('hush'/'USG', 'USG:Hush:hush').
card_rarity('hush'/'USG', 'Common').
card_artist('hush'/'USG', 'Rebecca Guay').
card_number('hush'/'USG', '266').
card_multiverse_id('hush'/'USG', '5820').

card_in_set('ill-gotten gains', 'USG').
card_original_type('ill-gotten gains'/'USG', 'Sorcery').
card_original_text('ill-gotten gains'/'USG', 'Remove Ill-Gotten Gains from the game. All players discard their hands, then each player puts up to three cards from his or her graveyard into his or her hand.').
card_first_print('ill-gotten gains', 'USG').
card_image_name('ill-gotten gains'/'USG', 'ill-gotten gains').
card_uid('ill-gotten gains'/'USG', 'USG:Ill-Gotten Gains:ill-gotten gains').
card_rarity('ill-gotten gains'/'USG', 'Rare').
card_artist('ill-gotten gains'/'USG', 'Greg Staples').
card_number('ill-gotten gains'/'USG', '138').
card_flavor_text('ill-gotten gains'/'USG', 'Urza thought it a crusade. Xantcha knew it was a robbery.').
card_multiverse_id('ill-gotten gains'/'USG', '5570').

card_in_set('imaginary pet', 'USG').
card_original_type('imaginary pet'/'USG', 'Summon — Illusion').
card_original_text('imaginary pet'/'USG', 'During your upkeep, if you have a card in hand, return Imaginary Pet to owner\'s hand.').
card_first_print('imaginary pet', 'USG').
card_image_name('imaginary pet'/'USG', 'imaginary pet').
card_uid('imaginary pet'/'USG', 'USG:Imaginary Pet:imaginary pet').
card_rarity('imaginary pet'/'USG', 'Rare').
card_artist('imaginary pet'/'USG', 'Heather Hudson').
card_number('imaginary pet'/'USG', '81').
card_flavor_text('imaginary pet'/'USG', '\"It followed me home. Can I keep it?\"').
card_multiverse_id('imaginary pet'/'USG', '5772').

card_in_set('intrepid hero', 'USG').
card_original_type('intrepid hero'/'USG', 'Summon — Soldier').
card_original_text('intrepid hero'/'USG', '{T}: Destroy target creature with power 4 or greater.').
card_first_print('intrepid hero', 'USG').
card_image_name('intrepid hero'/'USG', 'intrepid hero').
card_uid('intrepid hero'/'USG', 'USG:Intrepid Hero:intrepid hero').
card_rarity('intrepid hero'/'USG', 'Rare').
card_artist('intrepid hero'/'USG', 'Brian Snõddy').
card_number('intrepid hero'/'USG', '19').
card_flavor_text('intrepid hero'/'USG', '\"We each have our own strengths, Radiant,\" Serra said with a sly smile. \"If all of my people were like this one, who would carry your scrolls?\"').
card_multiverse_id('intrepid hero'/'USG', '5678').

card_in_set('island', 'USG').
card_original_type('island'/'USG', 'Land').
card_original_text('island'/'USG', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'USG', 'island1').
card_uid('island'/'USG', 'USG:Island:island1').
card_rarity('island'/'USG', 'Basic Land').
card_artist('island'/'USG', 'Donato Giancola').
card_number('island'/'USG', '335').
card_multiverse_id('island'/'USG', '8326').

card_in_set('island', 'USG').
card_original_type('island'/'USG', 'Land').
card_original_text('island'/'USG', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'USG', 'island2').
card_uid('island'/'USG', 'USG:Island:island2').
card_rarity('island'/'USG', 'Basic Land').
card_artist('island'/'USG', 'Donato Giancola').
card_number('island'/'USG', '336').
card_multiverse_id('island'/'USG', '8327').

card_in_set('island', 'USG').
card_original_type('island'/'USG', 'Land').
card_original_text('island'/'USG', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'USG', 'island3').
card_uid('island'/'USG', 'USG:Island:island3').
card_rarity('island'/'USG', 'Basic Land').
card_artist('island'/'USG', 'Donato Giancola').
card_number('island'/'USG', '337').
card_multiverse_id('island'/'USG', '8328').

card_in_set('island', 'USG').
card_original_type('island'/'USG', 'Land').
card_original_text('island'/'USG', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'USG', 'island4').
card_uid('island'/'USG', 'USG:Island:island4').
card_rarity('island'/'USG', 'Basic Land').
card_artist('island'/'USG', 'Donato Giancola').
card_number('island'/'USG', '338').
card_multiverse_id('island'/'USG', '8329').

card_in_set('jagged lightning', 'USG').
card_original_type('jagged lightning'/'USG', 'Sorcery').
card_original_text('jagged lightning'/'USG', 'Jagged Lightning deals 3 damage to target creature and 3 damage to another target creature.').
card_image_name('jagged lightning'/'USG', 'jagged lightning').
card_uid('jagged lightning'/'USG', 'USG:Jagged Lightning:jagged lightning').
card_rarity('jagged lightning'/'USG', 'Uncommon').
card_artist('jagged lightning'/'USG', 'Mike Raabe').
card_number('jagged lightning'/'USG', '200').
card_flavor_text('jagged lightning'/'USG', 'The pungent smell of roasting flesh made both mages realize they\'d rather break for dinner than fight.').
card_multiverse_id('jagged lightning'/'USG', '5658').

card_in_set('karn, silver golem', 'USG').
card_original_type('karn, silver golem'/'USG', 'Legendary Artifact Creature').
card_original_text('karn, silver golem'/'USG', 'Whenever Karn, Silver Golem blocks or becomes blocked, it gets -4/+4 until end of turn.\n{1}: Target noncreature artifact is an artifact creature with power and toughness each equal to its casting cost until end of turn. (That artifact retains its abilities.)').
card_image_name('karn, silver golem'/'USG', 'karn, silver golem').
card_uid('karn, silver golem'/'USG', 'USG:Karn, Silver Golem:karn, silver golem').
card_rarity('karn, silver golem'/'USG', 'Rare').
card_artist('karn, silver golem'/'USG', 'Mark Zug').
card_number('karn, silver golem'/'USG', '298').
card_multiverse_id('karn, silver golem'/'USG', '9847').

card_in_set('launch', 'USG').
card_original_type('launch'/'USG', 'Enchant Creature').
card_original_text('launch'/'USG', 'Enchanted creature gains flying.\nWhen Launch is put into a graveyard from play, return Launch to owner\'s hand.').
card_first_print('launch', 'USG').
card_image_name('launch'/'USG', 'launch').
card_uid('launch'/'USG', 'USG:Launch:launch').
card_rarity('launch'/'USG', 'Common').
card_artist('launch'/'USG', 'Val Mayerik').
card_number('launch'/'USG', '82').
card_multiverse_id('launch'/'USG', '5832').

card_in_set('lay waste', 'USG').
card_original_type('lay waste'/'USG', 'Sorcery').
card_original_text('lay waste'/'USG', 'Destroy target land.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('lay waste', 'USG').
card_image_name('lay waste'/'USG', 'lay waste').
card_uid('lay waste'/'USG', 'USG:Lay Waste:lay waste').
card_rarity('lay waste'/'USG', 'Common').
card_artist('lay waste'/'USG', 'Terese Nielsen').
card_number('lay waste'/'USG', '201').
card_multiverse_id('lay waste'/'USG', '5817').

card_in_set('lifeline', 'USG').
card_original_type('lifeline'/'USG', 'Artifact').
card_original_text('lifeline'/'USG', 'Whenever a creature is put into a graveyard and a creature is in play, return that creature from your graveyard to play at end of turn.').
card_first_print('lifeline', 'USG').
card_image_name('lifeline'/'USG', 'lifeline').
card_uid('lifeline'/'USG', 'USG:Lifeline:lifeline').
card_rarity('lifeline'/'USG', 'Rare').
card_artist('lifeline'/'USG', 'D. Alexander Gregory').
card_number('lifeline'/'USG', '299').
card_multiverse_id('lifeline'/'USG', '8808').

card_in_set('lightning dragon', 'USG').
card_original_type('lightning dragon'/'USG', 'Summon — Dragon').
card_original_text('lightning dragon'/'USG', 'Flying; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\n{R} Lightning Dragon gets +1/+0 until end of turn.').
card_image_name('lightning dragon'/'USG', 'lightning dragon').
card_uid('lightning dragon'/'USG', 'USG:Lightning Dragon:lightning dragon').
card_rarity('lightning dragon'/'USG', 'Rare').
card_artist('lightning dragon'/'USG', 'Ron Spencer').
card_number('lightning dragon'/'USG', '202').
card_multiverse_id('lightning dragon'/'USG', '5666').

card_in_set('lilting refrain', 'USG').
card_original_type('lilting refrain'/'USG', 'Enchantment').
card_original_text('lilting refrain'/'USG', 'During your upkeep, you may put a verse counter on Lilting Refrain.\nSacrifice Lilting Refrain: Counter target spell unless its caster pays an additional {X}, where X is the number of verse counters on Lilting Refrain. Play this ability as an interrupt.').
card_first_print('lilting refrain', 'USG').
card_image_name('lilting refrain'/'USG', 'lilting refrain').
card_uid('lilting refrain'/'USG', 'USG:Lilting Refrain:lilting refrain').
card_rarity('lilting refrain'/'USG', 'Uncommon').
card_artist('lilting refrain'/'USG', 'Berry').
card_number('lilting refrain'/'USG', '83').
card_multiverse_id('lilting refrain'/'USG', '5615').

card_in_set('lingering mirage', 'USG').
card_original_type('lingering mirage'/'USG', 'Enchant Land').
card_original_text('lingering mirage'/'USG', 'Enchanted land is an island.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('lingering mirage', 'USG').
card_image_name('lingering mirage'/'USG', 'lingering mirage').
card_uid('lingering mirage'/'USG', 'USG:Lingering Mirage:lingering mirage').
card_rarity('lingering mirage'/'USG', 'Uncommon').
card_artist('lingering mirage'/'USG', 'Jerry Tiritilli').
card_number('lingering mirage'/'USG', '84').
card_flavor_text('lingering mirage'/'USG', 'Birds frozen in flight. Sea turned to glass. Tolaria hidden in a mirror.').
card_multiverse_id('lingering mirage'/'USG', '10462').

card_in_set('looming shade', 'USG').
card_original_type('looming shade'/'USG', 'Summon — Shade').
card_original_text('looming shade'/'USG', '{B} Looming Shade gets +1/+1 until end of turn.').
card_first_print('looming shade', 'USG').
card_image_name('looming shade'/'USG', 'looming shade').
card_uid('looming shade'/'USG', 'USG:Looming Shade:looming shade').
card_rarity('looming shade'/'USG', 'Common').
card_artist('looming shade'/'USG', 'Vincent Evans').
card_number('looming shade'/'USG', '139').
card_flavor_text('looming shade'/'USG', 'The shade tracks victims by reverberations of the pipes, as a spider senses prey tangled in its trembling web.').
card_multiverse_id('looming shade'/'USG', '5591').

card_in_set('lotus blossom', 'USG').
card_original_type('lotus blossom'/'USG', 'Artifact').
card_original_text('lotus blossom'/'USG', 'During your upkeep, you may put a petal counter on Lotus Blossom.\n{T}, Sacrifice Lotus Blossom: Add X mana of one color to your mana pool, where X is the number of petal counters on Lotus Blossom. Play this ability as a mana source.').
card_first_print('lotus blossom', 'USG').
card_image_name('lotus blossom'/'USG', 'lotus blossom').
card_uid('lotus blossom'/'USG', 'USG:Lotus Blossom:lotus blossom').
card_rarity('lotus blossom'/'USG', 'Rare').
card_artist('lotus blossom'/'USG', 'Randy Gallegos').
card_number('lotus blossom'/'USG', '300').
card_multiverse_id('lotus blossom'/'USG', '5717').

card_in_set('lull', 'USG').
card_original_type('lull'/'USG', 'Instant').
card_original_text('lull'/'USG', 'Creatures deal no combat damage this turn.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('lull', 'USG').
card_image_name('lull'/'USG', 'lull').
card_uid('lull'/'USG', 'USG:Lull:lull').
card_rarity('lull'/'USG', 'Common').
card_artist('lull'/'USG', 'Terese Nielsen').
card_number('lull'/'USG', '267').
card_multiverse_id('lull'/'USG', '5756').

card_in_set('lurking evil', 'USG').
card_original_type('lurking evil'/'USG', 'Enchantment').
card_original_text('lurking evil'/'USG', 'Pay half your life, rounded up: Lurking Evil becomes a 4/4 creature with flying that counts as a Horror.').
card_first_print('lurking evil', 'USG').
card_image_name('lurking evil'/'USG', 'lurking evil').
card_uid('lurking evil'/'USG', 'USG:Lurking Evil:lurking evil').
card_rarity('lurking evil'/'USG', 'Rare').
card_artist('lurking evil'/'USG', 'Scott Kirschner').
card_number('lurking evil'/'USG', '140').
card_flavor_text('lurking evil'/'USG', '\"Ash is our air, darkness our flesh.\"\n—Phyrexian Scriptures').
card_multiverse_id('lurking evil'/'USG', '8817').

card_in_set('mana leech', 'USG').
card_original_type('mana leech'/'USG', 'Summon — Worm').
card_original_text('mana leech'/'USG', 'You may choose not to untap Mana Leech during your untap phase.\n{T}: Tap target land. As long as Mana Leech remains tapped, that land does not untap during its controller\'s untap phase.').
card_first_print('mana leech', 'USG').
card_image_name('mana leech'/'USG', 'mana leech').
card_uid('mana leech'/'USG', 'USG:Mana Leech:mana leech').
card_rarity('mana leech'/'USG', 'Uncommon').
card_artist('mana leech'/'USG', 'Mark A. Nelson').
card_number('mana leech'/'USG', '141').
card_multiverse_id('mana leech'/'USG', '8804').

card_in_set('meltdown', 'USG').
card_original_type('meltdown'/'USG', 'Sorcery').
card_original_text('meltdown'/'USG', 'Destroy each artifact with total casting cost X or less.').
card_first_print('meltdown', 'USG').
card_image_name('meltdown'/'USG', 'meltdown').
card_uid('meltdown'/'USG', 'USG:Meltdown:meltdown').
card_rarity('meltdown'/'USG', 'Uncommon').
card_artist('meltdown'/'USG', 'Donato Giancola').
card_number('meltdown'/'USG', '203').
card_flavor_text('meltdown'/'USG', 'Catastrophes happened so often at the mana rig that the viashino language had a special word to describe them.').
card_multiverse_id('meltdown'/'USG', '10466').

card_in_set('metrognome', 'USG').
card_original_type('metrognome'/'USG', 'Artifact').
card_original_text('metrognome'/'USG', 'When a spell or ability one of your opponents controls causes you to discard Metrognome, put four Gnome tokens into play. Treat these tokens as 1/1 artifact creatures.\n{4}, {T}: Put a Gnome token into play. Treat this token as a 1/1 artifact creature.').
card_first_print('metrognome', 'USG').
card_image_name('metrognome'/'USG', 'metrognome').
card_uid('metrognome'/'USG', 'USG:Metrognome:metrognome').
card_rarity('metrognome'/'USG', 'Rare').
card_artist('metrognome'/'USG', 'Jeff Laubenstein').
card_number('metrognome'/'USG', '301').
card_multiverse_id('metrognome'/'USG', '8862').

card_in_set('midsummer revel', 'USG').
card_original_type('midsummer revel'/'USG', 'Enchantment').
card_original_text('midsummer revel'/'USG', 'During your upkeep, you may put a verse counter on Midsummer Revel.\n{G}, Sacrifice Midsummer Revel: Put X Beast tokens into play, where X is the number of verse counters on Midsummer Revel. Treat these tokens as 3/3 green creatures.').
card_first_print('midsummer revel', 'USG').
card_image_name('midsummer revel'/'USG', 'midsummer revel').
card_uid('midsummer revel'/'USG', 'USG:Midsummer Revel:midsummer revel').
card_rarity('midsummer revel'/'USG', 'Rare').
card_artist('midsummer revel'/'USG', 'Steve Firchow').
card_number('midsummer revel'/'USG', '268').
card_multiverse_id('midsummer revel'/'USG', '8448').

card_in_set('mishra\'s helix', 'USG').
card_original_type('mishra\'s helix'/'USG', 'Artifact').
card_original_text('mishra\'s helix'/'USG', '{X}, {T}: Tap X lands.').
card_first_print('mishra\'s helix', 'USG').
card_image_name('mishra\'s helix'/'USG', 'mishra\'s helix').
card_uid('mishra\'s helix'/'USG', 'USG:Mishra\'s Helix:mishra\'s helix').
card_rarity('mishra\'s helix'/'USG', 'Rare').
card_artist('mishra\'s helix'/'USG', 'Berry').
card_number('mishra\'s helix'/'USG', '302').
card_flavor_text('mishra\'s helix'/'USG', 'The helix was the finest example of Mishra\'s campaign strategy: if he couldn\'t have Argoth, no one could.').
card_multiverse_id('mishra\'s helix'/'USG', '10650').

card_in_set('mobile fort', 'USG').
card_original_type('mobile fort'/'USG', 'Artifact Creature').
card_original_text('mobile fort'/'USG', 'Mobile Fort counts as a Wall. (Walls cannot attack.)\n{3}: Mobile Fort gets +3/-1 until end of turn and can attack this turn as though it were not a Wall. Play this ability only once each turn.').
card_first_print('mobile fort', 'USG').
card_image_name('mobile fort'/'USG', 'mobile fort').
card_uid('mobile fort'/'USG', 'USG:Mobile Fort:mobile fort').
card_rarity('mobile fort'/'USG', 'Uncommon').
card_artist('mobile fort'/'USG', 'Mark Tedin').
card_number('mobile fort'/'USG', '303').
card_multiverse_id('mobile fort'/'USG', '5550').

card_in_set('monk idealist', 'USG').
card_original_type('monk idealist'/'USG', 'Summon — Cleric').
card_original_text('monk idealist'/'USG', 'When Monk Idealist comes into play, return target enchantment card from your graveyard to your hand.').
card_first_print('monk idealist', 'USG').
card_image_name('monk idealist'/'USG', 'monk idealist').
card_uid('monk idealist'/'USG', 'USG:Monk Idealist:monk idealist').
card_rarity('monk idealist'/'USG', 'Uncommon').
card_artist('monk idealist'/'USG', 'Daren Bader').
card_number('monk idealist'/'USG', '20').
card_flavor_text('monk idealist'/'USG', '\"Belief is the strongest mortar.\"').
card_multiverse_id('monk idealist'/'USG', '5706').

card_in_set('monk realist', 'USG').
card_original_type('monk realist'/'USG', 'Summon — Cleric').
card_original_text('monk realist'/'USG', 'When Monk Realist comes into play, destroy target enchantment.').
card_first_print('monk realist', 'USG').
card_image_name('monk realist'/'USG', 'monk realist').
card_uid('monk realist'/'USG', 'USG:Monk Realist:monk realist').
card_rarity('monk realist'/'USG', 'Common').
card_artist('monk realist'/'USG', 'Daren Bader').
card_number('monk realist'/'USG', '21').
card_flavor_text('monk realist'/'USG', '\"We plant the seeds of doubt to harvest the crop of wisdom.\"').
card_multiverse_id('monk realist'/'USG', '5708').

card_in_set('morphling', 'USG').
card_original_type('morphling'/'USG', 'Summon — Shapeshifter').
card_original_text('morphling'/'USG', '{U} Untap Morphling.\n{U} Morphling gains flying until end of turn.\n{U} Morphling cannot be the target of spells or abilities until end of turn.\n{1}: Morphling gets +1/-1 until end of turn.\n{1}: Morphling gets -1/+1 until end of turn.').
card_image_name('morphling'/'USG', 'morphling').
card_uid('morphling'/'USG', 'USG:Morphling:morphling').
card_rarity('morphling'/'USG', 'Rare').
card_artist('morphling'/'USG', 'rk post').
card_number('morphling'/'USG', '85').
card_multiverse_id('morphling'/'USG', '5863').

card_in_set('mountain', 'USG').
card_original_type('mountain'/'USG', 'Land').
card_original_text('mountain'/'USG', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'USG', 'mountain1').
card_uid('mountain'/'USG', 'USG:Mountain:mountain1').
card_rarity('mountain'/'USG', 'Basic Land').
card_artist('mountain'/'USG', 'John Avon').
card_number('mountain'/'USG', '343').
card_multiverse_id('mountain'/'USG', '8334').

card_in_set('mountain', 'USG').
card_original_type('mountain'/'USG', 'Land').
card_original_text('mountain'/'USG', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'USG', 'mountain2').
card_uid('mountain'/'USG', 'USG:Mountain:mountain2').
card_rarity('mountain'/'USG', 'Basic Land').
card_artist('mountain'/'USG', 'John Avon').
card_number('mountain'/'USG', '344').
card_multiverse_id('mountain'/'USG', '8335').

card_in_set('mountain', 'USG').
card_original_type('mountain'/'USG', 'Land').
card_original_text('mountain'/'USG', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'USG', 'mountain3').
card_uid('mountain'/'USG', 'USG:Mountain:mountain3').
card_rarity('mountain'/'USG', 'Basic Land').
card_artist('mountain'/'USG', 'John Avon').
card_number('mountain'/'USG', '345').
card_multiverse_id('mountain'/'USG', '8336').

card_in_set('mountain', 'USG').
card_original_type('mountain'/'USG', 'Land').
card_original_text('mountain'/'USG', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'USG', 'mountain4').
card_uid('mountain'/'USG', 'USG:Mountain:mountain4').
card_rarity('mountain'/'USG', 'Basic Land').
card_artist('mountain'/'USG', 'John Avon').
card_number('mountain'/'USG', '346').
card_multiverse_id('mountain'/'USG', '8337').

card_in_set('no rest for the wicked', 'USG').
card_original_type('no rest for the wicked'/'USG', 'Enchantment').
card_original_text('no rest for the wicked'/'USG', 'Sacrifice No Rest for the Wicked: Return to your hand all creature cards put into your graveyard from play this turn.').
card_first_print('no rest for the wicked', 'USG').
card_image_name('no rest for the wicked'/'USG', 'no rest for the wicked').
card_uid('no rest for the wicked'/'USG', 'USG:No Rest for the Wicked:no rest for the wicked').
card_rarity('no rest for the wicked'/'USG', 'Uncommon').
card_artist('no rest for the wicked'/'USG', 'Carl Critchlow').
card_number('no rest for the wicked'/'USG', '142').
card_flavor_text('no rest for the wicked'/'USG', '\"The soul? Here, we have no use for such frivolities.\"\n—Sitrik, birth priest').
card_multiverse_id('no rest for the wicked'/'USG', '8802').

card_in_set('noetic scales', 'USG').
card_original_type('noetic scales'/'USG', 'Artifact').
card_original_text('noetic scales'/'USG', 'During each player\'s upkeep, return to owner\'s hand each creature that player controls with power greater than the number of cards in his or her hand.').
card_first_print('noetic scales', 'USG').
card_image_name('noetic scales'/'USG', 'noetic scales').
card_uid('noetic scales'/'USG', 'USG:Noetic Scales:noetic scales').
card_rarity('noetic scales'/'USG', 'Rare').
card_artist('noetic scales'/'USG', 'Andrew Robinson').
card_number('noetic scales'/'USG', '304').
card_multiverse_id('noetic scales'/'USG', '8809').

card_in_set('okk', 'USG').
card_original_type('okk'/'USG', 'Summon — Goblin').
card_original_text('okk'/'USG', 'Okk cannot attack unless a creature with greater power also attacks.\nOkk cannot block unless a creature with greater power also blocks.').
card_first_print('okk', 'USG').
card_image_name('okk'/'USG', 'okk').
card_uid('okk'/'USG', 'USG:Okk:okk').
card_rarity('okk'/'USG', 'Rare').
card_artist('okk'/'USG', 'Mike Raabe').
card_number('okk'/'USG', '204').
card_multiverse_id('okk'/'USG', '5586').

card_in_set('opal acrolith', 'USG').
card_original_type('opal acrolith'/'USG', 'Enchantment').
card_original_text('opal acrolith'/'USG', 'Whenever one of your opponents successfully casts a creature spell, if Opal Acrolith is an enchantment, Opal Acrolith becomes a 2/4 creature that counts as a Guardian.\n{0}: Opal Acrolith becomes an enchantment.').
card_first_print('opal acrolith', 'USG').
card_image_name('opal acrolith'/'USG', 'opal acrolith').
card_uid('opal acrolith'/'USG', 'USG:Opal Acrolith:opal acrolith').
card_rarity('opal acrolith'/'USG', 'Uncommon').
card_artist('opal acrolith'/'USG', 'Robh Ruppel').
card_number('opal acrolith'/'USG', '22').
card_multiverse_id('opal acrolith'/'USG', '5784').

card_in_set('opal archangel', 'USG').
card_original_type('opal archangel'/'USG', 'Enchantment').
card_original_text('opal archangel'/'USG', 'When one of your opponents successfully casts a creature spell, if Opal Archangel is an enchantment, Opal Archangel becomes a 5/5 creature with flying that counts as an Angel. Attacking does not cause Opal Archangel to tap.').
card_first_print('opal archangel', 'USG').
card_image_name('opal archangel'/'USG', 'opal archangel').
card_uid('opal archangel'/'USG', 'USG:Opal Archangel:opal archangel').
card_rarity('opal archangel'/'USG', 'Rare').
card_artist('opal archangel'/'USG', 'Jeff Miracola').
card_number('opal archangel'/'USG', '23').
card_multiverse_id('opal archangel'/'USG', '5787').

card_in_set('opal caryatid', 'USG').
card_original_type('opal caryatid'/'USG', 'Enchantment').
card_original_text('opal caryatid'/'USG', 'When one of your opponents successfully casts a creature spell, if Opal Caryatid is an enchantment, Opal Caryatid becomes a 2/2 creature that counts as a Soldier.').
card_first_print('opal caryatid', 'USG').
card_image_name('opal caryatid'/'USG', 'opal caryatid').
card_uid('opal caryatid'/'USG', 'USG:Opal Caryatid:opal caryatid').
card_rarity('opal caryatid'/'USG', 'Common').
card_artist('opal caryatid'/'USG', 'Berry').
card_number('opal caryatid'/'USG', '24').
card_multiverse_id('opal caryatid'/'USG', '5795').

card_in_set('opal gargoyle', 'USG').
card_original_type('opal gargoyle'/'USG', 'Enchantment').
card_original_text('opal gargoyle'/'USG', 'When one of your opponents successfully casts a creature spell, if Opal Gargoyle is an enchantment, Opal Gargoyle becomes a 2/2 creature with flying that counts as a Gargoyle.').
card_first_print('opal gargoyle', 'USG').
card_image_name('opal gargoyle'/'USG', 'opal gargoyle').
card_uid('opal gargoyle'/'USG', 'USG:Opal Gargoyle:opal gargoyle').
card_rarity('opal gargoyle'/'USG', 'Common').
card_artist('opal gargoyle'/'USG', 'Kev Walker').
card_number('opal gargoyle'/'USG', '25').
card_multiverse_id('opal gargoyle'/'USG', '5791').

card_in_set('opal titan', 'USG').
card_original_type('opal titan'/'USG', 'Enchantment').
card_original_text('opal titan'/'USG', 'When one of your opponents successfully casts a creature spell, if Opal Titan is an enchantment, Opal Titan becomes a 4/4 creature with protection from each of that spell\'s colors and that counts as a Giant.').
card_first_print('opal titan', 'USG').
card_image_name('opal titan'/'USG', 'opal titan').
card_uid('opal titan'/'USG', 'USG:Opal Titan:opal titan').
card_rarity('opal titan'/'USG', 'Rare').
card_artist('opal titan'/'USG', 'Paolo Parente').
card_number('opal titan'/'USG', '26').
card_multiverse_id('opal titan'/'USG', '5782').

card_in_set('oppression', 'USG').
card_original_type('oppression'/'USG', 'Enchantment').
card_original_text('oppression'/'USG', 'Whenever a player successfully casts a spell, that player chooses and discards a card.').
card_first_print('oppression', 'USG').
card_image_name('oppression'/'USG', 'oppression').
card_uid('oppression'/'USG', 'USG:Oppression:oppression').
card_rarity('oppression'/'USG', 'Rare').
card_artist('oppression'/'USG', 'Pete Venters').
card_number('oppression'/'USG', '143').
card_flavor_text('oppression'/'USG', '\"Do not presume to speak for yourself.\"\n—Gix, to Xantcha').
card_multiverse_id('oppression'/'USG', '5746').

card_in_set('order of yawgmoth', 'USG').
card_original_type('order of yawgmoth'/'USG', 'Summon — Knight').
card_original_text('order of yawgmoth'/'USG', 'Order of Yawgmoth cannot be blocked except by artifact creatures and black creatures.\nWhenever Order of Yawgmoth successfully deals damage to a player, that player chooses and discards a card.').
card_first_print('order of yawgmoth', 'USG').
card_image_name('order of yawgmoth'/'USG', 'order of yawgmoth').
card_uid('order of yawgmoth'/'USG', 'USG:Order of Yawgmoth:order of yawgmoth').
card_rarity('order of yawgmoth'/'USG', 'Uncommon').
card_artist('order of yawgmoth'/'USG', 'Chippy').
card_number('order of yawgmoth'/'USG', '144').
card_multiverse_id('order of yawgmoth'/'USG', '5625').

card_in_set('outmaneuver', 'USG').
card_original_type('outmaneuver'/'USG', 'Instant').
card_original_text('outmaneuver'/'USG', 'X target blocked creatures deal combat damage to defending player instead of to blocking creatures this turn.').
card_first_print('outmaneuver', 'USG').
card_image_name('outmaneuver'/'USG', 'outmaneuver').
card_uid('outmaneuver'/'USG', 'USG:Outmaneuver:outmaneuver').
card_rarity('outmaneuver'/'USG', 'Uncommon').
card_artist('outmaneuver'/'USG', 'Greg Staples').
card_number('outmaneuver'/'USG', '205').
card_flavor_text('outmaneuver'/'USG', '\"Push one goblin into sight, an\' run a lot. That\'s tactics.\"\n—Jula, goblin raider').
card_multiverse_id('outmaneuver'/'USG', '8899').

card_in_set('pacifism', 'USG').
card_original_type('pacifism'/'USG', 'Enchant Creature').
card_original_text('pacifism'/'USG', 'Enchanted creature cannot attack or block.').
card_image_name('pacifism'/'USG', 'pacifism').
card_uid('pacifism'/'USG', 'USG:Pacifism:pacifism').
card_rarity('pacifism'/'USG', 'Common').
card_artist('pacifism'/'USG', 'Randy Gallegos').
card_number('pacifism'/'USG', '27').
card_flavor_text('pacifism'/'USG', '\"Fight? I cannot. I do not care if I live or die, so long as I can rest.\"\n—Urza, to Serra').
card_multiverse_id('pacifism'/'USG', '5748').

card_in_set('parasitic bond', 'USG').
card_original_type('parasitic bond'/'USG', 'Enchant Creature').
card_original_text('parasitic bond'/'USG', 'During the upkeep of enchanted creature\'s controller, Parasitic Bond deals 2 damage to that player.').
card_first_print('parasitic bond', 'USG').
card_image_name('parasitic bond'/'USG', 'parasitic bond').
card_uid('parasitic bond'/'USG', 'USG:Parasitic Bond:parasitic bond').
card_rarity('parasitic bond'/'USG', 'Uncommon').
card_artist('parasitic bond'/'USG', 'Scott Kirschner').
card_number('parasitic bond'/'USG', '145').
card_flavor_text('parasitic bond'/'USG', '\"All bonds are parasitic. Only rulership is pure.\"\n—Gix, Yawgmoth praetor').
card_multiverse_id('parasitic bond'/'USG', '8816').

card_in_set('pariah', 'USG').
card_original_type('pariah'/'USG', 'Enchant Creature').
card_original_text('pariah'/'USG', 'Redirect to enchanted creature all damage dealt to you.').
card_first_print('pariah', 'USG').
card_image_name('pariah'/'USG', 'pariah').
card_uid('pariah'/'USG', 'USG:Pariah:pariah').
card_rarity('pariah'/'USG', 'Rare').
card_artist('pariah'/'USG', 'Jon J. Muth').
card_number('pariah'/'USG', '28').
card_flavor_text('pariah'/'USG', '\"It is not sad,\" Radiant chided the lesser angel. \"It is right. Every society must have its outcasts.\"').
card_multiverse_id('pariah'/'USG', '5773').

card_in_set('path of peace', 'USG').
card_original_type('path of peace'/'USG', 'Sorcery').
card_original_text('path of peace'/'USG', 'Destroy target creature. That creature\'s owner gains 4 life.').
card_image_name('path of peace'/'USG', 'path of peace').
card_uid('path of peace'/'USG', 'USG:Path of Peace:path of peace').
card_rarity('path of peace'/'USG', 'Common').
card_artist('path of peace'/'USG', 'Val Mayerik').
card_number('path of peace'/'USG', '29').
card_flavor_text('path of peace'/'USG', '\"When the sword becomes a burden, let the warrior lay it aside that another with a truer heart might take it up.\"\n—Radiant, archangel').
card_multiverse_id('path of peace'/'USG', '5684').

card_in_set('pegasus charger', 'USG').
card_original_type('pegasus charger'/'USG', 'Summon — Pegasus').
card_original_text('pegasus charger'/'USG', 'Flying, first strike').
card_first_print('pegasus charger', 'USG').
card_image_name('pegasus charger'/'USG', 'pegasus charger').
card_uid('pegasus charger'/'USG', 'USG:Pegasus Charger:pegasus charger').
card_rarity('pegasus charger'/'USG', 'Common').
card_artist('pegasus charger'/'USG', 'Val Mayerik').
card_number('pegasus charger'/'USG', '30').
card_flavor_text('pegasus charger'/'USG', '\"The clouds came alive and dove to the earth! Hooves flashed among the dark army, who fled before the spectacle of fury.\"\n—Song of All, canto 211').
card_multiverse_id('pegasus charger'/'USG', '5729').

card_in_set('pendrell drake', 'USG').
card_original_type('pendrell drake'/'USG', 'Summon — Drake').
card_original_text('pendrell drake'/'USG', 'Flying\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('pendrell drake', 'USG').
card_image_name('pendrell drake'/'USG', 'pendrell drake').
card_uid('pendrell drake'/'USG', 'USG:Pendrell Drake:pendrell drake').
card_rarity('pendrell drake'/'USG', 'Common').
card_artist('pendrell drake'/'USG', 'Jeff Miracola').
card_number('pendrell drake'/'USG', '86').
card_flavor_text('pendrell drake'/'USG', 'The mages of Tolaria found strange ways to spend their free time. Occasionally they had contests to see whose kite was eaten last.').
card_multiverse_id('pendrell drake'/'USG', '5575').

card_in_set('pendrell flux', 'USG').
card_original_type('pendrell flux'/'USG', 'Enchant Creature').
card_original_text('pendrell flux'/'USG', 'Enchanted creature gains \"During your upkeep, pay this creature\'s casting cost or sacrifice it.\"').
card_first_print('pendrell flux', 'USG').
card_image_name('pendrell flux'/'USG', 'pendrell flux').
card_uid('pendrell flux'/'USG', 'USG:Pendrell Flux:pendrell flux').
card_rarity('pendrell flux'/'USG', 'Common').
card_artist('pendrell flux'/'USG', 'Andrew Robinson').
card_number('pendrell flux'/'USG', '87').
card_flavor_text('pendrell flux'/'USG', 'Devoured by the mists, Tolaria was stuck in time, trapped between two eternal heartbeats.').
card_multiverse_id('pendrell flux'/'USG', '5614').

card_in_set('peregrine drake', 'USG').
card_original_type('peregrine drake'/'USG', 'Summon — Drake').
card_original_text('peregrine drake'/'USG', 'Flying\nWhen Peregrine Drake comes into play, untap up to five lands.').
card_first_print('peregrine drake', 'USG').
card_image_name('peregrine drake'/'USG', 'peregrine drake').
card_uid('peregrine drake'/'USG', 'USG:Peregrine Drake:peregrine drake').
card_rarity('peregrine drake'/'USG', 'Uncommon').
card_artist('peregrine drake'/'USG', 'Bob Eggleton').
card_number('peregrine drake'/'USG', '88').
card_flavor_text('peregrine drake'/'USG', 'That the Tolarian mists parted for the drakes was warning enough to stay away.').
card_multiverse_id('peregrine drake'/'USG', '5671').

card_in_set('persecute', 'USG').
card_original_type('persecute'/'USG', 'Sorcery').
card_original_text('persecute'/'USG', 'Choose a color. Look at target player\'s hand. That player discards all cards of the chosen color.').
card_first_print('persecute', 'USG').
card_image_name('persecute'/'USG', 'persecute').
card_uid('persecute'/'USG', 'USG:Persecute:persecute').
card_rarity('persecute'/'USG', 'Rare').
card_artist('persecute'/'USG', 'D. Alexander Gregory').
card_number('persecute'/'USG', '146').
card_flavor_text('persecute'/'USG', '\"My finest warrior was lost to the Phyrexians. I pray that Lady Selenia died honorably.\"\n—Radiant, archangel').
card_multiverse_id('persecute'/'USG', '8843').

card_in_set('pestilence', 'USG').
card_original_type('pestilence'/'USG', 'Enchantment').
card_original_text('pestilence'/'USG', 'At the end of each turn, if no creatures are in play, sacrifice Pestilence.\n{B} Pestilence deals 1 damage to each creature and player.').
card_image_name('pestilence'/'USG', 'pestilence').
card_uid('pestilence'/'USG', 'USG:Pestilence:pestilence').
card_rarity('pestilence'/'USG', 'Common').
card_artist('pestilence'/'USG', 'Pete Venters').
card_number('pestilence'/'USG', '147').
card_multiverse_id('pestilence'/'USG', '5619').

card_in_set('phyrexian colossus', 'USG').
card_original_type('phyrexian colossus'/'USG', 'Artifact Creature').
card_original_text('phyrexian colossus'/'USG', 'Phyrexian Colossus does not untap during your untap phase.\nPay 8 life: Untap Phyrexian Colossus.\nPhyrexian Colossus cannot be blocked by fewer than three creatures.').
card_first_print('phyrexian colossus', 'USG').
card_image_name('phyrexian colossus'/'USG', 'phyrexian colossus').
card_uid('phyrexian colossus'/'USG', 'USG:Phyrexian Colossus:phyrexian colossus').
card_rarity('phyrexian colossus'/'USG', 'Rare').
card_artist('phyrexian colossus'/'USG', 'Mark Tedin').
card_number('phyrexian colossus'/'USG', '305').
card_multiverse_id('phyrexian colossus'/'USG', '8865').

card_in_set('phyrexian ghoul', 'USG').
card_original_type('phyrexian ghoul'/'USG', 'Summon — Zombie').
card_original_text('phyrexian ghoul'/'USG', 'Sacrifice a creature: Phyrexian Ghoul gets +2/+2 until end of turn.').
card_first_print('phyrexian ghoul', 'USG').
card_image_name('phyrexian ghoul'/'USG', 'phyrexian ghoul').
card_uid('phyrexian ghoul'/'USG', 'USG:Phyrexian Ghoul:phyrexian ghoul').
card_rarity('phyrexian ghoul'/'USG', 'Common').
card_artist('phyrexian ghoul'/'USG', 'Pete Venters').
card_number('phyrexian ghoul'/'USG', '148').
card_flavor_text('phyrexian ghoul'/'USG', 'Phyrexia wastes nothing. Its food chain is a spiraling cycle.').
card_multiverse_id('phyrexian ghoul'/'USG', '5652').

card_in_set('phyrexian processor', 'USG').
card_original_type('phyrexian processor'/'USG', 'Artifact').
card_original_text('phyrexian processor'/'USG', 'When Phyrexian Processor comes into play, pay any amount of life.\n{4}, {T}: Put a Minion token into play. Treat this token as a black creature with power and toughness each equal to the amount of life paid at the time Phyrexian Processor came into play.').
card_first_print('phyrexian processor', 'USG').
card_image_name('phyrexian processor'/'USG', 'phyrexian processor').
card_uid('phyrexian processor'/'USG', 'USG:Phyrexian Processor:phyrexian processor').
card_rarity('phyrexian processor'/'USG', 'Rare').
card_artist('phyrexian processor'/'USG', 'Ron Spencer').
card_number('phyrexian processor'/'USG', '306').
card_multiverse_id('phyrexian processor'/'USG', '5610').

card_in_set('phyrexian tower', 'USG').
card_original_type('phyrexian tower'/'USG', 'Legendary Land').
card_original_text('phyrexian tower'/'USG', '{T}: Add one colorless mana to your mana pool.\n{T}, Sacrifice a creature: Add {B}{B} to your mana pool.').
card_first_print('phyrexian tower', 'USG').
card_image_name('phyrexian tower'/'USG', 'phyrexian tower').
card_uid('phyrexian tower'/'USG', 'USG:Phyrexian Tower:phyrexian tower').
card_rarity('phyrexian tower'/'USG', 'Rare').
card_artist('phyrexian tower'/'USG', 'Chippy').
card_number('phyrexian tower'/'USG', '322').
card_flavor_text('phyrexian tower'/'USG', 'Living metal encasing dying flesh.').
card_multiverse_id('phyrexian tower'/'USG', '10677').

card_in_set('pit trap', 'USG').
card_original_type('pit trap'/'USG', 'Artifact').
card_original_text('pit trap'/'USG', '{2}, {T}, Sacrifice Pit Trap: Destroy target attacking creature without flying. That creature cannot be regenerated this turn.').
card_image_name('pit trap'/'USG', 'pit trap').
card_uid('pit trap'/'USG', 'USG:Pit Trap:pit trap').
card_rarity('pit trap'/'USG', 'Uncommon').
card_artist('pit trap'/'USG', 'Brian Snõddy').
card_number('pit trap'/'USG', '307').
card_flavor_text('pit trap'/'USG', 'Yotian soldiers were designed to fight, not watch their feet.').
card_multiverse_id('pit trap'/'USG', '8801').

card_in_set('plains', 'USG').
card_original_type('plains'/'USG', 'Land').
card_original_text('plains'/'USG', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'USG', 'plains1').
card_uid('plains'/'USG', 'USG:Plains:plains1').
card_rarity('plains'/'USG', 'Basic Land').
card_artist('plains'/'USG', 'Rob Alexander').
card_number('plains'/'USG', '331').
card_multiverse_id('plains'/'USG', '8322').

card_in_set('plains', 'USG').
card_original_type('plains'/'USG', 'Land').
card_original_text('plains'/'USG', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'USG', 'plains2').
card_uid('plains'/'USG', 'USG:Plains:plains2').
card_rarity('plains'/'USG', 'Basic Land').
card_artist('plains'/'USG', 'Rob Alexander').
card_number('plains'/'USG', '332').
card_multiverse_id('plains'/'USG', '8323').

card_in_set('plains', 'USG').
card_original_type('plains'/'USG', 'Land').
card_original_text('plains'/'USG', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'USG', 'plains3').
card_uid('plains'/'USG', 'USG:Plains:plains3').
card_rarity('plains'/'USG', 'Basic Land').
card_artist('plains'/'USG', 'Rob Alexander').
card_number('plains'/'USG', '333').
card_multiverse_id('plains'/'USG', '8324').

card_in_set('plains', 'USG').
card_original_type('plains'/'USG', 'Land').
card_original_text('plains'/'USG', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'USG', 'plains4').
card_uid('plains'/'USG', 'USG:Plains:plains4').
card_rarity('plains'/'USG', 'Basic Land').
card_artist('plains'/'USG', 'Rob Alexander').
card_number('plains'/'USG', '334').
card_multiverse_id('plains'/'USG', '8325').

card_in_set('planar birth', 'USG').
card_original_type('planar birth'/'USG', 'Sorcery').
card_original_text('planar birth'/'USG', 'Put all basic lands from all graveyards into play under their owners\' control, tapped.').
card_first_print('planar birth', 'USG').
card_image_name('planar birth'/'USG', 'planar birth').
card_uid('planar birth'/'USG', 'USG:Planar Birth:planar birth').
card_rarity('planar birth'/'USG', 'Rare').
card_artist('planar birth'/'USG', 'Adam Rex').
card_number('planar birth'/'USG', '31').
card_flavor_text('planar birth'/'USG', '\"From womb of nothingness sprang this place of beauty, purity, and hope realized.\"\n—Song of All, canto 3').
card_multiverse_id('planar birth'/'USG', '5766').

card_in_set('planar void', 'USG').
card_original_type('planar void'/'USG', 'Enchantment').
card_original_text('planar void'/'USG', 'Whenever a card is put into a graveyard, remove that card from the game.').
card_first_print('planar void', 'USG').
card_image_name('planar void'/'USG', 'planar void').
card_uid('planar void'/'USG', 'USG:Planar Void:planar void').
card_rarity('planar void'/'USG', 'Uncommon').
card_artist('planar void'/'USG', 'Andrew Goldhawk').
card_number('planar void'/'USG', '149').
card_flavor_text('planar void'/'USG', '\"Planeswalking isn\'t about walking. It\'s about falling and screaming.\"\n—Xantcha, Phyrexian outcast').
card_multiverse_id('planar void'/'USG', '5608').

card_in_set('polluted mire', 'USG').
card_original_type('polluted mire'/'USG', 'Land').
card_original_text('polluted mire'/'USG', 'Polluted Mire comes into play tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('polluted mire', 'USG').
card_image_name('polluted mire'/'USG', 'polluted mire').
card_uid('polluted mire'/'USG', 'USG:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'USG', 'Common').
card_artist('polluted mire'/'USG', 'Stephen Daniele').
card_number('polluted mire'/'USG', '323').
card_multiverse_id('polluted mire'/'USG', '5808').

card_in_set('pouncing jaguar', 'USG').
card_original_type('pouncing jaguar'/'USG', 'Summon — Cat').
card_original_text('pouncing jaguar'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_image_name('pouncing jaguar'/'USG', 'pouncing jaguar').
card_uid('pouncing jaguar'/'USG', 'USG:Pouncing Jaguar:pouncing jaguar').
card_rarity('pouncing jaguar'/'USG', 'Common').
card_artist('pouncing jaguar'/'USG', 'Daren Bader').
card_number('pouncing jaguar'/'USG', '269').
card_flavor_text('pouncing jaguar'/'USG', 'One pounce, she\'s hungry—you die quickly. Two, she\'s teaching her cubs—you\'re in for a long day.').
card_multiverse_id('pouncing jaguar'/'USG', '5664').

card_in_set('power sink', 'USG').
card_original_type('power sink'/'USG', 'Interrupt').
card_original_text('power sink'/'USG', 'Counter target spell unless its caster pays an additional {X}. If he or she does not, tap all mana-producing lands that player controls and remove all mana from his or her mana pool.').
card_image_name('power sink'/'USG', 'power sink').
card_uid('power sink'/'USG', 'USG:Power Sink:power sink').
card_rarity('power sink'/'USG', 'Common').
card_artist('power sink'/'USG', 'Andrew Robinson').
card_number('power sink'/'USG', '89').
card_multiverse_id('power sink'/'USG', '5757').

card_in_set('power taint', 'USG').
card_original_type('power taint'/'USG', 'Enchant Enchantment').
card_original_text('power taint'/'USG', 'During the upkeep of enchanted enchantment\'s controller, that player pays {2} or loses 2 life.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('power taint', 'USG').
card_image_name('power taint'/'USG', 'power taint').
card_uid('power taint'/'USG', 'USG:Power Taint:power taint').
card_rarity('power taint'/'USG', 'Common').
card_artist('power taint'/'USG', 'Brian Snõddy').
card_number('power taint'/'USG', '90').
card_multiverse_id('power taint'/'USG', '5662').

card_in_set('presence of the master', 'USG').
card_original_type('presence of the master'/'USG', 'Enchantment').
card_original_text('presence of the master'/'USG', 'Whenever a player plays an enchantment spell, counter it.').
card_image_name('presence of the master'/'USG', 'presence of the master').
card_uid('presence of the master'/'USG', 'USG:Presence of the Master:presence of the master').
card_rarity('presence of the master'/'USG', 'Uncommon').
card_artist('presence of the master'/'USG', 'Ciruelo').
card_number('presence of the master'/'USG', '32').
card_flavor_text('presence of the master'/'USG', 'Peace to all. Peace be all.\n—Great Hall floor inscription').
card_multiverse_id('presence of the master'/'USG', '8811').

card_in_set('priest of gix', 'USG').
card_original_type('priest of gix'/'USG', 'Summon — Minion').
card_original_text('priest of gix'/'USG', 'When Priest of Gix comes into play, add {B}{B}{B} to your mana pool.').
card_first_print('priest of gix', 'USG').
card_image_name('priest of gix'/'USG', 'priest of gix').
card_uid('priest of gix'/'USG', 'USG:Priest of Gix:priest of gix').
card_rarity('priest of gix'/'USG', 'Uncommon').
card_artist('priest of gix'/'USG', 'Brian Despain').
card_number('priest of gix'/'USG', '150').
card_flavor_text('priest of gix'/'USG', '\"Gix doesn\'t want a people to rule but puppets to entertain his madness.\"\n—Xantcha, Phyrexian outcast').
card_multiverse_id('priest of gix'/'USG', '9724').

card_in_set('priest of titania', 'USG').
card_original_type('priest of titania'/'USG', 'Summon — Elf').
card_original_text('priest of titania'/'USG', '{T}: Add {G} to your mana pool for each Elf in play. Play this ability as a mana source.').
card_first_print('priest of titania', 'USG').
card_image_name('priest of titania'/'USG', 'priest of titania').
card_uid('priest of titania'/'USG', 'USG:Priest of Titania:priest of titania').
card_rarity('priest of titania'/'USG', 'Common').
card_artist('priest of titania'/'USG', 'Rebecca Guay').
card_number('priest of titania'/'USG', '270').
card_flavor_text('priest of titania'/'USG', 'Titania rewards all those who honor the forest by making them a living part of it.').
card_multiverse_id('priest of titania'/'USG', '5823').

card_in_set('purging scythe', 'USG').
card_original_type('purging scythe'/'USG', 'Artifact').
card_original_text('purging scythe'/'USG', 'During your upkeep, Purging Scythe deals 2 damage to the creature with the lowest toughness. If two or more creatures are tied for the lowest toughness, you decide to which creature Purging Scythe deals damage.').
card_first_print('purging scythe', 'USG').
card_image_name('purging scythe'/'USG', 'purging scythe').
card_uid('purging scythe'/'USG', 'USG:Purging Scythe:purging scythe').
card_rarity('purging scythe'/'USG', 'Rare').
card_artist('purging scythe'/'USG', 'Matthew D. Wilson').
card_number('purging scythe'/'USG', '308').
card_multiverse_id('purging scythe'/'USG', '10649').

card_in_set('rain of filth', 'USG').
card_original_type('rain of filth'/'USG', 'Instant').
card_original_text('rain of filth'/'USG', 'Each land you control gains \"Sacrifice this land: Add {B} to your mana pool\" until end of turn.').
card_first_print('rain of filth', 'USG').
card_image_name('rain of filth'/'USG', 'rain of filth').
card_uid('rain of filth'/'USG', 'USG:Rain of Filth:rain of filth').
card_rarity('rain of filth'/'USG', 'Uncommon').
card_artist('rain of filth'/'USG', 'Stephen Daniele').
card_number('rain of filth'/'USG', '151').
card_flavor_text('rain of filth'/'USG', '\"When I say it rained, it was not small drops, but a thick, greasy drool pouring from the heavens.\"\n—Urza, journal').
card_multiverse_id('rain of filth'/'USG', '5831').

card_in_set('rain of salt', 'USG').
card_original_type('rain of salt'/'USG', 'Sorcery').
card_original_text('rain of salt'/'USG', 'Destroy two target lands.').
card_image_name('rain of salt'/'USG', 'rain of salt').
card_uid('rain of salt'/'USG', 'USG:Rain of Salt:rain of salt').
card_rarity('rain of salt'/'USG', 'Uncommon').
card_artist('rain of salt'/'USG', 'Adam Rex').
card_number('rain of salt'/'USG', '206').
card_flavor_text('rain of salt'/'USG', 'Here, rain does not wash the land; it desiccates it.').
card_multiverse_id('rain of salt'/'USG', '5761').

card_in_set('ravenous skirge', 'USG').
card_original_type('ravenous skirge'/'USG', 'Summon — Imp').
card_original_text('ravenous skirge'/'USG', 'Flying\nWhenever Ravenous Skirge attacks, it gets +2/+0 until end of turn.').
card_first_print('ravenous skirge', 'USG').
card_image_name('ravenous skirge'/'USG', 'ravenous skirge').
card_uid('ravenous skirge'/'USG', 'USG:Ravenous Skirge:ravenous skirge').
card_rarity('ravenous skirge'/'USG', 'Common').
card_artist('ravenous skirge'/'USG', 'Ron Spencer').
card_number('ravenous skirge'/'USG', '152').
card_flavor_text('ravenous skirge'/'USG', 'Hunger is a kind of madness—and here, all madness flourishes.').
card_multiverse_id('ravenous skirge'/'USG', '9720').

card_in_set('raze', 'USG').
card_original_type('raze'/'USG', 'Sorcery').
card_original_text('raze'/'USG', 'At the time you play Raze, sacrifice a land.\nDestroy target land.').
card_first_print('raze', 'USG').
card_image_name('raze'/'USG', 'raze').
card_uid('raze'/'USG', 'USG:Raze:raze').
card_rarity('raze'/'USG', 'Common').
card_artist('raze'/'USG', 'Mike Raabe').
card_number('raze'/'USG', '207').
card_flavor_text('raze'/'USG', 'The viashino believe that the oldest mountains hate everyone equally.').
card_multiverse_id('raze'/'USG', '5544').

card_in_set('recantation', 'USG').
card_original_type('recantation'/'USG', 'Enchantment').
card_original_text('recantation'/'USG', 'During your upkeep, you may put a verse counter on Recantation.\n{U}, Sacrifice Recantation: Return up to X target permanents to owner\'s hand, where X is the number of verse counters on Recantation.').
card_first_print('recantation', 'USG').
card_image_name('recantation'/'USG', 'recantation').
card_uid('recantation'/'USG', 'USG:Recantation:recantation').
card_rarity('recantation'/'USG', 'Rare').
card_artist('recantation'/'USG', 'Greg Simanson').
card_number('recantation'/'USG', '91').
card_multiverse_id('recantation'/'USG', '8445').

card_in_set('reclusive wight', 'USG').
card_original_type('reclusive wight'/'USG', 'Summon — Minion').
card_original_text('reclusive wight'/'USG', 'During your upkeep, if you control any other nonland permanents, sacrifice Reclusive Wight.').
card_first_print('reclusive wight', 'USG').
card_image_name('reclusive wight'/'USG', 'reclusive wight').
card_uid('reclusive wight'/'USG', 'USG:Reclusive Wight:reclusive wight').
card_rarity('reclusive wight'/'USG', 'Uncommon').
card_artist('reclusive wight'/'USG', 'Vincent Evans').
card_number('reclusive wight'/'USG', '153').
card_flavor_text('reclusive wight'/'USG', 'There are places so horrible that even the dead hide their faces.').
card_multiverse_id('reclusive wight'/'USG', '8803').

card_in_set('redeem', 'USG').
card_original_type('redeem'/'USG', 'Instant').
card_original_text('redeem'/'USG', 'Prevent all damage to one or two creatures. (Treat further damage normally.)').
card_first_print('redeem', 'USG').
card_image_name('redeem'/'USG', 'redeem').
card_uid('redeem'/'USG', 'USG:Redeem:redeem').
card_rarity('redeem'/'USG', 'Uncommon').
card_artist('redeem'/'USG', 'D. Alexander Gregory').
card_number('redeem'/'USG', '33').
card_flavor_text('redeem'/'USG', '\"That they are saved from death is immaterial. What is important is that they know the source of their benefaction.\"\n—Radiant, archangel').
card_multiverse_id('redeem'/'USG', '9716').

card_in_set('reflexes', 'USG').
card_original_type('reflexes'/'USG', 'Enchant Creature').
card_original_text('reflexes'/'USG', 'Enchanted creature gains first strike.').
card_first_print('reflexes', 'USG').
card_image_name('reflexes'/'USG', 'reflexes').
card_uid('reflexes'/'USG', 'USG:Reflexes:reflexes').
card_rarity('reflexes'/'USG', 'Common').
card_artist('reflexes'/'USG', 'Steve White').
card_number('reflexes'/'USG', '208').
card_flavor_text('reflexes'/'USG', '\"Here\'s how ya win. Don\'t let the other guy hit back first.\"\n—Jula, goblin raider').
card_multiverse_id('reflexes'/'USG', '10465').

card_in_set('rejuvenate', 'USG').
card_original_type('rejuvenate'/'USG', 'Sorcery').
card_original_text('rejuvenate'/'USG', 'Gain 6 life.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rejuvenate', 'USG').
card_image_name('rejuvenate'/'USG', 'rejuvenate').
card_uid('rejuvenate'/'USG', 'USG:Rejuvenate:rejuvenate').
card_rarity('rejuvenate'/'USG', 'Common').
card_artist('rejuvenate'/'USG', 'Greg Simanson').
card_number('rejuvenate'/'USG', '271').
card_multiverse_id('rejuvenate'/'USG', '5818').

card_in_set('remembrance', 'USG').
card_original_type('remembrance'/'USG', 'Enchantment').
card_original_text('remembrance'/'USG', 'Whenever a nontoken creature you control is put into a graveyard, you may search your library for a copy of that creature card. If you do, reveal the card, put it into your hand, and shuffle your library afterward.').
card_first_print('remembrance', 'USG').
card_image_name('remembrance'/'USG', 'remembrance').
card_uid('remembrance'/'USG', 'USG:Remembrance:remembrance').
card_rarity('remembrance'/'USG', 'Rare').
card_artist('remembrance'/'USG', 'Val Mayerik').
card_number('remembrance'/'USG', '34').
card_multiverse_id('remembrance'/'USG', '10419').

card_in_set('remote isle', 'USG').
card_original_type('remote isle'/'USG', 'Land').
card_original_text('remote isle'/'USG', 'Remote Isle comes into play tapped.\n{T}: Add {U} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('remote isle', 'USG').
card_image_name('remote isle'/'USG', 'remote isle').
card_uid('remote isle'/'USG', 'USG:Remote Isle:remote isle').
card_rarity('remote isle'/'USG', 'Common').
card_artist('remote isle'/'USG', 'Ciruelo').
card_number('remote isle'/'USG', '324').
card_multiverse_id('remote isle'/'USG', '5807').

card_in_set('reprocess', 'USG').
card_original_type('reprocess'/'USG', 'Sorcery').
card_original_text('reprocess'/'USG', 'Sacrifice any number of artifacts, creatures, and/or lands and draw a card for each one sacrificed this way.').
card_first_print('reprocess', 'USG').
card_image_name('reprocess'/'USG', 'reprocess').
card_uid('reprocess'/'USG', 'USG:Reprocess:reprocess').
card_rarity('reprocess'/'USG', 'Rare').
card_artist('reprocess'/'USG', 'Mark Tedin').
card_number('reprocess'/'USG', '154').
card_flavor_text('reprocess'/'USG', 'Everything will find its use in Phyrexia. Eventually.').
card_multiverse_id('reprocess'/'USG', '5558').

card_in_set('rescind', 'USG').
card_original_type('rescind'/'USG', 'Instant').
card_original_text('rescind'/'USG', 'Return target permanent to owner\'s hand.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rescind', 'USG').
card_image_name('rescind'/'USG', 'rescind').
card_uid('rescind'/'USG', 'USG:Rescind:rescind').
card_rarity('rescind'/'USG', 'Common').
card_artist('rescind'/'USG', 'Adam Rex').
card_number('rescind'/'USG', '92').
card_multiverse_id('rescind'/'USG', '5809').

card_in_set('retaliation', 'USG').
card_original_type('retaliation'/'USG', 'Enchantment').
card_original_text('retaliation'/'USG', 'Each creature you control gains \"Whenever a creature blocks it, this creature gets +1/+1 until end of turn.\"').
card_first_print('retaliation', 'USG').
card_image_name('retaliation'/'USG', 'retaliation').
card_uid('retaliation'/'USG', 'USG:Retaliation:retaliation').
card_rarity('retaliation'/'USG', 'Uncommon').
card_artist('retaliation'/'USG', 'Tom Fleming').
card_number('retaliation'/'USG', '272').
card_flavor_text('retaliation'/'USG', 'A foul, metallic stench clogged Urza\'s senses. It was then he knew his brother was no more.').
card_multiverse_id('retaliation'/'USG', '10408').

card_in_set('retromancer', 'USG').
card_original_type('retromancer'/'USG', 'Summon — Viashino').
card_original_text('retromancer'/'USG', 'Whenever Retromancer is the target of a spell or ability, Retromancer deals 3 damage to that spell or ability\'s controller.').
card_first_print('retromancer', 'USG').
card_image_name('retromancer'/'USG', 'retromancer').
card_uid('retromancer'/'USG', 'USG:Retromancer:retromancer').
card_rarity('retromancer'/'USG', 'Common').
card_artist('retromancer'/'USG', 'Robh Ruppel').
card_number('retromancer'/'USG', '209').
card_flavor_text('retromancer'/'USG', '\"If one harm us, strike them in return. So sayeth the bey.\"').
card_multiverse_id('retromancer'/'USG', '5826').

card_in_set('rewind', 'USG').
card_original_type('rewind'/'USG', 'Interrupt').
card_original_text('rewind'/'USG', 'Counter target spell. Untap up to four lands.').
card_image_name('rewind'/'USG', 'rewind').
card_uid('rewind'/'USG', 'USG:Rewind:rewind').
card_rarity('rewind'/'USG', 'Common').
card_artist('rewind'/'USG', 'Dermot Power').
card_number('rewind'/'USG', '93').
card_flavor_text('rewind'/'USG', '\"Time flows like a river. In Tolaria we practice the art of building dams.\"\n—Barrin, master wizard').
card_multiverse_id('rewind'/'USG', '5670').

card_in_set('rumbling crescendo', 'USG').
card_original_type('rumbling crescendo'/'USG', 'Enchantment').
card_original_text('rumbling crescendo'/'USG', 'During your upkeep, you may put a verse counter on Rumbling Crescendo.\n{R}, Sacrifice Rumbling Crescendo: Destroy up to X target lands, where X is the number of verse counters on Rumbling Crescendo.').
card_first_print('rumbling crescendo', 'USG').
card_image_name('rumbling crescendo'/'USG', 'rumbling crescendo').
card_uid('rumbling crescendo'/'USG', 'USG:Rumbling Crescendo:rumbling crescendo').
card_rarity('rumbling crescendo'/'USG', 'Rare').
card_artist('rumbling crescendo'/'USG', 'Lawrence Snelly').
card_number('rumbling crescendo'/'USG', '210').
card_multiverse_id('rumbling crescendo'/'USG', '8447').

card_in_set('rune of protection: artifacts', 'USG').
card_original_type('rune of protection: artifacts'/'USG', 'Enchantment').
card_original_text('rune of protection: artifacts'/'USG', '{W} Prevent all damage to you from an artifact source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: artifacts', 'USG').
card_image_name('rune of protection: artifacts'/'USG', 'rune of protection artifacts').
card_uid('rune of protection: artifacts'/'USG', 'USG:Rune of Protection: Artifacts:rune of protection artifacts').
card_rarity('rune of protection: artifacts'/'USG', 'Uncommon').
card_artist('rune of protection: artifacts'/'USG', 'Scott M. Fischer').
card_number('rune of protection: artifacts'/'USG', '35').
card_multiverse_id('rune of protection: artifacts'/'USG', '10694').

card_in_set('rune of protection: black', 'USG').
card_original_type('rune of protection: black'/'USG', 'Enchantment').
card_original_text('rune of protection: black'/'USG', '{W} Prevent all damage to you from a black source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: black', 'USG').
card_image_name('rune of protection: black'/'USG', 'rune of protection black').
card_uid('rune of protection: black'/'USG', 'USG:Rune of Protection: Black:rune of protection black').
card_rarity('rune of protection: black'/'USG', 'Common').
card_artist('rune of protection: black'/'USG', 'Scott M. Fischer').
card_number('rune of protection: black'/'USG', '36').
card_multiverse_id('rune of protection: black'/'USG', '5810').

card_in_set('rune of protection: blue', 'USG').
card_original_type('rune of protection: blue'/'USG', 'Enchantment').
card_original_text('rune of protection: blue'/'USG', '{W} Prevent all damage to you from a blue source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: blue', 'USG').
card_image_name('rune of protection: blue'/'USG', 'rune of protection blue').
card_uid('rune of protection: blue'/'USG', 'USG:Rune of Protection: Blue:rune of protection blue').
card_rarity('rune of protection: blue'/'USG', 'Common').
card_artist('rune of protection: blue'/'USG', 'Scott M. Fischer').
card_number('rune of protection: blue'/'USG', '37').
card_multiverse_id('rune of protection: blue'/'USG', '5811').

card_in_set('rune of protection: green', 'USG').
card_original_type('rune of protection: green'/'USG', 'Enchantment').
card_original_text('rune of protection: green'/'USG', '{W} Prevent all damage to you from a green source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: green', 'USG').
card_image_name('rune of protection: green'/'USG', 'rune of protection green').
card_uid('rune of protection: green'/'USG', 'USG:Rune of Protection: Green:rune of protection green').
card_rarity('rune of protection: green'/'USG', 'Common').
card_artist('rune of protection: green'/'USG', 'Scott M. Fischer').
card_number('rune of protection: green'/'USG', '38').
card_multiverse_id('rune of protection: green'/'USG', '5812').

card_in_set('rune of protection: lands', 'USG').
card_original_type('rune of protection: lands'/'USG', 'Enchantment').
card_original_text('rune of protection: lands'/'USG', '{W} Prevent all damage to you from a land source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: lands', 'USG').
card_image_name('rune of protection: lands'/'USG', 'rune of protection lands').
card_uid('rune of protection: lands'/'USG', 'USG:Rune of Protection: Lands:rune of protection lands').
card_rarity('rune of protection: lands'/'USG', 'Rare').
card_artist('rune of protection: lands'/'USG', 'Scott M. Fischer').
card_number('rune of protection: lands'/'USG', '39').
card_multiverse_id('rune of protection: lands'/'USG', '11698').

card_in_set('rune of protection: red', 'USG').
card_original_type('rune of protection: red'/'USG', 'Enchantment').
card_original_text('rune of protection: red'/'USG', '{W} Prevent all damage to you from a red source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: red', 'USG').
card_image_name('rune of protection: red'/'USG', 'rune of protection red').
card_uid('rune of protection: red'/'USG', 'USG:Rune of Protection: Red:rune of protection red').
card_rarity('rune of protection: red'/'USG', 'Common').
card_artist('rune of protection: red'/'USG', 'Scott M. Fischer').
card_number('rune of protection: red'/'USG', '40').
card_multiverse_id('rune of protection: red'/'USG', '5813').

card_in_set('rune of protection: white', 'USG').
card_original_type('rune of protection: white'/'USG', 'Enchantment').
card_original_text('rune of protection: white'/'USG', '{W} Prevent all damage to you from a white source. (Treat further damage from that source normally.)\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('rune of protection: white', 'USG').
card_image_name('rune of protection: white'/'USG', 'rune of protection white').
card_uid('rune of protection: white'/'USG', 'USG:Rune of Protection: White:rune of protection white').
card_rarity('rune of protection: white'/'USG', 'Common').
card_artist('rune of protection: white'/'USG', 'Scott M. Fischer').
card_number('rune of protection: white'/'USG', '41').
card_multiverse_id('rune of protection: white'/'USG', '5814').

card_in_set('sanctum custodian', 'USG').
card_original_type('sanctum custodian'/'USG', 'Summon — Cleric').
card_original_text('sanctum custodian'/'USG', '{T}: Prevent up to 2 damage to a creature or player.').
card_first_print('sanctum custodian', 'USG').
card_image_name('sanctum custodian'/'USG', 'sanctum custodian').
card_uid('sanctum custodian'/'USG', 'USG:Sanctum Custodian:sanctum custodian').
card_rarity('sanctum custodian'/'USG', 'Common').
card_artist('sanctum custodian'/'USG', 'Paolo Parente').
card_number('sanctum custodian'/'USG', '42').
card_flavor_text('sanctum custodian'/'USG', 'Serra told them to guard Urza as he healed. Five years they stood.').
card_multiverse_id('sanctum custodian'/'USG', '5695').

card_in_set('sanctum guardian', 'USG').
card_original_type('sanctum guardian'/'USG', 'Summon — Soldier').
card_original_text('sanctum guardian'/'USG', 'Sacrifice Sanctum Guardian: Prevent all damage to a creature or player from one source. (Treat further damage from that source normally.)').
card_first_print('sanctum guardian', 'USG').
card_image_name('sanctum guardian'/'USG', 'sanctum guardian').
card_uid('sanctum guardian'/'USG', 'USG:Sanctum Guardian:sanctum guardian').
card_rarity('sanctum guardian'/'USG', 'Uncommon').
card_artist('sanctum guardian'/'USG', 'Donato Giancola').
card_number('sanctum guardian'/'USG', '43').
card_flavor_text('sanctum guardian'/'USG', '\"Protect our mother in her womb.\"').
card_multiverse_id('sanctum guardian'/'USG', '5691').

card_in_set('sandbar merfolk', 'USG').
card_original_type('sandbar merfolk'/'USG', 'Summon — Merfolk').
card_original_text('sandbar merfolk'/'USG', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('sandbar merfolk', 'USG').
card_image_name('sandbar merfolk'/'USG', 'sandbar merfolk').
card_uid('sandbar merfolk'/'USG', 'USG:Sandbar Merfolk:sandbar merfolk').
card_rarity('sandbar merfolk'/'USG', 'Common').
card_artist('sandbar merfolk'/'USG', 'rk post').
card_number('sandbar merfolk'/'USG', '94').
card_flavor_text('sandbar merfolk'/'USG', 'You are not prey until a predator knows of your existence.').
card_multiverse_id('sandbar merfolk'/'USG', '5815').

card_in_set('sandbar serpent', 'USG').
card_original_type('sandbar serpent'/'USG', 'Summon — Serpent').
card_original_text('sandbar serpent'/'USG', 'Cycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('sandbar serpent', 'USG').
card_image_name('sandbar serpent'/'USG', 'sandbar serpent').
card_uid('sandbar serpent'/'USG', 'USG:Sandbar Serpent:sandbar serpent').
card_rarity('sandbar serpent'/'USG', 'Uncommon').
card_artist('sandbar serpent'/'USG', 'Jim Nelson').
card_number('sandbar serpent'/'USG', '95').
card_flavor_text('sandbar serpent'/'USG', 'Treacherous and unpredictable currents around Tolaria earned the nickname \"serpent wakes.\"').
card_multiverse_id('sandbar serpent'/'USG', '9722').

card_in_set('sanguine guard', 'USG').
card_original_type('sanguine guard'/'USG', 'Summon — Knight').
card_original_text('sanguine guard'/'USG', 'First strike\n{1}{B} Regenerate Sanguine Guard.').
card_first_print('sanguine guard', 'USG').
card_image_name('sanguine guard'/'USG', 'sanguine guard').
card_uid('sanguine guard'/'USG', 'USG:Sanguine Guard:sanguine guard').
card_rarity('sanguine guard'/'USG', 'Uncommon').
card_artist('sanguine guard'/'USG', 'Kev Walker').
card_number('sanguine guard'/'USG', '155').
card_flavor_text('sanguine guard'/'USG', '\"Father of Machines! Your filigree gaze carves us, and the scars dance upon our grateful flesh.\"\n—Phyrexian Scriptures').
card_multiverse_id('sanguine guard'/'USG', '5627').

card_in_set('scald', 'USG').
card_original_type('scald'/'USG', 'Enchantment').
card_original_text('scald'/'USG', 'Whenever a player taps an island for mana, Scald deals 1 damage to that player.').
card_first_print('scald', 'USG').
card_image_name('scald'/'USG', 'scald').
card_uid('scald'/'USG', 'USG:Scald:scald').
card_rarity('scald'/'USG', 'Uncommon').
card_artist('scald'/'USG', 'Adam Rex').
card_number('scald'/'USG', '211').
card_flavor_text('scald'/'USG', '\"Shiv may be surrounded by water, but the mountains go far deeper.\"\n—Fire Eye, viashino bey').
card_multiverse_id('scald'/'USG', '10657').

card_in_set('scoria wurm', 'USG').
card_original_type('scoria wurm'/'USG', 'Summon — Wurm').
card_original_text('scoria wurm'/'USG', 'During your upkeep, flip a coin. If you lose the flip, return Scoria Wurm to owner\'s hand.').
card_first_print('scoria wurm', 'USG').
card_image_name('scoria wurm'/'USG', 'scoria wurm').
card_uid('scoria wurm'/'USG', 'USG:Scoria Wurm:scoria wurm').
card_rarity('scoria wurm'/'USG', 'Rare').
card_artist('scoria wurm'/'USG', 'Steve Firchow').
card_number('scoria wurm'/'USG', '212').
card_flavor_text('scoria wurm'/'USG', 'Late at night, ululations echo from deep under Shiv, as the wurms sing of times older than humanity.').
card_multiverse_id('scoria wurm'/'USG', '5569').

card_in_set('scrap', 'USG').
card_original_type('scrap'/'USG', 'Instant').
card_original_text('scrap'/'USG', 'Destroy target artifact.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('scrap', 'USG').
card_image_name('scrap'/'USG', 'scrap').
card_uid('scrap'/'USG', 'USG:Scrap:scrap').
card_rarity('scrap'/'USG', 'Common').
card_artist('scrap'/'USG', 'Donato Giancola').
card_number('scrap'/'USG', '213').
card_multiverse_id('scrap'/'USG', '5816').

card_in_set('seasoned marshal', 'USG').
card_original_type('seasoned marshal'/'USG', 'Summon — Soldier').
card_original_text('seasoned marshal'/'USG', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'USG', 'seasoned marshal').
card_uid('seasoned marshal'/'USG', 'USG:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'USG', 'Uncommon').
card_artist('seasoned marshal'/'USG', 'Matthew D. Wilson').
card_number('seasoned marshal'/'USG', '44').
card_flavor_text('seasoned marshal'/'USG', 'There are only two rules of tactics: never be without a plan, and never rely on it.').
card_multiverse_id('seasoned marshal'/'USG', '5776').

card_in_set('serra avatar', 'USG').
card_original_type('serra avatar'/'USG', 'Summon — Avatar').
card_original_text('serra avatar'/'USG', 'Serra Avatar has power and toughness each equal to your life total.\nWhen Serra Avatar is put into a graveyard, shuffle Serra Avatar into owner\'s library.').
card_image_name('serra avatar'/'USG', 'serra avatar').
card_uid('serra avatar'/'USG', 'USG:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'USG', 'Rare').
card_artist('serra avatar'/'USG', 'Dermot Power').
card_number('serra avatar'/'USG', '45').
card_multiverse_id('serra avatar'/'USG', '5713').

card_in_set('serra zealot', 'USG').
card_original_type('serra zealot'/'USG', 'Summon — Soldier').
card_original_text('serra zealot'/'USG', 'First strike').
card_first_print('serra zealot', 'USG').
card_image_name('serra zealot'/'USG', 'serra zealot').
card_uid('serra zealot'/'USG', 'USG:Serra Zealot:serra zealot').
card_rarity('serra zealot'/'USG', 'Common').
card_artist('serra zealot'/'USG', 'DiTerlizzi').
card_number('serra zealot'/'USG', '46').
card_flavor_text('serra zealot'/'USG', '\"The humans are useful in their way, but they must be commanded as the builder commands the stone. Be soft with them, and they will become soft.\"\n—Radiant, archangel').
card_multiverse_id('serra zealot'/'USG', '5707').

card_in_set('serra\'s embrace', 'USG').
card_original_type('serra\'s embrace'/'USG', 'Enchant Creature').
card_original_text('serra\'s embrace'/'USG', 'Enchanted creature gets +2/+2 and gains flying. Attacking does not cause enchanted creature to tap.').
card_first_print('serra\'s embrace', 'USG').
card_image_name('serra\'s embrace'/'USG', 'serra\'s embrace').
card_uid('serra\'s embrace'/'USG', 'USG:Serra\'s Embrace:serra\'s embrace').
card_rarity('serra\'s embrace'/'USG', 'Uncommon').
card_artist('serra\'s embrace'/'USG', 'Terese Nielsen').
card_number('serra\'s embrace'/'USG', '47').
card_flavor_text('serra\'s embrace'/'USG', '\"Lifted beyond herself, for that battle Brindri was an angel of light and fury.\"\n—Song of All, canto 524').
card_multiverse_id('serra\'s embrace'/'USG', '9682').

card_in_set('serra\'s hymn', 'USG').
card_original_type('serra\'s hymn'/'USG', 'Enchantment').
card_original_text('serra\'s hymn'/'USG', 'During your upkeep, you may put a verse counter on Serra\'s Hymn.\nSacrifice Serra\'s Hymn: Prevent up to X damage total to any number of creatures and/or players, where X is the number of verse counters on Serra\'s Hymn.').
card_first_print('serra\'s hymn', 'USG').
card_image_name('serra\'s hymn'/'USG', 'serra\'s hymn').
card_uid('serra\'s hymn'/'USG', 'USG:Serra\'s Hymn:serra\'s hymn').
card_rarity('serra\'s hymn'/'USG', 'Uncommon').
card_artist('serra\'s hymn'/'USG', 'Rebecca Guay').
card_number('serra\'s hymn'/'USG', '48').
card_multiverse_id('serra\'s hymn'/'USG', '5602').

card_in_set('serra\'s liturgy', 'USG').
card_original_type('serra\'s liturgy'/'USG', 'Enchantment').
card_original_text('serra\'s liturgy'/'USG', 'During your upkeep, you may put a verse counter on Serra\'s Liturgy.\n{W}, Sacrifice Serra\'s Liturgy: Destroy up to X target artifacts and/or enchantments, where X is the number of verse counters on Serra\'s Liturgy.').
card_first_print('serra\'s liturgy', 'USG').
card_image_name('serra\'s liturgy'/'USG', 'serra\'s liturgy').
card_uid('serra\'s liturgy'/'USG', 'USG:Serra\'s Liturgy:serra\'s liturgy').
card_rarity('serra\'s liturgy'/'USG', 'Rare').
card_artist('serra\'s liturgy'/'USG', 'rk post').
card_number('serra\'s liturgy'/'USG', '49').
card_multiverse_id('serra\'s liturgy'/'USG', '8444').

card_in_set('serra\'s sanctum', 'USG').
card_original_type('serra\'s sanctum'/'USG', 'Legendary Land').
card_original_text('serra\'s sanctum'/'USG', '{T}: Add {W} to your mana pool for each enchantment you control.').
card_first_print('serra\'s sanctum', 'USG').
card_image_name('serra\'s sanctum'/'USG', 'serra\'s sanctum').
card_uid('serra\'s sanctum'/'USG', 'USG:Serra\'s Sanctum:serra\'s sanctum').
card_rarity('serra\'s sanctum'/'USG', 'Rare').
card_artist('serra\'s sanctum'/'USG', 'Ciruelo').
card_number('serra\'s sanctum'/'USG', '325').
card_flavor_text('serra\'s sanctum'/'USG', 'A fragile cocoon of dreaming will.').
card_multiverse_id('serra\'s sanctum'/'USG', '9674').

card_in_set('shimmering barrier', 'USG').
card_original_type('shimmering barrier'/'USG', 'Summon — Wall').
card_original_text('shimmering barrier'/'USG', '(Walls cannot attack.)\nFirst strike\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('shimmering barrier', 'USG').
card_image_name('shimmering barrier'/'USG', 'shimmering barrier').
card_uid('shimmering barrier'/'USG', 'USG:Shimmering Barrier:shimmering barrier').
card_rarity('shimmering barrier'/'USG', 'Uncommon').
card_artist('shimmering barrier'/'USG', 'D. Alexander Gregory').
card_number('shimmering barrier'/'USG', '50').
card_multiverse_id('shimmering barrier'/'USG', '5743').

card_in_set('shiv\'s embrace', 'USG').
card_original_type('shiv\'s embrace'/'USG', 'Enchant Creature').
card_original_text('shiv\'s embrace'/'USG', 'Enchanted creature gets +2/+2 and gains flying.\n{R} Enchanted creature gets +1/+0 until end of turn.').
card_first_print('shiv\'s embrace', 'USG').
card_image_name('shiv\'s embrace'/'USG', 'shiv\'s embrace').
card_uid('shiv\'s embrace'/'USG', 'USG:Shiv\'s Embrace:shiv\'s embrace').
card_rarity('shiv\'s embrace'/'USG', 'Uncommon').
card_artist('shiv\'s embrace'/'USG', 'Bob Eggleton').
card_number('shiv\'s embrace'/'USG', '214').
card_flavor_text('shiv\'s embrace'/'USG', '\"Wear the foe\'s form to best it in battle. So sayeth the bey.\"').
card_multiverse_id('shiv\'s embrace'/'USG', '5721').

card_in_set('shivan gorge', 'USG').
card_original_type('shivan gorge'/'USG', 'Legendary Land').
card_original_text('shivan gorge'/'USG', '{T}: Add one colorless mana to your mana pool.\n{2}{R}, {T}: Shivan Gorge deals 1 damage to each of your opponents.').
card_first_print('shivan gorge', 'USG').
card_image_name('shivan gorge'/'USG', 'shivan gorge').
card_uid('shivan gorge'/'USG', 'USG:Shivan Gorge:shivan gorge').
card_rarity('shivan gorge'/'USG', 'Rare').
card_artist('shivan gorge'/'USG', 'John Matson').
card_number('shivan gorge'/'USG', '326').
card_flavor_text('shivan gorge'/'USG', 'Both forge and pyre.').
card_multiverse_id('shivan gorge'/'USG', '10482').

card_in_set('shivan hellkite', 'USG').
card_original_type('shivan hellkite'/'USG', 'Summon — Dragon').
card_original_text('shivan hellkite'/'USG', 'Flying\n{1}{R} Shivan Hellkite deals 1 damage to target creature or player.').
card_first_print('shivan hellkite', 'USG').
card_image_name('shivan hellkite'/'USG', 'shivan hellkite').
card_uid('shivan hellkite'/'USG', 'USG:Shivan Hellkite:shivan hellkite').
card_rarity('shivan hellkite'/'USG', 'Rare').
card_artist('shivan hellkite'/'USG', 'Bob Eggleton').
card_number('shivan hellkite'/'USG', '215').
card_flavor_text('shivan hellkite'/'USG', 'A dragon\'s scale can be carved into a mighty shield, provided you can procure a dragontooth to cut it.').
card_multiverse_id('shivan hellkite'/'USG', '5660').

card_in_set('shivan raptor', 'USG').
card_original_type('shivan raptor'/'USG', 'Summon — Lizard').
card_original_text('shivan raptor'/'USG', 'First strike; echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nShivan Raptor is unaffected by summoning sickness.').
card_first_print('shivan raptor', 'USG').
card_image_name('shivan raptor'/'USG', 'shivan raptor').
card_uid('shivan raptor'/'USG', 'USG:Shivan Raptor:shivan raptor').
card_rarity('shivan raptor'/'USG', 'Uncommon').
card_artist('shivan raptor'/'USG', 'Bob Eggleton').
card_number('shivan raptor'/'USG', '216').
card_multiverse_id('shivan raptor'/'USG', '5562').

card_in_set('show and tell', 'USG').
card_original_type('show and tell'/'USG', 'Sorcery').
card_original_text('show and tell'/'USG', 'Each player may choose an artifact, creature, enchantment, or land card in his or her hand and put that permanent into play.').
card_image_name('show and tell'/'USG', 'show and tell').
card_uid('show and tell'/'USG', 'USG:Show and Tell:show and tell').
card_rarity('show and tell'/'USG', 'Rare').
card_artist('show and tell'/'USG', 'Jeff Laubenstein').
card_number('show and tell'/'USG', '96').
card_flavor_text('show and tell'/'USG', 'At the academy, \"show and tell\" too often becomes \"run and hide.\"').
card_multiverse_id('show and tell'/'USG', '5697').

card_in_set('shower of sparks', 'USG').
card_original_type('shower of sparks'/'USG', 'Instant').
card_original_text('shower of sparks'/'USG', 'Shower of Sparks deals 1 damage to target creature and 1 damage to target player.').
card_first_print('shower of sparks', 'USG').
card_image_name('shower of sparks'/'USG', 'shower of sparks').
card_uid('shower of sparks'/'USG', 'USG:Shower of Sparks:shower of sparks').
card_rarity('shower of sparks'/'USG', 'Common').
card_artist('shower of sparks'/'USG', 'Christopher Moeller').
card_number('shower of sparks'/'USG', '217').
card_flavor_text('shower of sparks'/'USG', 'The viashino had learned how to operate the rig through trial and error—mostly error.').
card_multiverse_id('shower of sparks'/'USG', '5545').

card_in_set('sicken', 'USG').
card_original_type('sicken'/'USG', 'Enchant Creature').
card_original_text('sicken'/'USG', 'Enchanted creature gets -1/-1.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('sicken', 'USG').
card_image_name('sicken'/'USG', 'sicken').
card_uid('sicken'/'USG', 'USG:Sicken:sicken').
card_rarity('sicken'/'USG', 'Common').
card_artist('sicken'/'USG', 'Heather Hudson').
card_number('sicken'/'USG', '156').
card_flavor_text('sicken'/'USG', 'Urza dared to attack Phyrexia. Slowly, it retaliated.').
card_multiverse_id('sicken'/'USG', '5553').

card_in_set('silent attendant', 'USG').
card_original_type('silent attendant'/'USG', 'Summon — Cleric').
card_original_text('silent attendant'/'USG', '{T}: Gain 1 life.').
card_first_print('silent attendant', 'USG').
card_image_name('silent attendant'/'USG', 'silent attendant').
card_uid('silent attendant'/'USG', 'USG:Silent Attendant:silent attendant').
card_rarity('silent attendant'/'USG', 'Common').
card_artist('silent attendant'/'USG', 'Rebecca Guay').
card_number('silent attendant'/'USG', '51').
card_flavor_text('silent attendant'/'USG', '\"The answer to life should never be death; it should always be more life, wrapped tight around us like precious silks.\"').
card_multiverse_id('silent attendant'/'USG', '5651').

card_in_set('skirge familiar', 'USG').
card_original_type('skirge familiar'/'USG', 'Summon — Imp').
card_original_text('skirge familiar'/'USG', 'Flying\nChoose and discard a card: Add {B} to your mana pool. Play this ability as a mana source.').
card_first_print('skirge familiar', 'USG').
card_image_name('skirge familiar'/'USG', 'skirge familiar').
card_uid('skirge familiar'/'USG', 'USG:Skirge Familiar:skirge familiar').
card_rarity('skirge familiar'/'USG', 'Uncommon').
card_artist('skirge familiar'/'USG', 'Ron Spencer').
card_number('skirge familiar'/'USG', '157').
card_flavor_text('skirge familiar'/'USG', 'The first Yawgmoth priest to harness their power was rewarded with several unique mutilations.').
card_multiverse_id('skirge familiar'/'USG', '5851').

card_in_set('skittering skirge', 'USG').
card_original_type('skittering skirge'/'USG', 'Summon — Imp').
card_original_text('skittering skirge'/'USG', 'Flying\nWhen you successfully cast a creature spell, sacrifice Skittering Skirge.').
card_image_name('skittering skirge'/'USG', 'skittering skirge').
card_uid('skittering skirge'/'USG', 'USG:Skittering Skirge:skittering skirge').
card_rarity('skittering skirge'/'USG', 'Common').
card_artist('skittering skirge'/'USG', 'Ron Spencer').
card_number('skittering skirge'/'USG', '158').
card_flavor_text('skittering skirge'/'USG', 'The imps\' warbling cries echo through Phyrexia\'s towers like those of mourning doves in a cathedral.').
card_multiverse_id('skittering skirge'/'USG', '5573').

card_in_set('sleeper agent', 'USG').
card_original_type('sleeper agent'/'USG', 'Summon — Minion').
card_original_text('sleeper agent'/'USG', 'When Sleeper Agent comes into play, target opponent gains control of it.\nDuring your upkeep, Sleeper Agent deals 2 damage to you.').
card_first_print('sleeper agent', 'USG').
card_image_name('sleeper agent'/'USG', 'sleeper agent').
card_uid('sleeper agent'/'USG', 'USG:Sleeper Agent:sleeper agent').
card_rarity('sleeper agent'/'USG', 'Rare').
card_artist('sleeper agent'/'USG', 'Randy Gallegos').
card_number('sleeper agent'/'USG', '159').
card_multiverse_id('sleeper agent'/'USG', '5751').

card_in_set('slippery karst', 'USG').
card_original_type('slippery karst'/'USG', 'Land').
card_original_text('slippery karst'/'USG', 'Slippery Karst comes into play tapped.\n{T}: Add {G} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('slippery karst', 'USG').
card_image_name('slippery karst'/'USG', 'slippery karst').
card_uid('slippery karst'/'USG', 'USG:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'USG', 'Common').
card_artist('slippery karst'/'USG', 'Stephen Daniele').
card_number('slippery karst'/'USG', '327').
card_multiverse_id('slippery karst'/'USG', '5803').

card_in_set('smokestack', 'USG').
card_original_type('smokestack'/'USG', 'Artifact').
card_original_text('smokestack'/'USG', 'During your upkeep, you may put a soot counter on Smokestack.\nDuring each player\'s upkeep, that player sacrifices a permanent for each soot counter on Smokestack.').
card_first_print('smokestack', 'USG').
card_image_name('smokestack'/'USG', 'smokestack').
card_uid('smokestack'/'USG', 'USG:Smokestack:smokestack').
card_rarity('smokestack'/'USG', 'Rare').
card_artist('smokestack'/'USG', 'Scott Kirschner').
card_number('smokestack'/'USG', '309').
card_multiverse_id('smokestack'/'USG', '5730').

card_in_set('smoldering crater', 'USG').
card_original_type('smoldering crater'/'USG', 'Land').
card_original_text('smoldering crater'/'USG', 'Smoldering Crater comes into play tapped.\n{T}: Add {R} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('smoldering crater', 'USG').
card_image_name('smoldering crater'/'USG', 'smoldering crater').
card_uid('smoldering crater'/'USG', 'USG:Smoldering Crater:smoldering crater').
card_rarity('smoldering crater'/'USG', 'Common').
card_artist('smoldering crater'/'USG', 'Mark Tedin').
card_number('smoldering crater'/'USG', '328').
card_multiverse_id('smoldering crater'/'USG', '5804').

card_in_set('sneak attack', 'USG').
card_original_type('sneak attack'/'USG', 'Enchantment').
card_original_text('sneak attack'/'USG', '{R} Choose a creature card from your hand and put that creature into play. The creature is unaffected by summoning sickness. At end of turn, sacrifice the creature.').
card_image_name('sneak attack'/'USG', 'sneak attack').
card_uid('sneak attack'/'USG', 'USG:Sneak Attack:sneak attack').
card_rarity('sneak attack'/'USG', 'Rare').
card_artist('sneak attack'/'USG', 'Jerry Tiritilli').
card_number('sneak attack'/'USG', '218').
card_flavor_text('sneak attack'/'USG', '\"Nothin\' beat surprise—\'cept rock.\"').
card_multiverse_id('sneak attack'/'USG', '5594').

card_in_set('somnophore', 'USG').
card_original_type('somnophore'/'USG', 'Summon — Illusion').
card_original_text('somnophore'/'USG', 'Flying\nWhenever Somnophore successfully deals damage to a player, tap target creature that player controls. That creature does not untap during its controller\'s untap phase as long as Somnophore remains in play.').
card_first_print('somnophore', 'USG').
card_image_name('somnophore'/'USG', 'somnophore').
card_uid('somnophore'/'USG', 'USG:Somnophore:somnophore').
card_rarity('somnophore'/'USG', 'Rare').
card_artist('somnophore'/'USG', 'Andrew Robinson').
card_number('somnophore'/'USG', '97').
card_multiverse_id('somnophore'/'USG', '5745').

card_in_set('songstitcher', 'USG').
card_original_type('songstitcher'/'USG', 'Summon — Cleric').
card_original_text('songstitcher'/'USG', '{1}{W} Target attacking creature with flying deals no combat damage this turn.').
card_first_print('songstitcher', 'USG').
card_image_name('songstitcher'/'USG', 'songstitcher').
card_uid('songstitcher'/'USG', 'USG:Songstitcher:songstitcher').
card_rarity('songstitcher'/'USG', 'Uncommon').
card_artist('songstitcher'/'USG', 'Berry').
card_number('songstitcher'/'USG', '52').
card_flavor_text('songstitcher'/'USG', 'The true names of birds are songs woven into their souls.').
card_multiverse_id('songstitcher'/'USG', '5669').

card_in_set('soul sculptor', 'USG').
card_original_type('soul sculptor'/'USG', 'Summon — Townsfolk').
card_original_text('soul sculptor'/'USG', '{1}{W}, {T}: Target creature becomes an enchantment and loses all abilities until a player successfully casts a creature spell.').
card_first_print('soul sculptor', 'USG').
card_image_name('soul sculptor'/'USG', 'soul sculptor').
card_uid('soul sculptor'/'USG', 'USG:Soul Sculptor:soul sculptor').
card_rarity('soul sculptor'/'USG', 'Rare').
card_artist('soul sculptor'/'USG', 'Ciruelo').
card_number('soul sculptor'/'USG', '53').
card_flavor_text('soul sculptor'/'USG', 'Does the stone mimic life, or did it once live?').
card_multiverse_id('soul sculptor'/'USG', '9846').

card_in_set('spined fluke', 'USG').
card_original_type('spined fluke'/'USG', 'Summon — Horror').
card_original_text('spined fluke'/'USG', 'When Spined Fluke comes into play, sacrifice a creature.\n{B} Regenerate Spined Fluke.').
card_first_print('spined fluke', 'USG').
card_image_name('spined fluke'/'USG', 'spined fluke').
card_uid('spined fluke'/'USG', 'USG:Spined Fluke:spined fluke').
card_rarity('spined fluke'/'USG', 'Uncommon').
card_artist('spined fluke'/'USG', 'Mark A. Nelson').
card_number('spined fluke'/'USG', '160').
card_flavor_text('spined fluke'/'USG', 'Its spines are prized as writing quills by the priests of Gix.').
card_multiverse_id('spined fluke'/'USG', '5849').

card_in_set('spire owl', 'USG').
card_original_type('spire owl'/'USG', 'Summon — Bird').
card_original_text('spire owl'/'USG', 'Flying\nWhen Spire Owl comes into play, look at the top four cards of your library and put them back in any order.').
card_first_print('spire owl', 'USG').
card_image_name('spire owl'/'USG', 'spire owl').
card_uid('spire owl'/'USG', 'USG:Spire Owl:spire owl').
card_rarity('spire owl'/'USG', 'Common').
card_artist('spire owl'/'USG', 'Steve Firchow').
card_number('spire owl'/'USG', '98').
card_multiverse_id('spire owl'/'USG', '7167').

card_in_set('sporogenesis', 'USG').
card_original_type('sporogenesis'/'USG', 'Enchantment').
card_original_text('sporogenesis'/'USG', 'During your upkeep, you may put a fungus counter on target nontoken creature.\nWhenever a creature with a fungus counter on it is put into a graveyard, put a Saproling token into play for each of those fungus counters. Treat these tokens as 1/1 green creatures.\nWhen Sporogenesis leaves play, remove all fungus counters from all creatures.').
card_first_print('sporogenesis', 'USG').
card_image_name('sporogenesis'/'USG', 'sporogenesis').
card_uid('sporogenesis'/'USG', 'USG:Sporogenesis:sporogenesis').
card_rarity('sporogenesis'/'USG', 'Rare').
card_artist('sporogenesis'/'USG', 'Ron Spencer').
card_number('sporogenesis'/'USG', '273').
card_multiverse_id('sporogenesis'/'USG', '5750').

card_in_set('spreading algae', 'USG').
card_original_type('spreading algae'/'USG', 'Enchant Land').
card_original_text('spreading algae'/'USG', 'Play Spreading Algae only on a swamp. \nWhen enchanted land becomes tapped, destroy that land.\nWhen Spreading Algae is put into a graveyard from play, return Spreading Algae to owner\'s hand.').
card_first_print('spreading algae', 'USG').
card_image_name('spreading algae'/'USG', 'spreading algae').
card_uid('spreading algae'/'USG', 'USG:Spreading Algae:spreading algae').
card_rarity('spreading algae'/'USG', 'Uncommon').
card_artist('spreading algae'/'USG', 'Stephen Daniele').
card_number('spreading algae'/'USG', '274').
card_multiverse_id('spreading algae'/'USG', '10409').

card_in_set('steam blast', 'USG').
card_original_type('steam blast'/'USG', 'Sorcery').
card_original_text('steam blast'/'USG', 'Steam Blast deals 2 damage to each creature and player.').
card_first_print('steam blast', 'USG').
card_image_name('steam blast'/'USG', 'steam blast').
card_uid('steam blast'/'USG', 'USG:Steam Blast:steam blast').
card_rarity('steam blast'/'USG', 'Uncommon').
card_artist('steam blast'/'USG', 'Mike Raabe').
card_number('steam blast'/'USG', '219').
card_flavor_text('steam blast'/'USG', 'The viashino knew of the cracked pipes but deliberately left them unmended to bolster the rig\'s defenses.').
card_multiverse_id('steam blast'/'USG', '5563').

card_in_set('stern proctor', 'USG').
card_original_type('stern proctor'/'USG', 'Summon — Wizard').
card_original_text('stern proctor'/'USG', 'When Stern Proctor comes into play, return target artifact or enchantment to owner\'s hand.').
card_first_print('stern proctor', 'USG').
card_image_name('stern proctor'/'USG', 'stern proctor').
card_uid('stern proctor'/'USG', 'USG:Stern Proctor:stern proctor').
card_rarity('stern proctor'/'USG', 'Uncommon').
card_artist('stern proctor'/'USG', 'Randy Gallegos').
card_number('stern proctor'/'USG', '99').
card_flavor_text('stern proctor'/'USG', '\"I preferred the harsh tutors—they made mischief all the more fun.\"\n—Teferi, third-level student').
card_multiverse_id('stern proctor'/'USG', '5661').

card_in_set('stroke of genius', 'USG').
card_original_type('stroke of genius'/'USG', 'Instant').
card_original_text('stroke of genius'/'USG', 'Target player draws X cards.').
card_image_name('stroke of genius'/'USG', 'stroke of genius').
card_uid('stroke of genius'/'USG', 'USG:Stroke of Genius:stroke of genius').
card_rarity('stroke of genius'/'USG', 'Rare').
card_artist('stroke of genius'/'USG', 'Stephen Daniele').
card_number('stroke of genius'/'USG', '100').
card_flavor_text('stroke of genius'/'USG', 'After a hundred failed experiments, Urza was stunned to find that common silver passed through the portal undamaged. He immediately designed a golem made of the metal.').
card_multiverse_id('stroke of genius'/'USG', '5677').

card_in_set('sulfuric vapors', 'USG').
card_original_type('sulfuric vapors'/'USG', 'Enchantment').
card_original_text('sulfuric vapors'/'USG', 'Whenever a red spell deals damage, it instead deals that amount of damage plus 1.').
card_first_print('sulfuric vapors', 'USG').
card_image_name('sulfuric vapors'/'USG', 'sulfuric vapors').
card_uid('sulfuric vapors'/'USG', 'USG:Sulfuric Vapors:sulfuric vapors').
card_rarity('sulfuric vapors'/'USG', 'Rare').
card_artist('sulfuric vapors'/'USG', 'Lawrence Snelly').
card_number('sulfuric vapors'/'USG', '220').
card_flavor_text('sulfuric vapors'/'USG', 'It was the dragons who first learned to chase their prey into the vapors before igniting them.').
card_multiverse_id('sulfuric vapors'/'USG', '5638').

card_in_set('sunder', 'USG').
card_original_type('sunder'/'USG', 'Instant').
card_original_text('sunder'/'USG', 'Return all lands to owners\' hands.').
card_first_print('sunder', 'USG').
card_image_name('sunder'/'USG', 'sunder').
card_uid('sunder'/'USG', 'USG:Sunder:sunder').
card_rarity('sunder'/'USG', 'Rare').
card_artist('sunder'/'USG', 'Stephen Daniele').
card_number('sunder'/'USG', '101').
card_flavor_text('sunder'/'USG', 'The flow of time was disrupted; like a flooding river it rose from its banks. Tolaria was drowned in an instant that stretched toward infinity.').
card_multiverse_id('sunder'/'USG', '5599').

card_in_set('swamp', 'USG').
card_original_type('swamp'/'USG', 'Land').
card_original_text('swamp'/'USG', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'USG', 'swamp1').
card_uid('swamp'/'USG', 'USG:Swamp:swamp1').
card_rarity('swamp'/'USG', 'Basic Land').
card_artist('swamp'/'USG', 'John Avon').
card_number('swamp'/'USG', '339').
card_multiverse_id('swamp'/'USG', '8330').

card_in_set('swamp', 'USG').
card_original_type('swamp'/'USG', 'Land').
card_original_text('swamp'/'USG', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'USG', 'swamp2').
card_uid('swamp'/'USG', 'USG:Swamp:swamp2').
card_rarity('swamp'/'USG', 'Basic Land').
card_artist('swamp'/'USG', 'John Avon').
card_number('swamp'/'USG', '340').
card_multiverse_id('swamp'/'USG', '8331').

card_in_set('swamp', 'USG').
card_original_type('swamp'/'USG', 'Land').
card_original_text('swamp'/'USG', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'USG', 'swamp3').
card_uid('swamp'/'USG', 'USG:Swamp:swamp3').
card_rarity('swamp'/'USG', 'Basic Land').
card_artist('swamp'/'USG', 'John Avon').
card_number('swamp'/'USG', '341').
card_multiverse_id('swamp'/'USG', '8332').

card_in_set('swamp', 'USG').
card_original_type('swamp'/'USG', 'Land').
card_original_text('swamp'/'USG', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'USG', 'swamp4').
card_uid('swamp'/'USG', 'USG:Swamp:swamp4').
card_rarity('swamp'/'USG', 'Basic Land').
card_artist('swamp'/'USG', 'John Avon').
card_number('swamp'/'USG', '342').
card_multiverse_id('swamp'/'USG', '8333').

card_in_set('symbiosis', 'USG').
card_original_type('symbiosis'/'USG', 'Instant').
card_original_text('symbiosis'/'USG', 'Two target creatures each get +2/+2 until end of turn.').
card_first_print('symbiosis', 'USG').
card_image_name('symbiosis'/'USG', 'symbiosis').
card_uid('symbiosis'/'USG', 'USG:Symbiosis:symbiosis').
card_rarity('symbiosis'/'USG', 'Common').
card_artist('symbiosis'/'USG', 'Jeff Miracola').
card_number('symbiosis'/'USG', '275').
card_flavor_text('symbiosis'/'USG', 'Although the elves of Argoth always considered them a nuisance, the pixies made fine allies during the war against the machines.').
card_multiverse_id('symbiosis'/'USG', '5632').

card_in_set('tainted æther', 'USG').
card_original_type('tainted æther'/'USG', 'Enchantment').
card_original_text('tainted æther'/'USG', 'Whenever a creature comes into play, its controller sacrifices a creature or land.').
card_first_print('tainted æther', 'USG').
card_image_name('tainted æther'/'USG', 'tainted aether').
card_uid('tainted æther'/'USG', 'USG:Tainted Æther:tainted aether').
card_rarity('tainted æther'/'USG', 'Rare').
card_artist('tainted æther'/'USG', 'Thomas M. Baxa').
card_number('tainted æther'/'USG', '161').
card_flavor_text('tainted æther'/'USG', 'Gix despised the sylvan morass. The gouge that the portal had torn in the forest was the only pleasing sight.').
card_multiverse_id('tainted æther'/'USG', '5752').

card_in_set('telepathy', 'USG').
card_original_type('telepathy'/'USG', 'Enchantment').
card_original_text('telepathy'/'USG', 'Each of your opponents plays with his or her hand revealed.').
card_first_print('telepathy', 'USG').
card_image_name('telepathy'/'USG', 'telepathy').
card_uid('telepathy'/'USG', 'USG:Telepathy:telepathy').
card_rarity('telepathy'/'USG', 'Uncommon').
card_artist('telepathy'/'USG', 'Matthew D. Wilson').
card_number('telepathy'/'USG', '102').
card_flavor_text('telepathy'/'USG', '\"The most disappointing thing about learning telepathy is finding out how boring people really are.\"\n—Teferi, fourth-level student').
card_multiverse_id('telepathy'/'USG', '5841').

card_in_set('temporal aperture', 'USG').
card_original_type('temporal aperture'/'USG', 'Artifact').
card_original_text('temporal aperture'/'USG', '{5}, {T}: Shuffle your library and reveal the top card. Until end of turn, as long as that card remains on top of your library, you may play the card as though it were in your hand without paying its casting cost. (If the spell has {X} in its casting cost, X is 0.)').
card_first_print('temporal aperture', 'USG').
card_image_name('temporal aperture'/'USG', 'temporal aperture').
card_uid('temporal aperture'/'USG', 'USG:Temporal Aperture:temporal aperture').
card_rarity('temporal aperture'/'USG', 'Rare').
card_artist('temporal aperture'/'USG', 'Michael Sutfin').
card_number('temporal aperture'/'USG', '310').
card_multiverse_id('temporal aperture'/'USG', '5548').

card_in_set('thran quarry', 'USG').
card_original_type('thran quarry'/'USG', 'Land').
card_original_text('thran quarry'/'USG', 'At the end of each turn, if you control no creatures, sacrifice Thran Quarry.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('thran quarry', 'USG').
card_image_name('thran quarry'/'USG', 'thran quarry').
card_uid('thran quarry'/'USG', 'USG:Thran Quarry:thran quarry').
card_rarity('thran quarry'/'USG', 'Rare').
card_artist('thran quarry'/'USG', 'Michael Sutfin').
card_number('thran quarry'/'USG', '329').
card_multiverse_id('thran quarry'/'USG', '8828').

card_in_set('thran turbine', 'USG').
card_original_type('thran turbine'/'USG', 'Artifact').
card_original_text('thran turbine'/'USG', 'During your upkeep, you may add up to two colorless mana to your mana pool. This mana cannot be spent to play spells.').
card_first_print('thran turbine', 'USG').
card_image_name('thran turbine'/'USG', 'thran turbine').
card_uid('thran turbine'/'USG', 'USG:Thran Turbine:thran turbine').
card_rarity('thran turbine'/'USG', 'Uncommon').
card_artist('thran turbine'/'USG', 'Brian Snõddy').
card_number('thran turbine'/'USG', '311').
card_flavor_text('thran turbine'/'USG', 'When Urza asked the viashino what it did, they answered: \"It hums.\"').
card_multiverse_id('thran turbine'/'USG', '8895').

card_in_set('thundering giant', 'USG').
card_original_type('thundering giant'/'USG', 'Summon — Giant').
card_original_text('thundering giant'/'USG', 'Thundering Giant is unaffected by summoning sickness.').
card_first_print('thundering giant', 'USG').
card_image_name('thundering giant'/'USG', 'thundering giant').
card_uid('thundering giant'/'USG', 'USG:Thundering Giant:thundering giant').
card_rarity('thundering giant'/'USG', 'Uncommon').
card_artist('thundering giant'/'USG', 'Mark Zug').
card_number('thundering giant'/'USG', '221').
card_flavor_text('thundering giant'/'USG', 'The giant was felt a few seconds before he was seen.').
card_multiverse_id('thundering giant'/'USG', '5759').

card_in_set('time spiral', 'USG').
card_original_type('time spiral'/'USG', 'Sorcery').
card_original_text('time spiral'/'USG', 'Remove Time Spiral from the game. Each player shuffles his or her graveyard and hand into his or her library, then draws seven cards. You untap up to six lands.').
card_first_print('time spiral', 'USG').
card_image_name('time spiral'/'USG', 'time spiral').
card_uid('time spiral'/'USG', 'USG:Time Spiral:time spiral').
card_rarity('time spiral'/'USG', 'Rare').
card_artist('time spiral'/'USG', 'Michael Sutfin').
card_number('time spiral'/'USG', '103').
card_multiverse_id('time spiral'/'USG', '10423').

card_in_set('titania\'s boon', 'USG').
card_original_type('titania\'s boon'/'USG', 'Sorcery').
card_original_text('titania\'s boon'/'USG', 'Put a +1/+1 counter on each creature you control.').
card_first_print('titania\'s boon', 'USG').
card_image_name('titania\'s boon'/'USG', 'titania\'s boon').
card_uid('titania\'s boon'/'USG', 'USG:Titania\'s Boon:titania\'s boon').
card_rarity('titania\'s boon'/'USG', 'Uncommon').
card_artist('titania\'s boon'/'USG', 'Val Mayerik').
card_number('titania\'s boon'/'USG', '276').
card_flavor_text('titania\'s boon'/'USG', 'When the winds rock the trees, listen for voices in the creaking of the trunks. If you hear your name, you are one of the Goddess\'s chosen.').
card_multiverse_id('titania\'s boon'/'USG', '8819').

card_in_set('titania\'s chosen', 'USG').
card_original_type('titania\'s chosen'/'USG', 'Summon — Elf').
card_original_text('titania\'s chosen'/'USG', 'Whenever a player successfully casts a green spell, put a +1/+1 counter on Titania\'s Chosen.').
card_first_print('titania\'s chosen', 'USG').
card_image_name('titania\'s chosen'/'USG', 'titania\'s chosen').
card_uid('titania\'s chosen'/'USG', 'USG:Titania\'s Chosen:titania\'s chosen').
card_rarity('titania\'s chosen'/'USG', 'Uncommon').
card_artist('titania\'s chosen'/'USG', 'Mark Zug').
card_number('titania\'s chosen'/'USG', '277').
card_flavor_text('titania\'s chosen'/'USG', '\"What do a hero and an arrow have in common? In times of war are many more made.\"\n—Elvish riddle').
card_multiverse_id('titania\'s chosen'/'USG', '5838').

card_in_set('tolarian academy', 'USG').
card_original_type('tolarian academy'/'USG', 'Legendary Land').
card_original_text('tolarian academy'/'USG', '{T}: Add {U} to your mana pool for each artifact you control.').
card_first_print('tolarian academy', 'USG').
card_image_name('tolarian academy'/'USG', 'tolarian academy').
card_uid('tolarian academy'/'USG', 'USG:Tolarian Academy:tolarian academy').
card_rarity('tolarian academy'/'USG', 'Rare').
card_artist('tolarian academy'/'USG', 'Stephen Daniele').
card_number('tolarian academy'/'USG', '330').
card_flavor_text('tolarian academy'/'USG', 'The academy worked with time—until time ran out.').
card_multiverse_id('tolarian academy'/'USG', '8883').

card_in_set('tolarian winds', 'USG').
card_original_type('tolarian winds'/'USG', 'Instant').
card_original_text('tolarian winds'/'USG', 'Discard your hand, then draw that many cards.').
card_first_print('tolarian winds', 'USG').
card_image_name('tolarian winds'/'USG', 'tolarian winds').
card_uid('tolarian winds'/'USG', 'USG:Tolarian Winds:tolarian winds').
card_rarity('tolarian winds'/'USG', 'Common').
card_artist('tolarian winds'/'USG', 'Lawrence Snelly').
card_number('tolarian winds'/'USG', '104').
card_flavor_text('tolarian winds'/'USG', 'Afterward, Tolaria\'s winds were like the whispers of lost wizards, calling for life.').
card_multiverse_id('tolarian winds'/'USG', '5834').

card_in_set('torch song', 'USG').
card_original_type('torch song'/'USG', 'Enchantment').
card_original_text('torch song'/'USG', 'During your upkeep, you may put a verse counter on Torch Song.\n{2}{R}, Sacrifice Torch Song: Torch Song deals X damage to target creature or player, where X is the number of verse counters on Torch Song.').
card_first_print('torch song', 'USG').
card_image_name('torch song'/'USG', 'torch song').
card_uid('torch song'/'USG', 'USG:Torch Song:torch song').
card_rarity('torch song'/'USG', 'Uncommon').
card_artist('torch song'/'USG', 'Thomas M. Baxa').
card_number('torch song'/'USG', '222').
card_multiverse_id('torch song'/'USG', '5659').

card_in_set('treefolk seedlings', 'USG').
card_original_type('treefolk seedlings'/'USG', 'Summon — Treefolk').
card_original_text('treefolk seedlings'/'USG', 'Treefolk Seedlings has toughness equal to the number of forests you control.').
card_first_print('treefolk seedlings', 'USG').
card_image_name('treefolk seedlings'/'USG', 'treefolk seedlings').
card_uid('treefolk seedlings'/'USG', 'USG:Treefolk Seedlings:treefolk seedlings').
card_rarity('treefolk seedlings'/'USG', 'Uncommon').
card_artist('treefolk seedlings'/'USG', 'John Matson').
card_number('treefolk seedlings'/'USG', '278').
card_flavor_text('treefolk seedlings'/'USG', 'The year that the brothers landed on Argoth, the trees produced five times as many seeds as normal.').
card_multiverse_id('treefolk seedlings'/'USG', '5866').

card_in_set('treetop rangers', 'USG').
card_original_type('treetop rangers'/'USG', 'Summon — Elves').
card_original_text('treetop rangers'/'USG', 'Treetop Rangers cannot be blocked except by creatures with flying.').
card_first_print('treetop rangers', 'USG').
card_image_name('treetop rangers'/'USG', 'treetop rangers').
card_uid('treetop rangers'/'USG', 'USG:Treetop Rangers:treetop rangers').
card_rarity('treetop rangers'/'USG', 'Common').
card_artist('treetop rangers'/'USG', 'Daren Bader').
card_number('treetop rangers'/'USG', '279').
card_flavor_text('treetop rangers'/'USG', '\"If you can\'t catch them, cut the trees down from beneath them. Force them to fight on our terms.\"\n—Mishra').
card_multiverse_id('treetop rangers'/'USG', '5681').

card_in_set('turnabout', 'USG').
card_original_type('turnabout'/'USG', 'Instant').
card_original_text('turnabout'/'USG', 'Tap or untap all artifacts, creatures, or lands target player controls.').
card_image_name('turnabout'/'USG', 'turnabout').
card_uid('turnabout'/'USG', 'USG:Turnabout:turnabout').
card_rarity('turnabout'/'USG', 'Uncommon').
card_artist('turnabout'/'USG', 'Heather Hudson').
card_number('turnabout'/'USG', '105').
card_flavor_text('turnabout'/'USG', 'The best cure for a big ego is a little failure.').
card_multiverse_id('turnabout'/'USG', '5728').

card_in_set('umbilicus', 'USG').
card_original_type('umbilicus'/'USG', 'Artifact').
card_original_text('umbilicus'/'USG', 'During each player\'s upkeep, that player pays 2 life or returns a permanent he or she controls to owner\'s hand.').
card_first_print('umbilicus', 'USG').
card_image_name('umbilicus'/'USG', 'umbilicus').
card_uid('umbilicus'/'USG', 'USG:Umbilicus:umbilicus').
card_rarity('umbilicus'/'USG', 'Rare').
card_artist('umbilicus'/'USG', 'Dermot Power').
card_number('umbilicus'/'USG', '312').
card_flavor_text('umbilicus'/'USG', 'It was the explorers\' only tether to reality.').
card_multiverse_id('umbilicus'/'USG', '8810').

card_in_set('unnerve', 'USG').
card_original_type('unnerve'/'USG', 'Sorcery').
card_original_text('unnerve'/'USG', 'Each of your opponents chooses and discards two cards.').
card_first_print('unnerve', 'USG').
card_image_name('unnerve'/'USG', 'unnerve').
card_uid('unnerve'/'USG', 'USG:Unnerve:unnerve').
card_rarity('unnerve'/'USG', 'Common').
card_artist('unnerve'/'USG', 'Terese Nielsen').
card_number('unnerve'/'USG', '162').
card_flavor_text('unnerve'/'USG', '\"If fear is the only tool you have left, then you\'ll never control me.\"\n—Xantcha, to Gix').
card_multiverse_id('unnerve'/'USG', '5554').

card_in_set('unworthy dead', 'USG').
card_original_type('unworthy dead'/'USG', 'Summon — Skeletons').
card_original_text('unworthy dead'/'USG', '{B} Regenerate Unworthy Dead.').
card_first_print('unworthy dead', 'USG').
card_image_name('unworthy dead'/'USG', 'unworthy dead').
card_uid('unworthy dead'/'USG', 'USG:Unworthy Dead:unworthy dead').
card_rarity('unworthy dead'/'USG', 'Common').
card_artist('unworthy dead'/'USG', 'Carl Critchlow').
card_number('unworthy dead'/'USG', '163').
card_flavor_text('unworthy dead'/'USG', '\"Great Yawgmoth moves across the seas of shard and bone and rust. We exalt him in life, in death, and in between.\"\n—Phyrexian Scriptures').
card_multiverse_id('unworthy dead'/'USG', '5780').

card_in_set('urza\'s armor', 'USG').
card_original_type('urza\'s armor'/'USG', 'Artifact').
card_original_text('urza\'s armor'/'USG', 'Whenever a source deals damage to you, that damage is reduced by 1.').
card_first_print('urza\'s armor', 'USG').
card_image_name('urza\'s armor'/'USG', 'urza\'s armor').
card_uid('urza\'s armor'/'USG', 'USG:Urza\'s Armor:urza\'s armor').
card_rarity('urza\'s armor'/'USG', 'Uncommon').
card_artist('urza\'s armor'/'USG', 'rk post').
card_number('urza\'s armor'/'USG', '313').
card_flavor_text('urza\'s armor'/'USG', '\"Tawnos\'s blueprints were critical to the creation of my armor. As he once sealed himself in steel, I sealed myself in a walking crypt.\"\n—Urza').
card_multiverse_id('urza\'s armor'/'USG', '5574').

card_in_set('vampiric embrace', 'USG').
card_original_type('vampiric embrace'/'USG', 'Enchant Creature').
card_original_text('vampiric embrace'/'USG', 'Enchanted creature gets +2/+2 and gains flying.\nWhenever a creature successfully dealt damage by enchanted creature this turn is put into a graveyard, put a +1/+1 counter on enchanted creature.').
card_first_print('vampiric embrace', 'USG').
card_image_name('vampiric embrace'/'USG', 'vampiric embrace').
card_uid('vampiric embrace'/'USG', 'USG:Vampiric Embrace:vampiric embrace').
card_rarity('vampiric embrace'/'USG', 'Uncommon').
card_artist('vampiric embrace'/'USG', 'Thomas M. Baxa').
card_number('vampiric embrace'/'USG', '164').
card_multiverse_id('vampiric embrace'/'USG', '9684').

card_in_set('vebulid', 'USG').
card_original_type('vebulid'/'USG', 'Summon — Horror').
card_original_text('vebulid'/'USG', 'Vebulid comes into play with one +1/+1 counter on it.\nDuring your upkeep, you may put a +1/+1 counter on Vebulid.\nWhen Vebulid attacks or blocks, destroy it at end of combat.').
card_first_print('vebulid', 'USG').
card_image_name('vebulid'/'USG', 'vebulid').
card_uid('vebulid'/'USG', 'USG:Vebulid:vebulid').
card_rarity('vebulid'/'USG', 'Rare').
card_artist('vebulid'/'USG', 'Ron Spencer').
card_number('vebulid'/'USG', '165').
card_multiverse_id('vebulid'/'USG', '5688').

card_in_set('veil of birds', 'USG').
card_original_type('veil of birds'/'USG', 'Enchantment').
card_original_text('veil of birds'/'USG', 'When one of your opponents successfully casts a spell, if Veil of Birds is an enchantment, Veil of Birds becomes a 1/1 creature with flying that counts as a Bird.').
card_first_print('veil of birds', 'USG').
card_image_name('veil of birds'/'USG', 'veil of birds').
card_uid('veil of birds'/'USG', 'USG:Veil of Birds:veil of birds').
card_rarity('veil of birds'/'USG', 'Common').
card_artist('veil of birds'/'USG', 'Heather Hudson').
card_number('veil of birds'/'USG', '106').
card_flavor_text('veil of birds'/'USG', 'When wind marries sky, even the bride\'s veil sings her praises.').
card_multiverse_id('veil of birds'/'USG', '5788').

card_in_set('veiled apparition', 'USG').
card_original_type('veiled apparition'/'USG', 'Enchantment').
card_original_text('veiled apparition'/'USG', 'When one of your opponents successfully casts a spell, if Veiled Apparition is an enchantment, Veiled Apparition becomes a 3/3 creature with flying and \"During your upkeep, pay {1}{U} or sacrifice Veiled Apparition\" and that counts as an Illusion.').
card_first_print('veiled apparition', 'USG').
card_image_name('veiled apparition'/'USG', 'veiled apparition').
card_uid('veiled apparition'/'USG', 'USG:Veiled Apparition:veiled apparition').
card_rarity('veiled apparition'/'USG', 'Uncommon').
card_artist('veiled apparition'/'USG', 'Andrew Robinson').
card_number('veiled apparition'/'USG', '107').
card_multiverse_id('veiled apparition'/'USG', '5797').

card_in_set('veiled crocodile', 'USG').
card_original_type('veiled crocodile'/'USG', 'Enchantment').
card_original_text('veiled crocodile'/'USG', 'When a player has no cards in hand, if Veiled Crocodile is an enchantment, Veiled Crocodile becomes a 4/4 creature that counts as a Crocodile.').
card_first_print('veiled crocodile', 'USG').
card_image_name('veiled crocodile'/'USG', 'veiled crocodile').
card_uid('veiled crocodile'/'USG', 'USG:Veiled Crocodile:veiled crocodile').
card_rarity('veiled crocodile'/'USG', 'Rare').
card_artist('veiled crocodile'/'USG', 'Paolo Parente').
card_number('veiled crocodile'/'USG', '108').
card_flavor_text('veiled crocodile'/'USG', 'Some roads are paved with bad intentions.').
card_multiverse_id('veiled crocodile'/'USG', '5789').

card_in_set('veiled sentry', 'USG').
card_original_type('veiled sentry'/'USG', 'Enchantment').
card_original_text('veiled sentry'/'USG', 'When one of your opponents successfully casts a spell, if Veiled Sentry is an enchantment, Veiled Sentry becomes a creature with power and toughness each equal to the total casting cost of that spell and that counts as an Illusion.').
card_first_print('veiled sentry', 'USG').
card_image_name('veiled sentry'/'USG', 'veiled sentry').
card_uid('veiled sentry'/'USG', 'USG:Veiled Sentry:veiled sentry').
card_rarity('veiled sentry'/'USG', 'Uncommon').
card_artist('veiled sentry'/'USG', 'Ron Spears').
card_number('veiled sentry'/'USG', '109').
card_multiverse_id('veiled sentry'/'USG', '5793').

card_in_set('veiled serpent', 'USG').
card_original_type('veiled serpent'/'USG', 'Enchantment').
card_original_text('veiled serpent'/'USG', 'When one of your opponents successfully casts a spell, if Veiled Serpent is an enchantment, Veiled Serpent becomes a 4/4 creature that cannot attack unless defending player controls an island and that counts as a Serpent.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('veiled serpent', 'USG').
card_image_name('veiled serpent'/'USG', 'veiled serpent').
card_uid('veiled serpent'/'USG', 'USG:Veiled Serpent:veiled serpent').
card_rarity('veiled serpent'/'USG', 'Common').
card_artist('veiled serpent'/'USG', 'Bob Eggleton').
card_number('veiled serpent'/'USG', '110').
card_multiverse_id('veiled serpent'/'USG', '8906').

card_in_set('venomous fangs', 'USG').
card_original_type('venomous fangs'/'USG', 'Enchant Creature').
card_original_text('venomous fangs'/'USG', 'Whenever enchanted creature successfully deals damage to a creature, destroy that creature.').
card_first_print('venomous fangs', 'USG').
card_image_name('venomous fangs'/'USG', 'venomous fangs').
card_uid('venomous fangs'/'USG', 'USG:Venomous Fangs:venomous fangs').
card_rarity('venomous fangs'/'USG', 'Common').
card_artist('venomous fangs'/'USG', 'Lawrence Snelly').
card_number('venomous fangs'/'USG', '280').
card_flavor_text('venomous fangs'/'USG', 'All the pain of the shattered forest contained in a single drop.').
card_multiverse_id('venomous fangs'/'USG', '8908').

card_in_set('vernal bloom', 'USG').
card_original_type('vernal bloom'/'USG', 'Enchantment').
card_original_text('vernal bloom'/'USG', 'Whenever a forest is tapped for mana, it produces an additional {G}.').
card_first_print('vernal bloom', 'USG').
card_image_name('vernal bloom'/'USG', 'vernal bloom').
card_uid('vernal bloom'/'USG', 'USG:Vernal Bloom:vernal bloom').
card_rarity('vernal bloom'/'USG', 'Rare').
card_artist('vernal bloom'/'USG', 'Bob Eggleton').
card_number('vernal bloom'/'USG', '281').
card_flavor_text('vernal bloom'/'USG', 'Many cultures have legends of a lush, hidden paradise. The elves of Argoth had no need of such stories.').
card_multiverse_id('vernal bloom'/'USG', '5687').

card_in_set('viashino outrider', 'USG').
card_original_type('viashino outrider'/'USG', 'Summon — Viashino').
card_original_text('viashino outrider'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_first_print('viashino outrider', 'USG').
card_image_name('viashino outrider'/'USG', 'viashino outrider').
card_uid('viashino outrider'/'USG', 'USG:Viashino Outrider:viashino outrider').
card_rarity('viashino outrider'/'USG', 'Common').
card_artist('viashino outrider'/'USG', 'Ciruelo').
card_number('viashino outrider'/'USG', '223').
card_flavor_text('viashino outrider'/'USG', '\"Give thy ration to thy mount, if the road be long. So sayeth the bey.\"').
card_multiverse_id('viashino outrider'/'USG', '5739').

card_in_set('viashino runner', 'USG').
card_original_type('viashino runner'/'USG', 'Summon — Viashino').
card_original_text('viashino runner'/'USG', 'Viashino Runner cannot be blocked by only one creature.').
card_first_print('viashino runner', 'USG').
card_image_name('viashino runner'/'USG', 'viashino runner').
card_uid('viashino runner'/'USG', 'USG:Viashino Runner:viashino runner').
card_rarity('viashino runner'/'USG', 'Common').
card_artist('viashino runner'/'USG', 'Steve White').
card_number('viashino runner'/'USG', '224').
card_flavor_text('viashino runner'/'USG', '\"It moved this way, an\' that way, an\' then before I could stick it, it jumped over my head an\' was gone.\"\n—Jula, goblin raider').
card_multiverse_id('viashino runner'/'USG', '5657').

card_in_set('viashino sandswimmer', 'USG').
card_original_type('viashino sandswimmer'/'USG', 'Summon — Viashino').
card_original_text('viashino sandswimmer'/'USG', '{R} Flip a coin. If you win the flip, return Viashino Sandswimmer to owner\'s hand. Otherwise, sacrifice Viashino Sandswimmer.').
card_first_print('viashino sandswimmer', 'USG').
card_image_name('viashino sandswimmer'/'USG', 'viashino sandswimmer').
card_uid('viashino sandswimmer'/'USG', 'USG:Viashino Sandswimmer:viashino sandswimmer').
card_rarity('viashino sandswimmer'/'USG', 'Rare').
card_artist('viashino sandswimmer'/'USG', 'Pete Venters').
card_number('viashino sandswimmer'/'USG', '225').
card_flavor_text('viashino sandswimmer'/'USG', 'Few swim in a place of such thirst.').
card_multiverse_id('viashino sandswimmer'/'USG', '5565').

card_in_set('viashino weaponsmith', 'USG').
card_original_type('viashino weaponsmith'/'USG', 'Summon — Viashino').
card_original_text('viashino weaponsmith'/'USG', 'Whenever a creature blocks it, Viashino Weaponsmith gets +2/+2 until end of turn.').
card_first_print('viashino weaponsmith', 'USG').
card_image_name('viashino weaponsmith'/'USG', 'viashino weaponsmith').
card_uid('viashino weaponsmith'/'USG', 'USG:Viashino Weaponsmith:viashino weaponsmith').
card_rarity('viashino weaponsmith'/'USG', 'Common').
card_artist('viashino weaponsmith'/'USG', 'Dermot Power').
card_number('viashino weaponsmith'/'USG', '226').
card_flavor_text('viashino weaponsmith'/'USG', 'Within the rig settlement, those who have mastered the making of weapons earn highest honor.').
card_multiverse_id('viashino weaponsmith'/'USG', '5590').

card_in_set('victimize', 'USG').
card_original_type('victimize'/'USG', 'Sorcery').
card_original_text('victimize'/'USG', 'Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, put the two chosen creatures into play tapped.').
card_first_print('victimize', 'USG').
card_image_name('victimize'/'USG', 'victimize').
card_uid('victimize'/'USG', 'USG:Victimize:victimize').
card_rarity('victimize'/'USG', 'Uncommon').
card_artist('victimize'/'USG', 'Val Mayerik').
card_number('victimize'/'USG', '166').
card_flavor_text('victimize'/'USG', 'The priest cast Xantcha to the ground. \"It is defective. We must scrap it.\"').
card_multiverse_id('victimize'/'USG', '10655').

card_in_set('vile requiem', 'USG').
card_original_type('vile requiem'/'USG', 'Enchantment').
card_original_text('vile requiem'/'USG', 'During your upkeep, you may put a verse counter on Vile Requiem.\n{1}{B}, Sacrifice Vile Requiem: Destroy up to X target nonblack creatures, where X is the number of verse counters on Vile Requiem. Those creatures cannot be regenerated this turn.').
card_first_print('vile requiem', 'USG').
card_image_name('vile requiem'/'USG', 'vile requiem').
card_uid('vile requiem'/'USG', 'USG:Vile Requiem:vile requiem').
card_rarity('vile requiem'/'USG', 'Uncommon').
card_artist('vile requiem'/'USG', 'Carl Critchlow').
card_number('vile requiem'/'USG', '167').
card_multiverse_id('vile requiem'/'USG', '5690').

card_in_set('voice of grace', 'USG').
card_original_type('voice of grace'/'USG', 'Summon — Angel').
card_original_text('voice of grace'/'USG', 'Flying, protection from black').
card_first_print('voice of grace', 'USG').
card_image_name('voice of grace'/'USG', 'voice of grace').
card_uid('voice of grace'/'USG', 'USG:Voice of Grace:voice of grace').
card_rarity('voice of grace'/'USG', 'Uncommon').
card_artist('voice of grace'/'USG', 'Jeff Miracola').
card_number('voice of grace'/'USG', '54').
card_flavor_text('voice of grace'/'USG', '\"Opposite Law is Grace, and Grace must be preserved. If the strands of Grace are unraveled, its design will be lost, and the people with it.\"\n—Song of All, canto 167').
card_multiverse_id('voice of grace'/'USG', '5635').

card_in_set('voice of law', 'USG').
card_original_type('voice of law'/'USG', 'Summon — Angel').
card_original_text('voice of law'/'USG', 'Flying, protection from red').
card_first_print('voice of law', 'USG').
card_image_name('voice of law'/'USG', 'voice of law').
card_uid('voice of law'/'USG', 'USG:Voice of Law:voice of law').
card_rarity('voice of law'/'USG', 'Uncommon').
card_artist('voice of law'/'USG', 'Mark Zug').
card_number('voice of law'/'USG', '55').
card_flavor_text('voice of law'/'USG', '\"Life\'s balance is as a star: on one point is Law, and Law must be upheld. If the knots of order are loosened, chaos will spill through.\"\n—Song of All, canto 167').
card_multiverse_id('voice of law'/'USG', '5673').

card_in_set('voltaic key', 'USG').
card_original_type('voltaic key'/'USG', 'Artifact').
card_original_text('voltaic key'/'USG', '{1}, {T}: Untap target artifact.').
card_first_print('voltaic key', 'USG').
card_image_name('voltaic key'/'USG', 'voltaic key').
card_uid('voltaic key'/'USG', 'USG:Voltaic Key:voltaic key').
card_rarity('voltaic key'/'USG', 'Uncommon').
card_artist('voltaic key'/'USG', 'Henry G. Higgenbotham').
card_number('voltaic key'/'USG', '314').
card_flavor_text('voltaic key'/'USG', 'The key did not work on a single lock, yet it opened many doors.').
card_multiverse_id('voltaic key'/'USG', '5547').

card_in_set('vug lizard', 'USG').
card_original_type('vug lizard'/'USG', 'Summon — Lizard').
card_original_text('vug lizard'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)\nMountainwalk (If defending player controls a mountain, this creature is unblockable.)').
card_first_print('vug lizard', 'USG').
card_image_name('vug lizard'/'USG', 'vug lizard').
card_uid('vug lizard'/'USG', 'USG:Vug Lizard:vug lizard').
card_rarity('vug lizard'/'USG', 'Uncommon').
card_artist('vug lizard'/'USG', 'Heather Hudson').
card_number('vug lizard'/'USG', '227').
card_multiverse_id('vug lizard'/'USG', '5679').

card_in_set('wall of junk', 'USG').
card_original_type('wall of junk'/'USG', 'Artifact Creature').
card_original_text('wall of junk'/'USG', 'Whenever Wall of Junk blocks, return it to owner\'s hand at end of combat.').
card_first_print('wall of junk', 'USG').
card_image_name('wall of junk'/'USG', 'wall of junk').
card_uid('wall of junk'/'USG', 'USG:Wall of Junk:wall of junk').
card_rarity('wall of junk'/'USG', 'Uncommon').
card_artist('wall of junk'/'USG', 'Adam Rex').
card_number('wall of junk'/'USG', '315').
card_flavor_text('wall of junk'/'USG', 'Urza saw the wall and realized that even if he tore every Phyrexian to pieces, they would still resist him.').
card_multiverse_id('wall of junk'/'USG', '8871').

card_in_set('war dance', 'USG').
card_original_type('war dance'/'USG', 'Enchantment').
card_original_text('war dance'/'USG', 'During your upkeep, you may put a verse counter on War Dance.\nSacrifice War Dance: Target creature gets +X/+X until end of turn, where X is the number of verse counters on War Dance.').
card_first_print('war dance', 'USG').
card_image_name('war dance'/'USG', 'war dance').
card_uid('war dance'/'USG', 'USG:War Dance:war dance').
card_rarity('war dance'/'USG', 'Uncommon').
card_artist('war dance'/'USG', 'Terese Nielsen').
card_number('war dance'/'USG', '282').
card_multiverse_id('war dance'/'USG', '5689').

card_in_set('waylay', 'USG').
card_original_type('waylay'/'USG', 'Instant').
card_original_text('waylay'/'USG', 'Put three Knight tokens into play. Treat these tokens as 2/2 white creatures. Remove them from the game at end of turn.').
card_first_print('waylay', 'USG').
card_image_name('waylay'/'USG', 'waylay').
card_uid('waylay'/'USG', 'USG:Waylay:waylay').
card_rarity('waylay'/'USG', 'Uncommon').
card_artist('waylay'/'USG', 'Greg Staples').
card_number('waylay'/'USG', '56').
card_flavor_text('waylay'/'USG', '\"You reek of corruption,\" spat the knight. \"Why are you even here?\"').
card_multiverse_id('waylay'/'USG', '5833').

card_in_set('western paladin', 'USG').
card_original_type('western paladin'/'USG', 'Summon — Knight').
card_original_text('western paladin'/'USG', '{B}{B}, {T}: Destroy target white creature.').
card_first_print('western paladin', 'USG').
card_image_name('western paladin'/'USG', 'western paladin').
card_uid('western paladin'/'USG', 'USG:Western Paladin:western paladin').
card_rarity('western paladin'/'USG', 'Rare').
card_artist('western paladin'/'USG', 'Carl Critchlow').
card_number('western paladin'/'USG', '168').
card_flavor_text('western paladin'/'USG', '\"Their weak laws. Their flawed systems. They inhibit the Grand Evolution. In Yawgmoth\'s name, we shall erase them.\"\n—Oath of the West').
card_multiverse_id('western paladin'/'USG', '5861').

card_in_set('whetstone', 'USG').
card_original_type('whetstone'/'USG', 'Artifact').
card_original_text('whetstone'/'USG', '{3}: Each player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('whetstone', 'USG').
card_image_name('whetstone'/'USG', 'whetstone').
card_uid('whetstone'/'USG', 'USG:Whetstone:whetstone').
card_rarity('whetstone'/'USG', 'Rare').
card_artist('whetstone'/'USG', 'Greg Simanson').
card_number('whetstone'/'USG', '316').
card_flavor_text('whetstone'/'USG', 'To hone swords and dull minds.\n—Whetstone inscription').
card_multiverse_id('whetstone'/'USG', '7247').

card_in_set('whirlwind', 'USG').
card_original_type('whirlwind'/'USG', 'Sorcery').
card_original_text('whirlwind'/'USG', 'Destroy all creatures with flying.').
card_first_print('whirlwind', 'USG').
card_image_name('whirlwind'/'USG', 'whirlwind').
card_uid('whirlwind'/'USG', 'USG:Whirlwind:whirlwind').
card_rarity('whirlwind'/'USG', 'Rare').
card_artist('whirlwind'/'USG', 'John Matson').
card_number('whirlwind'/'USG', '283').
card_flavor_text('whirlwind'/'USG', 'Urza tried to rule the air, but Gaea taught him that she controlled all the elements.').
card_multiverse_id('whirlwind'/'USG', '8849').

card_in_set('wild dogs', 'USG').
card_original_type('wild dogs'/'USG', 'Summon — Hounds').
card_original_text('wild dogs'/'USG', 'During your upkeep, if a player has more life than any other, that player gains control of Wild Dogs.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_first_print('wild dogs', 'USG').
card_image_name('wild dogs'/'USG', 'wild dogs').
card_uid('wild dogs'/'USG', 'USG:Wild Dogs:wild dogs').
card_rarity('wild dogs'/'USG', 'Common').
card_artist('wild dogs'/'USG', 'Terese Nielsen').
card_number('wild dogs'/'USG', '284').
card_multiverse_id('wild dogs'/'USG', '8826').

card_in_set('wildfire', 'USG').
card_original_type('wildfire'/'USG', 'Sorcery').
card_original_text('wildfire'/'USG', 'Each player sacrifices four lands, then Wildfire deals 4 damage to each creature.').
card_image_name('wildfire'/'USG', 'wildfire').
card_uid('wildfire'/'USG', 'USG:Wildfire:wildfire').
card_rarity('wildfire'/'USG', 'Rare').
card_artist('wildfire'/'USG', 'Carl Critchlow').
card_number('wildfire'/'USG', '228').
card_flavor_text('wildfire'/'USG', '\"Shiv hatched from a shell of stone around a yolk of flame.\"\n—Viashino myth').
card_multiverse_id('wildfire'/'USG', '5566').

card_in_set('windfall', 'USG').
card_original_type('windfall'/'USG', 'Sorcery').
card_original_text('windfall'/'USG', 'Each player discards his or her hand and draws cards equal to the greatest number a player discarded this way.').
card_first_print('windfall', 'USG').
card_image_name('windfall'/'USG', 'windfall').
card_uid('windfall'/'USG', 'USG:Windfall:windfall').
card_rarity('windfall'/'USG', 'Uncommon').
card_artist('windfall'/'USG', 'Pete Venters').
card_number('windfall'/'USG', '111').
card_flavor_text('windfall'/'USG', '\"To fill your mind with knowledge, we must start by emptying it.\"\n—Barrin, master wizard').
card_multiverse_id('windfall'/'USG', '7168').

card_in_set('winding wurm', 'USG').
card_original_type('winding wurm'/'USG', 'Summon — Wurm').
card_original_text('winding wurm'/'USG', 'Echo (During your next upkeep after this permanent comes under your control, pay its casting cost or sacrifice it.)').
card_first_print('winding wurm', 'USG').
card_image_name('winding wurm'/'USG', 'winding wurm').
card_uid('winding wurm'/'USG', 'USG:Winding Wurm:winding wurm').
card_rarity('winding wurm'/'USG', 'Common').
card_artist('winding wurm'/'USG', 'DiTerlizzi').
card_number('winding wurm'/'USG', '285').
card_flavor_text('winding wurm'/'USG', 'Entire trees were stripped of their bark and branches by the wurm\'s writhing path.').
card_multiverse_id('winding wurm'/'USG', '5850').

card_in_set('wirecat', 'USG').
card_original_type('wirecat'/'USG', 'Artifact Creature').
card_original_text('wirecat'/'USG', 'Wirecat cannot attack or block if an enchantment is in play.').
card_first_print('wirecat', 'USG').
card_image_name('wirecat'/'USG', 'wirecat').
card_uid('wirecat'/'USG', 'USG:Wirecat:wirecat').
card_rarity('wirecat'/'USG', 'Uncommon').
card_artist('wirecat'/'USG', 'Michael Sutfin').
card_number('wirecat'/'USG', '317').
card_flavor_text('wirecat'/'USG', 'Its purr is the sound of iron filings sliding down satin.').
card_multiverse_id('wirecat'/'USG', '8834').

card_in_set('witch engine', 'USG').
card_original_type('witch engine'/'USG', 'Summon — Horror').
card_original_text('witch engine'/'USG', 'Swampwalk (If defending player controls a swamp, this creature is unblockable.)\n{T}: Add {B}{B}{B}{B} to your mana pool. Target opponent gains control of Witch Engine. (Play this ability as an instant.)').
card_first_print('witch engine', 'USG').
card_image_name('witch engine'/'USG', 'witch engine').
card_uid('witch engine'/'USG', 'USG:Witch Engine:witch engine').
card_rarity('witch engine'/'USG', 'Rare').
card_artist('witch engine'/'USG', 'Kev Walker').
card_number('witch engine'/'USG', '169').
card_multiverse_id('witch engine'/'USG', '5836').

card_in_set('wizard mentor', 'USG').
card_original_type('wizard mentor'/'USG', 'Summon — Wizard').
card_original_text('wizard mentor'/'USG', '{T}: Return Wizard Mentor and target creature you control to owner\'s hand.').
card_first_print('wizard mentor', 'USG').
card_image_name('wizard mentor'/'USG', 'wizard mentor').
card_uid('wizard mentor'/'USG', 'USG:Wizard Mentor:wizard mentor').
card_rarity('wizard mentor'/'USG', 'Common').
card_artist('wizard mentor'/'USG', 'Jeff Miracola').
card_number('wizard mentor'/'USG', '112').
card_flavor_text('wizard mentor'/'USG', 'Although some of the students quickly grasped the concept, the others could summon only blackboards.').
card_multiverse_id('wizard mentor'/'USG', '8907').

card_in_set('worn powerstone', 'USG').
card_original_type('worn powerstone'/'USG', 'Artifact').
card_original_text('worn powerstone'/'USG', 'Worn Powerstone comes into play tapped.\n{T}: Add two colorless mana to your mana pool. Play this ability as a mana source.').
card_first_print('worn powerstone', 'USG').
card_image_name('worn powerstone'/'USG', 'worn powerstone').
card_uid('worn powerstone'/'USG', 'USG:Worn Powerstone:worn powerstone').
card_rarity('worn powerstone'/'USG', 'Uncommon').
card_artist('worn powerstone'/'USG', 'Henry G. Higgenbotham').
card_number('worn powerstone'/'USG', '318').
card_multiverse_id('worn powerstone'/'USG', '8876').

card_in_set('worship', 'USG').
card_original_type('worship'/'USG', 'Enchantment').
card_original_text('worship'/'USG', 'Damage that would reduce your life total to less than 1 instead reduces it to 1 if you control a creature.').
card_first_print('worship', 'USG').
card_image_name('worship'/'USG', 'worship').
card_uid('worship'/'USG', 'USG:Worship:worship').
card_rarity('worship'/'USG', 'Rare').
card_artist('worship'/'USG', 'Mark Zug').
card_number('worship'/'USG', '57').
card_flavor_text('worship'/'USG', '\"Believe in the ideal, not the idol.\"\n—Serra').
card_multiverse_id('worship'/'USG', '5576').

card_in_set('yawgmoth\'s edict', 'USG').
card_original_type('yawgmoth\'s edict'/'USG', 'Enchantment').
card_original_text('yawgmoth\'s edict'/'USG', 'Whenever one of your opponents successfully casts a white spell, that player loses 1 life and you gain 1 life.').
card_first_print('yawgmoth\'s edict', 'USG').
card_image_name('yawgmoth\'s edict'/'USG', 'yawgmoth\'s edict').
card_uid('yawgmoth\'s edict'/'USG', 'USG:Yawgmoth\'s Edict:yawgmoth\'s edict').
card_rarity('yawgmoth\'s edict'/'USG', 'Uncommon').
card_artist('yawgmoth\'s edict'/'USG', 'Scott Kirschner').
card_number('yawgmoth\'s edict'/'USG', '170').
card_flavor_text('yawgmoth\'s edict'/'USG', '\"Phyrexia\'s purity permits no other.\"\n—Xantcha, Phyrexian outcast').
card_multiverse_id('yawgmoth\'s edict'/'USG', '5696').

card_in_set('yawgmoth\'s will', 'USG').
card_original_type('yawgmoth\'s will'/'USG', 'Sorcery').
card_original_text('yawgmoth\'s will'/'USG', 'Until end of turn, you may play cards in your graveyard as though they were in your hand. Cards put into your graveyard this turn are removed from the game instead.').
card_image_name('yawgmoth\'s will'/'USG', 'yawgmoth\'s will').
card_uid('yawgmoth\'s will'/'USG', 'USG:Yawgmoth\'s Will:yawgmoth\'s will').
card_rarity('yawgmoth\'s will'/'USG', 'Rare').
card_artist('yawgmoth\'s will'/'USG', 'Ron Spencer').
card_number('yawgmoth\'s will'/'USG', '171').
card_multiverse_id('yawgmoth\'s will'/'USG', '5629').

card_in_set('zephid', 'USG').
card_original_type('zephid'/'USG', 'Summon — Illusion').
card_original_text('zephid'/'USG', 'Flying\nZephid cannot be the target of spells or abilities.').
card_first_print('zephid', 'USG').
card_image_name('zephid'/'USG', 'zephid').
card_uid('zephid'/'USG', 'USG:Zephid:zephid').
card_rarity('zephid'/'USG', 'Rare').
card_artist('zephid'/'USG', 'Daren Bader').
card_number('zephid'/'USG', '113').
card_flavor_text('zephid'/'USG', 'Once you\'ve seen one, you\'ll understand why spells won\'t go near them.').
card_multiverse_id('zephid'/'USG', '5825').

card_in_set('zephid\'s embrace', 'USG').
card_original_type('zephid\'s embrace'/'USG', 'Enchant Creature').
card_original_text('zephid\'s embrace'/'USG', 'Enchanted creature gets +2/+2, gains flying, and cannot be the target of spells or abilities.').
card_first_print('zephid\'s embrace', 'USG').
card_image_name('zephid\'s embrace'/'USG', 'zephid\'s embrace').
card_uid('zephid\'s embrace'/'USG', 'USG:Zephid\'s Embrace:zephid\'s embrace').
card_rarity('zephid\'s embrace'/'USG', 'Uncommon').
card_artist('zephid\'s embrace'/'USG', 'Daren Bader').
card_number('zephid\'s embrace'/'USG', '114').
card_flavor_text('zephid\'s embrace'/'USG', 'Spells will shun you, as will everyone else.').
card_multiverse_id('zephid\'s embrace'/'USG', '8787').
