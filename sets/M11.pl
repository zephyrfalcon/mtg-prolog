% Magic 2011

set('M11').
set_name('M11', 'Magic 2011').
set_release_date('M11', '2010-07-16').
set_border('M11', 'black').
set_type('M11', 'core').

card_in_set('acidic slime', 'M11').
card_original_type('acidic slime'/'M11', 'Creature — Ooze').
card_original_text('acidic slime'/'M11', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'M11', 'acidic slime').
card_uid('acidic slime'/'M11', 'M11:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'M11', 'Uncommon').
card_artist('acidic slime'/'M11', 'Karl Kopinski').
card_number('acidic slime'/'M11', '161').
card_multiverse_id('acidic slime'/'M11', '207333').

card_in_set('act of treason', 'M11').
card_original_type('act of treason'/'M11', 'Sorcery').
card_original_text('act of treason'/'M11', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('act of treason'/'M11', 'act of treason').
card_uid('act of treason'/'M11', 'M11:Act of Treason:act of treason').
card_rarity('act of treason'/'M11', 'Common').
card_artist('act of treason'/'M11', 'Eric Deschamps').
card_number('act of treason'/'M11', '121').
card_flavor_text('act of treason'/'M11', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').
card_multiverse_id('act of treason'/'M11', '205072').

card_in_set('æther adept', 'M11').
card_original_type('æther adept'/'M11', 'Creature — Human Wizard').
card_original_text('æther adept'/'M11', 'When Æther Adept enters the battlefield, return target creature to its owner\'s hand.').
card_first_print('æther adept', 'M11').
card_image_name('æther adept'/'M11', 'aether adept').
card_uid('æther adept'/'M11', 'M11:Æther Adept:aether adept').
card_rarity('æther adept'/'M11', 'Common').
card_artist('æther adept'/'M11', 'Eric Deschamps').
card_number('æther adept'/'M11', '41').
card_flavor_text('æther adept'/'M11', 'Some mages do their best work in solitude. Others do their best work creating it.').
card_multiverse_id('æther adept'/'M11', '205020').

card_in_set('air servant', 'M11').
card_original_type('air servant'/'M11', 'Creature — Elemental').
card_original_text('air servant'/'M11', 'Flying\n{2}{U}: Tap target creature with flying.').
card_first_print('air servant', 'M11').
card_image_name('air servant'/'M11', 'air servant').
card_uid('air servant'/'M11', 'M11:Air Servant:air servant').
card_rarity('air servant'/'M11', 'Uncommon').
card_artist('air servant'/'M11', 'Lars Grant-West').
card_number('air servant'/'M11', '42').
card_flavor_text('air servant'/'M11', '\"Wind is forceful, yet ephemeral. It can knock a dragon out of the sky, yet pass through the smallest crack unhindered.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('air servant'/'M11', '205070').

card_in_set('ajani goldmane', 'M11').
card_original_type('ajani goldmane'/'M11', 'Planeswalker — Ajani').
card_original_text('ajani goldmane'/'M11', '+1: You gain 2 life.\n-1: Put a +1/+1 counter on each creature you control. Those creatures gain vigilance until end of turn.\n-6: Put a white Avatar creature token onto the battlefield. It has \"This creature\'s power and toughness are each equal to your life total.\"').
card_image_name('ajani goldmane'/'M11', 'ajani goldmane').
card_uid('ajani goldmane'/'M11', 'M11:Ajani Goldmane:ajani goldmane').
card_rarity('ajani goldmane'/'M11', 'Mythic Rare').
card_artist('ajani goldmane'/'M11', 'Aleksi Briclot').
card_number('ajani goldmane'/'M11', '1').
card_multiverse_id('ajani goldmane'/'M11', '205957').

card_in_set('ajani\'s mantra', 'M11').
card_original_type('ajani\'s mantra'/'M11', 'Enchantment').
card_original_text('ajani\'s mantra'/'M11', 'At the beginning of your upkeep, you may gain 1 life.').
card_first_print('ajani\'s mantra', 'M11').
card_image_name('ajani\'s mantra'/'M11', 'ajani\'s mantra').
card_uid('ajani\'s mantra'/'M11', 'M11:Ajani\'s Mantra:ajani\'s mantra').
card_rarity('ajani\'s mantra'/'M11', 'Common').
card_artist('ajani\'s mantra'/'M11', 'James Paick').
card_number('ajani\'s mantra'/'M11', '2').
card_flavor_text('ajani\'s mantra'/'M11', '\"He hasn\'t returned to the Cloud Forest. But I can still sense his calming presence.\"\n—Zaliki of Naya').
card_multiverse_id('ajani\'s mantra'/'M11', '204987').

card_in_set('ajani\'s pridemate', 'M11').
card_original_type('ajani\'s pridemate'/'M11', 'Creature — Cat Soldier').
card_original_text('ajani\'s pridemate'/'M11', 'Whenever you gain life, you may put a +1/+1 counter on Ajani\'s Pridemate. (For example, if an effect causes you to gain 3 life, you may put one +1/+1 counter on this creature.)').
card_first_print('ajani\'s pridemate', 'M11').
card_image_name('ajani\'s pridemate'/'M11', 'ajani\'s pridemate').
card_uid('ajani\'s pridemate'/'M11', 'M11:Ajani\'s Pridemate:ajani\'s pridemate').
card_rarity('ajani\'s pridemate'/'M11', 'Uncommon').
card_artist('ajani\'s pridemate'/'M11', 'Svetlin Velinov').
card_number('ajani\'s pridemate'/'M11', '3').
card_flavor_text('ajani\'s pridemate'/'M11', '\"When one of us prospers, the pride prospers.\"\n—Jazal Goldmane').
card_multiverse_id('ajani\'s pridemate'/'M11', '205065').

card_in_set('alluring siren', 'M11').
card_original_type('alluring siren'/'M11', 'Creature — Siren').
card_original_text('alluring siren'/'M11', '{T}: Target creature an opponent controls attacks you this turn if able.').
card_image_name('alluring siren'/'M11', 'alluring siren').
card_uid('alluring siren'/'M11', 'M11:Alluring Siren:alluring siren').
card_rarity('alluring siren'/'M11', 'Uncommon').
card_artist('alluring siren'/'M11', 'Chippy').
card_number('alluring siren'/'M11', '43').
card_flavor_text('alluring siren'/'M11', '\"The ground polluted floats with human gore,\nAnd human carnage taints the dreadful shore\nFly swift the dangerous coast: let every ear\nBe stopp\'d against the song! \'tis death to hear!\"\n—Homer, The Odyssey, trans. Pope').
card_multiverse_id('alluring siren'/'M11', '205073').

card_in_set('ancient hellkite', 'M11').
card_original_type('ancient hellkite'/'M11', 'Creature — Dragon').
card_original_text('ancient hellkite'/'M11', 'Flying\n{R}: Ancient Hellkite deals 1 damage to target creature defending player controls. Activate this ability only if Ancient Hellkite is attacking.').
card_image_name('ancient hellkite'/'M11', 'ancient hellkite').
card_uid('ancient hellkite'/'M11', 'M11:Ancient Hellkite:ancient hellkite').
card_rarity('ancient hellkite'/'M11', 'Rare').
card_artist('ancient hellkite'/'M11', 'Jason Chan').
card_number('ancient hellkite'/'M11', '122').
card_flavor_text('ancient hellkite'/'M11', 'When a dragon attacks, there are no bystanders.').
card_multiverse_id('ancient hellkite'/'M11', '204980').

card_in_set('angel\'s feather', 'M11').
card_original_type('angel\'s feather'/'M11', 'Artifact').
card_original_text('angel\'s feather'/'M11', 'Whenever a player casts a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'M11', 'angel\'s feather').
card_uid('angel\'s feather'/'M11', 'M11:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'M11', 'Uncommon').
card_artist('angel\'s feather'/'M11', 'Alan Pollack').
card_number('angel\'s feather'/'M11', '201').
card_flavor_text('angel\'s feather'/'M11', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'M11', '206325').

card_in_set('angelic arbiter', 'M11').
card_original_type('angelic arbiter'/'M11', 'Creature — Angel').
card_original_text('angelic arbiter'/'M11', 'Flying\nEach opponent who cast a spell this turn can\'t attack with creatures.\nEach opponent who attacked with a creature this turn can\'t cast spells.').
card_first_print('angelic arbiter', 'M11').
card_image_name('angelic arbiter'/'M11', 'angelic arbiter').
card_uid('angelic arbiter'/'M11', 'M11:Angelic Arbiter:angelic arbiter').
card_rarity('angelic arbiter'/'M11', 'Rare').
card_artist('angelic arbiter'/'M11', 'Steve Argyle').
card_number('angelic arbiter'/'M11', '4').
card_multiverse_id('angelic arbiter'/'M11', '204975').

card_in_set('arc runner', 'M11').
card_original_type('arc runner'/'M11', 'Creature — Elemental Ox').
card_original_text('arc runner'/'M11', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nAt the beginning of the end step, sacrifice Arc Runner.').
card_first_print('arc runner', 'M11').
card_image_name('arc runner'/'M11', 'arc runner').
card_uid('arc runner'/'M11', 'M11:Arc Runner:arc runner').
card_rarity('arc runner'/'M11', 'Common').
card_artist('arc runner'/'M11', 'Nils Hamm').
card_number('arc runner'/'M11', '123').
card_flavor_text('arc runner'/'M11', 'The storms of the wastelands form quickly and hit hard. Few have anything to do with rain.').
card_multiverse_id('arc runner'/'M11', '204991').

card_in_set('armored ascension', 'M11').
card_original_type('armored ascension'/'M11', 'Enchantment — Aura').
card_original_text('armored ascension'/'M11', 'Enchant creature\nEnchanted creature gets +1/+1 for each Plains you control and has flying.').
card_image_name('armored ascension'/'M11', 'armored ascension').
card_uid('armored ascension'/'M11', 'M11:Armored Ascension:armored ascension').
card_rarity('armored ascension'/'M11', 'Uncommon').
card_artist('armored ascension'/'M11', 'Jesper Ejsing').
card_number('armored ascension'/'M11', '5').
card_flavor_text('armored ascension'/'M11', 'With skill and strength honed upon the prairie, Naxtil won his battles high above it.').
card_multiverse_id('armored ascension'/'M11', '204997').

card_in_set('armored cancrix', 'M11').
card_original_type('armored cancrix'/'M11', 'Creature — Crab').
card_original_text('armored cancrix'/'M11', '').
card_first_print('armored cancrix', 'M11').
card_image_name('armored cancrix'/'M11', 'armored cancrix').
card_uid('armored cancrix'/'M11', 'M11:Armored Cancrix:armored cancrix').
card_rarity('armored cancrix'/'M11', 'Common').
card_artist('armored cancrix'/'M11', 'Tomasz Jedruszek').
card_number('armored cancrix'/'M11', '44').
card_flavor_text('armored cancrix'/'M11', 'Creatures displaced from time still turn up every year, stranded by the temporal disaster that once swept across Dominaria.').
card_multiverse_id('armored cancrix'/'M11', '205083').

card_in_set('assassinate', 'M11').
card_original_type('assassinate'/'M11', 'Sorcery').
card_original_text('assassinate'/'M11', 'Destroy target tapped creature.').
card_image_name('assassinate'/'M11', 'assassinate').
card_uid('assassinate'/'M11', 'M11:Assassinate:assassinate').
card_rarity('assassinate'/'M11', 'Common').
card_artist('assassinate'/'M11', 'Kev Walker').
card_number('assassinate'/'M11', '81').
card_flavor_text('assassinate'/'M11', '\"This is how wars are won—not with armies of soldiers but with a single knife blade, artfully placed.\"\n—Yurin, royal assassin').
card_multiverse_id('assassinate'/'M11', '205217').

card_in_set('assault griffin', 'M11').
card_original_type('assault griffin'/'M11', 'Creature — Griffin').
card_original_text('assault griffin'/'M11', 'Flying').
card_first_print('assault griffin', 'M11').
card_image_name('assault griffin'/'M11', 'assault griffin').
card_uid('assault griffin'/'M11', 'M11:Assault Griffin:assault griffin').
card_rarity('assault griffin'/'M11', 'Common').
card_artist('assault griffin'/'M11', 'Jesper Ejsing').
card_number('assault griffin'/'M11', '6').
card_flavor_text('assault griffin'/'M11', '\"Fine soldiers guarded the northern front. They waited for two-legged foes and left the skies unheeded.\"\n—General Avitora').
card_multiverse_id('assault griffin'/'M11', '208296').

card_in_set('augury owl', 'M11').
card_original_type('augury owl'/'M11', 'Creature — Bird').
card_original_text('augury owl'/'M11', 'Flying\nWhen Augury Owl enters the battlefield, scry 3. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('augury owl', 'M11').
card_image_name('augury owl'/'M11', 'augury owl').
card_uid('augury owl'/'M11', 'M11:Augury Owl:augury owl').
card_rarity('augury owl'/'M11', 'Common').
card_artist('augury owl'/'M11', 'Jim Nelson').
card_number('augury owl'/'M11', '45').
card_multiverse_id('augury owl'/'M11', '208225').

card_in_set('autumn\'s veil', 'M11').
card_original_type('autumn\'s veil'/'M11', 'Instant').
card_original_text('autumn\'s veil'/'M11', 'Spells you control can\'t be countered by blue or black spells this turn, and creatures you control can\'t be the targets of blue or black spells this turn.').
card_first_print('autumn\'s veil', 'M11').
card_image_name('autumn\'s veil'/'M11', 'autumn\'s veil').
card_uid('autumn\'s veil'/'M11', 'M11:Autumn\'s Veil:autumn\'s veil').
card_rarity('autumn\'s veil'/'M11', 'Uncommon').
card_artist('autumn\'s veil'/'M11', 'Kekai Kotaki').
card_number('autumn\'s veil'/'M11', '162').
card_flavor_text('autumn\'s veil'/'M11', 'The rustling of leaves and a passing shadow are a dryad\'s only trace.').
card_multiverse_id('autumn\'s veil'/'M11', '205051').

card_in_set('awakener druid', 'M11').
card_original_type('awakener druid'/'M11', 'Creature — Human Druid').
card_original_text('awakener druid'/'M11', 'When Awakener Druid enters the battlefield, target Forest becomes a 4/5 green Treefolk creature for as long as Awakener Druid is on the battlefield. It\'s still a land.').
card_image_name('awakener druid'/'M11', 'awakener druid').
card_uid('awakener druid'/'M11', 'M11:Awakener Druid:awakener druid').
card_rarity('awakener druid'/'M11', 'Uncommon').
card_artist('awakener druid'/'M11', 'Jason Chan').
card_number('awakener druid'/'M11', '163').
card_flavor_text('awakener druid'/'M11', 'The druids of Mazar treat each seedcone with respect for the warrior it will become.').
card_multiverse_id('awakener druid'/'M11', '205076').

card_in_set('azure drake', 'M11').
card_original_type('azure drake'/'M11', 'Creature — Drake').
card_original_text('azure drake'/'M11', 'Flying').
card_image_name('azure drake'/'M11', 'azure drake').
card_uid('azure drake'/'M11', 'M11:Azure Drake:azure drake').
card_rarity('azure drake'/'M11', 'Common').
card_artist('azure drake'/'M11', 'Janine Johnston').
card_number('azure drake'/'M11', '46').
card_flavor_text('azure drake'/'M11', 'More so than \"storm\" or \"pirate,\" \"drake\" is the word a sailor least wants to hear.').
card_multiverse_id('azure drake'/'M11', '204984').

card_in_set('back to nature', 'M11').
card_original_type('back to nature'/'M11', 'Instant').
card_original_text('back to nature'/'M11', 'Destroy all enchantments.').
card_first_print('back to nature', 'M11').
card_image_name('back to nature'/'M11', 'back to nature').
card_uid('back to nature'/'M11', 'M11:Back to Nature:back to nature').
card_rarity('back to nature'/'M11', 'Uncommon').
card_artist('back to nature'/'M11', 'Howard Lyon').
card_number('back to nature'/'M11', '164').
card_flavor_text('back to nature'/'M11', '\"Nature is a mutable cloud which is always and never the same.\"\n—Ralph Waldo Emerson, Essays').
card_multiverse_id('back to nature'/'M11', '208284').

card_in_set('baneslayer angel', 'M11').
card_original_type('baneslayer angel'/'M11', 'Creature — Angel').
card_original_text('baneslayer angel'/'M11', 'Flying, first strike, lifelink, protection from Demons and from Dragons').
card_image_name('baneslayer angel'/'M11', 'baneslayer angel').
card_uid('baneslayer angel'/'M11', 'M11:Baneslayer Angel:baneslayer angel').
card_rarity('baneslayer angel'/'M11', 'Mythic Rare').
card_artist('baneslayer angel'/'M11', 'Greg Staples').
card_number('baneslayer angel'/'M11', '7').
card_flavor_text('baneslayer angel'/'M11', 'Some angels protect the meek and innocent. Others seek out and smite evil wherever it lurks.').
card_multiverse_id('baneslayer angel'/'M11', '205077').

card_in_set('barony vampire', 'M11').
card_original_type('barony vampire'/'M11', 'Creature — Vampire').
card_original_text('barony vampire'/'M11', '').
card_first_print('barony vampire', 'M11').
card_image_name('barony vampire'/'M11', 'barony vampire').
card_uid('barony vampire'/'M11', 'M11:Barony Vampire:barony vampire').
card_rarity('barony vampire'/'M11', 'Common').
card_artist('barony vampire'/'M11', 'Daarken').
card_number('barony vampire'/'M11', '82').
card_flavor_text('barony vampire'/'M11', '\"Poor little sun-dweller out past curfew. And to think, you might have survived if it wasn\'t so close to suppertime.\"').
card_multiverse_id('barony vampire'/'M11', '205001').

card_in_set('berserkers of blood ridge', 'M11').
card_original_type('berserkers of blood ridge'/'M11', 'Creature — Human Berserker').
card_original_text('berserkers of blood ridge'/'M11', 'Berserkers of Blood Ridge attacks each turn if able.').
card_image_name('berserkers of blood ridge'/'M11', 'berserkers of blood ridge').
card_uid('berserkers of blood ridge'/'M11', 'M11:Berserkers of Blood Ridge:berserkers of blood ridge').
card_rarity('berserkers of blood ridge'/'M11', 'Common').
card_artist('berserkers of blood ridge'/'M11', 'Karl Kopinski').
card_number('berserkers of blood ridge'/'M11', '124').
card_flavor_text('berserkers of blood ridge'/'M11', 'They take a blood oath to die in battle. So far none have failed to fulfill the promise.').
card_multiverse_id('berserkers of blood ridge'/'M11', '205053').

card_in_set('birds of paradise', 'M11').
card_original_type('birds of paradise'/'M11', 'Creature — Bird').
card_original_text('birds of paradise'/'M11', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'M11', 'birds of paradise').
card_uid('birds of paradise'/'M11', 'M11:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'M11', 'Rare').
card_artist('birds of paradise'/'M11', 'Marcelo Vignali').
card_number('birds of paradise'/'M11', '165').
card_flavor_text('birds of paradise'/'M11', '\"The gods used their feathers to paint all the colors of the world.\"\n—Yare-Tiva, warden of Gramur forest').
card_multiverse_id('birds of paradise'/'M11', '207334').

card_in_set('black knight', 'M11').
card_original_type('black knight'/'M11', 'Creature — Human Knight').
card_original_text('black knight'/'M11', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from white (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything white.)').
card_image_name('black knight'/'M11', 'black knight').
card_uid('black knight'/'M11', 'M11:Black Knight:black knight').
card_rarity('black knight'/'M11', 'Uncommon').
card_artist('black knight'/'M11', 'Christopher Moeller').
card_number('black knight'/'M11', '83').
card_multiverse_id('black knight'/'M11', '205218').

card_in_set('blinding mage', 'M11').
card_original_type('blinding mage'/'M11', 'Creature — Human Wizard').
card_original_text('blinding mage'/'M11', '{W}, {T}: Tap target creature.').
card_image_name('blinding mage'/'M11', 'blinding mage').
card_uid('blinding mage'/'M11', 'M11:Blinding Mage:blinding mage').
card_rarity('blinding mage'/'M11', 'Common').
card_artist('blinding mage'/'M11', 'Eric Deschamps').
card_number('blinding mage'/'M11', '8').
card_flavor_text('blinding mage'/'M11', '\"I carry the light of truth. Do not pity those it blinds, for they never had eyes to see.\"').
card_multiverse_id('blinding mage'/'M11', '205219').

card_in_set('blood tithe', 'M11').
card_original_type('blood tithe'/'M11', 'Sorcery').
card_original_text('blood tithe'/'M11', 'Each opponent loses 3 life. You gain life equal to the life lost this way.').
card_first_print('blood tithe', 'M11').
card_image_name('blood tithe'/'M11', 'blood tithe').
card_uid('blood tithe'/'M11', 'M11:Blood Tithe:blood tithe').
card_rarity('blood tithe'/'M11', 'Common').
card_artist('blood tithe'/'M11', 'Robh Ruppel').
card_number('blood tithe'/'M11', '84').
card_flavor_text('blood tithe'/'M11', 'The Crimson Throne may be empty, but its coffers are full.').
card_multiverse_id('blood tithe'/'M11', '205054').

card_in_set('bloodcrazed goblin', 'M11').
card_original_type('bloodcrazed goblin'/'M11', 'Creature — Goblin Berserker').
card_original_text('bloodcrazed goblin'/'M11', 'Bloodcrazed Goblin can\'t attack unless an opponent has been dealt damage this turn.').
card_first_print('bloodcrazed goblin', 'M11').
card_image_name('bloodcrazed goblin'/'M11', 'bloodcrazed goblin').
card_uid('bloodcrazed goblin'/'M11', 'M11:Bloodcrazed Goblin:bloodcrazed goblin').
card_rarity('bloodcrazed goblin'/'M11', 'Common').
card_artist('bloodcrazed goblin'/'M11', 'Steve Prescott').
card_number('bloodcrazed goblin'/'M11', '125').
card_flavor_text('bloodcrazed goblin'/'M11', '\"A single drop of blood draws their attention, then they strip your bones clean before you have time to die.\"\n—Jamias, hermit of Telfer Peak').
card_multiverse_id('bloodcrazed goblin'/'M11', '205007').

card_in_set('bloodthrone vampire', 'M11').
card_original_type('bloodthrone vampire'/'M11', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'M11', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_image_name('bloodthrone vampire'/'M11', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'M11', 'M11:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'M11', 'Common').
card_artist('bloodthrone vampire'/'M11', 'Steve Argyle').
card_number('bloodthrone vampire'/'M11', '85').
card_flavor_text('bloodthrone vampire'/'M11', '\"The underclass often forget that they are not tenants, or servants, but property.\"').
card_multiverse_id('bloodthrone vampire'/'M11', '205238').

card_in_set('bog raiders', 'M11').
card_original_type('bog raiders'/'M11', 'Creature — Zombie').
card_original_text('bog raiders'/'M11', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('bog raiders'/'M11', 'bog raiders').
card_uid('bog raiders'/'M11', 'M11:Bog Raiders:bog raiders').
card_rarity('bog raiders'/'M11', 'Common').
card_artist('bog raiders'/'M11', 'Carl Critchlow').
card_number('bog raiders'/'M11', '86').
card_flavor_text('bog raiders'/'M11', 'Zombies are the perfect marshland fighters—they don\'t need to breathe and they don\'t care what they step in.').
card_multiverse_id('bog raiders'/'M11', '207105').

card_in_set('brindle boar', 'M11').
card_original_type('brindle boar'/'M11', 'Creature — Boar').
card_original_text('brindle boar'/'M11', 'Sacrifice Brindle Boar: You gain 4 life.').
card_first_print('brindle boar', 'M11').
card_image_name('brindle boar'/'M11', 'brindle boar').
card_uid('brindle boar'/'M11', 'M11:Brindle Boar:brindle boar').
card_rarity('brindle boar'/'M11', 'Common').
card_artist('brindle boar'/'M11', 'Dave Allsop').
card_number('brindle boar'/'M11', '166').
card_flavor_text('brindle boar'/'M11', 'The war lasted for generations. The boars didn\'t need to hunt for food anymore. They fed on the fallen corpses, and the living fed off of them.').
card_multiverse_id('brindle boar'/'M11', '205039').

card_in_set('brittle effigy', 'M11').
card_original_type('brittle effigy'/'M11', 'Artifact').
card_original_text('brittle effigy'/'M11', '{4}, {T}, Exile Brittle Effigy: Exile target creature.').
card_first_print('brittle effigy', 'M11').
card_image_name('brittle effigy'/'M11', 'brittle effigy').
card_uid('brittle effigy'/'M11', 'M11:Brittle Effigy:brittle effigy').
card_rarity('brittle effigy'/'M11', 'Rare').
card_artist('brittle effigy'/'M11', 'John Avon').
card_number('brittle effigy'/'M11', '202').
card_flavor_text('brittle effigy'/'M11', '\"In my early experiments in phylactery, I found that fragile forms have their uses.\"\n—Rocati, Duke of Martyne').
card_multiverse_id('brittle effigy'/'M11', '205002').

card_in_set('call to mind', 'M11').
card_original_type('call to mind'/'M11', 'Sorcery').
card_original_text('call to mind'/'M11', 'Return target instant or sorcery card from your graveyard to your hand.').
card_first_print('call to mind', 'M11').
card_image_name('call to mind'/'M11', 'call to mind').
card_uid('call to mind'/'M11', 'M11:Call to Mind:call to mind').
card_rarity('call to mind'/'M11', 'Uncommon').
card_artist('call to mind'/'M11', 'Terese Nielsen').
card_number('call to mind'/'M11', '47').
card_flavor_text('call to mind'/'M11', '\"It\'s hard to say which is more satisfying: the search for that missing piece or fitting that piece into place.\"\n—Evo Ragus').
card_multiverse_id('call to mind'/'M11', '208218').

card_in_set('cancel', 'M11').
card_original_type('cancel'/'M11', 'Instant').
card_original_text('cancel'/'M11', 'Counter target spell.').
card_image_name('cancel'/'M11', 'cancel').
card_uid('cancel'/'M11', 'M11:Cancel:cancel').
card_rarity('cancel'/'M11', 'Common').
card_artist('cancel'/'M11', 'David Palumbo').
card_number('cancel'/'M11', '48').
card_multiverse_id('cancel'/'M11', '208217').

card_in_set('canyon minotaur', 'M11').
card_original_type('canyon minotaur'/'M11', 'Creature — Minotaur Warrior').
card_original_text('canyon minotaur'/'M11', '').
card_image_name('canyon minotaur'/'M11', 'canyon minotaur').
card_uid('canyon minotaur'/'M11', 'M11:Canyon Minotaur:canyon minotaur').
card_rarity('canyon minotaur'/'M11', 'Common').
card_artist('canyon minotaur'/'M11', 'Steve Prescott').
card_number('canyon minotaur'/'M11', '126').
card_flavor_text('canyon minotaur'/'M11', '\"We\'ll scale these cliffs, traverse Brittle Bridge, and then fight our way down the volcanic slopes on the other side.\"\n\"Isn\'t the shortest route through the canyon?\"\n\"Yes.\"\n\"So shouldn\'t we—\"\n\"No.\"').
card_multiverse_id('canyon minotaur'/'M11', '208000').

card_in_set('captivating vampire', 'M11').
card_original_type('captivating vampire'/'M11', 'Creature — Vampire').
card_original_text('captivating vampire'/'M11', 'Other Vampire creatures you control get +1/+1.\nTap five untapped Vampires you control: Gain control of target creature. It becomes a Vampire in addition to its other types.').
card_first_print('captivating vampire', 'M11').
card_image_name('captivating vampire'/'M11', 'captivating vampire').
card_uid('captivating vampire'/'M11', 'M11:Captivating Vampire:captivating vampire').
card_rarity('captivating vampire'/'M11', 'Rare').
card_artist('captivating vampire'/'M11', 'Eric Deschamps').
card_number('captivating vampire'/'M11', '87').
card_multiverse_id('captivating vampire'/'M11', '205041').

card_in_set('celestial purge', 'M11').
card_original_type('celestial purge'/'M11', 'Instant').
card_original_text('celestial purge'/'M11', 'Exile target black or red permanent.').
card_image_name('celestial purge'/'M11', 'celestial purge').
card_uid('celestial purge'/'M11', 'M11:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'M11', 'Uncommon').
card_artist('celestial purge'/'M11', 'David Palumbo').
card_number('celestial purge'/'M11', '9').
card_flavor_text('celestial purge'/'M11', '\"They say only the good die young. Obviously, you are one of the exceptions.\"\n—Delrobah, cleric of Ivora Gate').
card_multiverse_id('celestial purge'/'M11', '208290').

card_in_set('chandra nalaar', 'M11').
card_original_type('chandra nalaar'/'M11', 'Planeswalker — Chandra').
card_original_text('chandra nalaar'/'M11', '+1: Chandra Nalaar deals 1 damage to target player.\n-X: Chandra Nalaar deals X damage to target creature.\n-8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_image_name('chandra nalaar'/'M11', 'chandra nalaar').
card_uid('chandra nalaar'/'M11', 'M11:Chandra Nalaar:chandra nalaar').
card_rarity('chandra nalaar'/'M11', 'Mythic Rare').
card_artist('chandra nalaar'/'M11', 'Aleksi Briclot').
card_number('chandra nalaar'/'M11', '127').
card_multiverse_id('chandra nalaar'/'M11', '205958').

card_in_set('chandra\'s outrage', 'M11').
card_original_type('chandra\'s outrage'/'M11', 'Instant').
card_original_text('chandra\'s outrage'/'M11', 'Chandra\'s Outrage deals 4 damage to target creature and 2 damage to that creature\'s controller.').
card_image_name('chandra\'s outrage'/'M11', 'chandra\'s outrage').
card_uid('chandra\'s outrage'/'M11', 'M11:Chandra\'s Outrage:chandra\'s outrage').
card_rarity('chandra\'s outrage'/'M11', 'Common').
card_artist('chandra\'s outrage'/'M11', 'Christopher Moeller').
card_number('chandra\'s outrage'/'M11', '128').
card_flavor_text('chandra\'s outrage'/'M11', '\"Her mind is an incredible mix of emotion and power. Even if I could grasp it, I couldn\'t hold it for long.\"\n—Jace Beleren, on Chandra Nalaar').
card_multiverse_id('chandra\'s outrage'/'M11', '205071').

card_in_set('chandra\'s spitfire', 'M11').
card_original_type('chandra\'s spitfire'/'M11', 'Creature — Elemental').
card_original_text('chandra\'s spitfire'/'M11', 'Flying\nWhenever an opponent is dealt noncombat damage, Chandra\'s Spitfire gets +3/+0 until end of turn.').
card_first_print('chandra\'s spitfire', 'M11').
card_image_name('chandra\'s spitfire'/'M11', 'chandra\'s spitfire').
card_uid('chandra\'s spitfire'/'M11', 'M11:Chandra\'s Spitfire:chandra\'s spitfire').
card_rarity('chandra\'s spitfire'/'M11', 'Uncommon').
card_artist('chandra\'s spitfire'/'M11', 'Justin Sweet').
card_number('chandra\'s spitfire'/'M11', '129').
card_flavor_text('chandra\'s spitfire'/'M11', '\"I\'ve lit most everything on fire—trees, rocks, even the water. Now it\'s time to burn the clouds.\"').
card_multiverse_id('chandra\'s spitfire'/'M11', '205026').

card_in_set('child of night', 'M11').
card_original_type('child of night'/'M11', 'Creature — Vampire').
card_original_text('child of night'/'M11', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('child of night'/'M11', 'child of night').
card_uid('child of night'/'M11', 'M11:Child of Night:child of night').
card_rarity('child of night'/'M11', 'Common').
card_artist('child of night'/'M11', 'Ash Wood').
card_number('child of night'/'M11', '88').
card_flavor_text('child of night'/'M11', 'A vampire enacts vengeance on the entire world, claiming her debt two tiny pinpricks at a time.').
card_multiverse_id('child of night'/'M11', '205122').

card_in_set('clone', 'M11').
card_original_type('clone'/'M11', 'Creature — Shapeshifter').
card_original_text('clone'/'M11', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_image_name('clone'/'M11', 'clone').
card_uid('clone'/'M11', 'M11:Clone:clone').
card_rarity('clone'/'M11', 'Rare').
card_artist('clone'/'M11', 'Kev Walker').
card_number('clone'/'M11', '49').
card_flavor_text('clone'/'M11', 'The shapeshifter mimics with a twin\'s esteem and a mirror\'s cruelty.').
card_multiverse_id('clone'/'M11', '205220').

card_in_set('cloud crusader', 'M11').
card_original_type('cloud crusader'/'M11', 'Creature — Human Knight').
card_original_text('cloud crusader'/'M11', 'Flying\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_first_print('cloud crusader', 'M11').
card_image_name('cloud crusader'/'M11', 'cloud crusader').
card_uid('cloud crusader'/'M11', 'M11:Cloud Crusader:cloud crusader').
card_rarity('cloud crusader'/'M11', 'Common').
card_artist('cloud crusader'/'M11', 'Aleksi Briclot').
card_number('cloud crusader'/'M11', '10').
card_flavor_text('cloud crusader'/'M11', 'Each crusader bonds with a griffin fledgling while still a child, and the two grow up as siblings.').
card_multiverse_id('cloud crusader'/'M11', '208287').

card_in_set('cloud elemental', 'M11').
card_original_type('cloud elemental'/'M11', 'Creature — Elemental').
card_original_text('cloud elemental'/'M11', 'Flying\nCloud Elemental can block only creatures with flying.').
card_image_name('cloud elemental'/'M11', 'cloud elemental').
card_uid('cloud elemental'/'M11', 'M11:Cloud Elemental:cloud elemental').
card_rarity('cloud elemental'/'M11', 'Common').
card_artist('cloud elemental'/'M11', 'Michael Sutfin').
card_number('cloud elemental'/'M11', '50').
card_flavor_text('cloud elemental'/'M11', '\"The sky teems with just as much life as the forest or the deep seas.\"\n—Hadi Kasten, Calla Dale naturalist').
card_multiverse_id('cloud elemental'/'M11', '208224').

card_in_set('combust', 'M11').
card_original_type('combust'/'M11', 'Instant').
card_original_text('combust'/'M11', 'Combust can\'t be countered by spells or abilities.\nCombust deals 5 damage to target white or blue creature. The damage can\'t be prevented.').
card_first_print('combust', 'M11').
card_image_name('combust'/'M11', 'combust').
card_uid('combust'/'M11', 'M11:Combust:combust').
card_rarity('combust'/'M11', 'Uncommon').
card_artist('combust'/'M11', 'Jaime Jones').
card_number('combust'/'M11', '130').
card_multiverse_id('combust'/'M11', '205011').

card_in_set('condemn', 'M11').
card_original_type('condemn'/'M11', 'Instant').
card_original_text('condemn'/'M11', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_image_name('condemn'/'M11', 'condemn').
card_uid('condemn'/'M11', 'M11:Condemn:condemn').
card_rarity('condemn'/'M11', 'Uncommon').
card_artist('condemn'/'M11', 'Daren Bader').
card_number('condemn'/'M11', '11').
card_flavor_text('condemn'/'M11', '\"No doubt the arbiters would put you away, after all the documents are signed. But I will have justice now.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('condemn'/'M11', '205098').

card_in_set('conundrum sphinx', 'M11').
card_original_type('conundrum sphinx'/'M11', 'Creature — Sphinx').
card_original_text('conundrum sphinx'/'M11', 'Flying\nWhenever Conundrum Sphinx attacks, each player names a card. Then each player reveals the top card of his or her library. If the card a player revealed is the card he or she named, that player puts it into his or her hand. If it\'s not, that player puts it on the bottom of his or her library.').
card_first_print('conundrum sphinx', 'M11').
card_image_name('conundrum sphinx'/'M11', 'conundrum sphinx').
card_uid('conundrum sphinx'/'M11', 'M11:Conundrum Sphinx:conundrum sphinx').
card_rarity('conundrum sphinx'/'M11', 'Rare').
card_artist('conundrum sphinx'/'M11', 'Michael Komarck').
card_number('conundrum sphinx'/'M11', '51').
card_multiverse_id('conundrum sphinx'/'M11', '204982').

card_in_set('corrupt', 'M11').
card_original_type('corrupt'/'M11', 'Sorcery').
card_original_text('corrupt'/'M11', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'M11', 'corrupt').
card_uid('corrupt'/'M11', 'M11:Corrupt:corrupt').
card_rarity('corrupt'/'M11', 'Uncommon').
card_artist('corrupt'/'M11', 'Dave Allsop').
card_number('corrupt'/'M11', '89').
card_flavor_text('corrupt'/'M11', 'The power of the fens sleeps lightly, and always awakens hungry.').
card_multiverse_id('corrupt'/'M11', '207106').

card_in_set('crystal ball', 'M11').
card_original_type('crystal ball'/'M11', 'Artifact').
card_original_text('crystal ball'/'M11', '{1}, {T}: Scry 2. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('crystal ball', 'M11').
card_image_name('crystal ball'/'M11', 'crystal ball').
card_uid('crystal ball'/'M11', 'M11:Crystal Ball:crystal ball').
card_rarity('crystal ball'/'M11', 'Uncommon').
card_artist('crystal ball'/'M11', 'Ron Spencer').
card_number('crystal ball'/'M11', '203').
card_flavor_text('crystal ball'/'M11', 'It glints with arcane truths to those who know how to glimpse them.').
card_multiverse_id('crystal ball'/'M11', '205049').

card_in_set('cudgel troll', 'M11').
card_original_type('cudgel troll'/'M11', 'Creature — Troll').
card_original_text('cudgel troll'/'M11', '{G}: Regenerate Cudgel Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('cudgel troll'/'M11', 'cudgel troll').
card_uid('cudgel troll'/'M11', 'M11:Cudgel Troll:cudgel troll').
card_rarity('cudgel troll'/'M11', 'Uncommon').
card_artist('cudgel troll'/'M11', 'Jesper Ejsing').
card_number('cudgel troll'/'M11', '167').
card_multiverse_id('cudgel troll'/'M11', '205084').

card_in_set('cultivate', 'M11').
card_original_type('cultivate'/'M11', 'Sorcery').
card_original_text('cultivate'/'M11', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('cultivate'/'M11', 'cultivate').
card_uid('cultivate'/'M11', 'M11:Cultivate:cultivate').
card_rarity('cultivate'/'M11', 'Common').
card_artist('cultivate'/'M11', 'Anthony Palumbo').
card_number('cultivate'/'M11', '168').
card_flavor_text('cultivate'/'M11', 'All seeds share a common bond, calling to each other across infinity.').
card_multiverse_id('cultivate'/'M11', '204996').

card_in_set('cyclops gladiator', 'M11').
card_original_type('cyclops gladiator'/'M11', 'Creature — Cyclops Warrior').
card_original_text('cyclops gladiator'/'M11', 'Whenever Cyclops Gladiator attacks, you may have it deal damage equal to its power to target creature defending player controls. If you do, that creature deals damage equal to its power to Cyclops Gladiator.').
card_first_print('cyclops gladiator', 'M11').
card_image_name('cyclops gladiator'/'M11', 'cyclops gladiator').
card_uid('cyclops gladiator'/'M11', 'M11:Cyclops Gladiator:cyclops gladiator').
card_rarity('cyclops gladiator'/'M11', 'Rare').
card_artist('cyclops gladiator'/'M11', 'Kev Walker').
card_number('cyclops gladiator'/'M11', '131').
card_multiverse_id('cyclops gladiator'/'M11', '205107').

card_in_set('dark tutelage', 'M11').
card_original_type('dark tutelage'/'M11', 'Enchantment').
card_original_text('dark tutelage'/'M11', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_first_print('dark tutelage', 'M11').
card_image_name('dark tutelage'/'M11', 'dark tutelage').
card_uid('dark tutelage'/'M11', 'M11:Dark Tutelage:dark tutelage').
card_rarity('dark tutelage'/'M11', 'Rare').
card_artist('dark tutelage'/'M11', 'James Ryman').
card_number('dark tutelage'/'M11', '90').
card_flavor_text('dark tutelage'/'M11', '\"It is a rough road that leads to the heights of greatness.\"\n—Seneca, Epistles, trans. Gummere').
card_multiverse_id('dark tutelage'/'M11', '204979').

card_in_set('day of judgment', 'M11').
card_original_type('day of judgment'/'M11', 'Sorcery').
card_original_text('day of judgment'/'M11', 'Destroy all creatures.').
card_image_name('day of judgment'/'M11', 'day of judgment').
card_uid('day of judgment'/'M11', 'M11:Day of Judgment:day of judgment').
card_rarity('day of judgment'/'M11', 'Rare').
card_artist('day of judgment'/'M11', 'Vincent Proce').
card_number('day of judgment'/'M11', '12').
card_multiverse_id('day of judgment'/'M11', '208297').

card_in_set('deathmark', 'M11').
card_original_type('deathmark'/'M11', 'Sorcery').
card_original_text('deathmark'/'M11', 'Destroy target green or white creature.').
card_image_name('deathmark'/'M11', 'deathmark').
card_uid('deathmark'/'M11', 'M11:Deathmark:deathmark').
card_rarity('deathmark'/'M11', 'Uncommon').
card_artist('deathmark'/'M11', 'Steven Belledin').
card_number('deathmark'/'M11', '91').
card_flavor_text('deathmark'/'M11', 'There are few ways to escape the deathmark: bargaining with a demon, washing in the fabled waters of youth, and of course, death.').
card_multiverse_id('deathmark'/'M11', '207100').

card_in_set('demolish', 'M11').
card_original_type('demolish'/'M11', 'Sorcery').
card_original_text('demolish'/'M11', 'Destroy target artifact or land.').
card_image_name('demolish'/'M11', 'demolish').
card_uid('demolish'/'M11', 'M11:Demolish:demolish').
card_rarity('demolish'/'M11', 'Common').
card_artist('demolish'/'M11', 'John Avon').
card_number('demolish'/'M11', '132').
card_multiverse_id('demolish'/'M11', '208005').

card_in_set('demon of death\'s gate', 'M11').
card_original_type('demon of death\'s gate'/'M11', 'Creature — Demon').
card_original_text('demon of death\'s gate'/'M11', 'You may pay 6 life and sacrifice three black creatures rather than pay Demon of Death\'s Gate\'s mana cost.\nFlying, trample').
card_first_print('demon of death\'s gate', 'M11').
card_image_name('demon of death\'s gate'/'M11', 'demon of death\'s gate').
card_uid('demon of death\'s gate'/'M11', 'M11:Demon of Death\'s Gate:demon of death\'s gate').
card_rarity('demon of death\'s gate'/'M11', 'Mythic Rare').
card_artist('demon of death\'s gate'/'M11', 'Vance Kovacs').
card_number('demon of death\'s gate'/'M11', '92').
card_flavor_text('demon of death\'s gate'/'M11', 'The locks on the cages of the damned are opened with keys of marrow, sinew, and blood.').
card_multiverse_id('demon of death\'s gate'/'M11', '205131').

card_in_set('demon\'s horn', 'M11').
card_original_type('demon\'s horn'/'M11', 'Artifact').
card_original_text('demon\'s horn'/'M11', 'Whenever a player casts a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'M11', 'demon\'s horn').
card_uid('demon\'s horn'/'M11', 'M11:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'M11', 'Uncommon').
card_artist('demon\'s horn'/'M11', 'Alan Pollack').
card_number('demon\'s horn'/'M11', '204').
card_flavor_text('demon\'s horn'/'M11', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'M11', '206326').

card_in_set('destructive force', 'M11').
card_original_type('destructive force'/'M11', 'Sorcery').
card_original_text('destructive force'/'M11', 'Each player sacrifices five lands. Destructive Force deals 5 damage to each creature.').
card_first_print('destructive force', 'M11').
card_image_name('destructive force'/'M11', 'destructive force').
card_uid('destructive force'/'M11', 'M11:Destructive Force:destructive force').
card_rarity('destructive force'/'M11', 'Rare').
card_artist('destructive force'/'M11', 'Jung Park').
card_number('destructive force'/'M11', '133').
card_flavor_text('destructive force'/'M11', 'The end of the world rarely comes quietly.').
card_multiverse_id('destructive force'/'M11', '205046').

card_in_set('diabolic tutor', 'M11').
card_original_type('diabolic tutor'/'M11', 'Sorcery').
card_original_text('diabolic tutor'/'M11', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'M11', 'diabolic tutor').
card_uid('diabolic tutor'/'M11', 'M11:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'M11', 'Uncommon').
card_artist('diabolic tutor'/'M11', 'Greg Staples').
card_number('diabolic tutor'/'M11', '93').
card_flavor_text('diabolic tutor'/'M11', 'The wise always keep an ear open to the whispers of power.').
card_multiverse_id('diabolic tutor'/'M11', '205222').

card_in_set('diminish', 'M11').
card_original_type('diminish'/'M11', 'Instant').
card_original_text('diminish'/'M11', 'Target creature becomes 1/1 until end of turn.').
card_first_print('diminish', 'M11').
card_image_name('diminish'/'M11', 'diminish').
card_uid('diminish'/'M11', 'M11:Diminish:diminish').
card_rarity('diminish'/'M11', 'Common').
card_artist('diminish'/'M11', 'Eric Deschamps').
card_number('diminish'/'M11', '52').
card_flavor_text('diminish'/'M11', '\"‘That was a narrow escape!\' said Alice, a good deal frightened at the sudden change, but very glad to find herself still in existence.\"\n—Lewis Carroll, Alice\'s Adventures in Wonderland').
card_multiverse_id('diminish'/'M11', '208226').

card_in_set('disentomb', 'M11').
card_original_type('disentomb'/'M11', 'Sorcery').
card_original_text('disentomb'/'M11', 'Return target creature card from your graveyard to your hand.').
card_image_name('disentomb'/'M11', 'disentomb').
card_uid('disentomb'/'M11', 'M11:Disentomb:disentomb').
card_rarity('disentomb'/'M11', 'Common').
card_artist('disentomb'/'M11', 'Alex Horley-Orlandelli').
card_number('disentomb'/'M11', '94').
card_flavor_text('disentomb'/'M11', 'The armies of the dead always report well rested.').
card_multiverse_id('disentomb'/'M11', '207101').

card_in_set('doom blade', 'M11').
card_original_type('doom blade'/'M11', 'Instant').
card_original_text('doom blade'/'M11', 'Destroy target nonblack creature.').
card_image_name('doom blade'/'M11', 'doom blade').
card_uid('doom blade'/'M11', 'M11:Doom Blade:doom blade').
card_rarity('doom blade'/'M11', 'Common').
card_artist('doom blade'/'M11', 'Chippy').
card_number('doom blade'/'M11', '95').
card_flavor_text('doom blade'/'M11', 'The void is without substance but cuts like steel.').
card_multiverse_id('doom blade'/'M11', '205088').

card_in_set('dragon\'s claw', 'M11').
card_original_type('dragon\'s claw'/'M11', 'Artifact').
card_original_text('dragon\'s claw'/'M11', 'Whenever a player casts a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'M11', 'dragon\'s claw').
card_uid('dragon\'s claw'/'M11', 'M11:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'M11', 'Uncommon').
card_artist('dragon\'s claw'/'M11', 'Alan Pollack').
card_number('dragon\'s claw'/'M11', '205').
card_flavor_text('dragon\'s claw'/'M11', '\"If there is a fire that never ceases to burn, it surely lies in the maw of a dragon.\"\n—Sarkhan Vol').
card_multiverse_id('dragon\'s claw'/'M11', '206327').

card_in_set('dragonskull summit', 'M11').
card_original_type('dragonskull summit'/'M11', 'Land').
card_original_text('dragonskull summit'/'M11', 'Dragonskull Summit enters the battlefield tapped unless you control a Swamp or a Mountain.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('dragonskull summit'/'M11', 'dragonskull summit').
card_uid('dragonskull summit'/'M11', 'M11:Dragonskull Summit:dragonskull summit').
card_rarity('dragonskull summit'/'M11', 'Rare').
card_artist('dragonskull summit'/'M11', 'Jon Foster').
card_number('dragonskull summit'/'M11', '223').
card_multiverse_id('dragonskull summit'/'M11', '205089').

card_in_set('drowned catacomb', 'M11').
card_original_type('drowned catacomb'/'M11', 'Land').
card_original_text('drowned catacomb'/'M11', 'Drowned Catacomb enters the battlefield tapped unless you control an Island or a Swamp.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('drowned catacomb'/'M11', 'drowned catacomb').
card_uid('drowned catacomb'/'M11', 'M11:Drowned Catacomb:drowned catacomb').
card_rarity('drowned catacomb'/'M11', 'Rare').
card_artist('drowned catacomb'/'M11', 'Dave Kendall').
card_number('drowned catacomb'/'M11', '224').
card_multiverse_id('drowned catacomb'/'M11', '205090').

card_in_set('dryad\'s favor', 'M11').
card_original_type('dryad\'s favor'/'M11', 'Enchantment — Aura').
card_original_text('dryad\'s favor'/'M11', 'Enchant creature\nEnchanted creature has forestwalk. (It\'s unblockable as long as defending player controls a Forest.)').
card_first_print('dryad\'s favor', 'M11').
card_image_name('dryad\'s favor'/'M11', 'dryad\'s favor').
card_uid('dryad\'s favor'/'M11', 'M11:Dryad\'s Favor:dryad\'s favor').
card_rarity('dryad\'s favor'/'M11', 'Common').
card_artist('dryad\'s favor'/'M11', 'Jesper Ejsing').
card_number('dryad\'s favor'/'M11', '169').
card_flavor_text('dryad\'s favor'/'M11', 'She grants knowledge of the ways to traverse the forest\'s invisible connections.').
card_multiverse_id('dryad\'s favor'/'M11', '205109').

card_in_set('duress', 'M11').
card_original_type('duress'/'M11', 'Sorcery').
card_original_text('duress'/'M11', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'M11', 'duress').
card_uid('duress'/'M11', 'M11:Duress:duress').
card_rarity('duress'/'M11', 'Common').
card_artist('duress'/'M11', 'Steven Belledin').
card_number('duress'/'M11', '96').
card_flavor_text('duress'/'M11', '\"Don\'t worry. I\'m not going to deprive you of all your secrets. Just your most precious one.\"\n—Liliana Vess').
card_multiverse_id('duress'/'M11', '205024').

card_in_set('duskdale wurm', 'M11').
card_original_type('duskdale wurm'/'M11', 'Creature — Wurm').
card_original_text('duskdale wurm'/'M11', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('duskdale wurm'/'M11', 'duskdale wurm').
card_uid('duskdale wurm'/'M11', 'M11:Duskdale Wurm:duskdale wurm').
card_rarity('duskdale wurm'/'M11', 'Uncommon').
card_artist('duskdale wurm'/'M11', 'Dan Dos Santos').
card_number('duskdale wurm'/'M11', '170').
card_multiverse_id('duskdale wurm'/'M11', '207335').

card_in_set('earth servant', 'M11').
card_original_type('earth servant'/'M11', 'Creature — Elemental').
card_original_text('earth servant'/'M11', 'Earth Servant gets +0/+1 for each Mountain you control.').
card_first_print('earth servant', 'M11').
card_image_name('earth servant'/'M11', 'earth servant').
card_uid('earth servant'/'M11', 'M11:Earth Servant:earth servant').
card_rarity('earth servant'/'M11', 'Uncommon').
card_artist('earth servant'/'M11', 'Lucas Graciano').
card_number('earth servant'/'M11', '134').
card_flavor_text('earth servant'/'M11', '\"Fire. Air. Water. These elements spread, filling in the spaces they are not. But earth is implacable like a sullen child.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('earth servant'/'M11', '204986').

card_in_set('elite vanguard', 'M11').
card_original_type('elite vanguard'/'M11', 'Creature — Human Soldier').
card_original_text('elite vanguard'/'M11', '').
card_image_name('elite vanguard'/'M11', 'elite vanguard').
card_uid('elite vanguard'/'M11', 'M11:Elite Vanguard:elite vanguard').
card_rarity('elite vanguard'/'M11', 'Uncommon').
card_artist('elite vanguard'/'M11', 'Mark Tedin').
card_number('elite vanguard'/'M11', '13').
card_flavor_text('elite vanguard'/'M11', 'The vanguard is skilled at waging war alone. The enemy is often defeated before its reinforcements reach the front.').
card_multiverse_id('elite vanguard'/'M11', '208291').

card_in_set('elixir of immortality', 'M11').
card_original_type('elixir of immortality'/'M11', 'Artifact').
card_original_text('elixir of immortality'/'M11', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into your library.').
card_first_print('elixir of immortality', 'M11').
card_image_name('elixir of immortality'/'M11', 'elixir of immortality').
card_uid('elixir of immortality'/'M11', 'M11:Elixir of Immortality:elixir of immortality').
card_rarity('elixir of immortality'/'M11', 'Uncommon').
card_artist('elixir of immortality'/'M11', 'Zoltan Boros & Gabor Szikszai').
card_number('elixir of immortality'/'M11', '206').
card_flavor_text('elixir of immortality'/'M11', '\"Bottled life. Not as tasty as I\'m used to, rather stale, but it has the same effect.\"\n—Baron Sengir').
card_multiverse_id('elixir of immortality'/'M11', '205061').

card_in_set('elvish archdruid', 'M11').
card_original_type('elvish archdruid'/'M11', 'Creature — Elf Druid').
card_original_text('elvish archdruid'/'M11', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_image_name('elvish archdruid'/'M11', 'elvish archdruid').
card_uid('elvish archdruid'/'M11', 'M11:Elvish Archdruid:elvish archdruid').
card_rarity('elvish archdruid'/'M11', 'Rare').
card_artist('elvish archdruid'/'M11', 'Karl Kopinski').
card_number('elvish archdruid'/'M11', '171').
card_flavor_text('elvish archdruid'/'M11', 'He knows the name of every elf born in the last four centuries. More importantly, they all know his.').
card_multiverse_id('elvish archdruid'/'M11', '205091').

card_in_set('ember hauler', 'M11').
card_original_type('ember hauler'/'M11', 'Creature — Goblin').
card_original_text('ember hauler'/'M11', '{1}, Sacrifice Ember Hauler: Ember Hauler deals 2 damage to target creature or player.').
card_first_print('ember hauler', 'M11').
card_image_name('ember hauler'/'M11', 'ember hauler').
card_uid('ember hauler'/'M11', 'M11:Ember Hauler:ember hauler').
card_rarity('ember hauler'/'M11', 'Uncommon').
card_artist('ember hauler'/'M11', 'Steve Prescott').
card_number('ember hauler'/'M11', '135').
card_flavor_text('ember hauler'/'M11', 'Flurk\'s crude goblin language didn\'t differentiate between \"I bring the flame\" and \"I am the flame.\"').
card_multiverse_id('ember hauler'/'M11', '208008').

card_in_set('excommunicate', 'M11').
card_original_type('excommunicate'/'M11', 'Sorcery').
card_original_text('excommunicate'/'M11', 'Put target creature on top of its owner\'s library.').
card_image_name('excommunicate'/'M11', 'excommunicate').
card_uid('excommunicate'/'M11', 'M11:Excommunicate:excommunicate').
card_rarity('excommunicate'/'M11', 'Common').
card_artist('excommunicate'/'M11', 'Matt Stewart').
card_number('excommunicate'/'M11', '14').
card_flavor_text('excommunicate'/'M11', '\"Our law prohibits capital punishment. We believe even the gravest offenders have the chance to redeem themselves.\"\n—Torian Sha, soul warden').
card_multiverse_id('excommunicate'/'M11', '208273').

card_in_set('fauna shaman', 'M11').
card_original_type('fauna shaman'/'M11', 'Creature — Elf Shaman').
card_original_text('fauna shaman'/'M11', '{G}, {T}, Discard a creature card: Search your library for a creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('fauna shaman', 'M11').
card_image_name('fauna shaman'/'M11', 'fauna shaman').
card_uid('fauna shaman'/'M11', 'M11:Fauna Shaman:fauna shaman').
card_rarity('fauna shaman'/'M11', 'Rare').
card_artist('fauna shaman'/'M11', 'Steve Prescott').
card_number('fauna shaman'/'M11', '172').
card_flavor_text('fauna shaman'/'M11', 'She wears the talismans of every creature she can evoke.').
card_multiverse_id('fauna shaman'/'M11', '205059').

card_in_set('fiery hellhound', 'M11').
card_original_type('fiery hellhound'/'M11', 'Creature — Elemental Hound').
card_original_text('fiery hellhound'/'M11', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_image_name('fiery hellhound'/'M11', 'fiery hellhound').
card_uid('fiery hellhound'/'M11', 'M11:Fiery Hellhound:fiery hellhound').
card_rarity('fiery hellhound'/'M11', 'Common').
card_artist('fiery hellhound'/'M11', 'Ted Galaday').
card_number('fiery hellhound'/'M11', '136').
card_flavor_text('fiery hellhound'/'M11', '\"I had hoped to instill in it the loyalty of a guard dog, but with fire\'s power comes its unpredictability.\"\n—Maggath, Sardian elementalist').
card_multiverse_id('fiery hellhound'/'M11', '205093').

card_in_set('fire servant', 'M11').
card_original_type('fire servant'/'M11', 'Creature — Elemental').
card_original_text('fire servant'/'M11', 'If a red instant or sorcery spell you control would deal damage, it deals double that damage instead.').
card_first_print('fire servant', 'M11').
card_image_name('fire servant'/'M11', 'fire servant').
card_uid('fire servant'/'M11', 'M11:Fire Servant:fire servant').
card_rarity('fire servant'/'M11', 'Uncommon').
card_artist('fire servant'/'M11', 'Ryan Yee').
card_number('fire servant'/'M11', '137').
card_flavor_text('fire servant'/'M11', '\"Elemental fire shimmers with rose hues and, unlike terrestrial blazes, smolders long after it\'s extinguished.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('fire servant'/'M11', '204992').

card_in_set('fireball', 'M11').
card_original_type('fireball'/'M11', 'Sorcery').
card_original_text('fireball'/'M11', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'M11', 'fireball').
card_uid('fireball'/'M11', 'M11:Fireball:fireball').
card_rarity('fireball'/'M11', 'Uncommon').
card_artist('fireball'/'M11', 'Dave Dorman').
card_number('fireball'/'M11', '138').
card_multiverse_id('fireball'/'M11', '205223').

card_in_set('flashfreeze', 'M11').
card_original_type('flashfreeze'/'M11', 'Instant').
card_original_text('flashfreeze'/'M11', 'Counter target red or green spell.').
card_image_name('flashfreeze'/'M11', 'flashfreeze').
card_uid('flashfreeze'/'M11', 'M11:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'M11', 'Uncommon').
card_artist('flashfreeze'/'M11', 'Brian Despain').
card_number('flashfreeze'/'M11', '53').
card_flavor_text('flashfreeze'/'M11', '\"Your downfall was not your ignorance, your weakness, or your hubris, but your warm blood.\"\n—Heidar, Rimewind master').
card_multiverse_id('flashfreeze'/'M11', '208219').

card_in_set('fling', 'M11').
card_original_type('fling'/'M11', 'Instant').
card_original_text('fling'/'M11', 'As an additional cost to cast Fling, sacrifice a creature.\nFling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('fling'/'M11', 'fling').
card_uid('fling'/'M11', 'M11:Fling:fling').
card_rarity('fling'/'M11', 'Common').
card_artist('fling'/'M11', 'Paolo Parente').
card_number('fling'/'M11', '139').
card_flavor_text('fling'/'M11', 'Sometimes the cannoneer must become the cannonball.').
card_multiverse_id('fling'/'M11', '208001').

card_in_set('fog', 'M11').
card_original_type('fog'/'M11', 'Instant').
card_original_text('fog'/'M11', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'M11', 'fog').
card_uid('fog'/'M11', 'M11:Fog:fog').
card_rarity('fog'/'M11', 'Common').
card_artist('fog'/'M11', 'John Avon').
card_number('fog'/'M11', '173').
card_flavor_text('fog'/'M11', '\"I fear no army or beast, but only the morning fog. Our assault can survive everything else.\"\n—Lord Hilneth').
card_multiverse_id('fog'/'M11', '205064').

card_in_set('foresee', 'M11').
card_original_type('foresee'/'M11', 'Sorcery').
card_original_text('foresee'/'M11', 'Scry 4, then draw two cards. (To scry 4, look at the top four cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('foresee'/'M11', 'foresee').
card_uid('foresee'/'M11', 'M11:Foresee:foresee').
card_rarity('foresee'/'M11', 'Common').
card_artist('foresee'/'M11', 'Ron Spears').
card_number('foresee'/'M11', '54').
card_flavor_text('foresee'/'M11', 'Two eyes to see what could be, two eyes to see what will.').
card_multiverse_id('foresee'/'M11', '205068').

card_in_set('forest', 'M11').
card_original_type('forest'/'M11', 'Basic Land — Forest').
card_original_text('forest'/'M11', 'G').
card_image_name('forest'/'M11', 'forest1').
card_uid('forest'/'M11', 'M11:Forest:forest1').
card_rarity('forest'/'M11', 'Basic Land').
card_artist('forest'/'M11', 'Rob Alexander').
card_number('forest'/'M11', '246').
card_multiverse_id('forest'/'M11', '213628').

card_in_set('forest', 'M11').
card_original_type('forest'/'M11', 'Basic Land — Forest').
card_original_text('forest'/'M11', 'G').
card_image_name('forest'/'M11', 'forest2').
card_uid('forest'/'M11', 'M11:Forest:forest2').
card_rarity('forest'/'M11', 'Basic Land').
card_artist('forest'/'M11', 'John Avon').
card_number('forest'/'M11', '247').
card_multiverse_id('forest'/'M11', '213629').

card_in_set('forest', 'M11').
card_original_type('forest'/'M11', 'Basic Land — Forest').
card_original_text('forest'/'M11', 'G').
card_image_name('forest'/'M11', 'forest3').
card_uid('forest'/'M11', 'M11:Forest:forest3').
card_rarity('forest'/'M11', 'Basic Land').
card_artist('forest'/'M11', 'Steven Belledin').
card_number('forest'/'M11', '248').
card_multiverse_id('forest'/'M11', '213630').

card_in_set('forest', 'M11').
card_original_type('forest'/'M11', 'Basic Land — Forest').
card_original_text('forest'/'M11', 'G').
card_image_name('forest'/'M11', 'forest4').
card_uid('forest'/'M11', 'M11:Forest:forest4').
card_rarity('forest'/'M11', 'Basic Land').
card_artist('forest'/'M11', 'Jim Nelson').
card_number('forest'/'M11', '249').
card_multiverse_id('forest'/'M11', '213631').

card_in_set('frost titan', 'M11').
card_original_type('frost titan'/'M11', 'Creature — Giant').
card_original_text('frost titan'/'M11', 'Whenever Frost Titan becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.\nWhenever Frost Titan enters the battlefield or attacks, tap target permanent. It doesn\'t untap during its controller\'s next untap step.').
card_image_name('frost titan'/'M11', 'frost titan').
card_uid('frost titan'/'M11', 'M11:Frost Titan:frost titan').
card_rarity('frost titan'/'M11', 'Mythic Rare').
card_artist('frost titan'/'M11', 'Mike Bierek').
card_number('frost titan'/'M11', '55').
card_multiverse_id('frost titan'/'M11', '204999').

card_in_set('gaea\'s revenge', 'M11').
card_original_type('gaea\'s revenge'/'M11', 'Creature — Elemental').
card_original_text('gaea\'s revenge'/'M11', 'Gaea\'s Revenge can\'t be countered.\nHaste\nGaea\'s Revenge can\'t be the target of nongreen spells or abilities from nongreen sources.').
card_first_print('gaea\'s revenge', 'M11').
card_image_name('gaea\'s revenge'/'M11', 'gaea\'s revenge').
card_uid('gaea\'s revenge'/'M11', 'M11:Gaea\'s Revenge:gaea\'s revenge').
card_rarity('gaea\'s revenge'/'M11', 'Mythic Rare').
card_artist('gaea\'s revenge'/'M11', 'Kekai Kotaki').
card_number('gaea\'s revenge'/'M11', '174').
card_multiverse_id('gaea\'s revenge'/'M11', '205033').

card_in_set('gargoyle sentinel', 'M11').
card_original_type('gargoyle sentinel'/'M11', 'Artifact Creature — Gargoyle').
card_original_text('gargoyle sentinel'/'M11', 'Defender (This creature can\'t attack.)\n{3}: Until end of turn, Gargoyle Sentinel loses defender and gains flying.').
card_first_print('gargoyle sentinel', 'M11').
card_image_name('gargoyle sentinel'/'M11', 'gargoyle sentinel').
card_uid('gargoyle sentinel'/'M11', 'M11:Gargoyle Sentinel:gargoyle sentinel').
card_rarity('gargoyle sentinel'/'M11', 'Uncommon').
card_artist('gargoyle sentinel'/'M11', 'Drew Baker').
card_number('gargoyle sentinel'/'M11', '207').
card_flavor_text('gargoyle sentinel'/'M11', '\"Do not mistake a quiet rampart for one that is unguarded. Some things stir only when they need to.\"\n—Bonovar, siege commander').
card_multiverse_id('gargoyle sentinel'/'M11', '204985').

card_in_set('garruk wildspeaker', 'M11').
card_original_type('garruk wildspeaker'/'M11', 'Planeswalker — Garruk').
card_original_text('garruk wildspeaker'/'M11', '+1: Untap two target lands.\n-1: Put a 3/3 green Beast creature token onto the battlefield.\n-4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('garruk wildspeaker'/'M11', 'garruk wildspeaker').
card_uid('garruk wildspeaker'/'M11', 'M11:Garruk Wildspeaker:garruk wildspeaker').
card_rarity('garruk wildspeaker'/'M11', 'Mythic Rare').
card_artist('garruk wildspeaker'/'M11', 'Aleksi Briclot').
card_number('garruk wildspeaker'/'M11', '175').
card_multiverse_id('garruk wildspeaker'/'M11', '205959').

card_in_set('garruk\'s companion', 'M11').
card_original_type('garruk\'s companion'/'M11', 'Creature — Beast').
card_original_text('garruk\'s companion'/'M11', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('garruk\'s companion', 'M11').
card_image_name('garruk\'s companion'/'M11', 'garruk\'s companion').
card_uid('garruk\'s companion'/'M11', 'M11:Garruk\'s Companion:garruk\'s companion').
card_rarity('garruk\'s companion'/'M11', 'Common').
card_artist('garruk\'s companion'/'M11', 'Efrem Palacios').
card_number('garruk\'s companion'/'M11', '176').
card_multiverse_id('garruk\'s companion'/'M11', '205025').

card_in_set('garruk\'s packleader', 'M11').
card_original_type('garruk\'s packleader'/'M11', 'Creature — Beast').
card_original_text('garruk\'s packleader'/'M11', 'Whenever another creature with power 3 or greater enters the battlefield under your control, you may draw a card.').
card_first_print('garruk\'s packleader', 'M11').
card_image_name('garruk\'s packleader'/'M11', 'garruk\'s packleader').
card_uid('garruk\'s packleader'/'M11', 'M11:Garruk\'s Packleader:garruk\'s packleader').
card_rarity('garruk\'s packleader'/'M11', 'Uncommon').
card_artist('garruk\'s packleader'/'M11', 'Nils Hamm').
card_number('garruk\'s packleader'/'M11', '177').
card_flavor_text('garruk\'s packleader'/'M11', '\"He has learned much in his long years. And unlike selfish humans, he\'s willing to share.\"\n—Garruk Wildspeaker').
card_multiverse_id('garruk\'s packleader'/'M11', '205060').

card_in_set('giant growth', 'M11').
card_original_type('giant growth'/'M11', 'Instant').
card_original_text('giant growth'/'M11', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'M11', 'giant growth').
card_uid('giant growth'/'M11', 'M11:Giant Growth:giant growth').
card_rarity('giant growth'/'M11', 'Common').
card_artist('giant growth'/'M11', 'Matt Cavotta').
card_number('giant growth'/'M11', '178').
card_multiverse_id('giant growth'/'M11', '205224').

card_in_set('giant spider', 'M11').
card_original_type('giant spider'/'M11', 'Creature — Spider').
card_original_text('giant spider'/'M11', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'M11', 'giant spider').
card_uid('giant spider'/'M11', 'M11:Giant Spider:giant spider').
card_rarity('giant spider'/'M11', 'Common').
card_artist('giant spider'/'M11', 'Randy Gallegos').
card_number('giant spider'/'M11', '179').
card_flavor_text('giant spider'/'M11', 'The giant spider\'s silk naturally resists gravity, almost weaving itself as it blankets the forest canopy.').
card_multiverse_id('giant spider'/'M11', '205225').

card_in_set('glacial fortress', 'M11').
card_original_type('glacial fortress'/'M11', 'Land').
card_original_text('glacial fortress'/'M11', 'Glacial Fortress enters the battlefield tapped unless you control a Plains or an Island.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('glacial fortress'/'M11', 'glacial fortress').
card_uid('glacial fortress'/'M11', 'M11:Glacial Fortress:glacial fortress').
card_rarity('glacial fortress'/'M11', 'Rare').
card_artist('glacial fortress'/'M11', 'Franz Vohwinkel').
card_number('glacial fortress'/'M11', '225').
card_multiverse_id('glacial fortress'/'M11', '205094').

card_in_set('goblin balloon brigade', 'M11').
card_original_type('goblin balloon brigade'/'M11', 'Creature — Goblin Warrior').
card_original_text('goblin balloon brigade'/'M11', '{R}: Goblin Balloon Brigade gains flying until end of turn.').
card_image_name('goblin balloon brigade'/'M11', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'M11', 'M11:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'M11', 'Common').
card_artist('goblin balloon brigade'/'M11', 'Lars Grant-West').
card_number('goblin balloon brigade'/'M11', '140').
card_flavor_text('goblin balloon brigade'/'M11', '\"The enemy is getting too close! Quick! Inflate the toad!\"').
card_multiverse_id('goblin balloon brigade'/'M11', '208010').

card_in_set('goblin chieftain', 'M11').
card_original_type('goblin chieftain'/'M11', 'Creature — Goblin').
card_original_text('goblin chieftain'/'M11', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nOther Goblin creatures you control get +1/+1 and have haste.').
card_image_name('goblin chieftain'/'M11', 'goblin chieftain').
card_uid('goblin chieftain'/'M11', 'M11:Goblin Chieftain:goblin chieftain').
card_rarity('goblin chieftain'/'M11', 'Rare').
card_artist('goblin chieftain'/'M11', 'Sam Wood').
card_number('goblin chieftain'/'M11', '141').
card_flavor_text('goblin chieftain'/'M11', '\"We are goblinkind, heirs to the mountain empires of chieftains past. Rest is death to us, and arson is our call to war.\"').
card_multiverse_id('goblin chieftain'/'M11', '205096').

card_in_set('goblin piker', 'M11').
card_original_type('goblin piker'/'M11', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'M11', '').
card_image_name('goblin piker'/'M11', 'goblin piker').
card_uid('goblin piker'/'M11', 'M11:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'M11', 'Common').
card_artist('goblin piker'/'M11', 'DiTerlizzi').
card_number('goblin piker'/'M11', '142').
card_flavor_text('goblin piker'/'M11', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'M11', '205052').

card_in_set('goblin tunneler', 'M11').
card_original_type('goblin tunneler'/'M11', 'Creature — Goblin Rogue').
card_original_text('goblin tunneler'/'M11', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('goblin tunneler'/'M11', 'goblin tunneler').
card_uid('goblin tunneler'/'M11', 'M11:Goblin Tunneler:goblin tunneler').
card_rarity('goblin tunneler'/'M11', 'Common').
card_artist('goblin tunneler'/'M11', 'Jesper Ejsing').
card_number('goblin tunneler'/'M11', '143').
card_flavor_text('goblin tunneler'/'M11', '\"You never know what\'s going to be in goblin tunnels. Even in the best case, there will still be goblins.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('goblin tunneler'/'M11', '207999').

card_in_set('goldenglow moth', 'M11').
card_original_type('goldenglow moth'/'M11', 'Creature — Insect').
card_original_text('goldenglow moth'/'M11', 'Flying\nWhenever Goldenglow Moth blocks, you may gain 4 life.').
card_image_name('goldenglow moth'/'M11', 'goldenglow moth').
card_uid('goldenglow moth'/'M11', 'M11:Goldenglow Moth:goldenglow moth').
card_rarity('goldenglow moth'/'M11', 'Common').
card_artist('goldenglow moth'/'M11', 'Howard Lyon').
card_number('goldenglow moth'/'M11', '15').
card_flavor_text('goldenglow moth'/'M11', '\"Little moth, fluttering bright,\nWhat luck will you bring tonight?\"\n—Benalish children\'s rhyme').
card_multiverse_id('goldenglow moth'/'M11', '208295').

card_in_set('grave titan', 'M11').
card_original_type('grave titan'/'M11', 'Creature — Giant').
card_original_text('grave titan'/'M11', 'Deathtouch\nWhenever Grave Titan enters the battlefield or attacks, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_image_name('grave titan'/'M11', 'grave titan').
card_uid('grave titan'/'M11', 'M11:Grave Titan:grave titan').
card_rarity('grave titan'/'M11', 'Mythic Rare').
card_artist('grave titan'/'M11', 'Nils Hamm').
card_number('grave titan'/'M11', '97').
card_flavor_text('grave titan'/'M11', 'Death in form and function.').
card_multiverse_id('grave titan'/'M11', '205012').

card_in_set('gravedigger', 'M11').
card_original_type('gravedigger'/'M11', 'Creature — Zombie').
card_original_text('gravedigger'/'M11', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'M11', 'gravedigger').
card_uid('gravedigger'/'M11', 'M11:Gravedigger:gravedigger').
card_rarity('gravedigger'/'M11', 'Common').
card_artist('gravedigger'/'M11', 'Dermot Power').
card_number('gravedigger'/'M11', '98').
card_flavor_text('gravedigger'/'M11', 'Only a thin layer of dirt separates civilization from an undead apocalypse.').
card_multiverse_id('gravedigger'/'M11', '207102').

card_in_set('greater basilisk', 'M11').
card_original_type('greater basilisk'/'M11', 'Creature — Basilisk').
card_original_text('greater basilisk'/'M11', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('greater basilisk', 'M11').
card_image_name('greater basilisk'/'M11', 'greater basilisk').
card_uid('greater basilisk'/'M11', 'M11:Greater Basilisk:greater basilisk').
card_rarity('greater basilisk'/'M11', 'Common').
card_artist('greater basilisk'/'M11', 'James Ryman').
card_number('greater basilisk'/'M11', '180').
card_flavor_text('greater basilisk'/'M11', 'Bone, stone . . . both taste the same to a hungry basilisk.').
card_multiverse_id('greater basilisk'/'M11', '205016').

card_in_set('harbor serpent', 'M11').
card_original_type('harbor serpent'/'M11', 'Creature — Serpent').
card_original_text('harbor serpent'/'M11', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nHarbor Serpent can\'t attack unless there are five or more Islands on the battlefield.').
card_first_print('harbor serpent', 'M11').
card_image_name('harbor serpent'/'M11', 'harbor serpent').
card_uid('harbor serpent'/'M11', 'M11:Harbor Serpent:harbor serpent').
card_rarity('harbor serpent'/'M11', 'Common').
card_artist('harbor serpent'/'M11', 'Daarken').
card_number('harbor serpent'/'M11', '56').
card_flavor_text('harbor serpent'/'M11', 'Like most giant monsters villagers worship as gods, it proves a fickle master.').
card_multiverse_id('harbor serpent'/'M11', '204974').

card_in_set('haunting echoes', 'M11').
card_original_type('haunting echoes'/'M11', 'Sorcery').
card_original_text('haunting echoes'/'M11', 'Exile all cards from target player\'s graveyard other than basic land cards. For each card exiled this way, search that player\'s library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('haunting echoes'/'M11', 'haunting echoes').
card_uid('haunting echoes'/'M11', 'M11:Haunting Echoes:haunting echoes').
card_rarity('haunting echoes'/'M11', 'Rare').
card_artist('haunting echoes'/'M11', 'Nils Hamm').
card_number('haunting echoes'/'M11', '99').
card_multiverse_id('haunting echoes'/'M11', '205005').

card_in_set('hoarding dragon', 'M11').
card_original_type('hoarding dragon'/'M11', 'Creature — Dragon').
card_original_text('hoarding dragon'/'M11', 'Flying\nWhen Hoarding Dragon enters the battlefield, you may search your library for an artifact card, exile it, then shuffle your library.\nWhen Hoarding Dragon is put into a graveyard from the battlefield, you may put the exiled card into its owner\'s hand.').
card_first_print('hoarding dragon', 'M11').
card_image_name('hoarding dragon'/'M11', 'hoarding dragon').
card_uid('hoarding dragon'/'M11', 'M11:Hoarding Dragon:hoarding dragon').
card_rarity('hoarding dragon'/'M11', 'Rare').
card_artist('hoarding dragon'/'M11', 'Matt Cavotta').
card_number('hoarding dragon'/'M11', '144').
card_multiverse_id('hoarding dragon'/'M11', '205235').

card_in_set('holy strength', 'M11').
card_original_type('holy strength'/'M11', 'Enchantment — Aura').
card_original_text('holy strength'/'M11', 'Enchant creature\nEnchanted creature gets +1/+2.').
card_image_name('holy strength'/'M11', 'holy strength').
card_uid('holy strength'/'M11', 'M11:Holy Strength:holy strength').
card_rarity('holy strength'/'M11', 'Common').
card_artist('holy strength'/'M11', 'Terese Nielsen').
card_number('holy strength'/'M11', '16').
card_flavor_text('holy strength'/'M11', '\"Born under the sun, the first child will seek the foundation of honor and be fortified by its righteousness.\"\n—Codex of the Constellari').
card_multiverse_id('holy strength'/'M11', '205226').

card_in_set('honor of the pure', 'M11').
card_original_type('honor of the pure'/'M11', 'Enchantment').
card_original_text('honor of the pure'/'M11', 'White creatures you control get +1/+1.').
card_image_name('honor of the pure'/'M11', 'honor of the pure').
card_uid('honor of the pure'/'M11', 'M11:Honor of the Pure:honor of the pure').
card_rarity('honor of the pure'/'M11', 'Rare').
card_artist('honor of the pure'/'M11', 'Greg Staples').
card_number('honor of the pure'/'M11', '17').
card_flavor_text('honor of the pure'/'M11', 'Together the soldiers were like a golden blade, cutting down their enemies and scarring the darkness.').
card_multiverse_id('honor of the pure'/'M11', '205099').

card_in_set('hornet sting', 'M11').
card_original_type('hornet sting'/'M11', 'Instant').
card_original_text('hornet sting'/'M11', 'Hornet Sting deals 1 damage to target creature or player.').
card_first_print('hornet sting', 'M11').
card_image_name('hornet sting'/'M11', 'hornet sting').
card_uid('hornet sting'/'M11', 'M11:Hornet Sting:hornet sting').
card_rarity('hornet sting'/'M11', 'Common').
card_artist('hornet sting'/'M11', 'Matt Stewart').
card_number('hornet sting'/'M11', '181').
card_flavor_text('hornet sting'/'M11', 'It was only then—to his infinite sorrow—that Gork realized hornets don\'t make honey.').
card_multiverse_id('hornet sting'/'M11', '205050').

card_in_set('howling banshee', 'M11').
card_original_type('howling banshee'/'M11', 'Creature — Spirit').
card_original_text('howling banshee'/'M11', 'Flying\nWhen Howling Banshee enters the battlefield, each player loses 3 life.').
card_image_name('howling banshee'/'M11', 'howling banshee').
card_uid('howling banshee'/'M11', 'M11:Howling Banshee:howling banshee').
card_rarity('howling banshee'/'M11', 'Uncommon').
card_artist('howling banshee'/'M11', 'Andrew Robinson').
card_number('howling banshee'/'M11', '100').
card_flavor_text('howling banshee'/'M11', 'Villagers cloaked the town in magical silence, but their ears still bled.').
card_multiverse_id('howling banshee'/'M11', '205100').

card_in_set('hunters\' feast', 'M11').
card_original_type('hunters\' feast'/'M11', 'Sorcery').
card_original_text('hunters\' feast'/'M11', 'Any number of target players each gain 6 life.').
card_first_print('hunters\' feast', 'M11').
card_image_name('hunters\' feast'/'M11', 'hunters\' feast').
card_uid('hunters\' feast'/'M11', 'M11:Hunters\' Feast:hunters\' feast').
card_rarity('hunters\' feast'/'M11', 'Common').
card_artist('hunters\' feast'/'M11', 'Matt Stewart').
card_number('hunters\' feast'/'M11', '182').
card_flavor_text('hunters\' feast'/'M11', 'Those who kill a jawspur rhino must prepare spells to lift the carcass—even a dozen hunters can\'t lift its bulk.').
card_multiverse_id('hunters\' feast'/'M11', '205037').

card_in_set('ice cage', 'M11').
card_original_type('ice cage'/'M11', 'Enchantment — Aura').
card_original_text('ice cage'/'M11', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhen enchanted creature becomes the target of a spell or ability, destroy Ice Cage.').
card_image_name('ice cage'/'M11', 'ice cage').
card_uid('ice cage'/'M11', 'M11:Ice Cage:ice cage').
card_rarity('ice cage'/'M11', 'Common').
card_artist('ice cage'/'M11', 'Mike Bierek').
card_number('ice cage'/'M11', '57').
card_multiverse_id('ice cage'/'M11', '205101').

card_in_set('incite', 'M11').
card_original_type('incite'/'M11', 'Instant').
card_original_text('incite'/'M11', 'Target creature becomes red until end of turn and attacks this turn if able.').
card_first_print('incite', 'M11').
card_image_name('incite'/'M11', 'incite').
card_uid('incite'/'M11', 'M11:Incite:incite').
card_rarity('incite'/'M11', 'Common').
card_artist('incite'/'M11', 'Scott Chou').
card_number('incite'/'M11', '145').
card_flavor_text('incite'/'M11', 'The veneer of rationality peeled from the mage, revealing the spite and rage beneath.').
card_multiverse_id('incite'/'M11', '205045').

card_in_set('infantry veteran', 'M11').
card_original_type('infantry veteran'/'M11', 'Creature — Human Soldier').
card_original_text('infantry veteran'/'M11', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'M11', 'infantry veteran').
card_uid('infantry veteran'/'M11', 'M11:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'M11', 'Common').
card_artist('infantry veteran'/'M11', 'Christopher Rush').
card_number('infantry veteran'/'M11', '18').
card_flavor_text('infantry veteran'/'M11', '\"Any fool can pick up a sword and charge into battle. To emerge alive at the end, that is a gift that comes from leadership and experience.\"').
card_multiverse_id('infantry veteran'/'M11', '208278').

card_in_set('inferno titan', 'M11').
card_original_type('inferno titan'/'M11', 'Creature — Giant').
card_original_text('inferno titan'/'M11', '{R}: Inferno Titan gets +1/+0 until end of turn.\nWhenever Inferno Titan enters the battlefield or attacks, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('inferno titan'/'M11', 'inferno titan').
card_uid('inferno titan'/'M11', 'M11:Inferno Titan:inferno titan').
card_rarity('inferno titan'/'M11', 'Mythic Rare').
card_artist('inferno titan'/'M11', 'Kev Walker').
card_number('inferno titan'/'M11', '146').
card_multiverse_id('inferno titan'/'M11', '205042').

card_in_set('inspired charge', 'M11').
card_original_type('inspired charge'/'M11', 'Instant').
card_original_text('inspired charge'/'M11', 'Creatures you control get +2/+1 until end of turn.').
card_first_print('inspired charge', 'M11').
card_image_name('inspired charge'/'M11', 'inspired charge').
card_uid('inspired charge'/'M11', 'M11:Inspired Charge:inspired charge').
card_rarity('inspired charge'/'M11', 'Common').
card_artist('inspired charge'/'M11', 'Wayne Reynolds').
card_number('inspired charge'/'M11', '19').
card_flavor_text('inspired charge'/'M11', '\"Impossible! How could they overwhelm us? We had barricades, war elephants, . . . and they were barely a tenth of our number!\"\n—General Avitora').
card_multiverse_id('inspired charge'/'M11', '205074').

card_in_set('island', 'M11').
card_original_type('island'/'M11', 'Basic Land — Island').
card_original_text('island'/'M11', 'U').
card_image_name('island'/'M11', 'island1').
card_uid('island'/'M11', 'M11:Island:island1').
card_rarity('island'/'M11', 'Basic Land').
card_artist('island'/'M11', 'Rob Alexander').
card_number('island'/'M11', '234').
card_multiverse_id('island'/'M11', '213619').

card_in_set('island', 'M11').
card_original_type('island'/'M11', 'Basic Land — Island').
card_original_text('island'/'M11', 'U').
card_image_name('island'/'M11', 'island2').
card_uid('island'/'M11', 'M11:Island:island2').
card_rarity('island'/'M11', 'Basic Land').
card_artist('island'/'M11', 'John Avon').
card_number('island'/'M11', '235').
card_multiverse_id('island'/'M11', '213617').

card_in_set('island', 'M11').
card_original_type('island'/'M11', 'Basic Land — Island').
card_original_text('island'/'M11', 'U').
card_image_name('island'/'M11', 'island3').
card_uid('island'/'M11', 'M11:Island:island3').
card_rarity('island'/'M11', 'Basic Land').
card_artist('island'/'M11', 'Fred Fields').
card_number('island'/'M11', '236').
card_multiverse_id('island'/'M11', '213618').

card_in_set('island', 'M11').
card_original_type('island'/'M11', 'Basic Land — Island').
card_original_text('island'/'M11', 'U').
card_image_name('island'/'M11', 'island4').
card_uid('island'/'M11', 'M11:Island:island4').
card_rarity('island'/'M11', 'Basic Land').
card_artist('island'/'M11', 'Eric Peterson').
card_number('island'/'M11', '237').
card_multiverse_id('island'/'M11', '213616').

card_in_set('jace beleren', 'M11').
card_original_type('jace beleren'/'M11', 'Planeswalker — Jace').
card_original_text('jace beleren'/'M11', '+2: Each player draws a card.\n-1: Target player draws a card.\n-10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_image_name('jace beleren'/'M11', 'jace beleren').
card_uid('jace beleren'/'M11', 'M11:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'M11', 'Mythic Rare').
card_artist('jace beleren'/'M11', 'Aleksi Briclot').
card_number('jace beleren'/'M11', '58').
card_multiverse_id('jace beleren'/'M11', '205960').

card_in_set('jace\'s erasure', 'M11').
card_original_type('jace\'s erasure'/'M11', 'Enchantment').
card_original_text('jace\'s erasure'/'M11', 'Whenever you draw a card, you may have target player put the top card of his or her library into his or her graveyard.').
card_first_print('jace\'s erasure', 'M11').
card_image_name('jace\'s erasure'/'M11', 'jace\'s erasure').
card_uid('jace\'s erasure'/'M11', 'M11:Jace\'s Erasure:jace\'s erasure').
card_rarity('jace\'s erasure'/'M11', 'Common').
card_artist('jace\'s erasure'/'M11', 'Jason Chan').
card_number('jace\'s erasure'/'M11', '59').
card_flavor_text('jace\'s erasure'/'M11', '\"You should try to clear your mind of idle thoughts. And if you can\'t, I will.\"').
card_multiverse_id('jace\'s erasure'/'M11', '205043').

card_in_set('jace\'s ingenuity', 'M11').
card_original_type('jace\'s ingenuity'/'M11', 'Instant').
card_original_text('jace\'s ingenuity'/'M11', 'Draw three cards.').
card_image_name('jace\'s ingenuity'/'M11', 'jace\'s ingenuity').
card_uid('jace\'s ingenuity'/'M11', 'M11:Jace\'s Ingenuity:jace\'s ingenuity').
card_rarity('jace\'s ingenuity'/'M11', 'Uncommon').
card_artist('jace\'s ingenuity'/'M11', 'Igor Kieryluk').
card_number('jace\'s ingenuity'/'M11', '60').
card_flavor_text('jace\'s ingenuity'/'M11', '\"Brute force can sometimes kick down a locked door, but knowledge is a skeleton key.\"').
card_multiverse_id('jace\'s ingenuity'/'M11', '205015').

card_in_set('jinxed idol', 'M11').
card_original_type('jinxed idol'/'M11', 'Artifact').
card_original_text('jinxed idol'/'M11', 'At the beginning of your upkeep, Jinxed Idol deals 2 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Idol.').
card_image_name('jinxed idol'/'M11', 'jinxed idol').
card_uid('jinxed idol'/'M11', 'M11:Jinxed Idol:jinxed idol').
card_rarity('jinxed idol'/'M11', 'Rare').
card_artist('jinxed idol'/'M11', 'jD').
card_number('jinxed idol'/'M11', '208').
card_flavor_text('jinxed idol'/'M11', '\"Please, honored friend, take it with my compliments. I insist.\"').
card_multiverse_id('jinxed idol'/'M11', '208006').

card_in_set('juggernaut', 'M11').
card_original_type('juggernaut'/'M11', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'M11', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'M11', 'juggernaut').
card_uid('juggernaut'/'M11', 'M11:Juggernaut:juggernaut').
card_rarity('juggernaut'/'M11', 'Uncommon').
card_artist('juggernaut'/'M11', 'Mark Hyzer').
card_number('juggernaut'/'M11', '209').
card_flavor_text('juggernaut'/'M11', 'The mighty city of An Karras, with its rich history, built over thousands of years, fell in three hours.').
card_multiverse_id('juggernaut'/'M11', '205230').

card_in_set('knight exemplar', 'M11').
card_original_type('knight exemplar'/'M11', 'Creature — Human Knight').
card_original_text('knight exemplar'/'M11', 'First strike (This creature deals combat damage before creatures without first strike.)\nOther Knight creatures you control get +1/+1 and are indestructible. (Lethal damage and effects that say \"destroy\" don\'t destroy them.)').
card_image_name('knight exemplar'/'M11', 'knight exemplar').
card_uid('knight exemplar'/'M11', 'M11:Knight Exemplar:knight exemplar').
card_rarity('knight exemplar'/'M11', 'Rare').
card_artist('knight exemplar'/'M11', 'Jason Chan').
card_number('knight exemplar'/'M11', '20').
card_multiverse_id('knight exemplar'/'M11', '204988').

card_in_set('kraken\'s eye', 'M11').
card_original_type('kraken\'s eye'/'M11', 'Artifact').
card_original_text('kraken\'s eye'/'M11', 'Whenever a player casts a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'M11', 'kraken\'s eye').
card_uid('kraken\'s eye'/'M11', 'M11:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'M11', 'Uncommon').
card_artist('kraken\'s eye'/'M11', 'Alan Pollack').
card_number('kraken\'s eye'/'M11', '210').
card_flavor_text('kraken\'s eye'/'M11', 'It\'s seen the wonders of entire worlds trapped in the depths of a bottomless ocean.').
card_multiverse_id('kraken\'s eye'/'M11', '206328').

card_in_set('lava axe', 'M11').
card_original_type('lava axe'/'M11', 'Sorcery').
card_original_text('lava axe'/'M11', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'M11', 'lava axe').
card_uid('lava axe'/'M11', 'M11:Lava Axe:lava axe').
card_rarity('lava axe'/'M11', 'Common').
card_artist('lava axe'/'M11', 'Brian Snõddy').
card_number('lava axe'/'M11', '147').
card_flavor_text('lava axe'/'M11', 'From the heart of the mountain into the heart of the mage.').
card_multiverse_id('lava axe'/'M11', '205117').

card_in_set('leyline of anticipation', 'M11').
card_original_type('leyline of anticipation'/'M11', 'Enchantment').
card_original_text('leyline of anticipation'/'M11', 'If Leyline of Anticipation is in your opening hand, you may begin the game with it on the battlefield.\nYou may cast nonland cards as though they had flash. (You may cast them any time you could cast an instant.)').
card_first_print('leyline of anticipation', 'M11').
card_image_name('leyline of anticipation'/'M11', 'leyline of anticipation').
card_uid('leyline of anticipation'/'M11', 'M11:Leyline of Anticipation:leyline of anticipation').
card_rarity('leyline of anticipation'/'M11', 'Rare').
card_artist('leyline of anticipation'/'M11', 'Charles Urbach').
card_number('leyline of anticipation'/'M11', '61').
card_multiverse_id('leyline of anticipation'/'M11', '205008').

card_in_set('leyline of punishment', 'M11').
card_original_type('leyline of punishment'/'M11', 'Enchantment').
card_original_text('leyline of punishment'/'M11', 'If Leyline of Punishment is in your opening hand, you may begin the game with it on the battlefield.\nPlayers can\'t gain life.\nDamage can\'t be prevented.').
card_first_print('leyline of punishment', 'M11').
card_image_name('leyline of punishment'/'M11', 'leyline of punishment').
card_uid('leyline of punishment'/'M11', 'M11:Leyline of Punishment:leyline of punishment').
card_rarity('leyline of punishment'/'M11', 'Rare').
card_artist('leyline of punishment'/'M11', 'Charles Urbach').
card_number('leyline of punishment'/'M11', '148').
card_multiverse_id('leyline of punishment'/'M11', '205018').

card_in_set('leyline of sanctity', 'M11').
card_original_type('leyline of sanctity'/'M11', 'Enchantment').
card_original_text('leyline of sanctity'/'M11', 'If Leyline of Sanctity is in your opening hand, you may begin the game with it on the battlefield.\nYou can\'t be the target of spells or abilities your opponents control.').
card_first_print('leyline of sanctity', 'M11').
card_image_name('leyline of sanctity'/'M11', 'leyline of sanctity').
card_uid('leyline of sanctity'/'M11', 'M11:Leyline of Sanctity:leyline of sanctity').
card_rarity('leyline of sanctity'/'M11', 'Rare').
card_artist('leyline of sanctity'/'M11', 'Ryan Pancoast').
card_number('leyline of sanctity'/'M11', '21').
card_multiverse_id('leyline of sanctity'/'M11', '204993').

card_in_set('leyline of the void', 'M11').
card_original_type('leyline of the void'/'M11', 'Enchantment').
card_original_text('leyline of the void'/'M11', 'If Leyline of the Void is in your opening hand, you may begin the game with it on the battlefield.\nIf a card would be put into an opponent\'s graveyard from anywhere, exile it instead.').
card_image_name('leyline of the void'/'M11', 'leyline of the void').
card_uid('leyline of the void'/'M11', 'M11:Leyline of the Void:leyline of the void').
card_rarity('leyline of the void'/'M11', 'Rare').
card_artist('leyline of the void'/'M11', 'Rob Alexander').
card_number('leyline of the void'/'M11', '101').
card_multiverse_id('leyline of the void'/'M11', '205013').

card_in_set('leyline of vitality', 'M11').
card_original_type('leyline of vitality'/'M11', 'Enchantment').
card_original_text('leyline of vitality'/'M11', 'If Leyline of Vitality is in your opening hand, you may begin the game with it on the battlefield.\nCreatures you control get +0/+1.\nWhenever a creature enters the battlefield under your control, you may gain 1 life.').
card_first_print('leyline of vitality', 'M11').
card_image_name('leyline of vitality'/'M11', 'leyline of vitality').
card_uid('leyline of vitality'/'M11', 'M11:Leyline of Vitality:leyline of vitality').
card_rarity('leyline of vitality'/'M11', 'Rare').
card_artist('leyline of vitality'/'M11', 'Jim Nelson').
card_number('leyline of vitality'/'M11', '183').
card_multiverse_id('leyline of vitality'/'M11', '205031').

card_in_set('lightning bolt', 'M11').
card_original_type('lightning bolt'/'M11', 'Instant').
card_original_text('lightning bolt'/'M11', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'M11', 'lightning bolt').
card_uid('lightning bolt'/'M11', 'M11:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'M11', 'Common').
card_artist('lightning bolt'/'M11', 'Christopher Moeller').
card_number('lightning bolt'/'M11', '149').
card_multiverse_id('lightning bolt'/'M11', '205227').

card_in_set('liliana vess', 'M11').
card_original_type('liliana vess'/'M11', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'M11', '+1: Target player discards a card.\n-2: Search your library for a card, then shuffle your library and put that card on top of it.\n-8: Put all creature cards in all graveyards onto the battlefield under your control.').
card_image_name('liliana vess'/'M11', 'liliana vess').
card_uid('liliana vess'/'M11', 'M11:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'M11', 'Mythic Rare').
card_artist('liliana vess'/'M11', 'Aleksi Briclot').
card_number('liliana vess'/'M11', '102').
card_multiverse_id('liliana vess'/'M11', '205961').

card_in_set('liliana\'s caress', 'M11').
card_original_type('liliana\'s caress'/'M11', 'Enchantment').
card_original_text('liliana\'s caress'/'M11', 'Whenever an opponent discards a card, that player loses 2 life.').
card_first_print('liliana\'s caress', 'M11').
card_image_name('liliana\'s caress'/'M11', 'liliana\'s caress').
card_uid('liliana\'s caress'/'M11', 'M11:Liliana\'s Caress:liliana\'s caress').
card_rarity('liliana\'s caress'/'M11', 'Uncommon').
card_artist('liliana\'s caress'/'M11', 'Steve Argyle').
card_number('liliana\'s caress'/'M11', '103').
card_flavor_text('liliana\'s caress'/'M11', '\"This might hurt less if you don\'t fight so hard. But I doubt it.\"').
card_multiverse_id('liliana\'s caress'/'M11', '205035').

card_in_set('liliana\'s specter', 'M11').
card_original_type('liliana\'s specter'/'M11', 'Creature — Specter').
card_original_text('liliana\'s specter'/'M11', 'Flying\nWhen Liliana\'s Specter enters the battlefield, each opponent discards a card.').
card_image_name('liliana\'s specter'/'M11', 'liliana\'s specter').
card_uid('liliana\'s specter'/'M11', 'M11:Liliana\'s Specter:liliana\'s specter').
card_rarity('liliana\'s specter'/'M11', 'Common').
card_artist('liliana\'s specter'/'M11', 'Vance Kovacs').
card_number('liliana\'s specter'/'M11', '104').
card_flavor_text('liliana\'s specter'/'M11', '\"The finest minions know what I need without me ever saying a thing.\"\n—Liliana Vess').
card_multiverse_id('liliana\'s specter'/'M11', '204998').

card_in_set('llanowar elves', 'M11').
card_original_type('llanowar elves'/'M11', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'M11', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'M11', 'llanowar elves').
card_uid('llanowar elves'/'M11', 'M11:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'M11', 'Common').
card_artist('llanowar elves'/'M11', 'Kev Walker').
card_number('llanowar elves'/'M11', '184').
card_flavor_text('llanowar elves'/'M11', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'M11', '205228').

card_in_set('magma phoenix', 'M11').
card_original_type('magma phoenix'/'M11', 'Creature — Phoenix').
card_original_text('magma phoenix'/'M11', 'Flying\nWhen Magma Phoenix is put into a graveyard from the battlefield, it deals 3 damage to each creature and each player.\n{3}{R}{R}: Return Magma Phoenix from your graveyard to your hand.').
card_image_name('magma phoenix'/'M11', 'magma phoenix').
card_uid('magma phoenix'/'M11', 'M11:Magma Phoenix:magma phoenix').
card_rarity('magma phoenix'/'M11', 'Rare').
card_artist('magma phoenix'/'M11', 'Raymond Swanland').
card_number('magma phoenix'/'M11', '150').
card_multiverse_id('magma phoenix'/'M11', '208003').

card_in_set('mana leak', 'M11').
card_original_type('mana leak'/'M11', 'Instant').
card_original_text('mana leak'/'M11', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'M11', 'mana leak').
card_uid('mana leak'/'M11', 'M11:Mana Leak:mana leak').
card_rarity('mana leak'/'M11', 'Common').
card_artist('mana leak'/'M11', 'Howard Lyon').
card_number('mana leak'/'M11', '62').
card_flavor_text('mana leak'/'M11', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'M11', '204981').

card_in_set('manic vandal', 'M11').
card_original_type('manic vandal'/'M11', 'Creature — Human Warrior').
card_original_text('manic vandal'/'M11', 'When Manic Vandal enters the battlefield, destroy target artifact.').
card_first_print('manic vandal', 'M11').
card_image_name('manic vandal'/'M11', 'manic vandal').
card_uid('manic vandal'/'M11', 'M11:Manic Vandal:manic vandal').
card_rarity('manic vandal'/'M11', 'Common').
card_artist('manic vandal'/'M11', 'Christopher Moeller').
card_number('manic vandal'/'M11', '151').
card_flavor_text('manic vandal'/'M11', 'It\'s fun. He doesn\'t need another reason.').
card_multiverse_id('manic vandal'/'M11', '208007').

card_in_set('maritime guard', 'M11').
card_original_type('maritime guard'/'M11', 'Creature — Merfolk Soldier').
card_original_text('maritime guard'/'M11', '').
card_first_print('maritime guard', 'M11').
card_image_name('maritime guard'/'M11', 'maritime guard').
card_uid('maritime guard'/'M11', 'M11:Maritime Guard:maritime guard').
card_rarity('maritime guard'/'M11', 'Common').
card_artist('maritime guard'/'M11', 'Allen Williams').
card_number('maritime guard'/'M11', '63').
card_flavor_text('maritime guard'/'M11', 'Once common in the Kapsho Seas, merfolk were hunted almost to extinction. They survived those dark days and emerged calculating and determined to endure.').
card_multiverse_id('maritime guard'/'M11', '205040').

card_in_set('mass polymorph', 'M11').
card_original_type('mass polymorph'/'M11', 'Sorcery').
card_original_text('mass polymorph'/'M11', 'Exile all creatures you control, then reveal cards from the top of your library until you reveal that many creature cards. Put all creature cards revealed this way onto the battlefield, then shuffle the rest of the revealed cards into your library.').
card_first_print('mass polymorph', 'M11').
card_image_name('mass polymorph'/'M11', 'mass polymorph').
card_uid('mass polymorph'/'M11', 'M11:Mass Polymorph:mass polymorph').
card_rarity('mass polymorph'/'M11', 'Rare').
card_artist('mass polymorph'/'M11', 'Christopher Moeller').
card_number('mass polymorph'/'M11', '64').
card_multiverse_id('mass polymorph'/'M11', '204977').

card_in_set('merfolk sovereign', 'M11').
card_original_type('merfolk sovereign'/'M11', 'Creature — Merfolk').
card_original_text('merfolk sovereign'/'M11', 'Other Merfolk creatures you control get +1/+1.\n{T}: Target Merfolk creature is unblockable this turn.').
card_image_name('merfolk sovereign'/'M11', 'merfolk sovereign').
card_uid('merfolk sovereign'/'M11', 'M11:Merfolk Sovereign:merfolk sovereign').
card_rarity('merfolk sovereign'/'M11', 'Rare').
card_artist('merfolk sovereign'/'M11', 'Jesper Ejsing').
card_number('merfolk sovereign'/'M11', '65').
card_flavor_text('merfolk sovereign'/'M11', '\"Be like the sea: flow around that which is unmovable; for everything else, crash into it unrelentingly.\"').
card_multiverse_id('merfolk sovereign'/'M11', '205108').

card_in_set('merfolk spy', 'M11').
card_original_type('merfolk spy'/'M11', 'Creature — Merfolk Rogue').
card_original_text('merfolk spy'/'M11', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nWhenever Merfolk Spy deals combat damage to a player, that player reveals a card at random from his or her hand.').
card_first_print('merfolk spy', 'M11').
card_image_name('merfolk spy'/'M11', 'merfolk spy').
card_uid('merfolk spy'/'M11', 'M11:Merfolk Spy:merfolk spy').
card_rarity('merfolk spy'/'M11', 'Common').
card_artist('merfolk spy'/'M11', 'Matt Cavotta & Richard Whitters').
card_number('merfolk spy'/'M11', '66').
card_multiverse_id('merfolk spy'/'M11', '205009').

card_in_set('mighty leap', 'M11').
card_original_type('mighty leap'/'M11', 'Instant').
card_original_text('mighty leap'/'M11', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_first_print('mighty leap', 'M11').
card_image_name('mighty leap'/'M11', 'mighty leap').
card_uid('mighty leap'/'M11', 'M11:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'M11', 'Common').
card_artist('mighty leap'/'M11', 'rk post').
card_number('mighty leap'/'M11', '22').
card_flavor_text('mighty leap'/'M11', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'M11', '208280').

card_in_set('mind control', 'M11').
card_original_type('mind control'/'M11', 'Enchantment — Aura').
card_original_text('mind control'/'M11', 'Enchant creature\nYou control enchanted creature.').
card_image_name('mind control'/'M11', 'mind control').
card_uid('mind control'/'M11', 'M11:Mind Control:mind control').
card_rarity('mind control'/'M11', 'Uncommon').
card_artist('mind control'/'M11', 'Ryan Pancoast').
card_number('mind control'/'M11', '67').
card_flavor_text('mind control'/'M11', '\"The mind: a great weapon and an even greater weakness.\"\n—Jace Beleren').
card_multiverse_id('mind control'/'M11', '208220').

card_in_set('mind rot', 'M11').
card_original_type('mind rot'/'M11', 'Sorcery').
card_original_text('mind rot'/'M11', 'Target player discards two cards.').
card_image_name('mind rot'/'M11', 'mind rot').
card_uid('mind rot'/'M11', 'M11:Mind Rot:mind rot').
card_rarity('mind rot'/'M11', 'Common').
card_artist('mind rot'/'M11', 'Steve Luke').
card_number('mind rot'/'M11', '105').
card_flavor_text('mind rot'/'M11', 'Not every thought is a good one.').
card_multiverse_id('mind rot'/'M11', '205010').

card_in_set('mitotic slime', 'M11').
card_original_type('mitotic slime'/'M11', 'Creature — Ooze').
card_original_text('mitotic slime'/'M11', 'When Mitotic Slime is put into a graveyard from the battlefield, put two 2/2 green Ooze creature tokens onto the battlefield. They have \"When this creature is put into a graveyard, put two 1/1 green Ooze creature tokens onto the battlefield.\"').
card_image_name('mitotic slime'/'M11', 'mitotic slime').
card_uid('mitotic slime'/'M11', 'M11:Mitotic Slime:mitotic slime').
card_rarity('mitotic slime'/'M11', 'Rare').
card_artist('mitotic slime'/'M11', 'Raymond Swanland').
card_number('mitotic slime'/'M11', '185').
card_multiverse_id('mitotic slime'/'M11', '205032').

card_in_set('mountain', 'M11').
card_original_type('mountain'/'M11', 'Basic Land — Mountain').
card_original_text('mountain'/'M11', 'R').
card_image_name('mountain'/'M11', 'mountain1').
card_uid('mountain'/'M11', 'M11:Mountain:mountain1').
card_rarity('mountain'/'M11', 'Basic Land').
card_artist('mountain'/'M11', 'Rob Alexander').
card_number('mountain'/'M11', '242').
card_multiverse_id('mountain'/'M11', '213624').

card_in_set('mountain', 'M11').
card_original_type('mountain'/'M11', 'Basic Land — Mountain').
card_original_text('mountain'/'M11', 'R').
card_image_name('mountain'/'M11', 'mountain2').
card_uid('mountain'/'M11', 'M11:Mountain:mountain2').
card_rarity('mountain'/'M11', 'Basic Land').
card_artist('mountain'/'M11', 'John Avon').
card_number('mountain'/'M11', '243').
card_multiverse_id('mountain'/'M11', '213625').

card_in_set('mountain', 'M11').
card_original_type('mountain'/'M11', 'Basic Land — Mountain').
card_original_text('mountain'/'M11', 'R').
card_image_name('mountain'/'M11', 'mountain3').
card_uid('mountain'/'M11', 'M11:Mountain:mountain3').
card_rarity('mountain'/'M11', 'Basic Land').
card_artist('mountain'/'M11', 'Nils Hamm').
card_number('mountain'/'M11', '244').
card_multiverse_id('mountain'/'M11', '213626').

card_in_set('mountain', 'M11').
card_original_type('mountain'/'M11', 'Basic Land — Mountain').
card_original_text('mountain'/'M11', 'R').
card_image_name('mountain'/'M11', 'mountain4').
card_uid('mountain'/'M11', 'M11:Mountain:mountain4').
card_rarity('mountain'/'M11', 'Basic Land').
card_artist('mountain'/'M11', 'Karl Kopinski').
card_number('mountain'/'M11', '245').
card_multiverse_id('mountain'/'M11', '213627').

card_in_set('mystifying maze', 'M11').
card_original_type('mystifying maze'/'M11', 'Land').
card_original_text('mystifying maze'/'M11', '{T}: Add {1} to your mana pool.\n{4}, {T}: Exile target attacking creature an opponent controls. At the beginning of the next end step, return it to the battlefield tapped under its owner\'s control.').
card_first_print('mystifying maze', 'M11').
card_image_name('mystifying maze'/'M11', 'mystifying maze').
card_uid('mystifying maze'/'M11', 'M11:Mystifying Maze:mystifying maze').
card_rarity('mystifying maze'/'M11', 'Rare').
card_artist('mystifying maze'/'M11', 'Robh Ruppel').
card_number('mystifying maze'/'M11', '226').
card_multiverse_id('mystifying maze'/'M11', '204978').

card_in_set('nantuko shade', 'M11').
card_original_type('nantuko shade'/'M11', 'Creature — Insect Shade').
card_original_text('nantuko shade'/'M11', '{B}: Nantuko Shade gets +1/+1 until end of turn.').
card_image_name('nantuko shade'/'M11', 'nantuko shade').
card_uid('nantuko shade'/'M11', 'M11:Nantuko Shade:nantuko shade').
card_rarity('nantuko shade'/'M11', 'Rare').
card_artist('nantuko shade'/'M11', 'Brian Snõddy').
card_number('nantuko shade'/'M11', '106').
card_flavor_text('nantuko shade'/'M11', 'In life, the nantuko study nature by revering it. In death, they study nature by disemboweling it.').
card_multiverse_id('nantuko shade'/'M11', '205229').

card_in_set('naturalize', 'M11').
card_original_type('naturalize'/'M11', 'Instant').
card_original_text('naturalize'/'M11', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M11', 'naturalize').
card_uid('naturalize'/'M11', 'M11:Naturalize:naturalize').
card_rarity('naturalize'/'M11', 'Common').
card_artist('naturalize'/'M11', 'Tim Hildebrandt').
card_number('naturalize'/'M11', '186').
card_flavor_text('naturalize'/'M11', '\"Nature does not need adornment.\"\n—Garruk Wildspeaker').
card_multiverse_id('naturalize'/'M11', '207336').

card_in_set('nature\'s spiral', 'M11').
card_original_type('nature\'s spiral'/'M11', 'Sorcery').
card_original_text('nature\'s spiral'/'M11', 'Return target permanent card from your graveyard to your hand. (A permanent card is an artifact, creature, enchantment, land, or planeswalker card.)').
card_image_name('nature\'s spiral'/'M11', 'nature\'s spiral').
card_uid('nature\'s spiral'/'M11', 'M11:Nature\'s Spiral:nature\'s spiral').
card_rarity('nature\'s spiral'/'M11', 'Uncommon').
card_artist('nature\'s spiral'/'M11', 'Terese Nielsen').
card_number('nature\'s spiral'/'M11', '187').
card_flavor_text('nature\'s spiral'/'M11', 'Snail shell, fern, tornado—the world connects all things in form.').
card_multiverse_id('nature\'s spiral'/'M11', '207338').

card_in_set('necrotic plague', 'M11').
card_original_type('necrotic plague'/'M11', 'Enchantment — Aura').
card_original_text('necrotic plague'/'M11', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, sacrifice this creature.\"\nWhen enchanted creature is put into a graveyard, its controller chooses target creature one of his or her opponents controls. Return Necrotic Plague from its owner\'s graveyard to the battlefield attached to that creature.').
card_first_print('necrotic plague', 'M11').
card_image_name('necrotic plague'/'M11', 'necrotic plague').
card_uid('necrotic plague'/'M11', 'M11:Necrotic Plague:necrotic plague').
card_rarity('necrotic plague'/'M11', 'Rare').
card_artist('necrotic plague'/'M11', 'Jaime Jones').
card_number('necrotic plague'/'M11', '107').
card_multiverse_id('necrotic plague'/'M11', '207104').

card_in_set('negate', 'M11').
card_original_type('negate'/'M11', 'Instant').
card_original_text('negate'/'M11', 'Counter target noncreature spell.').
card_image_name('negate'/'M11', 'negate').
card_uid('negate'/'M11', 'M11:Negate:negate').
card_rarity('negate'/'M11', 'Common').
card_artist('negate'/'M11', 'Jeremy Jarvis').
card_number('negate'/'M11', '68').
card_flavor_text('negate'/'M11', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M11', '208222').

card_in_set('nether horror', 'M11').
card_original_type('nether horror'/'M11', 'Creature — Horror').
card_original_text('nether horror'/'M11', '').
card_first_print('nether horror', 'M11').
card_image_name('nether horror'/'M11', 'nether horror').
card_uid('nether horror'/'M11', 'M11:Nether Horror:nether horror').
card_rarity('nether horror'/'M11', 'Common').
card_artist('nether horror'/'M11', 'Allen Williams').
card_number('nether horror'/'M11', '108').
card_flavor_text('nether horror'/'M11', '\"My dreams darkened and seeped into my waking hours. Spellcraft and nightmare merged to create this monstrosity.\"\n—Sunniva, witch of Holm Hollow').
card_multiverse_id('nether horror'/'M11', '204994').

card_in_set('nightwing shade', 'M11').
card_original_type('nightwing shade'/'M11', 'Creature — Shade').
card_original_text('nightwing shade'/'M11', 'Flying\n{1}{B}: Nightwing Shade gets +1/+1 until end of turn.').
card_first_print('nightwing shade', 'M11').
card_image_name('nightwing shade'/'M11', 'nightwing shade').
card_uid('nightwing shade'/'M11', 'M11:Nightwing Shade:nightwing shade').
card_rarity('nightwing shade'/'M11', 'Common').
card_artist('nightwing shade'/'M11', 'Lucas Graciano').
card_number('nightwing shade'/'M11', '109').
card_flavor_text('nightwing shade'/'M11', '\"There is one hour of the night even we do not watch.\"\n—Sedva, captain of the watch').
card_multiverse_id('nightwing shade'/'M11', '207103').

card_in_set('obstinate baloth', 'M11').
card_original_type('obstinate baloth'/'M11', 'Creature — Beast').
card_original_text('obstinate baloth'/'M11', 'When Obstinate Baloth enters the battlefield, you gain 4 life.\nIf a spell or ability an opponent controls causes you to discard Obstinate Baloth, put it onto the battlefield instead of putting it into your graveyard.').
card_first_print('obstinate baloth', 'M11').
card_image_name('obstinate baloth'/'M11', 'obstinate baloth').
card_uid('obstinate baloth'/'M11', 'M11:Obstinate Baloth:obstinate baloth').
card_rarity('obstinate baloth'/'M11', 'Rare').
card_artist('obstinate baloth'/'M11', 'Chris Rahn').
card_number('obstinate baloth'/'M11', '188').
card_multiverse_id('obstinate baloth'/'M11', '205075').

card_in_set('ornithopter', 'M11').
card_original_type('ornithopter'/'M11', 'Artifact Creature — Thopter').
card_original_text('ornithopter'/'M11', 'Flying').
card_image_name('ornithopter'/'M11', 'ornithopter').
card_uid('ornithopter'/'M11', 'M11:Ornithopter:ornithopter').
card_rarity('ornithopter'/'M11', 'Uncommon').
card_artist('ornithopter'/'M11', 'Franz Vohwinkel').
card_number('ornithopter'/'M11', '211').
card_flavor_text('ornithopter'/'M11', 'Regardless of the century, plane, or species, developing artificers never fail to invent the ornithopter.').
card_multiverse_id('ornithopter'/'M11', '206331').

card_in_set('overwhelming stampede', 'M11').
card_original_type('overwhelming stampede'/'M11', 'Sorcery').
card_original_text('overwhelming stampede'/'M11', 'Until end of turn, creatures you control gain trample and get +X/+X, where X is the greatest power among creatures you control. (If a creature you control would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('overwhelming stampede', 'M11').
card_image_name('overwhelming stampede'/'M11', 'overwhelming stampede').
card_uid('overwhelming stampede'/'M11', 'M11:Overwhelming Stampede:overwhelming stampede').
card_rarity('overwhelming stampede'/'M11', 'Rare').
card_artist('overwhelming stampede'/'M11', 'Steven Belledin').
card_number('overwhelming stampede'/'M11', '189').
card_multiverse_id('overwhelming stampede'/'M11', '205003').

card_in_set('pacifism', 'M11').
card_original_type('pacifism'/'M11', 'Enchantment — Aura').
card_original_text('pacifism'/'M11', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'M11', 'pacifism').
card_uid('pacifism'/'M11', 'M11:Pacifism:pacifism').
card_rarity('pacifism'/'M11', 'Common').
card_artist('pacifism'/'M11', 'Robert Bliss').
card_number('pacifism'/'M11', '23').
card_flavor_text('pacifism'/'M11', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'M11', '208292').

card_in_set('palace guard', 'M11').
card_original_type('palace guard'/'M11', 'Creature — Human Soldier').
card_original_text('palace guard'/'M11', 'Palace Guard can block any number of creatures.').
card_image_name('palace guard'/'M11', 'palace guard').
card_uid('palace guard'/'M11', 'M11:Palace Guard:palace guard').
card_rarity('palace guard'/'M11', 'Common').
card_artist('palace guard'/'M11', 'Volkan Baga').
card_number('palace guard'/'M11', '24').
card_flavor_text('palace guard'/'M11', '\"I\'m not nearly as bothered by being outnumbered as I am by their foul stench.\"').
card_multiverse_id('palace guard'/'M11', '205110').

card_in_set('phantom beast', 'M11').
card_original_type('phantom beast'/'M11', 'Creature — Illusion Beast').
card_original_text('phantom beast'/'M11', 'When Phantom Beast becomes the target of a spell or ability, sacrifice it.').
card_first_print('phantom beast', 'M11').
card_image_name('phantom beast'/'M11', 'phantom beast').
card_uid('phantom beast'/'M11', 'M11:Phantom Beast:phantom beast').
card_rarity('phantom beast'/'M11', 'Common').
card_artist('phantom beast'/'M11', 'Ryan Yee').
card_number('phantom beast'/'M11', '69').
card_flavor_text('phantom beast'/'M11', 'As insubstantial as fear, and as dangerous.').
card_multiverse_id('phantom beast'/'M11', '205017').

card_in_set('phylactery lich', 'M11').
card_original_type('phylactery lich'/'M11', 'Creature — Zombie').
card_original_text('phylactery lich'/'M11', 'As Phylactery Lich enters the battlefield, put a phylactery counter on an artifact you control.\nPhylactery Lich is indestructible.\nWhen you control no permanents with phylactery counters on them, sacrifice Phylactery Lich.').
card_first_print('phylactery lich', 'M11').
card_image_name('phylactery lich'/'M11', 'phylactery lich').
card_uid('phylactery lich'/'M11', 'M11:Phylactery Lich:phylactery lich').
card_rarity('phylactery lich'/'M11', 'Rare').
card_artist('phylactery lich'/'M11', 'Michael Komarck').
card_number('phylactery lich'/'M11', '110').
card_multiverse_id('phylactery lich'/'M11', '205034').

card_in_set('plains', 'M11').
card_original_type('plains'/'M11', 'Basic Land — Plains').
card_original_text('plains'/'M11', 'W').
card_image_name('plains'/'M11', 'plains1').
card_uid('plains'/'M11', 'M11:Plains:plains1').
card_rarity('plains'/'M11', 'Basic Land').
card_artist('plains'/'M11', 'John Avon').
card_number('plains'/'M11', '230').
card_multiverse_id('plains'/'M11', '213614').

card_in_set('plains', 'M11').
card_original_type('plains'/'M11', 'Basic Land — Plains').
card_original_text('plains'/'M11', 'W').
card_image_name('plains'/'M11', 'plains2').
card_uid('plains'/'M11', 'M11:Plains:plains2').
card_rarity('plains'/'M11', 'Basic Land').
card_artist('plains'/'M11', 'John Avon').
card_number('plains'/'M11', '231').
card_multiverse_id('plains'/'M11', '213612').

card_in_set('plains', 'M11').
card_original_type('plains'/'M11', 'Basic Land — Plains').
card_original_text('plains'/'M11', 'W').
card_image_name('plains'/'M11', 'plains3').
card_uid('plains'/'M11', 'M11:Plains:plains3').
card_rarity('plains'/'M11', 'Basic Land').
card_artist('plains'/'M11', 'D. J. Cleland-Hura').
card_number('plains'/'M11', '232').
card_multiverse_id('plains'/'M11', '213613').

card_in_set('plains', 'M11').
card_original_type('plains'/'M11', 'Basic Land — Plains').
card_original_text('plains'/'M11', 'W').
card_image_name('plains'/'M11', 'plains4').
card_uid('plains'/'M11', 'M11:Plains:plains4').
card_rarity('plains'/'M11', 'Basic Land').
card_artist('plains'/'M11', 'Ryan Pancoast').
card_number('plains'/'M11', '233').
card_multiverse_id('plains'/'M11', '213615').

card_in_set('platinum angel', 'M11').
card_original_type('platinum angel'/'M11', 'Artifact Creature — Angel').
card_original_text('platinum angel'/'M11', 'Flying\nYou can\'t lose the game and your opponents can\'t win the game.').
card_image_name('platinum angel'/'M11', 'platinum angel').
card_uid('platinum angel'/'M11', 'M11:Platinum Angel:platinum angel').
card_rarity('platinum angel'/'M11', 'Mythic Rare').
card_artist('platinum angel'/'M11', 'Brom').
card_number('platinum angel'/'M11', '212').
card_flavor_text('platinum angel'/'M11', 'She is the apex of the artificer\'s craft, the spirit of the divine called out of base metal.').
card_multiverse_id('platinum angel'/'M11', '206329').

card_in_set('plummet', 'M11').
card_original_type('plummet'/'M11', 'Instant').
card_original_text('plummet'/'M11', 'Destroy target creature with flying.').
card_image_name('plummet'/'M11', 'plummet').
card_uid('plummet'/'M11', 'M11:Plummet:plummet').
card_rarity('plummet'/'M11', 'Common').
card_artist('plummet'/'M11', 'Pete Venters').
card_number('plummet'/'M11', '190').
card_flavor_text('plummet'/'M11', '\"You are the grandest of all,\" said the archdruid to the trees. They became so proud of bark and branch that they suffered no creature to fly overhead or perch upon a bough.').
card_multiverse_id('plummet'/'M11', '205062').

card_in_set('preordain', 'M11').
card_original_type('preordain'/'M11', 'Sorcery').
card_original_text('preordain'/'M11', 'Scry 2, then draw a card. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('preordain', 'M11').
card_image_name('preordain'/'M11', 'preordain').
card_uid('preordain'/'M11', 'M11:Preordain:preordain').
card_rarity('preordain'/'M11', 'Common').
card_artist('preordain'/'M11', 'Svetlin Velinov').
card_number('preordain'/'M11', '70').
card_multiverse_id('preordain'/'M11', '205019').

card_in_set('primal cocoon', 'M11').
card_original_type('primal cocoon'/'M11', 'Enchantment — Aura').
card_original_text('primal cocoon'/'M11', 'Enchant creature\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature.\nWhen enchanted creature attacks or blocks, sacrifice Primal Cocoon.').
card_first_print('primal cocoon', 'M11').
card_image_name('primal cocoon'/'M11', 'primal cocoon').
card_uid('primal cocoon'/'M11', 'M11:Primal Cocoon:primal cocoon').
card_rarity('primal cocoon'/'M11', 'Common').
card_artist('primal cocoon'/'M11', 'Daniel Ljunggren').
card_number('primal cocoon'/'M11', '191').
card_multiverse_id('primal cocoon'/'M11', '204995').

card_in_set('primeval titan', 'M11').
card_original_type('primeval titan'/'M11', 'Creature — Giant').
card_original_text('primeval titan'/'M11', 'Trample\nWhenever Primeval Titan enters the battlefield or attacks, you may search your library for up to two land cards, put them onto the battlefield tapped, then shuffle your library.').
card_image_name('primeval titan'/'M11', 'primeval titan').
card_uid('primeval titan'/'M11', 'M11:Primeval Titan:primeval titan').
card_rarity('primeval titan'/'M11', 'Mythic Rare').
card_artist('primeval titan'/'M11', 'Aleksi Briclot').
card_number('primeval titan'/'M11', '192').
card_flavor_text('primeval titan'/'M11', 'When nature calls, run.').
card_multiverse_id('primeval titan'/'M11', '205027').

card_in_set('prized unicorn', 'M11').
card_original_type('prized unicorn'/'M11', 'Creature — Unicorn').
card_original_text('prized unicorn'/'M11', 'All creatures able to block Prized Unicorn do so.').
card_image_name('prized unicorn'/'M11', 'prized unicorn').
card_uid('prized unicorn'/'M11', 'M11:Prized Unicorn:prized unicorn').
card_rarity('prized unicorn'/'M11', 'Uncommon').
card_artist('prized unicorn'/'M11', 'Sam Wood').
card_number('prized unicorn'/'M11', '193').
card_flavor_text('prized unicorn'/'M11', '\"The desire for its magic horn inspires such bloodthirsty greed that all who see the unicorn will kill to possess it.\"\n—Dionus, elvish archdruid').
card_multiverse_id('prized unicorn'/'M11', '205111').

card_in_set('prodigal pyromancer', 'M11').
card_original_type('prodigal pyromancer'/'M11', 'Creature — Human Wizard').
card_original_text('prodigal pyromancer'/'M11', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_image_name('prodigal pyromancer'/'M11', 'prodigal pyromancer').
card_uid('prodigal pyromancer'/'M11', 'M11:Prodigal Pyromancer:prodigal pyromancer').
card_rarity('prodigal pyromancer'/'M11', 'Uncommon').
card_artist('prodigal pyromancer'/'M11', 'Jeremy Jarvis').
card_number('prodigal pyromancer'/'M11', '152').
card_flavor_text('prodigal pyromancer'/'M11', '\"What am I looking at? Ashes, dead man.\"').
card_multiverse_id('prodigal pyromancer'/'M11', '205231').

card_in_set('protean hydra', 'M11').
card_original_type('protean hydra'/'M11', 'Creature — Hydra').
card_original_text('protean hydra'/'M11', 'Protean Hydra enters the battlefield with X +1/+1 counters on it.\nIf damage would be dealt to Protean Hydra, prevent that damage and remove that many +1/+1 counters from it.\nWhenever a +1/+1 counter is removed from Protean Hydra, put two +1/+1 counters on it at the beginning of the next end step.').
card_image_name('protean hydra'/'M11', 'protean hydra').
card_uid('protean hydra'/'M11', 'M11:Protean Hydra:protean hydra').
card_rarity('protean hydra'/'M11', 'Rare').
card_artist('protean hydra'/'M11', 'Jim Murray').
card_number('protean hydra'/'M11', '194').
card_multiverse_id('protean hydra'/'M11', '205112').

card_in_set('pyretic ritual', 'M11').
card_original_type('pyretic ritual'/'M11', 'Instant').
card_original_text('pyretic ritual'/'M11', 'Add {R}{R}{R} to your mana pool.').
card_first_print('pyretic ritual', 'M11').
card_image_name('pyretic ritual'/'M11', 'pyretic ritual').
card_uid('pyretic ritual'/'M11', 'M11:Pyretic Ritual:pyretic ritual').
card_rarity('pyretic ritual'/'M11', 'Common').
card_artist('pyretic ritual'/'M11', 'James Paick').
card_number('pyretic ritual'/'M11', '153').
card_flavor_text('pyretic ritual'/'M11', 'The Multiverse is filled with limitless power just waiting for someone to reach out and seize it.').
card_multiverse_id('pyretic ritual'/'M11', '205067').

card_in_set('pyroclasm', 'M11').
card_original_type('pyroclasm'/'M11', 'Sorcery').
card_original_text('pyroclasm'/'M11', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'M11', 'pyroclasm').
card_uid('pyroclasm'/'M11', 'M11:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'M11', 'Uncommon').
card_artist('pyroclasm'/'M11', 'John Avon').
card_number('pyroclasm'/'M11', '154').
card_flavor_text('pyroclasm'/'M11', '\"Who\'d want to ignite things one at a time?\"\n—Chandra Nalaar').
card_multiverse_id('pyroclasm'/'M11', '208009').

card_in_set('quag sickness', 'M11').
card_original_type('quag sickness'/'M11', 'Enchantment — Aura').
card_original_text('quag sickness'/'M11', 'Enchant creature\nEnchanted creature gets -1/-1 for each Swamp you control.').
card_first_print('quag sickness', 'M11').
card_image_name('quag sickness'/'M11', 'quag sickness').
card_uid('quag sickness'/'M11', 'M11:Quag Sickness:quag sickness').
card_rarity('quag sickness'/'M11', 'Common').
card_artist('quag sickness'/'M11', 'Martina Pilcerova').
card_number('quag sickness'/'M11', '111').
card_flavor_text('quag sickness'/'M11', 'The dread gases didn\'t kill Farbid. But as he lay in the muck, miserable and helpless, watching ghouls and rats advance on him, he wished they had.').
card_multiverse_id('quag sickness'/'M11', '205028').

card_in_set('reassembling skeleton', 'M11').
card_original_type('reassembling skeleton'/'M11', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'M11', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_image_name('reassembling skeleton'/'M11', 'reassembling skeleton').
card_uid('reassembling skeleton'/'M11', 'M11:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'M11', 'Uncommon').
card_artist('reassembling skeleton'/'M11', 'Austin Hsu').
card_number('reassembling skeleton'/'M11', '112').
card_flavor_text('reassembling skeleton'/'M11', 'Though you may see the same bones, you\'ll never see the same skeleton twice.').
card_multiverse_id('reassembling skeleton'/'M11', '205066').

card_in_set('redirect', 'M11').
card_original_type('redirect'/'M11', 'Instant').
card_original_text('redirect'/'M11', 'You may choose new targets for target spell.').
card_first_print('redirect', 'M11').
card_image_name('redirect'/'M11', 'redirect').
card_uid('redirect'/'M11', 'M11:Redirect:redirect').
card_rarity('redirect'/'M11', 'Rare').
card_artist('redirect'/'M11', 'Izzy').
card_number('redirect'/'M11', '71').
card_flavor_text('redirect'/'M11', '\"Your strategy is marvelous. I\'ve just made a few minor adjustments.\"\n—Veris, watcher of Indi Keep').
card_multiverse_id('redirect'/'M11', '208227').

card_in_set('relentless rats', 'M11').
card_original_type('relentless rats'/'M11', 'Creature — Rat').
card_original_text('relentless rats'/'M11', 'Relentless Rats gets +1/+1 for each other creature on the battlefield named Relentless Rats.\nA deck can have any number of cards named Relentless Rats.').
card_image_name('relentless rats'/'M11', 'relentless rats').
card_uid('relentless rats'/'M11', 'M11:Relentless Rats:relentless rats').
card_rarity('relentless rats'/'M11', 'Uncommon').
card_artist('relentless rats'/'M11', 'Thomas M. Baxa').
card_number('relentless rats'/'M11', '113').
card_multiverse_id('relentless rats'/'M11', '205082').

card_in_set('reverberate', 'M11').
card_original_type('reverberate'/'M11', 'Instant').
card_original_text('reverberate'/'M11', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('reverberate', 'M11').
card_image_name('reverberate'/'M11', 'reverberate').
card_uid('reverberate'/'M11', 'M11:Reverberate:reverberate').
card_rarity('reverberate'/'M11', 'Rare').
card_artist('reverberate'/'M11', 'jD').
card_number('reverberate'/'M11', '155').
card_flavor_text('reverberate'/'M11', '\"I\'m not indecisive. I just don\'t like to limit my options.\"\n—Maxti, pyromancer').
card_multiverse_id('reverberate'/'M11', '205038').

card_in_set('rise from the grave', 'M11').
card_original_type('rise from the grave'/'M11', 'Sorcery').
card_original_text('rise from the grave'/'M11', 'Put target creature card in a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'M11', 'rise from the grave').
card_uid('rise from the grave'/'M11', 'M11:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'M11', 'Uncommon').
card_artist('rise from the grave'/'M11', 'Vance Kovacs').
card_number('rise from the grave'/'M11', '114').
card_flavor_text('rise from the grave'/'M11', '\"Death is no excuse for disobedience.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'M11', '205113').

card_in_set('roc egg', 'M11').
card_original_type('roc egg'/'M11', 'Creature — Bird').
card_original_text('roc egg'/'M11', 'Defender (This creature can\'t attack.)\nWhen Roc Egg is put into a graveyard from the battlefield, put a 3/3 white Bird creature token with flying onto the battlefield.').
card_first_print('roc egg', 'M11').
card_image_name('roc egg'/'M11', 'roc egg').
card_uid('roc egg'/'M11', 'M11:Roc Egg:roc egg').
card_rarity('roc egg'/'M11', 'Uncommon').
card_artist('roc egg'/'M11', 'Paul Bonner').
card_number('roc egg'/'M11', '25').
card_multiverse_id('roc egg'/'M11', '205055').

card_in_set('rootbound crag', 'M11').
card_original_type('rootbound crag'/'M11', 'Land').
card_original_text('rootbound crag'/'M11', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('rootbound crag'/'M11', 'rootbound crag').
card_uid('rootbound crag'/'M11', 'M11:Rootbound Crag:rootbound crag').
card_rarity('rootbound crag'/'M11', 'Rare').
card_artist('rootbound crag'/'M11', 'Matt Stewart').
card_number('rootbound crag'/'M11', '227').
card_multiverse_id('rootbound crag'/'M11', '205114').

card_in_set('rotting legion', 'M11').
card_original_type('rotting legion'/'M11', 'Creature — Zombie').
card_original_text('rotting legion'/'M11', 'Rotting Legion enters the battlefield tapped.').
card_first_print('rotting legion', 'M11').
card_image_name('rotting legion'/'M11', 'rotting legion').
card_uid('rotting legion'/'M11', 'M11:Rotting Legion:rotting legion').
card_rarity('rotting legion'/'M11', 'Common').
card_artist('rotting legion'/'M11', 'Carl Critchlow').
card_number('rotting legion'/'M11', '115').
card_flavor_text('rotting legion'/'M11', 'Zombies have one speed: shamble.').
card_multiverse_id('rotting legion'/'M11', '205047').

card_in_set('royal assassin', 'M11').
card_original_type('royal assassin'/'M11', 'Creature — Human Assassin').
card_original_text('royal assassin'/'M11', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'M11', 'royal assassin').
card_uid('royal assassin'/'M11', 'M11:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'M11', 'Rare').
card_artist('royal assassin'/'M11', 'Mark Zug').
card_number('royal assassin'/'M11', '116').
card_flavor_text('royal assassin'/'M11', 'An assassin is a king\'s most trusted courier, ensuring his messages are heard by even the most unwilling recipients.').
card_multiverse_id('royal assassin'/'M11', '205233').

card_in_set('runeclaw bear', 'M11').
card_original_type('runeclaw bear'/'M11', 'Creature — Bear').
card_original_text('runeclaw bear'/'M11', '').
card_image_name('runeclaw bear'/'M11', 'runeclaw bear').
card_uid('runeclaw bear'/'M11', 'M11:Runeclaw Bear:runeclaw bear').
card_rarity('runeclaw bear'/'M11', 'Common').
card_artist('runeclaw bear'/'M11', 'Jesper Ejsing').
card_number('runeclaw bear'/'M11', '195').
card_flavor_text('runeclaw bear'/'M11', '\"Nature has grown tired of the impositions of men and will begin to outfit its creations with tools for bloody instruction.\"\n—Hadi Kasten, Calla Dale naturalist').
card_multiverse_id('runeclaw bear'/'M11', '207337').

card_in_set('sacred wolf', 'M11').
card_original_type('sacred wolf'/'M11', 'Creature — Wolf').
card_original_text('sacred wolf'/'M11', 'Sacred Wolf can\'t be the target of spells or abilities your opponents control.').
card_first_print('sacred wolf', 'M11').
card_image_name('sacred wolf'/'M11', 'sacred wolf').
card_uid('sacred wolf'/'M11', 'M11:Sacred Wolf:sacred wolf').
card_rarity('sacred wolf'/'M11', 'Common').
card_artist('sacred wolf'/'M11', 'Matt Stewart').
card_number('sacred wolf'/'M11', '196').
card_flavor_text('sacred wolf'/'M11', '\"I raised my bow, and the wolf stared at me. Under its gaze, my finger would not release the string.\"\n—Aref the Hunter').
card_multiverse_id('sacred wolf'/'M11', '205023').

card_in_set('safe passage', 'M11').
card_original_type('safe passage'/'M11', 'Instant').
card_original_text('safe passage'/'M11', 'Prevent all damage that would be dealt to you and creatures you control this turn.').
card_image_name('safe passage'/'M11', 'safe passage').
card_uid('safe passage'/'M11', 'M11:Safe Passage:safe passage').
card_rarity('safe passage'/'M11', 'Common').
card_artist('safe passage'/'M11', 'Christopher Moeller').
card_number('safe passage'/'M11', '26').
card_flavor_text('safe passage'/'M11', 'With one flap of her wings, the angel beat back the fires of war.').
card_multiverse_id('safe passage'/'M11', '205115').

card_in_set('scroll thief', 'M11').
card_original_type('scroll thief'/'M11', 'Creature — Merfolk Rogue').
card_original_text('scroll thief'/'M11', 'Whenever Scroll Thief deals combat damage to a player, draw a card.').
card_first_print('scroll thief', 'M11').
card_image_name('scroll thief'/'M11', 'scroll thief').
card_uid('scroll thief'/'M11', 'M11:Scroll Thief:scroll thief').
card_rarity('scroll thief'/'M11', 'Common').
card_artist('scroll thief'/'M11', 'Alex Horley-Orlandelli').
card_number('scroll thief'/'M11', '72').
card_flavor_text('scroll thief'/'M11', 'I\'ve learned how to disable wards, pick locks, and decode the Agaran language—all before even reading the scroll!').
card_multiverse_id('scroll thief'/'M11', '205004').

card_in_set('serra angel', 'M11').
card_original_type('serra angel'/'M11', 'Creature — Angel').
card_original_text('serra angel'/'M11', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M11', 'serra angel').
card_uid('serra angel'/'M11', 'M11:Serra Angel:serra angel').
card_rarity('serra angel'/'M11', 'Uncommon').
card_artist('serra angel'/'M11', 'Greg Staples').
card_number('serra angel'/'M11', '27').
card_flavor_text('serra angel'/'M11', 'Perfect justice in service of a perfect will.').
card_multiverse_id('serra angel'/'M11', '205234').

card_in_set('serra ascendant', 'M11').
card_original_type('serra ascendant'/'M11', 'Creature — Human Monk').
card_original_text('serra ascendant'/'M11', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nAs long as you have 30 or more life, Serra Ascendant gets +5/+5 and has flying.').
card_first_print('serra ascendant', 'M11').
card_image_name('serra ascendant'/'M11', 'serra ascendant').
card_uid('serra ascendant'/'M11', 'M11:Serra Ascendant:serra ascendant').
card_rarity('serra ascendant'/'M11', 'Rare').
card_artist('serra ascendant'/'M11', 'Anthony Palumbo').
card_number('serra ascendant'/'M11', '28').
card_multiverse_id('serra ascendant'/'M11', '204989').

card_in_set('shiv\'s embrace', 'M11').
card_original_type('shiv\'s embrace'/'M11', 'Enchantment — Aura').
card_original_text('shiv\'s embrace'/'M11', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('shiv\'s embrace'/'M11', 'shiv\'s embrace').
card_uid('shiv\'s embrace'/'M11', 'M11:Shiv\'s Embrace:shiv\'s embrace').
card_rarity('shiv\'s embrace'/'M11', 'Uncommon').
card_artist('shiv\'s embrace'/'M11', 'Dave Kendall').
card_number('shiv\'s embrace'/'M11', '156').
card_multiverse_id('shiv\'s embrace'/'M11', '208002').

card_in_set('siege mastodon', 'M11').
card_original_type('siege mastodon'/'M11', 'Creature — Elephant').
card_original_text('siege mastodon'/'M11', '').
card_image_name('siege mastodon'/'M11', 'siege mastodon').
card_uid('siege mastodon'/'M11', 'M11:Siege Mastodon:siege mastodon').
card_rarity('siege mastodon'/'M11', 'Common').
card_artist('siege mastodon'/'M11', 'Matt Cavotta').
card_number('siege mastodon'/'M11', '29').
card_flavor_text('siege mastodon'/'M11', '\"The walls of the wicked will fall before us. Ready the siege engines. We proceed to war!\"\n—General Avitora').
card_multiverse_id('siege mastodon'/'M11', '208293').

card_in_set('sign in blood', 'M11').
card_original_type('sign in blood'/'M11', 'Sorcery').
card_original_text('sign in blood'/'M11', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'M11', 'sign in blood').
card_uid('sign in blood'/'M11', 'M11:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'M11', 'Common').
card_artist('sign in blood'/'M11', 'Howard Lyon').
card_number('sign in blood'/'M11', '117').
card_flavor_text('sign in blood'/'M11', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'M11', '205118').

card_in_set('silence', 'M11').
card_original_type('silence'/'M11', 'Instant').
card_original_text('silence'/'M11', 'Your opponents can\'t cast spells this turn. (Spells cast before this resolves are unaffected.)').
card_image_name('silence'/'M11', 'silence').
card_uid('silence'/'M11', 'M11:Silence:silence').
card_rarity('silence'/'M11', 'Rare').
card_artist('silence'/'M11', 'Wayne Reynolds').
card_number('silence'/'M11', '30').
card_flavor_text('silence'/'M11', '\"Take a quiet moment to reflect on your sins.\"\n—Cleph, Order of Redeemed Souls').
card_multiverse_id('silence'/'M11', '205119').

card_in_set('silvercoat lion', 'M11').
card_original_type('silvercoat lion'/'M11', 'Creature — Cat').
card_original_text('silvercoat lion'/'M11', '').
card_image_name('silvercoat lion'/'M11', 'silvercoat lion').
card_uid('silvercoat lion'/'M11', 'M11:Silvercoat Lion:silvercoat lion').
card_rarity('silvercoat lion'/'M11', 'Common').
card_artist('silvercoat lion'/'M11', 'Terese Nielsen').
card_number('silvercoat lion'/'M11', '31').
card_flavor_text('silvercoat lion'/'M11', '\"In the wild, white fur like mine is an aberration. We lack natural camouflage, but the inability to hide encourages other strengths.\"\n—Ajani Goldmane').
card_multiverse_id('silvercoat lion'/'M11', '208283').

card_in_set('sleep', 'M11').
card_original_type('sleep'/'M11', 'Sorcery').
card_original_text('sleep'/'M11', 'Tap all creatures target player controls. Those creatures don\'t untap during that player\'s next untap step.').
card_image_name('sleep'/'M11', 'sleep').
card_uid('sleep'/'M11', 'M11:Sleep:sleep').
card_rarity('sleep'/'M11', 'Uncommon').
card_artist('sleep'/'M11', 'Chris Rahn').
card_number('sleep'/'M11', '73').
card_flavor_text('sleep'/'M11', '\"I give them dreams so wondrous that they hesitate to return to the world of the conscious.\"\n—Garild, merfolk mage').
card_multiverse_id('sleep'/'M11', '205120').

card_in_set('solemn offering', 'M11').
card_original_type('solemn offering'/'M11', 'Sorcery').
card_original_text('solemn offering'/'M11', 'Destroy target artifact or enchantment. You gain 4 life.').
card_image_name('solemn offering'/'M11', 'solemn offering').
card_uid('solemn offering'/'M11', 'M11:Solemn Offering:solemn offering').
card_rarity('solemn offering'/'M11', 'Common').
card_artist('solemn offering'/'M11', 'Sam Wood').
card_number('solemn offering'/'M11', '32').
card_flavor_text('solemn offering'/'M11', '\"A relic donation is suggested.\"\n\"The suggestion is mandatory.\"\n—Temple signs').
card_multiverse_id('solemn offering'/'M11', '205095').

card_in_set('sorcerer\'s strongbox', 'M11').
card_original_type('sorcerer\'s strongbox'/'M11', 'Artifact').
card_original_text('sorcerer\'s strongbox'/'M11', '{2}, {T}: Flip a coin. If you win the flip, sacrifice Sorcerer\'s Strongbox and draw three cards.').
card_image_name('sorcerer\'s strongbox'/'M11', 'sorcerer\'s strongbox').
card_uid('sorcerer\'s strongbox'/'M11', 'M11:Sorcerer\'s Strongbox:sorcerer\'s strongbox').
card_rarity('sorcerer\'s strongbox'/'M11', 'Uncommon').
card_artist('sorcerer\'s strongbox'/'M11', 'Chuck Lukacs').
card_number('sorcerer\'s strongbox'/'M11', '213').
card_flavor_text('sorcerer\'s strongbox'/'M11', 'Simun the Quiet filled the chest with his most precious thoughts. But in a fit of paranoia, he locked up the memory of where he hid the key.').
card_multiverse_id('sorcerer\'s strongbox'/'M11', '205069').

card_in_set('spined wurm', 'M11').
card_original_type('spined wurm'/'M11', 'Creature — Wurm').
card_original_text('spined wurm'/'M11', '').
card_image_name('spined wurm'/'M11', 'spined wurm').
card_uid('spined wurm'/'M11', 'M11:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'M11', 'Common').
card_artist('spined wurm'/'M11', 'Carl Critchlow').
card_number('spined wurm'/'M11', '197').
card_flavor_text('spined wurm'/'M11', '\"They\'re as much like plants as animals. Leave them unchecked, and they\'ll choke out all other forms of life.\"\n—Hadi Kasten, Calla Dale naturalist').
card_multiverse_id('spined wurm'/'M11', '205123').

card_in_set('squadron hawk', 'M11').
card_original_type('squadron hawk'/'M11', 'Creature — Bird').
card_original_text('squadron hawk'/'M11', 'Flying\nWhen Squadron Hawk enters the battlefield, you may search your library for up to three cards named Squadron Hawk, reveal them, put them into your hand, then shuffle your library.').
card_image_name('squadron hawk'/'M11', 'squadron hawk').
card_uid('squadron hawk'/'M11', 'M11:Squadron Hawk:squadron hawk').
card_rarity('squadron hawk'/'M11', 'Common').
card_artist('squadron hawk'/'M11', 'Rob Alexander').
card_number('squadron hawk'/'M11', '33').
card_multiverse_id('squadron hawk'/'M11', '208279').

card_in_set('stabbing pain', 'M11').
card_original_type('stabbing pain'/'M11', 'Instant').
card_original_text('stabbing pain'/'M11', 'Target creature gets -1/-1 until end of turn. Tap that creature.').
card_first_print('stabbing pain', 'M11').
card_image_name('stabbing pain'/'M11', 'stabbing pain').
card_uid('stabbing pain'/'M11', 'M11:Stabbing Pain:stabbing pain').
card_rarity('stabbing pain'/'M11', 'Common').
card_artist('stabbing pain'/'M11', 'Zoltan Boros & Gabor Szikszai').
card_number('stabbing pain'/'M11', '118').
card_flavor_text('stabbing pain'/'M11', '\"You can take the fight out of nearly any creature if you know where its soft spot is.\"\n—Guttor, flesh-warper').
card_multiverse_id('stabbing pain'/'M11', '205048').

card_in_set('steel overseer', 'M11').
card_original_type('steel overseer'/'M11', 'Artifact Creature — Construct').
card_original_text('steel overseer'/'M11', '{T}: Put a +1/+1 counter on each artifact creature you control.').
card_first_print('steel overseer', 'M11').
card_image_name('steel overseer'/'M11', 'steel overseer').
card_uid('steel overseer'/'M11', 'M11:Steel Overseer:steel overseer').
card_rarity('steel overseer'/'M11', 'Rare').
card_artist('steel overseer'/'M11', 'Chris Rahn').
card_number('steel overseer'/'M11', '214').
card_flavor_text('steel overseer'/'M11', '\"The world is already run by all manner of machines. One day, they\'ll remind us of that fact.\"\n—Sargis Haz, artificer').
card_multiverse_id('steel overseer'/'M11', '205036').

card_in_set('stone golem', 'M11').
card_original_type('stone golem'/'M11', 'Artifact Creature — Golem').
card_original_text('stone golem'/'M11', '').
card_first_print('stone golem', 'M11').
card_image_name('stone golem'/'M11', 'stone golem').
card_uid('stone golem'/'M11', 'M11:Stone Golem:stone golem').
card_rarity('stone golem'/'M11', 'Uncommon').
card_artist('stone golem'/'M11', 'Martina Pilcerova').
card_number('stone golem'/'M11', '215').
card_flavor_text('stone golem'/'M11', 'The sculptor, like most artists, put his heart and soul in his work. But the newly awakened golem decided he wanted his creator\'s other, more tangible parts.').
card_multiverse_id('stone golem'/'M11', '205056').

card_in_set('stormfront pegasus', 'M11').
card_original_type('stormfront pegasus'/'M11', 'Creature — Pegasus').
card_original_text('stormfront pegasus'/'M11', 'Flying').
card_image_name('stormfront pegasus'/'M11', 'stormfront pegasus').
card_uid('stormfront pegasus'/'M11', 'M11:Stormfront Pegasus:stormfront pegasus').
card_rarity('stormfront pegasus'/'M11', 'Common').
card_artist('stormfront pegasus'/'M11', 'rk post').
card_number('stormfront pegasus'/'M11', '34').
card_flavor_text('stormfront pegasus'/'M11', '\"At summer\'s end, the pegasus herd stampedes across the sky. Their silent footfalls taunt the clouds and bid the rains to come.\"\n—Stormfront fable').
card_multiverse_id('stormfront pegasus'/'M11', '208294').

card_in_set('stormtide leviathan', 'M11').
card_original_type('stormtide leviathan'/'M11', 'Creature — Leviathan').
card_original_text('stormtide leviathan'/'M11', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nAll lands are Islands in addition to their other types.\nCreatures without flying or islandwalk can\'t attack.').
card_first_print('stormtide leviathan', 'M11').
card_image_name('stormtide leviathan'/'M11', 'stormtide leviathan').
card_uid('stormtide leviathan'/'M11', 'M11:Stormtide Leviathan:stormtide leviathan').
card_rarity('stormtide leviathan'/'M11', 'Rare').
card_artist('stormtide leviathan'/'M11', 'Karl Kopinski').
card_number('stormtide leviathan'/'M11', '74').
card_multiverse_id('stormtide leviathan'/'M11', '205029').

card_in_set('sun titan', 'M11').
card_original_type('sun titan'/'M11', 'Creature — Giant').
card_original_text('sun titan'/'M11', 'Vigilance\nWhenever Sun Titan enters the battlefield or attacks, you may return target permanent card with converted mana cost 3 or less from your graveyard to the battlefield.').
card_image_name('sun titan'/'M11', 'sun titan').
card_uid('sun titan'/'M11', 'M11:Sun Titan:sun titan').
card_rarity('sun titan'/'M11', 'Mythic Rare').
card_artist('sun titan'/'M11', 'Todd Lockwood').
card_number('sun titan'/'M11', '35').
card_flavor_text('sun titan'/'M11', 'A blazing sun that never sets.').
card_multiverse_id('sun titan'/'M11', '205030').

card_in_set('sunpetal grove', 'M11').
card_original_type('sunpetal grove'/'M11', 'Land').
card_original_text('sunpetal grove'/'M11', 'Sunpetal Grove enters the battlefield tapped unless you control a Forest or a Plains.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('sunpetal grove'/'M11', 'sunpetal grove').
card_uid('sunpetal grove'/'M11', 'M11:Sunpetal Grove:sunpetal grove').
card_rarity('sunpetal grove'/'M11', 'Rare').
card_artist('sunpetal grove'/'M11', 'Jason Chan').
card_number('sunpetal grove'/'M11', '228').
card_multiverse_id('sunpetal grove'/'M11', '205124').

card_in_set('swamp', 'M11').
card_original_type('swamp'/'M11', 'Basic Land — Swamp').
card_original_text('swamp'/'M11', 'B').
card_image_name('swamp'/'M11', 'swamp1').
card_uid('swamp'/'M11', 'M11:Swamp:swamp1').
card_rarity('swamp'/'M11', 'Basic Land').
card_artist('swamp'/'M11', 'John Avon').
card_number('swamp'/'M11', '238').
card_multiverse_id('swamp'/'M11', '213621').

card_in_set('swamp', 'M11').
card_original_type('swamp'/'M11', 'Basic Land — Swamp').
card_original_text('swamp'/'M11', 'B').
card_image_name('swamp'/'M11', 'swamp2').
card_uid('swamp'/'M11', 'M11:Swamp:swamp2').
card_rarity('swamp'/'M11', 'Basic Land').
card_artist('swamp'/'M11', 'Larry Elmore').
card_number('swamp'/'M11', '239').
card_multiverse_id('swamp'/'M11', '213620').

card_in_set('swamp', 'M11').
card_original_type('swamp'/'M11', 'Basic Land — Swamp').
card_original_text('swamp'/'M11', 'B').
card_image_name('swamp'/'M11', 'swamp3').
card_uid('swamp'/'M11', 'M11:Swamp:swamp3').
card_rarity('swamp'/'M11', 'Basic Land').
card_artist('swamp'/'M11', 'Alex Horley-Orlandelli').
card_number('swamp'/'M11', '240').
card_multiverse_id('swamp'/'M11', '213622').

card_in_set('swamp', 'M11').
card_original_type('swamp'/'M11', 'Basic Land — Swamp').
card_original_text('swamp'/'M11', 'B').
card_image_name('swamp'/'M11', 'swamp4').
card_uid('swamp'/'M11', 'M11:Swamp:swamp4').
card_rarity('swamp'/'M11', 'Basic Land').
card_artist('swamp'/'M11', 'Jim Pavelec').
card_number('swamp'/'M11', '241').
card_multiverse_id('swamp'/'M11', '213623').

card_in_set('sword of vengeance', 'M11').
card_original_type('sword of vengeance'/'M11', 'Artifact — Equipment').
card_original_text('sword of vengeance'/'M11', 'Equipped creature gets +2/+0 and has first strike, vigilance, trample, and haste.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('sword of vengeance', 'M11').
card_image_name('sword of vengeance'/'M11', 'sword of vengeance').
card_uid('sword of vengeance'/'M11', 'M11:Sword of Vengeance:sword of vengeance').
card_rarity('sword of vengeance'/'M11', 'Rare').
card_artist('sword of vengeance'/'M11', 'Dan Scott').
card_number('sword of vengeance'/'M11', '216').
card_flavor_text('sword of vengeance'/'M11', 'When wielded by a true believer, it matters little whether the sword is a relic or a replica.').
card_multiverse_id('sword of vengeance'/'M11', '205044').

card_in_set('sylvan ranger', 'M11').
card_original_type('sylvan ranger'/'M11', 'Creature — Elf Scout').
card_original_text('sylvan ranger'/'M11', 'When Sylvan Ranger enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('sylvan ranger'/'M11', 'sylvan ranger').
card_uid('sylvan ranger'/'M11', 'M11:Sylvan Ranger:sylvan ranger').
card_rarity('sylvan ranger'/'M11', 'Common').
card_artist('sylvan ranger'/'M11', 'Christopher Moeller').
card_number('sylvan ranger'/'M11', '198').
card_flavor_text('sylvan ranger'/'M11', '\"Not all paths are found on the forest floor.\"').
card_multiverse_id('sylvan ranger'/'M11', '204976').

card_in_set('temple bell', 'M11').
card_original_type('temple bell'/'M11', 'Artifact').
card_original_text('temple bell'/'M11', '{T}: Each player draws a card.').
card_first_print('temple bell', 'M11').
card_image_name('temple bell'/'M11', 'temple bell').
card_uid('temple bell'/'M11', 'M11:Temple Bell:temple bell').
card_rarity('temple bell'/'M11', 'Rare').
card_artist('temple bell'/'M11', 'Mark Tedin').
card_number('temple bell'/'M11', '217').
card_flavor_text('temple bell'/'M11', 'Enlightenment is not gained by striking the bell or hearing its toll. It\'s found in the silence that follows.').
card_multiverse_id('temple bell'/'M11', '205058').

card_in_set('terramorphic expanse', 'M11').
card_original_type('terramorphic expanse'/'M11', 'Land').
card_original_text('terramorphic expanse'/'M11', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'M11', 'terramorphic expanse').
card_uid('terramorphic expanse'/'M11', 'M11:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'M11', 'Common').
card_artist('terramorphic expanse'/'M11', 'Dan Scott').
card_number('terramorphic expanse'/'M11', '229').
card_multiverse_id('terramorphic expanse'/'M11', '205236').

card_in_set('thunder strike', 'M11').
card_original_type('thunder strike'/'M11', 'Instant').
card_original_text('thunder strike'/'M11', 'Target creature gets +2/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_first_print('thunder strike', 'M11').
card_image_name('thunder strike'/'M11', 'thunder strike').
card_uid('thunder strike'/'M11', 'M11:Thunder Strike:thunder strike').
card_rarity('thunder strike'/'M11', 'Common').
card_artist('thunder strike'/'M11', 'Wayne Reynolds').
card_number('thunder strike'/'M11', '157').
card_flavor_text('thunder strike'/'M11', 'Lightning reflexes, thunderous might.').
card_multiverse_id('thunder strike'/'M11', '204973').

card_in_set('time reversal', 'M11').
card_original_type('time reversal'/'M11', 'Sorcery').
card_original_text('time reversal'/'M11', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. Exile Time Reversal.').
card_first_print('time reversal', 'M11').
card_image_name('time reversal'/'M11', 'time reversal').
card_uid('time reversal'/'M11', 'M11:Time Reversal:time reversal').
card_rarity('time reversal'/'M11', 'Mythic Rare').
card_artist('time reversal'/'M11', 'Howard Lyon').
card_number('time reversal'/'M11', '75').
card_flavor_text('time reversal'/'M11', '\"Optimists see the world as they wish it to be. Pessimists see the world as it is. Must these roles be mutually exclusive?\"\n—Evo Ragus').
card_multiverse_id('time reversal'/'M11', '205022').

card_in_set('tireless missionaries', 'M11').
card_original_type('tireless missionaries'/'M11', 'Creature — Human Cleric').
card_original_text('tireless missionaries'/'M11', 'When Tireless Missionaries enters the battlefield, you gain 3 life.').
card_first_print('tireless missionaries', 'M11').
card_image_name('tireless missionaries'/'M11', 'tireless missionaries').
card_uid('tireless missionaries'/'M11', 'M11:Tireless Missionaries:tireless missionaries').
card_rarity('tireless missionaries'/'M11', 'Common').
card_artist('tireless missionaries'/'M11', 'Dave Kendall').
card_number('tireless missionaries'/'M11', '36').
card_flavor_text('tireless missionaries'/'M11', 'If they succeed in their holy work, their order will vanish into welcome obscurity, for there will be no more souls to redeem.').
card_multiverse_id('tireless missionaries'/'M11', '205014').

card_in_set('tome scour', 'M11').
card_original_type('tome scour'/'M11', 'Sorcery').
card_original_text('tome scour'/'M11', 'Target player puts the top five cards of his or her library into his or her graveyard.').
card_image_name('tome scour'/'M11', 'tome scour').
card_uid('tome scour'/'M11', 'M11:Tome Scour:tome scour').
card_rarity('tome scour'/'M11', 'Common').
card_artist('tome scour'/'M11', 'Steven Belledin').
card_number('tome scour'/'M11', '76').
card_flavor_text('tome scour'/'M11', 'Genius is overrated, especially when it\'s someone else\'s.').
card_multiverse_id('tome scour'/'M11', '205000').

card_in_set('traumatize', 'M11').
card_original_type('traumatize'/'M11', 'Sorcery').
card_original_text('traumatize'/'M11', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_image_name('traumatize'/'M11', 'traumatize').
card_uid('traumatize'/'M11', 'M11:Traumatize:traumatize').
card_rarity('traumatize'/'M11', 'Rare').
card_artist('traumatize'/'M11', 'Greg Staples').
card_number('traumatize'/'M11', '77').
card_flavor_text('traumatize'/'M11', 'He was left with just enough memory to understand what he had lost.').
card_multiverse_id('traumatize'/'M11', '208221').

card_in_set('triskelion', 'M11').
card_original_type('triskelion'/'M11', 'Artifact Creature — Construct').
card_original_text('triskelion'/'M11', 'Triskelion enters the battlefield with three +1/+1 counters on it.\nRemove a +1/+1 counter from Triskelion: Triskelion deals 1 damage to target creature or player.').
card_image_name('triskelion'/'M11', 'triskelion').
card_uid('triskelion'/'M11', 'M11:Triskelion:triskelion').
card_rarity('triskelion'/'M11', 'Rare').
card_artist('triskelion'/'M11', 'Christopher Moeller').
card_number('triskelion'/'M11', '218').
card_flavor_text('triskelion'/'M11', '\"Why do bad things always come in threes?\"\n—Gisulf, expedition survivor').
card_multiverse_id('triskelion'/'M11', '206719').

card_in_set('unholy strength', 'M11').
card_original_type('unholy strength'/'M11', 'Enchantment — Aura').
card_original_text('unholy strength'/'M11', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'M11', 'unholy strength').
card_uid('unholy strength'/'M11', 'M11:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'M11', 'Common').
card_artist('unholy strength'/'M11', 'Terese Nielsen').
card_number('unholy strength'/'M11', '119').
card_flavor_text('unholy strength'/'M11', '\"Born under the moon, the second child will stumble on temptation and seek power in the dark places of the heart.\"\n—Codex of the Constellari').
card_multiverse_id('unholy strength'/'M11', '205237').

card_in_set('unsummon', 'M11').
card_original_type('unsummon'/'M11', 'Instant').
card_original_text('unsummon'/'M11', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'M11', 'unsummon').
card_uid('unsummon'/'M11', 'M11:Unsummon:unsummon').
card_rarity('unsummon'/'M11', 'Common').
card_artist('unsummon'/'M11', 'Izzy').
card_number('unsummon'/'M11', '78').
card_flavor_text('unsummon'/'M11', '\"You\'re trespassing in my reality. Ask permission next time.\"\n—Simun the Quiet').
card_multiverse_id('unsummon'/'M11', '208223').

card_in_set('vengeful archon', 'M11').
card_original_type('vengeful archon'/'M11', 'Creature — Archon').
card_original_text('vengeful archon'/'M11', 'Flying\n{X}: Prevent the next X damage that would be dealt to you this turn. If damage is prevented this way, Vengeful Archon deals that much damage to target player.').
card_first_print('vengeful archon', 'M11').
card_image_name('vengeful archon'/'M11', 'vengeful archon').
card_uid('vengeful archon'/'M11', 'M11:Vengeful Archon:vengeful archon').
card_rarity('vengeful archon'/'M11', 'Rare').
card_artist('vengeful archon'/'M11', 'Greg Staples').
card_number('vengeful archon'/'M11', '37').
card_multiverse_id('vengeful archon'/'M11', '208288').

card_in_set('viscera seer', 'M11').
card_original_type('viscera seer'/'M11', 'Creature — Vampire Wizard').
card_original_text('viscera seer'/'M11', 'Sacrifice a creature: Scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('viscera seer', 'M11').
card_image_name('viscera seer'/'M11', 'viscera seer').
card_uid('viscera seer'/'M11', 'M11:Viscera Seer:viscera seer').
card_rarity('viscera seer'/'M11', 'Common').
card_artist('viscera seer'/'M11', 'John Stanko').
card_number('viscera seer'/'M11', '120').
card_flavor_text('viscera seer'/'M11', 'In matters of life and death, he trusts his gut.').
card_multiverse_id('viscera seer'/'M11', '205232').

card_in_set('volcanic strength', 'M11').
card_original_type('volcanic strength'/'M11', 'Enchantment — Aura').
card_original_text('volcanic strength'/'M11', 'Enchant creature\nEnchanted creature gets +2/+2 and has mountainwalk. (It\'s unblockable as long as defending player controls a Mountain.)').
card_first_print('volcanic strength', 'M11').
card_image_name('volcanic strength'/'M11', 'volcanic strength').
card_uid('volcanic strength'/'M11', 'M11:Volcanic Strength:volcanic strength').
card_rarity('volcanic strength'/'M11', 'Common').
card_artist('volcanic strength'/'M11', 'Izzy').
card_number('volcanic strength'/'M11', '158').
card_flavor_text('volcanic strength'/'M11', 'His blood boiled over, and he erupted with fists of stone.').
card_multiverse_id('volcanic strength'/'M11', '208004').

card_in_set('voltaic key', 'M11').
card_original_type('voltaic key'/'M11', 'Artifact').
card_original_text('voltaic key'/'M11', '{1}, {T}: Untap target artifact.').
card_image_name('voltaic key'/'M11', 'voltaic key').
card_uid('voltaic key'/'M11', 'M11:Voltaic Key:voltaic key').
card_rarity('voltaic key'/'M11', 'Uncommon').
card_artist('voltaic key'/'M11', 'Franz Vohwinkel').
card_number('voltaic key'/'M11', '219').
card_flavor_text('voltaic key'/'M11', 'The missing piece to every puzzle.').
card_multiverse_id('voltaic key'/'M11', '206332').

card_in_set('vulshok berserker', 'M11').
card_original_type('vulshok berserker'/'M11', 'Creature — Human Berserker').
card_original_text('vulshok berserker'/'M11', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('vulshok berserker'/'M11', 'vulshok berserker').
card_uid('vulshok berserker'/'M11', 'M11:Vulshok Berserker:vulshok berserker').
card_rarity('vulshok berserker'/'M11', 'Common').
card_artist('vulshok berserker'/'M11', 'Pete Venters').
card_number('vulshok berserker'/'M11', '159').
card_flavor_text('vulshok berserker'/'M11', 'He will carve out a legend for himself with his own two hands.').
card_multiverse_id('vulshok berserker'/'M11', '208011').

card_in_set('wall of frost', 'M11').
card_original_type('wall of frost'/'M11', 'Creature — Wall').
card_original_text('wall of frost'/'M11', 'Defender (This creature can\'t attack.)\nWhenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('wall of frost'/'M11', 'wall of frost').
card_uid('wall of frost'/'M11', 'M11:Wall of Frost:wall of frost').
card_rarity('wall of frost'/'M11', 'Uncommon').
card_artist('wall of frost'/'M11', 'Mike Bierek').
card_number('wall of frost'/'M11', '79').
card_flavor_text('wall of frost'/'M11', '\"I welcome it. The pasture is cleared of weeds and the wolves are frozen solid.\"\n—Lumi, goatherd').
card_multiverse_id('wall of frost'/'M11', '205128').

card_in_set('wall of vines', 'M11').
card_original_type('wall of vines'/'M11', 'Creature — Plant Wall').
card_original_text('wall of vines'/'M11', 'Defender (This creature can\'t attack.)\nReach (This creature can block creatures with flying.)').
card_first_print('wall of vines', 'M11').
card_image_name('wall of vines'/'M11', 'wall of vines').
card_uid('wall of vines'/'M11', 'M11:Wall of Vines:wall of vines').
card_rarity('wall of vines'/'M11', 'Common').
card_artist('wall of vines'/'M11', 'John Stanko').
card_number('wall of vines'/'M11', '199').
card_flavor_text('wall of vines'/'M11', 'Like all jungle plants, the vines must fight and claw for sunlight. Once their place is secured, they grow strong, sharp, and impenetrable.').
card_multiverse_id('wall of vines'/'M11', '204983').

card_in_set('war priest of thune', 'M11').
card_original_type('war priest of thune'/'M11', 'Creature — Human Cleric').
card_original_text('war priest of thune'/'M11', 'When War Priest of Thune enters the battlefield, you may destroy target enchantment.').
card_first_print('war priest of thune', 'M11').
card_image_name('war priest of thune'/'M11', 'war priest of thune').
card_uid('war priest of thune'/'M11', 'M11:War Priest of Thune:war priest of thune').
card_rarity('war priest of thune'/'M11', 'Uncommon').
card_artist('war priest of thune'/'M11', 'Izzy').
card_number('war priest of thune'/'M11', '38').
card_flavor_text('war priest of thune'/'M11', '\"Let nothing take away from the purity of swords clashing or of arrows taking flight toward the breast of evil.\"').
card_multiverse_id('war priest of thune'/'M11', '208274').

card_in_set('warlord\'s axe', 'M11').
card_original_type('warlord\'s axe'/'M11', 'Artifact — Equipment').
card_original_text('warlord\'s axe'/'M11', 'Equipped creature gets +3/+1.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('warlord\'s axe', 'M11').
card_image_name('warlord\'s axe'/'M11', 'warlord\'s axe').
card_uid('warlord\'s axe'/'M11', 'M11:Warlord\'s Axe:warlord\'s axe').
card_rarity('warlord\'s axe'/'M11', 'Uncommon').
card_artist('warlord\'s axe'/'M11', 'Franz Vohwinkel').
card_number('warlord\'s axe'/'M11', '220').
card_flavor_text('warlord\'s axe'/'M11', 'To split wood with it would be sacrilege. This tool has but one purpose, and that is war.').
card_multiverse_id('warlord\'s axe'/'M11', '205057').

card_in_set('water servant', 'M11').
card_original_type('water servant'/'M11', 'Creature — Elemental').
card_original_text('water servant'/'M11', '{U}: Water Servant gets +1/-1 until end of turn.\n{U}: Water Servant gets -1/+1 until end of turn.').
card_first_print('water servant', 'M11').
card_image_name('water servant'/'M11', 'water servant').
card_uid('water servant'/'M11', 'M11:Water Servant:water servant').
card_rarity('water servant'/'M11', 'Uncommon').
card_artist('water servant'/'M11', 'Igor Kieryluk').
card_number('water servant'/'M11', '80').
card_flavor_text('water servant'/'M11', '\"This creature has innate perceptiveness. It knows when to rise and when to vanish into the tides.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('water servant'/'M11', '205006').

card_in_set('whispersilk cloak', 'M11').
card_original_type('whispersilk cloak'/'M11', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'M11', 'Equipped creature is unblockable.\nEquipped creature has shroud. (It can\'t be the target of spells or abilities.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('whispersilk cloak'/'M11', 'whispersilk cloak').
card_uid('whispersilk cloak'/'M11', 'M11:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'M11', 'Uncommon').
card_artist('whispersilk cloak'/'M11', 'Daren Bader').
card_number('whispersilk cloak'/'M11', '221').
card_multiverse_id('whispersilk cloak'/'M11', '206342').

card_in_set('white knight', 'M11').
card_original_type('white knight'/'M11', 'Creature — Human Knight').
card_original_text('white knight'/'M11', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black.)').
card_image_name('white knight'/'M11', 'white knight').
card_uid('white knight'/'M11', 'M11:White Knight:white knight').
card_rarity('white knight'/'M11', 'Uncommon').
card_artist('white knight'/'M11', 'Christopher Moeller').
card_number('white knight'/'M11', '39').
card_multiverse_id('white knight'/'M11', '205239').

card_in_set('wild evocation', 'M11').
card_original_type('wild evocation'/'M11', 'Enchantment').
card_original_text('wild evocation'/'M11', 'At the beginning of each player\'s upkeep, that player reveals a card at random from his or her hand. If it\'s a land card, the player puts it onto the battlefield. Otherwise, the player casts it without paying its mana cost if able.').
card_first_print('wild evocation', 'M11').
card_image_name('wild evocation'/'M11', 'wild evocation').
card_uid('wild evocation'/'M11', 'M11:Wild Evocation:wild evocation').
card_rarity('wild evocation'/'M11', 'Rare').
card_artist('wild evocation'/'M11', 'Chippy').
card_number('wild evocation'/'M11', '160').
card_multiverse_id('wild evocation'/'M11', '204972').

card_in_set('wild griffin', 'M11').
card_original_type('wild griffin'/'M11', 'Creature — Griffin').
card_original_text('wild griffin'/'M11', 'Flying').
card_image_name('wild griffin'/'M11', 'wild griffin').
card_uid('wild griffin'/'M11', 'M11:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'M11', 'Common').
card_artist('wild griffin'/'M11', 'Matt Cavotta').
card_number('wild griffin'/'M11', '40').
card_flavor_text('wild griffin'/'M11', 'Farmers typically keep their flocks at thirteen sheep—one to distract the griffins while they take the other twelve to market.').
card_multiverse_id('wild griffin'/'M11', '208275').

card_in_set('wurm\'s tooth', 'M11').
card_original_type('wurm\'s tooth'/'M11', 'Artifact').
card_original_text('wurm\'s tooth'/'M11', 'Whenever a player casts a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'M11', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'M11', 'M11:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'M11', 'Uncommon').
card_artist('wurm\'s tooth'/'M11', 'Alan Pollack').
card_number('wurm\'s tooth'/'M11', '222').
card_flavor_text('wurm\'s tooth'/'M11', '\"The great wurm bit down on me. Instead of being crunched bloody, I slid into the space where its tooth was missing. One tooth—a boon most potent, indeed.\"\n—The Tall Tales of Oleander').
card_multiverse_id('wurm\'s tooth'/'M11', '206330').

card_in_set('yavimaya wurm', 'M11').
card_original_type('yavimaya wurm'/'M11', 'Creature — Wurm').
card_original_text('yavimaya wurm'/'M11', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('yavimaya wurm'/'M11', 'yavimaya wurm').
card_uid('yavimaya wurm'/'M11', 'M11:Yavimaya Wurm:yavimaya wurm').
card_rarity('yavimaya wurm'/'M11', 'Common').
card_artist('yavimaya wurm'/'M11', 'Steven Belledin').
card_number('yavimaya wurm'/'M11', '200').
card_flavor_text('yavimaya wurm'/'M11', 'Anywhere else, colossal. In the forests of Yavimaya, commonplace.').
card_multiverse_id('yavimaya wurm'/'M11', '205221').
