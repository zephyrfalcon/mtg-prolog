% Duel Decks: Jace vs. Chandra

set('DD2').
set_name('DD2', 'Duel Decks: Jace vs. Chandra').
set_release_date('DD2', '2008-11-07').
set_border('DD2', 'black').
set_type('DD2', 'duel deck').

card_in_set('æthersnipe', 'DD2').
card_original_type('æthersnipe'/'DD2', 'Creature — Elemental').
card_original_text('æthersnipe'/'DD2', 'When Æthersnipe comes into play, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('æthersnipe'/'DD2', 'aethersnipe').
card_uid('æthersnipe'/'DD2', 'DD2:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'DD2', 'Common').
card_artist('æthersnipe'/'DD2', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'DD2', '17').
card_multiverse_id('æthersnipe'/'DD2', '189211').

card_in_set('air elemental', 'DD2').
card_original_type('air elemental'/'DD2', 'Creature — Elemental').
card_original_text('air elemental'/'DD2', 'Flying').
card_image_name('air elemental'/'DD2', 'air elemental').
card_uid('air elemental'/'DD2', 'DD2:Air Elemental:air elemental').
card_rarity('air elemental'/'DD2', 'Uncommon').
card_artist('air elemental'/'DD2', 'Kev Walker').
card_number('air elemental'/'DD2', '13').
card_flavor_text('air elemental'/'DD2', 'Pray that it doesn\'t seek the safety of your lungs.').
card_multiverse_id('air elemental'/'DD2', '189223').

card_in_set('ancestral vision', 'DD2').
card_original_type('ancestral vision'/'DD2', 'Sorcery').
card_original_text('ancestral vision'/'DD2', 'Ancestral Vision is blue.\nSuspend 4—{U} (Rather than play this card from your hand, pay {U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost.)\nTarget player draws three cards.').
card_image_name('ancestral vision'/'DD2', 'ancestral vision').
card_uid('ancestral vision'/'DD2', 'DD2:Ancestral Vision:ancestral vision').
card_rarity('ancestral vision'/'DD2', 'Rare').
card_artist('ancestral vision'/'DD2', 'Mark Poole').
card_number('ancestral vision'/'DD2', '21').
card_multiverse_id('ancestral vision'/'DD2', '189244').

card_in_set('bottle gnomes', 'DD2').
card_original_type('bottle gnomes'/'DD2', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'DD2', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'DD2', 'bottle gnomes').
card_uid('bottle gnomes'/'DD2', 'DD2:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'DD2', 'Uncommon').
card_artist('bottle gnomes'/'DD2', 'Ben Thompson').
card_number('bottle gnomes'/'DD2', '7').
card_flavor_text('bottle gnomes'/'DD2', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'DD2', '189224').

card_in_set('brine elemental', 'DD2').
card_original_type('brine elemental'/'DD2', 'Creature — Elemental').
card_original_text('brine elemental'/'DD2', 'Morph {5}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Brine Elemental is turned face up, each opponent skips his or her next untap step.').
card_image_name('brine elemental'/'DD2', 'brine elemental').
card_uid('brine elemental'/'DD2', 'DD2:Brine Elemental:brine elemental').
card_rarity('brine elemental'/'DD2', 'Uncommon').
card_artist('brine elemental'/'DD2', 'Stephen Tappin').
card_number('brine elemental'/'DD2', '18').
card_flavor_text('brine elemental'/'DD2', 'Water calls to water, and the world is left exhausted and withered in its wake.').
card_multiverse_id('brine elemental'/'DD2', '189245').

card_in_set('chandra nalaar', 'DD2').
card_original_type('chandra nalaar'/'DD2', 'Planeswalker — Chandra').
card_original_text('chandra nalaar'/'DD2', '+1: Chandra Nalaar deals 1 damage to target player.\n-X: Chandra Nalaar deals X damage to target creature.\n-8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_image_name('chandra nalaar'/'DD2', 'chandra nalaar').
card_uid('chandra nalaar'/'DD2', 'DD2:Chandra Nalaar:chandra nalaar').
card_rarity('chandra nalaar'/'DD2', 'Mythic Rare').
card_artist('chandra nalaar'/'DD2', 'Kev Walker').
card_number('chandra nalaar'/'DD2', '34').
card_multiverse_id('chandra nalaar'/'DD2', '185815').

card_in_set('chartooth cougar', 'DD2').
card_original_type('chartooth cougar'/'DD2', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'DD2', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'DD2', 'chartooth cougar').
card_uid('chartooth cougar'/'DD2', 'DD2:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'DD2', 'Common').
card_artist('chartooth cougar'/'DD2', 'Tony Szczudlo').
card_number('chartooth cougar'/'DD2', '47').
card_multiverse_id('chartooth cougar'/'DD2', '189235').

card_in_set('condescend', 'DD2').
card_original_type('condescend'/'DD2', 'Instant').
card_original_text('condescend'/'DD2', 'Counter target spell unless its controller pays {X}.\nScry 2. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('condescend'/'DD2', 'condescend').
card_uid('condescend'/'DD2', 'DD2:Condescend:condescend').
card_rarity('condescend'/'DD2', 'Common').
card_artist('condescend'/'DD2', 'Ron Spears').
card_number('condescend'/'DD2', '28').
card_multiverse_id('condescend'/'DD2', '189237').

card_in_set('cone of flame', 'DD2').
card_original_type('cone of flame'/'DD2', 'Sorcery').
card_original_text('cone of flame'/'DD2', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'DD2', 'cone of flame').
card_uid('cone of flame'/'DD2', 'DD2:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'DD2', 'Uncommon').
card_artist('cone of flame'/'DD2', 'Chippy').
card_number('cone of flame'/'DD2', '54').
card_multiverse_id('cone of flame'/'DD2', '189225').

card_in_set('counterspell', 'DD2').
card_original_type('counterspell'/'DD2', 'Instant').
card_original_text('counterspell'/'DD2', 'Counter target spell.').
card_image_name('counterspell'/'DD2', 'counterspell').
card_uid('counterspell'/'DD2', 'DD2:Counterspell:counterspell').
card_rarity('counterspell'/'DD2', 'Common').
card_artist('counterspell'/'DD2', 'Jason Chan').
card_number('counterspell'/'DD2', '24').
card_flavor_text('counterspell'/'DD2', 'The pyromancer summoned up her mightiest onslaught of fire and rage. Jace feigned interest.').
card_multiverse_id('counterspell'/'DD2', '185820').

card_in_set('daze', 'DD2').
card_original_type('daze'/'DD2', 'Instant').
card_original_text('daze'/'DD2', 'You may return an Island you control to its owner\'s hand rather than pay Daze\'s mana cost.\nCounter target spell unless its controller pays {1}.').
card_image_name('daze'/'DD2', 'daze').
card_uid('daze'/'DD2', 'DD2:Daze:daze').
card_rarity('daze'/'DD2', 'Common').
card_artist('daze'/'DD2', 'Matthew D. Wilson').
card_number('daze'/'DD2', '23').
card_multiverse_id('daze'/'DD2', '189255').

card_in_set('demonfire', 'DD2').
card_original_type('demonfire'/'DD2', 'Sorcery').
card_original_text('demonfire'/'DD2', 'Demonfire deals X damage to target creature or player. If a creature dealt damage this way would be put into a graveyard this turn, remove it from the game instead.\nHellbent If you have no cards in hand, Demonfire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_image_name('demonfire'/'DD2', 'demonfire').
card_uid('demonfire'/'DD2', 'DD2:Demonfire:demonfire').
card_rarity('demonfire'/'DD2', 'Rare').
card_artist('demonfire'/'DD2', 'Greg Staples').
card_number('demonfire'/'DD2', '57').
card_multiverse_id('demonfire'/'DD2', '189229').

card_in_set('elemental shaman', 'DD2').
card_original_type('elemental shaman'/'DD2', 'Creature — Elemental Shaman').
card_original_text('elemental shaman'/'DD2', '').
card_first_print('elemental shaman', 'DD2').
card_image_name('elemental shaman'/'DD2', 'elemental shaman').
card_uid('elemental shaman'/'DD2', 'DD2:Elemental Shaman:elemental shaman').
card_rarity('elemental shaman'/'DD2', 'Common').
card_artist('elemental shaman'/'DD2', 'Jim Pavelec').
card_number('elemental shaman'/'DD2', 'T1').
card_multiverse_id('elemental shaman'/'DD2', '190199').

card_in_set('errant ephemeron', 'DD2').
card_original_type('errant ephemeron'/'DD2', 'Creature — Illusion').
card_original_text('errant ephemeron'/'DD2', 'Flying\nSuspend 4—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)').
card_image_name('errant ephemeron'/'DD2', 'errant ephemeron').
card_uid('errant ephemeron'/'DD2', 'DD2:Errant Ephemeron:errant ephemeron').
card_rarity('errant ephemeron'/'DD2', 'Common').
card_artist('errant ephemeron'/'DD2', 'Luca Zontini').
card_number('errant ephemeron'/'DD2', '20').
card_multiverse_id('errant ephemeron'/'DD2', '189246').

card_in_set('fact or fiction', 'DD2').
card_original_type('fact or fiction'/'DD2', 'Instant').
card_original_text('fact or fiction'/'DD2', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'DD2', 'fact or fiction').
card_uid('fact or fiction'/'DD2', 'DD2:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'DD2', 'Uncommon').
card_artist('fact or fiction'/'DD2', 'Matt Cavotta').
card_number('fact or fiction'/'DD2', '26').
card_flavor_text('fact or fiction'/'DD2', '\"Try to pretend like you understand what\'s important.\"').
card_multiverse_id('fact or fiction'/'DD2', '185819').

card_in_set('fathom seer', 'DD2').
card_original_type('fathom seer'/'DD2', 'Creature — Illusion').
card_original_text('fathom seer'/'DD2', 'Morph—Return two Islands you control to their owner\'s hand. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_image_name('fathom seer'/'DD2', 'fathom seer').
card_uid('fathom seer'/'DD2', 'DD2:Fathom Seer:fathom seer').
card_rarity('fathom seer'/'DD2', 'Common').
card_artist('fathom seer'/'DD2', 'Ralph Horsley').
card_number('fathom seer'/'DD2', '3').
card_multiverse_id('fathom seer'/'DD2', '189247').

card_in_set('fireball', 'DD2').
card_original_type('fireball'/'DD2', 'Sorcery').
card_original_text('fireball'/'DD2', 'As an additional cost to play Fireball, pay {1} for each target beyond the first.\nFireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.').
card_image_name('fireball'/'DD2', 'fireball').
card_uid('fireball'/'DD2', 'DD2:Fireball:fireball').
card_rarity('fireball'/'DD2', 'Uncommon').
card_artist('fireball'/'DD2', 'Dave Dorman').
card_number('fireball'/'DD2', '56').
card_flavor_text('fireball'/'DD2', 'The spell fell upon the crowd like a dragon, ancient and full of death.').
card_multiverse_id('fireball'/'DD2', '189231').

card_in_set('fireblast', 'DD2').
card_original_type('fireblast'/'DD2', 'Instant').
card_original_text('fireblast'/'DD2', 'You may sacrifice two Mountains rather than pay Fireblast\'s mana cost.\nFireblast deals 4 damage to target creature or player.').
card_image_name('fireblast'/'DD2', 'fireblast').
card_uid('fireblast'/'DD2', 'DD2:Fireblast:fireblast').
card_rarity('fireblast'/'DD2', 'Common').
card_artist('fireblast'/'DD2', 'Michael Danza').
card_number('fireblast'/'DD2', '55').
card_flavor_text('fireblast'/'DD2', 'Embermages aren\'t well known for their diplomatic skills.').
card_multiverse_id('fireblast'/'DD2', '189239').

card_in_set('firebolt', 'DD2').
card_original_type('firebolt'/'DD2', 'Sorcery').
card_original_text('firebolt'/'DD2', 'Firebolt deals 2 damage to target creature or player.\nFlashback {4}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('firebolt'/'DD2', 'firebolt').
card_uid('firebolt'/'DD2', 'DD2:Firebolt:firebolt').
card_rarity('firebolt'/'DD2', 'Common').
card_artist('firebolt'/'DD2', 'Ron Spencer').
card_number('firebolt'/'DD2', '49').
card_flavor_text('firebolt'/'DD2', 'Reach out and torch someone.').
card_multiverse_id('firebolt'/'DD2', '189236').

card_in_set('fireslinger', 'DD2').
card_original_type('fireslinger'/'DD2', 'Creature — Human Wizard').
card_original_text('fireslinger'/'DD2', '{T}: Fireslinger deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('fireslinger'/'DD2', 'fireslinger').
card_uid('fireslinger'/'DD2', 'DD2:Fireslinger:fireslinger').
card_rarity('fireslinger'/'DD2', 'Common').
card_artist('fireslinger'/'DD2', 'Jeff Reitz').
card_number('fireslinger'/'DD2', '36').
card_flavor_text('fireslinger'/'DD2', '\"Remember the moral of the fireslinger fable: with power comes isolation.\"\n—Karn, silver golem').
card_multiverse_id('fireslinger'/'DD2', '189219').

card_in_set('flame javelin', 'DD2').
card_original_type('flame javelin'/'DD2', 'Instant').
card_original_text('flame javelin'/'DD2', '({2/R} can be paid with any two mana or with {R}. This card\'s converted mana cost is 6.)\nFlame Javelin deals 4 damage to target creature or player.').
card_image_name('flame javelin'/'DD2', 'flame javelin').
card_uid('flame javelin'/'DD2', 'DD2:Flame Javelin:flame javelin').
card_rarity('flame javelin'/'DD2', 'Uncommon').
card_artist('flame javelin'/'DD2', 'Trevor Hairsine').
card_number('flame javelin'/'DD2', '53').
card_flavor_text('flame javelin'/'DD2', 'Gyara Spearhurler would have been renowned for her deadly accuracy, if it weren\'t for her deadly accuracy.').
card_multiverse_id('flame javelin'/'DD2', '189220').

card_in_set('flamekin brawler', 'DD2').
card_original_type('flamekin brawler'/'DD2', 'Creature — Elemental Warrior').
card_original_text('flamekin brawler'/'DD2', '{R}: Flamekin Brawler gets +1/+0 until end of turn.').
card_image_name('flamekin brawler'/'DD2', 'flamekin brawler').
card_uid('flamekin brawler'/'DD2', 'DD2:Flamekin Brawler:flamekin brawler').
card_rarity('flamekin brawler'/'DD2', 'Common').
card_artist('flamekin brawler'/'DD2', 'Daren Bader').
card_number('flamekin brawler'/'DD2', '35').
card_flavor_text('flamekin brawler'/'DD2', 'When he hits people, they stay hit.').
card_multiverse_id('flamekin brawler'/'DD2', '189212').

card_in_set('flametongue kavu', 'DD2').
card_original_type('flametongue kavu'/'DD2', 'Creature — Kavu').
card_original_text('flametongue kavu'/'DD2', 'When Flametongue Kavu comes into play, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'DD2', 'flametongue kavu').
card_uid('flametongue kavu'/'DD2', 'DD2:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'DD2', 'Uncommon').
card_artist('flametongue kavu'/'DD2', 'Pete Venters').
card_number('flametongue kavu'/'DD2', '42').
card_flavor_text('flametongue kavu'/'DD2', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'DD2', '189234').

card_in_set('flamewave invoker', 'DD2').
card_original_type('flamewave invoker'/'DD2', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'DD2', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_image_name('flamewave invoker'/'DD2', 'flamewave invoker').
card_uid('flamewave invoker'/'DD2', 'DD2:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'DD2', 'Uncommon').
card_artist('flamewave invoker'/'DD2', 'Dave Dorman').
card_number('flamewave invoker'/'DD2', '40').
card_flavor_text('flamewave invoker'/'DD2', 'Inside even the humblest goblin lurks the potential for far greater things—and far worse.').
card_multiverse_id('flamewave invoker'/'DD2', '189226').

card_in_set('fledgling mawcor', 'DD2').
card_original_type('fledgling mawcor'/'DD2', 'Creature — Beast').
card_original_text('fledgling mawcor'/'DD2', 'Flying\n{T}: Fledgling Mawcor deals 1 damage to target creature or player.\nMorph {U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('fledgling mawcor'/'DD2', 'fledgling mawcor').
card_uid('fledgling mawcor'/'DD2', 'DD2:Fledgling Mawcor:fledgling mawcor').
card_rarity('fledgling mawcor'/'DD2', 'Uncommon').
card_artist('fledgling mawcor'/'DD2', 'Kev Walker').
card_number('fledgling mawcor'/'DD2', '10').
card_multiverse_id('fledgling mawcor'/'DD2', '189248').

card_in_set('furnace whelp', 'DD2').
card_original_type('furnace whelp'/'DD2', 'Creature — Dragon').
card_original_text('furnace whelp'/'DD2', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'DD2', 'furnace whelp').
card_uid('furnace whelp'/'DD2', 'DD2:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'DD2', 'Uncommon').
card_artist('furnace whelp'/'DD2', 'Matt Cavotta').
card_number('furnace whelp'/'DD2', '43').
card_flavor_text('furnace whelp'/'DD2', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'DD2', '189227').

card_in_set('guile', 'DD2').
card_original_type('guile'/'DD2', 'Creature — Elemental Incarnation').
card_original_text('guile'/'DD2', 'Guile can\'t be blocked except by three or more creatures.\nIf a spell or ability you control would counter a spell, instead remove that spell from the game and you may play that card without paying its mana cost.\nWhen Guile is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('guile'/'DD2', 'guile').
card_uid('guile'/'DD2', 'DD2:Guile:guile').
card_rarity('guile'/'DD2', 'Rare').
card_artist('guile'/'DD2', 'Zoltan Boros & Gabor Szikszai').
card_number('guile'/'DD2', '14').
card_multiverse_id('guile'/'DD2', '189213').

card_in_set('gush', 'DD2').
card_original_type('gush'/'DD2', 'Instant').
card_original_text('gush'/'DD2', 'You may return two Islands you control to their owner\'s hand rather than pay Gush\'s mana cost.\nDraw two cards.').
card_image_name('gush'/'DD2', 'gush').
card_uid('gush'/'DD2', 'DD2:Gush:gush').
card_rarity('gush'/'DD2', 'Common').
card_artist('gush'/'DD2', 'Kev Walker').
card_number('gush'/'DD2', '27').
card_flavor_text('gush'/'DD2', 'Don\'t trust your secrets to the sea.').
card_multiverse_id('gush'/'DD2', '189258').

card_in_set('hostility', 'DD2').
card_original_type('hostility'/'DD2', 'Creature — Elemental Incarnation').
card_original_text('hostility'/'DD2', 'Haste\nIf a spell you control would deal damage to an opponent, prevent that damage. Put a 3/1 red Elemental Shaman creature token with haste into play for each 1 damage prevented this way.\nWhen Hostility is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('hostility'/'DD2', 'hostility').
card_uid('hostility'/'DD2', 'DD2:Hostility:hostility').
card_rarity('hostility'/'DD2', 'Rare').
card_artist('hostility'/'DD2', 'Omar Rayyan').
card_number('hostility'/'DD2', '48').
card_multiverse_id('hostility'/'DD2', '189214').

card_in_set('incinerate', 'DD2').
card_original_type('incinerate'/'DD2', 'Instant').
card_original_text('incinerate'/'DD2', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'DD2', 'incinerate').
card_uid('incinerate'/'DD2', 'DD2:Incinerate:incinerate').
card_rarity('incinerate'/'DD2', 'Common').
card_artist('incinerate'/'DD2', 'Steve Prescott').
card_number('incinerate'/'DD2', '51').
card_flavor_text('incinerate'/'DD2', '\"Who said there are no assurances in life? I assure you this is going to hurt.\"').
card_multiverse_id('incinerate'/'DD2', '185818').

card_in_set('ingot chewer', 'DD2').
card_original_type('ingot chewer'/'DD2', 'Creature — Elemental').
card_original_text('ingot chewer'/'DD2', 'When Ingot Chewer comes into play, destroy target artifact.\nEvoke {R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('ingot chewer'/'DD2', 'ingot chewer').
card_uid('ingot chewer'/'DD2', 'DD2:Ingot Chewer:ingot chewer').
card_rarity('ingot chewer'/'DD2', 'Common').
card_artist('ingot chewer'/'DD2', 'Kev Walker').
card_number('ingot chewer'/'DD2', '45').
card_flavor_text('ingot chewer'/'DD2', 'Elementals are ideas given form. This one is the idea of \"smashitude.\"').
card_multiverse_id('ingot chewer'/'DD2', '189215').

card_in_set('inner-flame acolyte', 'DD2').
card_original_type('inner-flame acolyte'/'DD2', 'Creature — Elemental Shaman').
card_original_text('inner-flame acolyte'/'DD2', 'When Inner-Flame Acolyte comes into play, target creature gets +2/+0 and gains haste until end of turn.\nEvoke {R} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('inner-flame acolyte'/'DD2', 'inner-flame acolyte').
card_uid('inner-flame acolyte'/'DD2', 'DD2:Inner-Flame Acolyte:inner-flame acolyte').
card_rarity('inner-flame acolyte'/'DD2', 'Common').
card_artist('inner-flame acolyte'/'DD2', 'Ron Spears').
card_number('inner-flame acolyte'/'DD2', '41').
card_multiverse_id('inner-flame acolyte'/'DD2', '189216').

card_in_set('island', 'DD2').
card_original_type('island'/'DD2', 'Basic Land — Island').
card_original_text('island'/'DD2', 'U').
card_image_name('island'/'DD2', 'island1').
card_uid('island'/'DD2', 'DD2:Island:island1').
card_rarity('island'/'DD2', 'Basic Land').
card_artist('island'/'DD2', 'John Avon').
card_number('island'/'DD2', '30').
card_multiverse_id('island'/'DD2', '190588').

card_in_set('island', 'DD2').
card_original_type('island'/'DD2', 'Basic Land — Island').
card_original_text('island'/'DD2', 'U').
card_image_name('island'/'DD2', 'island2').
card_uid('island'/'DD2', 'DD2:Island:island2').
card_rarity('island'/'DD2', 'Basic Land').
card_artist('island'/'DD2', 'Scott Bailey').
card_number('island'/'DD2', '31').
card_multiverse_id('island'/'DD2', '190583').

card_in_set('island', 'DD2').
card_original_type('island'/'DD2', 'Basic Land — Island').
card_original_text('island'/'DD2', 'U').
card_image_name('island'/'DD2', 'island3').
card_uid('island'/'DD2', 'DD2:Island:island3').
card_rarity('island'/'DD2', 'Basic Land').
card_artist('island'/'DD2', 'Donato Giancola').
card_number('island'/'DD2', '32').
card_multiverse_id('island'/'DD2', '190590').

card_in_set('island', 'DD2').
card_original_type('island'/'DD2', 'Basic Land — Island').
card_original_text('island'/'DD2', 'U').
card_image_name('island'/'DD2', 'island4').
card_uid('island'/'DD2', 'DD2:Island:island4').
card_rarity('island'/'DD2', 'Basic Land').
card_artist('island'/'DD2', 'Christopher Moeller').
card_number('island'/'DD2', '33').
card_multiverse_id('island'/'DD2', '190589').

card_in_set('jace beleren', 'DD2').
card_original_type('jace beleren'/'DD2', 'Planeswalker — Jace').
card_original_text('jace beleren'/'DD2', '+2: Each player draws a card.\n-1: Target player draws a card.\n-10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_image_name('jace beleren'/'DD2', 'jace beleren').
card_uid('jace beleren'/'DD2', 'DD2:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'DD2', 'Mythic Rare').
card_artist('jace beleren'/'DD2', 'Kev Walker').
card_number('jace beleren'/'DD2', '1').
card_multiverse_id('jace beleren'/'DD2', '185816').

card_in_set('keldon megaliths', 'DD2').
card_original_type('keldon megaliths'/'DD2', 'Land').
card_original_text('keldon megaliths'/'DD2', 'Keldon Megaliths comes into play tapped.\n{T}: Add {R} to your mana pool.\nHellbent {1}{R}, {T}: Keldon Megaliths deals 1 damage to target creature or player. Play this ability only if you have no cards in hand.').
card_image_name('keldon megaliths'/'DD2', 'keldon megaliths').
card_uid('keldon megaliths'/'DD2', 'DD2:Keldon Megaliths:keldon megaliths').
card_rarity('keldon megaliths'/'DD2', 'Uncommon').
card_artist('keldon megaliths'/'DD2', 'Philip Straub').
card_number('keldon megaliths'/'DD2', '58').
card_multiverse_id('keldon megaliths'/'DD2', '189242').

card_in_set('magma jet', 'DD2').
card_original_type('magma jet'/'DD2', 'Instant').
card_original_text('magma jet'/'DD2', 'Magma Jet deals 2 damage to target creature or player.\nScry 2. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('magma jet'/'DD2', 'magma jet').
card_uid('magma jet'/'DD2', 'DD2:Magma Jet:magma jet').
card_rarity('magma jet'/'DD2', 'Uncommon').
card_artist('magma jet'/'DD2', 'Justin Sweet').
card_number('magma jet'/'DD2', '52').
card_multiverse_id('magma jet'/'DD2', '189238').

card_in_set('man-o\'-war', 'DD2').
card_original_type('man-o\'-war'/'DD2', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'DD2', 'When Man-o\'-War comes into play, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'DD2', 'man-o\'-war').
card_uid('man-o\'-war'/'DD2', 'DD2:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'DD2', 'Common').
card_artist('man-o\'-war'/'DD2', 'Jon J. Muth').
card_number('man-o\'-war'/'DD2', '8').
card_flavor_text('man-o\'-war'/'DD2', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'DD2', '189240').

card_in_set('martyr of frost', 'DD2').
card_original_type('martyr of frost'/'DD2', 'Creature — Human Wizard').
card_original_text('martyr of frost'/'DD2', '{2}, Reveal X blue cards from your hand, Sacrifice Martyr of Frost: Counter target spell unless its controller pays {X}.').
card_image_name('martyr of frost'/'DD2', 'martyr of frost').
card_uid('martyr of frost'/'DD2', 'DD2:Martyr of Frost:martyr of frost').
card_rarity('martyr of frost'/'DD2', 'Common').
card_artist('martyr of frost'/'DD2', 'Wayne England').
card_number('martyr of frost'/'DD2', '2').
card_flavor_text('martyr of frost'/'DD2', '\"Do not call the Balduvians barbaric. Their magic is as potent as it is primal.\"\n—Zur the Enchanter').
card_multiverse_id('martyr of frost'/'DD2', '189243').

card_in_set('mind stone', 'DD2').
card_original_type('mind stone'/'DD2', 'Artifact').
card_original_text('mind stone'/'DD2', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_image_name('mind stone'/'DD2', 'mind stone').
card_uid('mind stone'/'DD2', 'DD2:Mind Stone:mind stone').
card_rarity('mind stone'/'DD2', 'Uncommon').
card_artist('mind stone'/'DD2', 'Adam Rex').
card_number('mind stone'/'DD2', '22').
card_flavor_text('mind stone'/'DD2', '\"What is mana but possibility, an idea not yet given form?\"\n—Jhoira, master artificer').
card_multiverse_id('mind stone'/'DD2', '189228').

card_in_set('mountain', 'DD2').
card_original_type('mountain'/'DD2', 'Basic Land — Mountain').
card_original_text('mountain'/'DD2', 'R').
card_image_name('mountain'/'DD2', 'mountain1').
card_uid('mountain'/'DD2', 'DD2:Mountain:mountain1').
card_rarity('mountain'/'DD2', 'Basic Land').
card_artist('mountain'/'DD2', 'Rob Alexander').
card_number('mountain'/'DD2', '59').
card_multiverse_id('mountain'/'DD2', '190584').

card_in_set('mountain', 'DD2').
card_original_type('mountain'/'DD2', 'Basic Land — Mountain').
card_original_text('mountain'/'DD2', 'R').
card_image_name('mountain'/'DD2', 'mountain2').
card_uid('mountain'/'DD2', 'DD2:Mountain:mountain2').
card_rarity('mountain'/'DD2', 'Basic Land').
card_artist('mountain'/'DD2', 'Christopher Moeller').
card_number('mountain'/'DD2', '60').
card_multiverse_id('mountain'/'DD2', '190586').

card_in_set('mountain', 'DD2').
card_original_type('mountain'/'DD2', 'Basic Land — Mountain').
card_original_text('mountain'/'DD2', 'R').
card_image_name('mountain'/'DD2', 'mountain3').
card_uid('mountain'/'DD2', 'DD2:Mountain:mountain3').
card_rarity('mountain'/'DD2', 'Basic Land').
card_artist('mountain'/'DD2', 'Greg Staples').
card_number('mountain'/'DD2', '61').
card_multiverse_id('mountain'/'DD2', '190587').

card_in_set('mountain', 'DD2').
card_original_type('mountain'/'DD2', 'Basic Land — Mountain').
card_original_text('mountain'/'DD2', 'R').
card_image_name('mountain'/'DD2', 'mountain4').
card_uid('mountain'/'DD2', 'DD2:Mountain:mountain4').
card_rarity('mountain'/'DD2', 'Basic Land').
card_artist('mountain'/'DD2', 'Mark Tedin').
card_number('mountain'/'DD2', '62').
card_multiverse_id('mountain'/'DD2', '190585').

card_in_set('mulldrifter', 'DD2').
card_original_type('mulldrifter'/'DD2', 'Creature — Elemental').
card_original_text('mulldrifter'/'DD2', 'Flying\nWhen Mulldrifter comes into play, draw two cards.\nEvoke {2}{U} (You may play this spell for its evoke cost. If you do, it\'s sacrificed when it comes into play.)').
card_image_name('mulldrifter'/'DD2', 'mulldrifter').
card_uid('mulldrifter'/'DD2', 'DD2:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'DD2', 'Common').
card_artist('mulldrifter'/'DD2', 'Eric Fortune').
card_number('mulldrifter'/'DD2', '12').
card_multiverse_id('mulldrifter'/'DD2', '189217').

card_in_set('ophidian', 'DD2').
card_original_type('ophidian'/'DD2', 'Creature — Snake').
card_original_text('ophidian'/'DD2', 'Whenever Ophidian attacks and isn\'t blocked, you may draw a card. If you do, Ophidian deals no combat damage this turn.').
card_image_name('ophidian'/'DD2', 'ophidian').
card_uid('ophidian'/'DD2', 'DD2:Ophidian:ophidian').
card_rarity('ophidian'/'DD2', 'Common').
card_artist('ophidian'/'DD2', 'Cliff Nielsen').
card_number('ophidian'/'DD2', '9').
card_multiverse_id('ophidian'/'DD2', '189253').

card_in_set('oxidda golem', 'DD2').
card_original_type('oxidda golem'/'DD2', 'Artifact Creature — Golem').
card_original_text('oxidda golem'/'DD2', 'Affinity for Mountains (This spell costs {1} less to play for each Mountain you control.)\nHaste').
card_image_name('oxidda golem'/'DD2', 'oxidda golem').
card_uid('oxidda golem'/'DD2', 'DD2:Oxidda Golem:oxidda golem').
card_rarity('oxidda golem'/'DD2', 'Common').
card_artist('oxidda golem'/'DD2', 'Greg Staples').
card_number('oxidda golem'/'DD2', '46').
card_flavor_text('oxidda golem'/'DD2', 'The longer it patrols the smoldering crags of the Oxidda Chain, the more it adopts their fiery temper.').
card_multiverse_id('oxidda golem'/'DD2', '189232').

card_in_set('pyre charger', 'DD2').
card_original_type('pyre charger'/'DD2', 'Creature — Elemental Warrior').
card_original_text('pyre charger'/'DD2', 'Haste\n{R}: Pyre Charger gets +1/+0 until end of turn.').
card_image_name('pyre charger'/'DD2', 'pyre charger').
card_uid('pyre charger'/'DD2', 'DD2:Pyre Charger:pyre charger').
card_rarity('pyre charger'/'DD2', 'Uncommon').
card_artist('pyre charger'/'DD2', 'Mark Zug').
card_number('pyre charger'/'DD2', '38').
card_flavor_text('pyre charger'/'DD2', 'His blade was forged over coals of moaning treefolk, curved at the optimum angle for severing heads, and heated to volcanic temperatures by his touch.').
card_multiverse_id('pyre charger'/'DD2', '189221').

card_in_set('quicksilver dragon', 'DD2').
card_original_type('quicksilver dragon'/'DD2', 'Creature — Dragon').
card_original_text('quicksilver dragon'/'DD2', 'Flying\n{U}: If target spell has only one target and that target is Quicksilver Dragon, change that spell\'s target to another creature.\nMorph {4}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('quicksilver dragon'/'DD2', 'quicksilver dragon').
card_uid('quicksilver dragon'/'DD2', 'DD2:Quicksilver Dragon:quicksilver dragon').
card_rarity('quicksilver dragon'/'DD2', 'Rare').
card_artist('quicksilver dragon'/'DD2', 'Ron Spencer').
card_number('quicksilver dragon'/'DD2', '19').
card_multiverse_id('quicksilver dragon'/'DD2', '189254').

card_in_set('rakdos pit dragon', 'DD2').
card_original_type('rakdos pit dragon'/'DD2', 'Creature — Dragon').
card_original_text('rakdos pit dragon'/'DD2', '{R}{R}: Rakdos Pit Dragon gains flying until end of turn.\n{R}: Rakdos Pit Dragon gets +1/+0 until end of turn.\nHellbent Rakdos Pit Dragon has double strike as long as you have no cards in hand.').
card_image_name('rakdos pit dragon'/'DD2', 'rakdos pit dragon').
card_uid('rakdos pit dragon'/'DD2', 'DD2:Rakdos Pit Dragon:rakdos pit dragon').
card_rarity('rakdos pit dragon'/'DD2', 'Rare').
card_artist('rakdos pit dragon'/'DD2', 'Kev Walker').
card_number('rakdos pit dragon'/'DD2', '44').
card_multiverse_id('rakdos pit dragon'/'DD2', '189230').

card_in_set('repulse', 'DD2').
card_original_type('repulse'/'DD2', 'Instant').
card_original_text('repulse'/'DD2', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_image_name('repulse'/'DD2', 'repulse').
card_uid('repulse'/'DD2', 'DD2:Repulse:repulse').
card_rarity('repulse'/'DD2', 'Common').
card_artist('repulse'/'DD2', 'Aaron Boyd').
card_number('repulse'/'DD2', '25').
card_flavor_text('repulse'/'DD2', '\"You aren\'t invited.\"').
card_multiverse_id('repulse'/'DD2', '189257').

card_in_set('riftwing cloudskate', 'DD2').
card_original_type('riftwing cloudskate'/'DD2', 'Creature — Illusion').
card_original_text('riftwing cloudskate'/'DD2', 'Flying\nWhen Riftwing Cloudskate comes into play, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, play it without paying its mana cost. It has haste.)').
card_image_name('riftwing cloudskate'/'DD2', 'riftwing cloudskate').
card_uid('riftwing cloudskate'/'DD2', 'DD2:Riftwing Cloudskate:riftwing cloudskate').
card_rarity('riftwing cloudskate'/'DD2', 'Uncommon').
card_artist('riftwing cloudskate'/'DD2', 'Carl Critchlow').
card_number('riftwing cloudskate'/'DD2', '15').
card_multiverse_id('riftwing cloudskate'/'DD2', '189249').

card_in_set('seal of fire', 'DD2').
card_original_type('seal of fire'/'DD2', 'Enchantment').
card_original_text('seal of fire'/'DD2', 'Sacrifice Seal of Fire: Seal of Fire deals 2 damage to target creature or player.').
card_image_name('seal of fire'/'DD2', 'seal of fire').
card_uid('seal of fire'/'DD2', 'DD2:Seal of Fire:seal of fire').
card_rarity('seal of fire'/'DD2', 'Common').
card_artist('seal of fire'/'DD2', 'Christopher Moeller').
card_number('seal of fire'/'DD2', '50').
card_flavor_text('seal of fire'/'DD2', 'After the fourth time Chandra left a place engulfed in flames, she decided to just go ahead and make it her thing.').
card_multiverse_id('seal of fire'/'DD2', '185817').

card_in_set('slith firewalker', 'DD2').
card_original_type('slith firewalker'/'DD2', 'Creature — Slith').
card_original_text('slith firewalker'/'DD2', 'Haste\nWhenever Slith Firewalker deals combat damage to a player, put a +1/+1 counter on it.').
card_image_name('slith firewalker'/'DD2', 'slith firewalker').
card_uid('slith firewalker'/'DD2', 'DD2:Slith Firewalker:slith firewalker').
card_rarity('slith firewalker'/'DD2', 'Uncommon').
card_artist('slith firewalker'/'DD2', 'Justin Sweet').
card_number('slith firewalker'/'DD2', '39').
card_flavor_text('slith firewalker'/'DD2', 'The slith incubate in the Great Furnace\'s heat, emerging on Mirrodin\'s surface only when the four suns have aligned overhead.').
card_multiverse_id('slith firewalker'/'DD2', '189222').

card_in_set('soulbright flamekin', 'DD2').
card_original_type('soulbright flamekin'/'DD2', 'Creature — Elemental Shaman').
card_original_text('soulbright flamekin'/'DD2', '{2}: Target creature gains trample until end of turn. If this is the third time this ability has resolved this turn, you may add {R}{R}{R}{R}{R}{R}{R}{R} to your mana pool.').
card_image_name('soulbright flamekin'/'DD2', 'soulbright flamekin').
card_uid('soulbright flamekin'/'DD2', 'DD2:Soulbright Flamekin:soulbright flamekin').
card_rarity('soulbright flamekin'/'DD2', 'Common').
card_artist('soulbright flamekin'/'DD2', 'Kev Walker').
card_number('soulbright flamekin'/'DD2', '37').
card_flavor_text('soulbright flamekin'/'DD2', 'When provoked, a flamekin\'s inner fire burns far hotter than any giant\'s forge.').
card_multiverse_id('soulbright flamekin'/'DD2', '189218').

card_in_set('spire golem', 'DD2').
card_original_type('spire golem'/'DD2', 'Artifact Creature — Golem').
card_original_text('spire golem'/'DD2', 'Affinity for Islands (This spell costs {1} less to play for each Island you control.)\nFlying').
card_image_name('spire golem'/'DD2', 'spire golem').
card_uid('spire golem'/'DD2', 'DD2:Spire Golem:spire golem').
card_rarity('spire golem'/'DD2', 'Common').
card_artist('spire golem'/'DD2', 'Daren Bader').
card_number('spire golem'/'DD2', '16').
card_flavor_text('spire golem'/'DD2', 'The longer it soars above the shimmering swirls of the Quicksilver Sea, the more it adopts their unpredictability.').
card_multiverse_id('spire golem'/'DD2', '189233').

card_in_set('terrain generator', 'DD2').
card_original_type('terrain generator'/'DD2', 'Land').
card_original_text('terrain generator'/'DD2', '{T}: Add {1} to your mana pool.\n{2}, {T}: You may put a basic land card from your hand into play tapped.').
card_image_name('terrain generator'/'DD2', 'terrain generator').
card_uid('terrain generator'/'DD2', 'DD2:Terrain Generator:terrain generator').
card_rarity('terrain generator'/'DD2', 'Uncommon').
card_artist('terrain generator'/'DD2', 'Alan Pollack').
card_number('terrain generator'/'DD2', '29').
card_multiverse_id('terrain generator'/'DD2', '189256').

card_in_set('voidmage apprentice', 'DD2').
card_original_type('voidmage apprentice'/'DD2', 'Creature — Human Wizard').
card_original_text('voidmage apprentice'/'DD2', 'Morph {2}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Voidmage Apprentice is turned face up, counter target spell.').
card_image_name('voidmage apprentice'/'DD2', 'voidmage apprentice').
card_uid('voidmage apprentice'/'DD2', 'DD2:Voidmage Apprentice:voidmage apprentice').
card_rarity('voidmage apprentice'/'DD2', 'Common').
card_artist('voidmage apprentice'/'DD2', 'Jim Nelson').
card_number('voidmage apprentice'/'DD2', '4').
card_multiverse_id('voidmage apprentice'/'DD2', '189250').

card_in_set('wall of deceit', 'DD2').
card_original_type('wall of deceit'/'DD2', 'Creature — Wall').
card_original_text('wall of deceit'/'DD2', 'Defender\n{3}: Turn Wall of Deceit face down.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('wall of deceit'/'DD2', 'wall of deceit').
card_uid('wall of deceit'/'DD2', 'DD2:Wall of Deceit:wall of deceit').
card_rarity('wall of deceit'/'DD2', 'Uncommon').
card_artist('wall of deceit'/'DD2', 'John Avon').
card_number('wall of deceit'/'DD2', '5').
card_multiverse_id('wall of deceit'/'DD2', '189251').

card_in_set('waterspout djinn', 'DD2').
card_original_type('waterspout djinn'/'DD2', 'Creature — Djinn').
card_original_text('waterspout djinn'/'DD2', 'Flying\nAt the beginning of your upkeep, sacrifice Waterspout Djinn unless you return an untapped Island you control to its owner\'s hand.').
card_image_name('waterspout djinn'/'DD2', 'waterspout djinn').
card_uid('waterspout djinn'/'DD2', 'DD2:Waterspout Djinn:waterspout djinn').
card_rarity('waterspout djinn'/'DD2', 'Uncommon').
card_artist('waterspout djinn'/'DD2', 'Thomas Gianni').
card_number('waterspout djinn'/'DD2', '11').
card_flavor_text('waterspout djinn'/'DD2', '\"Fly us higher, out of its storm.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('waterspout djinn'/'DD2', '189241').

card_in_set('willbender', 'DD2').
card_original_type('willbender'/'DD2', 'Creature — Human Wizard').
card_original_text('willbender'/'DD2', 'Morph {1}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'DD2', 'willbender').
card_uid('willbender'/'DD2', 'DD2:Willbender:willbender').
card_rarity('willbender'/'DD2', 'Uncommon').
card_artist('willbender'/'DD2', 'Eric Peterson').
card_number('willbender'/'DD2', '6').
card_multiverse_id('willbender'/'DD2', '189252').
