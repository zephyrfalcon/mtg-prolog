% From the Vault: Dragons

set('DRB').
set_name('DRB', 'From the Vault: Dragons').
set_release_date('DRB', '2008-08-29').
set_border('DRB', 'black').
set_type('DRB', 'from the vault').

card_in_set('bladewing the risen', 'DRB').
card_original_type('bladewing the risen'/'DRB', 'Legendary Creature — Zombie Dragon').
card_original_text('bladewing the risen'/'DRB', 'Flying\nWhen Bladewing the Risen comes into play, you may return target Dragon permanent card from your graveyard to play.\n{B}{R}: Dragon creatures get +1/+1 until end of turn.').
card_image_name('bladewing the risen'/'DRB', 'bladewing the risen').
card_uid('bladewing the risen'/'DRB', 'DRB:Bladewing the Risen:bladewing the risen').
card_rarity('bladewing the risen'/'DRB', 'Rare').
card_artist('bladewing the risen'/'DRB', 'Kev Walker').
card_number('bladewing the risen'/'DRB', '1').
card_multiverse_id('bladewing the risen'/'DRB', '178014').

card_in_set('bogardan hellkite', 'DRB').
card_original_type('bogardan hellkite'/'DRB', 'Creature — Dragon').
card_original_text('bogardan hellkite'/'DRB', 'Flash\nFlying\nWhen Bogardan Hellkite comes into play, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('bogardan hellkite'/'DRB', 'bogardan hellkite').
card_uid('bogardan hellkite'/'DRB', 'DRB:Bogardan Hellkite:bogardan hellkite').
card_rarity('bogardan hellkite'/'DRB', 'Rare').
card_artist('bogardan hellkite'/'DRB', 'Scott M. Fischer').
card_number('bogardan hellkite'/'DRB', '2').
card_flavor_text('bogardan hellkite'/'DRB', 'Cleansing the world, one breath at a time.').
card_multiverse_id('bogardan hellkite'/'DRB', '178017').

card_in_set('draco', 'DRB').
card_original_type('draco'/'DRB', 'Artifact Creature — Dragon').
card_original_text('draco'/'DRB', 'Draco costs {2} less to play for each basic land type among lands you control.\nFlying\nAt the beginning of your upkeep, sacrifice Draco unless you pay {10}. This cost is reduced by {2} for each basic land type among lands you control.').
card_image_name('draco'/'DRB', 'draco').
card_uid('draco'/'DRB', 'DRB:Draco:draco').
card_rarity('draco'/'DRB', 'Rare').
card_artist('draco'/'DRB', 'Sam Wood').
card_number('draco'/'DRB', '3').
card_multiverse_id('draco'/'DRB', '178018').

card_in_set('dragon whelp', 'DRB').
card_original_type('dragon whelp'/'DRB', 'Creature — Dragon').
card_original_text('dragon whelp'/'DRB', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. At end of turn, if this ability has been played four or more times this turn, sacrifice Dragon Whelp.').
card_image_name('dragon whelp'/'DRB', 'dragon whelp').
card_uid('dragon whelp'/'DRB', 'DRB:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'DRB', 'Rare').
card_artist('dragon whelp'/'DRB', 'Steven Belledin').
card_number('dragon whelp'/'DRB', '4').
card_flavor_text('dragon whelp'/'DRB', 'Dragon whelps face few threats besides their own enthusiasm.').
card_multiverse_id('dragon whelp'/'DRB', '178019').

card_in_set('dragonstorm', 'DRB').
card_original_type('dragonstorm'/'DRB', 'Sorcery').
card_original_text('dragonstorm'/'DRB', 'Search your library for a Dragon permanent card and put it into play. Then shuffle your library.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_image_name('dragonstorm'/'DRB', 'dragonstorm').
card_uid('dragonstorm'/'DRB', 'DRB:Dragonstorm:dragonstorm').
card_rarity('dragonstorm'/'DRB', 'Rare').
card_artist('dragonstorm'/'DRB', 'Kev Walker').
card_number('dragonstorm'/'DRB', '5').
card_flavor_text('dragonstorm'/'DRB', 'Dragons come not in packs or in herds but in storms.').
card_multiverse_id('dragonstorm'/'DRB', '178015').

card_in_set('ebon dragon', 'DRB').
card_original_type('ebon dragon'/'DRB', 'Creature — Dragon').
card_original_text('ebon dragon'/'DRB', 'Flying\nWhen Ebon Dragon comes into play, you may have target opponent discard a card.').
card_image_name('ebon dragon'/'DRB', 'ebon dragon').
card_uid('ebon dragon'/'DRB', 'DRB:Ebon Dragon:ebon dragon').
card_rarity('ebon dragon'/'DRB', 'Rare').
card_artist('ebon dragon'/'DRB', 'Donato Giancola').
card_number('ebon dragon'/'DRB', '6').
card_flavor_text('ebon dragon'/'DRB', '\"The dragon wing of night o\'erspreads the earth.\"\n—Troilus and Cressida, William Shakespeare').
card_multiverse_id('ebon dragon'/'DRB', '178021').

card_in_set('form of the dragon', 'DRB').
card_original_type('form of the dragon'/'DRB', 'Enchantment').
card_original_text('form of the dragon'/'DRB', 'At the beginning of your upkeep, Form of the Dragon deals 5 damage to target creature or player.\nAt the end of each turn, your life total becomes 5.\nCreatures without flying can\'t attack you.').
card_image_name('form of the dragon'/'DRB', 'form of the dragon').
card_uid('form of the dragon'/'DRB', 'DRB:Form of the Dragon:form of the dragon').
card_rarity('form of the dragon'/'DRB', 'Rare').
card_artist('form of the dragon'/'DRB', 'Daarken').
card_number('form of the dragon'/'DRB', '7').
card_multiverse_id('form of the dragon'/'DRB', '178016').

card_in_set('hellkite overlord', 'DRB').
card_original_type('hellkite overlord'/'DRB', 'Creature — Dragon').
card_original_text('hellkite overlord'/'DRB', 'Flying, trample, haste\n{R}: Hellkite Overlord gets +1/+0 until end of turn.\n{B}{G}: Regenerate Hellkite Overlord.').
card_first_print('hellkite overlord', 'DRB').
card_image_name('hellkite overlord'/'DRB', 'hellkite overlord').
card_uid('hellkite overlord'/'DRB', 'DRB:Hellkite Overlord:hellkite overlord').
card_rarity('hellkite overlord'/'DRB', 'Rare').
card_artist('hellkite overlord'/'DRB', 'Justin Sweet').
card_number('hellkite overlord'/'DRB', '8').
card_flavor_text('hellkite overlord'/'DRB', '\"The dragon has no pretense of compassion, no false mask of civilization—just hunger, heat, and need.\"\n—Sarkhan Vol').
card_multiverse_id('hellkite overlord'/'DRB', '178028').

card_in_set('kokusho, the evening star', 'DRB').
card_original_type('kokusho, the evening star'/'DRB', 'Legendary Creature — Dragon Spirit').
card_original_text('kokusho, the evening star'/'DRB', 'Flying\nWhen Kokusho, the Evening Star is put into a graveyard from play, each opponent loses 5 life. You gain life equal to the life lost this way.').
card_image_name('kokusho, the evening star'/'DRB', 'kokusho, the evening star').
card_uid('kokusho, the evening star'/'DRB', 'DRB:Kokusho, the Evening Star:kokusho, the evening star').
card_rarity('kokusho, the evening star'/'DRB', 'Rare').
card_artist('kokusho, the evening star'/'DRB', 'Tsutomu Kawade').
card_number('kokusho, the evening star'/'DRB', '9').
card_flavor_text('kokusho, the evening star'/'DRB', 'The fall of the evening star never heralds a gentle dawn.').
card_multiverse_id('kokusho, the evening star'/'DRB', '178022').

card_in_set('nicol bolas', 'DRB').
card_original_type('nicol bolas'/'DRB', 'Legendary Creature — Elder Dragon').
card_original_text('nicol bolas'/'DRB', 'Flying\nAt the beginning of your upkeep, sacrifice Nicol Bolas unless you pay {U}{B}{R}.\nWhenever Nicol Bolas deals damage to an opponent, that player discards his or her hand.').
card_image_name('nicol bolas'/'DRB', 'nicol bolas').
card_uid('nicol bolas'/'DRB', 'DRB:Nicol Bolas:nicol bolas').
card_rarity('nicol bolas'/'DRB', 'Rare').
card_artist('nicol bolas'/'DRB', 'D. Alexander Gregory').
card_number('nicol bolas'/'DRB', '10').
card_flavor_text('nicol bolas'/'DRB', 'Dominaria\'s most ancient evil.').
card_multiverse_id('nicol bolas'/'DRB', '178020').

card_in_set('niv-mizzet, the firemind', 'DRB').
card_original_type('niv-mizzet, the firemind'/'DRB', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, the firemind'/'DRB', 'Flying\nWhenever you draw a card, Niv-Mizzet, the Firemind deals 1 damage to target creature or player.\n{T}: Draw a card.').
card_image_name('niv-mizzet, the firemind'/'DRB', 'niv-mizzet, the firemind').
card_uid('niv-mizzet, the firemind'/'DRB', 'DRB:Niv-Mizzet, the Firemind:niv-mizzet, the firemind').
card_rarity('niv-mizzet, the firemind'/'DRB', 'Rare').
card_artist('niv-mizzet, the firemind'/'DRB', 'Todd Lockwood').
card_number('niv-mizzet, the firemind'/'DRB', '11').
card_flavor_text('niv-mizzet, the firemind'/'DRB', 'As brilliant as a cut diamond, and with just as cruel an edge.').
card_multiverse_id('niv-mizzet, the firemind'/'DRB', '178023').

card_in_set('rith, the awakener', 'DRB').
card_original_type('rith, the awakener'/'DRB', 'Legendary Creature — Dragon').
card_original_text('rith, the awakener'/'DRB', 'Flying\nWhenever Rith, the Awakener deals combat damage to a player, you may pay {2}{G}. If you do, choose a color. Put a 1/1 green Saproling creature token into play for each permanent of that color.').
card_image_name('rith, the awakener'/'DRB', 'rith, the awakener').
card_uid('rith, the awakener'/'DRB', 'DRB:Rith, the Awakener:rith, the awakener').
card_rarity('rith, the awakener'/'DRB', 'Rare').
card_artist('rith, the awakener'/'DRB', 'Todd Lockwood').
card_number('rith, the awakener'/'DRB', '12').
card_multiverse_id('rith, the awakener'/'DRB', '178024').

card_in_set('shivan dragon', 'DRB').
card_original_type('shivan dragon'/'DRB', 'Creature — Dragon').
card_original_text('shivan dragon'/'DRB', 'Flying\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'DRB', 'shivan dragon').
card_uid('shivan dragon'/'DRB', 'DRB:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'DRB', 'Rare').
card_artist('shivan dragon'/'DRB', 'Justin Sweet').
card_number('shivan dragon'/'DRB', '13').
card_flavor_text('shivan dragon'/'DRB', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'DRB', '178025').

card_in_set('thunder dragon', 'DRB').
card_original_type('thunder dragon'/'DRB', 'Creature — Dragon').
card_original_text('thunder dragon'/'DRB', 'Flying\nWhen Thunder Dragon comes into play, it deals 3 damage to each creature without flying.').
card_image_name('thunder dragon'/'DRB', 'thunder dragon').
card_uid('thunder dragon'/'DRB', 'DRB:Thunder Dragon:thunder dragon').
card_rarity('thunder dragon'/'DRB', 'Rare').
card_artist('thunder dragon'/'DRB', 'Chris Rahn').
card_number('thunder dragon'/'DRB', '14').
card_flavor_text('thunder dragon'/'DRB', 'Sometimes primitive cultures attributed natural phenomena to monsters. Sometimes they were right.').
card_multiverse_id('thunder dragon'/'DRB', '178026').

card_in_set('two-headed dragon', 'DRB').
card_original_type('two-headed dragon'/'DRB', 'Creature — Dragon').
card_original_text('two-headed dragon'/'DRB', 'Flying\n{1}{R}: Two-Headed Dragon gets +2/+0 until end of turn.\nTwo-Headed Dragon can\'t be blocked except by two or more creatures.\nTwo-Headed Dragon may block an additional creature.').
card_image_name('two-headed dragon'/'DRB', 'two-headed dragon').
card_uid('two-headed dragon'/'DRB', 'DRB:Two-Headed Dragon:two-headed dragon').
card_rarity('two-headed dragon'/'DRB', 'Rare').
card_artist('two-headed dragon'/'DRB', 'Sam Wood').
card_number('two-headed dragon'/'DRB', '15').
card_multiverse_id('two-headed dragon'/'DRB', '178027').
