% Judgment

set('JUD').
set_name('JUD', 'Judgment').
set_release_date('JUD', '2002-05-27').
set_border('JUD', 'black').
set_type('JUD', 'expansion').
set_block('JUD', 'Odyssey').

card_in_set('ancestor\'s chosen', 'JUD').
card_original_type('ancestor\'s chosen'/'JUD', 'Creature — Cleric').
card_original_text('ancestor\'s chosen'/'JUD', 'First strike\nWhen Ancestor\'s Chosen comes into play, you gain 1 life for each card in your graveyard.').
card_first_print('ancestor\'s chosen', 'JUD').
card_image_name('ancestor\'s chosen'/'JUD', 'ancestor\'s chosen').
card_uid('ancestor\'s chosen'/'JUD', 'JUD:Ancestor\'s Chosen:ancestor\'s chosen').
card_rarity('ancestor\'s chosen'/'JUD', 'Uncommon').
card_artist('ancestor\'s chosen'/'JUD', 'Pete Venters').
card_number('ancestor\'s chosen'/'JUD', '1').
card_flavor_text('ancestor\'s chosen'/'JUD', 'Empowered by generations of strength.').
card_multiverse_id('ancestor\'s chosen'/'JUD', '34243').

card_in_set('anger', 'JUD').
card_original_type('anger'/'JUD', 'Creature — Incarnation').
card_original_text('anger'/'JUD', 'Haste\nAs long as Anger is in your graveyard and you control a mountain, creatures you control have haste.').
card_first_print('anger', 'JUD').
card_image_name('anger'/'JUD', 'anger').
card_uid('anger'/'JUD', 'JUD:Anger:anger').
card_rarity('anger'/'JUD', 'Uncommon').
card_artist('anger'/'JUD', 'John Avon').
card_number('anger'/'JUD', '77').
card_flavor_text('anger'/'JUD', '\"For its time as a mortal, Anger chose a shell of boiling rock.\"\n—Scroll of Beginnings').
card_multiverse_id('anger'/'JUD', '33717').

card_in_set('anurid barkripper', 'JUD').
card_original_type('anurid barkripper'/'JUD', 'Creature — Beast').
card_original_text('anurid barkripper'/'JUD', 'Threshold Anurid Barkripper gets +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('anurid barkripper', 'JUD').
card_image_name('anurid barkripper'/'JUD', 'anurid barkripper').
card_uid('anurid barkripper'/'JUD', 'JUD:Anurid Barkripper:anurid barkripper').
card_rarity('anurid barkripper'/'JUD', 'Common').
card_artist('anurid barkripper'/'JUD', 'Randy Gallegos').
card_number('anurid barkripper'/'JUD', '104').
card_flavor_text('anurid barkripper'/'JUD', 'When it croaks, so do you.').
card_multiverse_id('anurid barkripper'/'JUD', '36039').

card_in_set('anurid brushhopper', 'JUD').
card_original_type('anurid brushhopper'/'JUD', 'Creature — Beast').
card_original_text('anurid brushhopper'/'JUD', 'Discard two cards from your hand: Remove Anurid Brushhopper from the game. Return it to play under its owner\'s control at end of turn.').
card_first_print('anurid brushhopper', 'JUD').
card_image_name('anurid brushhopper'/'JUD', 'anurid brushhopper').
card_uid('anurid brushhopper'/'JUD', 'JUD:Anurid Brushhopper:anurid brushhopper').
card_rarity('anurid brushhopper'/'JUD', 'Rare').
card_artist('anurid brushhopper'/'JUD', 'Arnie Swekel').
card_number('anurid brushhopper'/'JUD', '137').
card_flavor_text('anurid brushhopper'/'JUD', 'It\'s so tough it can frighten itself into hiding.').
card_multiverse_id('anurid brushhopper'/'JUD', '35059').

card_in_set('anurid swarmsnapper', 'JUD').
card_original_type('anurid swarmsnapper'/'JUD', 'Creature — Beast').
card_original_text('anurid swarmsnapper'/'JUD', 'Anurid Swarmsnapper may block as though it had flying.\n{1}{G}: Anurid Swarmsnapper may block an additional creature this turn.').
card_first_print('anurid swarmsnapper', 'JUD').
card_image_name('anurid swarmsnapper'/'JUD', 'anurid swarmsnapper').
card_uid('anurid swarmsnapper'/'JUD', 'JUD:Anurid Swarmsnapper:anurid swarmsnapper').
card_rarity('anurid swarmsnapper'/'JUD', 'Uncommon').
card_artist('anurid swarmsnapper'/'JUD', 'John Matson').
card_number('anurid swarmsnapper'/'JUD', '105').
card_flavor_text('anurid swarmsnapper'/'JUD', 'Anurids have the problem of airborne invaders licked.').
card_multiverse_id('anurid swarmsnapper'/'JUD', '36453').

card_in_set('arcane teachings', 'JUD').
card_original_type('arcane teachings'/'JUD', 'Enchant Creature').
card_original_text('arcane teachings'/'JUD', 'Enchanted creature gets +2/+2 and has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_first_print('arcane teachings', 'JUD').
card_image_name('arcane teachings'/'JUD', 'arcane teachings').
card_uid('arcane teachings'/'JUD', 'JUD:Arcane Teachings:arcane teachings').
card_rarity('arcane teachings'/'JUD', 'Common').
card_artist('arcane teachings'/'JUD', 'Mark Brill').
card_number('arcane teachings'/'JUD', '78').
card_flavor_text('arcane teachings'/'JUD', 'Dwarves may teach at the speed of stone, but their results are solid.').
card_multiverse_id('arcane teachings'/'JUD', '36430').

card_in_set('aven fogbringer', 'JUD').
card_original_type('aven fogbringer'/'JUD', 'Creature — Bird Wizard').
card_original_text('aven fogbringer'/'JUD', 'Flying\nWhen Aven Fogbringer comes into play, return target land to its owner\'s hand.').
card_first_print('aven fogbringer', 'JUD').
card_image_name('aven fogbringer'/'JUD', 'aven fogbringer').
card_uid('aven fogbringer'/'JUD', 'JUD:Aven Fogbringer:aven fogbringer').
card_rarity('aven fogbringer'/'JUD', 'Common').
card_artist('aven fogbringer'/'JUD', 'Edward P. Beard, Jr.').
card_number('aven fogbringer'/'JUD', '34').
card_flavor_text('aven fogbringer'/'JUD', '\"I cover the land with blankets and it sleeps.\"').
card_multiverse_id('aven fogbringer'/'JUD', '21275').

card_in_set('aven warcraft', 'JUD').
card_original_type('aven warcraft'/'JUD', 'Instant').
card_original_text('aven warcraft'/'JUD', 'Creatures you control get +0/+2 until end of turn.\nThreshold Creatures you control also gain protection from the color of your choice until end of turn. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('aven warcraft', 'JUD').
card_image_name('aven warcraft'/'JUD', 'aven warcraft').
card_uid('aven warcraft'/'JUD', 'JUD:Aven Warcraft:aven warcraft').
card_rarity('aven warcraft'/'JUD', 'Uncommon').
card_artist('aven warcraft'/'JUD', 'Roger Raupp').
card_number('aven warcraft'/'JUD', '2').
card_multiverse_id('aven warcraft'/'JUD', '34865').

card_in_set('balthor the defiled', 'JUD').
card_original_type('balthor the defiled'/'JUD', 'Creature — Zombie Dwarf Legend').
card_original_text('balthor the defiled'/'JUD', 'All Minions get +1/+1.\n{B}{B}{B}, Remove Balthor the Defiled from the game: Each player returns all black and all red creature cards from his or her graveyard to play.').
card_first_print('balthor the defiled', 'JUD').
card_image_name('balthor the defiled'/'JUD', 'balthor the defiled').
card_uid('balthor the defiled'/'JUD', 'JUD:Balthor the Defiled:balthor the defiled').
card_rarity('balthor the defiled'/'JUD', 'Rare').
card_artist('balthor the defiled'/'JUD', 'Carl Critchlow').
card_number('balthor the defiled'/'JUD', '61').
card_flavor_text('balthor the defiled'/'JUD', 'He remembers enough of his life to weep for what he has lost.').
card_multiverse_id('balthor the defiled'/'JUD', '20842').

card_in_set('barbarian bully', 'JUD').
card_original_type('barbarian bully'/'JUD', 'Creature — Barbarian').
card_original_text('barbarian bully'/'JUD', 'Discard a card at random from your hand: Barbarian Bully gets +2/+2 until end of turn unless a player has Barbarian Bully deal 4 damage to him or her. Play this ability only once each turn.').
card_first_print('barbarian bully', 'JUD').
card_image_name('barbarian bully'/'JUD', 'barbarian bully').
card_uid('barbarian bully'/'JUD', 'JUD:Barbarian Bully:barbarian bully').
card_rarity('barbarian bully'/'JUD', 'Common').
card_artist('barbarian bully'/'JUD', 'Mike Ploog').
card_number('barbarian bully'/'JUD', '79').
card_flavor_text('barbarian bully'/'JUD', '\"Out of my way!\"').
card_multiverse_id('barbarian bully'/'JUD', '34818').

card_in_set('battle screech', 'JUD').
card_original_type('battle screech'/'JUD', 'Sorcery').
card_original_text('battle screech'/'JUD', 'Put two 1/1 white Bird creature tokens with flying into play.\nFlashback—Tap three untapped white creatures you control. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('battle screech', 'JUD').
card_image_name('battle screech'/'JUD', 'battle screech').
card_uid('battle screech'/'JUD', 'JUD:Battle Screech:battle screech').
card_rarity('battle screech'/'JUD', 'Uncommon').
card_artist('battle screech'/'JUD', 'Randy Gallegos').
card_number('battle screech'/'JUD', '3').
card_multiverse_id('battle screech'/'JUD', '35079').

card_in_set('battlefield scrounger', 'JUD').
card_original_type('battlefield scrounger'/'JUD', 'Creature — Centaur').
card_original_text('battlefield scrounger'/'JUD', 'Threshold Put three cards from your graveyard on the bottom of your library: Battlefield Scrounger gets +3/+3 until end of turn. Play this ability only once each turn. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('battlefield scrounger', 'JUD').
card_image_name('battlefield scrounger'/'JUD', 'battlefield scrounger').
card_uid('battlefield scrounger'/'JUD', 'JUD:Battlefield Scrounger:battlefield scrounger').
card_rarity('battlefield scrounger'/'JUD', 'Common').
card_artist('battlefield scrounger'/'JUD', 'Daren Bader').
card_number('battlefield scrounger'/'JUD', '106').
card_multiverse_id('battlefield scrounger'/'JUD', '36040').

card_in_set('battlewise aven', 'JUD').
card_original_type('battlewise aven'/'JUD', 'Creature — Bird Soldier').
card_original_text('battlewise aven'/'JUD', 'Flying\nThreshold Battlewise Aven gets +1/+1 and has first strike. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('battlewise aven', 'JUD').
card_image_name('battlewise aven'/'JUD', 'battlewise aven').
card_uid('battlewise aven'/'JUD', 'JUD:Battlewise Aven:battlewise aven').
card_rarity('battlewise aven'/'JUD', 'Common').
card_artist('battlewise aven'/'JUD', 'Wayne England').
card_number('battlewise aven'/'JUD', '4').
card_flavor_text('battlewise aven'/'JUD', 'Experience is a good teacher, not a kind one.').
card_multiverse_id('battlewise aven'/'JUD', '34464').

card_in_set('benevolent bodyguard', 'JUD').
card_original_type('benevolent bodyguard'/'JUD', 'Creature — Cleric').
card_original_text('benevolent bodyguard'/'JUD', 'Sacrifice Benevolent Bodyguard: Target creature you control gains protection from the color of your choice until end of turn.').
card_first_print('benevolent bodyguard', 'JUD').
card_image_name('benevolent bodyguard'/'JUD', 'benevolent bodyguard').
card_uid('benevolent bodyguard'/'JUD', 'JUD:Benevolent Bodyguard:benevolent bodyguard').
card_rarity('benevolent bodyguard'/'JUD', 'Common').
card_artist('benevolent bodyguard'/'JUD', 'Roger Raupp').
card_number('benevolent bodyguard'/'JUD', '5').
card_flavor_text('benevolent bodyguard'/'JUD', '\"My destiny is to save others so their destinies may be achieved.\"').
card_multiverse_id('benevolent bodyguard'/'JUD', '36118').

card_in_set('book burning', 'JUD').
card_original_type('book burning'/'JUD', 'Sorcery').
card_original_text('book burning'/'JUD', 'Unless a player has Book Burning deal 6 damage to him or her, put the top six cards of target player\'s library into his or her graveyard.').
card_first_print('book burning', 'JUD').
card_image_name('book burning'/'JUD', 'book burning').
card_uid('book burning'/'JUD', 'JUD:Book Burning:book burning').
card_rarity('book burning'/'JUD', 'Common').
card_artist('book burning'/'JUD', 'Dave Dorman').
card_number('book burning'/'JUD', '80').
card_flavor_text('book burning'/'JUD', 'The wizard\'s spellbook was full of burning questions.').
card_multiverse_id('book burning'/'JUD', '35891').

card_in_set('border patrol', 'JUD').
card_original_type('border patrol'/'JUD', 'Creature — Nomad').
card_original_text('border patrol'/'JUD', 'Attacking doesn\'t cause Border Patrol to tap.').
card_first_print('border patrol', 'JUD').
card_image_name('border patrol'/'JUD', 'border patrol').
card_uid('border patrol'/'JUD', 'JUD:Border Patrol:border patrol').
card_rarity('border patrol'/'JUD', 'Common').
card_artist('border patrol'/'JUD', 'Roger Raupp').
card_number('border patrol'/'JUD', '6').
card_flavor_text('border patrol'/'JUD', '\"We have been victims of treachery, of the Cabal, and of our own ambitions. No more.\"\n—Commander Eesha').
card_multiverse_id('border patrol'/'JUD', '34201').

card_in_set('brawn', 'JUD').
card_original_type('brawn'/'JUD', 'Creature — Incarnation').
card_original_text('brawn'/'JUD', 'Trample\nAs long as Brawn is in your graveyard and you control a forest, creatures you control have trample.').
card_first_print('brawn', 'JUD').
card_image_name('brawn'/'JUD', 'brawn').
card_uid('brawn'/'JUD', 'JUD:Brawn:brawn').
card_rarity('brawn'/'JUD', 'Uncommon').
card_artist('brawn'/'JUD', 'Matt Cavotta').
card_number('brawn'/'JUD', '107').
card_flavor_text('brawn'/'JUD', '\"‘I have arrived,\' bellowed Brawn, and the plane shuddered.\"\n—Scroll of Beginnings').
card_multiverse_id('brawn'/'JUD', '33721').

card_in_set('breaking point', 'JUD').
card_original_type('breaking point'/'JUD', 'Sorcery').
card_original_text('breaking point'/'JUD', 'Destroy all creatures unless a player has Breaking Point deal 6 damage to him or her. Creatures destroyed this way can\'t be regenerated.').
card_first_print('breaking point', 'JUD').
card_image_name('breaking point'/'JUD', 'breaking point').
card_uid('breaking point'/'JUD', 'JUD:Breaking Point:breaking point').
card_rarity('breaking point'/'JUD', 'Rare').
card_artist('breaking point'/'JUD', 'Matthew D. Wilson').
card_number('breaking point'/'JUD', '81').
card_flavor_text('breaking point'/'JUD', '\"Enough!\"').
card_multiverse_id('breaking point'/'JUD', '29856').

card_in_set('browbeat', 'JUD').
card_original_type('browbeat'/'JUD', 'Sorcery').
card_original_text('browbeat'/'JUD', 'Unless a player has Browbeat deal 5 damage to him or her, target player draws three cards.').
card_image_name('browbeat'/'JUD', 'browbeat').
card_uid('browbeat'/'JUD', 'JUD:Browbeat:browbeat').
card_rarity('browbeat'/'JUD', 'Uncommon').
card_artist('browbeat'/'JUD', 'Mark Tedin').
card_number('browbeat'/'JUD', '82').
card_flavor_text('browbeat'/'JUD', '\"Even the threat of power has power.\"\n—Jeska, warrior adept').
card_multiverse_id('browbeat'/'JUD', '36038').

card_in_set('burning wish', 'JUD').
card_original_type('burning wish'/'JUD', 'Sorcery').
card_original_text('burning wish'/'JUD', 'Choose a sorcery card you own from outside the game, reveal that card, and put it into your hand. Remove Burning Wish from the game.').
card_image_name('burning wish'/'JUD', 'burning wish').
card_uid('burning wish'/'JUD', 'JUD:Burning Wish:burning wish').
card_rarity('burning wish'/'JUD', 'Rare').
card_artist('burning wish'/'JUD', 'Scott M. Fischer').
card_number('burning wish'/'JUD', '83').
card_flavor_text('burning wish'/'JUD', 'She wished for a weapon, but not for the skill to wield it.').
card_multiverse_id('burning wish'/'JUD', '34403').

card_in_set('cabal therapy', 'JUD').
card_original_type('cabal therapy'/'JUD', 'Sorcery').
card_original_text('cabal therapy'/'JUD', 'Name a nonland card. Target player reveals his or her hand and discards from it all cards with that name.\nFlashback—Sacrifice a creature. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('cabal therapy'/'JUD', 'cabal therapy').
card_uid('cabal therapy'/'JUD', 'JUD:Cabal Therapy:cabal therapy').
card_rarity('cabal therapy'/'JUD', 'Uncommon').
card_artist('cabal therapy'/'JUD', 'Ron Spencer').
card_number('cabal therapy'/'JUD', '62').
card_multiverse_id('cabal therapy'/'JUD', '34789').

card_in_set('cabal trainee', 'JUD').
card_original_type('cabal trainee'/'JUD', 'Creature — Minion').
card_original_text('cabal trainee'/'JUD', 'Sacrifice Cabal Trainee: Target creature gets -2/-0 until end of turn.').
card_first_print('cabal trainee', 'JUD').
card_image_name('cabal trainee'/'JUD', 'cabal trainee').
card_uid('cabal trainee'/'JUD', 'JUD:Cabal Trainee:cabal trainee').
card_rarity('cabal trainee'/'JUD', 'Common').
card_artist('cabal trainee'/'JUD', 'Pete Venters').
card_number('cabal trainee'/'JUD', '63').
card_flavor_text('cabal trainee'/'JUD', '\"Ah, a faceshredder. Very rare. Nearly unstoppable. Good luck\"\n—Cabal instructor').
card_multiverse_id('cabal trainee'/'JUD', '36117').

card_in_set('cagemail', 'JUD').
card_original_type('cagemail'/'JUD', 'Enchant Creature').
card_original_text('cagemail'/'JUD', 'Enchanted creature gets +2/+2 and can\'t attack.').
card_first_print('cagemail', 'JUD').
card_image_name('cagemail'/'JUD', 'cagemail').
card_uid('cagemail'/'JUD', 'JUD:Cagemail:cagemail').
card_rarity('cagemail'/'JUD', 'Common').
card_artist('cagemail'/'JUD', 'Scott M. Fischer').
card_number('cagemail'/'JUD', '7').
card_flavor_text('cagemail'/'JUD', '\"Power often costs too high a price.\"\n—Commander Eesha').
card_multiverse_id('cagemail'/'JUD', '36429').

card_in_set('canopy claws', 'JUD').
card_original_type('canopy claws'/'JUD', 'Instant').
card_original_text('canopy claws'/'JUD', 'Target creature loses flying until end of turn.\nFlashback {G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('canopy claws', 'JUD').
card_image_name('canopy claws'/'JUD', 'canopy claws').
card_uid('canopy claws'/'JUD', 'JUD:Canopy Claws:canopy claws').
card_rarity('canopy claws'/'JUD', 'Common').
card_artist('canopy claws'/'JUD', 'Matthew Mitchell').
card_number('canopy claws'/'JUD', '108').
card_multiverse_id('canopy claws'/'JUD', '36410').

card_in_set('centaur rootcaster', 'JUD').
card_original_type('centaur rootcaster'/'JUD', 'Creature — Centaur Druid').
card_original_text('centaur rootcaster'/'JUD', 'Whenever Centaur Rootcaster deals combat damage to a player, you may search your library for a basic land card and put that card into play tapped. If you do, shuffle your library.').
card_first_print('centaur rootcaster', 'JUD').
card_image_name('centaur rootcaster'/'JUD', 'centaur rootcaster').
card_uid('centaur rootcaster'/'JUD', 'JUD:Centaur Rootcaster:centaur rootcaster').
card_rarity('centaur rootcaster'/'JUD', 'Common').
card_artist('centaur rootcaster'/'JUD', 'Eric Peterson').
card_number('centaur rootcaster'/'JUD', '109').
card_multiverse_id('centaur rootcaster'/'JUD', '37120').

card_in_set('cephalid constable', 'JUD').
card_original_type('cephalid constable'/'JUD', 'Creature — Cephalid Wizard').
card_original_text('cephalid constable'/'JUD', 'Whenever Cephalid Constable deals combat damage to a player, return up to X target permanents that player controls to their owners\' hands, where X is the damage it dealt to that player.').
card_first_print('cephalid constable', 'JUD').
card_image_name('cephalid constable'/'JUD', 'cephalid constable').
card_uid('cephalid constable'/'JUD', 'JUD:Cephalid Constable:cephalid constable').
card_rarity('cephalid constable'/'JUD', 'Rare').
card_artist('cephalid constable'/'JUD', 'Alan Pollack').
card_number('cephalid constable'/'JUD', '35').
card_flavor_text('cephalid constable'/'JUD', 'Cephalids don\'t police people. They police loyalties.').
card_multiverse_id('cephalid constable'/'JUD', '26728').

card_in_set('cephalid inkshrouder', 'JUD').
card_original_type('cephalid inkshrouder'/'JUD', 'Creature — Cephalid').
card_original_text('cephalid inkshrouder'/'JUD', 'Discard a card from your hand: Cephalid Inkshrouder can\'t be the target of spells or abilities and is unblockable this turn.').
card_first_print('cephalid inkshrouder', 'JUD').
card_image_name('cephalid inkshrouder'/'JUD', 'cephalid inkshrouder').
card_uid('cephalid inkshrouder'/'JUD', 'JUD:Cephalid Inkshrouder:cephalid inkshrouder').
card_rarity('cephalid inkshrouder'/'JUD', 'Uncommon').
card_artist('cephalid inkshrouder'/'JUD', 'Tony Szczudlo').
card_number('cephalid inkshrouder'/'JUD', '36').
card_flavor_text('cephalid inkshrouder'/'JUD', 'Cephalids shrink from the harsh glare of scrutiny.').
card_multiverse_id('cephalid inkshrouder'/'JUD', '34784').

card_in_set('chastise', 'JUD').
card_original_type('chastise'/'JUD', 'Instant').
card_original_text('chastise'/'JUD', 'Destroy target attacking creature. You gain life equal to its power.').
card_first_print('chastise', 'JUD').
card_image_name('chastise'/'JUD', 'chastise').
card_uid('chastise'/'JUD', 'JUD:Chastise:chastise').
card_rarity('chastise'/'JUD', 'Uncommon').
card_artist('chastise'/'JUD', 'Carl Critchlow').
card_number('chastise'/'JUD', '8').
card_flavor_text('chastise'/'JUD', '\"Why do we pray to the Ancestor? Because She listens.\"\n—Mystic elder').
card_multiverse_id('chastise'/'JUD', '34958').

card_in_set('commander eesha', 'JUD').
card_original_type('commander eesha'/'JUD', 'Creature — Bird Soldier Legend').
card_original_text('commander eesha'/'JUD', 'Flying, protection from creatures').
card_first_print('commander eesha', 'JUD').
card_image_name('commander eesha'/'JUD', 'commander eesha').
card_uid('commander eesha'/'JUD', 'JUD:Commander Eesha:commander eesha').
card_rarity('commander eesha'/'JUD', 'Rare').
card_artist('commander eesha'/'JUD', 'Rebecca Guay').
card_number('commander eesha'/'JUD', '9').
card_flavor_text('commander eesha'/'JUD', '\"War glides on the simplest updrafts while peace struggles against hurricane winds. It is the way of the world. It must change.\"').
card_multiverse_id('commander eesha'/'JUD', '35893').

card_in_set('crush of wurms', 'JUD').
card_original_type('crush of wurms'/'JUD', 'Sorcery').
card_original_text('crush of wurms'/'JUD', 'Put three 6/6 green Wurm creature tokens into play.\nFlashback {9}{G}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('crush of wurms', 'JUD').
card_image_name('crush of wurms'/'JUD', 'crush of wurms').
card_uid('crush of wurms'/'JUD', 'JUD:Crush of Wurms:crush of wurms').
card_rarity('crush of wurms'/'JUD', 'Rare').
card_artist('crush of wurms'/'JUD', 'Christopher Moeller').
card_number('crush of wurms'/'JUD', '110').
card_multiverse_id('crush of wurms'/'JUD', '35170').

card_in_set('cunning wish', 'JUD').
card_original_type('cunning wish'/'JUD', 'Instant').
card_original_text('cunning wish'/'JUD', 'Choose an instant card you own from outside the game, reveal that card, and put it into your hand. Remove Cunning Wish from the game.').
card_image_name('cunning wish'/'JUD', 'cunning wish').
card_uid('cunning wish'/'JUD', 'JUD:Cunning Wish:cunning wish').
card_rarity('cunning wish'/'JUD', 'Rare').
card_artist('cunning wish'/'JUD', 'Jim Nelson').
card_number('cunning wish'/'JUD', '37').
card_flavor_text('cunning wish'/'JUD', 'He wished for knowledge, but not for the will to apply it.').
card_multiverse_id('cunning wish'/'JUD', '34400').

card_in_set('death wish', 'JUD').
card_original_type('death wish'/'JUD', 'Sorcery').
card_original_text('death wish'/'JUD', 'Choose a card you own from outside the game and put it into your hand. You lose half your life, rounded up. Remove Death Wish from the game.').
card_first_print('death wish', 'JUD').
card_image_name('death wish'/'JUD', 'death wish').
card_uid('death wish'/'JUD', 'JUD:Death Wish:death wish').
card_rarity('death wish'/'JUD', 'Rare').
card_artist('death wish'/'JUD', 'Jeff Easley').
card_number('death wish'/'JUD', '64').
card_flavor_text('death wish'/'JUD', 'He wished for power, but not for the longevity to abuse it.').
card_multiverse_id('death wish'/'JUD', '34402').

card_in_set('defy gravity', 'JUD').
card_original_type('defy gravity'/'JUD', 'Instant').
card_original_text('defy gravity'/'JUD', 'Target creature gains flying until end of turn.\nFlashback {U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('defy gravity', 'JUD').
card_image_name('defy gravity'/'JUD', 'defy gravity').
card_uid('defy gravity'/'JUD', 'JUD:Defy Gravity:defy gravity').
card_rarity('defy gravity'/'JUD', 'Common').
card_artist('defy gravity'/'JUD', 'Ben Thompson').
card_number('defy gravity'/'JUD', '38').
card_flavor_text('defy gravity'/'JUD', 'Magical or not, it\'s still a cloud.').
card_multiverse_id('defy gravity'/'JUD', '35076').

card_in_set('dwarven bloodboiler', 'JUD').
card_original_type('dwarven bloodboiler'/'JUD', 'Creature — Dwarf').
card_original_text('dwarven bloodboiler'/'JUD', 'Tap an untapped Dwarf you control: Target creature gets +2/+0 until end of turn.').
card_first_print('dwarven bloodboiler', 'JUD').
card_image_name('dwarven bloodboiler'/'JUD', 'dwarven bloodboiler').
card_uid('dwarven bloodboiler'/'JUD', 'JUD:Dwarven Bloodboiler:dwarven bloodboiler').
card_rarity('dwarven bloodboiler'/'JUD', 'Rare').
card_artist('dwarven bloodboiler'/'JUD', 'Arnie Swekel').
card_number('dwarven bloodboiler'/'JUD', '84').
card_flavor_text('dwarven bloodboiler'/'JUD', 'His battle cry unleashes an avalanche of dwarves.').
card_multiverse_id('dwarven bloodboiler'/'JUD', '35074').

card_in_set('dwarven driller', 'JUD').
card_original_type('dwarven driller'/'JUD', 'Creature — Dwarf').
card_original_text('dwarven driller'/'JUD', '{T}: Destroy target land unless its controller has Dwarven Driller deal 2 damage to him or her.').
card_first_print('dwarven driller', 'JUD').
card_image_name('dwarven driller'/'JUD', 'dwarven driller').
card_uid('dwarven driller'/'JUD', 'JUD:Dwarven Driller:dwarven driller').
card_rarity('dwarven driller'/'JUD', 'Uncommon').
card_artist('dwarven driller'/'JUD', 'Edward P. Beard, Jr.').
card_number('dwarven driller'/'JUD', '85').
card_flavor_text('dwarven driller'/'JUD', '\"Drill the planet, it\'s no trouble:\nMining granite, making rubble.\"\n—Dwarven drilling song').
card_multiverse_id('dwarven driller'/'JUD', '34950').

card_in_set('dwarven scorcher', 'JUD').
card_original_type('dwarven scorcher'/'JUD', 'Creature — Dwarf').
card_original_text('dwarven scorcher'/'JUD', 'Sacrifice Dwarven Scorcher: Dwarven Scorcher deals 1 damage to target creature unless that creature\'s controller has Dwarven Scorcher deal 2 damage to him or her.').
card_first_print('dwarven scorcher', 'JUD').
card_image_name('dwarven scorcher'/'JUD', 'dwarven scorcher').
card_uid('dwarven scorcher'/'JUD', 'JUD:Dwarven Scorcher:dwarven scorcher').
card_rarity('dwarven scorcher'/'JUD', 'Common').
card_artist('dwarven scorcher'/'JUD', 'Thomas M. Baxa').
card_number('dwarven scorcher'/'JUD', '86').
card_flavor_text('dwarven scorcher'/'JUD', 'Barbarians invented the blaze of glory. Dwarves perfected it.').
card_multiverse_id('dwarven scorcher'/'JUD', '36116').

card_in_set('earsplitting rats', 'JUD').
card_original_type('earsplitting rats'/'JUD', 'Creature — Rat').
card_original_text('earsplitting rats'/'JUD', 'When Earsplitting Rats comes into play, each player discards a card from his or her hand.\nDiscard a card from your hand: Regenerate Earsplitting Rats.').
card_first_print('earsplitting rats', 'JUD').
card_image_name('earsplitting rats'/'JUD', 'earsplitting rats').
card_uid('earsplitting rats'/'JUD', 'JUD:Earsplitting Rats:earsplitting rats').
card_rarity('earsplitting rats'/'JUD', 'Common').
card_artist('earsplitting rats'/'JUD', 'Heather Hudson').
card_number('earsplitting rats'/'JUD', '65').
card_flavor_text('earsplitting rats'/'JUD', 'Rats come in one amount: too many.').
card_multiverse_id('earsplitting rats'/'JUD', '35080').

card_in_set('elephant guide', 'JUD').
card_original_type('elephant guide'/'JUD', 'Enchant Creature').
card_original_text('elephant guide'/'JUD', 'Enchanted creature gets +3/+3.\nWhen enchanted creature is put into a graveyard, put a 3/3 green Elephant creature token into play.').
card_first_print('elephant guide', 'JUD').
card_image_name('elephant guide'/'JUD', 'elephant guide').
card_uid('elephant guide'/'JUD', 'JUD:Elephant Guide:elephant guide').
card_rarity('elephant guide'/'JUD', 'Uncommon').
card_artist('elephant guide'/'JUD', 'Jim Nelson').
card_number('elephant guide'/'JUD', '111').
card_flavor_text('elephant guide'/'JUD', 'Nature\'s strength outlives the strong.').
card_multiverse_id('elephant guide'/'JUD', '12692').

card_in_set('ember shot', 'JUD').
card_original_type('ember shot'/'JUD', 'Instant').
card_original_text('ember shot'/'JUD', 'Ember Shot deals 3 damage to target creature or player.\nDraw a card.').
card_first_print('ember shot', 'JUD').
card_image_name('ember shot'/'JUD', 'ember shot').
card_uid('ember shot'/'JUD', 'JUD:Ember Shot:ember shot').
card_rarity('ember shot'/'JUD', 'Common').
card_artist('ember shot'/'JUD', 'Alan Pollack').
card_number('ember shot'/'JUD', '87').
card_flavor_text('ember shot'/'JUD', 'Dwarves bring poor coal to market, use good coal in their homes, and throw their best coal away.').
card_multiverse_id('ember shot'/'JUD', '29862').

card_in_set('envelop', 'JUD').
card_original_type('envelop'/'JUD', 'Instant').
card_original_text('envelop'/'JUD', 'Counter target sorcery spell.').
card_first_print('envelop', 'JUD').
card_image_name('envelop'/'JUD', 'envelop').
card_uid('envelop'/'JUD', 'JUD:Envelop:envelop').
card_rarity('envelop'/'JUD', 'Common').
card_artist('envelop'/'JUD', 'Don Hazeltine').
card_number('envelop'/'JUD', '39').
card_flavor_text('envelop'/'JUD', '\"What you made will be unmade. What you learned will be unlearned. And when you\'re done, you will be undone.\"\n—Ambassador Laquatus').
card_multiverse_id('envelop'/'JUD', '36414').

card_in_set('epic struggle', 'JUD').
card_original_type('epic struggle'/'JUD', 'Enchantment').
card_original_text('epic struggle'/'JUD', 'At the beginning of your upkeep, if you control twenty or more creatures, you win the game.').
card_first_print('epic struggle', 'JUD').
card_image_name('epic struggle'/'JUD', 'epic struggle').
card_uid('epic struggle'/'JUD', 'JUD:Epic Struggle:epic struggle').
card_rarity('epic struggle'/'JUD', 'Rare').
card_artist('epic struggle'/'JUD', 'Greg & Tim Hildebrandt').
card_number('epic struggle'/'JUD', '112').
card_flavor_text('epic struggle'/'JUD', '\"If our foes will not listen to words, perhaps hooves and claws will make them take notice.\"\n—Seton, centaur druid').
card_multiverse_id('epic struggle'/'JUD', '33697').

card_in_set('erhnam djinn', 'JUD').
card_original_type('erhnam djinn'/'JUD', 'Creature — Djinn').
card_original_text('erhnam djinn'/'JUD', 'At the beginning of your upkeep, target non-Wall creature an opponent controls gains forestwalk until your next upkeep.').
card_image_name('erhnam djinn'/'JUD', 'erhnam djinn').
card_uid('erhnam djinn'/'JUD', 'JUD:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'JUD', 'Rare').
card_artist('erhnam djinn'/'JUD', 'Greg Staples').
card_number('erhnam djinn'/'JUD', '113').
card_flavor_text('erhnam djinn'/'JUD', 'He provides a safe path to nowhere.').
card_multiverse_id('erhnam djinn'/'JUD', '35553').

card_in_set('exoskeletal armor', 'JUD').
card_original_type('exoskeletal armor'/'JUD', 'Enchant Creature').
card_original_text('exoskeletal armor'/'JUD', 'Enchanted creature gets +X/+X, where X is the number of creature cards in all graveyards.').
card_first_print('exoskeletal armor', 'JUD').
card_image_name('exoskeletal armor'/'JUD', 'exoskeletal armor').
card_uid('exoskeletal armor'/'JUD', 'JUD:Exoskeletal Armor:exoskeletal armor').
card_rarity('exoskeletal armor'/'JUD', 'Uncommon').
card_artist('exoskeletal armor'/'JUD', 'Wayne England').
card_number('exoskeletal armor'/'JUD', '114').
card_flavor_text('exoskeletal armor'/'JUD', '\"We use only what we need—but in times of strife, our need becomes great.\"\n—Centaur warrior').
card_multiverse_id('exoskeletal armor'/'JUD', '29885').

card_in_set('filth', 'JUD').
card_original_type('filth'/'JUD', 'Creature — Incarnation').
card_original_text('filth'/'JUD', 'Swampwalk\nAs long as Filth is in your graveyard and you control a swamp, creatures you control have swampwalk.').
card_first_print('filth', 'JUD').
card_image_name('filth'/'JUD', 'filth').
card_uid('filth'/'JUD', 'JUD:Filth:filth').
card_rarity('filth'/'JUD', 'Uncommon').
card_artist('filth'/'JUD', 'Thomas M. Baxa').
card_number('filth'/'JUD', '66').
card_flavor_text('filth'/'JUD', '\"As the portal closed, Filth oozed through. The stench of evil followed it in.\"\n—Scroll of Beginnings').
card_multiverse_id('filth'/'JUD', '33722').

card_in_set('firecat blitz', 'JUD').
card_original_type('firecat blitz'/'JUD', 'Sorcery').
card_original_text('firecat blitz'/'JUD', 'Put X 1/1 red Cat creature tokens with haste into play. Remove them from the game at end of turn.\nFlashback—{R}{R}, Sacrifice X mountains. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('firecat blitz', 'JUD').
card_image_name('firecat blitz'/'JUD', 'firecat blitz').
card_uid('firecat blitz'/'JUD', 'JUD:Firecat Blitz:firecat blitz').
card_rarity('firecat blitz'/'JUD', 'Uncommon').
card_artist('firecat blitz'/'JUD', 'David Martin').
card_number('firecat blitz'/'JUD', '88').
card_multiverse_id('firecat blitz'/'JUD', '34929').

card_in_set('flaring pain', 'JUD').
card_original_type('flaring pain'/'JUD', 'Instant').
card_original_text('flaring pain'/'JUD', 'Damage can\'t be prevented this turn.\nFlashback {R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('flaring pain', 'JUD').
card_image_name('flaring pain'/'JUD', 'flaring pain').
card_uid('flaring pain'/'JUD', 'JUD:Flaring Pain:flaring pain').
card_rarity('flaring pain'/'JUD', 'Common').
card_artist('flaring pain'/'JUD', 'Glen Angus').
card_number('flaring pain'/'JUD', '89').
card_flavor_text('flaring pain'/'JUD', '\"I don\'t rub salt in wounds. I use sulfur.\"\n—Matoc, lavamancer').
card_multiverse_id('flaring pain'/'JUD', '34214').

card_in_set('flash of insight', 'JUD').
card_original_type('flash of insight'/'JUD', 'Instant').
card_original_text('flash of insight'/'JUD', 'Look at the top X cards of your library. Put one of them into your hand and the rest on the bottom of your library.\nFlashback—{1}{U}, Remove X blue cards in your graveyard from the game. (You can\'t remove Flash of Insight to pay for its own flashback cost.)').
card_first_print('flash of insight', 'JUD').
card_image_name('flash of insight'/'JUD', 'flash of insight').
card_uid('flash of insight'/'JUD', 'JUD:Flash of Insight:flash of insight').
card_rarity('flash of insight'/'JUD', 'Uncommon').
card_artist('flash of insight'/'JUD', 'Ben Thompson').
card_number('flash of insight'/'JUD', '40').
card_multiverse_id('flash of insight'/'JUD', '29831').

card_in_set('fledgling dragon', 'JUD').
card_original_type('fledgling dragon'/'JUD', 'Creature — Dragon').
card_original_text('fledgling dragon'/'JUD', 'Flying\nThreshold Fledgling Dragon gets +3/+3 and has \"{R}: Fledgling Dragon gets +1/+0 until end of turn.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('fledgling dragon', 'JUD').
card_image_name('fledgling dragon'/'JUD', 'fledgling dragon').
card_uid('fledgling dragon'/'JUD', 'JUD:Fledgling Dragon:fledgling dragon').
card_rarity('fledgling dragon'/'JUD', 'Rare').
card_artist('fledgling dragon'/'JUD', 'Greg Staples').
card_number('fledgling dragon'/'JUD', '90').
card_multiverse_id('fledgling dragon'/'JUD', '34930').

card_in_set('folk medicine', 'JUD').
card_original_type('folk medicine'/'JUD', 'Instant').
card_original_text('folk medicine'/'JUD', 'You gain 1 life for each creature you control.\nFlashback {1}{W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('folk medicine', 'JUD').
card_image_name('folk medicine'/'JUD', 'folk medicine').
card_uid('folk medicine'/'JUD', 'JUD:Folk Medicine:folk medicine').
card_rarity('folk medicine'/'JUD', 'Common').
card_artist('folk medicine'/'JUD', 'Matt Cavotta').
card_number('folk medicine'/'JUD', '115').
card_flavor_text('folk medicine'/'JUD', 'Sometimes the healers need healing.').
card_multiverse_id('folk medicine'/'JUD', '35071').

card_in_set('forcemage advocate', 'JUD').
card_original_type('forcemage advocate'/'JUD', 'Creature — Centaur').
card_original_text('forcemage advocate'/'JUD', '{T}: Return target card in an opponent\'s graveyard to his or her hand. Put a +1/+1 counter on target creature.').
card_first_print('forcemage advocate', 'JUD').
card_image_name('forcemage advocate'/'JUD', 'forcemage advocate').
card_uid('forcemage advocate'/'JUD', 'JUD:Forcemage Advocate:forcemage advocate').
card_rarity('forcemage advocate'/'JUD', 'Uncommon').
card_artist('forcemage advocate'/'JUD', 'Darrell Riche').
card_number('forcemage advocate'/'JUD', '116').
card_flavor_text('forcemage advocate'/'JUD', '\"Our unity renews our strength.\"').
card_multiverse_id('forcemage advocate'/'JUD', '34244').

card_in_set('funeral pyre', 'JUD').
card_original_type('funeral pyre'/'JUD', 'Instant').
card_original_text('funeral pyre'/'JUD', 'Remove target card in a graveyard from the game. Its owner puts a 1/1 white Spirit creature token with flying into play.').
card_first_print('funeral pyre', 'JUD').
card_image_name('funeral pyre'/'JUD', 'funeral pyre').
card_uid('funeral pyre'/'JUD', 'JUD:Funeral Pyre:funeral pyre').
card_rarity('funeral pyre'/'JUD', 'Common').
card_artist('funeral pyre'/'JUD', 'Carl Critchlow').
card_number('funeral pyre'/'JUD', '10').
card_flavor_text('funeral pyre'/'JUD', '\"Raze the body. Raise the soul.\"\n—Mystic elder').
card_multiverse_id('funeral pyre'/'JUD', '34889').

card_in_set('genesis', 'JUD').
card_original_type('genesis'/'JUD', 'Creature — Incarnation').
card_original_text('genesis'/'JUD', 'At the beginning of your upkeep, if Genesis is in your graveyard, you may pay {2}{G}. If you do, return target creature card from your graveyard to your hand.').
card_image_name('genesis'/'JUD', 'genesis').
card_uid('genesis'/'JUD', 'JUD:Genesis:genesis').
card_rarity('genesis'/'JUD', 'Rare').
card_artist('genesis'/'JUD', 'Mark Zug').
card_number('genesis'/'JUD', '117').
card_flavor_text('genesis'/'JUD', '\"First through the Riftstone was Genesis—and the world was lifeless no more.\"\n—Scroll of Beginnings').
card_multiverse_id('genesis'/'JUD', '34833').

card_in_set('giant warthog', 'JUD').
card_original_type('giant warthog'/'JUD', 'Creature — Beast').
card_original_text('giant warthog'/'JUD', 'Trample').
card_first_print('giant warthog', 'JUD').
card_image_name('giant warthog'/'JUD', 'giant warthog').
card_uid('giant warthog'/'JUD', 'JUD:Giant Warthog:giant warthog').
card_rarity('giant warthog'/'JUD', 'Common').
card_artist('giant warthog'/'JUD', 'Kev Walker').
card_number('giant warthog'/'JUD', '118').
card_flavor_text('giant warthog'/'JUD', '\"When the Ancestor saw the cruelty her human children were capable of, Her fury shook the world. From this outburst sprang the warthogs.\"\n—Nomad myth').
card_multiverse_id('giant warthog'/'JUD', '36112').

card_in_set('glory', 'JUD').
card_original_type('glory'/'JUD', 'Creature — Incarnation').
card_original_text('glory'/'JUD', 'Flying\n{2}{W}: Creatures you control gain protection from the color of your choice until end of turn. Play this ability only if Glory is in your graveyard.').
card_image_name('glory'/'JUD', 'glory').
card_uid('glory'/'JUD', 'JUD:Glory:glory').
card_rarity('glory'/'JUD', 'Rare').
card_artist('glory'/'JUD', 'Donato Giancola').
card_number('glory'/'JUD', '11').
card_flavor_text('glory'/'JUD', '\"Glory was gone; Glory was everywhere.\"\n—Scroll of Beginnings').
card_multiverse_id('glory'/'JUD', '34822').

card_in_set('golden wish', 'JUD').
card_original_type('golden wish'/'JUD', 'Sorcery').
card_original_text('golden wish'/'JUD', 'Choose an artifact or enchantment card you own from outside the game, reveal that card, and put it into your hand. Remove Golden Wish from the game.').
card_first_print('golden wish', 'JUD').
card_image_name('golden wish'/'JUD', 'golden wish').
card_uid('golden wish'/'JUD', 'JUD:Golden Wish:golden wish').
card_rarity('golden wish'/'JUD', 'Rare').
card_artist('golden wish'/'JUD', 'Alan Pollack').
card_number('golden wish'/'JUD', '12').
card_flavor_text('golden wish'/'JUD', 'She wished for nobility, but not for a nation to honor it.').
card_multiverse_id('golden wish'/'JUD', '34399').

card_in_set('goretusk firebeast', 'JUD').
card_original_type('goretusk firebeast'/'JUD', 'Creature — Beast').
card_original_text('goretusk firebeast'/'JUD', 'When Goretusk Firebeast comes into play, it deals 4 damage to target player.').
card_first_print('goretusk firebeast', 'JUD').
card_image_name('goretusk firebeast'/'JUD', 'goretusk firebeast').
card_uid('goretusk firebeast'/'JUD', 'JUD:Goretusk Firebeast:goretusk firebeast').
card_rarity('goretusk firebeast'/'JUD', 'Common').
card_artist('goretusk firebeast'/'JUD', 'Keith Garletts').
card_number('goretusk firebeast'/'JUD', '91').
card_flavor_text('goretusk firebeast'/'JUD', 'As a youth, Kamahl took one for a pet. His hair has yet to grow back.').
card_multiverse_id('goretusk firebeast'/'JUD', '34917').

card_in_set('grave consequences', 'JUD').
card_original_type('grave consequences'/'JUD', 'Instant').
card_original_text('grave consequences'/'JUD', 'Each player may remove any number of cards in his or her graveyard from the game. Then each player loses 1 life for each card in his or her graveyard.\nDraw a card.').
card_first_print('grave consequences', 'JUD').
card_image_name('grave consequences'/'JUD', 'grave consequences').
card_uid('grave consequences'/'JUD', 'JUD:Grave Consequences:grave consequences').
card_rarity('grave consequences'/'JUD', 'Uncommon').
card_artist('grave consequences'/'JUD', 'Tim Hildebrandt').
card_number('grave consequences'/'JUD', '67').
card_flavor_text('grave consequences'/'JUD', '\"You call that a nightmare?\"\n—Braids, dementia summoner').
card_multiverse_id('grave consequences'/'JUD', '35161').

card_in_set('grip of amnesia', 'JUD').
card_original_type('grip of amnesia'/'JUD', 'Instant').
card_original_text('grip of amnesia'/'JUD', 'Counter target spell unless its controller removes his or her graveyard from the game.\nDraw a card.').
card_first_print('grip of amnesia', 'JUD').
card_image_name('grip of amnesia'/'JUD', 'grip of amnesia').
card_uid('grip of amnesia'/'JUD', 'JUD:Grip of Amnesia:grip of amnesia').
card_rarity('grip of amnesia'/'JUD', 'Common').
card_artist('grip of amnesia'/'JUD', 'Bradley Williams').
card_number('grip of amnesia'/'JUD', '41').
card_flavor_text('grip of amnesia'/'JUD', '\"Would you abandon your past to save your future?\"\n—Ambassador Laquatus').
card_multiverse_id('grip of amnesia'/'JUD', '34808').

card_in_set('grizzly fate', 'JUD').
card_original_type('grizzly fate'/'JUD', 'Sorcery').
card_original_text('grizzly fate'/'JUD', 'Put two 2/2 green Bear creature tokens into play.\nThreshold Instead put four 2/2 green Bear creature tokens into play.\nFlashback {5}{G}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('grizzly fate', 'JUD').
card_image_name('grizzly fate'/'JUD', 'grizzly fate').
card_uid('grizzly fate'/'JUD', 'JUD:Grizzly Fate:grizzly fate').
card_rarity('grizzly fate'/'JUD', 'Uncommon').
card_artist('grizzly fate'/'JUD', 'Dave Dorman').
card_number('grizzly fate'/'JUD', '119').
card_multiverse_id('grizzly fate'/'JUD', '34209').

card_in_set('guided strike', 'JUD').
card_original_type('guided strike'/'JUD', 'Instant').
card_original_text('guided strike'/'JUD', 'Target creature gets +1/+0 and gains first strike until end of turn.\nDraw a card.').
card_image_name('guided strike'/'JUD', 'guided strike').
card_uid('guided strike'/'JUD', 'JUD:Guided Strike:guided strike').
card_rarity('guided strike'/'JUD', 'Common').
card_artist('guided strike'/'JUD', 'Dave Dorman').
card_number('guided strike'/'JUD', '13').
card_flavor_text('guided strike'/'JUD', '\"May the Ancestor strengthen my hand and guide my blade.\"\n—Nomad war prayer').
card_multiverse_id('guided strike'/'JUD', '36431').

card_in_set('guiltfeeder', 'JUD').
card_original_type('guiltfeeder'/'JUD', 'Creature — Horror').
card_original_text('guiltfeeder'/'JUD', 'Guiltfeeder can\'t be blocked except by artifact creatures and/or black creatures.\nWhenever Guiltfeeder attacks and isn\'t blocked, defending player loses 1 life for each card in his or her graveyard.').
card_first_print('guiltfeeder', 'JUD').
card_image_name('guiltfeeder'/'JUD', 'guiltfeeder').
card_uid('guiltfeeder'/'JUD', 'JUD:Guiltfeeder:guiltfeeder').
card_rarity('guiltfeeder'/'JUD', 'Rare').
card_artist('guiltfeeder'/'JUD', 'Mark Tedin').
card_number('guiltfeeder'/'JUD', '68').
card_multiverse_id('guiltfeeder'/'JUD', '29890').

card_in_set('hapless researcher', 'JUD').
card_original_type('hapless researcher'/'JUD', 'Creature — Wizard').
card_original_text('hapless researcher'/'JUD', 'Sacrifice Hapless Researcher: Draw a card, then discard a card from your hand.').
card_first_print('hapless researcher', 'JUD').
card_image_name('hapless researcher'/'JUD', 'hapless researcher').
card_uid('hapless researcher'/'JUD', 'JUD:Hapless Researcher:hapless researcher').
card_rarity('hapless researcher'/'JUD', 'Common').
card_artist('hapless researcher'/'JUD', 'Ron Spears').
card_number('hapless researcher'/'JUD', '42').
card_flavor_text('hapless researcher'/'JUD', 'Scholars possess such lofty knowledge that it shouldn\'t be surprising when they fall.').
card_multiverse_id('hapless researcher'/'JUD', '36115').

card_in_set('harvester druid', 'JUD').
card_original_type('harvester druid'/'JUD', 'Creature — Druid').
card_original_text('harvester druid'/'JUD', '{T}: Add to your mana pool one mana of any color that a land you control could produce.').
card_first_print('harvester druid', 'JUD').
card_image_name('harvester druid'/'JUD', 'harvester druid').
card_uid('harvester druid'/'JUD', 'JUD:Harvester Druid:harvester druid').
card_rarity('harvester druid'/'JUD', 'Common').
card_artist('harvester druid'/'JUD', 'David Martin').
card_number('harvester druid'/'JUD', '120').
card_flavor_text('harvester druid'/'JUD', '\"In the end, the same soil lies atop the field, within the mountain, and under the sea.\"').
card_multiverse_id('harvester druid'/'JUD', '36411').

card_in_set('hunting grounds', 'JUD').
card_original_type('hunting grounds'/'JUD', 'Enchantment').
card_original_text('hunting grounds'/'JUD', 'Threshold Whenever an opponent plays a spell, you may put a creature card from your hand into play. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('hunting grounds', 'JUD').
card_image_name('hunting grounds'/'JUD', 'hunting grounds').
card_uid('hunting grounds'/'JUD', 'JUD:Hunting Grounds:hunting grounds').
card_rarity('hunting grounds'/'JUD', 'Rare').
card_artist('hunting grounds'/'JUD', 'Mark Brill').
card_number('hunting grounds'/'JUD', '138').
card_flavor_text('hunting grounds'/'JUD', 'In the heavy hush of Krosa, drawing on mana is like ringing a dinner bell.').
card_multiverse_id('hunting grounds'/'JUD', '35167').

card_in_set('infectious rage', 'JUD').
card_original_type('infectious rage'/'JUD', 'Enchant Creature').
card_original_text('infectious rage'/'JUD', 'Enchanted creature gets +2/-1.\nWhen enchanted creature is put into a graveyard, choose a creature at random Infectious Rage can enchant. Return Infectious Rage to play enchanting that creature.').
card_first_print('infectious rage', 'JUD').
card_image_name('infectious rage'/'JUD', 'infectious rage').
card_uid('infectious rage'/'JUD', 'JUD:Infectious Rage:infectious rage').
card_rarity('infectious rage'/'JUD', 'Uncommon').
card_artist('infectious rage'/'JUD', 'Christopher Moeller').
card_number('infectious rage'/'JUD', '92').
card_multiverse_id('infectious rage'/'JUD', '35087').

card_in_set('ironshell beetle', 'JUD').
card_original_type('ironshell beetle'/'JUD', 'Creature — Insect').
card_original_text('ironshell beetle'/'JUD', 'When Ironshell Beetle comes into play, put a +1/+1 counter on target creature.').
card_first_print('ironshell beetle', 'JUD').
card_image_name('ironshell beetle'/'JUD', 'ironshell beetle').
card_uid('ironshell beetle'/'JUD', 'JUD:Ironshell Beetle:ironshell beetle').
card_rarity('ironshell beetle'/'JUD', 'Common').
card_artist('ironshell beetle'/'JUD', 'Heather Hudson').
card_number('ironshell beetle'/'JUD', '121').
card_flavor_text('ironshell beetle'/'JUD', '\"Why waste time creating weapons? Nature provides us with all we need.\"\n—Centaur warrior').
card_multiverse_id('ironshell beetle'/'JUD', '34758').

card_in_set('jeska, warrior adept', 'JUD').
card_original_type('jeska, warrior adept'/'JUD', 'Creature — Barbarian Legend').
card_original_text('jeska, warrior adept'/'JUD', 'First strike, haste\n{T}: Jeska, Warrior Adept deals 1 damage to target creature or player.').
card_first_print('jeska, warrior adept', 'JUD').
card_image_name('jeska, warrior adept'/'JUD', 'jeska, warrior adept').
card_uid('jeska, warrior adept'/'JUD', 'JUD:Jeska, Warrior Adept:jeska, warrior adept').
card_rarity('jeska, warrior adept'/'JUD', 'Rare').
card_artist('jeska, warrior adept'/'JUD', 'rk post').
card_number('jeska, warrior adept'/'JUD', '93').
card_flavor_text('jeska, warrior adept'/'JUD', '\"My brother and I both come from Balthor\'s forge. Kamahl has a temper of fire. I have a temper of steel.\"').
card_multiverse_id('jeska, warrior adept'/'JUD', '36451').

card_in_set('keep watch', 'JUD').
card_original_type('keep watch'/'JUD', 'Instant').
card_original_text('keep watch'/'JUD', 'Draw a card for each attacking creature.').
card_first_print('keep watch', 'JUD').
card_image_name('keep watch'/'JUD', 'keep watch').
card_uid('keep watch'/'JUD', 'JUD:Keep Watch:keep watch').
card_rarity('keep watch'/'JUD', 'Common').
card_artist('keep watch'/'JUD', 'Fred Rahmqvist').
card_number('keep watch'/'JUD', '43').
card_flavor_text('keep watch'/'JUD', '\"I see their moral dilemmas. I see their raw courage. I see their self-sacrifice. I see our victory.\"').
card_multiverse_id('keep watch'/'JUD', '35168').

card_in_set('krosan reclamation', 'JUD').
card_original_type('krosan reclamation'/'JUD', 'Instant').
card_original_text('krosan reclamation'/'JUD', 'Target player shuffles up to two target cards from his or her graveyard into his or her library.\nFlashback {1}{G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('krosan reclamation', 'JUD').
card_image_name('krosan reclamation'/'JUD', 'krosan reclamation').
card_uid('krosan reclamation'/'JUD', 'JUD:Krosan Reclamation:krosan reclamation').
card_rarity('krosan reclamation'/'JUD', 'Uncommon').
card_artist('krosan reclamation'/'JUD', 'Gary Ruddell').
card_number('krosan reclamation'/'JUD', '122').
card_multiverse_id('krosan reclamation'/'JUD', '36113').

card_in_set('krosan verge', 'JUD').
card_original_type('krosan verge'/'JUD', 'Land').
card_original_text('krosan verge'/'JUD', 'Krosan Verge comes into play tapped.\n{T}: Add one colorless mana to your mana pool.\n{2}, {T}, Sacrifice Krosan Verge: Search your library for a forest card and a plains card and put them into play tapped. Then shuffle your library.').
card_first_print('krosan verge', 'JUD').
card_image_name('krosan verge'/'JUD', 'krosan verge').
card_uid('krosan verge'/'JUD', 'JUD:Krosan Verge:krosan verge').
card_rarity('krosan verge'/'JUD', 'Uncommon').
card_artist('krosan verge'/'JUD', 'Tony Szczudlo').
card_number('krosan verge'/'JUD', '141').
card_multiverse_id('krosan verge'/'JUD', '30657').

card_in_set('krosan wayfarer', 'JUD').
card_original_type('krosan wayfarer'/'JUD', 'Creature — Druid').
card_original_text('krosan wayfarer'/'JUD', 'Sacrifice Krosan Wayfarer: Put a land card from your hand into play.').
card_first_print('krosan wayfarer', 'JUD').
card_image_name('krosan wayfarer'/'JUD', 'krosan wayfarer').
card_uid('krosan wayfarer'/'JUD', 'JUD:Krosan Wayfarer:krosan wayfarer').
card_rarity('krosan wayfarer'/'JUD', 'Common').
card_artist('krosan wayfarer'/'JUD', 'Edward P. Beard, Jr.').
card_number('krosan wayfarer'/'JUD', '123').
card_flavor_text('krosan wayfarer'/'JUD', '\"The Krosan Forest lives, and druids are its seeds.\"\n—Seton, centaur druid').
card_multiverse_id('krosan wayfarer'/'JUD', '35069').

card_in_set('laquatus\'s disdain', 'JUD').
card_original_type('laquatus\'s disdain'/'JUD', 'Instant').
card_original_text('laquatus\'s disdain'/'JUD', 'Counter target spell played from a graveyard.\nDraw a card.').
card_first_print('laquatus\'s disdain', 'JUD').
card_image_name('laquatus\'s disdain'/'JUD', 'laquatus\'s disdain').
card_uid('laquatus\'s disdain'/'JUD', 'JUD:Laquatus\'s Disdain:laquatus\'s disdain').
card_rarity('laquatus\'s disdain'/'JUD', 'Uncommon').
card_artist('laquatus\'s disdain'/'JUD', 'Pete Venters').
card_number('laquatus\'s disdain'/'JUD', '44').
card_flavor_text('laquatus\'s disdain'/'JUD', '\"The first time was amusing, but now you\'re getting tiresome.\"').
card_multiverse_id('laquatus\'s disdain'/'JUD', '27145').

card_in_set('lava dart', 'JUD').
card_original_type('lava dart'/'JUD', 'Instant').
card_original_text('lava dart'/'JUD', 'Lava Dart deals 1 damage to target creature or player.\nFlashback—Sacrifice a mountain. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('lava dart', 'JUD').
card_image_name('lava dart'/'JUD', 'lava dart').
card_uid('lava dart'/'JUD', 'JUD:Lava Dart:lava dart').
card_rarity('lava dart'/'JUD', 'Common').
card_artist('lava dart'/'JUD', 'Darrell Riche').
card_number('lava dart'/'JUD', '94').
card_multiverse_id('lava dart'/'JUD', '29766').

card_in_set('lead astray', 'JUD').
card_original_type('lead astray'/'JUD', 'Instant').
card_original_text('lead astray'/'JUD', 'Tap up to two target creatures.').
card_first_print('lead astray', 'JUD').
card_image_name('lead astray'/'JUD', 'lead astray').
card_uid('lead astray'/'JUD', 'JUD:Lead Astray:lead astray').
card_rarity('lead astray'/'JUD', 'Common').
card_artist('lead astray'/'JUD', 'Adam Rex').
card_number('lead astray'/'JUD', '14').
card_flavor_text('lead astray'/'JUD', '\"Never underestimate our enemy\'s strength, brutality, . . . or stupidity.\"\n—Commander Eesha').
card_multiverse_id('lead astray'/'JUD', '34768').

card_in_set('liberated dwarf', 'JUD').
card_original_type('liberated dwarf'/'JUD', 'Creature — Dwarf').
card_original_text('liberated dwarf'/'JUD', '{R}, Sacrifice Liberated Dwarf: Target green creature gets +1/+0 and gains first strike until end of turn.').
card_first_print('liberated dwarf', 'JUD').
card_image_name('liberated dwarf'/'JUD', 'liberated dwarf').
card_uid('liberated dwarf'/'JUD', 'JUD:Liberated Dwarf:liberated dwarf').
card_rarity('liberated dwarf'/'JUD', 'Common').
card_artist('liberated dwarf'/'JUD', 'Greg Hildebrandt').
card_number('liberated dwarf'/'JUD', '95').
card_flavor_text('liberated dwarf'/'JUD', 'Freed from their chains, these dwarves are bound only by honor.').
card_multiverse_id('liberated dwarf'/'JUD', '35083').

card_in_set('lightning surge', 'JUD').
card_original_type('lightning surge'/'JUD', 'Sorcery').
card_original_text('lightning surge'/'JUD', 'Lightning Surge deals 4 damage to target creature or player.\nThreshold Instead Lightning Surge deals 6 damage to that creature or player and the damage can\'t be prevented.\nFlashback {5}{R}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('lightning surge', 'JUD').
card_image_name('lightning surge'/'JUD', 'lightning surge').
card_uid('lightning surge'/'JUD', 'JUD:Lightning Surge:lightning surge').
card_rarity('lightning surge'/'JUD', 'Rare').
card_artist('lightning surge'/'JUD', 'Ron Spears').
card_number('lightning surge'/'JUD', '96').
card_multiverse_id('lightning surge'/'JUD', '34208').

card_in_set('living wish', 'JUD').
card_original_type('living wish'/'JUD', 'Sorcery').
card_original_text('living wish'/'JUD', 'Choose a creature or land card you own from outside the game, reveal that card, and put it into your hand. Remove Living Wish from the game.').
card_image_name('living wish'/'JUD', 'living wish').
card_uid('living wish'/'JUD', 'JUD:Living Wish:living wish').
card_rarity('living wish'/'JUD', 'Rare').
card_artist('living wish'/'JUD', 'Eric Peterson').
card_number('living wish'/'JUD', '124').
card_flavor_text('living wish'/'JUD', 'He wished for growth, but not for a way to control it.').
card_multiverse_id('living wish'/'JUD', '34405').

card_in_set('lost in thought', 'JUD').
card_original_type('lost in thought'/'JUD', 'Enchant Creature').
card_original_text('lost in thought'/'JUD', 'Enchanted creature can\'t attack or block and its activated abilities can\'t be played. Its controller may remove three cards in his or her graveyard from the game to ignore this ability until end of turn.').
card_first_print('lost in thought', 'JUD').
card_image_name('lost in thought'/'JUD', 'lost in thought').
card_uid('lost in thought'/'JUD', 'JUD:Lost in Thought:lost in thought').
card_rarity('lost in thought'/'JUD', 'Common').
card_artist('lost in thought'/'JUD', 'Ben Thompson').
card_number('lost in thought'/'JUD', '45').
card_multiverse_id('lost in thought'/'JUD', '34775').

card_in_set('masked gorgon', 'JUD').
card_original_type('masked gorgon'/'JUD', 'Creature — Gorgon').
card_original_text('masked gorgon'/'JUD', 'Green creatures and white creatures have protection from Gorgons.\nThreshold Masked Gorgon has protection from green and from white. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('masked gorgon', 'JUD').
card_image_name('masked gorgon'/'JUD', 'masked gorgon').
card_uid('masked gorgon'/'JUD', 'JUD:Masked Gorgon:masked gorgon').
card_rarity('masked gorgon'/'JUD', 'Rare').
card_artist('masked gorgon'/'JUD', 'Matthew D. Wilson').
card_number('masked gorgon'/'JUD', '69').
card_multiverse_id('masked gorgon'/'JUD', '34480').

card_in_set('mental note', 'JUD').
card_original_type('mental note'/'JUD', 'Instant').
card_original_text('mental note'/'JUD', 'Put the top two cards of your library into your graveyard.\nDraw a card.').
card_first_print('mental note', 'JUD').
card_image_name('mental note'/'JUD', 'mental note').
card_uid('mental note'/'JUD', 'JUD:Mental Note:mental note').
card_rarity('mental note'/'JUD', 'Common').
card_artist('mental note'/'JUD', 'Bradley Williams').
card_number('mental note'/'JUD', '46').
card_flavor_text('mental note'/'JUD', 'Some minds are more open than others.').
card_multiverse_id('mental note'/'JUD', '36114').

card_in_set('mirari\'s wake', 'JUD').
card_original_type('mirari\'s wake'/'JUD', 'Enchantment').
card_original_text('mirari\'s wake'/'JUD', 'Creatures you control get +1/+1.\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.').
card_first_print('mirari\'s wake', 'JUD').
card_image_name('mirari\'s wake'/'JUD', 'mirari\'s wake').
card_uid('mirari\'s wake'/'JUD', 'JUD:Mirari\'s Wake:mirari\'s wake').
card_rarity('mirari\'s wake'/'JUD', 'Rare').
card_artist('mirari\'s wake'/'JUD', 'David Martin').
card_number('mirari\'s wake'/'JUD', '139').
card_flavor_text('mirari\'s wake'/'JUD', 'The land drank power from the Mirari as though it had thirsted forever.').
card_multiverse_id('mirari\'s wake'/'JUD', '35057').

card_in_set('mirror wall', 'JUD').
card_original_type('mirror wall'/'JUD', 'Creature — Wall').
card_original_text('mirror wall'/'JUD', '(Walls can\'t attack.)\n{W}: Mirror Wall may attack this turn as though it weren\'t a Wall.').
card_first_print('mirror wall', 'JUD').
card_image_name('mirror wall'/'JUD', 'mirror wall').
card_uid('mirror wall'/'JUD', 'JUD:Mirror Wall:mirror wall').
card_rarity('mirror wall'/'JUD', 'Common').
card_artist('mirror wall'/'JUD', 'Mark Brill').
card_number('mirror wall'/'JUD', '47').
card_flavor_text('mirror wall'/'JUD', '\"Breaking it would be about four hundred years bad luck.\"\n—Nomad sentry').
card_multiverse_id('mirror wall'/'JUD', '36413').

card_in_set('mist of stagnation', 'JUD').
card_original_type('mist of stagnation'/'JUD', 'Enchantment').
card_original_text('mist of stagnation'/'JUD', 'Permanents don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player untaps a permanent for each card in his or her graveyard.').
card_first_print('mist of stagnation', 'JUD').
card_image_name('mist of stagnation'/'JUD', 'mist of stagnation').
card_uid('mist of stagnation'/'JUD', 'JUD:Mist of Stagnation:mist of stagnation').
card_rarity('mist of stagnation'/'JUD', 'Rare').
card_artist('mist of stagnation'/'JUD', 'Mike Ploog').
card_number('mist of stagnation'/'JUD', '48').
card_multiverse_id('mist of stagnation'/'JUD', '34777').

card_in_set('morality shift', 'JUD').
card_original_type('morality shift'/'JUD', 'Sorcery').
card_original_text('morality shift'/'JUD', 'Exchange your graveyard and library. Then shuffle your library.').
card_first_print('morality shift', 'JUD').
card_image_name('morality shift'/'JUD', 'morality shift').
card_uid('morality shift'/'JUD', 'JUD:Morality Shift:morality shift').
card_rarity('morality shift'/'JUD', 'Rare').
card_artist('morality shift'/'JUD', 'Jerry Tiritilli').
card_number('morality shift'/'JUD', '70').
card_flavor_text('morality shift'/'JUD', 'The mind sings, though at times it sings a dirge.').
card_multiverse_id('morality shift'/'JUD', '35171').

card_in_set('nantuko monastery', 'JUD').
card_original_type('nantuko monastery'/'JUD', 'Land').
card_original_text('nantuko monastery'/'JUD', '{T}: Add one colorless mana to your mana pool.\nThreshold {G}{W}: Nantuko Monastery becomes a 4/4 green and white creature with first strike until end of turn. It\'s still a land. (Play this ability only if seven or more cards are in your graveyard.)').
card_first_print('nantuko monastery', 'JUD').
card_image_name('nantuko monastery'/'JUD', 'nantuko monastery').
card_uid('nantuko monastery'/'JUD', 'JUD:Nantuko Monastery:nantuko monastery').
card_rarity('nantuko monastery'/'JUD', 'Uncommon').
card_artist('nantuko monastery'/'JUD', 'Rob Alexander').
card_number('nantuko monastery'/'JUD', '142').
card_multiverse_id('nantuko monastery'/'JUD', '34401').

card_in_set('nantuko tracer', 'JUD').
card_original_type('nantuko tracer'/'JUD', 'Creature — Insect Druid').
card_original_text('nantuko tracer'/'JUD', 'When Nantuko Tracer comes into play, you may put target card from a graveyard on the bottom of its owner\'s library.').
card_first_print('nantuko tracer', 'JUD').
card_image_name('nantuko tracer'/'JUD', 'nantuko tracer').
card_uid('nantuko tracer'/'JUD', 'JUD:Nantuko Tracer:nantuko tracer').
card_rarity('nantuko tracer'/'JUD', 'Common').
card_artist('nantuko tracer'/'JUD', 'Greg Staples').
card_number('nantuko tracer'/'JUD', '125').
card_flavor_text('nantuko tracer'/'JUD', 'Your past is a map to where you will go.\n—Nantuko teaching').
card_multiverse_id('nantuko tracer'/'JUD', '36526').

card_in_set('nomad mythmaker', 'JUD').
card_original_type('nomad mythmaker'/'JUD', 'Creature — Cleric').
card_original_text('nomad mythmaker'/'JUD', '{W}, {T}: Put target enchant creature card from a graveyard into play enchanting a creature you control. (You control that enchantment.)').
card_first_print('nomad mythmaker', 'JUD').
card_image_name('nomad mythmaker'/'JUD', 'nomad mythmaker').
card_uid('nomad mythmaker'/'JUD', 'JUD:Nomad Mythmaker:nomad mythmaker').
card_rarity('nomad mythmaker'/'JUD', 'Rare').
card_artist('nomad mythmaker'/'JUD', 'Eric Peterson').
card_number('nomad mythmaker'/'JUD', '15').
card_flavor_text('nomad mythmaker'/'JUD', 'Nomads weave tales thicker than tapestries.').
card_multiverse_id('nomad mythmaker'/'JUD', '34241').

card_in_set('nullmage advocate', 'JUD').
card_original_type('nullmage advocate'/'JUD', 'Creature — Insect Druid').
card_original_text('nullmage advocate'/'JUD', '{T}: Return two target cards in an opponent\'s graveyard to his or her hand. Destroy target artifact or enchantment.').
card_first_print('nullmage advocate', 'JUD').
card_image_name('nullmage advocate'/'JUD', 'nullmage advocate').
card_uid('nullmage advocate'/'JUD', 'JUD:Nullmage Advocate:nullmage advocate').
card_rarity('nullmage advocate'/'JUD', 'Common').
card_artist('nullmage advocate'/'JUD', 'Darrell Riche').
card_number('nullmage advocate'/'JUD', '126').
card_flavor_text('nullmage advocate'/'JUD', '\"Our unity unmasks your deceit.\"').
card_multiverse_id('nullmage advocate'/'JUD', '34245').

card_in_set('phantom centaur', 'JUD').
card_original_type('phantom centaur'/'JUD', 'Creature — Centaur Spirit').
card_original_text('phantom centaur'/'JUD', 'Protection from black\nPhantom Centaur comes into play with three +1/+1 counters on it.\nIf damage would be dealt to Phantom Centaur, prevent that damage. Remove a +1/+1 counter from Phantom Centaur.').
card_first_print('phantom centaur', 'JUD').
card_image_name('phantom centaur'/'JUD', 'phantom centaur').
card_uid('phantom centaur'/'JUD', 'JUD:Phantom Centaur:phantom centaur').
card_rarity('phantom centaur'/'JUD', 'Uncommon').
card_artist('phantom centaur'/'JUD', 'Carl Critchlow').
card_number('phantom centaur'/'JUD', '127').
card_multiverse_id('phantom centaur'/'JUD', '35073').

card_in_set('phantom flock', 'JUD').
card_original_type('phantom flock'/'JUD', 'Creature — Bird Soldier Spirit').
card_original_text('phantom flock'/'JUD', 'Flying\nPhantom Flock comes into play with three +1/+1 counters on it.\nIf damage would be dealt to Phantom Flock, prevent that damage. Remove a +1/+1 counter from Phantom Flock.').
card_first_print('phantom flock', 'JUD').
card_image_name('phantom flock'/'JUD', 'phantom flock').
card_uid('phantom flock'/'JUD', 'JUD:Phantom Flock:phantom flock').
card_rarity('phantom flock'/'JUD', 'Uncommon').
card_artist('phantom flock'/'JUD', 'David Martin').
card_number('phantom flock'/'JUD', '16').
card_multiverse_id('phantom flock'/'JUD', '34932').

card_in_set('phantom nantuko', 'JUD').
card_original_type('phantom nantuko'/'JUD', 'Creature — Insect Spirit').
card_original_text('phantom nantuko'/'JUD', 'Trample\nPhantom Nantuko comes into play with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Nantuko, prevent that damage. Remove a +1/+1 counter from Phantom Nantuko.\n{T}: Put a +1/+1 counter on Phantom Nantuko.').
card_first_print('phantom nantuko', 'JUD').
card_image_name('phantom nantuko'/'JUD', 'phantom nantuko').
card_uid('phantom nantuko'/'JUD', 'JUD:Phantom Nantuko:phantom nantuko').
card_rarity('phantom nantuko'/'JUD', 'Rare').
card_artist('phantom nantuko'/'JUD', 'Wayne England').
card_number('phantom nantuko'/'JUD', '128').
card_multiverse_id('phantom nantuko'/'JUD', '34948').

card_in_set('phantom nishoba', 'JUD').
card_original_type('phantom nishoba'/'JUD', 'Creature — Beast Spirit').
card_original_text('phantom nishoba'/'JUD', 'Trample\nPhantom Nishoba comes into play with seven +1/+1 counters on it.\nWhenever Phantom Nishoba deals damage, you gain that much life.\nIf damage would be dealt to Phantom Nishoba, prevent that damage. Remove a +1/+1 counter from Phantom Nishoba.').
card_first_print('phantom nishoba', 'JUD').
card_image_name('phantom nishoba'/'JUD', 'phantom nishoba').
card_uid('phantom nishoba'/'JUD', 'JUD:Phantom Nishoba:phantom nishoba').
card_rarity('phantom nishoba'/'JUD', 'Rare').
card_artist('phantom nishoba'/'JUD', 'Arnie Swekel').
card_number('phantom nishoba'/'JUD', '140').
card_multiverse_id('phantom nishoba'/'JUD', '37113').

card_in_set('phantom nomad', 'JUD').
card_original_type('phantom nomad'/'JUD', 'Creature — Nomad Spirit').
card_original_text('phantom nomad'/'JUD', 'Phantom Nomad comes into play with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Nomad, prevent that damage. Remove a +1/+1 counter from Phantom Nomad.').
card_first_print('phantom nomad', 'JUD').
card_image_name('phantom nomad'/'JUD', 'phantom nomad').
card_uid('phantom nomad'/'JUD', 'JUD:Phantom Nomad:phantom nomad').
card_rarity('phantom nomad'/'JUD', 'Common').
card_artist('phantom nomad'/'JUD', 'Jim Nelson').
card_number('phantom nomad'/'JUD', '17').
card_multiverse_id('phantom nomad'/'JUD', '34890').

card_in_set('phantom tiger', 'JUD').
card_original_type('phantom tiger'/'JUD', 'Creature — Cat Spirit').
card_original_text('phantom tiger'/'JUD', 'Phantom Tiger comes into play with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Tiger, prevent that damage. Remove a +1/+1 counter from Phantom Tiger.').
card_first_print('phantom tiger', 'JUD').
card_image_name('phantom tiger'/'JUD', 'phantom tiger').
card_uid('phantom tiger'/'JUD', 'JUD:Phantom Tiger:phantom tiger').
card_rarity('phantom tiger'/'JUD', 'Common').
card_artist('phantom tiger'/'JUD', 'Brian Snõddy').
card_number('phantom tiger'/'JUD', '129').
card_multiverse_id('phantom tiger'/'JUD', '34934').

card_in_set('planar chaos', 'JUD').
card_original_type('planar chaos'/'JUD', 'Enchantment').
card_original_text('planar chaos'/'JUD', 'At the beginning of your upkeep, flip a coin. If you lose the flip, sacrifice Planar Chaos.\nWhenever a player plays a spell, that player flips a coin. If he or she loses the flip, counter that spell.').
card_first_print('planar chaos', 'JUD').
card_image_name('planar chaos'/'JUD', 'planar chaos').
card_uid('planar chaos'/'JUD', 'JUD:Planar Chaos:planar chaos').
card_rarity('planar chaos'/'JUD', 'Uncommon').
card_artist('planar chaos'/'JUD', 'Ron Spencer').
card_number('planar chaos'/'JUD', '97').
card_multiverse_id('planar chaos'/'JUD', '35088').

card_in_set('prismatic strands', 'JUD').
card_original_type('prismatic strands'/'JUD', 'Instant').
card_original_text('prismatic strands'/'JUD', 'Prevent all damage that sources of the color of your choice would deal this turn.\nFlashback—Tap an untapped white creature you control. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('prismatic strands', 'JUD').
card_image_name('prismatic strands'/'JUD', 'prismatic strands').
card_uid('prismatic strands'/'JUD', 'JUD:Prismatic Strands:prismatic strands').
card_rarity('prismatic strands'/'JUD', 'Common').
card_artist('prismatic strands'/'JUD', 'Eric Peterson').
card_number('prismatic strands'/'JUD', '18').
card_multiverse_id('prismatic strands'/'JUD', '34233').

card_in_set('pulsemage advocate', 'JUD').
card_original_type('pulsemage advocate'/'JUD', 'Creature — Cleric').
card_original_text('pulsemage advocate'/'JUD', '{T}: Return three target cards in an opponent\'s graveyard to his or her hand.  Return target creature card from your graveyard to play.').
card_first_print('pulsemage advocate', 'JUD').
card_image_name('pulsemage advocate'/'JUD', 'pulsemage advocate').
card_uid('pulsemage advocate'/'JUD', 'JUD:Pulsemage Advocate:pulsemage advocate').
card_rarity('pulsemage advocate'/'JUD', 'Rare').
card_artist('pulsemage advocate'/'JUD', 'Jeff Easley').
card_number('pulsemage advocate'/'JUD', '19').
card_flavor_text('pulsemage advocate'/'JUD', '\"Our unity revives our hopes.\"').
card_multiverse_id('pulsemage advocate'/'JUD', '35575').

card_in_set('quiet speculation', 'JUD').
card_original_type('quiet speculation'/'JUD', 'Sorcery').
card_original_text('quiet speculation'/'JUD', 'Search target player\'s library for up to three cards with flashback and put them into that player\'s graveyard. Then the player shuffles his or her library.').
card_first_print('quiet speculation', 'JUD').
card_image_name('quiet speculation'/'JUD', 'quiet speculation').
card_uid('quiet speculation'/'JUD', 'JUD:Quiet Speculation:quiet speculation').
card_rarity('quiet speculation'/'JUD', 'Uncommon').
card_artist('quiet speculation'/'JUD', 'John Avon').
card_number('quiet speculation'/'JUD', '49').
card_flavor_text('quiet speculation'/'JUD', '\"The best foresight is hindsight.\"\n—Empress Llawan').
card_multiverse_id('quiet speculation'/'JUD', '20431').

card_in_set('rats\' feast', 'JUD').
card_original_type('rats\' feast'/'JUD', 'Sorcery').
card_original_text('rats\' feast'/'JUD', 'Remove X target cards in a single graveyard from the game.').
card_first_print('rats\' feast', 'JUD').
card_image_name('rats\' feast'/'JUD', 'rats\' feast').
card_uid('rats\' feast'/'JUD', 'JUD:Rats\' Feast:rats\' feast').
card_rarity('rats\' feast'/'JUD', 'Common').
card_artist('rats\' feast'/'JUD', 'Bob Petillo').
card_number('rats\' feast'/'JUD', '71').
card_flavor_text('rats\' feast'/'JUD', '\"That does it—I quit\"\n—Cabal grave robber').
card_multiverse_id('rats\' feast'/'JUD', '36412').

card_in_set('ray of revelation', 'JUD').
card_original_type('ray of revelation'/'JUD', 'Instant').
card_original_text('ray of revelation'/'JUD', 'Destroy target enchantment.\nFlashback {G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('ray of revelation', 'JUD').
card_image_name('ray of revelation'/'JUD', 'ray of revelation').
card_uid('ray of revelation'/'JUD', 'JUD:Ray of Revelation:ray of revelation').
card_rarity('ray of revelation'/'JUD', 'Common').
card_artist('ray of revelation'/'JUD', 'Doug Chaffee').
card_number('ray of revelation'/'JUD', '20').
card_flavor_text('ray of revelation'/'JUD', 'Webs of illusion unravel in the light of truth.').
card_multiverse_id('ray of revelation'/'JUD', '34199').

card_in_set('riftstone portal', 'JUD').
card_original_type('riftstone portal'/'JUD', 'Land').
card_original_text('riftstone portal'/'JUD', '{T}: Add one colorless mana to your mana pool.\nAs long as Riftstone Portal is in your graveyard, lands you control have \"{T}: Add {G} or {W} to your mana pool.\"').
card_first_print('riftstone portal', 'JUD').
card_image_name('riftstone portal'/'JUD', 'riftstone portal').
card_uid('riftstone portal'/'JUD', 'JUD:Riftstone Portal:riftstone portal').
card_rarity('riftstone portal'/'JUD', 'Uncommon').
card_artist('riftstone portal'/'JUD', 'Don Hazeltine').
card_number('riftstone portal'/'JUD', '143').
card_multiverse_id('riftstone portal'/'JUD', '34398').

card_in_set('scalpelexis', 'JUD').
card_original_type('scalpelexis'/'JUD', 'Creature — Beast').
card_original_text('scalpelexis'/'JUD', 'Flying\nWhenever Scalpelexis deals combat damage to a player, that player removes the top four cards of his or her library from the game. If two or more of those cards have the same name, repeat this process.').
card_first_print('scalpelexis', 'JUD').
card_image_name('scalpelexis'/'JUD', 'scalpelexis').
card_uid('scalpelexis'/'JUD', 'JUD:Scalpelexis:scalpelexis').
card_rarity('scalpelexis'/'JUD', 'Rare').
card_artist('scalpelexis'/'JUD', 'Mark Tedin').
card_number('scalpelexis'/'JUD', '50').
card_multiverse_id('scalpelexis'/'JUD', '29928').

card_in_set('seedtime', 'JUD').
card_original_type('seedtime'/'JUD', 'Instant').
card_original_text('seedtime'/'JUD', 'Play Seedtime only during your turn.\nTake an extra turn after this one if an opponent played a blue spell this turn.').
card_first_print('seedtime', 'JUD').
card_image_name('seedtime'/'JUD', 'seedtime').
card_uid('seedtime'/'JUD', 'JUD:Seedtime:seedtime').
card_rarity('seedtime'/'JUD', 'Rare').
card_artist('seedtime'/'JUD', 'Rebecca Guay').
card_number('seedtime'/'JUD', '130').
card_flavor_text('seedtime'/'JUD', 'The hippo grows wings to fight the condor.\n—Nantuko teaching').
card_multiverse_id('seedtime'/'JUD', '34205').

card_in_set('selfless exorcist', 'JUD').
card_original_type('selfless exorcist'/'JUD', 'Creature — Cleric').
card_original_text('selfless exorcist'/'JUD', '{T}: Remove target creature card in a graveyard from the game. That card deals damage equal to its power to Selfless Exorcist. (A * on a card not in play is 0.)').
card_first_print('selfless exorcist', 'JUD').
card_image_name('selfless exorcist'/'JUD', 'selfless exorcist').
card_uid('selfless exorcist'/'JUD', 'JUD:Selfless Exorcist:selfless exorcist').
card_rarity('selfless exorcist'/'JUD', 'Rare').
card_artist('selfless exorcist'/'JUD', 'Christopher Moeller').
card_number('selfless exorcist'/'JUD', '21').
card_multiverse_id('selfless exorcist'/'JUD', '34891').

card_in_set('serene sunset', 'JUD').
card_original_type('serene sunset'/'JUD', 'Instant').
card_original_text('serene sunset'/'JUD', 'Prevent all combat damage X target creatures would deal this turn.').
card_first_print('serene sunset', 'JUD').
card_image_name('serene sunset'/'JUD', 'serene sunset').
card_uid('serene sunset'/'JUD', 'JUD:Serene Sunset:serene sunset').
card_rarity('serene sunset'/'JUD', 'Uncommon').
card_artist('serene sunset'/'JUD', 'David Martin').
card_number('serene sunset'/'JUD', '131').
card_flavor_text('serene sunset'/'JUD', 'The armies paused, silent, as the warriors reflected on the sunsets of their youth.').
card_multiverse_id('serene sunset'/'JUD', '36454').

card_in_set('shaman\'s trance', 'JUD').
card_original_type('shaman\'s trance'/'JUD', 'Instant').
card_original_text('shaman\'s trance'/'JUD', 'Until end of turn, other players can\'t play cards from their graveyards, and you may play cards from other players\' graveyards as though they were in your graveyard.').
card_first_print('shaman\'s trance', 'JUD').
card_image_name('shaman\'s trance'/'JUD', 'shaman\'s trance').
card_uid('shaman\'s trance'/'JUD', 'JUD:Shaman\'s Trance:shaman\'s trance').
card_rarity('shaman\'s trance'/'JUD', 'Rare').
card_artist('shaman\'s trance'/'JUD', 'Greg Hildebrandt').
card_number('shaman\'s trance'/'JUD', '98').
card_multiverse_id('shaman\'s trance'/'JUD', '34795').

card_in_set('shieldmage advocate', 'JUD').
card_original_type('shieldmage advocate'/'JUD', 'Creature — Cleric').
card_original_text('shieldmage advocate'/'JUD', '{T}: Return target card in an opponent\'s graveyard to his or her hand. Prevent all damage that would be dealt to target creature or player this turn by a source of your choice.').
card_first_print('shieldmage advocate', 'JUD').
card_image_name('shieldmage advocate'/'JUD', 'shieldmage advocate').
card_uid('shieldmage advocate'/'JUD', 'JUD:Shieldmage Advocate:shieldmage advocate').
card_rarity('shieldmage advocate'/'JUD', 'Common').
card_artist('shieldmage advocate'/'JUD', 'Christopher Moeller').
card_number('shieldmage advocate'/'JUD', '22').
card_flavor_text('shieldmage advocate'/'JUD', '\"Our unity conquers all fears.\"').
card_multiverse_id('shieldmage advocate'/'JUD', '35067').

card_in_set('silver seraph', 'JUD').
card_original_type('silver seraph'/'JUD', 'Creature — Angel').
card_original_text('silver seraph'/'JUD', 'Flying\nThreshold Other creatures you control get +2/+2. (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('silver seraph', 'JUD').
card_image_name('silver seraph'/'JUD', 'silver seraph').
card_uid('silver seraph'/'JUD', 'JUD:Silver Seraph:silver seraph').
card_rarity('silver seraph'/'JUD', 'Rare').
card_artist('silver seraph'/'JUD', 'Matthew D. Wilson').
card_number('silver seraph'/'JUD', '23').
card_flavor_text('silver seraph'/'JUD', 'A symbol of hope for hopeless times.').
card_multiverse_id('silver seraph'/'JUD', '36036').

card_in_set('solitary confinement', 'JUD').
card_original_type('solitary confinement'/'JUD', 'Enchantment').
card_original_text('solitary confinement'/'JUD', 'At the beginning of your upkeep, sacrifice Solitary Confinement unless you discard a card from your hand.\nSkip your draw step.\nYou can\'t be the target of spells or abilities.\nPrevent all damage that would be dealt to you.').
card_first_print('solitary confinement', 'JUD').
card_image_name('solitary confinement'/'JUD', 'solitary confinement').
card_uid('solitary confinement'/'JUD', 'JUD:Solitary Confinement:solitary confinement').
card_rarity('solitary confinement'/'JUD', 'Rare').
card_artist('solitary confinement'/'JUD', 'Scott M. Fischer').
card_number('solitary confinement'/'JUD', '24').
card_multiverse_id('solitary confinement'/'JUD', '34769').

card_in_set('soulcatchers\' aerie', 'JUD').
card_original_type('soulcatchers\' aerie'/'JUD', 'Enchantment').
card_original_text('soulcatchers\' aerie'/'JUD', 'Whenever a Bird is put into your graveyard from play, put a feather counter on Soulcatchers\' Aerie.\nAll Birds get +1/+1 for each feather counter on Soulcatchers\' Aerie.').
card_first_print('soulcatchers\' aerie', 'JUD').
card_image_name('soulcatchers\' aerie'/'JUD', 'soulcatchers\' aerie').
card_uid('soulcatchers\' aerie'/'JUD', 'JUD:Soulcatchers\' Aerie:soulcatchers\' aerie').
card_rarity('soulcatchers\' aerie'/'JUD', 'Uncommon').
card_artist('soulcatchers\' aerie'/'JUD', 'Rob Alexander').
card_number('soulcatchers\' aerie'/'JUD', '25').
card_multiverse_id('soulcatchers\' aerie'/'JUD', '35095').

card_in_set('soulgorger orgg', 'JUD').
card_original_type('soulgorger orgg'/'JUD', 'Creature — Nightmare Orgg').
card_original_text('soulgorger orgg'/'JUD', 'Trample\nWhen Soulgorger Orgg comes into play, you lose all but 1 life.\nWhen Soulgorger Orgg leaves play, you gain life equal to the life you lost when it came into play.').
card_first_print('soulgorger orgg', 'JUD').
card_image_name('soulgorger orgg'/'JUD', 'soulgorger orgg').
card_uid('soulgorger orgg'/'JUD', 'JUD:Soulgorger Orgg:soulgorger orgg').
card_rarity('soulgorger orgg'/'JUD', 'Uncommon').
card_artist('soulgorger orgg'/'JUD', 'John Matson').
card_number('soulgorger orgg'/'JUD', '99').
card_multiverse_id('soulgorger orgg'/'JUD', '34942').

card_in_set('spellgorger barbarian', 'JUD').
card_original_type('spellgorger barbarian'/'JUD', 'Creature — Nightmare Barbarian').
card_original_text('spellgorger barbarian'/'JUD', 'When Spellgorger Barbarian comes into play, discard a card at random from your hand.\nWhen Spellgorger Barbarian leaves play, draw a card.').
card_first_print('spellgorger barbarian', 'JUD').
card_image_name('spellgorger barbarian'/'JUD', 'spellgorger barbarian').
card_uid('spellgorger barbarian'/'JUD', 'JUD:Spellgorger Barbarian:spellgorger barbarian').
card_rarity('spellgorger barbarian'/'JUD', 'Common').
card_artist('spellgorger barbarian'/'JUD', 'Mark Romanoski').
card_number('spellgorger barbarian'/'JUD', '100').
card_flavor_text('spellgorger barbarian'/'JUD', 'The epidemic insanity had a lasting effect on certain barbarians—but no one noticed.').
card_multiverse_id('spellgorger barbarian'/'JUD', '25805').

card_in_set('spelljack', 'JUD').
card_original_type('spelljack'/'JUD', 'Instant').
card_original_text('spelljack'/'JUD', 'Counter target spell. If it\'s countered this way, remove it from the game instead of putting it into its owner\'s graveyard. As long as it remains removed from the game, you may play it as though it were in your hand without paying its mana cost. If it has X in its mana cost, X is 0.').
card_first_print('spelljack', 'JUD').
card_image_name('spelljack'/'JUD', 'spelljack').
card_uid('spelljack'/'JUD', 'JUD:Spelljack:spelljack').
card_rarity('spelljack'/'JUD', 'Rare').
card_artist('spelljack'/'JUD', 'Pete Venters').
card_number('spelljack'/'JUD', '51').
card_multiverse_id('spelljack'/'JUD', '35169').

card_in_set('spirit cairn', 'JUD').
card_original_type('spirit cairn'/'JUD', 'Enchantment').
card_original_text('spirit cairn'/'JUD', 'Whenever a player discards a card from his or her hand, you may pay {W}. If you do, put a 1/1 white Spirit creature token with flying into play.').
card_first_print('spirit cairn', 'JUD').
card_image_name('spirit cairn'/'JUD', 'spirit cairn').
card_uid('spirit cairn'/'JUD', 'JUD:Spirit Cairn:spirit cairn').
card_rarity('spirit cairn'/'JUD', 'Uncommon').
card_artist('spirit cairn'/'JUD', 'Gary Ruddell').
card_number('spirit cairn'/'JUD', '26').
card_flavor_text('spirit cairn'/'JUD', 'It marks the border between here and hereafter.').
card_multiverse_id('spirit cairn'/'JUD', '34897').

card_in_set('spurnmage advocate', 'JUD').
card_original_type('spurnmage advocate'/'JUD', 'Creature — Nomad').
card_original_text('spurnmage advocate'/'JUD', '{T}: Return two target cards in an opponent\'s graveyard to his or her hand. Destroy target attacking creature.').
card_first_print('spurnmage advocate', 'JUD').
card_image_name('spurnmage advocate'/'JUD', 'spurnmage advocate').
card_uid('spurnmage advocate'/'JUD', 'JUD:Spurnmage Advocate:spurnmage advocate').
card_rarity('spurnmage advocate'/'JUD', 'Uncommon').
card_artist('spurnmage advocate'/'JUD', 'Ron Spears').
card_number('spurnmage advocate'/'JUD', '27').
card_flavor_text('spurnmage advocate'/'JUD', '\"Our unity humbles our foes.\"').
card_multiverse_id('spurnmage advocate'/'JUD', '34242').

card_in_set('stitch together', 'JUD').
card_original_type('stitch together'/'JUD', 'Sorcery').
card_original_text('stitch together'/'JUD', 'Return target creature card from your graveyard to your hand.\nThreshold Instead return that card from your graveyard to play. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('stitch together', 'JUD').
card_image_name('stitch together'/'JUD', 'stitch together').
card_uid('stitch together'/'JUD', 'JUD:Stitch Together:stitch together').
card_rarity('stitch together'/'JUD', 'Uncommon').
card_artist('stitch together'/'JUD', 'Arnie Swekel').
card_number('stitch together'/'JUD', '72').
card_multiverse_id('stitch together'/'JUD', '32209').

card_in_set('sudden strength', 'JUD').
card_original_type('sudden strength'/'JUD', 'Instant').
card_original_text('sudden strength'/'JUD', 'Target creature gets +3/+3 until end of turn.\nDraw a card.').
card_first_print('sudden strength', 'JUD').
card_image_name('sudden strength'/'JUD', 'sudden strength').
card_uid('sudden strength'/'JUD', 'JUD:Sudden Strength:sudden strength').
card_rarity('sudden strength'/'JUD', 'Common').
card_artist('sudden strength'/'JUD', 'Alan Pollack').
card_number('sudden strength'/'JUD', '132').
card_flavor_text('sudden strength'/'JUD', 'The Mirari\'s magic transformed the Krosan Forest—and its inhabitants.').
card_multiverse_id('sudden strength'/'JUD', '35114').

card_in_set('suntail hawk', 'JUD').
card_original_type('suntail hawk'/'JUD', 'Creature — Bird').
card_original_text('suntail hawk'/'JUD', 'Flying').
card_first_print('suntail hawk', 'JUD').
card_image_name('suntail hawk'/'JUD', 'suntail hawk').
card_uid('suntail hawk'/'JUD', 'JUD:Suntail Hawk:suntail hawk').
card_rarity('suntail hawk'/'JUD', 'Common').
card_artist('suntail hawk'/'JUD', 'Heather Hudson').
card_number('suntail hawk'/'JUD', '28').
card_flavor_text('suntail hawk'/'JUD', '\"Our forebears once flew like this—carefree, needing nothing but a warm nest and a full belly. Our intellect may be more burden than blessing.\"\n—Commander Eesha').
card_multiverse_id('suntail hawk'/'JUD', '34207').

card_in_set('sutured ghoul', 'JUD').
card_original_type('sutured ghoul'/'JUD', 'Creature — Zombie').
card_original_text('sutured ghoul'/'JUD', 'Trample \nAs Sutured Ghoul comes into play, remove any number of creature cards in your graveyard from the game.\nSutured Ghoul\'s power is equal to the total power of the removed cards and its toughness is equal to their total toughness. (A * on a card not in play is 0.)').
card_first_print('sutured ghoul', 'JUD').
card_image_name('sutured ghoul'/'JUD', 'sutured ghoul').
card_uid('sutured ghoul'/'JUD', 'JUD:Sutured Ghoul:sutured ghoul').
card_rarity('sutured ghoul'/'JUD', 'Rare').
card_artist('sutured ghoul'/'JUD', 'Carl Critchlow').
card_number('sutured ghoul'/'JUD', '73').
card_multiverse_id('sutured ghoul'/'JUD', '29944').

card_in_set('swelter', 'JUD').
card_original_type('swelter'/'JUD', 'Sorcery').
card_original_text('swelter'/'JUD', 'Swelter deals 2 damage to each of two target creatures.').
card_first_print('swelter', 'JUD').
card_image_name('swelter'/'JUD', 'swelter').
card_uid('swelter'/'JUD', 'JUD:Swelter:swelter').
card_rarity('swelter'/'JUD', 'Uncommon').
card_artist('swelter'/'JUD', 'Ben Thompson').
card_number('swelter'/'JUD', '101').
card_flavor_text('swelter'/'JUD', 'No circle has a beginning, but many have foreseeable ends.').
card_multiverse_id('swelter'/'JUD', '36119').

card_in_set('swirling sandstorm', 'JUD').
card_original_type('swirling sandstorm'/'JUD', 'Sorcery').
card_original_text('swirling sandstorm'/'JUD', 'Threshold Swirling Sandstorm deals 5 damage to each creature without flying. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('swirling sandstorm', 'JUD').
card_image_name('swirling sandstorm'/'JUD', 'swirling sandstorm').
card_uid('swirling sandstorm'/'JUD', 'JUD:Swirling Sandstorm:swirling sandstorm').
card_rarity('swirling sandstorm'/'JUD', 'Common').
card_artist('swirling sandstorm'/'JUD', 'Tony Szczudlo').
card_number('swirling sandstorm'/'JUD', '102').
card_flavor_text('swirling sandstorm'/'JUD', 'As dust swept out, vultures swept in.').
card_multiverse_id('swirling sandstorm'/'JUD', '29762').

card_in_set('sylvan safekeeper', 'JUD').
card_original_type('sylvan safekeeper'/'JUD', 'Creature — Wizard').
card_original_text('sylvan safekeeper'/'JUD', 'Sacrifice a land: Target creature you control can\'t be the target of spells or abilities this turn.').
card_first_print('sylvan safekeeper', 'JUD').
card_image_name('sylvan safekeeper'/'JUD', 'sylvan safekeeper').
card_uid('sylvan safekeeper'/'JUD', 'JUD:Sylvan Safekeeper:sylvan safekeeper').
card_rarity('sylvan safekeeper'/'JUD', 'Rare').
card_artist('sylvan safekeeper'/'JUD', 'Pete Venters').
card_number('sylvan safekeeper'/'JUD', '133').
card_flavor_text('sylvan safekeeper'/'JUD', '\"How could someone so small cause so much trouble?\"\n—Nomad sentry').
card_multiverse_id('sylvan safekeeper'/'JUD', '35062').

card_in_set('telekinetic bonds', 'JUD').
card_original_type('telekinetic bonds'/'JUD', 'Enchantment').
card_original_text('telekinetic bonds'/'JUD', 'Whenever a player discards a card from his or her hand, you may pay {1}{U}. If you do, tap or untap target permanent.').
card_first_print('telekinetic bonds', 'JUD').
card_image_name('telekinetic bonds'/'JUD', 'telekinetic bonds').
card_uid('telekinetic bonds'/'JUD', 'JUD:Telekinetic Bonds:telekinetic bonds').
card_rarity('telekinetic bonds'/'JUD', 'Rare').
card_artist('telekinetic bonds'/'JUD', 'Jim Nelson').
card_number('telekinetic bonds'/'JUD', '52').
card_flavor_text('telekinetic bonds'/'JUD', '\"Nature\'s been fired. The oceans are under new management.\"\n—Ambassador Laquatus').
card_multiverse_id('telekinetic bonds'/'JUD', '5589').

card_in_set('test of endurance', 'JUD').
card_original_type('test of endurance'/'JUD', 'Enchantment').
card_original_text('test of endurance'/'JUD', 'At the beginning of your upkeep, if you have 50 or more life, you win the game.').
card_first_print('test of endurance', 'JUD').
card_image_name('test of endurance'/'JUD', 'test of endurance').
card_uid('test of endurance'/'JUD', 'JUD:Test of Endurance:test of endurance').
card_rarity('test of endurance'/'JUD', 'Rare').
card_artist('test of endurance'/'JUD', 'Mike Ploog').
card_number('test of endurance'/'JUD', '29').
card_flavor_text('test of endurance'/'JUD', '\"If we have learned nothing else, we have learned to survive.\"\n—Mystic elder').
card_multiverse_id('test of endurance'/'JUD', '29924').

card_in_set('thriss, nantuko primus', 'JUD').
card_original_type('thriss, nantuko primus'/'JUD', 'Creature — Insect Druid Legend').
card_original_text('thriss, nantuko primus'/'JUD', '{G}, {T}: Target creature gets +5/+5 until end of turn.').
card_first_print('thriss, nantuko primus', 'JUD').
card_image_name('thriss, nantuko primus'/'JUD', 'thriss, nantuko primus').
card_uid('thriss, nantuko primus'/'JUD', 'JUD:Thriss, Nantuko Primus:thriss, nantuko primus').
card_rarity('thriss, nantuko primus'/'JUD', 'Rare').
card_artist('thriss, nantuko primus'/'JUD', 'John Avon').
card_number('thriss, nantuko primus'/'JUD', '134').
card_flavor_text('thriss, nantuko primus'/'JUD', '\"When you live for others, you live for yourself.\"').
card_multiverse_id('thriss, nantuko primus'/'JUD', '36525').

card_in_set('toxic stench', 'JUD').
card_original_type('toxic stench'/'JUD', 'Instant').
card_original_text('toxic stench'/'JUD', 'Target nonblack creature gets -1/-1 until end of turn.\nThreshold Instead destroy that creature. It can\'t be regenerated. (You have threshold if seven or more cards are in your graveyard.)').
card_first_print('toxic stench', 'JUD').
card_image_name('toxic stench'/'JUD', 'toxic stench').
card_uid('toxic stench'/'JUD', 'JUD:Toxic Stench:toxic stench').
card_rarity('toxic stench'/'JUD', 'Common').
card_artist('toxic stench'/'JUD', 'Bradley Williams').
card_number('toxic stench'/'JUD', '74').
card_multiverse_id('toxic stench'/'JUD', '36416').

card_in_set('trained pronghorn', 'JUD').
card_original_type('trained pronghorn'/'JUD', 'Creature — Antelope').
card_original_text('trained pronghorn'/'JUD', 'Discard a card from your hand: Prevent all damage that would be dealt to Trained Pronghorn this turn.').
card_first_print('trained pronghorn', 'JUD').
card_image_name('trained pronghorn'/'JUD', 'trained pronghorn').
card_uid('trained pronghorn'/'JUD', 'JUD:Trained Pronghorn:trained pronghorn').
card_rarity('trained pronghorn'/'JUD', 'Common').
card_artist('trained pronghorn'/'JUD', 'John Matson').
card_number('trained pronghorn'/'JUD', '30').
card_flavor_text('trained pronghorn'/'JUD', '\"The desert asked the Ancestor for children, and the antelope were born.\"\n—Nomad myth').
card_multiverse_id('trained pronghorn'/'JUD', '35068').

card_in_set('treacherous vampire', 'JUD').
card_original_type('treacherous vampire'/'JUD', 'Creature — Vampire').
card_original_text('treacherous vampire'/'JUD', 'Flying\nWhenever Treacherous Vampire attacks or blocks, sacrifice it unless you remove a card in your graveyard from the game.\nThreshold Treacherous Vampire gets +2/+2 and has \"When Treacherous Vampire is put into a graveyard from play, you lose 6 life.\"').
card_first_print('treacherous vampire', 'JUD').
card_image_name('treacherous vampire'/'JUD', 'treacherous vampire').
card_uid('treacherous vampire'/'JUD', 'JUD:Treacherous Vampire:treacherous vampire').
card_rarity('treacherous vampire'/'JUD', 'Uncommon').
card_artist('treacherous vampire'/'JUD', 'Kev Walker').
card_number('treacherous vampire'/'JUD', '75').
card_multiverse_id('treacherous vampire'/'JUD', '34788').

card_in_set('treacherous werewolf', 'JUD').
card_original_type('treacherous werewolf'/'JUD', 'Creature — Minion Wolf').
card_original_text('treacherous werewolf'/'JUD', 'Threshold Treacherous Werewolf gets +2/+2 and has \"When Treacherous Werewolf is put into a graveyard from play, you lose 4 life.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('treacherous werewolf', 'JUD').
card_image_name('treacherous werewolf'/'JUD', 'treacherous werewolf').
card_uid('treacherous werewolf'/'JUD', 'JUD:Treacherous Werewolf:treacherous werewolf').
card_rarity('treacherous werewolf'/'JUD', 'Common').
card_artist('treacherous werewolf'/'JUD', 'Mark Tedin').
card_number('treacherous werewolf'/'JUD', '76').
card_multiverse_id('treacherous werewolf'/'JUD', '36037').

card_in_set('tunneler wurm', 'JUD').
card_original_type('tunneler wurm'/'JUD', 'Creature — Wurm').
card_original_text('tunneler wurm'/'JUD', 'Discard a card from your hand: Regenerate Tunneler Wurm.').
card_first_print('tunneler wurm', 'JUD').
card_image_name('tunneler wurm'/'JUD', 'tunneler wurm').
card_uid('tunneler wurm'/'JUD', 'JUD:Tunneler Wurm:tunneler wurm').
card_rarity('tunneler wurm'/'JUD', 'Uncommon').
card_artist('tunneler wurm'/'JUD', 'Jeff Easley').
card_number('tunneler wurm'/'JUD', '135').
card_flavor_text('tunneler wurm'/'JUD', 'If an anurid\'s lucky, it\'ll find a warm tunnel for bedtime. If a wurm\'s lucky, it\'ll find a warm anurid for breakfast.').
card_multiverse_id('tunneler wurm'/'JUD', '26370').

card_in_set('unquestioned authority', 'JUD').
card_original_type('unquestioned authority'/'JUD', 'Enchant Creature').
card_original_text('unquestioned authority'/'JUD', 'When Unquestioned Authority comes into play, draw a card.\nEnchanted creature has protection from creatures.').
card_first_print('unquestioned authority', 'JUD').
card_image_name('unquestioned authority'/'JUD', 'unquestioned authority').
card_uid('unquestioned authority'/'JUD', 'JUD:Unquestioned Authority:unquestioned authority').
card_rarity('unquestioned authority'/'JUD', 'Uncommon').
card_artist('unquestioned authority'/'JUD', 'Alex Horley-Orlandelli').
card_number('unquestioned authority'/'JUD', '31').
card_flavor_text('unquestioned authority'/'JUD', '\"Only the Ancestor should be revered.\"\n—Mystic heretic').
card_multiverse_id('unquestioned authority'/'JUD', '24541').

card_in_set('valor', 'JUD').
card_original_type('valor'/'JUD', 'Creature — Incarnation').
card_original_text('valor'/'JUD', 'First strike\nAs long as Valor is in your graveyard and you control a plains, creatures you control have first strike.').
card_first_print('valor', 'JUD').
card_image_name('valor'/'JUD', 'valor').
card_uid('valor'/'JUD', 'JUD:Valor:valor').
card_rarity('valor'/'JUD', 'Uncommon').
card_artist('valor'/'JUD', 'Kev Walker').
card_number('valor'/'JUD', '32').
card_flavor_text('valor'/'JUD', '\"The cracks widened, the shell crumbled, and Valor spread throughout the land.\"\n—Scroll of Beginnings').
card_multiverse_id('valor'/'JUD', '33719').

card_in_set('venomous vines', 'JUD').
card_original_type('venomous vines'/'JUD', 'Sorcery').
card_original_text('venomous vines'/'JUD', 'Destroy target enchanted permanent.').
card_first_print('venomous vines', 'JUD').
card_image_name('venomous vines'/'JUD', 'venomous vines').
card_uid('venomous vines'/'JUD', 'JUD:Venomous Vines:venomous vines').
card_rarity('venomous vines'/'JUD', 'Common').
card_artist('venomous vines'/'JUD', 'Ron Spencer').
card_number('venomous vines'/'JUD', '136').
card_flavor_text('venomous vines'/'JUD', '\"True strength comes from within. The false promise of unnatural power only incurs Krosa\'s wrath.\"\n—Seton, centaur druid').
card_multiverse_id('venomous vines'/'JUD', '36409').

card_in_set('vigilant sentry', 'JUD').
card_original_type('vigilant sentry'/'JUD', 'Creature — Nomad').
card_original_text('vigilant sentry'/'JUD', 'Threshold Vigilant Sentry gets +1/+1 and has \"{T}: Target attacking or blocking creature gets +3/+3 until end of turn.\" (You have threshold as long as seven or more cards are in your graveyard.)').
card_first_print('vigilant sentry', 'JUD').
card_image_name('vigilant sentry'/'JUD', 'vigilant sentry').
card_uid('vigilant sentry'/'JUD', 'JUD:Vigilant Sentry:vigilant sentry').
card_rarity('vigilant sentry'/'JUD', 'Common').
card_artist('vigilant sentry'/'JUD', 'Eric Peterson').
card_number('vigilant sentry'/'JUD', '33').
card_multiverse_id('vigilant sentry'/'JUD', '36035').

card_in_set('web of inertia', 'JUD').
card_original_type('web of inertia'/'JUD', 'Enchantment').
card_original_text('web of inertia'/'JUD', 'At the beginning of each opponent\'s combat phase, that player may remove a card in his or her graveyard from the game. If the player doesn\'t, creatures he or she controls can\'t attack you this turn.').
card_first_print('web of inertia', 'JUD').
card_image_name('web of inertia'/'JUD', 'web of inertia').
card_uid('web of inertia'/'JUD', 'JUD:Web of Inertia:web of inertia').
card_rarity('web of inertia'/'JUD', 'Uncommon').
card_artist('web of inertia'/'JUD', 'Don Hazeltine').
card_number('web of inertia'/'JUD', '53').
card_flavor_text('web of inertia'/'JUD', 'Cephalids specialize in lose-lose situations.').
card_multiverse_id('web of inertia'/'JUD', '34823').

card_in_set('wonder', 'JUD').
card_original_type('wonder'/'JUD', 'Creature — Incarnation').
card_original_text('wonder'/'JUD', 'Flying\nAs long as Wonder is in your graveyard and you control an island, creatures you control have flying.').
card_image_name('wonder'/'JUD', 'wonder').
card_uid('wonder'/'JUD', 'JUD:Wonder:wonder').
card_rarity('wonder'/'JUD', 'Uncommon').
card_artist('wonder'/'JUD', 'Rebecca Guay').
card_number('wonder'/'JUD', '54').
card_flavor_text('wonder'/'JUD', '\"The awestruck birds gazed at Wonder. Slowly, timidly, they rose into the air.\"\n—Scroll of Beginnings').
card_multiverse_id('wonder'/'JUD', '33720').

card_in_set('worldgorger dragon', 'JUD').
card_original_type('worldgorger dragon'/'JUD', 'Creature — Nightmare Dragon').
card_original_text('worldgorger dragon'/'JUD', 'Flying, trample\nWhen Worldgorger Dragon comes into play, remove all other permanents you control from the game.\nWhen Worldgorger Dragon leaves play, return the removed cards to play under their owners\' control.').
card_first_print('worldgorger dragon', 'JUD').
card_image_name('worldgorger dragon'/'JUD', 'worldgorger dragon').
card_uid('worldgorger dragon'/'JUD', 'JUD:Worldgorger Dragon:worldgorger dragon').
card_rarity('worldgorger dragon'/'JUD', 'Rare').
card_artist('worldgorger dragon'/'JUD', 'Wayne England').
card_number('worldgorger dragon'/'JUD', '103').
card_multiverse_id('worldgorger dragon'/'JUD', '35056').

card_in_set('wormfang behemoth', 'JUD').
card_original_type('wormfang behemoth'/'JUD', 'Creature — Nightmare Beast').
card_original_text('wormfang behemoth'/'JUD', 'When Wormfang Behemoth comes into play, remove all cards in your hand from the game.\nWhen Wormfang Behemoth leaves play, return the removed cards to their owner\'s hand.').
card_first_print('wormfang behemoth', 'JUD').
card_image_name('wormfang behemoth'/'JUD', 'wormfang behemoth').
card_uid('wormfang behemoth'/'JUD', 'JUD:Wormfang Behemoth:wormfang behemoth').
card_rarity('wormfang behemoth'/'JUD', 'Rare').
card_artist('wormfang behemoth'/'JUD', 'Heather Hudson').
card_number('wormfang behemoth'/'JUD', '55').
card_multiverse_id('wormfang behemoth'/'JUD', '34811').

card_in_set('wormfang crab', 'JUD').
card_original_type('wormfang crab'/'JUD', 'Creature — Nightmare Crab').
card_original_text('wormfang crab'/'JUD', 'Wormfang Crab is unblockable.\nWhen Wormfang Crab comes into play, an opponent chooses a permanent you control and removes it from the game.\nWhen Wormfang Crab leaves play, return the removed card to play under its owner\'s control.').
card_first_print('wormfang crab', 'JUD').
card_image_name('wormfang crab'/'JUD', 'wormfang crab').
card_uid('wormfang crab'/'JUD', 'JUD:Wormfang Crab:wormfang crab').
card_rarity('wormfang crab'/'JUD', 'Uncommon').
card_artist('wormfang crab'/'JUD', 'Glen Angus').
card_number('wormfang crab'/'JUD', '56').
card_multiverse_id('wormfang crab'/'JUD', '34927').

card_in_set('wormfang drake', 'JUD').
card_original_type('wormfang drake'/'JUD', 'Creature — Nightmare Drake').
card_original_text('wormfang drake'/'JUD', 'Flying\nWhen Wormfang Drake comes into play, sacrifice it unless you remove a creature you control other than Wormfang Drake from the game.\nWhen Wormfang Drake leaves play, return the removed card to play under its owner\'s control.').
card_first_print('wormfang drake', 'JUD').
card_image_name('wormfang drake'/'JUD', 'wormfang drake').
card_uid('wormfang drake'/'JUD', 'JUD:Wormfang Drake:wormfang drake').
card_rarity('wormfang drake'/'JUD', 'Common').
card_artist('wormfang drake'/'JUD', 'Thomas M. Baxa').
card_number('wormfang drake'/'JUD', '57').
card_multiverse_id('wormfang drake'/'JUD', '25974').

card_in_set('wormfang manta', 'JUD').
card_original_type('wormfang manta'/'JUD', 'Creature — Nightmare Beast').
card_original_text('wormfang manta'/'JUD', 'Flying\nWhen Wormfang Manta comes into play, you skip your next turn.\nWhen Wormfang Manta leaves play, you take an extra turn after this one.').
card_first_print('wormfang manta', 'JUD').
card_image_name('wormfang manta'/'JUD', 'wormfang manta').
card_uid('wormfang manta'/'JUD', 'JUD:Wormfang Manta:wormfang manta').
card_rarity('wormfang manta'/'JUD', 'Rare').
card_artist('wormfang manta'/'JUD', 'Heather Hudson').
card_number('wormfang manta'/'JUD', '58').
card_multiverse_id('wormfang manta'/'JUD', '34939').

card_in_set('wormfang newt', 'JUD').
card_original_type('wormfang newt'/'JUD', 'Creature — Nightmare Beast').
card_original_text('wormfang newt'/'JUD', 'When Wormfang Newt comes into play, remove a land you control from the game.\nWhen Wormfang Newt leaves play, return the removed card to play under its owner\'s control.').
card_first_print('wormfang newt', 'JUD').
card_image_name('wormfang newt'/'JUD', 'wormfang newt').
card_uid('wormfang newt'/'JUD', 'JUD:Wormfang Newt:wormfang newt').
card_rarity('wormfang newt'/'JUD', 'Common').
card_artist('wormfang newt'/'JUD', 'Doug Chaffee').
card_number('wormfang newt'/'JUD', '59').
card_flavor_text('wormfang newt'/'JUD', 'Spawned by mages emulating the insane.').
card_multiverse_id('wormfang newt'/'JUD', '34809').

card_in_set('wormfang turtle', 'JUD').
card_original_type('wormfang turtle'/'JUD', 'Creature — Nightmare Beast').
card_original_text('wormfang turtle'/'JUD', 'When Wormfang Turtle comes into play, remove a land you control from the game.\nWhen Wormfang Turtle leaves play, return the removed card to play under its owner\'s control.').
card_first_print('wormfang turtle', 'JUD').
card_image_name('wormfang turtle'/'JUD', 'wormfang turtle').
card_uid('wormfang turtle'/'JUD', 'JUD:Wormfang Turtle:wormfang turtle').
card_rarity('wormfang turtle'/'JUD', 'Uncommon').
card_artist('wormfang turtle'/'JUD', 'John Avon').
card_number('wormfang turtle'/'JUD', '60').
card_flavor_text('wormfang turtle'/'JUD', 'Chainer\'s admirers keep his dreams alive.').
card_multiverse_id('wormfang turtle'/'JUD', '34954').
