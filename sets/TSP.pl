% Time Spiral

set('TSP').
set_name('TSP', 'Time Spiral').
set_release_date('TSP', '2006-10-06').
set_border('TSP', 'black').
set_type('TSP', 'expansion').
set_block('TSP', 'Time Spiral').

card_in_set('academy ruins', 'TSP').
card_original_type('academy ruins'/'TSP', 'Legendary Land').
card_original_text('academy ruins'/'TSP', '{T}: Add {1} to your mana pool.\n{1}{U}, {T}: Put target artifact card in your graveyard on top of your library.').
card_first_print('academy ruins', 'TSP').
card_image_name('academy ruins'/'TSP', 'academy ruins').
card_uid('academy ruins'/'TSP', 'TSP:Academy Ruins:academy ruins').
card_rarity('academy ruins'/'TSP', 'Rare').
card_artist('academy ruins'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('academy ruins'/'TSP', '269').
card_flavor_text('academy ruins'/'TSP', 'Its secrets once wrought the greatest artifice ever known. Now crabs loot the rubble to decorate their shells.').
card_multiverse_id('academy ruins'/'TSP', '116725').

card_in_set('æther web', 'TSP').
card_original_type('æther web'/'TSP', 'Enchantment — Aura').
card_original_text('æther web'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature gets +1/+1, can block as though it had flying, and can block creatures with shadow as though they didn\'t have shadow.').
card_first_print('æther web', 'TSP').
card_image_name('æther web'/'TSP', 'aether web').
card_uid('æther web'/'TSP', 'TSP:Æther Web:aether web').
card_rarity('æther web'/'TSP', 'Common').
card_artist('æther web'/'TSP', 'Justin Sweet').
card_number('æther web'/'TSP', '189').
card_multiverse_id('æther web'/'TSP', '108852').

card_in_set('ætherflame wall', 'TSP').
card_original_type('ætherflame wall'/'TSP', 'Creature — Wall').
card_original_text('ætherflame wall'/'TSP', 'Defender\nÆtherflame Wall can block creatures with shadow as though they didn\'t have shadow.\n{R}: Ætherflame Wall gets +1/+0 until end of turn.').
card_first_print('ætherflame wall', 'TSP').
card_image_name('ætherflame wall'/'TSP', 'aetherflame wall').
card_uid('ætherflame wall'/'TSP', 'TSP:Ætherflame Wall:aetherflame wall').
card_rarity('ætherflame wall'/'TSP', 'Common').
card_artist('ætherflame wall'/'TSP', 'Justin Sweet').
card_number('ætherflame wall'/'TSP', '142').
card_multiverse_id('ætherflame wall'/'TSP', '113558').

card_in_set('amrou scout', 'TSP').
card_original_type('amrou scout'/'TSP', 'Creature — Kithkin Rebel Scout').
card_original_text('amrou scout'/'TSP', '{4}, {T}: Search your library for a Rebel card with converted mana cost 3 or less and put it into play. Then shuffle your library.').
card_first_print('amrou scout', 'TSP').
card_image_name('amrou scout'/'TSP', 'amrou scout').
card_uid('amrou scout'/'TSP', 'TSP:Amrou Scout:amrou scout').
card_rarity('amrou scout'/'TSP', 'Common').
card_artist('amrou scout'/'TSP', 'Quinton Hoover').
card_number('amrou scout'/'TSP', '1').
card_flavor_text('amrou scout'/'TSP', 'The people of Amrou were scattered by war and driven into hiding. Scouts maintain the beacon fires that signal \"return\" and \"home.\"').
card_multiverse_id('amrou scout'/'TSP', '113619').

card_in_set('amrou seekers', 'TSP').
card_original_type('amrou seekers'/'TSP', 'Creature — Kithkin Rebel').
card_original_text('amrou seekers'/'TSP', 'Amrou Seekers can\'t be blocked except by artifact creatures and/or white creatures.').
card_first_print('amrou seekers', 'TSP').
card_image_name('amrou seekers'/'TSP', 'amrou seekers').
card_uid('amrou seekers'/'TSP', 'TSP:Amrou Seekers:amrou seekers').
card_rarity('amrou seekers'/'TSP', 'Common').
card_artist('amrou seekers'/'TSP', 'Quinton Hoover').
card_number('amrou seekers'/'TSP', '2').
card_flavor_text('amrou seekers'/'TSP', 'The paradise of Amrou Haven had become a field of cinders. Its people struggled to survive, hoping to see visions of its past in the strange storms that quickly passed.').
card_multiverse_id('amrou seekers'/'TSP', '113509').

card_in_set('ancestral vision', 'TSP').
card_original_type('ancestral vision'/'TSP', 'Sorcery').
card_original_text('ancestral vision'/'TSP', 'Ancestral Vision is blue.\nSuspend 4—{U} (Rather than play this card from your hand, pay {U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)\nTarget player draws three cards.').
card_first_print('ancestral vision', 'TSP').
card_image_name('ancestral vision'/'TSP', 'ancestral vision').
card_uid('ancestral vision'/'TSP', 'TSP:Ancestral Vision:ancestral vision').
card_rarity('ancestral vision'/'TSP', 'Rare').
card_artist('ancestral vision'/'TSP', 'Mark Poole').
card_number('ancestral vision'/'TSP', '48').
card_multiverse_id('ancestral vision'/'TSP', '113505').

card_in_set('ancient grudge', 'TSP').
card_original_type('ancient grudge'/'TSP', 'Instant').
card_original_text('ancient grudge'/'TSP', 'Destroy target artifact.\nFlashback {G} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_image_name('ancient grudge'/'TSP', 'ancient grudge').
card_uid('ancient grudge'/'TSP', 'TSP:Ancient Grudge:ancient grudge').
card_rarity('ancient grudge'/'TSP', 'Common').
card_artist('ancient grudge'/'TSP', 'Jim Nelson').
card_number('ancient grudge'/'TSP', '143').
card_flavor_text('ancient grudge'/'TSP', 'The time rifts remind Yavimaya of the enemies of its past and provide fresh fuel for its fires.').
card_multiverse_id('ancient grudge'/'TSP', '109751').

card_in_set('angel\'s grace', 'TSP').
card_original_type('angel\'s grace'/'TSP', 'Instant').
card_original_text('angel\'s grace'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nYou can\'t lose the game this turn and your opponents can\'t win the game this turn. Until end of turn, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_first_print('angel\'s grace', 'TSP').
card_image_name('angel\'s grace'/'TSP', 'angel\'s grace').
card_uid('angel\'s grace'/'TSP', 'TSP:Angel\'s Grace:angel\'s grace').
card_rarity('angel\'s grace'/'TSP', 'Rare').
card_artist('angel\'s grace'/'TSP', 'Mark Zug').
card_number('angel\'s grace'/'TSP', '3').
card_multiverse_id('angel\'s grace'/'TSP', '110504').

card_in_set('ashcoat bear', 'TSP').
card_original_type('ashcoat bear'/'TSP', 'Creature — Bear').
card_original_text('ashcoat bear'/'TSP', 'Flash (You may play this spell any time you could play an instant.)').
card_first_print('ashcoat bear', 'TSP').
card_image_name('ashcoat bear'/'TSP', 'ashcoat bear').
card_uid('ashcoat bear'/'TSP', 'TSP:Ashcoat Bear:ashcoat bear').
card_rarity('ashcoat bear'/'TSP', 'Common').
card_artist('ashcoat bear'/'TSP', 'Carl Critchlow').
card_number('ashcoat bear'/'TSP', '190').
card_flavor_text('ashcoat bear'/'TSP', 'The bears wade into time rifts to feed. They do not fear the storms, and they are quick enough to snatch prey just as it blinks in or out of time.').
card_multiverse_id('ashcoat bear'/'TSP', '114905').

card_in_set('aspect of mongoose', 'TSP').
card_original_type('aspect of mongoose'/'TSP', 'Enchantment — Aura').
card_original_text('aspect of mongoose'/'TSP', 'Enchant creature\nEnchanted creature can\'t be the target of spells or abilities.\nWhen Aspect of Mongoose is put into a graveyard from play, return Aspect of Mongoose to its owner\'s hand.').
card_first_print('aspect of mongoose', 'TSP').
card_image_name('aspect of mongoose'/'TSP', 'aspect of mongoose').
card_uid('aspect of mongoose'/'TSP', 'TSP:Aspect of Mongoose:aspect of mongoose').
card_rarity('aspect of mongoose'/'TSP', 'Uncommon').
card_artist('aspect of mongoose'/'TSP', 'Dave Dorman').
card_number('aspect of mongoose'/'TSP', '191').
card_multiverse_id('aspect of mongoose'/'TSP', '116740').

card_in_set('assassinate', 'TSP').
card_original_type('assassinate'/'TSP', 'Sorcery').
card_original_text('assassinate'/'TSP', 'Destroy target tapped creature.').
card_first_print('assassinate', 'TSP').
card_image_name('assassinate'/'TSP', 'assassinate').
card_uid('assassinate'/'TSP', 'TSP:Assassinate:assassinate').
card_rarity('assassinate'/'TSP', 'Common').
card_artist('assassinate'/'TSP', 'Kev Walker').
card_number('assassinate'/'TSP', '95').
card_flavor_text('assassinate'/'TSP', 'The rulers of old Dominaria kept assassins on retainer. However, the true loyalty of these master killers was always to their peers. This elite brotherhood survived the fall of the old royal order.').
card_multiverse_id('assassinate'/'TSP', '114918').

card_in_set('assembly-worker', 'TSP').
card_original_type('assembly-worker'/'TSP', 'Artifact Creature — Assembly-Worker').
card_original_text('assembly-worker'/'TSP', '{T}: Target Assembly-Worker gets +1/+1 until end of turn.').
card_first_print('assembly-worker', 'TSP').
card_image_name('assembly-worker'/'TSP', 'assembly-worker').
card_uid('assembly-worker'/'TSP', 'TSP:Assembly-Worker:assembly-worker').
card_rarity('assembly-worker'/'TSP', 'Uncommon').
card_artist('assembly-worker'/'TSP', 'Chippy').
card_number('assembly-worker'/'TSP', '248').
card_flavor_text('assembly-worker'/'TSP', 'With their factories long destroyed, some of Mishra\'s creations still toil in remote areas, endlessly performing and reperforming their last orders.').
card_multiverse_id('assembly-worker'/'TSP', '114916').

card_in_set('barbed shocker', 'TSP').
card_original_type('barbed shocker'/'TSP', 'Creature — Insect').
card_original_text('barbed shocker'/'TSP', 'Trample, haste\nWhenever Barbed Shocker deals damage to a player, that player discards all the cards in his or her hand, then draws that many cards.').
card_first_print('barbed shocker', 'TSP').
card_image_name('barbed shocker'/'TSP', 'barbed shocker').
card_uid('barbed shocker'/'TSP', 'TSP:Barbed Shocker:barbed shocker').
card_rarity('barbed shocker'/'TSP', 'Uncommon').
card_artist('barbed shocker'/'TSP', 'Tony Szczudlo').
card_number('barbed shocker'/'TSP', '144').
card_flavor_text('barbed shocker'/'TSP', 'Fervid shamans willingly submit to shockers in hopes of glimpsing the fortunes of the future.').
card_multiverse_id('barbed shocker'/'TSP', '109762').

card_in_set('basal sliver', 'TSP').
card_original_type('basal sliver'/'TSP', 'Creature — Sliver').
card_original_text('basal sliver'/'TSP', 'All Slivers have \"Sacrifice this creature: Add {B}{B} to your mana pool.\"').
card_first_print('basal sliver', 'TSP').
card_image_name('basal sliver'/'TSP', 'basal sliver').
card_uid('basal sliver'/'TSP', 'TSP:Basal Sliver:basal sliver').
card_rarity('basal sliver'/'TSP', 'Common').
card_artist('basal sliver'/'TSP', 'Drew Tucker').
card_number('basal sliver'/'TSP', '96').
card_flavor_text('basal sliver'/'TSP', '\"Fascinating . . . These creatures display the paradox of tenacity and purposed self-destruction I have sought to breed into my thrulls.\"\n—Endrek Sahr, master breeder').
card_multiverse_id('basal sliver'/'TSP', '108792').

card_in_set('basalt gargoyle', 'TSP').
card_original_type('basalt gargoyle'/'TSP', 'Creature — Gargoyle').
card_original_text('basalt gargoyle'/'TSP', 'Flying\nEcho {2}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{R}: Basalt Gargoyle gets +0/+1 until end of turn.').
card_first_print('basalt gargoyle', 'TSP').
card_image_name('basalt gargoyle'/'TSP', 'basalt gargoyle').
card_uid('basalt gargoyle'/'TSP', 'TSP:Basalt Gargoyle:basalt gargoyle').
card_rarity('basalt gargoyle'/'TSP', 'Uncommon').
card_artist('basalt gargoyle'/'TSP', 'Clint Langley').
card_number('basalt gargoyle'/'TSP', '145').
card_multiverse_id('basalt gargoyle'/'TSP', '118883').

card_in_set('benalish cavalry', 'TSP').
card_original_type('benalish cavalry'/'TSP', 'Creature — Human Knight').
card_original_text('benalish cavalry'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('benalish cavalry', 'TSP').
card_image_name('benalish cavalry'/'TSP', 'benalish cavalry').
card_uid('benalish cavalry'/'TSP', 'TSP:Benalish Cavalry:benalish cavalry').
card_rarity('benalish cavalry'/'TSP', 'Common').
card_artist('benalish cavalry'/'TSP', 'Paolo Parente').
card_number('benalish cavalry'/'TSP', '4').
card_flavor_text('benalish cavalry'/'TSP', '\"My people swore to protect Benalia to the end. It is battered, but yet stands, as do we.\"').
card_multiverse_id('benalish cavalry'/'TSP', '122082').

card_in_set('bewilder', 'TSP').
card_original_type('bewilder'/'TSP', 'Instant').
card_original_text('bewilder'/'TSP', 'Target creature gets -3/-0 until end of turn.\nDraw a card.').
card_first_print('bewilder', 'TSP').
card_image_name('bewilder'/'TSP', 'bewilder').
card_uid('bewilder'/'TSP', 'TSP:Bewilder:bewilder').
card_rarity('bewilder'/'TSP', 'Common').
card_artist('bewilder'/'TSP', 'Ralph Horsley').
card_number('bewilder'/'TSP', '49').
card_flavor_text('bewilder'/'TSP', '\"Teferi snapped his fingers, and the viashino started clucking like a chicken. Silly, yes, but exactly the thing we needed to break the stifling tension.\"\n—Jhoira, master artificer').
card_multiverse_id('bewilder'/'TSP', '118901').

card_in_set('blazing blade askari', 'TSP').
card_original_type('blazing blade askari'/'TSP', 'Creature — Human Knight').
card_original_text('blazing blade askari'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{2}: Blazing Blade Askari becomes colorless until end of turn.').
card_first_print('blazing blade askari', 'TSP').
card_image_name('blazing blade askari'/'TSP', 'blazing blade askari').
card_uid('blazing blade askari'/'TSP', 'TSP:Blazing Blade Askari:blazing blade askari').
card_rarity('blazing blade askari'/'TSP', 'Common').
card_artist('blazing blade askari'/'TSP', 'Dan Frazier').
card_number('blazing blade askari'/'TSP', '146').
card_flavor_text('blazing blade askari'/'TSP', '\"This can\'t be Jamuraa. We could not have failed her this badly . . . .\"').
card_multiverse_id('blazing blade askari'/'TSP', '108799').

card_in_set('bogardan hellkite', 'TSP').
card_original_type('bogardan hellkite'/'TSP', 'Creature — Dragon').
card_original_text('bogardan hellkite'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nWhen Bogardan Hellkite comes into play, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_first_print('bogardan hellkite', 'TSP').
card_image_name('bogardan hellkite'/'TSP', 'bogardan hellkite').
card_uid('bogardan hellkite'/'TSP', 'TSP:Bogardan Hellkite:bogardan hellkite').
card_rarity('bogardan hellkite'/'TSP', 'Rare').
card_artist('bogardan hellkite'/'TSP', 'Scott M. Fischer').
card_number('bogardan hellkite'/'TSP', '147').
card_multiverse_id('bogardan hellkite'/'TSP', '111041').

card_in_set('bogardan rager', 'TSP').
card_original_type('bogardan rager'/'TSP', 'Creature — Elemental').
card_original_text('bogardan rager'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nWhen Bogardan Rager comes into play, target creature gets +4/+0 until end of turn.').
card_first_print('bogardan rager', 'TSP').
card_image_name('bogardan rager'/'TSP', 'bogardan rager').
card_uid('bogardan rager'/'TSP', 'TSP:Bogardan Rager:bogardan rager').
card_rarity('bogardan rager'/'TSP', 'Common').
card_artist('bogardan rager'/'TSP', 'Clint Langley').
card_number('bogardan rager'/'TSP', '148').
card_flavor_text('bogardan rager'/'TSP', 'In the erupting heart of Bogardan, it\'s hard to tell hurtling volcanic rocks from pouncing volcanic beasts.').
card_multiverse_id('bogardan rager'/'TSP', '108901').

card_in_set('bonesplitter sliver', 'TSP').
card_original_type('bonesplitter sliver'/'TSP', 'Creature — Sliver').
card_original_text('bonesplitter sliver'/'TSP', 'All Slivers get +2/+0.').
card_first_print('bonesplitter sliver', 'TSP').
card_image_name('bonesplitter sliver'/'TSP', 'bonesplitter sliver').
card_uid('bonesplitter sliver'/'TSP', 'TSP:Bonesplitter Sliver:bonesplitter sliver').
card_rarity('bonesplitter sliver'/'TSP', 'Common').
card_artist('bonesplitter sliver'/'TSP', 'Dany Orizio').
card_number('bonesplitter sliver'/'TSP', '149').
card_flavor_text('bonesplitter sliver'/'TSP', 'As the time streams grew more and more unstable, Dominaria\'s creatures struggled to adapt. The intense pressures led to many dead ends but also to lethal new forms that appeared as suddenly as the ashen rains.').
card_multiverse_id('bonesplitter sliver'/'TSP', '109697').

card_in_set('brass gnat', 'TSP').
card_original_type('brass gnat'/'TSP', 'Artifact Creature — Insect').
card_original_text('brass gnat'/'TSP', 'Flying\nBrass Gnat doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {1}. If you do, untap Brass Gnat.').
card_first_print('brass gnat', 'TSP').
card_image_name('brass gnat'/'TSP', 'brass gnat').
card_uid('brass gnat'/'TSP', 'TSP:Brass Gnat:brass gnat').
card_rarity('brass gnat'/'TSP', 'Common').
card_artist('brass gnat'/'TSP', 'Martina Pilcerova').
card_number('brass gnat'/'TSP', '249').
card_flavor_text('brass gnat'/'TSP', 'Its buzzing is the sound of its inner mechanisms, ever winding down.').
card_multiverse_id('brass gnat'/'TSP', '118876').

card_in_set('brine elemental', 'TSP').
card_original_type('brine elemental'/'TSP', 'Creature — Elemental').
card_original_text('brine elemental'/'TSP', 'Morph {5}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Brine Elemental is turned face up, each opponent skips his or her next untap step.').
card_first_print('brine elemental', 'TSP').
card_image_name('brine elemental'/'TSP', 'brine elemental').
card_uid('brine elemental'/'TSP', 'TSP:Brine Elemental:brine elemental').
card_rarity('brine elemental'/'TSP', 'Uncommon').
card_artist('brine elemental'/'TSP', 'Stephen Tappin').
card_number('brine elemental'/'TSP', '50').
card_flavor_text('brine elemental'/'TSP', 'Water calls to water, and the world is left exhausted and withered in its wake.').
card_multiverse_id('brine elemental'/'TSP', '118895').

card_in_set('calciform pools', 'TSP').
card_original_type('calciform pools'/'TSP', 'Land').
card_original_text('calciform pools'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Calciform Pools.\n{1}, Remove X storage counters from Calciform Pools: Add X mana in any combination of {W} and/or {U} to your mana pool.').
card_first_print('calciform pools', 'TSP').
card_image_name('calciform pools'/'TSP', 'calciform pools').
card_uid('calciform pools'/'TSP', 'TSP:Calciform Pools:calciform pools').
card_rarity('calciform pools'/'TSP', 'Uncommon').
card_artist('calciform pools'/'TSP', 'Darrell Riche').
card_number('calciform pools'/'TSP', '270').
card_multiverse_id('calciform pools'/'TSP', '108930').

card_in_set('call to the netherworld', 'TSP').
card_original_type('call to the netherworld'/'TSP', 'Sorcery').
card_original_text('call to the netherworld'/'TSP', 'Return target black creature card from your graveyard to your hand.\nMadness {0} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('call to the netherworld', 'TSP').
card_image_name('call to the netherworld'/'TSP', 'call to the netherworld').
card_uid('call to the netherworld'/'TSP', 'TSP:Call to the Netherworld:call to the netherworld').
card_rarity('call to the netherworld'/'TSP', 'Common').
card_artist('call to the netherworld'/'TSP', 'Vance Kovacs').
card_number('call to the netherworld'/'TSP', '97').
card_flavor_text('call to the netherworld'/'TSP', 'The ritual was normally performed only by horrors and pit spawn. Lesser mages had but one sanity to crack in the casting.').
card_multiverse_id('call to the netherworld'/'TSP', '108832').

card_in_set('cancel', 'TSP').
card_original_type('cancel'/'TSP', 'Instant').
card_original_text('cancel'/'TSP', 'Counter target spell.').
card_image_name('cancel'/'TSP', 'cancel').
card_uid('cancel'/'TSP', 'TSP:Cancel:cancel').
card_rarity('cancel'/'TSP', 'Common').
card_artist('cancel'/'TSP', 'Mark Poole').
card_number('cancel'/'TSP', '51').
card_flavor_text('cancel'/'TSP', 'Fendros gasped as he watched the spell drip from the ends of his fingers. He moved his foot, afraid to disturb the spot where it lay slain.').
card_multiverse_id('cancel'/'TSP', '113523').

card_in_set('candles of leng', 'TSP').
card_original_type('candles of leng'/'TSP', 'Artifact').
card_original_text('candles of leng'/'TSP', '{4}, {T}: Reveal the top card of your library. If it has the same name as a card in your graveyard, put it into your graveyard. Otherwise, draw a card.').
card_first_print('candles of leng', 'TSP').
card_image_name('candles of leng'/'TSP', 'candles of leng').
card_uid('candles of leng'/'TSP', 'TSP:Candles of Leng:candles of leng').
card_rarity('candles of leng'/'TSP', 'Rare').
card_artist('candles of leng'/'TSP', 'Daniel Gelon').
card_number('candles of leng'/'TSP', '250').
card_flavor_text('candles of leng'/'TSP', '\"A tome stored in its light is as crisp as the day it was written.\"\n—Ettovard, Tolarian archivist').
card_multiverse_id('candles of leng'/'TSP', '113529').

card_in_set('careful consideration', 'TSP').
card_original_type('careful consideration'/'TSP', 'Instant').
card_original_text('careful consideration'/'TSP', 'Target player draws four cards, then discards three cards. If you played this spell during your main phase, instead that player draws four cards, then discards two cards.').
card_first_print('careful consideration', 'TSP').
card_image_name('careful consideration'/'TSP', 'careful consideration').
card_uid('careful consideration'/'TSP', 'TSP:Careful Consideration:careful consideration').
card_rarity('careful consideration'/'TSP', 'Uncommon').
card_artist('careful consideration'/'TSP', 'Janine Johnston').
card_number('careful consideration'/'TSP', '52').
card_flavor_text('careful consideration'/'TSP', '\"Sages who take the time to verify their sources are rewarded for their diligence.\"\n—Jhoira, to Teferi').
card_multiverse_id('careful consideration'/'TSP', '109691').

card_in_set('castle raptors', 'TSP').
card_original_type('castle raptors'/'TSP', 'Creature — Bird Soldier').
card_original_text('castle raptors'/'TSP', 'Flying\nAs long as Castle Raptors is untapped, it gets +0/+2.').
card_first_print('castle raptors', 'TSP').
card_image_name('castle raptors'/'TSP', 'castle raptors').
card_uid('castle raptors'/'TSP', 'TSP:Castle Raptors:castle raptors').
card_rarity('castle raptors'/'TSP', 'Common').
card_artist('castle raptors'/'TSP', 'Christopher Moeller').
card_number('castle raptors'/'TSP', '5').
card_flavor_text('castle raptors'/'TSP', '\"Each place we see is as ruined and lifeless as the last. So here we stay, because this ruin is ours.\"').
card_multiverse_id('castle raptors'/'TSP', '113614').

card_in_set('cavalry master', 'TSP').
card_original_type('cavalry master'/'TSP', 'Creature — Human Knight').
card_original_text('cavalry master'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nOther creatures you control with flanking have flanking. (Each instance of flanking triggers separately.)').
card_first_print('cavalry master', 'TSP').
card_image_name('cavalry master'/'TSP', 'cavalry master').
card_uid('cavalry master'/'TSP', 'TSP:Cavalry Master:cavalry master').
card_rarity('cavalry master'/'TSP', 'Uncommon').
card_artist('cavalry master'/'TSP', 'Thomas M. Baxa').
card_number('cavalry master'/'TSP', '6').
card_multiverse_id('cavalry master'/'TSP', '108903').

card_in_set('celestial crusader', 'TSP').
card_original_type('celestial crusader'/'TSP', 'Creature — Spirit').
card_original_text('celestial crusader'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nSplit second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nFlying\nOther white creatures get +1/+1.').
card_first_print('celestial crusader', 'TSP').
card_image_name('celestial crusader'/'TSP', 'celestial crusader').
card_uid('celestial crusader'/'TSP', 'TSP:Celestial Crusader:celestial crusader').
card_rarity('celestial crusader'/'TSP', 'Uncommon').
card_artist('celestial crusader'/'TSP', 'Jim Murray').
card_number('celestial crusader'/'TSP', '7').
card_multiverse_id('celestial crusader'/'TSP', '126281').

card_in_set('chameleon blur', 'TSP').
card_original_type('chameleon blur'/'TSP', 'Instant').
card_original_text('chameleon blur'/'TSP', 'Prevent all damage that creatures would deal to players this turn.').
card_first_print('chameleon blur', 'TSP').
card_image_name('chameleon blur'/'TSP', 'chameleon blur').
card_uid('chameleon blur'/'TSP', 'TSP:Chameleon Blur:chameleon blur').
card_rarity('chameleon blur'/'TSP', 'Common').
card_artist('chameleon blur'/'TSP', 'Anthony S. Waters').
card_number('chameleon blur'/'TSP', '192').
card_flavor_text('chameleon blur'/'TSP', '\"Sometimes the best plan is neither fighting nor running.\"\n—Freyalise').
card_multiverse_id('chameleon blur'/'TSP', '118925').

card_in_set('children of korlis', 'TSP').
card_original_type('children of korlis'/'TSP', 'Creature — Human Rebel Cleric').
card_original_text('children of korlis'/'TSP', 'Sacrifice Children of Korlis: You gain life equal to the life you\'ve lost this turn. (Damage causes loss of life.)').
card_first_print('children of korlis', 'TSP').
card_image_name('children of korlis'/'TSP', 'children of korlis').
card_uid('children of korlis'/'TSP', 'TSP:Children of Korlis:children of korlis').
card_rarity('children of korlis'/'TSP', 'Common').
card_artist('children of korlis'/'TSP', 'Quinton Hoover').
card_number('children of korlis'/'TSP', '8').
card_flavor_text('children of korlis'/'TSP', '\"We have a proud history of self-sacrifice. But it is easy, in these bleak times, to find one among us who is eager to die for any cause.\"\n—Tavalus, acolyte of Korlis').
card_multiverse_id('children of korlis'/'TSP', '110525').

card_in_set('chromatic star', 'TSP').
card_original_type('chromatic star'/'TSP', 'Artifact').
card_original_text('chromatic star'/'TSP', '{1}, {T}, Sacrifice Chromatic Star: Add one mana of any color to your mana pool.\nWhen Chromatic Star is put into a graveyard from play, draw a card.').
card_first_print('chromatic star', 'TSP').
card_image_name('chromatic star'/'TSP', 'chromatic star').
card_uid('chromatic star'/'TSP', 'TSP:Chromatic Star:chromatic star').
card_rarity('chromatic star'/'TSP', 'Common').
card_artist('chromatic star'/'TSP', 'Alex Horley-Orlandelli').
card_number('chromatic star'/'TSP', '251').
card_flavor_text('chromatic star'/'TSP', '\"This item is not from . . . now. It reflects a sky no longer ours and gleams with hope that does not exist.\"\n—Tavalus, acolyte of Korlis').
card_multiverse_id('chromatic star'/'TSP', '118891').

card_in_set('chronatog totem', 'TSP').
card_original_type('chronatog totem'/'TSP', 'Artifact').
card_original_text('chronatog totem'/'TSP', '{T}: Add {U} to your mana pool.\n{1}{U}: Chronatog Totem becomes a 1/2 blue Atog artifact creature until end of turn.\n{0}: Chronatog Totem gets +3/+3 until end of turn. You skip your next turn. Play this ability only once each turn and only if Chronatog Totem is a creature.').
card_first_print('chronatog totem', 'TSP').
card_image_name('chronatog totem'/'TSP', 'chronatog totem').
card_uid('chronatog totem'/'TSP', 'TSP:Chronatog Totem:chronatog totem').
card_rarity('chronatog totem'/'TSP', 'Uncommon').
card_artist('chronatog totem'/'TSP', 'Christopher Rush').
card_number('chronatog totem'/'TSP', '252').
card_multiverse_id('chronatog totem'/'TSP', '106630').

card_in_set('chronosavant', 'TSP').
card_original_type('chronosavant'/'TSP', 'Creature — Giant').
card_original_text('chronosavant'/'TSP', '{1}{W}: Return Chronosavant from your graveyard to play tapped. Skip your next turn.').
card_first_print('chronosavant', 'TSP').
card_image_name('chronosavant'/'TSP', 'chronosavant').
card_uid('chronosavant'/'TSP', 'TSP:Chronosavant:chronosavant').
card_rarity('chronosavant'/'TSP', 'Rare').
card_artist('chronosavant'/'TSP', 'Pete Venters').
card_number('chronosavant'/'TSP', '9').
card_flavor_text('chronosavant'/'TSP', '\"In my dreams, I hear the voices of my future selves who have died in times yet to come. I use that knowledge to avoid those dark futures and continue my search for peace.\"').
card_multiverse_id('chronosavant'/'TSP', '110502').

card_in_set('clockspinning', 'TSP').
card_original_type('clockspinning'/'TSP', 'Instant').
card_original_text('clockspinning'/'TSP', 'Buyback {3} (You may pay an additional {3} as you play this spell. If you do, put this card into your hand as it resolves.)\nChoose a counter on target permanent or suspended card. Remove that counter from that permanent or card or put another of those counters on it.').
card_first_print('clockspinning', 'TSP').
card_image_name('clockspinning'/'TSP', 'clockspinning').
card_uid('clockspinning'/'TSP', 'TSP:Clockspinning:clockspinning').
card_rarity('clockspinning'/'TSP', 'Common').
card_artist('clockspinning'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('clockspinning'/'TSP', '53').
card_multiverse_id('clockspinning'/'TSP', '109673').

card_in_set('clockwork hydra', 'TSP').
card_original_type('clockwork hydra'/'TSP', 'Artifact Creature — Hydra').
card_original_text('clockwork hydra'/'TSP', 'Clockwork Hydra comes into play with four +1/+1 counters on it.\nWhenever Clockwork Hydra attacks or blocks, remove a +1/+1 counter from it. If you do, Clockwork Hydra deals 1 damage to target creature or player.\n{T}: Put a +1/+1 counter on Clockwork Hydra.').
card_first_print('clockwork hydra', 'TSP').
card_image_name('clockwork hydra'/'TSP', 'clockwork hydra').
card_uid('clockwork hydra'/'TSP', 'TSP:Clockwork Hydra:clockwork hydra').
card_rarity('clockwork hydra'/'TSP', 'Uncommon').
card_artist('clockwork hydra'/'TSP', 'Daren Bader').
card_number('clockwork hydra'/'TSP', '253').
card_multiverse_id('clockwork hydra'/'TSP', '113530').

card_in_set('cloudchaser kestrel', 'TSP').
card_original_type('cloudchaser kestrel'/'TSP', 'Creature — Bird').
card_original_text('cloudchaser kestrel'/'TSP', 'Flying\nWhen Cloudchaser Kestrel comes into play, destroy target enchantment.\n{W}: Target permanent becomes white until end of turn.').
card_first_print('cloudchaser kestrel', 'TSP').
card_image_name('cloudchaser kestrel'/'TSP', 'cloudchaser kestrel').
card_uid('cloudchaser kestrel'/'TSP', 'TSP:Cloudchaser Kestrel:cloudchaser kestrel').
card_rarity('cloudchaser kestrel'/'TSP', 'Common').
card_artist('cloudchaser kestrel'/'TSP', 'Daren Bader').
card_number('cloudchaser kestrel'/'TSP', '10').
card_multiverse_id('cloudchaser kestrel'/'TSP', '113507').

card_in_set('coal stoker', 'TSP').
card_original_type('coal stoker'/'TSP', 'Creature — Elemental').
card_original_text('coal stoker'/'TSP', 'When Coal Stoker comes into play, if you played it from your hand, add {R}{R}{R} to your mana pool.').
card_first_print('coal stoker', 'TSP').
card_image_name('coal stoker'/'TSP', 'coal stoker').
card_uid('coal stoker'/'TSP', 'TSP:Coal Stoker:coal stoker').
card_rarity('coal stoker'/'TSP', 'Common').
card_artist('coal stoker'/'TSP', 'Mark Zug').
card_number('coal stoker'/'TSP', '150').
card_flavor_text('coal stoker'/'TSP', '\"The day is mine! I sent three such creatures against my foe, then watched as my magefire popped her soldiers like overripe spleenfruits.\"\n—Dobruk the Unstable, pyromancer').
card_multiverse_id('coal stoker'/'TSP', '113618').

card_in_set('conflagrate', 'TSP').
card_original_type('conflagrate'/'TSP', 'Sorcery').
card_original_text('conflagrate'/'TSP', 'Conflagrate deals X damage divided as you choose among any number of target creatures and/or players.\nFlashback—{R}{R}, Discard X cards. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('conflagrate', 'TSP').
card_image_name('conflagrate'/'TSP', 'conflagrate').
card_uid('conflagrate'/'TSP', 'TSP:Conflagrate:conflagrate').
card_rarity('conflagrate'/'TSP', 'Uncommon').
card_artist('conflagrate'/'TSP', 'Warren Mahy').
card_number('conflagrate'/'TSP', '151').
card_multiverse_id('conflagrate'/'TSP', '114909').

card_in_set('coral trickster', 'TSP').
card_original_type('coral trickster'/'TSP', 'Creature — Merfolk Rogue').
card_original_text('coral trickster'/'TSP', 'Morph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Coral Trickster is turned face up, you may tap or untap target permanent.').
card_first_print('coral trickster', 'TSP').
card_image_name('coral trickster'/'TSP', 'coral trickster').
card_uid('coral trickster'/'TSP', 'TSP:Coral Trickster:coral trickster').
card_rarity('coral trickster'/'TSP', 'Common').
card_artist('coral trickster'/'TSP', 'D. Alexander Gregory').
card_number('coral trickster'/'TSP', '54').
card_flavor_text('coral trickster'/'TSP', 'They wait in darkened depths, laughing eagerly.').
card_multiverse_id('coral trickster'/'TSP', '110511').

card_in_set('corpulent corpse', 'TSP').
card_original_type('corpulent corpse'/'TSP', 'Creature — Zombie').
card_original_text('corpulent corpse'/'TSP', 'Fear\nSuspend 5—{B} (Rather than play this card from your hand, you may pay {B} and remove it from the game with five time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('corpulent corpse', 'TSP').
card_image_name('corpulent corpse'/'TSP', 'corpulent corpse').
card_uid('corpulent corpse'/'TSP', 'TSP:Corpulent Corpse:corpulent corpse').
card_rarity('corpulent corpse'/'TSP', 'Common').
card_artist('corpulent corpse'/'TSP', 'Doug Chaffee').
card_number('corpulent corpse'/'TSP', '98').
card_multiverse_id('corpulent corpse'/'TSP', '108910').

card_in_set('crookclaw transmuter', 'TSP').
card_original_type('crookclaw transmuter'/'TSP', 'Creature — Bird Wizard').
card_original_text('crookclaw transmuter'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nWhen Crookclaw Transmuter comes into play, switch target creature\'s power and toughness until end of turn.').
card_first_print('crookclaw transmuter', 'TSP').
card_image_name('crookclaw transmuter'/'TSP', 'crookclaw transmuter').
card_uid('crookclaw transmuter'/'TSP', 'TSP:Crookclaw Transmuter:crookclaw transmuter').
card_rarity('crookclaw transmuter'/'TSP', 'Common').
card_artist('crookclaw transmuter'/'TSP', 'Ron Spencer').
card_number('crookclaw transmuter'/'TSP', '55').
card_multiverse_id('crookclaw transmuter'/'TSP', '106668').

card_in_set('curse of the cabal', 'TSP').
card_original_type('curse of the cabal'/'TSP', 'Sorcery').
card_original_text('curse of the cabal'/'TSP', 'Target player sacrifices half the permanents he or she controls, rounded down.\nSuspend 2—{2}{B}{B}\nAt the beginning of each player\'s upkeep, if Curse of the Cabal is suspended, that player may sacrifice a permanent. If he or she does, put two time counters on Curse of the Cabal.').
card_first_print('curse of the cabal', 'TSP').
card_image_name('curse of the cabal'/'TSP', 'curse of the cabal').
card_uid('curse of the cabal'/'TSP', 'TSP:Curse of the Cabal:curse of the cabal').
card_rarity('curse of the cabal'/'TSP', 'Rare').
card_artist('curse of the cabal'/'TSP', 'John Avon').
card_number('curse of the cabal'/'TSP', '99').
card_multiverse_id('curse of the cabal'/'TSP', '111049').

card_in_set('cyclopean giant', 'TSP').
card_original_type('cyclopean giant'/'TSP', 'Creature — Zombie Giant').
card_original_text('cyclopean giant'/'TSP', 'When Cyclopean Giant is put into a graveyard from play, target land becomes a Swamp. Remove Cyclopean Giant from the game.').
card_first_print('cyclopean giant', 'TSP').
card_image_name('cyclopean giant'/'TSP', 'cyclopean giant').
card_uid('cyclopean giant'/'TSP', 'TSP:Cyclopean Giant:cyclopean giant').
card_rarity('cyclopean giant'/'TSP', 'Common').
card_artist('cyclopean giant'/'TSP', 'Mark Tedin').
card_number('cyclopean giant'/'TSP', '100').
card_flavor_text('cyclopean giant'/'TSP', 'A tomb-eye may remain shut for centuries. It opens when it sees a world as twisted and hollow as the wretch beneath its lid.').
card_multiverse_id('cyclopean giant'/'TSP', '114915').

card_in_set('d\'avenant healer', 'TSP').
card_original_type('d\'avenant healer'/'TSP', 'Creature — Human Cleric Archer').
card_original_text('d\'avenant healer'/'TSP', '{T}: D\'Avenant Healer deals 1 damage to target attacking or blocking creature.\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('d\'avenant healer', 'TSP').
card_image_name('d\'avenant healer'/'TSP', 'd\'avenant healer').
card_uid('d\'avenant healer'/'TSP', 'TSP:D\'Avenant Healer:d\'avenant healer').
card_rarity('d\'avenant healer'/'TSP', 'Common').
card_artist('d\'avenant healer'/'TSP', 'Michael Sutfin').
card_number('d\'avenant healer'/'TSP', '11').
card_flavor_text('d\'avenant healer'/'TSP', '\"One arrow keenly fired might prevent more battlefield wounds than I could treat.\"').
card_multiverse_id('d\'avenant healer'/'TSP', '113518').

card_in_set('dark withering', 'TSP').
card_original_type('dark withering'/'TSP', 'Instant').
card_original_text('dark withering'/'TSP', 'Destroy target nonblack creature.\nMadness {B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('dark withering', 'TSP').
card_image_name('dark withering'/'TSP', 'dark withering').
card_uid('dark withering'/'TSP', 'TSP:Dark Withering:dark withering').
card_rarity('dark withering'/'TSP', 'Common').
card_artist('dark withering'/'TSP', 'Wayne Reynolds').
card_number('dark withering'/'TSP', '101').
card_multiverse_id('dark withering'/'TSP', '118892').

card_in_set('deathspore thallid', 'TSP').
card_original_type('deathspore thallid'/'TSP', 'Creature — Zombie Fungus').
card_original_text('deathspore thallid'/'TSP', 'At the beginning of your upkeep, put a spore counter on Deathspore Thallid.\nRemove three spore counters from Deathspore Thallid: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Target creature gets -1/-1 until end of turn.').
card_first_print('deathspore thallid', 'TSP').
card_image_name('deathspore thallid'/'TSP', 'deathspore thallid').
card_uid('deathspore thallid'/'TSP', 'TSP:Deathspore Thallid:deathspore thallid').
card_rarity('deathspore thallid'/'TSP', 'Common').
card_artist('deathspore thallid'/'TSP', 'Randy Elliott').
card_number('deathspore thallid'/'TSP', '102').
card_multiverse_id('deathspore thallid'/'TSP', '116743').

card_in_set('deep-sea kraken', 'TSP').
card_original_type('deep-sea kraken'/'TSP', 'Creature — Kraken').
card_original_text('deep-sea kraken'/'TSP', 'Deep-Sea Kraken is unblockable.\nSuspend 9—{2}{U}\nWhenever an opponent plays a spell, if Deep-Sea Kraken is suspended, remove a time counter from it.').
card_first_print('deep-sea kraken', 'TSP').
card_image_name('deep-sea kraken'/'TSP', 'deep-sea kraken').
card_uid('deep-sea kraken'/'TSP', 'TSP:Deep-Sea Kraken:deep-sea kraken').
card_rarity('deep-sea kraken'/'TSP', 'Rare').
card_artist('deep-sea kraken'/'TSP', 'Christopher Moeller').
card_number('deep-sea kraken'/'TSP', '56').
card_flavor_text('deep-sea kraken'/'TSP', 'The rift remained open for days, sluicing ancient seawater. It closed only after the last great tentacle squirmed its way through.').
card_multiverse_id('deep-sea kraken'/'TSP', '113506').

card_in_set('dementia sliver', 'TSP').
card_original_type('dementia sliver'/'TSP', 'Creature — Sliver').
card_original_text('dementia sliver'/'TSP', 'All Slivers have \"{T}: Name a card. Target opponent reveals a card at random from his or her hand. If it\'s the named card, that player discards it. Play this ability only during your turn.\"').
card_first_print('dementia sliver', 'TSP').
card_image_name('dementia sliver'/'TSP', 'dementia sliver').
card_uid('dementia sliver'/'TSP', 'TSP:Dementia Sliver:dementia sliver').
card_rarity('dementia sliver'/'TSP', 'Uncommon').
card_artist('dementia sliver'/'TSP', 'Una Fricker').
card_number('dementia sliver'/'TSP', '236').
card_multiverse_id('dementia sliver'/'TSP', '109668').

card_in_set('demonic collusion', 'TSP').
card_original_type('demonic collusion'/'TSP', 'Sorcery').
card_original_text('demonic collusion'/'TSP', 'Buyback—Discard two cards. (You may discard two cards in addition to any other costs as you play this spell. If you do, put this card into your hand as it resolves.)\nSearch your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('demonic collusion', 'TSP').
card_image_name('demonic collusion'/'TSP', 'demonic collusion').
card_uid('demonic collusion'/'TSP', 'TSP:Demonic Collusion:demonic collusion').
card_rarity('demonic collusion'/'TSP', 'Rare').
card_artist('demonic collusion'/'TSP', 'Jim Nelson').
card_number('demonic collusion'/'TSP', '103').
card_multiverse_id('demonic collusion'/'TSP', '118872').

card_in_set('detainment spell', 'TSP').
card_original_type('detainment spell'/'TSP', 'Enchantment — Aura').
card_original_text('detainment spell'/'TSP', 'Enchant creature\nEnchanted creature\'s activated abilities can\'t be played.\n{1}{W}: Attach Detainment Spell to target creature.').
card_first_print('detainment spell', 'TSP').
card_image_name('detainment spell'/'TSP', 'detainment spell').
card_uid('detainment spell'/'TSP', 'TSP:Detainment Spell:detainment spell').
card_rarity('detainment spell'/'TSP', 'Common').
card_artist('detainment spell'/'TSP', 'Darrell Riche').
card_number('detainment spell'/'TSP', '12').
card_flavor_text('detainment spell'/'TSP', 'Bind the mind rather than the wrist, and you stop the intent to harm before it starts.').
card_multiverse_id('detainment spell'/'TSP', '116728').

card_in_set('divine congregation', 'TSP').
card_original_type('divine congregation'/'TSP', 'Sorcery').
card_original_text('divine congregation'/'TSP', 'You gain 2 life for each creature target player controls.\nSuspend 5—{1}{W} (Rather than play this card from your hand, you may pay {1}{W} and remove it from the game with five time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_first_print('divine congregation', 'TSP').
card_image_name('divine congregation'/'TSP', 'divine congregation').
card_uid('divine congregation'/'TSP', 'TSP:Divine Congregation:divine congregation').
card_rarity('divine congregation'/'TSP', 'Common').
card_artist('divine congregation'/'TSP', 'Jeremy Jarvis').
card_number('divine congregation'/'TSP', '13').
card_multiverse_id('divine congregation'/'TSP', '108814').

card_in_set('draining whelk', 'TSP').
card_original_type('draining whelk'/'TSP', 'Creature — Illusion').
card_original_text('draining whelk'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nWhen Draining Whelk comes into play, counter target spell. Put X +1/+1 counters on Draining Whelk, where X is that spell\'s converted mana cost.').
card_first_print('draining whelk', 'TSP').
card_image_name('draining whelk'/'TSP', 'draining whelk').
card_uid('draining whelk'/'TSP', 'TSP:Draining Whelk:draining whelk').
card_rarity('draining whelk'/'TSP', 'Rare').
card_artist('draining whelk'/'TSP', 'Mark Tedin').
card_number('draining whelk'/'TSP', '57').
card_multiverse_id('draining whelk'/'TSP', '111057').

card_in_set('dralnu, lich lord', 'TSP').
card_original_type('dralnu, lich lord'/'TSP', 'Legendary Creature — Zombie Lord').
card_original_text('dralnu, lich lord'/'TSP', 'If damage would be dealt to Dralnu, sacrifice that many permanents instead.\n{T}: Target instant or sorcery card in your graveyard has flashback until end of turn. Its flashback cost becomes equal to its mana cost as you play it. (You may play that card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('dralnu, lich lord', 'TSP').
card_image_name('dralnu, lich lord'/'TSP', 'dralnu, lich lord').
card_uid('dralnu, lich lord'/'TSP', 'TSP:Dralnu, Lich Lord:dralnu, lich lord').
card_rarity('dralnu, lich lord'/'TSP', 'Rare').
card_artist('dralnu, lich lord'/'TSP', 'Greg Staples').
card_number('dralnu, lich lord'/'TSP', '237').
card_multiverse_id('dralnu, lich lord'/'TSP', '113541').

card_in_set('dread return', 'TSP').
card_original_type('dread return'/'TSP', 'Sorcery').
card_original_text('dread return'/'TSP', 'Return target creature card from your graveyard to play.\nFlashback—Sacrifice three creatures. (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('dread return', 'TSP').
card_image_name('dread return'/'TSP', 'dread return').
card_uid('dread return'/'TSP', 'TSP:Dread Return:dread return').
card_rarity('dread return'/'TSP', 'Uncommon').
card_artist('dread return'/'TSP', 'Kev Walker').
card_number('dread return'/'TSP', '104').
card_flavor_text('dread return'/'TSP', 'Those who forget the horrors of the past are doomed to re-meet them.').
card_multiverse_id('dread return'/'TSP', '116721').

card_in_set('dreadship reef', 'TSP').
card_original_type('dreadship reef'/'TSP', 'Land').
card_original_text('dreadship reef'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Dreadship Reef.\n{1}, Remove X storage counters from Dreadship Reef: Add X mana in any combination of {U} and/or {B} to your mana pool.').
card_first_print('dreadship reef', 'TSP').
card_image_name('dreadship reef'/'TSP', 'dreadship reef').
card_uid('dreadship reef'/'TSP', 'TSP:Dreadship Reef:dreadship reef').
card_rarity('dreadship reef'/'TSP', 'Uncommon').
card_artist('dreadship reef'/'TSP', 'Lars Grant-West').
card_number('dreadship reef'/'TSP', '271').
card_multiverse_id('dreadship reef'/'TSP', '108817').

card_in_set('dream stalker', 'TSP').
card_original_type('dream stalker'/'TSP', 'Creature — Illusion').
card_original_text('dream stalker'/'TSP', 'When Dream Stalker comes into play, return a permanent you control to its owner\'s hand.').
card_first_print('dream stalker', 'TSP').
card_image_name('dream stalker'/'TSP', 'dream stalker').
card_uid('dream stalker'/'TSP', 'TSP:Dream Stalker:dream stalker').
card_rarity('dream stalker'/'TSP', 'Common').
card_artist('dream stalker'/'TSP', 'Brian Despain').
card_number('dream stalker'/'TSP', '58').
card_flavor_text('dream stalker'/'TSP', 'What happens when it is the dream that wakes and the sleeper that fades into memory?').
card_multiverse_id('dream stalker'/'TSP', '108858').

card_in_set('drifter il-dal', 'TSP').
card_original_type('drifter il-dal'/'TSP', 'Creature — Human Wizard').
card_original_text('drifter il-dal'/'TSP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nAt the beginning of your upkeep, sacrifice Drifter il-Dal unless you pay {U}.').
card_first_print('drifter il-dal', 'TSP').
card_image_name('drifter il-dal'/'TSP', 'drifter il-dal').
card_uid('drifter il-dal'/'TSP', 'TSP:Drifter il-Dal:drifter il-dal').
card_rarity('drifter il-dal'/'TSP', 'Common').
card_artist('drifter il-dal'/'TSP', 'Justin Sweet').
card_number('drifter il-dal'/'TSP', '59').
card_flavor_text('drifter il-dal'/'TSP', 'They study the deeds of their traitorous ancestors, hoping the stories may reveal a way back to the physical world.').
card_multiverse_id('drifter il-dal'/'TSP', '108918').

card_in_set('drudge reavers', 'TSP').
card_original_type('drudge reavers'/'TSP', 'Creature — Skeleton').
card_original_text('drudge reavers'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\n{B}: Regenerate Drudge Reavers.').
card_first_print('drudge reavers', 'TSP').
card_image_name('drudge reavers'/'TSP', 'drudge reavers').
card_uid('drudge reavers'/'TSP', 'TSP:Drudge Reavers:drudge reavers').
card_rarity('drudge reavers'/'TSP', 'Common').
card_artist('drudge reavers'/'TSP', 'Greg Staples').
card_number('drudge reavers'/'TSP', '105').
card_flavor_text('drudge reavers'/'TSP', '\"The surface of the land is blanched with salt, but the soil beneath is reddened with death—a fertile ground for necromancy.\"\n—Lim-Dûl the Necromancer').
card_multiverse_id('drudge reavers'/'TSP', '108850').

card_in_set('durkwood baloth', 'TSP').
card_original_type('durkwood baloth'/'TSP', 'Creature — Beast').
card_original_text('durkwood baloth'/'TSP', 'Suspend 5—{G} (Rather than play this card from your hand, you may pay {G} and remove it from the game with five time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('durkwood baloth', 'TSP').
card_image_name('durkwood baloth'/'TSP', 'durkwood baloth').
card_uid('durkwood baloth'/'TSP', 'TSP:Durkwood Baloth:durkwood baloth').
card_rarity('durkwood baloth'/'TSP', 'Common').
card_artist('durkwood baloth'/'TSP', 'Dan Frazier').
card_number('durkwood baloth'/'TSP', '193').
card_multiverse_id('durkwood baloth'/'TSP', '108807').

card_in_set('durkwood tracker', 'TSP').
card_original_type('durkwood tracker'/'TSP', 'Creature — Giant').
card_original_text('durkwood tracker'/'TSP', '{1}{G}, {T}: If Durkwood Tracker is in play, it deals damage equal to its power to target attacking creature. That creature deals damage equal to its power to Durkwood Tracker.').
card_first_print('durkwood tracker', 'TSP').
card_image_name('durkwood tracker'/'TSP', 'durkwood tracker').
card_uid('durkwood tracker'/'TSP', 'TSP:Durkwood Tracker:durkwood tracker').
card_rarity('durkwood tracker'/'TSP', 'Uncommon').
card_artist('durkwood tracker'/'TSP', 'Michael Phillippi').
card_number('durkwood tracker'/'TSP', '194').
card_flavor_text('durkwood tracker'/'TSP', 'These days, trackers must follow their quarry over years as well as miles.').
card_multiverse_id('durkwood tracker'/'TSP', '118893').

card_in_set('duskrider peregrine', 'TSP').
card_original_type('duskrider peregrine'/'TSP', 'Creature — Bird').
card_original_text('duskrider peregrine'/'TSP', 'Flying, protection from black\nSuspend 3—{1}{W} (Rather than play this card from your hand, you may pay {1}{W} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('duskrider peregrine', 'TSP').
card_image_name('duskrider peregrine'/'TSP', 'duskrider peregrine').
card_uid('duskrider peregrine'/'TSP', 'TSP:Duskrider Peregrine:duskrider peregrine').
card_rarity('duskrider peregrine'/'TSP', 'Uncommon').
card_artist('duskrider peregrine'/'TSP', 'Una Fricker').
card_number('duskrider peregrine'/'TSP', '14').
card_multiverse_id('duskrider peregrine'/'TSP', '108887').

card_in_set('empty the warrens', 'TSP').
card_original_type('empty the warrens'/'TSP', 'Sorcery').
card_original_text('empty the warrens'/'TSP', 'Put two 1/1 red Goblin creature tokens into play.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_first_print('empty the warrens', 'TSP').
card_image_name('empty the warrens'/'TSP', 'empty the warrens').
card_uid('empty the warrens'/'TSP', 'TSP:Empty the Warrens:empty the warrens').
card_rarity('empty the warrens'/'TSP', 'Common').
card_artist('empty the warrens'/'TSP', 'Mark Brill').
card_number('empty the warrens'/'TSP', '152').
card_flavor_text('empty the warrens'/'TSP', '\"They\'d pour out of the warrens to make war (and to make room for the littering matrons).\"\n—Sarpadian Empires, vol. IV').
card_multiverse_id('empty the warrens'/'TSP', '109735').

card_in_set('endrek sahr, master breeder', 'TSP').
card_original_type('endrek sahr, master breeder'/'TSP', 'Legendary Creature — Human Wizard').
card_original_text('endrek sahr, master breeder'/'TSP', 'Whenever you play a creature spell, put X 1/1 black Thrull creature tokens into play, where X is that spell\'s converted mana cost.\nWhen you control seven or more Thrulls, sacrifice Endrek Sahr, Master Breeder.').
card_first_print('endrek sahr, master breeder', 'TSP').
card_image_name('endrek sahr, master breeder'/'TSP', 'endrek sahr, master breeder').
card_uid('endrek sahr, master breeder'/'TSP', 'TSP:Endrek Sahr, Master Breeder:endrek sahr, master breeder').
card_rarity('endrek sahr, master breeder'/'TSP', 'Rare').
card_artist('endrek sahr, master breeder'/'TSP', 'Mark Tedin').
card_number('endrek sahr, master breeder'/'TSP', '106').
card_multiverse_id('endrek sahr, master breeder'/'TSP', '113527').

card_in_set('errant doomsayers', 'TSP').
card_original_type('errant doomsayers'/'TSP', 'Creature — Human Rebel').
card_original_text('errant doomsayers'/'TSP', '{T}: Tap target creature with toughness 2 or less.').
card_first_print('errant doomsayers', 'TSP').
card_image_name('errant doomsayers'/'TSP', 'errant doomsayers').
card_uid('errant doomsayers'/'TSP', 'TSP:Errant Doomsayers:errant doomsayers').
card_rarity('errant doomsayers'/'TSP', 'Common').
card_artist('errant doomsayers'/'TSP', 'Liz Danforth').
card_number('errant doomsayers'/'TSP', '15').
card_flavor_text('errant doomsayers'/'TSP', '\"Heed my words, traveler. Plagues, war, desolation . . . all mere hints of what is yet to come from Dominaria\'s vault of horrors.\"').
card_multiverse_id('errant doomsayers'/'TSP', '116379').

card_in_set('errant ephemeron', 'TSP').
card_original_type('errant ephemeron'/'TSP', 'Creature — Illusion').
card_original_text('errant ephemeron'/'TSP', 'Flying\nSuspend 4—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('errant ephemeron', 'TSP').
card_image_name('errant ephemeron'/'TSP', 'errant ephemeron').
card_uid('errant ephemeron'/'TSP', 'TSP:Errant Ephemeron:errant ephemeron').
card_rarity('errant ephemeron'/'TSP', 'Common').
card_artist('errant ephemeron'/'TSP', 'Luca Zontini').
card_number('errant ephemeron'/'TSP', '60').
card_multiverse_id('errant ephemeron'/'TSP', '108909').

card_in_set('eternity snare', 'TSP').
card_original_type('eternity snare'/'TSP', 'Enchantment — Aura').
card_original_text('eternity snare'/'TSP', 'Enchant creature\nWhen Eternity Snare comes into play, draw a card.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('eternity snare', 'TSP').
card_image_name('eternity snare'/'TSP', 'eternity snare').
card_uid('eternity snare'/'TSP', 'TSP:Eternity Snare:eternity snare').
card_rarity('eternity snare'/'TSP', 'Common').
card_artist('eternity snare'/'TSP', 'Drew Tucker').
card_number('eternity snare'/'TSP', '61').
card_multiverse_id('eternity snare'/'TSP', '108788').

card_in_set('evangelize', 'TSP').
card_original_type('evangelize'/'TSP', 'Sorcery').
card_original_text('evangelize'/'TSP', 'Buyback {2}{W}{W} (You may pay an additional {2}{W}{W} as you play this spell. If you do, put this card into your hand as it resolves.)\nGain control of target creature of an opponent\'s choice that he or she controls.').
card_first_print('evangelize', 'TSP').
card_image_name('evangelize'/'TSP', 'evangelize').
card_uid('evangelize'/'TSP', 'TSP:Evangelize:evangelize').
card_rarity('evangelize'/'TSP', 'Rare').
card_artist('evangelize'/'TSP', 'Randy Elliott').
card_number('evangelize'/'TSP', '16').
card_multiverse_id('evangelize'/'TSP', '110507').

card_in_set('evil eye of urborg', 'TSP').
card_original_type('evil eye of urborg'/'TSP', 'Creature — Eye').
card_original_text('evil eye of urborg'/'TSP', 'Non-Eye creatures you control can\'t attack.\nWhenever Evil Eye of Urborg becomes blocked by a creature, destroy that creature.').
card_first_print('evil eye of urborg', 'TSP').
card_image_name('evil eye of urborg'/'TSP', 'evil eye of urborg').
card_uid('evil eye of urborg'/'TSP', 'TSP:Evil Eye of Urborg:evil eye of urborg').
card_rarity('evil eye of urborg'/'TSP', 'Uncommon').
card_artist('evil eye of urborg'/'TSP', 'Clint Langley').
card_number('evil eye of urborg'/'TSP', '107').
card_flavor_text('evil eye of urborg'/'TSP', '\"Edahlis, what is that thing? We should have stayed in Yavimaya . . . .\" —Aznaph, greenseeker').
card_multiverse_id('evil eye of urborg'/'TSP', '83811').

card_in_set('faceless devourer', 'TSP').
card_original_type('faceless devourer'/'TSP', 'Creature — Nightmare Horror').
card_original_text('faceless devourer'/'TSP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhen Faceless Devourer comes into play, remove another target creature with shadow from the game.\nWhen Faceless Devourer leaves play, return the removed card to play under its owner\'s control.').
card_first_print('faceless devourer', 'TSP').
card_image_name('faceless devourer'/'TSP', 'faceless devourer').
card_uid('faceless devourer'/'TSP', 'TSP:Faceless Devourer:faceless devourer').
card_rarity('faceless devourer'/'TSP', 'Uncommon').
card_artist('faceless devourer'/'TSP', 'Chippy').
card_number('faceless devourer'/'TSP', '108').
card_multiverse_id('faceless devourer'/'TSP', '109719').

card_in_set('fallen ideal', 'TSP').
card_original_type('fallen ideal'/'TSP', 'Enchantment — Aura').
card_original_text('fallen ideal'/'TSP', 'Enchant creature\nEnchanted creature has flying and \"Sacrifice a creature: This creature gets +2/+1 until end of turn.\"\nWhen Fallen Ideal is put into a graveyard from play, return Fallen Ideal to its owner\'s hand.').
card_first_print('fallen ideal', 'TSP').
card_image_name('fallen ideal'/'TSP', 'fallen ideal').
card_uid('fallen ideal'/'TSP', 'TSP:Fallen Ideal:fallen ideal').
card_rarity('fallen ideal'/'TSP', 'Uncommon').
card_artist('fallen ideal'/'TSP', 'Anson Maddocks').
card_number('fallen ideal'/'TSP', '109').
card_multiverse_id('fallen ideal'/'TSP', '114908').

card_in_set('fathom seer', 'TSP').
card_original_type('fathom seer'/'TSP', 'Creature — Illusion').
card_original_text('fathom seer'/'TSP', 'Morph—Return two Islands you control to their owner\'s hand. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_first_print('fathom seer', 'TSP').
card_image_name('fathom seer'/'TSP', 'fathom seer').
card_uid('fathom seer'/'TSP', 'TSP:Fathom Seer:fathom seer').
card_rarity('fathom seer'/'TSP', 'Common').
card_artist('fathom seer'/'TSP', 'Ralph Horsley').
card_number('fathom seer'/'TSP', '62').
card_multiverse_id('fathom seer'/'TSP', '118912').

card_in_set('feebleness', 'TSP').
card_original_type('feebleness'/'TSP', 'Enchantment — Aura').
card_original_text('feebleness'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature gets -2/-1.').
card_first_print('feebleness', 'TSP').
card_image_name('feebleness'/'TSP', 'feebleness').
card_uid('feebleness'/'TSP', 'TSP:Feebleness:feebleness').
card_rarity('feebleness'/'TSP', 'Common').
card_artist('feebleness'/'TSP', 'Kev Walker').
card_number('feebleness'/'TSP', '110').
card_flavor_text('feebleness'/'TSP', 'Just a small touch of magic can harness the debilitating power of Urborg\'s poisonous winds.').
card_multiverse_id('feebleness'/'TSP', '108892').

card_in_set('firemaw kavu', 'TSP').
card_original_type('firemaw kavu'/'TSP', 'Creature — Kavu').
card_original_text('firemaw kavu'/'TSP', 'Echo {5}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Firemaw Kavu comes into play, it deals 2 damage to target creature.\nWhen Firemaw Kavu leaves play, it deals 4 damage to target creature.').
card_first_print('firemaw kavu', 'TSP').
card_image_name('firemaw kavu'/'TSP', 'firemaw kavu').
card_uid('firemaw kavu'/'TSP', 'TSP:Firemaw Kavu:firemaw kavu').
card_rarity('firemaw kavu'/'TSP', 'Uncommon').
card_artist('firemaw kavu'/'TSP', 'Greg Hildebrandt').
card_number('firemaw kavu'/'TSP', '153').
card_multiverse_id('firemaw kavu'/'TSP', '111064').

card_in_set('firewake sliver', 'TSP').
card_original_type('firewake sliver'/'TSP', 'Creature — Sliver').
card_original_text('firewake sliver'/'TSP', 'All Slivers have haste and \"{1}, Sacrifice this creature: Target Sliver gets +2/+2 until end of turn.\"').
card_first_print('firewake sliver', 'TSP').
card_image_name('firewake sliver'/'TSP', 'firewake sliver').
card_uid('firewake sliver'/'TSP', 'TSP:Firewake Sliver:firewake sliver').
card_rarity('firewake sliver'/'TSP', 'Uncommon').
card_artist('firewake sliver'/'TSP', 'Anthony S. Waters').
card_number('firewake sliver'/'TSP', '238').
card_flavor_text('firewake sliver'/'TSP', '\"They are here, and they are hungry. And what they do not eat, they burn. Yavimaya is lost. We must leave now for Skyshroud.\"\n—Edahlis, greenseeker').
card_multiverse_id('firewake sliver'/'TSP', '109727').

card_in_set('flagstones of trokair', 'TSP').
card_original_type('flagstones of trokair'/'TSP', 'Legendary Land').
card_original_text('flagstones of trokair'/'TSP', '{T}: Add {W} to your mana pool.\nWhen Flagstones of Trokair is put into a graveyard from play, you may search your library for a Plains card and put it into play tapped. If you do, shuffle your library.').
card_first_print('flagstones of trokair', 'TSP').
card_image_name('flagstones of trokair'/'TSP', 'flagstones of trokair').
card_uid('flagstones of trokair'/'TSP', 'TSP:Flagstones of Trokair:flagstones of trokair').
card_rarity('flagstones of trokair'/'TSP', 'Rare').
card_artist('flagstones of trokair'/'TSP', 'Rob Alexander').
card_number('flagstones of trokair'/'TSP', '272').
card_flavor_text('flagstones of trokair'/'TSP', 'Dust-spiked winds eroded the stones, but the ground where they had stood still holds power.').
card_multiverse_id('flagstones of trokair'/'TSP', '116733').

card_in_set('flamecore elemental', 'TSP').
card_original_type('flamecore elemental'/'TSP', 'Creature — Elemental').
card_original_text('flamecore elemental'/'TSP', 'Echo {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_first_print('flamecore elemental', 'TSP').
card_image_name('flamecore elemental'/'TSP', 'flamecore elemental').
card_uid('flamecore elemental'/'TSP', 'TSP:Flamecore Elemental:flamecore elemental').
card_rarity('flamecore elemental'/'TSP', 'Common').
card_artist('flamecore elemental'/'TSP', 'Dave Dorman').
card_number('flamecore elemental'/'TSP', '154').
card_flavor_text('flamecore elemental'/'TSP', 'To a fire elemental, fools and fuels are one and the same.').
card_multiverse_id('flamecore elemental'/'TSP', '108870').

card_in_set('fledgling mawcor', 'TSP').
card_original_type('fledgling mawcor'/'TSP', 'Creature — Beast').
card_original_text('fledgling mawcor'/'TSP', 'Flying\n{T}: Fledgling Mawcor deals 1 damage to target creature or player.\nMorph {U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('fledgling mawcor', 'TSP').
card_image_name('fledgling mawcor'/'TSP', 'fledgling mawcor').
card_uid('fledgling mawcor'/'TSP', 'TSP:Fledgling Mawcor:fledgling mawcor').
card_rarity('fledgling mawcor'/'TSP', 'Uncommon').
card_artist('fledgling mawcor'/'TSP', 'Kev Walker').
card_number('fledgling mawcor'/'TSP', '63').
card_multiverse_id('fledgling mawcor'/'TSP', '110532').

card_in_set('flickering spirit', 'TSP').
card_original_type('flickering spirit'/'TSP', 'Creature — Spirit').
card_original_text('flickering spirit'/'TSP', 'Flying\n{3}{W}: Remove Flickering Spirit from the game, then return it to play under its owner\'s control.').
card_first_print('flickering spirit', 'TSP').
card_image_name('flickering spirit'/'TSP', 'flickering spirit').
card_uid('flickering spirit'/'TSP', 'TSP:Flickering Spirit:flickering spirit').
card_rarity('flickering spirit'/'TSP', 'Common').
card_artist('flickering spirit'/'TSP', 'Alex Horley-Orlandelli').
card_number('flickering spirit'/'TSP', '17').
card_flavor_text('flickering spirit'/'TSP', 'It disappears each time with the hope that it will not return to the same desolation it left behind.').
card_multiverse_id('flickering spirit'/'TSP', '108846').

card_in_set('flowstone channeler', 'TSP').
card_original_type('flowstone channeler'/'TSP', 'Creature — Human Spellshaper').
card_original_text('flowstone channeler'/'TSP', '{1}{R}, {T}, Discard a card: Target creature gets +1/-1 and gains haste until end of turn.').
card_first_print('flowstone channeler', 'TSP').
card_image_name('flowstone channeler'/'TSP', 'flowstone channeler').
card_uid('flowstone channeler'/'TSP', 'TSP:Flowstone Channeler:flowstone channeler').
card_rarity('flowstone channeler'/'TSP', 'Common').
card_artist('flowstone channeler'/'TSP', 'Alan Pollack').
card_number('flowstone channeler'/'TSP', '155').
card_flavor_text('flowstone channeler'/'TSP', 'With the evincars gone, flowstone became erratic and wild, a source of power for mages more desperate than wise.').
card_multiverse_id('flowstone channeler'/'TSP', '106653').

card_in_set('fool\'s demise', 'TSP').
card_original_type('fool\'s demise'/'TSP', 'Enchantment — Aura').
card_original_text('fool\'s demise'/'TSP', 'Enchant creature\nWhen enchanted creature is put into a graveyard, return that creature to play under your control.\nWhen Fool\'s Demise is put into a graveyard from play, return Fool\'s Demise to its owner\'s hand.').
card_first_print('fool\'s demise', 'TSP').
card_image_name('fool\'s demise'/'TSP', 'fool\'s demise').
card_uid('fool\'s demise'/'TSP', 'TSP:Fool\'s Demise:fool\'s demise').
card_rarity('fool\'s demise'/'TSP', 'Uncommon').
card_artist('fool\'s demise'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('fool\'s demise'/'TSP', '64').
card_multiverse_id('fool\'s demise'/'TSP', '126294').

card_in_set('forest', 'TSP').
card_original_type('forest'/'TSP', 'Basic Land — Forest').
card_original_text('forest'/'TSP', 'G').
card_image_name('forest'/'TSP', 'forest1').
card_uid('forest'/'TSP', 'TSP:Forest:forest1').
card_rarity('forest'/'TSP', 'Basic Land').
card_artist('forest'/'TSP', 'Rob Alexander').
card_number('forest'/'TSP', '298').
card_multiverse_id('forest'/'TSP', '122088').

card_in_set('forest', 'TSP').
card_original_type('forest'/'TSP', 'Basic Land — Forest').
card_original_text('forest'/'TSP', 'G').
card_image_name('forest'/'TSP', 'forest2').
card_uid('forest'/'TSP', 'TSP:Forest:forest2').
card_rarity('forest'/'TSP', 'Basic Land').
card_artist('forest'/'TSP', 'Vance Kovacs').
card_number('forest'/'TSP', '299').
card_multiverse_id('forest'/'TSP', '122083').

card_in_set('forest', 'TSP').
card_original_type('forest'/'TSP', 'Basic Land — Forest').
card_original_text('forest'/'TSP', 'G').
card_image_name('forest'/'TSP', 'forest3').
card_uid('forest'/'TSP', 'TSP:Forest:forest3').
card_rarity('forest'/'TSP', 'Basic Land').
card_artist('forest'/'TSP', 'Craig Mullins').
card_number('forest'/'TSP', '300').
card_multiverse_id('forest'/'TSP', '118927').

card_in_set('forest', 'TSP').
card_original_type('forest'/'TSP', 'Basic Land — Forest').
card_original_text('forest'/'TSP', 'G').
card_image_name('forest'/'TSP', 'forest4').
card_uid('forest'/'TSP', 'TSP:Forest:forest4').
card_rarity('forest'/'TSP', 'Basic Land').
card_artist('forest'/'TSP', 'Stephen Tappin').
card_number('forest'/'TSP', '301').
card_multiverse_id('forest'/'TSP', '118906').

card_in_set('foriysian interceptor', 'TSP').
card_original_type('foriysian interceptor'/'TSP', 'Creature — Human Soldier').
card_original_text('foriysian interceptor'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nDefender\nForiysian Interceptor can block an additional creature.').
card_first_print('foriysian interceptor', 'TSP').
card_image_name('foriysian interceptor'/'TSP', 'foriysian interceptor').
card_uid('foriysian interceptor'/'TSP', 'TSP:Foriysian Interceptor:foriysian interceptor').
card_rarity('foriysian interceptor'/'TSP', 'Common').
card_artist('foriysian interceptor'/'TSP', 'Anson Maddocks').
card_number('foriysian interceptor'/'TSP', '18').
card_flavor_text('foriysian interceptor'/'TSP', 'A focal point for time rifts, Foriys contends simultaneously with foes both new and old.').
card_multiverse_id('foriysian interceptor'/'TSP', '106652').

card_in_set('foriysian totem', 'TSP').
card_original_type('foriysian totem'/'TSP', 'Artifact').
card_original_text('foriysian totem'/'TSP', '{T}: Add {R} to your mana pool.\n{4}{R}: Foriysian Totem becomes a 4/4 red Giant artifact creature with trample until end of turn.\nAs long as Foriysian Totem is a creature, it can block an additional creature.').
card_first_print('foriysian totem', 'TSP').
card_image_name('foriysian totem'/'TSP', 'foriysian totem').
card_uid('foriysian totem'/'TSP', 'TSP:Foriysian Totem:foriysian totem').
card_rarity('foriysian totem'/'TSP', 'Uncommon').
card_artist('foriysian totem'/'TSP', 'Anson Maddocks').
card_number('foriysian totem'/'TSP', '254').
card_multiverse_id('foriysian totem'/'TSP', '108904').

card_in_set('fortify', 'TSP').
card_original_type('fortify'/'TSP', 'Instant').
card_original_text('fortify'/'TSP', 'Choose one Creatures you control get +2/+0 until end of turn; or creatures you control get +0/+2 until end of turn.').
card_first_print('fortify', 'TSP').
card_image_name('fortify'/'TSP', 'fortify').
card_uid('fortify'/'TSP', 'TSP:Fortify:fortify').
card_rarity('fortify'/'TSP', 'Common').
card_artist('fortify'/'TSP', 'Christopher Moeller').
card_number('fortify'/'TSP', '19').
card_flavor_text('fortify'/'TSP', '\"Where metal is tainted and wood is scarce, we are best armed by faith.\"\n—Tavalus, acolyte of Korlis').
card_multiverse_id('fortify'/'TSP', '116732').

card_in_set('fortune thief', 'TSP').
card_original_type('fortune thief'/'TSP', 'Creature — Human Rogue').
card_original_text('fortune thief'/'TSP', 'Damage that would reduce your life total to less than 1 reduces it to 1 instead.\nMorph {R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('fortune thief', 'TSP').
card_image_name('fortune thief'/'TSP', 'fortune thief').
card_uid('fortune thief'/'TSP', 'TSP:Fortune Thief:fortune thief').
card_rarity('fortune thief'/'TSP', 'Rare').
card_artist('fortune thief'/'TSP', 'Christopher Moeller').
card_number('fortune thief'/'TSP', '156').
card_multiverse_id('fortune thief'/'TSP', '126289').

card_in_set('fungal reaches', 'TSP').
card_original_type('fungal reaches'/'TSP', 'Land').
card_original_text('fungal reaches'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Fungal Reaches.\n{1}, Remove X storage counters from Fungal Reaches: Add X mana in any combination of {R} and/or {G} to your mana pool.').
card_first_print('fungal reaches', 'TSP').
card_image_name('fungal reaches'/'TSP', 'fungal reaches').
card_uid('fungal reaches'/'TSP', 'TSP:Fungal Reaches:fungal reaches').
card_rarity('fungal reaches'/'TSP', 'Uncommon').
card_artist('fungal reaches'/'TSP', 'Martina Pilcerova').
card_number('fungal reaches'/'TSP', '273').
card_multiverse_id('fungal reaches'/'TSP', '108796').

card_in_set('fungus sliver', 'TSP').
card_original_type('fungus sliver'/'TSP', 'Creature — Fungus Sliver').
card_original_text('fungus sliver'/'TSP', 'All Slivers have \"Whenever this creature is dealt damage, put a +1/+1 counter on it.\" (The damage is dealt before the counter is put on.)').
card_first_print('fungus sliver', 'TSP').
card_image_name('fungus sliver'/'TSP', 'fungus sliver').
card_uid('fungus sliver'/'TSP', 'TSP:Fungus Sliver:fungus sliver').
card_rarity('fungus sliver'/'TSP', 'Rare').
card_artist('fungus sliver'/'TSP', 'Daniel Gelon').
card_number('fungus sliver'/'TSP', '195').
card_flavor_text('fungus sliver'/'TSP', '\"When a sliver of this breed enters the hive, the others claw each other in frenzied fits, thereby ensuring their rapid growth.\"\n—Rukarumel, field journal').
card_multiverse_id('fungus sliver'/'TSP', '118881').

card_in_set('fury sliver', 'TSP').
card_original_type('fury sliver'/'TSP', 'Creature — Sliver').
card_original_text('fury sliver'/'TSP', 'All Slivers have double strike.').
card_first_print('fury sliver', 'TSP').
card_image_name('fury sliver'/'TSP', 'fury sliver').
card_uid('fury sliver'/'TSP', 'TSP:Fury Sliver:fury sliver').
card_rarity('fury sliver'/'TSP', 'Uncommon').
card_artist('fury sliver'/'TSP', 'Paolo Parente').
card_number('fury sliver'/'TSP', '157').
card_flavor_text('fury sliver'/'TSP', '\"A rift opened, and our arrows were abruptly stilled. To move was to push the world. But the sliver\'s claw still twitched, red wounds appeared in Thed\'s chest, and ribbons of blood hung in the air.\"\n—Adom Capashen, Benalish hero').
card_multiverse_id('fury sliver'/'TSP', '109722').

card_in_set('gauntlet of power', 'TSP').
card_original_type('gauntlet of power'/'TSP', 'Artifact').
card_original_text('gauntlet of power'/'TSP', 'As Gauntlet of Power comes into play, choose a color.\nCreatures of the chosen color get +1/+1.\nWhenever a basic land is tapped for mana of the chosen color, its controller adds one mana of that color to his or her mana pool.').
card_first_print('gauntlet of power', 'TSP').
card_image_name('gauntlet of power'/'TSP', 'gauntlet of power').
card_uid('gauntlet of power'/'TSP', 'TSP:Gauntlet of Power:gauntlet of power').
card_rarity('gauntlet of power'/'TSP', 'Rare').
card_artist('gauntlet of power'/'TSP', 'Greg Hildebrandt').
card_number('gauntlet of power'/'TSP', '255').
card_multiverse_id('gauntlet of power'/'TSP', '124220').

card_in_set('gaze of justice', 'TSP').
card_original_type('gaze of justice'/'TSP', 'Sorcery').
card_original_text('gaze of justice'/'TSP', 'As an additional cost to play Gaze of Justice, tap three untapped white creatures you control.\nRemove target creature from the game.\nFlashback {5}{W} (You may play this card from your graveyard for its flashback cost and any additional costs. Then remove it from the game.)').
card_first_print('gaze of justice', 'TSP').
card_image_name('gaze of justice'/'TSP', 'gaze of justice').
card_uid('gaze of justice'/'TSP', 'TSP:Gaze of Justice:gaze of justice').
card_rarity('gaze of justice'/'TSP', 'Common').
card_artist('gaze of justice'/'TSP', 'John Avon').
card_number('gaze of justice'/'TSP', '20').
card_multiverse_id('gaze of justice'/'TSP', '108819').

card_in_set('gemhide sliver', 'TSP').
card_original_type('gemhide sliver'/'TSP', 'Creature — Sliver').
card_original_text('gemhide sliver'/'TSP', 'All Slivers have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('gemhide sliver', 'TSP').
card_image_name('gemhide sliver'/'TSP', 'gemhide sliver').
card_uid('gemhide sliver'/'TSP', 'TSP:Gemhide Sliver:gemhide sliver').
card_rarity('gemhide sliver'/'TSP', 'Common').
card_artist('gemhide sliver'/'TSP', 'John Matson').
card_number('gemhide sliver'/'TSP', '196').
card_flavor_text('gemhide sliver'/'TSP', '\"The land is weary. Even Skyshroud is depleted. We must find another source of mana—one that is growing despite our withering world.\"\n—Freyalise').
card_multiverse_id('gemhide sliver'/'TSP', '118922').

card_in_set('gemstone caverns', 'TSP').
card_original_type('gemstone caverns'/'TSP', 'Legendary Land').
card_original_text('gemstone caverns'/'TSP', 'If Gemstone Caverns is in your opening hand and you\'re not playing first, you may begin the game with Gemstone Caverns in play with a luck counter on it. If you do, remove a card in your hand from the game.\n{T}: Add {1} to your mana pool. If Gemstone Caverns has a luck counter on it, instead add one mana of any color to your mana pool.').
card_first_print('gemstone caverns', 'TSP').
card_image_name('gemstone caverns'/'TSP', 'gemstone caverns').
card_uid('gemstone caverns'/'TSP', 'TSP:Gemstone Caverns:gemstone caverns').
card_rarity('gemstone caverns'/'TSP', 'Rare').
card_artist('gemstone caverns'/'TSP', 'Martina Pilcerova').
card_number('gemstone caverns'/'TSP', '274').
card_multiverse_id('gemstone caverns'/'TSP', '122094').

card_in_set('ghitu firebreathing', 'TSP').
card_original_type('ghitu firebreathing'/'TSP', 'Enchantment — Aura').
card_original_text('ghitu firebreathing'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.\n{R}: Return Ghitu Firebreathing to its owner\'s hand.').
card_first_print('ghitu firebreathing', 'TSP').
card_image_name('ghitu firebreathing'/'TSP', 'ghitu firebreathing').
card_uid('ghitu firebreathing'/'TSP', 'TSP:Ghitu Firebreathing:ghitu firebreathing').
card_rarity('ghitu firebreathing'/'TSP', 'Common').
card_artist('ghitu firebreathing'/'TSP', 'Jim Nelson').
card_number('ghitu firebreathing'/'TSP', '158').
card_multiverse_id('ghitu firebreathing'/'TSP', '108838').

card_in_set('ghostflame sliver', 'TSP').
card_original_type('ghostflame sliver'/'TSP', 'Creature — Sliver').
card_original_text('ghostflame sliver'/'TSP', 'All Slivers are colorless.').
card_first_print('ghostflame sliver', 'TSP').
card_image_name('ghostflame sliver'/'TSP', 'ghostflame sliver').
card_uid('ghostflame sliver'/'TSP', 'TSP:Ghostflame Sliver:ghostflame sliver').
card_rarity('ghostflame sliver'/'TSP', 'Uncommon').
card_artist('ghostflame sliver'/'TSP', 'Luca Zontini').
card_number('ghostflame sliver'/'TSP', '239').
card_flavor_text('ghostflame sliver'/'TSP', '\"This breed is on the cusp of evolution. It burns away the markings that connect it to a queen that no longer heeds its call. It seeks a new master. Perhaps I can give it one.\"\n—Freyalise').
card_multiverse_id('ghostflame sliver'/'TSP', '109717').

card_in_set('glass asp', 'TSP').
card_original_type('glass asp'/'TSP', 'Creature — Snake').
card_original_text('glass asp'/'TSP', 'Whenever Glass Asp deals combat damage to a player, that player loses 2 life at the beginning of his or her next draw step unless he or she pays {2} before that step.').
card_first_print('glass asp', 'TSP').
card_image_name('glass asp'/'TSP', 'glass asp').
card_uid('glass asp'/'TSP', 'TSP:Glass Asp:glass asp').
card_rarity('glass asp'/'TSP', 'Common').
card_artist('glass asp'/'TSP', 'Richard Kane Ferguson').
card_number('glass asp'/'TSP', '197').
card_flavor_text('glass asp'/'TSP', 'Its venom induces a fever that causes its victims to relive the attack in recurring, haunting visions.').
card_multiverse_id('glass asp'/'TSP', '113537').

card_in_set('goblin skycutter', 'TSP').
card_original_type('goblin skycutter'/'TSP', 'Creature — Goblin Warrior').
card_original_text('goblin skycutter'/'TSP', 'Sacrifice Goblin Skycutter: Goblin Skycutter deals 2 damage to target creature with flying. That creature loses flying until end of turn.').
card_first_print('goblin skycutter', 'TSP').
card_image_name('goblin skycutter'/'TSP', 'goblin skycutter').
card_uid('goblin skycutter'/'TSP', 'TSP:Goblin Skycutter:goblin skycutter').
card_rarity('goblin skycutter'/'TSP', 'Common').
card_artist('goblin skycutter'/'TSP', 'Clint Langley').
card_number('goblin skycutter'/'TSP', '159').
card_flavor_text('goblin skycutter'/'TSP', 'He takes out his fear of heights on anything above his head.').
card_multiverse_id('goblin skycutter'/'TSP', '124758').

card_in_set('gorgon recluse', 'TSP').
card_original_type('gorgon recluse'/'TSP', 'Creature — Gorgon').
card_original_text('gorgon recluse'/'TSP', 'Whenever Gorgon Recluse blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.\nMadness {B}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('gorgon recluse', 'TSP').
card_image_name('gorgon recluse'/'TSP', 'gorgon recluse').
card_uid('gorgon recluse'/'TSP', 'TSP:Gorgon Recluse:gorgon recluse').
card_rarity('gorgon recluse'/'TSP', 'Common').
card_artist('gorgon recluse'/'TSP', 'Darrell Riche').
card_number('gorgon recluse'/'TSP', '111').
card_multiverse_id('gorgon recluse'/'TSP', '118923').

card_in_set('grapeshot', 'TSP').
card_original_type('grapeshot'/'TSP', 'Sorcery').
card_original_text('grapeshot'/'TSP', 'Grapeshot deals 1 damage to target creature or player.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('grapeshot', 'TSP').
card_image_name('grapeshot'/'TSP', 'grapeshot').
card_uid('grapeshot'/'TSP', 'TSP:Grapeshot:grapeshot').
card_rarity('grapeshot'/'TSP', 'Common').
card_artist('grapeshot'/'TSP', 'Pete Venters').
card_number('grapeshot'/'TSP', '160').
card_flavor_text('grapeshot'/'TSP', 'Mages often seek to emulate the powerful relics lost to time and apocalypse.').
card_multiverse_id('grapeshot'/'TSP', '118882').

card_in_set('greater gargadon', 'TSP').
card_original_type('greater gargadon'/'TSP', 'Creature — Beast').
card_original_text('greater gargadon'/'TSP', 'Suspend 10—{R}\nSacrifice an artifact, creature, or land: Remove a time counter from Greater Gargadon. Play this ability only if Greater Gargadon is suspended.').
card_first_print('greater gargadon', 'TSP').
card_image_name('greater gargadon'/'TSP', 'greater gargadon').
card_uid('greater gargadon'/'TSP', 'TSP:Greater Gargadon:greater gargadon').
card_rarity('greater gargadon'/'TSP', 'Rare').
card_artist('greater gargadon'/'TSP', 'Rob Alexander').
card_number('greater gargadon'/'TSP', '161').
card_multiverse_id('greater gargadon'/'TSP', '111048').

card_in_set('greenseeker', 'TSP').
card_original_type('greenseeker'/'TSP', 'Creature — Elf Spellshaper').
card_original_text('greenseeker'/'TSP', '{G}, {T}, Discard a card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('greenseeker', 'TSP').
card_image_name('greenseeker'/'TSP', 'greenseeker').
card_uid('greenseeker'/'TSP', 'TSP:Greenseeker:greenseeker').
card_rarity('greenseeker'/'TSP', 'Common').
card_artist('greenseeker'/'TSP', 'Rebecca Guay').
card_number('greenseeker'/'TSP', '198').
card_flavor_text('greenseeker'/'TSP', '\"A rumor passes among the snakes, whispered by the rushes, inspiring the seekers: the goddess Freyalise is alive in Skyshroud.\"').
card_multiverse_id('greenseeker'/'TSP', '108830').

card_in_set('griffin guide', 'TSP').
card_original_type('griffin guide'/'TSP', 'Enchantment — Aura').
card_original_text('griffin guide'/'TSP', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\nWhen enchanted creature is put into a graveyard, put a 2/2 white Griffin creature token with flying into play.').
card_first_print('griffin guide', 'TSP').
card_image_name('griffin guide'/'TSP', 'griffin guide').
card_uid('griffin guide'/'TSP', 'TSP:Griffin Guide:griffin guide').
card_rarity('griffin guide'/'TSP', 'Uncommon').
card_artist('griffin guide'/'TSP', 'Jim Nelson').
card_number('griffin guide'/'TSP', '21').
card_multiverse_id('griffin guide'/'TSP', '110514').

card_in_set('ground rift', 'TSP').
card_original_type('ground rift'/'TSP', 'Sorcery').
card_original_text('ground rift'/'TSP', 'Target creature without flying can\'t block this turn.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('ground rift', 'TSP').
card_image_name('ground rift'/'TSP', 'ground rift').
card_uid('ground rift'/'TSP', 'TSP:Ground Rift:ground rift').
card_rarity('ground rift'/'TSP', 'Common').
card_artist('ground rift'/'TSP', 'Thomas M. Baxa').
card_number('ground rift'/'TSP', '162').
card_flavor_text('ground rift'/'TSP', 'Some time rifts didn\'t take away the people but just the ground they stood on.').
card_multiverse_id('ground rift'/'TSP', '108905').

card_in_set('gustcloak cavalier', 'TSP').
card_original_type('gustcloak cavalier'/'TSP', 'Creature — Human Knight').
card_original_text('gustcloak cavalier'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhenever Gustcloak Cavalier attacks, you may tap target creature.\nWhenever Gustcloak Cavalier becomes blocked, you may untap Gustcloak Cavalier and remove it from combat.').
card_first_print('gustcloak cavalier', 'TSP').
card_image_name('gustcloak cavalier'/'TSP', 'gustcloak cavalier').
card_uid('gustcloak cavalier'/'TSP', 'TSP:Gustcloak Cavalier:gustcloak cavalier').
card_rarity('gustcloak cavalier'/'TSP', 'Uncommon').
card_artist('gustcloak cavalier'/'TSP', 'Stuart Griffin').
card_number('gustcloak cavalier'/'TSP', '22').
card_multiverse_id('gustcloak cavalier'/'TSP', '108889').

card_in_set('harmonic sliver', 'TSP').
card_original_type('harmonic sliver'/'TSP', 'Creature — Sliver').
card_original_text('harmonic sliver'/'TSP', 'All Slivers have \"When this creature comes into play, destroy target artifact or enchantment.\"').
card_first_print('harmonic sliver', 'TSP').
card_image_name('harmonic sliver'/'TSP', 'harmonic sliver').
card_uid('harmonic sliver'/'TSP', 'TSP:Harmonic Sliver:harmonic sliver').
card_rarity('harmonic sliver'/'TSP', 'Uncommon').
card_artist('harmonic sliver'/'TSP', 'Luca Zontini').
card_number('harmonic sliver'/'TSP', '240').
card_flavor_text('harmonic sliver'/'TSP', '\"Since the last sliver arrived, even our mightiest relics splinter into shards as soon as we bring them against the hive.\"\n—Llanach, Skyshroud ranger').
card_multiverse_id('harmonic sliver'/'TSP', '109706').

card_in_set('haunting hymn', 'TSP').
card_original_type('haunting hymn'/'TSP', 'Instant').
card_original_text('haunting hymn'/'TSP', 'Target player discards two cards. If you played this spell during your main phase, that player discards four cards instead.').
card_first_print('haunting hymn', 'TSP').
card_image_name('haunting hymn'/'TSP', 'haunting hymn').
card_uid('haunting hymn'/'TSP', 'TSP:Haunting Hymn:haunting hymn').
card_rarity('haunting hymn'/'TSP', 'Uncommon').
card_artist('haunting hymn'/'TSP', 'rk post').
card_number('haunting hymn'/'TSP', '112').
card_flavor_text('haunting hymn'/'TSP', 'The hymn\'s melody has persisted since Tourach\'s time, but the words changed to invoke the phobias of each listener.').
card_multiverse_id('haunting hymn'/'TSP', '109731').

card_in_set('havenwood wurm', 'TSP').
card_original_type('havenwood wurm'/'TSP', 'Creature — Wurm').
card_original_text('havenwood wurm'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nTrample').
card_first_print('havenwood wurm', 'TSP').
card_image_name('havenwood wurm'/'TSP', 'havenwood wurm').
card_uid('havenwood wurm'/'TSP', 'TSP:Havenwood Wurm:havenwood wurm').
card_rarity('havenwood wurm'/'TSP', 'Common').
card_artist('havenwood wurm'/'TSP', 'Stuart Griffin').
card_number('havenwood wurm'/'TSP', '199').
card_flavor_text('havenwood wurm'/'TSP', 'Time rifts bring tunneling beasts of old into the hard, dry earth of the present. Most die there, trapped, but the mightiest burst through the surface.').
card_multiverse_id('havenwood wurm'/'TSP', '108808').

card_in_set('herd gnarr', 'TSP').
card_original_type('herd gnarr'/'TSP', 'Creature — Beast').
card_original_text('herd gnarr'/'TSP', 'Whenever another creature comes into play under your control, Herd Gnarr gets +2/+2 until end of turn.').
card_first_print('herd gnarr', 'TSP').
card_image_name('herd gnarr'/'TSP', 'herd gnarr').
card_uid('herd gnarr'/'TSP', 'TSP:Herd Gnarr:herd gnarr').
card_rarity('herd gnarr'/'TSP', 'Common').
card_artist('herd gnarr'/'TSP', 'Daren Bader').
card_number('herd gnarr'/'TSP', '200').
card_flavor_text('herd gnarr'/'TSP', 'Long ago, the solitary gnarr was a sign of good luck. Now they have become wild pack hunters, a sign of impending danger.').
card_multiverse_id('herd gnarr'/'TSP', '118873').

card_in_set('hivestone', 'TSP').
card_original_type('hivestone'/'TSP', 'Artifact').
card_original_text('hivestone'/'TSP', 'Creatures you control are Slivers in addition to their other creature types.').
card_first_print('hivestone', 'TSP').
card_image_name('hivestone'/'TSP', 'hivestone').
card_uid('hivestone'/'TSP', 'TSP:Hivestone:hivestone').
card_rarity('hivestone'/'TSP', 'Rare').
card_artist('hivestone'/'TSP', 'Dave Allsop').
card_number('hivestone'/'TSP', '256').
card_flavor_text('hivestone'/'TSP', 'Rath\'s evincars used hivestones to control the slivers, but the overlay put their power into unwitting hands. Now the stones make not masters, but slaves.').
card_multiverse_id('hivestone'/'TSP', '116387').

card_in_set('hypergenesis', 'TSP').
card_original_type('hypergenesis'/'TSP', 'Sorcery').
card_original_text('hypergenesis'/'TSP', 'Hypergenesis is green.\nSuspend 3—{1}{G}{G}\nStarting with you, each player may put an artifact, creature, enchantment, or land card from his or her hand into play. Repeat this process until no one puts a card into play.').
card_first_print('hypergenesis', 'TSP').
card_image_name('hypergenesis'/'TSP', 'hypergenesis').
card_uid('hypergenesis'/'TSP', 'TSP:Hypergenesis:hypergenesis').
card_rarity('hypergenesis'/'TSP', 'Rare').
card_artist('hypergenesis'/'TSP', 'Ron Spears').
card_number('hypergenesis'/'TSP', '201').
card_multiverse_id('hypergenesis'/'TSP', '113533').

card_in_set('ib halfheart, goblin tactician', 'TSP').
card_original_type('ib halfheart, goblin tactician'/'TSP', 'Legendary Creature — Goblin Advisor').
card_original_text('ib halfheart, goblin tactician'/'TSP', 'Whenever another Goblin you control becomes blocked, sacrifice it. If you do, it deals 4 damage to each creature blocking it.\nSacrifice two Mountains: Put two 1/1 red Goblin creature tokens into play.').
card_first_print('ib halfheart, goblin tactician', 'TSP').
card_image_name('ib halfheart, goblin tactician'/'TSP', 'ib halfheart, goblin tactician').
card_uid('ib halfheart, goblin tactician'/'TSP', 'TSP:Ib Halfheart, Goblin Tactician:ib halfheart, goblin tactician').
card_rarity('ib halfheart, goblin tactician'/'TSP', 'Rare').
card_artist('ib halfheart, goblin tactician'/'TSP', 'Wayne Reynolds').
card_number('ib halfheart, goblin tactician'/'TSP', '163').
card_flavor_text('ib halfheart, goblin tactician'/'TSP', '\"Everybody but me—CHARGE!\"').
card_multiverse_id('ib halfheart, goblin tactician'/'TSP', '114912').

card_in_set('icatian crier', 'TSP').
card_original_type('icatian crier'/'TSP', 'Creature — Human Spellshaper').
card_original_text('icatian crier'/'TSP', '{1}{W}, {T}, Discard a card: Put two 1/1 white Citizen creature tokens into play.').
card_first_print('icatian crier', 'TSP').
card_image_name('icatian crier'/'TSP', 'icatian crier').
card_uid('icatian crier'/'TSP', 'TSP:Icatian Crier:icatian crier').
card_rarity('icatian crier'/'TSP', 'Common').
card_artist('icatian crier'/'TSP', 'Michael Phillippi').
card_number('icatian crier'/'TSP', '23').
card_flavor_text('icatian crier'/'TSP', 'A thousand years removed from her home, her news of war had lost its context, but not its relevance.').
card_multiverse_id('icatian crier'/'TSP', '108900').

card_in_set('ignite memories', 'TSP').
card_original_type('ignite memories'/'TSP', 'Sorcery').
card_original_text('ignite memories'/'TSP', 'Target player reveals a card at random from his or her hand. Ignite Memories deals damage to that player equal to that card\'s converted mana cost.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('ignite memories', 'TSP').
card_image_name('ignite memories'/'TSP', 'ignite memories').
card_uid('ignite memories'/'TSP', 'TSP:Ignite Memories:ignite memories').
card_rarity('ignite memories'/'TSP', 'Uncommon').
card_artist('ignite memories'/'TSP', 'Justin Sweet').
card_number('ignite memories'/'TSP', '164').
card_multiverse_id('ignite memories'/'TSP', '109756').

card_in_set('ironclaw buzzardiers', 'TSP').
card_original_type('ironclaw buzzardiers'/'TSP', 'Creature — Orc Scout').
card_original_text('ironclaw buzzardiers'/'TSP', 'Ironclaw Buzzardiers can\'t block creatures with power 2 or greater.\n{R}: Ironclaw Buzzardiers gains flying until end of turn.').
card_first_print('ironclaw buzzardiers', 'TSP').
card_image_name('ironclaw buzzardiers'/'TSP', 'ironclaw buzzardiers').
card_uid('ironclaw buzzardiers'/'TSP', 'TSP:Ironclaw Buzzardiers:ironclaw buzzardiers').
card_rarity('ironclaw buzzardiers'/'TSP', 'Common').
card_artist('ironclaw buzzardiers'/'TSP', 'Carl Critchlow').
card_number('ironclaw buzzardiers'/'TSP', '165').
card_flavor_text('ironclaw buzzardiers'/'TSP', 'They warn the army of danger as they squawk past in swift retreat.').
card_multiverse_id('ironclaw buzzardiers'/'TSP', '116723').

card_in_set('island', 'TSP').
card_original_type('island'/'TSP', 'Basic Land — Island').
card_original_text('island'/'TSP', 'U').
card_image_name('island'/'TSP', 'island1').
card_uid('island'/'TSP', 'TSP:Island:island1').
card_rarity('island'/'TSP', 'Basic Land').
card_artist('island'/'TSP', 'Rob Alexander').
card_number('island'/'TSP', '286').
card_multiverse_id('island'/'TSP', '122095').

card_in_set('island', 'TSP').
card_original_type('island'/'TSP', 'Basic Land — Island').
card_original_text('island'/'TSP', 'U').
card_image_name('island'/'TSP', 'island2').
card_uid('island'/'TSP', 'TSP:Island:island2').
card_rarity('island'/'TSP', 'Basic Land').
card_artist('island'/'TSP', 'Jeremy Jarvis').
card_number('island'/'TSP', '287').
card_multiverse_id('island'/'TSP', '122077').

card_in_set('island', 'TSP').
card_original_type('island'/'TSP', 'Basic Land — Island').
card_original_text('island'/'TSP', 'U').
card_image_name('island'/'TSP', 'island3').
card_uid('island'/'TSP', 'TSP:Island:island3').
card_rarity('island'/'TSP', 'Basic Land').
card_artist('island'/'TSP', 'Craig Mullins').
card_number('island'/'TSP', '288').
card_multiverse_id('island'/'TSP', '122089').

card_in_set('island', 'TSP').
card_original_type('island'/'TSP', 'Basic Land — Island').
card_original_text('island'/'TSP', 'U').
card_image_name('island'/'TSP', 'island4').
card_uid('island'/'TSP', 'TSP:Island:island4').
card_rarity('island'/'TSP', 'Basic Land').
card_artist('island'/'TSP', 'Richard Wright').
card_number('island'/'TSP', '289').
card_multiverse_id('island'/'TSP', '118905').

card_in_set('ith, high arcanist', 'TSP').
card_original_type('ith, high arcanist'/'TSP', 'Legendary Creature — Human Wizard').
card_original_text('ith, high arcanist'/'TSP', 'Vigilance\n{T}: Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.\nSuspend 4—{W}{U}').
card_first_print('ith, high arcanist', 'TSP').
card_image_name('ith, high arcanist'/'TSP', 'ith, high arcanist').
card_uid('ith, high arcanist'/'TSP', 'TSP:Ith, High Arcanist:ith, high arcanist').
card_rarity('ith, high arcanist'/'TSP', 'Rare').
card_artist('ith, high arcanist'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('ith, high arcanist'/'TSP', '241').
card_multiverse_id('ith, high arcanist'/'TSP', '114903').

card_in_set('ivory giant', 'TSP').
card_original_type('ivory giant'/'TSP', 'Creature — Giant').
card_original_text('ivory giant'/'TSP', 'When Ivory Giant comes into play, tap all nonwhite creatures.\nSuspend 5—{W} (Rather than play this card from your hand, you may pay {W} and remove it from the game with five time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('ivory giant', 'TSP').
card_image_name('ivory giant'/'TSP', 'ivory giant').
card_uid('ivory giant'/'TSP', 'TSP:Ivory Giant:ivory giant').
card_rarity('ivory giant'/'TSP', 'Common').
card_artist('ivory giant'/'TSP', 'Jeff Miracola').
card_number('ivory giant'/'TSP', '24').
card_multiverse_id('ivory giant'/'TSP', '109741').

card_in_set('ixidron', 'TSP').
card_original_type('ixidron'/'TSP', 'Creature — Illusion').
card_original_text('ixidron'/'TSP', 'As Ixidron comes into play, turn all other nontoken creatures in play face down. They\'re 2/2 creatures.\nIxidron\'s power and toughness are each equal to the number of face-down creatures in play.').
card_first_print('ixidron', 'TSP').
card_image_name('ixidron'/'TSP', 'ixidron').
card_uid('ixidron'/'TSP', 'TSP:Ixidron:ixidron').
card_rarity('ixidron'/'TSP', 'Rare').
card_artist('ixidron'/'TSP', 'Terese Nielsen').
card_number('ixidron'/'TSP', '65').
card_multiverse_id('ixidron'/'TSP', '124313').

card_in_set('jaya ballard, task mage', 'TSP').
card_original_type('jaya ballard, task mage'/'TSP', 'Legendary Creature — Human Spellshaper').
card_original_text('jaya ballard, task mage'/'TSP', '{R}, {T}, Discard a card: Destroy target blue permanent.\n{1}{R}, {T}, Discard a card: Jaya Ballard, Task Mage deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.\n{5}{R}{R}, {T}, Discard a card: Jaya Ballard deals 6 damage to each creature and each player.').
card_image_name('jaya ballard, task mage'/'TSP', 'jaya ballard, task mage').
card_uid('jaya ballard, task mage'/'TSP', 'TSP:Jaya Ballard, Task Mage:jaya ballard, task mage').
card_rarity('jaya ballard, task mage'/'TSP', 'Rare').
card_artist('jaya ballard, task mage'/'TSP', 'Matt Cavotta').
card_number('jaya ballard, task mage'/'TSP', '166').
card_multiverse_id('jaya ballard, task mage'/'TSP', '109752').

card_in_set('jedit\'s dragoons', 'TSP').
card_original_type('jedit\'s dragoons'/'TSP', 'Creature — Cat Soldier').
card_original_text('jedit\'s dragoons'/'TSP', 'Vigilance\nWhen Jedit\'s Dragoons comes into play, you gain 4 life.').
card_first_print('jedit\'s dragoons', 'TSP').
card_image_name('jedit\'s dragoons'/'TSP', 'jedit\'s dragoons').
card_uid('jedit\'s dragoons'/'TSP', 'TSP:Jedit\'s Dragoons:jedit\'s dragoons').
card_rarity('jedit\'s dragoons'/'TSP', 'Common').
card_artist('jedit\'s dragoons'/'TSP', 'John Matson').
card_number('jedit\'s dragoons'/'TSP', '25').
card_flavor_text('jedit\'s dragoons'/'TSP', 'After Efrava was destroyed, the cat warriors scattered across Dominaria. Those who followed Jedit\'s example were strong enough to survive the ravages of apocalypse.').
card_multiverse_id('jedit\'s dragoons'/'TSP', '113571').

card_in_set('jhoira\'s timebug', 'TSP').
card_original_type('jhoira\'s timebug'/'TSP', 'Artifact Creature — Insect').
card_original_text('jhoira\'s timebug'/'TSP', '{T}: Choose target permanent you control or suspended card you own. If that permanent or card has a time counter on it, you may remove a time counter from it or put another time counter on it.').
card_first_print('jhoira\'s timebug', 'TSP').
card_image_name('jhoira\'s timebug'/'TSP', 'jhoira\'s timebug').
card_uid('jhoira\'s timebug'/'TSP', 'TSP:Jhoira\'s Timebug:jhoira\'s timebug').
card_rarity('jhoira\'s timebug'/'TSP', 'Common').
card_artist('jhoira\'s timebug'/'TSP', 'Dan Frazier').
card_number('jhoira\'s timebug'/'TSP', '257').
card_multiverse_id('jhoira\'s timebug'/'TSP', '118879').

card_in_set('kaervek the merciless', 'TSP').
card_original_type('kaervek the merciless'/'TSP', 'Legendary Creature — Human Shaman').
card_original_text('kaervek the merciless'/'TSP', 'Whenever an opponent plays a spell, Kaervek the Merciless deals damage to target creature or player equal to that spell\'s converted mana cost.').
card_first_print('kaervek the merciless', 'TSP').
card_image_name('kaervek the merciless'/'TSP', 'kaervek the merciless').
card_uid('kaervek the merciless'/'TSP', 'TSP:Kaervek the Merciless:kaervek the merciless').
card_rarity('kaervek the merciless'/'TSP', 'Rare').
card_artist('kaervek the merciless'/'TSP', 'rk post').
card_number('kaervek the merciless'/'TSP', '242').
card_flavor_text('kaervek the merciless'/'TSP', '\"Rats and jackals feast in his swath, but even they will not walk with him.\"\n—Mangara').
card_multiverse_id('kaervek the merciless'/'TSP', '113536').

card_in_set('keldon halberdier', 'TSP').
card_original_type('keldon halberdier'/'TSP', 'Creature — Human Warrior').
card_original_text('keldon halberdier'/'TSP', 'First strike\nSuspend 4—{R} (Rather than play this card from your hand, you may pay {R} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('keldon halberdier', 'TSP').
card_image_name('keldon halberdier'/'TSP', 'keldon halberdier').
card_uid('keldon halberdier'/'TSP', 'TSP:Keldon Halberdier:keldon halberdier').
card_rarity('keldon halberdier'/'TSP', 'Common').
card_artist('keldon halberdier'/'TSP', 'Paolo Parente').
card_number('keldon halberdier'/'TSP', '167').
card_multiverse_id('keldon halberdier'/'TSP', '108891').

card_in_set('kher keep', 'TSP').
card_original_type('kher keep'/'TSP', 'Legendary Land').
card_original_text('kher keep'/'TSP', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Put a 0/1 red Kobold creature token named Kobolds of Kher Keep into play.').
card_first_print('kher keep', 'TSP').
card_image_name('kher keep'/'TSP', 'kher keep').
card_uid('kher keep'/'TSP', 'TSP:Kher Keep:kher keep').
card_rarity('kher keep'/'TSP', 'Rare').
card_artist('kher keep'/'TSP', 'Paolo Parente').
card_number('kher keep'/'TSP', '275').
card_flavor_text('kher keep'/'TSP', '\"They\'re still here?! The cockroach may have finally met its match.\"\n—Teferi').
card_multiverse_id('kher keep'/'TSP', '113553').

card_in_set('knight of the holy nimbus', 'TSP').
card_original_type('knight of the holy nimbus'/'TSP', 'Creature — Human Rebel Knight').
card_original_text('knight of the holy nimbus'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nIf Knight of the Holy Nimbus would be destroyed, regenerate it.\n{2}: Knight of the Holy Nimbus can\'t be regenerated this turn. Only any opponent may play this ability.').
card_first_print('knight of the holy nimbus', 'TSP').
card_image_name('knight of the holy nimbus'/'TSP', 'knight of the holy nimbus').
card_uid('knight of the holy nimbus'/'TSP', 'TSP:Knight of the Holy Nimbus:knight of the holy nimbus').
card_rarity('knight of the holy nimbus'/'TSP', 'Uncommon').
card_artist('knight of the holy nimbus'/'TSP', 'Wayne England').
card_number('knight of the holy nimbus'/'TSP', '26').
card_multiverse_id('knight of the holy nimbus'/'TSP', '118907').

card_in_set('krosan grip', 'TSP').
card_original_type('krosan grip'/'TSP', 'Instant').
card_original_text('krosan grip'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nDestroy target artifact or enchantment.').
card_image_name('krosan grip'/'TSP', 'krosan grip').
card_uid('krosan grip'/'TSP', 'TSP:Krosan Grip:krosan grip').
card_rarity('krosan grip'/'TSP', 'Uncommon').
card_artist('krosan grip'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('krosan grip'/'TSP', '202').
card_flavor_text('krosan grip'/'TSP', '\"Give up these unnatural weapons, these scrolls. Heart and mind and fist are enough.\"\n—Zyd, Kamahlite druid').
card_multiverse_id('krosan grip'/'TSP', '126274').

card_in_set('liege of the pit', 'TSP').
card_original_type('liege of the pit'/'TSP', 'Creature — Demon').
card_original_text('liege of the pit'/'TSP', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Liege of the Pit. If you can\'t, Liege of the Pit deals 7 damage to you.\nMorph {B}{B}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('liege of the pit', 'TSP').
card_image_name('liege of the pit'/'TSP', 'liege of the pit').
card_uid('liege of the pit'/'TSP', 'TSP:Liege of the Pit:liege of the pit').
card_rarity('liege of the pit'/'TSP', 'Rare').
card_artist('liege of the pit'/'TSP', 'Jeremy Jarvis').
card_number('liege of the pit'/'TSP', '113').
card_multiverse_id('liege of the pit'/'TSP', '126285').

card_in_set('lightning axe', 'TSP').
card_original_type('lightning axe'/'TSP', 'Instant').
card_original_text('lightning axe'/'TSP', 'As an additional cost to play Lightning Axe, discard a card or pay {5}.\nLightning Axe deals 5 damage to target creature.').
card_first_print('lightning axe', 'TSP').
card_image_name('lightning axe'/'TSP', 'lightning axe').
card_uid('lightning axe'/'TSP', 'TSP:Lightning Axe:lightning axe').
card_rarity('lightning axe'/'TSP', 'Common').
card_artist('lightning axe'/'TSP', 'Dan Scott').
card_number('lightning axe'/'TSP', '168').
card_flavor_text('lightning axe'/'TSP', '\"A gargoyle\'s meat can be carved with an ordinary cleaver, but for its petrous hide . . .\"\n—Asmoranomardicadaistinaculdacar,\nThe Underworld Cookbook').
card_multiverse_id('lightning axe'/'TSP', '113567').

card_in_set('lim-dûl the necromancer', 'TSP').
card_original_type('lim-dûl the necromancer'/'TSP', 'Legendary Creature — Human Wizard').
card_original_text('lim-dûl the necromancer'/'TSP', 'Whenever a creature an opponent controls is put into a graveyard from play, you may pay {1}{B}. If you do, return that card to play under your control. If it\'s a creature, it\'s a Zombie in addition to its other creature types.\n{1}{B}: Regenerate target Zombie.').
card_first_print('lim-dûl the necromancer', 'TSP').
card_image_name('lim-dûl the necromancer'/'TSP', 'lim-dul the necromancer').
card_uid('lim-dûl the necromancer'/'TSP', 'TSP:Lim-Dûl the Necromancer:lim-dul the necromancer').
card_rarity('lim-dûl the necromancer'/'TSP', 'Rare').
card_artist('lim-dûl the necromancer'/'TSP', 'Matt Cavotta').
card_number('lim-dûl the necromancer'/'TSP', '114').
card_multiverse_id('lim-dûl the necromancer'/'TSP', '113522').

card_in_set('living end', 'TSP').
card_original_type('living end'/'TSP', 'Sorcery').
card_original_text('living end'/'TSP', 'Living End is black.\nSuspend 3—{2}{B}{B}\nEach player removes all creature cards in his or her graveyard from the game, then sacrifices all creatures he or she controls, then puts into play all cards he or she removed this way.').
card_first_print('living end', 'TSP').
card_image_name('living end'/'TSP', 'living end').
card_uid('living end'/'TSP', 'TSP:Living End:living end').
card_rarity('living end'/'TSP', 'Rare').
card_artist('living end'/'TSP', 'Greg Staples').
card_number('living end'/'TSP', '115').
card_multiverse_id('living end'/'TSP', '113521').

card_in_set('locket of yesterdays', 'TSP').
card_original_type('locket of yesterdays'/'TSP', 'Artifact').
card_original_text('locket of yesterdays'/'TSP', 'Spells you play cost {1} less to play for each card with the same name as that spell in your graveyard.').
card_first_print('locket of yesterdays', 'TSP').
card_image_name('locket of yesterdays'/'TSP', 'locket of yesterdays').
card_uid('locket of yesterdays'/'TSP', 'TSP:Locket of Yesterdays:locket of yesterdays').
card_rarity('locket of yesterdays'/'TSP', 'Uncommon').
card_artist('locket of yesterdays'/'TSP', 'Dany Orizio').
card_number('locket of yesterdays'/'TSP', '258').
card_flavor_text('locket of yesterdays'/'TSP', 'Every hour, at the turning of the glass, its owner had whispered to the locket, seeding it with secrets. This would be her legacy, her memory, if the rifts should take her.').
card_multiverse_id('locket of yesterdays'/'TSP', '113513').

card_in_set('looter il-kor', 'TSP').
card_original_type('looter il-kor'/'TSP', 'Creature — Kor Rogue').
card_original_text('looter il-kor'/'TSP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Looter il-Kor deals damage to an opponent, draw a card, then discard a card.').
card_first_print('looter il-kor', 'TSP').
card_image_name('looter il-kor'/'TSP', 'looter il-kor').
card_uid('looter il-kor'/'TSP', 'TSP:Looter il-Kor:looter il-kor').
card_rarity('looter il-kor'/'TSP', 'Common').
card_artist('looter il-kor'/'TSP', 'Mike Dringenberg').
card_number('looter il-kor'/'TSP', '66').
card_flavor_text('looter il-kor'/'TSP', 'Unable to touch items of value, he goes forth to steal secrets instead.').
card_multiverse_id('looter il-kor'/'TSP', '118918').

card_in_set('lotus bloom', 'TSP').
card_original_type('lotus bloom'/'TSP', 'Artifact').
card_original_text('lotus bloom'/'TSP', 'Suspend 3—{0} (Rather than play this card from your hand, pay {0} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)\n{T}, Sacrifice Lotus Bloom: Add three mana of any one color to your mana pool.').
card_image_name('lotus bloom'/'TSP', 'lotus bloom').
card_uid('lotus bloom'/'TSP', 'TSP:Lotus Bloom:lotus bloom').
card_rarity('lotus bloom'/'TSP', 'Rare').
card_artist('lotus bloom'/'TSP', 'Mark Zug').
card_number('lotus bloom'/'TSP', '259').
card_multiverse_id('lotus bloom'/'TSP', '114904').

card_in_set('magus of the candelabra', 'TSP').
card_original_type('magus of the candelabra'/'TSP', 'Creature — Human Wizard').
card_original_text('magus of the candelabra'/'TSP', '{X}, {T}: Untap X target lands.').
card_first_print('magus of the candelabra', 'TSP').
card_image_name('magus of the candelabra'/'TSP', 'magus of the candelabra').
card_uid('magus of the candelabra'/'TSP', 'TSP:Magus of the Candelabra:magus of the candelabra').
card_rarity('magus of the candelabra'/'TSP', 'Rare').
card_artist('magus of the candelabra'/'TSP', 'Terese Nielsen').
card_number('magus of the candelabra'/'TSP', '203').
card_flavor_text('magus of the candelabra'/'TSP', '\"What would Urza think, I wonder, if he could hear how the nature mages still snarl at his name, but venerate the work of his apprentice?\"\n—Teferi, to Jhoira').
card_multiverse_id('magus of the candelabra'/'TSP', '126273').

card_in_set('magus of the disk', 'TSP').
card_original_type('magus of the disk'/'TSP', 'Creature — Human Wizard').
card_original_text('magus of the disk'/'TSP', 'Magus of the Disk comes into play tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_first_print('magus of the disk', 'TSP').
card_image_name('magus of the disk'/'TSP', 'magus of the disk').
card_uid('magus of the disk'/'TSP', 'TSP:Magus of the Disk:magus of the disk').
card_rarity('magus of the disk'/'TSP', 'Rare').
card_artist('magus of the disk'/'TSP', 'Jeremy Jarvis').
card_number('magus of the disk'/'TSP', '27').
card_flavor_text('magus of the disk'/'TSP', 'Studying the journals of the necromage Nevinyrral leaves disciples gripped with the urge to exercise his draconian judgment.').
card_multiverse_id('magus of the disk'/'TSP', '126298').

card_in_set('magus of the jar', 'TSP').
card_original_type('magus of the jar'/'TSP', 'Creature — Human Wizard').
card_original_text('magus of the jar'/'TSP', '{T}, Sacrifice Magus of the Jar: Each player removes his or her hand from the game face down and draws seven cards. At end of turn, each player discards his or her hand and returns to his or her hand each card he or she removed from the game this way.').
card_first_print('magus of the jar', 'TSP').
card_image_name('magus of the jar'/'TSP', 'magus of the jar').
card_uid('magus of the jar'/'TSP', 'TSP:Magus of the Jar:magus of the jar').
card_rarity('magus of the jar'/'TSP', 'Rare').
card_artist('magus of the jar'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('magus of the jar'/'TSP', '67').
card_multiverse_id('magus of the jar'/'TSP', '126287').

card_in_set('magus of the mirror', 'TSP').
card_original_type('magus of the mirror'/'TSP', 'Creature — Human Wizard').
card_original_text('magus of the mirror'/'TSP', '{T}, Sacrifice Magus of the Mirror: Exchange life totals with target opponent. Play this ability only during your upkeep.').
card_first_print('magus of the mirror', 'TSP').
card_image_name('magus of the mirror'/'TSP', 'magus of the mirror').
card_uid('magus of the mirror'/'TSP', 'TSP:Magus of the Mirror:magus of the mirror').
card_rarity('magus of the mirror'/'TSP', 'Rare').
card_artist('magus of the mirror'/'TSP', 'Christopher Moeller').
card_number('magus of the mirror'/'TSP', '116').
card_flavor_text('magus of the mirror'/'TSP', '\"Behold! The image of the enemy and all that she has. Trust your envy, and take it.\"').
card_multiverse_id('magus of the mirror'/'TSP', '126278').

card_in_set('magus of the scroll', 'TSP').
card_original_type('magus of the scroll'/'TSP', 'Creature — Human Wizard').
card_original_text('magus of the scroll'/'TSP', '{3}, {T}: Name a card. Reveal a card at random from your hand. If it\'s the named card, Magus of the Scroll deals 2 damage to target creature or player.').
card_first_print('magus of the scroll', 'TSP').
card_image_name('magus of the scroll'/'TSP', 'magus of the scroll').
card_uid('magus of the scroll'/'TSP', 'TSP:Magus of the Scroll:magus of the scroll').
card_rarity('magus of the scroll'/'TSP', 'Rare').
card_artist('magus of the scroll'/'TSP', 'Greg Staples').
card_number('magus of the scroll'/'TSP', '169').
card_multiverse_id('magus of the scroll'/'TSP', '126279').

card_in_set('mana skimmer', 'TSP').
card_original_type('mana skimmer'/'TSP', 'Creature — Leech').
card_original_text('mana skimmer'/'TSP', 'Flying\nWhenever Mana Skimmer deals damage to a player, tap target land that player controls. That land doesn\'t untap during its controller\'s next untap step.').
card_first_print('mana skimmer', 'TSP').
card_image_name('mana skimmer'/'TSP', 'mana skimmer').
card_uid('mana skimmer'/'TSP', 'TSP:Mana Skimmer:mana skimmer').
card_rarity('mana skimmer'/'TSP', 'Common').
card_artist('mana skimmer'/'TSP', 'Daniel Gelon').
card_number('mana skimmer'/'TSP', '117').
card_flavor_text('mana skimmer'/'TSP', 'They traverse the mana-starved wastes in search of richer lands.').
card_multiverse_id('mana skimmer'/'TSP', '106658').

card_in_set('mangara of corondor', 'TSP').
card_original_type('mangara of corondor'/'TSP', 'Legendary Creature — Human Wizard').
card_original_text('mangara of corondor'/'TSP', '{T}: Remove Mangara of Corondor and target permanent from the game.').
card_first_print('mangara of corondor', 'TSP').
card_image_name('mangara of corondor'/'TSP', 'mangara of corondor').
card_uid('mangara of corondor'/'TSP', 'TSP:Mangara of Corondor:mangara of corondor').
card_rarity('mangara of corondor'/'TSP', 'Rare').
card_artist('mangara of corondor'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('mangara of corondor'/'TSP', '28').
card_flavor_text('mangara of corondor'/'TSP', '\"I have been brought to this place and I cannot leave. I may be free of the amber, but I am still in prison.\"').
card_multiverse_id('mangara of corondor'/'TSP', '113563').

card_in_set('might of old krosa', 'TSP').
card_original_type('might of old krosa'/'TSP', 'Instant').
card_original_text('might of old krosa'/'TSP', 'Target creature gets +2/+2 until end of turn. If you played this spell during your main phase, that creature gets +4/+4 until end of turn instead.').
card_first_print('might of old krosa', 'TSP').
card_image_name('might of old krosa'/'TSP', 'might of old krosa').
card_uid('might of old krosa'/'TSP', 'TSP:Might of Old Krosa:might of old krosa').
card_rarity('might of old krosa'/'TSP', 'Uncommon').
card_artist('might of old krosa'/'TSP', 'Una Fricker').
card_number('might of old krosa'/'TSP', '204').
card_flavor_text('might of old krosa'/'TSP', 'Sometimes even humble forest creatures were gifted with mighty emanations from the past.').
card_multiverse_id('might of old krosa'/'TSP', '109763').

card_in_set('might sliver', 'TSP').
card_original_type('might sliver'/'TSP', 'Creature — Sliver').
card_original_text('might sliver'/'TSP', 'All Slivers get +2/+2.').
card_first_print('might sliver', 'TSP').
card_image_name('might sliver'/'TSP', 'might sliver').
card_uid('might sliver'/'TSP', 'TSP:Might Sliver:might sliver').
card_rarity('might sliver'/'TSP', 'Uncommon').
card_artist('might sliver'/'TSP', 'Jeff Miracola').
card_number('might sliver'/'TSP', '205').
card_flavor_text('might sliver'/'TSP', '\"The colossal thing rumbled over the ridge, tree husks crumbling before it. The ones we were already fighting howled as it came, their muscles suddenly surging, and we knew it was time to flee.\"\n—Llanach, Skyshroud ranger').
card_multiverse_id('might sliver'/'TSP', '111042').

card_in_set('mindlash sliver', 'TSP').
card_original_type('mindlash sliver'/'TSP', 'Creature — Sliver').
card_original_text('mindlash sliver'/'TSP', 'All Slivers have \"{1}, Sacrifice this creature: Each player discards a card.\"').
card_first_print('mindlash sliver', 'TSP').
card_image_name('mindlash sliver'/'TSP', 'mindlash sliver').
card_uid('mindlash sliver'/'TSP', 'TSP:Mindlash Sliver:mindlash sliver').
card_rarity('mindlash sliver'/'TSP', 'Common').
card_artist('mindlash sliver'/'TSP', 'Jeff Miracola').
card_number('mindlash sliver'/'TSP', '118').
card_flavor_text('mindlash sliver'/'TSP', 'Though Dominaria\'s queenless slivers lack a single purpose, in some the instinct for self-sacrifice remains extremely strong.').
card_multiverse_id('mindlash sliver'/'TSP', '118867').

card_in_set('mindstab', 'TSP').
card_original_type('mindstab'/'TSP', 'Sorcery').
card_original_text('mindstab'/'TSP', 'Target player discards three cards.\nSuspend 4—{B} (Rather than play this card from your hand, you may pay {B} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_first_print('mindstab', 'TSP').
card_image_name('mindstab'/'TSP', 'mindstab').
card_uid('mindstab'/'TSP', 'TSP:Mindstab:mindstab').
card_rarity('mindstab'/'TSP', 'Common').
card_artist('mindstab'/'TSP', 'Mark Tedin').
card_number('mindstab'/'TSP', '119').
card_multiverse_id('mindstab'/'TSP', '108894').

card_in_set('mishra, artificer prodigy', 'TSP').
card_original_type('mishra, artificer prodigy'/'TSP', 'Legendary Creature — Human Artificer').
card_original_text('mishra, artificer prodigy'/'TSP', 'Whenever you play an artifact spell, you may search your graveyard, hand, and/or library for a card with the same name as that spell and put it into play. If you search your library this way, shuffle it.').
card_first_print('mishra, artificer prodigy', 'TSP').
card_image_name('mishra, artificer prodigy'/'TSP', 'mishra, artificer prodigy').
card_uid('mishra, artificer prodigy'/'TSP', 'TSP:Mishra, Artificer Prodigy:mishra, artificer prodigy').
card_rarity('mishra, artificer prodigy'/'TSP', 'Rare').
card_artist('mishra, artificer prodigy'/'TSP', 'Scott M. Fischer').
card_number('mishra, artificer prodigy'/'TSP', '243').
card_flavor_text('mishra, artificer prodigy'/'TSP', 'A sojourn through time gave dark inspiration to one gifted young mind.').
card_multiverse_id('mishra, artificer prodigy'/'TSP', '113539').

card_in_set('mogg war marshal', 'TSP').
card_original_type('mogg war marshal'/'TSP', 'Creature — Goblin Warrior').
card_original_text('mogg war marshal'/'TSP', 'Echo {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Mogg War Marshal comes into play or is put into a graveyard from play, put a 1/1 red Goblin creature token into play.').
card_first_print('mogg war marshal', 'TSP').
card_image_name('mogg war marshal'/'TSP', 'mogg war marshal').
card_uid('mogg war marshal'/'TSP', 'TSP:Mogg War Marshal:mogg war marshal').
card_rarity('mogg war marshal'/'TSP', 'Common').
card_artist('mogg war marshal'/'TSP', 'Wayne England').
card_number('mogg war marshal'/'TSP', '170').
card_multiverse_id('mogg war marshal'/'TSP', '116739').

card_in_set('molder', 'TSP').
card_original_type('molder'/'TSP', 'Instant').
card_original_text('molder'/'TSP', 'Destroy target artifact or enchantment with converted mana cost X. It can\'t be regenerated. You gain X life.').
card_first_print('molder', 'TSP').
card_image_name('molder'/'TSP', 'molder').
card_uid('molder'/'TSP', 'TSP:Molder:molder').
card_rarity('molder'/'TSP', 'Common').
card_artist('molder'/'TSP', 'Greg Hildebrandt').
card_number('molder'/'TSP', '206').
card_flavor_text('molder'/'TSP', '\"The forests have succumbed to ruin, but the spirit of Krosa remains alive and vital in these devouring spores.\"\n—Zyd, Kamahlite druid').
card_multiverse_id('molder'/'TSP', '114914').

card_in_set('molten slagheap', 'TSP').
card_original_type('molten slagheap'/'TSP', 'Land').
card_original_text('molten slagheap'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Molten Slagheap.\n{1}, Remove X storage counters from Molten Slagheap: Add X mana in any combination of {B} and/or {R} to your mana pool.').
card_first_print('molten slagheap', 'TSP').
card_image_name('molten slagheap'/'TSP', 'molten slagheap').
card_uid('molten slagheap'/'TSP', 'TSP:Molten Slagheap:molten slagheap').
card_rarity('molten slagheap'/'TSP', 'Uncommon').
card_artist('molten slagheap'/'TSP', 'Daren Bader').
card_number('molten slagheap'/'TSP', '276').
card_multiverse_id('molten slagheap'/'TSP', '108860').

card_in_set('momentary blink', 'TSP').
card_original_type('momentary blink'/'TSP', 'Instant').
card_original_text('momentary blink'/'TSP', 'Remove target creature you control from the game, then return it to play under its owner\'s control.\nFlashback {3}{U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('momentary blink', 'TSP').
card_image_name('momentary blink'/'TSP', 'momentary blink').
card_uid('momentary blink'/'TSP', 'TSP:Momentary Blink:momentary blink').
card_rarity('momentary blink'/'TSP', 'Common').
card_artist('momentary blink'/'TSP', 'Anthony S. Waters').
card_number('momentary blink'/'TSP', '29').
card_multiverse_id('momentary blink'/'TSP', '109733').

card_in_set('moonlace', 'TSP').
card_original_type('moonlace'/'TSP', 'Instant').
card_original_text('moonlace'/'TSP', 'Target spell or permanent becomes colorless.').
card_first_print('moonlace', 'TSP').
card_image_name('moonlace'/'TSP', 'moonlace').
card_uid('moonlace'/'TSP', 'TSP:Moonlace:moonlace').
card_rarity('moonlace'/'TSP', 'Rare').
card_artist('moonlace'/'TSP', 'Mike Dringenberg').
card_number('moonlace'/'TSP', '68').
card_flavor_text('moonlace'/'TSP', 'Once a vision of constancy in the sky, the moon had long been hidden from view by the haze that chokes the heavens. The very sight of it had become a sign that change was in the air.').
card_multiverse_id('moonlace'/'TSP', '116391').

card_in_set('mountain', 'TSP').
card_original_type('mountain'/'TSP', 'Basic Land — Mountain').
card_original_text('mountain'/'TSP', 'R').
card_image_name('mountain'/'TSP', 'mountain1').
card_uid('mountain'/'TSP', 'TSP:Mountain:mountain1').
card_rarity('mountain'/'TSP', 'Basic Land').
card_artist('mountain'/'TSP', 'John Avon').
card_number('mountain'/'TSP', '294').
card_multiverse_id('mountain'/'TSP', '118921').

card_in_set('mountain', 'TSP').
card_original_type('mountain'/'TSP', 'Basic Land — Mountain').
card_original_text('mountain'/'TSP', 'R').
card_image_name('mountain'/'TSP', 'mountain2').
card_uid('mountain'/'TSP', 'TSP:Mountain:mountain2').
card_rarity('mountain'/'TSP', 'Basic Land').
card_artist('mountain'/'TSP', 'D. Alexander Gregory').
card_number('mountain'/'TSP', '295').
card_multiverse_id('mountain'/'TSP', '122090').

card_in_set('mountain', 'TSP').
card_original_type('mountain'/'TSP', 'Basic Land — Mountain').
card_original_text('mountain'/'TSP', 'R').
card_image_name('mountain'/'TSP', 'mountain3').
card_uid('mountain'/'TSP', 'TSP:Mountain:mountain3').
card_rarity('mountain'/'TSP', 'Basic Land').
card_artist('mountain'/'TSP', 'Craig Mullins').
card_number('mountain'/'TSP', '296').
card_multiverse_id('mountain'/'TSP', '122080').

card_in_set('mountain', 'TSP').
card_original_type('mountain'/'TSP', 'Basic Land — Mountain').
card_original_text('mountain'/'TSP', 'R').
card_image_name('mountain'/'TSP', 'mountain4').
card_uid('mountain'/'TSP', 'TSP:Mountain:mountain4').
card_rarity('mountain'/'TSP', 'Basic Land').
card_artist('mountain'/'TSP', 'Greg Staples').
card_number('mountain'/'TSP', '297').
card_multiverse_id('mountain'/'TSP', '118926').

card_in_set('mwonvuli acid-moss', 'TSP').
card_original_type('mwonvuli acid-moss'/'TSP', 'Sorcery').
card_original_text('mwonvuli acid-moss'/'TSP', 'Destroy target land. Search your library for a Forest card and put that card into play tapped. Then shuffle your library.').
card_first_print('mwonvuli acid-moss', 'TSP').
card_image_name('mwonvuli acid-moss'/'TSP', 'mwonvuli acid-moss').
card_uid('mwonvuli acid-moss'/'TSP', 'TSP:Mwonvuli Acid-Moss:mwonvuli acid-moss').
card_rarity('mwonvuli acid-moss'/'TSP', 'Common').
card_artist('mwonvuli acid-moss'/'TSP', 'Randy Gallegos').
card_number('mwonvuli acid-moss'/'TSP', '207').
card_flavor_text('mwonvuli acid-moss'/'TSP', '\"Life gives way to death, and death to new life. Nature feeds upon her own decay.\"\n—Ezrith, druid of the Dark Hours').
card_multiverse_id('mwonvuli acid-moss'/'TSP', '118888').

card_in_set('mystical teachings', 'TSP').
card_original_type('mystical teachings'/'TSP', 'Instant').
card_original_text('mystical teachings'/'TSP', 'Search your library for an instant card or a card with flash, reveal it, and put it into your hand. Then shuffle your library.\nFlashback {5}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('mystical teachings', 'TSP').
card_image_name('mystical teachings'/'TSP', 'mystical teachings').
card_uid('mystical teachings'/'TSP', 'TSP:Mystical Teachings:mystical teachings').
card_rarity('mystical teachings'/'TSP', 'Common').
card_artist('mystical teachings'/'TSP', 'Ron Spears').
card_number('mystical teachings'/'TSP', '69').
card_multiverse_id('mystical teachings'/'TSP', '111058').

card_in_set('nantuko shaman', 'TSP').
card_original_type('nantuko shaman'/'TSP', 'Creature — Insect Shaman').
card_original_text('nantuko shaman'/'TSP', 'When Nantuko Shaman comes into play, if you control no tapped lands, draw a card.\nSuspend 1—{2}{G}{G} (Rather than play this card from your hand, you may pay {2}{G}{G} and remove it from the game with a time counter on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('nantuko shaman', 'TSP').
card_image_name('nantuko shaman'/'TSP', 'nantuko shaman').
card_uid('nantuko shaman'/'TSP', 'TSP:Nantuko Shaman:nantuko shaman').
card_rarity('nantuko shaman'/'TSP', 'Common').
card_artist('nantuko shaman'/'TSP', 'Daren Bader').
card_number('nantuko shaman'/'TSP', '208').
card_multiverse_id('nantuko shaman'/'TSP', '109726').

card_in_set('nether traitor', 'TSP').
card_original_type('nether traitor'/'TSP', 'Creature — Spirit').
card_original_text('nether traitor'/'TSP', 'Haste\nShadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever another creature is put into your graveyard from play, you may pay {B}. If you do, return Nether Traitor from your graveyard to play.').
card_first_print('nether traitor', 'TSP').
card_image_name('nether traitor'/'TSP', 'nether traitor').
card_uid('nether traitor'/'TSP', 'TSP:Nether Traitor:nether traitor').
card_rarity('nether traitor'/'TSP', 'Rare').
card_artist('nether traitor'/'TSP', 'Vance Kovacs').
card_number('nether traitor'/'TSP', '120').
card_multiverse_id('nether traitor'/'TSP', '116742').

card_in_set('nightshade assassin', 'TSP').
card_original_type('nightshade assassin'/'TSP', 'Creature — Human Assassin').
card_original_text('nightshade assassin'/'TSP', 'First strike\nWhen Nightshade Assassin comes into play, you may reveal X black cards in your hand. If you do, target creature gets -X/-X until end of turn.\nMadness {1}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('nightshade assassin', 'TSP').
card_image_name('nightshade assassin'/'TSP', 'nightshade assassin').
card_uid('nightshade assassin'/'TSP', 'TSP:Nightshade Assassin:nightshade assassin').
card_rarity('nightshade assassin'/'TSP', 'Uncommon').
card_artist('nightshade assassin'/'TSP', 'Alan Pollack').
card_number('nightshade assassin'/'TSP', '121').
card_multiverse_id('nightshade assassin'/'TSP', '111079').

card_in_set('norin the wary', 'TSP').
card_original_type('norin the wary'/'TSP', 'Legendary Creature — Human Warrior').
card_original_text('norin the wary'/'TSP', 'When a player plays a spell or a creature attacks, remove Norin the Wary from the game. Return it to play under its owner\'s control at end of turn.').
card_first_print('norin the wary', 'TSP').
card_image_name('norin the wary'/'TSP', 'norin the wary').
card_uid('norin the wary'/'TSP', 'TSP:Norin the Wary:norin the wary').
card_rarity('norin the wary'/'TSP', 'Rare').
card_artist('norin the wary'/'TSP', 'Heather Hudson').
card_number('norin the wary'/'TSP', '171').
card_flavor_text('norin the wary'/'TSP', '\"I have a bad feeling about this.\"').
card_multiverse_id('norin the wary'/'TSP', '113512').

card_in_set('opal guardian', 'TSP').
card_original_type('opal guardian'/'TSP', 'Enchantment').
card_original_text('opal guardian'/'TSP', 'When an opponent plays a creature spell, if Opal Guardian is an enchantment, Opal Guardian becomes a 3/4 Gargoyle creature with flying and protection from red.').
card_first_print('opal guardian', 'TSP').
card_image_name('opal guardian'/'TSP', 'opal guardian').
card_uid('opal guardian'/'TSP', 'TSP:Opal Guardian:opal guardian').
card_rarity('opal guardian'/'TSP', 'Rare').
card_artist('opal guardian'/'TSP', 'Christopher Rush').
card_number('opal guardian'/'TSP', '30').
card_flavor_text('opal guardian'/'TSP', 'It was a moment in time, cast in stone—a moment whose time had come again.').
card_multiverse_id('opal guardian'/'TSP', '116747').

card_in_set('opaline sliver', 'TSP').
card_original_type('opaline sliver'/'TSP', 'Creature — Sliver').
card_original_text('opaline sliver'/'TSP', 'All Slivers have \"Whenever this creature becomes the target of a spell an opponent controls, you may draw a card.\"').
card_first_print('opaline sliver', 'TSP').
card_image_name('opaline sliver'/'TSP', 'opaline sliver').
card_uid('opaline sliver'/'TSP', 'TSP:Opaline Sliver:opaline sliver').
card_rarity('opaline sliver'/'TSP', 'Uncommon').
card_artist('opaline sliver'/'TSP', 'Dave Dorman').
card_number('opaline sliver'/'TSP', '244').
card_flavor_text('opaline sliver'/'TSP', '\"When struck, its hide shimmers through sequences of color—a signaling language I am eager to unravel.\"\n—Rukarumel, field journal').
card_multiverse_id('opaline sliver'/'TSP', '109711').

card_in_set('ophidian eye', 'TSP').
card_original_type('ophidian eye'/'TSP', 'Enchantment — Aura').
card_original_text('ophidian eye'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_first_print('ophidian eye', 'TSP').
card_image_name('ophidian eye'/'TSP', 'ophidian eye').
card_uid('ophidian eye'/'TSP', 'TSP:Ophidian Eye:ophidian eye').
card_rarity('ophidian eye'/'TSP', 'Common').
card_artist('ophidian eye'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('ophidian eye'/'TSP', '70').
card_multiverse_id('ophidian eye'/'TSP', '108795').

card_in_set('orcish cannonade', 'TSP').
card_original_type('orcish cannonade'/'TSP', 'Instant').
card_original_text('orcish cannonade'/'TSP', 'Orcish Cannonade deals 2 damage to target creature or player and 3 damage to you.\nDraw a card.').
card_first_print('orcish cannonade', 'TSP').
card_image_name('orcish cannonade'/'TSP', 'orcish cannonade').
card_uid('orcish cannonade'/'TSP', 'TSP:Orcish Cannonade:orcish cannonade').
card_rarity('orcish cannonade'/'TSP', 'Common').
card_artist('orcish cannonade'/'TSP', 'Pete Venters').
card_number('orcish cannonade'/'TSP', '172').
card_flavor_text('orcish cannonade'/'TSP', '\"Crispy! Scarback! Load another volcano-ball.\"\n—Stumphobbler Thuj, Orcish captain').
card_multiverse_id('orcish cannonade'/'TSP', '114902').

card_in_set('outrider en-kor', 'TSP').
card_original_type('outrider en-kor'/'TSP', 'Creature — Kor Rebel Knight').
card_original_text('outrider en-kor'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{0}: The next 1 damage that would be dealt to Outrider en-Kor this turn is dealt to target creature you control instead.').
card_first_print('outrider en-kor', 'TSP').
card_image_name('outrider en-kor'/'TSP', 'outrider en-kor').
card_uid('outrider en-kor'/'TSP', 'TSP:Outrider en-Kor:outrider en-kor').
card_rarity('outrider en-kor'/'TSP', 'Uncommon').
card_artist('outrider en-kor'/'TSP', 'D. Alexander Gregory').
card_number('outrider en-kor'/'TSP', '31').
card_multiverse_id('outrider en-kor'/'TSP', '118902').

card_in_set('paradise plume', 'TSP').
card_original_type('paradise plume'/'TSP', 'Artifact').
card_original_text('paradise plume'/'TSP', 'As Paradise Plume comes into play, choose a color.\nWhenever a player plays a spell of the chosen color, you may gain 1 life.\n{T}: Add one mana of the chosen color to your mana pool.').
card_first_print('paradise plume', 'TSP').
card_image_name('paradise plume'/'TSP', 'paradise plume').
card_uid('paradise plume'/'TSP', 'TSP:Paradise Plume:paradise plume').
card_rarity('paradise plume'/'TSP', 'Uncommon').
card_artist('paradise plume'/'TSP', 'Wayne England').
card_number('paradise plume'/'TSP', '260').
card_flavor_text('paradise plume'/'TSP', 'A last wisp of paradise in a fallen world.').
card_multiverse_id('paradise plume'/'TSP', '122091').

card_in_set('paradox haze', 'TSP').
card_original_type('paradox haze'/'TSP', 'Enchantment — Aura').
card_original_text('paradox haze'/'TSP', 'Enchant player\nAt the beginning of enchanted player\'s first upkeep each turn, that player gets an additional upkeep step after this step.').
card_first_print('paradox haze', 'TSP').
card_image_name('paradox haze'/'TSP', 'paradox haze').
card_uid('paradox haze'/'TSP', 'TSP:Paradox Haze:paradox haze').
card_rarity('paradox haze'/'TSP', 'Uncommon').
card_artist('paradox haze'/'TSP', 'Greg Staples').
card_number('paradox haze'/'TSP', '71').
card_flavor_text('paradox haze'/'TSP', '\"Keep your pace steady through the haze, lest you step on the heels of your future self or trip the self a moment behind you.\"\n—Teferi').
card_multiverse_id('paradox haze'/'TSP', '109721').

card_in_set('pardic dragon', 'TSP').
card_original_type('pardic dragon'/'TSP', 'Creature — Dragon').
card_original_text('pardic dragon'/'TSP', 'Flying\n{R}: Pardic Dragon gets +1/+0 until end of turn.\nSuspend 2—{R}{R}\nWhenever an opponent plays a spell, if Pardic Dragon is suspended, that player may put a time counter on Pardic Dragon.').
card_first_print('pardic dragon', 'TSP').
card_image_name('pardic dragon'/'TSP', 'pardic dragon').
card_uid('pardic dragon'/'TSP', 'TSP:Pardic Dragon:pardic dragon').
card_rarity('pardic dragon'/'TSP', 'Rare').
card_artist('pardic dragon'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('pardic dragon'/'TSP', '173').
card_multiverse_id('pardic dragon'/'TSP', '109686').

card_in_set('pendelhaven elder', 'TSP').
card_original_type('pendelhaven elder'/'TSP', 'Creature — Elf Shaman').
card_original_text('pendelhaven elder'/'TSP', '{T}: Each 1/1 creature you control gets +1/+2 until end of turn.').
card_first_print('pendelhaven elder', 'TSP').
card_image_name('pendelhaven elder'/'TSP', 'pendelhaven elder').
card_uid('pendelhaven elder'/'TSP', 'TSP:Pendelhaven Elder:pendelhaven elder').
card_rarity('pendelhaven elder'/'TSP', 'Uncommon').
card_artist('pendelhaven elder'/'TSP', 'Pete Venters').
card_number('pendelhaven elder'/'TSP', '209').
card_flavor_text('pendelhaven elder'/'TSP', 'The elder who carries the ancestral mantle of Jacques le Vert is tasked with his ancient mission: to protect the creatures of Pendelhaven.').
card_multiverse_id('pendelhaven elder'/'TSP', '111083').

card_in_set('pentarch paladin', 'TSP').
card_original_type('pentarch paladin'/'TSP', 'Creature — Human Knight').
card_original_text('pentarch paladin'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nAs Pentarch Paladin comes into play, choose a color.\n{W}{W}, {T}: Destroy target permanent of the chosen color.').
card_first_print('pentarch paladin', 'TSP').
card_image_name('pentarch paladin'/'TSP', 'pentarch paladin').
card_uid('pentarch paladin'/'TSP', 'TSP:Pentarch Paladin:pentarch paladin').
card_rarity('pentarch paladin'/'TSP', 'Rare').
card_artist('pentarch paladin'/'TSP', 'Jim Murray').
card_number('pentarch paladin'/'TSP', '32').
card_multiverse_id('pentarch paladin'/'TSP', '110533').

card_in_set('pentarch ward', 'TSP').
card_original_type('pentarch ward'/'TSP', 'Enchantment — Aura').
card_original_text('pentarch ward'/'TSP', 'Enchant creature\nAs Pentarch Ward comes into play, choose a color.\nWhen Pentarch Ward comes into play, draw a card.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Pentarch Ward.').
card_first_print('pentarch ward', 'TSP').
card_image_name('pentarch ward'/'TSP', 'pentarch ward').
card_uid('pentarch ward'/'TSP', 'TSP:Pentarch Ward:pentarch ward').
card_rarity('pentarch ward'/'TSP', 'Common').
card_artist('pentarch ward'/'TSP', 'Dany Orizio').
card_number('pentarch ward'/'TSP', '33').
card_multiverse_id('pentarch ward'/'TSP', '108874').

card_in_set('penumbra spider', 'TSP').
card_original_type('penumbra spider'/'TSP', 'Creature — Spider').
card_original_text('penumbra spider'/'TSP', 'Penumbra Spider can block as though it had flying.\nWhen Penumbra Spider is put into a graveyard from play, put a 2/4 black Spider creature token into play that can block as though it had flying.').
card_first_print('penumbra spider', 'TSP').
card_image_name('penumbra spider'/'TSP', 'penumbra spider').
card_uid('penumbra spider'/'TSP', 'TSP:Penumbra Spider:penumbra spider').
card_rarity('penumbra spider'/'TSP', 'Common').
card_artist('penumbra spider'/'TSP', 'Jeff Easley').
card_number('penumbra spider'/'TSP', '210').
card_flavor_text('penumbra spider'/'TSP', 'When it snared a passing cockatrice, its own soul darkly doubled.').
card_multiverse_id('penumbra spider'/'TSP', '118899').

card_in_set('phantom wurm', 'TSP').
card_original_type('phantom wurm'/'TSP', 'Creature — Wurm Spirit').
card_original_text('phantom wurm'/'TSP', 'Phantom Wurm comes into play with four +1/+1 counters on it.\nIf damage would be dealt to Phantom Wurm, prevent that damage. Remove a +1/+1 counter from Phantom Wurm.').
card_first_print('phantom wurm', 'TSP').
card_image_name('phantom wurm'/'TSP', 'phantom wurm').
card_uid('phantom wurm'/'TSP', 'TSP:Phantom Wurm:phantom wurm').
card_rarity('phantom wurm'/'TSP', 'Uncommon').
card_artist('phantom wurm'/'TSP', 'Alan Pollack').
card_number('phantom wurm'/'TSP', '211').
card_flavor_text('phantom wurm'/'TSP', 'It died with the forest, yet both linger on in twisted form.').
card_multiverse_id('phantom wurm'/'TSP', '118868').

card_in_set('phthisis', 'TSP').
card_original_type('phthisis'/'TSP', 'Sorcery').
card_original_text('phthisis'/'TSP', 'Destroy target creature. Its controller loses life equal to its power plus its toughness.\nSuspend 5—{1}{B} (Rather than play this card from your hand, you may pay {1}{B} and remove it from the game with five time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_first_print('phthisis', 'TSP').
card_image_name('phthisis'/'TSP', 'phthisis').
card_uid('phthisis'/'TSP', 'TSP:Phthisis:phthisis').
card_rarity('phthisis'/'TSP', 'Uncommon').
card_artist('phthisis'/'TSP', 'Carl Critchlow').
card_number('phthisis'/'TSP', '122').
card_multiverse_id('phthisis'/'TSP', '109758').

card_in_set('phyrexian totem', 'TSP').
card_original_type('phyrexian totem'/'TSP', 'Artifact').
card_original_text('phyrexian totem'/'TSP', '{T}: Add {B} to your mana pool.\n{2}{B}: Phyrexian Totem becomes a 5/5 black Horror artifact creature with trample until end of turn.\nWhenever Phyrexian Totem is dealt damage, if it\'s a creature, sacrifice that many permanents.').
card_first_print('phyrexian totem', 'TSP').
card_image_name('phyrexian totem'/'TSP', 'phyrexian totem').
card_uid('phyrexian totem'/'TSP', 'TSP:Phyrexian Totem:phyrexian totem').
card_rarity('phyrexian totem'/'TSP', 'Uncommon').
card_artist('phyrexian totem'/'TSP', 'John Zeleznik').
card_number('phyrexian totem'/'TSP', '261').
card_multiverse_id('phyrexian totem'/'TSP', '108822').

card_in_set('pit keeper', 'TSP').
card_original_type('pit keeper'/'TSP', 'Creature — Human Wizard').
card_original_text('pit keeper'/'TSP', 'When Pit Keeper comes into play, if you have four or more creature cards in your graveyard, you may return target creature card from your graveyard to your hand.').
card_first_print('pit keeper', 'TSP').
card_image_name('pit keeper'/'TSP', 'pit keeper').
card_uid('pit keeper'/'TSP', 'TSP:Pit Keeper:pit keeper').
card_rarity('pit keeper'/'TSP', 'Common').
card_artist('pit keeper'/'TSP', 'Anthony S. Waters').
card_number('pit keeper'/'TSP', '123').
card_flavor_text('pit keeper'/'TSP', '\"The undead are not ‘awakened.\' They are evicted when there is no room left in the pit for more bodies.\"').
card_multiverse_id('pit keeper'/'TSP', '116396').

card_in_set('plague sliver', 'TSP').
card_original_type('plague sliver'/'TSP', 'Creature — Sliver').
card_original_text('plague sliver'/'TSP', 'All Slivers have \"At the beginning of your upkeep, this creature deals 1 damage to you.\"').
card_first_print('plague sliver', 'TSP').
card_image_name('plague sliver'/'TSP', 'plague sliver').
card_uid('plague sliver'/'TSP', 'TSP:Plague Sliver:plague sliver').
card_rarity('plague sliver'/'TSP', 'Rare').
card_artist('plague sliver'/'TSP', 'Dave Allsop').
card_number('plague sliver'/'TSP', '124').
card_flavor_text('plague sliver'/'TSP', 'A sliver shares everything with its hive—even its afflictions.').
card_multiverse_id('plague sliver'/'TSP', '116748').

card_in_set('plains', 'TSP').
card_original_type('plains'/'TSP', 'Basic Land — Plains').
card_original_text('plains'/'TSP', 'W').
card_image_name('plains'/'TSP', 'plains1').
card_uid('plains'/'TSP', 'TSP:Plains:plains1').
card_rarity('plains'/'TSP', 'Basic Land').
card_artist('plains'/'TSP', 'Rob Alexander').
card_number('plains'/'TSP', '282').
card_multiverse_id('plains'/'TSP', '122092').

card_in_set('plains', 'TSP').
card_original_type('plains'/'TSP', 'Basic Land — Plains').
card_original_text('plains'/'TSP', 'W').
card_image_name('plains'/'TSP', 'plains2').
card_uid('plains'/'TSP', 'TSP:Plains:plains2').
card_rarity('plains'/'TSP', 'Basic Land').
card_artist('plains'/'TSP', 'Craig Mullins').
card_number('plains'/'TSP', '283').
card_multiverse_id('plains'/'TSP', '122087').

card_in_set('plains', 'TSP').
card_original_type('plains'/'TSP', 'Basic Land — Plains').
card_original_text('plains'/'TSP', 'W').
card_image_name('plains'/'TSP', 'plains3').
card_uid('plains'/'TSP', 'TSP:Plains:plains3').
card_rarity('plains'/'TSP', 'Basic Land').
card_artist('plains'/'TSP', 'Justin Sweet').
card_number('plains'/'TSP', '284').
card_multiverse_id('plains'/'TSP', '122085').

card_in_set('plains', 'TSP').
card_original_type('plains'/'TSP', 'Basic Land — Plains').
card_original_text('plains'/'TSP', 'W').
card_image_name('plains'/'TSP', 'plains4').
card_uid('plains'/'TSP', 'TSP:Plains:plains4').
card_rarity('plains'/'TSP', 'Basic Land').
card_artist('plains'/'TSP', 'Richard Wright').
card_number('plains'/'TSP', '285').
card_multiverse_id('plains'/'TSP', '122084').

card_in_set('plated pegasus', 'TSP').
card_original_type('plated pegasus'/'TSP', 'Creature — Pegasus').
card_original_text('plated pegasus'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nFlying\nIf a spell would deal damage to a creature or player, prevent 1 damage that spell would deal to that creature or player.').
card_first_print('plated pegasus', 'TSP').
card_image_name('plated pegasus'/'TSP', 'plated pegasus').
card_uid('plated pegasus'/'TSP', 'TSP:Plated Pegasus:plated pegasus').
card_rarity('plated pegasus'/'TSP', 'Uncommon').
card_artist('plated pegasus'/'TSP', 'Greg Hildebrandt').
card_number('plated pegasus'/'TSP', '34').
card_multiverse_id('plated pegasus'/'TSP', '110510').

card_in_set('plunder', 'TSP').
card_original_type('plunder'/'TSP', 'Sorcery').
card_original_text('plunder'/'TSP', 'Destroy target artifact or land.\nSuspend 4—{1}{R} (Rather than play this card from your hand, you may pay {1}{R} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_first_print('plunder', 'TSP').
card_image_name('plunder'/'TSP', 'plunder').
card_uid('plunder'/'TSP', 'TSP:Plunder:plunder').
card_rarity('plunder'/'TSP', 'Common').
card_artist('plunder'/'TSP', 'Thomas M. Baxa').
card_number('plunder'/'TSP', '174').
card_multiverse_id('plunder'/'TSP', '108926').

card_in_set('premature burial', 'TSP').
card_original_type('premature burial'/'TSP', 'Sorcery').
card_original_text('premature burial'/'TSP', 'Destroy target nonblack creature that came into play since your last turn ended.').
card_first_print('premature burial', 'TSP').
card_image_name('premature burial'/'TSP', 'premature burial').
card_uid('premature burial'/'TSP', 'TSP:Premature Burial:premature burial').
card_rarity('premature burial'/'TSP', 'Uncommon').
card_artist('premature burial'/'TSP', 'Clint Langley').
card_number('premature burial'/'TSP', '125').
card_flavor_text('premature burial'/'TSP', '\"This soil knows well the taste of the dead. Give it the hunger for a living meal, and it can become a deadly ally.\"\n—Ezrith, druid of the Dark Hours').
card_multiverse_id('premature burial'/'TSP', '108884').

card_in_set('primal forcemage', 'TSP').
card_original_type('primal forcemage'/'TSP', 'Creature — Elf Shaman').
card_original_text('primal forcemage'/'TSP', 'Whenever another creature comes into play under your control, that creature gets +3/+3 until end of turn.').
card_first_print('primal forcemage', 'TSP').
card_image_name('primal forcemage'/'TSP', 'primal forcemage').
card_uid('primal forcemage'/'TSP', 'TSP:Primal Forcemage:primal forcemage').
card_rarity('primal forcemage'/'TSP', 'Uncommon').
card_artist('primal forcemage'/'TSP', 'Jeff Miracola').
card_number('primal forcemage'/'TSP', '212').
card_flavor_text('primal forcemage'/'TSP', 'Their calls unheeded by the withered forests, nature shamans channeled the life force of their brethren.').
card_multiverse_id('primal forcemage'/'TSP', '110508').

card_in_set('prismatic lens', 'TSP').
card_original_type('prismatic lens'/'TSP', 'Artifact').
card_original_text('prismatic lens'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('prismatic lens', 'TSP').
card_image_name('prismatic lens'/'TSP', 'prismatic lens').
card_uid('prismatic lens'/'TSP', 'TSP:Prismatic Lens:prismatic lens').
card_rarity('prismatic lens'/'TSP', 'Common').
card_artist('prismatic lens'/'TSP', 'Alan Pollack').
card_number('prismatic lens'/'TSP', '262').
card_flavor_text('prismatic lens'/'TSP', 'It bends not light but mana, aligning its chaotic currents into the sharp angles necessary for the mystic\'s purposes.').
card_multiverse_id('prismatic lens'/'TSP', '118880').

card_in_set('psionic sliver', 'TSP').
card_original_type('psionic sliver'/'TSP', 'Creature — Sliver').
card_original_text('psionic sliver'/'TSP', 'All Slivers have \"{T}: This creature deals 2 damage to target creature or player and 3 damage to itself.\"').
card_first_print('psionic sliver', 'TSP').
card_image_name('psionic sliver'/'TSP', 'psionic sliver').
card_uid('psionic sliver'/'TSP', 'TSP:Psionic Sliver:psionic sliver').
card_rarity('psionic sliver'/'TSP', 'Rare').
card_artist('psionic sliver'/'TSP', 'Wayne England').
card_number('psionic sliver'/'TSP', '72').
card_flavor_text('psionic sliver'/'TSP', 'They evolved the ability to concentrate their neural activity into a single pulse, causing a disruptive but usually suicidal blast of psionic energy.').
card_multiverse_id('psionic sliver'/'TSP', '113559').

card_in_set('psychotic episode', 'TSP').
card_original_type('psychotic episode'/'TSP', 'Sorcery').
card_original_text('psychotic episode'/'TSP', 'Target player reveals his or her hand and the top card of his or her library. You choose a card revealed this way. That player puts the chosen card on the bottom of his or her library.\nMadness {1}{B} (If you discard this card, you may play it for its madness cost instead of putting it into your graveyard.)').
card_first_print('psychotic episode', 'TSP').
card_image_name('psychotic episode'/'TSP', 'psychotic episode').
card_uid('psychotic episode'/'TSP', 'TSP:Psychotic Episode:psychotic episode').
card_rarity('psychotic episode'/'TSP', 'Common').
card_artist('psychotic episode'/'TSP', 'Drew Tucker').
card_number('psychotic episode'/'TSP', '126').
card_multiverse_id('psychotic episode'/'TSP', '116727').

card_in_set('pull from eternity', 'TSP').
card_original_type('pull from eternity'/'TSP', 'Instant').
card_original_text('pull from eternity'/'TSP', 'Put target face-up card that\'s removed from the game into its owner\'s graveyard.').
card_first_print('pull from eternity', 'TSP').
card_image_name('pull from eternity'/'TSP', 'pull from eternity').
card_uid('pull from eternity'/'TSP', 'TSP:Pull from Eternity:pull from eternity').
card_rarity('pull from eternity'/'TSP', 'Uncommon').
card_artist('pull from eternity'/'TSP', 'Ron Spears').
card_number('pull from eternity'/'TSP', '35').
card_flavor_text('pull from eternity'/'TSP', 'It is best to conquer dragons before they hatch.\n—Femeref adage').
card_multiverse_id('pull from eternity'/'TSP', '106657').

card_in_set('pulmonic sliver', 'TSP').
card_original_type('pulmonic sliver'/'TSP', 'Creature — Sliver').
card_original_text('pulmonic sliver'/'TSP', 'All Slivers have flying and \"If this creature would be put into a graveyard, you may put it on top of its owner\'s library instead.\"').
card_first_print('pulmonic sliver', 'TSP').
card_image_name('pulmonic sliver'/'TSP', 'pulmonic sliver').
card_uid('pulmonic sliver'/'TSP', 'TSP:Pulmonic Sliver:pulmonic sliver').
card_rarity('pulmonic sliver'/'TSP', 'Rare').
card_artist('pulmonic sliver'/'TSP', 'Jeff Easley').
card_number('pulmonic sliver'/'TSP', '36').
card_flavor_text('pulmonic sliver'/'TSP', 'Like a great bellows it hisses, and its kin, both living and dead, are lifted to safety.').
card_multiverse_id('pulmonic sliver'/'TSP', '113575').

card_in_set('quilled sliver', 'TSP').
card_original_type('quilled sliver'/'TSP', 'Creature — Sliver').
card_original_text('quilled sliver'/'TSP', 'All Slivers have \"{T}: This creature deals 1 damage to target attacking or blocking creature.\"').
card_first_print('quilled sliver', 'TSP').
card_image_name('quilled sliver'/'TSP', 'quilled sliver').
card_uid('quilled sliver'/'TSP', 'TSP:Quilled Sliver:quilled sliver').
card_rarity('quilled sliver'/'TSP', 'Uncommon').
card_artist('quilled sliver'/'TSP', 'John Matson').
card_number('quilled sliver'/'TSP', '37').
card_flavor_text('quilled sliver'/'TSP', '\"They have long kept us under attack, but we do not lack for ammunition. The very bodies of our foes arm us against them.\"\n—Adom Capashen, Benalish hero').
card_multiverse_id('quilled sliver'/'TSP', '111051').

card_in_set('reiterate', 'TSP').
card_original_type('reiterate'/'TSP', 'Instant').
card_original_text('reiterate'/'TSP', 'Buyback {3} (You may pay an additional {3} as you play this spell. If you do, put this card into your hand as it resolves.)\nCopy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('reiterate', 'TSP').
card_image_name('reiterate'/'TSP', 'reiterate').
card_uid('reiterate'/'TSP', 'TSP:Reiterate:reiterate').
card_rarity('reiterate'/'TSP', 'Rare').
card_artist('reiterate'/'TSP', 'Dan Scott').
card_number('reiterate'/'TSP', '175').
card_flavor_text('reiterate'/'TSP', '\"Echoes of the Mirari\'s power yet linger.\"\n—Zyd, Kamahlite druid').
card_multiverse_id('reiterate'/'TSP', '109729').

card_in_set('restore balance', 'TSP').
card_original_type('restore balance'/'TSP', 'Sorcery').
card_original_text('restore balance'/'TSP', 'Restore Balance is white.\nSuspend 6—{W}\nEach player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players sacrifice creatures and discard cards the same way.').
card_first_print('restore balance', 'TSP').
card_image_name('restore balance'/'TSP', 'restore balance').
card_uid('restore balance'/'TSP', 'TSP:Restore Balance:restore balance').
card_rarity('restore balance'/'TSP', 'Rare').
card_artist('restore balance'/'TSP', 'Mark Poole').
card_number('restore balance'/'TSP', '38').
card_multiverse_id('restore balance'/'TSP', '113520').

card_in_set('return to dust', 'TSP').
card_original_type('return to dust'/'TSP', 'Instant').
card_original_text('return to dust'/'TSP', 'Remove target artifact or enchantment from the game. If you played this spell during your main phase, you may remove up to one other target artifact or enchantment from the game.').
card_first_print('return to dust', 'TSP').
card_image_name('return to dust'/'TSP', 'return to dust').
card_uid('return to dust'/'TSP', 'TSP:Return to Dust:return to dust').
card_rarity('return to dust'/'TSP', 'Uncommon').
card_artist('return to dust'/'TSP', 'Wayne Reynolds').
card_number('return to dust'/'TSP', '39').
card_flavor_text('return to dust'/'TSP', 'Some timelines forever fray, branch, and intermingle. Others end abruptly.').
card_multiverse_id('return to dust'/'TSP', '109709').

card_in_set('rift bolt', 'TSP').
card_original_type('rift bolt'/'TSP', 'Sorcery').
card_original_text('rift bolt'/'TSP', 'Rift Bolt deals 3 damage to target creature or player.\nSuspend 1—{R} (Rather than play this card from your hand, you may pay {R} and remove it from the game with a time counter on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_image_name('rift bolt'/'TSP', 'rift bolt').
card_uid('rift bolt'/'TSP', 'TSP:Rift Bolt:rift bolt').
card_rarity('rift bolt'/'TSP', 'Common').
card_artist('rift bolt'/'TSP', 'Michael Sutfin').
card_number('rift bolt'/'TSP', '176').
card_multiverse_id('rift bolt'/'TSP', '108915').

card_in_set('riftwing cloudskate', 'TSP').
card_original_type('riftwing cloudskate'/'TSP', 'Creature — Illusion').
card_original_text('riftwing cloudskate'/'TSP', 'Flying\nWhen Riftwing Cloudskate comes into play, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than play this card from your hand, you may pay {1}{U} and remove it from the game with three time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('riftwing cloudskate', 'TSP').
card_image_name('riftwing cloudskate'/'TSP', 'riftwing cloudskate').
card_uid('riftwing cloudskate'/'TSP', 'TSP:Riftwing Cloudskate:riftwing cloudskate').
card_rarity('riftwing cloudskate'/'TSP', 'Uncommon').
card_artist('riftwing cloudskate'/'TSP', 'Carl Critchlow').
card_number('riftwing cloudskate'/'TSP', '73').
card_multiverse_id('riftwing cloudskate'/'TSP', '109715').

card_in_set('saffi eriksdotter', 'TSP').
card_original_type('saffi eriksdotter'/'TSP', 'Legendary Creature — Human Scout').
card_original_text('saffi eriksdotter'/'TSP', 'Sacrifice Saffi Eriksdotter: When target creature is put into your graveyard from play this turn, return that card to play.').
card_first_print('saffi eriksdotter', 'TSP').
card_image_name('saffi eriksdotter'/'TSP', 'saffi eriksdotter').
card_uid('saffi eriksdotter'/'TSP', 'TSP:Saffi Eriksdotter:saffi eriksdotter').
card_rarity('saffi eriksdotter'/'TSP', 'Rare').
card_artist('saffi eriksdotter'/'TSP', 'Christopher Moeller').
card_number('saffi eriksdotter'/'TSP', '245').
card_flavor_text('saffi eriksdotter'/'TSP', 'In the blink of an eye, she strode from deep snow to dusty waste. From the crease of light behind her, a voice rang hollow: \"Saffi, wait for me . . . .\"').
card_multiverse_id('saffi eriksdotter'/'TSP', '113540').

card_in_set('sage of epityr', 'TSP').
card_original_type('sage of epityr'/'TSP', 'Creature — Human Wizard').
card_original_text('sage of epityr'/'TSP', 'When Sage of Epityr comes into play, look at the top four cards of your library, then put them back in any order.').
card_first_print('sage of epityr', 'TSP').
card_image_name('sage of epityr'/'TSP', 'sage of epityr').
card_uid('sage of epityr'/'TSP', 'TSP:Sage of Epityr:sage of epityr').
card_rarity('sage of epityr'/'TSP', 'Common').
card_artist('sage of epityr'/'TSP', 'Randy Gallegos').
card_number('sage of epityr'/'TSP', '74').
card_flavor_text('sage of epityr'/'TSP', 'Clairvoyants across Dominaria were driven mad by the overload from the widening time rifts, while other random folk gained the gift of future sight.').
card_multiverse_id('sage of epityr'/'TSP', '118914').

card_in_set('saltcrusted steppe', 'TSP').
card_original_type('saltcrusted steppe'/'TSP', 'Land').
card_original_text('saltcrusted steppe'/'TSP', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Saltcrusted Steppe.\n{1}, Remove X storage counters from Saltcrusted Steppe: Add X mana in any combination of {G} and/or {W} to your mana pool.').
card_first_print('saltcrusted steppe', 'TSP').
card_image_name('saltcrusted steppe'/'TSP', 'saltcrusted steppe').
card_uid('saltcrusted steppe'/'TSP', 'TSP:Saltcrusted Steppe:saltcrusted steppe').
card_rarity('saltcrusted steppe'/'TSP', 'Uncommon').
card_artist('saltcrusted steppe'/'TSP', 'Greg Staples').
card_number('saltcrusted steppe'/'TSP', '277').
card_multiverse_id('saltcrusted steppe'/'TSP', '108839').

card_in_set('sangrophage', 'TSP').
card_original_type('sangrophage'/'TSP', 'Creature — Zombie').
card_original_text('sangrophage'/'TSP', 'At the beginning of your upkeep, tap Sangrophage unless you pay 2 life.').
card_first_print('sangrophage', 'TSP').
card_image_name('sangrophage'/'TSP', 'sangrophage').
card_uid('sangrophage'/'TSP', 'TSP:Sangrophage:sangrophage').
card_rarity('sangrophage'/'TSP', 'Common').
card_artist('sangrophage'/'TSP', 'Pete Venters').
card_number('sangrophage'/'TSP', '127').
card_flavor_text('sangrophage'/'TSP', 'The living is all it eats.').
card_multiverse_id('sangrophage'/'TSP', '116741').

card_in_set('sarpadian empires, vol. vii', 'TSP').
card_original_type('sarpadian empires, vol. vii'/'TSP', 'Artifact').
card_original_text('sarpadian empires, vol. vii'/'TSP', 'As Sarpadian Empires, Vol. VII comes into play, choose white Citizen, blue Camarid, black Thrull, red Goblin, or green Saproling.\n{3}, {T}: Put a 1/1 creature token of the chosen color and type into play.').
card_first_print('sarpadian empires, vol. vii', 'TSP').
card_image_name('sarpadian empires, vol. vii'/'TSP', 'sarpadian empires, vol. vii').
card_uid('sarpadian empires, vol. vii'/'TSP', 'TSP:Sarpadian Empires, Vol. VII:sarpadian empires, vol. vii').
card_rarity('sarpadian empires, vol. vii'/'TSP', 'Rare').
card_artist('sarpadian empires, vol. vii'/'TSP', 'Doug Chaffee').
card_number('sarpadian empires, vol. vii'/'TSP', '263').
card_flavor_text('sarpadian empires, vol. vii'/'TSP', 'The past still lives within its pages, waiting for its time to come again.').
card_multiverse_id('sarpadian empires, vol. vii'/'TSP', '114921').

card_in_set('savage thallid', 'TSP').
card_original_type('savage thallid'/'TSP', 'Creature — Fungus').
card_original_text('savage thallid'/'TSP', 'At the beginning of your upkeep, put a spore counter on Savage Thallid.\nRemove three spore counters from Savage Thallid: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Regenerate target Fungus.').
card_first_print('savage thallid', 'TSP').
card_image_name('savage thallid'/'TSP', 'savage thallid').
card_uid('savage thallid'/'TSP', 'TSP:Savage Thallid:savage thallid').
card_rarity('savage thallid'/'TSP', 'Common').
card_artist('savage thallid'/'TSP', 'Luca Zontini').
card_number('savage thallid'/'TSP', '213').
card_multiverse_id('savage thallid'/'TSP', '106643').

card_in_set('scarwood treefolk', 'TSP').
card_original_type('scarwood treefolk'/'TSP', 'Creature — Treefolk').
card_original_text('scarwood treefolk'/'TSP', 'Scarwood Treefolk comes into play tapped.').
card_first_print('scarwood treefolk', 'TSP').
card_image_name('scarwood treefolk'/'TSP', 'scarwood treefolk').
card_uid('scarwood treefolk'/'TSP', 'TSP:Scarwood Treefolk:scarwood treefolk').
card_rarity('scarwood treefolk'/'TSP', 'Common').
card_artist('scarwood treefolk'/'TSP', 'Stuart Griffin').
card_number('scarwood treefolk'/'TSP', '214').
card_flavor_text('scarwood treefolk'/'TSP', 'To treefolk\'s sense of time, ages pass as hours. They stood as witnesses to the apocalypse, the years of which they saw as one cacophonous, ultra-destructive moment.').
card_multiverse_id('scarwood treefolk'/'TSP', '110524').

card_in_set('scion of the ur-dragon', 'TSP').
card_original_type('scion of the ur-dragon'/'TSP', 'Legendary Creature — Dragon Avatar').
card_original_text('scion of the ur-dragon'/'TSP', 'Flying\n{2}: Search your library for a Dragon card and put it into your graveyard. If you do, Scion of the Ur-Dragon becomes a copy of that card until end of turn. Then shuffle your library.').
card_first_print('scion of the ur-dragon', 'TSP').
card_image_name('scion of the ur-dragon'/'TSP', 'scion of the ur-dragon').
card_uid('scion of the ur-dragon'/'TSP', 'TSP:Scion of the Ur-Dragon:scion of the ur-dragon').
card_rarity('scion of the ur-dragon'/'TSP', 'Rare').
card_artist('scion of the ur-dragon'/'TSP', 'Jim Murray').
card_number('scion of the ur-dragon'/'TSP', '246').
card_flavor_text('scion of the ur-dragon'/'TSP', '\"I am the blood of the ur-dragon, coursing through all dragonkind.\"').
card_multiverse_id('scion of the ur-dragon'/'TSP', '116745').

card_in_set('screeching sliver', 'TSP').
card_original_type('screeching sliver'/'TSP', 'Creature — Sliver').
card_original_text('screeching sliver'/'TSP', 'All Slivers have \"{T}: Target player puts the top card of his or her library into his or her graveyard.\"').
card_first_print('screeching sliver', 'TSP').
card_image_name('screeching sliver'/'TSP', 'screeching sliver').
card_uid('screeching sliver'/'TSP', 'TSP:Screeching Sliver:screeching sliver').
card_rarity('screeching sliver'/'TSP', 'Common').
card_artist('screeching sliver'/'TSP', 'Stuart Griffin').
card_number('screeching sliver'/'TSP', '75').
card_flavor_text('screeching sliver'/'TSP', '\"What wears down my people most, I think, is not the danger, but the endless screeching outside our camp.\"\n—Merrik Aidar, Benalish patrol').
card_multiverse_id('screeching sliver'/'TSP', '122387').

card_in_set('scryb ranger', 'TSP').
card_original_type('scryb ranger'/'TSP', 'Creature — Faerie').
card_original_text('scryb ranger'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nFlying, protection from blue\nReturn a Forest you control to its owner\'s hand: Untap target creature. Play this ability only once each turn.').
card_first_print('scryb ranger', 'TSP').
card_image_name('scryb ranger'/'TSP', 'scryb ranger').
card_uid('scryb ranger'/'TSP', 'TSP:Scryb Ranger:scryb ranger').
card_rarity('scryb ranger'/'TSP', 'Uncommon').
card_artist('scryb ranger'/'TSP', 'Rebecca Guay').
card_number('scryb ranger'/'TSP', '215').
card_multiverse_id('scryb ranger'/'TSP', '118924').

card_in_set('search for tomorrow', 'TSP').
card_original_type('search for tomorrow'/'TSP', 'Sorcery').
card_original_text('search for tomorrow'/'TSP', 'Search your library for a basic land card and put it into play. Then shuffle your library.\nSuspend 2—{G} (Rather than play this card from your hand, you may pay {G} and remove it from the game with two time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)').
card_first_print('search for tomorrow', 'TSP').
card_image_name('search for tomorrow'/'TSP', 'search for tomorrow').
card_uid('search for tomorrow'/'TSP', 'TSP:Search for Tomorrow:search for tomorrow').
card_rarity('search for tomorrow'/'TSP', 'Common').
card_artist('search for tomorrow'/'TSP', 'Randy Gallegos').
card_number('search for tomorrow'/'TSP', '216').
card_multiverse_id('search for tomorrow'/'TSP', '108802').

card_in_set('sedge sliver', 'TSP').
card_original_type('sedge sliver'/'TSP', 'Creature — Sliver').
card_original_text('sedge sliver'/'TSP', 'All Slivers have \"This creature gets +1/+1 as long as you control a Swamp\" and \"{B}: Regenerate this creature.\"').
card_first_print('sedge sliver', 'TSP').
card_image_name('sedge sliver'/'TSP', 'sedge sliver').
card_uid('sedge sliver'/'TSP', 'TSP:Sedge Sliver:sedge sliver').
card_rarity('sedge sliver'/'TSP', 'Rare').
card_artist('sedge sliver'/'TSP', 'Richard Kane Ferguson').
card_number('sedge sliver'/'TSP', '177').
card_flavor_text('sedge sliver'/'TSP', 'Their flesh is as sodden as the fens in which they live, oozing to fill any wound and squishing to absorb any blow.').
card_multiverse_id('sedge sliver'/'TSP', '118917').

card_in_set('sengir nosferatu', 'TSP').
card_original_type('sengir nosferatu'/'TSP', 'Creature — Vampire').
card_original_text('sengir nosferatu'/'TSP', 'Flying\n{1}{B}, Remove Sengir Nosferatu from the game: Put a 1/2 black Bat creature token with flying into play. It has \"{1}{B}, Sacrifice this creature: Return to play under its owner\'s control a card named Sengir Nosferatu that\'s removed from the game.\"').
card_first_print('sengir nosferatu', 'TSP').
card_image_name('sengir nosferatu'/'TSP', 'sengir nosferatu').
card_uid('sengir nosferatu'/'TSP', 'TSP:Sengir Nosferatu:sengir nosferatu').
card_rarity('sengir nosferatu'/'TSP', 'Rare').
card_artist('sengir nosferatu'/'TSP', 'Scott M. Fischer').
card_number('sengir nosferatu'/'TSP', '128').
card_multiverse_id('sengir nosferatu'/'TSP', '116383').

card_in_set('serra avenger', 'TSP').
card_original_type('serra avenger'/'TSP', 'Creature — Angel').
card_original_text('serra avenger'/'TSP', 'You can\'t play Serra Avenger during your first, second, or third turns of the game.\nFlying, vigilance').
card_image_name('serra avenger'/'TSP', 'serra avenger').
card_uid('serra avenger'/'TSP', 'TSP:Serra Avenger:serra avenger').
card_rarity('serra avenger'/'TSP', 'Rare').
card_artist('serra avenger'/'TSP', 'Scott M. Fischer').
card_number('serra avenger'/'TSP', '40').
card_flavor_text('serra avenger'/'TSP', '\"Those who endure in the face of suffering, those whose faith shines long in evil days, they shall see salvation.\"\n—Song of All, canto 904').
card_multiverse_id('serra avenger'/'TSP', '113519').

card_in_set('shadow sliver', 'TSP').
card_original_type('shadow sliver'/'TSP', 'Creature — Sliver').
card_original_text('shadow sliver'/'TSP', 'All Slivers have shadow. (They can block or be blocked by only creatures with shadow.)').
card_first_print('shadow sliver', 'TSP').
card_image_name('shadow sliver'/'TSP', 'shadow sliver').
card_uid('shadow sliver'/'TSP', 'TSP:Shadow Sliver:shadow sliver').
card_rarity('shadow sliver'/'TSP', 'Common').
card_artist('shadow sliver'/'TSP', 'Warren Mahy').
card_number('shadow sliver'/'TSP', '76').
card_flavor_text('shadow sliver'/'TSP', 'These slivers, trapped between worlds since the Rathi overlay, are among the last to claim direct lineage from the lost Sliver Queen.').
card_multiverse_id('shadow sliver'/'TSP', '108797').

card_in_set('sidewinder sliver', 'TSP').
card_original_type('sidewinder sliver'/'TSP', 'Creature — Sliver').
card_original_text('sidewinder sliver'/'TSP', 'All Slivers have flanking. (Whenever a creature without flanking blocks a Sliver, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('sidewinder sliver', 'TSP').
card_image_name('sidewinder sliver'/'TSP', 'sidewinder sliver').
card_uid('sidewinder sliver'/'TSP', 'TSP:Sidewinder Sliver:sidewinder sliver').
card_rarity('sidewinder sliver'/'TSP', 'Common').
card_artist('sidewinder sliver'/'TSP', 'Ron Spencer').
card_number('sidewinder sliver'/'TSP', '41').
card_flavor_text('sidewinder sliver'/'TSP', '\"They encircled our patrol with the stealth of snakes, corralling us like livestock.\"\n—Merrik Aidar, Benalish patrol').
card_multiverse_id('sidewinder sliver'/'TSP', '118908').

card_in_set('skittering monstrosity', 'TSP').
card_original_type('skittering monstrosity'/'TSP', 'Creature — Horror').
card_original_text('skittering monstrosity'/'TSP', 'When you play a creature spell, sacrifice Skittering Monstrosity.').
card_first_print('skittering monstrosity', 'TSP').
card_image_name('skittering monstrosity'/'TSP', 'skittering monstrosity').
card_uid('skittering monstrosity'/'TSP', 'TSP:Skittering Monstrosity:skittering monstrosity').
card_rarity('skittering monstrosity'/'TSP', 'Uncommon').
card_artist('skittering monstrosity'/'TSP', 'rk post').
card_number('skittering monstrosity'/'TSP', '129').
card_flavor_text('skittering monstrosity'/'TSP', 'Most living things were weakened and stunted in the ruinous aftermath of the Phyrexian invasion, but a few grew more horrid than ever.').
card_multiverse_id('skittering monstrosity'/'TSP', '118878').

card_in_set('skulking knight', 'TSP').
card_original_type('skulking knight'/'TSP', 'Creature — Zombie Knight').
card_original_text('skulking knight'/'TSP', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhen Skulking Knight becomes the target of a spell or ability, sacrifice it.').
card_first_print('skulking knight', 'TSP').
card_image_name('skulking knight'/'TSP', 'skulking knight').
card_uid('skulking knight'/'TSP', 'TSP:Skulking Knight:skulking knight').
card_rarity('skulking knight'/'TSP', 'Common').
card_artist('skulking knight'/'TSP', 'rk post').
card_number('skulking knight'/'TSP', '130').
card_multiverse_id('skulking knight'/'TSP', '118898').

card_in_set('slipstream serpent', 'TSP').
card_original_type('slipstream serpent'/'TSP', 'Creature — Serpent').
card_original_text('slipstream serpent'/'TSP', 'Slipstream Serpent can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Slipstream Serpent.\nMorph {5}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('slipstream serpent', 'TSP').
card_image_name('slipstream serpent'/'TSP', 'slipstream serpent').
card_uid('slipstream serpent'/'TSP', 'TSP:Slipstream Serpent:slipstream serpent').
card_rarity('slipstream serpent'/'TSP', 'Common').
card_artist('slipstream serpent'/'TSP', 'Darrell Riche').
card_number('slipstream serpent'/'TSP', '77').
card_multiverse_id('slipstream serpent'/'TSP', '116385').

card_in_set('smallpox', 'TSP').
card_original_type('smallpox'/'TSP', 'Sorcery').
card_original_text('smallpox'/'TSP', 'Each player loses 1 life, discards a card, sacrifices a creature, then sacrifices a land.').
card_first_print('smallpox', 'TSP').
card_image_name('smallpox'/'TSP', 'smallpox').
card_uid('smallpox'/'TSP', 'TSP:Smallpox:smallpox').
card_rarity('smallpox'/'TSP', 'Uncommon').
card_artist('smallpox'/'TSP', 'Janine Johnston').
card_number('smallpox'/'TSP', '131').
card_flavor_text('smallpox'/'TSP', '\"Great losses often bring only a numb shock. To truly plunge a victim into misery, you must overwhelm him with many small sufferings.\"\n—Ratadrabik of Urborg').
card_multiverse_id('smallpox'/'TSP', '113531').

card_in_set('snapback', 'TSP').
card_original_type('snapback'/'TSP', 'Instant').
card_original_text('snapback'/'TSP', 'You may remove a blue card in your hand from the game rather than pay Snapback\'s mana cost.\nReturn target creature to its owner\'s hand.').
card_first_print('snapback', 'TSP').
card_image_name('snapback'/'TSP', 'snapback').
card_uid('snapback'/'TSP', 'TSP:Snapback:snapback').
card_rarity('snapback'/'TSP', 'Common').
card_artist('snapback'/'TSP', 'Alan Pollack').
card_number('snapback'/'TSP', '78').
card_flavor_text('snapback'/'TSP', '\"If it returns years hence, our knowledge of how to combat it will be that much greater.\"\n—Andorel, Tolarian sentinel').
card_multiverse_id('snapback'/'TSP', '108897').

card_in_set('spectral force', 'TSP').
card_original_type('spectral force'/'TSP', 'Creature — Elemental Spirit').
card_original_text('spectral force'/'TSP', 'Trample\nWhenever Spectral Force attacks, if defending player controls no black permanents, it doesn\'t untap during your next untap step.').
card_first_print('spectral force', 'TSP').
card_image_name('spectral force'/'TSP', 'spectral force').
card_uid('spectral force'/'TSP', 'TSP:Spectral Force:spectral force').
card_rarity('spectral force'/'TSP', 'Rare').
card_artist('spectral force'/'TSP', 'Dan Scott').
card_number('spectral force'/'TSP', '217').
card_flavor_text('spectral force'/'TSP', '\"The spirits of old have not left us unguarded.\"\n—Uros, Pendelhaven elder').
card_multiverse_id('spectral force'/'TSP', '118871').

card_in_set('spell burst', 'TSP').
card_original_type('spell burst'/'TSP', 'Instant').
card_original_text('spell burst'/'TSP', 'Buyback {3} (You may pay an additional {3} as you play this spell. If you do, put this card into your hand as it resolves.)\nCounter target spell with converted mana cost X.').
card_first_print('spell burst', 'TSP').
card_image_name('spell burst'/'TSP', 'spell burst').
card_uid('spell burst'/'TSP', 'TSP:Spell Burst:spell burst').
card_rarity('spell burst'/'TSP', 'Uncommon').
card_artist('spell burst'/'TSP', 'Terese Nielsen').
card_number('spell burst'/'TSP', '79').
card_flavor_text('spell burst'/'TSP', 'The brutish mage\'s version of thinking.').
card_multiverse_id('spell burst'/'TSP', '109707').

card_in_set('spike tiller', 'TSP').
card_original_type('spike tiller'/'TSP', 'Creature — Spike').
card_original_text('spike tiller'/'TSP', 'Spike Tiller comes into play with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Tiller: Put a +1/+1 counter on target creature. \n{2}, Remove a +1/+1 counter from Spike Tiller: Target land becomes a 2/2 creature that\'s still a land. Put a +1/+1 counter on it.').
card_first_print('spike tiller', 'TSP').
card_image_name('spike tiller'/'TSP', 'spike tiller').
card_uid('spike tiller'/'TSP', 'TSP:Spike Tiller:spike tiller').
card_rarity('spike tiller'/'TSP', 'Rare').
card_artist('spike tiller'/'TSP', 'Tony Szczudlo').
card_number('spike tiller'/'TSP', '218').
card_multiverse_id('spike tiller'/'TSP', '109689').

card_in_set('spiketail drakeling', 'TSP').
card_original_type('spiketail drakeling'/'TSP', 'Creature — Drake').
card_original_text('spiketail drakeling'/'TSP', 'Flying\nSacrifice Spiketail Drakeling: Counter target spell unless its controller pays {2}.').
card_first_print('spiketail drakeling', 'TSP').
card_image_name('spiketail drakeling'/'TSP', 'spiketail drakeling').
card_uid('spiketail drakeling'/'TSP', 'TSP:Spiketail Drakeling:spiketail drakeling').
card_rarity('spiketail drakeling'/'TSP', 'Common').
card_artist('spiketail drakeling'/'TSP', 'Dave Dorman').
card_number('spiketail drakeling'/'TSP', '80').
card_flavor_text('spiketail drakeling'/'TSP', 'Drakelings fish with their tails, angling to snare succulent thoughts and memories on their psychic hooks.').
card_multiverse_id('spiketail drakeling'/'TSP', '118913').

card_in_set('spinneret sliver', 'TSP').
card_original_type('spinneret sliver'/'TSP', 'Creature — Sliver').
card_original_text('spinneret sliver'/'TSP', 'All Slivers have \"This creature can block as though it had flying.\"').
card_first_print('spinneret sliver', 'TSP').
card_image_name('spinneret sliver'/'TSP', 'spinneret sliver').
card_uid('spinneret sliver'/'TSP', 'TSP:Spinneret Sliver:spinneret sliver').
card_rarity('spinneret sliver'/'TSP', 'Common').
card_artist('spinneret sliver'/'TSP', 'Michael Sutfin').
card_number('spinneret sliver'/'TSP', '219').
card_flavor_text('spinneret sliver'/'TSP', 'Each new generation of slivers evolves to assimilate the strengths of the prey upon which their progenitors fed.').
card_multiverse_id('spinneret sliver'/'TSP', '111073').

card_in_set('spirit loop', 'TSP').
card_original_type('spirit loop'/'TSP', 'Enchantment — Aura').
card_original_text('spirit loop'/'TSP', 'Enchant creature you control\nWhenever enchanted creature deals damage, you gain that much life.\nWhen Spirit Loop is put into a graveyard from play, return Spirit Loop to its owner\'s hand.').
card_first_print('spirit loop', 'TSP').
card_image_name('spirit loop'/'TSP', 'spirit loop').
card_uid('spirit loop'/'TSP', 'TSP:Spirit Loop:spirit loop').
card_rarity('spirit loop'/'TSP', 'Uncommon').
card_artist('spirit loop'/'TSP', 'Wayne Reynolds').
card_number('spirit loop'/'TSP', '42').
card_multiverse_id('spirit loop'/'TSP', '109680').

card_in_set('sporesower thallid', 'TSP').
card_original_type('sporesower thallid'/'TSP', 'Creature — Fungus').
card_original_text('sporesower thallid'/'TSP', 'At the beginning of your upkeep, put a spore counter on each Fungus you control.\nRemove three spore counters from Sporesower Thallid: Put a 1/1 green Saproling creature token into play.').
card_first_print('sporesower thallid', 'TSP').
card_image_name('sporesower thallid'/'TSP', 'sporesower thallid').
card_uid('sporesower thallid'/'TSP', 'TSP:Sporesower Thallid:sporesower thallid').
card_rarity('sporesower thallid'/'TSP', 'Uncommon').
card_artist('sporesower thallid'/'TSP', 'Ron Spencer').
card_number('sporesower thallid'/'TSP', '220').
card_multiverse_id('sporesower thallid'/'TSP', '111068').

card_in_set('sprite noble', 'TSP').
card_original_type('sprite noble'/'TSP', 'Creature — Faerie').
card_original_text('sprite noble'/'TSP', 'Flying\nOther creatures you control with flying get +0/+1.\n{T}: Other creatures you control with flying get +1/+0 until end of turn.').
card_first_print('sprite noble', 'TSP').
card_image_name('sprite noble'/'TSP', 'sprite noble').
card_uid('sprite noble'/'TSP', 'TSP:Sprite Noble:sprite noble').
card_rarity('sprite noble'/'TSP', 'Rare').
card_artist('sprite noble'/'TSP', 'Randy Gallegos').
card_number('sprite noble'/'TSP', '81').
card_flavor_text('sprite noble'/'TSP', '\"By thorn and by stone, we will fight to see the day when beauty returns to this world.\"').
card_multiverse_id('sprite noble'/'TSP', '116386').

card_in_set('sprout', 'TSP').
card_original_type('sprout'/'TSP', 'Instant').
card_original_text('sprout'/'TSP', 'Put a 1/1 green Saproling creature token into play.').
card_first_print('sprout', 'TSP').
card_image_name('sprout'/'TSP', 'sprout').
card_uid('sprout'/'TSP', 'TSP:Sprout:sprout').
card_rarity('sprout'/'TSP', 'Common').
card_artist('sprout'/'TSP', 'Anthony S. Waters').
card_number('sprout'/'TSP', '221').
card_flavor_text('sprout'/'TSP', 'Centuries of temporal strife had stripped Dominaria of its natural defenses, but nature fought back with armies constructed of little more than grime and sunlight.').
card_multiverse_id('sprout'/'TSP', '122079').

card_in_set('squall line', 'TSP').
card_original_type('squall line'/'TSP', 'Instant').
card_original_text('squall line'/'TSP', 'Squall Line deals X damage to each creature with flying and each player.').
card_first_print('squall line', 'TSP').
card_image_name('squall line'/'TSP', 'squall line').
card_uid('squall line'/'TSP', 'TSP:Squall Line:squall line').
card_rarity('squall line'/'TSP', 'Rare').
card_artist('squall line'/'TSP', 'Lars Grant-West').
card_number('squall line'/'TSP', '222').
card_flavor_text('squall line'/'TSP', 'The constant shifting of Dominaria\'s shredded timeline played havoc with its atmosphere, combining savage electrical storms from ages past.').
card_multiverse_id('squall line'/'TSP', '109702').

card_in_set('stonebrow, krosan hero', 'TSP').
card_original_type('stonebrow, krosan hero'/'TSP', 'Legendary Creature — Centaur Warrior').
card_original_text('stonebrow, krosan hero'/'TSP', 'Trample\nWhenever a creature you control with trample attacks, it gets +2/+2 until end of turn.').
card_first_print('stonebrow, krosan hero', 'TSP').
card_image_name('stonebrow, krosan hero'/'TSP', 'stonebrow, krosan hero').
card_uid('stonebrow, krosan hero'/'TSP', 'TSP:Stonebrow, Krosan Hero:stonebrow, krosan hero').
card_rarity('stonebrow, krosan hero'/'TSP', 'Rare').
card_artist('stonebrow, krosan hero'/'TSP', 'Ron Spears').
card_number('stonebrow, krosan hero'/'TSP', '247').
card_flavor_text('stonebrow, krosan hero'/'TSP', 'Moments after a fickle rift dropped Stonebrow in the waste of his beloved Krosa, he took up his axe in rage-blind vengeance.').
card_multiverse_id('stonebrow, krosan hero'/'TSP', '118915').

card_in_set('stonewood invocation', 'TSP').
card_original_type('stonewood invocation'/'TSP', 'Instant').
card_original_text('stonewood invocation'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nTarget creature gets +5/+5 until end of turn and can\'t be the target of spells or abilities this turn.').
card_first_print('stonewood invocation', 'TSP').
card_image_name('stonewood invocation'/'TSP', 'stonewood invocation').
card_uid('stonewood invocation'/'TSP', 'TSP:Stonewood Invocation:stonewood invocation').
card_rarity('stonewood invocation'/'TSP', 'Rare').
card_artist('stonewood invocation'/'TSP', 'Pete Venters').
card_number('stonewood invocation'/'TSP', '223').
card_flavor_text('stonewood invocation'/'TSP', '\"Might of ancient wood and stone,\nInto spirit, flesh, and bone.\"').
card_multiverse_id('stonewood invocation'/'TSP', '118890').

card_in_set('stormcloud djinn', 'TSP').
card_original_type('stormcloud djinn'/'TSP', 'Creature — Djinn').
card_original_text('stormcloud djinn'/'TSP', 'Flying\nStormcloud Djinn can block only creatures with flying.\n{R}{R}: Stormcloud Djinn gets +2/+0 until end of turn and deals 1 damage to you.').
card_first_print('stormcloud djinn', 'TSP').
card_image_name('stormcloud djinn'/'TSP', 'stormcloud djinn').
card_uid('stormcloud djinn'/'TSP', 'TSP:Stormcloud Djinn:stormcloud djinn').
card_rarity('stormcloud djinn'/'TSP', 'Uncommon').
card_artist('stormcloud djinn'/'TSP', 'Greg Staples').
card_number('stormcloud djinn'/'TSP', '82').
card_flavor_text('stormcloud djinn'/'TSP', 'As fickle as lightning, as slippery as an eel.').
card_multiverse_id('stormcloud djinn'/'TSP', '118894').

card_in_set('strangling soot', 'TSP').
card_original_type('strangling soot'/'TSP', 'Instant').
card_original_text('strangling soot'/'TSP', 'Destroy target creature with toughness 3 or less.\nFlashback {5}{R} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('strangling soot', 'TSP').
card_image_name('strangling soot'/'TSP', 'strangling soot').
card_uid('strangling soot'/'TSP', 'TSP:Strangling Soot:strangling soot').
card_rarity('strangling soot'/'TSP', 'Common').
card_artist('strangling soot'/'TSP', 'Jim Murray').
card_number('strangling soot'/'TSP', '132').
card_flavor_text('strangling soot'/'TSP', '\"What good is your blade without your breath?\"\n—Greht, Gathan warlord').
card_multiverse_id('strangling soot'/'TSP', '109710').

card_in_set('strength in numbers', 'TSP').
card_original_type('strength in numbers'/'TSP', 'Instant').
card_original_text('strength in numbers'/'TSP', 'Until end of turn, target creature gains trample and gets +X/+X, where X is the number of attacking creatures.').
card_first_print('strength in numbers', 'TSP').
card_image_name('strength in numbers'/'TSP', 'strength in numbers').
card_uid('strength in numbers'/'TSP', 'TSP:Strength in Numbers:strength in numbers').
card_rarity('strength in numbers'/'TSP', 'Common').
card_artist('strength in numbers'/'TSP', 'Ron Spencer').
card_number('strength in numbers'/'TSP', '224').
card_flavor_text('strength in numbers'/'TSP', '\"The hermit dies. The outcast dies. The lone wolf dies. Only those who stick together survive this world.\"\n—Uros, Pendelhaven elder').
card_multiverse_id('strength in numbers'/'TSP', '116730').

card_in_set('stronghold overseer', 'TSP').
card_original_type('stronghold overseer'/'TSP', 'Creature — Demon').
card_original_text('stronghold overseer'/'TSP', 'Flying\nShadow (This creature can block or be blocked by only creatures with shadow.)\n{B}{B}: Creatures with shadow get +1/+0 until end of turn and creatures without shadow get -1/-0 until end of turn.').
card_first_print('stronghold overseer', 'TSP').
card_image_name('stronghold overseer'/'TSP', 'stronghold overseer').
card_uid('stronghold overseer'/'TSP', 'TSP:Stronghold Overseer:stronghold overseer').
card_rarity('stronghold overseer'/'TSP', 'Rare').
card_artist('stronghold overseer'/'TSP', 'Puddnhead').
card_number('stronghold overseer'/'TSP', '133').
card_multiverse_id('stronghold overseer'/'TSP', '109671').

card_in_set('stuffy doll', 'TSP').
card_original_type('stuffy doll'/'TSP', 'Artifact Creature — Construct').
card_original_text('stuffy doll'/'TSP', 'As Stuffy Doll comes into play, choose a player.\nStuffy Doll is indestructible.\nWhenever damage is dealt to Stuffy Doll, it deals that much damage to the chosen player.\n{T}: Stuffy Doll deals 1 damage to itself.').
card_first_print('stuffy doll', 'TSP').
card_image_name('stuffy doll'/'TSP', 'stuffy doll').
card_uid('stuffy doll'/'TSP', 'TSP:Stuffy Doll:stuffy doll').
card_rarity('stuffy doll'/'TSP', 'Rare').
card_artist('stuffy doll'/'TSP', 'Dave Allsop').
card_number('stuffy doll'/'TSP', '264').
card_multiverse_id('stuffy doll'/'TSP', '116724').

card_in_set('subterranean shambler', 'TSP').
card_original_type('subterranean shambler'/'TSP', 'Creature — Elemental').
card_original_text('subterranean shambler'/'TSP', 'Echo {3}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Subterranean Shambler comes into play or leaves play, it deals 1 damage to each creature without flying.').
card_first_print('subterranean shambler', 'TSP').
card_image_name('subterranean shambler'/'TSP', 'subterranean shambler').
card_uid('subterranean shambler'/'TSP', 'TSP:Subterranean Shambler:subterranean shambler').
card_rarity('subterranean shambler'/'TSP', 'Common').
card_artist('subterranean shambler'/'TSP', 'Kev Walker').
card_number('subterranean shambler'/'TSP', '178').
card_multiverse_id('subterranean shambler'/'TSP', '113611').

card_in_set('sudden death', 'TSP').
card_original_type('sudden death'/'TSP', 'Instant').
card_original_text('sudden death'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nTarget creature gets -4/-4 until end of turn.').
card_first_print('sudden death', 'TSP').
card_image_name('sudden death'/'TSP', 'sudden death').
card_uid('sudden death'/'TSP', 'TSP:Sudden Death:sudden death').
card_rarity('sudden death'/'TSP', 'Uncommon').
card_artist('sudden death'/'TSP', 'Dave Allsop').
card_number('sudden death'/'TSP', '134').
card_flavor_text('sudden death'/'TSP', 'Some mages tapped into the time rifts, forcing upon others alternate timelines in which unimaginable horrors befall them.').
card_multiverse_id('sudden death'/'TSP', '108804').

card_in_set('sudden shock', 'TSP').
card_original_type('sudden shock'/'TSP', 'Instant').
card_original_text('sudden shock'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nSudden Shock deals 2 damage to target creature or player.').
card_image_name('sudden shock'/'TSP', 'sudden shock').
card_uid('sudden shock'/'TSP', 'TSP:Sudden Shock:sudden shock').
card_rarity('sudden shock'/'TSP', 'Uncommon').
card_artist('sudden shock'/'TSP', 'Vance Kovacs').
card_number('sudden shock'/'TSP', '179').
card_flavor_text('sudden shock'/'TSP', 'Arc mages aren\'t known for their patience.').
card_multiverse_id('sudden shock'/'TSP', '114911').

card_in_set('sudden spoiling', 'TSP').
card_original_type('sudden spoiling'/'TSP', 'Instant').
card_original_text('sudden spoiling'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nCreatures target player controls become 0/2 and lose all abilities until end of turn.').
card_first_print('sudden spoiling', 'TSP').
card_image_name('sudden spoiling'/'TSP', 'sudden spoiling').
card_uid('sudden spoiling'/'TSP', 'TSP:Sudden Spoiling:sudden spoiling').
card_rarity('sudden spoiling'/'TSP', 'Rare').
card_artist('sudden spoiling'/'TSP', 'Alan Pollack').
card_number('sudden spoiling'/'TSP', '135').
card_multiverse_id('sudden spoiling'/'TSP', '113525').

card_in_set('sulfurous blast', 'TSP').
card_original_type('sulfurous blast'/'TSP', 'Instant').
card_original_text('sulfurous blast'/'TSP', 'Sulfurous Blast deals 2 damage to each creature and each player. If you played this spell during your main phase, Sulfurous Blast deals 3 damage to each creature and each player instead.').
card_first_print('sulfurous blast', 'TSP').
card_image_name('sulfurous blast'/'TSP', 'sulfurous blast').
card_uid('sulfurous blast'/'TSP', 'TSP:Sulfurous Blast:sulfurous blast').
card_rarity('sulfurous blast'/'TSP', 'Uncommon').
card_artist('sulfurous blast'/'TSP', 'Jeff Miracola').
card_number('sulfurous blast'/'TSP', '180').
card_flavor_text('sulfurous blast'/'TSP', 'The Keldons used the toxic vents in the cracked earth to bolster their home\'s defenses.').
card_multiverse_id('sulfurous blast'/'TSP', '109685').

card_in_set('swamp', 'TSP').
card_original_type('swamp'/'TSP', 'Basic Land — Swamp').
card_original_text('swamp'/'TSP', 'B').
card_image_name('swamp'/'TSP', 'swamp1').
card_uid('swamp'/'TSP', 'TSP:Swamp:swamp1').
card_rarity('swamp'/'TSP', 'Basic Land').
card_artist('swamp'/'TSP', 'John Avon').
card_number('swamp'/'TSP', '290').
card_multiverse_id('swamp'/'TSP', '122078').

card_in_set('swamp', 'TSP').
card_original_type('swamp'/'TSP', 'Basic Land — Swamp').
card_original_text('swamp'/'TSP', 'B').
card_image_name('swamp'/'TSP', 'swamp2').
card_uid('swamp'/'TSP', 'TSP:Swamp:swamp2').
card_rarity('swamp'/'TSP', 'Basic Land').
card_artist('swamp'/'TSP', 'Vance Kovacs').
card_number('swamp'/'TSP', '291').
card_multiverse_id('swamp'/'TSP', '122076').

card_in_set('swamp', 'TSP').
card_original_type('swamp'/'TSP', 'Basic Land — Swamp').
card_original_text('swamp'/'TSP', 'B').
card_image_name('swamp'/'TSP', 'swamp3').
card_uid('swamp'/'TSP', 'TSP:Swamp:swamp3').
card_rarity('swamp'/'TSP', 'Basic Land').
card_artist('swamp'/'TSP', 'Craig Mullins').
card_number('swamp'/'TSP', '292').
card_multiverse_id('swamp'/'TSP', '118904').

card_in_set('swamp', 'TSP').
card_original_type('swamp'/'TSP', 'Basic Land — Swamp').
card_original_text('swamp'/'TSP', 'B').
card_image_name('swamp'/'TSP', 'swamp4').
card_uid('swamp'/'TSP', 'TSP:Swamp:swamp4').
card_rarity('swamp'/'TSP', 'Basic Land').
card_artist('swamp'/'TSP', 'Richard Wright').
card_number('swamp'/'TSP', '293').
card_multiverse_id('swamp'/'TSP', '118903').

card_in_set('swarmyard', 'TSP').
card_original_type('swarmyard'/'TSP', 'Land').
card_original_text('swarmyard'/'TSP', '{T}: Add {1} to your mana pool.\n{T}: Regenerate target Insect, Rat, Spider, or Squirrel.').
card_first_print('swarmyard', 'TSP').
card_image_name('swarmyard'/'TSP', 'swarmyard').
card_uid('swarmyard'/'TSP', 'TSP:Swarmyard:swarmyard').
card_rarity('swarmyard'/'TSP', 'Rare').
card_artist('swarmyard'/'TSP', 'Thomas M. Baxa').
card_number('swarmyard'/'TSP', '278').
card_flavor_text('swarmyard'/'TSP', 'Where scavengers nest in hollowed-out rib cages and chittering crawlers peer from empty eye sockets, few dare to trespass.').
card_multiverse_id('swarmyard'/'TSP', '114913').

card_in_set('tectonic fiend', 'TSP').
card_original_type('tectonic fiend'/'TSP', 'Creature — Elemental').
card_original_text('tectonic fiend'/'TSP', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nTectonic Fiend attacks each turn if able.').
card_first_print('tectonic fiend', 'TSP').
card_image_name('tectonic fiend'/'TSP', 'tectonic fiend').
card_uid('tectonic fiend'/'TSP', 'TSP:Tectonic Fiend:tectonic fiend').
card_rarity('tectonic fiend'/'TSP', 'Uncommon').
card_artist('tectonic fiend'/'TSP', 'Mark Tedin').
card_number('tectonic fiend'/'TSP', '181').
card_flavor_text('tectonic fiend'/'TSP', 'The barbarians of Pardia are long dead, but its mountains still smolder with rage.').
card_multiverse_id('tectonic fiend'/'TSP', '113616').

card_in_set('teferi, mage of zhalfir', 'TSP').
card_original_type('teferi, mage of zhalfir'/'TSP', 'Legendary Creature — Human Wizard').
card_original_text('teferi, mage of zhalfir'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nCreature cards you own that aren\'t in play have flash.\nEach opponent can play spells only any time he or she could play a sorcery.').
card_first_print('teferi, mage of zhalfir', 'TSP').
card_image_name('teferi, mage of zhalfir'/'TSP', 'teferi, mage of zhalfir').
card_uid('teferi, mage of zhalfir'/'TSP', 'TSP:Teferi, Mage of Zhalfir:teferi, mage of zhalfir').
card_rarity('teferi, mage of zhalfir'/'TSP', 'Rare').
card_artist('teferi, mage of zhalfir'/'TSP', 'D. Alexander Gregory & Jeremy Jarvis').
card_number('teferi, mage of zhalfir'/'TSP', '83').
card_flavor_text('teferi, mage of zhalfir'/'TSP', 'To save this plane, he must forsake all others.').
card_multiverse_id('teferi, mage of zhalfir'/'TSP', '121271').

card_in_set('telekinetic sliver', 'TSP').
card_original_type('telekinetic sliver'/'TSP', 'Creature — Sliver').
card_original_text('telekinetic sliver'/'TSP', 'All Slivers have \"{T}: Tap target permanent.\"').
card_first_print('telekinetic sliver', 'TSP').
card_image_name('telekinetic sliver'/'TSP', 'telekinetic sliver').
card_uid('telekinetic sliver'/'TSP', 'TSP:Telekinetic Sliver:telekinetic sliver').
card_rarity('telekinetic sliver'/'TSP', 'Uncommon').
card_artist('telekinetic sliver'/'TSP', 'Randy Elliott').
card_number('telekinetic sliver'/'TSP', '84').
card_flavor_text('telekinetic sliver'/'TSP', '\"Slivers are guided only by simple instinct. Advance the hive, and you will be welcomed. Impede the hive, and you will face unrelenting opposition.\"\n—Freyalise').
card_multiverse_id('telekinetic sliver'/'TSP', '111085').

card_in_set('temporal eddy', 'TSP').
card_original_type('temporal eddy'/'TSP', 'Sorcery').
card_original_text('temporal eddy'/'TSP', 'Put target creature or land on top of its owner\'s library.').
card_first_print('temporal eddy', 'TSP').
card_image_name('temporal eddy'/'TSP', 'temporal eddy').
card_uid('temporal eddy'/'TSP', 'TSP:Temporal Eddy:temporal eddy').
card_rarity('temporal eddy'/'TSP', 'Common').
card_artist('temporal eddy'/'TSP', 'Wayne England').
card_number('temporal eddy'/'TSP', '85').
card_flavor_text('temporal eddy'/'TSP', 'As the temporal fractures spread and time itself slowly fell apart, visitors started to appear from across the past and future, and those native to the present began to disappear.').
card_multiverse_id('temporal eddy'/'TSP', '118887').

card_in_set('temporal isolation', 'TSP').
card_original_type('temporal isolation'/'TSP', 'Enchantment — Aura').
card_original_text('temporal isolation'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nEnchant creature\nEnchanted creature has shadow. (It can block or be blocked by only creatures with shadow.)\nPrevent all damage that would be dealt by enchanted creature.').
card_first_print('temporal isolation', 'TSP').
card_image_name('temporal isolation'/'TSP', 'temporal isolation').
card_uid('temporal isolation'/'TSP', 'TSP:Temporal Isolation:temporal isolation').
card_rarity('temporal isolation'/'TSP', 'Common').
card_artist('temporal isolation'/'TSP', 'Stephen Tappin').
card_number('temporal isolation'/'TSP', '43').
card_multiverse_id('temporal isolation'/'TSP', '106654').

card_in_set('tendrils of corruption', 'TSP').
card_original_type('tendrils of corruption'/'TSP', 'Instant').
card_original_text('tendrils of corruption'/'TSP', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_first_print('tendrils of corruption', 'TSP').
card_image_name('tendrils of corruption'/'TSP', 'tendrils of corruption').
card_uid('tendrils of corruption'/'TSP', 'TSP:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'TSP', 'Common').
card_artist('tendrils of corruption'/'TSP', 'Mike Dringenberg').
card_number('tendrils of corruption'/'TSP', '136').
card_flavor_text('tendrils of corruption'/'TSP', '\"Even swamps need sustenance. We will give it to them, and in turn, they will sustain us.\"\n—Ezrith, druid of the Dark Hours').
card_multiverse_id('tendrils of corruption'/'TSP', '106632').

card_in_set('terramorphic expanse', 'TSP').
card_original_type('terramorphic expanse'/'TSP', 'Land').
card_original_text('terramorphic expanse'/'TSP', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it into play tapped. Then shuffle your library.').
card_first_print('terramorphic expanse', 'TSP').
card_image_name('terramorphic expanse'/'TSP', 'terramorphic expanse').
card_uid('terramorphic expanse'/'TSP', 'TSP:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'TSP', 'Common').
card_artist('terramorphic expanse'/'TSP', 'Dan Scott').
card_number('terramorphic expanse'/'TSP', '279').
card_flavor_text('terramorphic expanse'/'TSP', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'TSP', '118874').

card_in_set('thallid germinator', 'TSP').
card_original_type('thallid germinator'/'TSP', 'Creature — Fungus').
card_original_text('thallid germinator'/'TSP', 'At the beginning of your upkeep, put a spore counter on Thallid Germinator.\nRemove three spore counters from Thallid Germinator: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Target creature gets +1/+1 until end of turn.').
card_first_print('thallid germinator', 'TSP').
card_image_name('thallid germinator'/'TSP', 'thallid germinator').
card_uid('thallid germinator'/'TSP', 'TSP:Thallid Germinator:thallid germinator').
card_rarity('thallid germinator'/'TSP', 'Common').
card_artist('thallid germinator'/'TSP', 'Tom Wänerstrand').
card_number('thallid germinator'/'TSP', '225').
card_multiverse_id('thallid germinator'/'TSP', '116381').

card_in_set('thallid shell-dweller', 'TSP').
card_original_type('thallid shell-dweller'/'TSP', 'Creature — Fungus').
card_original_text('thallid shell-dweller'/'TSP', 'Defender\nAt the beginning of your upkeep, put a spore counter on Thallid Shell-Dweller.\nRemove three spore counters from Thallid Shell-Dweller: Put a 1/1 green Saproling creature token into play.').
card_first_print('thallid shell-dweller', 'TSP').
card_image_name('thallid shell-dweller'/'TSP', 'thallid shell-dweller').
card_uid('thallid shell-dweller'/'TSP', 'TSP:Thallid Shell-Dweller:thallid shell-dweller').
card_rarity('thallid shell-dweller'/'TSP', 'Common').
card_artist('thallid shell-dweller'/'TSP', 'Carl Critchlow').
card_number('thallid shell-dweller'/'TSP', '226').
card_multiverse_id('thallid shell-dweller'/'TSP', '116731').

card_in_set('thelon of havenwood', 'TSP').
card_original_type('thelon of havenwood'/'TSP', 'Legendary Creature — Elf Druid').
card_original_text('thelon of havenwood'/'TSP', 'Each Fungus gets +1/+1 for each spore counter on it.\n{B}{G}, Remove a Fungus card in a graveyard from the game: Put a spore counter on each Fungus in play.').
card_first_print('thelon of havenwood', 'TSP').
card_image_name('thelon of havenwood'/'TSP', 'thelon of havenwood').
card_uid('thelon of havenwood'/'TSP', 'TSP:Thelon of Havenwood:thelon of havenwood').
card_rarity('thelon of havenwood'/'TSP', 'Rare').
card_artist('thelon of havenwood'/'TSP', 'Kev Walker').
card_number('thelon of havenwood'/'TSP', '227').
card_flavor_text('thelon of havenwood'/'TSP', '\"The sight of my thallids still thriving is a bittersweet welcome to this cold waste.\"').
card_multiverse_id('thelon of havenwood'/'TSP', '116737').

card_in_set('thelonite hermit', 'TSP').
card_original_type('thelonite hermit'/'TSP', 'Creature — Elf Shaman').
card_original_text('thelonite hermit'/'TSP', 'All Saprolings get +1/+1.\nMorph {3}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thelonite Hermit is turned face up, put four 1/1 green Saproling creature tokens into play.').
card_first_print('thelonite hermit', 'TSP').
card_image_name('thelonite hermit'/'TSP', 'thelonite hermit').
card_uid('thelonite hermit'/'TSP', 'TSP:Thelonite Hermit:thelonite hermit').
card_rarity('thelonite hermit'/'TSP', 'Rare').
card_artist('thelonite hermit'/'TSP', 'Chippy').
card_number('thelonite hermit'/'TSP', '228').
card_multiverse_id('thelonite hermit'/'TSP', '126275').

card_in_set('thick-skinned goblin', 'TSP').
card_original_type('thick-skinned goblin'/'TSP', 'Creature — Goblin Shaman').
card_original_text('thick-skinned goblin'/'TSP', 'You may pay {0} rather than pay the echo cost for permanents you control.\n{R}: Thick-Skinned Goblin gains protection from red until end of turn.').
card_first_print('thick-skinned goblin', 'TSP').
card_image_name('thick-skinned goblin'/'TSP', 'thick-skinned goblin').
card_uid('thick-skinned goblin'/'TSP', 'TSP:Thick-Skinned Goblin:thick-skinned goblin').
card_rarity('thick-skinned goblin'/'TSP', 'Uncommon').
card_artist('thick-skinned goblin'/'TSP', 'Ralph Horsley').
card_number('thick-skinned goblin'/'TSP', '182').
card_flavor_text('thick-skinned goblin'/'TSP', 'The shaman of the tribe is responsible for keeping track of all its treasures, including angry pets and cursed lamps of fiery doom.').
card_multiverse_id('thick-skinned goblin'/'TSP', '109703').

card_in_set('think twice', 'TSP').
card_original_type('think twice'/'TSP', 'Instant').
card_original_text('think twice'/'TSP', 'Draw a card.\nFlashback {2}{U} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('think twice', 'TSP').
card_image_name('think twice'/'TSP', 'think twice').
card_uid('think twice'/'TSP', 'TSP:Think Twice:think twice').
card_rarity('think twice'/'TSP', 'Common').
card_artist('think twice'/'TSP', 'Jim Nelson').
card_number('think twice'/'TSP', '86').
card_flavor_text('think twice'/'TSP', '\"Great books are meant to be read, then read again backwards or upside down!\"\n—Ettovard, Tolarian archivist').
card_multiverse_id('think twice'/'TSP', '108823').

card_in_set('thrill of the hunt', 'TSP').
card_original_type('thrill of the hunt'/'TSP', 'Instant').
card_original_text('thrill of the hunt'/'TSP', 'Target creature gets +1/+2 until end of turn.\nFlashback {W} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('thrill of the hunt', 'TSP').
card_image_name('thrill of the hunt'/'TSP', 'thrill of the hunt').
card_uid('thrill of the hunt'/'TSP', 'TSP:Thrill of the Hunt:thrill of the hunt').
card_rarity('thrill of the hunt'/'TSP', 'Common').
card_artist('thrill of the hunt'/'TSP', 'Stephen Tappin').
card_number('thrill of the hunt'/'TSP', '229').
card_multiverse_id('thrill of the hunt'/'TSP', '109675').

card_in_set('thunder totem', 'TSP').
card_original_type('thunder totem'/'TSP', 'Artifact').
card_original_text('thunder totem'/'TSP', '{T}: Add {W} to your mana pool.\n{1}{W}{W}: Thunder Totem becomes a 2/2 white Spirit artifact creature with flying and first strike until end of turn.').
card_first_print('thunder totem', 'TSP').
card_image_name('thunder totem'/'TSP', 'thunder totem').
card_uid('thunder totem'/'TSP', 'TSP:Thunder Totem:thunder totem').
card_rarity('thunder totem'/'TSP', 'Uncommon').
card_artist('thunder totem'/'TSP', 'Randy Gallegos').
card_number('thunder totem'/'TSP', '265').
card_multiverse_id('thunder totem'/'TSP', '108931').

card_in_set('tivadar of thorn', 'TSP').
card_original_type('tivadar of thorn'/'TSP', 'Legendary Creature — Human Lord').
card_original_text('tivadar of thorn'/'TSP', 'First strike, protection from red\nWhen Tivadar of Thorn comes into play, destroy target Goblin.').
card_first_print('tivadar of thorn', 'TSP').
card_image_name('tivadar of thorn'/'TSP', 'tivadar of thorn').
card_uid('tivadar of thorn'/'TSP', 'TSP:Tivadar of Thorn:tivadar of thorn').
card_rarity('tivadar of thorn'/'TSP', 'Rare').
card_artist('tivadar of thorn'/'TSP', 'Carl Critchlow').
card_number('tivadar of thorn'/'TSP', '44').
card_flavor_text('tivadar of thorn'/'TSP', 'His blade came down upon the neck of a goblin—but not the one he had charged. When Tivadar looked up, the world he had known was gone.').
card_multiverse_id('tivadar of thorn'/'TSP', '116736').

card_in_set('tolarian sentinel', 'TSP').
card_original_type('tolarian sentinel'/'TSP', 'Creature — Human Spellshaper').
card_original_text('tolarian sentinel'/'TSP', 'Flying\n{U}, {T}, Discard a card: Return target permanent you control to its owner\'s hand.').
card_first_print('tolarian sentinel', 'TSP').
card_image_name('tolarian sentinel'/'TSP', 'tolarian sentinel').
card_uid('tolarian sentinel'/'TSP', 'TSP:Tolarian Sentinel:tolarian sentinel').
card_rarity('tolarian sentinel'/'TSP', 'Common').
card_artist('tolarian sentinel'/'TSP', 'Thomas M. Baxa').
card_number('tolarian sentinel'/'TSP', '87').
card_flavor_text('tolarian sentinel'/'TSP', '\"It is not just our people I try to rescue. It is our culture, and our hope that we can return to greatness.\"').
card_multiverse_id('tolarian sentinel'/'TSP', '108907').

card_in_set('traitor\'s clutch', 'TSP').
card_original_type('traitor\'s clutch'/'TSP', 'Instant').
card_original_text('traitor\'s clutch'/'TSP', 'Target creature gets +1/+0, becomes black, and gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)\nFlashback {1}{B} (You may play this card from your graveyard for its flashback cost. Then remove it from the game.)').
card_first_print('traitor\'s clutch', 'TSP').
card_image_name('traitor\'s clutch'/'TSP', 'traitor\'s clutch').
card_uid('traitor\'s clutch'/'TSP', 'TSP:Traitor\'s Clutch:traitor\'s clutch').
card_rarity('traitor\'s clutch'/'TSP', 'Common').
card_artist('traitor\'s clutch'/'TSP', 'Dave Allsop').
card_number('traitor\'s clutch'/'TSP', '137').
card_multiverse_id('traitor\'s clutch'/'TSP', '108857').

card_in_set('trespasser il-vec', 'TSP').
card_original_type('trespasser il-vec'/'TSP', 'Creature — Human Rogue').
card_original_text('trespasser il-vec'/'TSP', 'Discard a card: Trespasser il-Vec gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_first_print('trespasser il-vec', 'TSP').
card_image_name('trespasser il-vec'/'TSP', 'trespasser il-vec').
card_uid('trespasser il-vec'/'TSP', 'TSP:Trespasser il-Vec:trespasser il-vec').
card_rarity('trespasser il-vec'/'TSP', 'Common').
card_artist('trespasser il-vec'/'TSP', 'Jim Murray').
card_number('trespasser il-vec'/'TSP', '138').
card_flavor_text('trespasser il-vec'/'TSP', 'At the epicenter of the Rathi overlay, the City of Traitors did not align with Dominaria. Many of its inhabitants were caught between the two worlds.').
card_multiverse_id('trespasser il-vec'/'TSP', '108841').

card_in_set('trickbind', 'TSP').
card_original_type('trickbind'/'TSP', 'Instant').
card_original_text('trickbind'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nCounter target activated or triggered ability. If a permanent\'s ability is countered this way, activated abilities of that permanent can\'t be played this turn. (Mana abilities can\'t be targeted.)').
card_first_print('trickbind', 'TSP').
card_image_name('trickbind'/'TSP', 'trickbind').
card_uid('trickbind'/'TSP', 'TSP:Trickbind:trickbind').
card_rarity('trickbind'/'TSP', 'Rare').
card_artist('trickbind'/'TSP', 'John Zeleznik').
card_number('trickbind'/'TSP', '88').
card_multiverse_id('trickbind'/'TSP', '110499').

card_in_set('triskelavus', 'TSP').
card_original_type('triskelavus'/'TSP', 'Artifact Creature — Construct').
card_original_text('triskelavus'/'TSP', 'Flying\nTriskelavus comes into play with three +1/+1 counters on it. \n{1}, Remove a +1/+1 counter from Triskelavus: Put a 1/1 Triskelavite artifact creature token with flying into play. It has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"').
card_first_print('triskelavus', 'TSP').
card_image_name('triskelavus'/'TSP', 'triskelavus').
card_uid('triskelavus'/'TSP', 'TSP:Triskelavus:triskelavus').
card_rarity('triskelavus'/'TSP', 'Rare').
card_artist('triskelavus'/'TSP', 'Mark Zug').
card_number('triskelavus'/'TSP', '266').
card_multiverse_id('triskelavus'/'TSP', '114919').

card_in_set('tromp the domains', 'TSP').
card_original_type('tromp the domains'/'TSP', 'Sorcery').
card_original_text('tromp the domains'/'TSP', 'Until end of turn, creatures you control gain trample and get +1/+1 for each basic land type among lands you control.').
card_first_print('tromp the domains', 'TSP').
card_image_name('tromp the domains'/'TSP', 'tromp the domains').
card_uid('tromp the domains'/'TSP', 'TSP:Tromp the Domains:tromp the domains').
card_rarity('tromp the domains'/'TSP', 'Uncommon').
card_artist('tromp the domains'/'TSP', 'Mike Dringenberg').
card_number('tromp the domains'/'TSP', '230').
card_flavor_text('tromp the domains'/'TSP', 'Ground into the footprints of the ravaging herd were clumps of salt from Benalia, moss from Llanowar, dust from Hurloon, and ash from as far as Urborg.').
card_multiverse_id('tromp the domains'/'TSP', '116726').

card_in_set('truth or tale', 'TSP').
card_original_type('truth or tale'/'TSP', 'Instant').
card_original_text('truth or tale'/'TSP', 'Reveal the top five cards of your library and separate them into two piles. An opponent chooses one of those piles. Put a card from the chosen pile into your hand, then put all other cards revealed this way on the bottom of your library in any order.').
card_first_print('truth or tale', 'TSP').
card_image_name('truth or tale'/'TSP', 'truth or tale').
card_uid('truth or tale'/'TSP', 'TSP:Truth or Tale:truth or tale').
card_rarity('truth or tale'/'TSP', 'Uncommon').
card_artist('truth or tale'/'TSP', 'Michael Phillippi').
card_number('truth or tale'/'TSP', '89').
card_multiverse_id('truth or tale'/'TSP', '118910').

card_in_set('two-headed sliver', 'TSP').
card_original_type('two-headed sliver'/'TSP', 'Creature — Sliver').
card_original_text('two-headed sliver'/'TSP', 'All Slivers have \"This creature can\'t be blocked except by two or more creatures.\"').
card_first_print('two-headed sliver', 'TSP').
card_image_name('two-headed sliver'/'TSP', 'two-headed sliver').
card_uid('two-headed sliver'/'TSP', 'TSP:Two-Headed Sliver:two-headed sliver').
card_rarity('two-headed sliver'/'TSP', 'Common').
card_artist('two-headed sliver'/'TSP', 'Dany Orizio').
card_number('two-headed sliver'/'TSP', '183').
card_flavor_text('two-headed sliver'/'TSP', '\"That which would be a fatal mutation in any other species is merely a source of new powers. I am intrigued, yet too fearful to examine it more closely.\"\n—Rukarumel, field journal').
card_multiverse_id('two-headed sliver'/'TSP', '108851').

card_in_set('undying rage', 'TSP').
card_original_type('undying rage'/'TSP', 'Enchantment — Aura').
card_original_text('undying rage'/'TSP', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t block.\nWhen Undying Rage is put into a graveyard from play, return Undying Rage to its owner\'s hand.').
card_first_print('undying rage', 'TSP').
card_image_name('undying rage'/'TSP', 'undying rage').
card_uid('undying rage'/'TSP', 'TSP:Undying Rage:undying rage').
card_rarity('undying rage'/'TSP', 'Uncommon').
card_artist('undying rage'/'TSP', 'Scott M. Fischer').
card_number('undying rage'/'TSP', '184').
card_flavor_text('undying rage'/'TSP', 'Radha was born in Skyshroud, but her temper is undeniably Keldon.').
card_multiverse_id('undying rage'/'TSP', '126280').

card_in_set('unyaro bees', 'TSP').
card_original_type('unyaro bees'/'TSP', 'Creature — Insect').
card_original_text('unyaro bees'/'TSP', 'Flying\n{G}: Unyaro Bees gets +1/+1 until end of turn.\n{3}{G}, Sacrifice Unyaro Bees: Unyaro Bees deals 2 damage to target creature or player.').
card_first_print('unyaro bees', 'TSP').
card_image_name('unyaro bees'/'TSP', 'unyaro bees').
card_uid('unyaro bees'/'TSP', 'TSP:Unyaro Bees:unyaro bees').
card_rarity('unyaro bees'/'TSP', 'Rare').
card_artist('unyaro bees'/'TSP', 'Tom Wänerstrand').
card_number('unyaro bees'/'TSP', '231').
card_flavor_text('unyaro bees'/'TSP', 'With no jungle left to contain it, the \"plague of daggers\" spread across Dominaria.').
card_multiverse_id('unyaro bees'/'TSP', '113579').

card_in_set('urborg syphon-mage', 'TSP').
card_original_type('urborg syphon-mage'/'TSP', 'Creature — Human Spellshaper').
card_original_text('urborg syphon-mage'/'TSP', '{2}{B}, {T}, Discard a card: Each other player loses 2 life. You gain life equal to the life lost this way.').
card_first_print('urborg syphon-mage', 'TSP').
card_image_name('urborg syphon-mage'/'TSP', 'urborg syphon-mage').
card_uid('urborg syphon-mage'/'TSP', 'TSP:Urborg Syphon-Mage:urborg syphon-mage').
card_rarity('urborg syphon-mage'/'TSP', 'Common').
card_artist('urborg syphon-mage'/'TSP', 'Greg Staples').
card_number('urborg syphon-mage'/'TSP', '139').
card_flavor_text('urborg syphon-mage'/'TSP', '\"I have become a gourmet of sorts. Each soul has its own distinctive flavor. The art is in inviting the right company to the feast.\"').
card_multiverse_id('urborg syphon-mage'/'TSP', '108798').

card_in_set('urza\'s factory', 'TSP').
card_original_type('urza\'s factory'/'TSP', 'Land — Urza\'s').
card_original_text('urza\'s factory'/'TSP', '{T}: Add {1} to your mana pool.\n{7}, {T}: Put a 2/2 Assembly-Worker artifact creature token into play.').
card_image_name('urza\'s factory'/'TSP', 'urza\'s factory').
card_uid('urza\'s factory'/'TSP', 'TSP:Urza\'s Factory:urza\'s factory').
card_rarity('urza\'s factory'/'TSP', 'Uncommon').
card_artist('urza\'s factory'/'TSP', 'Mark Tedin').
card_number('urza\'s factory'/'TSP', '280').
card_flavor_text('urza\'s factory'/'TSP', '\"Though their ideals are leagues apart, Urza\'s and Mishra\'s creations have a surprising harmony with one another.\"\n—Tocasia, journal entry').
card_multiverse_id('urza\'s factory'/'TSP', '116384').

card_in_set('vampiric sliver', 'TSP').
card_original_type('vampiric sliver'/'TSP', 'Creature — Sliver').
card_original_text('vampiric sliver'/'TSP', 'All Slivers have \"Whenever a creature dealt damage by this creature this turn is put into a graveyard, put a +1/+1 counter on this creature.\"').
card_first_print('vampiric sliver', 'TSP').
card_image_name('vampiric sliver'/'TSP', 'vampiric sliver').
card_uid('vampiric sliver'/'TSP', 'TSP:Vampiric Sliver:vampiric sliver').
card_rarity('vampiric sliver'/'TSP', 'Uncommon').
card_artist('vampiric sliver'/'TSP', 'Thomas M. Baxa').
card_number('vampiric sliver'/'TSP', '140').
card_flavor_text('vampiric sliver'/'TSP', 'They draw upon the strength of their hive and of their victims.').
card_multiverse_id('vampiric sliver'/'TSP', '118900').

card_in_set('venser\'s sliver', 'TSP').
card_original_type('venser\'s sliver'/'TSP', 'Artifact Creature — Sliver').
card_original_text('venser\'s sliver'/'TSP', '').
card_first_print('venser\'s sliver', 'TSP').
card_image_name('venser\'s sliver'/'TSP', 'venser\'s sliver').
card_uid('venser\'s sliver'/'TSP', 'TSP:Venser\'s Sliver:venser\'s sliver').
card_rarity('venser\'s sliver'/'TSP', 'Common').
card_artist('venser\'s sliver'/'TSP', 'Carl Critchlow').
card_number('venser\'s sliver'/'TSP', '267').
card_flavor_text('venser\'s sliver'/'TSP', 'Venser admired his handiwork and smiled. His first prototype had joined with the hive mind all too well, running with the brood and becoming a predator itself. This one, he thought, would be accepted into the hive but still obey his commands.').
card_multiverse_id('venser\'s sliver'/'TSP', '125866').

card_in_set('verdant embrace', 'TSP').
card_original_type('verdant embrace'/'TSP', 'Enchantment — Aura').
card_original_text('verdant embrace'/'TSP', 'Enchant creature\nEnchanted creature gets +3/+3 and has \"At the beginning of each upkeep, put a 1/1 green Saproling creature token into play under your control.\"').
card_first_print('verdant embrace', 'TSP').
card_image_name('verdant embrace'/'TSP', 'verdant embrace').
card_uid('verdant embrace'/'TSP', 'TSP:Verdant Embrace:verdant embrace').
card_rarity('verdant embrace'/'TSP', 'Rare').
card_artist('verdant embrace'/'TSP', 'Stephen Tappin').
card_number('verdant embrace'/'TSP', '232').
card_multiverse_id('verdant embrace'/'TSP', '109766').

card_in_set('vesuva', 'TSP').
card_original_type('vesuva'/'TSP', 'Land').
card_original_text('vesuva'/'TSP', 'As Vesuva comes into play, you may choose a land in play. If you do, Vesuva comes into play tapped as a copy of the chosen land.').
card_first_print('vesuva', 'TSP').
card_image_name('vesuva'/'TSP', 'vesuva').
card_uid('vesuva'/'TSP', 'TSP:Vesuva:vesuva').
card_rarity('vesuva'/'TSP', 'Rare').
card_artist('vesuva'/'TSP', 'Zoltan Boros & Gabor Szikszai').
card_number('vesuva'/'TSP', '281').
card_flavor_text('vesuva'/'TSP', 'It is everywhere you\'ve ever been.').
card_multiverse_id('vesuva'/'TSP', '113543').

card_in_set('vesuvan shapeshifter', 'TSP').
card_original_type('vesuvan shapeshifter'/'TSP', 'Creature — Shapeshifter').
card_original_text('vesuvan shapeshifter'/'TSP', 'As Vesuvan Shapeshifter comes into play or is turned face up, you may choose another creature in play. If you do, until Vesuvan Shapeshifter is turned face down, it becomes a copy of that creature and gains \"At the beginning of your upkeep, you may turn this creature face down.\"\nMorph {1}{U}').
card_first_print('vesuvan shapeshifter', 'TSP').
card_image_name('vesuvan shapeshifter'/'TSP', 'vesuvan shapeshifter').
card_uid('vesuvan shapeshifter'/'TSP', 'TSP:Vesuvan Shapeshifter:vesuvan shapeshifter').
card_rarity('vesuvan shapeshifter'/'TSP', 'Rare').
card_artist('vesuvan shapeshifter'/'TSP', 'Quinton Hoover').
card_number('vesuvan shapeshifter'/'TSP', '90').
card_multiverse_id('vesuvan shapeshifter'/'TSP', '109765').

card_in_set('viashino bladescout', 'TSP').
card_original_type('viashino bladescout'/'TSP', 'Creature — Viashino Scout').
card_original_text('viashino bladescout'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nWhen Viashino Bladescout comes into play, target creature gains first strike until end of turn.').
card_first_print('viashino bladescout', 'TSP').
card_image_name('viashino bladescout'/'TSP', 'viashino bladescout').
card_uid('viashino bladescout'/'TSP', 'TSP:Viashino Bladescout:viashino bladescout').
card_rarity('viashino bladescout'/'TSP', 'Common').
card_artist('viashino bladescout'/'TSP', 'Dany Orizio').
card_number('viashino bladescout'/'TSP', '185').
card_flavor_text('viashino bladescout'/'TSP', '\"Find your courage in a desperate moment, and you turn the tide of history. So sayeth the bey.\"').
card_multiverse_id('viashino bladescout'/'TSP', '111069').

card_in_set('viscerid deepwalker', 'TSP').
card_original_type('viscerid deepwalker'/'TSP', 'Creature — Homarid Warrior').
card_original_text('viscerid deepwalker'/'TSP', '{U}: Viscerid Deepwalker gets +1/+0 until end of turn.\nSuspend 4—{U} (Rather than play this card from your hand, you may pay {U} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost. It has haste.)').
card_first_print('viscerid deepwalker', 'TSP').
card_image_name('viscerid deepwalker'/'TSP', 'viscerid deepwalker').
card_uid('viscerid deepwalker'/'TSP', 'TSP:Viscerid Deepwalker:viscerid deepwalker').
card_rarity('viscerid deepwalker'/'TSP', 'Common').
card_artist('viscerid deepwalker'/'TSP', 'Heather Hudson').
card_number('viscerid deepwalker'/'TSP', '91').
card_multiverse_id('viscerid deepwalker'/'TSP', '113615').

card_in_set('viscid lemures', 'TSP').
card_original_type('viscid lemures'/'TSP', 'Creature — Spirit').
card_original_text('viscid lemures'/'TSP', '{0}: Viscid Lemures gets -1/-0 and gains swampwalk until end of turn.').
card_first_print('viscid lemures', 'TSP').
card_image_name('viscid lemures'/'TSP', 'viscid lemures').
card_uid('viscid lemures'/'TSP', 'TSP:Viscid Lemures:viscid lemures').
card_rarity('viscid lemures'/'TSP', 'Common').
card_artist('viscid lemures'/'TSP', 'Drew Tucker').
card_number('viscid lemures'/'TSP', '141').
card_flavor_text('viscid lemures'/'TSP', '\"Lemurs? Is that all? Finally, something harmless . . .\"\n—Norin the Wary').
card_multiverse_id('viscid lemures'/'TSP', '111082').

card_in_set('voidmage husher', 'TSP').
card_original_type('voidmage husher'/'TSP', 'Creature — Human Wizard').
card_original_text('voidmage husher'/'TSP', 'Flash (You may play this spell any time you could play an instant.)\nWhen Voidmage Husher comes into play, counter target activated ability. (Mana abilities can\'t be targeted.)\nWhenever you play a spell, you may return Voidmage Husher to its owner\'s hand.').
card_image_name('voidmage husher'/'TSP', 'voidmage husher').
card_uid('voidmage husher'/'TSP', 'TSP:Voidmage Husher:voidmage husher').
card_rarity('voidmage husher'/'TSP', 'Uncommon').
card_artist('voidmage husher'/'TSP', 'Chippy').
card_number('voidmage husher'/'TSP', '92').
card_multiverse_id('voidmage husher'/'TSP', '111076').

card_in_set('volcanic awakening', 'TSP').
card_original_type('volcanic awakening'/'TSP', 'Sorcery').
card_original_text('volcanic awakening'/'TSP', 'Destroy target land.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('volcanic awakening', 'TSP').
card_image_name('volcanic awakening'/'TSP', 'volcanic awakening').
card_uid('volcanic awakening'/'TSP', 'TSP:Volcanic Awakening:volcanic awakening').
card_rarity('volcanic awakening'/'TSP', 'Uncommon').
card_artist('volcanic awakening'/'TSP', 'Dan Scott').
card_number('volcanic awakening'/'TSP', '186').
card_flavor_text('volcanic awakening'/'TSP', 'With a great roar, the land opened like a titan\'s yawn, with teeth of blackened rock and a lolling tongue of magma.').
card_multiverse_id('volcanic awakening'/'TSP', '121206').

card_in_set('walk the aeons', 'TSP').
card_original_type('walk the aeons'/'TSP', 'Sorcery').
card_original_text('walk the aeons'/'TSP', 'Buyback—Sacrifice three Islands. (You may sacrifice three Islands in addition to any other costs as you play this spell. If you do, put this card into your hand as it resolves.)\nTarget player takes an extra turn after this one.').
card_first_print('walk the aeons', 'TSP').
card_image_name('walk the aeons'/'TSP', 'walk the aeons').
card_uid('walk the aeons'/'TSP', 'TSP:Walk the Aeons:walk the aeons').
card_rarity('walk the aeons'/'TSP', 'Rare').
card_artist('walk the aeons'/'TSP', 'Jeremy Jarvis').
card_number('walk the aeons'/'TSP', '93').
card_multiverse_id('walk the aeons'/'TSP', '110517').

card_in_set('watcher sliver', 'TSP').
card_original_type('watcher sliver'/'TSP', 'Creature — Sliver').
card_original_text('watcher sliver'/'TSP', 'All Slivers get +0/+2.').
card_first_print('watcher sliver', 'TSP').
card_image_name('watcher sliver'/'TSP', 'watcher sliver').
card_uid('watcher sliver'/'TSP', 'TSP:Watcher Sliver:watcher sliver').
card_rarity('watcher sliver'/'TSP', 'Common').
card_artist('watcher sliver'/'TSP', 'Liz Danforth').
card_number('watcher sliver'/'TSP', '45').
card_flavor_text('watcher sliver'/'TSP', '\"I have spied them, wandering and watching—but for what? I fear they watch for us.\"\n—Yonat of Amrou').
card_multiverse_id('watcher sliver'/'TSP', '106648').

card_in_set('weathered bodyguards', 'TSP').
card_original_type('weathered bodyguards'/'TSP', 'Creature — Human Soldier').
card_original_text('weathered bodyguards'/'TSP', 'As long as Weathered Bodyguards is untapped, all combat damage that would be dealt to you by unblocked creatures is dealt to Weathered Bodyguards instead.\nMorph {3}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('weathered bodyguards', 'TSP').
card_image_name('weathered bodyguards'/'TSP', 'weathered bodyguards').
card_uid('weathered bodyguards'/'TSP', 'TSP:Weathered Bodyguards:weathered bodyguards').
card_rarity('weathered bodyguards'/'TSP', 'Rare').
card_artist('weathered bodyguards'/'TSP', 'Wayne Reynolds').
card_number('weathered bodyguards'/'TSP', '46').
card_multiverse_id('weathered bodyguards'/'TSP', '126283').

card_in_set('weatherseed totem', 'TSP').
card_original_type('weatherseed totem'/'TSP', 'Artifact').
card_original_text('weatherseed totem'/'TSP', '{T}: Add {G} to your mana pool.\n{2}{G}{G}{G}: Weatherseed Totem becomes a 5/3 green Treefolk artifact creature with trample until end of turn.\nWhen Weatherseed Totem is put into a graveyard from play, if it was a creature, return this card to its owner\'s hand.').
card_first_print('weatherseed totem', 'TSP').
card_image_name('weatherseed totem'/'TSP', 'weatherseed totem').
card_uid('weatherseed totem'/'TSP', 'TSP:Weatherseed Totem:weatherseed totem').
card_rarity('weatherseed totem'/'TSP', 'Uncommon').
card_artist('weatherseed totem'/'TSP', 'Heather Hudson').
card_number('weatherseed totem'/'TSP', '268').
card_multiverse_id('weatherseed totem'/'TSP', '108809').

card_in_set('wheel of fate', 'TSP').
card_original_type('wheel of fate'/'TSP', 'Sorcery').
card_original_text('wheel of fate'/'TSP', 'Wheel of Fate is red.\nSuspend 4—{1}{R} (Rather than play this card from your hand, pay {1}{R} and remove it from the game with four time counters on it. At the beginning of your upkeep, remove a time counter. When you remove the last, play it without paying its mana cost.)\nEach player discards his or her hand, then draws seven cards.').
card_first_print('wheel of fate', 'TSP').
card_image_name('wheel of fate'/'TSP', 'wheel of fate').
card_uid('wheel of fate'/'TSP', 'TSP:Wheel of Fate:wheel of fate').
card_rarity('wheel of fate'/'TSP', 'Rare').
card_artist('wheel of fate'/'TSP', 'Kev Walker').
card_number('wheel of fate'/'TSP', '187').
card_multiverse_id('wheel of fate'/'TSP', '113528').

card_in_set('wipe away', 'TSP').
card_original_type('wipe away'/'TSP', 'Instant').
card_original_text('wipe away'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nReturn target permanent to its owner\'s hand.').
card_first_print('wipe away', 'TSP').
card_image_name('wipe away'/'TSP', 'wipe away').
card_uid('wipe away'/'TSP', 'TSP:Wipe Away:wipe away').
card_rarity('wipe away'/'TSP', 'Uncommon').
card_artist('wipe away'/'TSP', 'Jeff Miracola').
card_number('wipe away'/'TSP', '94').
card_flavor_text('wipe away'/'TSP', 'An eyeblink later, all that remained of the tree-beast was the breeze of its momentum.').
card_multiverse_id('wipe away'/'TSP', '118911').

card_in_set('word of seizing', 'TSP').
card_original_type('word of seizing'/'TSP', 'Instant').
card_original_text('word of seizing'/'TSP', 'Split second (As long as this spell is on the stack, players can\'t play spells or activated abilities that aren\'t mana abilities.)\nUntap target permanent and gain control of it until end of turn. It gains haste until end of turn.').
card_first_print('word of seizing', 'TSP').
card_image_name('word of seizing'/'TSP', 'word of seizing').
card_uid('word of seizing'/'TSP', 'TSP:Word of Seizing:word of seizing').
card_rarity('word of seizing'/'TSP', 'Rare').
card_artist('word of seizing'/'TSP', 'Vance Kovacs').
card_number('word of seizing'/'TSP', '188').
card_flavor_text('word of seizing'/'TSP', '\"Obey!\"').
card_multiverse_id('word of seizing'/'TSP', '118870').

card_in_set('wormwood dryad', 'TSP').
card_original_type('wormwood dryad'/'TSP', 'Creature — Dryad').
card_original_text('wormwood dryad'/'TSP', '{G}: Wormwood Dryad gains forestwalk until end of turn and it deals 1 damage to you.\n{B}: Wormwood Dryad gains swampwalk until end of turn and it deals 1 damage to you.').
card_first_print('wormwood dryad', 'TSP').
card_image_name('wormwood dryad'/'TSP', 'wormwood dryad').
card_uid('wormwood dryad'/'TSP', 'TSP:Wormwood Dryad:wormwood dryad').
card_rarity('wormwood dryad'/'TSP', 'Common').
card_artist('wormwood dryad'/'TSP', 'Warren Mahy').
card_number('wormwood dryad'/'TSP', '233').
card_multiverse_id('wormwood dryad'/'TSP', '113583').

card_in_set('wurmcalling', 'TSP').
card_original_type('wurmcalling'/'TSP', 'Sorcery').
card_original_text('wurmcalling'/'TSP', 'Buyback {2}{G} (You may pay an additional {2}{G} as you play this spell. If you do, put this card into your hand as it resolves.)\nPut an X/X green Wurm creature token into play.').
card_first_print('wurmcalling', 'TSP').
card_image_name('wurmcalling'/'TSP', 'wurmcalling').
card_uid('wurmcalling'/'TSP', 'TSP:Wurmcalling:wurmcalling').
card_rarity('wurmcalling'/'TSP', 'Rare').
card_artist('wurmcalling'/'TSP', 'Jeff Easley').
card_number('wurmcalling'/'TSP', '234').
card_multiverse_id('wurmcalling'/'TSP', '114917').

card_in_set('yavimaya dryad', 'TSP').
card_original_type('yavimaya dryad'/'TSP', 'Creature — Dryad').
card_original_text('yavimaya dryad'/'TSP', 'Forestwalk\nWhen Yavimaya Dryad comes into play, you may search your library for a Forest card and put it into play tapped under target player\'s control. If you do, shuffle your library.').
card_first_print('yavimaya dryad', 'TSP').
card_image_name('yavimaya dryad'/'TSP', 'yavimaya dryad').
card_uid('yavimaya dryad'/'TSP', 'TSP:Yavimaya Dryad:yavimaya dryad').
card_rarity('yavimaya dryad'/'TSP', 'Uncommon').
card_artist('yavimaya dryad'/'TSP', 'Rebecca Guay').
card_number('yavimaya dryad'/'TSP', '235').
card_flavor_text('yavimaya dryad'/'TSP', 'The parched earth still shielded those few souls with the heart to call it home.').
card_multiverse_id('yavimaya dryad'/'TSP', '118884').

card_in_set('zealot il-vec', 'TSP').
card_original_type('zealot il-vec'/'TSP', 'Creature — Human Rebel').
card_original_text('zealot il-vec'/'TSP', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Zealot il-Vec attacks and isn\'t blocked, you may have it deal 1 damage to target creature. If you do, prevent all combat damage Zealot il-Vec would deal this turn.').
card_first_print('zealot il-vec', 'TSP').
card_image_name('zealot il-vec'/'TSP', 'zealot il-vec').
card_uid('zealot il-vec'/'TSP', 'TSP:Zealot il-Vec:zealot il-vec').
card_rarity('zealot il-vec'/'TSP', 'Common').
card_artist('zealot il-vec'/'TSP', 'Chippy').
card_number('zealot il-vec'/'TSP', '47').
card_multiverse_id('zealot il-vec'/'TSP', '108898').
