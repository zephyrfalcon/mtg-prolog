% Vanguard

set('VAN').
set_name('VAN', 'Vanguard').
set_release_date('VAN', '1997-05-01').
set_border('VAN', 'black').
set_type('VAN', 'vanguard').

card_in_set('akroma, angel of wrath avatar', 'VAN').
card_original_type('akroma, angel of wrath avatar'/'VAN', 'Vanguard').
card_original_text('akroma, angel of wrath avatar'/'VAN', 'Whenever a creature enters the battlefield under your control, it gains two of the following abilities chosen at random: flying, first strike, trample, haste, protection from black, protection from red, and vigilance.').
card_first_print('akroma, angel of wrath avatar', 'VAN').
card_image_name('akroma, angel of wrath avatar'/'VAN', 'akroma, angel of wrath avatar').
card_uid('akroma, angel of wrath avatar'/'VAN', 'VAN:Akroma, Angel of Wrath Avatar:akroma, angel of wrath avatar').
card_rarity('akroma, angel of wrath avatar'/'VAN', 'Special').
card_artist('akroma, angel of wrath avatar'/'VAN', 'UDON').
card_number('akroma, angel of wrath avatar'/'VAN', '33').
card_flavor_text('akroma, angel of wrath avatar'/'VAN', '\"Chuck\'s Virtual Party\" avatar (2003)').
card_multiverse_id('akroma, angel of wrath avatar'/'VAN', '182290').

card_in_set('arcanis, the omnipotent avatar', 'VAN').
card_original_type('arcanis, the omnipotent avatar'/'VAN', 'Vanguard').
card_original_text('arcanis, the omnipotent avatar'/'VAN', '{X}, Return a creature you control with converted mana cost X to its owner\'s hand: Draw a number of cards chosen at random between 0 and X. X can\'t be 0.').
card_first_print('arcanis, the omnipotent avatar', 'VAN').
card_image_name('arcanis, the omnipotent avatar'/'VAN', 'arcanis, the omnipotent avatar').
card_uid('arcanis, the omnipotent avatar'/'VAN', 'VAN:Arcanis, the Omnipotent Avatar:arcanis, the omnipotent avatar').
card_rarity('arcanis, the omnipotent avatar'/'VAN', 'Special').
card_artist('arcanis, the omnipotent avatar'/'VAN', 'UDON').
card_number('arcanis, the omnipotent avatar'/'VAN', '70').
card_flavor_text('arcanis, the omnipotent avatar'/'VAN', '10E Participation Avatar').
card_multiverse_id('arcanis, the omnipotent avatar'/'VAN', '182263').

card_in_set('arcbound overseer avatar', 'VAN').
card_original_type('arcbound overseer avatar'/'VAN', 'Vanguard').
card_original_text('arcbound overseer avatar'/'VAN', 'At the beginning of your upkeep, you may put a +1/+1 counter on target creature you control.\nAt the beginning of your upkeep, you may put a charge counter on target permanent you control.').
card_first_print('arcbound overseer avatar', 'VAN').
card_image_name('arcbound overseer avatar'/'VAN', 'arcbound overseer avatar').
card_uid('arcbound overseer avatar'/'VAN', 'VAN:Arcbound Overseer Avatar:arcbound overseer avatar').
card_rarity('arcbound overseer avatar'/'VAN', 'Special').
card_artist('arcbound overseer avatar'/'VAN', 'UDON').
card_number('arcbound overseer avatar'/'VAN', '42').
card_flavor_text('arcbound overseer avatar'/'VAN', 'DST Prize Avatar').
card_multiverse_id('arcbound overseer avatar'/'VAN', '182296').

card_in_set('ashling the pilgrim avatar', 'VAN').
card_original_type('ashling the pilgrim avatar'/'VAN', 'Vanguard').
card_original_text('ashling the pilgrim avatar'/'VAN', '{2}: Ashling the Pilgrim deals 1 damage to each creature and each player.').
card_first_print('ashling the pilgrim avatar', 'VAN').
card_image_name('ashling the pilgrim avatar'/'VAN', 'ashling the pilgrim avatar').
card_uid('ashling the pilgrim avatar'/'VAN', 'VAN:Ashling the Pilgrim Avatar:ashling the pilgrim avatar').
card_rarity('ashling the pilgrim avatar'/'VAN', 'Special').
card_artist('ashling the pilgrim avatar'/'VAN', 'UDON').
card_number('ashling the pilgrim avatar'/'VAN', '73').
card_flavor_text('ashling the pilgrim avatar'/'VAN', 'LRW Participation Avatar').
card_multiverse_id('ashling the pilgrim avatar'/'VAN', '182283').

card_in_set('ashling, the extinguisher avatar', 'VAN').
card_original_type('ashling, the extinguisher avatar'/'VAN', 'Vanguard').
card_original_text('ashling, the extinguisher avatar'/'VAN', '{5}: Destroy all nonland permanents. Activate this ability only once and only during your turn.').
card_first_print('ashling, the extinguisher avatar', 'VAN').
card_image_name('ashling, the extinguisher avatar'/'VAN', 'ashling, the extinguisher avatar').
card_uid('ashling, the extinguisher avatar'/'VAN', 'VAN:Ashling, the Extinguisher Avatar:ashling, the extinguisher avatar').
card_rarity('ashling, the extinguisher avatar'/'VAN', 'Special').
card_artist('ashling, the extinguisher avatar'/'VAN', 'UDON').
card_number('ashling, the extinguisher avatar'/'VAN', '81').
card_flavor_text('ashling, the extinguisher avatar'/'VAN', 'EVE Prize Avatar').
card_multiverse_id('ashling, the extinguisher avatar'/'VAN', '191134').

card_in_set('ashnod', 'VAN').
card_original_type('ashnod'/'VAN', 'Vanguard — Character').
card_original_text('ashnod'/'VAN', 'Whenever a creature successfully damages you, destroy it.').
card_first_print('ashnod', 'VAN').
card_image_name('ashnod'/'VAN', 'ashnod').
card_uid('ashnod'/'VAN', 'VAN:Ashnod:ashnod').
card_rarity('ashnod'/'VAN', 'Special').
card_artist('ashnod'/'VAN', 'Ron Spencer').
card_flavor_text('ashnod'/'VAN', 'Ashnod served as Mishra\'s confidante while he led Fallaji weapons development before the Brothers\' War. Whereas Mishra works in metals and stone, however, Ashnod works in living flesh. She sees all living things as prototype machines; her \"improvements\" to them make her as feared by allies as by enemies.').
card_multiverse_id('ashnod'/'VAN', '12329').

card_in_set('barrin', 'VAN').
card_original_type('barrin'/'VAN', 'Vanguard — Character').
card_original_text('barrin'/'VAN', 'You may sacrifice a permanent to return any creature to its owner\'s hand. Play this ability as an instant.').
card_first_print('barrin', 'VAN').
card_image_name('barrin'/'VAN', 'barrin').
card_uid('barrin'/'VAN', 'VAN:Barrin:barrin').
card_rarity('barrin'/'VAN', 'Special').
card_artist('barrin'/'VAN', 'Christopher Rush').
card_flavor_text('barrin'/'VAN', 'An old and powerful wizard living on the mysterious isle of Tolaria, Barrin is the teacher who trained the young Ertai. He is also Hanna\'s father, but for reasons known only to the two of them neither acknowledges the other—or their strained relationship.').
card_multiverse_id('barrin'/'VAN', '4957').

card_in_set('birds of paradise avatar', 'VAN').
card_original_type('birds of paradise avatar'/'VAN', 'Vanguard').
card_original_text('birds of paradise avatar'/'VAN', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('birds of paradise avatar', 'VAN').
card_image_name('birds of paradise avatar'/'VAN', 'birds of paradise avatar1').
card_uid('birds of paradise avatar'/'VAN', 'VAN:Birds of Paradise Avatar:birds of paradise avatar1').
card_rarity('birds of paradise avatar'/'VAN', 'Special').
card_artist('birds of paradise avatar'/'VAN', 'Marcelo Vignali').
card_number('birds of paradise avatar'/'VAN', '107').
card_flavor_text('birds of paradise avatar'/'VAN', 'Long ago, birds of paradise littered the skies. Thanks to the city\'s sprawl, most now exist as pets of society\'s elite.').
card_multiverse_id('birds of paradise avatar'/'VAN', '214824').

card_in_set('birds of paradise avatar', 'VAN').
card_original_type('birds of paradise avatar'/'VAN', 'Vanguard').
card_original_text('birds of paradise avatar'/'VAN', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_image_name('birds of paradise avatar'/'VAN', 'birds of paradise avatar2').
card_uid('birds of paradise avatar'/'VAN', 'VAN:Birds of Paradise Avatar:birds of paradise avatar2').
card_rarity('birds of paradise avatar'/'VAN', 'Special').
card_artist('birds of paradise avatar'/'VAN', 'UDON').
card_number('birds of paradise avatar'/'VAN', '8').
card_flavor_text('birds of paradise avatar'/'VAN', 'Long ago, birds of paradise littered the skies. Thanks to the city\'s sprawl, most now exist as pets of society\'s elite.').
card_multiverse_id('birds of paradise avatar'/'VAN', '182291').

card_in_set('bosh, iron golem avatar', 'VAN').
card_original_type('bosh, iron golem avatar'/'VAN', 'Vanguard').
card_original_text('bosh, iron golem avatar'/'VAN', '{X}, Sacrifice an artifact with converted mana cost X: Bosh, Iron Golem deals X damage to target creature or player.').
card_first_print('bosh, iron golem avatar', 'VAN').
card_image_name('bosh, iron golem avatar'/'VAN', 'bosh, iron golem avatar').
card_uid('bosh, iron golem avatar'/'VAN', 'VAN:Bosh, Iron Golem Avatar:bosh, iron golem avatar').
card_rarity('bosh, iron golem avatar'/'VAN', 'Special').
card_artist('bosh, iron golem avatar'/'VAN', 'UDON').
card_number('bosh, iron golem avatar'/'VAN', '39').
card_flavor_text('bosh, iron golem avatar'/'VAN', 'MRD Participation Avatar').
card_multiverse_id('bosh, iron golem avatar'/'VAN', '182269').

card_in_set('braids, conjurer adept avatar', 'VAN').
card_original_type('braids, conjurer adept avatar'/'VAN', 'Vanguard').
card_original_text('braids, conjurer adept avatar'/'VAN', '{2}: Each player may put a land card from his or her hand onto the battlefield tapped.\n{3}: Each player may put a noncreature artifact card from his or her hand onto the battlefield.\n{4}: Each player may put a creature card from his or her hand onto the battlefield. Activate this ability only any time you could cast a sorcery.').
card_first_print('braids, conjurer adept avatar', 'VAN').
card_image_name('braids, conjurer adept avatar'/'VAN', 'braids, conjurer adept avatar').
card_uid('braids, conjurer adept avatar'/'VAN', 'VAN:Braids, Conjurer Adept Avatar:braids, conjurer adept avatar').
card_rarity('braids, conjurer adept avatar'/'VAN', 'Special').
card_artist('braids, conjurer adept avatar'/'VAN', 'UDON').
card_number('braids, conjurer adept avatar'/'VAN', '66').
card_flavor_text('braids, conjurer adept avatar'/'VAN', 'PLC Participation Avatar').
card_multiverse_id('braids, conjurer adept avatar'/'VAN', '182272').

card_in_set('chronatog avatar', 'VAN').
card_original_type('chronatog avatar'/'VAN', 'Vanguard').
card_original_text('chronatog avatar'/'VAN', 'You have no maximum hand size.\n{0}: Draw three cards. You skip your next turn. Activate this ability only once each turn.').
card_first_print('chronatog avatar', 'VAN').
card_image_name('chronatog avatar'/'VAN', 'chronatog avatar').
card_uid('chronatog avatar'/'VAN', 'VAN:Chronatog Avatar:chronatog avatar').
card_rarity('chronatog avatar'/'VAN', 'Special').
card_artist('chronatog avatar'/'VAN', 'UDON').
card_number('chronatog avatar'/'VAN', '59').
card_flavor_text('chronatog avatar'/'VAN', 'VI Prize Avatar').
card_multiverse_id('chronatog avatar'/'VAN', '182286').

card_in_set('crovax', 'VAN').
card_original_type('crovax'/'VAN', 'Vanguard — Character').
card_original_text('crovax'/'VAN', 'Whenever any of your creatures damages any creature or player, gain 1 life.').
card_first_print('crovax', 'VAN').
card_image_name('crovax'/'VAN', 'crovax').
card_uid('crovax'/'VAN', 'VAN:Crovax:crovax').
card_rarity('crovax'/'VAN', 'Special').
card_artist('crovax'/'VAN', 'Ron Spencer').
card_flavor_text('crovax'/'VAN', 'How does one escape a curse? For Crovax, a wealthy noble, the answer may lie in joining the crew of the Weatherlight as they seek to rescue their abducted captain Sisay.  Secretly, he hopes in his journeys to find Selenia, the angelic being who abandoned him and his family years ago.').
card_multiverse_id('crovax'/'VAN', '4958').

card_in_set('dakkon blackblade avatar', 'VAN').
card_original_type('dakkon blackblade avatar'/'VAN', 'Vanguard').
card_original_text('dakkon blackblade avatar'/'VAN', 'You may play any colored card from your hand as a copy of a basic land card chosen at random that can produce mana of one of the card\'s colors.').
card_first_print('dakkon blackblade avatar', 'VAN').
card_image_name('dakkon blackblade avatar'/'VAN', 'dakkon blackblade avatar').
card_uid('dakkon blackblade avatar'/'VAN', 'VAN:Dakkon Blackblade Avatar:dakkon blackblade avatar').
card_rarity('dakkon blackblade avatar'/'VAN', 'Special').
card_artist('dakkon blackblade avatar'/'VAN', 'UDON').
card_number('dakkon blackblade avatar'/'VAN', '72').
card_flavor_text('dakkon blackblade avatar'/'VAN', 'MED Participation Avatar').
card_multiverse_id('dakkon blackblade avatar'/'VAN', '182273').

card_in_set('dauntless escort avatar', 'VAN').
card_original_type('dauntless escort avatar'/'VAN', 'Vanguard').
card_original_text('dauntless escort avatar'/'VAN', 'Creatures you control have exalted.').
card_first_print('dauntless escort avatar', 'VAN').
card_image_name('dauntless escort avatar'/'VAN', 'dauntless escort avatar').
card_uid('dauntless escort avatar'/'VAN', 'VAN:Dauntless Escort Avatar:dauntless escort avatar').
card_rarity('dauntless escort avatar'/'VAN', 'Special').
card_artist('dauntless escort avatar'/'VAN', 'UDON').
card_number('dauntless escort avatar'/'VAN', '94').
card_flavor_text('dauntless escort avatar'/'VAN', 'ARB Participation Avatar').
card_multiverse_id('dauntless escort avatar'/'VAN', '201898').

card_in_set('diamond faerie avatar', 'VAN').
card_original_type('diamond faerie avatar'/'VAN', 'Vanguard').
card_original_text('diamond faerie avatar'/'VAN', ': Target creature you control gets +1/+1 until end of turn.').
card_first_print('diamond faerie avatar', 'VAN').
card_image_name('diamond faerie avatar'/'VAN', 'diamond faerie avatar').
card_uid('diamond faerie avatar'/'VAN', 'VAN:Diamond Faerie Avatar:diamond faerie avatar').
card_rarity('diamond faerie avatar'/'VAN', 'Special').
card_artist('diamond faerie avatar'/'VAN', 'UDON').
card_number('diamond faerie avatar'/'VAN', '63').
card_flavor_text('diamond faerie avatar'/'VAN', 'CSP Participation Avatar').
card_multiverse_id('diamond faerie avatar'/'VAN', '182274').

card_in_set('eight-and-a-half-tails avatar', 'VAN').
card_original_type('eight-and-a-half-tails avatar'/'VAN', 'Vanguard').
card_original_text('eight-and-a-half-tails avatar'/'VAN', '{1}: Until end of turn, target permanent you control gains protection from a color chosen at random from colors it doesn\'t have protection from.').
card_first_print('eight-and-a-half-tails avatar', 'VAN').
card_image_name('eight-and-a-half-tails avatar'/'VAN', 'eight-and-a-half-tails avatar').
card_uid('eight-and-a-half-tails avatar'/'VAN', 'VAN:Eight-and-a-Half-Tails Avatar:eight-and-a-half-tails avatar').
card_rarity('eight-and-a-half-tails avatar'/'VAN', 'Special').
card_artist('eight-and-a-half-tails avatar'/'VAN', 'UDON').
card_number('eight-and-a-half-tails avatar'/'VAN', '45').
card_flavor_text('eight-and-a-half-tails avatar'/'VAN', 'CHK Prize Avatar').
card_multiverse_id('eight-and-a-half-tails avatar'/'VAN', '182294').

card_in_set('eladamri', 'VAN').
card_original_type('eladamri'/'VAN', 'Vanguard — Character').
card_original_text('eladamri'/'VAN', 'You may redirect to yourself any amount of damage dealt to creatures you control.').
card_first_print('eladamri', 'VAN').
card_image_name('eladamri'/'VAN', 'eladamri').
card_uid('eladamri'/'VAN', 'VAN:Eladamri:eladamri').
card_rarity('eladamri'/'VAN', 'Special').
card_artist('eladamri'/'VAN', 'Mark Zug').
card_flavor_text('eladamri'/'VAN', 'Proud, shrewd, and charismatic, Eladamri has a commitment to his people that is absolute. The guerilla war he wages against Volrath has convinced him that the only way to liberate the Skyshroud elves is to lead all Rath\'s refugees to freedom. So be it.').
card_multiverse_id('eladamri'/'VAN', '12140').

card_in_set('eladamri, lord of leaves avatar', 'VAN').
card_original_type('eladamri, lord of leaves avatar'/'VAN', 'Vanguard').
card_original_text('eladamri, lord of leaves avatar'/'VAN', 'At the beginning of each player\'s precombat main phase, that player adds {G}{G} to his or her mana pool.').
card_first_print('eladamri, lord of leaves avatar', 'VAN').
card_image_name('eladamri, lord of leaves avatar'/'VAN', 'eladamri, lord of leaves avatar').
card_uid('eladamri, lord of leaves avatar'/'VAN', 'VAN:Eladamri, Lord of Leaves Avatar:eladamri, lord of leaves avatar').
card_rarity('eladamri, lord of leaves avatar'/'VAN', 'Special').
card_artist('eladamri, lord of leaves avatar'/'VAN', 'UDON').
card_number('eladamri, lord of leaves avatar'/'VAN', '88').
card_flavor_text('eladamri, lord of leaves avatar'/'VAN', 'TE Participation Avatar').
card_multiverse_id('eladamri, lord of leaves avatar'/'VAN', '195137').

card_in_set('elvish champion avatar', 'VAN').
card_original_type('elvish champion avatar'/'VAN', 'Vanguard').
card_original_text('elvish champion avatar'/'VAN', 'You begin the game with a 1/1 green Elf creature token on the battlefield that has \"{T}: Add {G} to your mana pool.\"').
card_first_print('elvish champion avatar', 'VAN').
card_image_name('elvish champion avatar'/'VAN', 'elvish champion avatar').
card_uid('elvish champion avatar'/'VAN', 'VAN:Elvish Champion Avatar:elvish champion avatar').
card_rarity('elvish champion avatar'/'VAN', 'Special').
card_artist('elvish champion avatar'/'VAN', 'UDON').
card_number('elvish champion avatar'/'VAN', '37').
card_flavor_text('elvish champion avatar'/'VAN', '8ED Participation Avatar').
card_multiverse_id('elvish champion avatar'/'VAN', '182299').

card_in_set('enigma sphinx avatar', 'VAN').
card_original_type('enigma sphinx avatar'/'VAN', 'Vanguard').
card_original_text('enigma sphinx avatar'/'VAN', 'Whenever you cast a colored artifact spell for the first time in a turn, search your library for a colored artifact card chosen at random whose converted mana cost is less than that spell\'s converted mana cost. You may cast that card without paying its mana cost. If you don\'t, put that card on the bottom of your library.').
card_first_print('enigma sphinx avatar', 'VAN').
card_image_name('enigma sphinx avatar'/'VAN', 'enigma sphinx avatar').
card_uid('enigma sphinx avatar'/'VAN', 'VAN:Enigma Sphinx Avatar:enigma sphinx avatar').
card_rarity('enigma sphinx avatar'/'VAN', 'Special').
card_artist('enigma sphinx avatar'/'VAN', 'UDON').
card_number('enigma sphinx avatar'/'VAN', '95').
card_flavor_text('enigma sphinx avatar'/'VAN', 'ARB Prize Avatar').
card_multiverse_id('enigma sphinx avatar'/'VAN', '201901').

card_in_set('erhnam djinn avatar', 'VAN').
card_original_type('erhnam djinn avatar'/'VAN', 'Vanguard').
card_original_text('erhnam djinn avatar'/'VAN', 'Whenever you cast a creature spell, put a 1/1 green Saproling creature token onto the battlefield.').
card_first_print('erhnam djinn avatar', 'VAN').
card_image_name('erhnam djinn avatar'/'VAN', 'erhnam djinn avatar1').
card_uid('erhnam djinn avatar'/'VAN', 'VAN:Erhnam Djinn Avatar:erhnam djinn avatar1').
card_rarity('erhnam djinn avatar'/'VAN', 'Special').
card_artist('erhnam djinn avatar'/'VAN', 'Greg Staples').
card_number('erhnam djinn avatar'/'VAN', '27').
card_flavor_text('erhnam djinn avatar'/'VAN', 'He provides a safe path to nowhere.').
card_multiverse_id('erhnam djinn avatar'/'VAN', '182259').

card_in_set('erhnam djinn avatar', 'VAN').
card_original_type('erhnam djinn avatar'/'VAN', 'Vanguard').
card_original_text('erhnam djinn avatar'/'VAN', 'Whenever you cast a creature spell, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('erhnam djinn avatar'/'VAN', 'erhnam djinn avatar2').
card_uid('erhnam djinn avatar'/'VAN', 'VAN:Erhnam Djinn Avatar:erhnam djinn avatar2').
card_rarity('erhnam djinn avatar'/'VAN', 'Special').
card_artist('erhnam djinn avatar'/'VAN', 'UDON').
card_number('erhnam djinn avatar'/'VAN', '28').
card_flavor_text('erhnam djinn avatar'/'VAN', 'He provides a safe path to nowhere.').
card_multiverse_id('erhnam djinn avatar'/'VAN', '210148').

card_in_set('ertai', 'VAN').
card_original_type('ertai'/'VAN', 'Vanguard — Character').
card_original_text('ertai'/'VAN', 'Your creatures can\'t be the target of your opponents\' spells or abilities.').
card_first_print('ertai', 'VAN').
card_image_name('ertai'/'VAN', 'ertai').
card_uid('ertai'/'VAN', 'VAN:Ertai:ertai').
card_rarity('ertai'/'VAN', 'Special').
card_artist('ertai'/'VAN', 'Randy Gallegos').
card_flavor_text('ertai'/'VAN', 'After serving his apprenticeship under Barrin of Tolaria, Ertai graced the Weatherlight crew with his presence as the ship\'s \"resident\" wizard. He realizes that few recognize his greatness—but then how could they, when they lack his insight and wisdom?').
card_multiverse_id('ertai'/'VAN', '4959').

card_in_set('etched oracle avatar', 'VAN').
card_original_type('etched oracle avatar'/'VAN', 'Vanguard').
card_original_text('etched oracle avatar'/'VAN', 'You may pay {W}{U}{B}{R}{G} rather than pay the mana cost for spells that you cast.').
card_first_print('etched oracle avatar', 'VAN').
card_image_name('etched oracle avatar'/'VAN', 'etched oracle avatar').
card_uid('etched oracle avatar'/'VAN', 'VAN:Etched Oracle Avatar:etched oracle avatar').
card_rarity('etched oracle avatar'/'VAN', 'Special').
card_artist('etched oracle avatar'/'VAN', 'UDON').
card_number('etched oracle avatar'/'VAN', '44').
card_flavor_text('etched oracle avatar'/'VAN', '5DN Participation Avatar').
card_multiverse_id('etched oracle avatar'/'VAN', '182298').

card_in_set('fallen angel avatar', 'VAN').
card_original_type('fallen angel avatar'/'VAN', 'Vanguard').
card_original_text('fallen angel avatar'/'VAN', 'Whenever a creature you control is put into a graveyard from the battlefield, target opponent loses 1 life and you gain 1 life.').
card_first_print('fallen angel avatar', 'VAN').
card_image_name('fallen angel avatar'/'VAN', 'fallen angel avatar').
card_uid('fallen angel avatar'/'VAN', 'VAN:Fallen Angel Avatar:fallen angel avatar').
card_rarity('fallen angel avatar'/'VAN', 'Special').
card_artist('fallen angel avatar'/'VAN', 'UDON').
card_number('fallen angel avatar'/'VAN', '9').
card_flavor_text('fallen angel avatar'/'VAN', '2004 Holiday Event Participation Avatar').
card_multiverse_id('fallen angel avatar'/'VAN', '182303').

card_in_set('figure of destiny avatar', 'VAN').
card_original_type('figure of destiny avatar'/'VAN', 'Vanguard').
card_original_text('figure of destiny avatar'/'VAN', '{X}: Put a +1/+1 counter on target creature with fewer than X +1/+1 counters on it.').
card_first_print('figure of destiny avatar', 'VAN').
card_image_name('figure of destiny avatar'/'VAN', 'figure of destiny avatar').
card_uid('figure of destiny avatar'/'VAN', 'VAN:Figure of Destiny Avatar:figure of destiny avatar').
card_rarity('figure of destiny avatar'/'VAN', 'Special').
card_artist('figure of destiny avatar'/'VAN', 'UDON').
card_number('figure of destiny avatar'/'VAN', '82').
card_flavor_text('figure of destiny avatar'/'VAN', 'EVE Participation Avatar').
card_multiverse_id('figure of destiny avatar'/'VAN', '191136').

card_in_set('flametongue kavu avatar', 'VAN').
card_original_type('flametongue kavu avatar'/'VAN', 'Vanguard').
card_original_text('flametongue kavu avatar'/'VAN', 'Whenever a nontoken creature enters the battlefield under your control, that creature deals X damage to target creature, where X is a number chosen at random from 0 to 4.').
card_first_print('flametongue kavu avatar', 'VAN').
card_image_name('flametongue kavu avatar'/'VAN', 'flametongue kavu avatar').
card_uid('flametongue kavu avatar'/'VAN', 'VAN:Flametongue Kavu Avatar:flametongue kavu avatar').
card_rarity('flametongue kavu avatar'/'VAN', 'Special').
card_artist('flametongue kavu avatar'/'VAN', 'UDON').
card_number('flametongue kavu avatar'/'VAN', '14').
card_flavor_text('flametongue kavu avatar'/'VAN', 'Awarded in celebration of the second birthday of Magic Online').
card_multiverse_id('flametongue kavu avatar'/'VAN', '182280').

card_in_set('frenetic efreet avatar', 'VAN').
card_original_type('frenetic efreet avatar'/'VAN', 'Vanguard').
card_original_text('frenetic efreet avatar'/'VAN', 'Permanents you control have phasing.\nAt the beginning of your end step, flip a coin. If you win the flip, take an extra turn after this one.').
card_first_print('frenetic efreet avatar', 'VAN').
card_image_name('frenetic efreet avatar'/'VAN', 'frenetic efreet avatar').
card_uid('frenetic efreet avatar'/'VAN', 'VAN:Frenetic Efreet Avatar:frenetic efreet avatar').
card_rarity('frenetic efreet avatar'/'VAN', 'Special').
card_artist('frenetic efreet avatar'/'VAN', 'UDON').
card_number('frenetic efreet avatar'/'VAN', '53').
card_flavor_text('frenetic efreet avatar'/'VAN', 'MI Prize Avatar').
card_multiverse_id('frenetic efreet avatar'/'VAN', '182275').

card_in_set('gerrard', 'VAN').
card_original_type('gerrard'/'VAN', 'Vanguard — Character').
card_original_text('gerrard'/'VAN', 'During your draw phase, draw an additional card.').
card_first_print('gerrard', 'VAN').
card_image_name('gerrard'/'VAN', 'gerrard').
card_uid('gerrard'/'VAN', 'VAN:Gerrard:gerrard').
card_rarity('gerrard'/'VAN', 'Special').
card_artist('gerrard'/'VAN', 'Douglas Shuler').
card_flavor_text('gerrard'/'VAN', 'Soldier. Adventurer. Heir to the Legacy. Gerrard has, over the years, traveled much of Dominaria in search of fortune and glory. Now, after serving nobly in the Benalish army, he has returned to the Weatherlight to serve as captain in Sisay\'s absence and to take up the battle against the Lord of the Wastes.').
card_multiverse_id('gerrard'/'VAN', '4960').

card_in_set('gix', 'VAN').
card_original_type('gix'/'VAN', 'Vanguard — Character').
card_original_text('gix'/'VAN', '{3}: Return target creature card from your graveyard to your hand.').
card_first_print('gix', 'VAN').
card_image_name('gix'/'VAN', 'gix').
card_uid('gix'/'VAN', 'VAN:Gix:gix').
card_rarity('gix'/'VAN', 'Special').
card_artist('gix'/'VAN', 'Pete Venters').
card_flavor_text('gix'/'VAN', 'Through ruthlessness and raw ambition, Gix rose through the Phyrexian regime to the exalted rank of praetor. Even while overseeing many of Phyrexia\'s most important undertakings, Gix continues his own quest for supremacy . . . for in Phyrexia, the weak become fodder for the strong.').
card_multiverse_id('gix'/'VAN', '12330').

card_in_set('goblin warchief avatar', 'VAN').
card_original_type('goblin warchief avatar'/'VAN', 'Vanguard').
card_original_text('goblin warchief avatar'/'VAN', 'Attacking creatures you control get +1/+0.').
card_first_print('goblin warchief avatar', 'VAN').
card_image_name('goblin warchief avatar'/'VAN', 'goblin warchief avatar1').
card_uid('goblin warchief avatar'/'VAN', 'VAN:Goblin Warchief Avatar:goblin warchief avatar1').
card_rarity('goblin warchief avatar'/'VAN', 'Special').
card_artist('goblin warchief avatar'/'VAN', 'Tim Hildebrandt').
card_number('goblin warchief avatar'/'VAN', '5').
card_flavor_text('goblin warchief avatar'/'VAN', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief avatar'/'VAN', '182251').

card_in_set('goblin warchief avatar', 'VAN').
card_original_type('goblin warchief avatar'/'VAN', 'Vanguard').
card_original_text('goblin warchief avatar'/'VAN', 'Attacking creatures you control get +1/+0.').
card_image_name('goblin warchief avatar'/'VAN', 'goblin warchief avatar2').
card_uid('goblin warchief avatar'/'VAN', 'VAN:Goblin Warchief Avatar:goblin warchief avatar2').
card_rarity('goblin warchief avatar'/'VAN', 'Special').
card_artist('goblin warchief avatar'/'VAN', 'UDON').
card_number('goblin warchief avatar'/'VAN', '6').
card_flavor_text('goblin warchief avatar'/'VAN', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief avatar'/'VAN', '210149').

card_in_set('greven il-vec', 'VAN').
card_original_type('greven il-vec'/'VAN', 'Vanguard — Character').
card_original_text('greven il-vec'/'VAN', 'Whenever any of your creatures damages any creature, bury the damaged creature.').
card_first_print('greven il-vec', 'VAN').
card_image_name('greven il-vec'/'VAN', 'greven il-vec').
card_uid('greven il-vec'/'VAN', 'VAN:Greven il-Vec:greven il-vec').
card_rarity('greven il-vec'/'VAN', 'Special').
card_artist('greven il-vec'/'VAN', 'Mark Tedin').
card_flavor_text('greven il-vec'/'VAN', 'As a ruthless leader for Volrath, Greven il-Vec serves his lord by commanding the flying ship Predator. Greven has attained his powerful rank through a series of underhanded schemes and assassinations, the majority of which were blessed by Volrath himself.').
card_multiverse_id('greven il-vec'/'VAN', '4961').

card_in_set('grinning demon avatar', 'VAN').
card_original_type('grinning demon avatar'/'VAN', 'Vanguard').
card_original_text('grinning demon avatar'/'VAN', 'Whenever a nontoken creature you control is put into a graveyard from the battlefield, target opponent discards a card.').
card_first_print('grinning demon avatar', 'VAN').
card_image_name('grinning demon avatar'/'VAN', 'grinning demon avatar1').
card_uid('grinning demon avatar'/'VAN', 'VAN:Grinning Demon Avatar:grinning demon avatar1').
card_rarity('grinning demon avatar'/'VAN', 'Special').
card_artist('grinning demon avatar'/'VAN', 'Mark Zug').
card_number('grinning demon avatar'/'VAN', '29').
card_flavor_text('grinning demon avatar'/'VAN', 'It\'s drawn to the scent of screaming.').
card_multiverse_id('grinning demon avatar'/'VAN', '182304').

card_in_set('grinning demon avatar', 'VAN').
card_original_type('grinning demon avatar'/'VAN', 'Vanguard').
card_original_text('grinning demon avatar'/'VAN', 'Whenever a nontoken creature you control is put into a graveyard from the battlefield, target opponent discards a card.').
card_image_name('grinning demon avatar'/'VAN', 'grinning demon avatar2').
card_uid('grinning demon avatar'/'VAN', 'VAN:Grinning Demon Avatar:grinning demon avatar2').
card_rarity('grinning demon avatar'/'VAN', 'Special').
card_artist('grinning demon avatar'/'VAN', 'UDON').
card_number('grinning demon avatar'/'VAN', '30').
card_flavor_text('grinning demon avatar'/'VAN', 'It\'s drawn to the scent of screaming.').
card_multiverse_id('grinning demon avatar'/'VAN', '210150').

card_in_set('haakon, stromgald scourge avatar', 'VAN').
card_original_type('haakon, stromgald scourge avatar'/'VAN', 'Vanguard').
card_original_text('haakon, stromgald scourge avatar'/'VAN', 'Pay 1 life: You may play target creature card in your graveyard this turn.\nWhenever you play a creature card from your graveyard, it becomes a black Zombie Knight.\nIf a Zombie Knight would be put into your graveyard from the battlefield, exile it instead.').
card_first_print('haakon, stromgald scourge avatar', 'VAN').
card_image_name('haakon, stromgald scourge avatar'/'VAN', 'haakon, stromgald scourge avatar').
card_uid('haakon, stromgald scourge avatar'/'VAN', 'VAN:Haakon, Stromgald Scourge Avatar:haakon, stromgald scourge avatar').
card_rarity('haakon, stromgald scourge avatar'/'VAN', 'Special').
card_artist('haakon, stromgald scourge avatar'/'VAN', 'UDON').
card_number('haakon, stromgald scourge avatar'/'VAN', '62').
card_flavor_text('haakon, stromgald scourge avatar'/'VAN', 'CSP Prize Avatar').
card_multiverse_id('haakon, stromgald scourge avatar'/'VAN', '182287').

card_in_set('hanna', 'VAN').
card_original_type('hanna'/'VAN', 'Vanguard — Character').
card_original_text('hanna'/'VAN', 'Your spells cost {1} less to play.').
card_first_print('hanna', 'VAN').
card_image_name('hanna'/'VAN', 'hanna').
card_uid('hanna'/'VAN', 'VAN:Hanna:hanna').
card_rarity('hanna'/'VAN', 'Special').
card_artist('hanna'/'VAN', 'Liz Danforth').
card_flavor_text('hanna'/'VAN', 'Educated in Argive and highly knowledgeable in the field of artifacts, this expert navigator of the Weatherlight believes that scholarship does not necessarily lead to understanding. Her diligence in pursuing both is her greatest asset.').
card_multiverse_id('hanna'/'VAN', '4962').

card_in_set('heartwood storyteller avatar', 'VAN').
card_original_type('heartwood storyteller avatar'/'VAN', 'Vanguard').
card_original_text('heartwood storyteller avatar'/'VAN', 'The first creature spell you cast each turn costs {1} less to cast.\nThe first noncreature spell each opponent casts each turn costs {1} more to cast.').
card_first_print('heartwood storyteller avatar', 'VAN').
card_image_name('heartwood storyteller avatar'/'VAN', 'heartwood storyteller avatar').
card_uid('heartwood storyteller avatar'/'VAN', 'VAN:Heartwood Storyteller Avatar:heartwood storyteller avatar').
card_rarity('heartwood storyteller avatar'/'VAN', 'Special').
card_artist('heartwood storyteller avatar'/'VAN', 'UDON').
card_number('heartwood storyteller avatar'/'VAN', '68').
card_flavor_text('heartwood storyteller avatar'/'VAN', 'FUT Prize Avatar').
card_multiverse_id('heartwood storyteller avatar'/'VAN', '182257').

card_in_set('hell\'s caretaker avatar', 'VAN').
card_original_type('hell\'s caretaker avatar'/'VAN', 'Vanguard').
card_original_text('hell\'s caretaker avatar'/'VAN', '{3}, Sacrifice a creature: Return target creature card from your graveyard to the battlefield.').
card_first_print('hell\'s caretaker avatar', 'VAN').
card_image_name('hell\'s caretaker avatar'/'VAN', 'hell\'s caretaker avatar').
card_uid('hell\'s caretaker avatar'/'VAN', 'VAN:Hell\'s Caretaker Avatar:hell\'s caretaker avatar').
card_rarity('hell\'s caretaker avatar'/'VAN', 'Special').
card_artist('hell\'s caretaker avatar'/'VAN', 'UDON').
card_number('hell\'s caretaker avatar'/'VAN', '51').
card_flavor_text('hell\'s caretaker avatar'/'VAN', '9ED Participation Avatar').
card_multiverse_id('hell\'s caretaker avatar'/'VAN', '182264').

card_in_set('hermit druid avatar', 'VAN').
card_original_type('hermit druid avatar'/'VAN', 'Vanguard').
card_original_text('hermit druid avatar'/'VAN', 'At the beginning of your upkeep, put a land card from your library chosen at random onto the battlefield.').
card_first_print('hermit druid avatar', 'VAN').
card_image_name('hermit druid avatar'/'VAN', 'hermit druid avatar').
card_uid('hermit druid avatar'/'VAN', 'VAN:Hermit Druid Avatar:hermit druid avatar').
card_rarity('hermit druid avatar'/'VAN', 'Special').
card_artist('hermit druid avatar'/'VAN', 'UDON').
card_number('hermit druid avatar'/'VAN', '93').
card_flavor_text('hermit druid avatar'/'VAN', 'ST Prize Avatar').
card_multiverse_id('hermit druid avatar'/'VAN', '201076').

card_in_set('higure, the still wind avatar', 'VAN').
card_original_type('higure, the still wind avatar'/'VAN', 'Vanguard').
card_original_text('higure, the still wind avatar'/'VAN', 'Whenever a nontoken creature you control deals combat damage to an opponent, choose a creature card at random from your library, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('higure, the still wind avatar', 'VAN').
card_image_name('higure, the still wind avatar'/'VAN', 'higure, the still wind avatar').
card_uid('higure, the still wind avatar'/'VAN', 'VAN:Higure, the Still Wind Avatar:higure, the still wind avatar').
card_rarity('higure, the still wind avatar'/'VAN', 'Special').
card_artist('higure, the still wind avatar'/'VAN', 'UDON').
card_number('higure, the still wind avatar'/'VAN', '47').
card_flavor_text('higure, the still wind avatar'/'VAN', 'BOK Participation Avatar').
card_multiverse_id('higure, the still wind avatar'/'VAN', '182270').

card_in_set('ink-eyes, servant of oni avatar', 'VAN').
card_original_type('ink-eyes, servant of oni avatar'/'VAN', 'Vanguard').
card_original_text('ink-eyes, servant of oni avatar'/'VAN', 'At the beginning of the game, look at target opponent\'s hand and choose a nonland card from it. That player discards that card.\n{X}, Pay X life: Put target creature card with converted mana cost X in an opponent\'s graveyard onto the battlefield under your control.').
card_first_print('ink-eyes, servant of oni avatar', 'VAN').
card_image_name('ink-eyes, servant of oni avatar'/'VAN', 'ink-eyes, servant of oni avatar').
card_uid('ink-eyes, servant of oni avatar'/'VAN', 'VAN:Ink-Eyes, Servant of Oni Avatar:ink-eyes, servant of oni avatar').
card_rarity('ink-eyes, servant of oni avatar'/'VAN', 'Special').
card_artist('ink-eyes, servant of oni avatar'/'VAN', 'UDON').
card_number('ink-eyes, servant of oni avatar'/'VAN', '48').
card_flavor_text('ink-eyes, servant of oni avatar'/'VAN', 'BOK Prize Avatar').
card_multiverse_id('ink-eyes, servant of oni avatar'/'VAN', '182268').

card_in_set('jaya ballard avatar', 'VAN').
card_original_type('jaya ballard avatar'/'VAN', 'Vanguard').
card_original_text('jaya ballard avatar'/'VAN', '{X}: Jaya Ballard deals an amount of damage chosen at random from 0 to X to target creature or player. Activate this ability only once each turn.').
card_first_print('jaya ballard avatar', 'VAN').
card_image_name('jaya ballard avatar'/'VAN', 'jaya ballard avatar').
card_uid('jaya ballard avatar'/'VAN', 'VAN:Jaya Ballard Avatar:jaya ballard avatar').
card_rarity('jaya ballard avatar'/'VAN', 'Special').
card_artist('jaya ballard avatar'/'VAN', 'UDON').
card_number('jaya ballard avatar'/'VAN', '64').
card_flavor_text('jaya ballard avatar'/'VAN', 'TSP Prize Avatar').
card_multiverse_id('jaya ballard avatar'/'VAN', '182255').

card_in_set('jhoira of the ghitu avatar', 'VAN').
card_original_type('jhoira of the ghitu avatar'/'VAN', 'Vanguard').
card_original_text('jhoira of the ghitu avatar'/'VAN', '{3}, Discard a card: Copy three instant cards chosen at random. You may cast one of the copies without paying its mana cost.\n{3}, Discard a card: Copy three sorcery cards chosen at random. You may cast one of the copies without paying its mana cost. Activate this ability only any time you could cast a sorcery.').
card_first_print('jhoira of the ghitu avatar', 'VAN').
card_image_name('jhoira of the ghitu avatar'/'VAN', 'jhoira of the ghitu avatar').
card_uid('jhoira of the ghitu avatar'/'VAN', 'VAN:Jhoira of the Ghitu Avatar:jhoira of the ghitu avatar').
card_rarity('jhoira of the ghitu avatar'/'VAN', 'Special').
card_artist('jhoira of the ghitu avatar'/'VAN', 'UDON').
card_number('jhoira of the ghitu avatar'/'VAN', '69').
card_flavor_text('jhoira of the ghitu avatar'/'VAN', 'FUT Particiption Avatar').
card_multiverse_id('jhoira of the ghitu avatar'/'VAN', '182252').

card_in_set('karn', 'VAN').
card_original_type('karn'/'VAN', 'Vanguard — Character').
card_original_text('karn'/'VAN', 'Each of your noncreature artifacts is also an artifact creature with power and toughness each equal to its total casting cost.').
card_first_print('karn', 'VAN').
card_image_name('karn'/'VAN', 'karn').
card_uid('karn'/'VAN', 'VAN:Karn:karn').
card_rarity('karn'/'VAN', 'Special').
card_artist('karn'/'VAN', 'Anthony S. Waters').
card_flavor_text('karn'/'VAN', 'Both part of the Legacy and an individual in his own right, the silver golem Karn is haunted by a single, terrible incident: while serving to protect the young Gerrard, Karn accidentally took the life of another. Wakened from years spent as a statue, the golem has vowed to never kill again as he rejoins Gerrard.').
card_multiverse_id('karn'/'VAN', '4963').

card_in_set('karona, false god avatar', 'VAN').
card_original_type('karona, false god avatar'/'VAN', 'Vanguard').
card_original_text('karona, false god avatar'/'VAN', 'At the beginning of your upkeep, exchange control of a permanent you control chosen at random and a permanent target opponent controls chosen at random.').
card_first_print('karona, false god avatar', 'VAN').
card_image_name('karona, false god avatar'/'VAN', 'karona, false god avatar').
card_uid('karona, false god avatar'/'VAN', 'VAN:Karona, False God Avatar:karona, false god avatar').
card_rarity('karona, false god avatar'/'VAN', 'Special').
card_artist('karona, false god avatar'/'VAN', 'UDON').
card_number('karona, false god avatar'/'VAN', '34').
card_flavor_text('karona, false god avatar'/'VAN', 'SCG Participation Avatar').
card_multiverse_id('karona, false god avatar'/'VAN', '182265').

card_in_set('kresh the bloodbraided avatar', 'VAN').
card_original_type('kresh the bloodbraided avatar'/'VAN', 'Vanguard').
card_original_text('kresh the bloodbraided avatar'/'VAN', 'Whenever a creature you control is devoured, put an X/X green Ooze creature token onto the battlefield, where X is the devoured creature\'s power.').
card_first_print('kresh the bloodbraided avatar', 'VAN').
card_image_name('kresh the bloodbraided avatar'/'VAN', 'kresh the bloodbraided avatar').
card_uid('kresh the bloodbraided avatar'/'VAN', 'VAN:Kresh the Bloodbraided Avatar:kresh the bloodbraided avatar').
card_rarity('kresh the bloodbraided avatar'/'VAN', 'Special').
card_artist('kresh the bloodbraided avatar'/'VAN', 'UDON').
card_number('kresh the bloodbraided avatar'/'VAN', '87').
card_flavor_text('kresh the bloodbraided avatar'/'VAN', 'ALA Prize Avatar').
card_multiverse_id('kresh the bloodbraided avatar'/'VAN', '193854').

card_in_set('loxodon hierarch avatar', 'VAN').
card_original_type('loxodon hierarch avatar'/'VAN', 'Vanguard').
card_original_text('loxodon hierarch avatar'/'VAN', 'Sacrifice a permanent: Regenerate target creature you control.').
card_first_print('loxodon hierarch avatar', 'VAN').
card_image_name('loxodon hierarch avatar'/'VAN', 'loxodon hierarch avatar').
card_uid('loxodon hierarch avatar'/'VAN', 'VAN:Loxodon Hierarch Avatar:loxodon hierarch avatar').
card_rarity('loxodon hierarch avatar'/'VAN', 'Special').
card_artist('loxodon hierarch avatar'/'VAN', 'UDON').
card_number('loxodon hierarch avatar'/'VAN', '54').
card_flavor_text('loxodon hierarch avatar'/'VAN', 'RAV Participation Avatar').
card_multiverse_id('loxodon hierarch avatar'/'VAN', '182266').

card_in_set('lyna', 'VAN').
card_original_type('lyna'/'VAN', 'Vanguard — Character').
card_original_text('lyna'/'VAN', 'All creatures you control gain shadow. (Those creatures can block and be blocked only by creatures with shadow.)').
card_first_print('lyna', 'VAN').
card_image_name('lyna'/'VAN', 'lyna').
card_uid('lyna'/'VAN', 'VAN:Lyna:lyna').
card_rarity('lyna'/'VAN', 'Special').
card_artist('lyna'/'VAN', 'Adam Rex').
card_flavor_text('lyna'/'VAN', 'One of the few Soltari who can cross the shadow barrier, Lyna serves as an emissary to the outside world. A quick thinker and convincing speaker, she does whatever she can to lead her people out of the shadows and into substance.').
card_multiverse_id('lyna'/'VAN', '12142').

card_in_set('lyzolda, the blood witch avatar', 'VAN').
card_original_type('lyzolda, the blood witch avatar'/'VAN', 'Vanguard').
card_original_text('lyzolda, the blood witch avatar'/'VAN', 'Hellbent — As long as you have no cards in hand, if a source you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.\nHellbent — At the beginning of your end step, if you have no cards in hand, each of your opponents discards a card.').
card_first_print('lyzolda, the blood witch avatar', 'VAN').
card_image_name('lyzolda, the blood witch avatar'/'VAN', 'lyzolda, the blood witch avatar').
card_uid('lyzolda, the blood witch avatar'/'VAN', 'VAN:Lyzolda, the Blood Witch Avatar:lyzolda, the blood witch avatar').
card_rarity('lyzolda, the blood witch avatar'/'VAN', 'Special').
card_artist('lyzolda, the blood witch avatar'/'VAN', 'UDON').
card_number('lyzolda, the blood witch avatar'/'VAN', '60').
card_flavor_text('lyzolda, the blood witch avatar'/'VAN', 'DIS Prize Avatar').
card_multiverse_id('lyzolda, the blood witch avatar'/'VAN', '182302').

card_in_set('maelstrom archangel avatar', 'VAN').
card_original_type('maelstrom archangel avatar'/'VAN', 'Vanguard').
card_original_text('maelstrom archangel avatar'/'VAN', 'At end of combat, for each creature you controlled at the time it dealt combat damage to a player this turn, copy a random card with the same mana cost as that creature. You may pay {3}. If you do, choose one of those copies. If a copy of a permanent card is chosen, you may put a token onto the battlefield that\'s a copy of that card. If a copy of an instant or sorcery card is chosen, you may cast the copy without paying its mana cost.').
card_first_print('maelstrom archangel avatar', 'VAN').
card_image_name('maelstrom archangel avatar'/'VAN', 'maelstrom archangel avatar').
card_uid('maelstrom archangel avatar'/'VAN', 'VAN:Maelstrom Archangel Avatar:maelstrom archangel avatar').
card_rarity('maelstrom archangel avatar'/'VAN', 'Special').
card_artist('maelstrom archangel avatar'/'VAN', 'UDON').
card_number('maelstrom archangel avatar'/'VAN', '91').
card_flavor_text('maelstrom archangel avatar'/'VAN', 'CON Prize Avatar').
card_multiverse_id('maelstrom archangel avatar'/'VAN', '198628').

card_in_set('malfegor avatar', 'VAN').
card_original_type('malfegor avatar'/'VAN', 'Vanguard').
card_original_text('malfegor avatar'/'VAN', 'Whenever a creature enters the battlefield under your control, if it was unearthed, it gets +3/+0.\nWhenever a creature you control is exiled, if it was unearthed, shuffle that card into its owner\'s library.').
card_first_print('malfegor avatar', 'VAN').
card_image_name('malfegor avatar'/'VAN', 'malfegor avatar').
card_uid('malfegor avatar'/'VAN', 'VAN:Malfegor Avatar:malfegor avatar').
card_rarity('malfegor avatar'/'VAN', 'Special').
card_artist('malfegor avatar'/'VAN', 'UDON').
card_number('malfegor avatar'/'VAN', '90').
card_flavor_text('malfegor avatar'/'VAN', 'CON Participation Avatar').
card_multiverse_id('malfegor avatar'/'VAN', '198627').

card_in_set('maralen of the mornsong avatar', 'VAN').
card_original_type('maralen of the mornsong avatar'/'VAN', 'Vanguard').
card_original_text('maralen of the mornsong avatar'/'VAN', 'At the beginning of the game, you may pay any amount of life.\nYou can\'t draw cards.\nAt the beginning of your draw step, look at the top X cards of your library, where X is the amount of life paid with Maralen of the Mornsong. Put one of them into your hand, then shuffle your library.').
card_first_print('maralen of the mornsong avatar', 'VAN').
card_image_name('maralen of the mornsong avatar'/'VAN', 'maralen of the mornsong avatar').
card_uid('maralen of the mornsong avatar'/'VAN', 'VAN:Maralen of the Mornsong Avatar:maralen of the mornsong avatar').
card_rarity('maralen of the mornsong avatar'/'VAN', 'Special').
card_artist('maralen of the mornsong avatar'/'VAN', 'UDON').
card_number('maralen of the mornsong avatar'/'VAN', '78').
card_flavor_text('maralen of the mornsong avatar'/'VAN', 'MOR Prize Avatar').
card_multiverse_id('maralen of the mornsong avatar'/'VAN', '182262').

card_in_set('maraxus', 'VAN').
card_original_type('maraxus'/'VAN', 'Vanguard — Character').
card_original_text('maraxus'/'VAN', 'Your creatures get +1/+0.').
card_first_print('maraxus', 'VAN').
card_image_name('maraxus'/'VAN', 'maraxus').
card_uid('maraxus'/'VAN', 'VAN:Maraxus:maraxus').
card_rarity('maraxus'/'VAN', 'Special').
card_artist('maraxus'/'VAN', 'Matthew D. Wilson').
card_flavor_text('maraxus'/'VAN', 'Too chaotic for even the other warlords of Keld, Maraxus has devoted himself to serving as a hired killer for the sinister Volrath. A man fond of his work, he\'s more than willing to put in those extra hours needed to perfect his craft.').
card_multiverse_id('maraxus'/'VAN', '4964').

card_in_set('maro avatar', 'VAN').
card_original_type('maro avatar'/'VAN', 'Vanguard').
card_original_text('maro avatar'/'VAN', 'Tap an untapped creature you control, Discard a card: Target creature you control gets +X/+X until end of turn, where X is the number of cards in your hand.').
card_first_print('maro avatar', 'VAN').
card_image_name('maro avatar'/'VAN', 'maro avatar').
card_uid('maro avatar'/'VAN', 'VAN:Maro Avatar:maro avatar').
card_rarity('maro avatar'/'VAN', 'Special').
card_artist('maro avatar'/'VAN', 'UDON').
card_number('maro avatar'/'VAN', '52').
card_flavor_text('maro avatar'/'VAN', '9ED Prize Avatar').
card_multiverse_id('maro avatar'/'VAN', '182297').

card_in_set('master of the wild hunt avatar', 'VAN').
card_original_type('master of the wild hunt avatar'/'VAN', 'Vanguard').
card_original_text('master of the wild hunt avatar'/'VAN', '{2}{G}: Put a green creature token onto the battlefield that\'s a 2/2 Wolf, a 2/3 Antelope with forestwalk, a 3/2 Cat with shroud, or a 4/4 green Rhino with trample, chosen at random.').
card_first_print('master of the wild hunt avatar', 'VAN').
card_image_name('master of the wild hunt avatar'/'VAN', 'master of the wild hunt avatar').
card_uid('master of the wild hunt avatar'/'VAN', 'VAN:Master of the Wild Hunt Avatar:master of the wild hunt avatar').
card_rarity('master of the wild hunt avatar'/'VAN', 'Special').
card_artist('master of the wild hunt avatar'/'VAN', 'UDON').
card_number('master of the wild hunt avatar'/'VAN', '96').
card_flavor_text('master of the wild hunt avatar'/'VAN', 'M10 Participation Avatar').
card_multiverse_id('master of the wild hunt avatar'/'VAN', '205956').

card_in_set('mayael the anima avatar', 'VAN').
card_original_type('mayael the anima avatar'/'VAN', 'Vanguard').
card_original_text('mayael the anima avatar'/'VAN', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card with power 5 or greater, put it into your hand. Otherwise, you may put it on the bottom of your library.').
card_first_print('mayael the anima avatar', 'VAN').
card_image_name('mayael the anima avatar'/'VAN', 'mayael the anima avatar').
card_uid('mayael the anima avatar'/'VAN', 'VAN:Mayael the Anima Avatar:mayael the anima avatar').
card_rarity('mayael the anima avatar'/'VAN', 'Special').
card_artist('mayael the anima avatar'/'VAN', 'UDON').
card_number('mayael the anima avatar'/'VAN', '86').
card_flavor_text('mayael the anima avatar'/'VAN', 'ALA Participation Avatar').
card_multiverse_id('mayael the anima avatar'/'VAN', '193853').

card_in_set('mirri', 'VAN').
card_original_type('mirri'/'VAN', 'Vanguard — Character').
card_original_text('mirri'/'VAN', 'Each of your basic lands may be tapped to produce any color of mana instead of its normal type.').
card_first_print('mirri', 'VAN').
card_image_name('mirri'/'VAN', 'mirri').
card_uid('mirri'/'VAN', 'VAN:Mirri:mirri').
card_rarity('mirri'/'VAN', 'Special').
card_artist('mirri'/'VAN', 'Richard Kane Ferguson').
card_flavor_text('mirri'/'VAN', 'Mirri and Gerrard have been the best of friends since they trained together under the maro-sorcerer Multani. A fierce and agile warrior, she rejoined Gerrard aboard the Weatherlight as his unofficial first mate shortly after he assumed command of the flying ship.').
card_multiverse_id('mirri'/'VAN', '4965').

card_in_set('mirri the cursed avatar', 'VAN').
card_original_type('mirri the cursed avatar'/'VAN', 'Vanguard').
card_original_text('mirri the cursed avatar'/'VAN', 'Creatures you control have \"{T}: Another target creature gets -1/-1 until end of turn. Put a +1/+1 counter on this creature.\"').
card_first_print('mirri the cursed avatar', 'VAN').
card_image_name('mirri the cursed avatar'/'VAN', 'mirri the cursed avatar').
card_uid('mirri the cursed avatar'/'VAN', 'VAN:Mirri the Cursed Avatar:mirri the cursed avatar').
card_rarity('mirri the cursed avatar'/'VAN', 'Special').
card_artist('mirri the cursed avatar'/'VAN', 'UDON').
card_number('mirri the cursed avatar'/'VAN', '67').
card_flavor_text('mirri the cursed avatar'/'VAN', 'PLC Prize Avatar').
card_multiverse_id('mirri the cursed avatar'/'VAN', '182279').

card_in_set('mirror entity avatar', 'VAN').
card_original_type('mirror entity avatar'/'VAN', 'Vanguard').
card_original_text('mirror entity avatar'/'VAN', '{X}: Choose a creature type. Until end of turn, creatures you control of the chosen type become X/X and gain all creature types.').
card_first_print('mirror entity avatar', 'VAN').
card_image_name('mirror entity avatar'/'VAN', 'mirror entity avatar').
card_uid('mirror entity avatar'/'VAN', 'VAN:Mirror Entity Avatar:mirror entity avatar').
card_rarity('mirror entity avatar'/'VAN', 'Special').
card_artist('mirror entity avatar'/'VAN', 'UDON').
card_number('mirror entity avatar'/'VAN', '74').
card_flavor_text('mirror entity avatar'/'VAN', 'LRW Prize Avatar').
card_multiverse_id('mirror entity avatar'/'VAN', '182284').

card_in_set('mishra', 'VAN').
card_original_type('mishra'/'VAN', 'Vanguard — Character').
card_original_text('mishra'/'VAN', 'Double all damage dealt by creatures you control.').
card_first_print('mishra', 'VAN').
card_image_name('mishra'/'VAN', 'mishra').
card_uid('mishra'/'VAN', 'VAN:Mishra:mishra').
card_rarity('mishra'/'VAN', 'Special').
card_artist('mishra'/'VAN', 'Anson Maddocks').
card_flavor_text('mishra'/'VAN', 'Both complement and antithesis to his older brother Urza, Mishra also mastered the intricacies of artifice. It was Mishra\'s strength that first gave rise to the massive Phyrexian war engines on Dominaria, and it was his weakness that pushed him, Urza, and Dominaria itself to the point of annihilation.').
card_multiverse_id('mishra'/'VAN', '12327').

card_in_set('momir vig, simic visionary avatar', 'VAN').
card_original_type('momir vig, simic visionary avatar'/'VAN', 'Vanguard').
card_original_text('momir vig, simic visionary avatar'/'VAN', '{X}, Discard a card: Put a token onto the battlefield that\'s a copy of a creature card with converted mana cost X chosen at random. Activate this ability only any time you could cast a sorcery and only once each turn.').
card_first_print('momir vig, simic visionary avatar', 'VAN').
card_image_name('momir vig, simic visionary avatar'/'VAN', 'momir vig, simic visionary avatar').
card_uid('momir vig, simic visionary avatar'/'VAN', 'VAN:Momir Vig, Simic Visionary Avatar:momir vig, simic visionary avatar').
card_rarity('momir vig, simic visionary avatar'/'VAN', 'Special').
card_artist('momir vig, simic visionary avatar'/'VAN', 'UDON').
card_number('momir vig, simic visionary avatar'/'VAN', '61').
card_flavor_text('momir vig, simic visionary avatar'/'VAN', 'DIS Participation Avatar').
card_multiverse_id('momir vig, simic visionary avatar'/'VAN', '182271').

card_in_set('morinfen avatar', 'VAN').
card_original_type('morinfen avatar'/'VAN', 'Vanguard').
card_original_text('morinfen avatar'/'VAN', 'At the beginning of your upkeep, you lose 1 life for each permanent you control.').
card_first_print('morinfen avatar', 'VAN').
card_image_name('morinfen avatar'/'VAN', 'morinfen avatar').
card_uid('morinfen avatar'/'VAN', 'VAN:Morinfen Avatar:morinfen avatar').
card_rarity('morinfen avatar'/'VAN', 'Special').
card_artist('morinfen avatar'/'VAN', 'UDON').
card_number('morinfen avatar'/'VAN', '76').
card_flavor_text('morinfen avatar'/'VAN', 'WL Prize Avatar').
card_multiverse_id('morinfen avatar'/'VAN', '182300').

card_in_set('multani', 'VAN').
card_original_type('multani'/'VAN', 'Vanguard — Character').
card_original_text('multani'/'VAN', 'All creatures you control get +X/+0, where X is the number of cards in your hand.').
card_first_print('multani', 'VAN').
card_image_name('multani'/'VAN', 'multani').
card_uid('multani'/'VAN', 'VAN:Multani:multani').
card_rarity('multani'/'VAN', 'Special').
card_artist('multani'/'VAN', 'Brom').
card_flavor_text('multani'/'VAN', 'In mentoring apprentice mages, the forest spirit Multani teaches each to respect nature while making use of its gifts. Multani taught Gerrard, Mirri, and Rofellos to hone their spellcasting in tandem with more physical disciplines so that both would be improved.').
card_multiverse_id('multani'/'VAN', '12146').

card_in_set('murderous redcap avatar', 'VAN').
card_original_type('murderous redcap avatar'/'VAN', 'Vanguard').
card_original_text('murderous redcap avatar'/'VAN', 'Whenever a creature enters the battlefield under your control with a counter on it, you may have it deal damage equal to its power to target creature or player.').
card_first_print('murderous redcap avatar', 'VAN').
card_image_name('murderous redcap avatar'/'VAN', 'murderous redcap avatar').
card_uid('murderous redcap avatar'/'VAN', 'VAN:Murderous Redcap Avatar:murderous redcap avatar').
card_rarity('murderous redcap avatar'/'VAN', 'Special').
card_artist('murderous redcap avatar'/'VAN', 'UDON').
card_number('murderous redcap avatar'/'VAN', '80').
card_flavor_text('murderous redcap avatar'/'VAN', 'SHM Participation Avatar').
card_multiverse_id('murderous redcap avatar'/'VAN', '191137').

card_in_set('necropotence avatar', 'VAN').
card_original_type('necropotence avatar'/'VAN', 'Vanguard').
card_original_text('necropotence avatar'/'VAN', 'Skip your draw step.\nAt the beginning of your end step, if it\'s not the first turn of the game, put a death counter on Necropotence. You draw X cards and you lose X life, where X is the number of death counters on Necropotence.').
card_first_print('necropotence avatar', 'VAN').
card_image_name('necropotence avatar'/'VAN', 'necropotence avatar').
card_uid('necropotence avatar'/'VAN', 'VAN:Necropotence Avatar:necropotence avatar').
card_rarity('necropotence avatar'/'VAN', 'Special').
card_artist('necropotence avatar'/'VAN', 'UDON').
card_number('necropotence avatar'/'VAN', '83').
card_flavor_text('necropotence avatar'/'VAN', 'ME2 Prize Avatar').
card_multiverse_id('necropotence avatar'/'VAN', '191306').

card_in_set('nekrataal avatar', 'VAN').
card_original_type('nekrataal avatar'/'VAN', 'Vanguard').
card_original_text('nekrataal avatar'/'VAN', 'Creature spells you cast cost {B} less to cast. This effect reduces only the amount of colored mana you pay.').
card_first_print('nekrataal avatar', 'VAN').
card_image_name('nekrataal avatar'/'VAN', 'nekrataal avatar').
card_uid('nekrataal avatar'/'VAN', 'VAN:Nekrataal Avatar:nekrataal avatar').
card_rarity('nekrataal avatar'/'VAN', 'Special').
card_artist('nekrataal avatar'/'VAN', 'UDON').
card_number('nekrataal avatar'/'VAN', '58').
card_flavor_text('nekrataal avatar'/'VAN', 'VI Participation Avatar').
card_multiverse_id('nekrataal avatar'/'VAN', '182267').

card_in_set('oni of wild places avatar', 'VAN').
card_original_type('oni of wild places avatar'/'VAN', 'Vanguard').
card_original_text('oni of wild places avatar'/'VAN', 'Creatures you control have haste.\nAt the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_first_print('oni of wild places avatar', 'VAN').
card_image_name('oni of wild places avatar'/'VAN', 'oni of wild places avatar').
card_uid('oni of wild places avatar'/'VAN', 'VAN:Oni of Wild Places Avatar:oni of wild places avatar').
card_rarity('oni of wild places avatar'/'VAN', 'Special').
card_artist('oni of wild places avatar'/'VAN', 'UDON').
card_number('oni of wild places avatar'/'VAN', '50').
card_flavor_text('oni of wild places avatar'/'VAN', 'SOK Participation Avatar').
card_multiverse_id('oni of wild places avatar'/'VAN', '182253').

card_in_set('oracle', 'VAN').
card_original_type('oracle'/'VAN', 'Vanguard — Character').
card_original_text('oracle'/'VAN', 'You may untap any attacking creatures you control and remove them from combat.').
card_first_print('oracle', 'VAN').
card_image_name('oracle'/'VAN', 'oracle').
card_uid('oracle'/'VAN', 'VAN:Oracle:oracle').
card_rarity('oracle'/'VAN', 'Special').
card_artist('oracle'/'VAN', 'Dan Frazier').
card_flavor_text('oracle'/'VAN', 'Known simply by her title, Oracle uses her gift of prophecy to aid her people. She believes Gerrard to be the Korvecdal, the one who will unite the people of Rath against Volrath\'s tyranny. Her only doubt is whether she will be strong enough to guide him to his destiny.').
card_multiverse_id('oracle'/'VAN', '12141').

card_in_set('orcish squatters avatar', 'VAN').
card_original_type('orcish squatters avatar'/'VAN', 'Vanguard').
card_original_text('orcish squatters avatar'/'VAN', 'At the beginning of your precombat main phase, add {X} to your mana pool, where X is the number of lands target opponent controls.').
card_first_print('orcish squatters avatar', 'VAN').
card_image_name('orcish squatters avatar'/'VAN', 'orcish squatters avatar').
card_uid('orcish squatters avatar'/'VAN', 'VAN:Orcish Squatters Avatar:orcish squatters avatar').
card_rarity('orcish squatters avatar'/'VAN', 'Special').
card_artist('orcish squatters avatar'/'VAN', 'UDON').
card_number('orcish squatters avatar'/'VAN', '84').
card_flavor_text('orcish squatters avatar'/'VAN', 'ME2 Participation Avatar').
card_multiverse_id('orcish squatters avatar'/'VAN', '191307').

card_in_set('orim', 'VAN').
card_original_type('orim'/'VAN', 'Vanguard — Character').
card_original_text('orim'/'VAN', 'Your creatures can block as though they had flying.').
card_first_print('orim', 'VAN').
card_image_name('orim'/'VAN', 'orim').
card_uid('orim'/'VAN', 'VAN:Orim:orim').
card_rarity('orim'/'VAN', 'Special').
card_artist('orim'/'VAN', 'Rebecca Guay').
card_flavor_text('orim'/'VAN', 'Trained in the Samite arts, Orim serves as the healer aboard the Weatherlight. She is quick with languages and was educated at the same Argivian university as Hanna. A student by nature, Orim keeps a diary of all her new discoveries—both physical and spiritual—during her travels.').
card_multiverse_id('orim'/'VAN', '4966').

card_in_set('peacekeeper avatar', 'VAN').
card_original_type('peacekeeper avatar'/'VAN', 'Vanguard').
card_original_text('peacekeeper avatar'/'VAN', '{3}: For each opponent who controls a creature, put a token onto the battlefield that\'s a copy of a card named Arrest and attach it to a creature that player controls chosen at random.').
card_first_print('peacekeeper avatar', 'VAN').
card_image_name('peacekeeper avatar'/'VAN', 'peacekeeper avatar').
card_uid('peacekeeper avatar'/'VAN', 'VAN:Peacekeeper Avatar:peacekeeper avatar').
card_rarity('peacekeeper avatar'/'VAN', 'Special').
card_artist('peacekeeper avatar'/'VAN', 'UDON').
card_number('peacekeeper avatar'/'VAN', '75').
card_flavor_text('peacekeeper avatar'/'VAN', 'WL Participation Avatar').
card_multiverse_id('peacekeeper avatar'/'VAN', '182281').

card_in_set('phage the untouchable avatar', 'VAN').
card_original_type('phage the untouchable avatar'/'VAN', 'Vanguard').
card_original_text('phage the untouchable avatar'/'VAN', 'Pay 1 life: Until end of turn, whenever a creature deals combat damage to you, destroy that creature.\nPay 1 life: Until end of turn, whenever target creature you control deals combat damage to a creature, destroy the damaged creature.').
card_first_print('phage the untouchable avatar', 'VAN').
card_image_name('phage the untouchable avatar'/'VAN', 'phage the untouchable avatar').
card_uid('phage the untouchable avatar'/'VAN', 'VAN:Phage the Untouchable Avatar:phage the untouchable avatar').
card_rarity('phage the untouchable avatar'/'VAN', 'Special').
card_artist('phage the untouchable avatar'/'VAN', 'UDON').
card_number('phage the untouchable avatar'/'VAN', '31').
card_flavor_text('phage the untouchable avatar'/'VAN', 'LGN Participation Avatar').
card_multiverse_id('phage the untouchable avatar'/'VAN', '182305').

card_in_set('platinum angel avatar', 'VAN').
card_original_type('platinum angel avatar'/'VAN', 'Vanguard').
card_original_text('platinum angel avatar'/'VAN', 'If you control an artifact, a creature, an enchantment, and a land, you can\'t lose the game and your opponents can\'t win the game.').
card_first_print('platinum angel avatar', 'VAN').
card_image_name('platinum angel avatar'/'VAN', 'platinum angel avatar1').
card_uid('platinum angel avatar'/'VAN', 'VAN:Platinum Angel Avatar:platinum angel avatar1').
card_rarity('platinum angel avatar'/'VAN', 'Special').
card_artist('platinum angel avatar'/'VAN', 'Brom').
card_number('platinum angel avatar'/'VAN', '108').
card_flavor_text('platinum angel avatar'/'VAN', 'In its heart lies the secret of immortality.').
card_multiverse_id('platinum angel avatar'/'VAN', '214825').

card_in_set('platinum angel avatar', 'VAN').
card_original_type('platinum angel avatar'/'VAN', 'Vanguard').
card_original_text('platinum angel avatar'/'VAN', 'If you control an artifact, a creature, an enchantment, and a land, you can\'t lose the game and your opponents can\'t win the game.').
card_image_name('platinum angel avatar'/'VAN', 'platinum angel avatar2').
card_uid('platinum angel avatar'/'VAN', 'VAN:Platinum Angel Avatar:platinum angel avatar2').
card_rarity('platinum angel avatar'/'VAN', 'Special').
card_artist('platinum angel avatar'/'VAN', 'UDON').
card_number('platinum angel avatar'/'VAN', '40').
card_flavor_text('platinum angel avatar'/'VAN', 'In its heart lies the secret of immortality.').
card_multiverse_id('platinum angel avatar'/'VAN', '182292').

card_in_set('prodigal sorcerer avatar', 'VAN').
card_original_type('prodigal sorcerer avatar'/'VAN', 'Vanguard').
card_original_text('prodigal sorcerer avatar'/'VAN', 'At the beginning of your upkeep, look at the top card of your library. You may put that card into your graveyard.').
card_first_print('prodigal sorcerer avatar', 'VAN').
card_image_name('prodigal sorcerer avatar'/'VAN', 'prodigal sorcerer avatar1').
card_uid('prodigal sorcerer avatar'/'VAN', 'VAN:Prodigal Sorcerer Avatar:prodigal sorcerer avatar1').
card_rarity('prodigal sorcerer avatar'/'VAN', 'Special').
card_artist('prodigal sorcerer avatar'/'VAN', 'Douglas Shuler').
card_number('prodigal sorcerer avatar'/'VAN', '17').
card_flavor_text('prodigal sorcerer avatar'/'VAN', 'Ocassionally, members of the Institute of Arcane Study acquire a taste for worldy pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer avatar'/'VAN', '182276').

card_in_set('prodigal sorcerer avatar', 'VAN').
card_original_type('prodigal sorcerer avatar'/'VAN', 'Vanguard').
card_original_text('prodigal sorcerer avatar'/'VAN', 'At the beginning of your upkeep, look at the top card of your library. You may put that card into your graveyard.').
card_image_name('prodigal sorcerer avatar'/'VAN', 'prodigal sorcerer avatar2').
card_uid('prodigal sorcerer avatar'/'VAN', 'VAN:Prodigal Sorcerer Avatar:prodigal sorcerer avatar2').
card_rarity('prodigal sorcerer avatar'/'VAN', 'Special').
card_artist('prodigal sorcerer avatar'/'VAN', 'Tony Szczudlo').
card_number('prodigal sorcerer avatar'/'VAN', '18').
card_flavor_text('prodigal sorcerer avatar'/'VAN', 'He wastes his amazing talents on proving how amazing he really is.').
card_multiverse_id('prodigal sorcerer avatar'/'VAN', '210151').

card_in_set('raksha golden cub avatar', 'VAN').
card_original_type('raksha golden cub avatar'/'VAN', 'Vanguard').
card_original_text('raksha golden cub avatar'/'VAN', 'Creatures you control get +0/+1.\nEquipped creatures you control get +1/+0 and have first strike.').
card_first_print('raksha golden cub avatar', 'VAN').
card_image_name('raksha golden cub avatar'/'VAN', 'raksha golden cub avatar').
card_uid('raksha golden cub avatar'/'VAN', 'VAN:Raksha Golden Cub Avatar:raksha golden cub avatar').
card_rarity('raksha golden cub avatar'/'VAN', 'Special').
card_artist('raksha golden cub avatar'/'VAN', 'UDON').
card_number('raksha golden cub avatar'/'VAN', '43').
card_flavor_text('raksha golden cub avatar'/'VAN', '5DN Prize Avatar').
card_multiverse_id('raksha golden cub avatar'/'VAN', '182258').

card_in_set('reaper king avatar', 'VAN').
card_original_type('reaper king avatar'/'VAN', 'Vanguard').
card_original_text('reaper king avatar'/'VAN', 'Each creature you control gets +1/+1 for each of its colors.').
card_first_print('reaper king avatar', 'VAN').
card_image_name('reaper king avatar'/'VAN', 'reaper king avatar').
card_uid('reaper king avatar'/'VAN', 'VAN:Reaper King Avatar:reaper king avatar').
card_rarity('reaper king avatar'/'VAN', 'Special').
card_artist('reaper king avatar'/'VAN', 'UDON').
card_number('reaper king avatar'/'VAN', '79').
card_flavor_text('reaper king avatar'/'VAN', 'SHM Prize Avatar').
card_multiverse_id('reaper king avatar'/'VAN', '191135').

card_in_set('rith, the awakener avatar', 'VAN').
card_original_type('rith, the awakener avatar'/'VAN', 'Vanguard').
card_original_text('rith, the awakener avatar'/'VAN', 'Whenever a creature you control deals combat damage to a player, you may pay {5}. If you do, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_first_print('rith, the awakener avatar', 'VAN').
card_image_name('rith, the awakener avatar'/'VAN', 'rith, the awakener avatar1').
card_uid('rith, the awakener avatar'/'VAN', 'VAN:Rith, the Awakener Avatar:rith, the awakener avatar1').
card_rarity('rith, the awakener avatar'/'VAN', 'Special').
card_artist('rith, the awakener avatar'/'VAN', 'UDON').
card_number('rith, the awakener avatar'/'VAN', '106').
card_flavor_text('rith, the awakener avatar'/'VAN', 'Alternative-art Avatar').
card_multiverse_id('rith, the awakener avatar'/'VAN', '214826').

card_in_set('rith, the awakener avatar', 'VAN').
card_original_type('rith, the awakener avatar'/'VAN', 'Vanguard').
card_original_text('rith, the awakener avatar'/'VAN', 'Whenever a creature you control deals combat damage to a player, you may pay {5}. If you do, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_image_name('rith, the awakener avatar'/'VAN', 'rith, the awakener avatar2').
card_uid('rith, the awakener avatar'/'VAN', 'VAN:Rith, the Awakener Avatar:rith, the awakener avatar2').
card_rarity('rith, the awakener avatar'/'VAN', 'Special').
card_artist('rith, the awakener avatar'/'VAN', 'UDON').
card_number('rith, the awakener avatar'/'VAN', '85').
card_flavor_text('rith, the awakener avatar'/'VAN', 'DRB Avatar').
card_multiverse_id('rith, the awakener avatar'/'VAN', '192073').

card_in_set('rofellos', 'VAN').
card_original_type('rofellos'/'VAN', 'Vanguard — Character').
card_original_text('rofellos'/'VAN', 'Whenever a creature you control is put into a graveyard from play, draw a card.').
card_first_print('rofellos', 'VAN').
card_image_name('rofellos'/'VAN', 'rofellos').
card_uid('rofellos'/'VAN', 'VAN:Rofellos:rofellos').
card_rarity('rofellos'/'VAN', 'Special').
card_artist('rofellos'/'VAN', 'Daren Bader').
card_flavor_text('rofellos'/'VAN', 'Gerrard\'s longtime friend and fellow student under the maro-sorcerer Multani, Rofellos is an excellent warrior and excellent company. Though adept at both magic and swordplay, he\'s far more comfortable with a flagon in his hand and a good companion at his side.').
card_multiverse_id('rofellos'/'VAN', '12145').

card_in_set('royal assassin avatar', 'VAN').
card_original_type('royal assassin avatar'/'VAN', 'Vanguard').
card_original_text('royal assassin avatar'/'VAN', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_first_print('royal assassin avatar', 'VAN').
card_image_name('royal assassin avatar'/'VAN', 'royal assassin avatar1').
card_uid('royal assassin avatar'/'VAN', 'VAN:Royal Assassin Avatar:royal assassin avatar1').
card_rarity('royal assassin avatar'/'VAN', 'Special').
card_artist('royal assassin avatar'/'VAN', 'UDON').
card_number('royal assassin avatar'/'VAN', '105').
card_flavor_text('royal assassin avatar'/'VAN', 'Alternative-art Avatar').
card_multiverse_id('royal assassin avatar'/'VAN', '214827').

card_in_set('royal assassin avatar', 'VAN').
card_original_type('royal assassin avatar'/'VAN', 'Vanguard').
card_original_text('royal assassin avatar'/'VAN', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_image_name('royal assassin avatar'/'VAN', 'royal assassin avatar2').
card_uid('royal assassin avatar'/'VAN', 'VAN:Royal Assassin Avatar:royal assassin avatar2').
card_rarity('royal assassin avatar'/'VAN', 'Special').
card_artist('royal assassin avatar'/'VAN', 'UDON').
card_number('royal assassin avatar'/'VAN', '38').
card_flavor_text('royal assassin avatar'/'VAN', '8ED Participation Avatar').
card_multiverse_id('royal assassin avatar'/'VAN', '182260').

card_in_set('rumbling slum avatar', 'VAN').
card_original_type('rumbling slum avatar'/'VAN', 'Vanguard').
card_original_text('rumbling slum avatar'/'VAN', 'At the beginning of your upkeep, Rumbling Slum deals 1 damage to each opponent.').
card_first_print('rumbling slum avatar', 'VAN').
card_image_name('rumbling slum avatar'/'VAN', 'rumbling slum avatar').
card_uid('rumbling slum avatar'/'VAN', 'VAN:Rumbling Slum Avatar:rumbling slum avatar').
card_rarity('rumbling slum avatar'/'VAN', 'Special').
card_artist('rumbling slum avatar'/'VAN', 'UDON').
card_number('rumbling slum avatar'/'VAN', '56').
card_flavor_text('rumbling slum avatar'/'VAN', 'GPT Prize Avatar').
card_multiverse_id('rumbling slum avatar'/'VAN', '182277').

card_in_set('sakashima the impostor avatar', 'VAN').
card_original_type('sakashima the impostor avatar'/'VAN', 'Vanguard').
card_original_text('sakashima the impostor avatar'/'VAN', '{2}: Choose a creature you control. It becomes a copy of target creature except for its name.').
card_first_print('sakashima the impostor avatar', 'VAN').
card_image_name('sakashima the impostor avatar'/'VAN', 'sakashima the impostor avatar').
card_uid('sakashima the impostor avatar'/'VAN', 'VAN:Sakashima the Impostor Avatar:sakashima the impostor avatar').
card_rarity('sakashima the impostor avatar'/'VAN', 'Special').
card_artist('sakashima the impostor avatar'/'VAN', 'UDON').
card_number('sakashima the impostor avatar'/'VAN', '49').
card_flavor_text('sakashima the impostor avatar'/'VAN', 'SOK Prize Avatar').
card_multiverse_id('sakashima the impostor avatar'/'VAN', '182289').

card_in_set('selenia', 'VAN').
card_original_type('selenia'/'VAN', 'Vanguard — Character').
card_original_text('selenia'/'VAN', 'Attacking doesn\'t cause your creatures to tap.').
card_first_print('selenia', 'VAN').
card_image_name('selenia'/'VAN', 'selenia').
card_uid('selenia'/'VAN', 'VAN:Selenia:selenia').
card_rarity('selenia'/'VAN', 'Special').
card_artist('selenia'/'VAN', 'Quinton Hoover').
card_flavor_text('selenia'/'VAN', 'Once a guardian of Crovax\'s estate, Selenia was freed from her magical bindings by the wealthy noble. Instead of remaining at his side, the angel chose to venture out into the world. For reasons unknown to Crovax, Selenia now serves the evil Volrath in his home realm of Rath.').
card_multiverse_id('selenia'/'VAN', '4967').

card_in_set('serra', 'VAN').
card_original_type('serra'/'VAN', 'Vanguard — Character').
card_original_text('serra'/'VAN', 'All creatures you control get +0/+2.').
card_first_print('serra', 'VAN').
card_image_name('serra'/'VAN', 'serra').
card_uid('serra'/'VAN', 'VAN:Serra:serra').
card_rarity('serra'/'VAN', 'Special').
card_artist('serra'/'VAN', 'Matthew D. Wilson').
card_flavor_text('serra'/'VAN', 'Named for a benevolent goddess at birth, Serra fulfilled the prophecy of that goddess\'s coming. As a planeswalker, she embodies white mana and helps whole civilizations develop while both directly and indirectly inspiring countless individuals to be steadfast and strong in the service of light.').
card_multiverse_id('serra'/'VAN', '12332').

card_in_set('serra angel avatar', 'VAN').
card_original_type('serra angel avatar'/'VAN', 'Vanguard').
card_original_text('serra angel avatar'/'VAN', 'Whenever you cast a spell, you gain 2 life.').
card_first_print('serra angel avatar', 'VAN').
card_image_name('serra angel avatar'/'VAN', 'serra angel avatar1').
card_uid('serra angel avatar'/'VAN', 'VAN:Serra Angel Avatar:serra angel avatar1').
card_rarity('serra angel avatar'/'VAN', 'Special').
card_artist('serra angel avatar'/'VAN', 'Greg Staples').
card_number('serra angel avatar'/'VAN', '1').
card_flavor_text('serra angel avatar'/'VAN', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel avatar'/'VAN', '182282').

card_in_set('serra angel avatar', 'VAN').
card_original_type('serra angel avatar'/'VAN', 'Vanguard').
card_original_text('serra angel avatar'/'VAN', 'Whenever you cast a spell, you gain 2 life.').
card_image_name('serra angel avatar'/'VAN', 'serra angel avatar2').
card_uid('serra angel avatar'/'VAN', 'VAN:Serra Angel Avatar:serra angel avatar2').
card_rarity('serra angel avatar'/'VAN', 'Special').
card_artist('serra angel avatar'/'VAN', 'UDON').
card_number('serra angel avatar'/'VAN', '2').
card_flavor_text('serra angel avatar'/'VAN', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel avatar'/'VAN', '205492').

card_in_set('seshiro the anointed avatar', 'VAN').
card_original_type('seshiro the anointed avatar'/'VAN', 'Vanguard').
card_original_text('seshiro the anointed avatar'/'VAN', 'At the beginning of the game, choose a creature type. Creatures you control, creature spells you control, and creature cards you own in any zone other than  the battlefield or the stack have the chosen type in addition to their other types.').
card_first_print('seshiro the anointed avatar', 'VAN').
card_image_name('seshiro the anointed avatar'/'VAN', 'seshiro the anointed avatar').
card_uid('seshiro the anointed avatar'/'VAN', 'VAN:Seshiro the Anointed Avatar:seshiro the anointed avatar').
card_rarity('seshiro the anointed avatar'/'VAN', 'Special').
card_artist('seshiro the anointed avatar'/'VAN', 'UDON').
card_number('seshiro the anointed avatar'/'VAN', '46').
card_flavor_text('seshiro the anointed avatar'/'VAN', 'CHK Participation Avatar').
card_multiverse_id('seshiro the anointed avatar'/'VAN', '182288').

card_in_set('sidar kondo', 'VAN').
card_original_type('sidar kondo'/'VAN', 'Vanguard — Character').
card_original_text('sidar kondo'/'VAN', '{3}: Target creature gets +3/+3 until end of turn.').
card_first_print('sidar kondo', 'VAN').
card_image_name('sidar kondo'/'VAN', 'sidar kondo').
card_uid('sidar kondo'/'VAN', 'VAN:Sidar Kondo:sidar kondo').
card_rarity('sidar kondo'/'VAN', 'Special').
card_artist('sidar kondo'/'VAN', 'Kev Walker').
card_flavor_text('sidar kondo'/'VAN', 'Kondo served as guardian to the infant Gerrard and raised him alongside his own son, Vuel. He taught both boys to be strong and to lead, but whereas Gerrard became a champion of honor, Vuel\'s bitterness led him to become an equally devastating force of malice.').
card_multiverse_id('sidar kondo'/'VAN', '12147').

card_in_set('sisay', 'VAN').
card_original_type('sisay'/'VAN', 'Vanguard — Character').
card_original_text('sisay'/'VAN', 'Whenever you tap a land for mana, it produces one additional mana of the same type.').
card_first_print('sisay', 'VAN').
card_image_name('sisay'/'VAN', 'sisay').
card_uid('sisay'/'VAN', 'VAN:Sisay:sisay').
card_rarity('sisay'/'VAN', 'Special').
card_artist('sisay'/'VAN', 'Kaja Foglio').
card_flavor_text('sisay'/'VAN', 'While captain of the Weatherlight, Sisay undertook a quest for the Legacy, a collection of artifacts needed to defeat the Lord of the Wastes. She continued this quest even when Gerrard, heir to the Legacy, chose to abandon it. Sisay is devoted to the cause of good at whatever price.').
card_multiverse_id('sisay'/'VAN', '4968').

card_in_set('sisters of stone death avatar', 'VAN').
card_original_type('sisters of stone death avatar'/'VAN', 'Vanguard').
card_original_text('sisters of stone death avatar'/'VAN', 'Each creature you control must be blocked if able.\n{4}: Exile target creature that\'s blocking a creature you control.').
card_first_print('sisters of stone death avatar', 'VAN').
card_image_name('sisters of stone death avatar'/'VAN', 'sisters of stone death avatar').
card_uid('sisters of stone death avatar'/'VAN', 'VAN:Sisters of Stone Death Avatar:sisters of stone death avatar').
card_rarity('sisters of stone death avatar'/'VAN', 'Special').
card_artist('sisters of stone death avatar'/'VAN', 'UDON').
card_number('sisters of stone death avatar'/'VAN', '55').
card_flavor_text('sisters of stone death avatar'/'VAN', 'RAV Prize Avatar').
card_multiverse_id('sisters of stone death avatar'/'VAN', '182261').

card_in_set('sliver queen avatar', 'VAN').
card_original_type('sliver queen avatar'/'VAN', 'Vanguard').
card_original_text('sliver queen avatar'/'VAN', 'Whenever you cast a non-Sliver creature spell, exile that spell. If you do, put a token onto the battlefield that\'s a copy of a random non-Shapeshifter Sliver creature card with the same converted mana cost as that spell.').
card_first_print('sliver queen avatar', 'VAN').
card_image_name('sliver queen avatar'/'VAN', 'sliver queen avatar').
card_uid('sliver queen avatar'/'VAN', 'VAN:Sliver Queen Avatar:sliver queen avatar').
card_rarity('sliver queen avatar'/'VAN', 'Special').
card_artist('sliver queen avatar'/'VAN', 'UDON').
card_number('sliver queen avatar'/'VAN', '92').
card_flavor_text('sliver queen avatar'/'VAN', 'ST Participation Avatar').
card_multiverse_id('sliver queen avatar'/'VAN', '201075').

card_in_set('sliver queen, brood mother', 'VAN').
card_original_type('sliver queen, brood mother'/'VAN', 'Vanguard — Character').
card_original_text('sliver queen, brood mother'/'VAN', '{3}: Put a Sliver token into play. Treat this token as a 1/1 colorless creature.').
card_first_print('sliver queen, brood mother', 'VAN').
card_image_name('sliver queen, brood mother'/'VAN', 'sliver queen, brood mother').
card_uid('sliver queen, brood mother'/'VAN', 'VAN:Sliver Queen, Brood Mother:sliver queen, brood mother').
card_rarity('sliver queen, brood mother'/'VAN', 'Special').
card_artist('sliver queen, brood mother'/'VAN', 'rk post').
card_flavor_text('sliver queen, brood mother'/'VAN', 'Though long held captive beneath Volrath\'s stronghold, the sliver queen is actually freer than most of Rath\'s denizens. Her maternal instincts always take precedence—even when they conflict with the evincar\'s agenda—and through her numerous litters, she ranges across all of Rath.').
card_multiverse_id('sliver queen, brood mother'/'VAN', '12143').

card_in_set('squee', 'VAN').
card_original_type('squee'/'VAN', 'Vanguard — Character').
card_original_text('squee'/'VAN', 'Your opponents play with their hands face up.').
card_first_print('squee', 'VAN').
card_image_name('squee'/'VAN', 'squee').
card_uid('squee'/'VAN', 'VAN:Squee:squee').
card_rarity('squee'/'VAN', 'Special').
card_artist('squee'/'VAN', 'Daniel Gelon').
card_flavor_text('squee'/'VAN', 'The smartest—and only—goblin cabin boy aboard the Weatherlight, Squee has learned that intelligence and common sense are very different things. This matters little, however, since he has neither. Fortunately, he doesn\'t know what he\'s missing.').
card_multiverse_id('squee'/'VAN', '4969').

card_in_set('squee, goblin nabob avatar', 'VAN').
card_original_type('squee, goblin nabob avatar'/'VAN', 'Vanguard').
card_original_text('squee, goblin nabob avatar'/'VAN', '{1}: Prevent the next 1 damage that would be dealt to target creature you control this turn.').
card_first_print('squee, goblin nabob avatar', 'VAN').
card_image_name('squee, goblin nabob avatar'/'VAN', 'squee, goblin nabob avatar').
card_uid('squee, goblin nabob avatar'/'VAN', 'VAN:Squee, Goblin Nabob Avatar:squee, goblin nabob avatar').
card_rarity('squee, goblin nabob avatar'/'VAN', 'Special').
card_artist('squee, goblin nabob avatar'/'VAN', 'UDON').
card_number('squee, goblin nabob avatar'/'VAN', '71').
card_flavor_text('squee, goblin nabob avatar'/'VAN', '10E Prize Avatar').
card_multiverse_id('squee, goblin nabob avatar'/'VAN', '182293').

card_in_set('stalking tiger avatar', 'VAN').
card_original_type('stalking tiger avatar'/'VAN', 'Vanguard').
card_original_text('stalking tiger avatar'/'VAN', 'Whenever you cast a creature spell, you may pay {1}. If you do, draw a card.').
card_first_print('stalking tiger avatar', 'VAN').
card_image_name('stalking tiger avatar'/'VAN', 'stalking tiger avatar').
card_uid('stalking tiger avatar'/'VAN', 'VAN:Stalking Tiger Avatar:stalking tiger avatar').
card_rarity('stalking tiger avatar'/'VAN', 'Special').
card_artist('stalking tiger avatar'/'VAN', 'UDON').
card_number('stalking tiger avatar'/'VAN', '7').
card_flavor_text('stalking tiger avatar'/'VAN', 'MI Participation Avatar').
card_multiverse_id('stalking tiger avatar'/'VAN', '182256').

card_in_set('starke', 'VAN').
card_original_type('starke'/'VAN', 'Vanguard — Character').
card_original_text('starke'/'VAN', 'During your draw phase, you may draw an additional card and then put any card in your hand on the bottom of your library.').
card_first_print('starke', 'VAN').
card_image_name('starke'/'VAN', 'starke').
card_uid('starke'/'VAN', 'VAN:Starke:starke').
card_rarity('starke'/'VAN', 'Special').
card_artist('starke'/'VAN', 'Donato Giancola').
card_flavor_text('starke'/'VAN', 'Starke believes in situational ethics—the situation determines the ethics. He\'s switched sides so often that he has become everyone\'s ally yet no one\'s friend. This former merchant of Rath accompanies the Weatherlight crew on their journey to his home world.').
card_multiverse_id('starke'/'VAN', '4970').

card_in_set('stonehewer giant avatar', 'VAN').
card_original_type('stonehewer giant avatar'/'VAN', 'Vanguard').
card_original_text('stonehewer giant avatar'/'VAN', 'Whenever a creature enters the battlefield under your control, put a token onto the battlefield that\'s a copy of a random Equipment card with converted mana cost less than or equal to that creature\'s converted mana cost. Attach that Equipment to that creature.').
card_first_print('stonehewer giant avatar', 'VAN').
card_image_name('stonehewer giant avatar'/'VAN', 'stonehewer giant avatar').
card_uid('stonehewer giant avatar'/'VAN', 'VAN:Stonehewer Giant Avatar:stonehewer giant avatar').
card_rarity('stonehewer giant avatar'/'VAN', 'Special').
card_artist('stonehewer giant avatar'/'VAN', 'UDON').
card_number('stonehewer giant avatar'/'VAN', '77').
card_flavor_text('stonehewer giant avatar'/'VAN', 'MOR Participation Avatar').
card_multiverse_id('stonehewer giant avatar'/'VAN', '182254').

card_in_set('stuffy doll avatar', 'VAN').
card_original_type('stuffy doll avatar'/'VAN', 'Vanguard').
card_original_text('stuffy doll avatar'/'VAN', '{0}: If damage would be dealt to you this turn by a source of your choice, prevent that damage. Stuffy Doll deals damage to you and target opponent equal to half the damage prevented this way, rounded up. Activate this ability only once each turn.').
card_first_print('stuffy doll avatar', 'VAN').
card_image_name('stuffy doll avatar'/'VAN', 'stuffy doll avatar').
card_uid('stuffy doll avatar'/'VAN', 'VAN:Stuffy Doll Avatar:stuffy doll avatar').
card_rarity('stuffy doll avatar'/'VAN', 'Special').
card_artist('stuffy doll avatar'/'VAN', 'UDON').
card_number('stuffy doll avatar'/'VAN', '65').
card_flavor_text('stuffy doll avatar'/'VAN', 'TSP Participation Avatar').
card_multiverse_id('stuffy doll avatar'/'VAN', '182278').

card_in_set('tahngarth', 'VAN').
card_original_type('tahngarth'/'VAN', 'Vanguard — Character').
card_original_text('tahngarth'/'VAN', 'Your creatures are unaffected by summoning sickness.').
card_first_print('tahngarth', 'VAN').
card_image_name('tahngarth'/'VAN', 'tahngarth').
card_uid('tahngarth'/'VAN', 'VAN:Tahngarth:tahngarth').
card_rarity('tahngarth'/'VAN', 'Special').
card_artist('tahngarth'/'VAN', 'Pete Venters').
card_flavor_text('tahngarth'/'VAN', 'Tahngarth, first mate of the Weatherlight under Sisay, knows that there are really only two forms of life: the ugly ones and the minotaurs. A narcissist, he considers the condition of his hide as important as the accuracy of his blade.').
card_multiverse_id('tahngarth'/'VAN', '4971').

card_in_set('takara', 'VAN').
card_original_type('takara'/'VAN', 'Vanguard — Character').
card_original_text('takara'/'VAN', 'Sacrifice a creature: Takara deals 1 damage to target creature or player.').
card_first_print('takara', 'VAN').
card_image_name('takara'/'VAN', 'takara').
card_uid('takara'/'VAN', 'VAN:Takara:takara').
card_rarity('takara'/'VAN', 'Special').
card_artist('takara'/'VAN', 'DiTerlizzi').
card_flavor_text('takara'/'VAN', 'Takara banishes the shadow of her father\'s ignoble reputation through the dedication, plain speaking, and swift action of a warrior. By refusing Starke\'s path of self-interest and opportunism, she has learned what he never will know: the value of sacrifice for a worthy cause.').
card_multiverse_id('takara'/'VAN', '12144').

card_in_set('tawnos', 'VAN').
card_original_type('tawnos'/'VAN', 'Vanguard — Character').
card_original_text('tawnos'/'VAN', 'You may play artifact, creature, and enchantment spells any time you could play an instant.').
card_first_print('tawnos', 'VAN').
card_image_name('tawnos'/'VAN', 'tawnos').
card_uid('tawnos'/'VAN', 'VAN:Tawnos:tawnos').
card_rarity('tawnos'/'VAN', 'Special').
card_artist('tawnos'/'VAN', 'Donato Giancola').
card_flavor_text('tawnos'/'VAN', 'Tawnos brings a childlike wonder and an inherent skill to Urza\'s studio and bridges the wizardry of the workshop with the grandeur of the royal court. His speed and flexibility quickly earned him promotion from apprentice to master artificer.').
card_multiverse_id('tawnos'/'VAN', '12328').

card_in_set('teysa, orzhov scion avatar', 'VAN').
card_original_type('teysa, orzhov scion avatar'/'VAN', 'Vanguard').
card_original_text('teysa, orzhov scion avatar'/'VAN', 'Whenever a nontoken creature is put into a graveyard from the battlefield, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('teysa, orzhov scion avatar', 'VAN').
card_image_name('teysa, orzhov scion avatar'/'VAN', 'teysa, orzhov scion avatar').
card_uid('teysa, orzhov scion avatar'/'VAN', 'VAN:Teysa, Orzhov Scion Avatar:teysa, orzhov scion avatar').
card_rarity('teysa, orzhov scion avatar'/'VAN', 'Special').
card_artist('teysa, orzhov scion avatar'/'VAN', 'UDON').
card_number('teysa, orzhov scion avatar'/'VAN', '57').
card_flavor_text('teysa, orzhov scion avatar'/'VAN', 'GPT Participation Avatar').
card_multiverse_id('teysa, orzhov scion avatar'/'VAN', '182295').

card_in_set('titania', 'VAN').
card_original_type('titania'/'VAN', 'Vanguard — Character').
card_original_text('titania'/'VAN', 'You may play an additional land each of your turns.').
card_first_print('titania', 'VAN').
card_image_name('titania'/'VAN', 'titania').
card_uid('titania'/'VAN', 'VAN:Titania:titania').
card_rarity('titania'/'VAN', 'Special').
card_artist('titania'/'VAN', 'Rebecca Guay').
card_flavor_text('titania'/'VAN', 'Voice of the Argoth forest, defender of its creatures, and enforcer of its laws, Titania is literally an aspect of the forest itself. Her power and her very existence are linked to the land: when the forest is healthy, Titania is ascendant. When Argoth suffers, Titania also bleeds.').
card_multiverse_id('titania'/'VAN', '12333').

card_in_set('tradewind rider avatar', 'VAN').
card_original_type('tradewind rider avatar'/'VAN', 'Vanguard').
card_original_text('tradewind rider avatar'/'VAN', '{3}: Each player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_first_print('tradewind rider avatar', 'VAN').
card_image_name('tradewind rider avatar'/'VAN', 'tradewind rider avatar1').
card_uid('tradewind rider avatar'/'VAN', 'VAN:Tradewind Rider Avatar:tradewind rider avatar1').
card_rarity('tradewind rider avatar'/'VAN', 'Special').
card_artist('tradewind rider avatar'/'VAN', 'UDON').
card_number('tradewind rider avatar'/'VAN', '104').
card_flavor_text('tradewind rider avatar'/'VAN', 'Alternative-art Avatar').
card_multiverse_id('tradewind rider avatar'/'VAN', '214828').

card_in_set('tradewind rider avatar', 'VAN').
card_original_type('tradewind rider avatar'/'VAN', 'Vanguard').
card_original_text('tradewind rider avatar'/'VAN', '{3}: Each player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_image_name('tradewind rider avatar'/'VAN', 'tradewind rider avatar2').
card_uid('tradewind rider avatar'/'VAN', 'VAN:Tradewind Rider Avatar:tradewind rider avatar2').
card_rarity('tradewind rider avatar'/'VAN', 'Special').
card_artist('tradewind rider avatar'/'VAN', 'UDON').
card_number('tradewind rider avatar'/'VAN', '89').
card_flavor_text('tradewind rider avatar'/'VAN', 'TE Prize Avatar').
card_multiverse_id('tradewind rider avatar'/'VAN', '195138').

card_in_set('two-headed giant of foriys avatar', 'VAN').
card_original_type('two-headed giant of foriys avatar'/'VAN', 'Vanguard').
card_original_text('two-headed giant of foriys avatar'/'VAN', 'Each creature you control can block an additional creature each combat.\nEach creature you control can\'t be blocked except by two or more creatures.').
card_first_print('two-headed giant of foriys avatar', 'VAN').
card_image_name('two-headed giant of foriys avatar'/'VAN', 'two-headed giant of foriys avatar').
card_uid('two-headed giant of foriys avatar'/'VAN', 'VAN:Two-Headed Giant of Foriys Avatar:two-headed giant of foriys avatar').
card_rarity('two-headed giant of foriys avatar'/'VAN', 'Special').
card_artist('two-headed giant of foriys avatar'/'VAN', 'UDON').
card_number('two-headed giant of foriys avatar'/'VAN', '36').
card_flavor_text('two-headed giant of foriys avatar'/'VAN', 'Awarded in celebration of the first birthday of Magic Online').
card_multiverse_id('two-headed giant of foriys avatar'/'VAN', '182301').

card_in_set('urza', 'VAN').
card_original_type('urza'/'VAN', 'Vanguard — Character').
card_original_text('urza'/'VAN', '{3}: Urza deals 1 damage to target creature or player.').
card_first_print('urza', 'VAN').
card_image_name('urza'/'VAN', 'urza').
card_uid('urza'/'VAN', 'VAN:Urza:urza').
card_rarity('urza'/'VAN', 'Special').
card_artist('urza'/'VAN', 'Mark Tedin').
card_flavor_text('urza'/'VAN', 'An artificer of unparalleled renown, Urza was a powerful cipher even before he was able to planeswalk. Seeing the world as a large, integrated machine is his greatest strength and weakness, enabling him to solve almost any problem but blinding him to his solutions\' effects on others.').
card_multiverse_id('urza'/'VAN', '12326').

card_in_set('vampire nocturnus avatar', 'VAN').
card_original_type('vampire nocturnus avatar'/'VAN', 'Vanguard').
card_original_text('vampire nocturnus avatar'/'VAN', 'Play with the top card of your library revealed.\nAs long as the top card of your library is black, black creatures you control get +2/+1.').
card_first_print('vampire nocturnus avatar', 'VAN').
card_image_name('vampire nocturnus avatar'/'VAN', 'vampire nocturnus avatar').
card_uid('vampire nocturnus avatar'/'VAN', 'VAN:Vampire Nocturnus Avatar:vampire nocturnus avatar').
card_rarity('vampire nocturnus avatar'/'VAN', 'Special').
card_artist('vampire nocturnus avatar'/'VAN', 'UDON').
card_number('vampire nocturnus avatar'/'VAN', '97').
card_flavor_text('vampire nocturnus avatar'/'VAN', 'M10 Winner Avatar').
card_multiverse_id('vampire nocturnus avatar'/'VAN', '206028').

card_in_set('viridian zealot avatar', 'VAN').
card_original_type('viridian zealot avatar'/'VAN', 'Vanguard').
card_original_text('viridian zealot avatar'/'VAN', '{2}, Sacrifice a creature: Destroy target artifact or enchantment. Search your library for a card with the same name as the sacrificed creature, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('viridian zealot avatar', 'VAN').
card_image_name('viridian zealot avatar'/'VAN', 'viridian zealot avatar').
card_uid('viridian zealot avatar'/'VAN', 'VAN:Viridian Zealot Avatar:viridian zealot avatar').
card_rarity('viridian zealot avatar'/'VAN', 'Special').
card_artist('viridian zealot avatar'/'VAN', 'UDON').
card_number('viridian zealot avatar'/'VAN', '41').
card_flavor_text('viridian zealot avatar'/'VAN', 'DST Participation Avatar').
card_multiverse_id('viridian zealot avatar'/'VAN', '182285').

card_in_set('volrath', 'VAN').
card_original_type('volrath'/'VAN', 'Vanguard — Character').
card_original_text('volrath'/'VAN', 'Whenever any of your creatures is put into your graveyard from play, you may put that creature on top of your library.').
card_first_print('volrath', 'VAN').
card_image_name('volrath'/'VAN', 'volrath').
card_uid('volrath'/'VAN', 'VAN:Volrath:volrath').
card_rarity('volrath'/'VAN', 'Special').
card_artist('volrath'/'VAN', 'Anson Maddocks').
card_flavor_text('volrath'/'VAN', 'The son of Gerrard\'s surrogate father, Sidar Kondo, Volrath stole Gerrard\'s Legacy and rose to power selling it off piece by piece. Now, he is an evincar, or agent, of the Lord of the Wastes. Armed with exceptional magical powers, he plots to conquer the entire multiverse.').
card_multiverse_id('volrath'/'VAN', '4972').

card_in_set('xantcha', 'VAN').
card_original_type('xantcha'/'VAN', 'Vanguard — Character').
card_original_text('xantcha'/'VAN', 'Sacrifice a permanent: Regenerate target creature.').
card_first_print('xantcha', 'VAN').
card_image_name('xantcha'/'VAN', 'xantcha').
card_uid('xantcha'/'VAN', 'VAN:Xantcha:xantcha').
card_rarity('xantcha'/'VAN', 'Special').
card_artist('xantcha'/'VAN', 'Heather Hudson').
card_flavor_text('xantcha'/'VAN', 'A Phyrexian \"sleeper agent,\" Xantcha has developed an inviolate sense of self. This aberration earned her a death sentence, but she was rescued by Urza and joined his crusade. Her Phyrexian credo of \"Waste not, want not\" has helped Urza more than even he comprehends.').
card_multiverse_id('xantcha'/'VAN', '12331').
