% From the Vault: Twenty

set('V13').
set_name('V13', 'From the Vault: Twenty').
set_release_date('V13', '2013-08-23').
set_border('V13', 'black').
set_type('V13', 'from the vault').

card_in_set('akroma\'s vengeance', 'V13').
card_original_type('akroma\'s vengeance'/'V13', 'Sorcery').
card_original_text('akroma\'s vengeance'/'V13', 'Destroy all artifacts, creatures, and enchantments.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('akroma\'s vengeance'/'V13', 'akroma\'s vengeance').
card_uid('akroma\'s vengeance'/'V13', 'V13:Akroma\'s Vengeance:akroma\'s vengeance').
card_rarity('akroma\'s vengeance'/'V13', 'Mythic Rare').
card_artist('akroma\'s vengeance'/'V13', 'Aleksi Briclot').
card_number('akroma\'s vengeance'/'V13', '11').
card_flavor_text('akroma\'s vengeance'/'V13', '\"Many must die for the one who should not have perished.\"').
card_multiverse_id('akroma\'s vengeance'/'V13', '373328').

card_in_set('chainer\'s edict', 'V13').
card_original_type('chainer\'s edict'/'V13', 'Sorcery').
card_original_text('chainer\'s edict'/'V13', 'Target player sacrifices a creature.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('chainer\'s edict'/'V13', 'chainer\'s edict').
card_uid('chainer\'s edict'/'V13', 'V13:Chainer\'s Edict:chainer\'s edict').
card_rarity('chainer\'s edict'/'V13', 'Mythic Rare').
card_artist('chainer\'s edict'/'V13', 'Mark Zug').
card_number('chainer\'s edict'/'V13', '10').
card_multiverse_id('chainer\'s edict'/'V13', '373317').

card_in_set('chameleon colossus', 'V13').
card_original_type('chameleon colossus'/'V13', 'Creature — Shapeshifter').
card_original_text('chameleon colossus'/'V13', 'Changeling (This card is every creature type at all times.)\nProtection from black\n{2}{G}{G}: Chameleon Colossus gets +X/+X until end of turn, where X is its power.').
card_image_name('chameleon colossus'/'V13', 'chameleon colossus').
card_uid('chameleon colossus'/'V13', 'V13:Chameleon Colossus:chameleon colossus').
card_rarity('chameleon colossus'/'V13', 'Mythic Rare').
card_artist('chameleon colossus'/'V13', 'Darrell Riche').
card_number('chameleon colossus'/'V13', '16').
card_multiverse_id('chameleon colossus'/'V13', '373321').

card_in_set('char', 'V13').
card_original_type('char'/'V13', 'Instant').
card_original_text('char'/'V13', 'Char deals 4 damage to target creature or player and 2 damage to you.').
card_image_name('char'/'V13', 'char').
card_uid('char'/'V13', 'V13:Char:char').
card_rarity('char'/'V13', 'Mythic Rare').
card_artist('char'/'V13', 'Adam Rex').
card_number('char'/'V13', '14').
card_flavor_text('char'/'V13', 'Izzet mages often acquire their magic reagents from dubious sources, so the potency of their spells is never predictable.').
card_multiverse_id('char'/'V13', '373332').

card_in_set('cruel ultimatum', 'V13').
card_original_type('cruel ultimatum'/'V13', 'Sorcery').
card_original_text('cruel ultimatum'/'V13', 'Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.').
card_image_name('cruel ultimatum'/'V13', 'cruel ultimatum').
card_uid('cruel ultimatum'/'V13', 'V13:Cruel Ultimatum:cruel ultimatum').
card_rarity('cruel ultimatum'/'V13', 'Mythic Rare').
card_artist('cruel ultimatum'/'V13', 'Todd Lockwood').
card_number('cruel ultimatum'/'V13', '17').
card_flavor_text('cruel ultimatum'/'V13', 'There is always a greater power.').
card_multiverse_id('cruel ultimatum'/'V13', '373318').

card_in_set('dark ritual', 'V13').
card_original_type('dark ritual'/'V13', 'Instant').
card_original_text('dark ritual'/'V13', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'V13', 'dark ritual').
card_uid('dark ritual'/'V13', 'V13:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'V13', 'Mythic Rare').
card_artist('dark ritual'/'V13', 'Clint Langley').
card_number('dark ritual'/'V13', '1').
card_flavor_text('dark ritual'/'V13', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('dark ritual'/'V13', '373329').

card_in_set('fact or fiction', 'V13').
card_original_type('fact or fiction'/'V13', 'Instant').
card_original_text('fact or fiction'/'V13', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'V13', 'fact or fiction').
card_uid('fact or fiction'/'V13', 'V13:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'V13', 'Mythic Rare').
card_artist('fact or fiction'/'V13', 'Terese Nielsen').
card_number('fact or fiction'/'V13', '9').
card_multiverse_id('fact or fiction'/'V13', '373325').

card_in_set('fyndhorn elves', 'V13').
card_original_type('fyndhorn elves'/'V13', 'Creature — Elf Druid').
card_original_text('fyndhorn elves'/'V13', '{T}: Add {G} to your mana pool.').
card_image_name('fyndhorn elves'/'V13', 'fyndhorn elves').
card_uid('fyndhorn elves'/'V13', 'V13:Fyndhorn Elves:fyndhorn elves').
card_rarity('fyndhorn elves'/'V13', 'Mythic Rare').
card_artist('fyndhorn elves'/'V13', 'Igor Kieryluk').
card_number('fyndhorn elves'/'V13', '4').
card_flavor_text('fyndhorn elves'/'V13', '\"We could no more abandon the forest than the stars could abandon the night sky.\"').
card_multiverse_id('fyndhorn elves'/'V13', '373320').

card_in_set('gilded lotus', 'V13').
card_original_type('gilded lotus'/'V13', 'Artifact').
card_original_text('gilded lotus'/'V13', '{T}: Add three mana of any one color to your mana pool.').
card_image_name('gilded lotus'/'V13', 'gilded lotus').
card_uid('gilded lotus'/'V13', 'V13:Gilded Lotus:gilded lotus').
card_rarity('gilded lotus'/'V13', 'Mythic Rare').
card_artist('gilded lotus'/'V13', 'Daniel Ljunggren').
card_number('gilded lotus'/'V13', '12').
card_flavor_text('gilded lotus'/'V13', 'Over such beauty, wars are fought. With such power, wars are won.').
card_multiverse_id('gilded lotus'/'V13', '373335').

card_in_set('green sun\'s zenith', 'V13').
card_original_type('green sun\'s zenith'/'V13', 'Sorcery').
card_original_text('green sun\'s zenith'/'V13', 'Search your library for a green creature card with converted mana cost X or less, put it onto the battlefield, then shuffle your library. Shuffle Green Sun\'s Zenith into its owner\'s library.').
card_image_name('green sun\'s zenith'/'V13', 'green sun\'s zenith').
card_uid('green sun\'s zenith'/'V13', 'V13:Green Sun\'s Zenith:green sun\'s zenith').
card_rarity('green sun\'s zenith'/'V13', 'Mythic Rare').
card_artist('green sun\'s zenith'/'V13', 'David Rapoza').
card_number('green sun\'s zenith'/'V13', '19').
card_flavor_text('green sun\'s zenith'/'V13', 'As the green sun crowned, Phyrexian prophecies glowed on the Tree of Tales.').
card_multiverse_id('green sun\'s zenith'/'V13', '373333').

card_in_set('hymn to tourach', 'V13').
card_original_type('hymn to tourach'/'V13', 'Sorcery').
card_original_text('hymn to tourach'/'V13', 'Target player discards two cards at random.').
card_image_name('hymn to tourach'/'V13', 'hymn to tourach').
card_uid('hymn to tourach'/'V13', 'V13:Hymn to Tourach:hymn to tourach').
card_rarity('hymn to tourach'/'V13', 'Mythic Rare').
card_artist('hymn to tourach'/'V13', 'Greg Staples').
card_number('hymn to tourach'/'V13', '3').
card_flavor_text('hymn to tourach'/'V13', 'The priests plead for your anguish and pray for your despair.').
card_multiverse_id('hymn to tourach'/'V13', '373324').

card_in_set('impulse', 'V13').
card_original_type('impulse'/'V13', 'Instant').
card_original_text('impulse'/'V13', 'Look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('impulse'/'V13', 'impulse').
card_uid('impulse'/'V13', 'V13:Impulse:impulse').
card_rarity('impulse'/'V13', 'Mythic Rare').
card_artist('impulse'/'V13', 'Izzy').
card_number('impulse'/'V13', '5').
card_flavor_text('impulse'/'V13', '\"You call it luck. I call it preparation.\"').
card_multiverse_id('impulse'/'V13', '373330').

card_in_set('ink-eyes, servant of oni', 'V13').
card_original_type('ink-eyes, servant of oni'/'V13', 'Legendary Creature — Rat Ninja').
card_original_text('ink-eyes, servant of oni'/'V13', 'Ninjutsu {3}{B}{B} ({3}{B}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Ink-Eyes, Servant of Oni deals combat damage to a player, you may put target creature card from that player\'s graveyard onto the battlefield under your control.\n{1}{B}: Regenerate Ink-Eyes.').
card_image_name('ink-eyes, servant of oni'/'V13', 'ink-eyes, servant of oni').
card_uid('ink-eyes, servant of oni'/'V13', 'V13:Ink-Eyes, Servant of Oni:ink-eyes, servant of oni').
card_rarity('ink-eyes, servant of oni'/'V13', 'Mythic Rare').
card_artist('ink-eyes, servant of oni'/'V13', 'Wayne Reynolds').
card_number('ink-eyes, servant of oni'/'V13', '13').
card_multiverse_id('ink-eyes, servant of oni'/'V13', '373327').

card_in_set('jace, the mind sculptor', 'V13').
card_original_type('jace, the mind sculptor'/'V13', 'Planeswalker — Jace').
card_original_text('jace, the mind sculptor'/'V13', '+2: Look at the top card of target player\'s library. You may put that card on the bottom of that player\'s library.\n0: Draw three cards, then put two cards from your hand on top of your library in any order.\n-1: Return target creature to its owner\'s hand.\n-12: Exile all cards from target player\'s library, then that player shuffles his or her hand into his or her library.').
card_image_name('jace, the mind sculptor'/'V13', 'jace, the mind sculptor').
card_uid('jace, the mind sculptor'/'V13', 'V13:Jace, the Mind Sculptor:jace, the mind sculptor').
card_rarity('jace, the mind sculptor'/'V13', 'Mythic Rare').
card_artist('jace, the mind sculptor'/'V13', 'Jason Chan').
card_number('jace, the mind sculptor'/'V13', '18').
card_multiverse_id('jace, the mind sculptor'/'V13', '373316').

card_in_set('kessig wolf run', 'V13').
card_original_type('kessig wolf run'/'V13', 'Land').
card_original_text('kessig wolf run'/'V13', '{T}: Add {1} to your mana pool.\n{X}{R}{G}, {T}: Target creature gets +X/+0 and gains trample until end of turn.').
card_image_name('kessig wolf run'/'V13', 'kessig wolf run').
card_uid('kessig wolf run'/'V13', 'V13:Kessig Wolf Run:kessig wolf run').
card_rarity('kessig wolf run'/'V13', 'Mythic Rare').
card_artist('kessig wolf run'/'V13', 'Eytan Zana').
card_number('kessig wolf run'/'V13', '20').
card_flavor_text('kessig wolf run'/'V13', 'When a werewolf changes for the first time, that first howl is said to echo through the wilds till moonset.').
card_multiverse_id('kessig wolf run'/'V13', '373323').

card_in_set('swords to plowshares', 'V13').
card_original_type('swords to plowshares'/'V13', 'Instant').
card_original_text('swords to plowshares'/'V13', 'Exile target creature. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'V13', 'swords to plowshares').
card_uid('swords to plowshares'/'V13', 'V13:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'V13', 'Mythic Rare').
card_artist('swords to plowshares'/'V13', 'Terese Nielsen').
card_number('swords to plowshares'/'V13', '2').
card_flavor_text('swords to plowshares'/'V13', 'The smallest seed of regret can bloom into redemption.').
card_multiverse_id('swords to plowshares'/'V13', '373334').

card_in_set('tangle wire', 'V13').
card_original_type('tangle wire'/'V13', 'Artifact').
card_original_text('tangle wire'/'V13', 'Fading 4 (This artifact enters the battlefield with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nAt the beginning of each player\'s upkeep, that player taps an untapped artifact, creature, or land he or she controls for each fade counter on Tangle Wire.').
card_image_name('tangle wire'/'V13', 'tangle wire').
card_uid('tangle wire'/'V13', 'V13:Tangle Wire:tangle wire').
card_rarity('tangle wire'/'V13', 'Mythic Rare').
card_artist('tangle wire'/'V13', 'Marco Nelor').
card_number('tangle wire'/'V13', '8').
card_multiverse_id('tangle wire'/'V13', '373322').

card_in_set('thran dynamo', 'V13').
card_original_type('thran dynamo'/'V13', 'Artifact').
card_original_text('thran dynamo'/'V13', '{T}: Add {3} to your mana pool.').
card_image_name('thran dynamo'/'V13', 'thran dynamo').
card_uid('thran dynamo'/'V13', 'V13:Thran Dynamo:thran dynamo').
card_rarity('thran dynamo'/'V13', 'Mythic Rare').
card_artist('thran dynamo'/'V13', 'Ron Spears').
card_number('thran dynamo'/'V13', '7').
card_flavor_text('thran dynamo'/'V13', 'Urza\'s metathran children were conceived, birthed, and nurtured by an integrated system of machines.').
card_multiverse_id('thran dynamo'/'V13', '373331').

card_in_set('venser, shaper savant', 'V13').
card_original_type('venser, shaper savant'/'V13', 'Legendary Creature — Human Wizard').
card_original_text('venser, shaper savant'/'V13', 'Flash\nWhen Venser, Shaper Savant enters the battlefield, return target spell or permanent to its owner\'s hand.').
card_image_name('venser, shaper savant'/'V13', 'venser, shaper savant').
card_uid('venser, shaper savant'/'V13', 'V13:Venser, Shaper Savant:venser, shaper savant').
card_rarity('venser, shaper savant'/'V13', 'Mythic Rare').
card_artist('venser, shaper savant'/'V13', 'Eric Deschamps').
card_number('venser, shaper savant'/'V13', '15').
card_flavor_text('venser, shaper savant'/'V13', 'His marvels of artifice pale in comparison to the developing machinery of his mind.').
card_multiverse_id('venser, shaper savant'/'V13', '373326').

card_in_set('wall of blossoms', 'V13').
card_original_type('wall of blossoms'/'V13', 'Creature — Plant Wall').
card_original_text('wall of blossoms'/'V13', 'Defender\nWhen Wall of Blossoms enters the battlefield, draw a card.').
card_image_name('wall of blossoms'/'V13', 'wall of blossoms').
card_uid('wall of blossoms'/'V13', 'V13:Wall of Blossoms:wall of blossoms').
card_rarity('wall of blossoms'/'V13', 'Mythic Rare').
card_artist('wall of blossoms'/'V13', 'Heather Hudson').
card_number('wall of blossoms'/'V13', '6').
card_flavor_text('wall of blossoms'/'V13', 'Each flower identical, every leaf and petal disturbingly exact.').
card_multiverse_id('wall of blossoms'/'V13', '373319').
