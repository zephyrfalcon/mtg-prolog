% Gateway

set('pGTW').
set_name('pGTW', 'Gateway').
set_release_date('pGTW', '2006-01-01').
set_border('pGTW', 'black').
set_type('pGTW', 'promo').

card_in_set('boggart ram-gang', 'pGTW').
card_original_type('boggart ram-gang'/'pGTW', 'Creature — Goblin Warrior').
card_original_text('boggart ram-gang'/'pGTW', '').
card_first_print('boggart ram-gang', 'pGTW').
card_image_name('boggart ram-gang'/'pGTW', 'boggart ram-gang').
card_uid('boggart ram-gang'/'pGTW', 'pGTW:Boggart Ram-Gang:boggart ram-gang').
card_rarity('boggart ram-gang'/'pGTW', 'Special').
card_artist('boggart ram-gang'/'pGTW', 'Lucio Parrillo').
card_number('boggart ram-gang'/'pGTW', '17').
card_flavor_text('boggart ram-gang'/'pGTW', '\"We’re going to need a bigger gate.\" —Bowen, Barrenton guardcaptain').

card_in_set('boomerang', 'pGTW').
card_original_type('boomerang'/'pGTW', 'Instant').
card_original_text('boomerang'/'pGTW', '').
card_image_name('boomerang'/'pGTW', 'boomerang').
card_uid('boomerang'/'pGTW', 'pGTW:Boomerang:boomerang').
card_rarity('boomerang'/'pGTW', 'Special').
card_artist('boomerang'/'pGTW', 'Rebecca Guay').
card_number('boomerang'/'pGTW', '4').
card_flavor_text('boomerang'/'pGTW', '\"Returne from whence ye came...\"\n—Edmund Spenser, The Faerie Queene').

card_in_set('calciderm', 'pGTW').
card_original_type('calciderm'/'pGTW', 'Creature — Beast').
card_original_text('calciderm'/'pGTW', '').
card_first_print('calciderm', 'pGTW').
card_image_name('calciderm'/'pGTW', 'calciderm').
card_uid('calciderm'/'pGTW', 'pGTW:Calciderm:calciderm').
card_rarity('calciderm'/'pGTW', 'Special').
card_artist('calciderm'/'pGTW', 'Dave Kendall').
card_number('calciderm'/'pGTW', '5').

card_in_set('cenn\'s tactician', 'pGTW').
card_original_type('cenn\'s tactician'/'pGTW', 'Creature — Kithkin Soldier').
card_original_text('cenn\'s tactician'/'pGTW', '').
card_first_print('cenn\'s tactician', 'pGTW').
card_image_name('cenn\'s tactician'/'pGTW', 'cenn\'s tactician').
card_uid('cenn\'s tactician'/'pGTW', 'pGTW:Cenn\'s Tactician:cenn\'s tactician').
card_rarity('cenn\'s tactician'/'pGTW', 'Special').
card_artist('cenn\'s tactician'/'pGTW', 'Steven Belledin').
card_number('cenn\'s tactician'/'pGTW', '14').
card_flavor_text('cenn\'s tactician'/'pGTW', '\"A well-trained force brings peace... which brings more time for training!\"').

card_in_set('dauntless dourbark', 'pGTW').
card_original_type('dauntless dourbark'/'pGTW', 'Creature — Treefolk Warrior').
card_original_text('dauntless dourbark'/'pGTW', '').
card_first_print('dauntless dourbark', 'pGTW').
card_image_name('dauntless dourbark'/'pGTW', 'dauntless dourbark').
card_uid('dauntless dourbark'/'pGTW', 'pGTW:Dauntless Dourbark:dauntless dourbark').
card_rarity('dauntless dourbark'/'pGTW', 'Special').
card_artist('dauntless dourbark'/'pGTW', 'Jeremy Jarvis').
card_number('dauntless dourbark'/'pGTW', '12').

card_in_set('duergar hedge-mage', 'pGTW').
card_original_type('duergar hedge-mage'/'pGTW', 'Creature — Dwarf Shaman').
card_original_text('duergar hedge-mage'/'pGTW', '').
card_first_print('duergar hedge-mage', 'pGTW').
card_image_name('duergar hedge-mage'/'pGTW', 'duergar hedge-mage').
card_uid('duergar hedge-mage'/'pGTW', 'pGTW:Duergar Hedge-Mage:duergar hedge-mage').
card_rarity('duergar hedge-mage'/'pGTW', 'Special').
card_artist('duergar hedge-mage'/'pGTW', 'Thomas M. Baxa').
card_number('duergar hedge-mage'/'pGTW', '19').

card_in_set('fiery temper', 'pGTW').
card_original_type('fiery temper'/'pGTW', 'Instant').
card_original_text('fiery temper'/'pGTW', '').
card_image_name('fiery temper'/'pGTW', 'fiery temper').
card_uid('fiery temper'/'pGTW', 'pGTW:Fiery Temper:fiery temper').
card_rarity('fiery temper'/'pGTW', 'Special').
card_artist('fiery temper'/'pGTW', 'Ben Wootten').
card_number('fiery temper'/'pGTW', '3').

card_in_set('gravedigger', 'pGTW').
card_original_type('gravedigger'/'pGTW', 'Creature — Zombie').
card_original_text('gravedigger'/'pGTW', '').
card_image_name('gravedigger'/'pGTW', 'gravedigger').
card_uid('gravedigger'/'pGTW', 'pGTW:Gravedigger:gravedigger').
card_rarity('gravedigger'/'pGTW', 'Special').
card_artist('gravedigger'/'pGTW', 'Tony Szczudlo').
card_number('gravedigger'/'pGTW', '16').
card_flavor_text('gravedigger'/'pGTW', 'A full coffin is like a full coffer-both are attractive to thieves.').

card_in_set('icatian javelineers', 'pGTW').
card_original_type('icatian javelineers'/'pGTW', 'Creature — Human Soldier').
card_original_text('icatian javelineers'/'pGTW', '').
card_image_name('icatian javelineers'/'pGTW', 'icatian javelineers').
card_uid('icatian javelineers'/'pGTW', 'pGTW:Icatian Javelineers:icatian javelineers').
card_rarity('icatian javelineers'/'pGTW', 'Special').
card_artist('icatian javelineers'/'pGTW', 'Michael Phillippi').
card_number('icatian javelineers'/'pGTW', '2').

card_in_set('lava axe', 'pGTW').
card_original_type('lava axe'/'pGTW', 'Sorcery').
card_original_text('lava axe'/'pGTW', '').
card_image_name('lava axe'/'pGTW', 'lava axe').
card_uid('lava axe'/'pGTW', 'pGTW:Lava Axe:lava axe').
card_rarity('lava axe'/'pGTW', 'Special').
card_artist('lava axe'/'pGTW', 'Ray Lago').
card_number('lava axe'/'pGTW', '13').
card_flavor_text('lava axe'/'pGTW', '\"Catch!\"').

card_in_set('llanowar elves', 'pGTW').
card_original_type('llanowar elves'/'pGTW', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'pGTW', '').
card_image_name('llanowar elves'/'pGTW', 'llanowar elves').
card_uid('llanowar elves'/'pGTW', 'pGTW:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'pGTW', 'Special').
card_artist('llanowar elves'/'pGTW', 'Anson Maddocks').
card_number('llanowar elves'/'pGTW', '9').
card_flavor_text('llanowar elves'/'pGTW', 'Whenever the Llanowar elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').

card_in_set('mind stone', 'pGTW').
card_original_type('mind stone'/'pGTW', 'Artifact').
card_original_text('mind stone'/'pGTW', '').
card_image_name('mind stone'/'pGTW', 'mind stone').
card_uid('mind stone'/'pGTW', 'pGTW:Mind Stone:mind stone').
card_rarity('mind stone'/'pGTW', 'Special').
card_artist('mind stone'/'pGTW', 'Martina Pilcerova').
card_number('mind stone'/'pGTW', '11').
card_flavor_text('mind stone'/'pGTW', '\"Except our own thoughts, there is nothing absolutely in our power.\"\n—Rene Descartes, Discourse on Method').

card_in_set('mogg fanatic', 'pGTW').
card_original_type('mogg fanatic'/'pGTW', 'Creature — Goblin').
card_original_text('mogg fanatic'/'pGTW', '').
card_image_name('mogg fanatic'/'pGTW', 'mogg fanatic').
card_uid('mogg fanatic'/'pGTW', 'pGTW:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'pGTW', 'Special').
card_artist('mogg fanatic'/'pGTW', 'Nils Hamm').
card_number('mogg fanatic'/'pGTW', '10').
card_flavor_text('mogg fanatic'/'pGTW', '\"As if his chest had been a mortar, he burst his hot heart\'s shell upon it.\"\n—Herman Melville, Moby Dick').

card_in_set('oona\'s blackguard', 'pGTW').
card_original_type('oona\'s blackguard'/'pGTW', 'Creature — Faerie Rogue').
card_original_text('oona\'s blackguard'/'pGTW', '').
card_first_print('oona\'s blackguard', 'pGTW').
card_image_name('oona\'s blackguard'/'pGTW', 'oona\'s blackguard').
card_uid('oona\'s blackguard'/'pGTW', 'pGTW:Oona\'s Blackguard:oona\'s blackguard').
card_rarity('oona\'s blackguard'/'pGTW', 'Special').
card_artist('oona\'s blackguard'/'pGTW', 'Darrell Riche').
card_number('oona\'s blackguard'/'pGTW', '15').

card_in_set('reckless wurm', 'pGTW').
card_original_type('reckless wurm'/'pGTW', 'Creature — Wurm').
card_original_text('reckless wurm'/'pGTW', '').
card_first_print('reckless wurm', 'pGTW').
card_image_name('reckless wurm'/'pGTW', 'reckless wurm').
card_uid('reckless wurm'/'pGTW', 'pGTW:Reckless Wurm:reckless wurm').
card_rarity('reckless wurm'/'pGTW', 'Special').
card_artist('reckless wurm'/'pGTW', 'Greg Staples').
card_number('reckless wurm'/'pGTW', '6').
card_flavor_text('reckless wurm'/'pGTW', 'Bred for battle in the Grand Coliseum, these wurms annihilated whole ecosystems when released into the wild.').

card_in_set('selkie hedge-mage', 'pGTW').
card_original_type('selkie hedge-mage'/'pGTW', 'Creature — Merfolk Wizard').
card_original_text('selkie hedge-mage'/'pGTW', '').
card_first_print('selkie hedge-mage', 'pGTW').
card_image_name('selkie hedge-mage'/'pGTW', 'selkie hedge-mage').
card_uid('selkie hedge-mage'/'pGTW', 'pGTW:Selkie Hedge-Mage:selkie hedge-mage').
card_rarity('selkie hedge-mage'/'pGTW', 'Special').
card_artist('selkie hedge-mage'/'pGTW', 'Brandon Kitkouski').
card_number('selkie hedge-mage'/'pGTW', '20').

card_in_set('wilt-leaf cavaliers', 'pGTW').
card_original_type('wilt-leaf cavaliers'/'pGTW', 'Creature — Elf Knight').
card_original_text('wilt-leaf cavaliers'/'pGTW', '').
card_first_print('wilt-leaf cavaliers', 'pGTW').
card_image_name('wilt-leaf cavaliers'/'pGTW', 'wilt-leaf cavaliers').
card_uid('wilt-leaf cavaliers'/'pGTW', 'pGTW:Wilt-Leaf Cavaliers:wilt-leaf cavaliers').
card_rarity('wilt-leaf cavaliers'/'pGTW', 'Special').
card_artist('wilt-leaf cavaliers'/'pGTW', 'Steven Belledin').
card_number('wilt-leaf cavaliers'/'pGTW', '18').
card_flavor_text('wilt-leaf cavaliers'/'pGTW', 'Every elf in Shadowmoor is charged from birth with a terrible duty: to strike back against the ugliness and darkness, even though they are all around and seemingly without end.').

card_in_set('wood elves', 'pGTW').
card_original_type('wood elves'/'pGTW', 'Creature — Elf Scout').
card_original_text('wood elves'/'pGTW', '').
card_image_name('wood elves'/'pGTW', 'wood elves').
card_uid('wood elves'/'pGTW', 'pGTW:Wood Elves:wood elves').
card_rarity('wood elves'/'pGTW', 'Special').
card_artist('wood elves'/'pGTW', 'Rebecca Guay').
card_number('wood elves'/'pGTW', '1').
card_flavor_text('wood elves'/'pGTW', 'Every branch a crossroads, every vine a swift steed.').

card_in_set('yixlid jailer', 'pGTW').
card_original_type('yixlid jailer'/'pGTW', 'Creature — Zombie Wizard').
card_original_text('yixlid jailer'/'pGTW', '').
card_first_print('yixlid jailer', 'pGTW').
card_image_name('yixlid jailer'/'pGTW', 'yixlid jailer').
card_uid('yixlid jailer'/'pGTW', 'pGTW:Yixlid Jailer:yixlid jailer').
card_rarity('yixlid jailer'/'pGTW', 'Special').
card_artist('yixlid jailer'/'pGTW', 'Alex Horley-Orlandelli').
card_number('yixlid jailer'/'pGTW', '7').
card_flavor_text('yixlid jailer'/'pGTW', 'He holds the keys to a tombful of incarcerated secrets.').

card_in_set('zoetic cavern', 'pGTW').
card_original_type('zoetic cavern'/'pGTW', 'Land').
card_original_text('zoetic cavern'/'pGTW', '').
card_first_print('zoetic cavern', 'pGTW').
card_image_name('zoetic cavern'/'pGTW', 'zoetic cavern').
card_uid('zoetic cavern'/'pGTW', 'pGTW:Zoetic Cavern:zoetic cavern').
card_rarity('zoetic cavern'/'pGTW', 'Special').
card_artist('zoetic cavern'/'pGTW', 'Martina Pilcerova').
card_number('zoetic cavern'/'pGTW', '8').
card_flavor_text('zoetic cavern'/'pGTW', 'Nothing lives within it, yet there is life.').
