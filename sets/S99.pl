% Starter 1999

set('S99').
set_name('S99', 'Starter 1999').
set_release_date('S99', '1999-07-01').
set_border('S99', 'white').
set_type('S99', 'starter').

card_in_set('abyssal horror', 'S99').
card_original_type('abyssal horror'/'S99', 'Creature — Horror').
card_original_text('abyssal horror'/'S99', 'Flying\nWhen Abyssal Horror comes into play, target player chooses and discards two cards from his or her hand. (If that player has only one card, he or she discards it.)').
card_image_name('abyssal horror'/'S99', 'abyssal horror').
card_uid('abyssal horror'/'S99', 'S99:Abyssal Horror:abyssal horror').
card_rarity('abyssal horror'/'S99', 'Rare').
card_artist('abyssal horror'/'S99', 'rk post').
card_number('abyssal horror'/'S99', '63').
card_multiverse_id('abyssal horror'/'S99', '21019').

card_in_set('air elemental', 'S99').
card_original_type('air elemental'/'S99', 'Creature — Elemental').
card_original_text('air elemental'/'S99', 'Flying').
card_image_name('air elemental'/'S99', 'air elemental').
card_uid('air elemental'/'S99', 'S99:Air Elemental:air elemental').
card_rarity('air elemental'/'S99', 'Uncommon').
card_artist('air elemental'/'S99', 'Doug Chaffee').
card_number('air elemental'/'S99', '32').
card_flavor_text('air elemental'/'S99', 'As insubstantial, and as powerful, as the wind that carries it.').
card_multiverse_id('air elemental'/'S99', '21041').

card_in_set('alluring scent', 'S99').
card_original_type('alluring scent'/'S99', 'Sorcery').
card_original_text('alluring scent'/'S99', 'All creatures able to block target creature this turn do so.').
card_image_name('alluring scent'/'S99', 'alluring scent').
card_uid('alluring scent'/'S99', 'S99:Alluring Scent:alluring scent').
card_rarity('alluring scent'/'S99', 'Rare').
card_artist('alluring scent'/'S99', 'Melissa A. Benson').
card_number('alluring scent'/'S99', '124').
card_flavor_text('alluring scent'/'S99', 'No earthly armor is protection against this sweetness.').
card_multiverse_id('alluring scent'/'S99', '20221').

card_in_set('ancient craving', 'S99').
card_original_type('ancient craving'/'S99', 'Sorcery').
card_original_text('ancient craving'/'S99', 'Draw three cards. You lose 3 life.').
card_image_name('ancient craving'/'S99', 'ancient craving').
card_uid('ancient craving'/'S99', 'S99:Ancient Craving:ancient craving').
card_rarity('ancient craving'/'S99', 'Rare').
card_artist('ancient craving'/'S99', 'Rob Alexander').
card_number('ancient craving'/'S99', '64').
card_flavor_text('ancient craving'/'S99', 'Knowledge demands sacrifice.').
card_multiverse_id('ancient craving'/'S99', '20360').

card_in_set('angel of light', 'S99').
card_original_type('angel of light'/'S99', 'Creature — Angel').
card_original_text('angel of light'/'S99', 'Flying\nAttacking doesn\'t cause Angel of Light to tap.').
card_first_print('angel of light', 'S99').
card_image_name('angel of light'/'S99', 'angel of light').
card_uid('angel of light'/'S99', 'S99:Angel of Light:angel of light').
card_rarity('angel of light'/'S99', 'Uncommon').
card_artist('angel of light'/'S99', 'Todd Lockwood').
card_number('angel of light'/'S99', '1').
card_multiverse_id('angel of light'/'S99', '20391').

card_in_set('angel of mercy', 'S99').
card_original_type('angel of mercy'/'S99', 'Creature — Angel').
card_original_text('angel of mercy'/'S99', 'Flying\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'S99', 'angel of mercy').
card_uid('angel of mercy'/'S99', 'S99:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'S99', 'Uncommon').
card_artist('angel of mercy'/'S99', 'Melissa A. Benson').
card_number('angel of mercy'/'S99', '2').
card_multiverse_id('angel of mercy'/'S99', '20392').

card_in_set('angelic blessing', 'S99').
card_original_type('angelic blessing'/'S99', 'Sorcery').
card_original_text('angelic blessing'/'S99', 'Target creature gets +3/+3 and gains flying until end of turn.').
card_image_name('angelic blessing'/'S99', 'angelic blessing').
card_uid('angelic blessing'/'S99', 'S99:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'S99', 'Common').
card_artist('angelic blessing'/'S99', 'Mark Zug').
card_number('angelic blessing'/'S99', '3').
card_flavor_text('angelic blessing'/'S99', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'S99', '20400').

card_in_set('archangel', 'S99').
card_original_type('archangel'/'S99', 'Creature — Angel').
card_original_text('archangel'/'S99', 'Flying\nAttacking doesn\'t cause Archangel to tap.').
card_image_name('archangel'/'S99', 'archangel').
card_uid('archangel'/'S99', 'S99:Archangel:archangel').
card_rarity('archangel'/'S99', 'Rare').
card_artist('archangel'/'S99', 'Quinton Hoover').
card_number('archangel'/'S99', '4').
card_multiverse_id('archangel'/'S99', '12825').

card_in_set('ardent militia', 'S99').
card_original_type('ardent militia'/'S99', 'Creature — Soldier').
card_original_text('ardent militia'/'S99', 'Attacking doesn\'t cause Ardent Militia to tap.').
card_image_name('ardent militia'/'S99', 'ardent militia').
card_uid('ardent militia'/'S99', 'S99:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'S99', 'Uncommon').
card_artist('ardent militia'/'S99', 'Zina Saunders').
card_number('ardent militia'/'S99', '5').
card_flavor_text('ardent militia'/'S99', 'Some fight for honor and some for gold, but the militia fight for hearth and home.').
card_multiverse_id('ardent militia'/'S99', '21062').

card_in_set('armageddon', 'S99').
card_original_type('armageddon'/'S99', 'Sorcery').
card_original_text('armageddon'/'S99', 'Destroy all lands. (This includes your lands.)').
card_image_name('armageddon'/'S99', 'armageddon').
card_uid('armageddon'/'S99', 'S99:Armageddon:armageddon').
card_rarity('armageddon'/'S99', 'Rare').
card_artist('armageddon'/'S99', 'Rob Alexander').
card_number('armageddon'/'S99', '6').
card_multiverse_id('armageddon'/'S99', '20387').

card_in_set('barbtooth wurm', 'S99').
card_original_type('barbtooth wurm'/'S99', 'Creature — Wurm').
card_original_text('barbtooth wurm'/'S99', '').
card_image_name('barbtooth wurm'/'S99', 'barbtooth wurm').
card_uid('barbtooth wurm'/'S99', 'S99:Barbtooth Wurm:barbtooth wurm').
card_rarity('barbtooth wurm'/'S99', 'Common').
card_artist('barbtooth wurm'/'S99', 'Rebecca Guay').
card_number('barbtooth wurm'/'S99', '125').
card_flavor_text('barbtooth wurm'/'S99', 'In its lair lies a carpet of bones.').
card_multiverse_id('barbtooth wurm'/'S99', '21077').

card_in_set('bargain', 'S99').
card_original_type('bargain'/'S99', 'Sorcery').
card_original_text('bargain'/'S99', 'Target opponent draws a card.\nYou gain 7 life.').
card_image_name('bargain'/'S99', 'bargain').
card_uid('bargain'/'S99', 'S99:Bargain:bargain').
card_rarity('bargain'/'S99', 'Uncommon').
card_artist('bargain'/'S99', 'Phil Foglio').
card_number('bargain'/'S99', '7').
card_flavor_text('bargain'/'S99', 'Bargaining with a goblin is like trading with a child; both believe they already own everything.').
card_multiverse_id('bargain'/'S99', '21024').

card_in_set('blinding light', 'S99').
card_original_type('blinding light'/'S99', 'Sorcery').
card_original_text('blinding light'/'S99', 'Tap all nonwhite creatures.').
card_image_name('blinding light'/'S99', 'blinding light').
card_uid('blinding light'/'S99', 'S99:Blinding Light:blinding light').
card_rarity('blinding light'/'S99', 'Rare').
card_artist('blinding light'/'S99', 'John Coulthart').
card_number('blinding light'/'S99', '8').
card_flavor_text('blinding light'/'S99', 'Let the unjust avert their faces and contemplate their peril.').
card_multiverse_id('blinding light'/'S99', '20388').

card_in_set('bog imp', 'S99').
card_original_type('bog imp'/'S99', 'Creature — Imp').
card_original_text('bog imp'/'S99', 'Flying').
card_image_name('bog imp'/'S99', 'bog imp').
card_uid('bog imp'/'S99', 'S99:Bog Imp:bog imp').
card_rarity('bog imp'/'S99', 'Common').
card_artist('bog imp'/'S99', 'Christopher Rush').
card_number('bog imp'/'S99', '65').
card_flavor_text('bog imp'/'S99', 'Don\'t be fooled by their looks. Think of them as little knives with wings.').
card_multiverse_id('bog imp'/'S99', '20376').

card_in_set('bog raiders', 'S99').
card_original_type('bog raiders'/'S99', 'Creature — Zombie').
card_original_text('bog raiders'/'S99', 'Swampwalk (This creature is unblockable as long as defending player has a swamp in play.)').
card_image_name('bog raiders'/'S99', 'bog raiders').
card_uid('bog raiders'/'S99', 'S99:Bog Raiders:bog raiders').
card_rarity('bog raiders'/'S99', 'Common').
card_artist('bog raiders'/'S99', 'Carl Critchlow').
card_number('bog raiders'/'S99', '66').
card_flavor_text('bog raiders'/'S99', 'Those who live amid decay must expect scavengers.').
card_multiverse_id('bog raiders'/'S99', '20384').

card_in_set('bog wraith', 'S99').
card_original_type('bog wraith'/'S99', 'Creature — Wraith').
card_original_text('bog wraith'/'S99', 'Swampwalk (This creature is unblockable as long as defending player has a swamp in play.)').
card_image_name('bog wraith'/'S99', 'bog wraith').
card_uid('bog wraith'/'S99', 'S99:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'S99', 'Uncommon').
card_artist('bog wraith'/'S99', 'Jeff A. Menges').
card_number('bog wraith'/'S99', '67').
card_flavor_text('bog wraith'/'S99', 'It moved through the troops leaving no footprints, save on their souls.').
card_multiverse_id('bog wraith'/'S99', '20374').

card_in_set('border guard', 'S99').
card_original_type('border guard'/'S99', 'Creature — Soldier').
card_original_text('border guard'/'S99', '').
card_image_name('border guard'/'S99', 'border guard').
card_uid('border guard'/'S99', 'S99:Border Guard:border guard').
card_rarity('border guard'/'S99', 'Common').
card_artist('border guard'/'S99', 'Kev Walker').
card_number('border guard'/'S99', '9').
card_flavor_text('border guard'/'S99', '\"Join the army, see foreign countries!\" they\'d said.').
card_multiverse_id('border guard'/'S99', '20396').

card_in_set('breath of life', 'S99').
card_original_type('breath of life'/'S99', 'Sorcery').
card_original_text('breath of life'/'S99', 'Put target creature card from your graveyard into play.').
card_image_name('breath of life'/'S99', 'breath of life').
card_uid('breath of life'/'S99', 'S99:Breath of Life:breath of life').
card_rarity('breath of life'/'S99', 'Uncommon').
card_artist('breath of life'/'S99', 'DiTerlizzi').
card_number('breath of life'/'S99', '10').
card_multiverse_id('breath of life'/'S99', '21022').

card_in_set('bull hippo', 'S99').
card_original_type('bull hippo'/'S99', 'Creature — Hippo').
card_original_text('bull hippo'/'S99', 'Islandwalk (This creature is unblockable as long as defending player has an island in play.)').
card_image_name('bull hippo'/'S99', 'bull hippo').
card_uid('bull hippo'/'S99', 'S99:Bull Hippo:bull hippo').
card_rarity('bull hippo'/'S99', 'Uncommon').
card_artist('bull hippo'/'S99', 'Daren Bader').
card_number('bull hippo'/'S99', '126').
card_flavor_text('bull hippo'/'S99', 'Stay alert: his lumbering may shake the ground itself, but in water, he glides.').
card_multiverse_id('bull hippo'/'S99', '20226').

card_in_set('champion lancer', 'S99').
card_original_type('champion lancer'/'S99', 'Creature — Knight').
card_original_text('champion lancer'/'S99', 'Prevent all damage that would be dealt to Champion Lancer by creatures.').
card_first_print('champion lancer', 'S99').
card_image_name('champion lancer'/'S99', 'champion lancer').
card_uid('champion lancer'/'S99', 'S99:Champion Lancer:champion lancer').
card_rarity('champion lancer'/'S99', 'Rare').
card_artist('champion lancer'/'S99', 'Chippy').
card_number('champion lancer'/'S99', '11').
card_flavor_text('champion lancer'/'S99', 'The flash of his lance projects the pure radiance of his honor.').
card_multiverse_id('champion lancer'/'S99', '20184').

card_in_set('charging paladin', 'S99').
card_original_type('charging paladin'/'S99', 'Creature — Knight').
card_original_text('charging paladin'/'S99', 'Whenever Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_image_name('charging paladin'/'S99', 'charging paladin').
card_uid('charging paladin'/'S99', 'S99:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'S99', 'Uncommon').
card_artist('charging paladin'/'S99', 'Kev Walker').
card_number('charging paladin'/'S99', '12').
card_flavor_text('charging paladin'/'S99', 'A true warrior\'s thoughts are of victory, not death.').
card_multiverse_id('charging paladin'/'S99', '20402').

card_in_set('chorus of woe', 'S99').
card_original_type('chorus of woe'/'S99', 'Sorcery').
card_original_text('chorus of woe'/'S99', 'Creatures you control get +1/+0 until end of turn.').
card_image_name('chorus of woe'/'S99', 'chorus of woe').
card_uid('chorus of woe'/'S99', 'S99:Chorus of Woe:chorus of woe').
card_rarity('chorus of woe'/'S99', 'Common').
card_artist('chorus of woe'/'S99', 'Randy Gallegos').
card_number('chorus of woe'/'S99', '68').
card_flavor_text('chorus of woe'/'S99', 'When nightstalkers sing, nothing in creation sleeps.').
card_multiverse_id('chorus of woe'/'S99', '20381').

card_in_set('cinder storm', 'S99').
card_original_type('cinder storm'/'S99', 'Sorcery').
card_original_text('cinder storm'/'S99', 'Cinder Storm deals 7 damage to target creature or player.').
card_first_print('cinder storm', 'S99').
card_image_name('cinder storm'/'S99', 'cinder storm').
card_uid('cinder storm'/'S99', 'S99:Cinder Storm:cinder storm').
card_rarity('cinder storm'/'S99', 'Uncommon').
card_artist('cinder storm'/'S99', 'Mark Tedin').
card_number('cinder storm'/'S99', '93').
card_flavor_text('cinder storm'/'S99', 'When the sky\'s rain has turned to fire, what will put it out?').
card_multiverse_id('cinder storm'/'S99', '20201').

card_in_set('coercion', 'S99').
card_original_type('coercion'/'S99', 'Sorcery').
card_original_text('coercion'/'S99', 'Look at target player\'s hand and choose a card from it. That player discards that card.').
card_image_name('coercion'/'S99', 'coercion').
card_uid('coercion'/'S99', 'S99:Coercion:coercion').
card_rarity('coercion'/'S99', 'Uncommon').
card_artist('coercion'/'S99', 'Jeffrey R. Busch').
card_number('coercion'/'S99', '69').
card_flavor_text('coercion'/'S99', '\"Human tenderness is simply weakness in a pretty package.\"').
card_multiverse_id('coercion'/'S99', '20366').

card_in_set('coral eel', 'S99').
card_original_type('coral eel'/'S99', 'Creature — Eel').
card_original_text('coral eel'/'S99', '').
card_image_name('coral eel'/'S99', 'coral eel').
card_uid('coral eel'/'S99', 'S99:Coral Eel:coral eel').
card_rarity('coral eel'/'S99', 'Common').
card_artist('coral eel'/'S99', 'Una Fricker').
card_number('coral eel'/'S99', '33').
card_flavor_text('coral eel'/'S99', 'Some fishers like to eat eels, and some eels like to eat fishers.').
card_multiverse_id('coral eel'/'S99', '21025').

card_in_set('counterspell', 'S99').
card_original_type('counterspell'/'S99', 'Instant').
card_original_text('counterspell'/'S99', 'Counter target spell.').
card_image_name('counterspell'/'S99', 'counterspell').
card_uid('counterspell'/'S99', 'S99:Counterspell:counterspell').
card_rarity('counterspell'/'S99', 'Uncommon').
card_artist('counterspell'/'S99', 'Hannibal King').
card_number('counterspell'/'S99', '34').
card_multiverse_id('counterspell'/'S99', '20382').

card_in_set('dakmor ghoul', 'S99').
card_original_type('dakmor ghoul'/'S99', 'Creature — Zombie').
card_original_text('dakmor ghoul'/'S99', 'When Dakmor Ghoul comes into play, target opponent loses 2 life. You gain 2 life.').
card_first_print('dakmor ghoul', 'S99').
card_image_name('dakmor ghoul'/'S99', 'dakmor ghoul').
card_uid('dakmor ghoul'/'S99', 'S99:Dakmor Ghoul:dakmor ghoul').
card_rarity('dakmor ghoul'/'S99', 'Uncommon').
card_artist('dakmor ghoul'/'S99', 'Dana Knutson').
card_number('dakmor ghoul'/'S99', '70').
card_flavor_text('dakmor ghoul'/'S99', '\"Cursed be the sickly forms that err from honest Nature\'s rule!\"\n—Alfred, Lord Tennyson, \"Locksley Hall\"').
card_multiverse_id('dakmor ghoul'/'S99', '20380').

card_in_set('dakmor lancer', 'S99').
card_original_type('dakmor lancer'/'S99', 'Creature — Knight').
card_original_text('dakmor lancer'/'S99', 'When Dakmor Lancer comes into play, destroy target nonblack creature.').
card_first_print('dakmor lancer', 'S99').
card_image_name('dakmor lancer'/'S99', 'dakmor lancer').
card_uid('dakmor lancer'/'S99', 'S99:Dakmor Lancer:dakmor lancer').
card_rarity('dakmor lancer'/'S99', 'Rare').
card_artist('dakmor lancer'/'S99', 'Chippy').
card_number('dakmor lancer'/'S99', '71').
card_flavor_text('dakmor lancer'/'S99', 'The darkness of his shield reflects the inky blackness of his soul.').
card_multiverse_id('dakmor lancer'/'S99', '20187').

card_in_set('dakmor plague', 'S99').
card_original_type('dakmor plague'/'S99', 'Sorcery').
card_original_text('dakmor plague'/'S99', 'Dakmor Plague deals 3 damage to each creature and each player. (This includes your creatures and you.)').
card_image_name('dakmor plague'/'S99', 'dakmor plague').
card_uid('dakmor plague'/'S99', 'S99:Dakmor Plague:dakmor plague').
card_rarity('dakmor plague'/'S99', 'Uncommon').
card_artist('dakmor plague'/'S99', 'Jeff Miracola').
card_number('dakmor plague'/'S99', '72').
card_flavor_text('dakmor plague'/'S99', 'The tiniest cough can be deadlier than the fiercest dragon.').
card_multiverse_id('dakmor plague'/'S99', '20367').

card_in_set('dakmor scorpion', 'S99').
card_original_type('dakmor scorpion'/'S99', 'Creature — Scorpion').
card_original_text('dakmor scorpion'/'S99', '').
card_image_name('dakmor scorpion'/'S99', 'dakmor scorpion').
card_uid('dakmor scorpion'/'S99', 'S99:Dakmor Scorpion:dakmor scorpion').
card_rarity('dakmor scorpion'/'S99', 'Common').
card_artist('dakmor scorpion'/'S99', 'Randy Gallegos').
card_number('dakmor scorpion'/'S99', '73').
card_flavor_text('dakmor scorpion'/'S99', 'A scorpion this big you won\'t find curled up in your boot. Maybe around your boot, but not in it.').
card_multiverse_id('dakmor scorpion'/'S99', '20383').

card_in_set('dakmor sorceress', 'S99').
card_original_type('dakmor sorceress'/'S99', 'Creature — Wizard').
card_original_text('dakmor sorceress'/'S99', 'Dakmor Sorceress\'s power is equal to the number of swamps you control. (Count only the swamps you have in play, including both tapped and untapped swamps.)').
card_image_name('dakmor sorceress'/'S99', 'dakmor sorceress').
card_uid('dakmor sorceress'/'S99', 'S99:Dakmor Sorceress:dakmor sorceress').
card_rarity('dakmor sorceress'/'S99', 'Rare').
card_artist('dakmor sorceress'/'S99', 'Matthew D. Wilson').
card_number('dakmor sorceress'/'S99', '74').
card_multiverse_id('dakmor sorceress'/'S99', '20183').

card_in_set('dark offering', 'S99').
card_original_type('dark offering'/'S99', 'Sorcery').
card_original_text('dark offering'/'S99', 'Destroy target nonblack creature. You gain 3 life.').
card_image_name('dark offering'/'S99', 'dark offering').
card_uid('dark offering'/'S99', 'S99:Dark Offering:dark offering').
card_rarity('dark offering'/'S99', 'Uncommon').
card_artist('dark offering'/'S99', 'Edward P. Beard, Jr.').
card_number('dark offering'/'S99', '75').
card_flavor_text('dark offering'/'S99', '\"Our greatest hope has become our enemy\'s greatest triumph.\"\n—Court counselor').
card_multiverse_id('dark offering'/'S99', '20368').

card_in_set('denizen of the deep', 'S99').
card_original_type('denizen of the deep'/'S99', 'Creature — Serpent').
card_original_text('denizen of the deep'/'S99', 'When Denizen of the Deep comes into play, return all other creatures you control from play to their owner\'s hand.').
card_image_name('denizen of the deep'/'S99', 'denizen of the deep').
card_uid('denizen of the deep'/'S99', 'S99:Denizen of the Deep:denizen of the deep').
card_rarity('denizen of the deep'/'S99', 'Rare').
card_artist('denizen of the deep'/'S99', 'Anson Maddocks').
card_number('denizen of the deep'/'S99', '35').
card_multiverse_id('denizen of the deep'/'S99', '20182').

card_in_set('devastation', 'S99').
card_original_type('devastation'/'S99', 'Sorcery').
card_original_text('devastation'/'S99', 'Destroy all creatures and all lands. (This includes your creatures and lands.)').
card_image_name('devastation'/'S99', 'devastation').
card_uid('devastation'/'S99', 'S99:Devastation:devastation').
card_rarity('devastation'/'S99', 'Rare').
card_artist('devastation'/'S99', 'Steve Luke').
card_number('devastation'/'S99', '94').
card_flavor_text('devastation'/'S99', 'There is much talk about the art of creation. What about the art of destruction?').
card_multiverse_id('devastation'/'S99', '20191').

card_in_set('devoted hero', 'S99').
card_original_type('devoted hero'/'S99', 'Creature — Soldier').
card_original_text('devoted hero'/'S99', '').
card_image_name('devoted hero'/'S99', 'devoted hero').
card_uid('devoted hero'/'S99', 'S99:Devoted Hero:devoted hero').
card_rarity('devoted hero'/'S99', 'Common').
card_artist('devoted hero'/'S99', 'DiTerlizzi').
card_number('devoted hero'/'S99', '13').
card_flavor_text('devoted hero'/'S99', 'The heart\'s courage is the soul\'s guardian.').
card_multiverse_id('devoted hero'/'S99', '21057').

card_in_set('devout monk', 'S99').
card_original_type('devout monk'/'S99', 'Creature — Cleric').
card_original_text('devout monk'/'S99', 'When Devout Monk comes into play, you gain 1 life.').
card_first_print('devout monk', 'S99').
card_image_name('devout monk'/'S99', 'devout monk').
card_uid('devout monk'/'S99', 'S99:Devout Monk:devout monk').
card_rarity('devout monk'/'S99', 'Common').
card_artist('devout monk'/'S99', 'Daniel Gelon').
card_number('devout monk'/'S99', '14').
card_flavor_text('devout monk'/'S99', 'Discipline wears many robes.').
card_multiverse_id('devout monk'/'S99', '20395').

card_in_set('dread reaper', 'S99').
card_original_type('dread reaper'/'S99', 'Creature — Horror').
card_original_text('dread reaper'/'S99', 'Flying\nWhen Dread Reaper comes into play, you lose 5 life.').
card_image_name('dread reaper'/'S99', 'dread reaper').
card_uid('dread reaper'/'S99', 'S99:Dread Reaper:dread reaper').
card_rarity('dread reaper'/'S99', 'Rare').
card_artist('dread reaper'/'S99', 'Christopher Rush').
card_number('dread reaper'/'S99', '76').
card_multiverse_id('dread reaper'/'S99', '20362').

card_in_set('durkwood boars', 'S99').
card_original_type('durkwood boars'/'S99', 'Creature — Boar').
card_original_text('durkwood boars'/'S99', '').
card_image_name('durkwood boars'/'S99', 'durkwood boars').
card_uid('durkwood boars'/'S99', 'S99:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'S99', 'Common').
card_artist('durkwood boars'/'S99', 'Mike Kimble').
card_number('durkwood boars'/'S99', '127').
card_flavor_text('durkwood boars'/'S99', '\"And the unclean spirits went out, and entered the swine; and the herd ran violently . . . .\" —The Bible, Mark 5:13').
card_multiverse_id('durkwood boars'/'S99', '21055').

card_in_set('eager cadet', 'S99').
card_original_type('eager cadet'/'S99', 'Creature — Soldier').
card_original_text('eager cadet'/'S99', '').
card_first_print('eager cadet', 'S99').
card_image_name('eager cadet'/'S99', 'eager cadet').
card_uid('eager cadet'/'S99', 'S99:Eager Cadet:eager cadet').
card_rarity('eager cadet'/'S99', 'Common').
card_artist('eager cadet'/'S99', 'Scott M. Fischer').
card_number('eager cadet'/'S99', '15').
card_flavor_text('eager cadet'/'S99', 'The enthusiasm of the young, sharpened by the discipline of a soldier.').
card_multiverse_id('eager cadet'/'S99', '21021').

card_in_set('earth elemental', 'S99').
card_original_type('earth elemental'/'S99', 'Creature — Elemental').
card_original_text('earth elemental'/'S99', '').
card_image_name('earth elemental'/'S99', 'earth elemental').
card_uid('earth elemental'/'S99', 'S99:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'S99', 'Uncommon').
card_artist('earth elemental'/'S99', 'Dan Frazier').
card_number('earth elemental'/'S99', '95').
card_flavor_text('earth elemental'/'S99', 'Its voice has the tremor of deafening thunder, its blow the force of an avalanche.').
card_multiverse_id('earth elemental'/'S99', '20199').

card_in_set('exhaustion', 'S99').
card_original_type('exhaustion'/'S99', 'Sorcery').
card_original_text('exhaustion'/'S99', 'Creatures and lands target opponent controls don\'t untap during his or her next untap step.').
card_image_name('exhaustion'/'S99', 'exhaustion').
card_uid('exhaustion'/'S99', 'S99:Exhaustion:exhaustion').
card_rarity('exhaustion'/'S99', 'Uncommon').
card_artist('exhaustion'/'S99', 'Paolo Parente').
card_number('exhaustion'/'S99', '36').
card_multiverse_id('exhaustion'/'S99', '21044').

card_in_set('extinguish', 'S99').
card_original_type('extinguish'/'S99', 'Instant').
card_original_text('extinguish'/'S99', 'Counter target sorcery spell.').
card_image_name('extinguish'/'S99', 'extinguish').
card_uid('extinguish'/'S99', 'S99:Extinguish:extinguish').
card_rarity('extinguish'/'S99', 'Common').
card_artist('extinguish'/'S99', 'Douglas Shuler').
card_number('extinguish'/'S99', '37').
card_multiverse_id('extinguish'/'S99', '21036').

card_in_set('eye spy', 'S99').
card_original_type('eye spy'/'S99', 'Sorcery').
card_original_text('eye spy'/'S99', 'Look at the top card of target player\'s library. Put that card back on top of that library or into that player\'s graveyard.').
card_image_name('eye spy'/'S99', 'eye spy').
card_uid('eye spy'/'S99', 'S99:Eye Spy:eye spy').
card_rarity('eye spy'/'S99', 'Uncommon').
card_artist('eye spy'/'S99', 'DiTerlizzi').
card_number('eye spy'/'S99', '38').
card_multiverse_id('eye spy'/'S99', '21035').

card_in_set('false peace', 'S99').
card_original_type('false peace'/'S99', 'Sorcery').
card_original_text('false peace'/'S99', 'Target player can\'t attack on his or her next turn.').
card_image_name('false peace'/'S99', 'false peace').
card_uid('false peace'/'S99', 'S99:False Peace:false peace').
card_rarity('false peace'/'S99', 'Uncommon').
card_artist('false peace'/'S99', 'Zina Saunders').
card_number('false peace'/'S99', '16').
card_flavor_text('false peace'/'S99', 'Mutual consent is not required for war.').
card_multiverse_id('false peace'/'S99', '21063').

card_in_set('feral shadow', 'S99').
card_original_type('feral shadow'/'S99', 'Creature — Night Stalker').
card_original_text('feral shadow'/'S99', 'Flying').
card_image_name('feral shadow'/'S99', 'feral shadow').
card_uid('feral shadow'/'S99', 'S99:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'S99', 'Common').
card_artist('feral shadow'/'S99', 'Cliff Nielsen').
card_number('feral shadow'/'S99', '77').
card_flavor_text('feral shadow'/'S99', 'Not all shadows are cast by light—some are cast by darkness.').
card_multiverse_id('feral shadow'/'S99', '20373').

card_in_set('fire elemental', 'S99').
card_original_type('fire elemental'/'S99', 'Creature — Elemental').
card_original_text('fire elemental'/'S99', '').
card_image_name('fire elemental'/'S99', 'fire elemental').
card_uid('fire elemental'/'S99', 'S99:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'S99', 'Uncommon').
card_artist('fire elemental'/'S99', 'Melissa A. Benson').
card_number('fire elemental'/'S99', '96').
card_flavor_text('fire elemental'/'S99', 'You are drawn to your death as a moth to the flame.').
card_multiverse_id('fire elemental'/'S99', '20198').

card_in_set('fire tempest', 'S99').
card_original_type('fire tempest'/'S99', 'Sorcery').
card_original_text('fire tempest'/'S99', 'Fire Tempest deals 6 damage to each creature and each player. (This includes your creatures and you.)').
card_image_name('fire tempest'/'S99', 'fire tempest').
card_uid('fire tempest'/'S99', 'S99:Fire Tempest:fire tempest').
card_rarity('fire tempest'/'S99', 'Rare').
card_artist('fire tempest'/'S99', 'Mike Dringenberg').
card_number('fire tempest'/'S99', '97').
card_multiverse_id('fire tempest'/'S99', '20192').

card_in_set('foot soldiers', 'S99').
card_original_type('foot soldiers'/'S99', 'Creature — Soldier').
card_original_text('foot soldiers'/'S99', '').
card_image_name('foot soldiers'/'S99', 'foot soldiers').
card_uid('foot soldiers'/'S99', 'S99:Foot Soldiers:foot soldiers').
card_rarity('foot soldiers'/'S99', 'Common').
card_artist('foot soldiers'/'S99', 'Kev Walker').
card_number('foot soldiers'/'S99', '17').
card_flavor_text('foot soldiers'/'S99', 'Infantry deployment is the art of putting your troops in the wrong place at the right time.').
card_multiverse_id('foot soldiers'/'S99', '20398').

card_in_set('forest', 'S99').
card_original_type('forest'/'S99', 'Land').
card_original_text('forest'/'S99', 'G').
card_image_name('forest'/'S99', 'forest1').
card_uid('forest'/'S99', 'S99:Forest:forest1').
card_rarity('forest'/'S99', 'Basic Land').
card_artist('forest'/'S99', 'Quinton Hoover').
card_number('forest'/'S99', '170').
card_multiverse_id('forest'/'S99', '21807').

card_in_set('forest', 'S99').
card_original_type('forest'/'S99', 'Land').
card_original_text('forest'/'S99', 'G').
card_image_name('forest'/'S99', 'forest2').
card_uid('forest'/'S99', 'S99:Forest:forest2').
card_rarity('forest'/'S99', 'Basic Land').
card_artist('forest'/'S99', 'Quinton Hoover').
card_number('forest'/'S99', '171').
card_multiverse_id('forest'/'S99', '21808').

card_in_set('forest', 'S99').
card_original_type('forest'/'S99', 'Land').
card_original_text('forest'/'S99', 'G').
card_image_name('forest'/'S99', 'forest3').
card_uid('forest'/'S99', 'S99:Forest:forest3').
card_rarity('forest'/'S99', 'Basic Land').
card_artist('forest'/'S99', 'John Avon').
card_number('forest'/'S99', '172').
card_multiverse_id('forest'/'S99', '21805').

card_in_set('forest', 'S99').
card_original_type('forest'/'S99', 'Land').
card_original_text('forest'/'S99', 'G').
card_image_name('forest'/'S99', 'forest4').
card_uid('forest'/'S99', 'S99:Forest:forest4').
card_rarity('forest'/'S99', 'Basic Land').
card_artist('forest'/'S99', 'John Avon').
card_number('forest'/'S99', '173').
card_multiverse_id('forest'/'S99', '21806').

card_in_set('gerrard\'s wisdom', 'S99').
card_original_type('gerrard\'s wisdom'/'S99', 'Sorcery').
card_original_text('gerrard\'s wisdom'/'S99', 'For each card in your hand, you gain 2 life. (Don\'t count this card.)').
card_image_name('gerrard\'s wisdom'/'S99', 'gerrard\'s wisdom').
card_uid('gerrard\'s wisdom'/'S99', 'S99:Gerrard\'s Wisdom:gerrard\'s wisdom').
card_rarity('gerrard\'s wisdom'/'S99', 'Rare').
card_artist('gerrard\'s wisdom'/'S99', 'Heather Hudson').
card_number('gerrard\'s wisdom'/'S99', '18').
card_flavor_text('gerrard\'s wisdom'/'S99', '\"You\'ll lose a duel if your enemy comes expecting a war.\"').
card_multiverse_id('gerrard\'s wisdom'/'S99', '20386').

card_in_set('giant octopus', 'S99').
card_original_type('giant octopus'/'S99', 'Creature — Octopus').
card_original_text('giant octopus'/'S99', '').
card_image_name('giant octopus'/'S99', 'giant octopus').
card_uid('giant octopus'/'S99', 'S99:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'S99', 'Common').
card_artist('giant octopus'/'S99', 'John Matson').
card_number('giant octopus'/'S99', '39').
card_flavor_text('giant octopus'/'S99', 'At the sight of the thing the calamari vendor\'s eyes went wide, but from fear or avarice none could tell.').
card_multiverse_id('giant octopus'/'S99', '21027').

card_in_set('goblin cavaliers', 'S99').
card_original_type('goblin cavaliers'/'S99', 'Creature — Goblin').
card_original_text('goblin cavaliers'/'S99', '').
card_image_name('goblin cavaliers'/'S99', 'goblin cavaliers').
card_uid('goblin cavaliers'/'S99', 'S99:Goblin Cavaliers:goblin cavaliers').
card_rarity('goblin cavaliers'/'S99', 'Common').
card_artist('goblin cavaliers'/'S99', 'DiTerlizzi').
card_number('goblin cavaliers'/'S99', '98').
card_flavor_text('goblin cavaliers'/'S99', 'They get along so well with their goats because they\'re practically goats themselves.').
card_multiverse_id('goblin cavaliers'/'S99', '20214').

card_in_set('goblin chariot', 'S99').
card_original_type('goblin chariot'/'S99', 'Creature — Goblin').
card_original_text('goblin chariot'/'S99', 'Haste (This creature may attack the turn it comes into play.)').
card_first_print('goblin chariot', 'S99').
card_image_name('goblin chariot'/'S99', 'goblin chariot').
card_uid('goblin chariot'/'S99', 'S99:Goblin Chariot:goblin chariot').
card_rarity('goblin chariot'/'S99', 'Common').
card_artist('goblin chariot'/'S99', 'Pete Venters').
card_number('goblin chariot'/'S99', '99').
card_flavor_text('goblin chariot'/'S99', 'They have to replace the wheels, they have to replace the boar, and sometimes they have to replace the goblin.').
card_multiverse_id('goblin chariot'/'S99', '20213').

card_in_set('goblin commando', 'S99').
card_original_type('goblin commando'/'S99', 'Creature — Goblin').
card_original_text('goblin commando'/'S99', 'When Goblin Commando comes into play, it deals 2 damage to target creature.').
card_first_print('goblin commando', 'S99').
card_image_name('goblin commando'/'S99', 'goblin commando').
card_uid('goblin commando'/'S99', 'S99:Goblin Commando:goblin commando').
card_rarity('goblin commando'/'S99', 'Uncommon').
card_artist('goblin commando'/'S99', 'Todd Lockwood').
card_number('goblin commando'/'S99', '100').
card_flavor_text('goblin commando'/'S99', 'With a commando around, somebody\'s gonna get hurt.').
card_multiverse_id('goblin commando'/'S99', '20196').

card_in_set('goblin general', 'S99').
card_original_type('goblin general'/'S99', 'Creature — Goblin').
card_original_text('goblin general'/'S99', 'When Goblin General attacks, all Goblins you control get +1/+1 until end of turn.').
card_image_name('goblin general'/'S99', 'goblin general').
card_uid('goblin general'/'S99', 'S99:Goblin General:goblin general').
card_rarity('goblin general'/'S99', 'Uncommon').
card_artist('goblin general'/'S99', 'Keith Parkinson').
card_number('goblin general'/'S99', '101').
card_flavor_text('goblin general'/'S99', 'Lead, follow, or run around like crazy.').
card_multiverse_id('goblin general'/'S99', '20185').

card_in_set('goblin glider', 'S99').
card_original_type('goblin glider'/'S99', 'Creature — Goblin').
card_original_text('goblin glider'/'S99', 'Flying\nGoblin Glider can\'t block.').
card_image_name('goblin glider'/'S99', 'goblin glider').
card_uid('goblin glider'/'S99', 'S99:Goblin Glider:goblin glider').
card_rarity('goblin glider'/'S99', 'Uncommon').
card_artist('goblin glider'/'S99', 'Pete Venters').
card_number('goblin glider'/'S99', '102').
card_flavor_text('goblin glider'/'S99', 'The goblins call the gliders \"death from above.\" Everyone else calls them \"clods in the clouds.\"').
card_multiverse_id('goblin glider'/'S99', '20195').

card_in_set('goblin hero', 'S99').
card_original_type('goblin hero'/'S99', 'Creature — Goblin').
card_original_text('goblin hero'/'S99', '').
card_image_name('goblin hero'/'S99', 'goblin hero').
card_uid('goblin hero'/'S99', 'S99:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'S99', 'Common').
card_artist('goblin hero'/'S99', 'Pete Venters').
card_number('goblin hero'/'S99', '103').
card_flavor_text('goblin hero'/'S99', 'When you\'re a goblin, you don\'t have to step forward to be a hero—everyone else just has to step back.').
card_multiverse_id('goblin hero'/'S99', '21066').

card_in_set('goblin lore', 'S99').
card_original_type('goblin lore'/'S99', 'Sorcery').
card_original_text('goblin lore'/'S99', 'Draw four cards, then discard three cards at random from your hand.').
card_image_name('goblin lore'/'S99', 'goblin lore').
card_uid('goblin lore'/'S99', 'S99:Goblin Lore:goblin lore').
card_rarity('goblin lore'/'S99', 'Uncommon').
card_artist('goblin lore'/'S99', 'D. Alexander Gregory').
card_number('goblin lore'/'S99', '104').
card_flavor_text('goblin lore'/'S99', '\"I done forgot more than you\'ll ever know, pipsqueak.\"\n\"Yeah—that\'s your problem.\"').
card_multiverse_id('goblin lore'/'S99', '20202').

card_in_set('goblin mountaineer', 'S99').
card_original_type('goblin mountaineer'/'S99', 'Creature — Goblin').
card_original_text('goblin mountaineer'/'S99', 'Mountainwalk (This creature is unblockable as long as defending player has a mountain in play.)').
card_image_name('goblin mountaineer'/'S99', 'goblin mountaineer').
card_uid('goblin mountaineer'/'S99', 'S99:Goblin Mountaineer:goblin mountaineer').
card_rarity('goblin mountaineer'/'S99', 'Common').
card_artist('goblin mountaineer'/'S99', 'DiTerlizzi').
card_number('goblin mountaineer'/'S99', '105').
card_flavor_text('goblin mountaineer'/'S99', 'Goblin mountaineer, barely keeps his family fed.').
card_multiverse_id('goblin mountaineer'/'S99', '20211').

card_in_set('goblin settler', 'S99').
card_original_type('goblin settler'/'S99', 'Creature — Goblin').
card_original_text('goblin settler'/'S99', 'When Goblin Settler comes into play, destroy target land.').
card_first_print('goblin settler', 'S99').
card_image_name('goblin settler'/'S99', 'goblin settler').
card_uid('goblin settler'/'S99', 'S99:Goblin Settler:goblin settler').
card_rarity('goblin settler'/'S99', 'Uncommon').
card_artist('goblin settler'/'S99', 'Carl Critchlow').
card_number('goblin settler'/'S99', '106').
card_flavor_text('goblin settler'/'S99', 'Be it ever so crumbled, there\'s no place like home.').
card_multiverse_id('goblin settler'/'S99', '20216').

card_in_set('gorilla warrior', 'S99').
card_original_type('gorilla warrior'/'S99', 'Creature — Ape').
card_original_text('gorilla warrior'/'S99', '').
card_image_name('gorilla warrior'/'S99', 'gorilla warrior').
card_uid('gorilla warrior'/'S99', 'S99:Gorilla Warrior:gorilla warrior').
card_rarity('gorilla warrior'/'S99', 'Common').
card_artist('gorilla warrior'/'S99', 'John Matson').
card_number('gorilla warrior'/'S99', '128').
card_flavor_text('gorilla warrior'/'S99', 'They were formidable even before they learned the use of weapons.').
card_multiverse_id('gorilla warrior'/'S99', '21073').

card_in_set('gravedigger', 'S99').
card_original_type('gravedigger'/'S99', 'Creature — Zombie').
card_original_text('gravedigger'/'S99', 'When Gravedigger comes into play, return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'S99', 'gravedigger').
card_uid('gravedigger'/'S99', 'S99:Gravedigger:gravedigger').
card_rarity('gravedigger'/'S99', 'Uncommon').
card_artist('gravedigger'/'S99', 'Dermot Power').
card_number('gravedigger'/'S99', '78').
card_multiverse_id('gravedigger'/'S99', '20372').

card_in_set('grim tutor', 'S99').
card_original_type('grim tutor'/'S99', 'Sorcery').
card_original_text('grim tutor'/'S99', 'Search your library for any card and put that card into your hand. You lose 3 life.').
card_first_print('grim tutor', 'S99').
card_image_name('grim tutor'/'S99', 'grim tutor').
card_uid('grim tutor'/'S99', 'S99:Grim Tutor:grim tutor').
card_rarity('grim tutor'/'S99', 'Rare').
card_artist('grim tutor'/'S99', 'Mark Tedin').
card_number('grim tutor'/'S99', '79').
card_flavor_text('grim tutor'/'S99', 'He who goes unpunished never learns.\n—Greek proverb').
card_multiverse_id('grim tutor'/'S99', '20359').

card_in_set('grizzly bears', 'S99').
card_original_type('grizzly bears'/'S99', 'Creature — Bear').
card_original_text('grizzly bears'/'S99', '').
card_image_name('grizzly bears'/'S99', 'grizzly bears').
card_uid('grizzly bears'/'S99', 'S99:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'S99', 'Common').
card_artist('grizzly bears'/'S99', 'Una Fricker').
card_number('grizzly bears'/'S99', '129').
card_flavor_text('grizzly bears'/'S99', 'Don\'t worry about provoking grizzly bears; they come that way.').
card_multiverse_id('grizzly bears'/'S99', '20233').

card_in_set('hand of death', 'S99').
card_original_type('hand of death'/'S99', 'Sorcery').
card_original_text('hand of death'/'S99', 'Destroy target nonblack creature.').
card_image_name('hand of death'/'S99', 'hand of death').
card_uid('hand of death'/'S99', 'S99:Hand of Death:hand of death').
card_rarity('hand of death'/'S99', 'Common').
card_artist('hand of death'/'S99', 'Heather Hudson').
card_number('hand of death'/'S99', '80').
card_flavor_text('hand of death'/'S99', 'The touch of death is never gentle.').
card_multiverse_id('hand of death'/'S99', '20379').

card_in_set('hollow dogs', 'S99').
card_original_type('hollow dogs'/'S99', 'Creature — Hound').
card_original_text('hollow dogs'/'S99', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_image_name('hollow dogs'/'S99', 'hollow dogs').
card_uid('hollow dogs'/'S99', 'S99:Hollow Dogs:hollow dogs').
card_rarity('hollow dogs'/'S99', 'Common').
card_artist('hollow dogs'/'S99', 'Jeff Miracola').
card_number('hollow dogs'/'S99', '81').
card_flavor_text('hollow dogs'/'S99', 'A hollow dog is never empty. It is filled with thirst for the hunt.').
card_multiverse_id('hollow dogs'/'S99', '20385').

card_in_set('howling fury', 'S99').
card_original_type('howling fury'/'S99', 'Sorcery').
card_original_text('howling fury'/'S99', 'Target creature gets +4/+0 until end of turn.').
card_image_name('howling fury'/'S99', 'howling fury').
card_uid('howling fury'/'S99', 'S99:Howling Fury:howling fury').
card_rarity('howling fury'/'S99', 'Uncommon').
card_artist('howling fury'/'S99', 'Mike Dringenberg').
card_number('howling fury'/'S99', '82').
card_flavor_text('howling fury'/'S99', 'I howl my soul to the moon, and the moon howls with me.').
card_multiverse_id('howling fury'/'S99', '20369').

card_in_set('hulking goblin', 'S99').
card_original_type('hulking goblin'/'S99', 'Creature — Goblin').
card_original_text('hulking goblin'/'S99', 'Hulking Goblin can\'t block.').
card_image_name('hulking goblin'/'S99', 'hulking goblin').
card_uid('hulking goblin'/'S99', 'S99:Hulking Goblin:hulking goblin').
card_rarity('hulking goblin'/'S99', 'Common').
card_artist('hulking goblin'/'S99', 'Pete Venters').
card_number('hulking goblin'/'S99', '107').
card_flavor_text('hulking goblin'/'S99', 'The bigger they are, the harder they avoid work.').
card_multiverse_id('hulking goblin'/'S99', '20212').

card_in_set('hulking ogre', 'S99').
card_original_type('hulking ogre'/'S99', 'Creature — Ogre').
card_original_text('hulking ogre'/'S99', 'Hulking Ogre can\'t block.').
card_image_name('hulking ogre'/'S99', 'hulking ogre').
card_uid('hulking ogre'/'S99', 'S99:Hulking Ogre:hulking ogre').
card_rarity('hulking ogre'/'S99', 'Uncommon').
card_artist('hulking ogre'/'S99', 'Greg & Tim Hildebrandt').
card_number('hulking ogre'/'S99', '108').
card_flavor_text('hulking ogre'/'S99', 'Once they realized the ogre had more size than speed, the soldiers simply went around it.').
card_multiverse_id('hulking ogre'/'S99', '20197').

card_in_set('ingenious thief', 'S99').
card_original_type('ingenious thief'/'S99', 'Creature — Thief').
card_original_text('ingenious thief'/'S99', 'Flying\nWhen Ingenious Thief comes into play, look at target player\'s hand.').
card_image_name('ingenious thief'/'S99', 'ingenious thief').
card_uid('ingenious thief'/'S99', 'S99:Ingenious Thief:ingenious thief').
card_rarity('ingenious thief'/'S99', 'Common').
card_artist('ingenious thief'/'S99', 'Dan Frazier').
card_number('ingenious thief'/'S99', '40').
card_multiverse_id('ingenious thief'/'S99', '21043').

card_in_set('island', 'S99').
card_original_type('island'/'S99', 'Land').
card_original_text('island'/'S99', 'U').
card_image_name('island'/'S99', 'island1').
card_uid('island'/'S99', 'S99:Island:island1').
card_rarity('island'/'S99', 'Basic Land').
card_artist('island'/'S99', 'Douglas Shuler').
card_number('island'/'S99', '158').
card_multiverse_id('island'/'S99', '21793').

card_in_set('island', 'S99').
card_original_type('island'/'S99', 'Land').
card_original_text('island'/'S99', 'U').
card_image_name('island'/'S99', 'island2').
card_uid('island'/'S99', 'S99:Island:island2').
card_rarity('island'/'S99', 'Basic Land').
card_artist('island'/'S99', 'J. W. Frost').
card_number('island'/'S99', '159').
card_multiverse_id('island'/'S99', '21794').

card_in_set('island', 'S99').
card_original_type('island'/'S99', 'Land').
card_original_text('island'/'S99', 'U').
card_image_name('island'/'S99', 'island3').
card_uid('island'/'S99', 'S99:Island:island3').
card_rarity('island'/'S99', 'Basic Land').
card_artist('island'/'S99', 'John Avon').
card_number('island'/'S99', '160').
card_multiverse_id('island'/'S99', '21800').

card_in_set('island', 'S99').
card_original_type('island'/'S99', 'Land').
card_original_text('island'/'S99', 'U').
card_image_name('island'/'S99', 'island4').
card_uid('island'/'S99', 'S99:Island:island4').
card_rarity('island'/'S99', 'Basic Land').
card_artist('island'/'S99', 'Eric Peterson').
card_number('island'/'S99', '161').
card_multiverse_id('island'/'S99', '21795').

card_in_set('jagged lightning', 'S99').
card_original_type('jagged lightning'/'S99', 'Sorcery').
card_original_text('jagged lightning'/'S99', 'Jagged Lightning deals 3 damage to target creature and 3 damage to another target creature. (You can\'t play this card unless you can choose two creatures in play.)').
card_image_name('jagged lightning'/'S99', 'jagged lightning').
card_uid('jagged lightning'/'S99', 'S99:Jagged Lightning:jagged lightning').
card_rarity('jagged lightning'/'S99', 'Uncommon').
card_artist('jagged lightning'/'S99', 'Michael Weaver').
card_number('jagged lightning'/'S99', '109').
card_multiverse_id('jagged lightning'/'S99', '20203').

card_in_set('knight errant', 'S99').
card_original_type('knight errant'/'S99', 'Creature — Knight').
card_original_text('knight errant'/'S99', '').
card_image_name('knight errant'/'S99', 'knight errant').
card_uid('knight errant'/'S99', 'S99:Knight Errant:knight errant').
card_rarity('knight errant'/'S99', 'Common').
card_artist('knight errant'/'S99', 'Dan Frazier').
card_number('knight errant'/'S99', '19').
card_flavor_text('knight errant'/'S99', '\". . . Before honor is humility.\"\n—The Bible, Proverbs 15:33').
card_multiverse_id('knight errant'/'S99', '20397').

card_in_set('last chance', 'S99').
card_original_type('last chance'/'S99', 'Sorcery').
card_original_text('last chance'/'S99', 'Take another turn after this one. You lose the game at the end of that turn. (You won\'t lose if you\'ve won before the end of that turn.)').
card_image_name('last chance'/'S99', 'last chance').
card_uid('last chance'/'S99', 'S99:Last Chance:last chance').
card_rarity('last chance'/'S99', 'Rare').
card_artist('last chance'/'S99', 'Hannibal King').
card_number('last chance'/'S99', '110').
card_multiverse_id('last chance'/'S99', '20194').

card_in_set('lava axe', 'S99').
card_original_type('lava axe'/'S99', 'Sorcery').
card_original_text('lava axe'/'S99', 'Lava Axe deals 5 damage to target opponent.').
card_image_name('lava axe'/'S99', 'lava axe').
card_uid('lava axe'/'S99', 'S99:Lava Axe:lava axe').
card_rarity('lava axe'/'S99', 'Common').
card_artist('lava axe'/'S99', 'Brian Snõddy').
card_number('lava axe'/'S99', '111').
card_flavor_text('lava axe'/'S99', 'Meant to cut through the body and burn straight to the soul.').
card_multiverse_id('lava axe'/'S99', '20205').

card_in_set('lone wolf', 'S99').
card_original_type('lone wolf'/'S99', 'Creature — Wolf').
card_original_text('lone wolf'/'S99', 'Lone Wolf may deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('lone wolf'/'S99', 'lone wolf').
card_uid('lone wolf'/'S99', 'S99:Lone Wolf:lone wolf').
card_rarity('lone wolf'/'S99', 'Common').
card_artist('lone wolf'/'S99', 'Una Fricker').
card_number('lone wolf'/'S99', '130').
card_multiverse_id('lone wolf'/'S99', '20225').

card_in_set('loyal sentry', 'S99').
card_original_type('loyal sentry'/'S99', 'Creature — Soldier').
card_original_text('loyal sentry'/'S99', 'When Loyal Sentry blocks, destroy it and the creature it blocks. (Destroy both creatures before dealing damage.)').
card_first_print('loyal sentry', 'S99').
card_image_name('loyal sentry'/'S99', 'loyal sentry').
card_uid('loyal sentry'/'S99', 'S99:Loyal Sentry:loyal sentry').
card_rarity('loyal sentry'/'S99', 'Rare').
card_artist('loyal sentry'/'S99', 'Ron Spears').
card_number('loyal sentry'/'S99', '20').
card_multiverse_id('loyal sentry'/'S99', '20390').

card_in_set('lynx', 'S99').
card_original_type('lynx'/'S99', 'Creature — Cat').
card_original_text('lynx'/'S99', 'Forestwalk (This creature is unblockable as long as defending player has a forest in play.)').
card_image_name('lynx'/'S99', 'lynx').
card_uid('lynx'/'S99', 'S99:Lynx:lynx').
card_rarity('lynx'/'S99', 'Uncommon').
card_artist('lynx'/'S99', 'Rebecca Guay').
card_number('lynx'/'S99', '131').
card_flavor_text('lynx'/'S99', 'Rarely seen, hardly heard, never caught.').
card_multiverse_id('lynx'/'S99', '21053').

card_in_set('man-o\'-war', 'S99').
card_original_type('man-o\'-war'/'S99', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'S99', 'When Man-o\'-War comes into play, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'S99', 'man-o\'-war').
card_uid('man-o\'-war'/'S99', 'S99:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'S99', 'Uncommon').
card_artist('man-o\'-war'/'S99', 'Una Fricker').
card_number('man-o\'-war'/'S99', '41').
card_flavor_text('man-o\'-war'/'S99', 'Beauty to the eye does not always translate to the touch.').
card_multiverse_id('man-o\'-war'/'S99', '21038').

card_in_set('merfolk of the pearl trident', 'S99').
card_original_type('merfolk of the pearl trident'/'S99', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'S99', '').
card_image_name('merfolk of the pearl trident'/'S99', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'S99', 'S99:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'S99', 'Common').
card_artist('merfolk of the pearl trident'/'S99', 'DiTerlizzi').
card_number('merfolk of the pearl trident'/'S99', '42').
card_flavor_text('merfolk of the pearl trident'/'S99', 'Are merfolk humans with fins, or are humans merfolk with feet?').
card_multiverse_id('merfolk of the pearl trident'/'S99', '21080').

card_in_set('mind rot', 'S99').
card_original_type('mind rot'/'S99', 'Sorcery').
card_original_text('mind rot'/'S99', 'Target player chooses and discards two cards from his or her hand. (If that player has only one card, he or she discards it.)').
card_image_name('mind rot'/'S99', 'mind rot').
card_uid('mind rot'/'S99', 'S99:Mind Rot:mind rot').
card_rarity('mind rot'/'S99', 'Common').
card_artist('mind rot'/'S99', 'Steve Luke').
card_number('mind rot'/'S99', '83').
card_multiverse_id('mind rot'/'S99', '20378').

card_in_set('mons\'s goblin raiders', 'S99').
card_original_type('mons\'s goblin raiders'/'S99', 'Creature — Goblin').
card_original_text('mons\'s goblin raiders'/'S99', '').
card_image_name('mons\'s goblin raiders'/'S99', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'S99', 'S99:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'S99', 'Common').
card_artist('mons\'s goblin raiders'/'S99', 'Pete Venters').
card_number('mons\'s goblin raiders'/'S99', '112').
card_flavor_text('mons\'s goblin raiders'/'S99', 'Just because they have a club doesn\'t mean they\'re organized.').
card_multiverse_id('mons\'s goblin raiders'/'S99', '21065').

card_in_set('monstrous growth', 'S99').
card_original_type('monstrous growth'/'S99', 'Sorcery').
card_original_text('monstrous growth'/'S99', 'Target creature gets +4/+4 until end of turn.').
card_image_name('monstrous growth'/'S99', 'monstrous growth').
card_uid('monstrous growth'/'S99', 'S99:Monstrous Growth:monstrous growth').
card_rarity('monstrous growth'/'S99', 'Common').
card_artist('monstrous growth'/'S99', 'Una Fricker').
card_number('monstrous growth'/'S99', '132').
card_flavor_text('monstrous growth'/'S99', 'The nightstalkers\' little game of tease-the-squirrel suddenly took an unexpected turn.').
card_multiverse_id('monstrous growth'/'S99', '20234').

card_in_set('moon sprite', 'S99').
card_original_type('moon sprite'/'S99', 'Creature — Faerie').
card_original_text('moon sprite'/'S99', 'Flying').
card_image_name('moon sprite'/'S99', 'moon sprite').
card_uid('moon sprite'/'S99', 'S99:Moon Sprite:moon sprite').
card_rarity('moon sprite'/'S99', 'Uncommon').
card_artist('moon sprite'/'S99', 'Terese Nielsen').
card_number('moon sprite'/'S99', '133').
card_flavor_text('moon sprite'/'S99', '\"I am that merry wanderer of the night.\"\n—William Shakespeare,\nA Midsummer Night\'s Dream').
card_multiverse_id('moon sprite'/'S99', '20229').

card_in_set('mountain', 'S99').
card_original_type('mountain'/'S99', 'Land').
card_original_text('mountain'/'S99', 'R').
card_image_name('mountain'/'S99', 'mountain1').
card_uid('mountain'/'S99', 'S99:Mountain:mountain1').
card_rarity('mountain'/'S99', 'Basic Land').
card_artist('mountain'/'S99', 'John Avon').
card_number('mountain'/'S99', '166').
card_multiverse_id('mountain'/'S99', '21801').

card_in_set('mountain', 'S99').
card_original_type('mountain'/'S99', 'Land').
card_original_text('mountain'/'S99', 'R').
card_image_name('mountain'/'S99', 'mountain2').
card_uid('mountain'/'S99', 'S99:Mountain:mountain2').
card_rarity('mountain'/'S99', 'Basic Land').
card_artist('mountain'/'S99', 'John Avon').
card_number('mountain'/'S99', '167').
card_multiverse_id('mountain'/'S99', '21804').

card_in_set('mountain', 'S99').
card_original_type('mountain'/'S99', 'Land').
card_original_text('mountain'/'S99', 'R').
card_image_name('mountain'/'S99', 'mountain3').
card_uid('mountain'/'S99', 'S99:Mountain:mountain3').
card_rarity('mountain'/'S99', 'Basic Land').
card_artist('mountain'/'S99', 'John Avon').
card_number('mountain'/'S99', '168').
card_multiverse_id('mountain'/'S99', '21803').

card_in_set('mountain', 'S99').
card_original_type('mountain'/'S99', 'Land').
card_original_text('mountain'/'S99', 'R').
card_image_name('mountain'/'S99', 'mountain4').
card_uid('mountain'/'S99', 'S99:Mountain:mountain4').
card_rarity('mountain'/'S99', 'Basic Land').
card_artist('mountain'/'S99', 'Brian Durfee').
card_number('mountain'/'S99', '169').
card_multiverse_id('mountain'/'S99', '21802').

card_in_set('muck rats', 'S99').
card_original_type('muck rats'/'S99', 'Creature — Rat').
card_original_text('muck rats'/'S99', '').
card_image_name('muck rats'/'S99', 'muck rats').
card_uid('muck rats'/'S99', 'S99:Muck Rats:muck rats').
card_rarity('muck rats'/'S99', 'Common').
card_artist('muck rats'/'S99', 'Una Fricker').
card_number('muck rats'/'S99', '84').
card_flavor_text('muck rats'/'S99', '\"Just as some fool said it couldn\'t get worse, the rats began to show up.\"\n—Foot soldier').
card_multiverse_id('muck rats'/'S99', '20375').

card_in_set('natural spring', 'S99').
card_original_type('natural spring'/'S99', 'Sorcery').
card_original_text('natural spring'/'S99', 'You gain 8 life.').
card_image_name('natural spring'/'S99', 'natural spring').
card_uid('natural spring'/'S99', 'S99:Natural Spring:natural spring').
card_rarity('natural spring'/'S99', 'Uncommon').
card_artist('natural spring'/'S99', 'Jeffrey R. Busch').
card_number('natural spring'/'S99', '134').
card_flavor_text('natural spring'/'S99', 'The dying ask for water because they know it contains life.').
card_multiverse_id('natural spring'/'S99', '20231').

card_in_set('nature\'s cloak', 'S99').
card_original_type('nature\'s cloak'/'S99', 'Sorcery').
card_original_text('nature\'s cloak'/'S99', 'Green creatures you control gain forestwalk until end of turn. (They\'re unblockable as long as defending player has a forest in play.)').
card_image_name('nature\'s cloak'/'S99', 'nature\'s cloak').
card_uid('nature\'s cloak'/'S99', 'S99:Nature\'s Cloak:nature\'s cloak').
card_rarity('nature\'s cloak'/'S99', 'Rare').
card_artist('nature\'s cloak'/'S99', 'Rebecca Guay').
card_number('nature\'s cloak'/'S99', '135').
card_multiverse_id('nature\'s cloak'/'S99', '20222').

card_in_set('nature\'s lore', 'S99').
card_original_type('nature\'s lore'/'S99', 'Sorcery').
card_original_text('nature\'s lore'/'S99', 'Search your library for a forest card and put that card into play. Then shuffle your library.').
card_image_name('nature\'s lore'/'S99', 'nature\'s lore').
card_uid('nature\'s lore'/'S99', 'S99:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'S99', 'Common').
card_artist('nature\'s lore'/'S99', 'Alan Rabinowitz').
card_number('nature\'s lore'/'S99', '136').
card_flavor_text('nature\'s lore'/'S99', 'Every seed planted is another lesson the forest can teach us.').
card_multiverse_id('nature\'s lore'/'S99', '20356').

card_in_set('norwood archers', 'S99').
card_original_type('norwood archers'/'S99', 'Creature — Elf').
card_original_text('norwood archers'/'S99', 'Norwood Archers can block as though it had flying.').
card_image_name('norwood archers'/'S99', 'norwood archers').
card_uid('norwood archers'/'S99', 'S99:Norwood Archers:norwood archers').
card_rarity('norwood archers'/'S99', 'Common').
card_artist('norwood archers'/'S99', 'Rebecca Guay').
card_number('norwood archers'/'S99', '137').
card_flavor_text('norwood archers'/'S99', '\"‘Air superiority?\' Not while our archers scan the skies.\"\n—Elvish scout').
card_multiverse_id('norwood archers'/'S99', '21054').

card_in_set('norwood ranger', 'S99').
card_original_type('norwood ranger'/'S99', 'Creature — Elf').
card_original_text('norwood ranger'/'S99', '').
card_image_name('norwood ranger'/'S99', 'norwood ranger').
card_uid('norwood ranger'/'S99', 'S99:Norwood Ranger:norwood ranger').
card_rarity('norwood ranger'/'S99', 'Common').
card_artist('norwood ranger'/'S99', 'Ron Spencer').
card_number('norwood ranger'/'S99', '138').
card_flavor_text('norwood ranger'/'S99', 'Some trees bear deadly fruit.').
card_multiverse_id('norwood ranger'/'S99', '21052').

card_in_set('ogre warrior', 'S99').
card_original_type('ogre warrior'/'S99', 'Creature — Ogre').
card_original_text('ogre warrior'/'S99', '').
card_image_name('ogre warrior'/'S99', 'ogre warrior').
card_uid('ogre warrior'/'S99', 'S99:Ogre Warrior:ogre warrior').
card_rarity('ogre warrior'/'S99', 'Common').
card_artist('ogre warrior'/'S99', 'Jeff Miracola').
card_number('ogre warrior'/'S99', '113').
card_flavor_text('ogre warrior'/'S99', 'Assault and battery included.').
card_multiverse_id('ogre warrior'/'S99', '21070').

card_in_set('owl familiar', 'S99').
card_original_type('owl familiar'/'S99', 'Creature — Bird').
card_original_text('owl familiar'/'S99', 'Flying\nWhen Owl Familiar comes into play, draw a card, then choose and discard a card from your hand.').
card_image_name('owl familiar'/'S99', 'owl familiar').
card_uid('owl familiar'/'S99', 'S99:Owl Familiar:owl familiar').
card_rarity('owl familiar'/'S99', 'Uncommon').
card_artist('owl familiar'/'S99', 'Janine Johnston').
card_number('owl familiar'/'S99', '43').
card_multiverse_id('owl familiar'/'S99', '21042').

card_in_set('path of peace', 'S99').
card_original_type('path of peace'/'S99', 'Sorcery').
card_original_text('path of peace'/'S99', 'Destroy target creature. Its owner gains 4 life.').
card_image_name('path of peace'/'S99', 'path of peace').
card_uid('path of peace'/'S99', 'S99:Path of Peace:path of peace').
card_rarity('path of peace'/'S99', 'Common').
card_artist('path of peace'/'S99', 'Val Mayerik').
card_number('path of peace'/'S99', '21').
card_flavor_text('path of peace'/'S99', '\"You could die famous or you could die old. I chose old.\"\n—Former soldier').
card_multiverse_id('path of peace'/'S99', '21060').

card_in_set('phantom warrior', 'S99').
card_original_type('phantom warrior'/'S99', 'Creature — Illusion').
card_original_text('phantom warrior'/'S99', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'S99', 'phantom warrior').
card_uid('phantom warrior'/'S99', 'S99:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'S99', 'Rare').
card_artist('phantom warrior'/'S99', 'John Matson').
card_number('phantom warrior'/'S99', '44').
card_flavor_text('phantom warrior'/'S99', 'The phantom warrior comes from nowhere and returns there just as quickly.').
card_multiverse_id('phantom warrior'/'S99', '21039').

card_in_set('piracy', 'S99').
card_original_type('piracy'/'S99', 'Sorcery').
card_original_text('piracy'/'S99', 'This turn, you may tap lands you don\'t control to help pay for your spells.').
card_image_name('piracy'/'S99', 'piracy').
card_uid('piracy'/'S99', 'S99:Piracy:piracy').
card_rarity('piracy'/'S99', 'Rare').
card_artist('piracy'/'S99', 'Bradley Williams').
card_number('piracy'/'S99', '45').
card_flavor_text('piracy'/'S99', 'The sea gives, and the sea takes. Some just help with the taking.').
card_multiverse_id('piracy'/'S99', '21049').

card_in_set('plains', 'S99').
card_original_type('plains'/'S99', 'Land').
card_original_text('plains'/'S99', 'W').
card_image_name('plains'/'S99', 'plains1').
card_uid('plains'/'S99', 'S99:Plains:plains1').
card_rarity('plains'/'S99', 'Basic Land').
card_artist('plains'/'S99', 'Tom Wänerstrand').
card_number('plains'/'S99', '154').
card_multiverse_id('plains'/'S99', '21789').

card_in_set('plains', 'S99').
card_original_type('plains'/'S99', 'Land').
card_original_text('plains'/'S99', 'W').
card_image_name('plains'/'S99', 'plains2').
card_uid('plains'/'S99', 'S99:Plains:plains2').
card_rarity('plains'/'S99', 'Basic Land').
card_artist('plains'/'S99', 'Tom Wänerstrand').
card_number('plains'/'S99', '155').
card_multiverse_id('plains'/'S99', '21792').

card_in_set('plains', 'S99').
card_original_type('plains'/'S99', 'Land').
card_original_text('plains'/'S99', 'W').
card_image_name('plains'/'S99', 'plains3').
card_uid('plains'/'S99', 'S99:Plains:plains3').
card_rarity('plains'/'S99', 'Basic Land').
card_artist('plains'/'S99', 'Douglas Shuler').
card_number('plains'/'S99', '156').
card_multiverse_id('plains'/'S99', '21791').

card_in_set('plains', 'S99').
card_original_type('plains'/'S99', 'Land').
card_original_text('plains'/'S99', 'W').
card_image_name('plains'/'S99', 'plains4').
card_uid('plains'/'S99', 'S99:Plains:plains4').
card_rarity('plains'/'S99', 'Basic Land').
card_artist('plains'/'S99', 'Fred Fields').
card_number('plains'/'S99', '157').
card_multiverse_id('plains'/'S99', '21790').

card_in_set('pride of lions', 'S99').
card_original_type('pride of lions'/'S99', 'Creature — Cat').
card_original_text('pride of lions'/'S99', 'You may have Pride of Lions deal its combat damage to defending player as though it weren\'t blocked.').
card_first_print('pride of lions', 'S99').
card_image_name('pride of lions'/'S99', 'pride of lions').
card_uid('pride of lions'/'S99', 'S99:Pride of Lions:pride of lions').
card_rarity('pride of lions'/'S99', 'Uncommon').
card_artist('pride of lions'/'S99', 'Carl Critchlow').
card_number('pride of lions'/'S99', '139').
card_multiverse_id('pride of lions'/'S99', '20223').

card_in_set('psychic transfer', 'S99').
card_original_type('psychic transfer'/'S99', 'Sorcery').
card_original_text('psychic transfer'/'S99', 'If the difference between your life total and target player\'s life total is 5 or less, exchange life totals with that player.').
card_image_name('psychic transfer'/'S99', 'psychic transfer').
card_uid('psychic transfer'/'S99', 'S99:Psychic Transfer:psychic transfer').
card_rarity('psychic transfer'/'S99', 'Rare').
card_artist('psychic transfer'/'S99', 'Dom!').
card_number('psychic transfer'/'S99', '46').
card_multiverse_id('psychic transfer'/'S99', '21050').

card_in_set('raging goblin', 'S99').
card_original_type('raging goblin'/'S99', 'Creature — Goblin').
card_original_text('raging goblin'/'S99', 'Haste (This creature may attack the turn it comes into play.)').
card_image_name('raging goblin'/'S99', 'raging goblin').
card_uid('raging goblin'/'S99', 'S99:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'S99', 'Common').
card_artist('raging goblin'/'S99', 'Jeff Miracola').
card_number('raging goblin'/'S99', '114').
card_flavor_text('raging goblin'/'S99', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'S99', '20210').

card_in_set('raise dead', 'S99').
card_original_type('raise dead'/'S99', 'Sorcery').
card_original_text('raise dead'/'S99', 'Return target creature card from your graveyard to your hand.').
card_image_name('raise dead'/'S99', 'raise dead').
card_uid('raise dead'/'S99', 'S99:Raise Dead:raise dead').
card_rarity('raise dead'/'S99', 'Common').
card_artist('raise dead'/'S99', 'Charles Gillespie').
card_number('raise dead'/'S99', '85').
card_flavor_text('raise dead'/'S99', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'S99', '20377').

card_in_set('ransack', 'S99').
card_original_type('ransack'/'S99', 'Sorcery').
card_original_text('ransack'/'S99', 'Look at the top five cards of target player\'s library. Put any number of them on the bottom of that library in any order and the rest on top of the library in any order.').
card_image_name('ransack'/'S99', 'ransack').
card_uid('ransack'/'S99', 'S99:Ransack:ransack').
card_rarity('ransack'/'S99', 'Rare').
card_artist('ransack'/'S99', 'Ron Spencer').
card_number('ransack'/'S99', '47').
card_multiverse_id('ransack'/'S99', '21069').

card_in_set('ravenous rats', 'S99').
card_original_type('ravenous rats'/'S99', 'Creature — Rat').
card_original_text('ravenous rats'/'S99', 'When Ravenous Rats comes into play, target player chooses and discards a card from his or her hand.').
card_image_name('ravenous rats'/'S99', 'ravenous rats').
card_uid('ravenous rats'/'S99', 'S99:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'S99', 'Uncommon').
card_artist('ravenous rats'/'S99', 'Carl Critchlow').
card_number('ravenous rats'/'S99', '86').
card_multiverse_id('ravenous rats'/'S99', '20370').

card_in_set('relearn', 'S99').
card_original_type('relearn'/'S99', 'Sorcery').
card_original_text('relearn'/'S99', 'Return target instant or sorcery card from your graveyard to your hand.').
card_image_name('relearn'/'S99', 'relearn').
card_uid('relearn'/'S99', 'S99:Relearn:relearn').
card_rarity('relearn'/'S99', 'Uncommon').
card_artist('relearn'/'S99', 'Zina Saunders').
card_number('relearn'/'S99', '48').
card_flavor_text('relearn'/'S99', 'The hardest lessons to grasp are the ones you\'ve already learned.').
card_multiverse_id('relearn'/'S99', '21046').

card_in_set('relentless assault', 'S99').
card_original_type('relentless assault'/'S99', 'Sorcery').
card_original_text('relentless assault'/'S99', 'Untap all creatures that attacked this turn. You get another combat phase, followed by another main phase, this turn.').
card_image_name('relentless assault'/'S99', 'relentless assault').
card_uid('relentless assault'/'S99', 'S99:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'S99', 'Rare').
card_artist('relentless assault'/'S99', 'G. Darrow & I. Rabarot').
card_number('relentless assault'/'S99', '115').
card_multiverse_id('relentless assault'/'S99', '20193').

card_in_set('remove soul', 'S99').
card_original_type('remove soul'/'S99', 'Instant').
card_original_text('remove soul'/'S99', 'Counter target creature spell.').
card_image_name('remove soul'/'S99', 'remove soul').
card_uid('remove soul'/'S99', 'S99:Remove Soul:remove soul').
card_rarity('remove soul'/'S99', 'Common').
card_artist('remove soul'/'S99', 'Mike Dringenberg').
card_number('remove soul'/'S99', '49').
card_flavor_text('remove soul'/'S99', 'When your enemies are denied soldiers, they are denied victory.').
card_multiverse_id('remove soul'/'S99', '21037').

card_in_set('renewing touch', 'S99').
card_original_type('renewing touch'/'S99', 'Sorcery').
card_original_text('renewing touch'/'S99', 'Shuffle any number of creature cards from your graveyard into your library.').
card_image_name('renewing touch'/'S99', 'renewing touch').
card_uid('renewing touch'/'S99', 'S99:Renewing Touch:renewing touch').
card_rarity('renewing touch'/'S99', 'Uncommon').
card_artist('renewing touch'/'S99', 'Rebecca Guay').
card_number('renewing touch'/'S99', '140').
card_flavor_text('renewing touch'/'S99', 'Death just encourages life the more.').
card_multiverse_id('renewing touch'/'S99', '20357').

card_in_set('righteous charge', 'S99').
card_original_type('righteous charge'/'S99', 'Sorcery').
card_original_text('righteous charge'/'S99', 'Creatures you control get +2/+2 until end of turn.').
card_image_name('righteous charge'/'S99', 'righteous charge').
card_uid('righteous charge'/'S99', 'S99:Righteous Charge:righteous charge').
card_rarity('righteous charge'/'S99', 'Uncommon').
card_artist('righteous charge'/'S99', 'Jeffrey R. Busch').
card_number('righteous charge'/'S99', '22').
card_flavor_text('righteous charge'/'S99', 'Bravery shines brightest in a pure soul.').
card_multiverse_id('righteous charge'/'S99', '21023').

card_in_set('righteous fury', 'S99').
card_original_type('righteous fury'/'S99', 'Sorcery').
card_original_text('righteous fury'/'S99', 'Destroy all tapped creatures. For each creature destroyed this way, you gain 2 life.').
card_image_name('righteous fury'/'S99', 'righteous fury').
card_uid('righteous fury'/'S99', 'S99:Righteous Fury:righteous fury').
card_rarity('righteous fury'/'S99', 'Rare').
card_artist('righteous fury'/'S99', 'Edward P. Beard, Jr.').
card_number('righteous fury'/'S99', '23').
card_multiverse_id('righteous fury'/'S99', '21064').

card_in_set('royal falcon', 'S99').
card_original_type('royal falcon'/'S99', 'Creature — Bird').
card_original_text('royal falcon'/'S99', 'Flying').
card_first_print('royal falcon', 'S99').
card_image_name('royal falcon'/'S99', 'royal falcon').
card_uid('royal falcon'/'S99', 'S99:Royal Falcon:royal falcon').
card_rarity('royal falcon'/'S99', 'Common').
card_artist('royal falcon'/'S99', 'Carl Critchlow').
card_number('royal falcon'/'S99', '24').
card_flavor_text('royal falcon'/'S99', 'Hunter by instinct, weapon by training.').
card_multiverse_id('royal falcon'/'S99', '21056').

card_in_set('royal trooper', 'S99').
card_original_type('royal trooper'/'S99', 'Creature — Soldier').
card_original_text('royal trooper'/'S99', 'When Royal Trooper blocks, it gets +2/+2 until end of turn.').
card_first_print('royal trooper', 'S99').
card_image_name('royal trooper'/'S99', 'royal trooper').
card_uid('royal trooper'/'S99', 'S99:Royal Trooper:royal trooper').
card_rarity('royal trooper'/'S99', 'Uncommon').
card_artist('royal trooper'/'S99', 'Scott M. Fischer').
card_number('royal trooper'/'S99', '25').
card_flavor_text('royal trooper'/'S99', '\"Fortune does not side with the faint-hearted.\"\n—Sophocles, Phaedra').
card_multiverse_id('royal trooper'/'S99', '20394').

card_in_set('sacred nectar', 'S99').
card_original_type('sacred nectar'/'S99', 'Sorcery').
card_original_text('sacred nectar'/'S99', 'You gain 4 life.').
card_image_name('sacred nectar'/'S99', 'sacred nectar').
card_uid('sacred nectar'/'S99', 'S99:Sacred Nectar:sacred nectar').
card_rarity('sacred nectar'/'S99', 'Common').
card_artist('sacred nectar'/'S99', 'Janine Johnston').
card_number('sacred nectar'/'S99', '26').
card_flavor_text('sacred nectar'/'S99', '\"For he on honey-dew hath fed,\nAnd drunk the milk of Paradise.\"\n—Samuel Taylor Coleridge, \"Kubla Khan\"').
card_multiverse_id('sacred nectar'/'S99', '21061').

card_in_set('scathe zombies', 'S99').
card_original_type('scathe zombies'/'S99', 'Creature — Zombie').
card_original_text('scathe zombies'/'S99', '').
card_image_name('scathe zombies'/'S99', 'scathe zombies').
card_uid('scathe zombies'/'S99', 'S99:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'S99', 'Common').
card_artist('scathe zombies'/'S99', 'Tom Kyffin').
card_number('scathe zombies'/'S99', '87').
card_flavor_text('scathe zombies'/'S99', 'Luckily for them, it doesn\'t take much brains to slaughter and maim.').
card_multiverse_id('scathe zombies'/'S99', '21093').

card_in_set('scorching spear', 'S99').
card_original_type('scorching spear'/'S99', 'Sorcery').
card_original_text('scorching spear'/'S99', 'Scorching Spear deals 1 damage to target creature or player.').
card_image_name('scorching spear'/'S99', 'scorching spear').
card_uid('scorching spear'/'S99', 'S99:Scorching Spear:scorching spear').
card_rarity('scorching spear'/'S99', 'Common').
card_artist('scorching spear'/'S99', 'Mike Raabe').
card_number('scorching spear'/'S99', '116').
card_flavor_text('scorching spear'/'S99', 'Lift your spear as you might lift your glass, and toast your enemy.').
card_multiverse_id('scorching spear'/'S99', '20209').

card_in_set('sea eagle', 'S99').
card_original_type('sea eagle'/'S99', 'Creature — Bird').
card_original_text('sea eagle'/'S99', 'Flying').
card_first_print('sea eagle', 'S99').
card_image_name('sea eagle'/'S99', 'sea eagle').
card_uid('sea eagle'/'S99', 'S99:Sea Eagle:sea eagle').
card_rarity('sea eagle'/'S99', 'Common').
card_artist('sea eagle'/'S99', 'Anthony S. Waters').
card_number('sea eagle'/'S99', '50').
card_flavor_text('sea eagle'/'S99', 'Where air meets water, fish meets talon.').
card_multiverse_id('sea eagle'/'S99', '21031').

card_in_set('serpent warrior', 'S99').
card_original_type('serpent warrior'/'S99', 'Creature — Soldier').
card_original_text('serpent warrior'/'S99', 'When Serpent Warrior comes into play, you lose 3 life.').
card_image_name('serpent warrior'/'S99', 'serpent warrior').
card_uid('serpent warrior'/'S99', 'S99:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'S99', 'Common').
card_artist('serpent warrior'/'S99', 'Ron Spencer').
card_number('serpent warrior'/'S99', '88').
card_flavor_text('serpent warrior'/'S99', 'A hiss before dying.').
card_multiverse_id('serpent warrior'/'S99', '20371').

card_in_set('shrieking specter', 'S99').
card_original_type('shrieking specter'/'S99', 'Creature — Specter').
card_original_text('shrieking specter'/'S99', 'Flying\nWhen Shrieking Specter attacks, defending player chooses and discards a card from his or her hand.').
card_first_print('shrieking specter', 'S99').
card_image_name('shrieking specter'/'S99', 'shrieking specter').
card_uid('shrieking specter'/'S99', 'S99:Shrieking Specter:shrieking specter').
card_rarity('shrieking specter'/'S99', 'Uncommon').
card_artist('shrieking specter'/'S99', 'rk post').
card_number('shrieking specter'/'S99', '89').
card_multiverse_id('shrieking specter'/'S99', '20363').

card_in_set('silverback ape', 'S99').
card_original_type('silverback ape'/'S99', 'Creature — Ape').
card_original_text('silverback ape'/'S99', '').
card_first_print('silverback ape', 'S99').
card_image_name('silverback ape'/'S99', 'silverback ape').
card_uid('silverback ape'/'S99', 'S99:Silverback Ape:silverback ape').
card_rarity('silverback ape'/'S99', 'Uncommon').
card_artist('silverback ape'/'S99', 'Ron Spears').
card_number('silverback ape'/'S99', '141').
card_flavor_text('silverback ape'/'S99', 'His true majesty is not in his silver, but in his size.').
card_multiverse_id('silverback ape'/'S99', '21072').

card_in_set('sleight of hand', 'S99').
card_original_type('sleight of hand'/'S99', 'Sorcery').
card_original_text('sleight of hand'/'S99', 'Look at the top two cards of your library. Put one of them into your hand and the other one on the bottom of your library.').
card_image_name('sleight of hand'/'S99', 'sleight of hand').
card_uid('sleight of hand'/'S99', 'S99:Sleight of Hand:sleight of hand').
card_rarity('sleight of hand'/'S99', 'Common').
card_artist('sleight of hand'/'S99', 'Phil Foglio').
card_number('sleight of hand'/'S99', '51').
card_multiverse_id('sleight of hand'/'S99', '21068').

card_in_set('snapping drake', 'S99').
card_original_type('snapping drake'/'S99', 'Creature — Drake').
card_original_text('snapping drake'/'S99', 'Flying').
card_image_name('snapping drake'/'S99', 'snapping drake').
card_uid('snapping drake'/'S99', 'S99:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'S99', 'Common').
card_artist('snapping drake'/'S99', 'Christopher Rush').
card_number('snapping drake'/'S99', '52').
card_flavor_text('snapping drake'/'S99', 'Drakes claim to be dragons—until the dragons show up.').
card_multiverse_id('snapping drake'/'S99', '21030').

card_in_set('soul feast', 'S99').
card_original_type('soul feast'/'S99', 'Sorcery').
card_original_text('soul feast'/'S99', 'Target player loses 4 life. You gain 4 life.').
card_image_name('soul feast'/'S99', 'soul feast').
card_uid('soul feast'/'S99', 'S99:Soul Feast:soul feast').
card_rarity('soul feast'/'S99', 'Uncommon').
card_artist('soul feast'/'S99', 'Ray Lago').
card_number('soul feast'/'S99', '90').
card_flavor_text('soul feast'/'S99', 'Evil often dines alone.').
card_multiverse_id('soul feast'/'S99', '20365').

card_in_set('southern elephant', 'S99').
card_original_type('southern elephant'/'S99', 'Creature — Elephant').
card_original_text('southern elephant'/'S99', '').
card_image_name('southern elephant'/'S99', 'southern elephant').
card_uid('southern elephant'/'S99', 'S99:Southern Elephant:southern elephant').
card_rarity('southern elephant'/'S99', 'Common').
card_artist('southern elephant'/'S99', 'Wang Yuqun').
card_number('southern elephant'/'S99', '142').
card_flavor_text('southern elephant'/'S99', 'Neither predator nor prey, the elephant chooses its place in the battle.').
card_multiverse_id('southern elephant'/'S99', '21071').

card_in_set('spitting earth', 'S99').
card_original_type('spitting earth'/'S99', 'Sorcery').
card_original_text('spitting earth'/'S99', 'Spitting Earth deals to target creature damage equal to the number of mountains you control. (Count only the mountains you have in play, including both tapped and untapped mountains.)').
card_image_name('spitting earth'/'S99', 'spitting earth').
card_uid('spitting earth'/'S99', 'S99:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'S99', 'Uncommon').
card_artist('spitting earth'/'S99', 'Brian Snõddy').
card_number('spitting earth'/'S99', '117').
card_multiverse_id('spitting earth'/'S99', '20200').

card_in_set('squall', 'S99').
card_original_type('squall'/'S99', 'Sorcery').
card_original_text('squall'/'S99', 'Squall deals 2 damage to each creature with flying. (This includes your creatures.)').
card_first_print('squall', 'S99').
card_image_name('squall'/'S99', 'squall').
card_uid('squall'/'S99', 'S99:Squall:squall').
card_rarity('squall'/'S99', 'Common').
card_artist('squall'/'S99', 'Carl Critchlow').
card_number('squall'/'S99', '143').
card_flavor_text('squall'/'S99', '\"To-night the winds begin to rise . . .\nThe rooks are blown about the skies . . . .\"\n—Alfred, Lord Tennyson, In Memoriam').
card_multiverse_id('squall'/'S99', '20220').

card_in_set('steadfastness', 'S99').
card_original_type('steadfastness'/'S99', 'Sorcery').
card_original_text('steadfastness'/'S99', 'Creatures you control get +0/+3 until end of turn.').
card_image_name('steadfastness'/'S99', 'steadfastness').
card_uid('steadfastness'/'S99', 'S99:Steadfastness:steadfastness').
card_rarity('steadfastness'/'S99', 'Common').
card_artist('steadfastness'/'S99', 'Kev Walker').
card_number('steadfastness'/'S99', '27').
card_flavor_text('steadfastness'/'S99', 'Brute force wins the battles. Conviction wins the wars.').
card_multiverse_id('steadfastness'/'S99', '20399').

card_in_set('stone rain', 'S99').
card_original_type('stone rain'/'S99', 'Sorcery').
card_original_text('stone rain'/'S99', 'Destroy target land.').
card_image_name('stone rain'/'S99', 'stone rain').
card_uid('stone rain'/'S99', 'S99:Stone Rain:stone rain').
card_rarity('stone rain'/'S99', 'Common').
card_artist('stone rain'/'S99', 'John Matson').
card_number('stone rain'/'S99', '118').
card_multiverse_id('stone rain'/'S99', '20208').

card_in_set('storm crow', 'S99').
card_original_type('storm crow'/'S99', 'Creature — Bird').
card_original_text('storm crow'/'S99', 'Flying').
card_image_name('storm crow'/'S99', 'storm crow').
card_uid('storm crow'/'S99', 'S99:Storm Crow:storm crow').
card_rarity('storm crow'/'S99', 'Common').
card_artist('storm crow'/'S99', 'Una Fricker').
card_number('storm crow'/'S99', '53').
card_flavor_text('storm crow'/'S99', 'Storm crow descending, winter unending.\nStorm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'S99', '21028').

card_in_set('stream of acid', 'S99').
card_original_type('stream of acid'/'S99', 'Sorcery').
card_original_text('stream of acid'/'S99', 'Destroy target land or nonblack creature.').
card_first_print('stream of acid', 'S99').
card_image_name('stream of acid'/'S99', 'stream of acid').
card_uid('stream of acid'/'S99', 'S99:Stream of Acid:stream of acid').
card_rarity('stream of acid'/'S99', 'Uncommon').
card_artist('stream of acid'/'S99', 'DiTerlizzi').
card_number('stream of acid'/'S99', '91').
card_multiverse_id('stream of acid'/'S99', '20364').

card_in_set('summer bloom', 'S99').
card_original_type('summer bloom'/'S99', 'Sorcery').
card_original_text('summer bloom'/'S99', 'Put up to three land cards from your hand into play.').
card_image_name('summer bloom'/'S99', 'summer bloom').
card_uid('summer bloom'/'S99', 'S99:Summer Bloom:summer bloom').
card_rarity('summer bloom'/'S99', 'Rare').
card_artist('summer bloom'/'S99', 'Kaja Foglio').
card_number('summer bloom'/'S99', '144').
card_flavor_text('summer bloom'/'S99', 'Summer sends its kiss with warmth and blooming life.').
card_multiverse_id('summer bloom'/'S99', '21078').

card_in_set('swamp', 'S99').
card_original_type('swamp'/'S99', 'Land').
card_original_text('swamp'/'S99', 'B').
card_image_name('swamp'/'S99', 'swamp1').
card_uid('swamp'/'S99', 'S99:Swamp:swamp1').
card_rarity('swamp'/'S99', 'Basic Land').
card_artist('swamp'/'S99', 'Romas Kukalis').
card_number('swamp'/'S99', '162').
card_multiverse_id('swamp'/'S99', '21797').

card_in_set('swamp', 'S99').
card_original_type('swamp'/'S99', 'Land').
card_original_text('swamp'/'S99', 'B').
card_image_name('swamp'/'S99', 'swamp2').
card_uid('swamp'/'S99', 'S99:Swamp:swamp2').
card_rarity('swamp'/'S99', 'Basic Land').
card_artist('swamp'/'S99', 'Dan Frazier').
card_number('swamp'/'S99', '163').
card_multiverse_id('swamp'/'S99', '21799').

card_in_set('swamp', 'S99').
card_original_type('swamp'/'S99', 'Land').
card_original_text('swamp'/'S99', 'B').
card_image_name('swamp'/'S99', 'swamp3').
card_uid('swamp'/'S99', 'S99:Swamp:swamp3').
card_rarity('swamp'/'S99', 'Basic Land').
card_artist('swamp'/'S99', 'Douglas Shuler').
card_number('swamp'/'S99', '164').
card_multiverse_id('swamp'/'S99', '21796').

card_in_set('swamp', 'S99').
card_original_type('swamp'/'S99', 'Land').
card_original_text('swamp'/'S99', 'B').
card_image_name('swamp'/'S99', 'swamp4').
card_uid('swamp'/'S99', 'S99:Swamp:swamp4').
card_rarity('swamp'/'S99', 'Basic Land').
card_artist('swamp'/'S99', 'Romas Kukalis').
card_number('swamp'/'S99', '165').
card_multiverse_id('swamp'/'S99', '21798').

card_in_set('sylvan basilisk', 'S99').
card_original_type('sylvan basilisk'/'S99', 'Creature — Basilisk').
card_original_text('sylvan basilisk'/'S99', 'When Sylvan Basilisk becomes blocked, destroy all creatures blocking it. (Destroy the creatures before they deal damage. Sylvan Basilisk still doesn\'t deal damage to defending player.)').
card_image_name('sylvan basilisk'/'S99', 'sylvan basilisk').
card_uid('sylvan basilisk'/'S99', 'S99:Sylvan Basilisk:sylvan basilisk').
card_rarity('sylvan basilisk'/'S99', 'Rare').
card_artist('sylvan basilisk'/'S99', 'Ron Spencer').
card_number('sylvan basilisk'/'S99', '145').
card_multiverse_id('sylvan basilisk'/'S99', '20218').

card_in_set('sylvan yeti', 'S99').
card_original_type('sylvan yeti'/'S99', 'Creature — Elemental').
card_original_text('sylvan yeti'/'S99', 'Sylvan Yeti\'s power is equal to the number of cards in your hand.').
card_image_name('sylvan yeti'/'S99', 'sylvan yeti').
card_uid('sylvan yeti'/'S99', 'S99:Sylvan Yeti:sylvan yeti').
card_rarity('sylvan yeti'/'S99', 'Rare').
card_artist('sylvan yeti'/'S99', 'Brom').
card_number('sylvan yeti'/'S99', '146').
card_flavor_text('sylvan yeti'/'S99', 'The deeper the wood, the greater its strength.').
card_multiverse_id('sylvan yeti'/'S99', '20217').

card_in_set('thorn elemental', 'S99').
card_original_type('thorn elemental'/'S99', 'Creature — Elemental').
card_original_text('thorn elemental'/'S99', 'You may have Thorn Elemental deal its combat damage to defending player as though it weren\'t blocked.').
card_image_name('thorn elemental'/'S99', 'thorn elemental').
card_uid('thorn elemental'/'S99', 'S99:Thorn Elemental:thorn elemental').
card_rarity('thorn elemental'/'S99', 'Rare').
card_artist('thorn elemental'/'S99', 'rk post').
card_number('thorn elemental'/'S99', '147').
card_multiverse_id('thorn elemental'/'S99', '20181').

card_in_set('thunder dragon', 'S99').
card_original_type('thunder dragon'/'S99', 'Creature — Dragon').
card_original_text('thunder dragon'/'S99', 'Flying\nWhen Thunder Dragon comes into play, it deals 3 damage to each creature without flying. (This includes your creatures.)').
card_first_print('thunder dragon', 'S99').
card_image_name('thunder dragon'/'S99', 'thunder dragon').
card_uid('thunder dragon'/'S99', 'S99:Thunder Dragon:thunder dragon').
card_rarity('thunder dragon'/'S99', 'Rare').
card_artist('thunder dragon'/'S99', 'Dana Knutson').
card_number('thunder dragon'/'S99', '119').
card_multiverse_id('thunder dragon'/'S99', '20180').

card_in_set('tidings', 'S99').
card_original_type('tidings'/'S99', 'Sorcery').
card_original_text('tidings'/'S99', 'Draw four cards.').
card_first_print('tidings', 'S99').
card_image_name('tidings'/'S99', 'tidings').
card_uid('tidings'/'S99', 'S99:Tidings:tidings').
card_rarity('tidings'/'S99', 'Uncommon').
card_artist('tidings'/'S99', 'Pete Venters').
card_number('tidings'/'S99', '54').
card_flavor_text('tidings'/'S99', '\"Many wearing rapiers are afraid of goose-quills.\"\n—William Shakespeare, Hamlet').
card_multiverse_id('tidings'/'S99', '21047').

card_in_set('time ebb', 'S99').
card_original_type('time ebb'/'S99', 'Sorcery').
card_original_text('time ebb'/'S99', 'Put target creature on top of its owner\'s library.').
card_image_name('time ebb'/'S99', 'time ebb').
card_uid('time ebb'/'S99', 'S99:Time Ebb:time ebb').
card_rarity('time ebb'/'S99', 'Common').
card_artist('time ebb'/'S99', 'Alan Rabinowitz').
card_number('time ebb'/'S99', '55').
card_flavor_text('time ebb'/'S99', 'Like the tide, time both ebbs and flows.').
card_multiverse_id('time ebb'/'S99', '21034').

card_in_set('time warp', 'S99').
card_original_type('time warp'/'S99', 'Sorcery').
card_original_text('time warp'/'S99', 'Target player takes another turn after this one.').
card_image_name('time warp'/'S99', 'time warp').
card_uid('time warp'/'S99', 'S99:Time Warp:time warp').
card_rarity('time warp'/'S99', 'Rare').
card_artist('time warp'/'S99', 'Pete Venters').
card_number('time warp'/'S99', '56').
card_flavor_text('time warp'/'S99', '\"Let\'s do it again!\"\n—Squee, goblin cabin hand').
card_multiverse_id('time warp'/'S99', '21048').

card_in_set('touch of brilliance', 'S99').
card_original_type('touch of brilliance'/'S99', 'Sorcery').
card_original_text('touch of brilliance'/'S99', 'Draw two cards.').
card_image_name('touch of brilliance'/'S99', 'touch of brilliance').
card_uid('touch of brilliance'/'S99', 'S99:Touch of Brilliance:touch of brilliance').
card_rarity('touch of brilliance'/'S99', 'Common').
card_artist('touch of brilliance'/'S99', 'Kaja Foglio').
card_number('touch of brilliance'/'S99', '57').
card_flavor_text('touch of brilliance'/'S99', '\"I don\'t want what I don\'t have. I want more of what I do have.\"').
card_multiverse_id('touch of brilliance'/'S99', '21033').

card_in_set('trained orgg', 'S99').
card_original_type('trained orgg'/'S99', 'Creature — Beast').
card_original_text('trained orgg'/'S99', '').
card_first_print('trained orgg', 'S99').
card_image_name('trained orgg'/'S99', 'trained orgg').
card_uid('trained orgg'/'S99', 'S99:Trained Orgg:trained orgg').
card_rarity('trained orgg'/'S99', 'Rare').
card_artist('trained orgg'/'S99', 'Eric Peterson').
card_number('trained orgg'/'S99', '120').
card_flavor_text('trained orgg'/'S99', 'All orggs know how to kill; training teaches them what to kill.').
card_multiverse_id('trained orgg'/'S99', '21785').

card_in_set('tremor', 'S99').
card_original_type('tremor'/'S99', 'Sorcery').
card_original_text('tremor'/'S99', 'Tremor deals 1 damage to each creature without flying. (This includes your creatures.)').
card_image_name('tremor'/'S99', 'tremor').
card_uid('tremor'/'S99', 'S99:Tremor:tremor').
card_rarity('tremor'/'S99', 'Common').
card_artist('tremor'/'S99', 'Pete Venters').
card_number('tremor'/'S99', '121').
card_flavor_text('tremor'/'S99', 'One little goblin shook up the ground. When the dust cleared, no one was found.').
card_multiverse_id('tremor'/'S99', '20207').

card_in_set('undo', 'S99').
card_original_type('undo'/'S99', 'Sorcery').
card_original_text('undo'/'S99', 'Return two target creatures to their owner\'s hand. (You can\'t play this card unless you can choose two creatures in play.)').
card_image_name('undo'/'S99', 'undo').
card_uid('undo'/'S99', 'S99:Undo:undo').
card_rarity('undo'/'S99', 'Uncommon').
card_artist('undo'/'S99', 'Terese Nielsen').
card_number('undo'/'S99', '58').
card_multiverse_id('undo'/'S99', '21045').

card_in_set('untamed wilds', 'S99').
card_original_type('untamed wilds'/'S99', 'Sorcery').
card_original_text('untamed wilds'/'S99', 'Search your library for a plains, island, swamp, mountain, or forest card and put that card into play. Then shuffle your library.').
card_image_name('untamed wilds'/'S99', 'untamed wilds').
card_uid('untamed wilds'/'S99', 'S99:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'S99', 'Uncommon').
card_artist('untamed wilds'/'S99', 'NéNé Thomas').
card_number('untamed wilds'/'S99', '148').
card_multiverse_id('untamed wilds'/'S99', '20228').

card_in_set('venerable monk', 'S99').
card_original_type('venerable monk'/'S99', 'Creature — Cleric').
card_original_text('venerable monk'/'S99', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'S99', 'venerable monk').
card_uid('venerable monk'/'S99', 'S99:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'S99', 'Common').
card_artist('venerable monk'/'S99', 'D. Alexander Gregory').
card_number('venerable monk'/'S99', '28').
card_flavor_text('venerable monk'/'S99', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'S99', '21059').

card_in_set('vengeance', 'S99').
card_original_type('vengeance'/'S99', 'Sorcery').
card_original_text('vengeance'/'S99', 'Destroy target tapped creature.').
card_image_name('vengeance'/'S99', 'vengeance').
card_uid('vengeance'/'S99', 'S99:Vengeance:vengeance').
card_rarity('vengeance'/'S99', 'Uncommon').
card_artist('vengeance'/'S99', 'Andrew Robinson').
card_number('vengeance'/'S99', '29').
card_flavor_text('vengeance'/'S99', 'Bitter as wormwood, sweet as mulled wine.').
card_multiverse_id('vengeance'/'S99', '20393').

card_in_set('veteran cavalier', 'S99').
card_original_type('veteran cavalier'/'S99', 'Creature — Knight').
card_original_text('veteran cavalier'/'S99', 'Attacking doesn\'t cause Veteran Cavalier to tap.').
card_first_print('veteran cavalier', 'S99').
card_image_name('veteran cavalier'/'S99', 'veteran cavalier').
card_uid('veteran cavalier'/'S99', 'S99:Veteran Cavalier:veteran cavalier').
card_rarity('veteran cavalier'/'S99', 'Uncommon').
card_artist('veteran cavalier'/'S99', 'rk post').
card_number('veteran cavalier'/'S99', '30').
card_flavor_text('veteran cavalier'/'S99', 'Spirit is the sword and experience the sharpening stone.\n—Arabian proverb').
card_multiverse_id('veteran cavalier'/'S99', '20403').

card_in_set('vizzerdrix', 'S99').
card_original_type('vizzerdrix'/'S99', 'Creature — Beast').
card_original_text('vizzerdrix'/'S99', '').
card_first_print('vizzerdrix', 'S99').
card_image_name('vizzerdrix'/'S99', 'vizzerdrix').
card_uid('vizzerdrix'/'S99', 'S99:Vizzerdrix:vizzerdrix').
card_rarity('vizzerdrix'/'S99', 'Rare').
card_artist('vizzerdrix'/'S99', 'Eric Peterson').
card_number('vizzerdrix'/'S99', '59').
card_flavor_text('vizzerdrix'/'S99', 'Its swamp cousin may be meaner, but the vizzerdrix is much bigger.').
card_multiverse_id('vizzerdrix'/'S99', '20188').

card_in_set('volcanic dragon', 'S99').
card_original_type('volcanic dragon'/'S99', 'Creature — Dragon').
card_original_text('volcanic dragon'/'S99', 'Flying; haste (This creature may attack the turn it comes into play.)').
card_image_name('volcanic dragon'/'S99', 'volcanic dragon').
card_uid('volcanic dragon'/'S99', 'S99:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'S99', 'Rare').
card_artist('volcanic dragon'/'S99', 'Janine Johnston').
card_number('volcanic dragon'/'S99', '122').
card_flavor_text('volcanic dragon'/'S99', 'Speed and fire are always a deadly combination.').
card_multiverse_id('volcanic dragon'/'S99', '21020').

card_in_set('volcanic hammer', 'S99').
card_original_type('volcanic hammer'/'S99', 'Sorcery').
card_original_text('volcanic hammer'/'S99', 'Volcanic Hammer deals 3 damage to target creature or player.').
card_image_name('volcanic hammer'/'S99', 'volcanic hammer').
card_uid('volcanic hammer'/'S99', 'S99:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'S99', 'Common').
card_artist('volcanic hammer'/'S99', 'Edward P. Beard, Jr.').
card_number('volcanic hammer'/'S99', '123').
card_flavor_text('volcanic hammer'/'S99', 'Fire finds its form in the heat of the forge.').
card_multiverse_id('volcanic hammer'/'S99', '20206').

card_in_set('water elemental', 'S99').
card_original_type('water elemental'/'S99', 'Creature — Elemental').
card_original_text('water elemental'/'S99', '').
card_image_name('water elemental'/'S99', 'water elemental').
card_uid('water elemental'/'S99', 'S99:Water Elemental:water elemental').
card_rarity('water elemental'/'S99', 'Uncommon').
card_artist('water elemental'/'S99', 'Jeff A. Menges').
card_number('water elemental'/'S99', '60').
card_flavor_text('water elemental'/'S99', 'The water that a ship sails on is the same water that swallows it up.\n—Chinese proverb').
card_multiverse_id('water elemental'/'S99', '21040').

card_in_set('whiptail wurm', 'S99').
card_original_type('whiptail wurm'/'S99', 'Creature — Wurm').
card_original_text('whiptail wurm'/'S99', '').
card_image_name('whiptail wurm'/'S99', 'whiptail wurm').
card_uid('whiptail wurm'/'S99', 'S99:Whiptail Wurm:whiptail wurm').
card_rarity('whiptail wurm'/'S99', 'Uncommon').
card_artist('whiptail wurm'/'S99', 'Una Fricker').
card_number('whiptail wurm'/'S99', '149').
card_flavor_text('whiptail wurm'/'S99', 'It\'s hard to say for certain which end is more dangerous.').
card_multiverse_id('whiptail wurm'/'S99', '20230').

card_in_set('whirlwind', 'S99').
card_original_type('whirlwind'/'S99', 'Sorcery').
card_original_text('whirlwind'/'S99', 'Destroy all creatures with flying. (This includes your creatures.)').
card_image_name('whirlwind'/'S99', 'whirlwind').
card_uid('whirlwind'/'S99', 'S99:Whirlwind:whirlwind').
card_rarity('whirlwind'/'S99', 'Rare').
card_artist('whirlwind'/'S99', 'John Matson').
card_number('whirlwind'/'S99', '150').
card_multiverse_id('whirlwind'/'S99', '20219').

card_in_set('wicked pact', 'S99').
card_original_type('wicked pact'/'S99', 'Sorcery').
card_original_text('wicked pact'/'S99', 'Destroy two target nonblack creatures. You lose 5 life. (You can\'t play this card unless you can choose two creatures in play.)').
card_image_name('wicked pact'/'S99', 'wicked pact').
card_uid('wicked pact'/'S99', 'S99:Wicked Pact:wicked pact').
card_rarity('wicked pact'/'S99', 'Rare').
card_artist('wicked pact'/'S99', 'Adam Rex').
card_number('wicked pact'/'S99', '92').
card_multiverse_id('wicked pact'/'S99', '20361').

card_in_set('wild griffin', 'S99').
card_original_type('wild griffin'/'S99', 'Creature — Griffin').
card_original_text('wild griffin'/'S99', 'Flying').
card_image_name('wild griffin'/'S99', 'wild griffin').
card_uid('wild griffin'/'S99', 'S99:Wild Griffin:wild griffin').
card_rarity('wild griffin'/'S99', 'Common').
card_artist('wild griffin'/'S99', 'Jeff Miracola').
card_number('wild griffin'/'S99', '31').
card_flavor_text('wild griffin'/'S99', 'Two little goblins, out in the sun. Down came a griffin, and then there was one.').
card_multiverse_id('wild griffin'/'S99', '21058').

card_in_set('wild ox', 'S99').
card_original_type('wild ox'/'S99', 'Creature — Ox').
card_original_text('wild ox'/'S99', 'Swampwalk (This creature is unblockable s long as defending player has a swamp in play.)').
card_image_name('wild ox'/'S99', 'wild ox').
card_uid('wild ox'/'S99', 'S99:Wild Ox:wild ox').
card_rarity('wild ox'/'S99', 'Uncommon').
card_artist('wild ox'/'S99', 'Jeffrey R. Busch').
card_number('wild ox'/'S99', '151').
card_flavor_text('wild ox'/'S99', 'It has the run of the swamps, and it knows it.').
card_multiverse_id('wild ox'/'S99', '20227').

card_in_set('willow elf', 'S99').
card_original_type('willow elf'/'S99', 'Creature — Elf').
card_original_text('willow elf'/'S99', '').
card_first_print('willow elf', 'S99').
card_image_name('willow elf'/'S99', 'willow elf').
card_uid('willow elf'/'S99', 'S99:Willow Elf:willow elf').
card_rarity('willow elf'/'S99', 'Common').
card_artist('willow elf'/'S99', 'DiTerlizzi').
card_number('willow elf'/'S99', '152').
card_flavor_text('willow elf'/'S99', 'The forest lives in the elf as the elf lives in the forest.').
card_multiverse_id('willow elf'/'S99', '21051').

card_in_set('wind drake', 'S99').
card_original_type('wind drake'/'S99', 'Creature — Drake').
card_original_text('wind drake'/'S99', 'Flying').
card_image_name('wind drake'/'S99', 'wind drake').
card_uid('wind drake'/'S99', 'S99:Wind Drake:wind drake').
card_rarity('wind drake'/'S99', 'Common').
card_artist('wind drake'/'S99', 'Zina Saunders').
card_number('wind drake'/'S99', '61').
card_flavor_text('wind drake'/'S99', '\"No bird soars too high, if he soars with his own wings.\" —William Blake, The Marriage of Heaven and Hell').
card_multiverse_id('wind drake'/'S99', '21029').

card_in_set('wind sail', 'S99').
card_original_type('wind sail'/'S99', 'Sorcery').
card_original_text('wind sail'/'S99', 'One or two target creatures gain flying until end of turn.').
card_image_name('wind sail'/'S99', 'wind sail').
card_uid('wind sail'/'S99', 'S99:Wind Sail:wind sail').
card_rarity('wind sail'/'S99', 'Uncommon').
card_artist('wind sail'/'S99', 'Matt Stawicki').
card_number('wind sail'/'S99', '62').
card_flavor_text('wind sail'/'S99', 'It pays to be at home both on the sea and in the sky.').
card_multiverse_id('wind sail'/'S99', '21076').

card_in_set('wood elves', 'S99').
card_original_type('wood elves'/'S99', 'Creature — Elf').
card_original_text('wood elves'/'S99', 'When Wood Elves comes into play, search your library for a forest card and put that card into play. Then shuffle your library.').
card_image_name('wood elves'/'S99', 'wood elves').
card_uid('wood elves'/'S99', 'S99:Wood Elves:wood elves').
card_rarity('wood elves'/'S99', 'Uncommon').
card_artist('wood elves'/'S99', 'Rebecca Guay').
card_number('wood elves'/'S99', '153').
card_multiverse_id('wood elves'/'S99', '20224').
