% Mirrodin

set('MRD').
set_name('MRD', 'Mirrodin').
set_release_date('MRD', '2003-10-02').
set_border('MRD', 'black').
set_type('MRD', 'expansion').
set_block('MRD', 'Mirrodin').

card_in_set('æther spellbomb', 'MRD').
card_original_type('æther spellbomb'/'MRD', 'Artifact').
card_original_text('æther spellbomb'/'MRD', '{U}, Sacrifice Æther Spellbomb: Return target creature to its owner\'s hand.\n{1}, Sacrifice Æther Spellbomb: Draw a card.').
card_first_print('æther spellbomb', 'MRD').
card_image_name('æther spellbomb'/'MRD', 'aether spellbomb').
card_uid('æther spellbomb'/'MRD', 'MRD:Æther Spellbomb:aether spellbomb').
card_rarity('æther spellbomb'/'MRD', 'Common').
card_artist('æther spellbomb'/'MRD', 'Jim Nelson').
card_number('æther spellbomb'/'MRD', '141').
card_flavor_text('æther spellbomb'/'MRD', '\"Release that which was never caged.\"\n—Spellbomb inscription').
card_multiverse_id('æther spellbomb'/'MRD', '48113').

card_in_set('alpha myr', 'MRD').
card_original_type('alpha myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('alpha myr'/'MRD', '').
card_first_print('alpha myr', 'MRD').
card_image_name('alpha myr'/'MRD', 'alpha myr').
card_uid('alpha myr'/'MRD', 'MRD:Alpha Myr:alpha myr').
card_rarity('alpha myr'/'MRD', 'Common').
card_artist('alpha myr'/'MRD', 'Dany Orizio').
card_number('alpha myr'/'MRD', '142').
card_flavor_text('alpha myr'/'MRD', 'First to charge, first to fight.').
card_multiverse_id('alpha myr'/'MRD', '49045').

card_in_set('altar of shadows', 'MRD').
card_original_type('altar of shadows'/'MRD', 'Artifact').
card_original_text('altar of shadows'/'MRD', 'At the beginning of your precombat main phase, add {B} to your mana pool for each charge counter on Altar of Shadows.\n{7}, {T}: Destroy target creature. Then put a charge counter on Altar of Shadows.').
card_first_print('altar of shadows', 'MRD').
card_image_name('altar of shadows'/'MRD', 'altar of shadows').
card_uid('altar of shadows'/'MRD', 'MRD:Altar of Shadows:altar of shadows').
card_rarity('altar of shadows'/'MRD', 'Rare').
card_artist('altar of shadows'/'MRD', 'Sam Wood').
card_number('altar of shadows'/'MRD', '143').
card_multiverse_id('altar of shadows'/'MRD', '48156').

card_in_set('altar\'s light', 'MRD').
card_original_type('altar\'s light'/'MRD', 'Instant').
card_original_text('altar\'s light'/'MRD', 'Remove target artifact or enchantment from the game.').
card_first_print('altar\'s light', 'MRD').
card_image_name('altar\'s light'/'MRD', 'altar\'s light').
card_uid('altar\'s light'/'MRD', 'MRD:Altar\'s Light:altar\'s light').
card_rarity('altar\'s light'/'MRD', 'Uncommon').
card_artist('altar\'s light'/'MRD', 'Daren Bader').
card_number('altar\'s light'/'MRD', '1').
card_flavor_text('altar\'s light'/'MRD', '\"The altar does nothing; the device is crushed under the weight of its own impurity.\"\n—Ushanti, leonin seer').
card_multiverse_id('altar\'s light'/'MRD', '48383').

card_in_set('ancient den', 'MRD').
card_original_type('ancient den'/'MRD', 'Artifact Land').
card_original_text('ancient den'/'MRD', '(Ancient Den isn\'t a spell.)\n{T}: Add {W} to your mana pool.').
card_first_print('ancient den', 'MRD').
card_image_name('ancient den'/'MRD', 'ancient den').
card_uid('ancient den'/'MRD', 'MRD:Ancient Den:ancient den').
card_rarity('ancient den'/'MRD', 'Common').
card_artist('ancient den'/'MRD', 'Rob Alexander').
card_number('ancient den'/'MRD', '278').
card_flavor_text('ancient den'/'MRD', 'Taj-Nar, throne of Raksha Golden Cub, destined leader of the leonin prides.').
card_multiverse_id('ancient den'/'MRD', '46063').

card_in_set('annul', 'MRD').
card_original_type('annul'/'MRD', 'Instant').
card_original_text('annul'/'MRD', 'Counter target artifact or enchantment spell.').
card_image_name('annul'/'MRD', 'annul').
card_uid('annul'/'MRD', 'MRD:Annul:annul').
card_rarity('annul'/'MRD', 'Common').
card_artist('annul'/'MRD', 'Brian Snõddy').
card_number('annul'/'MRD', '29').
card_flavor_text('annul'/'MRD', '\"Murder of the living is tragic, but murder of the idea is unforgivable.\"\n—Janus, speaker of the synod').
card_multiverse_id('annul'/'MRD', '45976').

card_in_set('arc-slogger', 'MRD').
card_original_type('arc-slogger'/'MRD', 'Creature — Beast').
card_original_text('arc-slogger'/'MRD', '{R}, Remove the top ten cards of your library from the game: Arc-Slogger deals 2 damage to target creature or player.').
card_first_print('arc-slogger', 'MRD').
card_image_name('arc-slogger'/'MRD', 'arc-slogger').
card_uid('arc-slogger'/'MRD', 'MRD:Arc-Slogger:arc-slogger').
card_rarity('arc-slogger'/'MRD', 'Rare').
card_artist('arc-slogger'/'MRD', 'Jeff Easley').
card_number('arc-slogger'/'MRD', '85').
card_flavor_text('arc-slogger'/'MRD', 'A shuffling sound and the smell of ozone follow the slogger as surely as its electric tail.').
card_multiverse_id('arc-slogger'/'MRD', '48436').

card_in_set('arrest', 'MRD').
card_original_type('arrest'/'MRD', 'Enchant Creature').
card_original_text('arrest'/'MRD', 'Enchanted creature can\'t attack or block, and its activated abilities can\'t be played.').
card_image_name('arrest'/'MRD', 'arrest').
card_uid('arrest'/'MRD', 'MRD:Arrest:arrest').
card_rarity('arrest'/'MRD', 'Common').
card_artist('arrest'/'MRD', 'Tim Hildebrandt').
card_number('arrest'/'MRD', '2').
card_flavor_text('arrest'/'MRD', '\"Unfortunately, it doesn\'t restrain the beast\'s smell.\"\n—Glissa Sunseeker').
card_multiverse_id('arrest'/'MRD', '48432').

card_in_set('assert authority', 'MRD').
card_original_type('assert authority'/'MRD', 'Instant').
card_original_text('assert authority'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nCounter target spell. If it\'s countered this way, remove it from the game instead of putting it into its owner\'s graveyard.').
card_first_print('assert authority', 'MRD').
card_image_name('assert authority'/'MRD', 'assert authority').
card_uid('assert authority'/'MRD', 'MRD:Assert Authority:assert authority').
card_rarity('assert authority'/'MRD', 'Uncommon').
card_artist('assert authority'/'MRD', 'Greg Hildebrandt').
card_number('assert authority'/'MRD', '30').
card_multiverse_id('assert authority'/'MRD', '49062').

card_in_set('atog', 'MRD').
card_original_type('atog'/'MRD', 'Creature — Atog').
card_original_text('atog'/'MRD', 'Sacrifice an artifact: Atog gets +2/+2 until end of turn.').
card_image_name('atog'/'MRD', 'atog').
card_uid('atog'/'MRD', 'MRD:Atog:atog').
card_rarity('atog'/'MRD', 'Uncommon').
card_artist('atog'/'MRD', 'Puddnhead').
card_number('atog'/'MRD', '86').
card_flavor_text('atog'/'MRD', 'On Dominaria, a scavenger. On Mirrodin, a predator.').
card_multiverse_id('atog'/'MRD', '46106').

card_in_set('auriok bladewarden', 'MRD').
card_original_type('auriok bladewarden'/'MRD', 'Creature — Human Soldier').
card_original_text('auriok bladewarden'/'MRD', '{T}: Target creature gets +X/+X until end of turn, where X is Auriok Bladewarden\'s power.').
card_first_print('auriok bladewarden', 'MRD').
card_image_name('auriok bladewarden'/'MRD', 'auriok bladewarden').
card_uid('auriok bladewarden'/'MRD', 'MRD:Auriok Bladewarden:auriok bladewarden').
card_rarity('auriok bladewarden'/'MRD', 'Uncommon').
card_artist('auriok bladewarden'/'MRD', 'Dave Dorman').
card_number('auriok bladewarden'/'MRD', '3').
card_flavor_text('auriok bladewarden'/'MRD', 'The Auriok have learned through constant struggle that allies are more precious than water.').
card_multiverse_id('auriok bladewarden'/'MRD', '48433').

card_in_set('auriok steelshaper', 'MRD').
card_original_type('auriok steelshaper'/'MRD', 'Creature — Human Soldier').
card_original_text('auriok steelshaper'/'MRD', 'Equip costs you pay cost {1} less.\nAs long as Auriok Steelshaper is equipped, Soldiers and Knights you control get +1/+1.').
card_first_print('auriok steelshaper', 'MRD').
card_image_name('auriok steelshaper'/'MRD', 'auriok steelshaper').
card_uid('auriok steelshaper'/'MRD', 'MRD:Auriok Steelshaper:auriok steelshaper').
card_rarity('auriok steelshaper'/'MRD', 'Rare').
card_artist('auriok steelshaper'/'MRD', 'Dany Orizio').
card_number('auriok steelshaper'/'MRD', '4').
card_flavor_text('auriok steelshaper'/'MRD', 'They put their safety in his hands. He puts sharpened steel in theirs.').
card_multiverse_id('auriok steelshaper'/'MRD', '46081').

card_in_set('auriok transfixer', 'MRD').
card_original_type('auriok transfixer'/'MRD', 'Creature — Human Scout').
card_original_text('auriok transfixer'/'MRD', '{W}, {T}: Tap target artifact.').
card_first_print('auriok transfixer', 'MRD').
card_image_name('auriok transfixer'/'MRD', 'auriok transfixer').
card_uid('auriok transfixer'/'MRD', 'MRD:Auriok Transfixer:auriok transfixer').
card_rarity('auriok transfixer'/'MRD', 'Common').
card_artist('auriok transfixer'/'MRD', 'Stephen Tappin').
card_number('auriok transfixer'/'MRD', '5').
card_flavor_text('auriok transfixer'/'MRD', '\"My grandfather knew enough spells to fill a hundred scrolls. Nowadays, if a spell cannot fight the levelers, it is not even taught to our young.\"').
card_multiverse_id('auriok transfixer'/'MRD', '45963').

card_in_set('awe strike', 'MRD').
card_original_type('awe strike'/'MRD', 'Instant').
card_original_text('awe strike'/'MRD', 'The next time target creature would deal damage this turn, prevent that damage. You gain life equal to the damage prevented this way.').
card_first_print('awe strike', 'MRD').
card_image_name('awe strike'/'MRD', 'awe strike').
card_uid('awe strike'/'MRD', 'MRD:Awe Strike:awe strike').
card_rarity('awe strike'/'MRD', 'Common').
card_artist('awe strike'/'MRD', 'Scott M. Fischer').
card_number('awe strike'/'MRD', '6').
card_flavor_text('awe strike'/'MRD', 'Stunned by the mere presence of the leonin kha, the nim raider quickly fell to its knees.').
card_multiverse_id('awe strike'/'MRD', '48074').

card_in_set('banshee\'s blade', 'MRD').
card_original_type('banshee\'s blade'/'MRD', 'Artifact — Equipment').
card_original_text('banshee\'s blade'/'MRD', 'Equipped creature gets +1/+1 for each charge counter on Banshee\'s Blade.\nWhenever equipped creature deals combat damage, put a charge counter on this card.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('banshee\'s blade', 'MRD').
card_image_name('banshee\'s blade'/'MRD', 'banshee\'s blade').
card_uid('banshee\'s blade'/'MRD', 'MRD:Banshee\'s Blade:banshee\'s blade').
card_rarity('banshee\'s blade'/'MRD', 'Uncommon').
card_artist('banshee\'s blade'/'MRD', 'Bradley Williams').
card_number('banshee\'s blade'/'MRD', '144').
card_multiverse_id('banshee\'s blade'/'MRD', '48194').

card_in_set('barter in blood', 'MRD').
card_original_type('barter in blood'/'MRD', 'Sorcery').
card_original_text('barter in blood'/'MRD', 'Each player sacrifices two creatures.').
card_first_print('barter in blood', 'MRD').
card_image_name('barter in blood'/'MRD', 'barter in blood').
card_uid('barter in blood'/'MRD', 'MRD:Barter in Blood:barter in blood').
card_rarity('barter in blood'/'MRD', 'Uncommon').
card_artist('barter in blood'/'MRD', 'Paolo Parente').
card_number('barter in blood'/'MRD', '57').
card_flavor_text('barter in blood'/'MRD', '\"In the game of conquest, who cares about the pawns if the king yet reigns?\"\n—Geth, keeper of the Vault').
card_multiverse_id('barter in blood'/'MRD', '48591').

card_in_set('battlegrowth', 'MRD').
card_original_type('battlegrowth'/'MRD', 'Instant').
card_original_text('battlegrowth'/'MRD', 'Put a +1/+1 counter on target creature.').
card_first_print('battlegrowth', 'MRD').
card_image_name('battlegrowth'/'MRD', 'battlegrowth').
card_uid('battlegrowth'/'MRD', 'MRD:Battlegrowth:battlegrowth').
card_rarity('battlegrowth'/'MRD', 'Common').
card_artist('battlegrowth'/'MRD', 'John Matson').
card_number('battlegrowth'/'MRD', '113').
card_flavor_text('battlegrowth'/'MRD', '\"I would gladly die for the forest, but I\'m much better at killing for it.\"').
card_multiverse_id('battlegrowth'/'MRD', '48116').

card_in_set('betrayal of flesh', 'MRD').
card_original_type('betrayal of flesh'/'MRD', 'Instant').
card_original_text('betrayal of flesh'/'MRD', 'Choose one Destroy target creature; or return target creature card from your graveyard to play.\nEntwine—Sacrifice three lands. (Choose both if you pay the entwine cost.)').
card_first_print('betrayal of flesh', 'MRD').
card_image_name('betrayal of flesh'/'MRD', 'betrayal of flesh').
card_uid('betrayal of flesh'/'MRD', 'MRD:Betrayal of Flesh:betrayal of flesh').
card_rarity('betrayal of flesh'/'MRD', 'Uncommon').
card_artist('betrayal of flesh'/'MRD', 'Wayne England').
card_number('betrayal of flesh'/'MRD', '58').
card_multiverse_id('betrayal of flesh'/'MRD', '48588').

card_in_set('blinding beam', 'MRD').
card_original_type('blinding beam'/'MRD', 'Instant').
card_original_text('blinding beam'/'MRD', 'Choose one Tap two target creatures; or creatures don\'t untap during target player\'s next untap step.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_first_print('blinding beam', 'MRD').
card_image_name('blinding beam'/'MRD', 'blinding beam').
card_uid('blinding beam'/'MRD', 'MRD:Blinding Beam:blinding beam').
card_rarity('blinding beam'/'MRD', 'Common').
card_artist('blinding beam'/'MRD', 'Doug Chaffee').
card_number('blinding beam'/'MRD', '7').
card_multiverse_id('blinding beam'/'MRD', '48099').

card_in_set('blinkmoth urn', 'MRD').
card_original_type('blinkmoth urn'/'MRD', 'Artifact').
card_original_text('blinkmoth urn'/'MRD', 'At the beginning of each player\'s precombat main phase, if Blinkmoth Urn is untapped, that player adds {1} to his or her mana pool for each artifact he or she controls.').
card_first_print('blinkmoth urn', 'MRD').
card_image_name('blinkmoth urn'/'MRD', 'blinkmoth urn').
card_uid('blinkmoth urn'/'MRD', 'MRD:Blinkmoth Urn:blinkmoth urn').
card_rarity('blinkmoth urn'/'MRD', 'Rare').
card_artist('blinkmoth urn'/'MRD', 'David Martin').
card_number('blinkmoth urn'/'MRD', '145').
card_flavor_text('blinkmoth urn'/'MRD', 'The vedalken embed such urns in their living artifact creations.').
card_multiverse_id('blinkmoth urn'/'MRD', '46714').

card_in_set('blinkmoth well', 'MRD').
card_original_type('blinkmoth well'/'MRD', 'Land').
card_original_text('blinkmoth well'/'MRD', '{T}: Add {1} to your mana pool.\n{2}, {T}: Tap target noncreature artifact.').
card_first_print('blinkmoth well', 'MRD').
card_image_name('blinkmoth well'/'MRD', 'blinkmoth well').
card_uid('blinkmoth well'/'MRD', 'MRD:Blinkmoth Well:blinkmoth well').
card_rarity('blinkmoth well'/'MRD', 'Uncommon').
card_artist('blinkmoth well'/'MRD', 'David Martin').
card_number('blinkmoth well'/'MRD', '279').
card_flavor_text('blinkmoth well'/'MRD', 'When dictated by blinkmoth migratory patterns, clouds of tiny lights well up from Mirrodin\'s core.').
card_multiverse_id('blinkmoth well'/'MRD', '48916').

card_in_set('bloodscent', 'MRD').
card_original_type('bloodscent'/'MRD', 'Instant').
card_original_text('bloodscent'/'MRD', 'All creatures able to block target creature this turn do so.').
card_first_print('bloodscent', 'MRD').
card_image_name('bloodscent'/'MRD', 'bloodscent').
card_uid('bloodscent'/'MRD', 'MRD:Bloodscent:bloodscent').
card_rarity('bloodscent'/'MRD', 'Uncommon').
card_artist('bloodscent'/'MRD', 'Matt Cavotta').
card_number('bloodscent'/'MRD', '114').
card_flavor_text('bloodscent'/'MRD', 'To study the predators of the Tangle, two people are required: one to watch from above, and one to run like hell.').
card_multiverse_id('bloodscent'/'MRD', '48597').

card_in_set('bonesplitter', 'MRD').
card_original_type('bonesplitter'/'MRD', 'Artifact — Equipment').
card_original_text('bonesplitter'/'MRD', 'Equipped creature gets +2/+0.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_image_name('bonesplitter'/'MRD', 'bonesplitter').
card_uid('bonesplitter'/'MRD', 'MRD:Bonesplitter:bonesplitter').
card_rarity('bonesplitter'/'MRD', 'Common').
card_artist('bonesplitter'/'MRD', 'Darrell Riche').
card_number('bonesplitter'/'MRD', '146').
card_multiverse_id('bonesplitter'/'MRD', '47442').

card_in_set('bosh, iron golem', 'MRD').
card_original_type('bosh, iron golem'/'MRD', 'Artifact Creature — Golem Legend').
card_original_text('bosh, iron golem'/'MRD', 'Trample\n{3}{R}, Sacrifice an artifact: Bosh, Iron Golem deals damage equal to the sacrificed artifact\'s converted mana cost to target creature or player.').
card_first_print('bosh, iron golem', 'MRD').
card_image_name('bosh, iron golem'/'MRD', 'bosh, iron golem').
card_uid('bosh, iron golem'/'MRD', 'MRD:Bosh, Iron Golem:bosh, iron golem').
card_rarity('bosh, iron golem'/'MRD', 'Rare').
card_artist('bosh, iron golem'/'MRD', 'Brom').
card_number('bosh, iron golem'/'MRD', '147').
card_flavor_text('bosh, iron golem'/'MRD', 'As Glissa searches for the truth about Memnarch, Bosh searches to unearth the secrets buried deep in his memory.').
card_multiverse_id('bosh, iron golem'/'MRD', '48581').

card_in_set('bottle gnomes', 'MRD').
card_original_type('bottle gnomes'/'MRD', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'MRD', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'MRD', 'bottle gnomes').
card_uid('bottle gnomes'/'MRD', 'MRD:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'MRD', 'Uncommon').
card_artist('bottle gnomes'/'MRD', 'Ben Thompson').
card_number('bottle gnomes'/'MRD', '148').
card_flavor_text('bottle gnomes'/'MRD', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'MRD', '46017').

card_in_set('broodstar', 'MRD').
card_original_type('broodstar'/'MRD', 'Creature — Beast').
card_original_text('broodstar'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nFlying\nBroodstar\'s power and toughness are each equal to the number of artifacts you control.').
card_first_print('broodstar', 'MRD').
card_image_name('broodstar'/'MRD', 'broodstar').
card_uid('broodstar'/'MRD', 'MRD:Broodstar:broodstar').
card_rarity('broodstar'/'MRD', 'Rare').
card_artist('broodstar'/'MRD', 'Glen Angus').
card_number('broodstar'/'MRD', '31').
card_multiverse_id('broodstar'/'MRD', '46556').

card_in_set('brown ouphe', 'MRD').
card_original_type('brown ouphe'/'MRD', 'Creature — Ouphe').
card_original_text('brown ouphe'/'MRD', '{1}{G}, {T}: Counter target activated ability from an artifact source. (Mana abilities can\'t be countered.)').
card_image_name('brown ouphe'/'MRD', 'brown ouphe').
card_uid('brown ouphe'/'MRD', 'MRD:Brown Ouphe:brown ouphe').
card_rarity('brown ouphe'/'MRD', 'Uncommon').
card_artist('brown ouphe'/'MRD', 'Greg Hildebrandt').
card_number('brown ouphe'/'MRD', '115').
card_flavor_text('brown ouphe'/'MRD', 'In a strange twist of fate, one of the most annoying creatures in the multiverse was brought to the place where it could cause the most damage.').
card_multiverse_id('brown ouphe'/'MRD', '46010').

card_in_set('cathodion', 'MRD').
card_original_type('cathodion'/'MRD', 'Artifact Creature').
card_original_text('cathodion'/'MRD', 'When Cathodion is put into a graveyard from play, add {3} to your mana pool.').
card_image_name('cathodion'/'MRD', 'cathodion').
card_uid('cathodion'/'MRD', 'MRD:Cathodion:cathodion').
card_rarity('cathodion'/'MRD', 'Uncommon').
card_artist('cathodion'/'MRD', 'Eric Peterson').
card_number('cathodion'/'MRD', '149').
card_flavor_text('cathodion'/'MRD', 'Cathodions repair the Great Furnace by soldering parts of themselves to the mechanism. Eventually, they become one with the machine.').
card_multiverse_id('cathodion'/'MRD', '48195').

card_in_set('chalice of the void', 'MRD').
card_original_type('chalice of the void'/'MRD', 'Artifact').
card_original_text('chalice of the void'/'MRD', 'Chalice of the Void comes into play with X charge counters on it.\nWhenever a player plays a spell with converted mana cost equal to the number of charge counters on Chalice of the Void, counter that spell.').
card_first_print('chalice of the void', 'MRD').
card_image_name('chalice of the void'/'MRD', 'chalice of the void').
card_uid('chalice of the void'/'MRD', 'MRD:Chalice of the Void:chalice of the void').
card_rarity('chalice of the void'/'MRD', 'Rare').
card_artist('chalice of the void'/'MRD', 'Mark Zug').
card_number('chalice of the void'/'MRD', '150').
card_multiverse_id('chalice of the void'/'MRD', '48326').

card_in_set('chimney imp', 'MRD').
card_original_type('chimney imp'/'MRD', 'Creature — Imp').
card_original_text('chimney imp'/'MRD', 'Flying\nWhen Chimney Imp is put into a graveyard from play, target opponent puts a card from his or her hand on top of his or her library.').
card_first_print('chimney imp', 'MRD').
card_image_name('chimney imp'/'MRD', 'chimney imp').
card_uid('chimney imp'/'MRD', 'MRD:Chimney Imp:chimney imp').
card_rarity('chimney imp'/'MRD', 'Common').
card_artist('chimney imp'/'MRD', 'Christopher Moeller').
card_number('chimney imp'/'MRD', '59').
card_multiverse_id('chimney imp'/'MRD', '45985').

card_in_set('chromatic sphere', 'MRD').
card_original_type('chromatic sphere'/'MRD', 'Artifact').
card_original_text('chromatic sphere'/'MRD', '{1}, {T}, Sacrifice Chromatic Sphere: Add one mana of any color to your mana pool. Draw a card.').
card_image_name('chromatic sphere'/'MRD', 'chromatic sphere').
card_uid('chromatic sphere'/'MRD', 'MRD:Chromatic Sphere:chromatic sphere').
card_rarity('chromatic sphere'/'MRD', 'Common').
card_artist('chromatic sphere'/'MRD', 'Brian Snõddy').
card_number('chromatic sphere'/'MRD', '151').
card_flavor_text('chromatic sphere'/'MRD', '\"As expected, this sphere\'s design reflects the colors of the four moons . . . plus another?\"\n—Pontifex, elder researcher').
card_multiverse_id('chromatic sphere'/'MRD', '46046').

card_in_set('chrome mox', 'MRD').
card_original_type('chrome mox'/'MRD', 'Artifact').
card_original_text('chrome mox'/'MRD', 'Imprint When Chrome Mox comes into play, you may remove a nonartifact, nonland card in your hand from the game. (The removed card is imprinted on this artifact.)\n{T}: Add one mana of any of the imprinted card\'s colors to your mana pool.').
card_first_print('chrome mox', 'MRD').
card_image_name('chrome mox'/'MRD', 'chrome mox').
card_uid('chrome mox'/'MRD', 'MRD:Chrome Mox:chrome mox').
card_rarity('chrome mox'/'MRD', 'Rare').
card_artist('chrome mox'/'MRD', 'Donato Giancola').
card_number('chrome mox'/'MRD', '152').
card_multiverse_id('chrome mox'/'MRD', '47446').

card_in_set('clockwork beetle', 'MRD').
card_original_type('clockwork beetle'/'MRD', 'Artifact Creature — Insect').
card_original_text('clockwork beetle'/'MRD', 'Clockwork Beetle comes into play with two +1/+1 counters on it.\nWhenever Clockwork Beetle attacks or blocks, remove a +1/+1 counter from it at end of combat.').
card_first_print('clockwork beetle', 'MRD').
card_image_name('clockwork beetle'/'MRD', 'clockwork beetle').
card_uid('clockwork beetle'/'MRD', 'MRD:Clockwork Beetle:clockwork beetle').
card_rarity('clockwork beetle'/'MRD', 'Common').
card_artist('clockwork beetle'/'MRD', 'Arnie Swekel').
card_number('clockwork beetle'/'MRD', '153').
card_multiverse_id('clockwork beetle'/'MRD', '48119').

card_in_set('clockwork condor', 'MRD').
card_original_type('clockwork condor'/'MRD', 'Artifact Creature — Bird').
card_original_text('clockwork condor'/'MRD', 'Flying\nClockwork Condor comes into play with three +1/+1 counters on it.\nWhenever Clockwork Condor attacks or blocks, remove a +1/+1 counter from it at end of combat.').
card_first_print('clockwork condor', 'MRD').
card_image_name('clockwork condor'/'MRD', 'clockwork condor').
card_uid('clockwork condor'/'MRD', 'MRD:Clockwork Condor:clockwork condor').
card_rarity('clockwork condor'/'MRD', 'Common').
card_artist('clockwork condor'/'MRD', 'Arnie Swekel').
card_number('clockwork condor'/'MRD', '154').
card_multiverse_id('clockwork condor'/'MRD', '46124').

card_in_set('clockwork dragon', 'MRD').
card_original_type('clockwork dragon'/'MRD', 'Artifact Creature — Dragon').
card_original_text('clockwork dragon'/'MRD', 'Flying\nClockwork Dragon comes into play with six +1/+1 counters on it.\nWhenever Clockwork Dragon attacks or blocks, remove a +1/+1 counter from it at end of combat.\n{3}: Put a +1/+1 counter on Clockwork Dragon.').
card_first_print('clockwork dragon', 'MRD').
card_image_name('clockwork dragon'/'MRD', 'clockwork dragon').
card_uid('clockwork dragon'/'MRD', 'MRD:Clockwork Dragon:clockwork dragon').
card_rarity('clockwork dragon'/'MRD', 'Rare').
card_artist('clockwork dragon'/'MRD', 'Arnie Swekel').
card_number('clockwork dragon'/'MRD', '155').
card_multiverse_id('clockwork dragon'/'MRD', '46704').

card_in_set('clockwork vorrac', 'MRD').
card_original_type('clockwork vorrac'/'MRD', 'Artifact Creature — Beast').
card_original_text('clockwork vorrac'/'MRD', 'Trample\nClockwork Vorrac comes into play with four +1/+1 counters on it.\nWhenever Clockwork Vorrac attacks or blocks, remove a +1/+1 counter from it at end of combat.\n{T}: Put a +1/+1 counter on Clockwork Vorrac.').
card_first_print('clockwork vorrac', 'MRD').
card_image_name('clockwork vorrac'/'MRD', 'clockwork vorrac').
card_uid('clockwork vorrac'/'MRD', 'MRD:Clockwork Vorrac:clockwork vorrac').
card_rarity('clockwork vorrac'/'MRD', 'Uncommon').
card_artist('clockwork vorrac'/'MRD', 'Arnie Swekel').
card_number('clockwork vorrac'/'MRD', '156').
card_multiverse_id('clockwork vorrac'/'MRD', '46123').

card_in_set('cloudpost', 'MRD').
card_original_type('cloudpost'/'MRD', 'Land — Locus').
card_original_text('cloudpost'/'MRD', 'Cloudpost comes into play tapped.\n{T}: Add {1} to your mana pool for each Locus in play.').
card_image_name('cloudpost'/'MRD', 'cloudpost').
card_uid('cloudpost'/'MRD', 'MRD:Cloudpost:cloudpost').
card_rarity('cloudpost'/'MRD', 'Common').
card_artist('cloudpost'/'MRD', 'Martina Pilcerova').
card_number('cloudpost'/'MRD', '280').
card_flavor_text('cloudpost'/'MRD', '\"He watches from above. He watches from below. He watches from within.\"\n—Inscription on Tel-Jilad, the Tree of Tales').
card_multiverse_id('cloudpost'/'MRD', '49050').

card_in_set('cobalt golem', 'MRD').
card_original_type('cobalt golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('cobalt golem'/'MRD', '{1}{U}: Cobalt Golem gains flying until end of turn.').
card_first_print('cobalt golem', 'MRD').
card_image_name('cobalt golem'/'MRD', 'cobalt golem').
card_uid('cobalt golem'/'MRD', 'MRD:Cobalt Golem:cobalt golem').
card_rarity('cobalt golem'/'MRD', 'Common').
card_artist('cobalt golem'/'MRD', 'Paolo Parente').
card_number('cobalt golem'/'MRD', '157').
card_flavor_text('cobalt golem'/'MRD', 'Centuries before the first tides of the Quicksilver Sea rose to meet each new sun, Mirrodin\'s light shone on the golems alone.').
card_multiverse_id('cobalt golem'/'MRD', '48053').

card_in_set('confusion in the ranks', 'MRD').
card_original_type('confusion in the ranks'/'MRD', 'Enchantment').
card_original_text('confusion in the ranks'/'MRD', 'Whenever an artifact, creature, or enchantment comes into play, its controller chooses target permanent another player controls that shares a type with it. Exchange control of those permanents.').
card_first_print('confusion in the ranks', 'MRD').
card_image_name('confusion in the ranks'/'MRD', 'confusion in the ranks').
card_uid('confusion in the ranks'/'MRD', 'MRD:Confusion in the Ranks:confusion in the ranks').
card_rarity('confusion in the ranks'/'MRD', 'Rare').
card_artist('confusion in the ranks'/'MRD', 'Ron Spencer').
card_number('confusion in the ranks'/'MRD', '87').
card_multiverse_id('confusion in the ranks'/'MRD', '49528').

card_in_set('consume spirit', 'MRD').
card_original_type('consume spirit'/'MRD', 'Sorcery').
card_original_text('consume spirit'/'MRD', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player. You gain X life.').
card_image_name('consume spirit'/'MRD', 'consume spirit').
card_uid('consume spirit'/'MRD', 'MRD:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'MRD', 'Common').
card_artist('consume spirit'/'MRD', 'Matt Thompson').
card_number('consume spirit'/'MRD', '60').
card_flavor_text('consume spirit'/'MRD', 'Mephidross changes all who dwell there, taking their lives and adding them to its own.').
card_multiverse_id('consume spirit'/'MRD', '45987').

card_in_set('contaminated bond', 'MRD').
card_original_type('contaminated bond'/'MRD', 'Enchant Creature').
card_original_text('contaminated bond'/'MRD', 'Whenever enchanted creature attacks or blocks, its controller loses 3 life.').
card_first_print('contaminated bond', 'MRD').
card_image_name('contaminated bond'/'MRD', 'contaminated bond').
card_uid('contaminated bond'/'MRD', 'MRD:Contaminated Bond:contaminated bond').
card_rarity('contaminated bond'/'MRD', 'Common').
card_artist('contaminated bond'/'MRD', 'Thomas M. Baxa').
card_number('contaminated bond'/'MRD', '61').
card_flavor_text('contaminated bond'/'MRD', 'This leash disciplines the master.').
card_multiverse_id('contaminated bond'/'MRD', '49444').

card_in_set('copper myr', 'MRD').
card_original_type('copper myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('copper myr'/'MRD', '{T}: Add {G} to your mana pool.').
card_first_print('copper myr', 'MRD').
card_image_name('copper myr'/'MRD', 'copper myr').
card_uid('copper myr'/'MRD', 'MRD:Copper Myr:copper myr').
card_rarity('copper myr'/'MRD', 'Common').
card_artist('copper myr'/'MRD', 'Kev Walker').
card_number('copper myr'/'MRD', '158').
card_flavor_text('copper myr'/'MRD', 'The elves thought of the myr as minor threats, just as the myr thought of the elves.').
card_multiverse_id('copper myr'/'MRD', '46056').

card_in_set('copperhoof vorrac', 'MRD').
card_original_type('copperhoof vorrac'/'MRD', 'Creature — Beast').
card_original_text('copperhoof vorrac'/'MRD', 'Copperhoof Vorrac gets +1/+1 for each untapped permanent your opponents control.').
card_first_print('copperhoof vorrac', 'MRD').
card_image_name('copperhoof vorrac'/'MRD', 'copperhoof vorrac').
card_uid('copperhoof vorrac'/'MRD', 'MRD:Copperhoof Vorrac:copperhoof vorrac').
card_rarity('copperhoof vorrac'/'MRD', 'Rare').
card_artist('copperhoof vorrac'/'MRD', 'Matt Cavotta').
card_number('copperhoof vorrac'/'MRD', '116').
card_flavor_text('copperhoof vorrac'/'MRD', 'Like all forest beasts, it lives by one rule: if there\'s no room to grow, make some.').
card_multiverse_id('copperhoof vorrac'/'MRD', '48603').

card_in_set('creeping mold', 'MRD').
card_original_type('creeping mold'/'MRD', 'Sorcery').
card_original_text('creeping mold'/'MRD', 'Destroy target artifact, enchantment, or land.').
card_image_name('creeping mold'/'MRD', 'creeping mold').
card_uid('creeping mold'/'MRD', 'MRD:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'MRD', 'Uncommon').
card_artist('creeping mold'/'MRD', 'Dany Orizio').
card_number('creeping mold'/'MRD', '117').
card_flavor_text('creeping mold'/'MRD', 'The deadliest force on Mirrodin isn\'t the largest organism—it\'s the smallest.').
card_multiverse_id('creeping mold'/'MRD', '49108').

card_in_set('crystal shard', 'MRD').
card_original_type('crystal shard'/'MRD', 'Artifact').
card_original_text('crystal shard'/'MRD', '{3}, {T} or {U}, {T}: Return target creature to its owner\'s hand unless its controller pays {1}.').
card_first_print('crystal shard', 'MRD').
card_image_name('crystal shard'/'MRD', 'crystal shard').
card_uid('crystal shard'/'MRD', 'MRD:Crystal Shard:crystal shard').
card_rarity('crystal shard'/'MRD', 'Uncommon').
card_artist('crystal shard'/'MRD', 'Doug Chaffee').
card_number('crystal shard'/'MRD', '159').
card_flavor_text('crystal shard'/'MRD', 'The vedalken know it is not of this world, so they know that this world is not the only one.').
card_multiverse_id('crystal shard'/'MRD', '46035').

card_in_set('culling scales', 'MRD').
card_original_type('culling scales'/'MRD', 'Artifact').
card_original_text('culling scales'/'MRD', 'At the beginning of your upkeep, destroy target nonland permanent with the lowest converted mana cost among nonland permanents in play. (If two or more permanents are tied for lowest cost, target any one of them.)').
card_first_print('culling scales', 'MRD').
card_image_name('culling scales'/'MRD', 'culling scales').
card_uid('culling scales'/'MRD', 'MRD:Culling Scales:culling scales').
card_rarity('culling scales'/'MRD', 'Rare').
card_artist('culling scales'/'MRD', 'Daren Bader').
card_number('culling scales'/'MRD', '160').
card_multiverse_id('culling scales'/'MRD', '47445').

card_in_set('damping matrix', 'MRD').
card_original_type('damping matrix'/'MRD', 'Artifact').
card_original_text('damping matrix'/'MRD', 'Activated abilities of artifacts and creatures can\'t be played unless they\'re mana abilities.').
card_first_print('damping matrix', 'MRD').
card_image_name('damping matrix'/'MRD', 'damping matrix').
card_uid('damping matrix'/'MRD', 'MRD:Damping Matrix:damping matrix').
card_rarity('damping matrix'/'MRD', 'Rare').
card_artist('damping matrix'/'MRD', 'Mike Dringenberg').
card_number('damping matrix'/'MRD', '161').
card_flavor_text('damping matrix'/'MRD', 'The priests tried cursing it. The mages tried dispelling it. In the end, they all obeyed it.').
card_multiverse_id('damping matrix'/'MRD', '46727').

card_in_set('dead-iron sledge', 'MRD').
card_original_type('dead-iron sledge'/'MRD', 'Artifact — Equipment').
card_original_text('dead-iron sledge'/'MRD', 'Whenever equipped creature blocks or becomes blocked by a creature, destroy that creature and equipped creature.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('dead-iron sledge', 'MRD').
card_image_name('dead-iron sledge'/'MRD', 'dead-iron sledge').
card_uid('dead-iron sledge'/'MRD', 'MRD:Dead-Iron Sledge:dead-iron sledge').
card_rarity('dead-iron sledge'/'MRD', 'Uncommon').
card_artist('dead-iron sledge'/'MRD', 'Ray Lago').
card_number('dead-iron sledge'/'MRD', '162').
card_multiverse_id('dead-iron sledge'/'MRD', '48139').

card_in_set('deconstruct', 'MRD').
card_original_type('deconstruct'/'MRD', 'Sorcery').
card_original_text('deconstruct'/'MRD', 'Destroy target artifact. Then add {G}{G}{G} to your mana pool.').
card_first_print('deconstruct', 'MRD').
card_image_name('deconstruct'/'MRD', 'deconstruct').
card_uid('deconstruct'/'MRD', 'MRD:Deconstruct:deconstruct').
card_rarity('deconstruct'/'MRD', 'Common').
card_artist('deconstruct'/'MRD', 'D. Alexander Gregory').
card_number('deconstruct'/'MRD', '118').
card_flavor_text('deconstruct'/'MRD', '\"Nothing in this world, not even magic, likes to be caged.\"').
card_multiverse_id('deconstruct'/'MRD', '49060').

card_in_set('detonate', 'MRD').
card_original_type('detonate'/'MRD', 'Sorcery').
card_original_text('detonate'/'MRD', 'Destroy target artifact with converted mana cost X. It can\'t be regenerated. Detonate deals X damage to that artifact\'s controller.').
card_image_name('detonate'/'MRD', 'detonate').
card_uid('detonate'/'MRD', 'MRD:Detonate:detonate').
card_rarity('detonate'/'MRD', 'Uncommon').
card_artist('detonate'/'MRD', 'Dave Dorman').
card_number('detonate'/'MRD', '88').
card_flavor_text('detonate'/'MRD', 'The goblins have forty-two different words for \"ow.\"').
card_multiverse_id('detonate'/'MRD', '46110').

card_in_set('disarm', 'MRD').
card_original_type('disarm'/'MRD', 'Instant').
card_original_text('disarm'/'MRD', 'Unattach all Equipment from target creature.').
card_first_print('disarm', 'MRD').
card_image_name('disarm'/'MRD', 'disarm').
card_uid('disarm'/'MRD', 'MRD:Disarm:disarm').
card_rarity('disarm'/'MRD', 'Common').
card_artist('disarm'/'MRD', 'Alex Horley-Orlandelli').
card_number('disarm'/'MRD', '32').
card_flavor_text('disarm'/'MRD', '\"Be thankful I left you your clothes.\"').
card_multiverse_id('disarm'/'MRD', '46084').

card_in_set('disciple of the vault', 'MRD').
card_original_type('disciple of the vault'/'MRD', 'Creature — Human Cleric').
card_original_text('disciple of the vault'/'MRD', 'Whenever an artifact is put into a graveyard from play, you may have target opponent lose 1 life.').
card_first_print('disciple of the vault', 'MRD').
card_image_name('disciple of the vault'/'MRD', 'disciple of the vault').
card_uid('disciple of the vault'/'MRD', 'MRD:Disciple of the Vault:disciple of the vault').
card_rarity('disciple of the vault'/'MRD', 'Common').
card_artist('disciple of the vault'/'MRD', 'Matt Thompson').
card_number('disciple of the vault'/'MRD', '62').
card_flavor_text('disciple of the vault'/'MRD', 'He stands in the shadow of his lord, Geth, drinking in the dark energies of the Vault.').
card_multiverse_id('disciple of the vault'/'MRD', '49090').

card_in_set('domineer', 'MRD').
card_original_type('domineer'/'MRD', 'Enchant Artifact Creature').
card_original_text('domineer'/'MRD', 'You control enchanted artifact creature.').
card_first_print('domineer', 'MRD').
card_image_name('domineer'/'MRD', 'domineer').
card_uid('domineer'/'MRD', 'MRD:Domineer:domineer').
card_rarity('domineer'/'MRD', 'Uncommon').
card_artist('domineer'/'MRD', 'Jon Foster').
card_number('domineer'/'MRD', '33').
card_flavor_text('domineer'/'MRD', 'Since they haven\'t seen their original master for millennia, golems are eager to take orders from anyone.').
card_multiverse_id('domineer'/'MRD', '48571').

card_in_set('dragon blood', 'MRD').
card_original_type('dragon blood'/'MRD', 'Artifact').
card_original_text('dragon blood'/'MRD', '{3}, {T}: Put a +1/+1 counter on target creature.').
card_image_name('dragon blood'/'MRD', 'dragon blood').
card_uid('dragon blood'/'MRD', 'MRD:Dragon Blood:dragon blood').
card_rarity('dragon blood'/'MRD', 'Uncommon').
card_artist('dragon blood'/'MRD', 'Ron Spencer').
card_number('dragon blood'/'MRD', '163').
card_flavor_text('dragon blood'/'MRD', 'A single drop turns skin to scale and fist to claw.').
card_multiverse_id('dragon blood'/'MRD', '48325').

card_in_set('dream\'s grip', 'MRD').
card_original_type('dream\'s grip'/'MRD', 'Instant').
card_original_text('dream\'s grip'/'MRD', 'Choose one Tap target permanent; or untap target permanent.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_first_print('dream\'s grip', 'MRD').
card_image_name('dream\'s grip'/'MRD', 'dream\'s grip').
card_uid('dream\'s grip'/'MRD', 'MRD:Dream\'s Grip:dream\'s grip').
card_rarity('dream\'s grip'/'MRD', 'Common').
card_artist('dream\'s grip'/'MRD', 'Jim Nelson').
card_number('dream\'s grip'/'MRD', '34').
card_multiverse_id('dream\'s grip'/'MRD', '48159').

card_in_set('dross harvester', 'MRD').
card_original_type('dross harvester'/'MRD', 'Creature — Horror').
card_original_text('dross harvester'/'MRD', 'Protection from white\nAt the end of your turn, you lose 4 life.\nWhenever a creature is put into a graveyard from play, you gain 2 life.').
card_first_print('dross harvester', 'MRD').
card_image_name('dross harvester'/'MRD', 'dross harvester').
card_uid('dross harvester'/'MRD', 'MRD:Dross Harvester:dross harvester').
card_rarity('dross harvester'/'MRD', 'Rare').
card_artist('dross harvester'/'MRD', 'Michael Sutfin').
card_number('dross harvester'/'MRD', '63').
card_multiverse_id('dross harvester'/'MRD', '50537').

card_in_set('dross prowler', 'MRD').
card_original_type('dross prowler'/'MRD', 'Creature — Zombie').
card_original_text('dross prowler'/'MRD', 'Fear').
card_first_print('dross prowler', 'MRD').
card_image_name('dross prowler'/'MRD', 'dross prowler').
card_uid('dross prowler'/'MRD', 'MRD:Dross Prowler:dross prowler').
card_rarity('dross prowler'/'MRD', 'Common').
card_artist('dross prowler'/'MRD', 'Michael Sutfin').
card_number('dross prowler'/'MRD', '64').
card_flavor_text('dross prowler'/'MRD', '\"I would never have believed that on a world with four suns there could exist a place so dark.\"\n—Glissa Sunseeker').
card_multiverse_id('dross prowler'/'MRD', '48570').

card_in_set('dross scorpion', 'MRD').
card_original_type('dross scorpion'/'MRD', 'Artifact Creature').
card_original_text('dross scorpion'/'MRD', 'Whenever Dross Scorpion or another artifact creature is put into a graveyard from play, you may untap target artifact.').
card_first_print('dross scorpion', 'MRD').
card_image_name('dross scorpion'/'MRD', 'dross scorpion').
card_uid('dross scorpion'/'MRD', 'MRD:Dross Scorpion:dross scorpion').
card_rarity('dross scorpion'/'MRD', 'Common').
card_artist('dross scorpion'/'MRD', 'Jim Nelson').
card_number('dross scorpion'/'MRD', '164').
card_flavor_text('dross scorpion'/'MRD', 'They skitter out of the mists to consume fresh kill before Mephidross has a chance to corrode it away.').
card_multiverse_id('dross scorpion'/'MRD', '46434').

card_in_set('duplicant', 'MRD').
card_original_type('duplicant'/'MRD', 'Artifact Creature — Shapeshifter').
card_original_text('duplicant'/'MRD', 'Imprint When Duplicant comes into play, you may remove target nontoken creature from the game. (The removed card is imprinted on this artifact.)\nAs long as a creature card is imprinted on Duplicant, Duplicant has that card\'s power, toughness, and creature types. It\'s still a Shapeshifter.').
card_first_print('duplicant', 'MRD').
card_image_name('duplicant'/'MRD', 'duplicant').
card_uid('duplicant'/'MRD', 'MRD:Duplicant:duplicant').
card_rarity('duplicant'/'MRD', 'Rare').
card_artist('duplicant'/'MRD', 'Thomas M. Baxa').
card_number('duplicant'/'MRD', '165').
card_multiverse_id('duplicant'/'MRD', '49439').

card_in_set('duskworker', 'MRD').
card_original_type('duskworker'/'MRD', 'Artifact Creature').
card_original_text('duskworker'/'MRD', 'Whenever Duskworker becomes blocked, regenerate it.\n{3}: Duskworker gets +1/+0 until end of turn.').
card_first_print('duskworker', 'MRD').
card_image_name('duskworker'/'MRD', 'duskworker').
card_uid('duskworker'/'MRD', 'MRD:Duskworker:duskworker').
card_rarity('duskworker'/'MRD', 'Uncommon').
card_artist('duskworker'/'MRD', 'Greg Staples').
card_number('duskworker'/'MRD', '166').
card_flavor_text('duskworker'/'MRD', 'At the setting of each sun, it emerges to clean Mirrodin\'s floor of the day\'s carrion.').
card_multiverse_id('duskworker'/'MRD', '46014').

card_in_set('electrostatic bolt', 'MRD').
card_original_type('electrostatic bolt'/'MRD', 'Instant').
card_original_text('electrostatic bolt'/'MRD', 'Electrostatic Bolt deals 2 damage to target creature. If it\'s an artifact creature, Electrostatic Bolt deals 4 damage to it instead.').
card_first_print('electrostatic bolt', 'MRD').
card_image_name('electrostatic bolt'/'MRD', 'electrostatic bolt').
card_uid('electrostatic bolt'/'MRD', 'MRD:Electrostatic Bolt:electrostatic bolt').
card_rarity('electrostatic bolt'/'MRD', 'Common').
card_artist('electrostatic bolt'/'MRD', 'Randy Gallegos').
card_number('electrostatic bolt'/'MRD', '89').
card_flavor_text('electrostatic bolt'/'MRD', 'It\'s hard to avoid electric shock when the entire plane is metallic.').
card_multiverse_id('electrostatic bolt'/'MRD', '49101').

card_in_set('elf replica', 'MRD').
card_original_type('elf replica'/'MRD', 'Artifact Creature — Elf').
card_original_text('elf replica'/'MRD', '{1}{G}, Sacrifice Elf Replica: Destroy target enchantment.').
card_first_print('elf replica', 'MRD').
card_image_name('elf replica'/'MRD', 'elf replica').
card_uid('elf replica'/'MRD', 'MRD:Elf Replica:elf replica').
card_rarity('elf replica'/'MRD', 'Common').
card_artist('elf replica'/'MRD', 'Carl Critchlow').
card_number('elf replica'/'MRD', '167').
card_flavor_text('elf replica'/'MRD', 'It hunts with unnerving ferocity.').
card_multiverse_id('elf replica'/'MRD', '46057').

card_in_set('empyrial plate', 'MRD').
card_original_type('empyrial plate'/'MRD', 'Artifact — Equipment').
card_original_text('empyrial plate'/'MRD', 'Equipped creature gets +1/+1 for each card in your hand.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('empyrial plate', 'MRD').
card_image_name('empyrial plate'/'MRD', 'empyrial plate').
card_uid('empyrial plate'/'MRD', 'MRD:Empyrial Plate:empyrial plate').
card_rarity('empyrial plate'/'MRD', 'Rare').
card_artist('empyrial plate'/'MRD', 'Paolo Parente').
card_number('empyrial plate'/'MRD', '168').
card_multiverse_id('empyrial plate'/'MRD', '49430').

card_in_set('extraplanar lens', 'MRD').
card_original_type('extraplanar lens'/'MRD', 'Artifact').
card_original_text('extraplanar lens'/'MRD', 'Imprint When Extraplanar Lens comes into play, you may remove target land you control from the game. (The removed card is imprinted on this artifact.)\nWhenever a land with the same name as the imprinted card is tapped for mana, its controller adds one mana to his or her mana pool of any type that land produced.').
card_first_print('extraplanar lens', 'MRD').
card_image_name('extraplanar lens'/'MRD', 'extraplanar lens').
card_uid('extraplanar lens'/'MRD', 'MRD:Extraplanar Lens:extraplanar lens').
card_rarity('extraplanar lens'/'MRD', 'Rare').
card_artist('extraplanar lens'/'MRD', 'Lars Grant-West').
card_number('extraplanar lens'/'MRD', '169').
card_multiverse_id('extraplanar lens'/'MRD', '49440').

card_in_set('fabricate', 'MRD').
card_original_type('fabricate'/'MRD', 'Sorcery').
card_original_text('fabricate'/'MRD', 'Search your library for an artifact card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('fabricate', 'MRD').
card_image_name('fabricate'/'MRD', 'fabricate').
card_uid('fabricate'/'MRD', 'MRD:Fabricate:fabricate').
card_rarity('fabricate'/'MRD', 'Uncommon').
card_artist('fabricate'/'MRD', 'Glen Angus').
card_number('fabricate'/'MRD', '35').
card_flavor_text('fabricate'/'MRD', '\"The secret to invention is to see something in your mind, then find where it hides in the world.\"').
card_multiverse_id('fabricate'/'MRD', '48568').

card_in_set('fangren hunter', 'MRD').
card_original_type('fangren hunter'/'MRD', 'Creature — Beast').
card_original_text('fangren hunter'/'MRD', 'Trample').
card_first_print('fangren hunter', 'MRD').
card_image_name('fangren hunter'/'MRD', 'fangren hunter').
card_uid('fangren hunter'/'MRD', 'MRD:Fangren Hunter:fangren hunter').
card_rarity('fangren hunter'/'MRD', 'Common').
card_artist('fangren hunter'/'MRD', 'Darrell Riche').
card_number('fangren hunter'/'MRD', '119').
card_flavor_text('fangren hunter'/'MRD', 'Big ones hunt the elves, so the elves hunt the small ones to keep them from getting big.').
card_multiverse_id('fangren hunter'/'MRD', '46115').

card_in_set('farsight mask', 'MRD').
card_original_type('farsight mask'/'MRD', 'Artifact').
card_original_text('farsight mask'/'MRD', 'Whenever a source an opponent controls deals damage to you, if Farsight Mask is untapped, you may draw a card.').
card_first_print('farsight mask', 'MRD').
card_image_name('farsight mask'/'MRD', 'farsight mask').
card_uid('farsight mask'/'MRD', 'MRD:Farsight Mask:farsight mask').
card_rarity('farsight mask'/'MRD', 'Uncommon').
card_artist('farsight mask'/'MRD', 'Ben Thompson').
card_number('farsight mask'/'MRD', '170').
card_flavor_text('farsight mask'/'MRD', 'It turns the adversity of the moment into the knowledge of a lifetime.').
card_multiverse_id('farsight mask'/'MRD', '46135').

card_in_set('fatespinner', 'MRD').
card_original_type('fatespinner'/'MRD', 'Creature — Human Wizard').
card_original_text('fatespinner'/'MRD', 'At the beginning of each opponent\'s upkeep, that player chooses draw step, main phase, or combat phase. The player skips each instance of the chosen step or phase this turn.').
card_first_print('fatespinner', 'MRD').
card_image_name('fatespinner'/'MRD', 'fatespinner').
card_uid('fatespinner'/'MRD', 'MRD:Fatespinner:fatespinner').
card_rarity('fatespinner'/'MRD', 'Rare').
card_artist('fatespinner'/'MRD', 'rk post').
card_number('fatespinner'/'MRD', '36').
card_flavor_text('fatespinner'/'MRD', 'No one knows what she is watching, but she never takes her eyes off it.').
card_multiverse_id('fatespinner'/'MRD', '45970').

card_in_set('fiery gambit', 'MRD').
card_original_type('fiery gambit'/'MRD', 'Sorcery').
card_original_text('fiery gambit'/'MRD', 'Flip a coin until you lose a flip or choose to stop flipping. If you lose a flip, Fiery Gambit has no effect. If you win one or more flips, Fiery Gambit deals 3 damage to target creature. If you win two or more flips, Fiery Gambit deals 6 damage to each opponent. If you win three or more flips, draw nine cards and untap all lands you control.').
card_first_print('fiery gambit', 'MRD').
card_image_name('fiery gambit'/'MRD', 'fiery gambit').
card_uid('fiery gambit'/'MRD', 'MRD:Fiery Gambit:fiery gambit').
card_rarity('fiery gambit'/'MRD', 'Rare').
card_artist('fiery gambit'/'MRD', 'Scott M. Fischer').
card_number('fiery gambit'/'MRD', '90').
card_multiverse_id('fiery gambit'/'MRD', '48073').

card_in_set('fireshrieker', 'MRD').
card_original_type('fireshrieker'/'MRD', 'Artifact — Equipment').
card_original_text('fireshrieker'/'MRD', 'Equipped creature has double strike. (It deals both first-strike and regular combat damage.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('fireshrieker', 'MRD').
card_image_name('fireshrieker'/'MRD', 'fireshrieker').
card_uid('fireshrieker'/'MRD', 'MRD:Fireshrieker:fireshrieker').
card_rarity('fireshrieker'/'MRD', 'Uncommon').
card_artist('fireshrieker'/'MRD', 'Christopher Moeller').
card_number('fireshrieker'/'MRD', '171').
card_multiverse_id('fireshrieker'/'MRD', '48203').

card_in_set('fists of the anvil', 'MRD').
card_original_type('fists of the anvil'/'MRD', 'Instant').
card_original_text('fists of the anvil'/'MRD', 'Target creature gets +4/+0 until end of turn.').
card_first_print('fists of the anvil', 'MRD').
card_image_name('fists of the anvil'/'MRD', 'fists of the anvil').
card_uid('fists of the anvil'/'MRD', 'MRD:Fists of the Anvil:fists of the anvil').
card_rarity('fists of the anvil'/'MRD', 'Common').
card_artist('fists of the anvil'/'MRD', 'Pete Venters').
card_number('fists of the anvil'/'MRD', '91').
card_flavor_text('fists of the anvil'/'MRD', 'Gron\'s mind reeled with possibilities, most of which are best left unmentioned.').
card_multiverse_id('fists of the anvil'/'MRD', '48377').

card_in_set('flayed nim', 'MRD').
card_original_type('flayed nim'/'MRD', 'Creature — Skeleton').
card_original_text('flayed nim'/'MRD', 'Whenever Flayed Nim deals combat damage to a creature, that creature\'s controller loses that much life.\n{2}{B}: Regenerate Flayed Nim.').
card_first_print('flayed nim', 'MRD').
card_image_name('flayed nim'/'MRD', 'flayed nim').
card_uid('flayed nim'/'MRD', 'MRD:Flayed Nim:flayed nim').
card_rarity('flayed nim'/'MRD', 'Uncommon').
card_artist('flayed nim'/'MRD', 'Trevor Hairsine').
card_number('flayed nim'/'MRD', '65').
card_multiverse_id('flayed nim'/'MRD', '46094').

card_in_set('forest', 'MRD').
card_original_type('forest'/'MRD', 'Basic Land — Forest').
card_original_text('forest'/'MRD', 'G').
card_image_name('forest'/'MRD', 'forest1').
card_uid('forest'/'MRD', 'MRD:Forest:forest1').
card_rarity('forest'/'MRD', 'Basic Land').
card_artist('forest'/'MRD', 'Mark Tedin').
card_number('forest'/'MRD', '303').
card_multiverse_id('forest'/'MRD', '48421').

card_in_set('forest', 'MRD').
card_original_type('forest'/'MRD', 'Basic Land — Forest').
card_original_text('forest'/'MRD', 'G').
card_image_name('forest'/'MRD', 'forest2').
card_uid('forest'/'MRD', 'MRD:Forest:forest2').
card_rarity('forest'/'MRD', 'Basic Land').
card_artist('forest'/'MRD', 'Rob Alexander').
card_number('forest'/'MRD', '304').
card_multiverse_id('forest'/'MRD', '48422').

card_in_set('forest', 'MRD').
card_original_type('forest'/'MRD', 'Basic Land — Forest').
card_original_text('forest'/'MRD', 'G').
card_image_name('forest'/'MRD', 'forest3').
card_uid('forest'/'MRD', 'MRD:Forest:forest3').
card_rarity('forest'/'MRD', 'Basic Land').
card_artist('forest'/'MRD', 'Martina Pilcerova').
card_number('forest'/'MRD', '305').
card_multiverse_id('forest'/'MRD', '48423').

card_in_set('forest', 'MRD').
card_original_type('forest'/'MRD', 'Basic Land — Forest').
card_original_text('forest'/'MRD', 'G').
card_image_name('forest'/'MRD', 'forest4').
card_uid('forest'/'MRD', 'MRD:Forest:forest4').
card_rarity('forest'/'MRD', 'Basic Land').
card_artist('forest'/'MRD', 'John Avon').
card_number('forest'/'MRD', '306').
card_multiverse_id('forest'/'MRD', '48424').

card_in_set('forge armor', 'MRD').
card_original_type('forge armor'/'MRD', 'Instant').
card_original_text('forge armor'/'MRD', 'As an additional cost to play Forge Armor, sacrifice an artifact.\nPut X +1/+1 counters on target creature, where X is the sacrificed artifact\'s converted mana cost.').
card_first_print('forge armor', 'MRD').
card_image_name('forge armor'/'MRD', 'forge armor').
card_uid('forge armor'/'MRD', 'MRD:Forge Armor:forge armor').
card_rarity('forge armor'/'MRD', 'Uncommon').
card_artist('forge armor'/'MRD', 'Tony Szczudlo').
card_number('forge armor'/'MRD', '92').
card_multiverse_id('forge armor'/'MRD', '46577').

card_in_set('fractured loyalty', 'MRD').
card_original_type('fractured loyalty'/'MRD', 'Enchant Creature').
card_original_text('fractured loyalty'/'MRD', 'Whenever enchanted creature becomes the target of a spell or ability, that spell or ability\'s controller gains control of enchanted creature. (This effect doesn\'t end at end of turn.)').
card_first_print('fractured loyalty', 'MRD').
card_image_name('fractured loyalty'/'MRD', 'fractured loyalty').
card_uid('fractured loyalty'/'MRD', 'MRD:Fractured Loyalty:fractured loyalty').
card_rarity('fractured loyalty'/'MRD', 'Uncommon').
card_artist('fractured loyalty'/'MRD', 'Greg Staples').
card_number('fractured loyalty'/'MRD', '93').
card_multiverse_id('fractured loyalty'/'MRD', '48390').

card_in_set('frogmite', 'MRD').
card_original_type('frogmite'/'MRD', 'Artifact Creature').
card_original_text('frogmite'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)').
card_first_print('frogmite', 'MRD').
card_image_name('frogmite'/'MRD', 'frogmite').
card_uid('frogmite'/'MRD', 'MRD:Frogmite:frogmite').
card_rarity('frogmite'/'MRD', 'Common').
card_artist('frogmite'/'MRD', 'Terese Nielsen').
card_number('frogmite'/'MRD', '172').
card_flavor_text('frogmite'/'MRD', 'At first, vedalken observers thought blinkmoths naturally avoided certain places. Then they realized those places were frogmite feeding grounds.').
card_multiverse_id('frogmite'/'MRD', '49429').

card_in_set('galvanic key', 'MRD').
card_original_type('galvanic key'/'MRD', 'Artifact').
card_original_text('galvanic key'/'MRD', 'You may play Galvanic Key any time you could play an instant.\n{3}, {T}: Untap target artifact.').
card_first_print('galvanic key', 'MRD').
card_image_name('galvanic key'/'MRD', 'galvanic key').
card_uid('galvanic key'/'MRD', 'MRD:Galvanic Key:galvanic key').
card_rarity('galvanic key'/'MRD', 'Common').
card_artist('galvanic key'/'MRD', 'Tony Szczudlo').
card_number('galvanic key'/'MRD', '173').
card_flavor_text('galvanic key'/'MRD', 'A solution in search of a problem.').
card_multiverse_id('galvanic key'/'MRD', '48399').

card_in_set('gate to the æther', 'MRD').
card_original_type('gate to the æther'/'MRD', 'Artifact').
card_original_text('gate to the æther'/'MRD', 'At the beginning of each player\'s upkeep, that player reveals the top card of his or her library. If it\'s an artifact, creature, enchantment, or land card, the player may put it into play.').
card_first_print('gate to the æther', 'MRD').
card_image_name('gate to the æther'/'MRD', 'gate to the aether').
card_uid('gate to the æther'/'MRD', 'MRD:Gate to the Æther:gate to the aether').
card_rarity('gate to the æther'/'MRD', 'Rare').
card_artist('gate to the æther'/'MRD', 'Pete Venters').
card_number('gate to the æther'/'MRD', '174').
card_multiverse_id('gate to the æther'/'MRD', '46721').

card_in_set('gilded lotus', 'MRD').
card_original_type('gilded lotus'/'MRD', 'Artifact').
card_original_text('gilded lotus'/'MRD', '{T}: Add three mana of any one color to your mana pool.').
card_first_print('gilded lotus', 'MRD').
card_image_name('gilded lotus'/'MRD', 'gilded lotus').
card_uid('gilded lotus'/'MRD', 'MRD:Gilded Lotus:gilded lotus').
card_rarity('gilded lotus'/'MRD', 'Rare').
card_artist('gilded lotus'/'MRD', 'Martina Pilcerova').
card_number('gilded lotus'/'MRD', '175').
card_flavor_text('gilded lotus'/'MRD', 'Over such beauty, wars are fought. With such power, wars are won.').
card_multiverse_id('gilded lotus'/'MRD', '48189').

card_in_set('glimmervoid', 'MRD').
card_original_type('glimmervoid'/'MRD', 'Land').
card_original_text('glimmervoid'/'MRD', 'At end of turn, if you control no artifacts, sacrifice Glimmervoid.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('glimmervoid', 'MRD').
card_image_name('glimmervoid'/'MRD', 'glimmervoid').
card_uid('glimmervoid'/'MRD', 'MRD:Glimmervoid:glimmervoid').
card_rarity('glimmervoid'/'MRD', 'Rare').
card_artist('glimmervoid'/'MRD', 'Lars Grant-West').
card_number('glimmervoid'/'MRD', '281').
card_flavor_text('glimmervoid'/'MRD', 'An empty canvas holds infinite possibilities.').
card_multiverse_id('glimmervoid'/'MRD', '48132').

card_in_set('glissa sunseeker', 'MRD').
card_original_type('glissa sunseeker'/'MRD', 'Creature — Elf Legend').
card_original_text('glissa sunseeker'/'MRD', 'First strike\n{T}: Destroy target artifact if its converted mana cost is equal to the amount of mana in your mana pool.').
card_first_print('glissa sunseeker', 'MRD').
card_image_name('glissa sunseeker'/'MRD', 'glissa sunseeker').
card_uid('glissa sunseeker'/'MRD', 'MRD:Glissa Sunseeker:glissa sunseeker').
card_rarity('glissa sunseeker'/'MRD', 'Rare').
card_artist('glissa sunseeker'/'MRD', 'Brom').
card_number('glissa sunseeker'/'MRD', '120').
card_flavor_text('glissa sunseeker'/'MRD', '\"There\'s a secret at the heart of this world, and I will unlock it.\"').
card_multiverse_id('glissa sunseeker'/'MRD', '48386').

card_in_set('goblin charbelcher', 'MRD').
card_original_type('goblin charbelcher'/'MRD', 'Artifact').
card_original_text('goblin charbelcher'/'MRD', '{3}, {T}: Reveal cards from the top of your library until you reveal a land card. Goblin Charbelcher deals damage equal to the number of nonland cards revealed this way to target creature or player. If the revealed land card was a Mountain, Goblin Charbelcher deals double that damage instead. Put the revealed cards on the bottom of your library in any order.').
card_first_print('goblin charbelcher', 'MRD').
card_image_name('goblin charbelcher'/'MRD', 'goblin charbelcher').
card_uid('goblin charbelcher'/'MRD', 'MRD:Goblin Charbelcher:goblin charbelcher').
card_rarity('goblin charbelcher'/'MRD', 'Rare').
card_artist('goblin charbelcher'/'MRD', 'Stephen Tappin').
card_number('goblin charbelcher'/'MRD', '176').
card_multiverse_id('goblin charbelcher'/'MRD', '49771').

card_in_set('goblin dirigible', 'MRD').
card_original_type('goblin dirigible'/'MRD', 'Artifact Creature').
card_original_text('goblin dirigible'/'MRD', 'Flying\nGoblin Dirigible doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {4}. If you do, untap Goblin Dirigible.').
card_first_print('goblin dirigible', 'MRD').
card_image_name('goblin dirigible'/'MRD', 'goblin dirigible').
card_uid('goblin dirigible'/'MRD', 'MRD:Goblin Dirigible:goblin dirigible').
card_rarity('goblin dirigible'/'MRD', 'Uncommon').
card_artist('goblin dirigible'/'MRD', 'Michael Sutfin').
card_number('goblin dirigible'/'MRD', '177').
card_multiverse_id('goblin dirigible'/'MRD', '48077').

card_in_set('goblin replica', 'MRD').
card_original_type('goblin replica'/'MRD', 'Artifact Creature — Goblin').
card_original_text('goblin replica'/'MRD', '{3}{R}, Sacrifice Goblin Replica: Destroy target artifact.').
card_first_print('goblin replica', 'MRD').
card_image_name('goblin replica'/'MRD', 'goblin replica').
card_uid('goblin replica'/'MRD', 'MRD:Goblin Replica:goblin replica').
card_rarity('goblin replica'/'MRD', 'Common').
card_artist('goblin replica'/'MRD', 'Carl Critchlow').
card_number('goblin replica'/'MRD', '178').
card_flavor_text('goblin replica'/'MRD', 'It destroys with unthinking glee.').
card_multiverse_id('goblin replica'/'MRD', '46049').

card_in_set('goblin striker', 'MRD').
card_original_type('goblin striker'/'MRD', 'Creature — Goblin Berserker').
card_original_text('goblin striker'/'MRD', 'First strike, haste').
card_first_print('goblin striker', 'MRD').
card_image_name('goblin striker'/'MRD', 'goblin striker').
card_uid('goblin striker'/'MRD', 'MRD:Goblin Striker:goblin striker').
card_rarity('goblin striker'/'MRD', 'Common').
card_artist('goblin striker'/'MRD', 'Kevin Dobler').
card_number('goblin striker'/'MRD', '94').
card_flavor_text('goblin striker'/'MRD', 'There\'s no word in the goblin language for \"strategy.\" Then again, there\'s no word in the goblin language for \"word.\"').
card_multiverse_id('goblin striker'/'MRD', '48592').

card_in_set('goblin war wagon', 'MRD').
card_original_type('goblin war wagon'/'MRD', 'Artifact Creature').
card_original_text('goblin war wagon'/'MRD', 'Goblin War Wagon doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {2}. If you do, untap Goblin War Wagon.').
card_first_print('goblin war wagon', 'MRD').
card_image_name('goblin war wagon'/'MRD', 'goblin war wagon').
card_uid('goblin war wagon'/'MRD', 'MRD:Goblin War Wagon:goblin war wagon').
card_rarity('goblin war wagon'/'MRD', 'Common').
card_artist('goblin war wagon'/'MRD', 'Doug Chaffee').
card_number('goblin war wagon'/'MRD', '179').
card_multiverse_id('goblin war wagon'/'MRD', '46044').

card_in_set('gold myr', 'MRD').
card_original_type('gold myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('gold myr'/'MRD', '{T}: Add {W} to your mana pool.').
card_first_print('gold myr', 'MRD').
card_image_name('gold myr'/'MRD', 'gold myr').
card_uid('gold myr'/'MRD', 'MRD:Gold Myr:gold myr').
card_rarity('gold myr'/'MRD', 'Common').
card_artist('gold myr'/'MRD', 'Kev Walker').
card_number('gold myr'/'MRD', '180').
card_flavor_text('gold myr'/'MRD', 'The leonin thought of the myr as omens, never imagining the sinister fate they foretold.').
card_multiverse_id('gold myr'/'MRD', '46024').

card_in_set('golem-skin gauntlets', 'MRD').
card_original_type('golem-skin gauntlets'/'MRD', 'Artifact — Equipment').
card_original_text('golem-skin gauntlets'/'MRD', 'Equipped creature gets +1/+0 for each Equipment attached to it.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('golem-skin gauntlets', 'MRD').
card_image_name('golem-skin gauntlets'/'MRD', 'golem-skin gauntlets').
card_uid('golem-skin gauntlets'/'MRD', 'MRD:Golem-Skin Gauntlets:golem-skin gauntlets').
card_rarity('golem-skin gauntlets'/'MRD', 'Uncommon').
card_artist('golem-skin gauntlets'/'MRD', 'Alan Pollack').
card_number('golem-skin gauntlets'/'MRD', '181').
card_multiverse_id('golem-skin gauntlets'/'MRD', '46061').

card_in_set('grab the reins', 'MRD').
card_original_type('grab the reins'/'MRD', 'Instant').
card_original_text('grab the reins'/'MRD', 'Choose one Until end of turn, you gain control of target creature and it gains haste; or sacrifice a creature, then Grab the Reins deals damage equal to that creature\'s power to target creature or player.\nEntwine {2}{R} (Choose both if you pay the entwine cost.)').
card_first_print('grab the reins', 'MRD').
card_image_name('grab the reins'/'MRD', 'grab the reins').
card_uid('grab the reins'/'MRD', 'MRD:Grab the Reins:grab the reins').
card_rarity('grab the reins'/'MRD', 'Uncommon').
card_artist('grab the reins'/'MRD', 'Michael Sutfin').
card_number('grab the reins'/'MRD', '95').
card_multiverse_id('grab the reins'/'MRD', '49435').

card_in_set('granite shard', 'MRD').
card_original_type('granite shard'/'MRD', 'Artifact').
card_original_text('granite shard'/'MRD', '{3}, {T} or {R}, {T}: Granite Shard deals 1 damage to target creature or player.').
card_first_print('granite shard', 'MRD').
card_image_name('granite shard'/'MRD', 'granite shard').
card_uid('granite shard'/'MRD', 'MRD:Granite Shard:granite shard').
card_rarity('granite shard'/'MRD', 'Uncommon').
card_artist('granite shard'/'MRD', 'Doug Chaffee').
card_number('granite shard'/'MRD', '182').
card_flavor_text('granite shard'/'MRD', 'It\'s a piece of a world the goblins have never seen but would dearly like to blow up.').
card_multiverse_id('granite shard'/'MRD', '46051').

card_in_set('great furnace', 'MRD').
card_original_type('great furnace'/'MRD', 'Artifact Land').
card_original_text('great furnace'/'MRD', '(Great Furnace isn\'t a spell.)\n{T}: Add {R} to your mana pool.').
card_first_print('great furnace', 'MRD').
card_image_name('great furnace'/'MRD', 'great furnace').
card_uid('great furnace'/'MRD', 'MRD:Great Furnace:great furnace').
card_rarity('great furnace'/'MRD', 'Common').
card_artist('great furnace'/'MRD', 'Rob Alexander').
card_number('great furnace'/'MRD', '282').
card_flavor_text('great furnace'/'MRD', 'Kuldotha, wellspring of molten metal, temple of the goblin horde.').
card_multiverse_id('great furnace'/'MRD', '46066').

card_in_set('grid monitor', 'MRD').
card_original_type('grid monitor'/'MRD', 'Artifact Creature').
card_original_text('grid monitor'/'MRD', 'You can\'t play creature spells.').
card_first_print('grid monitor', 'MRD').
card_image_name('grid monitor'/'MRD', 'grid monitor').
card_uid('grid monitor'/'MRD', 'MRD:Grid Monitor:grid monitor').
card_rarity('grid monitor'/'MRD', 'Rare').
card_artist('grid monitor'/'MRD', 'Arnie Swekel').
card_number('grid monitor'/'MRD', '183').
card_flavor_text('grid monitor'/'MRD', 'The vedalken protect the Knowledge Pool at any cost.').
card_multiverse_id('grid monitor'/'MRD', '48178').

card_in_set('grim reminder', 'MRD').
card_original_type('grim reminder'/'MRD', 'Instant').
card_original_text('grim reminder'/'MRD', 'Search your library for a nonland card and reveal it. Each opponent who played a card this turn with the same name as that card loses 6 life. Then shuffle the revealed card back into your library. \n{B}{B}: Return Grim Reminder from your graveyard to your hand. Play this ability only during your upkeep.').
card_first_print('grim reminder', 'MRD').
card_image_name('grim reminder'/'MRD', 'grim reminder').
card_uid('grim reminder'/'MRD', 'MRD:Grim Reminder:grim reminder').
card_rarity('grim reminder'/'MRD', 'Rare').
card_artist('grim reminder'/'MRD', 'Wayne England').
card_number('grim reminder'/'MRD', '66').
card_multiverse_id('grim reminder'/'MRD', '49081').

card_in_set('groffskithur', 'MRD').
card_original_type('groffskithur'/'MRD', 'Creature — Beast').
card_original_text('groffskithur'/'MRD', 'Whenever Groffskithur becomes blocked, you may return target card named Groffskithur from your graveyard to your hand.').
card_first_print('groffskithur', 'MRD').
card_image_name('groffskithur'/'MRD', 'groffskithur').
card_uid('groffskithur'/'MRD', 'MRD:Groffskithur:groffskithur').
card_rarity('groffskithur'/'MRD', 'Common').
card_artist('groffskithur'/'MRD', 'John Matson').
card_number('groffskithur'/'MRD', '121').
card_flavor_text('groffskithur'/'MRD', 'It growls not to threaten, but to summon.').
card_multiverse_id('groffskithur'/'MRD', '48600').

card_in_set('heartwood shard', 'MRD').
card_original_type('heartwood shard'/'MRD', 'Artifact').
card_original_text('heartwood shard'/'MRD', '{3}, {T} or {G}, {T}: Target creature gains trample until end of turn.').
card_first_print('heartwood shard', 'MRD').
card_image_name('heartwood shard'/'MRD', 'heartwood shard').
card_uid('heartwood shard'/'MRD', 'MRD:Heartwood Shard:heartwood shard').
card_rarity('heartwood shard'/'MRD', 'Uncommon').
card_artist('heartwood shard'/'MRD', 'Doug Chaffee').
card_number('heartwood shard'/'MRD', '184').
card_flavor_text('heartwood shard'/'MRD', 'Like all other relics, it was left on the Radix by the elves to be destroyed. Unlike all other relics, it persisted.').
card_multiverse_id('heartwood shard'/'MRD', '46059').

card_in_set('hematite golem', 'MRD').
card_original_type('hematite golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('hematite golem'/'MRD', '{1}{R}: Hematite Golem gets +2/+0 until end of turn.').
card_first_print('hematite golem', 'MRD').
card_image_name('hematite golem'/'MRD', 'hematite golem').
card_uid('hematite golem'/'MRD', 'MRD:Hematite Golem:hematite golem').
card_rarity('hematite golem'/'MRD', 'Common').
card_artist('hematite golem'/'MRD', 'Paolo Parente').
card_number('hematite golem'/'MRD', '185').
card_flavor_text('hematite golem'/'MRD', 'Centuries before the first peaks of the Oxidda Chain rewrote the laws of magnetism, the golems patrolled Mirrodin\'s featureless surface unhindered.').
card_multiverse_id('hematite golem'/'MRD', '48055').

card_in_set('hum of the radix', 'MRD').
card_original_type('hum of the radix'/'MRD', 'Enchantment').
card_original_text('hum of the radix'/'MRD', 'Each artifact spell costs {1} more to play for each artifact its controller controls.').
card_first_print('hum of the radix', 'MRD').
card_image_name('hum of the radix'/'MRD', 'hum of the radix').
card_uid('hum of the radix'/'MRD', 'MRD:Hum of the Radix:hum of the radix').
card_rarity('hum of the radix'/'MRD', 'Rare').
card_artist('hum of the radix'/'MRD', 'John Avon').
card_number('hum of the radix'/'MRD', '122').
card_flavor_text('hum of the radix'/'MRD', 'The elves learned long ago that anything left here slowly vanishes. Now it is a sacred site where the dead are laid to rest and where unnatural magic is erased forever.').
card_multiverse_id('hum of the radix'/'MRD', '48082').

card_in_set('icy manipulator', 'MRD').
card_original_type('icy manipulator'/'MRD', 'Artifact').
card_original_text('icy manipulator'/'MRD', '{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'MRD', 'icy manipulator').
card_uid('icy manipulator'/'MRD', 'MRD:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'MRD', 'Uncommon').
card_artist('icy manipulator'/'MRD', 'Mark Zug').
card_number('icy manipulator'/'MRD', '186').
card_flavor_text('icy manipulator'/'MRD', 'A model of Mirrodin in both shape and spirit.').
card_multiverse_id('icy manipulator'/'MRD', '46728').

card_in_set('incite war', 'MRD').
card_original_type('incite war'/'MRD', 'Instant').
card_original_text('incite war'/'MRD', 'Choose one Creatures target player controls attack this turn if able; or creatures you control gain first strike until end of turn.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_first_print('incite war', 'MRD').
card_image_name('incite war'/'MRD', 'incite war').
card_uid('incite war'/'MRD', 'MRD:Incite War:incite war').
card_rarity('incite war'/'MRD', 'Common').
card_artist('incite war'/'MRD', 'Alex Horley-Orlandelli').
card_number('incite war'/'MRD', '96').
card_multiverse_id('incite war'/'MRD', '48138').

card_in_set('inertia bubble', 'MRD').
card_original_type('inertia bubble'/'MRD', 'Enchant Artifact').
card_original_text('inertia bubble'/'MRD', 'Enchanted artifact doesn\'t untap during its controller\'s untap step.').
card_first_print('inertia bubble', 'MRD').
card_image_name('inertia bubble'/'MRD', 'inertia bubble').
card_uid('inertia bubble'/'MRD', 'MRD:Inertia Bubble:inertia bubble').
card_rarity('inertia bubble'/'MRD', 'Common').
card_artist('inertia bubble'/'MRD', 'Hugh Jamieson').
card_number('inertia bubble'/'MRD', '37').
card_flavor_text('inertia bubble'/'MRD', '\"I wouldn\'t want you to hurt yourself.\"\n—Bruenna, Neurok leader').
card_multiverse_id('inertia bubble'/'MRD', '49443').

card_in_set('iron myr', 'MRD').
card_original_type('iron myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('iron myr'/'MRD', '{T}: Add {R} to your mana pool.').
card_first_print('iron myr', 'MRD').
card_image_name('iron myr'/'MRD', 'iron myr').
card_uid('iron myr'/'MRD', 'MRD:Iron Myr:iron myr').
card_rarity('iron myr'/'MRD', 'Common').
card_artist('iron myr'/'MRD', 'Kev Walker').
card_number('iron myr'/'MRD', '187').
card_flavor_text('iron myr'/'MRD', 'The goblins didn\'t think of the myr at all, which allowed the myr to observe everywhere unhindered.').
card_multiverse_id('iron myr'/'MRD', '46048').

card_in_set('irradiate', 'MRD').
card_original_type('irradiate'/'MRD', 'Instant').
card_original_text('irradiate'/'MRD', 'Target creature gets -1/-1 until end of turn for each artifact you control.').
card_first_print('irradiate', 'MRD').
card_image_name('irradiate'/'MRD', 'irradiate').
card_uid('irradiate'/'MRD', 'MRD:Irradiate:irradiate').
card_rarity('irradiate'/'MRD', 'Common').
card_artist('irradiate'/'MRD', 'Dave Dorman').
card_number('irradiate'/'MRD', '67').
card_flavor_text('irradiate'/'MRD', 'The blast ignores the cage of metal but devours the flesh inside.').
card_multiverse_id('irradiate'/'MRD', '48585').

card_in_set('island', 'MRD').
card_original_type('island'/'MRD', 'Basic Land — Island').
card_original_text('island'/'MRD', 'U').
card_image_name('island'/'MRD', 'island1').
card_uid('island'/'MRD', 'MRD:Island:island1').
card_rarity('island'/'MRD', 'Basic Land').
card_artist('island'/'MRD', 'Mark Tedin').
card_number('island'/'MRD', '291').
card_multiverse_id('island'/'MRD', '48410').

card_in_set('island', 'MRD').
card_original_type('island'/'MRD', 'Basic Land — Island').
card_original_text('island'/'MRD', 'U').
card_image_name('island'/'MRD', 'island2').
card_uid('island'/'MRD', 'MRD:Island:island2').
card_rarity('island'/'MRD', 'Basic Land').
card_artist('island'/'MRD', 'Rob Alexander').
card_number('island'/'MRD', '292').
card_multiverse_id('island'/'MRD', '48411').

card_in_set('island', 'MRD').
card_original_type('island'/'MRD', 'Basic Land — Island').
card_original_text('island'/'MRD', 'U').
card_image_name('island'/'MRD', 'island3').
card_uid('island'/'MRD', 'MRD:Island:island3').
card_rarity('island'/'MRD', 'Basic Land').
card_artist('island'/'MRD', 'Martina Pilcerova').
card_number('island'/'MRD', '293').
card_multiverse_id('island'/'MRD', '48412').

card_in_set('island', 'MRD').
card_original_type('island'/'MRD', 'Basic Land — Island').
card_original_text('island'/'MRD', 'U').
card_image_name('island'/'MRD', 'island4').
card_uid('island'/'MRD', 'MRD:Island:island4').
card_rarity('island'/'MRD', 'Basic Land').
card_artist('island'/'MRD', 'John Avon').
card_number('island'/'MRD', '294').
card_multiverse_id('island'/'MRD', '48413').

card_in_set('isochron scepter', 'MRD').
card_original_type('isochron scepter'/'MRD', 'Artifact').
card_original_text('isochron scepter'/'MRD', 'Imprint When Isochron Scepter comes into play, you may remove an instant card with converted mana cost 2 or less in your hand from the game. (The removed card is imprinted on this artifact.)\n{2}, {T}: You may copy the imprinted instant card and play the copy without paying its mana cost.').
card_image_name('isochron scepter'/'MRD', 'isochron scepter').
card_uid('isochron scepter'/'MRD', 'MRD:Isochron Scepter:isochron scepter').
card_rarity('isochron scepter'/'MRD', 'Uncommon').
card_artist('isochron scepter'/'MRD', 'Mark Harrison').
card_number('isochron scepter'/'MRD', '188').
card_multiverse_id('isochron scepter'/'MRD', '46741').

card_in_set('jinxed choker', 'MRD').
card_original_type('jinxed choker'/'MRD', 'Artifact').
card_original_text('jinxed choker'/'MRD', 'At the end of your turn, target opponent gains control of Jinxed Choker and puts a charge counter on it.\nAt the beginning of your upkeep, Jinxed Choker deals damage to you equal to the number of charge counters on it.\n{3}: Put a charge counter on Jinxed Choker or remove one from it.').
card_first_print('jinxed choker', 'MRD').
card_image_name('jinxed choker'/'MRD', 'jinxed choker').
card_uid('jinxed choker'/'MRD', 'MRD:Jinxed Choker:jinxed choker').
card_rarity('jinxed choker'/'MRD', 'Rare').
card_artist('jinxed choker'/'MRD', 'Mike Dringenberg').
card_number('jinxed choker'/'MRD', '189').
card_multiverse_id('jinxed choker'/'MRD', '46760').

card_in_set('journey of discovery', 'MRD').
card_original_type('journey of discovery'/'MRD', 'Sorcery').
card_original_text('journey of discovery'/'MRD', 'Choose one Search your library for up to two basic land cards, reveal them, put them into your hand, then shuffle your library; or you may play up to two additional lands this turn.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_first_print('journey of discovery', 'MRD').
card_image_name('journey of discovery'/'MRD', 'journey of discovery').
card_uid('journey of discovery'/'MRD', 'MRD:Journey of Discovery:journey of discovery').
card_rarity('journey of discovery'/'MRD', 'Common').
card_artist('journey of discovery'/'MRD', 'John Matson').
card_number('journey of discovery'/'MRD', '123').
card_multiverse_id('journey of discovery'/'MRD', '49437').

card_in_set('krark\'s thumb', 'MRD').
card_original_type('krark\'s thumb'/'MRD', 'Legendary Artifact').
card_original_text('krark\'s thumb'/'MRD', 'If you would flip a coin, instead flip two coins and ignore one.').
card_first_print('krark\'s thumb', 'MRD').
card_image_name('krark\'s thumb'/'MRD', 'krark\'s thumb').
card_uid('krark\'s thumb'/'MRD', 'MRD:Krark\'s Thumb:krark\'s thumb').
card_rarity('krark\'s thumb'/'MRD', 'Rare').
card_artist('krark\'s thumb'/'MRD', 'Ron Spencer').
card_number('krark\'s thumb'/'MRD', '190').
card_flavor_text('krark\'s thumb'/'MRD', '\"I can think of one goblin it ain\'t so lucky for.\"\n—Slobad, goblin tinkerer').
card_multiverse_id('krark\'s thumb'/'MRD', '48923').

card_in_set('krark-clan grunt', 'MRD').
card_original_type('krark-clan grunt'/'MRD', 'Creature — Goblin Warrior').
card_original_text('krark-clan grunt'/'MRD', 'Sacrifice an artifact: Krark-Clan Grunt gets +1/+0 and gains first strike until end of turn.').
card_first_print('krark-clan grunt', 'MRD').
card_image_name('krark-clan grunt'/'MRD', 'krark-clan grunt').
card_uid('krark-clan grunt'/'MRD', 'MRD:Krark-Clan Grunt:krark-clan grunt').
card_rarity('krark-clan grunt'/'MRD', 'Common').
card_artist('krark-clan grunt'/'MRD', 'Thomas M. Baxa').
card_number('krark-clan grunt'/'MRD', '97').
card_flavor_text('krark-clan grunt'/'MRD', 'The more weapons a goblin breaks in battle, the more respected he becomes.').
card_multiverse_id('krark-clan grunt'/'MRD', '45992').

card_in_set('krark-clan shaman', 'MRD').
card_original_type('krark-clan shaman'/'MRD', 'Creature — Goblin Shaman').
card_original_text('krark-clan shaman'/'MRD', 'Sacrifice an artifact: Krark-Clan Shaman deals 1 damage to each creature without flying.').
card_first_print('krark-clan shaman', 'MRD').
card_image_name('krark-clan shaman'/'MRD', 'krark-clan shaman').
card_uid('krark-clan shaman'/'MRD', 'MRD:Krark-Clan Shaman:krark-clan shaman').
card_rarity('krark-clan shaman'/'MRD', 'Common').
card_artist('krark-clan shaman'/'MRD', 'Thomas M. Baxa').
card_number('krark-clan shaman'/'MRD', '98').
card_flavor_text('krark-clan shaman'/'MRD', '\"What do you mean, we\'re out of stuff to melt down? Give me your leg.\"').
card_multiverse_id('krark-clan shaman'/'MRD', '45994').

card_in_set('leaden myr', 'MRD').
card_original_type('leaden myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('leaden myr'/'MRD', '{T}: Add {B} to your mana pool.').
card_first_print('leaden myr', 'MRD').
card_image_name('leaden myr'/'MRD', 'leaden myr').
card_uid('leaden myr'/'MRD', 'MRD:Leaden Myr:leaden myr').
card_rarity('leaden myr'/'MRD', 'Common').
card_artist('leaden myr'/'MRD', 'Kev Walker').
card_number('leaden myr'/'MRD', '191').
card_flavor_text('leaden myr'/'MRD', 'The Moriok saw the myr as fellow scavengers, never knowing just who the myr were scavenging for.').
card_multiverse_id('leaden myr'/'MRD', '46040').

card_in_set('leonin abunas', 'MRD').
card_original_type('leonin abunas'/'MRD', 'Creature — Cat Cleric').
card_original_text('leonin abunas'/'MRD', 'Artifacts you control can\'t be the targets of spells or abilities your opponents control.').
card_first_print('leonin abunas', 'MRD').
card_image_name('leonin abunas'/'MRD', 'leonin abunas').
card_uid('leonin abunas'/'MRD', 'MRD:Leonin Abunas:leonin abunas').
card_rarity('leonin abunas'/'MRD', 'Rare').
card_artist('leonin abunas'/'MRD', 'Darrell Riche').
card_number('leonin abunas'/'MRD', '8').
card_flavor_text('leonin abunas'/'MRD', 'Only leonin clerics who can survive the Razor Fields for one turning of the suns can stand in the Cave of Light.').
card_multiverse_id('leonin abunas'/'MRD', '46550').

card_in_set('leonin bladetrap', 'MRD').
card_original_type('leonin bladetrap'/'MRD', 'Artifact').
card_original_text('leonin bladetrap'/'MRD', 'You may play Leonin Bladetrap any time you could play an instant.\n{2}, Sacrifice Leonin Bladetrap: Leonin Bladetrap deals 2 damage to each attacking creature without flying.').
card_first_print('leonin bladetrap', 'MRD').
card_image_name('leonin bladetrap'/'MRD', 'leonin bladetrap').
card_uid('leonin bladetrap'/'MRD', 'MRD:Leonin Bladetrap:leonin bladetrap').
card_rarity('leonin bladetrap'/'MRD', 'Uncommon').
card_artist('leonin bladetrap'/'MRD', 'Randy Gallegos').
card_number('leonin bladetrap'/'MRD', '192').
card_multiverse_id('leonin bladetrap'/'MRD', '48378').

card_in_set('leonin den-guard', 'MRD').
card_original_type('leonin den-guard'/'MRD', 'Creature — Cat Soldier').
card_original_text('leonin den-guard'/'MRD', 'As long as Leonin Den-Guard is equipped, it gets +1/+1 and attacking doesn\'t cause it to tap.').
card_first_print('leonin den-guard', 'MRD').
card_image_name('leonin den-guard'/'MRD', 'leonin den-guard').
card_uid('leonin den-guard'/'MRD', 'MRD:Leonin Den-Guard:leonin den-guard').
card_rarity('leonin den-guard'/'MRD', 'Common').
card_artist('leonin den-guard'/'MRD', 'Todd Lockwood').
card_number('leonin den-guard'/'MRD', '9').
card_flavor_text('leonin den-guard'/'MRD', 'No one under the four suns can elude the watchful eye of the den-guard.').
card_multiverse_id('leonin den-guard'/'MRD', '45958').

card_in_set('leonin elder', 'MRD').
card_original_type('leonin elder'/'MRD', 'Creature — Cat Cleric').
card_original_text('leonin elder'/'MRD', 'Whenever an artifact comes into play, you may gain 1 life.').
card_first_print('leonin elder', 'MRD').
card_image_name('leonin elder'/'MRD', 'leonin elder').
card_uid('leonin elder'/'MRD', 'MRD:Leonin Elder:leonin elder').
card_rarity('leonin elder'/'MRD', 'Common').
card_artist('leonin elder'/'MRD', 'Todd Lockwood').
card_number('leonin elder'/'MRD', '10').
card_flavor_text('leonin elder'/'MRD', '\"The wisdom of the elders is just as much a weapon as a sword or spear. We must learn to wield it.\"\n—Ushanti, leonin seer').
card_multiverse_id('leonin elder'/'MRD', '46072').

card_in_set('leonin scimitar', 'MRD').
card_original_type('leonin scimitar'/'MRD', 'Artifact — Equipment').
card_original_text('leonin scimitar'/'MRD', 'Equipped creature gets +1/+1.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('leonin scimitar', 'MRD').
card_image_name('leonin scimitar'/'MRD', 'leonin scimitar').
card_uid('leonin scimitar'/'MRD', 'MRD:Leonin Scimitar:leonin scimitar').
card_rarity('leonin scimitar'/'MRD', 'Common').
card_artist('leonin scimitar'/'MRD', 'Doug Chaffee').
card_number('leonin scimitar'/'MRD', '193').
card_multiverse_id('leonin scimitar'/'MRD', '46029').

card_in_set('leonin skyhunter', 'MRD').
card_original_type('leonin skyhunter'/'MRD', 'Creature — Cat Knight').
card_original_text('leonin skyhunter'/'MRD', 'Flying').
card_first_print('leonin skyhunter', 'MRD').
card_image_name('leonin skyhunter'/'MRD', 'leonin skyhunter').
card_uid('leonin skyhunter'/'MRD', 'MRD:Leonin Skyhunter:leonin skyhunter').
card_rarity('leonin skyhunter'/'MRD', 'Uncommon').
card_artist('leonin skyhunter'/'MRD', 'Kev Walker').
card_number('leonin skyhunter'/'MRD', '11').
card_flavor_text('leonin skyhunter'/'MRD', 'The skyhunters were born when the first leonin gazed at the heavens and wished to hunt the birds overhead.').
card_multiverse_id('leonin skyhunter'/'MRD', '45960').

card_in_set('leonin sun standard', 'MRD').
card_original_type('leonin sun standard'/'MRD', 'Artifact').
card_original_text('leonin sun standard'/'MRD', '{1}{W}: Creatures you control get +1/+1 until end of turn.').
card_first_print('leonin sun standard', 'MRD').
card_image_name('leonin sun standard'/'MRD', 'leonin sun standard').
card_uid('leonin sun standard'/'MRD', 'MRD:Leonin Sun Standard:leonin sun standard').
card_rarity('leonin sun standard'/'MRD', 'Rare').
card_artist('leonin sun standard'/'MRD', 'Jim Nelson').
card_number('leonin sun standard'/'MRD', '194').
card_flavor_text('leonin sun standard'/'MRD', 'The commander tells the troops where to go, but the standard reminds them why they\'re there.').
card_multiverse_id('leonin sun standard'/'MRD', '49768').

card_in_set('leveler', 'MRD').
card_original_type('leveler'/'MRD', 'Artifact Creature').
card_original_text('leveler'/'MRD', 'When Leveler comes into play, remove your library from the game.').
card_first_print('leveler', 'MRD').
card_image_name('leveler'/'MRD', 'leveler').
card_uid('leveler'/'MRD', 'MRD:Leveler:leveler').
card_rarity('leveler'/'MRD', 'Rare').
card_artist('leveler'/'MRD', 'Carl Critchlow').
card_number('leveler'/'MRD', '195').
card_flavor_text('leveler'/'MRD', 'Once a century, the levelers rip through every corner of Mirrodin, obeying the commands of an unseen master.').
card_multiverse_id('leveler'/'MRD', '46708').

card_in_set('liar\'s pendulum', 'MRD').
card_original_type('liar\'s pendulum'/'MRD', 'Artifact').
card_original_text('liar\'s pendulum'/'MRD', '{2}, {T}: Name a card. Target opponent guesses whether a card with that name is in your hand. You may reveal your hand. If you do and your opponent guessed wrong, draw a card.').
card_first_print('liar\'s pendulum', 'MRD').
card_image_name('liar\'s pendulum'/'MRD', 'liar\'s pendulum').
card_uid('liar\'s pendulum'/'MRD', 'MRD:Liar\'s Pendulum:liar\'s pendulum').
card_rarity('liar\'s pendulum'/'MRD', 'Rare').
card_artist('liar\'s pendulum'/'MRD', 'Christopher Moeller').
card_number('liar\'s pendulum'/'MRD', '196').
card_multiverse_id('liar\'s pendulum'/'MRD', '46733').

card_in_set('lifespark spellbomb', 'MRD').
card_original_type('lifespark spellbomb'/'MRD', 'Artifact').
card_original_text('lifespark spellbomb'/'MRD', '{G}, Sacrifice Lifespark Spellbomb: Until end of turn, target land becomes a 3/3 creature that\'s still a land.\n{1}, Sacrifice Lifespark Spellbomb: Draw a card.').
card_first_print('lifespark spellbomb', 'MRD').
card_image_name('lifespark spellbomb'/'MRD', 'lifespark spellbomb').
card_uid('lifespark spellbomb'/'MRD', 'MRD:Lifespark Spellbomb:lifespark spellbomb').
card_rarity('lifespark spellbomb'/'MRD', 'Common').
card_artist('lifespark spellbomb'/'MRD', 'Jim Nelson').
card_number('lifespark spellbomb'/'MRD', '197').
card_flavor_text('lifespark spellbomb'/'MRD', '\"Awaken that which was never asleep.\"\n—Spellbomb inscription').
card_multiverse_id('lifespark spellbomb'/'MRD', '48108').

card_in_set('lightning coils', 'MRD').
card_original_type('lightning coils'/'MRD', 'Artifact').
card_original_text('lightning coils'/'MRD', 'Whenever a nontoken creature you control is put into a graveyard from play, put a charge counter on Lightning Coils.\nAt the beginning of your upkeep, if Lightning Coils has five or more charge counters on it, remove all of them from it and put that many 3/1 red Elemental creature tokens with haste into play. Remove them from the game at end of turn.').
card_first_print('lightning coils', 'MRD').
card_image_name('lightning coils'/'MRD', 'lightning coils').
card_uid('lightning coils'/'MRD', 'MRD:Lightning Coils:lightning coils').
card_rarity('lightning coils'/'MRD', 'Rare').
card_artist('lightning coils'/'MRD', 'Brian Snõddy').
card_number('lightning coils'/'MRD', '198').
card_multiverse_id('lightning coils'/'MRD', '46109').

card_in_set('lightning greaves', 'MRD').
card_original_type('lightning greaves'/'MRD', 'Artifact — Equipment').
card_original_text('lightning greaves'/'MRD', 'Equipped creature has haste and can\'t be the target of spells or abilities.\nEquip {0} ({0}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_image_name('lightning greaves'/'MRD', 'lightning greaves').
card_uid('lightning greaves'/'MRD', 'MRD:Lightning Greaves:lightning greaves').
card_rarity('lightning greaves'/'MRD', 'Uncommon').
card_artist('lightning greaves'/'MRD', 'Jeremy Jarvis').
card_number('lightning greaves'/'MRD', '199').
card_multiverse_id('lightning greaves'/'MRD', '46021').

card_in_set('living hive', 'MRD').
card_original_type('living hive'/'MRD', 'Creature — Elemental').
card_original_text('living hive'/'MRD', 'Trample\nWhenever Living Hive deals combat damage to a player, put that many 1/1 green Insect creature tokens into play.').
card_first_print('living hive', 'MRD').
card_image_name('living hive'/'MRD', 'living hive').
card_uid('living hive'/'MRD', 'MRD:Living Hive:living hive').
card_rarity('living hive'/'MRD', 'Rare').
card_artist('living hive'/'MRD', 'Anthony S. Waters').
card_number('living hive'/'MRD', '124').
card_flavor_text('living hive'/'MRD', 'In its center is a single red ant, a queen that regulates the hive\'s movements.').
card_multiverse_id('living hive'/'MRD', '46694').

card_in_set('lodestone myr', 'MRD').
card_original_type('lodestone myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('lodestone myr'/'MRD', 'Trample\nTap an untapped artifact you control: Lodestone Myr gets +1/+1 until end of turn.').
card_first_print('lodestone myr', 'MRD').
card_image_name('lodestone myr'/'MRD', 'lodestone myr').
card_uid('lodestone myr'/'MRD', 'MRD:Lodestone Myr:lodestone myr').
card_rarity('lodestone myr'/'MRD', 'Rare').
card_artist('lodestone myr'/'MRD', 'Greg Staples').
card_number('lodestone myr'/'MRD', '200').
card_flavor_text('lodestone myr'/'MRD', 'When necessary, myr can override and control any artificial object, as can their creator.').
card_multiverse_id('lodestone myr'/'MRD', '46128').

card_in_set('looming hoverguard', 'MRD').
card_original_type('looming hoverguard'/'MRD', 'Creature — Drone').
card_original_text('looming hoverguard'/'MRD', 'Flying\nWhen Looming Hoverguard comes into play, put target artifact on top of its owner\'s library.').
card_first_print('looming hoverguard', 'MRD').
card_image_name('looming hoverguard'/'MRD', 'looming hoverguard').
card_uid('looming hoverguard'/'MRD', 'MRD:Looming Hoverguard:looming hoverguard').
card_rarity('looming hoverguard'/'MRD', 'Uncommon').
card_artist('looming hoverguard'/'MRD', 'Scott M. Fischer').
card_number('looming hoverguard'/'MRD', '38').
card_flavor_text('looming hoverguard'/'MRD', 'Although mute, hoverguards get their message across loud and clear.').
card_multiverse_id('looming hoverguard'/'MRD', '48114').

card_in_set('loxodon mender', 'MRD').
card_original_type('loxodon mender'/'MRD', 'Creature — Elephant Cleric').
card_original_text('loxodon mender'/'MRD', '{W}, {T}: Regenerate target artifact.').
card_first_print('loxodon mender', 'MRD').
card_image_name('loxodon mender'/'MRD', 'loxodon mender').
card_uid('loxodon mender'/'MRD', 'MRD:Loxodon Mender:loxodon mender').
card_rarity('loxodon mender'/'MRD', 'Common').
card_artist('loxodon mender'/'MRD', 'Heather Hudson').
card_number('loxodon mender'/'MRD', '12').
card_flavor_text('loxodon mender'/'MRD', 'The Auriok believe that in the hands of a loxodon, no weapon can be broken.').
card_multiverse_id('loxodon mender'/'MRD', '46071').

card_in_set('loxodon peacekeeper', 'MRD').
card_original_type('loxodon peacekeeper'/'MRD', 'Creature — Elephant Soldier').
card_original_text('loxodon peacekeeper'/'MRD', 'At the beginning of your upkeep, the player with the lowest life total gains control of Loxodon Peacekeeper. If two or more players are tied for lowest life total, you choose one of them, and that player gains control of Loxodon Peacekeeper.').
card_first_print('loxodon peacekeeper', 'MRD').
card_image_name('loxodon peacekeeper'/'MRD', 'loxodon peacekeeper').
card_uid('loxodon peacekeeper'/'MRD', 'MRD:Loxodon Peacekeeper:loxodon peacekeeper').
card_rarity('loxodon peacekeeper'/'MRD', 'Rare').
card_artist('loxodon peacekeeper'/'MRD', 'Michael Sutfin').
card_number('loxodon peacekeeper'/'MRD', '13').
card_multiverse_id('loxodon peacekeeper'/'MRD', '48180').

card_in_set('loxodon punisher', 'MRD').
card_original_type('loxodon punisher'/'MRD', 'Creature — Elephant Soldier').
card_original_text('loxodon punisher'/'MRD', 'Loxodon Punisher gets +2/+2 for each Equipment attached to it.').
card_first_print('loxodon punisher', 'MRD').
card_image_name('loxodon punisher'/'MRD', 'loxodon punisher').
card_uid('loxodon punisher'/'MRD', 'MRD:Loxodon Punisher:loxodon punisher').
card_rarity('loxodon punisher'/'MRD', 'Rare').
card_artist('loxodon punisher'/'MRD', 'Terese Nielsen').
card_number('loxodon punisher'/'MRD', '14').
card_flavor_text('loxodon punisher'/'MRD', 'The loxodons believe punishment comes in two steps: pain and atonement. They carry a weapon for each.').
card_multiverse_id('loxodon punisher'/'MRD', '46105').

card_in_set('loxodon warhammer', 'MRD').
card_original_type('loxodon warhammer'/'MRD', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'MRD', 'Equipped creature gets +3/+0, has trample, and has \"Whenever this creature deals damage, you gain that much life.\"\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('loxodon warhammer', 'MRD').
card_image_name('loxodon warhammer'/'MRD', 'loxodon warhammer').
card_uid('loxodon warhammer'/'MRD', 'MRD:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'MRD', 'Uncommon').
card_artist('loxodon warhammer'/'MRD', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'MRD', '201').
card_multiverse_id('loxodon warhammer'/'MRD', '49759').

card_in_set('lumengrid augur', 'MRD').
card_original_type('lumengrid augur'/'MRD', 'Creature — Vedalken Wizard').
card_original_text('lumengrid augur'/'MRD', '{1}, {T}: Target player draws a card, then discards a card from his or her hand. If that player discards an artifact card this way, untap Lumengrid Augur.').
card_first_print('lumengrid augur', 'MRD').
card_image_name('lumengrid augur'/'MRD', 'lumengrid augur').
card_uid('lumengrid augur'/'MRD', 'MRD:Lumengrid Augur:lumengrid augur').
card_rarity('lumengrid augur'/'MRD', 'Rare').
card_artist('lumengrid augur'/'MRD', 'rk post').
card_number('lumengrid augur'/'MRD', '39').
card_flavor_text('lumengrid augur'/'MRD', 'Information pumps like blood through vedalken society.').
card_multiverse_id('lumengrid augur'/'MRD', '47791').

card_in_set('lumengrid sentinel', 'MRD').
card_original_type('lumengrid sentinel'/'MRD', 'Creature — Human Wizard').
card_original_text('lumengrid sentinel'/'MRD', 'Flying\nWhenever an artifact comes into play under your control, you may tap target permanent.').
card_first_print('lumengrid sentinel', 'MRD').
card_image_name('lumengrid sentinel'/'MRD', 'lumengrid sentinel').
card_uid('lumengrid sentinel'/'MRD', 'MRD:Lumengrid Sentinel:lumengrid sentinel').
card_rarity('lumengrid sentinel'/'MRD', 'Uncommon').
card_artist('lumengrid sentinel'/'MRD', 'Scott M. Fischer').
card_number('lumengrid sentinel'/'MRD', '40').
card_flavor_text('lumengrid sentinel'/'MRD', 'The vedalken order their Neurok sentinels to watch over the shores of the Quicksilver Sea, as if they know of intruders yet to come.').
card_multiverse_id('lumengrid sentinel'/'MRD', '45972').

card_in_set('lumengrid warden', 'MRD').
card_original_type('lumengrid warden'/'MRD', 'Creature — Human Wizard').
card_original_text('lumengrid warden'/'MRD', '').
card_first_print('lumengrid warden', 'MRD').
card_image_name('lumengrid warden'/'MRD', 'lumengrid warden').
card_uid('lumengrid warden'/'MRD', 'MRD:Lumengrid Warden:lumengrid warden').
card_rarity('lumengrid warden'/'MRD', 'Common').
card_artist('lumengrid warden'/'MRD', 'Matt Thompson').
card_number('lumengrid warden'/'MRD', '41').
card_flavor_text('lumengrid warden'/'MRD', 'The Neurok, like the vedalken, are on a constant quest for knowledge. It is their currency, their trade, their life.').
card_multiverse_id('lumengrid warden'/'MRD', '48438').

card_in_set('luminous angel', 'MRD').
card_original_type('luminous angel'/'MRD', 'Creature — Angel').
card_original_text('luminous angel'/'MRD', 'Flying\nAt the beginning of your upkeep, you may put a 1/1 white Spirit creature token with flying into play.').
card_first_print('luminous angel', 'MRD').
card_image_name('luminous angel'/'MRD', 'luminous angel').
card_uid('luminous angel'/'MRD', 'MRD:Luminous Angel:luminous angel').
card_rarity('luminous angel'/'MRD', 'Rare').
card_artist('luminous angel'/'MRD', 'Matthew D. Wilson').
card_number('luminous angel'/'MRD', '15').
card_multiverse_id('luminous angel'/'MRD', '46546').

card_in_set('malachite golem', 'MRD').
card_original_type('malachite golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('malachite golem'/'MRD', '{1}{G}: Malachite Golem gains trample until end of turn.').
card_first_print('malachite golem', 'MRD').
card_image_name('malachite golem'/'MRD', 'malachite golem').
card_uid('malachite golem'/'MRD', 'MRD:Malachite Golem:malachite golem').
card_rarity('malachite golem'/'MRD', 'Common').
card_artist('malachite golem'/'MRD', 'Paolo Parente').
card_number('malachite golem'/'MRD', '202').
card_flavor_text('malachite golem'/'MRD', 'Centuries before the first branches of the Tangle gave shelter from the suns\' cold light, the shadows of golems were Mirrodin\'s only shade.').
card_multiverse_id('malachite golem'/'MRD', '48056').

card_in_set('march of the machines', 'MRD').
card_original_type('march of the machines'/'MRD', 'Enchantment').
card_original_text('march of the machines'/'MRD', 'Each noncreature artifact is an artifact creature with power and toughness each equal to its converted mana cost. (Equipment that\'s a creature can\'t equip a creature.)').
card_first_print('march of the machines', 'MRD').
card_image_name('march of the machines'/'MRD', 'march of the machines').
card_uid('march of the machines'/'MRD', 'MRD:March of the Machines:march of the machines').
card_rarity('march of the machines'/'MRD', 'Rare').
card_artist('march of the machines'/'MRD', 'Ben Thompson').
card_number('march of the machines'/'MRD', '42').
card_multiverse_id('march of the machines'/'MRD', '46699').

card_in_set('mask of memory', 'MRD').
card_original_type('mask of memory'/'MRD', 'Artifact — Equipment').
card_original_text('mask of memory'/'MRD', 'Whenever equipped creature deals combat damage to a player, you may draw two cards. If you do, discard a card from your hand.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('mask of memory', 'MRD').
card_image_name('mask of memory'/'MRD', 'mask of memory').
card_uid('mask of memory'/'MRD', 'MRD:Mask of Memory:mask of memory').
card_rarity('mask of memory'/'MRD', 'Uncommon').
card_artist('mask of memory'/'MRD', 'Alan Pollack').
card_number('mask of memory'/'MRD', '203').
card_multiverse_id('mask of memory'/'MRD', '46153').

card_in_set('mass hysteria', 'MRD').
card_original_type('mass hysteria'/'MRD', 'Enchantment').
card_original_text('mass hysteria'/'MRD', 'All creatures have haste.').
card_first_print('mass hysteria', 'MRD').
card_image_name('mass hysteria'/'MRD', 'mass hysteria').
card_uid('mass hysteria'/'MRD', 'MRD:Mass Hysteria:mass hysteria').
card_rarity('mass hysteria'/'MRD', 'Rare').
card_artist('mass hysteria'/'MRD', 'Adam Rex').
card_number('mass hysteria'/'MRD', '99').
card_flavor_text('mass hysteria'/'MRD', 'The sooner you see the whites of their eyes, the sooner you\'ll spill the red of their blood.').
card_multiverse_id('mass hysteria'/'MRD', '48191').

card_in_set('megatog', 'MRD').
card_original_type('megatog'/'MRD', 'Creature — Atog').
card_original_text('megatog'/'MRD', 'Sacrifice an artifact: Megatog gets +3/+3 and gains trample until end of turn.').
card_first_print('megatog', 'MRD').
card_image_name('megatog'/'MRD', 'megatog').
card_uid('megatog'/'MRD', 'MRD:Megatog:megatog').
card_rarity('megatog'/'MRD', 'Rare').
card_artist('megatog'/'MRD', 'Pete Venters').
card_number('megatog'/'MRD', '100').
card_flavor_text('megatog'/'MRD', 'In an ironic bit of evolution, the megatog\'s dozens of teeth are mainly ornamental. It prefers swallowing things whole.').
card_multiverse_id('megatog'/'MRD', '46579').

card_in_set('mesmeric orb', 'MRD').
card_original_type('mesmeric orb'/'MRD', 'Artifact').
card_original_text('mesmeric orb'/'MRD', 'Whenever a permanent becomes untapped, that permanent\'s controller puts the top card of his or her library into his or her graveyard.').
card_first_print('mesmeric orb', 'MRD').
card_image_name('mesmeric orb'/'MRD', 'mesmeric orb').
card_uid('mesmeric orb'/'MRD', 'MRD:Mesmeric Orb:mesmeric orb').
card_rarity('mesmeric orb'/'MRD', 'Rare').
card_artist('mesmeric orb'/'MRD', 'David Martin').
card_number('mesmeric orb'/'MRD', '204').
card_flavor_text('mesmeric orb'/'MRD', 'A step in one direction is two steps away from another.').
card_multiverse_id('mesmeric orb'/'MRD', '30008').

card_in_set('mind\'s eye', 'MRD').
card_original_type('mind\'s eye'/'MRD', 'Artifact').
card_original_text('mind\'s eye'/'MRD', 'Whenever an opponent draws a card, you may pay {1}. If you do, draw a card.').
card_first_print('mind\'s eye', 'MRD').
card_image_name('mind\'s eye'/'MRD', 'mind\'s eye').
card_uid('mind\'s eye'/'MRD', 'MRD:Mind\'s Eye:mind\'s eye').
card_rarity('mind\'s eye'/'MRD', 'Rare').
card_artist('mind\'s eye'/'MRD', 'Edward P. Beard, Jr.').
card_number('mind\'s eye'/'MRD', '205').
card_flavor_text('mind\'s eye'/'MRD', '\"Ideas drift like petals on the wind. I have only to lift my face to the breeze.\"').
card_multiverse_id('mind\'s eye'/'MRD', '48919').

card_in_set('mindslaver', 'MRD').
card_original_type('mindslaver'/'MRD', 'Legendary Artifact').
card_original_text('mindslaver'/'MRD', '{4}, {T}, Sacrifice Mindslaver: You control target player\'s next turn. (You see all cards that player could see and make all decisions for the player. He or she doesn\'t lose life because of mana burn.)').
card_first_print('mindslaver', 'MRD').
card_image_name('mindslaver'/'MRD', 'mindslaver').
card_uid('mindslaver'/'MRD', 'MRD:Mindslaver:mindslaver').
card_rarity('mindslaver'/'MRD', 'Rare').
card_artist('mindslaver'/'MRD', 'Glen Angus').
card_number('mindslaver'/'MRD', '206').
card_multiverse_id('mindslaver'/'MRD', '46724').

card_in_set('mindstorm crown', 'MRD').
card_original_type('mindstorm crown'/'MRD', 'Artifact').
card_original_text('mindstorm crown'/'MRD', 'At the beginning of your upkeep, draw a card if you had no cards in hand at the beginning of this turn. If you had a card in hand, Mindstorm Crown deals 1 damage to you.').
card_first_print('mindstorm crown', 'MRD').
card_image_name('mindstorm crown'/'MRD', 'mindstorm crown').
card_uid('mindstorm crown'/'MRD', 'MRD:Mindstorm Crown:mindstorm crown').
card_rarity('mindstorm crown'/'MRD', 'Uncommon').
card_artist('mindstorm crown'/'MRD', 'Ben Thompson').
card_number('mindstorm crown'/'MRD', '207').
card_multiverse_id('mindstorm crown'/'MRD', '46715').

card_in_set('mirror golem', 'MRD').
card_original_type('mirror golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('mirror golem'/'MRD', 'Imprint When Mirror Golem comes into play, you may remove target card in a graveyard from the game. (The removed card is imprinted on this artifact.)\nMirror Golem has protection from each of the imprinted card\'s card types. (The card types are artifact, creature, enchantment, instant, land, and sorcery.)').
card_first_print('mirror golem', 'MRD').
card_image_name('mirror golem'/'MRD', 'mirror golem').
card_uid('mirror golem'/'MRD', 'MRD:Mirror Golem:mirror golem').
card_rarity('mirror golem'/'MRD', 'Uncommon').
card_artist('mirror golem'/'MRD', 'Franz Vohwinkel').
card_number('mirror golem'/'MRD', '208').
card_multiverse_id('mirror golem'/'MRD', '48210').

card_in_set('molder slug', 'MRD').
card_original_type('molder slug'/'MRD', 'Creature — Beast').
card_original_text('molder slug'/'MRD', 'At the beginning of each player\'s upkeep, that player sacrifices an artifact.').
card_first_print('molder slug', 'MRD').
card_image_name('molder slug'/'MRD', 'molder slug').
card_uid('molder slug'/'MRD', 'MRD:Molder Slug:molder slug').
card_rarity('molder slug'/'MRD', 'Rare').
card_artist('molder slug'/'MRD', 'Heather Hudson').
card_number('molder slug'/'MRD', '125').
card_flavor_text('molder slug'/'MRD', 'Fortunately for it, Mirrodin is a plane without salt.').
card_multiverse_id('molder slug'/'MRD', '48083').

card_in_set('molten rain', 'MRD').
card_original_type('molten rain'/'MRD', 'Sorcery').
card_original_text('molten rain'/'MRD', 'Destroy target land. If that land is nonbasic, Molten Rain deals 2 damage to the land\'s controller.').
card_first_print('molten rain', 'MRD').
card_image_name('molten rain'/'MRD', 'molten rain').
card_uid('molten rain'/'MRD', 'MRD:Molten Rain:molten rain').
card_rarity('molten rain'/'MRD', 'Common').
card_artist('molten rain'/'MRD', 'Hugh Jamieson').
card_number('molten rain'/'MRD', '101').
card_flavor_text('molten rain'/'MRD', 'When the molten rains fall, entire landscapes melt and flow away in rivulets of fire.').
card_multiverse_id('molten rain'/'MRD', '46000').

card_in_set('moriok scavenger', 'MRD').
card_original_type('moriok scavenger'/'MRD', 'Creature — Human Rogue').
card_original_text('moriok scavenger'/'MRD', 'When Moriok Scavenger comes into play, you may return target artifact creature card from your graveyard to your hand.').
card_first_print('moriok scavenger', 'MRD').
card_image_name('moriok scavenger'/'MRD', 'moriok scavenger').
card_uid('moriok scavenger'/'MRD', 'MRD:Moriok Scavenger:moriok scavenger').
card_rarity('moriok scavenger'/'MRD', 'Common').
card_artist('moriok scavenger'/'MRD', 'Puddnhead').
card_number('moriok scavenger'/'MRD', '68').
card_flavor_text('moriok scavenger'/'MRD', 'Many go to Mephidross in search of lost riches. Most end up as part of the cache.').
card_multiverse_id('moriok scavenger'/'MRD', '45965').

card_in_set('mountain', 'MRD').
card_original_type('mountain'/'MRD', 'Basic Land — Mountain').
card_original_text('mountain'/'MRD', 'R').
card_image_name('mountain'/'MRD', 'mountain1').
card_uid('mountain'/'MRD', 'MRD:Mountain:mountain1').
card_rarity('mountain'/'MRD', 'Basic Land').
card_artist('mountain'/'MRD', 'Mark Tedin').
card_number('mountain'/'MRD', '299').
card_multiverse_id('mountain'/'MRD', '48417').

card_in_set('mountain', 'MRD').
card_original_type('mountain'/'MRD', 'Basic Land — Mountain').
card_original_text('mountain'/'MRD', 'R').
card_image_name('mountain'/'MRD', 'mountain2').
card_uid('mountain'/'MRD', 'MRD:Mountain:mountain2').
card_rarity('mountain'/'MRD', 'Basic Land').
card_artist('mountain'/'MRD', 'Rob Alexander').
card_number('mountain'/'MRD', '300').
card_multiverse_id('mountain'/'MRD', '48418').

card_in_set('mountain', 'MRD').
card_original_type('mountain'/'MRD', 'Basic Land — Mountain').
card_original_text('mountain'/'MRD', 'R').
card_image_name('mountain'/'MRD', 'mountain3').
card_uid('mountain'/'MRD', 'MRD:Mountain:mountain3').
card_rarity('mountain'/'MRD', 'Basic Land').
card_artist('mountain'/'MRD', 'Martina Pilcerova').
card_number('mountain'/'MRD', '301').
card_multiverse_id('mountain'/'MRD', '48419').

card_in_set('mountain', 'MRD').
card_original_type('mountain'/'MRD', 'Basic Land — Mountain').
card_original_text('mountain'/'MRD', 'R').
card_image_name('mountain'/'MRD', 'mountain4').
card_uid('mountain'/'MRD', 'MRD:Mountain:mountain4').
card_rarity('mountain'/'MRD', 'Basic Land').
card_artist('mountain'/'MRD', 'John Avon').
card_number('mountain'/'MRD', '302').
card_multiverse_id('mountain'/'MRD', '48420').

card_in_set('mourner\'s shield', 'MRD').
card_original_type('mourner\'s shield'/'MRD', 'Artifact').
card_original_text('mourner\'s shield'/'MRD', 'Imprint When Mourner\'s Shield comes into play, you may remove target card in a graveyard from the game. (The removed card is imprinted on this artifact.)\n{2}, {T}: Prevent all damage that would be dealt this turn by a source of your choice that shares a color with the imprinted card.').
card_first_print('mourner\'s shield', 'MRD').
card_image_name('mourner\'s shield'/'MRD', 'mourner\'s shield').
card_uid('mourner\'s shield'/'MRD', 'MRD:Mourner\'s Shield:mourner\'s shield').
card_rarity('mourner\'s shield'/'MRD', 'Uncommon').
card_artist('mourner\'s shield'/'MRD', 'Carl Critchlow').
card_number('mourner\'s shield'/'MRD', '209').
card_multiverse_id('mourner\'s shield'/'MRD', '50538').

card_in_set('myr adapter', 'MRD').
card_original_type('myr adapter'/'MRD', 'Artifact Creature — Myr').
card_original_text('myr adapter'/'MRD', 'Myr Adapter gets +1/+1 for each Equipment attached to it.').
card_first_print('myr adapter', 'MRD').
card_image_name('myr adapter'/'MRD', 'myr adapter').
card_uid('myr adapter'/'MRD', 'MRD:Myr Adapter:myr adapter').
card_rarity('myr adapter'/'MRD', 'Common').
card_artist('myr adapter'/'MRD', 'Ben Thompson').
card_number('myr adapter'/'MRD', '210').
card_flavor_text('myr adapter'/'MRD', '\"The simplest way to plan ahead is merely to be ready for everything.\"\n—Pontifex, elder researcher').
card_multiverse_id('myr adapter'/'MRD', '46013').

card_in_set('myr enforcer', 'MRD').
card_original_type('myr enforcer'/'MRD', 'Artifact Creature — Myr').
card_original_text('myr enforcer'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)').
card_image_name('myr enforcer'/'MRD', 'myr enforcer').
card_uid('myr enforcer'/'MRD', 'MRD:Myr Enforcer:myr enforcer').
card_rarity('myr enforcer'/'MRD', 'Common').
card_artist('myr enforcer'/'MRD', 'Greg Staples').
card_number('myr enforcer'/'MRD', '211').
card_flavor_text('myr enforcer'/'MRD', 'Most myr monitor other species. Some myr monitor other myr.').
card_multiverse_id('myr enforcer'/'MRD', '46015').

card_in_set('myr incubator', 'MRD').
card_original_type('myr incubator'/'MRD', 'Artifact').
card_original_text('myr incubator'/'MRD', '{6}, {T}, Sacrifice Myr Incubator: Search your library for any number of artifact cards, remove them from the game, then put that many 1/1 Myr artifact creature tokens into play. Then shuffle your library.').
card_first_print('myr incubator', 'MRD').
card_image_name('myr incubator'/'MRD', 'myr incubator').
card_uid('myr incubator'/'MRD', 'MRD:Myr Incubator:myr incubator').
card_rarity('myr incubator'/'MRD', 'Rare').
card_artist('myr incubator'/'MRD', 'Alex Horley-Orlandelli').
card_number('myr incubator'/'MRD', '212').
card_multiverse_id('myr incubator'/'MRD', '47450').

card_in_set('myr mindservant', 'MRD').
card_original_type('myr mindservant'/'MRD', 'Artifact Creature — Myr').
card_original_text('myr mindservant'/'MRD', '{2}, {T}: Shuffle your library.').
card_first_print('myr mindservant', 'MRD').
card_image_name('myr mindservant'/'MRD', 'myr mindservant').
card_uid('myr mindservant'/'MRD', 'MRD:Myr Mindservant:myr mindservant').
card_rarity('myr mindservant'/'MRD', 'Uncommon').
card_artist('myr mindservant'/'MRD', 'Dave Dorman').
card_number('myr mindservant'/'MRD', '213').
card_flavor_text('myr mindservant'/'MRD', 'It knows what you are planning, and does not approve.').
card_multiverse_id('myr mindservant'/'MRD', '48925').

card_in_set('myr prototype', 'MRD').
card_original_type('myr prototype'/'MRD', 'Artifact Creature — Myr').
card_original_text('myr prototype'/'MRD', 'At the beginning of your upkeep, put a +1/+1 counter on Myr Prototype.\nMyr Prototype can\'t attack or block unless you pay {1} for each +1/+1 counter on it. (This cost is paid as attackers or blockers are declared.)').
card_first_print('myr prototype', 'MRD').
card_image_name('myr prototype'/'MRD', 'myr prototype').
card_uid('myr prototype'/'MRD', 'MRD:Myr Prototype:myr prototype').
card_rarity('myr prototype'/'MRD', 'Uncommon').
card_artist('myr prototype'/'MRD', 'Dave Dorman').
card_number('myr prototype'/'MRD', '214').
card_multiverse_id('myr prototype'/'MRD', '49046').

card_in_set('myr retriever', 'MRD').
card_original_type('myr retriever'/'MRD', 'Artifact Creature — Myr').
card_original_text('myr retriever'/'MRD', 'When Myr Retriever is put into a graveyard from play, return another target artifact card from your graveyard to your hand.').
card_first_print('myr retriever', 'MRD').
card_image_name('myr retriever'/'MRD', 'myr retriever').
card_uid('myr retriever'/'MRD', 'MRD:Myr Retriever:myr retriever').
card_rarity('myr retriever'/'MRD', 'Uncommon').
card_artist('myr retriever'/'MRD', 'Trevor Hairsine').
card_number('myr retriever'/'MRD', '215').
card_flavor_text('myr retriever'/'MRD', 'Mephidross gives up treasure easily . . . as long as you take its place.').
card_multiverse_id('myr retriever'/'MRD', '46126').

card_in_set('necrogen mists', 'MRD').
card_original_type('necrogen mists'/'MRD', 'Enchantment').
card_original_text('necrogen mists'/'MRD', 'At the beginning of each player\'s upkeep, that player discards a card from his or her hand.').
card_first_print('necrogen mists', 'MRD').
card_image_name('necrogen mists'/'MRD', 'necrogen mists').
card_uid('necrogen mists'/'MRD', 'MRD:Necrogen Mists:necrogen mists').
card_rarity('necrogen mists'/'MRD', 'Rare').
card_artist('necrogen mists'/'MRD', 'Alex Horley-Orlandelli').
card_number('necrogen mists'/'MRD', '69').
card_flavor_text('necrogen mists'/'MRD', 'Mephidross filled with roars of fury and wails of despair as the leonin fought to keep their memories from being pulled into the mists.').
card_multiverse_id('necrogen mists'/'MRD', '48589').

card_in_set('necrogen spellbomb', 'MRD').
card_original_type('necrogen spellbomb'/'MRD', 'Artifact').
card_original_text('necrogen spellbomb'/'MRD', '{B}, Sacrifice Necrogen Spellbomb: Target player discards a card from his or her hand.\n{1}, Sacrifice Necrogen Spellbomb: Draw a card.').
card_first_print('necrogen spellbomb', 'MRD').
card_image_name('necrogen spellbomb'/'MRD', 'necrogen spellbomb').
card_uid('necrogen spellbomb'/'MRD', 'MRD:Necrogen Spellbomb:necrogen spellbomb').
card_rarity('necrogen spellbomb'/'MRD', 'Common').
card_artist('necrogen spellbomb'/'MRD', 'Jim Nelson').
card_number('necrogen spellbomb'/'MRD', '216').
card_flavor_text('necrogen spellbomb'/'MRD', '\"Forget that which was never known.\"\n—Spellbomb inscription').
card_multiverse_id('necrogen spellbomb'/'MRD', '46055').

card_in_set('needlebug', 'MRD').
card_original_type('needlebug'/'MRD', 'Artifact Creature — Insect').
card_original_text('needlebug'/'MRD', 'Protection from artifacts\nYou may play Needlebug any time you could play an instant.').
card_first_print('needlebug', 'MRD').
card_image_name('needlebug'/'MRD', 'needlebug').
card_uid('needlebug'/'MRD', 'MRD:Needlebug:needlebug').
card_rarity('needlebug'/'MRD', 'Uncommon').
card_artist('needlebug'/'MRD', 'Paolo Parente').
card_number('needlebug'/'MRD', '217').
card_flavor_text('needlebug'/'MRD', 'Near Tel-Jilad, the Tangle is almost silent, save for the trolls\' chants and the skittering of needlebugs.').
card_multiverse_id('needlebug'/'MRD', '48930').

card_in_set('neurok familiar', 'MRD').
card_original_type('neurok familiar'/'MRD', 'Creature — Bird').
card_original_text('neurok familiar'/'MRD', 'Flying\nWhen Neurok Familiar comes into play, reveal the top card of your library. If it\'s an artifact card, put it into your hand. Otherwise, put it into your graveyard.').
card_first_print('neurok familiar', 'MRD').
card_image_name('neurok familiar'/'MRD', 'neurok familiar').
card_uid('neurok familiar'/'MRD', 'MRD:Neurok Familiar:neurok familiar').
card_rarity('neurok familiar'/'MRD', 'Common').
card_artist('neurok familiar'/'MRD', 'Edward P. Beard, Jr.').
card_number('neurok familiar'/'MRD', '43').
card_multiverse_id('neurok familiar'/'MRD', '46082').

card_in_set('neurok hoversail', 'MRD').
card_original_type('neurok hoversail'/'MRD', 'Artifact — Equipment').
card_original_text('neurok hoversail'/'MRD', 'Equipped creature has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('neurok hoversail', 'MRD').
card_image_name('neurok hoversail'/'MRD', 'neurok hoversail').
card_uid('neurok hoversail'/'MRD', 'MRD:Neurok Hoversail:neurok hoversail').
card_rarity('neurok hoversail'/'MRD', 'Common').
card_artist('neurok hoversail'/'MRD', 'Alan Pollack').
card_number('neurok hoversail'/'MRD', '218').
card_multiverse_id('neurok hoversail'/'MRD', '46037').

card_in_set('neurok spy', 'MRD').
card_original_type('neurok spy'/'MRD', 'Creature — Human Rogue').
card_original_text('neurok spy'/'MRD', 'Neurok Spy is unblockable as long as defending player controls an artifact.').
card_first_print('neurok spy', 'MRD').
card_image_name('neurok spy'/'MRD', 'neurok spy').
card_uid('neurok spy'/'MRD', 'MRD:Neurok Spy:neurok spy').
card_rarity('neurok spy'/'MRD', 'Common').
card_artist('neurok spy'/'MRD', 'Daren Bader').
card_number('neurok spy'/'MRD', '44').
card_flavor_text('neurok spy'/'MRD', 'From the murk of Mephidross to the heart of Kuldotha, the vedalken send their servants forth to gather knowledge from every inch of Mirrodin.').
card_multiverse_id('neurok spy'/'MRD', '45974').

card_in_set('nightmare lash', 'MRD').
card_original_type('nightmare lash'/'MRD', 'Artifact — Equipment').
card_original_text('nightmare lash'/'MRD', 'Equipped creature gets +1/+1 for each Swamp you control.\nEquip—Pay 3 life. (Pay 3 life: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('nightmare lash', 'MRD').
card_image_name('nightmare lash'/'MRD', 'nightmare lash').
card_uid('nightmare lash'/'MRD', 'MRD:Nightmare Lash:nightmare lash').
card_rarity('nightmare lash'/'MRD', 'Rare').
card_artist('nightmare lash'/'MRD', 'Puddnhead').
card_number('nightmare lash'/'MRD', '219').
card_multiverse_id('nightmare lash'/'MRD', '49770').

card_in_set('nim devourer', 'MRD').
card_original_type('nim devourer'/'MRD', 'Creature — Zombie').
card_original_text('nim devourer'/'MRD', 'Nim Devourer gets +1/+0 for each artifact you control.\n{B}{B}: Return Nim Devourer from your graveyard to play, then sacrifice a creature. Play this ability only during your upkeep.').
card_first_print('nim devourer', 'MRD').
card_image_name('nim devourer'/'MRD', 'nim devourer').
card_uid('nim devourer'/'MRD', 'MRD:Nim Devourer:nim devourer').
card_rarity('nim devourer'/'MRD', 'Rare').
card_artist('nim devourer'/'MRD', 'Adam Rex').
card_number('nim devourer'/'MRD', '70').
card_multiverse_id('nim devourer'/'MRD', '46090').

card_in_set('nim lasher', 'MRD').
card_original_type('nim lasher'/'MRD', 'Creature — Zombie').
card_original_text('nim lasher'/'MRD', 'Nim Lasher gets +1/+0 for each artifact you control.').
card_first_print('nim lasher', 'MRD').
card_image_name('nim lasher'/'MRD', 'nim lasher').
card_uid('nim lasher'/'MRD', 'MRD:Nim Lasher:nim lasher').
card_rarity('nim lasher'/'MRD', 'Common').
card_artist('nim lasher'/'MRD', 'Adam Rex').
card_number('nim lasher'/'MRD', '71').
card_flavor_text('nim lasher'/'MRD', 'The rotting metal feeds the necrogen mists, and in turn the mists feed the nim.').
card_multiverse_id('nim lasher'/'MRD', '12394').

card_in_set('nim replica', 'MRD').
card_original_type('nim replica'/'MRD', 'Artifact Creature — Zombie').
card_original_text('nim replica'/'MRD', '{2}{B}, Sacrifice Nim Replica: Target creature gets -1/-1 until end of turn.').
card_first_print('nim replica', 'MRD').
card_image_name('nim replica'/'MRD', 'nim replica').
card_uid('nim replica'/'MRD', 'MRD:Nim Replica:nim replica').
card_rarity('nim replica'/'MRD', 'Common').
card_artist('nim replica'/'MRD', 'Carl Critchlow').
card_number('nim replica'/'MRD', '220').
card_flavor_text('nim replica'/'MRD', 'It kills with unfeeling malice.').
card_multiverse_id('nim replica'/'MRD', '46041').

card_in_set('nim shambler', 'MRD').
card_original_type('nim shambler'/'MRD', 'Creature — Zombie').
card_original_text('nim shambler'/'MRD', 'Nim Shambler gets +1/+0 for each artifact you control.\nSacrifice a creature: Regenerate Nim Shambler.').
card_first_print('nim shambler', 'MRD').
card_image_name('nim shambler'/'MRD', 'nim shambler').
card_uid('nim shambler'/'MRD', 'MRD:Nim Shambler:nim shambler').
card_rarity('nim shambler'/'MRD', 'Uncommon').
card_artist('nim shambler'/'MRD', 'Adam Rex').
card_number('nim shambler'/'MRD', '72').
card_flavor_text('nim shambler'/'MRD', 'Called \"the Dross\" by its inhabitants, Mephidross is home to the nim, Mirrodin\'s mindless, ravenous undead.').
card_multiverse_id('nim shambler'/'MRD', '48126').

card_in_set('nim shrieker', 'MRD').
card_original_type('nim shrieker'/'MRD', 'Creature — Zombie').
card_original_text('nim shrieker'/'MRD', 'Flying\nNim Shrieker gets +1/+0 for each artifact you control.').
card_first_print('nim shrieker', 'MRD').
card_image_name('nim shrieker'/'MRD', 'nim shrieker').
card_uid('nim shrieker'/'MRD', 'MRD:Nim Shrieker:nim shrieker').
card_rarity('nim shrieker'/'MRD', 'Common').
card_artist('nim shrieker'/'MRD', 'Adam Rex').
card_number('nim shrieker'/'MRD', '73').
card_flavor_text('nim shrieker'/'MRD', 'As imps they were an annoyance. As nim they are a pestilence.').
card_multiverse_id('nim shrieker'/'MRD', '48584').

card_in_set('nuisance engine', 'MRD').
card_original_type('nuisance engine'/'MRD', 'Artifact').
card_original_text('nuisance engine'/'MRD', '{2}, {T}: Put a 0/1 Pest artifact creature token into play.').
card_first_print('nuisance engine', 'MRD').
card_image_name('nuisance engine'/'MRD', 'nuisance engine').
card_uid('nuisance engine'/'MRD', 'MRD:Nuisance Engine:nuisance engine').
card_rarity('nuisance engine'/'MRD', 'Uncommon').
card_artist('nuisance engine'/'MRD', 'Stephen Tappin').
card_number('nuisance engine'/'MRD', '221').
card_flavor_text('nuisance engine'/'MRD', 'All Auriok children know the tale, \"Bolgri and the Long Day of Squashing.\"').
card_multiverse_id('nuisance engine'/'MRD', '50104').

card_in_set('oblivion stone', 'MRD').
card_original_type('oblivion stone'/'MRD', 'Artifact').
card_original_text('oblivion stone'/'MRD', '{4}, {T}: Put a fate counter on target permanent.\n{5}, {T}, Sacrifice Oblivion Stone: Destroy each nonland permanent without a fate counter on it, then remove all fate counters from all permanents.').
card_first_print('oblivion stone', 'MRD').
card_image_name('oblivion stone'/'MRD', 'oblivion stone').
card_uid('oblivion stone'/'MRD', 'MRD:Oblivion Stone:oblivion stone').
card_rarity('oblivion stone'/'MRD', 'Rare').
card_artist('oblivion stone'/'MRD', 'Sam Wood').
card_number('oblivion stone'/'MRD', '222').
card_multiverse_id('oblivion stone'/'MRD', '34901').

card_in_set('ogre leadfoot', 'MRD').
card_original_type('ogre leadfoot'/'MRD', 'Creature — Ogre').
card_original_text('ogre leadfoot'/'MRD', 'Whenever Ogre Leadfoot becomes blocked by an artifact creature, destroy that creature.').
card_first_print('ogre leadfoot', 'MRD').
card_image_name('ogre leadfoot'/'MRD', 'ogre leadfoot').
card_uid('ogre leadfoot'/'MRD', 'MRD:Ogre Leadfoot:ogre leadfoot').
card_rarity('ogre leadfoot'/'MRD', 'Common').
card_artist('ogre leadfoot'/'MRD', 'Heather Hudson').
card_number('ogre leadfoot'/'MRD', '102').
card_flavor_text('ogre leadfoot'/'MRD', 'When the goblins need more scrap for the Great Furnace, they simply let the ogres loose and follow in their wake.').
card_multiverse_id('ogre leadfoot'/'MRD', '45993').

card_in_set('omega myr', 'MRD').
card_original_type('omega myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('omega myr'/'MRD', '').
card_first_print('omega myr', 'MRD').
card_image_name('omega myr'/'MRD', 'omega myr').
card_uid('omega myr'/'MRD', 'MRD:Omega Myr:omega myr').
card_rarity('omega myr'/'MRD', 'Common').
card_artist('omega myr'/'MRD', 'Dany Orizio').
card_number('omega myr'/'MRD', '223').
card_flavor_text('omega myr'/'MRD', 'Last to charge, last to fall.').
card_multiverse_id('omega myr'/'MRD', '48605').

card_in_set('one dozen eyes', 'MRD').
card_original_type('one dozen eyes'/'MRD', 'Sorcery').
card_original_text('one dozen eyes'/'MRD', 'Choose one Put a 5/5 green Beast creature token into play; or put five 1/1 green Insect creature tokens into play.\nEntwine {G}{G}{G} (Choose both if you pay the entwine cost.)').
card_first_print('one dozen eyes', 'MRD').
card_image_name('one dozen eyes'/'MRD', 'one dozen eyes').
card_uid('one dozen eyes'/'MRD', 'MRD:One Dozen Eyes:one dozen eyes').
card_rarity('one dozen eyes'/'MRD', 'Uncommon').
card_artist('one dozen eyes'/'MRD', 'Darrell Riche').
card_number('one dozen eyes'/'MRD', '126').
card_multiverse_id('one dozen eyes'/'MRD', '48385').

card_in_set('ornithopter', 'MRD').
card_original_type('ornithopter'/'MRD', 'Artifact Creature').
card_original_text('ornithopter'/'MRD', 'Flying').
card_image_name('ornithopter'/'MRD', 'ornithopter').
card_uid('ornithopter'/'MRD', 'MRD:Ornithopter:ornithopter').
card_rarity('ornithopter'/'MRD', 'Uncommon').
card_artist('ornithopter'/'MRD', 'Dana Knutson').
card_number('ornithopter'/'MRD', '224').
card_flavor_text('ornithopter'/'MRD', 'Regardless of the century, plane, or species, developing artificers never fail to invent the ornithopter.').
card_multiverse_id('ornithopter'/'MRD', '46016').

card_in_set('override', 'MRD').
card_original_type('override'/'MRD', 'Instant').
card_original_text('override'/'MRD', 'Counter target spell unless its controller pays {1} for each artifact you control.').
card_first_print('override', 'MRD').
card_image_name('override'/'MRD', 'override').
card_uid('override'/'MRD', 'MRD:Override:override').
card_rarity('override'/'MRD', 'Common').
card_artist('override'/'MRD', 'Hugh Jamieson').
card_number('override'/'MRD', '45').
card_flavor_text('override'/'MRD', '\"The Knowledge Pool has all the answers—especially ‘No.\'\"').
card_multiverse_id('override'/'MRD', '46089').

card_in_set('pearl shard', 'MRD').
card_original_type('pearl shard'/'MRD', 'Artifact').
card_original_text('pearl shard'/'MRD', '{3}, {T} or {W}, {T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('pearl shard', 'MRD').
card_image_name('pearl shard'/'MRD', 'pearl shard').
card_uid('pearl shard'/'MRD', 'MRD:Pearl Shard:pearl shard').
card_rarity('pearl shard'/'MRD', 'Uncommon').
card_artist('pearl shard'/'MRD', 'Doug Chaffee').
card_number('pearl shard'/'MRD', '225').
card_flavor_text('pearl shard'/'MRD', 'Leonin folktales claim it was brought from beyond the sky by the first kha.').
card_multiverse_id('pearl shard'/'MRD', '46027').

card_in_set('pentavus', 'MRD').
card_original_type('pentavus'/'MRD', 'Artifact Creature').
card_original_text('pentavus'/'MRD', 'Pentavus comes into play with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 Pentavite artifact creature token with flying into play.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_first_print('pentavus', 'MRD').
card_image_name('pentavus'/'MRD', 'pentavus').
card_uid('pentavus'/'MRD', 'MRD:Pentavus:pentavus').
card_rarity('pentavus'/'MRD', 'Rare').
card_artist('pentavus'/'MRD', 'Greg Staples').
card_number('pentavus'/'MRD', '226').
card_multiverse_id('pentavus'/'MRD', '46703').

card_in_set('pewter golem', 'MRD').
card_original_type('pewter golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('pewter golem'/'MRD', '{1}{B}: Regenerate Pewter Golem.').
card_first_print('pewter golem', 'MRD').
card_image_name('pewter golem'/'MRD', 'pewter golem').
card_uid('pewter golem'/'MRD', 'MRD:Pewter Golem:pewter golem').
card_rarity('pewter golem'/'MRD', 'Common').
card_artist('pewter golem'/'MRD', 'Paolo Parente').
card_number('pewter golem'/'MRD', '227').
card_flavor_text('pewter golem'/'MRD', 'Centuries before the first chimneys of Mephidross belched clouds of toxic gas, only the golems\' movements stirred the air of Mirrodin.').
card_multiverse_id('pewter golem'/'MRD', '48054').

card_in_set('plains', 'MRD').
card_original_type('plains'/'MRD', 'Basic Land — Plains').
card_original_text('plains'/'MRD', 'W').
card_image_name('plains'/'MRD', 'plains1').
card_uid('plains'/'MRD', 'MRD:Plains:plains1').
card_rarity('plains'/'MRD', 'Basic Land').
card_artist('plains'/'MRD', 'Mark Tedin').
card_number('plains'/'MRD', '287').
card_multiverse_id('plains'/'MRD', '48406').

card_in_set('plains', 'MRD').
card_original_type('plains'/'MRD', 'Basic Land — Plains').
card_original_text('plains'/'MRD', 'W').
card_image_name('plains'/'MRD', 'plains2').
card_uid('plains'/'MRD', 'MRD:Plains:plains2').
card_rarity('plains'/'MRD', 'Basic Land').
card_artist('plains'/'MRD', 'Rob Alexander').
card_number('plains'/'MRD', '288').
card_multiverse_id('plains'/'MRD', '48407').

card_in_set('plains', 'MRD').
card_original_type('plains'/'MRD', 'Basic Land — Plains').
card_original_text('plains'/'MRD', 'W').
card_image_name('plains'/'MRD', 'plains3').
card_uid('plains'/'MRD', 'MRD:Plains:plains3').
card_rarity('plains'/'MRD', 'Basic Land').
card_artist('plains'/'MRD', 'Martina Pilcerova').
card_number('plains'/'MRD', '289').
card_multiverse_id('plains'/'MRD', '48408').

card_in_set('plains', 'MRD').
card_original_type('plains'/'MRD', 'Basic Land — Plains').
card_original_text('plains'/'MRD', 'W').
card_image_name('plains'/'MRD', 'plains4').
card_uid('plains'/'MRD', 'MRD:Plains:plains4').
card_rarity('plains'/'MRD', 'Basic Land').
card_artist('plains'/'MRD', 'John Avon').
card_number('plains'/'MRD', '290').
card_multiverse_id('plains'/'MRD', '48409').

card_in_set('plated slagwurm', 'MRD').
card_original_type('plated slagwurm'/'MRD', 'Creature — Wurm').
card_original_text('plated slagwurm'/'MRD', 'Plated Slagwurm can\'t be the target of spells or abilities your opponents control.').
card_first_print('plated slagwurm', 'MRD').
card_image_name('plated slagwurm'/'MRD', 'plated slagwurm').
card_uid('plated slagwurm'/'MRD', 'MRD:Plated Slagwurm:plated slagwurm').
card_rarity('plated slagwurm'/'MRD', 'Rare').
card_artist('plated slagwurm'/'MRD', 'Justin Sweet').
card_number('plated slagwurm'/'MRD', '127').
card_flavor_text('plated slagwurm'/'MRD', 'Beneath the Tangle, the wurm tunnels stretch . . . wide as a stone\'s throw, long as forever, deep as you dare.').
card_multiverse_id('plated slagwurm'/'MRD', '47781').

card_in_set('platinum angel', 'MRD').
card_original_type('platinum angel'/'MRD', 'Artifact Creature — Angel').
card_original_text('platinum angel'/'MRD', 'Flying\nYou can\'t lose the game and your opponents can\'t win the game.').
card_first_print('platinum angel', 'MRD').
card_image_name('platinum angel'/'MRD', 'platinum angel').
card_uid('platinum angel'/'MRD', 'MRD:Platinum Angel:platinum angel').
card_rarity('platinum angel'/'MRD', 'Rare').
card_artist('platinum angel'/'MRD', 'Brom').
card_number('platinum angel'/'MRD', '228').
card_flavor_text('platinum angel'/'MRD', 'In its heart lies the secret of immortality.').
card_multiverse_id('platinum angel'/'MRD', '48580').

card_in_set('power conduit', 'MRD').
card_original_type('power conduit'/'MRD', 'Artifact').
card_original_text('power conduit'/'MRD', '{T}, Remove a counter from a permanent you control: Choose one Put a charge counter on target artifact; or put a +1/+1 counter on target creature.').
card_first_print('power conduit', 'MRD').
card_image_name('power conduit'/'MRD', 'power conduit').
card_uid('power conduit'/'MRD', 'MRD:Power Conduit:power conduit').
card_rarity('power conduit'/'MRD', 'Uncommon').
card_artist('power conduit'/'MRD', 'Todd Lockwood').
card_number('power conduit'/'MRD', '229').
card_flavor_text('power conduit'/'MRD', 'Never content, vedalken artificers continually tinker with their creations.').
card_multiverse_id('power conduit'/'MRD', '49047').

card_in_set('predator\'s strike', 'MRD').
card_original_type('predator\'s strike'/'MRD', 'Instant').
card_original_text('predator\'s strike'/'MRD', 'Target creature gets +3/+3 and gains trample until end of turn.').
card_first_print('predator\'s strike', 'MRD').
card_image_name('predator\'s strike'/'MRD', 'predator\'s strike').
card_uid('predator\'s strike'/'MRD', 'MRD:Predator\'s Strike:predator\'s strike').
card_rarity('predator\'s strike'/'MRD', 'Common').
card_artist('predator\'s strike'/'MRD', 'Tony Szczudlo').
card_number('predator\'s strike'/'MRD', '128').
card_flavor_text('predator\'s strike'/'MRD', 'If you hear it coming, you\'re not its prey.').
card_multiverse_id('predator\'s strike'/'MRD', '48595').

card_in_set('promise of power', 'MRD').
card_original_type('promise of power'/'MRD', 'Sorcery').
card_original_text('promise of power'/'MRD', 'Choose one You draw five cards and you lose 5 life; or put a black Demon creature token with flying into play with power and toughness each equal to the number of cards in your hand as the token comes into play.\nEntwine {4} (Choose both if you pay the entwine cost.)').
card_first_print('promise of power', 'MRD').
card_image_name('promise of power'/'MRD', 'promise of power').
card_uid('promise of power'/'MRD', 'MRD:Promise of Power:promise of power').
card_rarity('promise of power'/'MRD', 'Rare').
card_artist('promise of power'/'MRD', 'Kev Walker').
card_number('promise of power'/'MRD', '74').
card_multiverse_id('promise of power'/'MRD', '48096').

card_in_set('proteus staff', 'MRD').
card_original_type('proteus staff'/'MRD', 'Artifact').
card_original_text('proteus staff'/'MRD', '{2}{U}, {T}: Put target creature on the bottom of its owner\'s library. That creature\'s controller reveals cards from the top of his or her library until he or she reveals a creature card. The player puts that card into play and the rest on the bottom of his or her library in any order. Play this ability only any time you could play a sorcery.').
card_first_print('proteus staff', 'MRD').
card_image_name('proteus staff'/'MRD', 'proteus staff').
card_uid('proteus staff'/'MRD', 'MRD:Proteus Staff:proteus staff').
card_rarity('proteus staff'/'MRD', 'Rare').
card_artist('proteus staff'/'MRD', 'Trevor Hairsine').
card_number('proteus staff'/'MRD', '230').
card_multiverse_id('proteus staff'/'MRD', '49769').

card_in_set('psychic membrane', 'MRD').
card_original_type('psychic membrane'/'MRD', 'Creature — Wall').
card_original_text('psychic membrane'/'MRD', '(Walls can\'t attack.)\nWhenever Psychic Membrane blocks, you may draw a card.').
card_first_print('psychic membrane', 'MRD').
card_image_name('psychic membrane'/'MRD', 'psychic membrane').
card_uid('psychic membrane'/'MRD', 'MRD:Psychic Membrane:psychic membrane').
card_rarity('psychic membrane'/'MRD', 'Uncommon').
card_artist('psychic membrane'/'MRD', 'Tony Szczudlo').
card_number('psychic membrane'/'MRD', '46').
card_flavor_text('psychic membrane'/'MRD', 'The vedalken always put their best thought forward.').
card_multiverse_id('psychic membrane'/'MRD', '48167').

card_in_set('psychogenic probe', 'MRD').
card_original_type('psychogenic probe'/'MRD', 'Artifact').
card_original_text('psychogenic probe'/'MRD', 'Whenever a spell or ability causes a player to shuffle his or her library, Psychogenic Probe deals 2 damage to him or her.').
card_first_print('psychogenic probe', 'MRD').
card_image_name('psychogenic probe'/'MRD', 'psychogenic probe').
card_uid('psychogenic probe'/'MRD', 'MRD:Psychogenic Probe:psychogenic probe').
card_rarity('psychogenic probe'/'MRD', 'Rare').
card_artist('psychogenic probe'/'MRD', 'Jeremy Jarvis').
card_number('psychogenic probe'/'MRD', '231').
card_flavor_text('psychogenic probe'/'MRD', 'The same devices sold as surgeons\' tools in Lumengrid are sold as implements of torture in Mephidross.').
card_multiverse_id('psychogenic probe'/'MRD', '46582').

card_in_set('pyrite spellbomb', 'MRD').
card_original_type('pyrite spellbomb'/'MRD', 'Artifact').
card_original_text('pyrite spellbomb'/'MRD', '{R}, Sacrifice Pyrite Spellbomb: Pyrite Spellbomb deals 2 damage to target creature or player.\n{1}, Sacrifice Pyrite Spellbomb: Draw a card.').
card_first_print('pyrite spellbomb', 'MRD').
card_image_name('pyrite spellbomb'/'MRD', 'pyrite spellbomb').
card_uid('pyrite spellbomb'/'MRD', 'MRD:Pyrite Spellbomb:pyrite spellbomb').
card_rarity('pyrite spellbomb'/'MRD', 'Common').
card_artist('pyrite spellbomb'/'MRD', 'Jim Nelson').
card_number('pyrite spellbomb'/'MRD', '232').
card_flavor_text('pyrite spellbomb'/'MRD', '\"Melt that which was never frozen.\"\n—Spellbomb inscription').
card_multiverse_id('pyrite spellbomb'/'MRD', '48110').

card_in_set('quicksilver elemental', 'MRD').
card_original_type('quicksilver elemental'/'MRD', 'Creature — Elemental').
card_original_text('quicksilver elemental'/'MRD', '{U}: Quicksilver Elemental gains all activated abilities of target creature until end of turn. (If any of the abilities use that creature\'s name, use this creature\'s name instead.)\nYou may spend blue mana as though it were mana of any color to pay the activation costs of Quicksilver Elemental\'s abilities.').
card_first_print('quicksilver elemental', 'MRD').
card_image_name('quicksilver elemental'/'MRD', 'quicksilver elemental').
card_uid('quicksilver elemental'/'MRD', 'MRD:Quicksilver Elemental:quicksilver elemental').
card_rarity('quicksilver elemental'/'MRD', 'Rare').
card_artist('quicksilver elemental'/'MRD', 'Tony Szczudlo').
card_number('quicksilver elemental'/'MRD', '47').
card_multiverse_id('quicksilver elemental'/'MRD', '46559').

card_in_set('quicksilver fountain', 'MRD').
card_original_type('quicksilver fountain'/'MRD', 'Artifact').
card_original_text('quicksilver fountain'/'MRD', 'At the beginning of each player\'s upkeep, that player puts a flood counter on target non-Island land he or she controls. That land is an Island as long as it has a flood counter on it.\nAt end of turn, if all lands in play are Islands, remove all flood counters from them.').
card_first_print('quicksilver fountain', 'MRD').
card_image_name('quicksilver fountain'/'MRD', 'quicksilver fountain').
card_uid('quicksilver fountain'/'MRD', 'MRD:Quicksilver Fountain:quicksilver fountain').
card_rarity('quicksilver fountain'/'MRD', 'Rare').
card_artist('quicksilver fountain'/'MRD', 'Trevor Hairsine').
card_number('quicksilver fountain'/'MRD', '233').
card_multiverse_id('quicksilver fountain'/'MRD', '49772').

card_in_set('raise the alarm', 'MRD').
card_original_type('raise the alarm'/'MRD', 'Instant').
card_original_text('raise the alarm'/'MRD', 'Put two 1/1 white Soldier creature tokens into play.').
card_first_print('raise the alarm', 'MRD').
card_image_name('raise the alarm'/'MRD', 'raise the alarm').
card_uid('raise the alarm'/'MRD', 'MRD:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'MRD', 'Common').
card_artist('raise the alarm'/'MRD', 'John Matson').
card_number('raise the alarm'/'MRD', '16').
card_flavor_text('raise the alarm'/'MRD', '\"The nim raid our homes without warning. We must defend our homes without hesitation.\"').
card_multiverse_id('raise the alarm'/'MRD', '48103').

card_in_set('razor barrier', 'MRD').
card_original_type('razor barrier'/'MRD', 'Instant').
card_original_text('razor barrier'/'MRD', 'Target permanent you control gains protection from artifacts or from the color of your choice until end of turn.').
card_first_print('razor barrier', 'MRD').
card_image_name('razor barrier'/'MRD', 'razor barrier').
card_uid('razor barrier'/'MRD', 'MRD:Razor Barrier:razor barrier').
card_rarity('razor barrier'/'MRD', 'Common').
card_artist('razor barrier'/'MRD', 'Ron Spencer').
card_number('razor barrier'/'MRD', '17').
card_flavor_text('razor barrier'/'MRD', '\"We protect our homelands. Why should they not protect us?\"').
card_multiverse_id('razor barrier'/'MRD', '47935').

card_in_set('regress', 'MRD').
card_original_type('regress'/'MRD', 'Instant').
card_original_text('regress'/'MRD', 'Return target permanent to its owner\'s hand.').
card_first_print('regress', 'MRD').
card_image_name('regress'/'MRD', 'regress').
card_uid('regress'/'MRD', 'MRD:Regress:regress').
card_rarity('regress'/'MRD', 'Common').
card_artist('regress'/'MRD', 'Randy Gallegos').
card_number('regress'/'MRD', '48').
card_flavor_text('regress'/'MRD', '\"Once cast, a spell can be undone. But once revealed, a secret can never again be kept.\"\n—Pontifex, elder researcher').
card_multiverse_id('regress'/'MRD', '49061').

card_in_set('reiver demon', 'MRD').
card_original_type('reiver demon'/'MRD', 'Creature — Demon').
card_original_text('reiver demon'/'MRD', 'Flying\nWhen Reiver Demon comes into play, if you played it from your hand, destroy all nonartifact, nonblack creatures. They can\'t be regenerated.').
card_first_print('reiver demon', 'MRD').
card_image_name('reiver demon'/'MRD', 'reiver demon').
card_uid('reiver demon'/'MRD', 'MRD:Reiver Demon:reiver demon').
card_rarity('reiver demon'/'MRD', 'Rare').
card_artist('reiver demon'/'MRD', 'Brom').
card_number('reiver demon'/'MRD', '75').
card_multiverse_id('reiver demon'/'MRD', '48427').

card_in_set('relic bane', 'MRD').
card_original_type('relic bane'/'MRD', 'Enchant Artifact').
card_original_text('relic bane'/'MRD', 'Enchanted artifact has \"At the beginning of your upkeep, you lose 2 life.\"').
card_first_print('relic bane', 'MRD').
card_image_name('relic bane'/'MRD', 'relic bane').
card_uid('relic bane'/'MRD', 'MRD:Relic Bane:relic bane').
card_rarity('relic bane'/'MRD', 'Uncommon').
card_artist('relic bane'/'MRD', 'Eric Peterson').
card_number('relic bane'/'MRD', '76').
card_flavor_text('relic bane'/'MRD', 'A sword that has seen cowardice in battle exacts the price of honor from its wielder.').
card_multiverse_id('relic bane'/'MRD', '48329').

card_in_set('roar of the kha', 'MRD').
card_original_type('roar of the kha'/'MRD', 'Instant').
card_original_text('roar of the kha'/'MRD', 'Choose one Creatures you control get +1/+1 until end of turn; or untap all creatures you control.\nEntwine {1}{W} (Choose both if you pay the entwine cost.)').
card_first_print('roar of the kha', 'MRD').
card_image_name('roar of the kha'/'MRD', 'roar of the kha').
card_uid('roar of the kha'/'MRD', 'MRD:Roar of the Kha:roar of the kha').
card_rarity('roar of the kha'/'MRD', 'Uncommon').
card_artist('roar of the kha'/'MRD', 'Matt Cavotta').
card_number('roar of the kha'/'MRD', '18').
card_multiverse_id('roar of the kha'/'MRD', '48092').

card_in_set('rule of law', 'MRD').
card_original_type('rule of law'/'MRD', 'Enchantment').
card_original_text('rule of law'/'MRD', 'Each player can\'t play more than one spell each turn.').
card_first_print('rule of law', 'MRD').
card_image_name('rule of law'/'MRD', 'rule of law').
card_uid('rule of law'/'MRD', 'MRD:Rule of Law:rule of law').
card_rarity('rule of law'/'MRD', 'Rare').
card_artist('rule of law'/'MRD', 'Scott M. Fischer').
card_number('rule of law'/'MRD', '19').
card_flavor_text('rule of law'/'MRD', 'Appointed by the kha himself, members of the tribunal ensure all disputes are settled with the utmost fairness.').
card_multiverse_id('rule of law'/'MRD', '48112').

card_in_set('rust elemental', 'MRD').
card_original_type('rust elemental'/'MRD', 'Artifact Creature — Elemental').
card_original_text('rust elemental'/'MRD', 'Flying\nAt the beginning of your upkeep, sacrifice an artifact other than Rust Elemental. If you can\'t, tap Rust Elemental and you lose 4 life.').
card_first_print('rust elemental', 'MRD').
card_image_name('rust elemental'/'MRD', 'rust elemental').
card_uid('rust elemental'/'MRD', 'MRD:Rust Elemental:rust elemental').
card_rarity('rust elemental'/'MRD', 'Uncommon').
card_artist('rust elemental'/'MRD', 'Arnie Swekel').
card_number('rust elemental'/'MRD', '234').
card_multiverse_id('rust elemental'/'MRD', '48174').

card_in_set('rustmouth ogre', 'MRD').
card_original_type('rustmouth ogre'/'MRD', 'Creature — Ogre').
card_original_text('rustmouth ogre'/'MRD', 'Whenever Rustmouth Ogre deals combat damage to a player, you may destroy target artifact that player controls.').
card_first_print('rustmouth ogre', 'MRD').
card_image_name('rustmouth ogre'/'MRD', 'rustmouth ogre').
card_uid('rustmouth ogre'/'MRD', 'MRD:Rustmouth Ogre:rustmouth ogre').
card_rarity('rustmouth ogre'/'MRD', 'Uncommon').
card_artist('rustmouth ogre'/'MRD', 'Brian Snõddy').
card_number('rustmouth ogre'/'MRD', '103').
card_flavor_text('rustmouth ogre'/'MRD', 'It has an iron stomach. Literally.').
card_multiverse_id('rustmouth ogre'/'MRD', '46118').

card_in_set('rustspore ram', 'MRD').
card_original_type('rustspore ram'/'MRD', 'Artifact Creature').
card_original_text('rustspore ram'/'MRD', 'When Rustspore Ram comes into play, destroy target Equipment.').
card_first_print('rustspore ram', 'MRD').
card_image_name('rustspore ram'/'MRD', 'rustspore ram').
card_uid('rustspore ram'/'MRD', 'MRD:Rustspore Ram:rustspore ram').
card_rarity('rustspore ram'/'MRD', 'Uncommon').
card_artist('rustspore ram'/'MRD', 'Arnie Swekel').
card_number('rustspore ram'/'MRD', '235').
card_flavor_text('rustspore ram'/'MRD', 'Where herds have passed, the dented ground is lined with piles of rust.').
card_multiverse_id('rustspore ram'/'MRD', '46138').

card_in_set('scale of chiss-goria', 'MRD').
card_original_type('scale of chiss-goria'/'MRD', 'Artifact').
card_original_text('scale of chiss-goria'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nYou may play Scale of Chiss-Goria any time you could play an instant.\n{T}: Target creature gets +0/+1 until end of turn.').
card_first_print('scale of chiss-goria', 'MRD').
card_image_name('scale of chiss-goria'/'MRD', 'scale of chiss-goria').
card_uid('scale of chiss-goria'/'MRD', 'MRD:Scale of Chiss-Goria:scale of chiss-goria').
card_rarity('scale of chiss-goria'/'MRD', 'Common').
card_artist('scale of chiss-goria'/'MRD', 'Alan Pollack').
card_number('scale of chiss-goria'/'MRD', '236').
card_multiverse_id('scale of chiss-goria'/'MRD', '48400').

card_in_set('scrabbling claws', 'MRD').
card_original_type('scrabbling claws'/'MRD', 'Artifact').
card_original_text('scrabbling claws'/'MRD', '{T}: Target player removes a card in his or her graveyard from the game.\n{1}, Sacrifice Scrabbling Claws: Remove target card in a graveyard from the game. Draw a card.').
card_first_print('scrabbling claws', 'MRD').
card_image_name('scrabbling claws'/'MRD', 'scrabbling claws').
card_uid('scrabbling claws'/'MRD', 'MRD:Scrabbling Claws:scrabbling claws').
card_rarity('scrabbling claws'/'MRD', 'Uncommon').
card_artist('scrabbling claws'/'MRD', 'Thomas M. Baxa').
card_number('scrabbling claws'/'MRD', '237').
card_multiverse_id('scrabbling claws'/'MRD', '49763').

card_in_set('sculpting steel', 'MRD').
card_original_type('sculpting steel'/'MRD', 'Artifact').
card_original_text('sculpting steel'/'MRD', 'As Sculpting Steel comes into play, you may choose an artifact in play. If you do, Sculpting Steel comes into play as a copy of that artifact.').
card_first_print('sculpting steel', 'MRD').
card_image_name('sculpting steel'/'MRD', 'sculpting steel').
card_uid('sculpting steel'/'MRD', 'MRD:Sculpting Steel:sculpting steel').
card_rarity('sculpting steel'/'MRD', 'Rare').
card_artist('sculpting steel'/'MRD', 'Heather Hudson').
card_number('sculpting steel'/'MRD', '238').
card_flavor_text('sculpting steel'/'MRD', 'An artificer once dropped one in a vault full of coins. She has yet to find it.').
card_multiverse_id('sculpting steel'/'MRD', '46720').

card_in_set('scythe of the wretched', 'MRD').
card_original_type('scythe of the wretched'/'MRD', 'Artifact — Equipment').
card_original_text('scythe of the wretched'/'MRD', 'Equipped creature gets +2/+2.\nWhenever a creature dealt damage by equipped creature this turn is put into a graveyard, return that card to play under your control. Attach Scythe of the Wretched to that creature.\nEquip {4}').
card_first_print('scythe of the wretched', 'MRD').
card_image_name('scythe of the wretched'/'MRD', 'scythe of the wretched').
card_uid('scythe of the wretched'/'MRD', 'MRD:Scythe of the Wretched:scythe of the wretched').
card_rarity('scythe of the wretched'/'MRD', 'Rare').
card_artist('scythe of the wretched'/'MRD', 'Matt Cavotta').
card_number('scythe of the wretched'/'MRD', '239').
card_multiverse_id('scythe of the wretched'/'MRD', '49048').

card_in_set('seat of the synod', 'MRD').
card_original_type('seat of the synod'/'MRD', 'Artifact Land').
card_original_text('seat of the synod'/'MRD', '(Seat of the Synod isn\'t a spell.)\n{T}: Add {U} to your mana pool.').
card_first_print('seat of the synod', 'MRD').
card_image_name('seat of the synod'/'MRD', 'seat of the synod').
card_uid('seat of the synod'/'MRD', 'MRD:Seat of the Synod:seat of the synod').
card_rarity('seat of the synod'/'MRD', 'Common').
card_artist('seat of the synod'/'MRD', 'John Avon').
card_number('seat of the synod'/'MRD', '283').
card_flavor_text('seat of the synod'/'MRD', 'Lumengrid, site of the Knowledge Pool, source of vedalken arcana.').
card_multiverse_id('seat of the synod'/'MRD', '46064').

card_in_set('second sunrise', 'MRD').
card_original_type('second sunrise'/'MRD', 'Instant').
card_original_text('second sunrise'/'MRD', 'Each player returns to play all artifact, creature, enchantment, and land cards that were put into his or her graveyard from play this turn.').
card_first_print('second sunrise', 'MRD').
card_image_name('second sunrise'/'MRD', 'second sunrise').
card_uid('second sunrise'/'MRD', 'MRD:Second Sunrise:second sunrise').
card_rarity('second sunrise'/'MRD', 'Rare').
card_artist('second sunrise'/'MRD', 'Greg Staples').
card_number('second sunrise'/'MRD', '20').
card_flavor_text('second sunrise'/'MRD', 'The bright tunnel sometimes leads back to life.').
card_multiverse_id('second sunrise'/'MRD', '49009').

card_in_set('seething song', 'MRD').
card_original_type('seething song'/'MRD', 'Instant').
card_original_text('seething song'/'MRD', 'Add {R}{R}{R}{R}{R} to your mana pool.').
card_first_print('seething song', 'MRD').
card_image_name('seething song'/'MRD', 'seething song').
card_uid('seething song'/'MRD', 'MRD:Seething Song:seething song').
card_rarity('seething song'/'MRD', 'Common').
card_artist('seething song'/'MRD', 'Martina Pilcerova').
card_number('seething song'/'MRD', '104').
card_flavor_text('seething song'/'MRD', 'Vulshok mana rituals echo the day when the red sun burst through Mirrodin\'s surface to take its place in the heavens.').
card_multiverse_id('seething song'/'MRD', '34945').

card_in_set('serum tank', 'MRD').
card_original_type('serum tank'/'MRD', 'Artifact').
card_original_text('serum tank'/'MRD', 'Whenever Serum Tank or another artifact comes into play, put a charge counter on Serum Tank.\n{3}, {T}, Remove a charge counter from Serum Tank: Draw a card.').
card_first_print('serum tank', 'MRD').
card_image_name('serum tank'/'MRD', 'serum tank').
card_uid('serum tank'/'MRD', 'MRD:Serum Tank:serum tank').
card_rarity('serum tank'/'MRD', 'Uncommon').
card_artist('serum tank'/'MRD', 'Corey D. Macourek').
card_number('serum tank'/'MRD', '240').
card_multiverse_id('serum tank'/'MRD', '46156').

card_in_set('shared fate', 'MRD').
card_original_type('shared fate'/'MRD', 'Enchantment').
card_original_text('shared fate'/'MRD', 'If a player would draw a card, that player removes the top card of an opponent\'s library from the game face down instead.\nEach player may look at and play cards he or she removed from the game with Shared Fate as though they were in his or her hand.').
card_first_print('shared fate', 'MRD').
card_image_name('shared fate'/'MRD', 'shared fate').
card_uid('shared fate'/'MRD', 'MRD:Shared Fate:shared fate').
card_rarity('shared fate'/'MRD', 'Rare').
card_artist('shared fate'/'MRD', 'Matt Cavotta').
card_number('shared fate'/'MRD', '49').
card_multiverse_id('shared fate'/'MRD', '49764').

card_in_set('shatter', 'MRD').
card_original_type('shatter'/'MRD', 'Instant').
card_original_text('shatter'/'MRD', 'Destroy target artifact.').
card_image_name('shatter'/'MRD', 'shatter').
card_uid('shatter'/'MRD', 'MRD:Shatter:shatter').
card_rarity('shatter'/'MRD', 'Common').
card_artist('shatter'/'MRD', 'Tim Hildebrandt').
card_number('shatter'/'MRD', '105').
card_flavor_text('shatter'/'MRD', 'Days of planning. Weeks of building. Months of perfecting. Seconds of smashing.').
card_multiverse_id('shatter'/'MRD', '45999').

card_in_set('shrapnel blast', 'MRD').
card_original_type('shrapnel blast'/'MRD', 'Instant').
card_original_text('shrapnel blast'/'MRD', 'As an additional cost to play Shrapnel Blast, sacrifice an artifact.\nShrapnel Blast deals 5 damage to target creature or player.').
card_image_name('shrapnel blast'/'MRD', 'shrapnel blast').
card_uid('shrapnel blast'/'MRD', 'MRD:Shrapnel Blast:shrapnel blast').
card_rarity('shrapnel blast'/'MRD', 'Uncommon').
card_artist('shrapnel blast'/'MRD', 'Dave Dorman').
card_number('shrapnel blast'/'MRD', '106').
card_flavor_text('shrapnel blast'/'MRD', 'From trinket to trauma.').
card_multiverse_id('shrapnel blast'/'MRD', '49835').

card_in_set('silver myr', 'MRD').
card_original_type('silver myr'/'MRD', 'Artifact Creature — Myr').
card_original_text('silver myr'/'MRD', '{T}: Add {U} to your mana pool.').
card_first_print('silver myr', 'MRD').
card_image_name('silver myr'/'MRD', 'silver myr').
card_uid('silver myr'/'MRD', 'MRD:Silver Myr:silver myr').
card_rarity('silver myr'/'MRD', 'Common').
card_artist('silver myr'/'MRD', 'Kev Walker').
card_number('silver myr'/'MRD', '241').
card_flavor_text('silver myr'/'MRD', 'The vedalken saw the myr as toys, unaware of the intelligence lurking behind their empty eyes.').
card_multiverse_id('silver myr'/'MRD', '46032').

card_in_set('skeleton shard', 'MRD').
card_original_type('skeleton shard'/'MRD', 'Artifact').
card_original_text('skeleton shard'/'MRD', '{3}, {T} or {B}, {T}: Return target artifact creature card from your graveyard to your hand.').
card_first_print('skeleton shard', 'MRD').
card_image_name('skeleton shard'/'MRD', 'skeleton shard').
card_uid('skeleton shard'/'MRD', 'MRD:Skeleton Shard:skeleton shard').
card_rarity('skeleton shard'/'MRD', 'Uncommon').
card_artist('skeleton shard'/'MRD', 'Doug Chaffee').
card_number('skeleton shard'/'MRD', '242').
card_flavor_text('skeleton shard'/'MRD', 'Metal permeates the marrow of every bone in Mephidross—except one.').
card_multiverse_id('skeleton shard'/'MRD', '49774').

card_in_set('skyhunter cub', 'MRD').
card_original_type('skyhunter cub'/'MRD', 'Creature — Cat Knight').
card_original_text('skyhunter cub'/'MRD', 'As long as Skyhunter Cub is equipped, it gets +1/+1 and has flying.').
card_first_print('skyhunter cub', 'MRD').
card_image_name('skyhunter cub'/'MRD', 'skyhunter cub').
card_uid('skyhunter cub'/'MRD', 'MRD:Skyhunter Cub:skyhunter cub').
card_rarity('skyhunter cub'/'MRD', 'Common').
card_artist('skyhunter cub'/'MRD', 'Pete Venters').
card_number('skyhunter cub'/'MRD', '21').
card_flavor_text('skyhunter cub'/'MRD', 'Every young leonin wishes to become a skyhunter, for they soar closest to the suns.').
card_multiverse_id('skyhunter cub'/'MRD', '48046').

card_in_set('skyhunter patrol', 'MRD').
card_original_type('skyhunter patrol'/'MRD', 'Creature — Cat Knight').
card_original_text('skyhunter patrol'/'MRD', 'Flying, first strike').
card_first_print('skyhunter patrol', 'MRD').
card_image_name('skyhunter patrol'/'MRD', 'skyhunter patrol').
card_uid('skyhunter patrol'/'MRD', 'MRD:Skyhunter Patrol:skyhunter patrol').
card_rarity('skyhunter patrol'/'MRD', 'Common').
card_artist('skyhunter patrol'/'MRD', 'Matt Cavotta').
card_number('skyhunter patrol'/'MRD', '22').
card_flavor_text('skyhunter patrol'/'MRD', '\"They are the first to raise the alarm when the levelers attack, and the first to risk their lives to defend the pride.\"\n—Raksha Golden Cub, leonin kha').
card_multiverse_id('skyhunter patrol'/'MRD', '48431').

card_in_set('slagwurm armor', 'MRD').
card_original_type('slagwurm armor'/'MRD', 'Artifact — Equipment').
card_original_text('slagwurm armor'/'MRD', 'Equipped creature gets +0/+6.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('slagwurm armor', 'MRD').
card_image_name('slagwurm armor'/'MRD', 'slagwurm armor').
card_uid('slagwurm armor'/'MRD', 'MRD:Slagwurm Armor:slagwurm armor').
card_rarity('slagwurm armor'/'MRD', 'Common').
card_artist('slagwurm armor'/'MRD', 'Justin Sweet').
card_number('slagwurm armor'/'MRD', '243').
card_multiverse_id('slagwurm armor'/'MRD', '46028').

card_in_set('slith ascendant', 'MRD').
card_original_type('slith ascendant'/'MRD', 'Creature — Slith').
card_original_text('slith ascendant'/'MRD', 'Flying\nWhenever Slith Ascendant deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('slith ascendant', 'MRD').
card_image_name('slith ascendant'/'MRD', 'slith ascendant').
card_uid('slith ascendant'/'MRD', 'MRD:Slith Ascendant:slith ascendant').
card_rarity('slith ascendant'/'MRD', 'Uncommon').
card_artist('slith ascendant'/'MRD', 'Justin Sweet').
card_number('slith ascendant'/'MRD', '23').
card_flavor_text('slith ascendant'/'MRD', 'Instinctively drawn to the light of its \"mother-sun,\" each slith follows that sun\'s path around Mirrodin.').
card_multiverse_id('slith ascendant'/'MRD', '46069').

card_in_set('slith bloodletter', 'MRD').
card_original_type('slith bloodletter'/'MRD', 'Creature — Slith').
card_original_text('slith bloodletter'/'MRD', 'Whenever Slith Bloodletter deals combat damage to a player, put a +1/+1 counter on it.\n{1}{B}: Regenerate Slith Bloodletter.').
card_first_print('slith bloodletter', 'MRD').
card_image_name('slith bloodletter'/'MRD', 'slith bloodletter').
card_uid('slith bloodletter'/'MRD', 'MRD:Slith Bloodletter:slith bloodletter').
card_rarity('slith bloodletter'/'MRD', 'Uncommon').
card_artist('slith bloodletter'/'MRD', 'Justin Sweet').
card_number('slith bloodletter'/'MRD', '77').
card_flavor_text('slith bloodletter'/'MRD', 'Goblins fear the slith, believing they are children banished from the womb of the Steel Mother, deep within Kuldotha.').
card_multiverse_id('slith bloodletter'/'MRD', '46091').

card_in_set('slith firewalker', 'MRD').
card_original_type('slith firewalker'/'MRD', 'Creature — Slith').
card_original_text('slith firewalker'/'MRD', 'Haste\nWhenever Slith Firewalker deals combat damage to a player, put a +1/+1 counter on it.').
card_image_name('slith firewalker'/'MRD', 'slith firewalker').
card_uid('slith firewalker'/'MRD', 'MRD:Slith Firewalker:slith firewalker').
card_rarity('slith firewalker'/'MRD', 'Uncommon').
card_artist('slith firewalker'/'MRD', 'Justin Sweet').
card_number('slith firewalker'/'MRD', '107').
card_flavor_text('slith firewalker'/'MRD', 'The slith incubate in the Great Furnace\'s heat, emerging on Mirrodin\'s surface only when the four suns have aligned overhead.').
card_multiverse_id('slith firewalker'/'MRD', '46102').

card_in_set('slith predator', 'MRD').
card_original_type('slith predator'/'MRD', 'Creature — Slith').
card_original_text('slith predator'/'MRD', 'Trample\nWhenever Slith Predator deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('slith predator', 'MRD').
card_image_name('slith predator'/'MRD', 'slith predator').
card_uid('slith predator'/'MRD', 'MRD:Slith Predator:slith predator').
card_rarity('slith predator'/'MRD', 'Uncommon').
card_artist('slith predator'/'MRD', 'Justin Sweet').
card_number('slith predator'/'MRD', '129').
card_flavor_text('slith predator'/'MRD', 'Born amid the molten metal of the Great Furnace, the slith have more than adapted to the perils of a metal world.').
card_multiverse_id('slith predator'/'MRD', '46113').

card_in_set('slith strider', 'MRD').
card_original_type('slith strider'/'MRD', 'Creature — Slith').
card_original_text('slith strider'/'MRD', 'Whenever Slith Strider becomes blocked, draw a card.\nWhenever Slith Strider deals combat damage to a player, put a +1/+1 counter on it.').
card_first_print('slith strider', 'MRD').
card_image_name('slith strider'/'MRD', 'slith strider').
card_uid('slith strider'/'MRD', 'MRD:Slith Strider:slith strider').
card_rarity('slith strider'/'MRD', 'Uncommon').
card_artist('slith strider'/'MRD', 'Justin Sweet').
card_number('slith strider'/'MRD', '50').
card_flavor_text('slith strider'/'MRD', 'A slith\'s form and function are determined by the color of the sun under which it\'s born.').
card_multiverse_id('slith strider'/'MRD', '46080').

card_in_set('solar tide', 'MRD').
card_original_type('solar tide'/'MRD', 'Sorcery').
card_original_text('solar tide'/'MRD', 'Choose one Destroy all creatures with power 2 or less; or destroy all creatures with power 3 or greater.\nEntwine—Sacrifice two lands. (Choose both if you pay the entwine cost.)').
card_first_print('solar tide', 'MRD').
card_image_name('solar tide'/'MRD', 'solar tide').
card_uid('solar tide'/'MRD', 'MRD:Solar Tide:solar tide').
card_rarity('solar tide'/'MRD', 'Rare').
card_artist('solar tide'/'MRD', 'Dave Dorman').
card_number('solar tide'/'MRD', '24').
card_multiverse_id('solar tide'/'MRD', '48437').

card_in_set('soldier replica', 'MRD').
card_original_type('soldier replica'/'MRD', 'Artifact Creature — Soldier').
card_original_text('soldier replica'/'MRD', '{1}{W}, Sacrifice Soldier Replica: Soldier Replica deals 3 damage to target attacking or blocking creature.').
card_first_print('soldier replica', 'MRD').
card_image_name('soldier replica'/'MRD', 'soldier replica').
card_uid('soldier replica'/'MRD', 'MRD:Soldier Replica:soldier replica').
card_rarity('soldier replica'/'MRD', 'Common').
card_artist('soldier replica'/'MRD', 'Carl Critchlow').
card_number('soldier replica'/'MRD', '244').
card_flavor_text('soldier replica'/'MRD', 'It fights with inhuman vigor.').
card_multiverse_id('soldier replica'/'MRD', '46025').

card_in_set('solemn simulacrum', 'MRD').
card_original_type('solemn simulacrum'/'MRD', 'Artifact Creature').
card_original_text('solemn simulacrum'/'MRD', 'When Solemn Simulacrum comes into play, you may search your library for a basic land card and put that card into play tapped. If you do, shuffle your library.\nWhen Solemn Simulacrum is put into a graveyard from play, you may draw a card.').
card_first_print('solemn simulacrum', 'MRD').
card_image_name('solemn simulacrum'/'MRD', 'solemn simulacrum').
card_uid('solemn simulacrum'/'MRD', 'MRD:Solemn Simulacrum:solemn simulacrum').
card_rarity('solemn simulacrum'/'MRD', 'Rare').
card_artist('solemn simulacrum'/'MRD', 'Greg Staples').
card_number('solemn simulacrum'/'MRD', '245').
card_multiverse_id('solemn simulacrum'/'MRD', '49434').

card_in_set('somber hoverguard', 'MRD').
card_original_type('somber hoverguard'/'MRD', 'Creature — Drone').
card_original_text('somber hoverguard'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nFlying').
card_first_print('somber hoverguard', 'MRD').
card_image_name('somber hoverguard'/'MRD', 'somber hoverguard').
card_uid('somber hoverguard'/'MRD', 'MRD:Somber Hoverguard:somber hoverguard').
card_rarity('somber hoverguard'/'MRD', 'Common').
card_artist('somber hoverguard'/'MRD', 'Adam Rex').
card_number('somber hoverguard'/'MRD', '51').
card_flavor_text('somber hoverguard'/'MRD', 'The vedalken interrogate all intruders—once the hoverguards are done with them.').
card_multiverse_id('somber hoverguard'/'MRD', '45973').

card_in_set('soul foundry', 'MRD').
card_original_type('soul foundry'/'MRD', 'Artifact').
card_original_text('soul foundry'/'MRD', 'Imprint When Soul Foundry comes into play, you may remove a creature card in your hand from the game. (The removed card is imprinted on this artifact.)\n{X}, {T}: Put a creature token into play that\'s a copy of the imprinted creature card. X is the converted mana cost of that card.').
card_first_print('soul foundry', 'MRD').
card_image_name('soul foundry'/'MRD', 'soul foundry').
card_uid('soul foundry'/'MRD', 'MRD:Soul Foundry:soul foundry').
card_rarity('soul foundry'/'MRD', 'Rare').
card_artist('soul foundry'/'MRD', 'Arnie Swekel').
card_number('soul foundry'/'MRD', '246').
card_multiverse_id('soul foundry'/'MRD', '46723').

card_in_set('soul nova', 'MRD').
card_original_type('soul nova'/'MRD', 'Instant').
card_original_text('soul nova'/'MRD', 'Remove target attacking creature and all Equipment attached to it from the game.').
card_first_print('soul nova', 'MRD').
card_image_name('soul nova'/'MRD', 'soul nova').
card_uid('soul nova'/'MRD', 'MRD:Soul Nova:soul nova').
card_rarity('soul nova'/'MRD', 'Uncommon').
card_artist('soul nova'/'MRD', 'Keith Garletts').
card_number('soul nova'/'MRD', '25').
card_flavor_text('soul nova'/'MRD', 'Within seconds, the nim was consumed in blinding sunfire. Afterwards, only a puddle of molten iron remained.').
card_multiverse_id('soul nova'/'MRD', '48101').

card_in_set('spellweaver helix', 'MRD').
card_original_type('spellweaver helix'/'MRD', 'Artifact').
card_original_text('spellweaver helix'/'MRD', 'Imprint When Spellweaver Helix comes into play, you may remove two target sorcery cards in a single graveyard from the game. (The removed cards are imprinted on this artifact.)\nWhenever a card is played, if it has the same name as one of the imprinted sorcery cards, you may copy the other and play the copy without paying its mana cost.').
card_first_print('spellweaver helix', 'MRD').
card_image_name('spellweaver helix'/'MRD', 'spellweaver helix').
card_uid('spellweaver helix'/'MRD', 'MRD:Spellweaver Helix:spellweaver helix').
card_rarity('spellweaver helix'/'MRD', 'Rare').
card_artist('spellweaver helix'/'MRD', 'Luca Zontini').
card_number('spellweaver helix'/'MRD', '247').
card_multiverse_id('spellweaver helix'/'MRD', '48047').

card_in_set('sphere of purity', 'MRD').
card_original_type('sphere of purity'/'MRD', 'Enchantment').
card_original_text('sphere of purity'/'MRD', 'If an artifact would deal damage to you, prevent 1 of that damage.').
card_first_print('sphere of purity', 'MRD').
card_image_name('sphere of purity'/'MRD', 'sphere of purity').
card_uid('sphere of purity'/'MRD', 'MRD:Sphere of Purity:sphere of purity').
card_rarity('sphere of purity'/'MRD', 'Common').
card_artist('sphere of purity'/'MRD', 'Thomas Gianni').
card_number('sphere of purity'/'MRD', '26').
card_flavor_text('sphere of purity'/'MRD', 'Purity rejects artifice.').
card_multiverse_id('sphere of purity'/'MRD', '45968').

card_in_set('spikeshot goblin', 'MRD').
card_original_type('spikeshot goblin'/'MRD', 'Creature — Goblin Shaman').
card_original_text('spikeshot goblin'/'MRD', '{R}, {T}: Spikeshot Goblin deals damage equal to its power to target creature or player.').
card_first_print('spikeshot goblin', 'MRD').
card_image_name('spikeshot goblin'/'MRD', 'spikeshot goblin').
card_uid('spikeshot goblin'/'MRD', 'MRD:Spikeshot Goblin:spikeshot goblin').
card_rarity('spikeshot goblin'/'MRD', 'Common').
card_artist('spikeshot goblin'/'MRD', 'Alan Pollack').
card_number('spikeshot goblin'/'MRD', '108').
card_flavor_text('spikeshot goblin'/'MRD', 'The path of a goblin shaman is a journey through a thousand ways of hurting people.').
card_multiverse_id('spikeshot goblin'/'MRD', '46103').

card_in_set('spoils of the vault', 'MRD').
card_original_type('spoils of the vault'/'MRD', 'Instant').
card_original_text('spoils of the vault'/'MRD', 'Name a card. Reveal cards from the top of your library until you reveal the named card, then put that card into your hand. Remove all other cards revealed this way from the game, and you lose 1 life for each of the removed cards.').
card_first_print('spoils of the vault', 'MRD').
card_image_name('spoils of the vault'/'MRD', 'spoils of the vault').
card_uid('spoils of the vault'/'MRD', 'MRD:Spoils of the Vault:spoils of the vault').
card_rarity('spoils of the vault'/'MRD', 'Rare').
card_artist('spoils of the vault'/'MRD', 'Thomas M. Baxa').
card_number('spoils of the vault'/'MRD', '78').
card_multiverse_id('spoils of the vault'/'MRD', '46572').

card_in_set('stalking stones', 'MRD').
card_original_type('stalking stones'/'MRD', 'Land').
card_original_text('stalking stones'/'MRD', '{T}: Add {1} to your mana pool.\n{6}: Stalking Stones becomes a 3/3 artifact creature that\'s still a land. (This effect doesn\'t end at end of turn.)').
card_image_name('stalking stones'/'MRD', 'stalking stones').
card_uid('stalking stones'/'MRD', 'MRD:Stalking Stones:stalking stones').
card_rarity('stalking stones'/'MRD', 'Uncommon').
card_artist('stalking stones'/'MRD', 'David Day').
card_number('stalking stones'/'MRD', '284').
card_multiverse_id('stalking stones'/'MRD', '48917').

card_in_set('steel wall', 'MRD').
card_original_type('steel wall'/'MRD', 'Artifact Creature — Wall').
card_original_text('steel wall'/'MRD', '(Walls can\'t attack.)').
card_first_print('steel wall', 'MRD').
card_image_name('steel wall'/'MRD', 'steel wall').
card_uid('steel wall'/'MRD', 'MRD:Steel Wall:steel wall').
card_rarity('steel wall'/'MRD', 'Common').
card_artist('steel wall'/'MRD', 'David Day').
card_number('steel wall'/'MRD', '248').
card_flavor_text('steel wall'/'MRD', '\"We sculpt the land into what we need—homes, armament, fortresses of war. Our strength comes not only from knowing, but from commanding the terrain.\"\n—Raksha Golden Cub, leonin kha').
card_multiverse_id('steel wall'/'MRD', '48910').

card_in_set('sun droplet', 'MRD').
card_original_type('sun droplet'/'MRD', 'Artifact').
card_original_text('sun droplet'/'MRD', 'Whenever you\'re dealt damage, put that many charge counters on Sun Droplet.\nAt the beginning of each player\'s upkeep, you may remove a charge counter from Sun Droplet. If you do, you gain 1 life.').
card_first_print('sun droplet', 'MRD').
card_image_name('sun droplet'/'MRD', 'sun droplet').
card_uid('sun droplet'/'MRD', 'MRD:Sun Droplet:sun droplet').
card_rarity('sun droplet'/'MRD', 'Uncommon').
card_artist('sun droplet'/'MRD', 'Tim Hildebrandt').
card_number('sun droplet'/'MRD', '249').
card_multiverse_id('sun droplet'/'MRD', '48931').

card_in_set('sunbeam spellbomb', 'MRD').
card_original_type('sunbeam spellbomb'/'MRD', 'Artifact').
card_original_text('sunbeam spellbomb'/'MRD', '{W}, Sacrifice Sunbeam Spellbomb: You gain 5 life.\n{1}, Sacrifice Sunbeam Spellbomb: Draw a card.').
card_first_print('sunbeam spellbomb', 'MRD').
card_image_name('sunbeam spellbomb'/'MRD', 'sunbeam spellbomb').
card_uid('sunbeam spellbomb'/'MRD', 'MRD:Sunbeam Spellbomb:sunbeam spellbomb').
card_rarity('sunbeam spellbomb'/'MRD', 'Common').
card_artist('sunbeam spellbomb'/'MRD', 'Jim Nelson').
card_number('sunbeam spellbomb'/'MRD', '250').
card_flavor_text('sunbeam spellbomb'/'MRD', '\"Cure that which was never ill.\"\n—Spellbomb inscription').
card_multiverse_id('sunbeam spellbomb'/'MRD', '48214').

card_in_set('swamp', 'MRD').
card_original_type('swamp'/'MRD', 'Basic Land — Swamp').
card_original_text('swamp'/'MRD', 'B').
card_image_name('swamp'/'MRD', 'swamp1').
card_uid('swamp'/'MRD', 'MRD:Swamp:swamp1').
card_rarity('swamp'/'MRD', 'Basic Land').
card_artist('swamp'/'MRD', 'Mark Tedin').
card_number('swamp'/'MRD', '295').
card_multiverse_id('swamp'/'MRD', '48414').

card_in_set('swamp', 'MRD').
card_original_type('swamp'/'MRD', 'Basic Land — Swamp').
card_original_text('swamp'/'MRD', 'B').
card_image_name('swamp'/'MRD', 'swamp2').
card_uid('swamp'/'MRD', 'MRD:Swamp:swamp2').
card_rarity('swamp'/'MRD', 'Basic Land').
card_artist('swamp'/'MRD', 'Rob Alexander').
card_number('swamp'/'MRD', '296').
card_multiverse_id('swamp'/'MRD', '48415').

card_in_set('swamp', 'MRD').
card_original_type('swamp'/'MRD', 'Basic Land — Swamp').
card_original_text('swamp'/'MRD', 'B').
card_image_name('swamp'/'MRD', 'swamp3').
card_uid('swamp'/'MRD', 'MRD:Swamp:swamp3').
card_rarity('swamp'/'MRD', 'Basic Land').
card_artist('swamp'/'MRD', 'Martina Pilcerova').
card_number('swamp'/'MRD', '297').
card_multiverse_id('swamp'/'MRD', '48416').

card_in_set('swamp', 'MRD').
card_original_type('swamp'/'MRD', 'Basic Land — Swamp').
card_original_text('swamp'/'MRD', 'B').
card_image_name('swamp'/'MRD', 'swamp4').
card_uid('swamp'/'MRD', 'MRD:Swamp:swamp4').
card_rarity('swamp'/'MRD', 'Basic Land').
card_artist('swamp'/'MRD', 'John Avon').
card_number('swamp'/'MRD', '298').
card_multiverse_id('swamp'/'MRD', '48425').

card_in_set('sword of kaldra', 'MRD').
card_original_type('sword of kaldra'/'MRD', 'Legendary Artifact — Equipment').
card_original_text('sword of kaldra'/'MRD', 'Equipped creature gets +5/+5.\nWhenever equipped creature deals damage to a creature, remove that creature from the game.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_image_name('sword of kaldra'/'MRD', 'sword of kaldra').
card_uid('sword of kaldra'/'MRD', 'MRD:Sword of Kaldra:sword of kaldra').
card_rarity('sword of kaldra'/'MRD', 'Rare').
card_artist('sword of kaldra'/'MRD', 'Donato Giancola').
card_number('sword of kaldra'/'MRD', '251').
card_multiverse_id('sword of kaldra'/'MRD', '48583').

card_in_set('sylvan scrying', 'MRD').
card_original_type('sylvan scrying'/'MRD', 'Sorcery').
card_original_text('sylvan scrying'/'MRD', 'Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('sylvan scrying', 'MRD').
card_image_name('sylvan scrying'/'MRD', 'sylvan scrying').
card_uid('sylvan scrying'/'MRD', 'MRD:Sylvan Scrying:sylvan scrying').
card_rarity('sylvan scrying'/'MRD', 'Uncommon').
card_artist('sylvan scrying'/'MRD', 'Scott M. Fischer').
card_number('sylvan scrying'/'MRD', '130').
card_flavor_text('sylvan scrying'/'MRD', 'One glimpse of an elf\'s home lasts her weeks away in the wild.').
card_multiverse_id('sylvan scrying'/'MRD', '49529').

card_in_set('synod sanctum', 'MRD').
card_original_type('synod sanctum'/'MRD', 'Artifact').
card_original_text('synod sanctum'/'MRD', '{2}, {T}: Remove target permanent you control from the game.\n{2}, Sacrifice Synod Sanctum: Return to play under your control all cards removed from the game with Synod Sanctum.').
card_first_print('synod sanctum', 'MRD').
card_image_name('synod sanctum'/'MRD', 'synod sanctum').
card_uid('synod sanctum'/'MRD', 'MRD:Synod Sanctum:synod sanctum').
card_rarity('synod sanctum'/'MRD', 'Uncommon').
card_artist('synod sanctum'/'MRD', 'Dana Knutson').
card_number('synod sanctum'/'MRD', '252').
card_multiverse_id('synod sanctum'/'MRD', '46719').

card_in_set('taj-nar swordsmith', 'MRD').
card_original_type('taj-nar swordsmith'/'MRD', 'Creature — Cat Soldier').
card_original_text('taj-nar swordsmith'/'MRD', 'When Taj-Nar Swordsmith comes into play, you may pay {X}. If you do, search your library for an Equipment card with converted mana cost X or less and put that card into play. Then shuffle your library.').
card_first_print('taj-nar swordsmith', 'MRD').
card_image_name('taj-nar swordsmith'/'MRD', 'taj-nar swordsmith').
card_uid('taj-nar swordsmith'/'MRD', 'MRD:Taj-Nar Swordsmith:taj-nar swordsmith').
card_rarity('taj-nar swordsmith'/'MRD', 'Uncommon').
card_artist('taj-nar swordsmith'/'MRD', 'Todd Lockwood').
card_number('taj-nar swordsmith'/'MRD', '27').
card_multiverse_id('taj-nar swordsmith'/'MRD', '46079').

card_in_set('talisman of dominance', 'MRD').
card_original_type('talisman of dominance'/'MRD', 'Artifact').
card_original_text('talisman of dominance'/'MRD', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Talisman of Dominance deals 1 damage to you.').
card_first_print('talisman of dominance', 'MRD').
card_image_name('talisman of dominance'/'MRD', 'talisman of dominance').
card_uid('talisman of dominance'/'MRD', 'MRD:Talisman of Dominance:talisman of dominance').
card_rarity('talisman of dominance'/'MRD', 'Uncommon').
card_artist('talisman of dominance'/'MRD', 'Mike Dringenberg').
card_number('talisman of dominance'/'MRD', '253').
card_multiverse_id('talisman of dominance'/'MRD', '39598').

card_in_set('talisman of impulse', 'MRD').
card_original_type('talisman of impulse'/'MRD', 'Artifact').
card_original_text('talisman of impulse'/'MRD', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Talisman of Impulse deals 1 damage to you.').
card_first_print('talisman of impulse', 'MRD').
card_image_name('talisman of impulse'/'MRD', 'talisman of impulse').
card_uid('talisman of impulse'/'MRD', 'MRD:Talisman of Impulse:talisman of impulse').
card_rarity('talisman of impulse'/'MRD', 'Uncommon').
card_artist('talisman of impulse'/'MRD', 'Mike Dringenberg').
card_number('talisman of impulse'/'MRD', '254').
card_multiverse_id('talisman of impulse'/'MRD', '39600').

card_in_set('talisman of indulgence', 'MRD').
card_original_type('talisman of indulgence'/'MRD', 'Artifact').
card_original_text('talisman of indulgence'/'MRD', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Talisman of Indulgence deals 1 damage to you.').
card_first_print('talisman of indulgence', 'MRD').
card_image_name('talisman of indulgence'/'MRD', 'talisman of indulgence').
card_uid('talisman of indulgence'/'MRD', 'MRD:Talisman of Indulgence:talisman of indulgence').
card_rarity('talisman of indulgence'/'MRD', 'Uncommon').
card_artist('talisman of indulgence'/'MRD', 'Mike Dringenberg').
card_number('talisman of indulgence'/'MRD', '255').
card_multiverse_id('talisman of indulgence'/'MRD', '39599').

card_in_set('talisman of progress', 'MRD').
card_original_type('talisman of progress'/'MRD', 'Artifact').
card_original_text('talisman of progress'/'MRD', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Talisman of Progress deals 1 damage to you.').
card_first_print('talisman of progress', 'MRD').
card_image_name('talisman of progress'/'MRD', 'talisman of progress').
card_uid('talisman of progress'/'MRD', 'MRD:Talisman of Progress:talisman of progress').
card_rarity('talisman of progress'/'MRD', 'Uncommon').
card_artist('talisman of progress'/'MRD', 'Mike Dringenberg').
card_number('talisman of progress'/'MRD', '256').
card_multiverse_id('talisman of progress'/'MRD', '39597').

card_in_set('talisman of unity', 'MRD').
card_original_type('talisman of unity'/'MRD', 'Artifact').
card_original_text('talisman of unity'/'MRD', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Talisman of Unity deals 1 damage to you.').
card_first_print('talisman of unity', 'MRD').
card_image_name('talisman of unity'/'MRD', 'talisman of unity').
card_uid('talisman of unity'/'MRD', 'MRD:Talisman of Unity:talisman of unity').
card_rarity('talisman of unity'/'MRD', 'Uncommon').
card_artist('talisman of unity'/'MRD', 'Mike Dringenberg').
card_number('talisman of unity'/'MRD', '257').
card_multiverse_id('talisman of unity'/'MRD', '39601').

card_in_set('tanglebloom', 'MRD').
card_original_type('tanglebloom'/'MRD', 'Artifact').
card_original_text('tanglebloom'/'MRD', '{1}, {T}: You gain 1 life.').
card_first_print('tanglebloom', 'MRD').
card_image_name('tanglebloom'/'MRD', 'tanglebloom').
card_uid('tanglebloom'/'MRD', 'MRD:Tanglebloom:tanglebloom').
card_rarity('tanglebloom'/'MRD', 'Common').
card_artist('tanglebloom'/'MRD', 'Val Mayerik').
card_number('tanglebloom'/'MRD', '258').
card_flavor_text('tanglebloom'/'MRD', 'Druids converted swaths of the Tangle into tanglebloom orchards. Though heavy tools are needed to cut a leaf in the Tangle, tanglebloom fruit is easily plucked by hand.').
card_multiverse_id('tanglebloom'/'MRD', '48913').

card_in_set('tangleroot', 'MRD').
card_original_type('tangleroot'/'MRD', 'Artifact').
card_original_text('tangleroot'/'MRD', 'Whenever a player plays a creature spell, that player adds {G} to his or her mana pool.').
card_first_print('tangleroot', 'MRD').
card_image_name('tangleroot'/'MRD', 'tangleroot').
card_uid('tangleroot'/'MRD', 'MRD:Tangleroot:tangleroot').
card_rarity('tangleroot'/'MRD', 'Rare').
card_artist('tangleroot'/'MRD', 'Dana Knutson').
card_number('tangleroot'/'MRD', '259').
card_flavor_text('tangleroot'/'MRD', 'As if there\'s glitch in the system, the Tangle sometimes folds in on itself, throwing off sparks of mana in a mystifying display.').
card_multiverse_id('tangleroot'/'MRD', '49767').

card_in_set('tel-jilad archers', 'MRD').
card_original_type('tel-jilad archers'/'MRD', 'Creature — Elf Archer').
card_original_text('tel-jilad archers'/'MRD', 'Protection from artifacts\nTel-Jilad Archers may block as though it had flying.').
card_first_print('tel-jilad archers', 'MRD').
card_image_name('tel-jilad archers'/'MRD', 'tel-jilad archers').
card_uid('tel-jilad archers'/'MRD', 'MRD:Tel-Jilad Archers:tel-jilad archers').
card_rarity('tel-jilad archers'/'MRD', 'Common').
card_artist('tel-jilad archers'/'MRD', 'Marcelo Vignali').
card_number('tel-jilad archers'/'MRD', '131').
card_flavor_text('tel-jilad archers'/'MRD', 'They are extensions of the Tangle, stretching its vines into the furthest reaches of the sky.').
card_multiverse_id('tel-jilad archers'/'MRD', '46004').

card_in_set('tel-jilad chosen', 'MRD').
card_original_type('tel-jilad chosen'/'MRD', 'Creature — Elf Warrior').
card_original_text('tel-jilad chosen'/'MRD', 'Protection from artifacts').
card_first_print('tel-jilad chosen', 'MRD').
card_image_name('tel-jilad chosen'/'MRD', 'tel-jilad chosen').
card_uid('tel-jilad chosen'/'MRD', 'MRD:Tel-Jilad Chosen:tel-jilad chosen').
card_rarity('tel-jilad chosen'/'MRD', 'Common').
card_artist('tel-jilad chosen'/'MRD', 'Matthew D. Wilson').
card_number('tel-jilad chosen'/'MRD', '132').
card_flavor_text('tel-jilad chosen'/'MRD', '\"It is my honor to keep safe Tel-Jilad\'s secrets, not to know them.\"').
card_multiverse_id('tel-jilad chosen'/'MRD', '46003').

card_in_set('tel-jilad exile', 'MRD').
card_original_type('tel-jilad exile'/'MRD', 'Creature — Troll Warrior').
card_original_text('tel-jilad exile'/'MRD', '{1}{G}: Regenerate Tel-Jilad Exile.').
card_first_print('tel-jilad exile', 'MRD').
card_image_name('tel-jilad exile'/'MRD', 'tel-jilad exile').
card_uid('tel-jilad exile'/'MRD', 'MRD:Tel-Jilad Exile:tel-jilad exile').
card_rarity('tel-jilad exile'/'MRD', 'Common').
card_artist('tel-jilad exile'/'MRD', 'Justin Sweet').
card_number('tel-jilad exile'/'MRD', '133').
card_flavor_text('tel-jilad exile'/'MRD', 'For his crimes, he was made to forget all the trolls\' secrets. Now he knows only that he is outcast, but not why.').
card_multiverse_id('tel-jilad exile'/'MRD', '49436').

card_in_set('tel-jilad stylus', 'MRD').
card_original_type('tel-jilad stylus'/'MRD', 'Artifact').
card_original_text('tel-jilad stylus'/'MRD', '{T}: Put target permanent you own on the bottom of your library.').
card_first_print('tel-jilad stylus', 'MRD').
card_image_name('tel-jilad stylus'/'MRD', 'tel-jilad stylus').
card_uid('tel-jilad stylus'/'MRD', 'MRD:Tel-Jilad Stylus:tel-jilad stylus').
card_rarity('tel-jilad stylus'/'MRD', 'Uncommon').
card_artist('tel-jilad stylus'/'MRD', 'Darrell Riche').
card_number('tel-jilad stylus'/'MRD', '260').
card_flavor_text('tel-jilad stylus'/'MRD', 'Etched on Tel-Jilad\'s trunk is an entire history of Mirrodin—except for an expanse near the ground scrubbed smooth by an unknown hand.').
card_multiverse_id('tel-jilad stylus'/'MRD', '46729').

card_in_set('tempest of light', 'MRD').
card_original_type('tempest of light'/'MRD', 'Instant').
card_original_text('tempest of light'/'MRD', 'Destroy all enchantments.').
card_first_print('tempest of light', 'MRD').
card_image_name('tempest of light'/'MRD', 'tempest of light').
card_uid('tempest of light'/'MRD', 'MRD:Tempest of Light:tempest of light').
card_rarity('tempest of light'/'MRD', 'Uncommon').
card_artist('tempest of light'/'MRD', 'Wayne England').
card_number('tempest of light'/'MRD', '28').
card_flavor_text('tempest of light'/'MRD', '\"This world reeks of another\'s hand. Someone or something is defying the power of the gods and shaping this planet. I intend for it to stop.\"\n—Glissa Sunseeker').
card_multiverse_id('tempest of light'/'MRD', '46075').

card_in_set('temporal cascade', 'MRD').
card_original_type('temporal cascade'/'MRD', 'Sorcery').
card_original_text('temporal cascade'/'MRD', 'Choose one Each player shuffles his or her hand and graveyard into his or her library; or each player draws seven cards.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_first_print('temporal cascade', 'MRD').
card_image_name('temporal cascade'/'MRD', 'temporal cascade').
card_uid('temporal cascade'/'MRD', 'MRD:Temporal Cascade:temporal cascade').
card_rarity('temporal cascade'/'MRD', 'Rare').
card_artist('temporal cascade'/'MRD', 'Puddnhead').
card_number('temporal cascade'/'MRD', '52').
card_multiverse_id('temporal cascade'/'MRD', '49055').

card_in_set('terror', 'MRD').
card_original_type('terror'/'MRD', 'Instant').
card_original_text('terror'/'MRD', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_image_name('terror'/'MRD', 'terror').
card_uid('terror'/'MRD', 'MRD:Terror:terror').
card_rarity('terror'/'MRD', 'Common').
card_artist('terror'/'MRD', 'Puddnhead').
card_number('terror'/'MRD', '79').
card_flavor_text('terror'/'MRD', '\"A simple trap in a dark corner of the mind, and their nightmares catch up with them.\"\n—Geth, keeper of the Vault').
card_multiverse_id('terror'/'MRD', '45988').

card_in_set('thirst for knowledge', 'MRD').
card_original_type('thirst for knowledge'/'MRD', 'Instant').
card_original_text('thirst for knowledge'/'MRD', 'Draw three cards. Then discard two cards from your hand unless you discard an artifact card from your hand.').
card_image_name('thirst for knowledge'/'MRD', 'thirst for knowledge').
card_uid('thirst for knowledge'/'MRD', 'MRD:Thirst for Knowledge:thirst for knowledge').
card_rarity('thirst for knowledge'/'MRD', 'Uncommon').
card_artist('thirst for knowledge'/'MRD', 'Ben Thompson').
card_number('thirst for knowledge'/'MRD', '53').
card_flavor_text('thirst for knowledge'/'MRD', 'Lymph, the fluid essence of blinkmoths, is prized by wizards for the rush of intellect it provides.').
card_multiverse_id('thirst for knowledge'/'MRD', '45978').

card_in_set('thought prison', 'MRD').
card_original_type('thought prison'/'MRD', 'Artifact').
card_original_text('thought prison'/'MRD', 'Imprint When Thought Prison comes into play, you may have target player reveal his or her hand. If you do, choose a nonland card from it and remove that card from the game. (The removed card is imprinted on this artifact.)\nWhenever a player plays a spell that shares a color or converted mana cost with the imprinted card, Thought Prison deals 2 damage to that player.').
card_first_print('thought prison', 'MRD').
card_image_name('thought prison'/'MRD', 'thought prison').
card_uid('thought prison'/'MRD', 'MRD:Thought Prison:thought prison').
card_rarity('thought prison'/'MRD', 'Uncommon').
card_artist('thought prison'/'MRD', 'Glen Angus').
card_number('thought prison'/'MRD', '261').
card_multiverse_id('thought prison'/'MRD', '48915').

card_in_set('thoughtcast', 'MRD').
card_original_type('thoughtcast'/'MRD', 'Sorcery').
card_original_text('thoughtcast'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nDraw two cards.').
card_first_print('thoughtcast', 'MRD').
card_image_name('thoughtcast'/'MRD', 'thoughtcast').
card_uid('thoughtcast'/'MRD', 'MRD:Thoughtcast:thoughtcast').
card_rarity('thoughtcast'/'MRD', 'Common').
card_artist('thoughtcast'/'MRD', 'Greg Hildebrandt').
card_number('thoughtcast'/'MRD', '54').
card_flavor_text('thoughtcast'/'MRD', 'Vedalken eyes don\'t see the beauty in things. They see only what those things can teach.').
card_multiverse_id('thoughtcast'/'MRD', '46085').

card_in_set('timesifter', 'MRD').
card_original_type('timesifter'/'MRD', 'Artifact').
card_original_text('timesifter'/'MRD', 'At the beginning of each player\'s upkeep, each player removes the top card of his or her library from the game. The player who removed the card with the highest converted mana cost takes an extra turn after this one. If two or more players\' cards are tied for highest cost, the tied players repeat this process until the tie is broken.').
card_first_print('timesifter', 'MRD').
card_image_name('timesifter'/'MRD', 'timesifter').
card_uid('timesifter'/'MRD', 'MRD:Timesifter:timesifter').
card_rarity('timesifter'/'MRD', 'Rare').
card_artist('timesifter'/'MRD', 'Dany Orizio').
card_number('timesifter'/'MRD', '262').
card_multiverse_id('timesifter'/'MRD', '49053').

card_in_set('titanium golem', 'MRD').
card_original_type('titanium golem'/'MRD', 'Artifact Creature — Golem').
card_original_text('titanium golem'/'MRD', '{1}{W}: Titanium Golem gains first strike until end of turn.').
card_first_print('titanium golem', 'MRD').
card_image_name('titanium golem'/'MRD', 'titanium golem').
card_uid('titanium golem'/'MRD', 'MRD:Titanium Golem:titanium golem').
card_rarity('titanium golem'/'MRD', 'Common').
card_artist('titanium golem'/'MRD', 'Paolo Parente').
card_number('titanium golem'/'MRD', '263').
card_flavor_text('titanium golem'/'MRD', 'Centuries before the first blades of the Razor Fields chimed in the wind, Mirrodin echoed with the golems\' footsteps.').
card_multiverse_id('titanium golem'/'MRD', '48052').

card_in_set('tooth and nail', 'MRD').
card_original_type('tooth and nail'/'MRD', 'Sorcery').
card_original_text('tooth and nail'/'MRD', 'Choose one Search your library for up to two creature cards, reveal them, put them into your hand, then shuffle your library; or put up to two creature cards from your hand into play.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_first_print('tooth and nail', 'MRD').
card_image_name('tooth and nail'/'MRD', 'tooth and nail').
card_uid('tooth and nail'/'MRD', 'MRD:Tooth and Nail:tooth and nail').
card_rarity('tooth and nail'/'MRD', 'Rare').
card_artist('tooth and nail'/'MRD', 'Greg Hildebrandt').
card_number('tooth and nail'/'MRD', '134').
card_multiverse_id('tooth and nail'/'MRD', '48122').

card_in_set('tooth of chiss-goria', 'MRD').
card_original_type('tooth of chiss-goria'/'MRD', 'Artifact').
card_original_text('tooth of chiss-goria'/'MRD', 'Affinity for artifacts (This spell costs {1} less to play for each artifact you control.)\nYou may play Tooth of Chiss-Goria any time you could play an instant.\n{T}: Target creature gets +1/+0 until end of turn.').
card_first_print('tooth of chiss-goria', 'MRD').
card_image_name('tooth of chiss-goria'/'MRD', 'tooth of chiss-goria').
card_uid('tooth of chiss-goria'/'MRD', 'MRD:Tooth of Chiss-Goria:tooth of chiss-goria').
card_rarity('tooth of chiss-goria'/'MRD', 'Common').
card_artist('tooth of chiss-goria'/'MRD', 'Alan Pollack').
card_number('tooth of chiss-goria'/'MRD', '264').
card_multiverse_id('tooth of chiss-goria'/'MRD', '48401').

card_in_set('tower of champions', 'MRD').
card_original_type('tower of champions'/'MRD', 'Artifact').
card_original_text('tower of champions'/'MRD', '{8}, {T}: Target creature gets +6/+6 until end of turn.').
card_first_print('tower of champions', 'MRD').
card_image_name('tower of champions'/'MRD', 'tower of champions').
card_uid('tower of champions'/'MRD', 'MRD:Tower of Champions:tower of champions').
card_rarity('tower of champions'/'MRD', 'Rare').
card_artist('tower of champions'/'MRD', 'Greg Staples').
card_number('tower of champions'/'MRD', '265').
card_flavor_text('tower of champions'/'MRD', 'The ur-golem runes tell of the transformation of Mirrodin\'s warden from silent guardian to merciless god.').
card_multiverse_id('tower of champions'/'MRD', '48155').

card_in_set('tower of eons', 'MRD').
card_original_type('tower of eons'/'MRD', 'Artifact').
card_original_text('tower of eons'/'MRD', '{8}, {T}: You gain 10 life.').
card_first_print('tower of eons', 'MRD').
card_image_name('tower of eons'/'MRD', 'tower of eons').
card_uid('tower of eons'/'MRD', 'MRD:Tower of Eons:tower of eons').
card_rarity('tower of eons'/'MRD', 'Rare').
card_artist('tower of eons'/'MRD', 'John Avon').
card_number('tower of eons'/'MRD', '266').
card_flavor_text('tower of eons'/'MRD', 'Its etchings tell the ur-golems\' stories of an entity able to force life into a lifeless plane.').
card_multiverse_id('tower of eons'/'MRD', '48914').

card_in_set('tower of fortunes', 'MRD').
card_original_type('tower of fortunes'/'MRD', 'Artifact').
card_original_text('tower of fortunes'/'MRD', '{8}, {T}: Draw four cards.').
card_first_print('tower of fortunes', 'MRD').
card_image_name('tower of fortunes'/'MRD', 'tower of fortunes').
card_uid('tower of fortunes'/'MRD', 'MRD:Tower of Fortunes:tower of fortunes').
card_rarity('tower of fortunes'/'MRD', 'Rare').
card_artist('tower of fortunes'/'MRD', 'Matt Cavotta').
card_number('tower of fortunes'/'MRD', '267').
card_flavor_text('tower of fortunes'/'MRD', 'The ur-golem etchings begin by celebrating Mirrodin\'s creator, a golem of almost limitless power. They end by cursing its protector, a being called Memnarch.').
card_multiverse_id('tower of fortunes'/'MRD', '35464').

card_in_set('tower of murmurs', 'MRD').
card_original_type('tower of murmurs'/'MRD', 'Artifact').
card_original_text('tower of murmurs'/'MRD', '{8}, {T}: Target player puts the top eight cards of his or her library into his or her graveyard.').
card_first_print('tower of murmurs', 'MRD').
card_image_name('tower of murmurs'/'MRD', 'tower of murmurs').
card_uid('tower of murmurs'/'MRD', 'MRD:Tower of Murmurs:tower of murmurs').
card_rarity('tower of murmurs'/'MRD', 'Rare').
card_artist('tower of murmurs'/'MRD', 'Glen Angus').
card_number('tower of murmurs'/'MRD', '268').
card_flavor_text('tower of murmurs'/'MRD', 'Etched on its surface are warnings from a long-lost race of ur-golems pushed to the brink of extinction.').
card_multiverse_id('tower of murmurs'/'MRD', '46730').

card_in_set('trash for treasure', 'MRD').
card_original_type('trash for treasure'/'MRD', 'Sorcery').
card_original_text('trash for treasure'/'MRD', 'As an additional cost to play Trash for Treasure, sacrifice an artifact.\nReturn target artifact card from your graveyard to play.').
card_first_print('trash for treasure', 'MRD').
card_image_name('trash for treasure'/'MRD', 'trash for treasure').
card_uid('trash for treasure'/'MRD', 'MRD:Trash for Treasure:trash for treasure').
card_rarity('trash for treasure'/'MRD', 'Rare').
card_artist('trash for treasure'/'MRD', 'Lars Grant-West').
card_number('trash for treasure'/'MRD', '109').
card_flavor_text('trash for treasure'/'MRD', 'Goblins generally have two possessions: one per hand.').
card_multiverse_id('trash for treasure'/'MRD', '48586').

card_in_set('tree of tales', 'MRD').
card_original_type('tree of tales'/'MRD', 'Artifact Land').
card_original_text('tree of tales'/'MRD', '(Tree of Tales isn\'t a spell.)\n{T}: Add {G} to your mana pool.').
card_first_print('tree of tales', 'MRD').
card_image_name('tree of tales'/'MRD', 'tree of tales').
card_uid('tree of tales'/'MRD', 'MRD:Tree of Tales:tree of tales').
card_rarity('tree of tales'/'MRD', 'Common').
card_artist('tree of tales'/'MRD', 'John Avon').
card_number('tree of tales'/'MRD', '285').
card_flavor_text('tree of tales'/'MRD', 'Tel-Jilad, sanctum of the ancient trolls, keepers of the secret of Mirrodin\'s origin.').
card_multiverse_id('tree of tales'/'MRD', '46067').

card_in_set('triskelion', 'MRD').
card_original_type('triskelion'/'MRD', 'Artifact Creature').
card_original_text('triskelion'/'MRD', 'Triskelion comes into play with three +1/+1 counters on it.\nRemove a +1/+1 counter from Triskelion: Triskelion deals 1 damage to target creature or player.').
card_image_name('triskelion'/'MRD', 'triskelion').
card_uid('triskelion'/'MRD', 'MRD:Triskelion:triskelion').
card_rarity('triskelion'/'MRD', 'Rare').
card_artist('triskelion'/'MRD', 'Christopher Moeller').
card_number('triskelion'/'MRD', '269').
card_multiverse_id('triskelion'/'MRD', '46702').

card_in_set('troll ascetic', 'MRD').
card_original_type('troll ascetic'/'MRD', 'Creature — Troll Shaman').
card_original_text('troll ascetic'/'MRD', 'Troll Ascetic can\'t be the target of spells or abilities your opponents control.\n{1}{G}: Regenerate Troll Ascetic.').
card_first_print('troll ascetic', 'MRD').
card_image_name('troll ascetic'/'MRD', 'troll ascetic').
card_uid('troll ascetic'/'MRD', 'MRD:Troll Ascetic:troll ascetic').
card_rarity('troll ascetic'/'MRD', 'Rare').
card_artist('troll ascetic'/'MRD', 'Puddnhead').
card_number('troll ascetic'/'MRD', '135').
card_flavor_text('troll ascetic'/'MRD', 'It\'s no coincidence that the oldest trolls are also the angriest.').
card_multiverse_id('troll ascetic'/'MRD', '49828').

card_in_set('trolls of tel-jilad', 'MRD').
card_original_type('trolls of tel-jilad'/'MRD', 'Creature — Troll Shaman').
card_original_text('trolls of tel-jilad'/'MRD', '{1}{G}: Regenerate target green creature.').
card_first_print('trolls of tel-jilad', 'MRD').
card_image_name('trolls of tel-jilad'/'MRD', 'trolls of tel-jilad').
card_uid('trolls of tel-jilad'/'MRD', 'MRD:Trolls of Tel-Jilad:trolls of tel-jilad').
card_rarity('trolls of tel-jilad'/'MRD', 'Uncommon').
card_artist('trolls of tel-jilad'/'MRD', 'Marcelo Vignali').
card_number('trolls of tel-jilad'/'MRD', '136').
card_flavor_text('trolls of tel-jilad'/'MRD', '\"The secret of this world weighs upon us, and we have been shaped by time and duty to bear it.\"').
card_multiverse_id('trolls of tel-jilad'/'MRD', '46117').

card_in_set('turn to dust', 'MRD').
card_original_type('turn to dust'/'MRD', 'Instant').
card_original_text('turn to dust'/'MRD', 'Destroy target Equipment. Then add {G} to your mana pool.').
card_first_print('turn to dust', 'MRD').
card_image_name('turn to dust'/'MRD', 'turn to dust').
card_uid('turn to dust'/'MRD', 'MRD:Turn to Dust:turn to dust').
card_rarity('turn to dust'/'MRD', 'Common').
card_artist('turn to dust'/'MRD', 'Wayne England').
card_number('turn to dust'/'MRD', '137').
card_flavor_text('turn to dust'/'MRD', 'Mirrodin\'s inhabitants must be prepared for anything—including suddenly being unprepared.').
card_multiverse_id('turn to dust'/'MRD', '48594').

card_in_set('vault of whispers', 'MRD').
card_original_type('vault of whispers'/'MRD', 'Artifact Land').
card_original_text('vault of whispers'/'MRD', '(Vault of Whispers isn\'t a spell.)\n{T}: Add {B} to your mana pool.').
card_first_print('vault of whispers', 'MRD').
card_image_name('vault of whispers'/'MRD', 'vault of whispers').
card_uid('vault of whispers'/'MRD', 'MRD:Vault of Whispers:vault of whispers').
card_rarity('vault of whispers'/'MRD', 'Common').
card_artist('vault of whispers'/'MRD', 'Rob Alexander').
card_number('vault of whispers'/'MRD', '286').
card_flavor_text('vault of whispers'/'MRD', 'Ish Sah, den of the warlord Geth, commander of countless nim.').
card_multiverse_id('vault of whispers'/'MRD', '46065').

card_in_set('vedalken archmage', 'MRD').
card_original_type('vedalken archmage'/'MRD', 'Creature — Vedalken Wizard').
card_original_text('vedalken archmage'/'MRD', 'Whenever you play an artifact spell, draw a card.').
card_first_print('vedalken archmage', 'MRD').
card_image_name('vedalken archmage'/'MRD', 'vedalken archmage').
card_uid('vedalken archmage'/'MRD', 'MRD:Vedalken Archmage:vedalken archmage').
card_rarity('vedalken archmage'/'MRD', 'Rare').
card_artist('vedalken archmage'/'MRD', 'Kev Walker').
card_number('vedalken archmage'/'MRD', '55').
card_flavor_text('vedalken archmage'/'MRD', '\"The Knowledge Pool knows. Memnarch understands.\"\n—Janus, speaker of the synod').
card_multiverse_id('vedalken archmage'/'MRD', '46557').

card_in_set('vermiculos', 'MRD').
card_original_type('vermiculos'/'MRD', 'Creature — Horror').
card_original_text('vermiculos'/'MRD', 'Whenever an artifact comes into play, Vermiculos gets +4/+4 until end of turn.').
card_first_print('vermiculos', 'MRD').
card_image_name('vermiculos'/'MRD', 'vermiculos').
card_uid('vermiculos'/'MRD', 'MRD:Vermiculos:vermiculos').
card_rarity('vermiculos'/'MRD', 'Rare').
card_artist('vermiculos'/'MRD', 'Daren Bader').
card_number('vermiculos'/'MRD', '80').
card_flavor_text('vermiculos'/'MRD', 'Mirrodin\'s artificial environment requires its own predators, scavengers, and senseless forces of nature.').
card_multiverse_id('vermiculos'/'MRD', '47936').

card_in_set('viridian joiner', 'MRD').
card_original_type('viridian joiner'/'MRD', 'Creature — Elf Druid').
card_original_text('viridian joiner'/'MRD', '{T}: Add an amount of {G} to your mana pool equal to Viridian Joiner\'s power.').
card_first_print('viridian joiner', 'MRD').
card_image_name('viridian joiner'/'MRD', 'viridian joiner').
card_uid('viridian joiner'/'MRD', 'MRD:Viridian Joiner:viridian joiner').
card_rarity('viridian joiner'/'MRD', 'Common').
card_artist('viridian joiner'/'MRD', 'Daren Bader').
card_number('viridian joiner'/'MRD', '138').
card_flavor_text('viridian joiner'/'MRD', '\"The Tangle reaches out to itself constantly, forming new connections. We must follow its example.\"').
card_multiverse_id('viridian joiner'/'MRD', '46114').

card_in_set('viridian longbow', 'MRD').
card_original_type('viridian longbow'/'MRD', 'Artifact — Equipment').
card_original_text('viridian longbow'/'MRD', 'Equipped creature has \"{T}: This creature deals 1 damage to target creature or player.\"\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('viridian longbow', 'MRD').
card_image_name('viridian longbow'/'MRD', 'viridian longbow').
card_uid('viridian longbow'/'MRD', 'MRD:Viridian Longbow:viridian longbow').
card_rarity('viridian longbow'/'MRD', 'Common').
card_artist('viridian longbow'/'MRD', 'Jeremy Jarvis').
card_number('viridian longbow'/'MRD', '270').
card_multiverse_id('viridian longbow'/'MRD', '46018').

card_in_set('viridian shaman', 'MRD').
card_original_type('viridian shaman'/'MRD', 'Creature — Elf Shaman').
card_original_text('viridian shaman'/'MRD', 'When Viridian Shaman comes into play, destroy target artifact.').
card_first_print('viridian shaman', 'MRD').
card_image_name('viridian shaman'/'MRD', 'viridian shaman').
card_uid('viridian shaman'/'MRD', 'MRD:Viridian Shaman:viridian shaman').
card_rarity('viridian shaman'/'MRD', 'Uncommon').
card_artist('viridian shaman'/'MRD', 'Scott M. Fischer').
card_number('viridian shaman'/'MRD', '139').
card_flavor_text('viridian shaman'/'MRD', 'Because the elves are so in touch with Mirrodin\'s nature, they understand best how to dismantle it.').
card_multiverse_id('viridian shaman'/'MRD', '46119').

card_in_set('vorrac battlehorns', 'MRD').
card_original_type('vorrac battlehorns'/'MRD', 'Artifact — Equipment').
card_original_text('vorrac battlehorns'/'MRD', 'Equipped creature has trample and can\'t be blocked by more than one creature.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('vorrac battlehorns', 'MRD').
card_image_name('vorrac battlehorns'/'MRD', 'vorrac battlehorns').
card_uid('vorrac battlehorns'/'MRD', 'MRD:Vorrac Battlehorns:vorrac battlehorns').
card_rarity('vorrac battlehorns'/'MRD', 'Common').
card_artist('vorrac battlehorns'/'MRD', 'David Martin').
card_number('vorrac battlehorns'/'MRD', '271').
card_multiverse_id('vorrac battlehorns'/'MRD', '46053').

card_in_set('vulshok battlegear', 'MRD').
card_original_type('vulshok battlegear'/'MRD', 'Artifact — Equipment').
card_original_text('vulshok battlegear'/'MRD', 'Equipped creature gets +3/+3.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('vulshok battlegear', 'MRD').
card_image_name('vulshok battlegear'/'MRD', 'vulshok battlegear').
card_uid('vulshok battlegear'/'MRD', 'MRD:Vulshok Battlegear:vulshok battlegear').
card_rarity('vulshok battlegear'/'MRD', 'Uncommon').
card_artist('vulshok battlegear'/'MRD', 'Kevin Dobler').
card_number('vulshok battlegear'/'MRD', '272').
card_multiverse_id('vulshok battlegear'/'MRD', '48121').

card_in_set('vulshok battlemaster', 'MRD').
card_original_type('vulshok battlemaster'/'MRD', 'Creature — Human Warrior').
card_original_text('vulshok battlemaster'/'MRD', 'Haste\nWhen Vulshok Battlemaster comes into play, attach all Equipment in play to it. (Control of the Equipment doesn\'t change.)').
card_first_print('vulshok battlemaster', 'MRD').
card_image_name('vulshok battlemaster'/'MRD', 'vulshok battlemaster').
card_uid('vulshok battlemaster'/'MRD', 'MRD:Vulshok Battlemaster:vulshok battlemaster').
card_rarity('vulshok battlemaster'/'MRD', 'Rare').
card_artist('vulshok battlemaster'/'MRD', 'Kev Walker').
card_number('vulshok battlemaster'/'MRD', '110').
card_flavor_text('vulshok battlemaster'/'MRD', '\"I could demonstrate how the leonin sunsplicer works, but then you\'d be too dead to buy one.\"').
card_multiverse_id('vulshok battlemaster'/'MRD', '48125').

card_in_set('vulshok berserker', 'MRD').
card_original_type('vulshok berserker'/'MRD', 'Creature — Human Berserker').
card_original_text('vulshok berserker'/'MRD', 'Haste').
card_first_print('vulshok berserker', 'MRD').
card_image_name('vulshok berserker'/'MRD', 'vulshok berserker').
card_uid('vulshok berserker'/'MRD', 'MRD:Vulshok Berserker:vulshok berserker').
card_rarity('vulshok berserker'/'MRD', 'Common').
card_artist('vulshok berserker'/'MRD', 'Pete Venters').
card_number('vulshok berserker'/'MRD', '111').
card_flavor_text('vulshok berserker'/'MRD', 'He experiences every emotion with passion and repays every slight with vengeance.').
card_multiverse_id('vulshok berserker'/'MRD', '5585').

card_in_set('vulshok gauntlets', 'MRD').
card_original_type('vulshok gauntlets'/'MRD', 'Artifact — Equipment').
card_original_text('vulshok gauntlets'/'MRD', 'Equipped creature gets +4/+2 and doesn\'t untap during its controller\'s untap step.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('vulshok gauntlets', 'MRD').
card_image_name('vulshok gauntlets'/'MRD', 'vulshok gauntlets').
card_uid('vulshok gauntlets'/'MRD', 'MRD:Vulshok Gauntlets:vulshok gauntlets').
card_rarity('vulshok gauntlets'/'MRD', 'Common').
card_artist('vulshok gauntlets'/'MRD', 'Richard Sardinha').
card_number('vulshok gauntlets'/'MRD', '273').
card_multiverse_id('vulshok gauntlets'/'MRD', '48397').

card_in_set('wail of the nim', 'MRD').
card_original_type('wail of the nim'/'MRD', 'Instant').
card_original_text('wail of the nim'/'MRD', 'Choose one Regenerate each creature you control; or Wail of the Nim deals 1 damage to each creature and each player.\nEntwine {B} (Choose both if you pay the entwine cost.)').
card_first_print('wail of the nim', 'MRD').
card_image_name('wail of the nim'/'MRD', 'wail of the nim').
card_uid('wail of the nim'/'MRD', 'MRD:Wail of the Nim:wail of the nim').
card_rarity('wail of the nim'/'MRD', 'Common').
card_artist('wail of the nim'/'MRD', 'John Matson').
card_number('wail of the nim'/'MRD', '81').
card_multiverse_id('wail of the nim'/'MRD', '48105').

card_in_set('wall of blood', 'MRD').
card_original_type('wall of blood'/'MRD', 'Creature — Wall').
card_original_text('wall of blood'/'MRD', '(Walls can\'t attack.)\nPay 1 life: Wall of Blood gets +1/+1 until end of turn.').
card_first_print('wall of blood', 'MRD').
card_image_name('wall of blood'/'MRD', 'wall of blood').
card_uid('wall of blood'/'MRD', 'MRD:Wall of Blood:wall of blood').
card_rarity('wall of blood'/'MRD', 'Uncommon').
card_artist('wall of blood'/'MRD', 'Tony Szczudlo').
card_number('wall of blood'/'MRD', '82').
card_flavor_text('wall of blood'/'MRD', 'Blood is thicker than mortar.').
card_multiverse_id('wall of blood'/'MRD', '48184').

card_in_set('wanderguard sentry', 'MRD').
card_original_type('wanderguard sentry'/'MRD', 'Creature — Drone').
card_original_text('wanderguard sentry'/'MRD', 'When Wanderguard Sentry comes into play, look at target opponent\'s hand.').
card_first_print('wanderguard sentry', 'MRD').
card_image_name('wanderguard sentry'/'MRD', 'wanderguard sentry').
card_uid('wanderguard sentry'/'MRD', 'MRD:Wanderguard Sentry:wanderguard sentry').
card_rarity('wanderguard sentry'/'MRD', 'Common').
card_artist('wanderguard sentry'/'MRD', 'Luca Zontini').
card_number('wanderguard sentry'/'MRD', '56').
card_flavor_text('wanderguard sentry'/'MRD', 'Created by the vedalken to guard Lumengrid, the drones\' empty eyes look beyond the Quicksilver Sea.').
card_multiverse_id('wanderguard sentry'/'MRD', '48439').

card_in_set('war elemental', 'MRD').
card_original_type('war elemental'/'MRD', 'Creature — Elemental').
card_original_text('war elemental'/'MRD', 'When War Elemental comes into play, sacrifice it unless an opponent was dealt damage this turn.\nWhenever damage is dealt to an opponent, put that many +1/+1 counters on War Elemental.').
card_first_print('war elemental', 'MRD').
card_image_name('war elemental'/'MRD', 'war elemental').
card_uid('war elemental'/'MRD', 'MRD:War Elemental:war elemental').
card_rarity('war elemental'/'MRD', 'Rare').
card_artist('war elemental'/'MRD', 'Anthony S. Waters').
card_number('war elemental'/'MRD', '112').
card_multiverse_id('war elemental'/'MRD', '43614').

card_in_set('welding jar', 'MRD').
card_original_type('welding jar'/'MRD', 'Artifact').
card_original_text('welding jar'/'MRD', 'Sacrifice Welding Jar: Regenerate target artifact.').
card_first_print('welding jar', 'MRD').
card_image_name('welding jar'/'MRD', 'welding jar').
card_uid('welding jar'/'MRD', 'MRD:Welding Jar:welding jar').
card_rarity('welding jar'/'MRD', 'Common').
card_artist('welding jar'/'MRD', 'Mark Brill').
card_number('welding jar'/'MRD', '274').
card_flavor_text('welding jar'/'MRD', 'The wires crawl over broken metal and heat themselves to melting, filling cracks quickly and efficiently.').
card_multiverse_id('welding jar'/'MRD', '48328').

card_in_set('wizard replica', 'MRD').
card_original_type('wizard replica'/'MRD', 'Artifact Creature — Wizard').
card_original_text('wizard replica'/'MRD', 'Flying\n{U}, Sacrifice Wizard Replica: Counter target spell unless its controller pays {2}.').
card_first_print('wizard replica', 'MRD').
card_image_name('wizard replica'/'MRD', 'wizard replica').
card_uid('wizard replica'/'MRD', 'MRD:Wizard Replica:wizard replica').
card_rarity('wizard replica'/'MRD', 'Common').
card_artist('wizard replica'/'MRD', 'Carl Critchlow').
card_number('wizard replica'/'MRD', '275').
card_flavor_text('wizard replica'/'MRD', 'It responds with unnatural precision.').
card_multiverse_id('wizard replica'/'MRD', '46033').

card_in_set('woebearer', 'MRD').
card_original_type('woebearer'/'MRD', 'Creature — Zombie').
card_original_text('woebearer'/'MRD', 'Fear\nWhenever Woebearer deals combat damage to a player, you may return target creature card from your graveyard to your hand.').
card_first_print('woebearer', 'MRD').
card_image_name('woebearer'/'MRD', 'woebearer').
card_uid('woebearer'/'MRD', 'MRD:Woebearer:woebearer').
card_rarity('woebearer'/'MRD', 'Uncommon').
card_artist('woebearer'/'MRD', 'Matt Thompson').
card_number('woebearer'/'MRD', '83').
card_multiverse_id('woebearer'/'MRD', '46074').

card_in_set('worldslayer', 'MRD').
card_original_type('worldslayer'/'MRD', 'Artifact — Equipment').
card_original_text('worldslayer'/'MRD', 'Whenever equipped creature deals combat damage to a player, destroy all permanents other than Worldslayer.\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery. This card comes into play unattached and stays in play if the creature leaves play.)').
card_first_print('worldslayer', 'MRD').
card_image_name('worldslayer'/'MRD', 'worldslayer').
card_uid('worldslayer'/'MRD', 'MRD:Worldslayer:worldslayer').
card_rarity('worldslayer'/'MRD', 'Rare').
card_artist('worldslayer'/'MRD', 'Greg Staples').
card_number('worldslayer'/'MRD', '276').
card_multiverse_id('worldslayer'/'MRD', '46791').

card_in_set('wrench mind', 'MRD').
card_original_type('wrench mind'/'MRD', 'Sorcery').
card_original_text('wrench mind'/'MRD', 'Target player discards two cards from his or her hand unless he or she discards an artifact card from his or her hand.').
card_first_print('wrench mind', 'MRD').
card_image_name('wrench mind'/'MRD', 'wrench mind').
card_uid('wrench mind'/'MRD', 'MRD:Wrench Mind:wrench mind').
card_rarity('wrench mind'/'MRD', 'Common').
card_artist('wrench mind'/'MRD', 'Pete Venters').
card_number('wrench mind'/'MRD', '84').
card_flavor_text('wrench mind'/'MRD', 'What is the sound of one head snapping?').
card_multiverse_id('wrench mind'/'MRD', '45990').

card_in_set('wurmskin forger', 'MRD').
card_original_type('wurmskin forger'/'MRD', 'Creature — Elf Warrior').
card_original_text('wurmskin forger'/'MRD', 'When Wurmskin Forger comes into play, distribute three +1/+1 counters among any number of target creatures.').
card_first_print('wurmskin forger', 'MRD').
card_image_name('wurmskin forger'/'MRD', 'wurmskin forger').
card_uid('wurmskin forger'/'MRD', 'MRD:Wurmskin Forger:wurmskin forger').
card_rarity('wurmskin forger'/'MRD', 'Common').
card_artist('wurmskin forger'/'MRD', 'Justin Sweet').
card_number('wurmskin forger'/'MRD', '140').
card_flavor_text('wurmskin forger'/'MRD', 'It takes three weeks for a patrol of hunters to down a slagwurm. It takes just as long to make a single cut in its hide.').
card_multiverse_id('wurmskin forger'/'MRD', '48599').

card_in_set('yotian soldier', 'MRD').
card_original_type('yotian soldier'/'MRD', 'Artifact Creature — Soldier').
card_original_text('yotian soldier'/'MRD', 'Attacking doesn\'t cause Yotian Soldier to tap.').
card_image_name('yotian soldier'/'MRD', 'yotian soldier').
card_uid('yotian soldier'/'MRD', 'MRD:Yotian Soldier:yotian soldier').
card_rarity('yotian soldier'/'MRD', 'Common').
card_artist('yotian soldier'/'MRD', 'Luca Zontini').
card_number('yotian soldier'/'MRD', '277').
card_flavor_text('yotian soldier'/'MRD', 'Poets dream the verses of otherworldly stories. Artificers dream the blueprints of otherplanar artifacts.').
card_multiverse_id('yotian soldier'/'MRD', '46026').
