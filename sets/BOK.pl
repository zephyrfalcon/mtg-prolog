% Betrayers of Kamigawa

set('BOK').
set_name('BOK', 'Betrayers of Kamigawa').
set_release_date('BOK', '2005-02-04').
set_border('BOK', 'black').
set_type('BOK', 'expansion').
set_block('BOK', 'Kamigawa').

card_in_set('akki blizzard-herder', 'BOK').
card_original_type('akki blizzard-herder'/'BOK', 'Creature — Goblin Shaman').
card_original_text('akki blizzard-herder'/'BOK', 'When Akki Blizzard-Herder is put into a graveyard from play, each player sacrifices a land.').
card_first_print('akki blizzard-herder', 'BOK').
card_image_name('akki blizzard-herder'/'BOK', 'akki blizzard-herder').
card_uid('akki blizzard-herder'/'BOK', 'BOK:Akki Blizzard-Herder:akki blizzard-herder').
card_rarity('akki blizzard-herder'/'BOK', 'Common').
card_artist('akki blizzard-herder'/'BOK', 'Pete Venters').
card_number('akki blizzard-herder'/'BOK', '91').
card_flavor_text('akki blizzard-herder'/'BOK', '\"It remains unclear whether the akki shamans could in fact create storms or simply predicted them.\"\n—The History of Kamigawa').
card_multiverse_id('akki blizzard-herder'/'BOK', '74428').

card_in_set('akki raider', 'BOK').
card_original_type('akki raider'/'BOK', 'Creature — Goblin Warrior').
card_original_text('akki raider'/'BOK', 'Whenever a land is put into a graveyard from play, Akki Raider gets +1/+0 until end of turn.').
card_first_print('akki raider', 'BOK').
card_image_name('akki raider'/'BOK', 'akki raider').
card_uid('akki raider'/'BOK', 'BOK:Akki Raider:akki raider').
card_rarity('akki raider'/'BOK', 'Uncommon').
card_artist('akki raider'/'BOK', 'Martina Pilcerova').
card_number('akki raider'/'BOK', '92').
card_flavor_text('akki raider'/'BOK', 'Akki were very territorial, attacking and destroying any settlements that came too close to their mountain homes. The more successful their raids, the bolder they became.').
card_multiverse_id('akki raider'/'BOK', '74596').

card_in_set('ashen monstrosity', 'BOK').
card_original_type('ashen monstrosity'/'BOK', 'Creature — Spirit').
card_original_text('ashen monstrosity'/'BOK', 'Haste\nAshen Monstrosity attacks each turn if able.').
card_first_print('ashen monstrosity', 'BOK').
card_image_name('ashen monstrosity'/'BOK', 'ashen monstrosity').
card_uid('ashen monstrosity'/'BOK', 'BOK:Ashen Monstrosity:ashen monstrosity').
card_rarity('ashen monstrosity'/'BOK', 'Uncommon').
card_artist('ashen monstrosity'/'BOK', 'Chris Appelhans').
card_number('ashen monstrosity'/'BOK', '93').
card_flavor_text('ashen monstrosity'/'BOK', 'There was a dull thunder at the vanguard, then shocked cries in the midst of the column. Moments later, Takada, last of the rearguard, faced the kami alone.').
card_multiverse_id('ashen monstrosity'/'BOK', '74469').

card_in_set('aura barbs', 'BOK').
card_original_type('aura barbs'/'BOK', 'Instant — Arcane').
card_original_text('aura barbs'/'BOK', 'Each enchantment deals 2 damage to its controller, then each enchantment enchanting a creature deals 2 damage to the creature it\'s enchanting.').
card_first_print('aura barbs', 'BOK').
card_image_name('aura barbs'/'BOK', 'aura barbs').
card_uid('aura barbs'/'BOK', 'BOK:Aura Barbs:aura barbs').
card_rarity('aura barbs'/'BOK', 'Uncommon').
card_artist('aura barbs'/'BOK', 'Aleksi Briclot').
card_number('aura barbs'/'BOK', '94').
card_multiverse_id('aura barbs'/'BOK', '74522').

card_in_set('azamuki, treachery incarnate', 'BOK').
card_original_type('azamuki, treachery incarnate'/'BOK', 'Creature — Human Warrior').
card_original_text('azamuki, treachery incarnate'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Cunning Bandit.\nAt end of turn, if there are two or more ki counters on Cunning Bandit, you may flip it.\n----\nAzamuki, Treachery Incarnate\nLegendary Creature Spirit\n5/2\nRemove a ki counter from Azamuki, Treachery Incarnate: Gain control of target creature until end of turn.').
card_first_print('azamuki, treachery incarnate', 'BOK').
card_image_name('azamuki, treachery incarnate'/'BOK', 'azamuki, treachery incarnate').
card_uid('azamuki, treachery incarnate'/'BOK', 'BOK:Azamuki, Treachery Incarnate:azamuki, treachery incarnate').
card_rarity('azamuki, treachery incarnate'/'BOK', 'Uncommon').
card_artist('azamuki, treachery incarnate'/'BOK', 'Paolo Parente').
card_number('azamuki, treachery incarnate'/'BOK', '99b').
card_multiverse_id('azamuki, treachery incarnate'/'BOK', '74671').

card_in_set('baku altar', 'BOK').
card_original_type('baku altar'/'BOK', 'Artifact').
card_original_text('baku altar'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Baku Altar.\n{2}, {T}, Remove a ki counter from Baku Altar: Put a 1/1 colorless Spirit creature token into play.').
card_first_print('baku altar', 'BOK').
card_image_name('baku altar'/'BOK', 'baku altar').
card_uid('baku altar'/'BOK', 'BOK:Baku Altar:baku altar').
card_rarity('baku altar'/'BOK', 'Rare').
card_artist('baku altar'/'BOK', 'Edward P. Beard, Jr.').
card_number('baku altar'/'BOK', '152').
card_multiverse_id('baku altar'/'BOK', '74422').

card_in_set('bile urchin', 'BOK').
card_original_type('bile urchin'/'BOK', 'Creature — Spirit').
card_original_text('bile urchin'/'BOK', 'Sacrifice Bile Urchin: Target player loses 1 life.').
card_first_print('bile urchin', 'BOK').
card_image_name('bile urchin'/'BOK', 'bile urchin').
card_uid('bile urchin'/'BOK', 'BOK:Bile Urchin:bile urchin').
card_rarity('bile urchin'/'BOK', 'Common').
card_artist('bile urchin'/'BOK', 'Dany Orizio').
card_number('bile urchin'/'BOK', '61').
card_flavor_text('bile urchin'/'BOK', '\"The two youths argued and tempers flared. One youth opened his mouth to utter a vile curse, but what he spat out instead was a kami of poison and filth.\"\n—Sensei Golden-Tail').
card_multiverse_id('bile urchin'/'BOK', '74427').

card_in_set('blademane baku', 'BOK').
card_original_type('blademane baku'/'BOK', 'Creature — Spirit').
card_original_text('blademane baku'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Blademane Baku.\n{1}, Remove X ki counters from Blademane Baku: For each counter removed, Blademane Baku gets +2/+0 until end of turn.').
card_first_print('blademane baku', 'BOK').
card_image_name('blademane baku'/'BOK', 'blademane baku').
card_uid('blademane baku'/'BOK', 'BOK:Blademane Baku:blademane baku').
card_rarity('blademane baku'/'BOK', 'Common').
card_artist('blademane baku'/'BOK', 'Edward P. Beard, Jr.').
card_number('blademane baku'/'BOK', '95').
card_multiverse_id('blademane baku'/'BOK', '74535').

card_in_set('blazing shoal', 'BOK').
card_original_type('blazing shoal'/'BOK', 'Instant — Arcane').
card_original_text('blazing shoal'/'BOK', 'You may remove a red card with converted mana cost X in your hand from the game rather than pay Blazing Shoal\'s mana cost.\nTarget creature gets +X/+0 until end of turn.').
card_first_print('blazing shoal', 'BOK').
card_image_name('blazing shoal'/'BOK', 'blazing shoal').
card_uid('blazing shoal'/'BOK', 'BOK:Blazing Shoal:blazing shoal').
card_rarity('blazing shoal'/'BOK', 'Rare').
card_artist('blazing shoal'/'BOK', 'Glen Angus').
card_number('blazing shoal'/'BOK', '96').
card_multiverse_id('blazing shoal'/'BOK', '74441').

card_in_set('blessing of leeches', 'BOK').
card_original_type('blessing of leeches'/'BOK', 'Enchant Creature').
card_original_text('blessing of leeches'/'BOK', 'You may play Blessing of Leeches any time you could play an instant.\nAt the beginning of your upkeep, you lose 1 life.\n{0}: Regenerate enchanted creature.').
card_first_print('blessing of leeches', 'BOK').
card_image_name('blessing of leeches'/'BOK', 'blessing of leeches').
card_uid('blessing of leeches'/'BOK', 'BOK:Blessing of Leeches:blessing of leeches').
card_rarity('blessing of leeches'/'BOK', 'Common').
card_artist('blessing of leeches'/'BOK', 'Rebecca Guay').
card_number('blessing of leeches'/'BOK', '62').
card_multiverse_id('blessing of leeches'/'BOK', '74417').

card_in_set('blinding powder', 'BOK').
card_original_type('blinding powder'/'BOK', 'Artifact — Equipment').
card_original_text('blinding powder'/'BOK', 'Equipped creature has \"Unattach Blinding Powder: Prevent all combat damage that would be dealt to this creature this turn.\"\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('blinding powder', 'BOK').
card_image_name('blinding powder'/'BOK', 'blinding powder').
card_uid('blinding powder'/'BOK', 'BOK:Blinding Powder:blinding powder').
card_rarity('blinding powder'/'BOK', 'Uncommon').
card_artist('blinding powder'/'BOK', 'Greg Hildebrandt').
card_number('blinding powder'/'BOK', '153').
card_multiverse_id('blinding powder'/'BOK', '81989').

card_in_set('body of jukai', 'BOK').
card_original_type('body of jukai'/'BOK', 'Creature — Spirit').
card_original_text('body of jukai'/'BOK', 'Trample\nSoulshift 8 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 8 or less from your graveyard to your hand.)').
card_first_print('body of jukai', 'BOK').
card_image_name('body of jukai'/'BOK', 'body of jukai').
card_uid('body of jukai'/'BOK', 'BOK:Body of Jukai:body of jukai').
card_rarity('body of jukai'/'BOK', 'Uncommon').
card_artist('body of jukai'/'BOK', 'Luca Zontini').
card_number('body of jukai'/'BOK', '121').
card_multiverse_id('body of jukai'/'BOK', '74613').

card_in_set('budoka pupil', 'BOK').
card_original_type('budoka pupil'/'BOK', 'Creature — Human Monk').
card_original_text('budoka pupil'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Budoka Pupil.\nAt end of turn, if there are two or more ki counters on Budoka Pupil, you may flip it.\n----\nIchiga, Who Topples Oaks\nLegendary Creature Spirit\n4/3\nTrample\nRemove a ki counter from Ichiga, Who Topples Oaks: Target creature gets +2/+2 until end of turn.').
card_image_name('budoka pupil'/'BOK', 'budoka pupil').
card_uid('budoka pupil'/'BOK', 'BOK:Budoka Pupil:budoka pupil').
card_rarity('budoka pupil'/'BOK', 'Uncommon').
card_artist('budoka pupil'/'BOK', 'Shishizaru').
card_number('budoka pupil'/'BOK', '122a').
card_multiverse_id('budoka pupil'/'BOK', '74536').

card_in_set('call for blood', 'BOK').
card_original_type('call for blood'/'BOK', 'Instant — Arcane').
card_original_text('call for blood'/'BOK', 'As an additional cost to play Call for Blood, sacrifice a creature.\nTarget creature gets -X/-X until end of turn, where X is the sacrificed creature\'s power.').
card_first_print('call for blood', 'BOK').
card_image_name('call for blood'/'BOK', 'call for blood').
card_uid('call for blood'/'BOK', 'BOK:Call for Blood:call for blood').
card_rarity('call for blood'/'BOK', 'Common').
card_artist('call for blood'/'BOK', 'Carl Critchlow').
card_number('call for blood'/'BOK', '63').
card_multiverse_id('call for blood'/'BOK', '74616').

card_in_set('callow jushi', 'BOK').
card_original_type('callow jushi'/'BOK', 'Creature — Human Wizard').
card_original_text('callow jushi'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Callow Jushi.\nAt end of turn, if there are two or more ki counters on Callow Jushi, you may flip it.\n----\nJaraku the Interloper\nLegendary Creature Spirit\n3/4\nRemove a ki counter from Jaraku the Interloper: Counter target spell unless its controller pays {2}.').
card_first_print('callow jushi', 'BOK').
card_image_name('callow jushi'/'BOK', 'callow jushi').
card_uid('callow jushi'/'BOK', 'BOK:Callow Jushi:callow jushi').
card_rarity('callow jushi'/'BOK', 'Uncommon').
card_artist('callow jushi'/'BOK', 'Tsutomu Kawade').
card_number('callow jushi'/'BOK', '31a').
card_multiverse_id('callow jushi'/'BOK', '74489').

card_in_set('child of thorns', 'BOK').
card_original_type('child of thorns'/'BOK', 'Creature — Spirit').
card_original_text('child of thorns'/'BOK', 'Sacrifice Child of Thorns: Target creature gets +1/+1 until end of turn.').
card_first_print('child of thorns', 'BOK').
card_image_name('child of thorns'/'BOK', 'child of thorns').
card_uid('child of thorns'/'BOK', 'BOK:Child of Thorns:child of thorns').
card_rarity('child of thorns'/'BOK', 'Common').
card_artist('child of thorns'/'BOK', 'Jeff Easley').
card_number('child of thorns'/'BOK', '123').
card_flavor_text('child of thorns'/'BOK', '\"The soratami scoff at the perils of Jukai, calling the forest an ‘unruly garden.\' Perhaps we should send them a rose such as this.\"\n—Dokai, Weaver of Life').
card_multiverse_id('child of thorns'/'BOK', '74461').

card_in_set('chisei, heart of oceans', 'BOK').
card_original_type('chisei, heart of oceans'/'BOK', 'Legendary Creature — Spirit').
card_original_text('chisei, heart of oceans'/'BOK', 'Flying\nAt the beginning of your upkeep, sacrifice Chisei, Heart of Oceans unless you remove a counter from a permanent you control.').
card_first_print('chisei, heart of oceans', 'BOK').
card_image_name('chisei, heart of oceans'/'BOK', 'chisei, heart of oceans').
card_uid('chisei, heart of oceans'/'BOK', 'BOK:Chisei, Heart of Oceans:chisei, heart of oceans').
card_rarity('chisei, heart of oceans'/'BOK', 'Rare').
card_artist('chisei, heart of oceans'/'BOK', 'Matt Cavotta').
card_number('chisei, heart of oceans'/'BOK', '32').
card_multiverse_id('chisei, heart of oceans'/'BOK', '74096').

card_in_set('clash of realities', 'BOK').
card_original_type('clash of realities'/'BOK', 'Enchantment').
card_original_text('clash of realities'/'BOK', 'All Spirits have \"When this creature comes into play, you may have it deal 3 damage to target non-Spirit creature.\"\nAll non-Spirit creatures have \"When this creature comes into play, you may have it deal 3 damage to target Spirit.\"').
card_first_print('clash of realities', 'BOK').
card_image_name('clash of realities'/'BOK', 'clash of realities').
card_uid('clash of realities'/'BOK', 'BOK:Clash of Realities:clash of realities').
card_rarity('clash of realities'/'BOK', 'Rare').
card_artist('clash of realities'/'BOK', 'Jim Nelson').
card_number('clash of realities'/'BOK', '97').
card_multiverse_id('clash of realities'/'BOK', '74598').

card_in_set('crack the earth', 'BOK').
card_original_type('crack the earth'/'BOK', 'Sorcery — Arcane').
card_original_text('crack the earth'/'BOK', 'Each player sacrifices a permanent.').
card_first_print('crack the earth', 'BOK').
card_image_name('crack the earth'/'BOK', 'crack the earth').
card_uid('crack the earth'/'BOK', 'BOK:Crack the Earth:crack the earth').
card_rarity('crack the earth'/'BOK', 'Common').
card_artist('crack the earth'/'BOK', 'Wayne Reynolds').
card_number('crack the earth'/'BOK', '98').
card_flavor_text('crack the earth'/'BOK', '\"As the war progressed, the destruction the kami caused became more widespread and less predictable.\"\n—Observations of the Kami War').
card_multiverse_id('crack the earth'/'BOK', '74480').

card_in_set('crawling filth', 'BOK').
card_original_type('crawling filth'/'BOK', 'Creature — Spirit').
card_original_text('crawling filth'/'BOK', 'Fear\nSoulshift 5 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_first_print('crawling filth', 'BOK').
card_image_name('crawling filth'/'BOK', 'crawling filth').
card_uid('crawling filth'/'BOK', 'BOK:Crawling Filth:crawling filth').
card_rarity('crawling filth'/'BOK', 'Common').
card_artist('crawling filth'/'BOK', 'Martina Pilcerova').
card_number('crawling filth'/'BOK', '64').
card_multiverse_id('crawling filth'/'BOK', '74032').

card_in_set('cunning bandit', 'BOK').
card_original_type('cunning bandit'/'BOK', 'Creature — Human Warrior').
card_original_text('cunning bandit'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Cunning Bandit.\nAt end of turn, if there are two or more ki counters on Cunning Bandit, you may flip it.\n----\nAzamuki, Treachery Incarnate\nLegendary Creature Spirit\n5/2\nRemove a ki counter from Azamuki, Treachery Incarnate: Gain control of target creature until end of turn.').
card_first_print('cunning bandit', 'BOK').
card_image_name('cunning bandit'/'BOK', 'cunning bandit').
card_uid('cunning bandit'/'BOK', 'BOK:Cunning Bandit:cunning bandit').
card_rarity('cunning bandit'/'BOK', 'Uncommon').
card_artist('cunning bandit'/'BOK', 'Paolo Parente').
card_number('cunning bandit'/'BOK', '99a').
card_multiverse_id('cunning bandit'/'BOK', '74671').

card_in_set('day of destiny', 'BOK').
card_original_type('day of destiny'/'BOK', 'Legendary Enchantment').
card_original_text('day of destiny'/'BOK', 'Legendary creatures you control get +2/+2.').
card_first_print('day of destiny', 'BOK').
card_image_name('day of destiny'/'BOK', 'day of destiny').
card_uid('day of destiny'/'BOK', 'BOK:Day of Destiny:day of destiny').
card_rarity('day of destiny'/'BOK', 'Rare').
card_artist('day of destiny'/'BOK', 'Daren Bader').
card_number('day of destiny'/'BOK', '1').
card_flavor_text('day of destiny'/'BOK', '\"Rise like the sun, stand like the mountain, charge like the lion, die as a hero.\"\n—General Takeno').
card_multiverse_id('day of destiny'/'BOK', '74517').

card_in_set('disrupting shoal', 'BOK').
card_original_type('disrupting shoal'/'BOK', 'Instant — Arcane').
card_original_text('disrupting shoal'/'BOK', 'You may remove a blue card with converted mana cost X in your hand from the game rather than pay Disrupting Shoal\'s mana cost.\nCounter target spell if its converted mana cost is X.').
card_first_print('disrupting shoal', 'BOK').
card_image_name('disrupting shoal'/'BOK', 'disrupting shoal').
card_uid('disrupting shoal'/'BOK', 'BOK:Disrupting Shoal:disrupting shoal').
card_rarity('disrupting shoal'/'BOK', 'Rare').
card_artist('disrupting shoal'/'BOK', 'Scott M. Fischer').
card_number('disrupting shoal'/'BOK', '33').
card_multiverse_id('disrupting shoal'/'BOK', '74128').

card_in_set('empty-shrine kannushi', 'BOK').
card_original_type('empty-shrine kannushi'/'BOK', 'Creature — Human Cleric').
card_original_text('empty-shrine kannushi'/'BOK', 'Empty-Shrine Kannushi has protection from the colors of permanents you control.').
card_first_print('empty-shrine kannushi', 'BOK').
card_image_name('empty-shrine kannushi'/'BOK', 'empty-shrine kannushi').
card_uid('empty-shrine kannushi'/'BOK', 'BOK:Empty-Shrine Kannushi:empty-shrine kannushi').
card_rarity('empty-shrine kannushi'/'BOK', 'Uncommon').
card_artist('empty-shrine kannushi'/'BOK', 'Ron Spears').
card_number('empty-shrine kannushi'/'BOK', '2').
card_flavor_text('empty-shrine kannushi'/'BOK', '\"Crease the folds, bend the paper, turn the spirits, shield the soul.\"').
card_multiverse_id('empty-shrine kannushi'/'BOK', '74513').

card_in_set('enshrined memories', 'BOK').
card_original_type('enshrined memories'/'BOK', 'Sorcery').
card_original_text('enshrined memories'/'BOK', 'Reveal the top X cards of your library. Put all creature cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_first_print('enshrined memories', 'BOK').
card_image_name('enshrined memories'/'BOK', 'enshrined memories').
card_uid('enshrined memories'/'BOK', 'BOK:Enshrined Memories:enshrined memories').
card_rarity('enshrined memories'/'BOK', 'Rare').
card_artist('enshrined memories'/'BOK', 'Jeff Easley').
card_number('enshrined memories'/'BOK', '124').
card_flavor_text('enshrined memories'/'BOK', '\"The race is run in the mind before the first step is taken.\"\n—Dosan the Falling Leaf').
card_multiverse_id('enshrined memories'/'BOK', '81998').

card_in_set('eradicate', 'BOK').
card_original_type('eradicate'/'BOK', 'Sorcery').
card_original_text('eradicate'/'BOK', 'Remove target nonblack creature from the game. Search its controller\'s graveyard, hand, and library for all cards with the same name as that creature and remove them from the game. That player then shuffles his or her library.').
card_image_name('eradicate'/'BOK', 'eradicate').
card_uid('eradicate'/'BOK', 'BOK:Eradicate:eradicate').
card_rarity('eradicate'/'BOK', 'Uncommon').
card_artist('eradicate'/'BOK', 'Glen Angus').
card_number('eradicate'/'BOK', '65').
card_multiverse_id('eradicate'/'BOK', '74546').

card_in_set('faithful squire', 'BOK').
card_original_type('faithful squire'/'BOK', 'Creature — Human Soldier').
card_original_text('faithful squire'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Faithful Squire.\nAt end of turn, if there are two or more ki counters on Faithful Squire, you may flip it.\n-----\nKaiso, Memory of Loyalty\nLegendary Creature Spirit\n3/4\nFlying\nRemove a ki counter from Kaiso, Memory of Loyalty: Prevent all damage that would be dealt to target creature this turn.').
card_first_print('faithful squire', 'BOK').
card_image_name('faithful squire'/'BOK', 'faithful squire').
card_uid('faithful squire'/'BOK', 'BOK:Faithful Squire:faithful squire').
card_rarity('faithful squire'/'BOK', 'Uncommon').
card_artist('faithful squire'/'BOK', 'Mark Zug').
card_number('faithful squire'/'BOK', '3a').
card_multiverse_id('faithful squire'/'BOK', '74093').

card_in_set('final judgment', 'BOK').
card_original_type('final judgment'/'BOK', 'Sorcery').
card_original_text('final judgment'/'BOK', 'Remove all creatures from the game.').
card_first_print('final judgment', 'BOK').
card_image_name('final judgment'/'BOK', 'final judgment').
card_uid('final judgment'/'BOK', 'BOK:Final Judgment:final judgment').
card_rarity('final judgment'/'BOK', 'Rare').
card_artist('final judgment'/'BOK', 'Kev Walker').
card_number('final judgment'/'BOK', '4').
card_flavor_text('final judgment'/'BOK', '\"The clashing warriors turned to face O-Kagachi, the greatest kami, and their sigh of awe was their last breath.\"\n—Great Battles of Kamigawa').
card_multiverse_id('final judgment'/'BOK', '81991').

card_in_set('first volley', 'BOK').
card_original_type('first volley'/'BOK', 'Instant — Arcane').
card_original_text('first volley'/'BOK', 'First Volley deals 1 damage to target creature and 1 damage to that creature\'s controller.').
card_first_print('first volley', 'BOK').
card_image_name('first volley'/'BOK', 'first volley').
card_uid('first volley'/'BOK', 'BOK:First Volley:first volley').
card_rarity('first volley'/'BOK', 'Common').
card_artist('first volley'/'BOK', 'Glen Angus').
card_number('first volley'/'BOK', '100').
card_flavor_text('first volley'/'BOK', '\"We searched their bodies for signs of the blades that had killed them, but found nothing more than scorched flesh.\"\n—Tender-Hand, kitsune healer').
card_multiverse_id('first volley'/'BOK', '74451').

card_in_set('flames of the blood hand', 'BOK').
card_original_type('flames of the blood hand'/'BOK', 'Instant').
card_original_text('flames of the blood hand'/'BOK', 'Flames of the Blood Hand deals 4 damage to target player. The damage can\'t be prevented. If that player would gain life this turn, that player gains no life instead.').
card_first_print('flames of the blood hand', 'BOK').
card_image_name('flames of the blood hand'/'BOK', 'flames of the blood hand').
card_uid('flames of the blood hand'/'BOK', 'BOK:Flames of the Blood Hand:flames of the blood hand').
card_rarity('flames of the blood hand'/'BOK', 'Uncommon').
card_artist('flames of the blood hand'/'BOK', 'Aleksi Briclot').
card_number('flames of the blood hand'/'BOK', '101').
card_flavor_text('flames of the blood hand'/'BOK', 'Many ogres extracted blood oaths from the oni they summoned. Others simply extracted blood.').
card_multiverse_id('flames of the blood hand'/'BOK', '74625').

card_in_set('floodbringer', 'BOK').
card_original_type('floodbringer'/'BOK', 'Creature — Moonfolk Wizard').
card_original_text('floodbringer'/'BOK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Tap target land.').
card_first_print('floodbringer', 'BOK').
card_image_name('floodbringer'/'BOK', 'floodbringer').
card_uid('floodbringer'/'BOK', 'BOK:Floodbringer:floodbringer').
card_rarity('floodbringer'/'BOK', 'Common').
card_artist('floodbringer'/'BOK', 'Ittoku').
card_number('floodbringer'/'BOK', '34').
card_flavor_text('floodbringer'/'BOK', '\"Such a small thing, a drop of water . . . And yet enough of them together can flood a field, cleanse a mire, or choke a forest.\"').
card_multiverse_id('floodbringer'/'BOK', '74637').

card_in_set('forked-branch garami', 'BOK').
card_original_type('forked-branch garami'/'BOK', 'Creature — Spirit').
card_original_text('forked-branch garami'/'BOK', 'Soulshift 4, soulshift 4 (When this is put into a graveyard from play, you may return up to two target Spirit cards with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('forked-branch garami', 'BOK').
card_image_name('forked-branch garami'/'BOK', 'forked-branch garami').
card_uid('forked-branch garami'/'BOK', 'BOK:Forked-Branch Garami:forked-branch garami').
card_rarity('forked-branch garami'/'BOK', 'Uncommon').
card_artist('forked-branch garami'/'BOK', 'Dany Orizio').
card_number('forked-branch garami'/'BOK', '125').
card_multiverse_id('forked-branch garami'/'BOK', '74410').

card_in_set('frost ogre', 'BOK').
card_original_type('frost ogre'/'BOK', 'Creature — Ogre Warrior').
card_original_text('frost ogre'/'BOK', '').
card_first_print('frost ogre', 'BOK').
card_image_name('frost ogre'/'BOK', 'frost ogre').
card_uid('frost ogre'/'BOK', 'BOK:Frost Ogre:frost ogre').
card_rarity('frost ogre'/'BOK', 'Common').
card_artist('frost ogre'/'BOK', 'Dan Scott').
card_number('frost ogre'/'BOK', '102').
card_flavor_text('frost ogre'/'BOK', 'Mountain ogres allowed blizzards to sheathe them in ice, both to reinforce their armor and to hide their pungent musk from potential prey.').
card_multiverse_id('frost ogre'/'BOK', '74589').

card_in_set('frostling', 'BOK').
card_original_type('frostling'/'BOK', 'Creature — Spirit').
card_original_text('frostling'/'BOK', 'Sacrifice Frostling: Frostling deals 1 damage to target creature.').
card_first_print('frostling', 'BOK').
card_image_name('frostling'/'BOK', 'frostling').
card_uid('frostling'/'BOK', 'BOK:Frostling:frostling').
card_rarity('frostling'/'BOK', 'Common').
card_artist('frostling'/'BOK', 'Carl Critchlow').
card_number('frostling'/'BOK', '103').
card_flavor_text('frostling'/'BOK', 'Its bite will take off more than a toe.').
card_multiverse_id('frostling'/'BOK', '74621').

card_in_set('fumiko the lowblood', 'BOK').
card_original_type('fumiko the lowblood'/'BOK', 'Legendary Creature — Human Samurai').
card_original_text('fumiko the lowblood'/'BOK', 'Fumiko the Lowblood has bushido X, where X is the number of attacking creatures. (When this blocks or becomes blocked, it gets +X/+X until end of turn.)\nCreatures your opponents control attack each turn if able.').
card_first_print('fumiko the lowblood', 'BOK').
card_image_name('fumiko the lowblood'/'BOK', 'fumiko the lowblood').
card_uid('fumiko the lowblood'/'BOK', 'BOK:Fumiko the Lowblood:fumiko the lowblood').
card_rarity('fumiko the lowblood'/'BOK', 'Rare').
card_artist('fumiko the lowblood'/'BOK', 'Michael Sutfin').
card_number('fumiko the lowblood'/'BOK', '104').
card_multiverse_id('fumiko the lowblood'/'BOK', '74534').

card_in_set('genju of the cedars', 'BOK').
card_original_type('genju of the cedars'/'BOK', 'Enchant Forest').
card_original_text('genju of the cedars'/'BOK', '{2}: Enchanted Forest becomes a 4/4 green Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Forest is put into a graveyard, you may return Genju of the Cedars from your graveyard to your hand.').
card_first_print('genju of the cedars', 'BOK').
card_image_name('genju of the cedars'/'BOK', 'genju of the cedars').
card_uid('genju of the cedars'/'BOK', 'BOK:Genju of the Cedars:genju of the cedars').
card_rarity('genju of the cedars'/'BOK', 'Uncommon').
card_artist('genju of the cedars'/'BOK', 'Arnie Swekel').
card_number('genju of the cedars'/'BOK', '126').
card_multiverse_id('genju of the cedars'/'BOK', '74424').

card_in_set('genju of the falls', 'BOK').
card_original_type('genju of the falls'/'BOK', 'Enchant Island').
card_original_text('genju of the falls'/'BOK', '{2}: Enchanted Island becomes a 3/2 blue Spirit creature with flying until end of turn. It\'s still a land.\nWhen enchanted Island is put into a graveyard, you may return Genju of the Falls from your graveyard to your hand.').
card_first_print('genju of the falls', 'BOK').
card_image_name('genju of the falls'/'BOK', 'genju of the falls').
card_uid('genju of the falls'/'BOK', 'BOK:Genju of the Falls:genju of the falls').
card_rarity('genju of the falls'/'BOK', 'Uncommon').
card_artist('genju of the falls'/'BOK', 'Glen Angus').
card_number('genju of the falls'/'BOK', '35').
card_multiverse_id('genju of the falls'/'BOK', '74582').

card_in_set('genju of the fens', 'BOK').
card_original_type('genju of the fens'/'BOK', 'Enchant Swamp').
card_original_text('genju of the fens'/'BOK', '{2}: Until end of turn, enchanted Swamp becomes a 2/2 black Spirit creature with \"{B}: This creature gets +1/+1 until end of turn.\" It\'s still a land.\nWhen enchanted Swamp is put into a graveyard, you may return Genju of the Fens from your graveyard to your hand.').
card_first_print('genju of the fens', 'BOK').
card_image_name('genju of the fens'/'BOK', 'genju of the fens').
card_uid('genju of the fens'/'BOK', 'BOK:Genju of the Fens:genju of the fens').
card_rarity('genju of the fens'/'BOK', 'Uncommon').
card_artist('genju of the fens'/'BOK', 'Tsutomu Kawade').
card_number('genju of the fens'/'BOK', '66').
card_multiverse_id('genju of the fens'/'BOK', '74035').

card_in_set('genju of the fields', 'BOK').
card_original_type('genju of the fields'/'BOK', 'Enchant Plains').
card_original_text('genju of the fields'/'BOK', '{2}: Until end of turn, enchanted Plains becomes a 2/5 white Spirit creature with \"Whenever this creature deals damage, you gain that much life.\" It\'s still a land.\nWhen enchanted Plains is put into a graveyard, you may return Genju of the Fields from your graveyard to your hand.').
card_first_print('genju of the fields', 'BOK').
card_image_name('genju of the fields'/'BOK', 'genju of the fields').
card_uid('genju of the fields'/'BOK', 'BOK:Genju of the Fields:genju of the fields').
card_rarity('genju of the fields'/'BOK', 'Uncommon').
card_artist('genju of the fields'/'BOK', 'Greg Staples').
card_number('genju of the fields'/'BOK', '5').
card_multiverse_id('genju of the fields'/'BOK', '74087').

card_in_set('genju of the realm', 'BOK').
card_original_type('genju of the realm'/'BOK', 'Legendary Enchant Land').
card_original_text('genju of the realm'/'BOK', '{2}: Enchanted land becomes a legendary 8/12 Spirit creature with trample until end of turn. It\'s still a land.\nWhen enchanted land is put into a graveyard, you may return Genju of the Realm from your graveyard to your hand.').
card_first_print('genju of the realm', 'BOK').
card_image_name('genju of the realm'/'BOK', 'genju of the realm').
card_uid('genju of the realm'/'BOK', 'BOK:Genju of the Realm:genju of the realm').
card_rarity('genju of the realm'/'BOK', 'Rare').
card_artist('genju of the realm'/'BOK', 'Scott M. Fischer').
card_number('genju of the realm'/'BOK', '151').
card_multiverse_id('genju of the realm'/'BOK', '75364').

card_in_set('genju of the spires', 'BOK').
card_original_type('genju of the spires'/'BOK', 'Enchant Mountain').
card_original_text('genju of the spires'/'BOK', '{2}: Enchanted Mountain becomes a 6/1 red Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Mountain is put into a graveyard, you may return Genju of the Spires from your graveyard to your hand.').
card_image_name('genju of the spires'/'BOK', 'genju of the spires').
card_uid('genju of the spires'/'BOK', 'BOK:Genju of the Spires:genju of the spires').
card_rarity('genju of the spires'/'BOK', 'Uncommon').
card_artist('genju of the spires'/'BOK', 'Joel Thomas').
card_number('genju of the spires'/'BOK', '105').
card_multiverse_id('genju of the spires'/'BOK', '74666').

card_in_set('gnarled mass', 'BOK').
card_original_type('gnarled mass'/'BOK', 'Creature — Spirit').
card_original_text('gnarled mass'/'BOK', '').
card_first_print('gnarled mass', 'BOK').
card_image_name('gnarled mass'/'BOK', 'gnarled mass').
card_uid('gnarled mass'/'BOK', 'BOK:Gnarled Mass:gnarled mass').
card_rarity('gnarled mass'/'BOK', 'Common').
card_artist('gnarled mass'/'BOK', 'Tony Szczudlo').
card_number('gnarled mass'/'BOK', '127').
card_flavor_text('gnarled mass'/'BOK', '\"On the fifty-seventh day of the Battle of Silk, the bell again tolled in hopes of summoning mortal aid. This time, a new breed of kami rose to answer its call.\"\n—Great Battles of Kamigawa').
card_multiverse_id('gnarled mass'/'BOK', '74435').

card_in_set('goblin cohort', 'BOK').
card_original_type('goblin cohort'/'BOK', 'Creature — Goblin Warrior').
card_original_text('goblin cohort'/'BOK', 'Goblin Cohort can\'t attack unless you\'ve played a creature spell this turn.').
card_first_print('goblin cohort', 'BOK').
card_image_name('goblin cohort'/'BOK', 'goblin cohort').
card_uid('goblin cohort'/'BOK', 'BOK:Goblin Cohort:goblin cohort').
card_rarity('goblin cohort'/'BOK', 'Common').
card_artist('goblin cohort'/'BOK', 'Darrell Riche').
card_number('goblin cohort'/'BOK', '106').
card_flavor_text('goblin cohort'/'BOK', 'Akki shells provided good protection when downhill charging became headlong tumbling.').
card_multiverse_id('goblin cohort'/'BOK', '74433').

card_in_set('gods\' eye, gate to the reikai', 'BOK').
card_original_type('gods\' eye, gate to the reikai'/'BOK', 'Legendary Land').
card_original_text('gods\' eye, gate to the reikai'/'BOK', '{T}: Add {1} to your mana pool.\nWhen Gods\' Eye, Gate to the Reikai is put into a graveyard from play, put a 1/1 colorless Spirit creature token into play.').
card_first_print('gods\' eye, gate to the reikai', 'BOK').
card_image_name('gods\' eye, gate to the reikai'/'BOK', 'gods\' eye, gate to the reikai').
card_uid('gods\' eye, gate to the reikai'/'BOK', 'BOK:Gods\' Eye, Gate to the Reikai:gods\' eye, gate to the reikai').
card_rarity('gods\' eye, gate to the reikai'/'BOK', 'Uncommon').
card_artist('gods\' eye, gate to the reikai'/'BOK', 'John Avon').
card_number('gods\' eye, gate to the reikai'/'BOK', '164').
card_flavor_text('gods\' eye, gate to the reikai'/'BOK', 'Listen at its walls and you will hear the whispers of gods.').
card_multiverse_id('gods\' eye, gate to the reikai'/'BOK', '74668').

card_in_set('goryo\'s vengeance', 'BOK').
card_original_type('goryo\'s vengeance'/'BOK', 'Instant — Arcane').
card_original_text('goryo\'s vengeance'/'BOK', 'Return target legendary creature card from your graveyard to play. That creature gains haste. Remove it from the game at end of turn.\nSplice onto Arcane {2}{B} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('goryo\'s vengeance', 'BOK').
card_image_name('goryo\'s vengeance'/'BOK', 'goryo\'s vengeance').
card_uid('goryo\'s vengeance'/'BOK', 'BOK:Goryo\'s Vengeance:goryo\'s vengeance').
card_rarity('goryo\'s vengeance'/'BOK', 'Rare').
card_artist('goryo\'s vengeance'/'BOK', 'Ittoku').
card_number('goryo\'s vengeance'/'BOK', '67').
card_multiverse_id('goryo\'s vengeance'/'BOK', '74475').

card_in_set('harbinger of spring', 'BOK').
card_original_type('harbinger of spring'/'BOK', 'Creature — Spirit').
card_original_text('harbinger of spring'/'BOK', 'Protection from non-Spirit creatures\nSoulshift 4 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_first_print('harbinger of spring', 'BOK').
card_image_name('harbinger of spring'/'BOK', 'harbinger of spring').
card_uid('harbinger of spring'/'BOK', 'BOK:Harbinger of Spring:harbinger of spring').
card_rarity('harbinger of spring'/'BOK', 'Common').
card_artist('harbinger of spring'/'BOK', 'Cara Mitten').
card_number('harbinger of spring'/'BOK', '128').
card_multiverse_id('harbinger of spring'/'BOK', '74580').

card_in_set('heart of light', 'BOK').
card_original_type('heart of light'/'BOK', 'Enchant Creature').
card_original_text('heart of light'/'BOK', 'Prevent all damage that would be dealt to and dealt by enchanted creature.').
card_first_print('heart of light', 'BOK').
card_image_name('heart of light'/'BOK', 'heart of light').
card_uid('heart of light'/'BOK', 'BOK:Heart of Light:heart of light').
card_rarity('heart of light'/'BOK', 'Common').
card_artist('heart of light'/'BOK', 'Luca Zontini').
card_number('heart of light'/'BOK', '6').
card_flavor_text('heart of light'/'BOK', '\"There is a shining in the mortal heart that even the kami cannot reach.\"\n—Sensei Golden-Tail').
card_multiverse_id('heart of light'/'BOK', '81982').

card_in_set('heartless hidetsugu', 'BOK').
card_original_type('heartless hidetsugu'/'BOK', 'Legendary Creature — Ogre Shaman').
card_original_text('heartless hidetsugu'/'BOK', '{T}: Heartless Hidetsugu deals to each player damage equal to half that player\'s life total, rounded down.').
card_first_print('heartless hidetsugu', 'BOK').
card_image_name('heartless hidetsugu'/'BOK', 'heartless hidetsugu').
card_uid('heartless hidetsugu'/'BOK', 'BOK:Heartless Hidetsugu:heartless hidetsugu').
card_rarity('heartless hidetsugu'/'BOK', 'Rare').
card_artist('heartless hidetsugu'/'BOK', 'Carl Critchlow').
card_number('heartless hidetsugu'/'BOK', '107').
card_flavor_text('heartless hidetsugu'/'BOK', 'Hidetsugu held over a dozen oni in blood oath. At his touch, rocks scorched. At his word, cities burned.').
card_multiverse_id('heartless hidetsugu'/'BOK', '74572').

card_in_set('heed the mists', 'BOK').
card_original_type('heed the mists'/'BOK', 'Sorcery — Arcane').
card_original_text('heed the mists'/'BOK', 'Put the top card of your library into your graveyard, then draw cards equal to that card\'s converted mana cost.').
card_first_print('heed the mists', 'BOK').
card_image_name('heed the mists'/'BOK', 'heed the mists').
card_uid('heed the mists'/'BOK', 'BOK:Heed the Mists:heed the mists').
card_rarity('heed the mists'/'BOK', 'Uncommon').
card_artist('heed the mists'/'BOK', 'Christopher Rush').
card_number('heed the mists'/'BOK', '36').
card_flavor_text('heed the mists'/'BOK', '\"Once, I looked to the mists for wisdom. Now I wish only for understanding.\"').
card_multiverse_id('heed the mists'/'BOK', '74660').

card_in_set('hero\'s demise', 'BOK').
card_original_type('hero\'s demise'/'BOK', 'Instant').
card_original_text('hero\'s demise'/'BOK', 'Destroy target legendary creature.').
card_first_print('hero\'s demise', 'BOK').
card_image_name('hero\'s demise'/'BOK', 'hero\'s demise').
card_uid('hero\'s demise'/'BOK', 'BOK:Hero\'s Demise:hero\'s demise').
card_rarity('hero\'s demise'/'BOK', 'Rare').
card_artist('hero\'s demise'/'BOK', 'Jim Nelson').
card_number('hero\'s demise'/'BOK', '68').
card_flavor_text('hero\'s demise'/'BOK', '\"What will it say on our graves, Lord Konda? Will it say we led our world to conquer immortal forces or that we were crushed by our own arrogance? At each defeat, I wonder.\"\n—Sensei Hisoka, letter to Lord Konda').
card_multiverse_id('hero\'s demise'/'BOK', '74465').

card_in_set('higure, the still wind', 'BOK').
card_original_type('higure, the still wind'/'BOK', 'Legendary Creature — Human Ninja').
card_original_text('higure, the still wind'/'BOK', 'Ninjutsu {2}{U}{U} ({2}{U}{U}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Higure deals combat damage to a player, you may search your library for a Ninja card, reveal it, and put it into your hand. If you do, shuffle your library.\n{2}: Target Ninja is unblockable this turn.').
card_first_print('higure, the still wind', 'BOK').
card_image_name('higure, the still wind'/'BOK', 'higure, the still wind').
card_uid('higure, the still wind'/'BOK', 'BOK:Higure, the Still Wind:higure, the still wind').
card_rarity('higure, the still wind'/'BOK', 'Rare').
card_artist('higure, the still wind'/'BOK', 'Christopher Moeller').
card_number('higure, the still wind'/'BOK', '37').
card_multiverse_id('higure, the still wind'/'BOK', '74448').

card_in_set('hired muscle', 'BOK').
card_original_type('hired muscle'/'BOK', 'Creature — Human Warrior').
card_original_text('hired muscle'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Hired Muscle.\nAt end of turn, if there are two or more ki counters on Hired Muscle, you may flip it.\n----\nScarmaker\nLegendary Creature Spirit\n4/4\nRemove a ki counter from Scarmaker: Target creature gains fear until end of turn.').
card_first_print('hired muscle', 'BOK').
card_image_name('hired muscle'/'BOK', 'hired muscle').
card_uid('hired muscle'/'BOK', 'BOK:Hired Muscle:hired muscle').
card_rarity('hired muscle'/'BOK', 'Uncommon').
card_artist('hired muscle'/'BOK', 'Arnie Swekel').
card_number('hired muscle'/'BOK', '69a').
card_multiverse_id('hired muscle'/'BOK', '74476').

card_in_set('hokori, dust drinker', 'BOK').
card_original_type('hokori, dust drinker'/'BOK', 'Legendary Creature — Spirit').
card_original_text('hokori, dust drinker'/'BOK', 'Lands don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player untaps a land he or she controls.').
card_first_print('hokori, dust drinker', 'BOK').
card_image_name('hokori, dust drinker'/'BOK', 'hokori, dust drinker').
card_uid('hokori, dust drinker'/'BOK', 'BOK:Hokori, Dust Drinker:hokori, dust drinker').
card_rarity('hokori, dust drinker'/'BOK', 'Rare').
card_artist('hokori, dust drinker'/'BOK', 'Darrell Riche').
card_number('hokori, dust drinker'/'BOK', '7').
card_multiverse_id('hokori, dust drinker'/'BOK', '74647').

card_in_set('horobi\'s whisper', 'BOK').
card_original_type('horobi\'s whisper'/'BOK', 'Instant — Arcane').
card_original_text('horobi\'s whisper'/'BOK', 'If you control a Swamp, destroy target nonblack creature.\nSplice onto Arcane—Remove four cards in your graveyard from the game. (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('horobi\'s whisper', 'BOK').
card_image_name('horobi\'s whisper'/'BOK', 'horobi\'s whisper').
card_uid('horobi\'s whisper'/'BOK', 'BOK:Horobi\'s Whisper:horobi\'s whisper').
card_rarity('horobi\'s whisper'/'BOK', 'Common').
card_artist('horobi\'s whisper'/'BOK', 'Aleksi Briclot').
card_number('horobi\'s whisper'/'BOK', '70').
card_multiverse_id('horobi\'s whisper'/'BOK', '74590').

card_in_set('hundred-talon strike', 'BOK').
card_original_type('hundred-talon strike'/'BOK', 'Instant — Arcane').
card_original_text('hundred-talon strike'/'BOK', 'Target creature gets +1/+0 and gains first strike until end of turn.\nSplice onto Arcane—Tap an untapped white creature you control. (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('hundred-talon strike', 'BOK').
card_image_name('hundred-talon strike'/'BOK', 'hundred-talon strike').
card_uid('hundred-talon strike'/'BOK', 'BOK:Hundred-Talon Strike:hundred-talon strike').
card_rarity('hundred-talon strike'/'BOK', 'Common').
card_artist('hundred-talon strike'/'BOK', 'Matt Cavotta').
card_number('hundred-talon strike'/'BOK', '8').
card_multiverse_id('hundred-talon strike'/'BOK', '74581').

card_in_set('ichiga, who topples oaks', 'BOK').
card_original_type('ichiga, who topples oaks'/'BOK', 'Creature — Human Monk').
card_original_text('ichiga, who topples oaks'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Budoka Pupil.\nAt end of turn, if there are two or more ki counters on Budoka Pupil, you may flip it.\n----\nIchiga, Who Topples Oaks\nLegendary Creature Spirit\n4/3\nTrample\nRemove a ki counter from Ichiga, Who Topples Oaks: Target creature gets +2/+2 until end of turn.').
card_image_name('ichiga, who topples oaks'/'BOK', 'ichiga, who topples oaks').
card_uid('ichiga, who topples oaks'/'BOK', 'BOK:Ichiga, Who Topples Oaks:ichiga, who topples oaks').
card_rarity('ichiga, who topples oaks'/'BOK', 'Uncommon').
card_artist('ichiga, who topples oaks'/'BOK', 'Shishizaru').
card_number('ichiga, who topples oaks'/'BOK', '122b').
card_multiverse_id('ichiga, who topples oaks'/'BOK', '74536').

card_in_set('in the web of war', 'BOK').
card_original_type('in the web of war'/'BOK', 'Enchantment').
card_original_text('in the web of war'/'BOK', 'Whenever a creature comes into play under your control, it gets +2/+0 and gains haste until end of turn.').
card_first_print('in the web of war', 'BOK').
card_image_name('in the web of war'/'BOK', 'in the web of war').
card_uid('in the web of war'/'BOK', 'BOK:In the Web of War:in the web of war').
card_rarity('in the web of war'/'BOK', 'Rare').
card_artist('in the web of war'/'BOK', 'Ron Spencer').
card_number('in the web of war'/'BOK', '108').
card_flavor_text('in the web of war'/'BOK', '\"In desperation, Konda sent warriors to parley with the ogre-magi. No one knows whether they were slaughtered at Shinka or if they even reached its bloodstained walls.\"\n—Observations of the Kami War').
card_multiverse_id('in the web of war'/'BOK', '74041').

card_in_set('indebted samurai', 'BOK').
card_original_type('indebted samurai'/'BOK', 'Creature — Human Samurai').
card_original_text('indebted samurai'/'BOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever a Samurai you control is put into a graveyard from play, you may put a +1/+1 counter on Indebted Samurai.').
card_first_print('indebted samurai', 'BOK').
card_image_name('indebted samurai'/'BOK', 'indebted samurai').
card_uid('indebted samurai'/'BOK', 'BOK:Indebted Samurai:indebted samurai').
card_rarity('indebted samurai'/'BOK', 'Uncommon').
card_artist('indebted samurai'/'BOK', 'Carl Critchlow').
card_number('indebted samurai'/'BOK', '9').
card_flavor_text('indebted samurai'/'BOK', '\"Forgive me, Saburo. I could not save you. But your blade will not rest in my hand.\"').
card_multiverse_id('indebted samurai'/'BOK', '74091').

card_in_set('ink-eyes, servant of oni', 'BOK').
card_original_type('ink-eyes, servant of oni'/'BOK', 'Legendary Creature — Rat Ninja').
card_original_text('ink-eyes, servant of oni'/'BOK', 'Ninjutsu {3}{B}{B} ({3}{B}{B}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Ink-Eyes, Servant of Oni deals combat damage to a player, you may put target creature card from that player\'s graveyard into play under your control.\n{1}{B}: Regenerate Ink-Eyes.').
card_image_name('ink-eyes, servant of oni'/'BOK', 'ink-eyes, servant of oni').
card_uid('ink-eyes, servant of oni'/'BOK', 'BOK:Ink-Eyes, Servant of Oni:ink-eyes, servant of oni').
card_rarity('ink-eyes, servant of oni'/'BOK', 'Rare').
card_artist('ink-eyes, servant of oni'/'BOK', 'Wayne Reynolds').
card_number('ink-eyes, servant of oni'/'BOK', '71').
card_multiverse_id('ink-eyes, servant of oni'/'BOK', '74626').

card_in_set('ire of kaminari', 'BOK').
card_original_type('ire of kaminari'/'BOK', 'Instant — Arcane').
card_original_text('ire of kaminari'/'BOK', 'Ire of Kaminari deals damage to target creature or player equal to the number of Arcane cards in your graveyard.').
card_first_print('ire of kaminari', 'BOK').
card_image_name('ire of kaminari'/'BOK', 'ire of kaminari').
card_uid('ire of kaminari'/'BOK', 'BOK:Ire of Kaminari:ire of kaminari').
card_rarity('ire of kaminari'/'BOK', 'Common').
card_artist('ire of kaminari'/'BOK', 'Kev Walker').
card_number('ire of kaminari'/'BOK', '109').
card_flavor_text('ire of kaminari'/'BOK', '\"Thunder broke the brittle silence over the Araba. A surge of raw energy lifted the soldier\'s body into the air and briefly, in the heart of the flash, he saw the face of a god.\"\n—Great Battles of Kamigawa').
card_multiverse_id('ire of kaminari'/'BOK', '74609').

card_in_set('isao, enlightened bushi', 'BOK').
card_original_type('isao, enlightened bushi'/'BOK', 'Legendary Creature — Human Samurai').
card_original_text('isao, enlightened bushi'/'BOK', 'Isao, Enlightened Bushi can\'t be countered.\nBushido 2 (When this blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{2}: Regenerate target Samurai.').
card_first_print('isao, enlightened bushi', 'BOK').
card_image_name('isao, enlightened bushi'/'BOK', 'isao, enlightened bushi').
card_uid('isao, enlightened bushi'/'BOK', 'BOK:Isao, Enlightened Bushi:isao, enlightened bushi').
card_rarity('isao, enlightened bushi'/'BOK', 'Rare').
card_artist('isao, enlightened bushi'/'BOK', 'Christopher Moeller').
card_number('isao, enlightened bushi'/'BOK', '129').
card_flavor_text('isao, enlightened bushi'/'BOK', '\"I do not care if he claims no allegiance in this war. Find him, Takeno.\"\n—Lord Konda').
card_multiverse_id('isao, enlightened bushi'/'BOK', '74039').

card_in_set('ishi-ishi, akki crackshot', 'BOK').
card_original_type('ishi-ishi, akki crackshot'/'BOK', 'Legendary Creature — Goblin Warrior').
card_original_text('ishi-ishi, akki crackshot'/'BOK', 'Whenever an opponent plays a Spirit or Arcane spell, Ishi-Ishi, Akki Crackshot deals 2 damage to that player.').
card_first_print('ishi-ishi, akki crackshot', 'BOK').
card_image_name('ishi-ishi, akki crackshot'/'BOK', 'ishi-ishi, akki crackshot').
card_uid('ishi-ishi, akki crackshot'/'BOK', 'BOK:Ishi-Ishi, Akki Crackshot:ishi-ishi, akki crackshot').
card_rarity('ishi-ishi, akki crackshot'/'BOK', 'Rare').
card_artist('ishi-ishi, akki crackshot'/'BOK', 'Christopher Rush').
card_number('ishi-ishi, akki crackshot'/'BOK', '110').
card_flavor_text('ishi-ishi, akki crackshot'/'BOK', '\"Here fell Ishi-Ishi, King of the Flaming Pebbles, Scourge of the Mountain Kami, Lover of Goats. May his shell never burn.\"\n—Cave inscription').
card_multiverse_id('ishi-ishi, akki crackshot'/'BOK', '74636').

card_in_set('iwamori of the open fist', 'BOK').
card_original_type('iwamori of the open fist'/'BOK', 'Legendary Creature — Human Monk').
card_original_text('iwamori of the open fist'/'BOK', 'Trample\nWhen Iwamori of the Open Fist comes into play, each opponent may put a legendary creature card from his or her hand into play.').
card_first_print('iwamori of the open fist', 'BOK').
card_image_name('iwamori of the open fist'/'BOK', 'iwamori of the open fist').
card_uid('iwamori of the open fist'/'BOK', 'BOK:Iwamori of the Open Fist:iwamori of the open fist').
card_rarity('iwamori of the open fist'/'BOK', 'Rare').
card_artist('iwamori of the open fist'/'BOK', 'Paolo Parente').
card_number('iwamori of the open fist'/'BOK', '130').
card_multiverse_id('iwamori of the open fist'/'BOK', '74507').

card_in_set('jaraku the interloper', 'BOK').
card_original_type('jaraku the interloper'/'BOK', 'Creature — Human Wizard').
card_original_text('jaraku the interloper'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Callow Jushi.\nAt end of turn, if there are two or more ki counters on Callow Jushi, you may flip it.\n----\nJaraku the Interloper\nLegendary Creature Spirit\n3/4\nRemove a ki counter from Jaraku the Interloper: Counter target spell unless its controller pays {2}.').
card_first_print('jaraku the interloper', 'BOK').
card_image_name('jaraku the interloper'/'BOK', 'jaraku the interloper').
card_uid('jaraku the interloper'/'BOK', 'BOK:Jaraku the Interloper:jaraku the interloper').
card_rarity('jaraku the interloper'/'BOK', 'Uncommon').
card_artist('jaraku the interloper'/'BOK', 'Tsutomu Kawade').
card_number('jaraku the interloper'/'BOK', '31b').
card_multiverse_id('jaraku the interloper'/'BOK', '74489').

card_in_set('jetting glasskite', 'BOK').
card_original_type('jetting glasskite'/'BOK', 'Creature — Spirit').
card_original_text('jetting glasskite'/'BOK', 'Flying\nWhenever Jetting Glasskite becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.').
card_first_print('jetting glasskite', 'BOK').
card_image_name('jetting glasskite'/'BOK', 'jetting glasskite').
card_uid('jetting glasskite'/'BOK', 'BOK:Jetting Glasskite:jetting glasskite').
card_rarity('jetting glasskite'/'BOK', 'Uncommon').
card_artist('jetting glasskite'/'BOK', 'Shishizaru').
card_number('jetting glasskite'/'BOK', '38').
card_flavor_text('jetting glasskite'/'BOK', 'The bolt struck with a flash and there was a terrible sound, as of glass shattering, but the creature was unharmed.').
card_multiverse_id('jetting glasskite'/'BOK', '81976').

card_in_set('kaijin of the vanishing touch', 'BOK').
card_original_type('kaijin of the vanishing touch'/'BOK', 'Creature — Spirit').
card_original_text('kaijin of the vanishing touch'/'BOK', 'Defender (This creature can\'t attack.)\nWhenever Kaijin of the Vanishing Touch blocks a creature, return that creature to its owner\'s hand at end of combat. (Return it only if it\'s in play.)').
card_first_print('kaijin of the vanishing touch', 'BOK').
card_image_name('kaijin of the vanishing touch'/'BOK', 'kaijin of the vanishing touch').
card_uid('kaijin of the vanishing touch'/'BOK', 'BOK:Kaijin of the Vanishing Touch:kaijin of the vanishing touch').
card_rarity('kaijin of the vanishing touch'/'BOK', 'Uncommon').
card_artist('kaijin of the vanishing touch'/'BOK', 'Randy Gallegos').
card_number('kaijin of the vanishing touch'/'BOK', '39').
card_flavor_text('kaijin of the vanishing touch'/'BOK', 'When it\'s finished, all that\'s left of you is a ripple on a still pond.').
card_multiverse_id('kaijin of the vanishing touch'/'BOK', '74443').

card_in_set('kaiso, memory of loyalty', 'BOK').
card_original_type('kaiso, memory of loyalty'/'BOK', 'Creature — Human Soldier').
card_original_text('kaiso, memory of loyalty'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Faithful Squire.\nAt end of turn, if there are two or more ki counters on Faithful Squire, you may flip it.\n-----\nKaiso, Memory of Loyalty\nLegendary Creature Spirit\n3/4\nFlying\nRemove a ki counter from Kaiso, Memory of Loyalty: Prevent all damage that would be dealt to target creature this turn.').
card_first_print('kaiso, memory of loyalty', 'BOK').
card_image_name('kaiso, memory of loyalty'/'BOK', 'kaiso, memory of loyalty').
card_uid('kaiso, memory of loyalty'/'BOK', 'BOK:Kaiso, Memory of Loyalty:kaiso, memory of loyalty').
card_rarity('kaiso, memory of loyalty'/'BOK', 'Uncommon').
card_artist('kaiso, memory of loyalty'/'BOK', 'Mark Zug').
card_number('kaiso, memory of loyalty'/'BOK', '3b').
card_multiverse_id('kaiso, memory of loyalty'/'BOK', '74093').

card_in_set('kami of false hope', 'BOK').
card_original_type('kami of false hope'/'BOK', 'Creature — Spirit').
card_original_text('kami of false hope'/'BOK', 'Sacrifice Kami of False Hope: Prevent all combat damage that would be dealt this turn.').
card_first_print('kami of false hope', 'BOK').
card_image_name('kami of false hope'/'BOK', 'kami of false hope').
card_uid('kami of false hope'/'BOK', 'BOK:Kami of False Hope:kami of false hope').
card_rarity('kami of false hope'/'BOK', 'Common').
card_artist('kami of false hope'/'BOK', 'Daren Bader').
card_number('kami of false hope'/'BOK', '10').
card_flavor_text('kami of false hope'/'BOK', '\"Across the rift of battle, a bridge of gossamer. And for one moment, it holds.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('kami of false hope'/'BOK', '74097').

card_in_set('kami of tattered shoji', 'BOK').
card_original_type('kami of tattered shoji'/'BOK', 'Creature — Spirit').
card_original_text('kami of tattered shoji'/'BOK', 'Whenever you play a Spirit or Arcane spell, Kami of Tattered Shoji gains flying until end of turn.').
card_first_print('kami of tattered shoji', 'BOK').
card_image_name('kami of tattered shoji'/'BOK', 'kami of tattered shoji').
card_uid('kami of tattered shoji'/'BOK', 'BOK:Kami of Tattered Shoji:kami of tattered shoji').
card_rarity('kami of tattered shoji'/'BOK', 'Common').
card_artist('kami of tattered shoji'/'BOK', 'Shishizaru').
card_number('kami of tattered shoji'/'BOK', '11').
card_flavor_text('kami of tattered shoji'/'BOK', 'It remembered all the shadows lantern-cast upon its paper wings, and sometimes those silhouettes played across its shape again, acting out silent tragedies.').
card_multiverse_id('kami of tattered shoji'/'BOK', '74460').

card_in_set('kami of the honored dead', 'BOK').
card_original_type('kami of the honored dead'/'BOK', 'Creature — Spirit').
card_original_text('kami of the honored dead'/'BOK', 'Flying\nWhenever Kami of the Honored Dead is dealt damage, you gain that much life.\nSoulshift 6 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_first_print('kami of the honored dead', 'BOK').
card_image_name('kami of the honored dead'/'BOK', 'kami of the honored dead').
card_uid('kami of the honored dead'/'BOK', 'BOK:Kami of the Honored Dead:kami of the honored dead').
card_rarity('kami of the honored dead'/'BOK', 'Uncommon').
card_artist('kami of the honored dead'/'BOK', 'Mark Zug').
card_number('kami of the honored dead'/'BOK', '12').
card_multiverse_id('kami of the honored dead'/'BOK', '81977').

card_in_set('kentaro, the smiling cat', 'BOK').
card_original_type('kentaro, the smiling cat'/'BOK', 'Legendary Creature — Human Samurai').
card_original_text('kentaro, the smiling cat'/'BOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nYou may pay {X} rather than pay the mana cost for Samurai spells you play, where X is that spell\'s converted mana cost.').
card_first_print('kentaro, the smiling cat', 'BOK').
card_image_name('kentaro, the smiling cat'/'BOK', 'kentaro, the smiling cat').
card_uid('kentaro, the smiling cat'/'BOK', 'BOK:Kentaro, the Smiling Cat:kentaro, the smiling cat').
card_rarity('kentaro, the smiling cat'/'BOK', 'Rare').
card_artist('kentaro, the smiling cat'/'BOK', 'Donato Giancola').
card_number('kentaro, the smiling cat'/'BOK', '13').
card_flavor_text('kentaro, the smiling cat'/'BOK', '\"The bonds of bushido will draw the dishonored back someday, and I will be there waiting.\"').
card_multiverse_id('kentaro, the smiling cat'/'BOK', '81968').

card_in_set('kira, great glass-spinner', 'BOK').
card_original_type('kira, great glass-spinner'/'BOK', 'Legendary Creature — Spirit').
card_original_text('kira, great glass-spinner'/'BOK', 'Flying\nCreatures you control have \"Whenever this creature becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.\"').
card_first_print('kira, great glass-spinner', 'BOK').
card_image_name('kira, great glass-spinner'/'BOK', 'kira, great glass-spinner').
card_uid('kira, great glass-spinner'/'BOK', 'BOK:Kira, Great Glass-Spinner:kira, great glass-spinner').
card_rarity('kira, great glass-spinner'/'BOK', 'Rare').
card_artist('kira, great glass-spinner'/'BOK', 'Kev Walker').
card_number('kira, great glass-spinner'/'BOK', '40').
card_flavor_text('kira, great glass-spinner'/'BOK', 'Each spell is an intricate tapestry, and Kira is the great unraveler.').
card_multiverse_id('kira, great glass-spinner'/'BOK', '74445').

card_in_set('kitsune palliator', 'BOK').
card_original_type('kitsune palliator'/'BOK', 'Creature — Fox Cleric').
card_original_text('kitsune palliator'/'BOK', '{T}: Prevent the next 1 damage that would be dealt to each creature and each player this turn.').
card_first_print('kitsune palliator', 'BOK').
card_image_name('kitsune palliator'/'BOK', 'kitsune palliator').
card_uid('kitsune palliator'/'BOK', 'BOK:Kitsune Palliator:kitsune palliator').
card_rarity('kitsune palliator'/'BOK', 'Uncommon').
card_artist('kitsune palliator'/'BOK', 'Dave Dorman').
card_number('kitsune palliator'/'BOK', '14').
card_flavor_text('kitsune palliator'/'BOK', '\"Who am I to judge who is deserving and who is not? That is a question for lords, not healers.\"').
card_multiverse_id('kitsune palliator'/'BOK', '74603').

card_in_set('kodama of the center tree', 'BOK').
card_original_type('kodama of the center tree'/'BOK', 'Legendary Creature — Spirit').
card_original_text('kodama of the center tree'/'BOK', 'Kodama of the Center Tree\'s power and toughness are each equal to the number of Spirits you control.\nKodama of the Center Tree has soulshift X, where X is the number of Spirits you control.').
card_first_print('kodama of the center tree', 'BOK').
card_image_name('kodama of the center tree'/'BOK', 'kodama of the center tree').
card_uid('kodama of the center tree'/'BOK', 'BOK:Kodama of the Center Tree:kodama of the center tree').
card_rarity('kodama of the center tree'/'BOK', 'Rare').
card_artist('kodama of the center tree'/'BOK', 'Jim Murray').
card_number('kodama of the center tree'/'BOK', '131').
card_multiverse_id('kodama of the center tree'/'BOK', '74086').

card_in_set('kumano\'s blessing', 'BOK').
card_original_type('kumano\'s blessing'/'BOK', 'Enchant Creature').
card_original_text('kumano\'s blessing'/'BOK', 'You may play Kumano\'s Blessing any time you could play an instant.\nIf a creature dealt damage by enchanted creature this turn would be put into a graveyard, remove it from the game instead.').
card_first_print('kumano\'s blessing', 'BOK').
card_image_name('kumano\'s blessing'/'BOK', 'kumano\'s blessing').
card_uid('kumano\'s blessing'/'BOK', 'BOK:Kumano\'s Blessing:kumano\'s blessing').
card_rarity('kumano\'s blessing'/'BOK', 'Common').
card_artist('kumano\'s blessing'/'BOK', 'Keith Garletts').
card_number('kumano\'s blessing'/'BOK', '111').
card_multiverse_id('kumano\'s blessing'/'BOK', '74573').

card_in_set('kyoki, sanity\'s eclipse', 'BOK').
card_original_type('kyoki, sanity\'s eclipse'/'BOK', 'Legendary Creature — Demon Spirit').
card_original_text('kyoki, sanity\'s eclipse'/'BOK', 'Whenever you play a Spirit or Arcane spell, target opponent removes a card in his or her hand from the game.').
card_first_print('kyoki, sanity\'s eclipse', 'BOK').
card_image_name('kyoki, sanity\'s eclipse'/'BOK', 'kyoki, sanity\'s eclipse').
card_uid('kyoki, sanity\'s eclipse'/'BOK', 'BOK:Kyoki, Sanity\'s Eclipse:kyoki, sanity\'s eclipse').
card_rarity('kyoki, sanity\'s eclipse'/'BOK', 'Rare').
card_artist('kyoki, sanity\'s eclipse'/'BOK', 'Paolo Parente').
card_number('kyoki, sanity\'s eclipse'/'BOK', '72').
card_flavor_text('kyoki, sanity\'s eclipse'/'BOK', '\"Kyoki, Eater of Minds, Corrupter of Thoughts, Bringer of Madness, Lord of Fear. Return, by our blood, and walk again.\"\n—Ogre chant').
card_multiverse_id('kyoki, sanity\'s eclipse'/'BOK', '74552').

card_in_set('lifegift', 'BOK').
card_original_type('lifegift'/'BOK', 'Enchantment').
card_original_text('lifegift'/'BOK', 'Whenever a land comes into play, you may gain 1 life.').
card_first_print('lifegift', 'BOK').
card_image_name('lifegift'/'BOK', 'lifegift').
card_uid('lifegift'/'BOK', 'BOK:Lifegift:lifegift').
card_rarity('lifegift'/'BOK', 'Rare').
card_artist('lifegift'/'BOK', 'John Matson').
card_number('lifegift'/'BOK', '132').
card_flavor_text('lifegift'/'BOK', '\"Walk the world and you\'ll find your soul; search your soul and you\'ll discover the world.\"\n—Diary of Azusa').
card_multiverse_id('lifegift'/'BOK', '74571').

card_in_set('lifespinner', 'BOK').
card_original_type('lifespinner'/'BOK', 'Creature — Spirit').
card_original_text('lifespinner'/'BOK', '{T}, Sacrifice three Spirits: Search your library for a legendary Spirit card and put it into play. Then shuffle your library.').
card_first_print('lifespinner', 'BOK').
card_image_name('lifespinner'/'BOK', 'lifespinner').
card_uid('lifespinner'/'BOK', 'BOK:Lifespinner:lifespinner').
card_rarity('lifespinner'/'BOK', 'Uncommon').
card_artist('lifespinner'/'BOK', 'Cara Mitten').
card_number('lifespinner'/'BOK', '133').
card_flavor_text('lifespinner'/'BOK', '\"The husk splits like a madman\'s smile\nAnd mystery steps forth.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('lifespinner'/'BOK', '74642').

card_in_set('loam dweller', 'BOK').
card_original_type('loam dweller'/'BOK', 'Creature — Spirit').
card_original_text('loam dweller'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a land card from your hand into play tapped.').
card_first_print('loam dweller', 'BOK').
card_image_name('loam dweller'/'BOK', 'loam dweller').
card_uid('loam dweller'/'BOK', 'BOK:Loam Dweller:loam dweller').
card_rarity('loam dweller'/'BOK', 'Uncommon').
card_artist('loam dweller'/'BOK', 'Paolo Parente').
card_number('loam dweller'/'BOK', '134').
card_flavor_text('loam dweller'/'BOK', '\"You know its mood by the plants that spring up around it. When it is placid, fields of lilies flow in its wake; when it is angry, every stem has a thorn.\"\n—Isao, Enlightened Bushi').
card_multiverse_id('loam dweller'/'BOK', '74584').

card_in_set('mannichi, the fevered dream', 'BOK').
card_original_type('mannichi, the fevered dream'/'BOK', 'Legendary Creature — Spirit').
card_original_text('mannichi, the fevered dream'/'BOK', '{1}{R}: Switch each creature\'s power and toughness until end of turn.').
card_first_print('mannichi, the fevered dream', 'BOK').
card_image_name('mannichi, the fevered dream'/'BOK', 'mannichi, the fevered dream').
card_uid('mannichi, the fevered dream'/'BOK', 'BOK:Mannichi, the Fevered Dream:mannichi, the fevered dream').
card_rarity('mannichi, the fevered dream'/'BOK', 'Rare').
card_artist('mannichi, the fevered dream'/'BOK', 'Martina Pilcerova').
card_number('mannichi, the fevered dream'/'BOK', '112').
card_flavor_text('mannichi, the fevered dream'/'BOK', '\"Early in his reign, Konda fell ill. His head burned with fever, and he saw visions of his future. In them, he saw a spirit-child, and, in that child\'s eyes, a way to make his empire last forever.\"\n—The History of Kamigawa').
card_multiverse_id('mannichi, the fevered dream'/'BOK', '74490').

card_in_set('mark of sakiko', 'BOK').
card_original_type('mark of sakiko'/'BOK', 'Enchant Creature').
card_original_text('mark of sakiko'/'BOK', 'Enchanted creature has \"Whenever this creature deals combat damage to a player, add that much {G} to your mana pool. This mana doesn\'t cause mana burn. Until end of turn, this mana doesn\'t empty from your mana pool as phases end.\"').
card_first_print('mark of sakiko', 'BOK').
card_image_name('mark of sakiko'/'BOK', 'mark of sakiko').
card_uid('mark of sakiko'/'BOK', 'BOK:Mark of Sakiko:mark of sakiko').
card_rarity('mark of sakiko'/'BOK', 'Uncommon').
card_artist('mark of sakiko'/'BOK', 'Alex Horley-Orlandelli').
card_number('mark of sakiko'/'BOK', '135').
card_multiverse_id('mark of sakiko'/'BOK', '74430').

card_in_set('mark of the oni', 'BOK').
card_original_type('mark of the oni'/'BOK', 'Enchant Creature').
card_original_text('mark of the oni'/'BOK', 'You control enchanted creature.\nAt end of turn, if you control no Demons, sacrifice Mark of the Oni.').
card_first_print('mark of the oni', 'BOK').
card_image_name('mark of the oni'/'BOK', 'mark of the oni').
card_uid('mark of the oni'/'BOK', 'BOK:Mark of the Oni:mark of the oni').
card_rarity('mark of the oni'/'BOK', 'Uncommon').
card_artist('mark of the oni'/'BOK', 'Heather Hudson').
card_number('mark of the oni'/'BOK', '73').
card_flavor_text('mark of the oni'/'BOK', '\"As more oni walked Kamigawa, more darkness infested its inhabitants\' souls.\"\n—The History of Kamigawa').
card_multiverse_id('mark of the oni'/'BOK', '74006').

card_in_set('matsu-tribe sniper', 'BOK').
card_original_type('matsu-tribe sniper'/'BOK', 'Creature — Snake Warrior Archer').
card_original_text('matsu-tribe sniper'/'BOK', '{T}: Matsu-Tribe Sniper deals 1 damage to target creature with flying.\nWhenever Matsu-Tribe Sniper deals damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('matsu-tribe sniper', 'BOK').
card_image_name('matsu-tribe sniper'/'BOK', 'matsu-tribe sniper').
card_uid('matsu-tribe sniper'/'BOK', 'BOK:Matsu-Tribe Sniper:matsu-tribe sniper').
card_rarity('matsu-tribe sniper'/'BOK', 'Common').
card_artist('matsu-tribe sniper'/'BOK', 'Carl Critchlow').
card_number('matsu-tribe sniper'/'BOK', '136').
card_multiverse_id('matsu-tribe sniper'/'BOK', '74619').

card_in_set('mending hands', 'BOK').
card_original_type('mending hands'/'BOK', 'Instant').
card_original_text('mending hands'/'BOK', 'Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_first_print('mending hands', 'BOK').
card_image_name('mending hands'/'BOK', 'mending hands').
card_uid('mending hands'/'BOK', 'BOK:Mending Hands:mending hands').
card_rarity('mending hands'/'BOK', 'Common').
card_artist('mending hands'/'BOK', 'Douglas Shuler').
card_number('mending hands'/'BOK', '15').
card_flavor_text('mending hands'/'BOK', '\"I can staunch their blood, mend their flesh, and knit their bones. But I cannot restore their hope.\"\n—Tender-Hand, kitsune healer').
card_multiverse_id('mending hands'/'BOK', '74538').

card_in_set('minamo sightbender', 'BOK').
card_original_type('minamo sightbender'/'BOK', 'Creature — Human Wizard').
card_original_text('minamo sightbender'/'BOK', '{X}, {T}: Target creature with power X or less is unblockable this turn.').
card_first_print('minamo sightbender', 'BOK').
card_image_name('minamo sightbender'/'BOK', 'minamo sightbender').
card_uid('minamo sightbender'/'BOK', 'BOK:Minamo Sightbender:minamo sightbender').
card_rarity('minamo sightbender'/'BOK', 'Uncommon').
card_artist('minamo sightbender'/'BOK', 'Luca Zontini').
card_number('minamo sightbender'/'BOK', '41').
card_flavor_text('minamo sightbender'/'BOK', 'Woven from threads of the spirit world, the mistcloak rendered its wearer invisible, but quickly fell to tatters.').
card_multiverse_id('minamo sightbender'/'BOK', '74600').

card_in_set('minamo\'s meddling', 'BOK').
card_original_type('minamo\'s meddling'/'BOK', 'Instant').
card_original_text('minamo\'s meddling'/'BOK', 'Counter target spell. That spell\'s controller reveals his or her hand, then discards each card with the same name as a card spliced onto that spell.').
card_first_print('minamo\'s meddling', 'BOK').
card_image_name('minamo\'s meddling'/'BOK', 'minamo\'s meddling').
card_uid('minamo\'s meddling'/'BOK', 'BOK:Minamo\'s Meddling:minamo\'s meddling').
card_rarity('minamo\'s meddling'/'BOK', 'Common').
card_artist('minamo\'s meddling'/'BOK', 'Alex Horley-Orlandelli').
card_number('minamo\'s meddling'/'BOK', '42').
card_flavor_text('minamo\'s meddling'/'BOK', 'Like storm water through the segments of bamboo, the spell followed the links of magic. Then it destroyed them.').
card_multiverse_id('minamo\'s meddling'/'BOK', '74665').

card_in_set('mirror gallery', 'BOK').
card_original_type('mirror gallery'/'BOK', 'Artifact').
card_original_text('mirror gallery'/'BOK', 'The \"legend rule\" doesn\'t apply.').
card_first_print('mirror gallery', 'BOK').
card_image_name('mirror gallery'/'BOK', 'mirror gallery').
card_uid('mirror gallery'/'BOK', 'BOK:Mirror Gallery:mirror gallery').
card_rarity('mirror gallery'/'BOK', 'Rare').
card_artist('mirror gallery'/'BOK', 'Scott M. Fischer').
card_number('mirror gallery'/'BOK', '154').
card_flavor_text('mirror gallery'/'BOK', 'Only in mirrors do heroes find their equal.').
card_multiverse_id('mirror gallery'/'BOK', '74555').

card_in_set('mistblade shinobi', 'BOK').
card_original_type('mistblade shinobi'/'BOK', 'Creature — Human Ninja').
card_original_text('mistblade shinobi'/'BOK', 'Ninjutsu {U} ({U}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Mistblade Shinobi deals combat damage to a player, you may return target creature that player controls to its owner\'s hand.').
card_first_print('mistblade shinobi', 'BOK').
card_image_name('mistblade shinobi'/'BOK', 'mistblade shinobi').
card_uid('mistblade shinobi'/'BOK', 'BOK:Mistblade Shinobi:mistblade shinobi').
card_rarity('mistblade shinobi'/'BOK', 'Common').
card_artist('mistblade shinobi'/'BOK', 'Kev Walker').
card_number('mistblade shinobi'/'BOK', '43').
card_multiverse_id('mistblade shinobi'/'BOK', '74084').

card_in_set('moonlit strider', 'BOK').
card_original_type('moonlit strider'/'BOK', 'Creature — Spirit').
card_original_text('moonlit strider'/'BOK', 'Sacrifice Moonlit Strider: Target creature you control gains protection from the color of your choice until end of turn.\nSoulshift 3 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_first_print('moonlit strider', 'BOK').
card_image_name('moonlit strider'/'BOK', 'moonlit strider').
card_uid('moonlit strider'/'BOK', 'BOK:Moonlit Strider:moonlit strider').
card_rarity('moonlit strider'/'BOK', 'Common').
card_artist('moonlit strider'/'BOK', 'John Avon').
card_number('moonlit strider'/'BOK', '16').
card_multiverse_id('moonlit strider'/'BOK', '74028').

card_in_set('neko-te', 'BOK').
card_original_type('neko-te'/'BOK', 'Artifact — Equipment').
card_original_text('neko-te'/'BOK', 'Whenever equipped creature deals damage to a creature, tap that creature. As long as Neko-Te remains in play, that creature doesn\'t untap during its controller\'s untap step.\nWhenever equipped creature deals damage to a player, that player loses 1 life.\nEquip {2}').
card_first_print('neko-te', 'BOK').
card_image_name('neko-te'/'BOK', 'neko-te').
card_uid('neko-te'/'BOK', 'BOK:Neko-Te:neko-te').
card_rarity('neko-te'/'BOK', 'Rare').
card_artist('neko-te'/'BOK', 'Alex Horley-Orlandelli').
card_number('neko-te'/'BOK', '155').
card_multiverse_id('neko-te'/'BOK', '74578').

card_in_set('nezumi shadow-watcher', 'BOK').
card_original_type('nezumi shadow-watcher'/'BOK', 'Creature — Rat Warrior').
card_original_text('nezumi shadow-watcher'/'BOK', 'Sacrifice Nezumi Shadow-Watcher: Destroy target Ninja.').
card_first_print('nezumi shadow-watcher', 'BOK').
card_image_name('nezumi shadow-watcher'/'BOK', 'nezumi shadow-watcher').
card_uid('nezumi shadow-watcher'/'BOK', 'BOK:Nezumi Shadow-Watcher:nezumi shadow-watcher').
card_rarity('nezumi shadow-watcher'/'BOK', 'Uncommon').
card_artist('nezumi shadow-watcher'/'BOK', 'Pete Venters').
card_number('nezumi shadow-watcher'/'BOK', '74').
card_flavor_text('nezumi shadow-watcher'/'BOK', '\"The Okiba Gang! Night-cursed thieves and assassins! I\'ve had enough of their meddling! Triple the guard!\"\n—Marrow-Gnawer').
card_multiverse_id('nezumi shadow-watcher'/'BOK', '74467').

card_in_set('ninja of the deep hours', 'BOK').
card_original_type('ninja of the deep hours'/'BOK', 'Creature — Human Ninja').
card_original_text('ninja of the deep hours'/'BOK', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Ninja of the Deep Hours deals combat damage to a player, you may draw a card.').
card_first_print('ninja of the deep hours', 'BOK').
card_image_name('ninja of the deep hours'/'BOK', 'ninja of the deep hours').
card_uid('ninja of the deep hours'/'BOK', 'BOK:Ninja of the Deep Hours:ninja of the deep hours').
card_rarity('ninja of the deep hours'/'BOK', 'Common').
card_artist('ninja of the deep hours'/'BOK', 'Dan Scott').
card_number('ninja of the deep hours'/'BOK', '44').
card_multiverse_id('ninja of the deep hours'/'BOK', '74587').

card_in_set('nourishing shoal', 'BOK').
card_original_type('nourishing shoal'/'BOK', 'Instant — Arcane').
card_original_text('nourishing shoal'/'BOK', 'You may remove a green card with converted mana cost X in your hand from the game rather than pay Nourishing Shoal\'s mana cost.\nYou gain X life.').
card_first_print('nourishing shoal', 'BOK').
card_image_name('nourishing shoal'/'BOK', 'nourishing shoal').
card_uid('nourishing shoal'/'BOK', 'BOK:Nourishing Shoal:nourishing shoal').
card_rarity('nourishing shoal'/'BOK', 'Rare').
card_artist('nourishing shoal'/'BOK', 'Greg Staples').
card_number('nourishing shoal'/'BOK', '137').
card_multiverse_id('nourishing shoal'/'BOK', '74100').

card_in_set('ogre marauder', 'BOK').
card_original_type('ogre marauder'/'BOK', 'Creature — Ogre Warrior').
card_original_text('ogre marauder'/'BOK', 'Whenever Ogre Marauder attacks, it can\'t be blocked this turn unless defending player sacrifices a creature.').
card_first_print('ogre marauder', 'BOK').
card_image_name('ogre marauder'/'BOK', 'ogre marauder').
card_uid('ogre marauder'/'BOK', 'BOK:Ogre Marauder:ogre marauder').
card_rarity('ogre marauder'/'BOK', 'Uncommon').
card_artist('ogre marauder'/'BOK', 'Adam Rex').
card_number('ogre marauder'/'BOK', '75').
card_flavor_text('ogre marauder'/'BOK', 'Once freed, the oni demanded more and more sacrifices to appease them. The ogres happily obliged.').
card_multiverse_id('ogre marauder'/'BOK', '74594').

card_in_set('ogre recluse', 'BOK').
card_original_type('ogre recluse'/'BOK', 'Creature — Ogre Warrior').
card_original_text('ogre recluse'/'BOK', 'Whenever a player plays a spell, tap Ogre Recluse.').
card_first_print('ogre recluse', 'BOK').
card_image_name('ogre recluse'/'BOK', 'ogre recluse').
card_uid('ogre recluse'/'BOK', 'BOK:Ogre Recluse:ogre recluse').
card_rarity('ogre recluse'/'BOK', 'Uncommon').
card_artist('ogre recluse'/'BOK', 'Jim Murray').
card_number('ogre recluse'/'BOK', '113').
card_flavor_text('ogre recluse'/'BOK', '\"Those ogres who did not embrace oni worship were cast out, cursed, and forced into hermitage, waiting for the day the oni would come for them.\"\n—The History of Kamigawa').
card_multiverse_id('ogre recluse'/'BOK', '74645').

card_in_set('okiba-gang shinobi', 'BOK').
card_original_type('okiba-gang shinobi'/'BOK', 'Creature — Rat Ninja').
card_original_text('okiba-gang shinobi'/'BOK', 'Ninjutsu {3}{B} ({3}{B}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Okiba-Gang Shinobi deals combat damage to a player, that player discards two cards.').
card_first_print('okiba-gang shinobi', 'BOK').
card_image_name('okiba-gang shinobi'/'BOK', 'okiba-gang shinobi').
card_uid('okiba-gang shinobi'/'BOK', 'BOK:Okiba-Gang Shinobi:okiba-gang shinobi').
card_rarity('okiba-gang shinobi'/'BOK', 'Common').
card_artist('okiba-gang shinobi'/'BOK', 'Mark Zug').
card_number('okiba-gang shinobi'/'BOK', '76').
card_multiverse_id('okiba-gang shinobi'/'BOK', '74576').

card_in_set('opal-eye, konda\'s yojimbo', 'BOK').
card_original_type('opal-eye, konda\'s yojimbo'/'BOK', 'Legendary Creature — Fox Samurai').
card_original_text('opal-eye, konda\'s yojimbo'/'BOK', 'Bushido 1; defender (This creature can\'t attack.)\n{T}: The next time a source of your choice would deal damage this turn, that damage is dealt to Opal-Eye, Konda\'s Yojimbo instead.\n{1}{W}: Prevent the next 1 damage that would be dealt to Opal-Eye this turn.').
card_first_print('opal-eye, konda\'s yojimbo', 'BOK').
card_image_name('opal-eye, konda\'s yojimbo'/'BOK', 'opal-eye, konda\'s yojimbo').
card_uid('opal-eye, konda\'s yojimbo'/'BOK', 'BOK:Opal-Eye, Konda\'s Yojimbo:opal-eye, konda\'s yojimbo').
card_rarity('opal-eye, konda\'s yojimbo'/'BOK', 'Rare').
card_artist('opal-eye, konda\'s yojimbo'/'BOK', 'Greg Staples').
card_number('opal-eye, konda\'s yojimbo'/'BOK', '17').
card_multiverse_id('opal-eye, konda\'s yojimbo'/'BOK', '74655').

card_in_set('orb of dreams', 'BOK').
card_original_type('orb of dreams'/'BOK', 'Artifact').
card_original_text('orb of dreams'/'BOK', 'Permanents come into play tapped.').
card_first_print('orb of dreams', 'BOK').
card_image_name('orb of dreams'/'BOK', 'orb of dreams').
card_uid('orb of dreams'/'BOK', 'BOK:Orb of Dreams:orb of dreams').
card_rarity('orb of dreams'/'BOK', 'Rare').
card_artist('orb of dreams'/'BOK', 'Dany Orizio').
card_number('orb of dreams'/'BOK', '156').
card_flavor_text('orb of dreams'/'BOK', '\"Behind the eyelids, a flickering flame.\nBeneath the dream, a flickering sorrow.\nCan peace find you in this sleep\nWhen all the world is tossed by war?\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('orb of dreams'/'BOK', '74607').

card_in_set('ornate kanzashi', 'BOK').
card_original_type('ornate kanzashi'/'BOK', 'Artifact').
card_original_text('ornate kanzashi'/'BOK', '{2}, {T}: Target opponent removes the top card of his or her library from the game. You may play that card this turn.').
card_first_print('ornate kanzashi', 'BOK').
card_image_name('ornate kanzashi'/'BOK', 'ornate kanzashi').
card_uid('ornate kanzashi'/'BOK', 'BOK:Ornate Kanzashi:ornate kanzashi').
card_rarity('ornate kanzashi'/'BOK', 'Rare').
card_artist('ornate kanzashi'/'BOK', 'Heather Hudson').
card_number('ornate kanzashi'/'BOK', '157').
card_flavor_text('ornate kanzashi'/'BOK', 'Masako was privy to a great many of Konda\'s deepest secrets, and her hair was always perfect.').
card_multiverse_id('ornate kanzashi'/'BOK', '74512').

card_in_set('overblaze', 'BOK').
card_original_type('overblaze'/'BOK', 'Instant — Arcane').
card_original_text('overblaze'/'BOK', 'Each time target permanent would deal damage to a creature or player this turn, it deals double that damage to that creature or player instead.\nSplice onto Arcane {2}{R}{R} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('overblaze', 'BOK').
card_image_name('overblaze'/'BOK', 'overblaze').
card_uid('overblaze'/'BOK', 'BOK:Overblaze:overblaze').
card_rarity('overblaze'/'BOK', 'Uncommon').
card_artist('overblaze'/'BOK', 'Ron Spencer').
card_number('overblaze'/'BOK', '114').
card_multiverse_id('overblaze'/'BOK', '81986').

card_in_set('oyobi, who split the heavens', 'BOK').
card_original_type('oyobi, who split the heavens'/'BOK', 'Legendary Creature — Spirit').
card_original_text('oyobi, who split the heavens'/'BOK', 'Flying\nWhenever you play a Spirit or Arcane spell, put a 3/3 white Spirit creature token with flying into play.').
card_first_print('oyobi, who split the heavens', 'BOK').
card_image_name('oyobi, who split the heavens'/'BOK', 'oyobi, who split the heavens').
card_uid('oyobi, who split the heavens'/'BOK', 'BOK:Oyobi, Who Split the Heavens:oyobi, who split the heavens').
card_rarity('oyobi, who split the heavens'/'BOK', 'Rare').
card_artist('oyobi, who split the heavens'/'BOK', 'Christopher Moeller').
card_number('oyobi, who split the heavens'/'BOK', '18').
card_flavor_text('oyobi, who split the heavens'/'BOK', 'Her angry call split the sky. From that rift descended her champions.').
card_multiverse_id('oyobi, who split the heavens'/'BOK', '74027').

card_in_set('patron of the akki', 'BOK').
card_original_type('patron of the akki'/'BOK', 'Legendary Creature — Spirit').
card_original_text('patron of the akki'/'BOK', 'Goblin offering (You may play this card any time you could play an instant by sacrificing a Goblin and paying the difference in mana costs between this and the sacrificed Goblin. Mana cost includes color.)\nWhenever Patron of the Akki attacks, creatures you control get +2/+0 until end of turn.').
card_first_print('patron of the akki', 'BOK').
card_image_name('patron of the akki'/'BOK', 'patron of the akki').
card_uid('patron of the akki'/'BOK', 'BOK:Patron of the Akki:patron of the akki').
card_rarity('patron of the akki'/'BOK', 'Rare').
card_artist('patron of the akki'/'BOK', 'Jim Nelson').
card_number('patron of the akki'/'BOK', '115').
card_multiverse_id('patron of the akki'/'BOK', '74109').

card_in_set('patron of the kitsune', 'BOK').
card_original_type('patron of the kitsune'/'BOK', 'Legendary Creature — Spirit').
card_original_text('patron of the kitsune'/'BOK', 'Fox offering (You may play this card any time you could play an instant by sacrificing a Fox and paying the difference in mana costs between this and the sacrificed Fox. Mana cost includes color.)\nWhenever a creature attacks, you may gain 1 life.').
card_first_print('patron of the kitsune', 'BOK').
card_image_name('patron of the kitsune'/'BOK', 'patron of the kitsune').
card_uid('patron of the kitsune'/'BOK', 'BOK:Patron of the Kitsune:patron of the kitsune').
card_rarity('patron of the kitsune'/'BOK', 'Rare').
card_artist('patron of the kitsune'/'BOK', 'Ben Thompson').
card_number('patron of the kitsune'/'BOK', '19').
card_multiverse_id('patron of the kitsune'/'BOK', '74620').

card_in_set('patron of the moon', 'BOK').
card_original_type('patron of the moon'/'BOK', 'Legendary Creature — Spirit').
card_original_text('patron of the moon'/'BOK', 'Moonfolk offering (You may play this card any time you could play an instant by sacrificing a Moonfolk and paying the difference in mana costs between this and the sacrificed Moonfolk. Mana cost includes color.)\nFlying\n{1}: Put up to two land cards from your hand into play tapped.').
card_first_print('patron of the moon', 'BOK').
card_image_name('patron of the moon'/'BOK', 'patron of the moon').
card_uid('patron of the moon'/'BOK', 'BOK:Patron of the Moon:patron of the moon').
card_rarity('patron of the moon'/'BOK', 'Rare').
card_artist('patron of the moon'/'BOK', 'Scott M. Fischer').
card_number('patron of the moon'/'BOK', '45').
card_multiverse_id('patron of the moon'/'BOK', '74669').

card_in_set('patron of the nezumi', 'BOK').
card_original_type('patron of the nezumi'/'BOK', 'Legendary Creature — Spirit').
card_original_text('patron of the nezumi'/'BOK', 'Rat offering (You may play this card any time you could play an instant by sacrificing a Rat and paying the difference in mana costs between this and the sacrificed Rat. Mana cost includes color.)\nWhenever a permanent is put into an opponent\'s graveyard, that player loses 1 life.').
card_first_print('patron of the nezumi', 'BOK').
card_image_name('patron of the nezumi'/'BOK', 'patron of the nezumi').
card_uid('patron of the nezumi'/'BOK', 'BOK:Patron of the Nezumi:patron of the nezumi').
card_rarity('patron of the nezumi'/'BOK', 'Rare').
card_artist('patron of the nezumi'/'BOK', 'Kev Walker').
card_number('patron of the nezumi'/'BOK', '77').
card_multiverse_id('patron of the nezumi'/'BOK', '74646').

card_in_set('patron of the orochi', 'BOK').
card_original_type('patron of the orochi'/'BOK', 'Legendary Creature — Spirit').
card_original_text('patron of the orochi'/'BOK', 'Snake offering (You may play this card any time you could play an instant by sacrificing a Snake and paying the difference in mana costs between this and the sacrificed Snake. Mana cost includes color.)\n{T}: Untap all Forests and all green creatures. Play this ability only once each turn.').
card_first_print('patron of the orochi', 'BOK').
card_image_name('patron of the orochi'/'BOK', 'patron of the orochi').
card_uid('patron of the orochi'/'BOK', 'BOK:Patron of the Orochi:patron of the orochi').
card_rarity('patron of the orochi'/'BOK', 'Rare').
card_artist('patron of the orochi'/'BOK', 'Christopher Moeller').
card_number('patron of the orochi'/'BOK', '138').
card_multiverse_id('patron of the orochi'/'BOK', '74542').

card_in_set('petalmane baku', 'BOK').
card_original_type('petalmane baku'/'BOK', 'Creature — Spirit').
card_original_text('petalmane baku'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Petalmane Baku.\n{1}, Remove X ki counters from Petalmane Baku: Add X mana of any one color to your mana pool.').
card_first_print('petalmane baku', 'BOK').
card_image_name('petalmane baku'/'BOK', 'petalmane baku').
card_uid('petalmane baku'/'BOK', 'BOK:Petalmane Baku:petalmane baku').
card_rarity('petalmane baku'/'BOK', 'Common').
card_artist('petalmane baku'/'BOK', 'Rebecca Guay').
card_number('petalmane baku'/'BOK', '139').
card_multiverse_id('petalmane baku'/'BOK', '74455').

card_in_set('phantom wings', 'BOK').
card_original_type('phantom wings'/'BOK', 'Enchant Creature').
card_original_text('phantom wings'/'BOK', 'Enchanted creature has flying.\nSacrifice Phantom Wings: Return enchanted creature to its owner\'s hand.').
card_image_name('phantom wings'/'BOK', 'phantom wings').
card_uid('phantom wings'/'BOK', 'BOK:Phantom Wings:phantom wings').
card_rarity('phantom wings'/'BOK', 'Common').
card_artist('phantom wings'/'BOK', 'Greg Staples').
card_number('phantom wings'/'BOK', '46').
card_flavor_text('phantom wings'/'BOK', 'Many kami could fly, which put some warriors at a distinct disadvantage. The mages of Minamo took it upon themselves to correct that imbalance.').
card_multiverse_id('phantom wings'/'BOK', '74496').

card_in_set('psychic spear', 'BOK').
card_original_type('psychic spear'/'BOK', 'Sorcery').
card_original_text('psychic spear'/'BOK', 'Target player reveals his or her hand. Choose a Spirit or Arcane card from it. That player discards that card.').
card_first_print('psychic spear', 'BOK').
card_image_name('psychic spear'/'BOK', 'psychic spear').
card_uid('psychic spear'/'BOK', 'BOK:Psychic Spear:psychic spear').
card_rarity('psychic spear'/'BOK', 'Common').
card_artist('psychic spear'/'BOK', 'Ron Spears').
card_number('psychic spear'/'BOK', '78').
card_flavor_text('psychic spear'/'BOK', '\"The wizards of Takenuma Swamp faced the horrors of humanity every day. It\'s no wonder they fared so well against the kami.\"\n—Observations of the Kami War').
card_multiverse_id('psychic spear'/'BOK', '74554').

card_in_set('pus kami', 'BOK').
card_original_type('pus kami'/'BOK', 'Creature — Spirit').
card_original_text('pus kami'/'BOK', '{B}, Sacrifice Pus Kami: Destroy target nonblack creature.\nSoulshift 6 (When this is put into a graveyard from play, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_first_print('pus kami', 'BOK').
card_image_name('pus kami'/'BOK', 'pus kami').
card_uid('pus kami'/'BOK', 'BOK:Pus Kami:pus kami').
card_rarity('pus kami'/'BOK', 'Uncommon').
card_artist('pus kami'/'BOK', 'Dave Allsop').
card_number('pus kami'/'BOK', '79').
card_multiverse_id('pus kami'/'BOK', '74124').

card_in_set('quash', 'BOK').
card_original_type('quash'/'BOK', 'Instant').
card_original_text('quash'/'BOK', 'Counter target instant or sorcery spell. Search its controller\'s graveyard, hand, and library for all cards with the same name as that spell and remove them from the game. That player then shuffles his or her library.').
card_image_name('quash'/'BOK', 'quash').
card_uid('quash'/'BOK', 'BOK:Quash:quash').
card_rarity('quash'/'BOK', 'Uncommon').
card_artist('quash'/'BOK', 'Shishizaru').
card_number('quash'/'BOK', '47').
card_multiverse_id('quash'/'BOK', '74500').

card_in_set('quillmane baku', 'BOK').
card_original_type('quillmane baku'/'BOK', 'Creature — Spirit').
card_original_text('quillmane baku'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Quillmane Baku.\n{1}, {T}, Remove X ki counters from Quillmane Baku: Return target creature with converted mana cost X or less to its owner\'s hand.').
card_first_print('quillmane baku', 'BOK').
card_image_name('quillmane baku'/'BOK', 'quillmane baku').
card_uid('quillmane baku'/'BOK', 'BOK:Quillmane Baku:quillmane baku').
card_rarity('quillmane baku'/'BOK', 'Common').
card_artist('quillmane baku'/'BOK', 'David Martin').
card_number('quillmane baku'/'BOK', '48').
card_multiverse_id('quillmane baku'/'BOK', '74657').

card_in_set('reduce to dreams', 'BOK').
card_original_type('reduce to dreams'/'BOK', 'Sorcery').
card_original_text('reduce to dreams'/'BOK', 'Return all artifacts and enchantments to their owners\' hands.').
card_first_print('reduce to dreams', 'BOK').
card_image_name('reduce to dreams'/'BOK', 'reduce to dreams').
card_uid('reduce to dreams'/'BOK', 'BOK:Reduce to Dreams:reduce to dreams').
card_rarity('reduce to dreams'/'BOK', 'Rare').
card_artist('reduce to dreams'/'BOK', 'Daren Bader').
card_number('reduce to dreams'/'BOK', '49').
card_flavor_text('reduce to dreams'/'BOK', '\"This world is a dream. We cling to our toys like children, but sooner or later we must learn to live without them.\"\n—Sensei Hisoka').
card_multiverse_id('reduce to dreams'/'BOK', '74110').

card_in_set('ribbons of the reikai', 'BOK').
card_original_type('ribbons of the reikai'/'BOK', 'Sorcery — Arcane').
card_original_text('ribbons of the reikai'/'BOK', 'Draw a card for each Spirit you control.').
card_first_print('ribbons of the reikai', 'BOK').
card_image_name('ribbons of the reikai'/'BOK', 'ribbons of the reikai').
card_uid('ribbons of the reikai'/'BOK', 'BOK:Ribbons of the Reikai:ribbons of the reikai').
card_rarity('ribbons of the reikai'/'BOK', 'Common').
card_artist('ribbons of the reikai'/'BOK', 'Martina Pilcerova').
card_number('ribbons of the reikai'/'BOK', '50').
card_flavor_text('ribbons of the reikai'/'BOK', '\"If wisdom is a river, then we cup our hands, reach in, and drink from it in sips. The kami, however, are like fish, swimming, breathing, surrounded in its presence.\"\n—Dosan the Falling Leaf').
card_multiverse_id('ribbons of the reikai'/'BOK', '74532').

card_in_set('roar of jukai', 'BOK').
card_original_type('roar of jukai'/'BOK', 'Instant — Arcane').
card_original_text('roar of jukai'/'BOK', 'If you control a Forest, each blocked creature gets +2/+2 until end of turn.\nSplice onto Arcane—An opponent gains 5 life. (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('roar of jukai', 'BOK').
card_image_name('roar of jukai'/'BOK', 'roar of jukai').
card_uid('roar of jukai'/'BOK', 'BOK:Roar of Jukai:roar of jukai').
card_rarity('roar of jukai'/'BOK', 'Common').
card_artist('roar of jukai'/'BOK', 'Ron Spencer').
card_number('roar of jukai'/'BOK', '140').
card_multiverse_id('roar of jukai'/'BOK', '74629').

card_in_set('ronin cliffrider', 'BOK').
card_original_type('ronin cliffrider'/'BOK', 'Creature — Human Samurai').
card_original_text('ronin cliffrider'/'BOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever Ronin Cliffrider attacks, you may have it deal 1 damage to each creature defending player controls.').
card_first_print('ronin cliffrider', 'BOK').
card_image_name('ronin cliffrider'/'BOK', 'ronin cliffrider').
card_uid('ronin cliffrider'/'BOK', 'BOK:Ronin Cliffrider:ronin cliffrider').
card_rarity('ronin cliffrider'/'BOK', 'Uncommon').
card_artist('ronin cliffrider'/'BOK', 'Dan Scott').
card_number('ronin cliffrider'/'BOK', '116').
card_multiverse_id('ronin cliffrider'/'BOK', '74122').

card_in_set('ronin warclub', 'BOK').
card_original_type('ronin warclub'/'BOK', 'Artifact — Equipment').
card_original_text('ronin warclub'/'BOK', 'Equipped creature gets +2/+1.\nWhenever a creature comes into play under your control, attach Ronin Warclub to that creature.\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ronin warclub', 'BOK').
card_image_name('ronin warclub'/'BOK', 'ronin warclub').
card_uid('ronin warclub'/'BOK', 'BOK:Ronin Warclub:ronin warclub').
card_rarity('ronin warclub'/'BOK', 'Uncommon').
card_artist('ronin warclub'/'BOK', 'Pete Venters').
card_number('ronin warclub'/'BOK', '158').
card_multiverse_id('ronin warclub'/'BOK', '74473').

card_in_set('sakiko, mother of summer', 'BOK').
card_original_type('sakiko, mother of summer'/'BOK', 'Legendary Creature — Snake Shaman').
card_original_text('sakiko, mother of summer'/'BOK', 'Whenever a creature you control deals combat damage to a player, add that much {G} to your mana pool. This mana doesn\'t cause mana burn. Until end of turn, this mana doesn\'t empty from your mana pool as phases end.').
card_first_print('sakiko, mother of summer', 'BOK').
card_image_name('sakiko, mother of summer'/'BOK', 'sakiko, mother of summer').
card_uid('sakiko, mother of summer'/'BOK', 'BOK:Sakiko, Mother of Summer:sakiko, mother of summer').
card_rarity('sakiko, mother of summer'/'BOK', 'Rare').
card_artist('sakiko, mother of summer'/'BOK', 'Michael Sutfin').
card_number('sakiko, mother of summer'/'BOK', '141').
card_multiverse_id('sakiko, mother of summer'/'BOK', '74544').

card_in_set('sakura-tribe springcaller', 'BOK').
card_original_type('sakura-tribe springcaller'/'BOK', 'Creature — Snake Shaman').
card_original_text('sakura-tribe springcaller'/'BOK', 'At the beginning of your upkeep, add {G} to your mana pool. This mana doesn\'t cause mana burn. Until end of turn, this mana doesn\'t empty from your mana pool as phases end.').
card_first_print('sakura-tribe springcaller', 'BOK').
card_image_name('sakura-tribe springcaller'/'BOK', 'sakura-tribe springcaller').
card_uid('sakura-tribe springcaller'/'BOK', 'BOK:Sakura-Tribe Springcaller:sakura-tribe springcaller').
card_rarity('sakura-tribe springcaller'/'BOK', 'Common').
card_artist('sakura-tribe springcaller'/'BOK', 'Pete Venters').
card_number('sakura-tribe springcaller'/'BOK', '142').
card_multiverse_id('sakura-tribe springcaller'/'BOK', '74092').

card_in_set('scaled hulk', 'BOK').
card_original_type('scaled hulk'/'BOK', 'Creature — Spirit').
card_original_text('scaled hulk'/'BOK', 'Whenever you play a Spirit or Arcane spell, Scaled Hulk gets +2/+2 until end of turn.').
card_first_print('scaled hulk', 'BOK').
card_image_name('scaled hulk'/'BOK', 'scaled hulk').
card_uid('scaled hulk'/'BOK', 'BOK:Scaled Hulk:scaled hulk').
card_rarity('scaled hulk'/'BOK', 'Common').
card_artist('scaled hulk'/'BOK', 'Arnie Swekel').
card_number('scaled hulk'/'BOK', '143').
card_flavor_text('scaled hulk'/'BOK', '\"Say, what rhymes with ‘run for your lives\'?\"\n—Ku-Ku, akki poet').
card_multiverse_id('scaled hulk'/'BOK', '74541').

card_in_set('scarmaker', 'BOK').
card_original_type('scarmaker'/'BOK', 'Creature — Human Warrior').
card_original_text('scarmaker'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Hired Muscle.\nAt end of turn, if there are two or more ki counters on Hired Muscle, you may flip it.\n----\nScarmaker\nLegendary Creature Spirit\n4/4\nRemove a ki counter from Scarmaker: Target creature gains fear until end of turn.').
card_first_print('scarmaker', 'BOK').
card_image_name('scarmaker'/'BOK', 'scarmaker').
card_uid('scarmaker'/'BOK', 'BOK:Scarmaker:scarmaker').
card_rarity('scarmaker'/'BOK', 'Uncommon').
card_artist('scarmaker'/'BOK', 'Arnie Swekel').
card_number('scarmaker'/'BOK', '69b').
card_multiverse_id('scarmaker'/'BOK', '74476').

card_in_set('scour', 'BOK').
card_original_type('scour'/'BOK', 'Instant').
card_original_text('scour'/'BOK', 'Remove target enchantment from the game. Search its controller\'s graveyard, hand, and library for all cards with the same name as that enchantment and remove them from the game. That player then shuffles his or her library.').
card_image_name('scour'/'BOK', 'scour').
card_uid('scour'/'BOK', 'BOK:Scour:scour').
card_rarity('scour'/'BOK', 'Uncommon').
card_artist('scour'/'BOK', 'Ittoku').
card_number('scour'/'BOK', '20').
card_multiverse_id('scour'/'BOK', '74640').

card_in_set('scourge of numai', 'BOK').
card_original_type('scourge of numai'/'BOK', 'Creature — Demon Spirit').
card_original_text('scourge of numai'/'BOK', 'At the beginning of your upkeep, you lose 2 life if you don\'t control an Ogre.').
card_first_print('scourge of numai', 'BOK').
card_image_name('scourge of numai'/'BOK', 'scourge of numai').
card_uid('scourge of numai'/'BOK', 'BOK:Scourge of Numai:scourge of numai').
card_rarity('scourge of numai'/'BOK', 'Uncommon').
card_artist('scourge of numai'/'BOK', 'Arnie Swekel').
card_number('scourge of numai'/'BOK', '80').
card_flavor_text('scourge of numai'/'BOK', '\"Where a once-proud human city stood, only the ruins of Numai remain, deep amid rotting bamboo and plagued by oni.\"\n—The History of Kamigawa').
card_multiverse_id('scourge of numai'/'BOK', '74488').

card_in_set('shimmering glasskite', 'BOK').
card_original_type('shimmering glasskite'/'BOK', 'Creature — Spirit').
card_original_text('shimmering glasskite'/'BOK', 'Flying\nWhenever Shimmering Glasskite becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.').
card_first_print('shimmering glasskite', 'BOK').
card_image_name('shimmering glasskite'/'BOK', 'shimmering glasskite').
card_uid('shimmering glasskite'/'BOK', 'BOK:Shimmering Glasskite:shimmering glasskite').
card_rarity('shimmering glasskite'/'BOK', 'Common').
card_artist('shimmering glasskite'/'BOK', 'John Avon').
card_number('shimmering glasskite'/'BOK', '51').
card_flavor_text('shimmering glasskite'/'BOK', 'A child\'s whisper could crack its shell, but not even an oni\'s scream could penetrate it.').
card_multiverse_id('shimmering glasskite'/'BOK', '81978').

card_in_set('shining shoal', 'BOK').
card_original_type('shining shoal'/'BOK', 'Instant — Arcane').
card_original_text('shining shoal'/'BOK', 'You may remove a white card with converted mana cost X in your hand from the game rather than pay Shining Shoal\'s mana cost.\nThe next X damage that a source of your choice would deal to you or a creature you control this turn is dealt to target creature or player instead.').
card_first_print('shining shoal', 'BOK').
card_image_name('shining shoal'/'BOK', 'shining shoal').
card_uid('shining shoal'/'BOK', 'BOK:Shining Shoal:shining shoal').
card_rarity('shining shoal'/'BOK', 'Rare').
card_artist('shining shoal'/'BOK', 'Ben Thompson').
card_number('shining shoal'/'BOK', '21').
card_multiverse_id('shining shoal'/'BOK', '74519').

card_in_set('shinka gatekeeper', 'BOK').
card_original_type('shinka gatekeeper'/'BOK', 'Creature — Ogre Warrior').
card_original_text('shinka gatekeeper'/'BOK', 'Whenever Shinka Gatekeeper is dealt damage, it deals that much damage to you.').
card_first_print('shinka gatekeeper', 'BOK').
card_image_name('shinka gatekeeper'/'BOK', 'shinka gatekeeper').
card_uid('shinka gatekeeper'/'BOK', 'BOK:Shinka Gatekeeper:shinka gatekeeper').
card_rarity('shinka gatekeeper'/'BOK', 'Common').
card_artist('shinka gatekeeper'/'BOK', 'Pete Venters').
card_number('shinka gatekeeper'/'BOK', '117').
card_flavor_text('shinka gatekeeper'/'BOK', '\"Understanding is not a virtue of the ogre. Do not seek to reason your way into Shinka Keep.\"\n—Kiku, Night\'s Flower').
card_multiverse_id('shinka gatekeeper'/'BOK', '74099').

card_in_set('shirei, shizo\'s caretaker', 'BOK').
card_original_type('shirei, shizo\'s caretaker'/'BOK', 'Legendary Creature — Spirit').
card_original_text('shirei, shizo\'s caretaker'/'BOK', 'Whenever a creature with power 1 or less is put into your graveyard from play, you may return that creature card to play under your control at end of turn if Shirei, Shizo\'s Caretaker is still in play.').
card_first_print('shirei, shizo\'s caretaker', 'BOK').
card_image_name('shirei, shizo\'s caretaker'/'BOK', 'shirei, shizo\'s caretaker').
card_uid('shirei, shizo\'s caretaker'/'BOK', 'BOK:Shirei, Shizo\'s Caretaker:shirei, shizo\'s caretaker').
card_rarity('shirei, shizo\'s caretaker'/'BOK', 'Rare').
card_artist('shirei, shizo\'s caretaker'/'BOK', 'Wayne Reynolds').
card_number('shirei, shizo\'s caretaker'/'BOK', '81').
card_multiverse_id('shirei, shizo\'s caretaker'/'BOK', '81987').

card_in_set('shizuko, caller of autumn', 'BOK').
card_original_type('shizuko, caller of autumn'/'BOK', 'Legendary Creature — Snake Shaman').
card_original_text('shizuko, caller of autumn'/'BOK', 'At the beginning of each player\'s upkeep, that player adds {G}{G}{G} to his or her mana pool. This mana doesn\'t cause mana burn. Until end of turn, this mana doesn\'t empty from that player\'s mana pool as phases end.').
card_first_print('shizuko, caller of autumn', 'BOK').
card_image_name('shizuko, caller of autumn'/'BOK', 'shizuko, caller of autumn').
card_uid('shizuko, caller of autumn'/'BOK', 'BOK:Shizuko, Caller of Autumn:shizuko, caller of autumn').
card_rarity('shizuko, caller of autumn'/'BOK', 'Rare').
card_artist('shizuko, caller of autumn'/'BOK', 'Michael Sutfin').
card_number('shizuko, caller of autumn'/'BOK', '144').
card_multiverse_id('shizuko, caller of autumn'/'BOK', '74436').

card_in_set('shuko', 'BOK').
card_original_type('shuko'/'BOK', 'Artifact — Equipment').
card_original_text('shuko'/'BOK', 'Equipped creature gets +1/+0.\nEquip {0} ({0}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('shuko', 'BOK').
card_image_name('shuko'/'BOK', 'shuko').
card_uid('shuko'/'BOK', 'BOK:Shuko:shuko').
card_rarity('shuko'/'BOK', 'Uncommon').
card_artist('shuko'/'BOK', 'Tim Hildebrandt').
card_number('shuko'/'BOK', '159').
card_flavor_text('shuko'/'BOK', '\"One scratch on a wall meant safe passage, two that the target had already been raided, and three warned of a trap.\"\n—Weaponry of Kamigawa').
card_multiverse_id('shuko'/'BOK', '74415').

card_in_set('shuriken', 'BOK').
card_original_type('shuriken'/'BOK', 'Artifact — Equipment').
card_original_text('shuriken'/'BOK', 'Equipped creature has \"{T}, Unattach Shuriken: Shuriken deals 2 damage to target creature. That creature\'s controller gains control of Shuriken unless it was unattached from a Ninja.\"\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('shuriken', 'BOK').
card_image_name('shuriken'/'BOK', 'shuriken').
card_uid('shuriken'/'BOK', 'BOK:Shuriken:shuriken').
card_rarity('shuriken'/'BOK', 'Uncommon').
card_artist('shuriken'/'BOK', 'Matt Cavotta').
card_number('shuriken'/'BOK', '160').
card_multiverse_id('shuriken'/'BOK', '81997').

card_in_set('sickening shoal', 'BOK').
card_original_type('sickening shoal'/'BOK', 'Instant — Arcane').
card_original_text('sickening shoal'/'BOK', 'You may remove a black card with converted mana cost X in your hand from the game rather than pay Sickening Shoal\'s mana cost.\nTarget creature gets -X/-X until end of turn.').
card_first_print('sickening shoal', 'BOK').
card_image_name('sickening shoal'/'BOK', 'sickening shoal').
card_uid('sickening shoal'/'BOK', 'BOK:Sickening Shoal:sickening shoal').
card_rarity('sickening shoal'/'BOK', 'Rare').
card_artist('sickening shoal'/'BOK', 'Dan Scott').
card_number('sickening shoal'/'BOK', '82').
card_multiverse_id('sickening shoal'/'BOK', '74127').

card_in_set('silverstorm samurai', 'BOK').
card_original_type('silverstorm samurai'/'BOK', 'Creature — Fox Samurai').
card_original_text('silverstorm samurai'/'BOK', 'You may play Silverstorm Samurai any time you could play an instant.\nBushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_first_print('silverstorm samurai', 'BOK').
card_image_name('silverstorm samurai'/'BOK', 'silverstorm samurai').
card_uid('silverstorm samurai'/'BOK', 'BOK:Silverstorm Samurai:silverstorm samurai').
card_rarity('silverstorm samurai'/'BOK', 'Common').
card_artist('silverstorm samurai'/'BOK', 'Michael Sutfin').
card_number('silverstorm samurai'/'BOK', '22').
card_multiverse_id('silverstorm samurai'/'BOK', '74545').

card_in_set('skullmane baku', 'BOK').
card_original_type('skullmane baku'/'BOK', 'Creature — Spirit').
card_original_text('skullmane baku'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Skullmane Baku.\n{1}, {T}, Remove X ki counters from Skullmane Baku: Target creature gets -X/-X until end of turn.').
card_first_print('skullmane baku', 'BOK').
card_image_name('skullmane baku'/'BOK', 'skullmane baku').
card_uid('skullmane baku'/'BOK', 'BOK:Skullmane Baku:skullmane baku').
card_rarity('skullmane baku'/'BOK', 'Common').
card_artist('skullmane baku'/'BOK', 'Tim Hildebrandt').
card_number('skullmane baku'/'BOK', '83').
card_multiverse_id('skullmane baku'/'BOK', '74577').

card_in_set('skullsnatcher', 'BOK').
card_original_type('skullsnatcher'/'BOK', 'Creature — Rat Ninja').
card_original_text('skullsnatcher'/'BOK', 'Ninjutsu {B} ({B}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Skullsnatcher deals combat damage to a player, remove up to two target cards in that player\'s graveyard from the game.').
card_first_print('skullsnatcher', 'BOK').
card_image_name('skullsnatcher'/'BOK', 'skullsnatcher').
card_uid('skullsnatcher'/'BOK', 'BOK:Skullsnatcher:skullsnatcher').
card_rarity('skullsnatcher'/'BOK', 'Common').
card_artist('skullsnatcher'/'BOK', 'Matt Cavotta').
card_number('skullsnatcher'/'BOK', '84').
card_multiverse_id('skullsnatcher'/'BOK', '81983').

card_in_set('slumbering tora', 'BOK').
card_original_type('slumbering tora'/'BOK', 'Artifact').
card_original_text('slumbering tora'/'BOK', '{2}, Discard a Spirit or Arcane card: Slumbering Tora becomes an X/X artifact creature until end of turn, where X is the discarded card\'s converted mana cost.').
card_first_print('slumbering tora', 'BOK').
card_image_name('slumbering tora'/'BOK', 'slumbering tora').
card_uid('slumbering tora'/'BOK', 'BOK:Slumbering Tora:slumbering tora').
card_rarity('slumbering tora'/'BOK', 'Rare').
card_artist('slumbering tora'/'BOK', 'Doug Chaffee').
card_number('slumbering tora'/'BOK', '161').
card_multiverse_id('slumbering tora'/'BOK', '74628').

card_in_set('soratami mindsweeper', 'BOK').
card_original_type('soratami mindsweeper'/'BOK', 'Creature — Moonfolk Wizard').
card_original_text('soratami mindsweeper'/'BOK', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('soratami mindsweeper', 'BOK').
card_image_name('soratami mindsweeper'/'BOK', 'soratami mindsweeper').
card_uid('soratami mindsweeper'/'BOK', 'BOK:Soratami Mindsweeper:soratami mindsweeper').
card_rarity('soratami mindsweeper'/'BOK', 'Uncommon').
card_artist('soratami mindsweeper'/'BOK', 'Alex Horley-Orlandelli').
card_number('soratami mindsweeper'/'BOK', '52').
card_multiverse_id('soratami mindsweeper'/'BOK', '73995').

card_in_set('sosuke\'s summons', 'BOK').
card_original_type('sosuke\'s summons'/'BOK', 'Sorcery').
card_original_text('sosuke\'s summons'/'BOK', 'Put two 1/1 green Snake creature tokens into play.\nWhenever a nontoken Snake comes into play under your control, you may return Sosuke\'s Summons from your graveyard to your hand.').
card_first_print('sosuke\'s summons', 'BOK').
card_image_name('sosuke\'s summons'/'BOK', 'sosuke\'s summons').
card_uid('sosuke\'s summons'/'BOK', 'BOK:Sosuke\'s Summons:sosuke\'s summons').
card_rarity('sosuke\'s summons'/'BOK', 'Uncommon').
card_artist('sosuke\'s summons'/'BOK', 'Kev Walker').
card_number('sosuke\'s summons'/'BOK', '145').
card_multiverse_id('sosuke\'s summons'/'BOK', '74090').

card_in_set('sowing salt', 'BOK').
card_original_type('sowing salt'/'BOK', 'Sorcery').
card_original_text('sowing salt'/'BOK', 'Remove target nonbasic land from the game. Search its controller\'s graveyard, hand, and library for all cards with the same name as that land and remove them from the game. That player then shuffles his or her library.').
card_image_name('sowing salt'/'BOK', 'sowing salt').
card_uid('sowing salt'/'BOK', 'BOK:Sowing Salt:sowing salt').
card_rarity('sowing salt'/'BOK', 'Uncommon').
card_artist('sowing salt'/'BOK', 'Hideaki Takamura').
card_number('sowing salt'/'BOK', '118').
card_multiverse_id('sowing salt'/'BOK', '74623').

card_in_set('splinter', 'BOK').
card_original_type('splinter'/'BOK', 'Sorcery').
card_original_text('splinter'/'BOK', 'Remove target artifact from the game. Search its controller\'s graveyard, hand, and library for all cards with the same name as that artifact and remove them from the game. That player then shuffles his or her library.').
card_image_name('splinter'/'BOK', 'splinter').
card_uid('splinter'/'BOK', 'BOK:Splinter:splinter').
card_rarity('splinter'/'BOK', 'Uncommon').
card_artist('splinter'/'BOK', 'Tsutomu Kawade').
card_number('splinter'/'BOK', '146').
card_multiverse_id('splinter'/'BOK', '74615').

card_in_set('split-tail miko', 'BOK').
card_original_type('split-tail miko'/'BOK', 'Creature — Fox Cleric').
card_original_text('split-tail miko'/'BOK', '{W}, {T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('split-tail miko', 'BOK').
card_image_name('split-tail miko'/'BOK', 'split-tail miko').
card_uid('split-tail miko'/'BOK', 'BOK:Split-Tail Miko:split-tail miko').
card_rarity('split-tail miko'/'BOK', 'Common').
card_artist('split-tail miko'/'BOK', 'Kev Walker').
card_number('split-tail miko'/'BOK', '23').
card_flavor_text('split-tail miko'/'BOK', '\"I wish there were no use for those with my talents. I wish that I could walk Kamigawa forgotten and unneeded, with no war wounds to heal and no broken bones to mend.\"').
card_multiverse_id('split-tail miko'/'BOK', '74493').

card_in_set('stir the grave', 'BOK').
card_original_type('stir the grave'/'BOK', 'Sorcery').
card_original_text('stir the grave'/'BOK', 'Return target creature card with converted mana cost X or less from your graveyard to play.').
card_first_print('stir the grave', 'BOK').
card_image_name('stir the grave'/'BOK', 'stir the grave').
card_uid('stir the grave'/'BOK', 'BOK:Stir the Grave:stir the grave').
card_rarity('stir the grave'/'BOK', 'Common').
card_artist('stir the grave'/'BOK', 'Jim Nelson').
card_number('stir the grave'/'BOK', '85').
card_flavor_text('stir the grave'/'BOK', '\"Your lungs may not draw breath, but while your hands can grip a sword, you will be useful to me.\"\n—Nighteyes, nezumi necromancer').
card_multiverse_id('stir the grave'/'BOK', '74643').

card_in_set('stream of consciousness', 'BOK').
card_original_type('stream of consciousness'/'BOK', 'Instant — Arcane').
card_original_text('stream of consciousness'/'BOK', 'Target player shuffles up to four target cards from his or her graveyard into his or her library.').
card_first_print('stream of consciousness', 'BOK').
card_image_name('stream of consciousness'/'BOK', 'stream of consciousness').
card_uid('stream of consciousness'/'BOK', 'BOK:Stream of Consciousness:stream of consciousness').
card_rarity('stream of consciousness'/'BOK', 'Uncommon').
card_artist('stream of consciousness'/'BOK', 'John Avon').
card_number('stream of consciousness'/'BOK', '53').
card_flavor_text('stream of consciousness'/'BOK', '\"All things return to their beginnings. The waters that spill across the Kamitaki Falls flow to sea, only to be returned to her as the rain that joins the mighty river.\"\n—Dosan the Falling Leaf').
card_multiverse_id('stream of consciousness'/'BOK', '73997').

card_in_set('sway of the stars', 'BOK').
card_original_type('sway of the stars'/'BOK', 'Sorcery').
card_original_text('sway of the stars'/'BOK', 'Each player shuffles his or her hand, graveyard, and permanents he or she owns into his or her library, then draws seven cards. Each player\'s life total becomes 7.').
card_first_print('sway of the stars', 'BOK').
card_image_name('sway of the stars'/'BOK', 'sway of the stars').
card_uid('sway of the stars'/'BOK', 'BOK:Sway of the Stars:sway of the stars').
card_rarity('sway of the stars'/'BOK', 'Rare').
card_artist('sway of the stars'/'BOK', 'Randy Gallegos').
card_number('sway of the stars'/'BOK', '54').
card_multiverse_id('sway of the stars'/'BOK', '74034').

card_in_set('takeno\'s cavalry', 'BOK').
card_original_type('takeno\'s cavalry'/'BOK', 'Creature — Human Samurai Archer').
card_original_text('takeno\'s cavalry'/'BOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{T}: Takeno\'s Cavalry deals 1 damage to target attacking or blocking Spirit.').
card_first_print('takeno\'s cavalry', 'BOK').
card_image_name('takeno\'s cavalry'/'BOK', 'takeno\'s cavalry').
card_uid('takeno\'s cavalry'/'BOK', 'BOK:Takeno\'s Cavalry:takeno\'s cavalry').
card_rarity('takeno\'s cavalry'/'BOK', 'Common').
card_artist('takeno\'s cavalry'/'BOK', 'Darrell Riche').
card_number('takeno\'s cavalry'/'BOK', '24').
card_flavor_text('takeno\'s cavalry'/'BOK', 'At night he perfected his aim, shooting spiderwebs by starlight.').
card_multiverse_id('takeno\'s cavalry'/'BOK', '74597').

card_in_set('takenuma bleeder', 'BOK').
card_original_type('takenuma bleeder'/'BOK', 'Creature — Ogre Shaman').
card_original_text('takenuma bleeder'/'BOK', 'Whenever Takenuma Bleeder attacks or blocks, you lose 1 life if you don\'t control a Demon.').
card_first_print('takenuma bleeder', 'BOK').
card_image_name('takenuma bleeder'/'BOK', 'takenuma bleeder').
card_uid('takenuma bleeder'/'BOK', 'BOK:Takenuma Bleeder:takenuma bleeder').
card_rarity('takenuma bleeder'/'BOK', 'Common').
card_artist('takenuma bleeder'/'BOK', 'Kev Walker').
card_number('takenuma bleeder'/'BOK', '86').
card_flavor_text('takenuma bleeder'/'BOK', '\"I prefer to weave my magic through oni blood, but yours will do in a pinch.\"').
card_multiverse_id('takenuma bleeder'/'BOK', '74095').

card_in_set('tallowisp', 'BOK').
card_original_type('tallowisp'/'BOK', 'Creature — Spirit').
card_original_text('tallowisp'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may search your library for an enchant creature card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('tallowisp', 'BOK').
card_image_name('tallowisp'/'BOK', 'tallowisp').
card_uid('tallowisp'/'BOK', 'BOK:Tallowisp:tallowisp').
card_rarity('tallowisp'/'BOK', 'Uncommon').
card_artist('tallowisp'/'BOK', 'Ron Spears').
card_number('tallowisp'/'BOK', '25').
card_flavor_text('tallowisp'/'BOK', 'Isolated on its little candlewick, the kami never heard of the crime that drove its comrades to war.').
card_multiverse_id('tallowisp'/'BOK', '74412').

card_in_set('teardrop kami', 'BOK').
card_original_type('teardrop kami'/'BOK', 'Creature — Spirit').
card_original_text('teardrop kami'/'BOK', 'Sacrifice Teardrop Kami: Tap or untap target creature.').
card_first_print('teardrop kami', 'BOK').
card_image_name('teardrop kami'/'BOK', 'teardrop kami').
card_uid('teardrop kami'/'BOK', 'BOK:Teardrop Kami:teardrop kami').
card_rarity('teardrop kami'/'BOK', 'Common').
card_artist('teardrop kami'/'BOK', 'Michael Sutfin').
card_number('teardrop kami'/'BOK', '55').
card_flavor_text('teardrop kami'/'BOK', '\"Do not fall into the trap of thinking you understand the kami. Cannot a drop of water be dew on the meadow, a glacier\'s thaw, or the tear of a child?\"\n—Sensei Hisoka').
card_multiverse_id('teardrop kami'/'BOK', '74458').

card_in_set('tendo ice bridge', 'BOK').
card_original_type('tendo ice bridge'/'BOK', 'Land').
card_original_text('tendo ice bridge'/'BOK', 'Tendo Ice Bridge comes into play with a charge counter on it.\n{T}: Add {1} to your mana pool.\n{T}, Remove a charge counter from Tendo Ice Bridge: Add one mana of any color to your mana pool.').
card_first_print('tendo ice bridge', 'BOK').
card_image_name('tendo ice bridge'/'BOK', 'tendo ice bridge').
card_uid('tendo ice bridge'/'BOK', 'BOK:Tendo Ice Bridge:tendo ice bridge').
card_rarity('tendo ice bridge'/'BOK', 'Rare').
card_artist('tendo ice bridge'/'BOK', 'Rob Alexander').
card_number('tendo ice bridge'/'BOK', '165').
card_multiverse_id('tendo ice bridge'/'BOK', '74487').

card_in_set('terashi\'s grasp', 'BOK').
card_original_type('terashi\'s grasp'/'BOK', 'Sorcery — Arcane').
card_original_text('terashi\'s grasp'/'BOK', 'Destroy target artifact or enchantment. You gain life equal to its converted mana cost.').
card_first_print('terashi\'s grasp', 'BOK').
card_image_name('terashi\'s grasp'/'BOK', 'terashi\'s grasp').
card_uid('terashi\'s grasp'/'BOK', 'BOK:Terashi\'s Grasp:terashi\'s grasp').
card_rarity('terashi\'s grasp'/'BOK', 'Common').
card_artist('terashi\'s grasp'/'BOK', 'Mark Tedin').
card_number('terashi\'s grasp'/'BOK', '26').
card_flavor_text('terashi\'s grasp'/'BOK', '\"The jeweler, the potter, the smith . . . They all imbue a bit of their souls into their creations. The kami destroy that crafted mortal shell and absorb the soul within.\"\n—Noboru, master kitemaker').
card_multiverse_id('terashi\'s grasp'/'BOK', '74585').

card_in_set('terashi\'s verdict', 'BOK').
card_original_type('terashi\'s verdict'/'BOK', 'Instant — Arcane').
card_original_text('terashi\'s verdict'/'BOK', 'Destroy target attacking creature with power 3 or less.').
card_first_print('terashi\'s verdict', 'BOK').
card_image_name('terashi\'s verdict'/'BOK', 'terashi\'s verdict').
card_uid('terashi\'s verdict'/'BOK', 'BOK:Terashi\'s Verdict:terashi\'s verdict').
card_rarity('terashi\'s verdict'/'BOK', 'Uncommon').
card_artist('terashi\'s verdict'/'BOK', 'Ron Spears').
card_number('terashi\'s verdict'/'BOK', '27').
card_flavor_text('terashi\'s verdict'/'BOK', 'It was as though the sky opened its eyes and found what it saw displeasing.').
card_multiverse_id('terashi\'s verdict'/'BOK', '74481').

card_in_set('that which was taken', 'BOK').
card_original_type('that which was taken'/'BOK', 'Legendary Artifact').
card_original_text('that which was taken'/'BOK', '{4}, {T}: Put a divinity counter on target permanent other than That Which Was Taken.\nEach permanent with a divinity counter on it is indestructible.').
card_first_print('that which was taken', 'BOK').
card_image_name('that which was taken'/'BOK', 'that which was taken').
card_uid('that which was taken'/'BOK', 'BOK:That Which Was Taken:that which was taken').
card_rarity('that which was taken'/'BOK', 'Rare').
card_artist('that which was taken'/'BOK', 'Adam Rex').
card_number('that which was taken'/'BOK', '162').
card_flavor_text('that which was taken'/'BOK', '\"This god held prisoner in human hands has put all humanity in the hands of angry gods.\"\n—Masako the Humorless').
card_multiverse_id('that which was taken'/'BOK', '74644').

card_in_set('threads of disloyalty', 'BOK').
card_original_type('threads of disloyalty'/'BOK', 'Enchant Creature').
card_original_text('threads of disloyalty'/'BOK', 'Threads of Disloyalty can enchant only a creature with converted mana cost 2 or less.\nYou control enchanted creature.').
card_first_print('threads of disloyalty', 'BOK').
card_image_name('threads of disloyalty'/'BOK', 'threads of disloyalty').
card_uid('threads of disloyalty'/'BOK', 'BOK:Threads of Disloyalty:threads of disloyalty').
card_rarity('threads of disloyalty'/'BOK', 'Rare').
card_artist('threads of disloyalty'/'BOK', 'Anthony S. Waters').
card_number('threads of disloyalty'/'BOK', '56').
card_flavor_text('threads of disloyalty'/'BOK', '\"Over time, Konda grew ever more suspicious, fearing even his most loyal allies were being manipulated by unseen hands.\"\n—The History of Kamigawa').
card_multiverse_id('threads of disloyalty'/'BOK', '74652').

card_in_set('three tragedies', 'BOK').
card_original_type('three tragedies'/'BOK', 'Sorcery — Arcane').
card_original_text('three tragedies'/'BOK', 'Target player discards three cards.').
card_first_print('three tragedies', 'BOK').
card_image_name('three tragedies'/'BOK', 'three tragedies').
card_uid('three tragedies'/'BOK', 'BOK:Three Tragedies:three tragedies').
card_rarity('three tragedies'/'BOK', 'Uncommon').
card_artist('three tragedies'/'BOK', 'Darrell Riche').
card_number('three tragedies'/'BOK', '87').
card_flavor_text('three tragedies'/'BOK', '\"As the kami passed over the village of Mita, the inhabitants relived their three most grievous tragedies. Some cried. Some raged. Some were driven to madness. But the next morning, none possessed the will to fight.\"\n—Observations of the Kami War').
card_multiverse_id('three tragedies'/'BOK', '74117').

card_in_set('throat slitter', 'BOK').
card_original_type('throat slitter'/'BOK', 'Creature — Rat Ninja').
card_original_text('throat slitter'/'BOK', 'Ninjutsu {2}{B} ({2}{B}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Throat Slitter deals combat damage to a player, destroy target nonblack creature that player controls.').
card_first_print('throat slitter', 'BOK').
card_image_name('throat slitter'/'BOK', 'throat slitter').
card_uid('throat slitter'/'BOK', 'BOK:Throat Slitter:throat slitter').
card_rarity('throat slitter'/'BOK', 'Uncommon').
card_artist('throat slitter'/'BOK', 'Paolo Parente').
card_number('throat slitter'/'BOK', '88').
card_multiverse_id('throat slitter'/'BOK', '74108').

card_in_set('toils of night and day', 'BOK').
card_original_type('toils of night and day'/'BOK', 'Instant — Arcane').
card_original_text('toils of night and day'/'BOK', 'Tap or untap target permanent, then tap or untap another target permanent.').
card_first_print('toils of night and day', 'BOK').
card_image_name('toils of night and day'/'BOK', 'toils of night and day').
card_uid('toils of night and day'/'BOK', 'BOK:Toils of Night and Day:toils of night and day').
card_rarity('toils of night and day'/'BOK', 'Common').
card_artist('toils of night and day'/'BOK', 'Matt Cavotta').
card_number('toils of night and day'/'BOK', '57').
card_flavor_text('toils of night and day'/'BOK', '\"The war sent Kamigawa into turmoil. Here it was spring and there winter. For some, time stood still, while for others, moments flashed past like minnows in a pond.\"\n—Observations of the Kami War').
card_multiverse_id('toils of night and day'/'BOK', '74113').

card_in_set('tomorrow, azami\'s familiar', 'BOK').
card_original_type('tomorrow, azami\'s familiar'/'BOK', 'Legendary Creature — Spirit').
card_original_text('tomorrow, azami\'s familiar'/'BOK', 'If you would draw a card, look at the top three cards of your library instead. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_first_print('tomorrow, azami\'s familiar', 'BOK').
card_image_name('tomorrow, azami\'s familiar'/'BOK', 'tomorrow, azami\'s familiar').
card_uid('tomorrow, azami\'s familiar'/'BOK', 'BOK:Tomorrow, Azami\'s Familiar:tomorrow, azami\'s familiar').
card_rarity('tomorrow, azami\'s familiar'/'BOK', 'Rare').
card_artist('tomorrow, azami\'s familiar'/'BOK', 'Christopher Rush').
card_number('tomorrow, azami\'s familiar'/'BOK', '58').
card_multiverse_id('tomorrow, azami\'s familiar'/'BOK', '74447').

card_in_set('torrent of stone', 'BOK').
card_original_type('torrent of stone'/'BOK', 'Instant — Arcane').
card_original_text('torrent of stone'/'BOK', 'Torrent of Stone deals 4 damage to target creature.\nSplice onto Arcane—Sacrifice two mountains. (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('torrent of stone', 'BOK').
card_image_name('torrent of stone'/'BOK', 'torrent of stone').
card_uid('torrent of stone'/'BOK', 'BOK:Torrent of Stone:torrent of stone').
card_rarity('torrent of stone'/'BOK', 'Common').
card_artist('torrent of stone'/'BOK', 'Greg Staples').
card_number('torrent of stone'/'BOK', '119').
card_multiverse_id('torrent of stone'/'BOK', '81990').

card_in_set('toshiro umezawa', 'BOK').
card_original_type('toshiro umezawa'/'BOK', 'Legendary Creature — Human Samurai').
card_original_text('toshiro umezawa'/'BOK', 'Bushido 1 (When this blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever a creature an opponent controls is put into a graveyard from play, you may play target instant card in your graveyard. If that card would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('toshiro umezawa', 'BOK').
card_image_name('toshiro umezawa'/'BOK', 'toshiro umezawa').
card_uid('toshiro umezawa'/'BOK', 'BOK:Toshiro Umezawa:toshiro umezawa').
card_rarity('toshiro umezawa'/'BOK', 'Rare').
card_artist('toshiro umezawa'/'BOK', 'Christopher Moeller').
card_number('toshiro umezawa'/'BOK', '89').
card_multiverse_id('toshiro umezawa'/'BOK', '74431').

card_in_set('traproot kami', 'BOK').
card_original_type('traproot kami'/'BOK', 'Creature — Spirit').
card_original_text('traproot kami'/'BOK', 'Defender (This creature can\'t attack.)\nTraproot Kami\'s toughness is equal to the number of Forests in play.\nTraproot Kami may block as though it had flying.').
card_first_print('traproot kami', 'BOK').
card_image_name('traproot kami'/'BOK', 'traproot kami').
card_uid('traproot kami'/'BOK', 'BOK:Traproot Kami:traproot kami').
card_rarity('traproot kami'/'BOK', 'Common').
card_artist('traproot kami'/'BOK', 'Carl Critchlow').
card_number('traproot kami'/'BOK', '147').
card_multiverse_id('traproot kami'/'BOK', '74627').

card_in_set('twist allegiance', 'BOK').
card_original_type('twist allegiance'/'BOK', 'Sorcery').
card_original_text('twist allegiance'/'BOK', 'You and target opponent each gain control of all creatures the other controls until end of turn. Untap those creatures. Those creatures gain haste until end of turn.').
card_first_print('twist allegiance', 'BOK').
card_image_name('twist allegiance'/'BOK', 'twist allegiance').
card_uid('twist allegiance'/'BOK', 'BOK:Twist Allegiance:twist allegiance').
card_rarity('twist allegiance'/'BOK', 'Rare').
card_artist('twist allegiance'/'BOK', 'Wayne Reynolds').
card_number('twist allegiance'/'BOK', '120').
card_multiverse_id('twist allegiance'/'BOK', '74023').

card_in_set('umezawa\'s jitte', 'BOK').
card_original_type('umezawa\'s jitte'/'BOK', 'Legendary Artifact — Equipment').
card_original_text('umezawa\'s jitte'/'BOK', 'Whenever equipped creature deals combat damage, put two charge counters on Umezawa\'s Jitte.\nRemove a charge counter from Umezawa\'s Jitte: Choose one Equipped creature gets +2/+2 until end of turn; or target creature gets -1/-1 until end of turn; or you gain 2 life.\nEquip {2}').
card_first_print('umezawa\'s jitte', 'BOK').
card_image_name('umezawa\'s jitte'/'BOK', 'umezawa\'s jitte').
card_uid('umezawa\'s jitte'/'BOK', 'BOK:Umezawa\'s Jitte:umezawa\'s jitte').
card_rarity('umezawa\'s jitte'/'BOK', 'Rare').
card_artist('umezawa\'s jitte'/'BOK', 'Christopher Moeller').
card_number('umezawa\'s jitte'/'BOK', '163').
card_multiverse_id('umezawa\'s jitte'/'BOK', '81979').

card_in_set('unchecked growth', 'BOK').
card_original_type('unchecked growth'/'BOK', 'Instant — Arcane').
card_original_text('unchecked growth'/'BOK', 'Target creature gets +4/+4 until end of turn. If it\'s a Spirit, it gains trample until end of turn.').
card_first_print('unchecked growth', 'BOK').
card_image_name('unchecked growth'/'BOK', 'unchecked growth').
card_uid('unchecked growth'/'BOK', 'BOK:Unchecked Growth:unchecked growth').
card_rarity('unchecked growth'/'BOK', 'Uncommon').
card_artist('unchecked growth'/'BOK', 'Tsutomu Kawade').
card_number('unchecked growth'/'BOK', '148').
card_flavor_text('unchecked growth'/'BOK', '\"That energy was once granted to growing things. Now the kami keep it for themselves, releasing it in sudden floods that overwhelm flesh and foliage alike.\"\n—Sakiko, Mother of Summer').
card_multiverse_id('unchecked growth'/'BOK', '74670').

card_in_set('uproot', 'BOK').
card_original_type('uproot'/'BOK', 'Sorcery — Arcane').
card_original_text('uproot'/'BOK', 'Put target land on top of its owner\'s library.').
card_first_print('uproot', 'BOK').
card_image_name('uproot'/'BOK', 'uproot').
card_uid('uproot'/'BOK', 'BOK:Uproot:uproot').
card_rarity('uproot'/'BOK', 'Common').
card_artist('uproot'/'BOK', 'Heather Hudson').
card_number('uproot'/'BOK', '149').
card_flavor_text('uproot'/'BOK', '\"We arrived at the battlefield too late. Again. Another error on your part, and you will have to answer to me personally.\"\n—General Takeno, letter to the imperial mapmaker').
card_multiverse_id('uproot'/'BOK', '74568').

card_in_set('veil of secrecy', 'BOK').
card_original_type('veil of secrecy'/'BOK', 'Instant — Arcane').
card_original_text('veil of secrecy'/'BOK', 'Target creature is unblockable and can\'t be the target of spells or abilities this turn.\nSplice onto Arcane—Return a blue creature you control to its owner\'s hand. (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('veil of secrecy', 'BOK').
card_image_name('veil of secrecy'/'BOK', 'veil of secrecy').
card_uid('veil of secrecy'/'BOK', 'BOK:Veil of Secrecy:veil of secrecy').
card_rarity('veil of secrecy'/'BOK', 'Common').
card_artist('veil of secrecy'/'BOK', 'Arnie Swekel').
card_number('veil of secrecy'/'BOK', '59').
card_multiverse_id('veil of secrecy'/'BOK', '74515').

card_in_set('vital surge', 'BOK').
card_original_type('vital surge'/'BOK', 'Instant — Arcane').
card_original_text('vital surge'/'BOK', 'You gain 3 life.\nSplice onto Arcane {1}{G} (As you play an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_first_print('vital surge', 'BOK').
card_image_name('vital surge'/'BOK', 'vital surge').
card_uid('vital surge'/'BOK', 'BOK:Vital Surge:vital surge').
card_rarity('vital surge'/'BOK', 'Common').
card_artist('vital surge'/'BOK', 'Douglas Shuler').
card_number('vital surge'/'BOK', '150').
card_multiverse_id('vital surge'/'BOK', '74612').

card_in_set('walker of secret ways', 'BOK').
card_original_type('walker of secret ways'/'BOK', 'Creature — Human Ninja').
card_original_text('walker of secret ways'/'BOK', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card into play from your hand tapped and attacking.)\nWhenever Walker of Secret Ways deals combat damage to a player, look at that player\'s hand.\n{1}{U}: Return target Ninja you control to its owner\'s hand. Play this ability only during your turn.').
card_first_print('walker of secret ways', 'BOK').
card_image_name('walker of secret ways'/'BOK', 'walker of secret ways').
card_uid('walker of secret ways'/'BOK', 'BOK:Walker of Secret Ways:walker of secret ways').
card_rarity('walker of secret ways'/'BOK', 'Uncommon').
card_artist('walker of secret ways'/'BOK', 'Scott M. Fischer').
card_number('walker of secret ways'/'BOK', '60').
card_multiverse_id('walker of secret ways'/'BOK', '74033').

card_in_set('ward of piety', 'BOK').
card_original_type('ward of piety'/'BOK', 'Enchant Creature').
card_original_text('ward of piety'/'BOK', '{1}{W}: The next 1 damage that would be dealt to enchanted creature this turn is dealt to target creature or player instead.').
card_first_print('ward of piety', 'BOK').
card_image_name('ward of piety'/'BOK', 'ward of piety').
card_uid('ward of piety'/'BOK', 'BOK:Ward of Piety:ward of piety').
card_rarity('ward of piety'/'BOK', 'Uncommon').
card_artist('ward of piety'/'BOK', 'Tim Hildebrandt').
card_number('ward of piety'/'BOK', '28').
card_flavor_text('ward of piety'/'BOK', 'Reluctant to strike at the kami directly, some kitsune trained in more passive fighting techniques.').
card_multiverse_id('ward of piety'/'BOK', '74610').

card_in_set('waxmane baku', 'BOK').
card_original_type('waxmane baku'/'BOK', 'Creature — Spirit').
card_original_text('waxmane baku'/'BOK', 'Whenever you play a Spirit or Arcane spell, you may put a ki counter on Waxmane Baku.\n{1}, Remove X ki counters from Waxmane Baku: Tap X target creatures.').
card_first_print('waxmane baku', 'BOK').
card_image_name('waxmane baku'/'BOK', 'waxmane baku').
card_uid('waxmane baku'/'BOK', 'BOK:Waxmane Baku:waxmane baku').
card_rarity('waxmane baku'/'BOK', 'Common').
card_artist('waxmane baku'/'BOK', 'Greg Hildebrandt').
card_number('waxmane baku'/'BOK', '29').
card_multiverse_id('waxmane baku'/'BOK', '74518').

card_in_set('yomiji, who bars the way', 'BOK').
card_original_type('yomiji, who bars the way'/'BOK', 'Legendary Creature — Spirit').
card_original_text('yomiji, who bars the way'/'BOK', 'Whenever a legendary permanent other than Yomiji, Who Bars the Way is put into a graveyard from play, return that card to its owner\'s hand.').
card_first_print('yomiji, who bars the way', 'BOK').
card_image_name('yomiji, who bars the way'/'BOK', 'yomiji, who bars the way').
card_uid('yomiji, who bars the way'/'BOK', 'BOK:Yomiji, Who Bars the Way:yomiji, who bars the way').
card_rarity('yomiji, who bars the way'/'BOK', 'Rare').
card_artist('yomiji, who bars the way'/'BOK', 'Hideaki Takamura').
card_number('yomiji, who bars the way'/'BOK', '30').
card_flavor_text('yomiji, who bars the way'/'BOK', '\"As I died, I rejoiced. I would see my family again. But then I woke up back on the battlefield. Back in Kamigawa. Back in hell.\"\n—Kenzo the Hardhearted').
card_multiverse_id('yomiji, who bars the way'/'BOK', '74526').

card_in_set('yukora, the prisoner', 'BOK').
card_original_type('yukora, the prisoner'/'BOK', 'Legendary Creature — Demon Spirit').
card_original_text('yukora, the prisoner'/'BOK', 'When Yukora, the Prisoner leaves play, sacrifice all non-Ogre creatures you control.').
card_first_print('yukora, the prisoner', 'BOK').
card_image_name('yukora, the prisoner'/'BOK', 'yukora, the prisoner').
card_uid('yukora, the prisoner'/'BOK', 'BOK:Yukora, the Prisoner:yukora, the prisoner').
card_rarity('yukora, the prisoner'/'BOK', 'Rare').
card_artist('yukora, the prisoner'/'BOK', 'Tony Szczudlo').
card_number('yukora, the prisoner'/'BOK', '90').
card_flavor_text('yukora, the prisoner'/'BOK', 'It took ninety-nine monks to weave the spell that trapped Yukora. Upon the death of the ninety-nine, the spell was broken, and the demon returned to the mortal world seeking vengeance for its imprisonment.').
card_multiverse_id('yukora, the prisoner'/'BOK', '74510').
