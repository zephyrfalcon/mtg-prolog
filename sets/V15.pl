% From the Vault: Angels

set('V15').
set_name('V15', 'From the Vault: Angels').
set_release_date('V15', '2015-08-21').
set_border('V15', 'black').
set_type('V15', 'from the vault').

card_in_set('akroma, angel of fury', 'V15').
card_original_type('akroma, angel of fury'/'V15', 'Legendary Creature — Angel').
card_original_text('akroma, angel of fury'/'V15', 'Akroma, Angel of Fury can\'t be countered.\nFlying, trample, protection from white and from blue\n{R}: Akroma, Angel of Fury gets +1/+0 until end of turn.\nMorph {3}{R}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('akroma, angel of fury'/'V15', 'akroma, angel of fury').
card_uid('akroma, angel of fury'/'V15', 'V15:Akroma, Angel of Fury:akroma, angel of fury').
card_rarity('akroma, angel of fury'/'V15', 'Mythic Rare').
card_artist('akroma, angel of fury'/'V15', 'Terese Nielsen').
card_number('akroma, angel of fury'/'V15', '1').
card_multiverse_id('akroma, angel of fury'/'V15', '401628').

card_in_set('akroma, angel of wrath', 'V15').
card_original_type('akroma, angel of wrath'/'V15', 'Legendary Creature — Angel').
card_original_text('akroma, angel of wrath'/'V15', 'Flying, first strike, vigilance, trample, haste, protection from black and from red').
card_image_name('akroma, angel of wrath'/'V15', 'akroma, angel of wrath').
card_uid('akroma, angel of wrath'/'V15', 'V15:Akroma, Angel of Wrath:akroma, angel of wrath').
card_rarity('akroma, angel of wrath'/'V15', 'Mythic Rare').
card_artist('akroma, angel of wrath'/'V15', 'Terese Nielsen').
card_number('akroma, angel of wrath'/'V15', '2').
card_flavor_text('akroma, angel of wrath'/'V15', '\"Wrath is no vice when inflicted upon the deserving.\"').
card_multiverse_id('akroma, angel of wrath'/'V15', '401629').

card_in_set('archangel of strife', 'V15').
card_original_type('archangel of strife'/'V15', 'Creature — Angel').
card_original_text('archangel of strife'/'V15', 'Flying\nAs Archangel of Strife enters the battlefield, each player chooses war or peace.\nCreatures controlled by players who chose war get +3/+0.\nCreatures controlled by players who chose peace get +0/+3.').
card_image_name('archangel of strife'/'V15', 'archangel of strife').
card_uid('archangel of strife'/'V15', 'V15:Archangel of Strife:archangel of strife').
card_rarity('archangel of strife'/'V15', 'Mythic Rare').
card_artist('archangel of strife'/'V15', 'Greg Staples').
card_number('archangel of strife'/'V15', '3').
card_multiverse_id('archangel of strife'/'V15', '401630').

card_in_set('aurelia, the warleader', 'V15').
card_original_type('aurelia, the warleader'/'V15', 'Legendary Creature — Angel').
card_original_text('aurelia, the warleader'/'V15', 'Flying, vigilance, haste\nWhenever Aurelia, the Warleader attacks for the first time each turn, untap all creatures you control. After this phase, there is an additional combat phase.').
card_image_name('aurelia, the warleader'/'V15', 'aurelia, the warleader').
card_uid('aurelia, the warleader'/'V15', 'V15:Aurelia, the Warleader:aurelia, the warleader').
card_rarity('aurelia, the warleader'/'V15', 'Mythic Rare').
card_artist('aurelia, the warleader'/'V15', 'Slawomir Maniak').
card_number('aurelia, the warleader'/'V15', '4').
card_flavor_text('aurelia, the warleader'/'V15', 'Where Razia was aloof and untouchable, Aurelia is on the frontlines, calling for war.').
card_multiverse_id('aurelia, the warleader'/'V15', '401631').

card_in_set('avacyn, angel of hope', 'V15').
card_original_type('avacyn, angel of hope'/'V15', 'Legendary Creature — Angel').
card_original_text('avacyn, angel of hope'/'V15', 'Flying, vigilance, indestructible\nOther permanents you control have indestructible.').
card_image_name('avacyn, angel of hope'/'V15', 'avacyn, angel of hope').
card_uid('avacyn, angel of hope'/'V15', 'V15:Avacyn, Angel of Hope:avacyn, angel of hope').
card_rarity('avacyn, angel of hope'/'V15', 'Mythic Rare').
card_artist('avacyn, angel of hope'/'V15', 'Jason Chan').
card_number('avacyn, angel of hope'/'V15', '5').
card_flavor_text('avacyn, angel of hope'/'V15', 'A golden helix streaked skyward from the Helvault. A thunderous explosion shattered the silver monolith and Avacyn emerged, free from her prison at last.').
card_multiverse_id('avacyn, angel of hope'/'V15', '401632').

card_in_set('baneslayer angel', 'V15').
card_original_type('baneslayer angel'/'V15', 'Creature — Angel').
card_original_text('baneslayer angel'/'V15', 'Flying, first strike, lifelink, protection from Demons and from Dragons').
card_image_name('baneslayer angel'/'V15', 'baneslayer angel').
card_uid('baneslayer angel'/'V15', 'V15:Baneslayer Angel:baneslayer angel').
card_rarity('baneslayer angel'/'V15', 'Mythic Rare').
card_artist('baneslayer angel'/'V15', 'Greg Staples').
card_number('baneslayer angel'/'V15', '6').
card_flavor_text('baneslayer angel'/'V15', 'Some angels protect the meek and innocent. Others seek out and smite evil wherever it lurks.').
card_multiverse_id('baneslayer angel'/'V15', '401633').

card_in_set('entreat the angels', 'V15').
card_original_type('entreat the angels'/'V15', 'Sorcery').
card_original_text('entreat the angels'/'V15', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nMiracle {X}{W}{W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_image_name('entreat the angels'/'V15', 'entreat the angels').
card_uid('entreat the angels'/'V15', 'V15:Entreat the Angels:entreat the angels').
card_rarity('entreat the angels'/'V15', 'Mythic Rare').
card_artist('entreat the angels'/'V15', 'Todd Lockwood').
card_number('entreat the angels'/'V15', '7').
card_multiverse_id('entreat the angels'/'V15', '401634').

card_in_set('exalted angel', 'V15').
card_original_type('exalted angel'/'V15', 'Creature — Angel').
card_original_text('exalted angel'/'V15', 'Flying\nWhenever Exalted Angel deals damage, you gain that much life.\nMorph {2}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('exalted angel'/'V15', 'exalted angel').
card_uid('exalted angel'/'V15', 'V15:Exalted Angel:exalted angel').
card_rarity('exalted angel'/'V15', 'Mythic Rare').
card_artist('exalted angel'/'V15', 'Tyler Jacobson').
card_number('exalted angel'/'V15', '8').
card_multiverse_id('exalted angel'/'V15', '401635').

card_in_set('iona, shield of emeria', 'V15').
card_original_type('iona, shield of emeria'/'V15', 'Legendary Creature — Angel').
card_original_text('iona, shield of emeria'/'V15', 'Flying\nAs Iona, Shield of Emeria enters the battlefield, choose a color.\nYour opponents can\'t cast spells of the chosen color.').
card_image_name('iona, shield of emeria'/'V15', 'iona, shield of emeria').
card_uid('iona, shield of emeria'/'V15', 'V15:Iona, Shield of Emeria:iona, shield of emeria').
card_rarity('iona, shield of emeria'/'V15', 'Mythic Rare').
card_artist('iona, shield of emeria'/'V15', 'Jason Chan').
card_number('iona, shield of emeria'/'V15', '9').
card_flavor_text('iona, shield of emeria'/'V15', 'No more shall the righteous cower before evil.').
card_multiverse_id('iona, shield of emeria'/'V15', '401636').

card_in_set('iridescent angel', 'V15').
card_original_type('iridescent angel'/'V15', 'Creature — Angel').
card_original_text('iridescent angel'/'V15', 'Flying, protection from all colors').
card_image_name('iridescent angel'/'V15', 'iridescent angel').
card_uid('iridescent angel'/'V15', 'V15:Iridescent Angel:iridescent angel').
card_rarity('iridescent angel'/'V15', 'Mythic Rare').
card_artist('iridescent angel'/'V15', 'Ryan Alexander Lee').
card_number('iridescent angel'/'V15', '10').
card_flavor_text('iridescent angel'/'V15', 'She enraptures all, encompasses all, endures all.').
card_multiverse_id('iridescent angel'/'V15', '401637').

card_in_set('jenara, asura of war', 'V15').
card_original_type('jenara, asura of war'/'V15', 'Legendary Creature — Angel').
card_original_text('jenara, asura of war'/'V15', 'Flying\n{1}{W}: Put a +1/+1 counter on Jenara, Asura of War.').
card_image_name('jenara, asura of war'/'V15', 'jenara, asura of war').
card_uid('jenara, asura of war'/'V15', 'V15:Jenara, Asura of War:jenara, asura of war').
card_rarity('jenara, asura of war'/'V15', 'Mythic Rare').
card_artist('jenara, asura of war'/'V15', 'Chris Rahn').
card_number('jenara, asura of war'/'V15', '11').
card_flavor_text('jenara, asura of war'/'V15', 'Wounded soldiers looked up, grateful for her appearance. But she passed over them, her eyes firmly on their foe.').
card_multiverse_id('jenara, asura of war'/'V15', '401638').

card_in_set('lightning angel', 'V15').
card_original_type('lightning angel'/'V15', 'Creature — Angel').
card_original_text('lightning angel'/'V15', 'Flying, vigilance, haste').
card_image_name('lightning angel'/'V15', 'lightning angel').
card_uid('lightning angel'/'V15', 'V15:Lightning Angel:lightning angel').
card_rarity('lightning angel'/'V15', 'Mythic Rare').
card_artist('lightning angel'/'V15', 'rk post').
card_number('lightning angel'/'V15', '12').
card_flavor_text('lightning angel'/'V15', 'Sudden vengeance, echoing fury.').
card_multiverse_id('lightning angel'/'V15', '401639').

card_in_set('platinum angel', 'V15').
card_original_type('platinum angel'/'V15', 'Artifact Creature — Angel').
card_original_text('platinum angel'/'V15', 'Flying\nYou can\'t lose the game and your opponents can\'t win the game.').
card_image_name('platinum angel'/'V15', 'platinum angel').
card_uid('platinum angel'/'V15', 'V15:Platinum Angel:platinum angel').
card_rarity('platinum angel'/'V15', 'Mythic Rare').
card_artist('platinum angel'/'V15', 'Brom').
card_number('platinum angel'/'V15', '13').
card_flavor_text('platinum angel'/'V15', 'She is the apex of the artificer\'s craft, the spirit of the divine called out of base metal.').
card_multiverse_id('platinum angel'/'V15', '401640').

card_in_set('serra angel', 'V15').
card_original_type('serra angel'/'V15', 'Creature — Angel').
card_original_text('serra angel'/'V15', 'Flying, vigilance').
card_image_name('serra angel'/'V15', 'serra angel').
card_uid('serra angel'/'V15', 'V15:Serra Angel:serra angel').
card_rarity('serra angel'/'V15', 'Mythic Rare').
card_artist('serra angel'/'V15', 'Rebecca Guay').
card_number('serra angel'/'V15', '14').
card_flavor_text('serra angel'/'V15', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'V15', '401641').

card_in_set('tariel, reckoner of souls', 'V15').
card_original_type('tariel, reckoner of souls'/'V15', 'Legendary Creature — Angel').
card_original_text('tariel, reckoner of souls'/'V15', 'Flying, vigilance\n{T}: Choose a creature card at random from target opponent\'s graveyard. Put that card onto the battlefield under your control.').
card_image_name('tariel, reckoner of souls'/'V15', 'tariel, reckoner of souls').
card_uid('tariel, reckoner of souls'/'V15', 'V15:Tariel, Reckoner of Souls:tariel, reckoner of souls').
card_rarity('tariel, reckoner of souls'/'V15', 'Mythic Rare').
card_artist('tariel, reckoner of souls'/'V15', 'Wayne Reynolds').
card_number('tariel, reckoner of souls'/'V15', '15').
card_flavor_text('tariel, reckoner of souls'/'V15', '\"After death you face paradise, damnation, or Tariel.\"\n—Priest\'s teaching').
card_multiverse_id('tariel, reckoner of souls'/'V15', '401642').
