% Premium Deck Series: Fire and Lightning

set('PD2').
set_name('PD2', 'Premium Deck Series: Fire and Lightning').
set_release_date('PD2', '2010-11-19').
set_border('PD2', 'black').
set_type('PD2', 'premium deck').

card_in_set('ball lightning', 'PD2').
card_original_type('ball lightning'/'PD2', 'Creature — Elemental').
card_original_text('ball lightning'/'PD2', 'Trample, haste\nAt the beginning of the end step, sacrifice Ball Lightning.').
card_image_name('ball lightning'/'PD2', 'ball lightning').
card_uid('ball lightning'/'PD2', 'PD2:Ball Lightning:ball lightning').
card_rarity('ball lightning'/'PD2', 'Rare').
card_artist('ball lightning'/'PD2', 'Trevor Claxton').
card_number('ball lightning'/'PD2', '12').
card_multiverse_id('ball lightning'/'PD2', '234722').

card_in_set('barbarian ring', 'PD2').
card_original_type('barbarian ring'/'PD2', 'Land').
card_original_text('barbarian ring'/'PD2', '{T}: Add {R} to your mana pool. Barbarian Ring deals 1 damage to you.\nThreshold — {R}, {T}, Sacrifice Barbarian Ring: Barbarian Ring deals 2 damage to target creature or player. Activate this ability only if seven or more cards are in your graveyard.').
card_image_name('barbarian ring'/'PD2', 'barbarian ring').
card_uid('barbarian ring'/'PD2', 'PD2:Barbarian Ring:barbarian ring').
card_rarity('barbarian ring'/'PD2', 'Uncommon').
card_artist('barbarian ring'/'PD2', 'John Avon').
card_number('barbarian ring'/'PD2', '28').
card_multiverse_id('barbarian ring'/'PD2', '234737').

card_in_set('boggart ram-gang', 'PD2').
card_original_type('boggart ram-gang'/'PD2', 'Creature — Goblin Warrior').
card_original_text('boggart ram-gang'/'PD2', 'Haste\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_image_name('boggart ram-gang'/'PD2', 'boggart ram-gang').
card_uid('boggart ram-gang'/'PD2', 'PD2:Boggart Ram-Gang:boggart ram-gang').
card_rarity('boggart ram-gang'/'PD2', 'Uncommon').
card_artist('boggart ram-gang'/'PD2', 'Dave Allsop').
card_number('boggart ram-gang'/'PD2', '13').
card_flavor_text('boggart ram-gang'/'PD2', '\"We\'re going to need a bigger gate.\"\n—Bowen, Barrenton guardcaptain').
card_multiverse_id('boggart ram-gang'/'PD2', '234723').

card_in_set('browbeat', 'PD2').
card_original_type('browbeat'/'PD2', 'Sorcery').
card_original_text('browbeat'/'PD2', 'Any player may have Browbeat deal 5 damage to him or her. If no one does, target player draws three cards.').
card_image_name('browbeat'/'PD2', 'browbeat').
card_uid('browbeat'/'PD2', 'PD2:Browbeat:browbeat').
card_rarity('browbeat'/'PD2', 'Uncommon').
card_artist('browbeat'/'PD2', 'Mark Tedin').
card_number('browbeat'/'PD2', '21').
card_flavor_text('browbeat'/'PD2', '\"Even the threat of power has power.\"\n—Jeska, warrior adept').
card_multiverse_id('browbeat'/'PD2', '234712').

card_in_set('chain lightning', 'PD2').
card_original_type('chain lightning'/'PD2', 'Sorcery').
card_original_text('chain lightning'/'PD2', 'Chain Lightning deals 3 damage to target creature or player. Then that player or that creature\'s controller may pay {R}{R}. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_image_name('chain lightning'/'PD2', 'chain lightning').
card_uid('chain lightning'/'PD2', 'PD2:Chain Lightning:chain lightning').
card_rarity('chain lightning'/'PD2', 'Common').
card_artist('chain lightning'/'PD2', 'Christopher Moeller').
card_number('chain lightning'/'PD2', '16').
card_multiverse_id('chain lightning'/'PD2', '217977').

card_in_set('cinder pyromancer', 'PD2').
card_original_type('cinder pyromancer'/'PD2', 'Creature — Elemental Shaman').
card_original_text('cinder pyromancer'/'PD2', '{T}: Cinder Pyromancer deals 1 damage to target player.\nWhenever you cast a red spell, you may untap Cinder Pyromancer.').
card_image_name('cinder pyromancer'/'PD2', 'cinder pyromancer').
card_uid('cinder pyromancer'/'PD2', 'PD2:Cinder Pyromancer:cinder pyromancer').
card_rarity('cinder pyromancer'/'PD2', 'Common').
card_artist('cinder pyromancer'/'PD2', 'Greg Staples').
card_number('cinder pyromancer'/'PD2', '9').
card_flavor_text('cinder pyromancer'/'PD2', '\"If the whole world burns, I\'ll never grow cold.\"').
card_multiverse_id('cinder pyromancer'/'PD2', '234717').

card_in_set('figure of destiny', 'PD2').
card_original_type('figure of destiny'/'PD2', 'Creature — Kithkin').
card_original_text('figure of destiny'/'PD2', '{R/W}: Figure of Destiny becomes a 2/2 Kithkin Spirit.\n{R/W}{R/W}{R/W}: If Figure of Destiny is a Spirit, it becomes a 4/4 Kithkin Spirit Warrior.\n{R/W}{R/W}{R/W}{R/W}{R/W}{R/W}: If Figure of Destiny is a Warrior, it becomes an 8/8 Kithkin Spirit Warrior Avatar with flying and first strike.').
card_image_name('figure of destiny'/'PD2', 'figure of destiny').
card_uid('figure of destiny'/'PD2', 'PD2:Figure of Destiny:figure of destiny').
card_rarity('figure of destiny'/'PD2', 'Rare').
card_artist('figure of destiny'/'PD2', 'Scott M. Fischer').
card_number('figure of destiny'/'PD2', '5').
card_multiverse_id('figure of destiny'/'PD2', '236456').

card_in_set('fire servant', 'PD2').
card_original_type('fire servant'/'PD2', 'Creature — Elemental').
card_original_text('fire servant'/'PD2', 'If a red instant or sorcery spell you control would deal damage, it deals double that damage instead.').
card_image_name('fire servant'/'PD2', 'fire servant').
card_uid('fire servant'/'PD2', 'PD2:Fire Servant:fire servant').
card_rarity('fire servant'/'PD2', 'Uncommon').
card_artist('fire servant'/'PD2', 'Ryan Yee').
card_number('fire servant'/'PD2', '15').
card_flavor_text('fire servant'/'PD2', '\"Elemental fire shimmers with rose hues and, unlike terrestrial blazes, smolders long after it\'s extinguished.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('fire servant'/'PD2', '234702').

card_in_set('fireball', 'PD2').
card_original_type('fireball'/'PD2', 'Sorcery').
card_original_text('fireball'/'PD2', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'PD2', 'fireball').
card_uid('fireball'/'PD2', 'PD2:Fireball:fireball').
card_rarity('fireball'/'PD2', 'Uncommon').
card_artist('fireball'/'PD2', 'Dave Dorman').
card_number('fireball'/'PD2', '27').
card_multiverse_id('fireball'/'PD2', '234703').

card_in_set('fireblast', 'PD2').
card_original_type('fireblast'/'PD2', 'Instant').
card_original_text('fireblast'/'PD2', 'You may sacrifice two Mountains rather than pay Fireblast\'s mana cost.\nFireblast deals 4 damage to target creature or player.').
card_image_name('fireblast'/'PD2', 'fireblast').
card_uid('fireblast'/'PD2', 'PD2:Fireblast:fireblast').
card_rarity('fireblast'/'PD2', 'Common').
card_artist('fireblast'/'PD2', 'Michael Danza').
card_number('fireblast'/'PD2', '26').
card_flavor_text('fireblast'/'PD2', 'Embermages aren\'t well known for their diplomatic skills.').
card_multiverse_id('fireblast'/'PD2', '234736').

card_in_set('flames of the blood hand', 'PD2').
card_original_type('flames of the blood hand'/'PD2', 'Instant').
card_original_text('flames of the blood hand'/'PD2', 'Flames of the Blood Hand deals 4 damage to target player. The damage can\'t be prevented. If that player would gain life this turn, that player gains no life instead.').
card_image_name('flames of the blood hand'/'PD2', 'flames of the blood hand').
card_uid('flames of the blood hand'/'PD2', 'PD2:Flames of the Blood Hand:flames of the blood hand').
card_rarity('flames of the blood hand'/'PD2', 'Uncommon').
card_artist('flames of the blood hand'/'PD2', 'Aleksi Briclot').
card_number('flames of the blood hand'/'PD2', '22').
card_flavor_text('flames of the blood hand'/'PD2', 'Many ogres extracted blood oaths from the oni they summoned. Others simply extracted blood.').
card_multiverse_id('flames of the blood hand'/'PD2', '234718').

card_in_set('ghitu encampment', 'PD2').
card_original_type('ghitu encampment'/'PD2', 'Land').
card_original_text('ghitu encampment'/'PD2', 'Ghitu Encampment enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{1}{R}: Ghitu Encampment becomes a 2/1 red Warrior creature with first strike until end of turn. It\'s still a land.').
card_image_name('ghitu encampment'/'PD2', 'ghitu encampment').
card_uid('ghitu encampment'/'PD2', 'PD2:Ghitu Encampment:ghitu encampment').
card_rarity('ghitu encampment'/'PD2', 'Uncommon').
card_artist('ghitu encampment'/'PD2', 'John Avon').
card_number('ghitu encampment'/'PD2', '29').
card_multiverse_id('ghitu encampment'/'PD2', '234698').

card_in_set('grim lavamancer', 'PD2').
card_original_type('grim lavamancer'/'PD2', 'Creature — Human Wizard').
card_original_text('grim lavamancer'/'PD2', '{R}, {T}, Exile two cards from your graveyard: Grim Lavamancer deals 2 damage to target creature or player.').
card_image_name('grim lavamancer'/'PD2', 'grim lavamancer').
card_uid('grim lavamancer'/'PD2', 'PD2:Grim Lavamancer:grim lavamancer').
card_rarity('grim lavamancer'/'PD2', 'Rare').
card_artist('grim lavamancer'/'PD2', 'Michael Sutfin').
card_number('grim lavamancer'/'PD2', '1').
card_flavor_text('grim lavamancer'/'PD2', '\"Fools dig for water, corpses, or gold. The earth\'s real treasure is far deeper.\"').
card_multiverse_id('grim lavamancer'/'PD2', '234706').

card_in_set('hammer of bogardan', 'PD2').
card_original_type('hammer of bogardan'/'PD2', 'Sorcery').
card_original_text('hammer of bogardan'/'PD2', 'Hammer of Bogardan deals 3 damage to target creature or player.\n{2}{R}{R}{R}: Return Hammer of Bogardan from your graveyard to your hand. Activate this ability only during your upkeep.').
card_image_name('hammer of bogardan'/'PD2', 'hammer of bogardan').
card_uid('hammer of bogardan'/'PD2', 'PD2:Hammer of Bogardan:hammer of bogardan').
card_rarity('hammer of bogardan'/'PD2', 'Rare').
card_artist('hammer of bogardan'/'PD2', 'Ron Spencer').
card_number('hammer of bogardan'/'PD2', '23').
card_multiverse_id('hammer of bogardan'/'PD2', '234721').

card_in_set('hellspark elemental', 'PD2').
card_original_type('hellspark elemental'/'PD2', 'Creature — Elemental').
card_original_text('hellspark elemental'/'PD2', 'Trample, haste\nAt the beginning of the end step, sacrifice Hellspark Elemental.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('hellspark elemental'/'PD2', 'hellspark elemental').
card_uid('hellspark elemental'/'PD2', 'PD2:Hellspark Elemental:hellspark elemental').
card_rarity('hellspark elemental'/'PD2', 'Uncommon').
card_artist('hellspark elemental'/'PD2', 'Justin Sweet').
card_number('hellspark elemental'/'PD2', '6').
card_multiverse_id('hellspark elemental'/'PD2', '234711').

card_in_set('jackal pup', 'PD2').
card_original_type('jackal pup'/'PD2', 'Creature — Hound').
card_original_text('jackal pup'/'PD2', 'Whenever Jackal Pup is dealt damage, it deals that much damage to you.').
card_image_name('jackal pup'/'PD2', 'jackal pup').
card_uid('jackal pup'/'PD2', 'PD2:Jackal Pup:jackal pup').
card_rarity('jackal pup'/'PD2', 'Uncommon').
card_artist('jackal pup'/'PD2', 'Kev Walker').
card_number('jackal pup'/'PD2', '2').
card_flavor_text('jackal pup'/'PD2', 'The first morning after acquiring her familiar, the wizard awoke with fleabites and mange.').
card_multiverse_id('jackal pup'/'PD2', '217974').

card_in_set('jaya ballard, task mage', 'PD2').
card_original_type('jaya ballard, task mage'/'PD2', 'Legendary Creature — Human Spellshaper').
card_original_text('jaya ballard, task mage'/'PD2', '{R}, {T}, Discard a card: Destroy target blue permanent.\n{1}{R}, {T}, Discard a card: Jaya Ballard, Task Mage deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.\n{5}{R}{R}, {T}, Discard a card: Jaya Ballard deals 6 damage to each creature and each player.').
card_image_name('jaya ballard, task mage'/'PD2', 'jaya ballard, task mage').
card_uid('jaya ballard, task mage'/'PD2', 'PD2:Jaya Ballard, Task Mage:jaya ballard, task mage').
card_rarity('jaya ballard, task mage'/'PD2', 'Rare').
card_artist('jaya ballard, task mage'/'PD2', 'Matt Cavotta').
card_number('jaya ballard, task mage'/'PD2', '10').
card_multiverse_id('jaya ballard, task mage'/'PD2', '234707').

card_in_set('keldon champion', 'PD2').
card_original_type('keldon champion'/'PD2', 'Creature — Human Barbarian').
card_original_text('keldon champion'/'PD2', 'Haste\nEcho {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Keldon Champion enters the battlefield, it deals 3 damage to target player.').
card_image_name('keldon champion'/'PD2', 'keldon champion').
card_uid('keldon champion'/'PD2', 'PD2:Keldon Champion:keldon champion').
card_rarity('keldon champion'/'PD2', 'Uncommon').
card_artist('keldon champion'/'PD2', 'Mark Tedin').
card_number('keldon champion'/'PD2', '14').
card_multiverse_id('keldon champion'/'PD2', '234713').

card_in_set('keldon marauders', 'PD2').
card_original_type('keldon marauders'/'PD2', 'Creature — Human Warrior').
card_original_text('keldon marauders'/'PD2', 'Vanishing 2 (This permanent enters the battlefield with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Keldon Marauders enters the battlefield or leaves the battlefield, it deals 1 damage to target player.').
card_image_name('keldon marauders'/'PD2', 'keldon marauders').
card_uid('keldon marauders'/'PD2', 'PD2:Keldon Marauders:keldon marauders').
card_rarity('keldon marauders'/'PD2', 'Common').
card_artist('keldon marauders'/'PD2', 'Alex Horley-Orlandelli').
card_number('keldon marauders'/'PD2', '7').
card_multiverse_id('keldon marauders'/'PD2', '234709').

card_in_set('lightning bolt', 'PD2').
card_original_type('lightning bolt'/'PD2', 'Instant').
card_original_text('lightning bolt'/'PD2', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'PD2', 'lightning bolt').
card_uid('lightning bolt'/'PD2', 'PD2:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'PD2', 'Common').
card_artist('lightning bolt'/'PD2', 'Christopher Moeller').
card_number('lightning bolt'/'PD2', '17').
card_multiverse_id('lightning bolt'/'PD2', '234704').

card_in_set('mogg fanatic', 'PD2').
card_original_type('mogg fanatic'/'PD2', 'Creature — Goblin').
card_original_text('mogg fanatic'/'PD2', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'PD2', 'mogg fanatic').
card_uid('mogg fanatic'/'PD2', 'PD2:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'PD2', 'Uncommon').
card_artist('mogg fanatic'/'PD2', 'Brom').
card_number('mogg fanatic'/'PD2', '3').
card_flavor_text('mogg fanatic'/'PD2', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'PD2', '234699').

card_in_set('mogg flunkies', 'PD2').
card_original_type('mogg flunkies'/'PD2', 'Creature — Goblin').
card_original_text('mogg flunkies'/'PD2', 'Mogg Flunkies can\'t attack or block alone.').
card_image_name('mogg flunkies'/'PD2', 'mogg flunkies').
card_uid('mogg flunkies'/'PD2', 'PD2:Mogg Flunkies:mogg flunkies').
card_rarity('mogg flunkies'/'PD2', 'Common').
card_artist('mogg flunkies'/'PD2', 'Brom').
card_number('mogg flunkies'/'PD2', '8').
card_flavor_text('mogg flunkies'/'PD2', 'They\'ll attack whatever\'s in front of them—as long as you tell them where that is.').
card_multiverse_id('mogg flunkies'/'PD2', '234710').

card_in_set('mountain', 'PD2').
card_original_type('mountain'/'PD2', 'Basic Land — Mountain').
card_original_text('mountain'/'PD2', 'R').
card_image_name('mountain'/'PD2', 'mountain1').
card_uid('mountain'/'PD2', 'PD2:Mountain:mountain1').
card_rarity('mountain'/'PD2', 'Basic Land').
card_artist('mountain'/'PD2', 'John Avon').
card_number('mountain'/'PD2', '31').
card_multiverse_id('mountain'/'PD2', '234738').

card_in_set('mountain', 'PD2').
card_original_type('mountain'/'PD2', 'Basic Land — Mountain').
card_original_text('mountain'/'PD2', 'R').
card_image_name('mountain'/'PD2', 'mountain2').
card_uid('mountain'/'PD2', 'PD2:Mountain:mountain2').
card_rarity('mountain'/'PD2', 'Basic Land').
card_artist('mountain'/'PD2', 'Aleksi Briclot').
card_number('mountain'/'PD2', '32').
card_multiverse_id('mountain'/'PD2', '234739').

card_in_set('mountain', 'PD2').
card_original_type('mountain'/'PD2', 'Basic Land — Mountain').
card_original_text('mountain'/'PD2', 'R').
card_image_name('mountain'/'PD2', 'mountain3').
card_uid('mountain'/'PD2', 'PD2:Mountain:mountain3').
card_rarity('mountain'/'PD2', 'Basic Land').
card_artist('mountain'/'PD2', 'Aleksi Briclot').
card_number('mountain'/'PD2', '33').
card_multiverse_id('mountain'/'PD2', '234740').

card_in_set('mountain', 'PD2').
card_original_type('mountain'/'PD2', 'Basic Land — Mountain').
card_original_text('mountain'/'PD2', 'R').
card_image_name('mountain'/'PD2', 'mountain4').
card_uid('mountain'/'PD2', 'PD2:Mountain:mountain4').
card_rarity('mountain'/'PD2', 'Basic Land').
card_artist('mountain'/'PD2', 'Richard Wright').
card_number('mountain'/'PD2', '34').
card_multiverse_id('mountain'/'PD2', '234741').

card_in_set('pillage', 'PD2').
card_original_type('pillage'/'PD2', 'Sorcery').
card_original_text('pillage'/'PD2', 'Destroy target artifact or land. It can\'t be regenerated.').
card_image_name('pillage'/'PD2', 'pillage').
card_uid('pillage'/'PD2', 'PD2:Pillage:pillage').
card_rarity('pillage'/'PD2', 'Uncommon').
card_artist('pillage'/'PD2', 'Bradley Williams').
card_number('pillage'/'PD2', '24').
card_multiverse_id('pillage'/'PD2', '234720').

card_in_set('price of progress', 'PD2').
card_original_type('price of progress'/'PD2', 'Instant').
card_original_text('price of progress'/'PD2', 'Price of Progress deals damage to each player equal to twice the number of nonbasic lands that player controls.').
card_image_name('price of progress'/'PD2', 'price of progress').
card_uid('price of progress'/'PD2', 'PD2:Price of Progress:price of progress').
card_rarity('price of progress'/'PD2', 'Uncommon').
card_artist('price of progress'/'PD2', 'Richard Kane Ferguson').
card_number('price of progress'/'PD2', '18').
card_flavor_text('price of progress'/'PD2', 'Man versus nature is not a fair fight.').
card_multiverse_id('price of progress'/'PD2', '234714').

card_in_set('reverberate', 'PD2').
card_original_type('reverberate'/'PD2', 'Instant').
card_original_text('reverberate'/'PD2', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('reverberate'/'PD2', 'reverberate').
card_uid('reverberate'/'PD2', 'PD2:Reverberate:reverberate').
card_rarity('reverberate'/'PD2', 'Rare').
card_artist('reverberate'/'PD2', 'jD').
card_number('reverberate'/'PD2', '20').
card_flavor_text('reverberate'/'PD2', '\"I\'m not indecisive. I just don\'t like to limit my options.\"\n—Maxti, pyromancer').
card_multiverse_id('reverberate'/'PD2', '234705').

card_in_set('spark elemental', 'PD2').
card_original_type('spark elemental'/'PD2', 'Creature — Elemental').
card_original_text('spark elemental'/'PD2', 'Trample, haste\nAt the beginning of the end step, sacrifice Spark Elemental.').
card_image_name('spark elemental'/'PD2', 'spark elemental').
card_uid('spark elemental'/'PD2', 'PD2:Spark Elemental:spark elemental').
card_rarity('spark elemental'/'PD2', 'Uncommon').
card_artist('spark elemental'/'PD2', 'John Avon').
card_number('spark elemental'/'PD2', '4').
card_flavor_text('spark elemental'/'PD2', 'Vulshok shamans could never keep them alive for more than a few seconds, yet those few seconds seemed to be enough.').
card_multiverse_id('spark elemental'/'PD2', '234700').

card_in_set('sudden impact', 'PD2').
card_original_type('sudden impact'/'PD2', 'Instant').
card_original_text('sudden impact'/'PD2', 'Sudden Impact deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('sudden impact'/'PD2', 'sudden impact').
card_uid('sudden impact'/'PD2', 'PD2:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'PD2', 'Uncommon').
card_artist('sudden impact'/'PD2', 'Wayne Reynolds').
card_number('sudden impact'/'PD2', '25').
card_flavor_text('sudden impact'/'PD2', '\"Some say it\'s better to think before you act. While those people are considering all the options, that\'s usually when I kill them.\"\n—Dravus, lava mage').
card_multiverse_id('sudden impact'/'PD2', '234701').

card_in_set('teetering peaks', 'PD2').
card_original_type('teetering peaks'/'PD2', 'Land').
card_original_text('teetering peaks'/'PD2', 'Teetering Peaks enters the battlefield tapped.\nWhen Teetering Peaks enters the battlefield, target creature gets +2/+0 until end of turn.\n{T}: Add {R} to your mana pool.').
card_image_name('teetering peaks'/'PD2', 'teetering peaks').
card_uid('teetering peaks'/'PD2', 'PD2:Teetering Peaks:teetering peaks').
card_rarity('teetering peaks'/'PD2', 'Common').
card_artist('teetering peaks'/'PD2', 'Fred Fields').
card_number('teetering peaks'/'PD2', '30').
card_multiverse_id('teetering peaks'/'PD2', '234715').

card_in_set('thunderbolt', 'PD2').
card_original_type('thunderbolt'/'PD2', 'Instant').
card_original_text('thunderbolt'/'PD2', 'Choose one — Thunderbolt deals 3 damage to target player; or Thunderbolt deals 4 damage to target creature with flying.').
card_image_name('thunderbolt'/'PD2', 'thunderbolt').
card_uid('thunderbolt'/'PD2', 'PD2:Thunderbolt:thunderbolt').
card_rarity('thunderbolt'/'PD2', 'Common').
card_artist('thunderbolt'/'PD2', 'Dylan Martens').
card_number('thunderbolt'/'PD2', '19').
card_flavor_text('thunderbolt'/'PD2', '\"Most wizards consider a thunderbolt to be a proper retort.\"\n—Ertai, wizard adept').
card_multiverse_id('thunderbolt'/'PD2', '234716').

card_in_set('vulshok sorcerer', 'PD2').
card_original_type('vulshok sorcerer'/'PD2', 'Creature — Human Shaman').
card_original_text('vulshok sorcerer'/'PD2', 'Haste\n{T}: Vulshok Sorcerer deals 1 damage to target creature or player.').
card_image_name('vulshok sorcerer'/'PD2', 'vulshok sorcerer').
card_uid('vulshok sorcerer'/'PD2', 'PD2:Vulshok Sorcerer:vulshok sorcerer').
card_rarity('vulshok sorcerer'/'PD2', 'Common').
card_artist('vulshok sorcerer'/'PD2', 'rk post').
card_number('vulshok sorcerer'/'PD2', '11').
card_flavor_text('vulshok sorcerer'/'PD2', 'Vulshok sorcerers train by leaping into electrified storm clouds. Dead or alive, they come back down with smiles on their faces.').
card_multiverse_id('vulshok sorcerer'/'PD2', '234719').
