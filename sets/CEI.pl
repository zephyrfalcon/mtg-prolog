% International Collector's Edition

set('CEI').
set_name('CEI', 'International Collector\'s Edition').
set_release_date('CEI', '1993-12-01').
set_border('CEI', 'black').
set_type('CEI', 'reprint').

card_in_set('air elemental', 'CEI').
card_original_type('air elemental'/'CEI', 'Creature — Elemental').
card_original_text('air elemental'/'CEI', '').
card_image_name('air elemental'/'CEI', 'air elemental').
card_uid('air elemental'/'CEI', 'CEI:Air Elemental:air elemental').
card_rarity('air elemental'/'CEI', 'Uncommon').
card_artist('air elemental'/'CEI', 'Richard Thomas').
card_flavor_text('air elemental'/'CEI', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').

card_in_set('ancestral recall', 'CEI').
card_original_type('ancestral recall'/'CEI', 'Instant').
card_original_text('ancestral recall'/'CEI', '').
card_image_name('ancestral recall'/'CEI', 'ancestral recall').
card_uid('ancestral recall'/'CEI', 'CEI:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'CEI', 'Rare').
card_artist('ancestral recall'/'CEI', 'Mark Poole').

card_in_set('animate artifact', 'CEI').
card_original_type('animate artifact'/'CEI', 'Enchantment — Aura').
card_original_text('animate artifact'/'CEI', '').
card_image_name('animate artifact'/'CEI', 'animate artifact').
card_uid('animate artifact'/'CEI', 'CEI:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'CEI', 'Uncommon').
card_artist('animate artifact'/'CEI', 'Douglas Shuler').

card_in_set('animate dead', 'CEI').
card_original_type('animate dead'/'CEI', 'Enchantment — Aura').
card_original_text('animate dead'/'CEI', '').
card_image_name('animate dead'/'CEI', 'animate dead').
card_uid('animate dead'/'CEI', 'CEI:Animate Dead:animate dead').
card_rarity('animate dead'/'CEI', 'Uncommon').
card_artist('animate dead'/'CEI', 'Anson Maddocks').

card_in_set('animate wall', 'CEI').
card_original_type('animate wall'/'CEI', 'Enchantment — Aura').
card_original_text('animate wall'/'CEI', '').
card_image_name('animate wall'/'CEI', 'animate wall').
card_uid('animate wall'/'CEI', 'CEI:Animate Wall:animate wall').
card_rarity('animate wall'/'CEI', 'Rare').
card_artist('animate wall'/'CEI', 'Dan Frazier').

card_in_set('ankh of mishra', 'CEI').
card_original_type('ankh of mishra'/'CEI', 'Artifact').
card_original_text('ankh of mishra'/'CEI', '').
card_image_name('ankh of mishra'/'CEI', 'ankh of mishra').
card_uid('ankh of mishra'/'CEI', 'CEI:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'CEI', 'Rare').
card_artist('ankh of mishra'/'CEI', 'Amy Weber').

card_in_set('armageddon', 'CEI').
card_original_type('armageddon'/'CEI', 'Sorcery').
card_original_text('armageddon'/'CEI', '').
card_image_name('armageddon'/'CEI', 'armageddon').
card_uid('armageddon'/'CEI', 'CEI:Armageddon:armageddon').
card_rarity('armageddon'/'CEI', 'Rare').
card_artist('armageddon'/'CEI', 'Jesper Myrfors').

card_in_set('aspect of wolf', 'CEI').
card_original_type('aspect of wolf'/'CEI', 'Enchantment — Aura').
card_original_text('aspect of wolf'/'CEI', '').
card_image_name('aspect of wolf'/'CEI', 'aspect of wolf').
card_uid('aspect of wolf'/'CEI', 'CEI:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'CEI', 'Rare').
card_artist('aspect of wolf'/'CEI', 'Jeff A. Menges').

card_in_set('bad moon', 'CEI').
card_original_type('bad moon'/'CEI', 'Enchantment').
card_original_text('bad moon'/'CEI', '').
card_image_name('bad moon'/'CEI', 'bad moon').
card_uid('bad moon'/'CEI', 'CEI:Bad Moon:bad moon').
card_rarity('bad moon'/'CEI', 'Rare').
card_artist('bad moon'/'CEI', 'Jesper Myrfors').

card_in_set('badlands', 'CEI').
card_original_type('badlands'/'CEI', 'Land — Swamp Mountain').
card_original_text('badlands'/'CEI', '').
card_image_name('badlands'/'CEI', 'badlands').
card_uid('badlands'/'CEI', 'CEI:Badlands:badlands').
card_rarity('badlands'/'CEI', 'Rare').
card_artist('badlands'/'CEI', 'Rob Alexander').

card_in_set('balance', 'CEI').
card_original_type('balance'/'CEI', 'Sorcery').
card_original_text('balance'/'CEI', '').
card_image_name('balance'/'CEI', 'balance').
card_uid('balance'/'CEI', 'CEI:Balance:balance').
card_rarity('balance'/'CEI', 'Rare').
card_artist('balance'/'CEI', 'Mark Poole').

card_in_set('basalt monolith', 'CEI').
card_original_type('basalt monolith'/'CEI', 'Artifact').
card_original_text('basalt monolith'/'CEI', '').
card_image_name('basalt monolith'/'CEI', 'basalt monolith').
card_uid('basalt monolith'/'CEI', 'CEI:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'CEI', 'Uncommon').
card_artist('basalt monolith'/'CEI', 'Jesper Myrfors').

card_in_set('bayou', 'CEI').
card_original_type('bayou'/'CEI', 'Land — Swamp Forest').
card_original_text('bayou'/'CEI', '').
card_image_name('bayou'/'CEI', 'bayou').
card_uid('bayou'/'CEI', 'CEI:Bayou:bayou').
card_rarity('bayou'/'CEI', 'Rare').
card_artist('bayou'/'CEI', 'Jesper Myrfors').

card_in_set('benalish hero', 'CEI').
card_original_type('benalish hero'/'CEI', 'Creature — Human Soldier').
card_original_text('benalish hero'/'CEI', '').
card_image_name('benalish hero'/'CEI', 'benalish hero').
card_uid('benalish hero'/'CEI', 'CEI:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'CEI', 'Common').
card_artist('benalish hero'/'CEI', 'Douglas Shuler').
card_flavor_text('benalish hero'/'CEI', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').

card_in_set('berserk', 'CEI').
card_original_type('berserk'/'CEI', 'Instant').
card_original_text('berserk'/'CEI', '').
card_image_name('berserk'/'CEI', 'berserk').
card_uid('berserk'/'CEI', 'CEI:Berserk:berserk').
card_rarity('berserk'/'CEI', 'Uncommon').
card_artist('berserk'/'CEI', 'Dan Frazier').

card_in_set('birds of paradise', 'CEI').
card_original_type('birds of paradise'/'CEI', 'Creature — Bird').
card_original_text('birds of paradise'/'CEI', '').
card_image_name('birds of paradise'/'CEI', 'birds of paradise').
card_uid('birds of paradise'/'CEI', 'CEI:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'CEI', 'Rare').
card_artist('birds of paradise'/'CEI', 'Mark Poole').

card_in_set('black knight', 'CEI').
card_original_type('black knight'/'CEI', 'Creature — Human Knight').
card_original_text('black knight'/'CEI', '').
card_image_name('black knight'/'CEI', 'black knight').
card_uid('black knight'/'CEI', 'CEI:Black Knight:black knight').
card_rarity('black knight'/'CEI', 'Uncommon').
card_artist('black knight'/'CEI', 'Jeff A. Menges').
card_flavor_text('black knight'/'CEI', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').

card_in_set('black lotus', 'CEI').
card_original_type('black lotus'/'CEI', 'Artifact').
card_original_text('black lotus'/'CEI', '').
card_image_name('black lotus'/'CEI', 'black lotus').
card_uid('black lotus'/'CEI', 'CEI:Black Lotus:black lotus').
card_rarity('black lotus'/'CEI', 'Rare').
card_artist('black lotus'/'CEI', 'Christopher Rush').

card_in_set('black vise', 'CEI').
card_original_type('black vise'/'CEI', 'Artifact').
card_original_text('black vise'/'CEI', '').
card_image_name('black vise'/'CEI', 'black vise').
card_uid('black vise'/'CEI', 'CEI:Black Vise:black vise').
card_rarity('black vise'/'CEI', 'Uncommon').
card_artist('black vise'/'CEI', 'Richard Thomas').

card_in_set('black ward', 'CEI').
card_original_type('black ward'/'CEI', 'Enchantment — Aura').
card_original_text('black ward'/'CEI', '').
card_image_name('black ward'/'CEI', 'black ward').
card_uid('black ward'/'CEI', 'CEI:Black Ward:black ward').
card_rarity('black ward'/'CEI', 'Uncommon').
card_artist('black ward'/'CEI', 'Dan Frazier').

card_in_set('blaze of glory', 'CEI').
card_original_type('blaze of glory'/'CEI', 'Instant').
card_original_text('blaze of glory'/'CEI', '').
card_image_name('blaze of glory'/'CEI', 'blaze of glory').
card_uid('blaze of glory'/'CEI', 'CEI:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'CEI', 'Rare').
card_artist('blaze of glory'/'CEI', 'Richard Thomas').

card_in_set('blessing', 'CEI').
card_original_type('blessing'/'CEI', 'Enchantment — Aura').
card_original_text('blessing'/'CEI', '').
card_image_name('blessing'/'CEI', 'blessing').
card_uid('blessing'/'CEI', 'CEI:Blessing:blessing').
card_rarity('blessing'/'CEI', 'Rare').
card_artist('blessing'/'CEI', 'Julie Baroh').

card_in_set('blue elemental blast', 'CEI').
card_original_type('blue elemental blast'/'CEI', 'Instant').
card_original_text('blue elemental blast'/'CEI', '').
card_image_name('blue elemental blast'/'CEI', 'blue elemental blast').
card_uid('blue elemental blast'/'CEI', 'CEI:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'CEI', 'Common').
card_artist('blue elemental blast'/'CEI', 'Richard Thomas').

card_in_set('blue ward', 'CEI').
card_original_type('blue ward'/'CEI', 'Enchantment — Aura').
card_original_text('blue ward'/'CEI', '').
card_image_name('blue ward'/'CEI', 'blue ward').
card_uid('blue ward'/'CEI', 'CEI:Blue Ward:blue ward').
card_rarity('blue ward'/'CEI', 'Uncommon').
card_artist('blue ward'/'CEI', 'Dan Frazier').

card_in_set('bog wraith', 'CEI').
card_original_type('bog wraith'/'CEI', 'Creature — Wraith').
card_original_text('bog wraith'/'CEI', '').
card_image_name('bog wraith'/'CEI', 'bog wraith').
card_uid('bog wraith'/'CEI', 'CEI:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'CEI', 'Uncommon').
card_artist('bog wraith'/'CEI', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'CEI', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').

card_in_set('braingeyser', 'CEI').
card_original_type('braingeyser'/'CEI', 'Sorcery').
card_original_text('braingeyser'/'CEI', '').
card_image_name('braingeyser'/'CEI', 'braingeyser').
card_uid('braingeyser'/'CEI', 'CEI:Braingeyser:braingeyser').
card_rarity('braingeyser'/'CEI', 'Rare').
card_artist('braingeyser'/'CEI', 'Mark Tedin').

card_in_set('burrowing', 'CEI').
card_original_type('burrowing'/'CEI', 'Enchantment — Aura').
card_original_text('burrowing'/'CEI', '').
card_image_name('burrowing'/'CEI', 'burrowing').
card_uid('burrowing'/'CEI', 'CEI:Burrowing:burrowing').
card_rarity('burrowing'/'CEI', 'Uncommon').
card_artist('burrowing'/'CEI', 'Mark Poole').

card_in_set('camouflage', 'CEI').
card_original_type('camouflage'/'CEI', 'Instant').
card_original_text('camouflage'/'CEI', '').
card_image_name('camouflage'/'CEI', 'camouflage').
card_uid('camouflage'/'CEI', 'CEI:Camouflage:camouflage').
card_rarity('camouflage'/'CEI', 'Uncommon').
card_artist('camouflage'/'CEI', 'Jesper Myrfors').

card_in_set('castle', 'CEI').
card_original_type('castle'/'CEI', 'Enchantment').
card_original_text('castle'/'CEI', '').
card_image_name('castle'/'CEI', 'castle').
card_uid('castle'/'CEI', 'CEI:Castle:castle').
card_rarity('castle'/'CEI', 'Uncommon').
card_artist('castle'/'CEI', 'Dameon Willich').

card_in_set('celestial prism', 'CEI').
card_original_type('celestial prism'/'CEI', 'Artifact').
card_original_text('celestial prism'/'CEI', '').
card_image_name('celestial prism'/'CEI', 'celestial prism').
card_uid('celestial prism'/'CEI', 'CEI:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'CEI', 'Uncommon').
card_artist('celestial prism'/'CEI', 'Amy Weber').

card_in_set('channel', 'CEI').
card_original_type('channel'/'CEI', 'Sorcery').
card_original_text('channel'/'CEI', '').
card_image_name('channel'/'CEI', 'channel').
card_uid('channel'/'CEI', 'CEI:Channel:channel').
card_rarity('channel'/'CEI', 'Uncommon').
card_artist('channel'/'CEI', 'Richard Thomas').

card_in_set('chaos orb', 'CEI').
card_original_type('chaos orb'/'CEI', 'Artifact').
card_original_text('chaos orb'/'CEI', '').
card_image_name('chaos orb'/'CEI', 'chaos orb').
card_uid('chaos orb'/'CEI', 'CEI:Chaos Orb:chaos orb').
card_rarity('chaos orb'/'CEI', 'Rare').
card_artist('chaos orb'/'CEI', 'Mark Tedin').

card_in_set('chaoslace', 'CEI').
card_original_type('chaoslace'/'CEI', 'Instant').
card_original_text('chaoslace'/'CEI', '').
card_image_name('chaoslace'/'CEI', 'chaoslace').
card_uid('chaoslace'/'CEI', 'CEI:Chaoslace:chaoslace').
card_rarity('chaoslace'/'CEI', 'Rare').
card_artist('chaoslace'/'CEI', 'Dameon Willich').

card_in_set('circle of protection: black', 'CEI').
card_original_type('circle of protection: black'/'CEI', 'Enchantment').
card_original_text('circle of protection: black'/'CEI', '').
card_image_name('circle of protection: black'/'CEI', 'circle of protection black').
card_uid('circle of protection: black'/'CEI', 'CEI:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'CEI', 'Common').
card_artist('circle of protection: black'/'CEI', 'Jesper Myrfors').

card_in_set('circle of protection: blue', 'CEI').
card_original_type('circle of protection: blue'/'CEI', 'Enchantment').
card_original_text('circle of protection: blue'/'CEI', '').
card_image_name('circle of protection: blue'/'CEI', 'circle of protection blue').
card_uid('circle of protection: blue'/'CEI', 'CEI:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'CEI', 'Common').
card_artist('circle of protection: blue'/'CEI', 'Dameon Willich').

card_in_set('circle of protection: green', 'CEI').
card_original_type('circle of protection: green'/'CEI', 'Enchantment').
card_original_text('circle of protection: green'/'CEI', '').
card_image_name('circle of protection: green'/'CEI', 'circle of protection green').
card_uid('circle of protection: green'/'CEI', 'CEI:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'CEI', 'Common').
card_artist('circle of protection: green'/'CEI', 'Sandra Everingham').

card_in_set('circle of protection: red', 'CEI').
card_original_type('circle of protection: red'/'CEI', 'Enchantment').
card_original_text('circle of protection: red'/'CEI', '').
card_image_name('circle of protection: red'/'CEI', 'circle of protection red').
card_uid('circle of protection: red'/'CEI', 'CEI:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'CEI', 'Common').
card_artist('circle of protection: red'/'CEI', 'Mark Tedin').

card_in_set('circle of protection: white', 'CEI').
card_original_type('circle of protection: white'/'CEI', 'Enchantment').
card_original_text('circle of protection: white'/'CEI', '').
card_image_name('circle of protection: white'/'CEI', 'circle of protection white').
card_uid('circle of protection: white'/'CEI', 'CEI:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'CEI', 'Common').
card_artist('circle of protection: white'/'CEI', 'Douglas Shuler').

card_in_set('clockwork beast', 'CEI').
card_original_type('clockwork beast'/'CEI', 'Artifact Creature — Beast').
card_original_text('clockwork beast'/'CEI', '').
card_image_name('clockwork beast'/'CEI', 'clockwork beast').
card_uid('clockwork beast'/'CEI', 'CEI:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'CEI', 'Rare').
card_artist('clockwork beast'/'CEI', 'Drew Tucker').

card_in_set('clone', 'CEI').
card_original_type('clone'/'CEI', 'Creature — Shapeshifter').
card_original_text('clone'/'CEI', '').
card_image_name('clone'/'CEI', 'clone').
card_uid('clone'/'CEI', 'CEI:Clone:clone').
card_rarity('clone'/'CEI', 'Uncommon').
card_artist('clone'/'CEI', 'Julie Baroh').

card_in_set('cockatrice', 'CEI').
card_original_type('cockatrice'/'CEI', 'Creature — Cockatrice').
card_original_text('cockatrice'/'CEI', '').
card_image_name('cockatrice'/'CEI', 'cockatrice').
card_uid('cockatrice'/'CEI', 'CEI:Cockatrice:cockatrice').
card_rarity('cockatrice'/'CEI', 'Rare').
card_artist('cockatrice'/'CEI', 'Dan Frazier').

card_in_set('consecrate land', 'CEI').
card_original_type('consecrate land'/'CEI', 'Enchantment — Aura').
card_original_text('consecrate land'/'CEI', '').
card_image_name('consecrate land'/'CEI', 'consecrate land').
card_uid('consecrate land'/'CEI', 'CEI:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'CEI', 'Uncommon').
card_artist('consecrate land'/'CEI', 'Jeff A. Menges').

card_in_set('conservator', 'CEI').
card_original_type('conservator'/'CEI', 'Artifact').
card_original_text('conservator'/'CEI', '').
card_image_name('conservator'/'CEI', 'conservator').
card_uid('conservator'/'CEI', 'CEI:Conservator:conservator').
card_rarity('conservator'/'CEI', 'Uncommon').
card_artist('conservator'/'CEI', 'Amy Weber').

card_in_set('contract from below', 'CEI').
card_original_type('contract from below'/'CEI', 'Sorcery').
card_original_text('contract from below'/'CEI', '').
card_image_name('contract from below'/'CEI', 'contract from below').
card_uid('contract from below'/'CEI', 'CEI:Contract from Below:contract from below').
card_rarity('contract from below'/'CEI', 'Rare').
card_artist('contract from below'/'CEI', 'Douglas Shuler').

card_in_set('control magic', 'CEI').
card_original_type('control magic'/'CEI', 'Enchantment — Aura').
card_original_text('control magic'/'CEI', '').
card_image_name('control magic'/'CEI', 'control magic').
card_uid('control magic'/'CEI', 'CEI:Control Magic:control magic').
card_rarity('control magic'/'CEI', 'Uncommon').
card_artist('control magic'/'CEI', 'Dameon Willich').

card_in_set('conversion', 'CEI').
card_original_type('conversion'/'CEI', 'Enchantment').
card_original_text('conversion'/'CEI', '').
card_image_name('conversion'/'CEI', 'conversion').
card_uid('conversion'/'CEI', 'CEI:Conversion:conversion').
card_rarity('conversion'/'CEI', 'Uncommon').
card_artist('conversion'/'CEI', 'Jesper Myrfors').

card_in_set('copper tablet', 'CEI').
card_original_type('copper tablet'/'CEI', 'Artifact').
card_original_text('copper tablet'/'CEI', '').
card_image_name('copper tablet'/'CEI', 'copper tablet').
card_uid('copper tablet'/'CEI', 'CEI:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'CEI', 'Uncommon').
card_artist('copper tablet'/'CEI', 'Amy Weber').

card_in_set('copy artifact', 'CEI').
card_original_type('copy artifact'/'CEI', 'Enchantment').
card_original_text('copy artifact'/'CEI', '').
card_image_name('copy artifact'/'CEI', 'copy artifact').
card_uid('copy artifact'/'CEI', 'CEI:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'CEI', 'Rare').
card_artist('copy artifact'/'CEI', 'Amy Weber').

card_in_set('counterspell', 'CEI').
card_original_type('counterspell'/'CEI', 'Instant').
card_original_text('counterspell'/'CEI', '').
card_image_name('counterspell'/'CEI', 'counterspell').
card_uid('counterspell'/'CEI', 'CEI:Counterspell:counterspell').
card_rarity('counterspell'/'CEI', 'Uncommon').
card_artist('counterspell'/'CEI', 'Mark Poole').

card_in_set('craw wurm', 'CEI').
card_original_type('craw wurm'/'CEI', 'Creature — Wurm').
card_original_text('craw wurm'/'CEI', '').
card_image_name('craw wurm'/'CEI', 'craw wurm').
card_uid('craw wurm'/'CEI', 'CEI:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'CEI', 'Common').
card_artist('craw wurm'/'CEI', 'Daniel Gelon').
card_flavor_text('craw wurm'/'CEI', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').

card_in_set('creature bond', 'CEI').
card_original_type('creature bond'/'CEI', 'Enchantment — Aura').
card_original_text('creature bond'/'CEI', '').
card_image_name('creature bond'/'CEI', 'creature bond').
card_uid('creature bond'/'CEI', 'CEI:Creature Bond:creature bond').
card_rarity('creature bond'/'CEI', 'Common').
card_artist('creature bond'/'CEI', 'Anson Maddocks').

card_in_set('crusade', 'CEI').
card_original_type('crusade'/'CEI', 'Enchantment').
card_original_text('crusade'/'CEI', '').
card_image_name('crusade'/'CEI', 'crusade').
card_uid('crusade'/'CEI', 'CEI:Crusade:crusade').
card_rarity('crusade'/'CEI', 'Rare').
card_artist('crusade'/'CEI', 'Mark Poole').

card_in_set('crystal rod', 'CEI').
card_original_type('crystal rod'/'CEI', 'Artifact').
card_original_text('crystal rod'/'CEI', '').
card_image_name('crystal rod'/'CEI', 'crystal rod').
card_uid('crystal rod'/'CEI', 'CEI:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'CEI', 'Uncommon').
card_artist('crystal rod'/'CEI', 'Amy Weber').

card_in_set('cursed land', 'CEI').
card_original_type('cursed land'/'CEI', 'Enchantment — Aura').
card_original_text('cursed land'/'CEI', '').
card_image_name('cursed land'/'CEI', 'cursed land').
card_uid('cursed land'/'CEI', 'CEI:Cursed Land:cursed land').
card_rarity('cursed land'/'CEI', 'Uncommon').
card_artist('cursed land'/'CEI', 'Jesper Myrfors').

card_in_set('cyclopean tomb', 'CEI').
card_original_type('cyclopean tomb'/'CEI', 'Artifact').
card_original_text('cyclopean tomb'/'CEI', '').
card_image_name('cyclopean tomb'/'CEI', 'cyclopean tomb').
card_uid('cyclopean tomb'/'CEI', 'CEI:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'CEI', 'Rare').
card_artist('cyclopean tomb'/'CEI', 'Anson Maddocks').

card_in_set('dark ritual', 'CEI').
card_original_type('dark ritual'/'CEI', 'Instant').
card_original_text('dark ritual'/'CEI', '').
card_image_name('dark ritual'/'CEI', 'dark ritual').
card_uid('dark ritual'/'CEI', 'CEI:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'CEI', 'Common').
card_artist('dark ritual'/'CEI', 'Sandra Everingham').

card_in_set('darkpact', 'CEI').
card_original_type('darkpact'/'CEI', 'Sorcery').
card_original_text('darkpact'/'CEI', '').
card_image_name('darkpact'/'CEI', 'darkpact').
card_uid('darkpact'/'CEI', 'CEI:Darkpact:darkpact').
card_rarity('darkpact'/'CEI', 'Rare').
card_artist('darkpact'/'CEI', 'Quinton Hoover').

card_in_set('death ward', 'CEI').
card_original_type('death ward'/'CEI', 'Instant').
card_original_text('death ward'/'CEI', '').
card_image_name('death ward'/'CEI', 'death ward').
card_uid('death ward'/'CEI', 'CEI:Death Ward:death ward').
card_rarity('death ward'/'CEI', 'Common').
card_artist('death ward'/'CEI', 'Mark Poole').

card_in_set('deathgrip', 'CEI').
card_original_type('deathgrip'/'CEI', 'Enchantment').
card_original_text('deathgrip'/'CEI', '').
card_image_name('deathgrip'/'CEI', 'deathgrip').
card_uid('deathgrip'/'CEI', 'CEI:Deathgrip:deathgrip').
card_rarity('deathgrip'/'CEI', 'Uncommon').
card_artist('deathgrip'/'CEI', 'Anson Maddocks').

card_in_set('deathlace', 'CEI').
card_original_type('deathlace'/'CEI', 'Instant').
card_original_text('deathlace'/'CEI', '').
card_image_name('deathlace'/'CEI', 'deathlace').
card_uid('deathlace'/'CEI', 'CEI:Deathlace:deathlace').
card_rarity('deathlace'/'CEI', 'Rare').
card_artist('deathlace'/'CEI', 'Sandra Everingham').

card_in_set('demonic attorney', 'CEI').
card_original_type('demonic attorney'/'CEI', 'Sorcery').
card_original_text('demonic attorney'/'CEI', '').
card_image_name('demonic attorney'/'CEI', 'demonic attorney').
card_uid('demonic attorney'/'CEI', 'CEI:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'CEI', 'Rare').
card_artist('demonic attorney'/'CEI', 'Daniel Gelon').

card_in_set('demonic hordes', 'CEI').
card_original_type('demonic hordes'/'CEI', 'Creature — Demon').
card_original_text('demonic hordes'/'CEI', '').
card_image_name('demonic hordes'/'CEI', 'demonic hordes').
card_uid('demonic hordes'/'CEI', 'CEI:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'CEI', 'Rare').
card_artist('demonic hordes'/'CEI', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'CEI', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').

card_in_set('demonic tutor', 'CEI').
card_original_type('demonic tutor'/'CEI', 'Sorcery').
card_original_text('demonic tutor'/'CEI', '').
card_image_name('demonic tutor'/'CEI', 'demonic tutor').
card_uid('demonic tutor'/'CEI', 'CEI:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'CEI', 'Uncommon').
card_artist('demonic tutor'/'CEI', 'Douglas Shuler').

card_in_set('dingus egg', 'CEI').
card_original_type('dingus egg'/'CEI', 'Artifact').
card_original_text('dingus egg'/'CEI', '').
card_image_name('dingus egg'/'CEI', 'dingus egg').
card_uid('dingus egg'/'CEI', 'CEI:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'CEI', 'Rare').
card_artist('dingus egg'/'CEI', 'Dan Frazier').

card_in_set('disenchant', 'CEI').
card_original_type('disenchant'/'CEI', 'Instant').
card_original_text('disenchant'/'CEI', '').
card_image_name('disenchant'/'CEI', 'disenchant').
card_uid('disenchant'/'CEI', 'CEI:Disenchant:disenchant').
card_rarity('disenchant'/'CEI', 'Common').
card_artist('disenchant'/'CEI', 'Amy Weber').

card_in_set('disintegrate', 'CEI').
card_original_type('disintegrate'/'CEI', 'Sorcery').
card_original_text('disintegrate'/'CEI', '').
card_image_name('disintegrate'/'CEI', 'disintegrate').
card_uid('disintegrate'/'CEI', 'CEI:Disintegrate:disintegrate').
card_rarity('disintegrate'/'CEI', 'Common').
card_artist('disintegrate'/'CEI', 'Anson Maddocks').

card_in_set('disrupting scepter', 'CEI').
card_original_type('disrupting scepter'/'CEI', 'Artifact').
card_original_text('disrupting scepter'/'CEI', '').
card_image_name('disrupting scepter'/'CEI', 'disrupting scepter').
card_uid('disrupting scepter'/'CEI', 'CEI:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'CEI', 'Rare').
card_artist('disrupting scepter'/'CEI', 'Dan Frazier').

card_in_set('dragon whelp', 'CEI').
card_original_type('dragon whelp'/'CEI', 'Creature — Dragon').
card_original_text('dragon whelp'/'CEI', '').
card_image_name('dragon whelp'/'CEI', 'dragon whelp').
card_uid('dragon whelp'/'CEI', 'CEI:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'CEI', 'Uncommon').
card_artist('dragon whelp'/'CEI', 'Amy Weber').
card_flavor_text('dragon whelp'/'CEI', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').

card_in_set('drain life', 'CEI').
card_original_type('drain life'/'CEI', 'Sorcery').
card_original_text('drain life'/'CEI', '').
card_image_name('drain life'/'CEI', 'drain life').
card_uid('drain life'/'CEI', 'CEI:Drain Life:drain life').
card_rarity('drain life'/'CEI', 'Common').
card_artist('drain life'/'CEI', 'Douglas Shuler').

card_in_set('drain power', 'CEI').
card_original_type('drain power'/'CEI', 'Sorcery').
card_original_text('drain power'/'CEI', '').
card_image_name('drain power'/'CEI', 'drain power').
card_uid('drain power'/'CEI', 'CEI:Drain Power:drain power').
card_rarity('drain power'/'CEI', 'Rare').
card_artist('drain power'/'CEI', 'Douglas Shuler').

card_in_set('drudge skeletons', 'CEI').
card_original_type('drudge skeletons'/'CEI', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'CEI', '').
card_image_name('drudge skeletons'/'CEI', 'drudge skeletons').
card_uid('drudge skeletons'/'CEI', 'CEI:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'CEI', 'Common').
card_artist('drudge skeletons'/'CEI', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'CEI', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').

card_in_set('dwarven demolition team', 'CEI').
card_original_type('dwarven demolition team'/'CEI', 'Creature — Dwarf').
card_original_text('dwarven demolition team'/'CEI', '').
card_image_name('dwarven demolition team'/'CEI', 'dwarven demolition team').
card_uid('dwarven demolition team'/'CEI', 'CEI:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'CEI', 'Uncommon').
card_artist('dwarven demolition team'/'CEI', 'Kev Brockschmidt').
card_flavor_text('dwarven demolition team'/'CEI', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').

card_in_set('dwarven warriors', 'CEI').
card_original_type('dwarven warriors'/'CEI', 'Creature — Dwarf Warrior').
card_original_text('dwarven warriors'/'CEI', '').
card_image_name('dwarven warriors'/'CEI', 'dwarven warriors').
card_uid('dwarven warriors'/'CEI', 'CEI:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'CEI', 'Common').
card_artist('dwarven warriors'/'CEI', 'Douglas Shuler').

card_in_set('earth elemental', 'CEI').
card_original_type('earth elemental'/'CEI', 'Creature — Elemental').
card_original_text('earth elemental'/'CEI', '').
card_image_name('earth elemental'/'CEI', 'earth elemental').
card_uid('earth elemental'/'CEI', 'CEI:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'CEI', 'Uncommon').
card_artist('earth elemental'/'CEI', 'Dan Frazier').
card_flavor_text('earth elemental'/'CEI', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').

card_in_set('earthbind', 'CEI').
card_original_type('earthbind'/'CEI', 'Enchantment — Aura').
card_original_text('earthbind'/'CEI', '').
card_image_name('earthbind'/'CEI', 'earthbind').
card_uid('earthbind'/'CEI', 'CEI:Earthbind:earthbind').
card_rarity('earthbind'/'CEI', 'Common').
card_artist('earthbind'/'CEI', 'Quinton Hoover').

card_in_set('earthquake', 'CEI').
card_original_type('earthquake'/'CEI', 'Sorcery').
card_original_text('earthquake'/'CEI', '').
card_image_name('earthquake'/'CEI', 'earthquake').
card_uid('earthquake'/'CEI', 'CEI:Earthquake:earthquake').
card_rarity('earthquake'/'CEI', 'Rare').
card_artist('earthquake'/'CEI', 'Dan Frazier').

card_in_set('elvish archers', 'CEI').
card_original_type('elvish archers'/'CEI', 'Creature — Elf Archer').
card_original_text('elvish archers'/'CEI', '').
card_image_name('elvish archers'/'CEI', 'elvish archers').
card_uid('elvish archers'/'CEI', 'CEI:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'CEI', 'Rare').
card_artist('elvish archers'/'CEI', 'Anson Maddocks').
card_flavor_text('elvish archers'/'CEI', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').

card_in_set('evil presence', 'CEI').
card_original_type('evil presence'/'CEI', 'Enchantment — Aura').
card_original_text('evil presence'/'CEI', '').
card_image_name('evil presence'/'CEI', 'evil presence').
card_uid('evil presence'/'CEI', 'CEI:Evil Presence:evil presence').
card_rarity('evil presence'/'CEI', 'Uncommon').
card_artist('evil presence'/'CEI', 'Sandra Everingham').

card_in_set('false orders', 'CEI').
card_original_type('false orders'/'CEI', 'Instant').
card_original_text('false orders'/'CEI', '').
card_image_name('false orders'/'CEI', 'false orders').
card_uid('false orders'/'CEI', 'CEI:False Orders:false orders').
card_rarity('false orders'/'CEI', 'Common').
card_artist('false orders'/'CEI', 'Anson Maddocks').

card_in_set('farmstead', 'CEI').
card_original_type('farmstead'/'CEI', 'Enchantment — Aura').
card_original_text('farmstead'/'CEI', '').
card_image_name('farmstead'/'CEI', 'farmstead').
card_uid('farmstead'/'CEI', 'CEI:Farmstead:farmstead').
card_rarity('farmstead'/'CEI', 'Rare').
card_artist('farmstead'/'CEI', 'Mark Poole').

card_in_set('fastbond', 'CEI').
card_original_type('fastbond'/'CEI', 'Enchantment').
card_original_text('fastbond'/'CEI', '').
card_image_name('fastbond'/'CEI', 'fastbond').
card_uid('fastbond'/'CEI', 'CEI:Fastbond:fastbond').
card_rarity('fastbond'/'CEI', 'Rare').
card_artist('fastbond'/'CEI', 'Mark Poole').

card_in_set('fear', 'CEI').
card_original_type('fear'/'CEI', 'Enchantment — Aura').
card_original_text('fear'/'CEI', '').
card_image_name('fear'/'CEI', 'fear').
card_uid('fear'/'CEI', 'CEI:Fear:fear').
card_rarity('fear'/'CEI', 'Common').
card_artist('fear'/'CEI', 'Mark Poole').

card_in_set('feedback', 'CEI').
card_original_type('feedback'/'CEI', 'Enchantment — Aura').
card_original_text('feedback'/'CEI', '').
card_image_name('feedback'/'CEI', 'feedback').
card_uid('feedback'/'CEI', 'CEI:Feedback:feedback').
card_rarity('feedback'/'CEI', 'Uncommon').
card_artist('feedback'/'CEI', 'Quinton Hoover').

card_in_set('fire elemental', 'CEI').
card_original_type('fire elemental'/'CEI', 'Creature — Elemental').
card_original_text('fire elemental'/'CEI', '').
card_image_name('fire elemental'/'CEI', 'fire elemental').
card_uid('fire elemental'/'CEI', 'CEI:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'CEI', 'Uncommon').
card_artist('fire elemental'/'CEI', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'CEI', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').

card_in_set('fireball', 'CEI').
card_original_type('fireball'/'CEI', 'Sorcery').
card_original_text('fireball'/'CEI', '').
card_image_name('fireball'/'CEI', 'fireball').
card_uid('fireball'/'CEI', 'CEI:Fireball:fireball').
card_rarity('fireball'/'CEI', 'Common').
card_artist('fireball'/'CEI', 'Mark Tedin').

card_in_set('firebreathing', 'CEI').
card_original_type('firebreathing'/'CEI', 'Enchantment — Aura').
card_original_text('firebreathing'/'CEI', '').
card_image_name('firebreathing'/'CEI', 'firebreathing').
card_uid('firebreathing'/'CEI', 'CEI:Firebreathing:firebreathing').
card_rarity('firebreathing'/'CEI', 'Common').
card_artist('firebreathing'/'CEI', 'Dan Frazier').
card_flavor_text('firebreathing'/'CEI', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').

card_in_set('flashfires', 'CEI').
card_original_type('flashfires'/'CEI', 'Sorcery').
card_original_text('flashfires'/'CEI', '').
card_image_name('flashfires'/'CEI', 'flashfires').
card_uid('flashfires'/'CEI', 'CEI:Flashfires:flashfires').
card_rarity('flashfires'/'CEI', 'Uncommon').
card_artist('flashfires'/'CEI', 'Dameon Willich').

card_in_set('flight', 'CEI').
card_original_type('flight'/'CEI', 'Enchantment — Aura').
card_original_text('flight'/'CEI', '').
card_image_name('flight'/'CEI', 'flight').
card_uid('flight'/'CEI', 'CEI:Flight:flight').
card_rarity('flight'/'CEI', 'Common').
card_artist('flight'/'CEI', 'Anson Maddocks').

card_in_set('fog', 'CEI').
card_original_type('fog'/'CEI', 'Instant').
card_original_text('fog'/'CEI', '').
card_image_name('fog'/'CEI', 'fog').
card_uid('fog'/'CEI', 'CEI:Fog:fog').
card_rarity('fog'/'CEI', 'Common').
card_artist('fog'/'CEI', 'Jesper Myrfors').

card_in_set('force of nature', 'CEI').
card_original_type('force of nature'/'CEI', 'Creature — Elemental').
card_original_text('force of nature'/'CEI', '').
card_image_name('force of nature'/'CEI', 'force of nature').
card_uid('force of nature'/'CEI', 'CEI:Force of Nature:force of nature').
card_rarity('force of nature'/'CEI', 'Rare').
card_artist('force of nature'/'CEI', 'Douglas Shuler').

card_in_set('forcefield', 'CEI').
card_original_type('forcefield'/'CEI', 'Artifact').
card_original_text('forcefield'/'CEI', '').
card_image_name('forcefield'/'CEI', 'forcefield').
card_uid('forcefield'/'CEI', 'CEI:Forcefield:forcefield').
card_rarity('forcefield'/'CEI', 'Rare').
card_artist('forcefield'/'CEI', 'Dan Frazier').

card_in_set('forest', 'CEI').
card_original_type('forest'/'CEI', 'Basic Land — Forest').
card_original_text('forest'/'CEI', '').
card_image_name('forest'/'CEI', 'forest1').
card_uid('forest'/'CEI', 'CEI:Forest:forest1').
card_rarity('forest'/'CEI', 'Basic Land').
card_artist('forest'/'CEI', 'Christopher Rush').

card_in_set('forest', 'CEI').
card_original_type('forest'/'CEI', 'Basic Land — Forest').
card_original_text('forest'/'CEI', '').
card_image_name('forest'/'CEI', 'forest2').
card_uid('forest'/'CEI', 'CEI:Forest:forest2').
card_rarity('forest'/'CEI', 'Basic Land').
card_artist('forest'/'CEI', 'Christopher Rush').

card_in_set('forest', 'CEI').
card_original_type('forest'/'CEI', 'Basic Land — Forest').
card_original_text('forest'/'CEI', '').
card_image_name('forest'/'CEI', 'forest3').
card_uid('forest'/'CEI', 'CEI:Forest:forest3').
card_rarity('forest'/'CEI', 'Basic Land').
card_artist('forest'/'CEI', 'Christopher Rush').

card_in_set('fork', 'CEI').
card_original_type('fork'/'CEI', 'Instant').
card_original_text('fork'/'CEI', '').
card_image_name('fork'/'CEI', 'fork').
card_uid('fork'/'CEI', 'CEI:Fork:fork').
card_rarity('fork'/'CEI', 'Rare').
card_artist('fork'/'CEI', 'Amy Weber').

card_in_set('frozen shade', 'CEI').
card_original_type('frozen shade'/'CEI', 'Creature — Shade').
card_original_text('frozen shade'/'CEI', '').
card_image_name('frozen shade'/'CEI', 'frozen shade').
card_uid('frozen shade'/'CEI', 'CEI:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'CEI', 'Common').
card_artist('frozen shade'/'CEI', 'Douglas Shuler').
card_flavor_text('frozen shade'/'CEI', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').

card_in_set('fungusaur', 'CEI').
card_original_type('fungusaur'/'CEI', 'Creature — Fungus Lizard').
card_original_text('fungusaur'/'CEI', '').
card_image_name('fungusaur'/'CEI', 'fungusaur').
card_uid('fungusaur'/'CEI', 'CEI:Fungusaur:fungusaur').
card_rarity('fungusaur'/'CEI', 'Rare').
card_artist('fungusaur'/'CEI', 'Daniel Gelon').
card_flavor_text('fungusaur'/'CEI', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').

card_in_set('gaea\'s liege', 'CEI').
card_original_type('gaea\'s liege'/'CEI', 'Creature — Avatar').
card_original_text('gaea\'s liege'/'CEI', '').
card_image_name('gaea\'s liege'/'CEI', 'gaea\'s liege').
card_uid('gaea\'s liege'/'CEI', 'CEI:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'CEI', 'Rare').
card_artist('gaea\'s liege'/'CEI', 'Dameon Willich').

card_in_set('gauntlet of might', 'CEI').
card_original_type('gauntlet of might'/'CEI', 'Artifact').
card_original_text('gauntlet of might'/'CEI', '').
card_image_name('gauntlet of might'/'CEI', 'gauntlet of might').
card_uid('gauntlet of might'/'CEI', 'CEI:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'CEI', 'Rare').
card_artist('gauntlet of might'/'CEI', 'Christopher Rush').

card_in_set('giant growth', 'CEI').
card_original_type('giant growth'/'CEI', 'Instant').
card_original_text('giant growth'/'CEI', '').
card_image_name('giant growth'/'CEI', 'giant growth').
card_uid('giant growth'/'CEI', 'CEI:Giant Growth:giant growth').
card_rarity('giant growth'/'CEI', 'Common').
card_artist('giant growth'/'CEI', 'Sandra Everingham').

card_in_set('giant spider', 'CEI').
card_original_type('giant spider'/'CEI', 'Creature — Spider').
card_original_text('giant spider'/'CEI', '').
card_image_name('giant spider'/'CEI', 'giant spider').
card_uid('giant spider'/'CEI', 'CEI:Giant Spider:giant spider').
card_rarity('giant spider'/'CEI', 'Common').
card_artist('giant spider'/'CEI', 'Sandra Everingham').
card_flavor_text('giant spider'/'CEI', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').

card_in_set('glasses of urza', 'CEI').
card_original_type('glasses of urza'/'CEI', 'Artifact').
card_original_text('glasses of urza'/'CEI', '').
card_image_name('glasses of urza'/'CEI', 'glasses of urza').
card_uid('glasses of urza'/'CEI', 'CEI:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'CEI', 'Uncommon').
card_artist('glasses of urza'/'CEI', 'Douglas Shuler').

card_in_set('gloom', 'CEI').
card_original_type('gloom'/'CEI', 'Enchantment').
card_original_text('gloom'/'CEI', '').
card_image_name('gloom'/'CEI', 'gloom').
card_uid('gloom'/'CEI', 'CEI:Gloom:gloom').
card_rarity('gloom'/'CEI', 'Uncommon').
card_artist('gloom'/'CEI', 'Dan Frazier').

card_in_set('goblin balloon brigade', 'CEI').
card_original_type('goblin balloon brigade'/'CEI', 'Creature — Goblin Warrior').
card_original_text('goblin balloon brigade'/'CEI', '').
card_image_name('goblin balloon brigade'/'CEI', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'CEI', 'CEI:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'CEI', 'Uncommon').
card_artist('goblin balloon brigade'/'CEI', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'CEI', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').

card_in_set('goblin king', 'CEI').
card_original_type('goblin king'/'CEI', 'Creature — Goblin').
card_original_text('goblin king'/'CEI', '').
card_image_name('goblin king'/'CEI', 'goblin king').
card_uid('goblin king'/'CEI', 'CEI:Goblin King:goblin king').
card_rarity('goblin king'/'CEI', 'Rare').
card_artist('goblin king'/'CEI', 'Jesper Myrfors').
card_flavor_text('goblin king'/'CEI', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').

card_in_set('granite gargoyle', 'CEI').
card_original_type('granite gargoyle'/'CEI', 'Creature — Gargoyle').
card_original_text('granite gargoyle'/'CEI', '').
card_image_name('granite gargoyle'/'CEI', 'granite gargoyle').
card_uid('granite gargoyle'/'CEI', 'CEI:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'CEI', 'Rare').
card_artist('granite gargoyle'/'CEI', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'CEI', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook by Asmoranomardicadaistinaculdacar').

card_in_set('gray ogre', 'CEI').
card_original_type('gray ogre'/'CEI', 'Creature — Ogre').
card_original_text('gray ogre'/'CEI', '').
card_image_name('gray ogre'/'CEI', 'gray ogre').
card_uid('gray ogre'/'CEI', 'CEI:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'CEI', 'Common').
card_artist('gray ogre'/'CEI', 'Dan Frazier').
card_flavor_text('gray ogre'/'CEI', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').

card_in_set('green ward', 'CEI').
card_original_type('green ward'/'CEI', 'Enchantment — Aura').
card_original_text('green ward'/'CEI', '').
card_image_name('green ward'/'CEI', 'green ward').
card_uid('green ward'/'CEI', 'CEI:Green Ward:green ward').
card_rarity('green ward'/'CEI', 'Uncommon').
card_artist('green ward'/'CEI', 'Dan Frazier').

card_in_set('grizzly bears', 'CEI').
card_original_type('grizzly bears'/'CEI', 'Creature — Bear').
card_original_text('grizzly bears'/'CEI', '').
card_image_name('grizzly bears'/'CEI', 'grizzly bears').
card_uid('grizzly bears'/'CEI', 'CEI:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'CEI', 'Common').
card_artist('grizzly bears'/'CEI', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'CEI', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').

card_in_set('guardian angel', 'CEI').
card_original_type('guardian angel'/'CEI', 'Instant').
card_original_text('guardian angel'/'CEI', '').
card_image_name('guardian angel'/'CEI', 'guardian angel').
card_uid('guardian angel'/'CEI', 'CEI:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'CEI', 'Common').
card_artist('guardian angel'/'CEI', 'Anson Maddocks').

card_in_set('healing salve', 'CEI').
card_original_type('healing salve'/'CEI', 'Instant').
card_original_text('healing salve'/'CEI', '').
card_image_name('healing salve'/'CEI', 'healing salve').
card_uid('healing salve'/'CEI', 'CEI:Healing Salve:healing salve').
card_rarity('healing salve'/'CEI', 'Common').
card_artist('healing salve'/'CEI', 'Dan Frazier').

card_in_set('helm of chatzuk', 'CEI').
card_original_type('helm of chatzuk'/'CEI', 'Artifact').
card_original_text('helm of chatzuk'/'CEI', '').
card_image_name('helm of chatzuk'/'CEI', 'helm of chatzuk').
card_uid('helm of chatzuk'/'CEI', 'CEI:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'CEI', 'Rare').
card_artist('helm of chatzuk'/'CEI', 'Mark Tedin').

card_in_set('hill giant', 'CEI').
card_original_type('hill giant'/'CEI', 'Creature — Giant').
card_original_text('hill giant'/'CEI', '').
card_image_name('hill giant'/'CEI', 'hill giant').
card_uid('hill giant'/'CEI', 'CEI:Hill Giant:hill giant').
card_rarity('hill giant'/'CEI', 'Common').
card_artist('hill giant'/'CEI', 'Dan Frazier').
card_flavor_text('hill giant'/'CEI', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').

card_in_set('holy armor', 'CEI').
card_original_type('holy armor'/'CEI', 'Enchantment — Aura').
card_original_text('holy armor'/'CEI', '').
card_image_name('holy armor'/'CEI', 'holy armor').
card_uid('holy armor'/'CEI', 'CEI:Holy Armor:holy armor').
card_rarity('holy armor'/'CEI', 'Common').
card_artist('holy armor'/'CEI', 'Melissa A. Benson').

card_in_set('holy strength', 'CEI').
card_original_type('holy strength'/'CEI', 'Enchantment — Aura').
card_original_text('holy strength'/'CEI', '').
card_image_name('holy strength'/'CEI', 'holy strength').
card_uid('holy strength'/'CEI', 'CEI:Holy Strength:holy strength').
card_rarity('holy strength'/'CEI', 'Common').
card_artist('holy strength'/'CEI', 'Anson Maddocks').

card_in_set('howl from beyond', 'CEI').
card_original_type('howl from beyond'/'CEI', 'Instant').
card_original_text('howl from beyond'/'CEI', '').
card_image_name('howl from beyond'/'CEI', 'howl from beyond').
card_uid('howl from beyond'/'CEI', 'CEI:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'CEI', 'Common').
card_artist('howl from beyond'/'CEI', 'Mark Poole').

card_in_set('howling mine', 'CEI').
card_original_type('howling mine'/'CEI', 'Artifact').
card_original_text('howling mine'/'CEI', '').
card_image_name('howling mine'/'CEI', 'howling mine').
card_uid('howling mine'/'CEI', 'CEI:Howling Mine:howling mine').
card_rarity('howling mine'/'CEI', 'Rare').
card_artist('howling mine'/'CEI', 'Mark Poole').

card_in_set('hurloon minotaur', 'CEI').
card_original_type('hurloon minotaur'/'CEI', 'Creature — Minotaur').
card_original_text('hurloon minotaur'/'CEI', '').
card_image_name('hurloon minotaur'/'CEI', 'hurloon minotaur').
card_uid('hurloon minotaur'/'CEI', 'CEI:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'CEI', 'Common').
card_artist('hurloon minotaur'/'CEI', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'CEI', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').

card_in_set('hurricane', 'CEI').
card_original_type('hurricane'/'CEI', 'Sorcery').
card_original_text('hurricane'/'CEI', '').
card_image_name('hurricane'/'CEI', 'hurricane').
card_uid('hurricane'/'CEI', 'CEI:Hurricane:hurricane').
card_rarity('hurricane'/'CEI', 'Uncommon').
card_artist('hurricane'/'CEI', 'Dameon Willich').

card_in_set('hypnotic specter', 'CEI').
card_original_type('hypnotic specter'/'CEI', 'Creature — Specter').
card_original_text('hypnotic specter'/'CEI', '').
card_image_name('hypnotic specter'/'CEI', 'hypnotic specter').
card_uid('hypnotic specter'/'CEI', 'CEI:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'CEI', 'Uncommon').
card_artist('hypnotic specter'/'CEI', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'CEI', '\"...There was no trace/ Of aught on that illumined face...\"\n—Samuel Coleridge, \"Phantom\"').

card_in_set('ice storm', 'CEI').
card_original_type('ice storm'/'CEI', 'Sorcery').
card_original_text('ice storm'/'CEI', '').
card_image_name('ice storm'/'CEI', 'ice storm').
card_uid('ice storm'/'CEI', 'CEI:Ice Storm:ice storm').
card_rarity('ice storm'/'CEI', 'Uncommon').
card_artist('ice storm'/'CEI', 'Dan Frazier').

card_in_set('icy manipulator', 'CEI').
card_original_type('icy manipulator'/'CEI', 'Artifact').
card_original_text('icy manipulator'/'CEI', '').
card_image_name('icy manipulator'/'CEI', 'icy manipulator').
card_uid('icy manipulator'/'CEI', 'CEI:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'CEI', 'Uncommon').
card_artist('icy manipulator'/'CEI', 'Douglas Shuler').

card_in_set('illusionary mask', 'CEI').
card_original_type('illusionary mask'/'CEI', 'Artifact').
card_original_text('illusionary mask'/'CEI', '').
card_image_name('illusionary mask'/'CEI', 'illusionary mask').
card_uid('illusionary mask'/'CEI', 'CEI:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'CEI', 'Rare').
card_artist('illusionary mask'/'CEI', 'Amy Weber').

card_in_set('instill energy', 'CEI').
card_original_type('instill energy'/'CEI', 'Enchantment — Aura').
card_original_text('instill energy'/'CEI', '').
card_image_name('instill energy'/'CEI', 'instill energy').
card_uid('instill energy'/'CEI', 'CEI:Instill Energy:instill energy').
card_rarity('instill energy'/'CEI', 'Uncommon').
card_artist('instill energy'/'CEI', 'Dameon Willich').

card_in_set('invisibility', 'CEI').
card_original_type('invisibility'/'CEI', 'Enchantment — Aura').
card_original_text('invisibility'/'CEI', '').
card_image_name('invisibility'/'CEI', 'invisibility').
card_uid('invisibility'/'CEI', 'CEI:Invisibility:invisibility').
card_rarity('invisibility'/'CEI', 'Common').
card_artist('invisibility'/'CEI', 'Anson Maddocks').

card_in_set('iron star', 'CEI').
card_original_type('iron star'/'CEI', 'Artifact').
card_original_text('iron star'/'CEI', '').
card_image_name('iron star'/'CEI', 'iron star').
card_uid('iron star'/'CEI', 'CEI:Iron Star:iron star').
card_rarity('iron star'/'CEI', 'Uncommon').
card_artist('iron star'/'CEI', 'Dan Frazier').

card_in_set('ironclaw orcs', 'CEI').
card_original_type('ironclaw orcs'/'CEI', 'Creature — Orc').
card_original_text('ironclaw orcs'/'CEI', '').
card_image_name('ironclaw orcs'/'CEI', 'ironclaw orcs').
card_uid('ironclaw orcs'/'CEI', 'CEI:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'CEI', 'Common').
card_artist('ironclaw orcs'/'CEI', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'CEI', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').

card_in_set('ironroot treefolk', 'CEI').
card_original_type('ironroot treefolk'/'CEI', 'Creature — Treefolk').
card_original_text('ironroot treefolk'/'CEI', '').
card_image_name('ironroot treefolk'/'CEI', 'ironroot treefolk').
card_uid('ironroot treefolk'/'CEI', 'CEI:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'CEI', 'Common').
card_artist('ironroot treefolk'/'CEI', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'CEI', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').

card_in_set('island', 'CEI').
card_original_type('island'/'CEI', 'Basic Land — Island').
card_original_text('island'/'CEI', '').
card_image_name('island'/'CEI', 'island1').
card_uid('island'/'CEI', 'CEI:Island:island1').
card_rarity('island'/'CEI', 'Basic Land').
card_artist('island'/'CEI', 'Mark Poole').

card_in_set('island', 'CEI').
card_original_type('island'/'CEI', 'Basic Land — Island').
card_original_text('island'/'CEI', '').
card_image_name('island'/'CEI', 'island2').
card_uid('island'/'CEI', 'CEI:Island:island2').
card_rarity('island'/'CEI', 'Basic Land').
card_artist('island'/'CEI', 'Mark Poole').

card_in_set('island', 'CEI').
card_original_type('island'/'CEI', 'Basic Land — Island').
card_original_text('island'/'CEI', '').
card_image_name('island'/'CEI', 'island3').
card_uid('island'/'CEI', 'CEI:Island:island3').
card_rarity('island'/'CEI', 'Basic Land').
card_artist('island'/'CEI', 'Mark Poole').

card_in_set('island sanctuary', 'CEI').
card_original_type('island sanctuary'/'CEI', 'Enchantment').
card_original_text('island sanctuary'/'CEI', '').
card_image_name('island sanctuary'/'CEI', 'island sanctuary').
card_uid('island sanctuary'/'CEI', 'CEI:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'CEI', 'Rare').
card_artist('island sanctuary'/'CEI', 'Mark Poole').

card_in_set('ivory cup', 'CEI').
card_original_type('ivory cup'/'CEI', 'Artifact').
card_original_text('ivory cup'/'CEI', '').
card_image_name('ivory cup'/'CEI', 'ivory cup').
card_uid('ivory cup'/'CEI', 'CEI:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'CEI', 'Uncommon').
card_artist('ivory cup'/'CEI', 'Anson Maddocks').

card_in_set('jade monolith', 'CEI').
card_original_type('jade monolith'/'CEI', 'Artifact').
card_original_text('jade monolith'/'CEI', '').
card_image_name('jade monolith'/'CEI', 'jade monolith').
card_uid('jade monolith'/'CEI', 'CEI:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'CEI', 'Rare').
card_artist('jade monolith'/'CEI', 'Anson Maddocks').

card_in_set('jade statue', 'CEI').
card_original_type('jade statue'/'CEI', 'Artifact').
card_original_text('jade statue'/'CEI', '').
card_image_name('jade statue'/'CEI', 'jade statue').
card_uid('jade statue'/'CEI', 'CEI:Jade Statue:jade statue').
card_rarity('jade statue'/'CEI', 'Uncommon').
card_artist('jade statue'/'CEI', 'Dan Frazier').
card_flavor_text('jade statue'/'CEI', '\"Some of the other guys dared me to touch it, but I knew it weren\'t no ordinary hunk o\' rock.\"\n—Norin the Wary').

card_in_set('jayemdae tome', 'CEI').
card_original_type('jayemdae tome'/'CEI', 'Artifact').
card_original_text('jayemdae tome'/'CEI', '').
card_image_name('jayemdae tome'/'CEI', 'jayemdae tome').
card_uid('jayemdae tome'/'CEI', 'CEI:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'CEI', 'Rare').
card_artist('jayemdae tome'/'CEI', 'Mark Tedin').

card_in_set('juggernaut', 'CEI').
card_original_type('juggernaut'/'CEI', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'CEI', '').
card_image_name('juggernaut'/'CEI', 'juggernaut').
card_uid('juggernaut'/'CEI', 'CEI:Juggernaut:juggernaut').
card_rarity('juggernaut'/'CEI', 'Uncommon').
card_artist('juggernaut'/'CEI', 'Dan Frazier').
card_flavor_text('juggernaut'/'CEI', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').

card_in_set('jump', 'CEI').
card_original_type('jump'/'CEI', 'Instant').
card_original_text('jump'/'CEI', '').
card_image_name('jump'/'CEI', 'jump').
card_uid('jump'/'CEI', 'CEI:Jump:jump').
card_rarity('jump'/'CEI', 'Common').
card_artist('jump'/'CEI', 'Mark Poole').

card_in_set('karma', 'CEI').
card_original_type('karma'/'CEI', 'Enchantment').
card_original_text('karma'/'CEI', '').
card_image_name('karma'/'CEI', 'karma').
card_uid('karma'/'CEI', 'CEI:Karma:karma').
card_rarity('karma'/'CEI', 'Uncommon').
card_artist('karma'/'CEI', 'Richard Thomas').

card_in_set('keldon warlord', 'CEI').
card_original_type('keldon warlord'/'CEI', 'Creature — Human Barbarian').
card_original_text('keldon warlord'/'CEI', '').
card_image_name('keldon warlord'/'CEI', 'keldon warlord').
card_uid('keldon warlord'/'CEI', 'CEI:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'CEI', 'Uncommon').
card_artist('keldon warlord'/'CEI', 'Kev Brockschmidt').

card_in_set('kormus bell', 'CEI').
card_original_type('kormus bell'/'CEI', 'Artifact').
card_original_text('kormus bell'/'CEI', '').
card_image_name('kormus bell'/'CEI', 'kormus bell').
card_uid('kormus bell'/'CEI', 'CEI:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'CEI', 'Rare').
card_artist('kormus bell'/'CEI', 'Christopher Rush').

card_in_set('kudzu', 'CEI').
card_original_type('kudzu'/'CEI', 'Enchantment — Aura').
card_original_text('kudzu'/'CEI', '').
card_image_name('kudzu'/'CEI', 'kudzu').
card_uid('kudzu'/'CEI', 'CEI:Kudzu:kudzu').
card_rarity('kudzu'/'CEI', 'Rare').
card_artist('kudzu'/'CEI', 'Mark Poole').

card_in_set('lance', 'CEI').
card_original_type('lance'/'CEI', 'Enchantment — Aura').
card_original_text('lance'/'CEI', '').
card_image_name('lance'/'CEI', 'lance').
card_uid('lance'/'CEI', 'CEI:Lance:lance').
card_rarity('lance'/'CEI', 'Uncommon').
card_artist('lance'/'CEI', 'Rob Alexander').

card_in_set('ley druid', 'CEI').
card_original_type('ley druid'/'CEI', 'Creature — Human Druid').
card_original_text('ley druid'/'CEI', '').
card_image_name('ley druid'/'CEI', 'ley druid').
card_uid('ley druid'/'CEI', 'CEI:Ley Druid:ley druid').
card_rarity('ley druid'/'CEI', 'Uncommon').
card_artist('ley druid'/'CEI', 'Sandra Everingham').
card_flavor_text('ley druid'/'CEI', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').

card_in_set('library of leng', 'CEI').
card_original_type('library of leng'/'CEI', 'Artifact').
card_original_text('library of leng'/'CEI', '').
card_image_name('library of leng'/'CEI', 'library of leng').
card_uid('library of leng'/'CEI', 'CEI:Library of Leng:library of leng').
card_rarity('library of leng'/'CEI', 'Uncommon').
card_artist('library of leng'/'CEI', 'Daniel Gelon').

card_in_set('lich', 'CEI').
card_original_type('lich'/'CEI', 'Enchantment').
card_original_text('lich'/'CEI', '').
card_image_name('lich'/'CEI', 'lich').
card_uid('lich'/'CEI', 'CEI:Lich:lich').
card_rarity('lich'/'CEI', 'Rare').
card_artist('lich'/'CEI', 'Daniel Gelon').

card_in_set('lifeforce', 'CEI').
card_original_type('lifeforce'/'CEI', 'Enchantment').
card_original_text('lifeforce'/'CEI', '').
card_image_name('lifeforce'/'CEI', 'lifeforce').
card_uid('lifeforce'/'CEI', 'CEI:Lifeforce:lifeforce').
card_rarity('lifeforce'/'CEI', 'Uncommon').
card_artist('lifeforce'/'CEI', 'Dameon Willich').

card_in_set('lifelace', 'CEI').
card_original_type('lifelace'/'CEI', 'Instant').
card_original_text('lifelace'/'CEI', '').
card_image_name('lifelace'/'CEI', 'lifelace').
card_uid('lifelace'/'CEI', 'CEI:Lifelace:lifelace').
card_rarity('lifelace'/'CEI', 'Rare').
card_artist('lifelace'/'CEI', 'Amy Weber').

card_in_set('lifetap', 'CEI').
card_original_type('lifetap'/'CEI', 'Enchantment').
card_original_text('lifetap'/'CEI', '').
card_image_name('lifetap'/'CEI', 'lifetap').
card_uid('lifetap'/'CEI', 'CEI:Lifetap:lifetap').
card_rarity('lifetap'/'CEI', 'Uncommon').
card_artist('lifetap'/'CEI', 'Anson Maddocks').

card_in_set('lightning bolt', 'CEI').
card_original_type('lightning bolt'/'CEI', 'Instant').
card_original_text('lightning bolt'/'CEI', '').
card_image_name('lightning bolt'/'CEI', 'lightning bolt').
card_uid('lightning bolt'/'CEI', 'CEI:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'CEI', 'Common').
card_artist('lightning bolt'/'CEI', 'Christopher Rush').

card_in_set('living artifact', 'CEI').
card_original_type('living artifact'/'CEI', 'Enchantment — Aura').
card_original_text('living artifact'/'CEI', '').
card_image_name('living artifact'/'CEI', 'living artifact').
card_uid('living artifact'/'CEI', 'CEI:Living Artifact:living artifact').
card_rarity('living artifact'/'CEI', 'Rare').
card_artist('living artifact'/'CEI', 'Anson Maddocks').

card_in_set('living lands', 'CEI').
card_original_type('living lands'/'CEI', 'Enchantment').
card_original_text('living lands'/'CEI', '').
card_image_name('living lands'/'CEI', 'living lands').
card_uid('living lands'/'CEI', 'CEI:Living Lands:living lands').
card_rarity('living lands'/'CEI', 'Rare').
card_artist('living lands'/'CEI', 'Jesper Myrfors').

card_in_set('living wall', 'CEI').
card_original_type('living wall'/'CEI', 'Artifact Creature — Wall').
card_original_text('living wall'/'CEI', '').
card_image_name('living wall'/'CEI', 'living wall').
card_uid('living wall'/'CEI', 'CEI:Living Wall:living wall').
card_rarity('living wall'/'CEI', 'Uncommon').
card_artist('living wall'/'CEI', 'Anson Maddocks').
card_flavor_text('living wall'/'CEI', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').

card_in_set('llanowar elves', 'CEI').
card_original_type('llanowar elves'/'CEI', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'CEI', '').
card_image_name('llanowar elves'/'CEI', 'llanowar elves').
card_uid('llanowar elves'/'CEI', 'CEI:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'CEI', 'Common').
card_artist('llanowar elves'/'CEI', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'CEI', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').

card_in_set('lord of atlantis', 'CEI').
card_original_type('lord of atlantis'/'CEI', 'Creature — Merfolk').
card_original_text('lord of atlantis'/'CEI', '').
card_image_name('lord of atlantis'/'CEI', 'lord of atlantis').
card_uid('lord of atlantis'/'CEI', 'CEI:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'CEI', 'Rare').
card_artist('lord of atlantis'/'CEI', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'CEI', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').

card_in_set('lord of the pit', 'CEI').
card_original_type('lord of the pit'/'CEI', 'Creature — Demon').
card_original_text('lord of the pit'/'CEI', '').
card_image_name('lord of the pit'/'CEI', 'lord of the pit').
card_uid('lord of the pit'/'CEI', 'CEI:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'CEI', 'Rare').
card_artist('lord of the pit'/'CEI', 'Mark Tedin').

card_in_set('lure', 'CEI').
card_original_type('lure'/'CEI', 'Enchantment — Aura').
card_original_text('lure'/'CEI', '').
card_image_name('lure'/'CEI', 'lure').
card_uid('lure'/'CEI', 'CEI:Lure:lure').
card_rarity('lure'/'CEI', 'Uncommon').
card_artist('lure'/'CEI', 'Anson Maddocks').

card_in_set('magical hack', 'CEI').
card_original_type('magical hack'/'CEI', 'Instant').
card_original_text('magical hack'/'CEI', '').
card_image_name('magical hack'/'CEI', 'magical hack').
card_uid('magical hack'/'CEI', 'CEI:Magical Hack:magical hack').
card_rarity('magical hack'/'CEI', 'Rare').
card_artist('magical hack'/'CEI', 'Julie Baroh').

card_in_set('mahamoti djinn', 'CEI').
card_original_type('mahamoti djinn'/'CEI', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'CEI', '').
card_image_name('mahamoti djinn'/'CEI', 'mahamoti djinn').
card_uid('mahamoti djinn'/'CEI', 'CEI:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'CEI', 'Rare').
card_artist('mahamoti djinn'/'CEI', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'CEI', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').

card_in_set('mana flare', 'CEI').
card_original_type('mana flare'/'CEI', 'Enchantment').
card_original_text('mana flare'/'CEI', '').
card_image_name('mana flare'/'CEI', 'mana flare').
card_uid('mana flare'/'CEI', 'CEI:Mana Flare:mana flare').
card_rarity('mana flare'/'CEI', 'Rare').
card_artist('mana flare'/'CEI', 'Christopher Rush').

card_in_set('mana short', 'CEI').
card_original_type('mana short'/'CEI', 'Instant').
card_original_text('mana short'/'CEI', '').
card_image_name('mana short'/'CEI', 'mana short').
card_uid('mana short'/'CEI', 'CEI:Mana Short:mana short').
card_rarity('mana short'/'CEI', 'Rare').
card_artist('mana short'/'CEI', 'Dameon Willich').

card_in_set('mana vault', 'CEI').
card_original_type('mana vault'/'CEI', 'Artifact').
card_original_text('mana vault'/'CEI', '').
card_image_name('mana vault'/'CEI', 'mana vault').
card_uid('mana vault'/'CEI', 'CEI:Mana Vault:mana vault').
card_rarity('mana vault'/'CEI', 'Rare').
card_artist('mana vault'/'CEI', 'Mark Tedin').

card_in_set('manabarbs', 'CEI').
card_original_type('manabarbs'/'CEI', 'Enchantment').
card_original_text('manabarbs'/'CEI', '').
card_image_name('manabarbs'/'CEI', 'manabarbs').
card_uid('manabarbs'/'CEI', 'CEI:Manabarbs:manabarbs').
card_rarity('manabarbs'/'CEI', 'Rare').
card_artist('manabarbs'/'CEI', 'Christopher Rush').

card_in_set('meekstone', 'CEI').
card_original_type('meekstone'/'CEI', 'Artifact').
card_original_text('meekstone'/'CEI', '').
card_image_name('meekstone'/'CEI', 'meekstone').
card_uid('meekstone'/'CEI', 'CEI:Meekstone:meekstone').
card_rarity('meekstone'/'CEI', 'Rare').
card_artist('meekstone'/'CEI', 'Quinton Hoover').

card_in_set('merfolk of the pearl trident', 'CEI').
card_original_type('merfolk of the pearl trident'/'CEI', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'CEI', '').
card_image_name('merfolk of the pearl trident'/'CEI', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'CEI', 'CEI:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'CEI', 'Common').
card_artist('merfolk of the pearl trident'/'CEI', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'CEI', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').

card_in_set('mesa pegasus', 'CEI').
card_original_type('mesa pegasus'/'CEI', 'Creature — Pegasus').
card_original_text('mesa pegasus'/'CEI', '').
card_image_name('mesa pegasus'/'CEI', 'mesa pegasus').
card_uid('mesa pegasus'/'CEI', 'CEI:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'CEI', 'Common').
card_artist('mesa pegasus'/'CEI', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'CEI', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').

card_in_set('mind twist', 'CEI').
card_original_type('mind twist'/'CEI', 'Sorcery').
card_original_text('mind twist'/'CEI', '').
card_image_name('mind twist'/'CEI', 'mind twist').
card_uid('mind twist'/'CEI', 'CEI:Mind Twist:mind twist').
card_rarity('mind twist'/'CEI', 'Rare').
card_artist('mind twist'/'CEI', 'Julie Baroh').

card_in_set('mons\'s goblin raiders', 'CEI').
card_original_type('mons\'s goblin raiders'/'CEI', 'Creature — Goblin').
card_original_text('mons\'s goblin raiders'/'CEI', '').
card_image_name('mons\'s goblin raiders'/'CEI', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'CEI', 'CEI:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'CEI', 'Common').
card_artist('mons\'s goblin raiders'/'CEI', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'CEI', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').

card_in_set('mountain', 'CEI').
card_original_type('mountain'/'CEI', 'Basic Land — Mountain').
card_original_text('mountain'/'CEI', '').
card_image_name('mountain'/'CEI', 'mountain1').
card_uid('mountain'/'CEI', 'CEI:Mountain:mountain1').
card_rarity('mountain'/'CEI', 'Basic Land').
card_artist('mountain'/'CEI', 'Douglas Shuler').

card_in_set('mountain', 'CEI').
card_original_type('mountain'/'CEI', 'Basic Land — Mountain').
card_original_text('mountain'/'CEI', '').
card_image_name('mountain'/'CEI', 'mountain2').
card_uid('mountain'/'CEI', 'CEI:Mountain:mountain2').
card_rarity('mountain'/'CEI', 'Basic Land').
card_artist('mountain'/'CEI', 'Douglas Shuler').

card_in_set('mountain', 'CEI').
card_original_type('mountain'/'CEI', 'Basic Land — Mountain').
card_original_text('mountain'/'CEI', '').
card_image_name('mountain'/'CEI', 'mountain3').
card_uid('mountain'/'CEI', 'CEI:Mountain:mountain3').
card_rarity('mountain'/'CEI', 'Basic Land').
card_artist('mountain'/'CEI', 'Douglas Shuler').

card_in_set('mox emerald', 'CEI').
card_original_type('mox emerald'/'CEI', 'Artifact').
card_original_text('mox emerald'/'CEI', '').
card_image_name('mox emerald'/'CEI', 'mox emerald').
card_uid('mox emerald'/'CEI', 'CEI:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'CEI', 'Rare').
card_artist('mox emerald'/'CEI', 'Dan Frazier').

card_in_set('mox jet', 'CEI').
card_original_type('mox jet'/'CEI', 'Artifact').
card_original_text('mox jet'/'CEI', '').
card_image_name('mox jet'/'CEI', 'mox jet').
card_uid('mox jet'/'CEI', 'CEI:Mox Jet:mox jet').
card_rarity('mox jet'/'CEI', 'Rare').
card_artist('mox jet'/'CEI', 'Dan Frazier').

card_in_set('mox pearl', 'CEI').
card_original_type('mox pearl'/'CEI', 'Artifact').
card_original_text('mox pearl'/'CEI', '').
card_image_name('mox pearl'/'CEI', 'mox pearl').
card_uid('mox pearl'/'CEI', 'CEI:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'CEI', 'Rare').
card_artist('mox pearl'/'CEI', 'Dan Frazier').

card_in_set('mox ruby', 'CEI').
card_original_type('mox ruby'/'CEI', 'Artifact').
card_original_text('mox ruby'/'CEI', '').
card_image_name('mox ruby'/'CEI', 'mox ruby').
card_uid('mox ruby'/'CEI', 'CEI:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'CEI', 'Rare').
card_artist('mox ruby'/'CEI', 'Dan Frazier').

card_in_set('mox sapphire', 'CEI').
card_original_type('mox sapphire'/'CEI', 'Artifact').
card_original_text('mox sapphire'/'CEI', '').
card_image_name('mox sapphire'/'CEI', 'mox sapphire').
card_uid('mox sapphire'/'CEI', 'CEI:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'CEI', 'Rare').
card_artist('mox sapphire'/'CEI', 'Dan Frazier').

card_in_set('natural selection', 'CEI').
card_original_type('natural selection'/'CEI', 'Instant').
card_original_text('natural selection'/'CEI', '').
card_image_name('natural selection'/'CEI', 'natural selection').
card_uid('natural selection'/'CEI', 'CEI:Natural Selection:natural selection').
card_rarity('natural selection'/'CEI', 'Rare').
card_artist('natural selection'/'CEI', 'Mark Poole').

card_in_set('nether shadow', 'CEI').
card_original_type('nether shadow'/'CEI', 'Creature — Spirit').
card_original_text('nether shadow'/'CEI', '').
card_image_name('nether shadow'/'CEI', 'nether shadow').
card_uid('nether shadow'/'CEI', 'CEI:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'CEI', 'Rare').
card_artist('nether shadow'/'CEI', 'Christopher Rush').

card_in_set('nettling imp', 'CEI').
card_original_type('nettling imp'/'CEI', 'Creature — Imp').
card_original_text('nettling imp'/'CEI', '').
card_image_name('nettling imp'/'CEI', 'nettling imp').
card_uid('nettling imp'/'CEI', 'CEI:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'CEI', 'Uncommon').
card_artist('nettling imp'/'CEI', 'Quinton Hoover').

card_in_set('nevinyrral\'s disk', 'CEI').
card_original_type('nevinyrral\'s disk'/'CEI', 'Artifact').
card_original_text('nevinyrral\'s disk'/'CEI', '').
card_image_name('nevinyrral\'s disk'/'CEI', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'CEI', 'CEI:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'CEI', 'Rare').
card_artist('nevinyrral\'s disk'/'CEI', 'Mark Tedin').

card_in_set('nightmare', 'CEI').
card_original_type('nightmare'/'CEI', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'CEI', '').
card_image_name('nightmare'/'CEI', 'nightmare').
card_uid('nightmare'/'CEI', 'CEI:Nightmare:nightmare').
card_rarity('nightmare'/'CEI', 'Rare').
card_artist('nightmare'/'CEI', 'Melissa A. Benson').
card_flavor_text('nightmare'/'CEI', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').

card_in_set('northern paladin', 'CEI').
card_original_type('northern paladin'/'CEI', 'Creature — Human Knight').
card_original_text('northern paladin'/'CEI', '').
card_image_name('northern paladin'/'CEI', 'northern paladin').
card_uid('northern paladin'/'CEI', 'CEI:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'CEI', 'Rare').
card_artist('northern paladin'/'CEI', 'Douglas Shuler').
card_flavor_text('northern paladin'/'CEI', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').

card_in_set('obsianus golem', 'CEI').
card_original_type('obsianus golem'/'CEI', 'Artifact Creature — Golem').
card_original_text('obsianus golem'/'CEI', '').
card_image_name('obsianus golem'/'CEI', 'obsianus golem').
card_uid('obsianus golem'/'CEI', 'CEI:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'CEI', 'Uncommon').
card_artist('obsianus golem'/'CEI', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'CEI', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . .\"\n—Song of the Artificer').

card_in_set('orcish artillery', 'CEI').
card_original_type('orcish artillery'/'CEI', 'Creature — Orc Warrior').
card_original_text('orcish artillery'/'CEI', '').
card_image_name('orcish artillery'/'CEI', 'orcish artillery').
card_uid('orcish artillery'/'CEI', 'CEI:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'CEI', 'Uncommon').
card_artist('orcish artillery'/'CEI', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'CEI', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').

card_in_set('orcish oriflamme', 'CEI').
card_original_type('orcish oriflamme'/'CEI', 'Enchantment').
card_original_text('orcish oriflamme'/'CEI', '').
card_image_name('orcish oriflamme'/'CEI', 'orcish oriflamme').
card_uid('orcish oriflamme'/'CEI', 'CEI:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'CEI', 'Uncommon').
card_artist('orcish oriflamme'/'CEI', 'Dan Frazier').

card_in_set('paralyze', 'CEI').
card_original_type('paralyze'/'CEI', 'Enchantment — Aura').
card_original_text('paralyze'/'CEI', '').
card_image_name('paralyze'/'CEI', 'paralyze').
card_uid('paralyze'/'CEI', 'CEI:Paralyze:paralyze').
card_rarity('paralyze'/'CEI', 'Common').
card_artist('paralyze'/'CEI', 'Anson Maddocks').

card_in_set('pearled unicorn', 'CEI').
card_original_type('pearled unicorn'/'CEI', 'Creature — Unicorn').
card_original_text('pearled unicorn'/'CEI', '').
card_image_name('pearled unicorn'/'CEI', 'pearled unicorn').
card_uid('pearled unicorn'/'CEI', 'CEI:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'CEI', 'Common').
card_artist('pearled unicorn'/'CEI', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'CEI', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\"\n—Lewis Carroll').

card_in_set('personal incarnation', 'CEI').
card_original_type('personal incarnation'/'CEI', 'Creature — Avatar Incarnation').
card_original_text('personal incarnation'/'CEI', '').
card_image_name('personal incarnation'/'CEI', 'personal incarnation').
card_uid('personal incarnation'/'CEI', 'CEI:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'CEI', 'Rare').
card_artist('personal incarnation'/'CEI', 'Kev Brockschmidt').

card_in_set('pestilence', 'CEI').
card_original_type('pestilence'/'CEI', 'Enchantment').
card_original_text('pestilence'/'CEI', '').
card_image_name('pestilence'/'CEI', 'pestilence').
card_uid('pestilence'/'CEI', 'CEI:Pestilence:pestilence').
card_rarity('pestilence'/'CEI', 'Common').
card_artist('pestilence'/'CEI', 'Jesper Myrfors').

card_in_set('phantasmal forces', 'CEI').
card_original_type('phantasmal forces'/'CEI', 'Creature — Illusion').
card_original_text('phantasmal forces'/'CEI', '').
card_image_name('phantasmal forces'/'CEI', 'phantasmal forces').
card_uid('phantasmal forces'/'CEI', 'CEI:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'CEI', 'Uncommon').
card_artist('phantasmal forces'/'CEI', 'Mark Poole').
card_flavor_text('phantasmal forces'/'CEI', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').

card_in_set('phantasmal terrain', 'CEI').
card_original_type('phantasmal terrain'/'CEI', 'Enchantment — Aura').
card_original_text('phantasmal terrain'/'CEI', '').
card_image_name('phantasmal terrain'/'CEI', 'phantasmal terrain').
card_uid('phantasmal terrain'/'CEI', 'CEI:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'CEI', 'Common').
card_artist('phantasmal terrain'/'CEI', 'Dameon Willich').

card_in_set('phantom monster', 'CEI').
card_original_type('phantom monster'/'CEI', 'Creature — Illusion').
card_original_text('phantom monster'/'CEI', '').
card_image_name('phantom monster'/'CEI', 'phantom monster').
card_uid('phantom monster'/'CEI', 'CEI:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'CEI', 'Uncommon').
card_artist('phantom monster'/'CEI', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'CEI', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').

card_in_set('pirate ship', 'CEI').
card_original_type('pirate ship'/'CEI', 'Creature — Human Pirate').
card_original_text('pirate ship'/'CEI', '').
card_image_name('pirate ship'/'CEI', 'pirate ship').
card_uid('pirate ship'/'CEI', 'CEI:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'CEI', 'Rare').
card_artist('pirate ship'/'CEI', 'Tom Wänerstrand').

card_in_set('plague rats', 'CEI').
card_original_type('plague rats'/'CEI', 'Creature — Rat').
card_original_text('plague rats'/'CEI', '').
card_image_name('plague rats'/'CEI', 'plague rats').
card_uid('plague rats'/'CEI', 'CEI:Plague Rats:plague rats').
card_rarity('plague rats'/'CEI', 'Common').
card_artist('plague rats'/'CEI', 'Anson Maddocks').
card_flavor_text('plague rats'/'CEI', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you...\"\n—Samuel Coleridge, \"Recantation\"').

card_in_set('plains', 'CEI').
card_original_type('plains'/'CEI', 'Basic Land — Plains').
card_original_text('plains'/'CEI', '').
card_image_name('plains'/'CEI', 'plains1').
card_uid('plains'/'CEI', 'CEI:Plains:plains1').
card_rarity('plains'/'CEI', 'Basic Land').
card_artist('plains'/'CEI', 'Jesper Myrfors').

card_in_set('plains', 'CEI').
card_original_type('plains'/'CEI', 'Basic Land — Plains').
card_original_text('plains'/'CEI', '').
card_image_name('plains'/'CEI', 'plains2').
card_uid('plains'/'CEI', 'CEI:Plains:plains2').
card_rarity('plains'/'CEI', 'Basic Land').
card_artist('plains'/'CEI', 'Jesper Myrfors').

card_in_set('plains', 'CEI').
card_original_type('plains'/'CEI', 'Basic Land — Plains').
card_original_text('plains'/'CEI', '').
card_image_name('plains'/'CEI', 'plains3').
card_uid('plains'/'CEI', 'CEI:Plains:plains3').
card_rarity('plains'/'CEI', 'Basic Land').
card_artist('plains'/'CEI', 'Jesper Myrfors').

card_in_set('plateau', 'CEI').
card_original_type('plateau'/'CEI', 'Land — Mountain Plains').
card_original_text('plateau'/'CEI', '').
card_image_name('plateau'/'CEI', 'plateau').
card_uid('plateau'/'CEI', 'CEI:Plateau:plateau').
card_rarity('plateau'/'CEI', 'Rare').
card_artist('plateau'/'CEI', 'Drew Tucker').

card_in_set('power leak', 'CEI').
card_original_type('power leak'/'CEI', 'Enchantment — Aura').
card_original_text('power leak'/'CEI', '').
card_image_name('power leak'/'CEI', 'power leak').
card_uid('power leak'/'CEI', 'CEI:Power Leak:power leak').
card_rarity('power leak'/'CEI', 'Common').
card_artist('power leak'/'CEI', 'Drew Tucker').

card_in_set('power sink', 'CEI').
card_original_type('power sink'/'CEI', 'Instant').
card_original_text('power sink'/'CEI', '').
card_image_name('power sink'/'CEI', 'power sink').
card_uid('power sink'/'CEI', 'CEI:Power Sink:power sink').
card_rarity('power sink'/'CEI', 'Common').
card_artist('power sink'/'CEI', 'Richard Thomas').

card_in_set('power surge', 'CEI').
card_original_type('power surge'/'CEI', 'Enchantment').
card_original_text('power surge'/'CEI', '').
card_image_name('power surge'/'CEI', 'power surge').
card_uid('power surge'/'CEI', 'CEI:Power Surge:power surge').
card_rarity('power surge'/'CEI', 'Rare').
card_artist('power surge'/'CEI', 'Douglas Shuler').

card_in_set('prodigal sorcerer', 'CEI').
card_original_type('prodigal sorcerer'/'CEI', 'Creature — Human Wizard').
card_original_text('prodigal sorcerer'/'CEI', '').
card_image_name('prodigal sorcerer'/'CEI', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'CEI', 'CEI:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'CEI', 'Common').
card_artist('prodigal sorcerer'/'CEI', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'CEI', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').

card_in_set('psionic blast', 'CEI').
card_original_type('psionic blast'/'CEI', 'Instant').
card_original_text('psionic blast'/'CEI', '').
card_image_name('psionic blast'/'CEI', 'psionic blast').
card_uid('psionic blast'/'CEI', 'CEI:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'CEI', 'Uncommon').
card_artist('psionic blast'/'CEI', 'Douglas Shuler').

card_in_set('psychic venom', 'CEI').
card_original_type('psychic venom'/'CEI', 'Enchantment — Aura').
card_original_text('psychic venom'/'CEI', '').
card_image_name('psychic venom'/'CEI', 'psychic venom').
card_uid('psychic venom'/'CEI', 'CEI:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'CEI', 'Common').
card_artist('psychic venom'/'CEI', 'Brian Snõddy').

card_in_set('purelace', 'CEI').
card_original_type('purelace'/'CEI', 'Instant').
card_original_text('purelace'/'CEI', '').
card_image_name('purelace'/'CEI', 'purelace').
card_uid('purelace'/'CEI', 'CEI:Purelace:purelace').
card_rarity('purelace'/'CEI', 'Rare').
card_artist('purelace'/'CEI', 'Sandra Everingham').

card_in_set('raging river', 'CEI').
card_original_type('raging river'/'CEI', 'Enchantment').
card_original_text('raging river'/'CEI', '').
card_image_name('raging river'/'CEI', 'raging river').
card_uid('raging river'/'CEI', 'CEI:Raging River:raging river').
card_rarity('raging river'/'CEI', 'Rare').
card_artist('raging river'/'CEI', 'Sandra Everingham').

card_in_set('raise dead', 'CEI').
card_original_type('raise dead'/'CEI', 'Sorcery').
card_original_text('raise dead'/'CEI', '').
card_image_name('raise dead'/'CEI', 'raise dead').
card_uid('raise dead'/'CEI', 'CEI:Raise Dead:raise dead').
card_rarity('raise dead'/'CEI', 'Common').
card_artist('raise dead'/'CEI', 'Jeff A. Menges').

card_in_set('red elemental blast', 'CEI').
card_original_type('red elemental blast'/'CEI', 'Instant').
card_original_text('red elemental blast'/'CEI', '').
card_image_name('red elemental blast'/'CEI', 'red elemental blast').
card_uid('red elemental blast'/'CEI', 'CEI:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'CEI', 'Common').
card_artist('red elemental blast'/'CEI', 'Richard Thomas').

card_in_set('red ward', 'CEI').
card_original_type('red ward'/'CEI', 'Enchantment — Aura').
card_original_text('red ward'/'CEI', '').
card_image_name('red ward'/'CEI', 'red ward').
card_uid('red ward'/'CEI', 'CEI:Red Ward:red ward').
card_rarity('red ward'/'CEI', 'Uncommon').
card_artist('red ward'/'CEI', 'Dan Frazier').

card_in_set('regeneration', 'CEI').
card_original_type('regeneration'/'CEI', 'Enchantment — Aura').
card_original_text('regeneration'/'CEI', '').
card_image_name('regeneration'/'CEI', 'regeneration').
card_uid('regeneration'/'CEI', 'CEI:Regeneration:regeneration').
card_rarity('regeneration'/'CEI', 'Common').
card_artist('regeneration'/'CEI', 'Quinton Hoover').

card_in_set('regrowth', 'CEI').
card_original_type('regrowth'/'CEI', 'Sorcery').
card_original_text('regrowth'/'CEI', '').
card_image_name('regrowth'/'CEI', 'regrowth').
card_uid('regrowth'/'CEI', 'CEI:Regrowth:regrowth').
card_rarity('regrowth'/'CEI', 'Uncommon').
card_artist('regrowth'/'CEI', 'Dameon Willich').

card_in_set('resurrection', 'CEI').
card_original_type('resurrection'/'CEI', 'Sorcery').
card_original_text('resurrection'/'CEI', '').
card_image_name('resurrection'/'CEI', 'resurrection').
card_uid('resurrection'/'CEI', 'CEI:Resurrection:resurrection').
card_rarity('resurrection'/'CEI', 'Uncommon').
card_artist('resurrection'/'CEI', 'Dan Frazier').

card_in_set('reverse damage', 'CEI').
card_original_type('reverse damage'/'CEI', 'Instant').
card_original_text('reverse damage'/'CEI', '').
card_image_name('reverse damage'/'CEI', 'reverse damage').
card_uid('reverse damage'/'CEI', 'CEI:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'CEI', 'Rare').
card_artist('reverse damage'/'CEI', 'Dameon Willich').

card_in_set('righteousness', 'CEI').
card_original_type('righteousness'/'CEI', 'Instant').
card_original_text('righteousness'/'CEI', '').
card_image_name('righteousness'/'CEI', 'righteousness').
card_uid('righteousness'/'CEI', 'CEI:Righteousness:righteousness').
card_rarity('righteousness'/'CEI', 'Rare').
card_artist('righteousness'/'CEI', 'Douglas Shuler').

card_in_set('roc of kher ridges', 'CEI').
card_original_type('roc of kher ridges'/'CEI', 'Creature — Bird').
card_original_text('roc of kher ridges'/'CEI', '').
card_image_name('roc of kher ridges'/'CEI', 'roc of kher ridges').
card_uid('roc of kher ridges'/'CEI', 'CEI:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'CEI', 'Rare').
card_artist('roc of kher ridges'/'CEI', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'CEI', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').

card_in_set('rock hydra', 'CEI').
card_original_type('rock hydra'/'CEI', 'Creature — Hydra').
card_original_text('rock hydra'/'CEI', '').
card_image_name('rock hydra'/'CEI', 'rock hydra').
card_uid('rock hydra'/'CEI', 'CEI:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'CEI', 'Rare').
card_artist('rock hydra'/'CEI', 'Jeff A. Menges').

card_in_set('rod of ruin', 'CEI').
card_original_type('rod of ruin'/'CEI', 'Artifact').
card_original_text('rod of ruin'/'CEI', '').
card_image_name('rod of ruin'/'CEI', 'rod of ruin').
card_uid('rod of ruin'/'CEI', 'CEI:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'CEI', 'Uncommon').
card_artist('rod of ruin'/'CEI', 'Christopher Rush').

card_in_set('royal assassin', 'CEI').
card_original_type('royal assassin'/'CEI', 'Creature — Human Assassin').
card_original_text('royal assassin'/'CEI', '').
card_image_name('royal assassin'/'CEI', 'royal assassin').
card_uid('royal assassin'/'CEI', 'CEI:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'CEI', 'Rare').
card_artist('royal assassin'/'CEI', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'CEI', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').

card_in_set('sacrifice', 'CEI').
card_original_type('sacrifice'/'CEI', 'Instant').
card_original_text('sacrifice'/'CEI', '').
card_image_name('sacrifice'/'CEI', 'sacrifice').
card_uid('sacrifice'/'CEI', 'CEI:Sacrifice:sacrifice').
card_rarity('sacrifice'/'CEI', 'Uncommon').
card_artist('sacrifice'/'CEI', 'Dan Frazier').

card_in_set('samite healer', 'CEI').
card_original_type('samite healer'/'CEI', 'Creature — Human Cleric').
card_original_text('samite healer'/'CEI', '').
card_image_name('samite healer'/'CEI', 'samite healer').
card_uid('samite healer'/'CEI', 'CEI:Samite Healer:samite healer').
card_rarity('samite healer'/'CEI', 'Common').
card_artist('samite healer'/'CEI', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'CEI', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').

card_in_set('savannah', 'CEI').
card_original_type('savannah'/'CEI', 'Land — Forest Plains').
card_original_text('savannah'/'CEI', '').
card_image_name('savannah'/'CEI', 'savannah').
card_uid('savannah'/'CEI', 'CEI:Savannah:savannah').
card_rarity('savannah'/'CEI', 'Rare').
card_artist('savannah'/'CEI', 'Rob Alexander').

card_in_set('savannah lions', 'CEI').
card_original_type('savannah lions'/'CEI', 'Creature — Cat').
card_original_text('savannah lions'/'CEI', '').
card_image_name('savannah lions'/'CEI', 'savannah lions').
card_uid('savannah lions'/'CEI', 'CEI:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'CEI', 'Rare').
card_artist('savannah lions'/'CEI', 'Daniel Gelon').
card_flavor_text('savannah lions'/'CEI', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').

card_in_set('scathe zombies', 'CEI').
card_original_type('scathe zombies'/'CEI', 'Creature — Zombie').
card_original_text('scathe zombies'/'CEI', '').
card_image_name('scathe zombies'/'CEI', 'scathe zombies').
card_uid('scathe zombies'/'CEI', 'CEI:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'CEI', 'Common').
card_artist('scathe zombies'/'CEI', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'CEI', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/\nTo have seen those dead men rise.\"/\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('scavenging ghoul', 'CEI').
card_original_type('scavenging ghoul'/'CEI', 'Creature — Zombie').
card_original_text('scavenging ghoul'/'CEI', '').
card_image_name('scavenging ghoul'/'CEI', 'scavenging ghoul').
card_uid('scavenging ghoul'/'CEI', 'CEI:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'CEI', 'Uncommon').
card_artist('scavenging ghoul'/'CEI', 'Jeff A. Menges').

card_in_set('scrubland', 'CEI').
card_original_type('scrubland'/'CEI', 'Land — Plains Swamp').
card_original_text('scrubland'/'CEI', '').
card_image_name('scrubland'/'CEI', 'scrubland').
card_uid('scrubland'/'CEI', 'CEI:Scrubland:scrubland').
card_rarity('scrubland'/'CEI', 'Rare').
card_artist('scrubland'/'CEI', 'Jesper Myrfors').

card_in_set('scryb sprites', 'CEI').
card_original_type('scryb sprites'/'CEI', 'Creature — Faerie').
card_original_text('scryb sprites'/'CEI', '').
card_image_name('scryb sprites'/'CEI', 'scryb sprites').
card_uid('scryb sprites'/'CEI', 'CEI:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'CEI', 'Common').
card_artist('scryb sprites'/'CEI', 'Amy Weber').
card_flavor_text('scryb sprites'/'CEI', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').

card_in_set('sea serpent', 'CEI').
card_original_type('sea serpent'/'CEI', 'Creature — Serpent').
card_original_text('sea serpent'/'CEI', '').
card_image_name('sea serpent'/'CEI', 'sea serpent').
card_uid('sea serpent'/'CEI', 'CEI:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'CEI', 'Common').
card_artist('sea serpent'/'CEI', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'CEI', 'Legend has it that Serpents used to be bigger, but how could that be?').

card_in_set('sedge troll', 'CEI').
card_original_type('sedge troll'/'CEI', 'Creature — Troll').
card_original_text('sedge troll'/'CEI', '').
card_image_name('sedge troll'/'CEI', 'sedge troll').
card_uid('sedge troll'/'CEI', 'CEI:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'CEI', 'Rare').
card_artist('sedge troll'/'CEI', 'Dan Frazier').
card_flavor_text('sedge troll'/'CEI', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').

card_in_set('sengir vampire', 'CEI').
card_original_type('sengir vampire'/'CEI', 'Creature — Vampire').
card_original_text('sengir vampire'/'CEI', '').
card_image_name('sengir vampire'/'CEI', 'sengir vampire').
card_uid('sengir vampire'/'CEI', 'CEI:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'CEI', 'Uncommon').
card_artist('sengir vampire'/'CEI', 'Anson Maddocks').

card_in_set('serra angel', 'CEI').
card_original_type('serra angel'/'CEI', 'Creature — Angel').
card_original_text('serra angel'/'CEI', '').
card_image_name('serra angel'/'CEI', 'serra angel').
card_uid('serra angel'/'CEI', 'CEI:Serra Angel:serra angel').
card_rarity('serra angel'/'CEI', 'Uncommon').
card_artist('serra angel'/'CEI', 'Douglas Shuler').
card_flavor_text('serra angel'/'CEI', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').

card_in_set('shanodin dryads', 'CEI').
card_original_type('shanodin dryads'/'CEI', 'Creature — Nymph Dryad').
card_original_text('shanodin dryads'/'CEI', '').
card_image_name('shanodin dryads'/'CEI', 'shanodin dryads').
card_uid('shanodin dryads'/'CEI', 'CEI:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'CEI', 'Common').
card_artist('shanodin dryads'/'CEI', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'CEI', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').

card_in_set('shatter', 'CEI').
card_original_type('shatter'/'CEI', 'Instant').
card_original_text('shatter'/'CEI', '').
card_image_name('shatter'/'CEI', 'shatter').
card_uid('shatter'/'CEI', 'CEI:Shatter:shatter').
card_rarity('shatter'/'CEI', 'Common').
card_artist('shatter'/'CEI', 'Amy Weber').

card_in_set('shivan dragon', 'CEI').
card_original_type('shivan dragon'/'CEI', 'Creature — Dragon').
card_original_text('shivan dragon'/'CEI', '').
card_image_name('shivan dragon'/'CEI', 'shivan dragon').
card_uid('shivan dragon'/'CEI', 'CEI:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'CEI', 'Rare').
card_artist('shivan dragon'/'CEI', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'CEI', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').

card_in_set('simulacrum', 'CEI').
card_original_type('simulacrum'/'CEI', 'Instant').
card_original_text('simulacrum'/'CEI', '').
card_image_name('simulacrum'/'CEI', 'simulacrum').
card_uid('simulacrum'/'CEI', 'CEI:Simulacrum:simulacrum').
card_rarity('simulacrum'/'CEI', 'Uncommon').
card_artist('simulacrum'/'CEI', 'Mark Poole').

card_in_set('sinkhole', 'CEI').
card_original_type('sinkhole'/'CEI', 'Sorcery').
card_original_text('sinkhole'/'CEI', '').
card_image_name('sinkhole'/'CEI', 'sinkhole').
card_uid('sinkhole'/'CEI', 'CEI:Sinkhole:sinkhole').
card_rarity('sinkhole'/'CEI', 'Common').
card_artist('sinkhole'/'CEI', 'Sandra Everingham').

card_in_set('siren\'s call', 'CEI').
card_original_type('siren\'s call'/'CEI', 'Instant').
card_original_text('siren\'s call'/'CEI', '').
card_image_name('siren\'s call'/'CEI', 'siren\'s call').
card_uid('siren\'s call'/'CEI', 'CEI:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'CEI', 'Uncommon').
card_artist('siren\'s call'/'CEI', 'Anson Maddocks').

card_in_set('sleight of mind', 'CEI').
card_original_type('sleight of mind'/'CEI', 'Instant').
card_original_text('sleight of mind'/'CEI', '').
card_image_name('sleight of mind'/'CEI', 'sleight of mind').
card_uid('sleight of mind'/'CEI', 'CEI:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'CEI', 'Rare').
card_artist('sleight of mind'/'CEI', 'Mark Poole').

card_in_set('smoke', 'CEI').
card_original_type('smoke'/'CEI', 'Enchantment').
card_original_text('smoke'/'CEI', '').
card_image_name('smoke'/'CEI', 'smoke').
card_uid('smoke'/'CEI', 'CEI:Smoke:smoke').
card_rarity('smoke'/'CEI', 'Rare').
card_artist('smoke'/'CEI', 'Jesper Myrfors').

card_in_set('sol ring', 'CEI').
card_original_type('sol ring'/'CEI', 'Artifact').
card_original_text('sol ring'/'CEI', '').
card_image_name('sol ring'/'CEI', 'sol ring').
card_uid('sol ring'/'CEI', 'CEI:Sol Ring:sol ring').
card_rarity('sol ring'/'CEI', 'Uncommon').
card_artist('sol ring'/'CEI', 'Mark Tedin').

card_in_set('soul net', 'CEI').
card_original_type('soul net'/'CEI', 'Artifact').
card_original_text('soul net'/'CEI', '').
card_image_name('soul net'/'CEI', 'soul net').
card_uid('soul net'/'CEI', 'CEI:Soul Net:soul net').
card_rarity('soul net'/'CEI', 'Uncommon').
card_artist('soul net'/'CEI', 'Dameon Willich').

card_in_set('spell blast', 'CEI').
card_original_type('spell blast'/'CEI', 'Instant').
card_original_text('spell blast'/'CEI', '').
card_image_name('spell blast'/'CEI', 'spell blast').
card_uid('spell blast'/'CEI', 'CEI:Spell Blast:spell blast').
card_rarity('spell blast'/'CEI', 'Common').
card_artist('spell blast'/'CEI', 'Brian Snõddy').

card_in_set('stasis', 'CEI').
card_original_type('stasis'/'CEI', 'Enchantment').
card_original_text('stasis'/'CEI', '').
card_image_name('stasis'/'CEI', 'stasis').
card_uid('stasis'/'CEI', 'CEI:Stasis:stasis').
card_rarity('stasis'/'CEI', 'Rare').
card_artist('stasis'/'CEI', 'Fay Jones').

card_in_set('steal artifact', 'CEI').
card_original_type('steal artifact'/'CEI', 'Enchantment — Aura').
card_original_text('steal artifact'/'CEI', '').
card_image_name('steal artifact'/'CEI', 'steal artifact').
card_uid('steal artifact'/'CEI', 'CEI:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'CEI', 'Uncommon').
card_artist('steal artifact'/'CEI', 'Amy Weber').

card_in_set('stone giant', 'CEI').
card_original_type('stone giant'/'CEI', 'Creature — Giant').
card_original_text('stone giant'/'CEI', '').
card_image_name('stone giant'/'CEI', 'stone giant').
card_uid('stone giant'/'CEI', 'CEI:Stone Giant:stone giant').
card_rarity('stone giant'/'CEI', 'Uncommon').
card_artist('stone giant'/'CEI', 'Dameon Willich').
card_flavor_text('stone giant'/'CEI', 'What goes up, must come down.').

card_in_set('stone rain', 'CEI').
card_original_type('stone rain'/'CEI', 'Sorcery').
card_original_text('stone rain'/'CEI', '').
card_image_name('stone rain'/'CEI', 'stone rain').
card_uid('stone rain'/'CEI', 'CEI:Stone Rain:stone rain').
card_rarity('stone rain'/'CEI', 'Common').
card_artist('stone rain'/'CEI', 'Daniel Gelon').

card_in_set('stream of life', 'CEI').
card_original_type('stream of life'/'CEI', 'Sorcery').
card_original_text('stream of life'/'CEI', '').
card_image_name('stream of life'/'CEI', 'stream of life').
card_uid('stream of life'/'CEI', 'CEI:Stream of Life:stream of life').
card_rarity('stream of life'/'CEI', 'Common').
card_artist('stream of life'/'CEI', 'Mark Poole').

card_in_set('sunglasses of urza', 'CEI').
card_original_type('sunglasses of urza'/'CEI', 'Artifact').
card_original_text('sunglasses of urza'/'CEI', '').
card_image_name('sunglasses of urza'/'CEI', 'sunglasses of urza').
card_uid('sunglasses of urza'/'CEI', 'CEI:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'CEI', 'Rare').
card_artist('sunglasses of urza'/'CEI', 'Dan Frazier').

card_in_set('swamp', 'CEI').
card_original_type('swamp'/'CEI', 'Basic Land — Swamp').
card_original_text('swamp'/'CEI', '').
card_image_name('swamp'/'CEI', 'swamp1').
card_uid('swamp'/'CEI', 'CEI:Swamp:swamp1').
card_rarity('swamp'/'CEI', 'Basic Land').
card_artist('swamp'/'CEI', 'Dan Frazier').

card_in_set('swamp', 'CEI').
card_original_type('swamp'/'CEI', 'Basic Land — Swamp').
card_original_text('swamp'/'CEI', '').
card_image_name('swamp'/'CEI', 'swamp2').
card_uid('swamp'/'CEI', 'CEI:Swamp:swamp2').
card_rarity('swamp'/'CEI', 'Basic Land').
card_artist('swamp'/'CEI', 'Dan Frazier').

card_in_set('swamp', 'CEI').
card_original_type('swamp'/'CEI', 'Basic Land — Swamp').
card_original_text('swamp'/'CEI', '').
card_image_name('swamp'/'CEI', 'swamp3').
card_uid('swamp'/'CEI', 'CEI:Swamp:swamp3').
card_rarity('swamp'/'CEI', 'Basic Land').
card_artist('swamp'/'CEI', 'Dan Frazier').

card_in_set('swords to plowshares', 'CEI').
card_original_type('swords to plowshares'/'CEI', 'Instant').
card_original_text('swords to plowshares'/'CEI', '').
card_image_name('swords to plowshares'/'CEI', 'swords to plowshares').
card_uid('swords to plowshares'/'CEI', 'CEI:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'CEI', 'Uncommon').
card_artist('swords to plowshares'/'CEI', 'Jeff A. Menges').

card_in_set('taiga', 'CEI').
card_original_type('taiga'/'CEI', 'Land — Mountain Forest').
card_original_text('taiga'/'CEI', '').
card_image_name('taiga'/'CEI', 'taiga').
card_uid('taiga'/'CEI', 'CEI:Taiga:taiga').
card_rarity('taiga'/'CEI', 'Rare').
card_artist('taiga'/'CEI', 'Rob Alexander').

card_in_set('terror', 'CEI').
card_original_type('terror'/'CEI', 'Instant').
card_original_text('terror'/'CEI', '').
card_image_name('terror'/'CEI', 'terror').
card_uid('terror'/'CEI', 'CEI:Terror:terror').
card_rarity('terror'/'CEI', 'Common').
card_artist('terror'/'CEI', 'Ron Spencer').

card_in_set('the hive', 'CEI').
card_original_type('the hive'/'CEI', 'Artifact').
card_original_text('the hive'/'CEI', '').
card_image_name('the hive'/'CEI', 'the hive').
card_uid('the hive'/'CEI', 'CEI:The Hive:the hive').
card_rarity('the hive'/'CEI', 'Rare').
card_artist('the hive'/'CEI', 'Sandra Everingham').

card_in_set('thicket basilisk', 'CEI').
card_original_type('thicket basilisk'/'CEI', 'Creature — Basilisk').
card_original_text('thicket basilisk'/'CEI', '').
card_image_name('thicket basilisk'/'CEI', 'thicket basilisk').
card_uid('thicket basilisk'/'CEI', 'CEI:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'CEI', 'Uncommon').
card_artist('thicket basilisk'/'CEI', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'CEI', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').

card_in_set('thoughtlace', 'CEI').
card_original_type('thoughtlace'/'CEI', 'Instant').
card_original_text('thoughtlace'/'CEI', '').
card_image_name('thoughtlace'/'CEI', 'thoughtlace').
card_uid('thoughtlace'/'CEI', 'CEI:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'CEI', 'Rare').
card_artist('thoughtlace'/'CEI', 'Mark Poole').

card_in_set('throne of bone', 'CEI').
card_original_type('throne of bone'/'CEI', 'Artifact').
card_original_text('throne of bone'/'CEI', '').
card_image_name('throne of bone'/'CEI', 'throne of bone').
card_uid('throne of bone'/'CEI', 'CEI:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'CEI', 'Uncommon').
card_artist('throne of bone'/'CEI', 'Anson Maddocks').

card_in_set('timber wolves', 'CEI').
card_original_type('timber wolves'/'CEI', 'Creature — Wolf').
card_original_text('timber wolves'/'CEI', '').
card_image_name('timber wolves'/'CEI', 'timber wolves').
card_uid('timber wolves'/'CEI', 'CEI:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'CEI', 'Rare').
card_artist('timber wolves'/'CEI', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'CEI', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').

card_in_set('time vault', 'CEI').
card_original_type('time vault'/'CEI', 'Artifact').
card_original_text('time vault'/'CEI', '').
card_image_name('time vault'/'CEI', 'time vault').
card_uid('time vault'/'CEI', 'CEI:Time Vault:time vault').
card_rarity('time vault'/'CEI', 'Rare').
card_artist('time vault'/'CEI', 'Mark Tedin').

card_in_set('time walk', 'CEI').
card_original_type('time walk'/'CEI', 'Sorcery').
card_original_text('time walk'/'CEI', '').
card_image_name('time walk'/'CEI', 'time walk').
card_uid('time walk'/'CEI', 'CEI:Time Walk:time walk').
card_rarity('time walk'/'CEI', 'Rare').
card_artist('time walk'/'CEI', 'Amy Weber').

card_in_set('timetwister', 'CEI').
card_original_type('timetwister'/'CEI', 'Sorcery').
card_original_text('timetwister'/'CEI', '').
card_image_name('timetwister'/'CEI', 'timetwister').
card_uid('timetwister'/'CEI', 'CEI:Timetwister:timetwister').
card_rarity('timetwister'/'CEI', 'Rare').
card_artist('timetwister'/'CEI', 'Mark Tedin').

card_in_set('tranquility', 'CEI').
card_original_type('tranquility'/'CEI', 'Sorcery').
card_original_text('tranquility'/'CEI', '').
card_image_name('tranquility'/'CEI', 'tranquility').
card_uid('tranquility'/'CEI', 'CEI:Tranquility:tranquility').
card_rarity('tranquility'/'CEI', 'Common').
card_artist('tranquility'/'CEI', 'Douglas Shuler').

card_in_set('tropical island', 'CEI').
card_original_type('tropical island'/'CEI', 'Land — Forest Island').
card_original_text('tropical island'/'CEI', '').
card_image_name('tropical island'/'CEI', 'tropical island').
card_uid('tropical island'/'CEI', 'CEI:Tropical Island:tropical island').
card_rarity('tropical island'/'CEI', 'Rare').
card_artist('tropical island'/'CEI', 'Jesper Myrfors').

card_in_set('tsunami', 'CEI').
card_original_type('tsunami'/'CEI', 'Sorcery').
card_original_text('tsunami'/'CEI', '').
card_image_name('tsunami'/'CEI', 'tsunami').
card_uid('tsunami'/'CEI', 'CEI:Tsunami:tsunami').
card_rarity('tsunami'/'CEI', 'Uncommon').
card_artist('tsunami'/'CEI', 'Richard Thomas').

card_in_set('tundra', 'CEI').
card_original_type('tundra'/'CEI', 'Land — Plains Island').
card_original_text('tundra'/'CEI', '').
card_image_name('tundra'/'CEI', 'tundra').
card_uid('tundra'/'CEI', 'CEI:Tundra:tundra').
card_rarity('tundra'/'CEI', 'Rare').
card_artist('tundra'/'CEI', 'Jesper Myrfors').

card_in_set('tunnel', 'CEI').
card_original_type('tunnel'/'CEI', 'Instant').
card_original_text('tunnel'/'CEI', '').
card_image_name('tunnel'/'CEI', 'tunnel').
card_uid('tunnel'/'CEI', 'CEI:Tunnel:tunnel').
card_rarity('tunnel'/'CEI', 'Uncommon').
card_artist('tunnel'/'CEI', 'Dan Frazier').

card_in_set('twiddle', 'CEI').
card_original_type('twiddle'/'CEI', 'Instant').
card_original_text('twiddle'/'CEI', '').
card_image_name('twiddle'/'CEI', 'twiddle').
card_uid('twiddle'/'CEI', 'CEI:Twiddle:twiddle').
card_rarity('twiddle'/'CEI', 'Common').
card_artist('twiddle'/'CEI', 'Rob Alexander').

card_in_set('two-headed giant of foriys', 'CEI').
card_original_type('two-headed giant of foriys'/'CEI', 'Creature — Giant').
card_original_text('two-headed giant of foriys'/'CEI', '').
card_image_name('two-headed giant of foriys'/'CEI', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'CEI', 'CEI:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'CEI', 'Rare').
card_artist('two-headed giant of foriys'/'CEI', 'Anson Maddocks').
card_flavor_text('two-headed giant of foriys'/'CEI', 'None know if this Giant is the result of aberrant magics, Siamese twins, or a mentalist\'s schizophrenia.').

card_in_set('underground sea', 'CEI').
card_original_type('underground sea'/'CEI', 'Land — Island Swamp').
card_original_text('underground sea'/'CEI', '').
card_image_name('underground sea'/'CEI', 'underground sea').
card_uid('underground sea'/'CEI', 'CEI:Underground Sea:underground sea').
card_rarity('underground sea'/'CEI', 'Rare').
card_artist('underground sea'/'CEI', 'Rob Alexander').

card_in_set('unholy strength', 'CEI').
card_original_type('unholy strength'/'CEI', 'Enchantment — Aura').
card_original_text('unholy strength'/'CEI', '').
card_image_name('unholy strength'/'CEI', 'unholy strength').
card_uid('unholy strength'/'CEI', 'CEI:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'CEI', 'Common').
card_artist('unholy strength'/'CEI', 'Douglas Shuler').

card_in_set('unsummon', 'CEI').
card_original_type('unsummon'/'CEI', 'Instant').
card_original_text('unsummon'/'CEI', '').
card_image_name('unsummon'/'CEI', 'unsummon').
card_uid('unsummon'/'CEI', 'CEI:Unsummon:unsummon').
card_rarity('unsummon'/'CEI', 'Common').
card_artist('unsummon'/'CEI', 'Douglas Shuler').

card_in_set('uthden troll', 'CEI').
card_original_type('uthden troll'/'CEI', 'Creature — Troll').
card_original_text('uthden troll'/'CEI', '').
card_image_name('uthden troll'/'CEI', 'uthden troll').
card_uid('uthden troll'/'CEI', 'CEI:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'CEI', 'Uncommon').
card_artist('uthden troll'/'CEI', 'Douglas Shuler').
card_flavor_text('uthden troll'/'CEI', '\"Oi oi oi, me gotta hurt in \'ere\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').

card_in_set('verduran enchantress', 'CEI').
card_original_type('verduran enchantress'/'CEI', 'Creature — Human Druid').
card_original_text('verduran enchantress'/'CEI', '').
card_image_name('verduran enchantress'/'CEI', 'verduran enchantress').
card_uid('verduran enchantress'/'CEI', 'CEI:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'CEI', 'Rare').
card_artist('verduran enchantress'/'CEI', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'CEI', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').

card_in_set('vesuvan doppelganger', 'CEI').
card_original_type('vesuvan doppelganger'/'CEI', 'Creature — Shapeshifter').
card_original_text('vesuvan doppelganger'/'CEI', '').
card_image_name('vesuvan doppelganger'/'CEI', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'CEI', 'CEI:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'CEI', 'Rare').
card_artist('vesuvan doppelganger'/'CEI', 'Quinton Hoover').

card_in_set('veteran bodyguard', 'CEI').
card_original_type('veteran bodyguard'/'CEI', 'Creature — Human').
card_original_text('veteran bodyguard'/'CEI', '').
card_image_name('veteran bodyguard'/'CEI', 'veteran bodyguard').
card_uid('veteran bodyguard'/'CEI', 'CEI:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'CEI', 'Rare').
card_artist('veteran bodyguard'/'CEI', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'CEI', 'Good bodyguards are hard to find, mainly because they don\'t live long.').

card_in_set('volcanic eruption', 'CEI').
card_original_type('volcanic eruption'/'CEI', 'Sorcery').
card_original_text('volcanic eruption'/'CEI', '').
card_image_name('volcanic eruption'/'CEI', 'volcanic eruption').
card_uid('volcanic eruption'/'CEI', 'CEI:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'CEI', 'Rare').
card_artist('volcanic eruption'/'CEI', 'Douglas Shuler').

card_in_set('volcanic island', 'CEI').
card_original_type('volcanic island'/'CEI', 'Land — Island Mountain').
card_original_text('volcanic island'/'CEI', '').
card_image_name('volcanic island'/'CEI', 'volcanic island').
card_uid('volcanic island'/'CEI', 'CEI:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'CEI', 'Rare').
card_artist('volcanic island'/'CEI', 'Brian Snõddy').

card_in_set('wall of air', 'CEI').
card_original_type('wall of air'/'CEI', 'Creature — Wall').
card_original_text('wall of air'/'CEI', '').
card_image_name('wall of air'/'CEI', 'wall of air').
card_uid('wall of air'/'CEI', 'CEI:Wall of Air:wall of air').
card_rarity('wall of air'/'CEI', 'Uncommon').
card_artist('wall of air'/'CEI', 'Richard Thomas').
card_flavor_text('wall of air'/'CEI', '\"This \'standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').

card_in_set('wall of bone', 'CEI').
card_original_type('wall of bone'/'CEI', 'Creature — Skeleton Wall').
card_original_text('wall of bone'/'CEI', '').
card_image_name('wall of bone'/'CEI', 'wall of bone').
card_uid('wall of bone'/'CEI', 'CEI:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'CEI', 'Uncommon').
card_artist('wall of bone'/'CEI', 'Anson Maddocks').
card_flavor_text('wall of bone'/'CEI', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').

card_in_set('wall of brambles', 'CEI').
card_original_type('wall of brambles'/'CEI', 'Creature — Plant Wall').
card_original_text('wall of brambles'/'CEI', '').
card_image_name('wall of brambles'/'CEI', 'wall of brambles').
card_uid('wall of brambles'/'CEI', 'CEI:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'CEI', 'Uncommon').
card_artist('wall of brambles'/'CEI', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'CEI', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n—Conrad Aiken').

card_in_set('wall of fire', 'CEI').
card_original_type('wall of fire'/'CEI', 'Creature — Wall').
card_original_text('wall of fire'/'CEI', '').
card_image_name('wall of fire'/'CEI', 'wall of fire').
card_uid('wall of fire'/'CEI', 'CEI:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'CEI', 'Uncommon').
card_artist('wall of fire'/'CEI', 'Richard Thomas').
card_flavor_text('wall of fire'/'CEI', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').

card_in_set('wall of ice', 'CEI').
card_original_type('wall of ice'/'CEI', 'Creature — Wall').
card_original_text('wall of ice'/'CEI', '').
card_image_name('wall of ice'/'CEI', 'wall of ice').
card_uid('wall of ice'/'CEI', 'CEI:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'CEI', 'Uncommon').
card_artist('wall of ice'/'CEI', 'Richard Thomas').
card_flavor_text('wall of ice'/'CEI', '\"And through the drifts the snowy cliffs/ Did send a dismal sheen:/ Nor shapes of men nor beasts we ken—/ The ice was all between.\"/\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('wall of stone', 'CEI').
card_original_type('wall of stone'/'CEI', 'Creature — Wall').
card_original_text('wall of stone'/'CEI', '').
card_image_name('wall of stone'/'CEI', 'wall of stone').
card_uid('wall of stone'/'CEI', 'CEI:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'CEI', 'Uncommon').
card_artist('wall of stone'/'CEI', 'Dan Frazier').
card_flavor_text('wall of stone'/'CEI', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').

card_in_set('wall of swords', 'CEI').
card_original_type('wall of swords'/'CEI', 'Creature — Wall').
card_original_text('wall of swords'/'CEI', '').
card_image_name('wall of swords'/'CEI', 'wall of swords').
card_uid('wall of swords'/'CEI', 'CEI:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'CEI', 'Uncommon').
card_artist('wall of swords'/'CEI', 'Mark Tedin').
card_flavor_text('wall of swords'/'CEI', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').

card_in_set('wall of water', 'CEI').
card_original_type('wall of water'/'CEI', 'Creature — Wall').
card_original_text('wall of water'/'CEI', '').
card_image_name('wall of water'/'CEI', 'wall of water').
card_uid('wall of water'/'CEI', 'CEI:Wall of Water:wall of water').
card_rarity('wall of water'/'CEI', 'Uncommon').
card_artist('wall of water'/'CEI', 'Richard Thomas').
card_flavor_text('wall of water'/'CEI', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').

card_in_set('wall of wood', 'CEI').
card_original_type('wall of wood'/'CEI', 'Creature — Wall').
card_original_text('wall of wood'/'CEI', '').
card_image_name('wall of wood'/'CEI', 'wall of wood').
card_uid('wall of wood'/'CEI', 'CEI:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'CEI', 'Common').
card_artist('wall of wood'/'CEI', 'Mark Tedin').
card_flavor_text('wall of wood'/'CEI', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').

card_in_set('wanderlust', 'CEI').
card_original_type('wanderlust'/'CEI', 'Enchantment — Aura').
card_original_text('wanderlust'/'CEI', '').
card_image_name('wanderlust'/'CEI', 'wanderlust').
card_uid('wanderlust'/'CEI', 'CEI:Wanderlust:wanderlust').
card_rarity('wanderlust'/'CEI', 'Uncommon').
card_artist('wanderlust'/'CEI', 'Cornelius Brudi').

card_in_set('war mammoth', 'CEI').
card_original_type('war mammoth'/'CEI', 'Creature — Elephant').
card_original_text('war mammoth'/'CEI', '').
card_image_name('war mammoth'/'CEI', 'war mammoth').
card_uid('war mammoth'/'CEI', 'CEI:War Mammoth:war mammoth').
card_rarity('war mammoth'/'CEI', 'Common').
card_artist('war mammoth'/'CEI', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'CEI', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').

card_in_set('warp artifact', 'CEI').
card_original_type('warp artifact'/'CEI', 'Enchantment — Aura').
card_original_text('warp artifact'/'CEI', '').
card_image_name('warp artifact'/'CEI', 'warp artifact').
card_uid('warp artifact'/'CEI', 'CEI:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'CEI', 'Rare').
card_artist('warp artifact'/'CEI', 'Amy Weber').

card_in_set('water elemental', 'CEI').
card_original_type('water elemental'/'CEI', 'Creature — Elemental').
card_original_text('water elemental'/'CEI', '').
card_image_name('water elemental'/'CEI', 'water elemental').
card_uid('water elemental'/'CEI', 'CEI:Water Elemental:water elemental').
card_rarity('water elemental'/'CEI', 'Uncommon').
card_artist('water elemental'/'CEI', 'Jeff A. Menges').
card_flavor_text('water elemental'/'CEI', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').

card_in_set('weakness', 'CEI').
card_original_type('weakness'/'CEI', 'Enchantment — Aura').
card_original_text('weakness'/'CEI', '').
card_image_name('weakness'/'CEI', 'weakness').
card_uid('weakness'/'CEI', 'CEI:Weakness:weakness').
card_rarity('weakness'/'CEI', 'Common').
card_artist('weakness'/'CEI', 'Douglas Shuler').

card_in_set('web', 'CEI').
card_original_type('web'/'CEI', 'Enchantment — Aura').
card_original_text('web'/'CEI', '').
card_image_name('web'/'CEI', 'web').
card_uid('web'/'CEI', 'CEI:Web:web').
card_rarity('web'/'CEI', 'Rare').
card_artist('web'/'CEI', 'Rob Alexander').

card_in_set('wheel of fortune', 'CEI').
card_original_type('wheel of fortune'/'CEI', 'Sorcery').
card_original_text('wheel of fortune'/'CEI', '').
card_image_name('wheel of fortune'/'CEI', 'wheel of fortune').
card_uid('wheel of fortune'/'CEI', 'CEI:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'CEI', 'Rare').
card_artist('wheel of fortune'/'CEI', 'Daniel Gelon').

card_in_set('white knight', 'CEI').
card_original_type('white knight'/'CEI', 'Creature — Human Knight').
card_original_text('white knight'/'CEI', '').
card_image_name('white knight'/'CEI', 'white knight').
card_uid('white knight'/'CEI', 'CEI:White Knight:white knight').
card_rarity('white knight'/'CEI', 'Uncommon').
card_artist('white knight'/'CEI', 'Daniel Gelon').
card_flavor_text('white knight'/'CEI', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').

card_in_set('white ward', 'CEI').
card_original_type('white ward'/'CEI', 'Enchantment — Aura').
card_original_text('white ward'/'CEI', '').
card_image_name('white ward'/'CEI', 'white ward').
card_uid('white ward'/'CEI', 'CEI:White Ward:white ward').
card_rarity('white ward'/'CEI', 'Uncommon').
card_artist('white ward'/'CEI', 'Dan Frazier').

card_in_set('wild growth', 'CEI').
card_original_type('wild growth'/'CEI', 'Enchantment — Aura').
card_original_text('wild growth'/'CEI', '').
card_image_name('wild growth'/'CEI', 'wild growth').
card_uid('wild growth'/'CEI', 'CEI:Wild Growth:wild growth').
card_rarity('wild growth'/'CEI', 'Common').
card_artist('wild growth'/'CEI', 'Mark Poole').

card_in_set('will-o\'-the-wisp', 'CEI').
card_original_type('will-o\'-the-wisp'/'CEI', 'Creature — Spirit').
card_original_text('will-o\'-the-wisp'/'CEI', '').
card_image_name('will-o\'-the-wisp'/'CEI', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'CEI', 'CEI:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'CEI', 'Rare').
card_artist('will-o\'-the-wisp'/'CEI', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'CEI', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('winter orb', 'CEI').
card_original_type('winter orb'/'CEI', 'Artifact').
card_original_text('winter orb'/'CEI', '').
card_image_name('winter orb'/'CEI', 'winter orb').
card_uid('winter orb'/'CEI', 'CEI:Winter Orb:winter orb').
card_rarity('winter orb'/'CEI', 'Rare').
card_artist('winter orb'/'CEI', 'Mark Tedin').

card_in_set('wooden sphere', 'CEI').
card_original_type('wooden sphere'/'CEI', 'Artifact').
card_original_text('wooden sphere'/'CEI', '').
card_image_name('wooden sphere'/'CEI', 'wooden sphere').
card_uid('wooden sphere'/'CEI', 'CEI:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'CEI', 'Uncommon').
card_artist('wooden sphere'/'CEI', 'Mark Tedin').

card_in_set('word of command', 'CEI').
card_original_type('word of command'/'CEI', 'Instant').
card_original_text('word of command'/'CEI', '').
card_image_name('word of command'/'CEI', 'word of command').
card_uid('word of command'/'CEI', 'CEI:Word of Command:word of command').
card_rarity('word of command'/'CEI', 'Rare').
card_artist('word of command'/'CEI', 'Jesper Myrfors').

card_in_set('wrath of god', 'CEI').
card_original_type('wrath of god'/'CEI', 'Sorcery').
card_original_text('wrath of god'/'CEI', '').
card_image_name('wrath of god'/'CEI', 'wrath of god').
card_uid('wrath of god'/'CEI', 'CEI:Wrath of God:wrath of god').
card_rarity('wrath of god'/'CEI', 'Rare').
card_artist('wrath of god'/'CEI', 'Quinton Hoover').

card_in_set('zombie master', 'CEI').
card_original_type('zombie master'/'CEI', 'Creature — Zombie').
card_original_text('zombie master'/'CEI', '').
card_image_name('zombie master'/'CEI', 'zombie master').
card_uid('zombie master'/'CEI', 'CEI:Zombie Master:zombie master').
card_rarity('zombie master'/'CEI', 'Rare').
card_artist('zombie master'/'CEI', 'Jeff A. Menges').
card_flavor_text('zombie master'/'CEI', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
