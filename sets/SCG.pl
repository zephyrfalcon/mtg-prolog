% Scourge

set('SCG').
set_name('SCG', 'Scourge').
set_release_date('SCG', '2003-05-26').
set_border('SCG', 'black').
set_type('SCG', 'expansion').
set_block('SCG', 'Onslaught').

card_in_set('accelerated mutation', 'SCG').
card_original_type('accelerated mutation'/'SCG', 'Instant').
card_original_text('accelerated mutation'/'SCG', 'Target creature gets +X/+X until end of turn, where X is the highest converted mana cost among permanents you control.').
card_first_print('accelerated mutation', 'SCG').
card_image_name('accelerated mutation'/'SCG', 'accelerated mutation').
card_uid('accelerated mutation'/'SCG', 'SCG:Accelerated Mutation:accelerated mutation').
card_rarity('accelerated mutation'/'SCG', 'Common').
card_artist('accelerated mutation'/'SCG', 'Edward P. Beard, Jr.').
card_number('accelerated mutation'/'SCG', '109').
card_flavor_text('accelerated mutation'/'SCG', 'Its food just got a lot smaller and its stomach a lot bigger.').
card_multiverse_id('accelerated mutation'/'SCG', '43539').

card_in_set('ageless sentinels', 'SCG').
card_original_type('ageless sentinels'/'SCG', 'Creature — Wall').
card_original_text('ageless sentinels'/'SCG', '(Walls can\'t attack.)\nFlying\nWhen Ageless Sentinels blocks, its creature type becomes Giant Bird. (It\'s no longer a Wall. This effect doesn\'t end at end of turn.)').
card_first_print('ageless sentinels', 'SCG').
card_image_name('ageless sentinels'/'SCG', 'ageless sentinels').
card_uid('ageless sentinels'/'SCG', 'SCG:Ageless Sentinels:ageless sentinels').
card_rarity('ageless sentinels'/'SCG', 'Rare').
card_artist('ageless sentinels'/'SCG', 'Tony Szczudlo').
card_number('ageless sentinels'/'SCG', '1').
card_multiverse_id('ageless sentinels'/'SCG', '43713').

card_in_set('alpha status', 'SCG').
card_original_type('alpha status'/'SCG', 'Enchant Creature').
card_original_text('alpha status'/'SCG', 'Enchanted creature gets +2/+2 for each other creature in play that shares a creature type with it.').
card_first_print('alpha status', 'SCG').
card_image_name('alpha status'/'SCG', 'alpha status').
card_uid('alpha status'/'SCG', 'SCG:Alpha Status:alpha status').
card_rarity('alpha status'/'SCG', 'Uncommon').
card_artist('alpha status'/'SCG', 'Darrell Riche').
card_number('alpha status'/'SCG', '110').
card_flavor_text('alpha status'/'SCG', 'The best leaders are made by their followers.').
card_multiverse_id('alpha status'/'SCG', '35409').

card_in_set('ambush commander', 'SCG').
card_original_type('ambush commander'/'SCG', 'Creature — Elf').
card_original_text('ambush commander'/'SCG', 'Forests you control are 1/1 green Elf creatures that are still lands.\n{1}{G}, Sacrifice an Elf: Target creature gets +3/+3 until end of turn.').
card_first_print('ambush commander', 'SCG').
card_image_name('ambush commander'/'SCG', 'ambush commander').
card_uid('ambush commander'/'SCG', 'SCG:Ambush Commander:ambush commander').
card_rarity('ambush commander'/'SCG', 'Rare').
card_artist('ambush commander'/'SCG', 'Darrell Riche').
card_number('ambush commander'/'SCG', '111').
card_multiverse_id('ambush commander'/'SCG', '43599').

card_in_set('ancient ooze', 'SCG').
card_original_type('ancient ooze'/'SCG', 'Creature — Ooze').
card_original_text('ancient ooze'/'SCG', 'Ancient Ooze\'s power and toughness are each equal to the total converted mana cost of other creatures you control.').
card_first_print('ancient ooze', 'SCG').
card_image_name('ancient ooze'/'SCG', 'ancient ooze').
card_uid('ancient ooze'/'SCG', 'SCG:Ancient Ooze:ancient ooze').
card_rarity('ancient ooze'/'SCG', 'Rare').
card_artist('ancient ooze'/'SCG', 'Erica Gassalasca-Jape').
card_number('ancient ooze'/'SCG', '112').
card_flavor_text('ancient ooze'/'SCG', 'The ooze has always been. The ooze will always be.').
card_multiverse_id('ancient ooze'/'SCG', '45164').

card_in_set('aphetto runecaster', 'SCG').
card_original_type('aphetto runecaster'/'SCG', 'Creature — Wizard').
card_original_text('aphetto runecaster'/'SCG', 'Whenever a creature is turned face up, you may draw a card.').
card_first_print('aphetto runecaster', 'SCG').
card_image_name('aphetto runecaster'/'SCG', 'aphetto runecaster').
card_uid('aphetto runecaster'/'SCG', 'SCG:Aphetto Runecaster:aphetto runecaster').
card_rarity('aphetto runecaster'/'SCG', 'Uncommon').
card_artist('aphetto runecaster'/'SCG', 'Scott M. Fischer').
card_number('aphetto runecaster'/'SCG', '28').
card_flavor_text('aphetto runecaster'/'SCG', '\"The ignorant fear change; the wise use it as a weapon.\"').
card_multiverse_id('aphetto runecaster'/'SCG', '44339').

card_in_set('ark of blight', 'SCG').
card_original_type('ark of blight'/'SCG', 'Artifact').
card_original_text('ark of blight'/'SCG', '{3}, {T}, Sacrifice Ark of Blight: Destroy target land.').
card_first_print('ark of blight', 'SCG').
card_image_name('ark of blight'/'SCG', 'ark of blight').
card_uid('ark of blight'/'SCG', 'SCG:Ark of Blight:ark of blight').
card_rarity('ark of blight'/'SCG', 'Uncommon').
card_artist('ark of blight'/'SCG', 'David Martin').
card_number('ark of blight'/'SCG', '140').
card_flavor_text('ark of blight'/'SCG', 'When opened, it erases entire landscapes from minds and maps.').
card_multiverse_id('ark of blight'/'SCG', '47281').

card_in_set('astral steel', 'SCG').
card_original_type('astral steel'/'SCG', 'Instant').
card_original_text('astral steel'/'SCG', 'Target creature gets +1/+2 until end of turn.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('astral steel', 'SCG').
card_image_name('astral steel'/'SCG', 'astral steel').
card_uid('astral steel'/'SCG', 'SCG:Astral Steel:astral steel').
card_rarity('astral steel'/'SCG', 'Common').
card_artist('astral steel'/'SCG', 'Matt Cavotta').
card_number('astral steel'/'SCG', '2').
card_multiverse_id('astral steel'/'SCG', '45833').

card_in_set('aven farseer', 'SCG').
card_original_type('aven farseer'/'SCG', 'Creature — Bird Soldier').
card_original_text('aven farseer'/'SCG', 'Flying\nWhenever a creature is turned face up, put a +1/+1 counter on Aven Farseer.').
card_first_print('aven farseer', 'SCG').
card_image_name('aven farseer'/'SCG', 'aven farseer').
card_uid('aven farseer'/'SCG', 'SCG:Aven Farseer:aven farseer').
card_rarity('aven farseer'/'SCG', 'Common').
card_artist('aven farseer'/'SCG', 'Luca Zontini').
card_number('aven farseer'/'SCG', '3').
card_flavor_text('aven farseer'/'SCG', 'His enemies\' trickery and deceit only fuel his righteous anger.').
card_multiverse_id('aven farseer'/'SCG', '44343').

card_in_set('aven liberator', 'SCG').
card_original_type('aven liberator'/'SCG', 'Creature — Bird Soldier').
card_original_text('aven liberator'/'SCG', 'Flying\nMorph {3}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Aven Liberator is turned face up, target creature you control gains protection from the color of your choice until end of turn.').
card_first_print('aven liberator', 'SCG').
card_image_name('aven liberator'/'SCG', 'aven liberator').
card_uid('aven liberator'/'SCG', 'SCG:Aven Liberator:aven liberator').
card_rarity('aven liberator'/'SCG', 'Common').
card_artist('aven liberator'/'SCG', 'Brian Snõddy').
card_number('aven liberator'/'SCG', '4').
card_multiverse_id('aven liberator'/'SCG', '43626').

card_in_set('bladewing the risen', 'SCG').
card_original_type('bladewing the risen'/'SCG', 'Creature — Dragon Legend').
card_original_text('bladewing the risen'/'SCG', 'Flying\nWhen Bladewing the Risen comes into play, you may return target Dragon card from your graveyard to play.\n{B}{R}: All Dragons get +1/+1 until end of turn.').
card_first_print('bladewing the risen', 'SCG').
card_image_name('bladewing the risen'/'SCG', 'bladewing the risen').
card_uid('bladewing the risen'/'SCG', 'SCG:Bladewing the Risen:bladewing the risen').
card_rarity('bladewing the risen'/'SCG', 'Rare').
card_artist('bladewing the risen'/'SCG', 'Kev Walker').
card_number('bladewing the risen'/'SCG', '136').
card_multiverse_id('bladewing the risen'/'SCG', '42024').

card_in_set('bladewing\'s thrall', 'SCG').
card_original_type('bladewing\'s thrall'/'SCG', 'Creature — Zombie').
card_original_text('bladewing\'s thrall'/'SCG', 'Bladewing\'s Thrall has flying as long as you control a Dragon.\nWhen a Dragon comes into play, you may return Bladewing\'s Thrall from your graveyard to play.').
card_first_print('bladewing\'s thrall', 'SCG').
card_image_name('bladewing\'s thrall'/'SCG', 'bladewing\'s thrall').
card_uid('bladewing\'s thrall'/'SCG', 'SCG:Bladewing\'s Thrall:bladewing\'s thrall').
card_rarity('bladewing\'s thrall'/'SCG', 'Uncommon').
card_artist('bladewing\'s thrall'/'SCG', 'Kev Walker').
card_number('bladewing\'s thrall'/'SCG', '55').
card_multiverse_id('bladewing\'s thrall'/'SCG', '47224').

card_in_set('bonethorn valesk', 'SCG').
card_original_type('bonethorn valesk'/'SCG', 'Creature — Beast').
card_original_text('bonethorn valesk'/'SCG', 'Whenever a creature is turned face up, Bonethorn Valesk deals 1 damage to target creature or player.').
card_first_print('bonethorn valesk', 'SCG').
card_image_name('bonethorn valesk'/'SCG', 'bonethorn valesk').
card_uid('bonethorn valesk'/'SCG', 'SCG:Bonethorn Valesk:bonethorn valesk').
card_rarity('bonethorn valesk'/'SCG', 'Common').
card_artist('bonethorn valesk'/'SCG', 'Alan Pollack').
card_number('bonethorn valesk'/'SCG', '82').
card_flavor_text('bonethorn valesk'/'SCG', 'Barbarians weave its spurs into ceremonial charms to proclaim their hardiness in battle.').
card_multiverse_id('bonethorn valesk'/'SCG', '44342').

card_in_set('brain freeze', 'SCG').
card_original_type('brain freeze'/'SCG', 'Instant').
card_original_text('brain freeze'/'SCG', 'Target player puts the top three cards of his or her library into his or her graveyard.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('brain freeze', 'SCG').
card_image_name('brain freeze'/'SCG', 'brain freeze').
card_uid('brain freeze'/'SCG', 'SCG:Brain Freeze:brain freeze').
card_rarity('brain freeze'/'SCG', 'Uncommon').
card_artist('brain freeze'/'SCG', 'Tim Hildebrandt').
card_number('brain freeze'/'SCG', '29').
card_multiverse_id('brain freeze'/'SCG', '47599').

card_in_set('break asunder', 'SCG').
card_original_type('break asunder'/'SCG', 'Sorcery').
card_original_text('break asunder'/'SCG', 'Destroy target artifact or enchantment.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('break asunder', 'SCG').
card_image_name('break asunder'/'SCG', 'break asunder').
card_uid('break asunder'/'SCG', 'SCG:Break Asunder:break asunder').
card_rarity('break asunder'/'SCG', 'Common').
card_artist('break asunder'/'SCG', 'Jim Nelson').
card_number('break asunder'/'SCG', '113').
card_flavor_text('break asunder'/'SCG', '\"No good will come of this.\"').
card_multiverse_id('break asunder'/'SCG', '47219').

card_in_set('cabal conditioning', 'SCG').
card_original_type('cabal conditioning'/'SCG', 'Sorcery').
card_original_text('cabal conditioning'/'SCG', 'Any number of target players each discards cards from his or her hand equal to the highest converted mana cost among permanents you control.').
card_first_print('cabal conditioning', 'SCG').
card_image_name('cabal conditioning'/'SCG', 'cabal conditioning').
card_uid('cabal conditioning'/'SCG', 'SCG:Cabal Conditioning:cabal conditioning').
card_rarity('cabal conditioning'/'SCG', 'Rare').
card_artist('cabal conditioning'/'SCG', 'Scott M. Fischer').
card_number('cabal conditioning'/'SCG', '56').
card_flavor_text('cabal conditioning'/'SCG', '\"Hear only the Cabal\'s voice.\nSee only the Cabal\'s way.\nSpeak only the Cabal\'s word.\"\n—Cabal mantra').
card_multiverse_id('cabal conditioning'/'SCG', '44491').

card_in_set('cabal interrogator', 'SCG').
card_original_type('cabal interrogator'/'SCG', 'Creature — Zombie Wizard').
card_original_text('cabal interrogator'/'SCG', '{X}{B}, {T}: Target player reveals X cards from his or her hand and you choose one of them. That player discards that card. Play this ability only any time you could play a sorcery.').
card_first_print('cabal interrogator', 'SCG').
card_image_name('cabal interrogator'/'SCG', 'cabal interrogator').
card_uid('cabal interrogator'/'SCG', 'SCG:Cabal Interrogator:cabal interrogator').
card_rarity('cabal interrogator'/'SCG', 'Uncommon').
card_artist('cabal interrogator'/'SCG', 'Tony Szczudlo').
card_number('cabal interrogator'/'SCG', '57').
card_multiverse_id('cabal interrogator'/'SCG', '39750').

card_in_set('call to the grave', 'SCG').
card_original_type('call to the grave'/'SCG', 'Enchantment').
card_original_text('call to the grave'/'SCG', 'At the beginning of each player\'s upkeep, that player sacrifices a non-Zombie creature.\nAt end of turn, if no creatures are in play, sacrifice Call to the Grave.').
card_first_print('call to the grave', 'SCG').
card_image_name('call to the grave'/'SCG', 'call to the grave').
card_uid('call to the grave'/'SCG', 'SCG:Call to the Grave:call to the grave').
card_rarity('call to the grave'/'SCG', 'Rare').
card_artist('call to the grave'/'SCG', 'Daren Bader').
card_number('call to the grave'/'SCG', '58').
card_multiverse_id('call to the grave'/'SCG', '43725').

card_in_set('carbonize', 'SCG').
card_original_type('carbonize'/'SCG', 'Instant').
card_original_text('carbonize'/'SCG', 'Carbonize deals 3 damage to target creature or player. That creature can\'t be regenerated this turn. If the creature would be put into a graveyard this turn, remove it from the game instead.').
card_first_print('carbonize', 'SCG').
card_image_name('carbonize'/'SCG', 'carbonize').
card_uid('carbonize'/'SCG', 'SCG:Carbonize:carbonize').
card_rarity('carbonize'/'SCG', 'Uncommon').
card_artist('carbonize'/'SCG', 'Alex Horley-Orlandelli').
card_number('carbonize'/'SCG', '83').
card_multiverse_id('carbonize'/'SCG', '47226').

card_in_set('carrion feeder', 'SCG').
card_original_type('carrion feeder'/'SCG', 'Creature — Zombie').
card_original_text('carrion feeder'/'SCG', 'Carrion Feeder can\'t block.\nSacrifice a creature: Put a +1/+1 counter on Carrion Feeder.').
card_image_name('carrion feeder'/'SCG', 'carrion feeder').
card_uid('carrion feeder'/'SCG', 'SCG:Carrion Feeder:carrion feeder').
card_rarity('carrion feeder'/'SCG', 'Common').
card_artist('carrion feeder'/'SCG', 'Brian Snõddy').
card_number('carrion feeder'/'SCG', '59').
card_flavor_text('carrion feeder'/'SCG', 'Stinking of rot, it leaps between gravestones in search of its next meal.').
card_multiverse_id('carrion feeder'/'SCG', '43512').

card_in_set('chartooth cougar', 'SCG').
card_original_type('chartooth cougar'/'SCG', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'SCG', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card from your hand: Search your library for a mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('chartooth cougar', 'SCG').
card_image_name('chartooth cougar'/'SCG', 'chartooth cougar').
card_uid('chartooth cougar'/'SCG', 'SCG:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'SCG', 'Common').
card_artist('chartooth cougar'/'SCG', 'Tony Szczudlo').
card_number('chartooth cougar'/'SCG', '84').
card_multiverse_id('chartooth cougar'/'SCG', '44281').

card_in_set('chill haunting', 'SCG').
card_original_type('chill haunting'/'SCG', 'Instant').
card_original_text('chill haunting'/'SCG', 'As an additional cost to play Chill Haunting, remove X creature cards in your graveyard from the game.\nTarget creature gets -X/-X until end of turn.').
card_first_print('chill haunting', 'SCG').
card_image_name('chill haunting'/'SCG', 'chill haunting').
card_uid('chill haunting'/'SCG', 'SCG:Chill Haunting:chill haunting').
card_rarity('chill haunting'/'SCG', 'Uncommon').
card_artist('chill haunting'/'SCG', 'Brian Snõddy').
card_number('chill haunting'/'SCG', '60').
card_multiverse_id('chill haunting'/'SCG', '45863').

card_in_set('claws of wirewood', 'SCG').
card_original_type('claws of wirewood'/'SCG', 'Sorcery').
card_original_text('claws of wirewood'/'SCG', 'Claws of Wirewood deals 3 damage to each creature with flying and each player.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('claws of wirewood', 'SCG').
card_image_name('claws of wirewood'/'SCG', 'claws of wirewood').
card_uid('claws of wirewood'/'SCG', 'SCG:Claws of Wirewood:claws of wirewood').
card_rarity('claws of wirewood'/'SCG', 'Uncommon').
card_artist('claws of wirewood'/'SCG', 'Tony Szczudlo').
card_number('claws of wirewood'/'SCG', '114').
card_flavor_text('claws of wirewood'/'SCG', 'They say the forest has eyes. They never mention its claws.').
card_multiverse_id('claws of wirewood'/'SCG', '43541').

card_in_set('clutch of undeath', 'SCG').
card_original_type('clutch of undeath'/'SCG', 'Enchant Creature').
card_original_text('clutch of undeath'/'SCG', 'Enchanted creature gets +3/+3 as long as it\'s a Zombie. Otherwise, it gets -3/-3.').
card_first_print('clutch of undeath', 'SCG').
card_image_name('clutch of undeath'/'SCG', 'clutch of undeath').
card_uid('clutch of undeath'/'SCG', 'SCG:Clutch of Undeath:clutch of undeath').
card_rarity('clutch of undeath'/'SCG', 'Common').
card_artist('clutch of undeath'/'SCG', 'Greg Hildebrandt').
card_number('clutch of undeath'/'SCG', '61').
card_flavor_text('clutch of undeath'/'SCG', 'The hand of death recognizes its own.').
card_multiverse_id('clutch of undeath'/'SCG', '44264').

card_in_set('coast watcher', 'SCG').
card_original_type('coast watcher'/'SCG', 'Creature — Bird Soldier').
card_original_text('coast watcher'/'SCG', 'Flying, protection from green').
card_first_print('coast watcher', 'SCG').
card_image_name('coast watcher'/'SCG', 'coast watcher').
card_uid('coast watcher'/'SCG', 'SCG:Coast Watcher:coast watcher').
card_rarity('coast watcher'/'SCG', 'Common').
card_artist('coast watcher'/'SCG', 'Luca Zontini').
card_number('coast watcher'/'SCG', '30').
card_flavor_text('coast watcher'/'SCG', 'The aven came to fear the forest\'s clawing branches, but the watchers quickly renewed their courage.').
card_multiverse_id('coast watcher'/'SCG', '47228').

card_in_set('consumptive goo', 'SCG').
card_original_type('consumptive goo'/'SCG', 'Creature — Ooze').
card_original_text('consumptive goo'/'SCG', '{2}{B}{B}: Target creature gets -1/-1 until end of turn. Put a +1/+1 counter on Consumptive Goo.').
card_first_print('consumptive goo', 'SCG').
card_image_name('consumptive goo'/'SCG', 'consumptive goo').
card_uid('consumptive goo'/'SCG', 'SCG:Consumptive Goo:consumptive goo').
card_rarity('consumptive goo'/'SCG', 'Rare').
card_artist('consumptive goo'/'SCG', 'Carl Critchlow').
card_number('consumptive goo'/'SCG', '62').
card_flavor_text('consumptive goo'/'SCG', 'Silent as fog and relentless as plague, it is wet, creeping death.').
card_multiverse_id('consumptive goo'/'SCG', '47029').

card_in_set('daru spiritualist', 'SCG').
card_original_type('daru spiritualist'/'SCG', 'Creature — Cleric').
card_original_text('daru spiritualist'/'SCG', 'Whenever a Cleric you control becomes the target of a spell or ability, it gets +0/+2 until end of turn.').
card_first_print('daru spiritualist', 'SCG').
card_image_name('daru spiritualist'/'SCG', 'daru spiritualist').
card_uid('daru spiritualist'/'SCG', 'SCG:Daru Spiritualist:daru spiritualist').
card_rarity('daru spiritualist'/'SCG', 'Common').
card_artist('daru spiritualist'/'SCG', 'Dave Dorman').
card_number('daru spiritualist'/'SCG', '5').
card_flavor_text('daru spiritualist'/'SCG', 'He lifts the spirits of his people so they may descend in wrath upon their foes.').
card_multiverse_id('daru spiritualist'/'SCG', '45139').

card_in_set('daru warchief', 'SCG').
card_original_type('daru warchief'/'SCG', 'Creature — Soldier').
card_original_text('daru warchief'/'SCG', 'Soldier spells you play cost {1} less to play.\nSoldiers you control get +1/+2.').
card_first_print('daru warchief', 'SCG').
card_image_name('daru warchief'/'SCG', 'daru warchief').
card_uid('daru warchief'/'SCG', 'SCG:Daru Warchief:daru warchief').
card_rarity('daru warchief'/'SCG', 'Uncommon').
card_artist('daru warchief'/'SCG', 'Tim Hildebrandt').
card_number('daru warchief'/'SCG', '6').
card_flavor_text('daru warchief'/'SCG', 'His victories speak for him.').
card_multiverse_id('daru warchief'/'SCG', '43625').

card_in_set('dawn elemental', 'SCG').
card_original_type('dawn elemental'/'SCG', 'Creature — Elemental').
card_original_text('dawn elemental'/'SCG', 'Flying\nPrevent all damage that would be dealt to Dawn Elemental.').
card_first_print('dawn elemental', 'SCG').
card_image_name('dawn elemental'/'SCG', 'dawn elemental').
card_uid('dawn elemental'/'SCG', 'SCG:Dawn Elemental:dawn elemental').
card_rarity('dawn elemental'/'SCG', 'Rare').
card_artist('dawn elemental'/'SCG', 'Anthony S. Waters').
card_number('dawn elemental'/'SCG', '7').
card_flavor_text('dawn elemental'/'SCG', 'It was midnight on the Daru Plains, yet it seemed the sun was rising.').
card_multiverse_id('dawn elemental'/'SCG', '44311').

card_in_set('day of the dragons', 'SCG').
card_original_type('day of the dragons'/'SCG', 'Enchantment').
card_original_text('day of the dragons'/'SCG', 'When Day of the Dragons comes into play, remove all creatures you control from the game. Then put that many 5/5 red Dragon creature tokens with flying into play.\nWhen Day of the Dragons leaves play, sacrifice all Dragons you control. Then return the removed cards to play under your control.').
card_first_print('day of the dragons', 'SCG').
card_image_name('day of the dragons'/'SCG', 'day of the dragons').
card_uid('day of the dragons'/'SCG', 'SCG:Day of the Dragons:day of the dragons').
card_rarity('day of the dragons'/'SCG', 'Rare').
card_artist('day of the dragons'/'SCG', 'Matthew D. Wilson').
card_number('day of the dragons'/'SCG', '31').
card_multiverse_id('day of the dragons'/'SCG', '43602').

card_in_set('death\'s-head buzzard', 'SCG').
card_original_type('death\'s-head buzzard'/'SCG', 'Creature — Bird').
card_original_text('death\'s-head buzzard'/'SCG', 'Flying\nWhen Death\'s-Head Buzzard is put into a graveyard from play, all creatures get -1/-1 until end of turn.').
card_first_print('death\'s-head buzzard', 'SCG').
card_image_name('death\'s-head buzzard'/'SCG', 'death\'s-head buzzard').
card_uid('death\'s-head buzzard'/'SCG', 'SCG:Death\'s-Head Buzzard:death\'s-head buzzard').
card_rarity('death\'s-head buzzard'/'SCG', 'Common').
card_artist('death\'s-head buzzard'/'SCG', 'Marcelo Vignali').
card_number('death\'s-head buzzard'/'SCG', '63').
card_flavor_text('death\'s-head buzzard'/'SCG', 'Infested with vermin, ever hungering, dropping from night\'s sky.').
card_multiverse_id('death\'s-head buzzard'/'SCG', '47223').

card_in_set('decree of annihilation', 'SCG').
card_original_type('decree of annihilation'/'SCG', 'Sorcery').
card_original_text('decree of annihilation'/'SCG', 'Remove all artifacts, creatures, lands, graveyards, and hands from the game.\nCycling {5}{R}{R}\nWhen you cycle Decree of Annihilation, destroy all lands.').
card_first_print('decree of annihilation', 'SCG').
card_image_name('decree of annihilation'/'SCG', 'decree of annihilation').
card_uid('decree of annihilation'/'SCG', 'SCG:Decree of Annihilation:decree of annihilation').
card_rarity('decree of annihilation'/'SCG', 'Rare').
card_artist('decree of annihilation'/'SCG', 'John Avon').
card_number('decree of annihilation'/'SCG', '85').
card_multiverse_id('decree of annihilation'/'SCG', '44335').

card_in_set('decree of justice', 'SCG').
card_original_type('decree of justice'/'SCG', 'Sorcery').
card_original_text('decree of justice'/'SCG', 'Put X 4/4 white Angel creature tokens with flying into play.\nCycling {2}{W}\nWhen you cycle Decree of Justice, you may pay {X}. If you do, put X 1/1 white Soldier creature tokens into play.').
card_image_name('decree of justice'/'SCG', 'decree of justice').
card_uid('decree of justice'/'SCG', 'SCG:Decree of Justice:decree of justice').
card_rarity('decree of justice'/'SCG', 'Rare').
card_artist('decree of justice'/'SCG', 'Adam Rex').
card_number('decree of justice'/'SCG', '8').
card_multiverse_id('decree of justice'/'SCG', '45141').

card_in_set('decree of pain', 'SCG').
card_original_type('decree of pain'/'SCG', 'Sorcery').
card_original_text('decree of pain'/'SCG', 'Destroy all creatures. They can\'t be regenerated. Draw a card for each creature destroyed this way.\nCycling {3}{B}{B}\nWhen you cycle Decree of Pain, all creatures get -2/-2 until end of turn.').
card_first_print('decree of pain', 'SCG').
card_image_name('decree of pain'/'SCG', 'decree of pain').
card_uid('decree of pain'/'SCG', 'SCG:Decree of Pain:decree of pain').
card_rarity('decree of pain'/'SCG', 'Rare').
card_artist('decree of pain'/'SCG', 'Carl Critchlow').
card_number('decree of pain'/'SCG', '64').
card_multiverse_id('decree of pain'/'SCG', '43522').

card_in_set('decree of savagery', 'SCG').
card_original_type('decree of savagery'/'SCG', 'Instant').
card_original_text('decree of savagery'/'SCG', 'Put four +1/+1 counters on each creature you control.\nCycling {4}{G}{G}\nWhen you cycle Decree of Savagery, you may put four +1/+1 counters on target creature.').
card_first_print('decree of savagery', 'SCG').
card_image_name('decree of savagery'/'SCG', 'decree of savagery').
card_uid('decree of savagery'/'SCG', 'SCG:Decree of Savagery:decree of savagery').
card_rarity('decree of savagery'/'SCG', 'Rare').
card_artist('decree of savagery'/'SCG', 'Alex Horley-Orlandelli').
card_number('decree of savagery'/'SCG', '115').
card_multiverse_id('decree of savagery'/'SCG', '46423').

card_in_set('decree of silence', 'SCG').
card_original_type('decree of silence'/'SCG', 'Enchantment').
card_original_text('decree of silence'/'SCG', 'Whenever an opponent plays a spell, counter that spell and put a depletion counter on Decree of Silence. If there are three or more depletion counters on Decree of Silence, sacrifice it.\nCycling {4}{U}{U}\nWhen you cycle Decree of Silence, you may counter target spell.').
card_first_print('decree of silence', 'SCG').
card_image_name('decree of silence'/'SCG', 'decree of silence').
card_uid('decree of silence'/'SCG', 'SCG:Decree of Silence:decree of silence').
card_rarity('decree of silence'/'SCG', 'Rare').
card_artist('decree of silence'/'SCG', 'Adam Rex').
card_number('decree of silence'/'SCG', '32').
card_multiverse_id('decree of silence'/'SCG', '46430').

card_in_set('dimensional breach', 'SCG').
card_original_type('dimensional breach'/'SCG', 'Sorcery').
card_original_text('dimensional breach'/'SCG', 'Remove all permanents from the game. As long as any of those cards remain removed from the game, at the beginning of each player\'s upkeep, that player returns one of the removed cards he or she owns to play.').
card_first_print('dimensional breach', 'SCG').
card_image_name('dimensional breach'/'SCG', 'dimensional breach').
card_uid('dimensional breach'/'SCG', 'SCG:Dimensional Breach:dimensional breach').
card_rarity('dimensional breach'/'SCG', 'Rare').
card_artist('dimensional breach'/'SCG', 'Dave Dorman').
card_number('dimensional breach'/'SCG', '9').
card_multiverse_id('dimensional breach'/'SCG', '43573').

card_in_set('dispersal shield', 'SCG').
card_original_type('dispersal shield'/'SCG', 'Instant').
card_original_text('dispersal shield'/'SCG', 'Counter target spell if its converted mana cost is less than or equal to the highest converted mana cost among permanents you control.').
card_first_print('dispersal shield', 'SCG').
card_image_name('dispersal shield'/'SCG', 'dispersal shield').
card_uid('dispersal shield'/'SCG', 'SCG:Dispersal Shield:dispersal shield').
card_rarity('dispersal shield'/'SCG', 'Common').
card_artist('dispersal shield'/'SCG', 'Dave Dorman').
card_number('dispersal shield'/'SCG', '33').
card_flavor_text('dispersal shield'/'SCG', '\"Maybe next time.\"').
card_multiverse_id('dispersal shield'/'SCG', '35344').

card_in_set('divergent growth', 'SCG').
card_original_type('divergent growth'/'SCG', 'Instant').
card_original_text('divergent growth'/'SCG', 'Until end of turn, lands you control gain \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('divergent growth', 'SCG').
card_image_name('divergent growth'/'SCG', 'divergent growth').
card_uid('divergent growth'/'SCG', 'SCG:Divergent Growth:divergent growth').
card_rarity('divergent growth'/'SCG', 'Common').
card_artist('divergent growth'/'SCG', 'Rob Alexander').
card_number('divergent growth'/'SCG', '116').
card_flavor_text('divergent growth'/'SCG', 'Nature has forgotten its own rules.').
card_multiverse_id('divergent growth'/'SCG', '45864').

card_in_set('dragon breath', 'SCG').
card_original_type('dragon breath'/'SCG', 'Enchant Creature').
card_original_text('dragon breath'/'SCG', 'Enchanted creature has haste.\n{R} Enchanted creature gets +1/+0 until end of turn.\nWhen a creature with converted mana cost 6 or more comes into play, you may return Dragon Breath from your graveyard to play enchanting that creature.').
card_first_print('dragon breath', 'SCG').
card_image_name('dragon breath'/'SCG', 'dragon breath').
card_uid('dragon breath'/'SCG', 'SCG:Dragon Breath:dragon breath').
card_rarity('dragon breath'/'SCG', 'Common').
card_artist('dragon breath'/'SCG', 'Greg Staples').
card_number('dragon breath'/'SCG', '86').
card_multiverse_id('dragon breath'/'SCG', '43533').

card_in_set('dragon fangs', 'SCG').
card_original_type('dragon fangs'/'SCG', 'Enchant Creature').
card_original_text('dragon fangs'/'SCG', 'Enchanted creature gets +1/+1 and has trample.\nWhen a creature with converted mana cost 6 or more comes into play, you may return Dragon Fangs from your graveyard to play enchanting that creature.').
card_first_print('dragon fangs', 'SCG').
card_image_name('dragon fangs'/'SCG', 'dragon fangs').
card_uid('dragon fangs'/'SCG', 'SCG:Dragon Fangs:dragon fangs').
card_rarity('dragon fangs'/'SCG', 'Common').
card_artist('dragon fangs'/'SCG', 'Carl Critchlow').
card_number('dragon fangs'/'SCG', '117').
card_multiverse_id('dragon fangs'/'SCG', '47165').

card_in_set('dragon mage', 'SCG').
card_original_type('dragon mage'/'SCG', 'Creature — Dragon Wizard').
card_original_text('dragon mage'/'SCG', 'Flying\nWhenever Dragon Mage deals combat damage to a player, each player discards his or her hand and draws seven cards.').
card_first_print('dragon mage', 'SCG').
card_image_name('dragon mage'/'SCG', 'dragon mage').
card_uid('dragon mage'/'SCG', 'SCG:Dragon Mage:dragon mage').
card_rarity('dragon mage'/'SCG', 'Rare').
card_artist('dragon mage'/'SCG', 'Matthew D. Wilson').
card_number('dragon mage'/'SCG', '87').
card_flavor_text('dragon mage'/'SCG', '\"You\'ll bend to my will—with or without your precious sanity.\"').
card_multiverse_id('dragon mage'/'SCG', '43569').

card_in_set('dragon scales', 'SCG').
card_original_type('dragon scales'/'SCG', 'Enchant Creature').
card_original_text('dragon scales'/'SCG', 'Enchanted creature gets +1/+2 and attacking doesn\'t cause it to tap.\nWhen a creature with converted mana cost 6 or more comes into play, you may return Dragon Scales from your graveyard to play enchanting that creature.').
card_first_print('dragon scales', 'SCG').
card_image_name('dragon scales'/'SCG', 'dragon scales').
card_uid('dragon scales'/'SCG', 'SCG:Dragon Scales:dragon scales').
card_rarity('dragon scales'/'SCG', 'Common').
card_artist('dragon scales'/'SCG', 'Darrell Riche').
card_number('dragon scales'/'SCG', '10').
card_multiverse_id('dragon scales'/'SCG', '44400').

card_in_set('dragon shadow', 'SCG').
card_original_type('dragon shadow'/'SCG', 'Enchant Creature').
card_original_text('dragon shadow'/'SCG', 'Enchanted creature gets +1/+0 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen a creature with converted mana cost 6 or more comes into play, you may return Dragon Shadow from your graveyard to play enchanting that creature.').
card_first_print('dragon shadow', 'SCG').
card_image_name('dragon shadow'/'SCG', 'dragon shadow').
card_uid('dragon shadow'/'SCG', 'SCG:Dragon Shadow:dragon shadow').
card_rarity('dragon shadow'/'SCG', 'Common').
card_artist('dragon shadow'/'SCG', 'Kev Walker').
card_number('dragon shadow'/'SCG', '65').
card_multiverse_id('dragon shadow'/'SCG', '45155').

card_in_set('dragon tyrant', 'SCG').
card_original_type('dragon tyrant'/'SCG', 'Creature — Dragon').
card_original_text('dragon tyrant'/'SCG', 'Flying, trample\nDouble strike (This creature deals both first-strike and regular combat damage.)\nAt the beginning of your upkeep, sacrifice Dragon Tyrant unless you pay {R}{R}{R}{R}.\n{R}: Dragon Tyrant gets +1/+0 until end of turn.').
card_first_print('dragon tyrant', 'SCG').
card_image_name('dragon tyrant'/'SCG', 'dragon tyrant').
card_uid('dragon tyrant'/'SCG', 'SCG:Dragon Tyrant:dragon tyrant').
card_rarity('dragon tyrant'/'SCG', 'Rare').
card_artist('dragon tyrant'/'SCG', 'Kev Walker').
card_number('dragon tyrant'/'SCG', '88').
card_multiverse_id('dragon tyrant'/'SCG', '43711').

card_in_set('dragon wings', 'SCG').
card_original_type('dragon wings'/'SCG', 'Enchant Creature').
card_original_text('dragon wings'/'SCG', 'Enchanted creature has flying.\nCycling {1}{U} ({1}{U}, Discard this card from your hand: Draw a card.)\nWhen a creature with converted mana cost 6 or more comes into play, you may return Dragon Wings from your graveyard to play enchanting that creature.').
card_first_print('dragon wings', 'SCG').
card_image_name('dragon wings'/'SCG', 'dragon wings').
card_uid('dragon wings'/'SCG', 'SCG:Dragon Wings:dragon wings').
card_rarity('dragon wings'/'SCG', 'Common').
card_artist('dragon wings'/'SCG', 'Darrell Riche').
card_number('dragon wings'/'SCG', '34').
card_multiverse_id('dragon wings'/'SCG', '46427').

card_in_set('dragonspeaker shaman', 'SCG').
card_original_type('dragonspeaker shaman'/'SCG', 'Creature — Barbarian').
card_original_text('dragonspeaker shaman'/'SCG', 'Dragon spells you play cost {2} less to play.').
card_first_print('dragonspeaker shaman', 'SCG').
card_image_name('dragonspeaker shaman'/'SCG', 'dragonspeaker shaman').
card_uid('dragonspeaker shaman'/'SCG', 'SCG:Dragonspeaker Shaman:dragonspeaker shaman').
card_rarity('dragonspeaker shaman'/'SCG', 'Uncommon').
card_artist('dragonspeaker shaman'/'SCG', 'Kev Walker').
card_number('dragonspeaker shaman'/'SCG', '89').
card_flavor_text('dragonspeaker shaman'/'SCG', '\"We speak the dragons\' language of flame and rage. They speak our language of fury and honor. Together we shall weave a tale of destruction without equal.\"').
card_multiverse_id('dragonspeaker shaman'/'SCG', '46615').

card_in_set('dragonstalker', 'SCG').
card_original_type('dragonstalker'/'SCG', 'Creature — Bird Soldier').
card_original_text('dragonstalker'/'SCG', 'Flying, protection from Dragons').
card_first_print('dragonstalker', 'SCG').
card_image_name('dragonstalker'/'SCG', 'dragonstalker').
card_uid('dragonstalker'/'SCG', 'SCG:Dragonstalker:dragonstalker').
card_rarity('dragonstalker'/'SCG', 'Uncommon').
card_artist('dragonstalker'/'SCG', 'Ron Spencer').
card_number('dragonstalker'/'SCG', '11').
card_flavor_text('dragonstalker'/'SCG', '\"When the skies are filled with flames, the brave shall step forward to quench them.\"\n—Order prophecy').
card_multiverse_id('dragonstalker'/'SCG', '39714').

card_in_set('dragonstorm', 'SCG').
card_original_type('dragonstorm'/'SCG', 'Sorcery').
card_original_text('dragonstorm'/'SCG', 'Search your library for a Dragon card and put it into play. Then shuffle your library.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_first_print('dragonstorm', 'SCG').
card_image_name('dragonstorm'/'SCG', 'dragonstorm').
card_uid('dragonstorm'/'SCG', 'SCG:Dragonstorm:dragonstorm').
card_rarity('dragonstorm'/'SCG', 'Rare').
card_artist('dragonstorm'/'SCG', 'Kev Walker').
card_number('dragonstorm'/'SCG', '90').
card_multiverse_id('dragonstorm'/'SCG', '46426').

card_in_set('edgewalker', 'SCG').
card_original_type('edgewalker'/'SCG', 'Creature — Cleric').
card_original_text('edgewalker'/'SCG', 'Cleric spells you play cost {W}{B} less to play. This effect reduces only the amount of colored mana you pay. (For example, if you play a Cleric with mana cost {1}{W}, it costs {1} to play.)').
card_first_print('edgewalker', 'SCG').
card_image_name('edgewalker'/'SCG', 'edgewalker').
card_uid('edgewalker'/'SCG', 'SCG:Edgewalker:edgewalker').
card_rarity('edgewalker'/'SCG', 'Uncommon').
card_artist('edgewalker'/'SCG', 'Ben Thompson').
card_number('edgewalker'/'SCG', '137').
card_multiverse_id('edgewalker'/'SCG', '43513').

card_in_set('elvish aberration', 'SCG').
card_original_type('elvish aberration'/'SCG', 'Creature — Elf Mutant').
card_original_text('elvish aberration'/'SCG', '{T}: Add {G}{G}{G} to your mana pool.\nForestcycling {2} ({2}, Discard this card from your hand: Search your library for a forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('elvish aberration'/'SCG', 'elvish aberration').
card_uid('elvish aberration'/'SCG', 'SCG:Elvish Aberration:elvish aberration').
card_rarity('elvish aberration'/'SCG', 'Uncommon').
card_artist('elvish aberration'/'SCG', 'Matt Cavotta').
card_number('elvish aberration'/'SCG', '118').
card_multiverse_id('elvish aberration'/'SCG', '43538').

card_in_set('enrage', 'SCG').
card_original_type('enrage'/'SCG', 'Instant').
card_original_text('enrage'/'SCG', 'Target creature gets +X/+0 until end of turn.').
card_first_print('enrage', 'SCG').
card_image_name('enrage'/'SCG', 'enrage').
card_uid('enrage'/'SCG', 'SCG:Enrage:enrage').
card_rarity('enrage'/'SCG', 'Uncommon').
card_artist('enrage'/'SCG', 'Justin Sweet').
card_number('enrage'/'SCG', '91').
card_flavor_text('enrage'/'SCG', '\"You wouldn\'t like me when I\'m angry.\"').
card_multiverse_id('enrage'/'SCG', '45844').

card_in_set('eternal dragon', 'SCG').
card_original_type('eternal dragon'/'SCG', 'Creature — Dragon Spirit').
card_original_text('eternal dragon'/'SCG', 'Flying\n{3}{W}{W}: Return Eternal Dragon from your graveyard to your hand. Play this ability only during your upkeep.\nPlainscycling {2} ({2}, Discard this card from your hand: Search your library for a plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('eternal dragon', 'SCG').
card_image_name('eternal dragon'/'SCG', 'eternal dragon').
card_uid('eternal dragon'/'SCG', 'SCG:Eternal Dragon:eternal dragon').
card_rarity('eternal dragon'/'SCG', 'Rare').
card_artist('eternal dragon'/'SCG', 'Justin Sweet').
card_number('eternal dragon'/'SCG', '12').
card_multiverse_id('eternal dragon'/'SCG', '44398').

card_in_set('exiled doomsayer', 'SCG').
card_original_type('exiled doomsayer'/'SCG', 'Creature — Cleric').
card_original_text('exiled doomsayer'/'SCG', 'All morph costs cost {2} more. (This doesn\'t affect the cost to play creatures face down.)').
card_first_print('exiled doomsayer', 'SCG').
card_image_name('exiled doomsayer'/'SCG', 'exiled doomsayer').
card_uid('exiled doomsayer'/'SCG', 'SCG:Exiled Doomsayer:exiled doomsayer').
card_rarity('exiled doomsayer'/'SCG', 'Rare').
card_artist('exiled doomsayer'/'SCG', 'Brian Snõddy').
card_number('exiled doomsayer'/'SCG', '13').
card_flavor_text('exiled doomsayer'/'SCG', 'He\'s desperate to hold back the future because he has seen it.').
card_multiverse_id('exiled doomsayer'/'SCG', '40648').

card_in_set('extra arms', 'SCG').
card_original_type('extra arms'/'SCG', 'Enchant Creature').
card_original_text('extra arms'/'SCG', 'Whenever enchanted creature attacks, it deals 2 damage to target creature or player.').
card_first_print('extra arms', 'SCG').
card_image_name('extra arms'/'SCG', 'extra arms').
card_uid('extra arms'/'SCG', 'SCG:Extra Arms:extra arms').
card_rarity('extra arms'/'SCG', 'Uncommon').
card_artist('extra arms'/'SCG', 'Greg Staples').
card_number('extra arms'/'SCG', '92').
card_flavor_text('extra arms'/'SCG', '\"Perhaps extra heads would have served them better.\"\n—Foothill guide').
card_multiverse_id('extra arms'/'SCG', '43595').

card_in_set('faces of the past', 'SCG').
card_original_type('faces of the past'/'SCG', 'Enchantment').
card_original_text('faces of the past'/'SCG', 'Whenever a creature is put into a graveyard from play, tap or untap all creatures that share a creature type with it.').
card_first_print('faces of the past', 'SCG').
card_image_name('faces of the past'/'SCG', 'faces of the past').
card_uid('faces of the past'/'SCG', 'SCG:Faces of the Past:faces of the past').
card_rarity('faces of the past'/'SCG', 'Rare').
card_artist('faces of the past'/'SCG', 'Wayne England').
card_number('faces of the past'/'SCG', '35').
card_flavor_text('faces of the past'/'SCG', 'The ties that bind can also strangle.').
card_multiverse_id('faces of the past'/'SCG', '45869').

card_in_set('fatal mutation', 'SCG').
card_original_type('fatal mutation'/'SCG', 'Enchant Creature').
card_original_text('fatal mutation'/'SCG', 'When enchanted creature is turned face up, destroy it. It can\'t be regenerated.').
card_first_print('fatal mutation', 'SCG').
card_image_name('fatal mutation'/'SCG', 'fatal mutation').
card_uid('fatal mutation'/'SCG', 'SCG:Fatal Mutation:fatal mutation').
card_rarity('fatal mutation'/'SCG', 'Uncommon').
card_artist('fatal mutation'/'SCG', 'Erica Gassalasca-Jape').
card_number('fatal mutation'/'SCG', '66').
card_flavor_text('fatal mutation'/'SCG', '\"You wear that shell as a mask. Now it\'s your coffin.\"\n—Cabal cleric').
card_multiverse_id('fatal mutation'/'SCG', '44220').

card_in_set('fierce empath', 'SCG').
card_original_type('fierce empath'/'SCG', 'Creature — Elf').
card_original_text('fierce empath'/'SCG', 'When Fierce Empath comes into play, you may search your library for a creature card with converted mana cost 6 or more, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('fierce empath', 'SCG').
card_image_name('fierce empath'/'SCG', 'fierce empath').
card_uid('fierce empath'/'SCG', 'SCG:Fierce Empath:fierce empath').
card_rarity('fierce empath'/'SCG', 'Common').
card_artist('fierce empath'/'SCG', 'Alan Pollack').
card_number('fierce empath'/'SCG', '119').
card_multiverse_id('fierce empath'/'SCG', '44218').

card_in_set('final punishment', 'SCG').
card_original_type('final punishment'/'SCG', 'Sorcery').
card_original_text('final punishment'/'SCG', 'Target player loses life equal to the damage already dealt to him or her this turn.').
card_first_print('final punishment', 'SCG').
card_image_name('final punishment'/'SCG', 'final punishment').
card_uid('final punishment'/'SCG', 'SCG:Final Punishment:final punishment').
card_rarity('final punishment'/'SCG', 'Rare').
card_artist('final punishment'/'SCG', 'Matt Thompson').
card_number('final punishment'/'SCG', '67').
card_flavor_text('final punishment'/'SCG', 'The pain of a lifetime—every scrape, illness, and bruise—condensed into a single moment.').
card_multiverse_id('final punishment'/'SCG', '43611').

card_in_set('force bubble', 'SCG').
card_original_type('force bubble'/'SCG', 'Enchantment').
card_original_text('force bubble'/'SCG', 'If damage would be dealt to you, put that many depletion counters on Force Bubble instead.\nWhen there are four or more depletion counters on Force Bubble, sacrifice it.\nAt end of turn, remove all depletion counters from Force Bubble.').
card_first_print('force bubble', 'SCG').
card_image_name('force bubble'/'SCG', 'force bubble').
card_uid('force bubble'/'SCG', 'SCG:Force Bubble:force bubble').
card_rarity('force bubble'/'SCG', 'Rare').
card_artist('force bubble'/'SCG', 'Alan Pollack').
card_number('force bubble'/'SCG', '14').
card_multiverse_id('force bubble'/'SCG', '45138').

card_in_set('forgotten ancient', 'SCG').
card_original_type('forgotten ancient'/'SCG', 'Creature — Elemental').
card_original_text('forgotten ancient'/'SCG', 'Whenever a player plays a spell, you may put a +1/+1 counter on Forgotten Ancient.\nAt the beginning of your upkeep, you may move any number of +1/+1 counters from Forgotten Ancient onto other creatures.').
card_first_print('forgotten ancient', 'SCG').
card_image_name('forgotten ancient'/'SCG', 'forgotten ancient').
card_uid('forgotten ancient'/'SCG', 'SCG:Forgotten Ancient:forgotten ancient').
card_rarity('forgotten ancient'/'SCG', 'Rare').
card_artist('forgotten ancient'/'SCG', 'Mark Tedin').
card_number('forgotten ancient'/'SCG', '120').
card_flavor_text('forgotten ancient'/'SCG', 'Its blood is life. Its body is growth.').
card_multiverse_id('forgotten ancient'/'SCG', '44219').

card_in_set('form of the dragon', 'SCG').
card_original_type('form of the dragon'/'SCG', 'Enchantment').
card_original_text('form of the dragon'/'SCG', 'At the beginning of your upkeep, Form of the Dragon deals 5 damage to target creature or player.\nAt the end of each turn, your life total becomes 5.\nCreatures without flying can\'t attack you.').
card_first_print('form of the dragon', 'SCG').
card_image_name('form of the dragon'/'SCG', 'form of the dragon').
card_uid('form of the dragon'/'SCG', 'SCG:Form of the Dragon:form of the dragon').
card_rarity('form of the dragon'/'SCG', 'Rare').
card_artist('form of the dragon'/'SCG', 'Carl Critchlow').
card_number('form of the dragon'/'SCG', '93').
card_multiverse_id('form of the dragon'/'SCG', '43566').

card_in_set('frontline strategist', 'SCG').
card_original_type('frontline strategist'/'SCG', 'Creature — Soldier').
card_original_text('frontline strategist'/'SCG', 'Morph {W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Frontline Strategist is turned face up, prevent all combat damage non-Soldiers would deal this turn.').
card_first_print('frontline strategist', 'SCG').
card_image_name('frontline strategist'/'SCG', 'frontline strategist').
card_uid('frontline strategist'/'SCG', 'SCG:Frontline Strategist:frontline strategist').
card_rarity('frontline strategist'/'SCG', 'Common').
card_artist('frontline strategist'/'SCG', 'Christopher Moeller').
card_number('frontline strategist'/'SCG', '15').
card_multiverse_id('frontline strategist'/'SCG', '43493').

card_in_set('frozen solid', 'SCG').
card_original_type('frozen solid'/'SCG', 'Enchant Creature').
card_original_text('frozen solid'/'SCG', 'Enchanted creature doesn\'t untap during its controller\'s untap step.\nWhen damage is dealt to enchanted creature, destroy it.').
card_first_print('frozen solid', 'SCG').
card_image_name('frozen solid'/'SCG', 'frozen solid').
card_uid('frozen solid'/'SCG', 'SCG:Frozen Solid:frozen solid').
card_rarity('frozen solid'/'SCG', 'Common').
card_artist('frozen solid'/'SCG', 'Glen Angus').
card_number('frozen solid'/'SCG', '36').
card_multiverse_id('frozen solid'/'SCG', '43500').

card_in_set('gilded light', 'SCG').
card_original_type('gilded light'/'SCG', 'Instant').
card_original_text('gilded light'/'SCG', 'You can\'t be the target of spells or abilities this turn.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('gilded light', 'SCG').
card_image_name('gilded light'/'SCG', 'gilded light').
card_uid('gilded light'/'SCG', 'SCG:Gilded Light:gilded light').
card_rarity('gilded light'/'SCG', 'Uncommon').
card_artist('gilded light'/'SCG', 'John Avon').
card_number('gilded light'/'SCG', '16').
card_flavor_text('gilded light'/'SCG', '\"Whoever survives the first blow lives to land the second.\"').
card_multiverse_id('gilded light'/'SCG', '43623').

card_in_set('goblin brigand', 'SCG').
card_original_type('goblin brigand'/'SCG', 'Creature — Goblin').
card_original_text('goblin brigand'/'SCG', 'Goblin Brigand attacks each turn if able.').
card_first_print('goblin brigand', 'SCG').
card_image_name('goblin brigand'/'SCG', 'goblin brigand').
card_uid('goblin brigand'/'SCG', 'SCG:Goblin Brigand:goblin brigand').
card_rarity('goblin brigand'/'SCG', 'Common').
card_artist('goblin brigand'/'SCG', 'Arnie Swekel').
card_number('goblin brigand'/'SCG', '94').
card_flavor_text('goblin brigand'/'SCG', 'After the Skirk Ridge collapsed, the goblins erected a system of ropes and pulleys to hold up what was left.').
card_multiverse_id('goblin brigand'/'SCG', '43525').

card_in_set('goblin psychopath', 'SCG').
card_original_type('goblin psychopath'/'SCG', 'Creature — Goblin Mutant').
card_original_text('goblin psychopath'/'SCG', 'Whenever Goblin Psychopath attacks or blocks, flip a coin. If you lose the flip, the next time it would deal combat damage this turn, it deals that damage to you instead.').
card_first_print('goblin psychopath', 'SCG').
card_image_name('goblin psychopath'/'SCG', 'goblin psychopath').
card_uid('goblin psychopath'/'SCG', 'SCG:Goblin Psychopath:goblin psychopath').
card_rarity('goblin psychopath'/'SCG', 'Uncommon').
card_artist('goblin psychopath'/'SCG', 'Pete Venters').
card_number('goblin psychopath'/'SCG', '95').
card_flavor_text('goblin psychopath'/'SCG', 'The destruction he causes is nothing next to the chaos in his mind.').
card_multiverse_id('goblin psychopath'/'SCG', '43567').

card_in_set('goblin war strike', 'SCG').
card_original_type('goblin war strike'/'SCG', 'Sorcery').
card_original_text('goblin war strike'/'SCG', 'Goblin War Strike deals damage equal to the number of Goblins you control to target player.').
card_image_name('goblin war strike'/'SCG', 'goblin war strike').
card_uid('goblin war strike'/'SCG', 'SCG:Goblin War Strike:goblin war strike').
card_rarity('goblin war strike'/'SCG', 'Common').
card_artist('goblin war strike'/'SCG', 'Pete Venters').
card_number('goblin war strike'/'SCG', '96').
card_flavor_text('goblin war strike'/'SCG', '\"Fire, aim, ready!\"').
card_multiverse_id('goblin war strike'/'SCG', '44292').

card_in_set('goblin warchief', 'SCG').
card_original_type('goblin warchief'/'SCG', 'Creature — Goblin').
card_original_text('goblin warchief'/'SCG', 'Goblin spells you play cost {1} less to play.\nGoblins you control have haste.').
card_image_name('goblin warchief'/'SCG', 'goblin warchief').
card_uid('goblin warchief'/'SCG', 'SCG:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'SCG', 'Uncommon').
card_artist('goblin warchief'/'SCG', 'Tim Hildebrandt').
card_number('goblin warchief'/'SCG', '97').
card_flavor_text('goblin warchief'/'SCG', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief'/'SCG', '43591').

card_in_set('grip of chaos', 'SCG').
card_original_type('grip of chaos'/'SCG', 'Enchantment').
card_original_text('grip of chaos'/'SCG', 'Whenever a spell or ability is put onto the stack, reselect its target at random if it has a single target. (Select from among all legal targets.)').
card_first_print('grip of chaos', 'SCG').
card_image_name('grip of chaos'/'SCG', 'grip of chaos').
card_uid('grip of chaos'/'SCG', 'SCG:Grip of Chaos:grip of chaos').
card_rarity('grip of chaos'/'SCG', 'Rare').
card_artist('grip of chaos'/'SCG', 'Mark Tedin').
card_number('grip of chaos'/'SCG', '98').
card_flavor_text('grip of chaos'/'SCG', 'When the world is consumed by chaos, the skilled and the foolish are on equal footing.').
card_multiverse_id('grip of chaos'/'SCG', '47274').

card_in_set('guilty conscience', 'SCG').
card_original_type('guilty conscience'/'SCG', 'Enchant Creature').
card_original_text('guilty conscience'/'SCG', 'Whenever enchanted creature deals damage, Guilty Conscience deals that much damage to enchanted creature.').
card_first_print('guilty conscience', 'SCG').
card_image_name('guilty conscience'/'SCG', 'guilty conscience').
card_uid('guilty conscience'/'SCG', 'SCG:Guilty Conscience:guilty conscience').
card_rarity('guilty conscience'/'SCG', 'Common').
card_artist('guilty conscience'/'SCG', 'Christopher Moeller').
card_number('guilty conscience'/'SCG', '17').
card_flavor_text('guilty conscience'/'SCG', 'Those who most feel guilt don\'t need to, while those who most need to feel guilt never do.\n—Order proverb').
card_multiverse_id('guilty conscience'/'SCG', '46464').

card_in_set('hindering touch', 'SCG').
card_original_type('hindering touch'/'SCG', 'Instant').
card_original_text('hindering touch'/'SCG', 'Counter target spell unless its controller pays {2}.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('hindering touch', 'SCG').
card_image_name('hindering touch'/'SCG', 'hindering touch').
card_uid('hindering touch'/'SCG', 'SCG:Hindering Touch:hindering touch').
card_rarity('hindering touch'/'SCG', 'Common').
card_artist('hindering touch'/'SCG', 'Glen Angus').
card_number('hindering touch'/'SCG', '37').
card_multiverse_id('hindering touch'/'SCG', '45837').

card_in_set('hunting pack', 'SCG').
card_original_type('hunting pack'/'SCG', 'Instant').
card_original_text('hunting pack'/'SCG', 'Put a 4/4 green Beast creature token into play.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_first_print('hunting pack', 'SCG').
card_image_name('hunting pack'/'SCG', 'hunting pack').
card_uid('hunting pack'/'SCG', 'SCG:Hunting Pack:hunting pack').
card_rarity('hunting pack'/'SCG', 'Uncommon').
card_artist('hunting pack'/'SCG', 'Jim Nelson').
card_number('hunting pack'/'SCG', '121').
card_multiverse_id('hunting pack'/'SCG', '45848').

card_in_set('karona\'s zealot', 'SCG').
card_original_type('karona\'s zealot'/'SCG', 'Creature — Cleric').
card_original_text('karona\'s zealot'/'SCG', 'Morph {3}{W}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Karona\'s Zealot is turned face up, all damage that would be dealt to it this turn is dealt to target creature instead.').
card_first_print('karona\'s zealot', 'SCG').
card_image_name('karona\'s zealot'/'SCG', 'karona\'s zealot').
card_uid('karona\'s zealot'/'SCG', 'SCG:Karona\'s Zealot:karona\'s zealot').
card_rarity('karona\'s zealot'/'SCG', 'Uncommon').
card_artist('karona\'s zealot'/'SCG', 'Alan Pollack').
card_number('karona\'s zealot'/'SCG', '18').
card_multiverse_id('karona\'s zealot'/'SCG', '45858').

card_in_set('karona, false god', 'SCG').
card_original_type('karona, false god'/'SCG', 'Creature — Legend').
card_original_text('karona, false god'/'SCG', 'Haste\nAt the beginning of each player\'s upkeep, that player untaps Karona, False God and gains control of it.\nWhenever Karona attacks, creatures of the type of your choice get +3/+3 until end of turn.').
card_first_print('karona, false god', 'SCG').
card_image_name('karona, false god'/'SCG', 'karona, false god').
card_uid('karona, false god'/'SCG', 'SCG:Karona, False God:karona, false god').
card_rarity('karona, false god'/'SCG', 'Rare').
card_artist('karona, false god'/'SCG', 'Matthew D. Wilson').
card_number('karona, false god'/'SCG', '138').
card_multiverse_id('karona, false god'/'SCG', '44263').

card_in_set('krosan drover', 'SCG').
card_original_type('krosan drover'/'SCG', 'Creature — Elf').
card_original_text('krosan drover'/'SCG', 'Creature spells you play with converted mana cost 6 or more cost {2} less to play.').
card_first_print('krosan drover', 'SCG').
card_image_name('krosan drover'/'SCG', 'krosan drover').
card_uid('krosan drover'/'SCG', 'SCG:Krosan Drover:krosan drover').
card_rarity('krosan drover'/'SCG', 'Common').
card_artist('krosan drover'/'SCG', 'Arnie Swekel').
card_number('krosan drover'/'SCG', '122').
card_flavor_text('krosan drover'/'SCG', '\"Sit.\"').
card_multiverse_id('krosan drover'/'SCG', '44320').

card_in_set('krosan warchief', 'SCG').
card_original_type('krosan warchief'/'SCG', 'Creature — Beast').
card_original_text('krosan warchief'/'SCG', 'Beast spells you play cost {1} less to play.\n{1}{G}: Regenerate target Beast.').
card_image_name('krosan warchief'/'SCG', 'krosan warchief').
card_uid('krosan warchief'/'SCG', 'SCG:Krosan Warchief:krosan warchief').
card_rarity('krosan warchief'/'SCG', 'Uncommon').
card_artist('krosan warchief'/'SCG', 'Greg Hildebrandt').
card_number('krosan warchief'/'SCG', '123').
card_flavor_text('krosan warchief'/'SCG', 'It turns prey into predator.').
card_multiverse_id('krosan warchief'/'SCG', '43535').

card_in_set('kurgadon', 'SCG').
card_original_type('kurgadon'/'SCG', 'Creature — Beast').
card_original_text('kurgadon'/'SCG', 'Whenever you play a creature spell with converted mana cost 6 or more, put three +1/+1 counters on Kurgadon.').
card_first_print('kurgadon', 'SCG').
card_image_name('kurgadon'/'SCG', 'kurgadon').
card_uid('kurgadon'/'SCG', 'SCG:Kurgadon:kurgadon').
card_rarity('kurgadon'/'SCG', 'Uncommon').
card_artist('kurgadon'/'SCG', 'Carl Critchlow').
card_number('kurgadon'/'SCG', '124').
card_flavor_text('kurgadon'/'SCG', 'When it walks through Krosa, trees step aside, leaves take wing, and the ground shudders.').
card_multiverse_id('kurgadon'/'SCG', '45165').

card_in_set('lethal vapors', 'SCG').
card_original_type('lethal vapors'/'SCG', 'Enchantment').
card_original_text('lethal vapors'/'SCG', 'Whenever a creature comes into play, destroy it.\n{0}: Destroy Lethal Vapors. You skip your next turn. Any player may play this ability.').
card_first_print('lethal vapors', 'SCG').
card_image_name('lethal vapors'/'SCG', 'lethal vapors').
card_uid('lethal vapors'/'SCG', 'SCG:Lethal Vapors:lethal vapors').
card_rarity('lethal vapors'/'SCG', 'Rare').
card_artist('lethal vapors'/'SCG', 'John Avon').
card_number('lethal vapors'/'SCG', '68').
card_flavor_text('lethal vapors'/'SCG', 'The vapors infiltrate every crevice, poison every lung, and snuff out every life.').
card_multiverse_id('lethal vapors'/'SCG', '47586').

card_in_set('lingering death', 'SCG').
card_original_type('lingering death'/'SCG', 'Enchant Creature').
card_original_text('lingering death'/'SCG', 'The controller of enchanted creature sacrifices it at the end of his or her turn.').
card_first_print('lingering death', 'SCG').
card_image_name('lingering death'/'SCG', 'lingering death').
card_uid('lingering death'/'SCG', 'SCG:Lingering Death:lingering death').
card_rarity('lingering death'/'SCG', 'Common').
card_artist('lingering death'/'SCG', 'Matt Thompson').
card_number('lingering death'/'SCG', '69').
card_flavor_text('lingering death'/'SCG', '\"Looks bad. I don\'t know if he\'ll make it through the night.\"\n—Cabal cleric').
card_multiverse_id('lingering death'/'SCG', '43727').

card_in_set('long-term plans', 'SCG').
card_original_type('long-term plans'/'SCG', 'Instant').
card_original_text('long-term plans'/'SCG', 'Search your library for a card, shuffle your library, then put that card third from the top.').
card_first_print('long-term plans', 'SCG').
card_image_name('long-term plans'/'SCG', 'long-term plans').
card_uid('long-term plans'/'SCG', 'SCG:Long-Term Plans:long-term plans').
card_rarity('long-term plans'/'SCG', 'Uncommon').
card_artist('long-term plans'/'SCG', 'Ben Thompson').
card_number('long-term plans'/'SCG', '38').
card_flavor_text('long-term plans'/'SCG', '\"Wait, it\'ll come to me in a minute.\"').
card_multiverse_id('long-term plans'/'SCG', '43605').

card_in_set('mercurial kite', 'SCG').
card_original_type('mercurial kite'/'SCG', 'Creature — Bird').
card_original_text('mercurial kite'/'SCG', 'Flying\nWhenever Mercurial Kite deals combat damage to a creature, tap that creature. It doesn\'t untap during its controller\'s next untap step.').
card_first_print('mercurial kite', 'SCG').
card_image_name('mercurial kite'/'SCG', 'mercurial kite').
card_uid('mercurial kite'/'SCG', 'SCG:Mercurial Kite:mercurial kite').
card_rarity('mercurial kite'/'SCG', 'Common').
card_artist('mercurial kite'/'SCG', 'Richard Sardinha').
card_number('mercurial kite'/'SCG', '39').
card_multiverse_id('mercurial kite'/'SCG', '47222').

card_in_set('metamorphose', 'SCG').
card_original_type('metamorphose'/'SCG', 'Instant').
card_original_text('metamorphose'/'SCG', 'Put target permanent an opponent controls on top of its owner\'s library. That opponent may put an artifact, creature, enchantment, or land card from his or her hand into play.').
card_first_print('metamorphose', 'SCG').
card_image_name('metamorphose'/'SCG', 'metamorphose').
card_uid('metamorphose'/'SCG', 'SCG:Metamorphose:metamorphose').
card_rarity('metamorphose'/'SCG', 'Uncommon').
card_artist('metamorphose'/'SCG', 'Ron Spencer').
card_number('metamorphose'/'SCG', '40').
card_flavor_text('metamorphose'/'SCG', 'If it\'s not one thing, it\'s another.').
card_multiverse_id('metamorphose'/'SCG', '43733').

card_in_set('mind\'s desire', 'SCG').
card_original_type('mind\'s desire'/'SCG', 'Sorcery').
card_original_text('mind\'s desire'/'SCG', 'Shuffle your library. Then remove the top card of your library from the game. Until end of turn, you may play it as though it were in your hand without paying its mana cost. (If it has X in its mana cost, X is 0.)\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_image_name('mind\'s desire'/'SCG', 'mind\'s desire').
card_uid('mind\'s desire'/'SCG', 'SCG:Mind\'s Desire:mind\'s desire').
card_rarity('mind\'s desire'/'SCG', 'Rare').
card_artist('mind\'s desire'/'SCG', 'Ron Spencer').
card_number('mind\'s desire'/'SCG', '41').
card_multiverse_id('mind\'s desire'/'SCG', '46424').

card_in_set('mischievous quanar', 'SCG').
card_original_type('mischievous quanar'/'SCG', 'Creature — Beast').
card_original_text('mischievous quanar'/'SCG', '{3}{U}{U}: Turn Mischievous Quanar face down.\nMorph {1}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Mischievous Quanar is turned face up, copy target instant or sorcery spell. You may choose new targets for that copy.').
card_first_print('mischievous quanar', 'SCG').
card_image_name('mischievous quanar'/'SCG', 'mischievous quanar').
card_uid('mischievous quanar'/'SCG', 'SCG:Mischievous Quanar:mischievous quanar').
card_rarity('mischievous quanar'/'SCG', 'Rare').
card_artist('mischievous quanar'/'SCG', 'Lars Grant-West').
card_number('mischievous quanar'/'SCG', '42').
card_multiverse_id('mischievous quanar'/'SCG', '44403').

card_in_set('misguided rage', 'SCG').
card_original_type('misguided rage'/'SCG', 'Sorcery').
card_original_text('misguided rage'/'SCG', 'Target player sacrifices a permanent.').
card_first_print('misguided rage', 'SCG').
card_image_name('misguided rage'/'SCG', 'misguided rage').
card_uid('misguided rage'/'SCG', 'SCG:Misguided Rage:misguided rage').
card_rarity('misguided rage'/'SCG', 'Common').
card_artist('misguided rage'/'SCG', 'Michael Sutfin').
card_number('misguided rage'/'SCG', '99').
card_flavor_text('misguided rage'/'SCG', 'Only when the anger passed did Varv realize he had burned down his home, destroyed his weapons, and killed his friend Furt.').
card_multiverse_id('misguided rage'/'SCG', '43555').

card_in_set('mistform warchief', 'SCG').
card_original_type('mistform warchief'/'SCG', 'Creature — Illusion').
card_original_text('mistform warchief'/'SCG', 'Creature spells you play that share a creature type with Mistform Warchief cost {1} less to play.\n{T}: Mistform Warchief\'s type becomes the creature type of your choice until end of turn.').
card_first_print('mistform warchief', 'SCG').
card_image_name('mistform warchief'/'SCG', 'mistform warchief').
card_uid('mistform warchief'/'SCG', 'SCG:Mistform Warchief:mistform warchief').
card_rarity('mistform warchief'/'SCG', 'Uncommon').
card_artist('mistform warchief'/'SCG', 'Greg Hildebrandt').
card_number('mistform warchief'/'SCG', '43').
card_multiverse_id('mistform warchief'/'SCG', '47218').

card_in_set('nefashu', 'SCG').
card_original_type('nefashu'/'SCG', 'Creature — Zombie Mutant').
card_original_text('nefashu'/'SCG', 'Whenever Nefashu attacks, up to five target creatures each get -1/-1 until end of turn.').
card_first_print('nefashu', 'SCG').
card_image_name('nefashu'/'SCG', 'nefashu').
card_uid('nefashu'/'SCG', 'SCG:Nefashu:nefashu').
card_rarity('nefashu'/'SCG', 'Rare').
card_artist('nefashu'/'SCG', 'rk post').
card_number('nefashu'/'SCG', '70').
card_flavor_text('nefashu'/'SCG', 'Where the nefashu pass, blood rolls like a silk carpet.').
card_multiverse_id('nefashu'/'SCG', '43616').

card_in_set('noble templar', 'SCG').
card_original_type('noble templar'/'SCG', 'Creature — Cleric Soldier').
card_original_text('noble templar'/'SCG', 'Attacking doesn\'t cause Noble Templar to tap.\nPlainscycling {2} ({2}, Discard this card from your hand: Search your library for a plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('noble templar', 'SCG').
card_image_name('noble templar'/'SCG', 'noble templar').
card_uid('noble templar'/'SCG', 'SCG:Noble Templar:noble templar').
card_rarity('noble templar'/'SCG', 'Common').
card_artist('noble templar'/'SCG', 'Alex Horley-Orlandelli').
card_number('noble templar'/'SCG', '19').
card_multiverse_id('noble templar'/'SCG', '45834').

card_in_set('one with nature', 'SCG').
card_original_type('one with nature'/'SCG', 'Enchant Creature').
card_original_text('one with nature'/'SCG', 'Whenever enchanted creature deals combat damage to a player, you may search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('one with nature', 'SCG').
card_image_name('one with nature'/'SCG', 'one with nature').
card_uid('one with nature'/'SCG', 'SCG:One with Nature:one with nature').
card_rarity('one with nature'/'SCG', 'Uncommon').
card_artist('one with nature'/'SCG', 'Daren Bader').
card_number('one with nature'/'SCG', '125').
card_multiverse_id('one with nature'/'SCG', '43543').

card_in_set('parallel thoughts', 'SCG').
card_original_type('parallel thoughts'/'SCG', 'Enchantment').
card_original_text('parallel thoughts'/'SCG', 'When Parallel Thoughts comes into play, search your library for seven cards, remove them from the game in a face-down pile, and shuffle that pile. Then shuffle your library.\nIf you would draw a card, you may instead put the top card of the pile you removed into your hand.').
card_first_print('parallel thoughts', 'SCG').
card_image_name('parallel thoughts'/'SCG', 'parallel thoughts').
card_uid('parallel thoughts'/'SCG', 'SCG:Parallel Thoughts:parallel thoughts').
card_rarity('parallel thoughts'/'SCG', 'Rare').
card_artist('parallel thoughts'/'SCG', 'Ben Thompson').
card_number('parallel thoughts'/'SCG', '44').
card_multiverse_id('parallel thoughts'/'SCG', '43604').

card_in_set('pemmin\'s aura', 'SCG').
card_original_type('pemmin\'s aura'/'SCG', 'Enchant Creature').
card_original_text('pemmin\'s aura'/'SCG', '{U}: Untap enchanted creature.\n{U}: Enchanted creature gains flying until end of turn.\n{U}: Enchanted creature can\'t be the target of spells or abilities this turn.\n{1}: Enchanted creature gets +1/-1 or -1/+1 until end of turn.').
card_first_print('pemmin\'s aura', 'SCG').
card_image_name('pemmin\'s aura'/'SCG', 'pemmin\'s aura').
card_uid('pemmin\'s aura'/'SCG', 'SCG:Pemmin\'s Aura:pemmin\'s aura').
card_rarity('pemmin\'s aura'/'SCG', 'Uncommon').
card_artist('pemmin\'s aura'/'SCG', 'Greg Staples').
card_number('pemmin\'s aura'/'SCG', '45').
card_multiverse_id('pemmin\'s aura'/'SCG', '43581').

card_in_set('primitive etchings', 'SCG').
card_original_type('primitive etchings'/'SCG', 'Enchantment').
card_original_text('primitive etchings'/'SCG', 'Reveal the first card you draw each turn. Whenever you reveal a creature card this way, draw a card.').
card_first_print('primitive etchings', 'SCG').
card_image_name('primitive etchings'/'SCG', 'primitive etchings').
card_uid('primitive etchings'/'SCG', 'SCG:Primitive Etchings:primitive etchings').
card_rarity('primitive etchings'/'SCG', 'Rare').
card_artist('primitive etchings'/'SCG', 'David Martin').
card_number('primitive etchings'/'SCG', '126').
card_flavor_text('primitive etchings'/'SCG', 'Ancient carvings in the trees of Krosa glow with power once again.').
card_multiverse_id('primitive etchings'/'SCG', '46465').

card_in_set('proteus machine', 'SCG').
card_original_type('proteus machine'/'SCG', 'Artifact Creature').
card_original_text('proteus machine'/'SCG', 'Morph {0} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Proteus Machine is turned face up, its type becomes the creature type of your choice. (This effect doesn\'t end at end of turn.)').
card_first_print('proteus machine', 'SCG').
card_image_name('proteus machine'/'SCG', 'proteus machine').
card_uid('proteus machine'/'SCG', 'SCG:Proteus Machine:proteus machine').
card_rarity('proteus machine'/'SCG', 'Uncommon').
card_artist('proteus machine'/'SCG', 'Greg Staples').
card_number('proteus machine'/'SCG', '141').
card_multiverse_id('proteus machine'/'SCG', '46705').

card_in_set('putrid raptor', 'SCG').
card_original_type('putrid raptor'/'SCG', 'Creature — Zombie Beast').
card_original_text('putrid raptor'/'SCG', 'Morph—Discard a Zombie card from your hand. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('putrid raptor', 'SCG').
card_image_name('putrid raptor'/'SCG', 'putrid raptor').
card_uid('putrid raptor'/'SCG', 'SCG:Putrid Raptor:putrid raptor').
card_rarity('putrid raptor'/'SCG', 'Uncommon').
card_artist('putrid raptor'/'SCG', 'Pete Venters').
card_number('putrid raptor'/'SCG', '71').
card_flavor_text('putrid raptor'/'SCG', 'It eagerly gobbles up bits of its own rotting flesh as they fall to the swampy ground.').
card_multiverse_id('putrid raptor'/'SCG', '45861').

card_in_set('pyrostatic pillar', 'SCG').
card_original_type('pyrostatic pillar'/'SCG', 'Enchantment').
card_original_text('pyrostatic pillar'/'SCG', 'Whenever a player plays a spell with converted mana cost 3 or less, Pyrostatic Pillar deals 2 damage to that player.').
card_first_print('pyrostatic pillar', 'SCG').
card_image_name('pyrostatic pillar'/'SCG', 'pyrostatic pillar').
card_uid('pyrostatic pillar'/'SCG', 'SCG:Pyrostatic Pillar:pyrostatic pillar').
card_rarity('pyrostatic pillar'/'SCG', 'Uncommon').
card_artist('pyrostatic pillar'/'SCG', 'Pete Venters').
card_number('pyrostatic pillar'/'SCG', '100').
card_multiverse_id('pyrostatic pillar'/'SCG', '44290').

card_in_set('rain of blades', 'SCG').
card_original_type('rain of blades'/'SCG', 'Instant').
card_original_text('rain of blades'/'SCG', 'Rain of Blades deals 1 damage to each attacking creature.').
card_first_print('rain of blades', 'SCG').
card_image_name('rain of blades'/'SCG', 'rain of blades').
card_uid('rain of blades'/'SCG', 'SCG:Rain of Blades:rain of blades').
card_rarity('rain of blades'/'SCG', 'Uncommon').
card_artist('rain of blades'/'SCG', 'Rob Alexander').
card_number('rain of blades'/'SCG', '20').
card_flavor_text('rain of blades'/'SCG', 'Some say they are the weapons of heroes fallen in battle, eager for one last chance at glory.').
card_multiverse_id('rain of blades'/'SCG', '43622').

card_in_set('raven guild initiate', 'SCG').
card_original_type('raven guild initiate'/'SCG', 'Creature — Wizard').
card_original_text('raven guild initiate'/'SCG', 'Morph—Return a Bird you control to its owner\'s hand. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('raven guild initiate', 'SCG').
card_image_name('raven guild initiate'/'SCG', 'raven guild initiate').
card_uid('raven guild initiate'/'SCG', 'SCG:Raven Guild Initiate:raven guild initiate').
card_rarity('raven guild initiate'/'SCG', 'Common').
card_artist('raven guild initiate'/'SCG', 'Christopher Moeller').
card_number('raven guild initiate'/'SCG', '46').
card_flavor_text('raven guild initiate'/'SCG', 'The Raven Guild soars on winds of unease.').
card_multiverse_id('raven guild initiate'/'SCG', '44337').

card_in_set('raven guild master', 'SCG').
card_original_type('raven guild master'/'SCG', 'Creature — Wizard Mutant').
card_original_text('raven guild master'/'SCG', 'Whenever Raven Guild Master deals combat damage to a player, that player removes the top ten cards of his or her library from the game.\nMorph {2}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('raven guild master', 'SCG').
card_image_name('raven guild master'/'SCG', 'raven guild master').
card_uid('raven guild master'/'SCG', 'SCG:Raven Guild Master:raven guild master').
card_rarity('raven guild master'/'SCG', 'Rare').
card_artist('raven guild master'/'SCG', 'Kev Walker').
card_number('raven guild master'/'SCG', '47').
card_multiverse_id('raven guild master'/'SCG', '43582').

card_in_set('reaping the graves', 'SCG').
card_original_type('reaping the graves'/'SCG', 'Instant').
card_original_text('reaping the graves'/'SCG', 'Return target creature card from your graveyard to your hand.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('reaping the graves', 'SCG').
card_image_name('reaping the graves'/'SCG', 'reaping the graves').
card_uid('reaping the graves'/'SCG', 'SCG:Reaping the Graves:reaping the graves').
card_rarity('reaping the graves'/'SCG', 'Common').
card_artist('reaping the graves'/'SCG', 'Ron Spencer').
card_number('reaping the graves'/'SCG', '72').
card_multiverse_id('reaping the graves'/'SCG', '45845').

card_in_set('recuperate', 'SCG').
card_original_type('recuperate'/'SCG', 'Instant').
card_original_text('recuperate'/'SCG', 'Choose one You gain 6 life; or prevent the next 6 damage that would be dealt to target creature this turn.').
card_first_print('recuperate', 'SCG').
card_image_name('recuperate'/'SCG', 'recuperate').
card_uid('recuperate'/'SCG', 'SCG:Recuperate:recuperate').
card_rarity('recuperate'/'SCG', 'Common').
card_artist('recuperate'/'SCG', 'Tim Hildebrandt').
card_number('recuperate'/'SCG', '21').
card_flavor_text('recuperate'/'SCG', '\"Rise once more, so you may continue to destroy our enemies.\"\n—Karona, false god').
card_multiverse_id('recuperate'/'SCG', '47590').

card_in_set('reward the faithful', 'SCG').
card_original_type('reward the faithful'/'SCG', 'Instant').
card_original_text('reward the faithful'/'SCG', 'Any number of target players each gains life equal to the highest converted mana cost among permanents you control.').
card_first_print('reward the faithful', 'SCG').
card_image_name('reward the faithful'/'SCG', 'reward the faithful').
card_uid('reward the faithful'/'SCG', 'SCG:Reward the Faithful:reward the faithful').
card_rarity('reward the faithful'/'SCG', 'Uncommon').
card_artist('reward the faithful'/'SCG', 'Matt Cavotta').
card_number('reward the faithful'/'SCG', '22').
card_flavor_text('reward the faithful'/'SCG', '\"When you have drunk your fill, pass the cup to others who thirst.\"').
card_multiverse_id('reward the faithful'/'SCG', '44401').

card_in_set('riptide survivor', 'SCG').
card_original_type('riptide survivor'/'SCG', 'Creature — Wizard').
card_original_text('riptide survivor'/'SCG', 'Morph {1}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Riptide Survivor is turned face up, discard two cards from your hand, then draw three cards.').
card_first_print('riptide survivor', 'SCG').
card_image_name('riptide survivor'/'SCG', 'riptide survivor').
card_uid('riptide survivor'/'SCG', 'SCG:Riptide Survivor:riptide survivor').
card_rarity('riptide survivor'/'SCG', 'Uncommon').
card_artist('riptide survivor'/'SCG', 'Thomas M. Baxa').
card_number('riptide survivor'/'SCG', '48').
card_multiverse_id('riptide survivor'/'SCG', '43578').

card_in_set('rock jockey', 'SCG').
card_original_type('rock jockey'/'SCG', 'Creature — Goblin').
card_original_text('rock jockey'/'SCG', 'You can\'t play Rock Jockey if you played a land this turn.\nYou can\'t play lands if you played Rock Jockey this turn.').
card_first_print('rock jockey', 'SCG').
card_image_name('rock jockey'/'SCG', 'rock jockey').
card_uid('rock jockey'/'SCG', 'SCG:Rock Jockey:rock jockey').
card_rarity('rock jockey'/'SCG', 'Common').
card_artist('rock jockey'/'SCG', 'Glen Angus').
card_number('rock jockey'/'SCG', '101').
card_flavor_text('rock jockey'/'SCG', 'Goblins don\'t know much about physics, but they know lots about falling and rocks.').
card_multiverse_id('rock jockey'/'SCG', '22011').

card_in_set('root elemental', 'SCG').
card_original_type('root elemental'/'SCG', 'Creature — Elemental').
card_original_text('root elemental'/'SCG', 'Morph {5}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Root Elemental is turned face up, you may put a creature card from your hand into play.').
card_first_print('root elemental', 'SCG').
card_image_name('root elemental'/'SCG', 'root elemental').
card_uid('root elemental'/'SCG', 'SCG:Root Elemental:root elemental').
card_rarity('root elemental'/'SCG', 'Rare').
card_artist('root elemental'/'SCG', 'Anthony S. Waters').
card_number('root elemental'/'SCG', '127').
card_multiverse_id('root elemental'/'SCG', '44333').

card_in_set('rush of knowledge', 'SCG').
card_original_type('rush of knowledge'/'SCG', 'Sorcery').
card_original_text('rush of knowledge'/'SCG', 'Draw cards equal to the highest converted mana cost among permanents you control.').
card_first_print('rush of knowledge', 'SCG').
card_image_name('rush of knowledge'/'SCG', 'rush of knowledge').
card_uid('rush of knowledge'/'SCG', 'SCG:Rush of Knowledge:rush of knowledge').
card_rarity('rush of knowledge'/'SCG', 'Common').
card_artist('rush of knowledge'/'SCG', 'Eric Peterson').
card_number('rush of knowledge'/'SCG', '49').
card_flavor_text('rush of knowledge'/'SCG', '\"Limitless power is glorious until you gain limitless understanding.\"\n—Ixidor, reality sculptor').
card_multiverse_id('rush of knowledge'/'SCG', '43729').

card_in_set('scattershot', 'SCG').
card_original_type('scattershot'/'SCG', 'Instant').
card_original_text('scattershot'/'SCG', 'Scattershot deals 1 damage to target creature.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('scattershot', 'SCG').
card_image_name('scattershot'/'SCG', 'scattershot').
card_uid('scattershot'/'SCG', 'SCG:Scattershot:scattershot').
card_rarity('scattershot'/'SCG', 'Common').
card_artist('scattershot'/'SCG', 'Glen Angus').
card_number('scattershot'/'SCG', '102').
card_multiverse_id('scattershot'/'SCG', '45843').

card_in_set('scornful egotist', 'SCG').
card_original_type('scornful egotist'/'SCG', 'Creature — Wizard').
card_original_text('scornful egotist'/'SCG', 'Morph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('scornful egotist', 'SCG').
card_image_name('scornful egotist'/'SCG', 'scornful egotist').
card_uid('scornful egotist'/'SCG', 'SCG:Scornful Egotist:scornful egotist').
card_rarity('scornful egotist'/'SCG', 'Common').
card_artist('scornful egotist'/'SCG', 'Jim Nelson').
card_number('scornful egotist'/'SCG', '50').
card_flavor_text('scornful egotist'/'SCG', '\"Once I was human. Now I am far more.\"').
card_multiverse_id('scornful egotist'/'SCG', '46509').

card_in_set('shoreline ranger', 'SCG').
card_original_type('shoreline ranger'/'SCG', 'Creature — Bird Soldier').
card_original_text('shoreline ranger'/'SCG', 'Flying\nIslandcycling {2} ({2}, Discard this card from your hand: Search your library for an island card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('shoreline ranger', 'SCG').
card_image_name('shoreline ranger'/'SCG', 'shoreline ranger').
card_uid('shoreline ranger'/'SCG', 'SCG:Shoreline Ranger:shoreline ranger').
card_rarity('shoreline ranger'/'SCG', 'Common').
card_artist('shoreline ranger'/'SCG', 'Michael Sutfin').
card_number('shoreline ranger'/'SCG', '51').
card_multiverse_id('shoreline ranger'/'SCG', '45841').

card_in_set('siege-gang commander', 'SCG').
card_original_type('siege-gang commander'/'SCG', 'Creature — Goblin').
card_original_text('siege-gang commander'/'SCG', 'When Siege-Gang Commander comes into play, put three 1/1 red Goblin creature tokens into play.\n{1}{R}, Sacrifice a Goblin: Siege-Gang Commander deals 2 damage to target creature or player.').
card_first_print('siege-gang commander', 'SCG').
card_image_name('siege-gang commander'/'SCG', 'siege-gang commander').
card_uid('siege-gang commander'/'SCG', 'SCG:Siege-Gang Commander:siege-gang commander').
card_rarity('siege-gang commander'/'SCG', 'Rare').
card_artist('siege-gang commander'/'SCG', 'Christopher Moeller').
card_number('siege-gang commander'/'SCG', '103').
card_multiverse_id('siege-gang commander'/'SCG', '43552').

card_in_set('silver knight', 'SCG').
card_original_type('silver knight'/'SCG', 'Creature — Knight').
card_original_text('silver knight'/'SCG', 'First strike, protection from red').
card_image_name('silver knight'/'SCG', 'silver knight').
card_uid('silver knight'/'SCG', 'SCG:Silver Knight:silver knight').
card_rarity('silver knight'/'SCG', 'Uncommon').
card_artist('silver knight'/'SCG', 'Eric Peterson').
card_number('silver knight'/'SCG', '23').
card_flavor_text('silver knight'/'SCG', 'Otaria\'s last defense against the wave of chaos threatening to engulf it.').
card_multiverse_id('silver knight'/'SCG', '44313').

card_in_set('skirk volcanist', 'SCG').
card_original_type('skirk volcanist'/'SCG', 'Creature — Goblin').
card_original_text('skirk volcanist'/'SCG', 'Morph—Sacrifice two mountains. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Skirk Volcanist is turned face up, it deals 3 damage divided as you choose among any number of target creatures.').
card_first_print('skirk volcanist', 'SCG').
card_image_name('skirk volcanist'/'SCG', 'skirk volcanist').
card_uid('skirk volcanist'/'SCG', 'SCG:Skirk Volcanist:skirk volcanist').
card_rarity('skirk volcanist'/'SCG', 'Uncommon').
card_artist('skirk volcanist'/'SCG', 'Matt Cavotta').
card_number('skirk volcanist'/'SCG', '104').
card_multiverse_id('skirk volcanist'/'SCG', '43590').

card_in_set('skulltap', 'SCG').
card_original_type('skulltap'/'SCG', 'Sorcery').
card_original_text('skulltap'/'SCG', 'As an additional cost to play Skulltap, sacrifice a creature.\nDraw two cards.').
card_first_print('skulltap', 'SCG').
card_image_name('skulltap'/'SCG', 'skulltap').
card_uid('skulltap'/'SCG', 'SCG:Skulltap:skulltap').
card_rarity('skulltap'/'SCG', 'Common').
card_artist('skulltap'/'SCG', 'Adam Rex').
card_number('skulltap'/'SCG', '73').
card_flavor_text('skulltap'/'SCG', '\"Finally, a use for that head of yours.\"\n—Braids, dementia summoner').
card_multiverse_id('skulltap'/'SCG', '43734').

card_in_set('sliver overlord', 'SCG').
card_original_type('sliver overlord'/'SCG', 'Creature — Sliver Mutant Legend').
card_original_text('sliver overlord'/'SCG', '{3}: Search your library for a Sliver card, reveal that card, and put it into your hand. Then shuffle your library.\n{3}: Gain control of target Sliver. (This effect doesn\'t end at end of turn.)').
card_first_print('sliver overlord', 'SCG').
card_image_name('sliver overlord'/'SCG', 'sliver overlord').
card_uid('sliver overlord'/'SCG', 'SCG:Sliver Overlord:sliver overlord').
card_rarity('sliver overlord'/'SCG', 'Rare').
card_artist('sliver overlord'/'SCG', 'Tony Szczudlo').
card_number('sliver overlord'/'SCG', '139').
card_flavor_text('sliver overlord'/'SCG', 'The end of evolution.').
card_multiverse_id('sliver overlord'/'SCG', '45166').

card_in_set('soul collector', 'SCG').
card_original_type('soul collector'/'SCG', 'Creature — Vampire').
card_original_text('soul collector'/'SCG', 'Flying\nWhenever a creature dealt damage by Soul Collector this turn is put into a graveyard, return that card to play under your control.\nMorph {B}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('soul collector'/'SCG', 'soul collector').
card_uid('soul collector'/'SCG', 'SCG:Soul Collector:soul collector').
card_rarity('soul collector'/'SCG', 'Rare').
card_artist('soul collector'/'SCG', 'Matthew D. Wilson').
card_number('soul collector'/'SCG', '74').
card_multiverse_id('soul collector'/'SCG', '43615').

card_in_set('spark spray', 'SCG').
card_original_type('spark spray'/'SCG', 'Instant').
card_original_text('spark spray'/'SCG', 'Spark Spray deals 1 damage to target creature or player.\nCycling {R} ({R}, Discard this card from your hand: Draw a card.)').
card_first_print('spark spray', 'SCG').
card_image_name('spark spray'/'SCG', 'spark spray').
card_uid('spark spray'/'SCG', 'SCG:Spark Spray:spark spray').
card_rarity('spark spray'/'SCG', 'Common').
card_artist('spark spray'/'SCG', 'Pete Venters').
card_number('spark spray'/'SCG', '105').
card_flavor_text('spark spray'/'SCG', 'It\'s the only kind of shower goblins will tolerate.').
card_multiverse_id('spark spray'/'SCG', '44484').

card_in_set('sprouting vines', 'SCG').
card_original_type('sprouting vines'/'SCG', 'Instant').
card_original_text('sprouting vines'/'SCG', 'Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_first_print('sprouting vines', 'SCG').
card_image_name('sprouting vines'/'SCG', 'sprouting vines').
card_uid('sprouting vines'/'SCG', 'SCG:Sprouting Vines:sprouting vines').
card_rarity('sprouting vines'/'SCG', 'Common').
card_artist('sprouting vines'/'SCG', 'John Avon').
card_number('sprouting vines'/'SCG', '128').
card_multiverse_id('sprouting vines'/'SCG', '45847').

card_in_set('stabilizer', 'SCG').
card_original_type('stabilizer'/'SCG', 'Artifact').
card_original_text('stabilizer'/'SCG', 'Players can\'t cycle cards.').
card_first_print('stabilizer', 'SCG').
card_image_name('stabilizer'/'SCG', 'stabilizer').
card_uid('stabilizer'/'SCG', 'SCG:Stabilizer:stabilizer').
card_rarity('stabilizer'/'SCG', 'Rare').
card_artist('stabilizer'/'SCG', 'David Martin').
card_number('stabilizer'/'SCG', '142').
card_flavor_text('stabilizer'/'SCG', '\"Hold that thought.\"\n—Pemmin, Riptide survivor').
card_multiverse_id('stabilizer'/'SCG', '46466').

card_in_set('stifle', 'SCG').
card_original_type('stifle'/'SCG', 'Instant').
card_original_text('stifle'/'SCG', 'Counter target activated or triggered ability. (Mana abilities can\'t be countered.)').
card_image_name('stifle'/'SCG', 'stifle').
card_uid('stifle'/'SCG', 'SCG:Stifle:stifle').
card_rarity('stifle'/'SCG', 'Rare').
card_artist('stifle'/'SCG', 'Dany Orizio').
card_number('stifle'/'SCG', '52').
card_flavor_text('stifle'/'SCG', '\"If I wanted your opinion, I\'d have told you what it was.\"\n—Pemmin, Riptide survivor').
card_multiverse_id('stifle'/'SCG', '46558').

card_in_set('sulfuric vortex', 'SCG').
card_original_type('sulfuric vortex'/'SCG', 'Enchantment').
card_original_text('sulfuric vortex'/'SCG', 'At the beginning of each player\'s upkeep, Sulfuric Vortex deals 2 damage to that player.\nIf a player would gain life, that player gains no life instead.').
card_first_print('sulfuric vortex', 'SCG').
card_image_name('sulfuric vortex'/'SCG', 'sulfuric vortex').
card_uid('sulfuric vortex'/'SCG', 'SCG:Sulfuric Vortex:sulfuric vortex').
card_rarity('sulfuric vortex'/'SCG', 'Rare').
card_artist('sulfuric vortex'/'SCG', 'Greg Staples').
card_number('sulfuric vortex'/'SCG', '106').
card_multiverse_id('sulfuric vortex'/'SCG', '47461').

card_in_set('temple of the false god', 'SCG').
card_original_type('temple of the false god'/'SCG', 'Land').
card_original_text('temple of the false god'/'SCG', '{T}: Add {2} to your mana pool. Play this ability only if you control five or more lands.').
card_first_print('temple of the false god', 'SCG').
card_image_name('temple of the false god'/'SCG', 'temple of the false god').
card_uid('temple of the false god'/'SCG', 'SCG:Temple of the False God:temple of the false god').
card_rarity('temple of the false god'/'SCG', 'Uncommon').
card_artist('temple of the false god'/'SCG', 'Brian Snõddy').
card_number('temple of the false god'/'SCG', '143').
card_flavor_text('temple of the false god'/'SCG', 'Those who bring nothing to the temple take nothing away.').
card_multiverse_id('temple of the false god'/'SCG', '44336').

card_in_set('temporal fissure', 'SCG').
card_original_type('temporal fissure'/'SCG', 'Sorcery').
card_original_text('temporal fissure'/'SCG', 'Return target permanent to its owner\'s hand.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_first_print('temporal fissure', 'SCG').
card_image_name('temporal fissure'/'SCG', 'temporal fissure').
card_uid('temporal fissure'/'SCG', 'SCG:Temporal Fissure:temporal fissure').
card_rarity('temporal fissure'/'SCG', 'Common').
card_artist('temporal fissure'/'SCG', 'Edward P. Beard, Jr.').
card_number('temporal fissure'/'SCG', '53').
card_multiverse_id('temporal fissure'/'SCG', '45836').

card_in_set('tendrils of agony', 'SCG').
card_original_type('tendrils of agony'/'SCG', 'Sorcery').
card_original_text('tendrils of agony'/'SCG', 'Target player loses 2 life and you gain 2 life.\nStorm (When you play this spell, copy it for each spell played before it this turn. You may choose new targets for the copies.)').
card_image_name('tendrils of agony'/'SCG', 'tendrils of agony').
card_uid('tendrils of agony'/'SCG', 'SCG:Tendrils of Agony:tendrils of agony').
card_rarity('tendrils of agony'/'SCG', 'Uncommon').
card_artist('tendrils of agony'/'SCG', 'Pete Venters').
card_number('tendrils of agony'/'SCG', '75').
card_multiverse_id('tendrils of agony'/'SCG', '45842').

card_in_set('thundercloud elemental', 'SCG').
card_original_type('thundercloud elemental'/'SCG', 'Creature — Elemental').
card_original_text('thundercloud elemental'/'SCG', 'Flying\n{3}{U}: Tap all creatures with toughness 2 or less.\n{3}{U}: All other creatures lose flying until end of turn.').
card_first_print('thundercloud elemental', 'SCG').
card_image_name('thundercloud elemental'/'SCG', 'thundercloud elemental').
card_uid('thundercloud elemental'/'SCG', 'SCG:Thundercloud Elemental:thundercloud elemental').
card_rarity('thundercloud elemental'/'SCG', 'Uncommon').
card_artist('thundercloud elemental'/'SCG', 'Anthony S. Waters').
card_number('thundercloud elemental'/'SCG', '54').
card_flavor_text('thundercloud elemental'/'SCG', 'Some days it\'s better to stay inside.').
card_multiverse_id('thundercloud elemental'/'SCG', '43606').

card_in_set('titanic bulvox', 'SCG').
card_original_type('titanic bulvox'/'SCG', 'Creature — Beast').
card_original_text('titanic bulvox'/'SCG', 'Trample\nMorph {4}{G}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('titanic bulvox', 'SCG').
card_image_name('titanic bulvox'/'SCG', 'titanic bulvox').
card_uid('titanic bulvox'/'SCG', 'SCG:Titanic Bulvox:titanic bulvox').
card_rarity('titanic bulvox'/'SCG', 'Common').
card_artist('titanic bulvox'/'SCG', 'Wayne England').
card_number('titanic bulvox'/'SCG', '129').
card_flavor_text('titanic bulvox'/'SCG', 'With natural laws abandoned, excess thrives.').
card_multiverse_id('titanic bulvox'/'SCG', '46691').

card_in_set('torrent of fire', 'SCG').
card_original_type('torrent of fire'/'SCG', 'Sorcery').
card_original_text('torrent of fire'/'SCG', 'Torrent of Fire deals damage equal to the highest converted mana cost among permanents you control to target creature or player.').
card_first_print('torrent of fire', 'SCG').
card_image_name('torrent of fire'/'SCG', 'torrent of fire').
card_uid('torrent of fire'/'SCG', 'SCG:Torrent of Fire:torrent of fire').
card_rarity('torrent of fire'/'SCG', 'Common').
card_artist('torrent of fire'/'SCG', 'Thomas M. Baxa').
card_number('torrent of fire'/'SCG', '107').
card_flavor_text('torrent of fire'/'SCG', 'Dragon fire melts any instrument designed to measure it.').
card_multiverse_id('torrent of fire'/'SCG', '43530').

card_in_set('trap digger', 'SCG').
card_original_type('trap digger'/'SCG', 'Creature — Soldier').
card_original_text('trap digger'/'SCG', '{2}{W}, {T}: Put a trap counter on target land you control.\nSacrifice a land with a trap counter on it: Trap Digger deals 3 damage to target attacking creature without flying.').
card_first_print('trap digger', 'SCG').
card_image_name('trap digger'/'SCG', 'trap digger').
card_uid('trap digger'/'SCG', 'SCG:Trap Digger:trap digger').
card_rarity('trap digger'/'SCG', 'Rare').
card_artist('trap digger'/'SCG', 'Christopher Moeller').
card_number('trap digger'/'SCG', '24').
card_multiverse_id('trap digger'/'SCG', '43577').

card_in_set('treetop scout', 'SCG').
card_original_type('treetop scout'/'SCG', 'Creature — Elf').
card_original_text('treetop scout'/'SCG', 'Treetop Scout can\'t be blocked except by creatures with flying.').
card_first_print('treetop scout', 'SCG').
card_image_name('treetop scout'/'SCG', 'treetop scout').
card_uid('treetop scout'/'SCG', 'SCG:Treetop Scout:treetop scout').
card_rarity('treetop scout'/'SCG', 'Common').
card_artist('treetop scout'/'SCG', 'Alan Pollack').
card_number('treetop scout'/'SCG', '130').
card_flavor_text('treetop scout'/'SCG', 'At home among the swaying, supple branches of the treetops, some scouts live their entire lives never touching ground.').
card_multiverse_id('treetop scout'/'SCG', '43501').

card_in_set('twisted abomination', 'SCG').
card_original_type('twisted abomination'/'SCG', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'SCG', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card from your hand: Search your library for a swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('twisted abomination', 'SCG').
card_image_name('twisted abomination'/'SCG', 'twisted abomination').
card_uid('twisted abomination'/'SCG', 'SCG:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'SCG', 'Common').
card_artist('twisted abomination'/'SCG', 'Daren Bader').
card_number('twisted abomination'/'SCG', '76').
card_multiverse_id('twisted abomination'/'SCG', '43717').

card_in_set('unburden', 'SCG').
card_original_type('unburden'/'SCG', 'Sorcery').
card_original_text('unburden'/'SCG', 'Target player discards two cards.\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('unburden', 'SCG').
card_image_name('unburden'/'SCG', 'unburden').
card_uid('unburden'/'SCG', 'SCG:Unburden:unburden').
card_rarity('unburden'/'SCG', 'Common').
card_artist('unburden'/'SCG', 'Wayne England').
card_number('unburden'/'SCG', '77').
card_flavor_text('unburden'/'SCG', 'Cabal initiates enter training full of hopes and fears. They graduate with neither.').
card_multiverse_id('unburden'/'SCG', '41164').

card_in_set('uncontrolled infestation', 'SCG').
card_original_type('uncontrolled infestation'/'SCG', 'Enchant Land').
card_original_text('uncontrolled infestation'/'SCG', 'Uncontrolled Infestation can enchant only a nonbasic land.\nWhen enchanted land becomes tapped, destroy it.').
card_first_print('uncontrolled infestation', 'SCG').
card_image_name('uncontrolled infestation'/'SCG', 'uncontrolled infestation').
card_uid('uncontrolled infestation'/'SCG', 'SCG:Uncontrolled Infestation:uncontrolled infestation').
card_rarity('uncontrolled infestation'/'SCG', 'Common').
card_artist('uncontrolled infestation'/'SCG', 'Tony Szczudlo').
card_number('uncontrolled infestation'/'SCG', '108').
card_flavor_text('uncontrolled infestation'/'SCG', 'The grounds of the Riptide Project are now populated only by slivers, broken beakers, and the lonely screeching of gulls.').
card_multiverse_id('uncontrolled infestation'/'SCG', '46614').

card_in_set('undead warchief', 'SCG').
card_original_type('undead warchief'/'SCG', 'Creature — Zombie').
card_original_text('undead warchief'/'SCG', 'Zombie spells you play cost {1} less to play.\nZombies you control get +2/+1.').
card_first_print('undead warchief', 'SCG').
card_image_name('undead warchief'/'SCG', 'undead warchief').
card_uid('undead warchief'/'SCG', 'SCG:Undead Warchief:undead warchief').
card_rarity('undead warchief'/'SCG', 'Uncommon').
card_artist('undead warchief'/'SCG', 'Greg Hildebrandt').
card_number('undead warchief'/'SCG', '78').
card_flavor_text('undead warchief'/'SCG', 'It has the strength of seven men. In fact, it used to be seven men.').
card_multiverse_id('undead warchief'/'SCG', '43586').

card_in_set('unspeakable symbol', 'SCG').
card_original_type('unspeakable symbol'/'SCG', 'Enchantment').
card_original_text('unspeakable symbol'/'SCG', 'Pay 3 life: Put a +1/+1 counter on target creature.').
card_first_print('unspeakable symbol', 'SCG').
card_image_name('unspeakable symbol'/'SCG', 'unspeakable symbol').
card_uid('unspeakable symbol'/'SCG', 'SCG:Unspeakable Symbol:unspeakable symbol').
card_rarity('unspeakable symbol'/'SCG', 'Uncommon').
card_artist('unspeakable symbol'/'SCG', 'Arnie Swekel').
card_number('unspeakable symbol'/'SCG', '79').
card_flavor_text('unspeakable symbol'/'SCG', 'The symbols are spread throughout Aphetto, marking sites where minions of the Raven Guild and the Cabal can seek refuge.').
card_multiverse_id('unspeakable symbol'/'SCG', '46418').

card_in_set('upwelling', 'SCG').
card_original_type('upwelling'/'SCG', 'Enchantment').
card_original_text('upwelling'/'SCG', 'Mana pools don\'t empty at the end of phases or turns. (This effect stops mana burn.)').
card_first_print('upwelling', 'SCG').
card_image_name('upwelling'/'SCG', 'upwelling').
card_uid('upwelling'/'SCG', 'SCG:Upwelling:upwelling').
card_rarity('upwelling'/'SCG', 'Rare').
card_artist('upwelling'/'SCG', 'John Avon').
card_number('upwelling'/'SCG', '131').
card_flavor_text('upwelling'/'SCG', 'Once again, Kamahl felt the full force of the Mirari\'s pull, but he had learned much since the last time.').
card_multiverse_id('upwelling'/'SCG', '43558').

card_in_set('vengeful dead', 'SCG').
card_original_type('vengeful dead'/'SCG', 'Creature — Zombie').
card_original_text('vengeful dead'/'SCG', 'Whenever Vengeful Dead or another Zombie is put into a graveyard from play, each opponent loses 1 life.').
card_first_print('vengeful dead', 'SCG').
card_image_name('vengeful dead'/'SCG', 'vengeful dead').
card_uid('vengeful dead'/'SCG', 'SCG:Vengeful Dead:vengeful dead').
card_rarity('vengeful dead'/'SCG', 'Common').
card_artist('vengeful dead'/'SCG', 'Alex Horley-Orlandelli').
card_number('vengeful dead'/'SCG', '80').
card_flavor_text('vengeful dead'/'SCG', 'Those who don\'t learn from their deaths are destined to repeat them.').
card_multiverse_id('vengeful dead'/'SCG', '45862').

card_in_set('wing shards', 'SCG').
card_original_type('wing shards'/'SCG', 'Instant').
card_original_text('wing shards'/'SCG', 'Target player sacrifices an attacking creature.\nStorm (When you play this spell, copy it for each spell played before it this turn.)').
card_image_name('wing shards'/'SCG', 'wing shards').
card_uid('wing shards'/'SCG', 'SCG:Wing Shards:wing shards').
card_rarity('wing shards'/'SCG', 'Uncommon').
card_artist('wing shards'/'SCG', 'Daren Bader').
card_number('wing shards'/'SCG', '25').
card_multiverse_id('wing shards'/'SCG', '45835').

card_in_set('wipe clean', 'SCG').
card_original_type('wipe clean'/'SCG', 'Instant').
card_original_text('wipe clean'/'SCG', 'Remove target enchantment from the game.\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('wipe clean', 'SCG').
card_image_name('wipe clean'/'SCG', 'wipe clean').
card_uid('wipe clean'/'SCG', 'SCG:Wipe Clean:wipe clean').
card_rarity('wipe clean'/'SCG', 'Common').
card_artist('wipe clean'/'SCG', 'Arnie Swekel').
card_number('wipe clean'/'SCG', '26').
card_flavor_text('wipe clean'/'SCG', '\"The light of the Ancestor will scour the taint of darkness from the land.\"').
card_multiverse_id('wipe clean'/'SCG', '47221').

card_in_set('wirewood guardian', 'SCG').
card_original_type('wirewood guardian'/'SCG', 'Creature — Elf Mutant').
card_original_text('wirewood guardian'/'SCG', 'Forestcycling {2} ({2}, Discard this card from your hand: Search your library for a forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('wirewood guardian', 'SCG').
card_image_name('wirewood guardian'/'SCG', 'wirewood guardian').
card_uid('wirewood guardian'/'SCG', 'SCG:Wirewood Guardian:wirewood guardian').
card_rarity('wirewood guardian'/'SCG', 'Common').
card_artist('wirewood guardian'/'SCG', 'Mark Tedin').
card_number('wirewood guardian'/'SCG', '132').
card_flavor_text('wirewood guardian'/'SCG', 'Tall as a tree and probably related.').
card_multiverse_id('wirewood guardian'/'SCG', '43537').

card_in_set('wirewood symbiote', 'SCG').
card_original_type('wirewood symbiote'/'SCG', 'Creature — Insect').
card_original_text('wirewood symbiote'/'SCG', 'Return an Elf you control to its owner\'s hand: Untap target creature. Play this ability only once each turn.').
card_first_print('wirewood symbiote', 'SCG').
card_image_name('wirewood symbiote'/'SCG', 'wirewood symbiote').
card_uid('wirewood symbiote'/'SCG', 'SCG:Wirewood Symbiote:wirewood symbiote').
card_rarity('wirewood symbiote'/'SCG', 'Uncommon').
card_artist('wirewood symbiote'/'SCG', 'Thomas M. Baxa').
card_number('wirewood symbiote'/'SCG', '133').
card_flavor_text('wirewood symbiote'/'SCG', 'It drinks fatigue.').
card_multiverse_id('wirewood symbiote'/'SCG', '45859').

card_in_set('woodcloaker', 'SCG').
card_original_type('woodcloaker'/'SCG', 'Creature — Elf').
card_original_text('woodcloaker'/'SCG', 'Morph {2}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Woodcloaker is turned face up, target creature gains trample until end of turn.').
card_first_print('woodcloaker', 'SCG').
card_image_name('woodcloaker'/'SCG', 'woodcloaker').
card_uid('woodcloaker'/'SCG', 'SCG:Woodcloaker:woodcloaker').
card_rarity('woodcloaker'/'SCG', 'Common').
card_artist('woodcloaker'/'SCG', 'Jim Nelson').
card_number('woodcloaker'/'SCG', '134').
card_multiverse_id('woodcloaker'/'SCG', '43596').

card_in_set('xantid swarm', 'SCG').
card_original_type('xantid swarm'/'SCG', 'Creature — Insect').
card_original_text('xantid swarm'/'SCG', 'Flying\nWhenever Xantid Swarm attacks, defending player can\'t play spells this turn.').
card_first_print('xantid swarm', 'SCG').
card_image_name('xantid swarm'/'SCG', 'xantid swarm').
card_uid('xantid swarm'/'SCG', 'SCG:Xantid Swarm:xantid swarm').
card_rarity('xantid swarm'/'SCG', 'Rare').
card_artist('xantid swarm'/'SCG', 'David Martin').
card_number('xantid swarm'/'SCG', '135').
card_flavor_text('xantid swarm'/'SCG', 'When they land on you, all you can think about is tearing them off.').
card_multiverse_id('xantid swarm'/'SCG', '43564').

card_in_set('zealous inquisitor', 'SCG').
card_original_type('zealous inquisitor'/'SCG', 'Creature — Cleric').
card_original_text('zealous inquisitor'/'SCG', '{1}{W}: The next 1 damage that would be dealt to Zealous Inquisitor this turn is dealt to target creature instead.').
card_first_print('zealous inquisitor', 'SCG').
card_image_name('zealous inquisitor'/'SCG', 'zealous inquisitor').
card_uid('zealous inquisitor'/'SCG', 'SCG:Zealous Inquisitor:zealous inquisitor').
card_rarity('zealous inquisitor'/'SCG', 'Common').
card_artist('zealous inquisitor'/'SCG', 'Wayne England').
card_number('zealous inquisitor'/'SCG', '27').
card_flavor_text('zealous inquisitor'/'SCG', '\"I only return what rightly belongs to another.\"').
card_multiverse_id('zealous inquisitor'/'SCG', '42040').

card_in_set('zombie cutthroat', 'SCG').
card_original_type('zombie cutthroat'/'SCG', 'Creature — Zombie').
card_original_text('zombie cutthroat'/'SCG', 'Morph—Pay 5 life. (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('zombie cutthroat', 'SCG').
card_image_name('zombie cutthroat'/'SCG', 'zombie cutthroat').
card_uid('zombie cutthroat'/'SCG', 'SCG:Zombie Cutthroat:zombie cutthroat').
card_rarity('zombie cutthroat'/'SCG', 'Common').
card_artist('zombie cutthroat'/'SCG', 'Thomas M. Baxa').
card_number('zombie cutthroat'/'SCG', '81').
card_flavor_text('zombie cutthroat'/'SCG', 'The single-mindedness of a zombie, the cunning of an assassin.').
card_multiverse_id('zombie cutthroat'/'SCG', '43515').
