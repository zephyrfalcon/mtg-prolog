% Weatherlight

set('WTH').
set_name('WTH', 'Weatherlight').
set_release_date('WTH', '1997-06-09').
set_border('WTH', 'black').
set_type('WTH', 'expansion').
set_block('WTH', 'Mirage').

card_in_set('abduction', 'WTH').
card_original_type('abduction'/'WTH', 'Enchant Creature').
card_original_text('abduction'/'WTH', 'When Abduction comes into play, untap enchanted creature.\nGain control of enchanted creature.\nIf enchanted creature is put into any graveyard, put that creature into play under its owner\'s control.').
card_first_print('abduction', 'WTH').
card_image_name('abduction'/'WTH', 'abduction').
card_uid('abduction'/'WTH', 'WTH:Abduction:abduction').
card_rarity('abduction'/'WTH', 'Uncommon').
card_artist('abduction'/'WTH', 'Colin MacNeil').
card_multiverse_id('abduction'/'WTH', '4476').

card_in_set('abeyance', 'WTH').
card_original_type('abeyance'/'WTH', 'Instant').
card_original_text('abeyance'/'WTH', 'Until end of turn, target player cannot play instants, interrupts, sorceries, or abilities requiring an activation cost.\nDraw a card.').
card_first_print('abeyance', 'WTH').
card_image_name('abeyance'/'WTH', 'abeyance').
card_uid('abeyance'/'WTH', 'WTH:Abeyance:abeyance').
card_rarity('abeyance'/'WTH', 'Rare').
card_artist('abeyance'/'WTH', 'Thomas Gianni').
card_flavor_text('abeyance'/'WTH', '\"I\'m too modest a wizard to reveal the full extent of my abilities.\"\n—Ertai, wizard adept').
card_multiverse_id('abeyance'/'WTH', '4563').

card_in_set('abjure', 'WTH').
card_original_type('abjure'/'WTH', 'Interrupt').
card_original_text('abjure'/'WTH', 'Sacrifice a blue permanent: Counter target spell.').
card_first_print('abjure', 'WTH').
card_image_name('abjure'/'WTH', 'abjure').
card_uid('abjure'/'WTH', 'WTH:Abjure:abjure').
card_rarity('abjure'/'WTH', 'Common').
card_artist('abjure'/'WTH', 'Ted Naifeh').
card_flavor_text('abjure'/'WTH', 'Mirri\'s hackles rose as Ertai continued expounding his virtues. \"If that arrogant brat doesn\'t shut up soon,\" she growled to herself, \"I\'m going to have to kill him.\"').
card_multiverse_id('abjure'/'WTH', '4477').

card_in_set('aboroth', 'WTH').
card_original_type('aboroth'/'WTH', 'Summon — Aboroth').
card_original_text('aboroth'/'WTH', 'Cumulative upkeep—Put a -1/-1 counter on Aboroth').
card_first_print('aboroth', 'WTH').
card_image_name('aboroth'/'WTH', 'aboroth').
card_uid('aboroth'/'WTH', 'WTH:Aboroth:aboroth').
card_rarity('aboroth'/'WTH', 'Rare').
card_artist('aboroth'/'WTH', 'Brom').
card_flavor_text('aboroth'/'WTH', 'The Weatherlight banked sharply as the colossal beast rose up to crush the lone rider. \"My bet\'s on the dirt,\" Gerrard said flatly.').
card_multiverse_id('aboroth'/'WTH', '4505').

card_in_set('abyssal gatekeeper', 'WTH').
card_original_type('abyssal gatekeeper'/'WTH', 'Summon — Gatekeeper').
card_original_text('abyssal gatekeeper'/'WTH', 'If Abyssal Gatekeeper is put into any graveyard from play, each player chooses and buries a creature he or she controls.').
card_first_print('abyssal gatekeeper', 'WTH').
card_image_name('abyssal gatekeeper'/'WTH', 'abyssal gatekeeper').
card_uid('abyssal gatekeeper'/'WTH', 'WTH:Abyssal Gatekeeper:abyssal gatekeeper').
card_rarity('abyssal gatekeeper'/'WTH', 'Common').
card_artist('abyssal gatekeeper'/'WTH', 'Mark Tedin').
card_flavor_text('abyssal gatekeeper'/'WTH', '\"There are two ways for me to pass this gate. One involves you remaining conscious.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('abyssal gatekeeper'/'WTH', '4447').

card_in_set('æther flash', 'WTH').
card_original_type('æther flash'/'WTH', 'Enchantment').
card_original_text('æther flash'/'WTH', 'Whenever any creature comes into play, Æther Flash deals 2 damage to that creature.').
card_first_print('æther flash', 'WTH').
card_image_name('æther flash'/'WTH', 'aether flash').
card_uid('æther flash'/'WTH', 'WTH:Æther Flash:aether flash').
card_rarity('æther flash'/'WTH', 'Uncommon').
card_artist('æther flash'/'WTH', 'Ron Spencer').
card_flavor_text('æther flash'/'WTH', '\"You can\'t get there from here. Well, you can—it just won\'t be fun.\"\n—Ertai, wizard adept').
card_multiverse_id('æther flash'/'WTH', '4534').

card_in_set('agonizing memories', 'WTH').
card_original_type('agonizing memories'/'WTH', 'Sorcery').
card_original_text('agonizing memories'/'WTH', 'Look at target player\'s hand. Choose two of those cards and put them on top of his or her library in any order.').
card_first_print('agonizing memories', 'WTH').
card_image_name('agonizing memories'/'WTH', 'agonizing memories').
card_uid('agonizing memories'/'WTH', 'WTH:Agonizing Memories:agonizing memories').
card_rarity('agonizing memories'/'WTH', 'Uncommon').
card_artist('agonizing memories'/'WTH', 'Mike Dringenberg').
card_flavor_text('agonizing memories'/'WTH', '\"An innocent man died because of my anger. That knowledge will haunt me for all eternity.\"\n—Karn, silver golem').
card_multiverse_id('agonizing memories'/'WTH', '4448').

card_in_set('alabaster dragon', 'WTH').
card_original_type('alabaster dragon'/'WTH', 'Summon — Dragon').
card_original_text('alabaster dragon'/'WTH', 'Flying\nIf Alabaster Dragon is put into any graveyard from play, shuffle Alabaster Dragon into its owner\'s library.').
card_image_name('alabaster dragon'/'WTH', 'alabaster dragon').
card_uid('alabaster dragon'/'WTH', 'WTH:Alabaster Dragon:alabaster dragon').
card_rarity('alabaster dragon'/'WTH', 'Rare').
card_artist('alabaster dragon'/'WTH', 'Bob Eggleton').
card_multiverse_id('alabaster dragon'/'WTH', '4564').

card_in_set('alms', 'WTH').
card_original_type('alms'/'WTH', 'Enchantment').
card_original_text('alms'/'WTH', '{1}, Remove the top card in your graveyard from the game: Prevent 1 damage to any creature.').
card_first_print('alms', 'WTH').
card_image_name('alms'/'WTH', 'alms').
card_uid('alms'/'WTH', 'WTH:Alms:alms').
card_rarity('alms'/'WTH', 'Common').
card_artist('alms'/'WTH', 'Rogério Vilela').
card_flavor_text('alms'/'WTH', '\"Helping people is mostly a matter of teaching them to help themselves.\"\n—Sisay, journal').
card_multiverse_id('alms'/'WTH', '4565').

card_in_set('ancestral knowledge', 'WTH').
card_original_type('ancestral knowledge'/'WTH', 'Enchantment').
card_original_text('ancestral knowledge'/'WTH', 'Cumulative upkeep {1}\nWhen Ancestral Knowledge comes into play, look at the top ten cards of your library, then remove any number of them from the game and put the rest back on top of your library in any order.\nIf Ancestral Knowledge leaves play, shuffle your library.').
card_first_print('ancestral knowledge', 'WTH').
card_image_name('ancestral knowledge'/'WTH', 'ancestral knowledge').
card_uid('ancestral knowledge'/'WTH', 'WTH:Ancestral Knowledge:ancestral knowledge').
card_rarity('ancestral knowledge'/'WTH', 'Rare').
card_artist('ancestral knowledge'/'WTH', 'Colin MacNeil').
card_multiverse_id('ancestral knowledge'/'WTH', '4478').

card_in_set('angelic renewal', 'WTH').
card_original_type('angelic renewal'/'WTH', 'Enchantment').
card_original_text('angelic renewal'/'WTH', 'If any creatures are put into your graveyard from play, you may bury Angelic Renewal and put one of those creatures into play.').
card_first_print('angelic renewal', 'WTH').
card_image_name('angelic renewal'/'WTH', 'angelic renewal').
card_uid('angelic renewal'/'WTH', 'WTH:Angelic Renewal:angelic renewal').
card_rarity('angelic renewal'/'WTH', 'Common').
card_artist('angelic renewal'/'WTH', 'Rebecca Guay').
card_multiverse_id('angelic renewal'/'WTH', '4566').

card_in_set('apathy', 'WTH').
card_original_type('apathy'/'WTH', 'Enchant Creature').
card_original_text('apathy'/'WTH', 'Enchanted creature does not untap during its controller\'s untap phase.\nDuring the upkeep of enchanted creature\'s controller, that player may discard a card at random to untap that creature.').
card_first_print('apathy', 'WTH').
card_image_name('apathy'/'WTH', 'apathy').
card_uid('apathy'/'WTH', 'WTH:Apathy:apathy').
card_rarity('apathy'/'WTH', 'Common').
card_artist('apathy'/'WTH', 'Phil Foglio').
card_multiverse_id('apathy'/'WTH', '4479').

card_in_set('arctic wolves', 'WTH').
card_original_type('arctic wolves'/'WTH', 'Summon — Wolves').
card_original_text('arctic wolves'/'WTH', 'Cumulative upkeep {2}\nWhen Arctic Wolves comes into play, draw a card.').
card_first_print('arctic wolves', 'WTH').
card_image_name('arctic wolves'/'WTH', 'arctic wolves').
card_uid('arctic wolves'/'WTH', 'WTH:Arctic Wolves:arctic wolves').
card_rarity('arctic wolves'/'WTH', 'Uncommon').
card_artist('arctic wolves'/'WTH', 'Steve White').
card_flavor_text('arctic wolves'/'WTH', '\"No matter where we cat warriors go in the world, those stupid slobberers find us.\"\n—Mirri of the Weatherlight').
card_multiverse_id('arctic wolves'/'WTH', '4506').

card_in_set('ardent militia', 'WTH').
card_original_type('ardent militia'/'WTH', 'Summon — Soldiers').
card_original_text('ardent militia'/'WTH', 'Attacking does not cause Ardent Militia to tap.').
card_image_name('ardent militia'/'WTH', 'ardent militia').
card_uid('ardent militia'/'WTH', 'WTH:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'WTH', 'Common').
card_artist('ardent militia'/'WTH', 'Zina Saunders').
card_flavor_text('ardent militia'/'WTH', '\"While the rest of us got paid to serve the Benalish army, the militia didn\'t. I used to say they stood for freedom—free and dumb.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('ardent militia'/'WTH', '4567').

card_in_set('argivian find', 'WTH').
card_original_type('argivian find'/'WTH', 'Instant').
card_original_text('argivian find'/'WTH', 'Return target artifact or enchantment card from your graveyard to your hand.').
card_first_print('argivian find', 'WTH').
card_image_name('argivian find'/'WTH', 'argivian find').
card_uid('argivian find'/'WTH', 'WTH:Argivian Find:argivian find').
card_rarity('argivian find'/'WTH', 'Uncommon').
card_artist('argivian find'/'WTH', 'Roger Raupp').
card_flavor_text('argivian find'/'WTH', '\"We often bury our pasts, and then someone comes along and digs them up.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('argivian find'/'WTH', '4568').

card_in_set('argivian restoration', 'WTH').
card_original_type('argivian restoration'/'WTH', 'Sorcery').
card_original_text('argivian restoration'/'WTH', 'Put target artifact card from your graveyard into play.').
card_first_print('argivian restoration', 'WTH').
card_image_name('argivian restoration'/'WTH', 'argivian restoration').
card_uid('argivian restoration'/'WTH', 'WTH:Argivian Restoration:argivian restoration').
card_rarity('argivian restoration'/'WTH', 'Uncommon').
card_artist('argivian restoration'/'WTH', 'Roger Raupp').
card_flavor_text('argivian restoration'/'WTH', '\"The Argivian University taught me two things: always look to the past, and never dismiss what appears useless.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('argivian restoration'/'WTH', '4480').

card_in_set('aura of silence', 'WTH').
card_original_type('aura of silence'/'WTH', 'Enchantment').
card_original_text('aura of silence'/'WTH', 'Artifact and enchantment spells cost target opponent an additional {2} to play.\nSacrifice Aura of Silence: Destroy target artifact or enchantment.').
card_first_print('aura of silence', 'WTH').
card_image_name('aura of silence'/'WTH', 'aura of silence').
card_uid('aura of silence'/'WTH', 'WTH:Aura of Silence:aura of silence').
card_rarity('aura of silence'/'WTH', 'Uncommon').
card_artist('aura of silence'/'WTH', 'D. Alexander Gregory').
card_multiverse_id('aura of silence'/'WTH', '4569').

card_in_set('avizoa', 'WTH').
card_original_type('avizoa'/'WTH', 'Summon — Avizoa').
card_original_text('avizoa'/'WTH', 'Flying\nSkip your next untap phase: Avizoa gets +2/+2 until end of turn. Use this ability only once each turn.').
card_first_print('avizoa', 'WTH').
card_image_name('avizoa'/'WTH', 'avizoa').
card_uid('avizoa'/'WTH', 'WTH:Avizoa:avizoa').
card_rarity('avizoa'/'WTH', 'Rare').
card_artist('avizoa'/'WTH', 'Paolo Parente').
card_flavor_text('avizoa'/'WTH', '\"Maybe we can trap them with bait,\" thought Tahngarth, eyeing Squee.').
card_multiverse_id('avizoa'/'WTH', '4481').

card_in_set('barishi', 'WTH').
card_original_type('barishi'/'WTH', 'Summon — Barishi').
card_original_text('barishi'/'WTH', 'If Barishi is put into any graveyard from play, remove Barishi from the game, then shuffle all creature cards from your graveyard into your library.').
card_first_print('barishi', 'WTH').
card_image_name('barishi'/'WTH', 'barishi').
card_uid('barishi'/'WTH', 'WTH:Barishi:barishi').
card_rarity('barishi'/'WTH', 'Uncommon').
card_artist('barishi'/'WTH', 'Ted Naifeh').
card_multiverse_id('barishi'/'WTH', '4507').

card_in_set('barrow ghoul', 'WTH').
card_original_type('barrow ghoul'/'WTH', 'Summon — Zombie').
card_original_text('barrow ghoul'/'WTH', 'During your upkeep, remove the top creature card in your graveyard from the game or bury Barrow Ghoul.').
card_first_print('barrow ghoul', 'WTH').
card_image_name('barrow ghoul'/'WTH', 'barrow ghoul').
card_uid('barrow ghoul'/'WTH', 'WTH:Barrow Ghoul:barrow ghoul').
card_rarity('barrow ghoul'/'WTH', 'Common').
card_artist('barrow ghoul'/'WTH', 'Bryan Talbot').
card_flavor_text('barrow ghoul'/'WTH', '\"They killed my family to deny me a future. They fed on my ancestors to deny me a past.\"\n—Crovax').
card_multiverse_id('barrow ghoul'/'WTH', '4449').

card_in_set('benalish infantry', 'WTH').
card_original_type('benalish infantry'/'WTH', 'Summon — Soldiers').
card_original_text('benalish infantry'/'WTH', 'Banding').
card_first_print('benalish infantry', 'WTH').
card_image_name('benalish infantry'/'WTH', 'benalish infantry').
card_uid('benalish infantry'/'WTH', 'WTH:Benalish Infantry:benalish infantry').
card_rarity('benalish infantry'/'WTH', 'Common').
card_artist('benalish infantry'/'WTH', 'Dan Frazier').
card_flavor_text('benalish infantry'/'WTH', '\"My favorite part of getting into the infantry was getting out again.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('benalish infantry'/'WTH', '4570').

card_in_set('benalish knight', 'WTH').
card_original_type('benalish knight'/'WTH', 'Summon — Knight').
card_original_text('benalish knight'/'WTH', 'First strike\nYou may play Benalish Knight whenever you could play an instant.').
card_first_print('benalish knight', 'WTH').
card_image_name('benalish knight'/'WTH', 'benalish knight').
card_uid('benalish knight'/'WTH', 'WTH:Benalish Knight:benalish knight').
card_rarity('benalish knight'/'WTH', 'Common').
card_artist('benalish knight'/'WTH', 'Zina Saunders').
card_flavor_text('benalish knight'/'WTH', '\"We called them ‘armored lightning.\'\"\n—Gerrard of the Weatherlight').
card_multiverse_id('benalish knight'/'WTH', '4571').

card_in_set('benalish missionary', 'WTH').
card_original_type('benalish missionary'/'WTH', 'Summon — Cleric').
card_original_text('benalish missionary'/'WTH', '{1}{W}, {T}: Target blocked creature deals no combat damage this turn.').
card_first_print('benalish missionary', 'WTH').
card_image_name('benalish missionary'/'WTH', 'benalish missionary').
card_uid('benalish missionary'/'WTH', 'WTH:Benalish Missionary:benalish missionary').
card_rarity('benalish missionary'/'WTH', 'Common').
card_artist('benalish missionary'/'WTH', 'Pete Venters').
card_flavor_text('benalish missionary'/'WTH', '\"These horn-haters say no gods but theirs exist. I say let him find out for himself—right now!\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('benalish missionary'/'WTH', '4572').

card_in_set('betrothed of fire', 'WTH').
card_original_type('betrothed of fire'/'WTH', 'Enchant Creature').
card_original_text('betrothed of fire'/'WTH', 'Sacrifice an untapped creature: Enchanted creature gets +2/+0 until end of turn.\nSacrifice enchanted creature: All creatures you control get +2/+0 until end of turn.').
card_first_print('betrothed of fire', 'WTH').
card_image_name('betrothed of fire'/'WTH', 'betrothed of fire').
card_uid('betrothed of fire'/'WTH', 'WTH:Betrothed of Fire:betrothed of fire').
card_rarity('betrothed of fire'/'WTH', 'Common').
card_artist('betrothed of fire'/'WTH', 'Clint Langley').
card_multiverse_id('betrothed of fire'/'WTH', '4535').

card_in_set('bloodrock cyclops', 'WTH').
card_original_type('bloodrock cyclops'/'WTH', 'Summon — Cyclops').
card_original_text('bloodrock cyclops'/'WTH', 'Bloodrock Cyclops attacks each turn if able.').
card_first_print('bloodrock cyclops', 'WTH').
card_image_name('bloodrock cyclops'/'WTH', 'bloodrock cyclops').
card_uid('bloodrock cyclops'/'WTH', 'WTH:Bloodrock Cyclops:bloodrock cyclops').
card_rarity('bloodrock cyclops'/'WTH', 'Common').
card_artist('bloodrock cyclops'/'WTH', 'Tom Wänerstrand').
card_flavor_text('bloodrock cyclops'/'WTH', '\"He\'s big an\' dumb an\' ready to fight. A lot like ol\' Hornhead here.\"\n—Squee, goblin cabin hand,\nscouting report').
card_multiverse_id('bloodrock cyclops'/'WTH', '4536').

card_in_set('blossoming wreath', 'WTH').
card_original_type('blossoming wreath'/'WTH', 'Instant').
card_original_text('blossoming wreath'/'WTH', 'Gain life equal to the number of creature cards in your graveyard.').
card_first_print('blossoming wreath', 'WTH').
card_image_name('blossoming wreath'/'WTH', 'blossoming wreath').
card_uid('blossoming wreath'/'WTH', 'WTH:Blossoming Wreath:blossoming wreath').
card_rarity('blossoming wreath'/'WTH', 'Common').
card_artist('blossoming wreath'/'WTH', 'Brian Durfee').
card_flavor_text('blossoming wreath'/'WTH', '\"I place this wreath for you, my kin. Even in death, you give me strength to avenge you.\"\n—Mirri of the Weatherlight').
card_multiverse_id('blossoming wreath'/'WTH', '4508').

card_in_set('bogardan firefiend', 'WTH').
card_original_type('bogardan firefiend'/'WTH', 'Summon — Spirit').
card_original_text('bogardan firefiend'/'WTH', 'If Bogardan Firefiend is put into any graveyard from play, it deals 2 damage to target creature.').
card_first_print('bogardan firefiend', 'WTH').
card_image_name('bogardan firefiend'/'WTH', 'bogardan firefiend').
card_uid('bogardan firefiend'/'WTH', 'WTH:Bogardan Firefiend:bogardan firefiend').
card_rarity('bogardan firefiend'/'WTH', 'Common').
card_artist('bogardan firefiend'/'WTH', 'Terese Nielsen').
card_flavor_text('bogardan firefiend'/'WTH', '\"The next one who tells me to relax and curl up by a fire is dead.\"\n—Mirri of the Weatherlight').
card_multiverse_id('bogardan firefiend'/'WTH', '4537').

card_in_set('boiling blood', 'WTH').
card_original_type('boiling blood'/'WTH', 'Instant').
card_original_text('boiling blood'/'WTH', 'Target creature attacks this turn if able.\nDraw a card.').
card_first_print('boiling blood', 'WTH').
card_image_name('boiling blood'/'WTH', 'boiling blood').
card_uid('boiling blood'/'WTH', 'WTH:Boiling Blood:boiling blood').
card_rarity('boiling blood'/'WTH', 'Common').
card_artist('boiling blood'/'WTH', 'Cliff Nielsen').
card_flavor_text('boiling blood'/'WTH', 'Your father has no horns!\nYour mother wears a bell\nYou drink the milk of goats!\n—Talruum taunts').
card_multiverse_id('boiling blood'/'WTH', '4538').

card_in_set('bone dancer', 'WTH').
card_original_type('bone dancer'/'WTH', 'Summon — Zombie').
card_original_text('bone dancer'/'WTH', '{0}: Put the top creature card of defending player\'s graveyard into play under your control. Bone Dancer deals no combat damage this turn. Use this ability only if Bone Dancer is attacking and unblocked and only once each turn.').
card_first_print('bone dancer', 'WTH').
card_image_name('bone dancer'/'WTH', 'bone dancer').
card_uid('bone dancer'/'WTH', 'WTH:Bone Dancer:bone dancer').
card_rarity('bone dancer'/'WTH', 'Rare').
card_artist('bone dancer'/'WTH', 'Scott Kirschner').
card_multiverse_id('bone dancer'/'WTH', '4450').

card_in_set('bösium strip', 'WTH').
card_original_type('bösium strip'/'WTH', 'Artifact').
card_original_text('bösium strip'/'WTH', '{3}, {T}: Until end of turn, if at any time the top card in your graveyard is an instant, interrupt, or sorcery card, you may play that card as though it were in your hand. If you do so, remove the card from the game.').
card_first_print('bösium strip', 'WTH').
card_image_name('bösium strip'/'WTH', 'bosium strip').
card_uid('bösium strip'/'WTH', 'WTH:Bösium Strip:bosium strip').
card_rarity('bösium strip'/'WTH', 'Rare').
card_artist('bösium strip'/'WTH', 'Steve Luke').
card_multiverse_id('bösium strip'/'WTH', '4429').

card_in_set('briar shield', 'WTH').
card_original_type('briar shield'/'WTH', 'Enchant Creature').
card_original_text('briar shield'/'WTH', 'Enchanted creature gets +1/+1.\nSacrifice Briar Shield: Enchanted creature gets +3/+3 until end of turn.').
card_first_print('briar shield', 'WTH').
card_image_name('briar shield'/'WTH', 'briar shield').
card_uid('briar shield'/'WTH', 'WTH:Briar Shield:briar shield').
card_rarity('briar shield'/'WTH', 'Common').
card_artist('briar shield'/'WTH', 'Scott Kirschner').
card_flavor_text('briar shield'/'WTH', 'In all its forms, the forest is the elves\' best protector.').
card_multiverse_id('briar shield'/'WTH', '4509').

card_in_set('bubble matrix', 'WTH').
card_original_type('bubble matrix'/'WTH', 'Artifact').
card_original_text('bubble matrix'/'WTH', 'All damage dealt to creatures is reduced to 0.').
card_first_print('bubble matrix', 'WTH').
card_image_name('bubble matrix'/'WTH', 'bubble matrix').
card_uid('bubble matrix'/'WTH', 'WTH:Bubble Matrix:bubble matrix').
card_rarity('bubble matrix'/'WTH', 'Rare').
card_artist('bubble matrix'/'WTH', 'Brom').
card_flavor_text('bubble matrix'/'WTH', '\"This device was commisioned by a king who desired peace. Unfortunately, the royal artificer was also the king\'s jester.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('bubble matrix'/'WTH', '4430').

card_in_set('buried alive', 'WTH').
card_original_type('buried alive'/'WTH', 'Sorcery').
card_original_text('buried alive'/'WTH', 'Search your library for up to three creature cards and put them into your graveyard. Shuffle your library afterwards.').
card_first_print('buried alive', 'WTH').
card_image_name('buried alive'/'WTH', 'buried alive').
card_uid('buried alive'/'WTH', 'WTH:Buried Alive:buried alive').
card_rarity('buried alive'/'WTH', 'Uncommon').
card_artist('buried alive'/'WTH', 'Brian Horton').
card_flavor_text('buried alive'/'WTH', '\"Is it worse to walk while dead, or to be buried alive? I have witnessed both.\"\n—Crovax').
card_multiverse_id('buried alive'/'WTH', '4451').

card_in_set('call of the wild', 'WTH').
card_original_type('call of the wild'/'WTH', 'Enchantment').
card_original_text('call of the wild'/'WTH', '{2}{G}{G} Reveal the top card of your library to all players. If that card is a creature card, put it into play. Otherwise, bury it.').
card_first_print('call of the wild', 'WTH').
card_image_name('call of the wild'/'WTH', 'call of the wild').
card_uid('call of the wild'/'WTH', 'WTH:Call of the Wild:call of the wild').
card_rarity('call of the wild'/'WTH', 'Rare').
card_artist('call of the wild'/'WTH', 'Brom').
card_flavor_text('call of the wild'/'WTH', 'Thinking of dinner made Squee realize that the forest was probably thinking the same thing.').
card_multiverse_id('call of the wild'/'WTH', '4510').

card_in_set('chimeric sphere', 'WTH').
card_original_type('chimeric sphere'/'WTH', 'Artifact').
card_original_text('chimeric sphere'/'WTH', '{2}: Until end of turn, Chimeric Sphere is a 2/1 artifact creature with flying.\n{2}: Until end of turn, Chimeric Sphere is a 3/2 artifact creature without flying.').
card_first_print('chimeric sphere', 'WTH').
card_image_name('chimeric sphere'/'WTH', 'chimeric sphere').
card_uid('chimeric sphere'/'WTH', 'WTH:Chimeric Sphere:chimeric sphere').
card_rarity('chimeric sphere'/'WTH', 'Uncommon').
card_artist('chimeric sphere'/'WTH', 'Colin MacNeil').
card_multiverse_id('chimeric sphere'/'WTH', '4431').

card_in_set('choking vines', 'WTH').
card_original_type('choking vines'/'WTH', 'Instant').
card_original_text('choking vines'/'WTH', 'Play only when blockers are declared.\nX target attacking creatures are considered blocked. Choking Vines deals 1 damage to each of those creatures.').
card_first_print('choking vines', 'WTH').
card_image_name('choking vines'/'WTH', 'choking vines').
card_uid('choking vines'/'WTH', 'WTH:Choking Vines:choking vines').
card_rarity('choking vines'/'WTH', 'Common').
card_artist('choking vines'/'WTH', 'Ted Naifeh').
card_multiverse_id('choking vines'/'WTH', '4511').

card_in_set('cinder giant', 'WTH').
card_original_type('cinder giant'/'WTH', 'Summon — Giant').
card_original_text('cinder giant'/'WTH', 'During your upkeep, Cinder Giant deals 2 damage to each other creature you control.').
card_first_print('cinder giant', 'WTH').
card_image_name('cinder giant'/'WTH', 'cinder giant').
card_uid('cinder giant'/'WTH', 'WTH:Cinder Giant:cinder giant').
card_rarity('cinder giant'/'WTH', 'Uncommon').
card_artist('cinder giant'/'WTH', 'Rogério Vilela').
card_flavor_text('cinder giant'/'WTH', '\"The giant wept tears of embers for the deaths of his allies.\"\n—Azeworai, \"The Lonely Giant\"').
card_multiverse_id('cinder giant'/'WTH', '4539').

card_in_set('cinder wall', 'WTH').
card_original_type('cinder wall'/'WTH', 'Summon — Wall').
card_original_text('cinder wall'/'WTH', 'If Cinder Wall blocks, destroy it at end of combat.').
card_first_print('cinder wall', 'WTH').
card_image_name('cinder wall'/'WTH', 'cinder wall').
card_uid('cinder wall'/'WTH', 'WTH:Cinder Wall:cinder wall').
card_rarity('cinder wall'/'WTH', 'Common').
card_artist('cinder wall'/'WTH', 'Randy Gallegos').
card_flavor_text('cinder wall'/'WTH', '\"Easy. All ya gotta do is push somebody into it.\"\n—Squee, goblin cabin hand,\nscouting report').
card_multiverse_id('cinder wall'/'WTH', '4540').

card_in_set('circling vultures', 'WTH').
card_original_type('circling vultures'/'WTH', 'Summon — Birds').
card_original_text('circling vultures'/'WTH', 'Flying\nDuring your upkeep, remove the top creature card in your graveyard from the game or bury Circling Vultures.\nIf Circling Vultures is in your hand, you may discard it. Play this ability as an instant.').
card_first_print('circling vultures', 'WTH').
card_image_name('circling vultures'/'WTH', 'circling vultures').
card_uid('circling vultures'/'WTH', 'WTH:Circling Vultures:circling vultures').
card_rarity('circling vultures'/'WTH', 'Uncommon').
card_artist('circling vultures'/'WTH', 'Una Fricker').
card_multiverse_id('circling vultures'/'WTH', '4452').

card_in_set('cloud djinn', 'WTH').
card_original_type('cloud djinn'/'WTH', 'Summon — Djinn').
card_original_text('cloud djinn'/'WTH', 'Flying\nCloud Djinn can block only creatures with flying.').
card_first_print('cloud djinn', 'WTH').
card_image_name('cloud djinn'/'WTH', 'cloud djinn').
card_uid('cloud djinn'/'WTH', 'WTH:Cloud Djinn:cloud djinn').
card_rarity('cloud djinn'/'WTH', 'Uncommon').
card_artist('cloud djinn'/'WTH', 'Mike Dringenberg').
card_flavor_text('cloud djinn'/'WTH', 'As elusive as the footprint of a djinn\n—Suq\'Ata expression').
card_multiverse_id('cloud djinn'/'WTH', '4482').

card_in_set('coils of the medusa', 'WTH').
card_original_type('coils of the medusa'/'WTH', 'Enchant Creature').
card_original_text('coils of the medusa'/'WTH', 'Enchanted creature gets +1/-1.\nSacrifice Coils of the Medusa: Destroy all non-Wall creatures blocking enchanted creature.').
card_first_print('coils of the medusa', 'WTH').
card_image_name('coils of the medusa'/'WTH', 'coils of the medusa').
card_uid('coils of the medusa'/'WTH', 'WTH:Coils of the Medusa:coils of the medusa').
card_rarity('coils of the medusa'/'WTH', 'Common').
card_artist('coils of the medusa'/'WTH', 'Darbury Stenderu').
card_flavor_text('coils of the medusa'/'WTH', 'Serpentine locks shape ornate rocks.').
card_multiverse_id('coils of the medusa'/'WTH', '4453').

card_in_set('cone of flame', 'WTH').
card_original_type('cone of flame'/'WTH', 'Sorcery').
card_original_text('cone of flame'/'WTH', 'Choose three target creatures and/or players. Cone of Flame deals 1 damage to the first, 2 damage to the second, and 3 damage to the third.').
card_first_print('cone of flame', 'WTH').
card_image_name('cone of flame'/'WTH', 'cone of flame').
card_uid('cone of flame'/'WTH', 'WTH:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'WTH', 'Uncommon').
card_artist('cone of flame'/'WTH', 'Ron Spencer').
card_flavor_text('cone of flame'/'WTH', '\"Mine is not the warmth of compassion.\"\n—Ertai, wizard adept').
card_multiverse_id('cone of flame'/'WTH', '4541').

card_in_set('debt of loyalty', 'WTH').
card_original_type('debt of loyalty'/'WTH', 'Instant').
card_original_text('debt of loyalty'/'WTH', 'Regenerate target creature. Gain control of that creature.').
card_first_print('debt of loyalty', 'WTH').
card_image_name('debt of loyalty'/'WTH', 'debt of loyalty').
card_uid('debt of loyalty'/'WTH', 'WTH:Debt of Loyalty:debt of loyalty').
card_rarity('debt of loyalty'/'WTH', 'Rare').
card_artist('debt of loyalty'/'WTH', 'Pete Venters').
card_flavor_text('debt of loyalty'/'WTH', '\"I killed him because I had to,\" Starke lied to Gerrard. \"But now I pledge my loyalty to you.\"').
card_multiverse_id('debt of loyalty'/'WTH', '4573').

card_in_set('dense foliage', 'WTH').
card_original_type('dense foliage'/'WTH', 'Enchantment').
card_original_text('dense foliage'/'WTH', 'Creatures cannot be the target of spells.').
card_first_print('dense foliage', 'WTH').
card_image_name('dense foliage'/'WTH', 'dense foliage').
card_uid('dense foliage'/'WTH', 'WTH:Dense Foliage:dense foliage').
card_rarity('dense foliage'/'WTH', 'Rare').
card_artist('dense foliage'/'WTH', 'Alan Rabinowitz').
card_flavor_text('dense foliage'/'WTH', '\"Big plants not only good for hidin\', but full o\' tasty bugs, too.\"\n—Squee, goblin cabin hand').
card_multiverse_id('dense foliage'/'WTH', '4512').

card_in_set('desperate gambit', 'WTH').
card_original_type('desperate gambit'/'WTH', 'Instant').
card_original_text('desperate gambit'/'WTH', 'Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, double the damage dealt by a source you control. Otherwise, prevent all damage from that source.').
card_first_print('desperate gambit', 'WTH').
card_image_name('desperate gambit'/'WTH', 'desperate gambit').
card_uid('desperate gambit'/'WTH', 'WTH:Desperate Gambit:desperate gambit').
card_rarity('desperate gambit'/'WTH', 'Uncommon').
card_artist('desperate gambit'/'WTH', 'Pete Venters').
card_multiverse_id('desperate gambit'/'WTH', '4542').

card_in_set('dingus staff', 'WTH').
card_original_type('dingus staff'/'WTH', 'Artifact').
card_original_text('dingus staff'/'WTH', 'Whenever a creature is put into any graveyard from play, Dingus Staff deals 2 damage to that creature\'s controller.').
card_first_print('dingus staff', 'WTH').
card_image_name('dingus staff'/'WTH', 'dingus staff').
card_uid('dingus staff'/'WTH', 'WTH:Dingus Staff:dingus staff').
card_rarity('dingus staff'/'WTH', 'Uncommon').
card_artist('dingus staff'/'WTH', 'Richard Kane Ferguson').
card_flavor_text('dingus staff'/'WTH', '\"A sharp conscience is the weapon of the soul.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('dingus staff'/'WTH', '4432').

card_in_set('disrupt', 'WTH').
card_original_type('disrupt'/'WTH', 'Interrupt').
card_original_text('disrupt'/'WTH', 'Counter target instant, interrupt, or sorcery spell unless its caster pays an additional {1}.\nDraw a card.').
card_first_print('disrupt', 'WTH').
card_image_name('disrupt'/'WTH', 'disrupt').
card_uid('disrupt'/'WTH', 'WTH:Disrupt:disrupt').
card_rarity('disrupt'/'WTH', 'Common').
card_artist('disrupt'/'WTH', 'Adam Rex').
card_flavor_text('disrupt'/'WTH', 'Oh, I\'m sorry—did I break your concentration?').
card_multiverse_id('disrupt'/'WTH', '4483').

card_in_set('doomsday', 'WTH').
card_original_type('doomsday'/'WTH', 'Sorcery').
card_original_text('doomsday'/'WTH', 'Pay half your life, rounded up: Put your graveyard on top of your library, then remove all but five cards of your library from the game. Put the rest on top of your library in any order.').
card_first_print('doomsday', 'WTH').
card_image_name('doomsday'/'WTH', 'doomsday').
card_uid('doomsday'/'WTH', 'WTH:Doomsday:doomsday').
card_rarity('doomsday'/'WTH', 'Rare').
card_artist('doomsday'/'WTH', 'Adrian Smith').
card_multiverse_id('doomsday'/'WTH', '4454').

card_in_set('downdraft', 'WTH').
card_original_type('downdraft'/'WTH', 'Enchantment').
card_original_text('downdraft'/'WTH', '{G} Target creature loses flying until end of turn.\nSacrifice Downdraft: Downdraft deals 2 damage to each creature with flying.').
card_first_print('downdraft', 'WTH').
card_image_name('downdraft'/'WTH', 'downdraft').
card_uid('downdraft'/'WTH', 'WTH:Downdraft:downdraft').
card_rarity('downdraft'/'WTH', 'Uncommon').
card_artist('downdraft'/'WTH', 'John Matson').
card_multiverse_id('downdraft'/'WTH', '4513').

card_in_set('duskrider falcon', 'WTH').
card_original_type('duskrider falcon'/'WTH', 'Summon — Falcon').
card_original_text('duskrider falcon'/'WTH', 'Flying, protection from black').
card_first_print('duskrider falcon', 'WTH').
card_image_name('duskrider falcon'/'WTH', 'duskrider falcon').
card_uid('duskrider falcon'/'WTH', 'WTH:Duskrider Falcon:duskrider falcon').
card_rarity('duskrider falcon'/'WTH', 'Common').
card_artist('duskrider falcon'/'WTH', 'Cecil Fernando').
card_flavor_text('duskrider falcon'/'WTH', '\"Seaborne ships have their dolphins to dance in their wakes; I have my duskriders.\"\n—Sisay, journal').
card_multiverse_id('duskrider falcon'/'WTH', '4574').

card_in_set('dwarven berserker', 'WTH').
card_original_type('dwarven berserker'/'WTH', 'Summon — Dwarf').
card_original_text('dwarven berserker'/'WTH', 'If Dwarven Berserker is blocked, it gets +3/+0 and gains trample until end of turn.').
card_first_print('dwarven berserker', 'WTH').
card_image_name('dwarven berserker'/'WTH', 'dwarven berserker').
card_uid('dwarven berserker'/'WTH', 'WTH:Dwarven Berserker:dwarven berserker').
card_rarity('dwarven berserker'/'WTH', 'Common').
card_artist('dwarven berserker'/'WTH', 'Douglas Shuler').
card_flavor_text('dwarven berserker'/'WTH', '\"I may be small, but I can kick your butt.\"').
card_multiverse_id('dwarven berserker'/'WTH', '4543').

card_in_set('dwarven thaumaturgist', 'WTH').
card_original_type('dwarven thaumaturgist'/'WTH', 'Summon — Dwarf').
card_original_text('dwarven thaumaturgist'/'WTH', '{T}: Switch power and toughness of target creature until end of turn. Effects that alter that creature\'s power alter its toughness instead, and vice versa, until end of turn.').
card_first_print('dwarven thaumaturgist', 'WTH').
card_image_name('dwarven thaumaturgist'/'WTH', 'dwarven thaumaturgist').
card_uid('dwarven thaumaturgist'/'WTH', 'WTH:Dwarven Thaumaturgist:dwarven thaumaturgist').
card_rarity('dwarven thaumaturgist'/'WTH', 'Rare').
card_artist('dwarven thaumaturgist'/'WTH', 'Kipling West').
card_multiverse_id('dwarven thaumaturgist'/'WTH', '4544').

card_in_set('empyrial armor', 'WTH').
card_original_type('empyrial armor'/'WTH', 'Enchant Creature').
card_original_text('empyrial armor'/'WTH', 'Enchanted creature gets +X/+X, where X is equal to the number of cards in your hand.').
card_image_name('empyrial armor'/'WTH', 'empyrial armor').
card_uid('empyrial armor'/'WTH', 'WTH:Empyrial Armor:empyrial armor').
card_rarity('empyrial armor'/'WTH', 'Common').
card_artist('empyrial armor'/'WTH', 'D. Alexander Gregory').
card_flavor_text('empyrial armor'/'WTH', '\"An angel appeared in the smoldering skies above the fray, her clothes as flames, her armor as fire.\"\n—\"Hymn of Angelfire\"').
card_multiverse_id('empyrial armor'/'WTH', '4575').

card_in_set('ertai\'s familiar', 'WTH').
card_original_type('ertai\'s familiar'/'WTH', 'Summon — Illusion').
card_original_text('ertai\'s familiar'/'WTH', 'Phasing\nIf Ertai\'s Familiar leaves play, put the top three cards of your library into your graveyard.\n{U} Ertai\'s Familiar cannot phase out until the beginning of your next upkeep.').
card_first_print('ertai\'s familiar', 'WTH').
card_image_name('ertai\'s familiar'/'WTH', 'ertai\'s familiar').
card_uid('ertai\'s familiar'/'WTH', 'WTH:Ertai\'s Familiar:ertai\'s familiar').
card_rarity('ertai\'s familiar'/'WTH', 'Rare').
card_artist('ertai\'s familiar'/'WTH', 'Kipling West').
card_multiverse_id('ertai\'s familiar'/'WTH', '4484').

card_in_set('fallow wurm', 'WTH').
card_original_type('fallow wurm'/'WTH', 'Summon — Wurm').
card_original_text('fallow wurm'/'WTH', 'When Fallow Wurm comes into play, choose and discard a land card or bury Fallow Wurm.').
card_first_print('fallow wurm', 'WTH').
card_image_name('fallow wurm'/'WTH', 'fallow wurm').
card_uid('fallow wurm'/'WTH', 'WTH:Fallow Wurm:fallow wurm').
card_rarity('fallow wurm'/'WTH', 'Uncommon').
card_artist('fallow wurm'/'WTH', 'Stephen L. Walsh').
card_flavor_text('fallow wurm'/'WTH', 'The wurm\'s wake\n—Elvish expression meaning\n\"ruined crops\"').
card_multiverse_id('fallow wurm'/'WTH', '4514').

card_in_set('familiar ground', 'WTH').
card_original_type('familiar ground'/'WTH', 'Enchantment').
card_original_text('familiar ground'/'WTH', 'Each creature you control cannot be blocked by more than one creature.').
card_first_print('familiar ground', 'WTH').
card_image_name('familiar ground'/'WTH', 'familiar ground').
card_uid('familiar ground'/'WTH', 'WTH:Familiar Ground:familiar ground').
card_rarity('familiar ground'/'WTH', 'Uncommon').
card_artist('familiar ground'/'WTH', 'Jeff Miracola').
card_flavor_text('familiar ground'/'WTH', '\"I\'d rather fight on foreign soil. That way, if I lose I can blame it on the terrain.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('familiar ground'/'WTH', '4515').

card_in_set('fatal blow', 'WTH').
card_original_type('fatal blow'/'WTH', 'Instant').
card_original_text('fatal blow'/'WTH', 'Bury target creature that was damaged this turn.').
card_first_print('fatal blow', 'WTH').
card_image_name('fatal blow'/'WTH', 'fatal blow').
card_uid('fatal blow'/'WTH', 'WTH:Fatal Blow:fatal blow').
card_rarity('fatal blow'/'WTH', 'Common').
card_artist('fatal blow'/'WTH', 'George Pratt').
card_flavor_text('fatal blow'/'WTH', '\"What is crueler? To let a wound of the heart fester, or to simply cut it out?\"\n—Crovax').
card_multiverse_id('fatal blow'/'WTH', '4455').

card_in_set('fervor', 'WTH').
card_original_type('fervor'/'WTH', 'Enchantment').
card_original_text('fervor'/'WTH', 'All creatures you control are unaffected by summoning sickness.').
card_first_print('fervor', 'WTH').
card_image_name('fervor'/'WTH', 'fervor').
card_uid('fervor'/'WTH', 'WTH:Fervor:fervor').
card_rarity('fervor'/'WTH', 'Rare').
card_artist('fervor'/'WTH', 'Franz Vohwinkel').
card_flavor_text('fervor'/'WTH', '\"If your blood doesn\'t run hot, I will make it run in the sands!\"\n—Maraxus of Keld').
card_multiverse_id('fervor'/'WTH', '4545').

card_in_set('festering evil', 'WTH').
card_original_type('festering evil'/'WTH', 'Enchantment').
card_original_text('festering evil'/'WTH', 'During your upkeep, Festering Evil deals 1 damage to each creature and player.\n{B}{B}, Sacrifice Festering Evil: Festering Evil deals 3 damage to each creature and player.').
card_first_print('festering evil', 'WTH').
card_image_name('festering evil'/'WTH', 'festering evil').
card_uid('festering evil'/'WTH', 'WTH:Festering Evil:festering evil').
card_rarity('festering evil'/'WTH', 'Uncommon').
card_artist('festering evil'/'WTH', 'John Matson').
card_multiverse_id('festering evil'/'WTH', '4456').

card_in_set('fire whip', 'WTH').
card_original_type('fire whip'/'WTH', 'Enchant Creature').
card_original_text('fire whip'/'WTH', 'Play only on a creature you control.\nTap enchanted creature: Enchanted creature deals 1 damage to target creature or player.\nSacrifice Fire Whip: Fire Whip deals 1 damage to target creature or player.').
card_first_print('fire whip', 'WTH').
card_image_name('fire whip'/'WTH', 'fire whip').
card_uid('fire whip'/'WTH', 'WTH:Fire Whip:fire whip').
card_rarity('fire whip'/'WTH', 'Common').
card_artist('fire whip'/'WTH', 'Jeff Miracola').
card_multiverse_id('fire whip'/'WTH', '4546').

card_in_set('firestorm', 'WTH').
card_original_type('firestorm'/'WTH', 'Instant').
card_original_text('firestorm'/'WTH', 'Choose and discard X cards: Firestorm deals X damage to each of X target creatures and/or players.').
card_first_print('firestorm', 'WTH').
card_image_name('firestorm'/'WTH', 'firestorm').
card_uid('firestorm'/'WTH', 'WTH:Firestorm:firestorm').
card_rarity('firestorm'/'WTH', 'Rare').
card_artist('firestorm'/'WTH', 'Jeff Miracola').
card_flavor_text('firestorm'/'WTH', '\"Glok loved storms! He\'d sit an\' watch an\' laugh through the whole thing. I miss him.\"\n—Squee, goblin cabin hand').
card_multiverse_id('firestorm'/'WTH', '4547').

card_in_set('fit of rage', 'WTH').
card_original_type('fit of rage'/'WTH', 'Sorcery').
card_original_text('fit of rage'/'WTH', 'Target creature gets +3/+3 and gains first strike until end of turn.').
card_first_print('fit of rage', 'WTH').
card_image_name('fit of rage'/'WTH', 'fit of rage').
card_uid('fit of rage'/'WTH', 'WTH:Fit of Rage:fit of rage').
card_rarity('fit of rage'/'WTH', 'Common').
card_artist('fit of rage'/'WTH', 'Douglas Shuler').
card_flavor_text('fit of rage'/'WTH', '\"Anger is fleeting; remorse is eternal.\"\n—Karn, silver golem').
card_multiverse_id('fit of rage'/'WTH', '4548').

card_in_set('fledgling djinn', 'WTH').
card_original_type('fledgling djinn'/'WTH', 'Summon — Djinn').
card_original_text('fledgling djinn'/'WTH', 'Flying\nDuring your upkeep, Fledgling Djinn deals 1 damage to you.').
card_first_print('fledgling djinn', 'WTH').
card_image_name('fledgling djinn'/'WTH', 'fledgling djinn').
card_uid('fledgling djinn'/'WTH', 'WTH:Fledgling Djinn:fledgling djinn').
card_rarity('fledgling djinn'/'WTH', 'Common').
card_artist('fledgling djinn'/'WTH', 'Thomas Gianni').
card_flavor_text('fledgling djinn'/'WTH', '\"The young can be quite dangerous. Trust me, I should know.\"\n—Ertai, wizard adept').
card_multiverse_id('fledgling djinn'/'WTH', '4457').

card_in_set('flux', 'WTH').
card_original_type('flux'/'WTH', 'Sorcery').
card_original_text('flux'/'WTH', 'Each player chooses and discards any number of cards, then draws that many cards.\nDraw a card.').
card_image_name('flux'/'WTH', 'flux').
card_uid('flux'/'WTH', 'WTH:Flux:flux').
card_rarity('flux'/'WTH', 'Common').
card_artist('flux'/'WTH', 'Richard Kane Ferguson').
card_flavor_text('flux'/'WTH', '\"Every spell has the potential to be worthless.\"\n—Ertai, wizard adept').
card_multiverse_id('flux'/'WTH', '4485').

card_in_set('fog elemental', 'WTH').
card_original_type('fog elemental'/'WTH', 'Summon — Elemental').
card_original_text('fog elemental'/'WTH', 'Flying\nIf Fog Elemental attacks or blocks, bury it at end of combat.').
card_first_print('fog elemental', 'WTH').
card_image_name('fog elemental'/'WTH', 'fog elemental').
card_uid('fog elemental'/'WTH', 'WTH:Fog Elemental:fog elemental').
card_rarity('fog elemental'/'WTH', 'Common').
card_artist('fog elemental'/'WTH', 'Jon J. Muth').
card_flavor_text('fog elemental'/'WTH', '\"I\'ve seen fog so thick you could cut it, but none that could cut me.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('fog elemental'/'WTH', '4486').

card_in_set('foriysian brigade', 'WTH').
card_original_type('foriysian brigade'/'WTH', 'Summon — Soldiers').
card_original_text('foriysian brigade'/'WTH', 'Foriysian Brigade may block two creatures each combat. (All blocking assignments must still be legal.)').
card_first_print('foriysian brigade', 'WTH').
card_image_name('foriysian brigade'/'WTH', 'foriysian brigade').
card_uid('foriysian brigade'/'WTH', 'WTH:Foriysian Brigade:foriysian brigade').
card_rarity('foriysian brigade'/'WTH', 'Uncommon').
card_artist('foriysian brigade'/'WTH', 'Kev Walker').
card_flavor_text('foriysian brigade'/'WTH', '\"A double-edged sword lets you cut down your enemies with the backswing as well.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('foriysian brigade'/'WTH', '4576').

card_in_set('fungus elemental', 'WTH').
card_original_type('fungus elemental'/'WTH', 'Summon — Elemental').
card_original_text('fungus elemental'/'WTH', '{G}, Sacrifice a forest: Put a +2/+2 counter on Fungus Elemental. Use this ability only if Fungus Elemental came into play this turn.').
card_first_print('fungus elemental', 'WTH').
card_image_name('fungus elemental'/'WTH', 'fungus elemental').
card_uid('fungus elemental'/'WTH', 'WTH:Fungus Elemental:fungus elemental').
card_rarity('fungus elemental'/'WTH', 'Rare').
card_artist('fungus elemental'/'WTH', 'Scott M. Fischer').
card_flavor_text('fungus elemental'/'WTH', 'I rot growth and grow rot. What am I?\n—Elvish riddle').
card_multiverse_id('fungus elemental'/'WTH', '4516').

card_in_set('gaea\'s blessing', 'WTH').
card_original_type('gaea\'s blessing'/'WTH', 'Sorcery').
card_original_text('gaea\'s blessing'/'WTH', 'Target player shuffles up to three target cards from his or her graveyard into his or her library.\nDraw a card.\nIf Gaea\'s Blessing is put into your graveyard from your library, shuffle your graveyard into your library.').
card_image_name('gaea\'s blessing'/'WTH', 'gaea\'s blessing').
card_uid('gaea\'s blessing'/'WTH', 'WTH:Gaea\'s Blessing:gaea\'s blessing').
card_rarity('gaea\'s blessing'/'WTH', 'Uncommon').
card_artist('gaea\'s blessing'/'WTH', 'Rebecca Guay').
card_multiverse_id('gaea\'s blessing'/'WTH', '4517').

card_in_set('gallowbraid', 'WTH').
card_original_type('gallowbraid'/'WTH', 'Summon — Legend').
card_original_text('gallowbraid'/'WTH', 'Trample\nCumulative upkeep—1 life').
card_first_print('gallowbraid', 'WTH').
card_image_name('gallowbraid'/'WTH', 'gallowbraid').
card_uid('gallowbraid'/'WTH', 'WTH:Gallowbraid:gallowbraid').
card_rarity('gallowbraid'/'WTH', 'Rare').
card_artist('gallowbraid'/'WTH', 'Carl Critchlow').
card_flavor_text('gallowbraid'/'WTH', '\"If its skin looks like stone, it is only to match its heart.\"\n—Crovax').
card_multiverse_id('gallowbraid'/'WTH', '4458').

card_in_set('gemstone mine', 'WTH').
card_original_type('gemstone mine'/'WTH', 'Land').
card_original_text('gemstone mine'/'WTH', 'When Gemstone Mine comes into play, put three mining counters on it.\n{T}, Remove a mining counter from Gemstone Mine: Add one mana of any color to your mana pool. If there are no mining counters on Gemstone Mine, bury it.').
card_first_print('gemstone mine', 'WTH').
card_image_name('gemstone mine'/'WTH', 'gemstone mine').
card_uid('gemstone mine'/'WTH', 'WTH:Gemstone Mine:gemstone mine').
card_rarity('gemstone mine'/'WTH', 'Uncommon').
card_artist('gemstone mine'/'WTH', 'Brom').
card_multiverse_id('gemstone mine'/'WTH', '4592').

card_in_set('gerrard\'s wisdom', 'WTH').
card_original_type('gerrard\'s wisdom'/'WTH', 'Sorcery').
card_original_text('gerrard\'s wisdom'/'WTH', 'For each card in your hand, gain 2 life.').
card_first_print('gerrard\'s wisdom', 'WTH').
card_image_name('gerrard\'s wisdom'/'WTH', 'gerrard\'s wisdom').
card_uid('gerrard\'s wisdom'/'WTH', 'WTH:Gerrard\'s Wisdom:gerrard\'s wisdom').
card_rarity('gerrard\'s wisdom'/'WTH', 'Uncommon').
card_artist('gerrard\'s wisdom'/'WTH', 'Heather Hudson').
card_flavor_text('gerrard\'s wisdom'/'WTH', '\"Fighting without an army is called a duel, and you\'ll lose a duel if your enemy comes expecting a war.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('gerrard\'s wisdom'/'WTH', '4577').

card_in_set('goblin bomb', 'WTH').
card_original_type('goblin bomb'/'WTH', 'Enchantment').
card_original_text('goblin bomb'/'WTH', 'During your upkeep, you may choose to flip a coin. Target opponent calls heads or tails while the coin is in the air. If the flip ends up in your favor, put a fuse counter on Goblin Bomb. Otherwise, remove a fuse counter from Goblin Bomb.\nRemove 5 fuse counters from Goblin Bomb, Sacrifice Goblin Bomb: Goblin Bomb deals 20 damage to target player.').
card_first_print('goblin bomb', 'WTH').
card_image_name('goblin bomb'/'WTH', 'goblin bomb').
card_uid('goblin bomb'/'WTH', 'WTH:Goblin Bomb:goblin bomb').
card_rarity('goblin bomb'/'WTH', 'Rare').
card_artist('goblin bomb'/'WTH', 'Ron Spencer').
card_multiverse_id('goblin bomb'/'WTH', '4549').

card_in_set('goblin grenadiers', 'WTH').
card_original_type('goblin grenadiers'/'WTH', 'Summon — Goblins').
card_original_text('goblin grenadiers'/'WTH', 'Sacrifice Goblin Grenadiers: Destroy target creature and target land. Use this ability only if Goblin Grenadiers is attacking and unblocked.').
card_first_print('goblin grenadiers', 'WTH').
card_image_name('goblin grenadiers'/'WTH', 'goblin grenadiers').
card_uid('goblin grenadiers'/'WTH', 'WTH:Goblin Grenadiers:goblin grenadiers').
card_rarity('goblin grenadiers'/'WTH', 'Uncommon').
card_artist('goblin grenadiers'/'WTH', 'Dan Frazier').
card_flavor_text('goblin grenadiers'/'WTH', 'For once, Squee realized, he wasn\'t the dumb one.').
card_multiverse_id('goblin grenadiers'/'WTH', '4550').

card_in_set('goblin vandal', 'WTH').
card_original_type('goblin vandal'/'WTH', 'Summon — Goblin').
card_original_text('goblin vandal'/'WTH', '{R} Destroy target artifact defending player controls. Goblin Vandal deals no combat damage this turn. Use this ability only if Goblin Vandal is attacking and unblocked and only once each turn.').
card_first_print('goblin vandal', 'WTH').
card_image_name('goblin vandal'/'WTH', 'goblin vandal').
card_uid('goblin vandal'/'WTH', 'WTH:Goblin Vandal:goblin vandal').
card_rarity('goblin vandal'/'WTH', 'Common').
card_artist('goblin vandal'/'WTH', 'Franz Vohwinkel').
card_multiverse_id('goblin vandal'/'WTH', '4551').

card_in_set('guided strike', 'WTH').
card_original_type('guided strike'/'WTH', 'Instant').
card_original_text('guided strike'/'WTH', 'Target creature gets +1/+0 and gains first strike until end of turn.\nDraw a card.').
card_first_print('guided strike', 'WTH').
card_image_name('guided strike'/'WTH', 'guided strike').
card_uid('guided strike'/'WTH', 'WTH:Guided Strike:guided strike').
card_rarity('guided strike'/'WTH', 'Common').
card_artist('guided strike'/'WTH', 'Gary Leach').
card_flavor_text('guided strike'/'WTH', '\"If you can kill your enemy with the first stroke, you save yourself the labor of the second.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('guided strike'/'WTH', '4578').

card_in_set('harvest wurm', 'WTH').
card_original_type('harvest wurm'/'WTH', 'Summon — Wurm').
card_original_text('harvest wurm'/'WTH', 'When Harvest Wurm comes into play, return any basic land card from your graveyard to your hand or bury Harvest Wurm.').
card_first_print('harvest wurm', 'WTH').
card_image_name('harvest wurm'/'WTH', 'harvest wurm').
card_uid('harvest wurm'/'WTH', 'WTH:Harvest Wurm:harvest wurm').
card_rarity('harvest wurm'/'WTH', 'Common').
card_artist('harvest wurm'/'WTH', 'Stephen L. Walsh').
card_flavor_text('harvest wurm'/'WTH', 'The wurm\'s weave\n—Elvish expression meaning\n\"plowed fields\"').
card_multiverse_id('harvest wurm'/'WTH', '4518').

card_in_set('haunting misery', 'WTH').
card_original_type('haunting misery'/'WTH', 'Sorcery').
card_original_text('haunting misery'/'WTH', 'Remove X creature cards in your graveyard from the game: Haunting Misery deals X damage to target player.').
card_first_print('haunting misery', 'WTH').
card_image_name('haunting misery'/'WTH', 'haunting misery').
card_uid('haunting misery'/'WTH', 'WTH:Haunting Misery:haunting misery').
card_rarity('haunting misery'/'WTH', 'Common').
card_artist('haunting misery'/'WTH', 'Gary Leach').
card_flavor_text('haunting misery'/'WTH', '\"I am condemned without end.\"\n—Crovax').
card_multiverse_id('haunting misery'/'WTH', '4459').

card_in_set('heart of bogardan', 'WTH').
card_original_type('heart of bogardan'/'WTH', 'Enchantment').
card_original_text('heart of bogardan'/'WTH', 'Cumulative upkeep {2}\nIf Heart of Bogardan\'s cumulative upkeep cost is not paid, Heart of Bogardan deals damage equal to its last paid cumulative upkeep to target player and each creature he or she controls.').
card_first_print('heart of bogardan', 'WTH').
card_image_name('heart of bogardan'/'WTH', 'heart of bogardan').
card_uid('heart of bogardan'/'WTH', 'WTH:Heart of Bogardan:heart of bogardan').
card_rarity('heart of bogardan'/'WTH', 'Rare').
card_artist('heart of bogardan'/'WTH', 'Terese Nielsen').
card_multiverse_id('heart of bogardan'/'WTH', '4552').

card_in_set('heat stroke', 'WTH').
card_original_type('heat stroke'/'WTH', 'Enchantment').
card_original_text('heat stroke'/'WTH', 'At the end of each combat, destroy all creatures that blocked or were blocked this turn.').
card_first_print('heat stroke', 'WTH').
card_image_name('heat stroke'/'WTH', 'heat stroke').
card_uid('heat stroke'/'WTH', 'WTH:Heat Stroke:heat stroke').
card_rarity('heat stroke'/'WTH', 'Rare').
card_artist('heat stroke'/'WTH', 'Andrew Robinson').
card_flavor_text('heat stroke'/'WTH', '\"Stroke of heat or stroke of blade. Choose your fate, traitor.\"\n—Maraxus of Keld, to Starke').
card_multiverse_id('heat stroke'/'WTH', '4553').

card_in_set('heavy ballista', 'WTH').
card_original_type('heavy ballista'/'WTH', 'Summon — Soldiers').
card_original_text('heavy ballista'/'WTH', '{T}: Heavy Ballista deals 2 damage to target attacking or blocking creature.').
card_first_print('heavy ballista', 'WTH').
card_image_name('heavy ballista'/'WTH', 'heavy ballista').
card_uid('heavy ballista'/'WTH', 'WTH:Heavy Ballista:heavy ballista').
card_rarity('heavy ballista'/'WTH', 'Common').
card_artist('heavy ballista'/'WTH', 'Ron Spencer').
card_flavor_text('heavy ballista'/'WTH', '\"Archers, ballistae—you can\'t even get near the island of Avenant.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('heavy ballista'/'WTH', '4579').

card_in_set('hidden horror', 'WTH').
card_original_type('hidden horror'/'WTH', 'Summon — Undead').
card_original_text('hidden horror'/'WTH', 'When Hidden Horror comes into play, choose and discard a creature card or bury Hidden Horror.').
card_first_print('hidden horror', 'WTH').
card_image_name('hidden horror'/'WTH', 'hidden horror').
card_uid('hidden horror'/'WTH', 'WTH:Hidden Horror:hidden horror').
card_rarity('hidden horror'/'WTH', 'Uncommon').
card_artist('hidden horror'/'WTH', 'Clint Langley').
card_flavor_text('hidden horror'/'WTH', 'Lifespans are measured by its dreadful length.').
card_multiverse_id('hidden horror'/'WTH', '4460').

card_in_set('hurloon shaman', 'WTH').
card_original_type('hurloon shaman'/'WTH', 'Summon — Minotaur').
card_original_text('hurloon shaman'/'WTH', 'If Hurloon Shaman is put into any graveyard from play, each player chooses and buries a land he or she controls.').
card_first_print('hurloon shaman', 'WTH').
card_image_name('hurloon shaman'/'WTH', 'hurloon shaman').
card_uid('hurloon shaman'/'WTH', 'WTH:Hurloon Shaman:hurloon shaman').
card_rarity('hurloon shaman'/'WTH', 'Uncommon').
card_artist('hurloon shaman'/'WTH', 'Scott M. Fischer').
card_flavor_text('hurloon shaman'/'WTH', '\"I believe it when they say they\'re connected to the land—looks like somebody plowed her face.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('hurloon shaman'/'WTH', '4554').

card_in_set('infernal tribute', 'WTH').
card_original_type('infernal tribute'/'WTH', 'Enchantment').
card_original_text('infernal tribute'/'WTH', '{2}, Sacrifice a card in play: Draw a card.').
card_first_print('infernal tribute', 'WTH').
card_image_name('infernal tribute'/'WTH', 'infernal tribute').
card_uid('infernal tribute'/'WTH', 'WTH:Infernal Tribute:infernal tribute').
card_rarity('infernal tribute'/'WTH', 'Rare').
card_artist('infernal tribute'/'WTH', 'Terese Nielsen').
card_flavor_text('infernal tribute'/'WTH', '\"I would not barter my soul for any of the filth that they called power. So they took it from me and damned me to solitude.\"\n—Crovax').
card_multiverse_id('infernal tribute'/'WTH', '4461').

card_in_set('inner sanctum', 'WTH').
card_original_type('inner sanctum'/'WTH', 'Enchantment').
card_original_text('inner sanctum'/'WTH', 'Cumulative upkeep—2 life\nAll damage dealt to creatures you control is reduced to 0.').
card_first_print('inner sanctum', 'WTH').
card_image_name('inner sanctum'/'WTH', 'inner sanctum').
card_uid('inner sanctum'/'WTH', 'WTH:Inner Sanctum:inner sanctum').
card_rarity('inner sanctum'/'WTH', 'Rare').
card_artist('inner sanctum'/'WTH', 'D. Alexander Gregory').
card_flavor_text('inner sanctum'/'WTH', '\"Save me from Maraxus,\" Starke pleaded, \"or condemn me to his wrath. Either way, do not ignore me!\"').
card_multiverse_id('inner sanctum'/'WTH', '4580').

card_in_set('jabari\'s banner', 'WTH').
card_original_type('jabari\'s banner'/'WTH', 'Artifact').
card_original_text('jabari\'s banner'/'WTH', '{1}, {T}: Target creature gains flanking until end of turn. (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('jabari\'s banner', 'WTH').
card_image_name('jabari\'s banner'/'WTH', 'jabari\'s banner').
card_uid('jabari\'s banner'/'WTH', 'WTH:Jabari\'s Banner:jabari\'s banner').
card_rarity('jabari\'s banner'/'WTH', 'Uncommon').
card_artist('jabari\'s banner'/'WTH', 'Mark Harrison').
card_multiverse_id('jabari\'s banner'/'WTH', '4433').

card_in_set('jangling automaton', 'WTH').
card_original_type('jangling automaton'/'WTH', 'Artifact Creature').
card_original_text('jangling automaton'/'WTH', 'If Jangling Automaton attacks, untap all creatures defending player controls.').
card_first_print('jangling automaton', 'WTH').
card_image_name('jangling automaton'/'WTH', 'jangling automaton').
card_uid('jangling automaton'/'WTH', 'WTH:Jangling Automaton:jangling automaton').
card_rarity('jangling automaton'/'WTH', 'Common').
card_artist('jangling automaton'/'WTH', 'Adam Rex').
card_flavor_text('jangling automaton'/'WTH', '\"We always look upon our first creations as masterpieces, no matter how awful they are.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('jangling automaton'/'WTH', '4434').

card_in_set('kithkin armor', 'WTH').
card_original_type('kithkin armor'/'WTH', 'Enchant Creature').
card_original_text('kithkin armor'/'WTH', 'Enchanted creature cannot be blocked by creatures with power 3 or greater.\nSacrifice Kithkin Armor: Prevent all damage to enchanted creature from one source.').
card_first_print('kithkin armor', 'WTH').
card_image_name('kithkin armor'/'WTH', 'kithkin armor').
card_uid('kithkin armor'/'WTH', 'WTH:Kithkin Armor:kithkin armor').
card_rarity('kithkin armor'/'WTH', 'Common').
card_artist('kithkin armor'/'WTH', 'Charles Gillespie').
card_multiverse_id('kithkin armor'/'WTH', '4581').

card_in_set('lava hounds', 'WTH').
card_original_type('lava hounds'/'WTH', 'Summon — Hounds').
card_original_text('lava hounds'/'WTH', 'Lava Hounds is unaffected by summoning sickness.\nWhen Lava Hounds comes into play, it deals 4 damage to you.').
card_first_print('lava hounds', 'WTH').
card_image_name('lava hounds'/'WTH', 'lava hounds').
card_uid('lava hounds'/'WTH', 'WTH:Lava Hounds:lava hounds').
card_rarity('lava hounds'/'WTH', 'Uncommon').
card_artist('lava hounds'/'WTH', 'Steve White').
card_flavor_text('lava hounds'/'WTH', 'What cats bury, dogs eat.\n—Urborg aphorism').
card_multiverse_id('lava hounds'/'WTH', '4555').

card_in_set('lava storm', 'WTH').
card_original_type('lava storm'/'WTH', 'Instant').
card_original_text('lava storm'/'WTH', 'Lava Storm deals 2 damage to each attacking creature or 2 damage to each blocking creature.').
card_first_print('lava storm', 'WTH').
card_image_name('lava storm'/'WTH', 'lava storm').
card_uid('lava storm'/'WTH', 'WTH:Lava Storm:lava storm').
card_rarity('lava storm'/'WTH', 'Common').
card_artist('lava storm'/'WTH', 'Scott Kirschner').
card_flavor_text('lava storm'/'WTH', 'They say lava conquers all.').
card_multiverse_id('lava storm'/'WTH', '4556').

card_in_set('liege of the hollows', 'WTH').
card_original_type('liege of the hollows'/'WTH', 'Summon — Spirit').
card_original_text('liege of the hollows'/'WTH', 'If Liege of the Hollows is put into any graveyard from play, each player may pay any amount of mana to put that number of Squirrel tokens into play under his or her control. Treat those tokens as 1/1 green creatures.').
card_first_print('liege of the hollows', 'WTH').
card_image_name('liege of the hollows'/'WTH', 'liege of the hollows').
card_uid('liege of the hollows'/'WTH', 'WTH:Liege of the Hollows:liege of the hollows').
card_rarity('liege of the hollows'/'WTH', 'Rare').
card_artist('liege of the hollows'/'WTH', 'Ron Spencer').
card_multiverse_id('liege of the hollows'/'WTH', '4519').

card_in_set('llanowar behemoth', 'WTH').
card_original_type('llanowar behemoth'/'WTH', 'Summon — Behemoth').
card_original_text('llanowar behemoth'/'WTH', 'Tap a creature you control: +1/+1 until end of turn').
card_first_print('llanowar behemoth', 'WTH').
card_image_name('llanowar behemoth'/'WTH', 'llanowar behemoth').
card_uid('llanowar behemoth'/'WTH', 'WTH:Llanowar Behemoth:llanowar behemoth').
card_rarity('llanowar behemoth'/'WTH', 'Uncommon').
card_artist('llanowar behemoth'/'WTH', 'Hannibal King').
card_flavor_text('llanowar behemoth'/'WTH', '\"Most people can\'t build decent weapons out of stone or steel. Trust the elves to do it with only mud and vines.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('llanowar behemoth'/'WTH', '4520').

card_in_set('llanowar druid', 'WTH').
card_original_type('llanowar druid'/'WTH', 'Summon — Elf').
card_original_text('llanowar druid'/'WTH', '{T}, Sacrifice Llanowar Druid: Untap all forests.').
card_first_print('llanowar druid', 'WTH').
card_image_name('llanowar druid'/'WTH', 'llanowar druid').
card_uid('llanowar druid'/'WTH', 'WTH:Llanowar Druid:llanowar druid').
card_rarity('llanowar druid'/'WTH', 'Common').
card_artist('llanowar druid'/'WTH', 'Pete Venters').
card_flavor_text('llanowar druid'/'WTH', '\"This forest means more to the druids than their own kin. The loss of a tree is like the loss of a child.\"\n—Mirri of the Weatherlight').
card_multiverse_id('llanowar druid'/'WTH', '4521').

card_in_set('llanowar sentinel', 'WTH').
card_original_type('llanowar sentinel'/'WTH', 'Summon — Elf').
card_original_text('llanowar sentinel'/'WTH', 'When Llanowar Sentinel comes into play, you may pay {1}{G} to search your library for a Llanowar Sentinel card. Put that card into play. Shuffle your library afterwards.').
card_first_print('llanowar sentinel', 'WTH').
card_image_name('llanowar sentinel'/'WTH', 'llanowar sentinel').
card_uid('llanowar sentinel'/'WTH', 'WTH:Llanowar Sentinel:llanowar sentinel').
card_rarity('llanowar sentinel'/'WTH', 'Common').
card_artist('llanowar sentinel'/'WTH', 'Douglas Shuler').
card_flavor_text('llanowar sentinel'/'WTH', '\"The forest has as many eyes as leaves.\"\n—Mirri of the Weatherlight').
card_multiverse_id('llanowar sentinel'/'WTH', '4522').

card_in_set('lotus vale', 'WTH').
card_original_type('lotus vale'/'WTH', 'Land').
card_original_text('lotus vale'/'WTH', 'When Lotus Vale comes into play, sacrifice two untapped lands or bury Lotus Vale.\n{T}: Add three mana of any one color to your mana pool.').
card_first_print('lotus vale', 'WTH').
card_image_name('lotus vale'/'WTH', 'lotus vale').
card_uid('lotus vale'/'WTH', 'WTH:Lotus Vale:lotus vale').
card_rarity('lotus vale'/'WTH', 'Rare').
card_artist('lotus vale'/'WTH', 'John Avon').
card_flavor_text('lotus vale'/'WTH', 'At what price beauty?').
card_multiverse_id('lotus vale'/'WTH', '4593').

card_in_set('mana chains', 'WTH').
card_original_type('mana chains'/'WTH', 'Enchant Creature').
card_original_text('mana chains'/'WTH', 'Enchanted creature gains \"Cumulative upkeep {1}.\"').
card_first_print('mana chains', 'WTH').
card_image_name('mana chains'/'WTH', 'mana chains').
card_uid('mana chains'/'WTH', 'WTH:Mana Chains:mana chains').
card_rarity('mana chains'/'WTH', 'Common').
card_artist('mana chains'/'WTH', 'Bryan Talbot').
card_flavor_text('mana chains'/'WTH', '\"The Lord of the Wastes must be destroyed; I am bound to this destiny by the chains of fate. If only Gerrard would see his part.\"\n—Sisay, journal').
card_multiverse_id('mana chains'/'WTH', '4487').

card_in_set('mana web', 'WTH').
card_original_type('mana web'/'WTH', 'Artifact').
card_original_text('mana web'/'WTH', 'Whenever any land target opponent controls is tapped for mana, tap all lands he or she controls that can produce any type of mana that land can produce.').
card_first_print('mana web', 'WTH').
card_image_name('mana web'/'WTH', 'mana web').
card_uid('mana web'/'WTH', 'WTH:Mana Web:mana web').
card_rarity('mana web'/'WTH', 'Rare').
card_artist('mana web'/'WTH', 'Hannibal King').
card_multiverse_id('mana web'/'WTH', '4435').

card_in_set('manta ray', 'WTH').
card_original_type('manta ray'/'WTH', 'Summon — Fish').
card_original_text('manta ray'/'WTH', 'Islandhome (If defending player controls no islands, this creature cannot attack. If you control no islands, bury this creature.)\nManta Ray cannot be blocked except by blue creatures.').
card_first_print('manta ray', 'WTH').
card_image_name('manta ray'/'WTH', 'manta ray').
card_uid('manta ray'/'WTH', 'WTH:Manta Ray:manta ray').
card_rarity('manta ray'/'WTH', 'Common').
card_artist('manta ray'/'WTH', 'Una Fricker').
card_multiverse_id('manta ray'/'WTH', '4488').

card_in_set('maraxus of keld', 'WTH').
card_original_type('maraxus of keld'/'WTH', 'Summon — Legend').
card_original_text('maraxus of keld'/'WTH', 'Maraxus of Keld has power and toughness each equal to the total number of untapped artifacts, creatures, and lands you control.').
card_first_print('maraxus of keld', 'WTH').
card_image_name('maraxus of keld'/'WTH', 'maraxus of keld').
card_uid('maraxus of keld'/'WTH', 'WTH:Maraxus of Keld:maraxus of keld').
card_rarity('maraxus of keld'/'WTH', 'Rare').
card_artist('maraxus of keld'/'WTH', 'Adrian Smith').
card_flavor_text('maraxus of keld'/'WTH', '\"I have no master. I am chaos.\"\n—Maraxus of Keld').
card_multiverse_id('maraxus of keld'/'WTH', '4557').

card_in_set('master of arms', 'WTH').
card_original_type('master of arms'/'WTH', 'Summon — Soldier').
card_original_text('master of arms'/'WTH', 'First strike\n{1}{W} Tap target creature blocking Master of Arms.').
card_first_print('master of arms', 'WTH').
card_image_name('master of arms'/'WTH', 'master of arms').
card_uid('master of arms'/'WTH', 'WTH:Master of Arms:master of arms').
card_rarity('master of arms'/'WTH', 'Uncommon').
card_artist('master of arms'/'WTH', 'Dan Frazier').
card_flavor_text('master of arms'/'WTH', '\"Being the best usually means proving it to everyone.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('master of arms'/'WTH', '4582').

card_in_set('merfolk traders', 'WTH').
card_original_type('merfolk traders'/'WTH', 'Summon — Merfolk').
card_original_text('merfolk traders'/'WTH', 'When Merfolk Traders comes into play, draw a card, then choose and discard a card.').
card_first_print('merfolk traders', 'WTH').
card_image_name('merfolk traders'/'WTH', 'merfolk traders').
card_uid('merfolk traders'/'WTH', 'WTH:Merfolk Traders:merfolk traders').
card_rarity('merfolk traders'/'WTH', 'Common').
card_artist('merfolk traders'/'WTH', 'DiTerlizzi').
card_flavor_text('merfolk traders'/'WTH', '\"As much as I hate water, I do love fish . . . .\"\n—Mirri of the Weatherlight').
card_multiverse_id('merfolk traders'/'WTH', '4489').

card_in_set('mind stone', 'WTH').
card_original_type('mind stone'/'WTH', 'Artifact').
card_original_text('mind stone'/'WTH', '{T}: Add one colorless mana to your mana pool. Play this ability as a mana source.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_first_print('mind stone', 'WTH').
card_image_name('mind stone'/'WTH', 'mind stone').
card_uid('mind stone'/'WTH', 'WTH:Mind Stone:mind stone').
card_rarity('mind stone'/'WTH', 'Common').
card_artist('mind stone'/'WTH', 'Adam Rex').
card_multiverse_id('mind stone'/'WTH', '4436').

card_in_set('mischievous poltergeist', 'WTH').
card_original_type('mischievous poltergeist'/'WTH', 'Summon — Ghost').
card_original_text('mischievous poltergeist'/'WTH', 'Flying\nPay 1 life: Regenerate').
card_first_print('mischievous poltergeist', 'WTH').
card_image_name('mischievous poltergeist'/'WTH', 'mischievous poltergeist').
card_uid('mischievous poltergeist'/'WTH', 'WTH:Mischievous Poltergeist:mischievous poltergeist').
card_rarity('mischievous poltergeist'/'WTH', 'Uncommon').
card_artist('mischievous poltergeist'/'WTH', 'DiTerlizzi').
card_flavor_text('mischievous poltergeist'/'WTH', '\"The past is a ghost that haunts you from the moment it exists until the moment you don\'t.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('mischievous poltergeist'/'WTH', '4462').

card_in_set('mistmoon griffin', 'WTH').
card_original_type('mistmoon griffin'/'WTH', 'Summon — Griffin').
card_original_text('mistmoon griffin'/'WTH', 'Flying\nIf Mistmoon Griffin is put into any graveyard from play, remove Mistmoon Griffin from the game, then put the top creature card from your graveyard into play.').
card_first_print('mistmoon griffin', 'WTH').
card_image_name('mistmoon griffin'/'WTH', 'mistmoon griffin').
card_uid('mistmoon griffin'/'WTH', 'WTH:Mistmoon Griffin:mistmoon griffin').
card_rarity('mistmoon griffin'/'WTH', 'Uncommon').
card_artist('mistmoon griffin'/'WTH', 'David A. Cherry').
card_multiverse_id('mistmoon griffin'/'WTH', '4583').

card_in_set('morinfen', 'WTH').
card_original_type('morinfen'/'WTH', 'Summon — Legend').
card_original_text('morinfen'/'WTH', 'Flying\nCumulative upkeep—1 life').
card_first_print('morinfen', 'WTH').
card_image_name('morinfen'/'WTH', 'morinfen').
card_uid('morinfen'/'WTH', 'WTH:Morinfen:morinfen').
card_rarity('morinfen'/'WTH', 'Rare').
card_artist('morinfen'/'WTH', 'Carl Critchlow').
card_flavor_text('morinfen'/'WTH', '\"I looked into its eyes, and its soul was so empty I saw no reflection, no light there.\"\n—Crovax').
card_multiverse_id('morinfen'/'WTH', '4463').

card_in_set('mwonvuli ooze', 'WTH').
card_original_type('mwonvuli ooze'/'WTH', 'Summon — Ooze').
card_original_text('mwonvuli ooze'/'WTH', 'Cumulative upkeep {2}\nMwonvuli Ooze has power and toughness each equal to 1 plus its last paid cumulative upkeep.').
card_first_print('mwonvuli ooze', 'WTH').
card_image_name('mwonvuli ooze'/'WTH', 'mwonvuli ooze').
card_uid('mwonvuli ooze'/'WTH', 'WTH:Mwonvuli Ooze:mwonvuli ooze').
card_rarity('mwonvuli ooze'/'WTH', 'Rare').
card_artist('mwonvuli ooze'/'WTH', 'Zina Saunders').
card_flavor_text('mwonvuli ooze'/'WTH', '\"Ewww!\"').
card_multiverse_id('mwonvuli ooze'/'WTH', '4523').

card_in_set('nature\'s kiss', 'WTH').
card_original_type('nature\'s kiss'/'WTH', 'Enchant Creature').
card_original_text('nature\'s kiss'/'WTH', '{1}, Remove the top card in your graveyard from the game: Enchanted creature gets +1/+1 until end of turn.').
card_first_print('nature\'s kiss', 'WTH').
card_image_name('nature\'s kiss'/'WTH', 'nature\'s kiss').
card_uid('nature\'s kiss'/'WTH', 'WTH:Nature\'s Kiss:nature\'s kiss').
card_rarity('nature\'s kiss'/'WTH', 'Common').
card_artist('nature\'s kiss'/'WTH', 'Scott M. Fischer').
card_flavor_text('nature\'s kiss'/'WTH', '\"I cradle my rage even as the earth cradles my dead kin.\"\n—Mirri of the Weatherlight').
card_multiverse_id('nature\'s kiss'/'WTH', '4524').

card_in_set('nature\'s resurgence', 'WTH').
card_original_type('nature\'s resurgence'/'WTH', 'Sorcery').
card_original_text('nature\'s resurgence'/'WTH', 'Each player draws a number of cards equal to the number of creature cards in his or her graveyard.').
card_first_print('nature\'s resurgence', 'WTH').
card_image_name('nature\'s resurgence'/'WTH', 'nature\'s resurgence').
card_uid('nature\'s resurgence'/'WTH', 'WTH:Nature\'s Resurgence:nature\'s resurgence').
card_rarity('nature\'s resurgence'/'WTH', 'Rare').
card_artist('nature\'s resurgence'/'WTH', 'Scott M. Fischer').
card_flavor_text('nature\'s resurgence'/'WTH', 'Spring follows winter\n—Elvish expression meaning\n\"all things pass\"').
card_multiverse_id('nature\'s resurgence'/'WTH', '4525').

card_in_set('necratog', 'WTH').
card_original_type('necratog'/'WTH', 'Summon — Atog').
card_original_text('necratog'/'WTH', 'Remove the top creature card in your graveyard from the game: +2/+2 until end of turn').
card_first_print('necratog', 'WTH').
card_image_name('necratog'/'WTH', 'necratog').
card_uid('necratog'/'WTH', 'WTH:Necratog:necratog').
card_rarity('necratog'/'WTH', 'Uncommon').
card_artist('necratog'/'WTH', 'Bryan Talbot').
card_flavor_text('necratog'/'WTH', 'The necratog can always dig up a meal.').
card_multiverse_id('necratog'/'WTH', '4464').

card_in_set('noble benefactor', 'WTH').
card_original_type('noble benefactor'/'WTH', 'Summon — Cleric').
card_original_text('noble benefactor'/'WTH', 'If Noble Benefactor is put into any graveyard from play, each player may search his or her library for any one card and put that card into his or her hand. Each player who searches his or her library shuffles it afterwards.').
card_first_print('noble benefactor', 'WTH').
card_image_name('noble benefactor'/'WTH', 'noble benefactor').
card_uid('noble benefactor'/'WTH', 'WTH:Noble Benefactor:noble benefactor').
card_rarity('noble benefactor'/'WTH', 'Uncommon').
card_artist('noble benefactor'/'WTH', 'DiTerlizzi').
card_multiverse_id('noble benefactor'/'WTH', '4490').

card_in_set('null rod', 'WTH').
card_original_type('null rod'/'WTH', 'Artifact').
card_original_text('null rod'/'WTH', 'Players cannot play any artifact abilities requiring an activation cost.').
card_first_print('null rod', 'WTH').
card_image_name('null rod'/'WTH', 'null rod').
card_uid('null rod'/'WTH', 'WTH:Null Rod:null rod').
card_rarity('null rod'/'WTH', 'Rare').
card_artist('null rod'/'WTH', 'Anson Maddocks').
card_flavor_text('null rod'/'WTH', 'Gerrard: \"But it doesn\'t do anything!\"\nHanna: \"No—it does nothing.\"').
card_multiverse_id('null rod'/'WTH', '4437').

card_in_set('odylic wraith', 'WTH').
card_original_type('odylic wraith'/'WTH', 'Summon — Undead').
card_original_text('odylic wraith'/'WTH', 'Swampwalk\nIf Odylic Wraith damages any player, that player chooses and discards a card.').
card_first_print('odylic wraith', 'WTH').
card_image_name('odylic wraith'/'WTH', 'odylic wraith').
card_uid('odylic wraith'/'WTH', 'WTH:Odylic Wraith:odylic wraith').
card_rarity('odylic wraith'/'WTH', 'Uncommon').
card_artist('odylic wraith'/'WTH', 'Ian Miller').
card_flavor_text('odylic wraith'/'WTH', 'By the time you\'ve seen it, you won\'t remember that you did.').
card_multiverse_id('odylic wraith'/'WTH', '4465').

card_in_set('ophidian', 'WTH').
card_original_type('ophidian'/'WTH', 'Summon — Snake').
card_original_text('ophidian'/'WTH', '{0}: Draw a card. Ophidian deals no combat damage this turn. Use this ability only if Ophidian is attacking and unblocked and only once each turn.').
card_first_print('ophidian', 'WTH').
card_image_name('ophidian'/'WTH', 'ophidian').
card_uid('ophidian'/'WTH', 'WTH:Ophidian:ophidian').
card_rarity('ophidian'/'WTH', 'Common').
card_artist('ophidian'/'WTH', 'Cliff Nielsen').
card_multiverse_id('ophidian'/'WTH', '4491').

card_in_set('orcish settlers', 'WTH').
card_original_type('orcish settlers'/'WTH', 'Summon — Orcs').
card_original_text('orcish settlers'/'WTH', '{X}{X}{R}, {T}, Sacrifice Orcish Settlers: Destroy X target lands.').
card_first_print('orcish settlers', 'WTH').
card_image_name('orcish settlers'/'WTH', 'orcish settlers').
card_uid('orcish settlers'/'WTH', 'WTH:Orcish Settlers:orcish settlers').
card_rarity('orcish settlers'/'WTH', 'Uncommon').
card_artist('orcish settlers'/'WTH', 'Pete Venters').
card_flavor_text('orcish settlers'/'WTH', 'They wouldn\'t know their house from a charred hole in the ground.').
card_multiverse_id('orcish settlers'/'WTH', '4558').

card_in_set('paradigm shift', 'WTH').
card_original_type('paradigm shift'/'WTH', 'Sorcery').
card_original_text('paradigm shift'/'WTH', 'Remove all cards in your library from the game. Shuffle your graveyard into your library.').
card_first_print('paradigm shift', 'WTH').
card_image_name('paradigm shift'/'WTH', 'paradigm shift').
card_uid('paradigm shift'/'WTH', 'WTH:Paradigm Shift:paradigm shift').
card_rarity('paradigm shift'/'WTH', 'Rare').
card_artist('paradigm shift'/'WTH', 'Cliff Nielsen').
card_flavor_text('paradigm shift'/'WTH', '\"Barrin always said that reality is relative. ‘The world,\' he cautioned, ‘is but pieces of one\'s perception.\'\"\n—Ertai, wizard adept').
card_multiverse_id('paradigm shift'/'WTH', '4492').

card_in_set('peacekeeper', 'WTH').
card_original_type('peacekeeper'/'WTH', 'Summon — Peacekeeper').
card_original_text('peacekeeper'/'WTH', 'During your upkeep, pay {1}{W} or bury Peacekeeper.\nCreatures cannot attack.').
card_first_print('peacekeeper', 'WTH').
card_image_name('peacekeeper'/'WTH', 'peacekeeper').
card_uid('peacekeeper'/'WTH', 'WTH:Peacekeeper:peacekeeper').
card_rarity('peacekeeper'/'WTH', 'Rare').
card_artist('peacekeeper'/'WTH', 'Donato Giancola').
card_flavor_text('peacekeeper'/'WTH', '\"I have always imagined my mother as such a woman, strong and wise.\"\n—Sisay, journal').
card_multiverse_id('peacekeeper'/'WTH', '4584').

card_in_set('pendrell mists', 'WTH').
card_original_type('pendrell mists'/'WTH', 'Enchantment').
card_original_text('pendrell mists'/'WTH', 'Each creature gains \"During your upkeep, pay {1} or bury this creature.\"').
card_first_print('pendrell mists', 'WTH').
card_image_name('pendrell mists'/'WTH', 'pendrell mists').
card_uid('pendrell mists'/'WTH', 'WTH:Pendrell Mists:pendrell mists').
card_rarity('pendrell mists'/'WTH', 'Rare').
card_artist('pendrell mists'/'WTH', 'Andrew Robinson').
card_flavor_text('pendrell mists'/'WTH', 'As insubstantial—and as insistent—as appetite.').
card_multiverse_id('pendrell mists'/'WTH', '4493').

card_in_set('phantom warrior', 'WTH').
card_original_type('phantom warrior'/'WTH', 'Summon — Illusion').
card_original_text('phantom warrior'/'WTH', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'WTH', 'phantom warrior').
card_uid('phantom warrior'/'WTH', 'WTH:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'WTH', 'Uncommon').
card_artist('phantom warrior'/'WTH', 'John Matson').
card_flavor_text('phantom warrior'/'WTH', '\"Fishtails infest the water, and mists walk the land. Give me simple solid rock any day.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('phantom warrior'/'WTH', '4494').

card_in_set('phantom wings', 'WTH').
card_original_type('phantom wings'/'WTH', 'Enchant Creature').
card_original_text('phantom wings'/'WTH', 'Enchanted creature gains flying.\nSacrifice Phantom Wings: Return enchanted creature to owner\'s hand.').
card_first_print('phantom wings', 'WTH').
card_image_name('phantom wings'/'WTH', 'phantom wings').
card_uid('phantom wings'/'WTH', 'WTH:Phantom Wings:phantom wings').
card_rarity('phantom wings'/'WTH', 'Common').
card_artist('phantom wings'/'WTH', 'Una Fricker').
card_flavor_text('phantom wings'/'WTH', '\"But you said ‘when goats fly!\'\" Squee whined.').
card_multiverse_id('phantom wings'/'WTH', '4495').

card_in_set('phyrexian furnace', 'WTH').
card_original_type('phyrexian furnace'/'WTH', 'Artifact').
card_original_text('phyrexian furnace'/'WTH', '{T}: Remove the bottom card of target player\'s graveyard from the game.\n{1}, Sacrifice Phyrexian Furnace: Remove target card in any graveyard from the game and draw a card.').
card_first_print('phyrexian furnace', 'WTH').
card_image_name('phyrexian furnace'/'WTH', 'phyrexian furnace').
card_uid('phyrexian furnace'/'WTH', 'WTH:Phyrexian Furnace:phyrexian furnace').
card_rarity('phyrexian furnace'/'WTH', 'Uncommon').
card_artist('phyrexian furnace'/'WTH', 'George Pratt').
card_multiverse_id('phyrexian furnace'/'WTH', '4438').

card_in_set('psychic vortex', 'WTH').
card_original_type('psychic vortex'/'WTH', 'Enchantment').
card_original_text('psychic vortex'/'WTH', 'Cumulative upkeep—Draw a card\nAt the end of each of your turns, sacrifice a land and discard your hand.').
card_first_print('psychic vortex', 'WTH').
card_image_name('psychic vortex'/'WTH', 'psychic vortex').
card_uid('psychic vortex'/'WTH', 'WTH:Psychic Vortex:psychic vortex').
card_rarity('psychic vortex'/'WTH', 'Rare').
card_artist('psychic vortex'/'WTH', 'Steve Luke').
card_flavor_text('psychic vortex'/'WTH', '\"Tolaria floats upon a wheel of fortune.\"\n—Ertai, wizard adept').
card_multiverse_id('psychic vortex'/'WTH', '4496').

card_in_set('razortooth rats', 'WTH').
card_original_type('razortooth rats'/'WTH', 'Summon — Rats').
card_original_text('razortooth rats'/'WTH', 'Razortooth Rats cannot be blocked except by artifact creatures and black creatures.').
card_first_print('razortooth rats', 'WTH').
card_image_name('razortooth rats'/'WTH', 'razortooth rats').
card_uid('razortooth rats'/'WTH', 'WTH:Razortooth Rats:razortooth rats').
card_rarity('razortooth rats'/'WTH', 'Common').
card_artist('razortooth rats'/'WTH', 'Brian Horton').
card_flavor_text('razortooth rats'/'WTH', '\"Men and rats both hunger: we for our playthings; they, for us.\"\n—Crovax').
card_multiverse_id('razortooth rats'/'WTH', '4466').

card_in_set('redwood treefolk', 'WTH').
card_original_type('redwood treefolk'/'WTH', 'Summon — Treefolk').
card_original_text('redwood treefolk'/'WTH', '').
card_image_name('redwood treefolk'/'WTH', 'redwood treefolk').
card_uid('redwood treefolk'/'WTH', 'WTH:Redwood Treefolk:redwood treefolk').
card_rarity('redwood treefolk'/'WTH', 'Common').
card_artist('redwood treefolk'/'WTH', 'Phil Foglio').
card_flavor_text('redwood treefolk'/'WTH', '\"In the heart of Llanowar the magic is so strong that trees cannot stay rooted.\"\n—Mirri of the Weatherlight').
card_multiverse_id('redwood treefolk'/'WTH', '4526').

card_in_set('relearn', 'WTH').
card_original_type('relearn'/'WTH', 'Sorcery').
card_original_text('relearn'/'WTH', 'Return target instant, interrupt, or sorcery card from your graveyard to your hand.').
card_first_print('relearn', 'WTH').
card_image_name('relearn'/'WTH', 'relearn').
card_uid('relearn'/'WTH', 'WTH:Relearn:relearn').
card_rarity('relearn'/'WTH', 'Uncommon').
card_artist('relearn'/'WTH', 'Zina Saunders').
card_flavor_text('relearn'/'WTH', '\"Barrin taught me that the hardest lessons to grasp are the ones you\'ve already learned.\"\n—Ertai, wizard adept').
card_multiverse_id('relearn'/'WTH', '4497').

card_in_set('revered unicorn', 'WTH').
card_original_type('revered unicorn'/'WTH', 'Summon — Unicorn').
card_original_text('revered unicorn'/'WTH', 'Cumulative upkeep {1}\nIf Revered Unicorn leaves play, its controller gains life equal to Revered Unicorn\'s last paid cumulative upkeep.').
card_first_print('revered unicorn', 'WTH').
card_image_name('revered unicorn'/'WTH', 'revered unicorn').
card_uid('revered unicorn'/'WTH', 'WTH:Revered Unicorn:revered unicorn').
card_rarity('revered unicorn'/'WTH', 'Uncommon').
card_artist('revered unicorn'/'WTH', 'David A. Cherry').
card_flavor_text('revered unicorn'/'WTH', '\"I felt unworthy even to dream of it.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('revered unicorn'/'WTH', '4585').

card_in_set('roc hatchling', 'WTH').
card_original_type('roc hatchling'/'WTH', 'Summon — Bird').
card_original_text('roc hatchling'/'WTH', 'When Roc Hatchling comes into play, put four shell counters on it.\nDuring your upkeep, remove a shell counter from Roc Hatchling.\nAs long as Roc Hatchling has no shell counters on it, it gets +3/+2 and gains flying.').
card_first_print('roc hatchling', 'WTH').
card_image_name('roc hatchling'/'WTH', 'roc hatchling').
card_uid('roc hatchling'/'WTH', 'WTH:Roc Hatchling:roc hatchling').
card_rarity('roc hatchling'/'WTH', 'Uncommon').
card_artist('roc hatchling'/'WTH', 'Una Fricker').
card_multiverse_id('roc hatchling'/'WTH', '4559').

card_in_set('rogue elephant', 'WTH').
card_original_type('rogue elephant'/'WTH', 'Summon — Elephant').
card_original_text('rogue elephant'/'WTH', 'When Rogue Elephant comes into play, sacrifice a forest or bury Rogue Elephant.').
card_first_print('rogue elephant', 'WTH').
card_image_name('rogue elephant'/'WTH', 'rogue elephant').
card_uid('rogue elephant'/'WTH', 'WTH:Rogue Elephant:rogue elephant').
card_rarity('rogue elephant'/'WTH', 'Common').
card_artist('rogue elephant'/'WTH', 'Steve White').
card_flavor_text('rogue elephant'/'WTH', '\"When are trees like grass?\"\n—The One Thousand Questions').
card_multiverse_id('rogue elephant'/'WTH', '4527').

card_in_set('sage owl', 'WTH').
card_original_type('sage owl'/'WTH', 'Summon — Bird').
card_original_text('sage owl'/'WTH', 'Flying\nWhen Sage Owl comes into play, look at the top four cards of your library and put them back in any order.').
card_first_print('sage owl', 'WTH').
card_image_name('sage owl'/'WTH', 'sage owl').
card_uid('sage owl'/'WTH', 'WTH:Sage Owl:sage owl').
card_rarity('sage owl'/'WTH', 'Common').
card_artist('sage owl'/'WTH', 'Mark Poole').
card_flavor_text('sage owl'/'WTH', 'The owl asks but never answers.').
card_multiverse_id('sage owl'/'WTH', '4498').

card_in_set('sawtooth ogre', 'WTH').
card_original_type('sawtooth ogre'/'WTH', 'Summon — Ogre').
card_original_text('sawtooth ogre'/'WTH', 'If Sawtooth Ogre blocks or is blocked by any creature, Sawtooth Ogre deals 1 damage to that creature at end of combat.').
card_first_print('sawtooth ogre', 'WTH').
card_image_name('sawtooth ogre'/'WTH', 'sawtooth ogre').
card_uid('sawtooth ogre'/'WTH', 'WTH:Sawtooth Ogre:sawtooth ogre').
card_rarity('sawtooth ogre'/'WTH', 'Common').
card_artist('sawtooth ogre'/'WTH', 'Brom').
card_flavor_text('sawtooth ogre'/'WTH', '\"You will sharpen your teeth on my enemies\' bones,\" Maraxus promised them, and his ogres cheered.').
card_multiverse_id('sawtooth ogre'/'WTH', '4560').

card_in_set('scorched ruins', 'WTH').
card_original_type('scorched ruins'/'WTH', 'Land').
card_original_text('scorched ruins'/'WTH', 'When Scorched Ruins comes into play, sacrifice two untapped lands or bury Scorched Ruins.\n{T}: Add four colorless mana to your mana pool.').
card_first_print('scorched ruins', 'WTH').
card_image_name('scorched ruins'/'WTH', 'scorched ruins').
card_uid('scorched ruins'/'WTH', 'WTH:Scorched Ruins:scorched ruins').
card_rarity('scorched ruins'/'WTH', 'Rare').
card_artist('scorched ruins'/'WTH', 'John Avon').
card_multiverse_id('scorched ruins'/'WTH', '4594').

card_in_set('serenity', 'WTH').
card_original_type('serenity'/'WTH', 'Enchantment').
card_original_text('serenity'/'WTH', 'During your upkeep, bury all artifacts and enchantments.').
card_first_print('serenity', 'WTH').
card_image_name('serenity'/'WTH', 'serenity').
card_uid('serenity'/'WTH', 'WTH:Serenity:serenity').
card_rarity('serenity'/'WTH', 'Rare').
card_artist('serenity'/'WTH', 'Cliff Nielsen').
card_flavor_text('serenity'/'WTH', '\"Just think of me as the storm before the calm.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('serenity'/'WTH', '4586').

card_in_set('serra\'s blessing', 'WTH').
card_original_type('serra\'s blessing'/'WTH', 'Enchantment').
card_original_text('serra\'s blessing'/'WTH', 'Attacking does not cause creatures you control to tap.').
card_first_print('serra\'s blessing', 'WTH').
card_image_name('serra\'s blessing'/'WTH', 'serra\'s blessing').
card_uid('serra\'s blessing'/'WTH', 'WTH:Serra\'s Blessing:serra\'s blessing').
card_rarity('serra\'s blessing'/'WTH', 'Uncommon').
card_artist('serra\'s blessing'/'WTH', 'Rebecca Guay').
card_flavor_text('serra\'s blessing'/'WTH', '\"I have seen your strength imbued in angels\' wings, and I have felt your sorrow rain down on the ruins brought by the Lord of the Wastes.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('serra\'s blessing'/'WTH', '4587').

card_in_set('serrated biskelion', 'WTH').
card_original_type('serrated biskelion'/'WTH', 'Artifact Creature').
card_original_text('serrated biskelion'/'WTH', '{T}: Put a -1/-1 counter on Serrated Biskelion and a -1/-1 counter on target creature.').
card_first_print('serrated biskelion', 'WTH').
card_image_name('serrated biskelion'/'WTH', 'serrated biskelion').
card_uid('serrated biskelion'/'WTH', 'WTH:Serrated Biskelion:serrated biskelion').
card_rarity('serrated biskelion'/'WTH', 'Uncommon').
card_artist('serrated biskelion'/'WTH', 'Ron Spencer').
card_flavor_text('serrated biskelion'/'WTH', '\"Whereas I was created to protect, the biskelion was created to destroy.\"\n—Karn, silver golem').
card_multiverse_id('serrated biskelion'/'WTH', '4439').

card_in_set('shadow rider', 'WTH').
card_original_type('shadow rider'/'WTH', 'Summon — Knight').
card_original_text('shadow rider'/'WTH', 'Flanking (Whenever a creature without flanking is assigned to block this creature, the blocking creature gets -1/-1 until end of turn.)').
card_first_print('shadow rider', 'WTH').
card_image_name('shadow rider'/'WTH', 'shadow rider').
card_uid('shadow rider'/'WTH', 'WTH:Shadow Rider:shadow rider').
card_rarity('shadow rider'/'WTH', 'Common').
card_artist('shadow rider'/'WTH', 'Pete Venters').
card_flavor_text('shadow rider'/'WTH', 'In its world of complete darkness, it has no shadows to fear.').
card_multiverse_id('shadow rider'/'WTH', '4467').

card_in_set('shattered crypt', 'WTH').
card_original_type('shattered crypt'/'WTH', 'Sorcery').
card_original_text('shattered crypt'/'WTH', 'Return X target creature cards from your graveyard to your hand and lose X life.').
card_first_print('shattered crypt', 'WTH').
card_image_name('shattered crypt'/'WTH', 'shattered crypt').
card_uid('shattered crypt'/'WTH', 'WTH:Shattered Crypt:shattered crypt').
card_rarity('shattered crypt'/'WTH', 'Common').
card_artist('shattered crypt'/'WTH', 'Gary Leach').
card_flavor_text('shattered crypt'/'WTH', '\"You must be mad to want one such as I aboard the Weatherlight. But I would be mad to remain here with my rotting family. I accept.\"\n—Crovax').
card_multiverse_id('shattered crypt'/'WTH', '4468').

card_in_set('soul shepherd', 'WTH').
card_original_type('soul shepherd'/'WTH', 'Summon — Cleric').
card_original_text('soul shepherd'/'WTH', '{W}, Remove a creature card in your graveyard from the game: Gain 1 life.').
card_first_print('soul shepherd', 'WTH').
card_image_name('soul shepherd'/'WTH', 'soul shepherd').
card_uid('soul shepherd'/'WTH', 'WTH:Soul Shepherd:soul shepherd').
card_rarity('soul shepherd'/'WTH', 'Common').
card_artist('soul shepherd'/'WTH', 'John Coulthart').
card_flavor_text('soul shepherd'/'WTH', '\"The sidar who raised me had a saying: ‘The first step into death is the hardest.\'\"\n—Gerrard of the Weatherlight').
card_multiverse_id('soul shepherd'/'WTH', '4588').

card_in_set('southern paladin', 'WTH').
card_original_type('southern paladin'/'WTH', 'Summon — Knight').
card_original_text('southern paladin'/'WTH', '{W}{W}, {T}: Destroy target red permanent.').
card_first_print('southern paladin', 'WTH').
card_image_name('southern paladin'/'WTH', 'southern paladin').
card_uid('southern paladin'/'WTH', 'WTH:Southern Paladin:southern paladin').
card_rarity('southern paladin'/'WTH', 'Rare').
card_artist('southern paladin'/'WTH', 'Douglas Shuler').
card_flavor_text('southern paladin'/'WTH', '\"Look to the south; there you will find peace and serenity.\"\n—The Book of Tal').
card_multiverse_id('southern paladin'/'WTH', '4589').

card_in_set('spinning darkness', 'WTH').
card_original_type('spinning darkness'/'WTH', 'Instant').
card_original_text('spinning darkness'/'WTH', 'You may remove the top three black cards in your graveyard from the game instead of paying Spinning Darkness\'s casting cost. Spinning Darkness deals 3 damage to target nonblack creature. Gain 3 life.').
card_first_print('spinning darkness', 'WTH').
card_image_name('spinning darkness'/'WTH', 'spinning darkness').
card_uid('spinning darkness'/'WTH', 'WTH:Spinning Darkness:spinning darkness').
card_rarity('spinning darkness'/'WTH', 'Common').
card_artist('spinning darkness'/'WTH', 'John Coulthart').
card_multiverse_id('spinning darkness'/'WTH', '4469').

card_in_set('steel golem', 'WTH').
card_original_type('steel golem'/'WTH', 'Artifact Creature').
card_original_text('steel golem'/'WTH', 'You cannot play summon or artifact creature spells.').
card_first_print('steel golem', 'WTH').
card_image_name('steel golem'/'WTH', 'steel golem').
card_uid('steel golem'/'WTH', 'WTH:Steel Golem:steel golem').
card_rarity('steel golem'/'WTH', 'Uncommon').
card_artist('steel golem'/'WTH', 'Donato Giancola').
card_flavor_text('steel golem'/'WTH', '\"Although I would give my life to protect Gerrard, my conscience will not let me take another\'s. There are many who would not hesitate.\"\n—Karn, silver golem').
card_multiverse_id('steel golem'/'WTH', '4440').

card_in_set('strands of night', 'WTH').
card_original_type('strands of night'/'WTH', 'Enchantment').
card_original_text('strands of night'/'WTH', '{B}{B}, Pay 2 life, Sacrifice a swamp: Put target creature card from your graveyard into play.').
card_first_print('strands of night', 'WTH').
card_image_name('strands of night'/'WTH', 'strands of night').
card_uid('strands of night'/'WTH', 'WTH:Strands of Night:strands of night').
card_rarity('strands of night'/'WTH', 'Uncommon').
card_artist('strands of night'/'WTH', 'Patrick Kochakji').
card_flavor_text('strands of night'/'WTH', '\"I have seen the night torn into thin darkling strips and woven into shapes too bleak for dreams.\"\n—Crovax').
card_multiverse_id('strands of night'/'WTH', '4470').

card_in_set('straw golem', 'WTH').
card_original_type('straw golem'/'WTH', 'Artifact Creature').
card_original_text('straw golem'/'WTH', 'If any opponent successfully casts a summon or artifact creature spell, bury Straw Golem.').
card_first_print('straw golem', 'WTH').
card_image_name('straw golem'/'WTH', 'straw golem').
card_uid('straw golem'/'WTH', 'WTH:Straw Golem:straw golem').
card_rarity('straw golem'/'WTH', 'Uncommon').
card_artist('straw golem'/'WTH', 'Bryan Talbot').
card_flavor_text('straw golem'/'WTH', '\"Don\'t look at me. I didn\'t build it.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('straw golem'/'WTH', '4441').

card_in_set('striped bears', 'WTH').
card_original_type('striped bears'/'WTH', 'Summon — Bears').
card_original_text('striped bears'/'WTH', 'When Striped Bears comes into play, draw a card.').
card_first_print('striped bears', 'WTH').
card_image_name('striped bears'/'WTH', 'striped bears').
card_uid('striped bears'/'WTH', 'WTH:Striped Bears:striped bears').
card_rarity('striped bears'/'WTH', 'Common').
card_artist('striped bears'/'WTH', 'Una Fricker').
card_flavor_text('striped bears'/'WTH', '\"The bear is never cautious—which is why it wasn\'t born a human.\"\n—Mirri of the Weatherlight').
card_multiverse_id('striped bears'/'WTH', '4528').

card_in_set('sylvan hierophant', 'WTH').
card_original_type('sylvan hierophant'/'WTH', 'Summon — Cleric').
card_original_text('sylvan hierophant'/'WTH', 'If Sylvan Hierophant is put into any graveyard from play, remove Sylvan Hierophant from the game, then return a creature card from your graveyard to your hand.').
card_first_print('sylvan hierophant', 'WTH').
card_image_name('sylvan hierophant'/'WTH', 'sylvan hierophant').
card_uid('sylvan hierophant'/'WTH', 'WTH:Sylvan Hierophant:sylvan hierophant').
card_rarity('sylvan hierophant'/'WTH', 'Uncommon').
card_artist('sylvan hierophant'/'WTH', 'Brian Durfee').
card_multiverse_id('sylvan hierophant'/'WTH', '4529').

card_in_set('tariff', 'WTH').
card_original_type('tariff'/'WTH', 'Sorcery').
card_original_text('tariff'/'WTH', 'Each player chooses a creature with the highest total casting cost he or she controls, then pays an amount of mana equal to that creature\'s total casting cost or buries the creature.').
card_first_print('tariff', 'WTH').
card_image_name('tariff'/'WTH', 'tariff').
card_uid('tariff'/'WTH', 'WTH:Tariff:tariff').
card_rarity('tariff'/'WTH', 'Rare').
card_artist('tariff'/'WTH', 'Kev Walker').
card_multiverse_id('tariff'/'WTH', '4590').

card_in_set('teferi\'s veil', 'WTH').
card_original_type('teferi\'s veil'/'WTH', 'Enchantment').
card_original_text('teferi\'s veil'/'WTH', 'Whenever any creature you control attacks, it phases out at end of combat.').
card_first_print('teferi\'s veil', 'WTH').
card_image_name('teferi\'s veil'/'WTH', 'teferi\'s veil').
card_uid('teferi\'s veil'/'WTH', 'WTH:Teferi\'s Veil:teferi\'s veil').
card_rarity('teferi\'s veil'/'WTH', 'Uncommon').
card_artist('teferi\'s veil'/'WTH', 'Brom').
card_flavor_text('teferi\'s veil'/'WTH', '\"I don\'t like to speak ill of \'walkers . . . but just what did Teferi think he was doing?\"\n—Ertai, wizard adept').
card_multiverse_id('teferi\'s veil'/'WTH', '4499').

card_in_set('tendrils of despair', 'WTH').
card_original_type('tendrils of despair'/'WTH', 'Sorcery').
card_original_text('tendrils of despair'/'WTH', 'Sacrifice a creature: Target opponent chooses and discards two cards.').
card_first_print('tendrils of despair', 'WTH').
card_image_name('tendrils of despair'/'WTH', 'tendrils of despair').
card_uid('tendrils of despair'/'WTH', 'WTH:Tendrils of Despair:tendrils of despair').
card_rarity('tendrils of despair'/'WTH', 'Common').
card_artist('tendrils of despair'/'WTH', 'John Coulthart').
card_flavor_text('tendrils of despair'/'WTH', '\"Because I am incapable of tears does not mean I have no need to shed them.\"\n—Karn, silver golem').
card_multiverse_id('tendrils of despair'/'WTH', '4471').

card_in_set('thran forge', 'WTH').
card_original_type('thran forge'/'WTH', 'Artifact').
card_original_text('thran forge'/'WTH', '{2}: Until end of turn, target nonartifact creature gets +1/+0 and is an artifact creature.').
card_first_print('thran forge', 'WTH').
card_image_name('thran forge'/'WTH', 'thran forge').
card_uid('thran forge'/'WTH', 'WTH:Thran Forge:thran forge').
card_rarity('thran forge'/'WTH', 'Uncommon').
card_artist('thran forge'/'WTH', 'Mark Poole').
card_flavor_text('thran forge'/'WTH', '\"This will work,\" Gerrard called to the elves as he used the forge to strengthen the aboroth, \"but if it doesn\'t, we won\'t survive to care.\"').
card_multiverse_id('thran forge'/'WTH', '4442').

card_in_set('thran tome', 'WTH').
card_original_type('thran tome'/'WTH', 'Artifact').
card_original_text('thran tome'/'WTH', '{5}, {T}: Reveal the top three cards of your library to target opponent. Bury one of those cards of that opponent\'s choice. Draw the remaining cards.').
card_first_print('thran tome', 'WTH').
card_image_name('thran tome'/'WTH', 'thran tome').
card_uid('thran tome'/'WTH', 'WTH:Thran Tome:thran tome').
card_rarity('thran tome'/'WTH', 'Rare').
card_artist('thran tome'/'WTH', 'Donato Giancola').
card_flavor_text('thran tome'/'WTH', 'Every line holds a tale, and between lie a thousand more.').
card_multiverse_id('thran tome'/'WTH', '4443').

card_in_set('thunderbolt', 'WTH').
card_original_type('thunderbolt'/'WTH', 'Instant').
card_original_text('thunderbolt'/'WTH', 'Thunderbolt deals 3 damage to target player or 4 damage to target creature with flying.').
card_first_print('thunderbolt', 'WTH').
card_image_name('thunderbolt'/'WTH', 'thunderbolt').
card_uid('thunderbolt'/'WTH', 'WTH:Thunderbolt:thunderbolt').
card_rarity('thunderbolt'/'WTH', 'Common').
card_artist('thunderbolt'/'WTH', 'Dylan Martens').
card_flavor_text('thunderbolt'/'WTH', '\"Most wizards consider a thunderbolt to be a proper retort.\"\n—Ertai, wizard adept').
card_multiverse_id('thunderbolt'/'WTH', '4561').

card_in_set('thundermare', 'WTH').
card_original_type('thundermare'/'WTH', 'Summon — Thundermare').
card_original_text('thundermare'/'WTH', 'Thundermare is unaffected by summoning sickness.\nWhen Thundermare comes into play, tap all other creatures.').
card_image_name('thundermare'/'WTH', 'thundermare').
card_uid('thundermare'/'WTH', 'WTH:Thundermare:thundermare').
card_rarity('thundermare'/'WTH', 'Rare').
card_artist('thundermare'/'WTH', 'Bob Eggleton').
card_flavor_text('thundermare'/'WTH', 'Its hooves strike lightning and the thunder follows quickly after.').
card_multiverse_id('thundermare'/'WTH', '4562').

card_in_set('timid drake', 'WTH').
card_original_type('timid drake'/'WTH', 'Summon — Drake').
card_original_text('timid drake'/'WTH', 'Flying\nIf any other creature comes into play, return Timid Drake to owner\'s hand.').
card_first_print('timid drake', 'WTH').
card_image_name('timid drake'/'WTH', 'timid drake').
card_uid('timid drake'/'WTH', 'WTH:Timid Drake:timid drake').
card_rarity('timid drake'/'WTH', 'Uncommon').
card_artist('timid drake'/'WTH', 'Mike Dringenberg').
card_flavor_text('timid drake'/'WTH', '\"I sneezed, and it bolted.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('timid drake'/'WTH', '4500').

card_in_set('tolarian drake', 'WTH').
card_original_type('tolarian drake'/'WTH', 'Summon — Drake').
card_original_text('tolarian drake'/'WTH', 'Flying, phasing').
card_first_print('tolarian drake', 'WTH').
card_image_name('tolarian drake'/'WTH', 'tolarian drake').
card_uid('tolarian drake'/'WTH', 'WTH:Tolarian Drake:tolarian drake').
card_rarity('tolarian drake'/'WTH', 'Common').
card_artist('tolarian drake'/'WTH', 'Mark Harrison').
card_flavor_text('tolarian drake'/'WTH', 'Many have been lost just in staring at its wings. What\'s seen differs from account to account, but madness is always part of the story.').
card_multiverse_id('tolarian drake'/'WTH', '4501').

card_in_set('tolarian entrancer', 'WTH').
card_original_type('tolarian entrancer'/'WTH', 'Summon — Wizard').
card_original_text('tolarian entrancer'/'WTH', 'Whenever Tolarian Entrancer is blocked by any creature, gain control of that creature at end of combat.').
card_first_print('tolarian entrancer', 'WTH').
card_image_name('tolarian entrancer'/'WTH', 'tolarian entrancer').
card_uid('tolarian entrancer'/'WTH', 'WTH:Tolarian Entrancer:tolarian entrancer').
card_rarity('tolarian entrancer'/'WTH', 'Rare').
card_artist('tolarian entrancer'/'WTH', 'Bryan Talbot').
card_flavor_text('tolarian entrancer'/'WTH', '\"Why should I boast? The bards will do it for me—and with music.\"\n—Ertai, wizard adept').
card_multiverse_id('tolarian entrancer'/'WTH', '4502').

card_in_set('tolarian serpent', 'WTH').
card_original_type('tolarian serpent'/'WTH', 'Summon — Serpent').
card_original_text('tolarian serpent'/'WTH', 'During your upkeep, put the top seven cards of your library into your graveyard.').
card_first_print('tolarian serpent', 'WTH').
card_image_name('tolarian serpent'/'WTH', 'tolarian serpent').
card_uid('tolarian serpent'/'WTH', 'WTH:Tolarian Serpent:tolarian serpent').
card_rarity('tolarian serpent'/'WTH', 'Rare').
card_artist('tolarian serpent'/'WTH', 'Stuart Griffin').
card_flavor_text('tolarian serpent'/'WTH', '\"When the fishtails\' threats of magic didn\'t move me, they set their pet on us.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('tolarian serpent'/'WTH', '4503').

card_in_set('touchstone', 'WTH').
card_original_type('touchstone'/'WTH', 'Artifact').
card_original_text('touchstone'/'WTH', '{T}: Tap target artifact you do not control.').
card_first_print('touchstone', 'WTH').
card_image_name('touchstone'/'WTH', 'touchstone').
card_uid('touchstone'/'WTH', 'WTH:Touchstone:touchstone').
card_rarity('touchstone'/'WTH', 'Uncommon').
card_artist('touchstone'/'WTH', 'George Pratt').
card_flavor_text('touchstone'/'WTH', '\"One touch to the stone and the sidar\'s son turned me from stalwart to statue.\"\n—Karn, silver golem').
card_multiverse_id('touchstone'/'WTH', '4444').

card_in_set('tranquil grove', 'WTH').
card_original_type('tranquil grove'/'WTH', 'Enchantment').
card_original_text('tranquil grove'/'WTH', '{1}{G}{G} Destroy all other enchantments.').
card_first_print('tranquil grove', 'WTH').
card_image_name('tranquil grove'/'WTH', 'tranquil grove').
card_uid('tranquil grove'/'WTH', 'WTH:Tranquil Grove:tranquil grove').
card_rarity('tranquil grove'/'WTH', 'Rare').
card_artist('tranquil grove'/'WTH', 'Dylan Martens').
card_flavor_text('tranquil grove'/'WTH', '\"Even the fiercest storm has a peaceful eye.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('tranquil grove'/'WTH', '4530').

card_in_set('uktabi efreet', 'WTH').
card_original_type('uktabi efreet'/'WTH', 'Summon — Efreet').
card_original_text('uktabi efreet'/'WTH', 'Cumulative upkeep {G}').
card_first_print('uktabi efreet', 'WTH').
card_image_name('uktabi efreet'/'WTH', 'uktabi efreet').
card_uid('uktabi efreet'/'WTH', 'WTH:Uktabi Efreet:uktabi efreet').
card_rarity('uktabi efreet'/'WTH', 'Common').
card_artist('uktabi efreet'/'WTH', 'Alan Rabinowitz').
card_flavor_text('uktabi efreet'/'WTH', '\"The Uktabi efreet and I have something in common: both of our mothers wanted to give us back.\"\n—Mirri of the Weatherlight').
card_multiverse_id('uktabi efreet'/'WTH', '4531').

card_in_set('urborg justice', 'WTH').
card_original_type('urborg justice'/'WTH', 'Instant').
card_original_text('urborg justice'/'WTH', 'Target opponent chooses and buries a number of creatures he or she controls equal to the number of creatures put into your graveyard from play so far this turn.').
card_first_print('urborg justice', 'WTH').
card_image_name('urborg justice'/'WTH', 'urborg justice').
card_uid('urborg justice'/'WTH', 'WTH:Urborg Justice:urborg justice').
card_rarity('urborg justice'/'WTH', 'Rare').
card_artist('urborg justice'/'WTH', 'Gary Leach').
card_flavor_text('urborg justice'/'WTH', '\"It is a narrow line between justice and vengeance.\"\n—Crovax').
card_multiverse_id('urborg justice'/'WTH', '4472').

card_in_set('urborg stalker', 'WTH').
card_original_type('urborg stalker'/'WTH', 'Summon — Undead').
card_original_text('urborg stalker'/'WTH', 'During each player\'s upkeep, if that player controls any nonblack permanents other than lands, Urborg Stalker deals 1 damage to that player.').
card_first_print('urborg stalker', 'WTH').
card_image_name('urborg stalker'/'WTH', 'urborg stalker').
card_uid('urborg stalker'/'WTH', 'WTH:Urborg Stalker:urborg stalker').
card_rarity('urborg stalker'/'WTH', 'Rare').
card_artist('urborg stalker'/'WTH', 'Cliff Nielsen').
card_flavor_text('urborg stalker'/'WTH', '\"May you be a stalker\'s dream.\"\n—Urborg curse').
card_multiverse_id('urborg stalker'/'WTH', '4473').

card_in_set('veteran explorer', 'WTH').
card_original_type('veteran explorer'/'WTH', 'Summon — Soldier').
card_original_text('veteran explorer'/'WTH', 'If Veteran Explorer is put into any graveyard from play, each player may search his or her library for up to two basic land cards and put those lands into play. Each player shuffles his or her library afterwards.').
card_first_print('veteran explorer', 'WTH').
card_image_name('veteran explorer'/'WTH', 'veteran explorer').
card_uid('veteran explorer'/'WTH', 'WTH:Veteran Explorer:veteran explorer').
card_rarity('veteran explorer'/'WTH', 'Uncommon').
card_artist('veteran explorer'/'WTH', 'David A. Cherry').
card_multiverse_id('veteran explorer'/'WTH', '4532').

card_in_set('vitalize', 'WTH').
card_original_type('vitalize'/'WTH', 'Instant').
card_original_text('vitalize'/'WTH', 'Untap all creatures you control.').
card_first_print('vitalize', 'WTH').
card_image_name('vitalize'/'WTH', 'vitalize').
card_uid('vitalize'/'WTH', 'WTH:Vitalize:vitalize').
card_rarity('vitalize'/'WTH', 'Common').
card_artist('vitalize'/'WTH', 'Pete Venters').
card_flavor_text('vitalize'/'WTH', '\"After waking to the magic of Llanowar, no dreams can compare.\"\n—Mirri of the Weatherlight').
card_multiverse_id('vitalize'/'WTH', '4533').

card_in_set('vodalian illusionist', 'WTH').
card_original_type('vodalian illusionist'/'WTH', 'Summon — Merfolk').
card_original_text('vodalian illusionist'/'WTH', '{U}{U}, {T}: Target creature phases out.').
card_first_print('vodalian illusionist', 'WTH').
card_image_name('vodalian illusionist'/'WTH', 'vodalian illusionist').
card_uid('vodalian illusionist'/'WTH', 'WTH:Vodalian Illusionist:vodalian illusionist').
card_rarity('vodalian illusionist'/'WTH', 'Uncommon').
card_artist('vodalian illusionist'/'WTH', 'John Matson').
card_flavor_text('vodalian illusionist'/'WTH', '\"Torahn gore these shifty fishtails! You can\'t even get close to one.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('vodalian illusionist'/'WTH', '4504').

card_in_set('volunteer reserves', 'WTH').
card_original_type('volunteer reserves'/'WTH', 'Summon — Soldiers').
card_original_text('volunteer reserves'/'WTH', 'Banding\nCumulative upkeep {1}').
card_first_print('volunteer reserves', 'WTH').
card_image_name('volunteer reserves'/'WTH', 'volunteer reserves').
card_uid('volunteer reserves'/'WTH', 'WTH:Volunteer Reserves:volunteer reserves').
card_rarity('volunteer reserves'/'WTH', 'Uncommon').
card_artist('volunteer reserves'/'WTH', 'Kev Walker').
card_flavor_text('volunteer reserves'/'WTH', '\"I\'m always a little leery of anyone who offers to kill other people for free.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('volunteer reserves'/'WTH', '4591').

card_in_set('wave of terror', 'WTH').
card_original_type('wave of terror'/'WTH', 'Enchantment').
card_original_text('wave of terror'/'WTH', 'Cumulative upkeep {1}\nAt the end of your upkeep, bury each creature with casting cost equal to Wave of Terror\'s last paid cumulative upkeep.').
card_first_print('wave of terror', 'WTH').
card_image_name('wave of terror'/'WTH', 'wave of terror').
card_uid('wave of terror'/'WTH', 'WTH:Wave of Terror:wave of terror').
card_rarity('wave of terror'/'WTH', 'Rare').
card_artist('wave of terror'/'WTH', 'Adrian Smith').
card_flavor_text('wave of terror'/'WTH', 'The sea of fear has endless depths.').
card_multiverse_id('wave of terror'/'WTH', '4474').

card_in_set('well of knowledge', 'WTH').
card_original_type('well of knowledge'/'WTH', 'Artifact').
card_original_text('well of knowledge'/'WTH', 'Any player may pay {2} during his or her draw phase to draw a card. Players may use this ability as many times as they choose.').
card_first_print('well of knowledge', 'WTH').
card_image_name('well of knowledge'/'WTH', 'well of knowledge').
card_uid('well of knowledge'/'WTH', 'WTH:Well of Knowledge:well of knowledge').
card_rarity('well of knowledge'/'WTH', 'Rare').
card_artist('well of knowledge'/'WTH', 'D. Alexander Gregory').
card_multiverse_id('well of knowledge'/'WTH', '4445').

card_in_set('winding canyons', 'WTH').
card_original_type('winding canyons'/'WTH', 'Land').
card_original_text('winding canyons'/'WTH', '{T}: Add one colorless mana to your mana pool.\n{2}, {T}: Until end of turn, you may play creature cards whenever you could play instants.').
card_first_print('winding canyons', 'WTH').
card_image_name('winding canyons'/'WTH', 'winding canyons').
card_uid('winding canyons'/'WTH', 'WTH:Winding Canyons:winding canyons').
card_rarity('winding canyons'/'WTH', 'Rare').
card_artist('winding canyons'/'WTH', 'John Avon').
card_multiverse_id('winding canyons'/'WTH', '4595').

card_in_set('xanthic statue', 'WTH').
card_original_type('xanthic statue'/'WTH', 'Artifact').
card_original_text('xanthic statue'/'WTH', '{5}: Until end of turn, Xanthic Statue is an 8/8 artifact creature with trample.').
card_first_print('xanthic statue', 'WTH').
card_image_name('xanthic statue'/'WTH', 'xanthic statue').
card_uid('xanthic statue'/'WTH', 'WTH:Xanthic Statue:xanthic statue').
card_rarity('xanthic statue'/'WTH', 'Rare').
card_artist('xanthic statue'/'WTH', 'Hannibal King').
card_flavor_text('xanthic statue'/'WTH', '\"Until you have lived as a statue, do not talk to me of pigeons.\"\n—Karn, silver golem').
card_multiverse_id('xanthic statue'/'WTH', '4446').

card_in_set('zombie scavengers', 'WTH').
card_original_type('zombie scavengers'/'WTH', 'Summon — Zombies').
card_original_text('zombie scavengers'/'WTH', 'Remove the top creature card in your graveyard from the game: Regenerate').
card_first_print('zombie scavengers', 'WTH').
card_image_name('zombie scavengers'/'WTH', 'zombie scavengers').
card_uid('zombie scavengers'/'WTH', 'WTH:Zombie Scavengers:zombie scavengers').
card_rarity('zombie scavengers'/'WTH', 'Common').
card_artist('zombie scavengers'/'WTH', 'Patrick Kochakji').
card_flavor_text('zombie scavengers'/'WTH', '\"Pick a shell upon my shore and put it to your ear. That sound isn\'t the sea, but the whispers of the fallen.\"\n—Crovax').
card_multiverse_id('zombie scavengers'/'WTH', '4475').
