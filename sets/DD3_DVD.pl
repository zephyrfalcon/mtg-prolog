% Duel Decks Anthology, Divine vs. Demonic

set('DD3_DVD').
set_name('DD3_DVD', 'Duel Decks Anthology, Divine vs. Demonic').
set_release_date('DD3_DVD', '2014-12-05').
set_border('DD3_DVD', 'black').
set_type('DD3_DVD', 'duel deck').

card_in_set('abyssal gatekeeper', 'DD3_DVD').
card_original_type('abyssal gatekeeper'/'DD3_DVD', 'Creature — Horror').
card_original_text('abyssal gatekeeper'/'DD3_DVD', 'When Abyssal Gatekeeper dies, each player sacrifices a creature.').
card_image_name('abyssal gatekeeper'/'DD3_DVD', 'abyssal gatekeeper').
card_uid('abyssal gatekeeper'/'DD3_DVD', 'DD3_DVD:Abyssal Gatekeeper:abyssal gatekeeper').
card_rarity('abyssal gatekeeper'/'DD3_DVD', 'Common').
card_artist('abyssal gatekeeper'/'DD3_DVD', 'Mark Tedin').
card_number('abyssal gatekeeper'/'DD3_DVD', '31').
card_flavor_text('abyssal gatekeeper'/'DD3_DVD', '\"There are two ways for me to pass this gate. One involves you remaining conscious.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('abyssal gatekeeper'/'DD3_DVD', '394003').

card_in_set('abyssal specter', 'DD3_DVD').
card_original_type('abyssal specter'/'DD3_DVD', 'Creature — Specter').
card_original_text('abyssal specter'/'DD3_DVD', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card.').
card_image_name('abyssal specter'/'DD3_DVD', 'abyssal specter').
card_uid('abyssal specter'/'DD3_DVD', 'DD3_DVD:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'DD3_DVD', 'Uncommon').
card_artist('abyssal specter'/'DD3_DVD', 'Michael Sutfin').
card_number('abyssal specter'/'DD3_DVD', '40').
card_flavor_text('abyssal specter'/'DD3_DVD', 'To gaze under its hood is to invite death.').
card_multiverse_id('abyssal specter'/'DD3_DVD', '394004').

card_in_set('akroma, angel of wrath', 'DD3_DVD').
card_original_type('akroma, angel of wrath'/'DD3_DVD', 'Legendary Creature — Angel').
card_original_text('akroma, angel of wrath'/'DD3_DVD', 'Flying, first strike, vigilance, trample, haste, protection from black and from red').
card_image_name('akroma, angel of wrath'/'DD3_DVD', 'akroma, angel of wrath').
card_uid('akroma, angel of wrath'/'DD3_DVD', 'DD3_DVD:Akroma, Angel of Wrath:akroma, angel of wrath').
card_rarity('akroma, angel of wrath'/'DD3_DVD', 'Mythic Rare').
card_artist('akroma, angel of wrath'/'DD3_DVD', 'Chippy').
card_number('akroma, angel of wrath'/'DD3_DVD', '1').
card_flavor_text('akroma, angel of wrath'/'DD3_DVD', '\"Wrath is no vice when inflicted upon the deserving.\"').
card_multiverse_id('akroma, angel of wrath'/'DD3_DVD', '394005').

card_in_set('angel of mercy', 'DD3_DVD').
card_original_type('angel of mercy'/'DD3_DVD', 'Creature — Angel').
card_original_text('angel of mercy'/'DD3_DVD', 'Flying\nWhen Angel of Mercy enters the battlefield, you gain 3 life.').
card_image_name('angel of mercy'/'DD3_DVD', 'angel of mercy').
card_uid('angel of mercy'/'DD3_DVD', 'DD3_DVD:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'DD3_DVD', 'Uncommon').
card_artist('angel of mercy'/'DD3_DVD', 'Volkan Baga').
card_number('angel of mercy'/'DD3_DVD', '9').
card_flavor_text('angel of mercy'/'DD3_DVD', 'Every tear shed is a drop of immortality.').
card_multiverse_id('angel of mercy'/'DD3_DVD', '394006').

card_in_set('angel\'s feather', 'DD3_DVD').
card_original_type('angel\'s feather'/'DD3_DVD', 'Artifact').
card_original_text('angel\'s feather'/'DD3_DVD', 'Whenever a player casts a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'DD3_DVD', 'angel\'s feather').
card_uid('angel\'s feather'/'DD3_DVD', 'DD3_DVD:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'DD3_DVD', 'Uncommon').
card_artist('angel\'s feather'/'DD3_DVD', 'Alan Pollack').
card_number('angel\'s feather'/'DD3_DVD', '23').
card_flavor_text('angel\'s feather'/'DD3_DVD', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'DD3_DVD', '394007').

card_in_set('angelic benediction', 'DD3_DVD').
card_original_type('angelic benediction'/'DD3_DVD', 'Enchantment').
card_original_text('angelic benediction'/'DD3_DVD', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may tap target creature.').
card_image_name('angelic benediction'/'DD3_DVD', 'angelic benediction').
card_uid('angelic benediction'/'DD3_DVD', 'DD3_DVD:Angelic Benediction:angelic benediction').
card_rarity('angelic benediction'/'DD3_DVD', 'Uncommon').
card_artist('angelic benediction'/'DD3_DVD', 'Michael Komarck').
card_number('angelic benediction'/'DD3_DVD', '19').
card_flavor_text('angelic benediction'/'DD3_DVD', '\"Even in single combat, I am never alone.\"\n—Rafiq of the Many').
card_multiverse_id('angelic benediction'/'DD3_DVD', '394008').

card_in_set('angelic page', 'DD3_DVD').
card_original_type('angelic page'/'DD3_DVD', 'Creature — Angel Spirit').
card_original_text('angelic page'/'DD3_DVD', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_image_name('angelic page'/'DD3_DVD', 'angelic page').
card_uid('angelic page'/'DD3_DVD', 'DD3_DVD:Angelic Page:angelic page').
card_rarity('angelic page'/'DD3_DVD', 'Common').
card_artist('angelic page'/'DD3_DVD', 'Marc Fishman').
card_number('angelic page'/'DD3_DVD', '3').
card_flavor_text('angelic page'/'DD3_DVD', 'If only every message were as perfect as its bearer.').
card_multiverse_id('angelic page'/'DD3_DVD', '394009').

card_in_set('angelic protector', 'DD3_DVD').
card_original_type('angelic protector'/'DD3_DVD', 'Creature — Angel').
card_original_text('angelic protector'/'DD3_DVD', 'Flying\nWhenever Angelic Protector becomes the target of a spell or ability, Angelic Protector gets +0/+3 until end of turn.').
card_image_name('angelic protector'/'DD3_DVD', 'angelic protector').
card_uid('angelic protector'/'DD3_DVD', 'DD3_DVD:Angelic Protector:angelic protector').
card_rarity('angelic protector'/'DD3_DVD', 'Uncommon').
card_artist('angelic protector'/'DD3_DVD', 'DiTerlizzi').
card_number('angelic protector'/'DD3_DVD', '6').
card_flavor_text('angelic protector'/'DD3_DVD', '\"My family sheltered in her light, the dark was content to wait.\"\n—Crovax').
card_multiverse_id('angelic protector'/'DD3_DVD', '394010').

card_in_set('angelsong', 'DD3_DVD').
card_original_type('angelsong'/'DD3_DVD', 'Instant').
card_original_text('angelsong'/'DD3_DVD', 'Prevent all combat damage that would be dealt this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('angelsong'/'DD3_DVD', 'angelsong').
card_uid('angelsong'/'DD3_DVD', 'DD3_DVD:Angelsong:angelsong').
card_rarity('angelsong'/'DD3_DVD', 'Common').
card_artist('angelsong'/'DD3_DVD', 'Sal Villagran').
card_number('angelsong'/'DD3_DVD', '15').
card_flavor_text('angelsong'/'DD3_DVD', 'Clash of sword and cry of beast fall mute when angels sound the call to prayer.').
card_multiverse_id('angelsong'/'DD3_DVD', '394011').

card_in_set('barren moor', 'DD3_DVD').
card_original_type('barren moor'/'DD3_DVD', 'Land').
card_original_text('barren moor'/'DD3_DVD', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'DD3_DVD', 'barren moor').
card_uid('barren moor'/'DD3_DVD', 'DD3_DVD:Barren Moor:barren moor').
card_rarity('barren moor'/'DD3_DVD', 'Common').
card_artist('barren moor'/'DD3_DVD', 'Heather Hudson').
card_number('barren moor'/'DD3_DVD', '58').
card_multiverse_id('barren moor'/'DD3_DVD', '394012').

card_in_set('barter in blood', 'DD3_DVD').
card_original_type('barter in blood'/'DD3_DVD', 'Sorcery').
card_original_text('barter in blood'/'DD3_DVD', 'Each player sacrifices two creatures.').
card_image_name('barter in blood'/'DD3_DVD', 'barter in blood').
card_uid('barter in blood'/'DD3_DVD', 'DD3_DVD:Barter in Blood:barter in blood').
card_rarity('barter in blood'/'DD3_DVD', 'Uncommon').
card_artist('barter in blood'/'DD3_DVD', 'Paolo Parente').
card_number('barter in blood'/'DD3_DVD', '52').
card_flavor_text('barter in blood'/'DD3_DVD', '\"In the game of conquest, who cares about the pawns if the king yet reigns?\"\n—Geth, keeper of the Vault').
card_multiverse_id('barter in blood'/'DD3_DVD', '394013').

card_in_set('breeding pit', 'DD3_DVD').
card_original_type('breeding pit'/'DD3_DVD', 'Enchantment').
card_original_text('breeding pit'/'DD3_DVD', 'At the beginning of your upkeep, sacrifice Breeding Pit unless you pay {B}{B}.\nAt the beginning of your end step, put a 0/1 black Thrull creature token onto the battlefield.').
card_image_name('breeding pit'/'DD3_DVD', 'breeding pit').
card_uid('breeding pit'/'DD3_DVD', 'DD3_DVD:Breeding Pit:breeding pit').
card_rarity('breeding pit'/'DD3_DVD', 'Uncommon').
card_artist('breeding pit'/'DD3_DVD', 'Adrian Smith').
card_number('breeding pit'/'DD3_DVD', '53').
card_flavor_text('breeding pit'/'DD3_DVD', 'The thrulls bred at a terrifying pace. In the end, they overwhelmed the Order of the Ebon Hand.').
card_multiverse_id('breeding pit'/'DD3_DVD', '394014').

card_in_set('cackling imp', 'DD3_DVD').
card_original_type('cackling imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('cackling imp'/'DD3_DVD', 'Flying\n{T}: Target player loses 1 life.').
card_image_name('cackling imp'/'DD3_DVD', 'cackling imp').
card_uid('cackling imp'/'DD3_DVD', 'DD3_DVD:Cackling Imp:cackling imp').
card_rarity('cackling imp'/'DD3_DVD', 'Common').
card_artist('cackling imp'/'DD3_DVD', 'Matt Thompson').
card_number('cackling imp'/'DD3_DVD', '41').
card_flavor_text('cackling imp'/'DD3_DVD', 'Its laughter drives needles through your mind.').
card_multiverse_id('cackling imp'/'DD3_DVD', '394015').

card_in_set('charging paladin', 'DD3_DVD').
card_original_type('charging paladin'/'DD3_DVD', 'Creature — Human Knight').
card_original_text('charging paladin'/'DD3_DVD', 'Whenever Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_image_name('charging paladin'/'DD3_DVD', 'charging paladin').
card_uid('charging paladin'/'DD3_DVD', 'DD3_DVD:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'DD3_DVD', 'Common').
card_artist('charging paladin'/'DD3_DVD', 'Ciruelo').
card_number('charging paladin'/'DD3_DVD', '4').
card_flavor_text('charging paladin'/'DD3_DVD', '\"Hope shall be the blade that severs our bonds.\"').
card_multiverse_id('charging paladin'/'DD3_DVD', '394016').

card_in_set('consume spirit', 'DD3_DVD').
card_original_type('consume spirit'/'DD3_DVD', 'Sorcery').
card_original_text('consume spirit'/'DD3_DVD', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'DD3_DVD', 'consume spirit').
card_uid('consume spirit'/'DD3_DVD', 'DD3_DVD:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'DD3_DVD', 'Uncommon').
card_artist('consume spirit'/'DD3_DVD', 'Matt Thompson').
card_number('consume spirit'/'DD3_DVD', '56').
card_flavor_text('consume spirit'/'DD3_DVD', '\"Your blood, your marrow, your spirit—all are mine.\"\n—Mayvar, minion of Geth').
card_multiverse_id('consume spirit'/'DD3_DVD', '394017').

card_in_set('corrupt', 'DD3_DVD').
card_original_type('corrupt'/'DD3_DVD', 'Sorcery').
card_original_text('corrupt'/'DD3_DVD', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'DD3_DVD', 'corrupt').
card_uid('corrupt'/'DD3_DVD', 'DD3_DVD:Corrupt:corrupt').
card_rarity('corrupt'/'DD3_DVD', 'Uncommon').
card_artist('corrupt'/'DD3_DVD', 'Dave Allsop').
card_number('corrupt'/'DD3_DVD', '55').
card_flavor_text('corrupt'/'DD3_DVD', 'There are things beneath the murk for whom the bog is a stew, a thin broth in need of meat.').
card_multiverse_id('corrupt'/'DD3_DVD', '394018').

card_in_set('cruel edict', 'DD3_DVD').
card_original_type('cruel edict'/'DD3_DVD', 'Sorcery').
card_original_text('cruel edict'/'DD3_DVD', 'Target opponent sacrifices a creature.').
card_image_name('cruel edict'/'DD3_DVD', 'cruel edict').
card_uid('cruel edict'/'DD3_DVD', 'DD3_DVD:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'DD3_DVD', 'Uncommon').
card_artist('cruel edict'/'DD3_DVD', 'Michael Sutfin').
card_number('cruel edict'/'DD3_DVD', '48').
card_flavor_text('cruel edict'/'DD3_DVD', '\"Choose your next words carefully. They will be your last.\"\n—Phage the Untouchable').
card_multiverse_id('cruel edict'/'DD3_DVD', '394019').

card_in_set('daggerclaw imp', 'DD3_DVD').
card_original_type('daggerclaw imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('daggerclaw imp'/'DD3_DVD', 'Flying\nDaggerclaw Imp can\'t block.').
card_image_name('daggerclaw imp'/'DD3_DVD', 'daggerclaw imp').
card_uid('daggerclaw imp'/'DD3_DVD', 'DD3_DVD:Daggerclaw Imp:daggerclaw imp').
card_rarity('daggerclaw imp'/'DD3_DVD', 'Uncommon').
card_artist('daggerclaw imp'/'DD3_DVD', 'Pete Venters').
card_number('daggerclaw imp'/'DD3_DVD', '33').
card_flavor_text('daggerclaw imp'/'DD3_DVD', 'The Simic use the claws as scalpels, while the Rakdos use them for tattooing and torture. The Gruul use them to pick their teeth after lunching on the rest of the carcass.').
card_multiverse_id('daggerclaw imp'/'DD3_DVD', '394020').

card_in_set('dark banishing', 'DD3_DVD').
card_original_type('dark banishing'/'DD3_DVD', 'Instant').
card_original_text('dark banishing'/'DD3_DVD', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'DD3_DVD', 'dark banishing').
card_uid('dark banishing'/'DD3_DVD', 'DD3_DVD:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'DD3_DVD', 'Common').
card_artist('dark banishing'/'DD3_DVD', 'Dermot Power').
card_number('dark banishing'/'DD3_DVD', '50').
card_flavor_text('dark banishing'/'DD3_DVD', '\"Ha, banishment? Be merciful, say ‘death,\'\nFor exile hath more terror in his look,\nMuch more than death.\"\n—William Shakespeare, Romeo and Juliet').
card_multiverse_id('dark banishing'/'DD3_DVD', '394021').

card_in_set('dark ritual', 'DD3_DVD').
card_original_type('dark ritual'/'DD3_DVD', 'Instant').
card_original_text('dark ritual'/'DD3_DVD', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'DD3_DVD', 'dark ritual').
card_uid('dark ritual'/'DD3_DVD', 'DD3_DVD:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'DD3_DVD', 'Common').
card_artist('dark ritual'/'DD3_DVD', 'Clint Langley').
card_number('dark ritual'/'DD3_DVD', '45').
card_flavor_text('dark ritual'/'DD3_DVD', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('dark ritual'/'DD3_DVD', '394022').

card_in_set('demon\'s horn', 'DD3_DVD').
card_original_type('demon\'s horn'/'DD3_DVD', 'Artifact').
card_original_text('demon\'s horn'/'DD3_DVD', 'Whenever a player casts a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'DD3_DVD', 'demon\'s horn').
card_uid('demon\'s horn'/'DD3_DVD', 'DD3_DVD:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'DD3_DVD', 'Uncommon').
card_artist('demon\'s horn'/'DD3_DVD', 'Alan Pollack').
card_number('demon\'s horn'/'DD3_DVD', '57').
card_flavor_text('demon\'s horn'/'DD3_DVD', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'DD3_DVD', '394023').

card_in_set('demon\'s jester', 'DD3_DVD').
card_original_type('demon\'s jester'/'DD3_DVD', 'Creature — Imp').
card_original_text('demon\'s jester'/'DD3_DVD', 'Flying\nHellbent — Demon\'s Jester gets +2/+1 as long as you have no cards in hand.').
card_image_name('demon\'s jester'/'DD3_DVD', 'demon\'s jester').
card_uid('demon\'s jester'/'DD3_DVD', 'DD3_DVD:Demon\'s Jester:demon\'s jester').
card_rarity('demon\'s jester'/'DD3_DVD', 'Common').
card_artist('demon\'s jester'/'DD3_DVD', 'Pete Venters').
card_number('demon\'s jester'/'DD3_DVD', '38').
card_flavor_text('demon\'s jester'/'DD3_DVD', 'They knock \'em dead, with or without the punch line.').
card_multiverse_id('demon\'s jester'/'DD3_DVD', '394024').

card_in_set('demonic tutor', 'DD3_DVD').
card_original_type('demonic tutor'/'DD3_DVD', 'Sorcery').
card_original_text('demonic tutor'/'DD3_DVD', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('demonic tutor'/'DD3_DVD', 'demonic tutor').
card_uid('demonic tutor'/'DD3_DVD', 'DD3_DVD:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'DD3_DVD', 'Uncommon').
card_artist('demonic tutor'/'DD3_DVD', 'Scott Chou').
card_number('demonic tutor'/'DD3_DVD', '49').
card_flavor_text('demonic tutor'/'DD3_DVD', 'Liliana learned the secrets she sought, but at a price that was etched on her fate.').
card_multiverse_id('demonic tutor'/'DD3_DVD', '394025').

card_in_set('duress', 'DD3_DVD').
card_original_type('duress'/'DD3_DVD', 'Sorcery').
card_original_text('duress'/'DD3_DVD', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'DD3_DVD', 'duress').
card_uid('duress'/'DD3_DVD', 'DD3_DVD:Duress:duress').
card_rarity('duress'/'DD3_DVD', 'Common').
card_artist('duress'/'DD3_DVD', 'Steven Belledin').
card_number('duress'/'DD3_DVD', '46').
card_flavor_text('duress'/'DD3_DVD', '\"Don\'t worry. I\'m not going to deprive you of all your secrets. Just your most precious one.\"\n—Liliana Vess').
card_multiverse_id('duress'/'DD3_DVD', '394026').

card_in_set('dusk imp', 'DD3_DVD').
card_original_type('dusk imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('dusk imp'/'DD3_DVD', 'Flying').
card_image_name('dusk imp'/'DD3_DVD', 'dusk imp').
card_uid('dusk imp'/'DD3_DVD', 'DD3_DVD:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'DD3_DVD', 'Common').
card_artist('dusk imp'/'DD3_DVD', 'Pete Venters').
card_number('dusk imp'/'DD3_DVD', '34').
card_flavor_text('dusk imp'/'DD3_DVD', 'Imps are just intelligent enough to have an understanding of cruelty.').
card_multiverse_id('dusk imp'/'DD3_DVD', '394027').

card_in_set('faith\'s fetters', 'DD3_DVD').
card_original_type('faith\'s fetters'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('faith\'s fetters'/'DD3_DVD', 'Enchant permanent\nWhen Faith\'s Fetters enters the battlefield, you gain 4 life.\nEnchanted permanent\'s activated abilities can\'t be activated unless they\'re mana abilities. If enchanted permanent is a creature, it can\'t attack or block.').
card_image_name('faith\'s fetters'/'DD3_DVD', 'faith\'s fetters').
card_uid('faith\'s fetters'/'DD3_DVD', 'DD3_DVD:Faith\'s Fetters:faith\'s fetters').
card_rarity('faith\'s fetters'/'DD3_DVD', 'Common').
card_artist('faith\'s fetters'/'DD3_DVD', 'Brian Despain').
card_number('faith\'s fetters'/'DD3_DVD', '20').
card_multiverse_id('faith\'s fetters'/'DD3_DVD', '394028').

card_in_set('fallen angel', 'DD3_DVD').
card_original_type('fallen angel'/'DD3_DVD', 'Creature — Angel').
card_original_text('fallen angel'/'DD3_DVD', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'DD3_DVD', 'fallen angel').
card_uid('fallen angel'/'DD3_DVD', 'DD3_DVD:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'DD3_DVD', 'Uncommon').
card_artist('fallen angel'/'DD3_DVD', 'Matthew D. Wilson').
card_number('fallen angel'/'DD3_DVD', '42').
card_flavor_text('fallen angel'/'DD3_DVD', '\"Why do you weep for the dead? I rejoice, for they have died for me.\"').
card_multiverse_id('fallen angel'/'DD3_DVD', '394029').

card_in_set('foul imp', 'DD3_DVD').
card_original_type('foul imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('foul imp'/'DD3_DVD', 'Flying\nWhen Foul Imp enters the battlefield, you lose 2 life.').
card_image_name('foul imp'/'DD3_DVD', 'foul imp').
card_uid('foul imp'/'DD3_DVD', 'DD3_DVD:Foul Imp:foul imp').
card_rarity('foul imp'/'DD3_DVD', 'Common').
card_artist('foul imp'/'DD3_DVD', 'Kev Walker').
card_number('foul imp'/'DD3_DVD', '32').
card_flavor_text('foul imp'/'DD3_DVD', 'The imp, unaware of its own odor, paused to catch its breath . . . and promptly died.').
card_multiverse_id('foul imp'/'DD3_DVD', '394030').

card_in_set('healing salve', 'DD3_DVD').
card_original_type('healing salve'/'DD3_DVD', 'Instant').
card_original_text('healing salve'/'DD3_DVD', 'Choose one —\n• Target player gains 3 life.\n• Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'DD3_DVD', 'healing salve').
card_uid('healing salve'/'DD3_DVD', 'DD3_DVD:Healing Salve:healing salve').
card_rarity('healing salve'/'DD3_DVD', 'Common').
card_artist('healing salve'/'DD3_DVD', 'Greg & Tim Hildebrandt').
card_number('healing salve'/'DD3_DVD', '14').
card_multiverse_id('healing salve'/'DD3_DVD', '394031').

card_in_set('icatian priest', 'DD3_DVD').
card_original_type('icatian priest'/'DD3_DVD', 'Creature — Human Cleric').
card_original_text('icatian priest'/'DD3_DVD', '{1}{W}{W}: Target creature gets +1/+1 until end of turn.').
card_image_name('icatian priest'/'DD3_DVD', 'icatian priest').
card_uid('icatian priest'/'DD3_DVD', 'DD3_DVD:Icatian Priest:icatian priest').
card_rarity('icatian priest'/'DD3_DVD', 'Uncommon').
card_artist('icatian priest'/'DD3_DVD', 'Stephen Tappin').
card_number('icatian priest'/'DD3_DVD', '2').
card_flavor_text('icatian priest'/'DD3_DVD', 'Grelden knelt and felt the cool, dry hand of the priest on his brow. Hours later, when his wits returned, he was covered in his enemies\' blood on the field of victory.').
card_multiverse_id('icatian priest'/'DD3_DVD', '394032').

card_in_set('kuro, pitlord', 'DD3_DVD').
card_original_type('kuro, pitlord'/'DD3_DVD', 'Legendary Creature — Demon Spirit').
card_original_text('kuro, pitlord'/'DD3_DVD', 'At the beginning of your upkeep, sacrifice Kuro, Pitlord unless you pay {B}{B}{B}{B}.\nPay 1 life: Target creature gets -1/-1 until end of turn.').
card_image_name('kuro, pitlord'/'DD3_DVD', 'kuro, pitlord').
card_uid('kuro, pitlord'/'DD3_DVD', 'DD3_DVD:Kuro, Pitlord:kuro, pitlord').
card_rarity('kuro, pitlord'/'DD3_DVD', 'Rare').
card_artist('kuro, pitlord'/'DD3_DVD', 'Jon Foster').
card_number('kuro, pitlord'/'DD3_DVD', '44').
card_multiverse_id('kuro, pitlord'/'DD3_DVD', '394033').

card_in_set('lord of the pit', 'DD3_DVD').
card_original_type('lord of the pit'/'DD3_DVD', 'Creature — Demon').
card_original_text('lord of the pit'/'DD3_DVD', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Lord of the Pit. If you can\'t, Lord of the Pit deals 7 damage to you.').
card_image_name('lord of the pit'/'DD3_DVD', 'lord of the pit').
card_uid('lord of the pit'/'DD3_DVD', 'DD3_DVD:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'DD3_DVD', 'Mythic Rare').
card_artist('lord of the pit'/'DD3_DVD', 'Chippy').
card_number('lord of the pit'/'DD3_DVD', '30').
card_flavor_text('lord of the pit'/'DD3_DVD', '\"My summoning begins your debt, Planeswalker.\"').
card_multiverse_id('lord of the pit'/'DD3_DVD', '394034').

card_in_set('luminous angel', 'DD3_DVD').
card_original_type('luminous angel'/'DD3_DVD', 'Creature — Angel').
card_original_text('luminous angel'/'DD3_DVD', 'Flying\nAt the beginning of your upkeep, you may put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('luminous angel'/'DD3_DVD', 'luminous angel').
card_uid('luminous angel'/'DD3_DVD', 'DD3_DVD:Luminous Angel:luminous angel').
card_rarity('luminous angel'/'DD3_DVD', 'Rare').
card_artist('luminous angel'/'DD3_DVD', 'Jason Chan').
card_number('luminous angel'/'DD3_DVD', '12').
card_flavor_text('luminous angel'/'DD3_DVD', '\"The spread of evil\'s destruction shall always be matched by the rise of the noble fallen.\"').
card_multiverse_id('luminous angel'/'DD3_DVD', '394035').

card_in_set('marble diamond', 'DD3_DVD').
card_original_type('marble diamond'/'DD3_DVD', 'Artifact').
card_original_text('marble diamond'/'DD3_DVD', 'Marble Diamond enters the battlefield tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('marble diamond'/'DD3_DVD', 'marble diamond').
card_uid('marble diamond'/'DD3_DVD', 'DD3_DVD:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'DD3_DVD', 'Uncommon').
card_artist('marble diamond'/'DD3_DVD', 'David Martin').
card_number('marble diamond'/'DD3_DVD', '24').
card_multiverse_id('marble diamond'/'DD3_DVD', '394036').

card_in_set('oni possession', 'DD3_DVD').
card_original_type('oni possession'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('oni possession'/'DD3_DVD', 'Enchant creature\nAt the beginning of your upkeep, sacrifice a creature.\nEnchanted creature gets +3/+3 and has trample.\nEnchanted creature is a Demon Spirit.').
card_image_name('oni possession'/'DD3_DVD', 'oni possession').
card_uid('oni possession'/'DD3_DVD', 'DD3_DVD:Oni Possession:oni possession').
card_rarity('oni possession'/'DD3_DVD', 'Uncommon').
card_artist('oni possession'/'DD3_DVD', 'Aleksi Briclot').
card_number('oni possession'/'DD3_DVD', '51').
card_multiverse_id('oni possession'/'DD3_DVD', '394037').

card_in_set('otherworldly journey', 'DD3_DVD').
card_original_type('otherworldly journey'/'DD3_DVD', 'Instant — Arcane').
card_original_text('otherworldly journey'/'DD3_DVD', 'Exile target creature. At the beginning of the next end step, return that card to the battlefield under its owner\'s control with a +1/+1 counter on it.').
card_image_name('otherworldly journey'/'DD3_DVD', 'otherworldly journey').
card_uid('otherworldly journey'/'DD3_DVD', 'DD3_DVD:Otherworldly Journey:otherworldly journey').
card_rarity('otherworldly journey'/'DD3_DVD', 'Uncommon').
card_artist('otherworldly journey'/'DD3_DVD', 'Vance Kovacs').
card_number('otherworldly journey'/'DD3_DVD', '16').
card_flavor_text('otherworldly journey'/'DD3_DVD', '\"The landscape shimmered and I felt a chill breeze. When my vision cleared, I found myself alone among the corpses of my fallen friends.\"\n—Journal found in Numai').
card_multiverse_id('otherworldly journey'/'DD3_DVD', '394038').

card_in_set('overeager apprentice', 'DD3_DVD').
card_original_type('overeager apprentice'/'DD3_DVD', 'Creature — Human Minion').
card_original_text('overeager apprentice'/'DD3_DVD', 'Discard a card, Sacrifice Overeager Apprentice: Add {B}{B}{B} to your mana pool.').
card_image_name('overeager apprentice'/'DD3_DVD', 'overeager apprentice').
card_uid('overeager apprentice'/'DD3_DVD', 'DD3_DVD:Overeager Apprentice:overeager apprentice').
card_rarity('overeager apprentice'/'DD3_DVD', 'Common').
card_artist('overeager apprentice'/'DD3_DVD', 'Ray Lago').
card_number('overeager apprentice'/'DD3_DVD', '35').
card_flavor_text('overeager apprentice'/'DD3_DVD', 'Slow and steady may not always win the race, but at least it doesn\'t end up splattered on the walls.').
card_multiverse_id('overeager apprentice'/'DD3_DVD', '394039').

card_in_set('pacifism', 'DD3_DVD').
card_original_type('pacifism'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('pacifism'/'DD3_DVD', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'DD3_DVD', 'pacifism').
card_uid('pacifism'/'DD3_DVD', 'DD3_DVD:Pacifism:pacifism').
card_rarity('pacifism'/'DD3_DVD', 'Common').
card_artist('pacifism'/'DD3_DVD', 'Matthew D. Wilson').
card_number('pacifism'/'DD3_DVD', '17').
card_flavor_text('pacifism'/'DD3_DVD', 'Even those born to battle could only lay their blades at Akroma\'s feet.').
card_multiverse_id('pacifism'/'DD3_DVD', '394040').

card_in_set('plains', 'DD3_DVD').
card_original_type('plains'/'DD3_DVD', 'Basic Land — Plains').
card_original_text('plains'/'DD3_DVD', 'W').
card_image_name('plains'/'DD3_DVD', 'plains1').
card_uid('plains'/'DD3_DVD', 'DD3_DVD:Plains:plains1').
card_rarity('plains'/'DD3_DVD', 'Basic Land').
card_artist('plains'/'DD3_DVD', 'Rob Alexander').
card_number('plains'/'DD3_DVD', '26').
card_multiverse_id('plains'/'DD3_DVD', '394044').

card_in_set('plains', 'DD3_DVD').
card_original_type('plains'/'DD3_DVD', 'Basic Land — Plains').
card_original_text('plains'/'DD3_DVD', 'W').
card_image_name('plains'/'DD3_DVD', 'plains2').
card_uid('plains'/'DD3_DVD', 'DD3_DVD:Plains:plains2').
card_rarity('plains'/'DD3_DVD', 'Basic Land').
card_artist('plains'/'DD3_DVD', 'Rob Alexander').
card_number('plains'/'DD3_DVD', '27').
card_multiverse_id('plains'/'DD3_DVD', '394043').

card_in_set('plains', 'DD3_DVD').
card_original_type('plains'/'DD3_DVD', 'Basic Land — Plains').
card_original_text('plains'/'DD3_DVD', 'W').
card_image_name('plains'/'DD3_DVD', 'plains3').
card_uid('plains'/'DD3_DVD', 'DD3_DVD:Plains:plains3').
card_rarity('plains'/'DD3_DVD', 'Basic Land').
card_artist('plains'/'DD3_DVD', 'Rob Alexander').
card_number('plains'/'DD3_DVD', '28').
card_multiverse_id('plains'/'DD3_DVD', '394041').

card_in_set('plains', 'DD3_DVD').
card_original_type('plains'/'DD3_DVD', 'Basic Land — Plains').
card_original_text('plains'/'DD3_DVD', 'W').
card_image_name('plains'/'DD3_DVD', 'plains4').
card_uid('plains'/'DD3_DVD', 'DD3_DVD:Plains:plains4').
card_rarity('plains'/'DD3_DVD', 'Basic Land').
card_artist('plains'/'DD3_DVD', 'Rob Alexander').
card_number('plains'/'DD3_DVD', '29').
card_multiverse_id('plains'/'DD3_DVD', '394042').

card_in_set('promise of power', 'DD3_DVD').
card_original_type('promise of power'/'DD3_DVD', 'Sorcery').
card_original_text('promise of power'/'DD3_DVD', 'Choose one — \n• You draw five cards and you lose 5 life.\n• Put an X/X black Demon creature token with flying onto the battlefield, where X is the number of cards in your hand as the token enters the battlefield.\nEntwine {4} (Choose both if you pay the entwine cost.)').
card_image_name('promise of power'/'DD3_DVD', 'promise of power').
card_uid('promise of power'/'DD3_DVD', 'DD3_DVD:Promise of Power:promise of power').
card_rarity('promise of power'/'DD3_DVD', 'Rare').
card_artist('promise of power'/'DD3_DVD', 'Kev Walker').
card_number('promise of power'/'DD3_DVD', '54').
card_multiverse_id('promise of power'/'DD3_DVD', '394045').

card_in_set('reiver demon', 'DD3_DVD').
card_original_type('reiver demon'/'DD3_DVD', 'Creature — Demon').
card_original_text('reiver demon'/'DD3_DVD', 'Flying\nWhen Reiver Demon enters the battlefield, if you cast it from your hand, destroy all nonartifact, nonblack creatures. They can\'t be regenerated.').
card_image_name('reiver demon'/'DD3_DVD', 'reiver demon').
card_uid('reiver demon'/'DD3_DVD', 'DD3_DVD:Reiver Demon:reiver demon').
card_rarity('reiver demon'/'DD3_DVD', 'Rare').
card_artist('reiver demon'/'DD3_DVD', 'Brom').
card_number('reiver demon'/'DD3_DVD', '43').
card_multiverse_id('reiver demon'/'DD3_DVD', '394046').

card_in_set('reya dawnbringer', 'DD3_DVD').
card_original_type('reya dawnbringer'/'DD3_DVD', 'Legendary Creature — Angel').
card_original_text('reya dawnbringer'/'DD3_DVD', 'Flying\nAt the beginning of your upkeep, you may return target creature card from your graveyard to the battlefield.').
card_image_name('reya dawnbringer'/'DD3_DVD', 'reya dawnbringer').
card_uid('reya dawnbringer'/'DD3_DVD', 'DD3_DVD:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'DD3_DVD', 'Rare').
card_artist('reya dawnbringer'/'DD3_DVD', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'DD3_DVD', '13').
card_flavor_text('reya dawnbringer'/'DD3_DVD', '\"You have not died until I consent.\"').
card_multiverse_id('reya dawnbringer'/'DD3_DVD', '394047').

card_in_set('righteous cause', 'DD3_DVD').
card_original_type('righteous cause'/'DD3_DVD', 'Enchantment').
card_original_text('righteous cause'/'DD3_DVD', 'Whenever a creature attacks, you gain 1 life.').
card_image_name('righteous cause'/'DD3_DVD', 'righteous cause').
card_uid('righteous cause'/'DD3_DVD', 'DD3_DVD:Righteous Cause:righteous cause').
card_rarity('righteous cause'/'DD3_DVD', 'Uncommon').
card_artist('righteous cause'/'DD3_DVD', 'Scott M. Fischer').
card_number('righteous cause'/'DD3_DVD', '22').
card_flavor_text('righteous cause'/'DD3_DVD', '\"Until the world unites in vengeful fury and Phage is destroyed, I will not stay my hand.\"\n—Akroma, angelic avenger').
card_multiverse_id('righteous cause'/'DD3_DVD', '394048').

card_in_set('secluded steppe', 'DD3_DVD').
card_original_type('secluded steppe'/'DD3_DVD', 'Land').
card_original_text('secluded steppe'/'DD3_DVD', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'DD3_DVD', 'secluded steppe').
card_uid('secluded steppe'/'DD3_DVD', 'DD3_DVD:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'DD3_DVD', 'Common').
card_artist('secluded steppe'/'DD3_DVD', 'Heather Hudson').
card_number('secluded steppe'/'DD3_DVD', '25').
card_multiverse_id('secluded steppe'/'DD3_DVD', '394049').

card_in_set('serra advocate', 'DD3_DVD').
card_original_type('serra advocate'/'DD3_DVD', 'Creature — Angel').
card_original_text('serra advocate'/'DD3_DVD', 'Flying\n{T}: Target attacking or blocking creature gets +2/+2 until end of turn.').
card_image_name('serra advocate'/'DD3_DVD', 'serra advocate').
card_uid('serra advocate'/'DD3_DVD', 'DD3_DVD:Serra Advocate:serra advocate').
card_rarity('serra advocate'/'DD3_DVD', 'Uncommon').
card_artist('serra advocate'/'DD3_DVD', 'Matthew D. Wilson').
card_number('serra advocate'/'DD3_DVD', '7').
card_flavor_text('serra advocate'/'DD3_DVD', 'An angel\'s blessing prepares a soldier for war better than a thousand military drills.').
card_multiverse_id('serra advocate'/'DD3_DVD', '394050').

card_in_set('serra angel', 'DD3_DVD').
card_original_type('serra angel'/'DD3_DVD', 'Creature — Angel').
card_original_text('serra angel'/'DD3_DVD', 'Flying, vigilance').
card_image_name('serra angel'/'DD3_DVD', 'serra angel').
card_uid('serra angel'/'DD3_DVD', 'DD3_DVD:Serra Angel:serra angel').
card_rarity('serra angel'/'DD3_DVD', 'Uncommon').
card_artist('serra angel'/'DD3_DVD', 'Greg Staples').
card_number('serra angel'/'DD3_DVD', '10').
card_flavor_text('serra angel'/'DD3_DVD', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel'/'DD3_DVD', '394051').

card_in_set('serra\'s boon', 'DD3_DVD').
card_original_type('serra\'s boon'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('serra\'s boon'/'DD3_DVD', 'Enchant creature\nEnchanted creature gets +1/+2 as long as it\'s white. Otherwise, it gets -2/-1.').
card_image_name('serra\'s boon'/'DD3_DVD', 'serra\'s boon').
card_uid('serra\'s boon'/'DD3_DVD', 'DD3_DVD:Serra\'s Boon:serra\'s boon').
card_rarity('serra\'s boon'/'DD3_DVD', 'Uncommon').
card_artist('serra\'s boon'/'DD3_DVD', 'Steven Belledin').
card_number('serra\'s boon'/'DD3_DVD', '18').
card_flavor_text('serra\'s boon'/'DD3_DVD', '\"The light of an angel\'s glance is warm, but her fixed stare blinds and burns.\"\n—Calexis, deacon of the New Order of Serra').
card_multiverse_id('serra\'s boon'/'DD3_DVD', '394052').

card_in_set('serra\'s embrace', 'DD3_DVD').
card_original_type('serra\'s embrace'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('serra\'s embrace'/'DD3_DVD', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying and vigilance.').
card_image_name('serra\'s embrace'/'DD3_DVD', 'serra\'s embrace').
card_uid('serra\'s embrace'/'DD3_DVD', 'DD3_DVD:Serra\'s Embrace:serra\'s embrace').
card_rarity('serra\'s embrace'/'DD3_DVD', 'Uncommon').
card_artist('serra\'s embrace'/'DD3_DVD', 'Zoltan Boros & Gabor Szikszai').
card_number('serra\'s embrace'/'DD3_DVD', '21').
card_flavor_text('serra\'s embrace'/'DD3_DVD', 'The touch of Serra\'s angels bears hopes aloft and empowers noble causes.').
card_multiverse_id('serra\'s embrace'/'DD3_DVD', '394053').

card_in_set('soot imp', 'DD3_DVD').
card_original_type('soot imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('soot imp'/'DD3_DVD', 'Flying\nWhenever a player casts a nonblack spell, that player loses 1 life.').
card_image_name('soot imp'/'DD3_DVD', 'soot imp').
card_uid('soot imp'/'DD3_DVD', 'DD3_DVD:Soot Imp:soot imp').
card_rarity('soot imp'/'DD3_DVD', 'Uncommon').
card_artist('soot imp'/'DD3_DVD', 'Jesper Ejsing').
card_number('soot imp'/'DD3_DVD', '37').
card_flavor_text('soot imp'/'DD3_DVD', '\"If one gets in your chimney, you\'re going to need a long wick and a barrel of bangstuff to get it out.\"\n—Hob Heddil').
card_multiverse_id('soot imp'/'DD3_DVD', '394054').

card_in_set('souldrinker', 'DD3_DVD').
card_original_type('souldrinker'/'DD3_DVD', 'Creature — Spirit').
card_original_text('souldrinker'/'DD3_DVD', 'Pay 3 life: Put a +1/+1 counter on Souldrinker.').
card_image_name('souldrinker'/'DD3_DVD', 'souldrinker').
card_uid('souldrinker'/'DD3_DVD', 'DD3_DVD:Souldrinker:souldrinker').
card_rarity('souldrinker'/'DD3_DVD', 'Uncommon').
card_artist('souldrinker'/'DD3_DVD', 'Dermot Power').
card_number('souldrinker'/'DD3_DVD', '39').
card_flavor_text('souldrinker'/'DD3_DVD', 'Don\'t drink and thrive.').
card_multiverse_id('souldrinker'/'DD3_DVD', '394055').

card_in_set('stinkweed imp', 'DD3_DVD').
card_original_type('stinkweed imp'/'DD3_DVD', 'Creature — Imp').
card_original_text('stinkweed imp'/'DD3_DVD', 'Flying\nWhenever Stinkweed Imp deals combat damage to a creature, destroy that creature.\nDredge 5 (If you would draw a card, instead you may put exactly five cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('stinkweed imp'/'DD3_DVD', 'stinkweed imp').
card_uid('stinkweed imp'/'DD3_DVD', 'DD3_DVD:Stinkweed Imp:stinkweed imp').
card_rarity('stinkweed imp'/'DD3_DVD', 'Common').
card_artist('stinkweed imp'/'DD3_DVD', 'Nils Hamm').
card_number('stinkweed imp'/'DD3_DVD', '36').
card_multiverse_id('stinkweed imp'/'DD3_DVD', '394056').

card_in_set('sustainer of the realm', 'DD3_DVD').
card_original_type('sustainer of the realm'/'DD3_DVD', 'Creature — Angel').
card_original_text('sustainer of the realm'/'DD3_DVD', 'Flying\nWhenever Sustainer of the Realm blocks, it gets +0/+2 until end of turn.').
card_image_name('sustainer of the realm'/'DD3_DVD', 'sustainer of the realm').
card_uid('sustainer of the realm'/'DD3_DVD', 'DD3_DVD:Sustainer of the Realm:sustainer of the realm').
card_rarity('sustainer of the realm'/'DD3_DVD', 'Uncommon').
card_artist('sustainer of the realm'/'DD3_DVD', 'Mark Zug').
card_number('sustainer of the realm'/'DD3_DVD', '8').
card_flavor_text('sustainer of the realm'/'DD3_DVD', 'You may break her shield, but you\'ll never break her spirit.').
card_multiverse_id('sustainer of the realm'/'DD3_DVD', '394057').

card_in_set('swamp', 'DD3_DVD').
card_original_type('swamp'/'DD3_DVD', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_DVD', 'B').
card_image_name('swamp'/'DD3_DVD', 'swamp1').
card_uid('swamp'/'DD3_DVD', 'DD3_DVD:Swamp:swamp1').
card_rarity('swamp'/'DD3_DVD', 'Basic Land').
card_artist('swamp'/'DD3_DVD', 'Mark Tedin').
card_number('swamp'/'DD3_DVD', '59').
card_multiverse_id('swamp'/'DD3_DVD', '394061').

card_in_set('swamp', 'DD3_DVD').
card_original_type('swamp'/'DD3_DVD', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_DVD', 'B').
card_image_name('swamp'/'DD3_DVD', 'swamp2').
card_uid('swamp'/'DD3_DVD', 'DD3_DVD:Swamp:swamp2').
card_rarity('swamp'/'DD3_DVD', 'Basic Land').
card_artist('swamp'/'DD3_DVD', 'Mark Tedin').
card_number('swamp'/'DD3_DVD', '60').
card_multiverse_id('swamp'/'DD3_DVD', '394059').

card_in_set('swamp', 'DD3_DVD').
card_original_type('swamp'/'DD3_DVD', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_DVD', 'B').
card_image_name('swamp'/'DD3_DVD', 'swamp3').
card_uid('swamp'/'DD3_DVD', 'DD3_DVD:Swamp:swamp3').
card_rarity('swamp'/'DD3_DVD', 'Basic Land').
card_artist('swamp'/'DD3_DVD', 'John Avon').
card_number('swamp'/'DD3_DVD', '61').
card_multiverse_id('swamp'/'DD3_DVD', '394058').

card_in_set('swamp', 'DD3_DVD').
card_original_type('swamp'/'DD3_DVD', 'Basic Land — Swamp').
card_original_text('swamp'/'DD3_DVD', 'B').
card_image_name('swamp'/'DD3_DVD', 'swamp4').
card_uid('swamp'/'DD3_DVD', 'DD3_DVD:Swamp:swamp4').
card_rarity('swamp'/'DD3_DVD', 'Basic Land').
card_artist('swamp'/'DD3_DVD', 'Vance Kovacs').
card_number('swamp'/'DD3_DVD', '62').
card_multiverse_id('swamp'/'DD3_DVD', '394060').

card_in_set('twilight shepherd', 'DD3_DVD').
card_original_type('twilight shepherd'/'DD3_DVD', 'Creature — Angel').
card_original_text('twilight shepherd'/'DD3_DVD', 'Flying, vigilance\nWhen Twilight Shepherd enters the battlefield, return to your hand all cards in your graveyard that were put there from the battlefield this turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('twilight shepherd'/'DD3_DVD', 'twilight shepherd').
card_uid('twilight shepherd'/'DD3_DVD', 'DD3_DVD:Twilight Shepherd:twilight shepherd').
card_rarity('twilight shepherd'/'DD3_DVD', 'Rare').
card_artist('twilight shepherd'/'DD3_DVD', 'Jason Chan').
card_number('twilight shepherd'/'DD3_DVD', '11').
card_multiverse_id('twilight shepherd'/'DD3_DVD', '394062').

card_in_set('unholy strength', 'DD3_DVD').
card_original_type('unholy strength'/'DD3_DVD', 'Enchantment — Aura').
card_original_text('unholy strength'/'DD3_DVD', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'DD3_DVD', 'unholy strength').
card_uid('unholy strength'/'DD3_DVD', 'DD3_DVD:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'DD3_DVD', 'Common').
card_artist('unholy strength'/'DD3_DVD', 'Terese Nielsen').
card_number('unholy strength'/'DD3_DVD', '47').
card_flavor_text('unholy strength'/'DD3_DVD', 'Such power grows the body as it shrinks the soul.').
card_multiverse_id('unholy strength'/'DD3_DVD', '394063').

card_in_set('venerable monk', 'DD3_DVD').
card_original_type('venerable monk'/'DD3_DVD', 'Creature — Human Monk Cleric').
card_original_text('venerable monk'/'DD3_DVD', 'When Venerable Monk enters the battlefield, you gain 2 life.').
card_image_name('venerable monk'/'DD3_DVD', 'venerable monk').
card_uid('venerable monk'/'DD3_DVD', 'DD3_DVD:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'DD3_DVD', 'Common').
card_artist('venerable monk'/'DD3_DVD', 'D. Alexander Gregory').
card_number('venerable monk'/'DD3_DVD', '5').
card_flavor_text('venerable monk'/'DD3_DVD', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'DD3_DVD', '394064').
