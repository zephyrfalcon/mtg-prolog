% Fate Reforged

set('FRF').
set_name('FRF', 'Fate Reforged').
set_release_date('FRF', '2015-01-23').
set_border('FRF', 'black').
set_type('FRF', 'expansion').
set_block('FRF', 'Khans of Tarkir').

card_in_set('abzan advantage', 'FRF').
card_original_type('abzan advantage'/'FRF', 'Instant').
card_original_text('abzan advantage'/'FRF', 'Target player sacrifices an enchantment. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('abzan advantage', 'FRF').
card_image_name('abzan advantage'/'FRF', 'abzan advantage').
card_uid('abzan advantage'/'FRF', 'FRF:Abzan Advantage:abzan advantage').
card_rarity('abzan advantage'/'FRF', 'Common').
card_artist('abzan advantage'/'FRF', 'Winona Nelson').
card_number('abzan advantage'/'FRF', '2').
card_flavor_text('abzan advantage'/'FRF', '\"To survive, you must seize every opportunity.\"\n—Daghatar the Adamant').
card_multiverse_id('abzan advantage'/'FRF', '391781').
card_watermark('abzan advantage'/'FRF', 'Abzan').

card_in_set('abzan beastmaster', 'FRF').
card_original_type('abzan beastmaster'/'FRF', 'Creature — Hound Shaman').
card_original_text('abzan beastmaster'/'FRF', 'At the beginning of your upkeep, draw a card if you control the creature with the greatest toughness or tied for the greatest toughness.').
card_image_name('abzan beastmaster'/'FRF', 'abzan beastmaster').
card_uid('abzan beastmaster'/'FRF', 'FRF:Abzan Beastmaster:abzan beastmaster').
card_rarity('abzan beastmaster'/'FRF', 'Uncommon').
card_artist('abzan beastmaster'/'FRF', 'Winona Nelson').
card_number('abzan beastmaster'/'FRF', '119').
card_flavor_text('abzan beastmaster'/'FRF', 'His beasts move the great siege towers of the Abzan across the endless sands.').
card_multiverse_id('abzan beastmaster'/'FRF', '391782').
card_watermark('abzan beastmaster'/'FRF', 'Abzan').

card_in_set('abzan kin-guard', 'FRF').
card_original_type('abzan kin-guard'/'FRF', 'Creature — Human Warrior').
card_original_text('abzan kin-guard'/'FRF', 'Abzan Kin-Guard has lifelink as long as you control a white or black permanent.').
card_first_print('abzan kin-guard', 'FRF').
card_image_name('abzan kin-guard'/'FRF', 'abzan kin-guard').
card_uid('abzan kin-guard'/'FRF', 'FRF:Abzan Kin-Guard:abzan kin-guard').
card_rarity('abzan kin-guard'/'FRF', 'Uncommon').
card_artist('abzan kin-guard'/'FRF', 'Craig J Spearing').
card_number('abzan kin-guard'/'FRF', '120').
card_flavor_text('abzan kin-guard'/'FRF', '\"The Mardu rush headlong toward extinction. We ensure the longevity of our clan by protecting our territory, not rampaging through it.\"').
card_multiverse_id('abzan kin-guard'/'FRF', '391783').
card_watermark('abzan kin-guard'/'FRF', 'Abzan').

card_in_set('abzan runemark', 'FRF').
card_original_type('abzan runemark'/'FRF', 'Enchantment — Aura').
card_original_text('abzan runemark'/'FRF', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has vigilance as long as you control a black or green permanent.').
card_first_print('abzan runemark', 'FRF').
card_image_name('abzan runemark'/'FRF', 'abzan runemark').
card_uid('abzan runemark'/'FRF', 'FRF:Abzan Runemark:abzan runemark').
card_rarity('abzan runemark'/'FRF', 'Common').
card_artist('abzan runemark'/'FRF', 'James Ryman').
card_number('abzan runemark'/'FRF', '3').
card_multiverse_id('abzan runemark'/'FRF', '391784').
card_watermark('abzan runemark'/'FRF', 'Abzan').

card_in_set('abzan skycaptain', 'FRF').
card_original_type('abzan skycaptain'/'FRF', 'Creature — Bird Soldier').
card_original_text('abzan skycaptain'/'FRF', 'Flying\nWhen Abzan Skycaptain dies, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_first_print('abzan skycaptain', 'FRF').
card_image_name('abzan skycaptain'/'FRF', 'abzan skycaptain').
card_uid('abzan skycaptain'/'FRF', 'FRF:Abzan Skycaptain:abzan skycaptain').
card_rarity('abzan skycaptain'/'FRF', 'Common').
card_artist('abzan skycaptain'/'FRF', 'Matt Stewart').
card_number('abzan skycaptain'/'FRF', '4').
card_flavor_text('abzan skycaptain'/'FRF', '\"A tempest is coming. Turn your faces toward the wind!\"').
card_multiverse_id('abzan skycaptain'/'FRF', '391785').
card_watermark('abzan skycaptain'/'FRF', 'Abzan').

card_in_set('ainok guide', 'FRF').
card_original_type('ainok guide'/'FRF', 'Creature — Hound Scout').
card_original_text('ainok guide'/'FRF', 'When Ainok Guide enters the battlefield, choose one —\n• Put a +1/+1 counter on Ainok Guide.\n• Search your library for a basic land card, reveal it, then shuffle your library and put that card on top of it.').
card_first_print('ainok guide', 'FRF').
card_image_name('ainok guide'/'FRF', 'ainok guide').
card_uid('ainok guide'/'FRF', 'FRF:Ainok Guide:ainok guide').
card_rarity('ainok guide'/'FRF', 'Common').
card_artist('ainok guide'/'FRF', 'Lucas Graciano').
card_number('ainok guide'/'FRF', '121').
card_multiverse_id('ainok guide'/'FRF', '391786').

card_in_set('alesha\'s vanguard', 'FRF').
card_original_type('alesha\'s vanguard'/'FRF', 'Creature — Orc Warrior').
card_original_text('alesha\'s vanguard'/'FRF', 'Dash {2}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('alesha\'s vanguard', 'FRF').
card_image_name('alesha\'s vanguard'/'FRF', 'alesha\'s vanguard').
card_uid('alesha\'s vanguard'/'FRF', 'FRF:Alesha\'s Vanguard:alesha\'s vanguard').
card_rarity('alesha\'s vanguard'/'FRF', 'Common').
card_artist('alesha\'s vanguard'/'FRF', 'Torstein Nordstrand').
card_number('alesha\'s vanguard'/'FRF', '60').
card_flavor_text('alesha\'s vanguard'/'FRF', 'Boundaries drawn on maps or marked with stone have no meaning for the Mardu.').
card_multiverse_id('alesha\'s vanguard'/'FRF', '391788').
card_watermark('alesha\'s vanguard'/'FRF', 'Mardu').

card_in_set('alesha, who smiles at death', 'FRF').
card_original_type('alesha, who smiles at death'/'FRF', 'Legendary Creature — Human Warrior').
card_original_text('alesha, who smiles at death'/'FRF', 'First strike\nWhenever Alesha, Who Smiles at Death attacks, you may pay {W/B}{W/B}. If you do, return target creature card with power 2 or less from your graveyard to the battlefield tapped and attacking.').
card_first_print('alesha, who smiles at death', 'FRF').
card_image_name('alesha, who smiles at death'/'FRF', 'alesha, who smiles at death').
card_uid('alesha, who smiles at death'/'FRF', 'FRF:Alesha, Who Smiles at Death:alesha, who smiles at death').
card_rarity('alesha, who smiles at death'/'FRF', 'Rare').
card_artist('alesha, who smiles at death'/'FRF', 'Anastasia Ovchinnikova').
card_number('alesha, who smiles at death'/'FRF', '90').
card_flavor_text('alesha, who smiles at death'/'FRF', '\"Greet death with sword in hand.\"').
card_multiverse_id('alesha, who smiles at death'/'FRF', '391787').
card_watermark('alesha, who smiles at death'/'FRF', 'Mardu').

card_in_set('ambush krotiq', 'FRF').
card_original_type('ambush krotiq'/'FRF', 'Creature — Insect').
card_original_text('ambush krotiq'/'FRF', 'Trample\nWhen Ambush Krotiq enters the battlefield, return another creature you control to its owner\'s hand.').
card_first_print('ambush krotiq', 'FRF').
card_image_name('ambush krotiq'/'FRF', 'ambush krotiq').
card_uid('ambush krotiq'/'FRF', 'FRF:Ambush Krotiq:ambush krotiq').
card_rarity('ambush krotiq'/'FRF', 'Common').
card_artist('ambush krotiq'/'FRF', 'Filip Burburan').
card_number('ambush krotiq'/'FRF', '122').
card_flavor_text('ambush krotiq'/'FRF', 'The Abzan merchants soon discovered why this route was clear of dragons.').
card_multiverse_id('ambush krotiq'/'FRF', '391789').
card_watermark('ambush krotiq'/'FRF', 'Abzan').

card_in_set('ancestral vengeance', 'FRF').
card_original_type('ancestral vengeance'/'FRF', 'Enchantment — Aura').
card_original_text('ancestral vengeance'/'FRF', 'Enchant creature\nWhen Ancestral Vengeance enters the battlefield, put a +1/+1 counter on target creature you control.\nEnchanted creature gets -1/-1.').
card_first_print('ancestral vengeance', 'FRF').
card_image_name('ancestral vengeance'/'FRF', 'ancestral vengeance').
card_uid('ancestral vengeance'/'FRF', 'FRF:Ancestral Vengeance:ancestral vengeance').
card_rarity('ancestral vengeance'/'FRF', 'Common').
card_artist('ancestral vengeance'/'FRF', 'Yohann Schepacz').
card_number('ancestral vengeance'/'FRF', '61').
card_flavor_text('ancestral vengeance'/'FRF', 'The defenses of the Abzan extend well beyond the walls of their fortresses.').
card_multiverse_id('ancestral vengeance'/'FRF', '391790').

card_in_set('arashin cleric', 'FRF').
card_original_type('arashin cleric'/'FRF', 'Creature — Human Cleric').
card_original_text('arashin cleric'/'FRF', 'When Arashin Cleric enters the battlefield, you gain 3 life.').
card_first_print('arashin cleric', 'FRF').
card_image_name('arashin cleric'/'FRF', 'arashin cleric').
card_uid('arashin cleric'/'FRF', 'FRF:Arashin Cleric:arashin cleric').
card_rarity('arashin cleric'/'FRF', 'Common').
card_artist('arashin cleric'/'FRF', 'Chris Rahn').
card_number('arashin cleric'/'FRF', '5').
card_flavor_text('arashin cleric'/'FRF', '\"We\'ll have time to put out the fires when those trapped within the ruins are safe.\"').
card_multiverse_id('arashin cleric'/'FRF', '391791').
card_watermark('arashin cleric'/'FRF', 'Abzan').

card_in_set('arashin war beast', 'FRF').
card_original_type('arashin war beast'/'FRF', 'Creature — Beast').
card_original_text('arashin war beast'/'FRF', 'Whenever Arashin War Beast deals combat damage to one or more blocking creatures, manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('arashin war beast'/'FRF', 'arashin war beast').
card_uid('arashin war beast'/'FRF', 'FRF:Arashin War Beast:arashin war beast').
card_rarity('arashin war beast'/'FRF', 'Uncommon').
card_artist('arashin war beast'/'FRF', 'Eric Deschamps').
card_number('arashin war beast'/'FRF', '123').
card_multiverse_id('arashin war beast'/'FRF', '391792').
card_watermark('arashin war beast'/'FRF', 'Abzan').

card_in_set('arcbond', 'FRF').
card_original_type('arcbond'/'FRF', 'Instant').
card_original_text('arcbond'/'FRF', 'Choose target creature. Whenever that creature is dealt damage this turn, it deals that much damage to each other creature and each player.').
card_first_print('arcbond', 'FRF').
card_image_name('arcbond'/'FRF', 'arcbond').
card_uid('arcbond'/'FRF', 'FRF:Arcbond:arcbond').
card_rarity('arcbond'/'FRF', 'Rare').
card_artist('arcbond'/'FRF', 'Slawomir Maniak').
card_number('arcbond'/'FRF', '91').
card_flavor_text('arcbond'/'FRF', '\"If you must die today, make your death worthy of legend.\"\n—Alesha, Who Smiles at Death').
card_multiverse_id('arcbond'/'FRF', '391793').

card_in_set('archers of qarsi', 'FRF').
card_original_type('archers of qarsi'/'FRF', 'Creature — Naga Archer').
card_original_text('archers of qarsi'/'FRF', 'Defender\nReach (This creature can block creatures with flying.)').
card_first_print('archers of qarsi', 'FRF').
card_image_name('archers of qarsi'/'FRF', 'archers of qarsi').
card_uid('archers of qarsi'/'FRF', 'FRF:Archers of Qarsi:archers of qarsi').
card_rarity('archers of qarsi'/'FRF', 'Common').
card_artist('archers of qarsi'/'FRF', 'Alejandro Mirabal').
card_number('archers of qarsi'/'FRF', '124').
card_flavor_text('archers of qarsi'/'FRF', '\"We think of the naga as creeping cutthroats, slitherers in the swamps. But they are equally at home in the trees.\"\n—Goton, Qal Sisma shaman').
card_multiverse_id('archers of qarsi'/'FRF', '391794').
card_watermark('archers of qarsi'/'FRF', 'Sultai').

card_in_set('archfiend of depravity', 'FRF').
card_original_type('archfiend of depravity'/'FRF', 'Creature — Demon').
card_original_text('archfiend of depravity'/'FRF', 'Flying\nAt the beginning of each opponent\'s end step, that player chooses up to two creatures he or she controls, then sacrifices the rest.').
card_image_name('archfiend of depravity'/'FRF', 'archfiend of depravity').
card_uid('archfiend of depravity'/'FRF', 'FRF:Archfiend of Depravity:archfiend of depravity').
card_rarity('archfiend of depravity'/'FRF', 'Rare').
card_artist('archfiend of depravity'/'FRF', 'Daarken').
card_number('archfiend of depravity'/'FRF', '62').
card_flavor_text('archfiend of depravity'/'FRF', '\"Why would I kill you all? Who then would be left to worship me?\"').
card_multiverse_id('archfiend of depravity'/'FRF', '391795').
card_watermark('archfiend of depravity'/'FRF', 'Sultai').

card_in_set('atarka, world render', 'FRF').
card_original_type('atarka, world render'/'FRF', 'Legendary Creature — Dragon').
card_original_text('atarka, world render'/'FRF', 'Flying, trample\nWhenever a Dragon you control attacks, it gains double strike until end of turn.').
card_first_print('atarka, world render', 'FRF').
card_image_name('atarka, world render'/'FRF', 'atarka, world render').
card_uid('atarka, world render'/'FRF', 'FRF:Atarka, World Render:atarka, world render').
card_rarity('atarka, world render'/'FRF', 'Rare').
card_artist('atarka, world render'/'FRF', 'Karl Kopinski').
card_number('atarka, world render'/'FRF', '149').
card_flavor_text('atarka, world render'/'FRF', '\"Her hunger knows no limit. Left unchecked, she would feast on all of Tarkir.\"\n—Yasova Dragonclaw').
card_multiverse_id('atarka, world render'/'FRF', '391796').
card_watermark('atarka, world render'/'FRF', 'Atarka').

card_in_set('aven skirmisher', 'FRF').
card_original_type('aven skirmisher'/'FRF', 'Creature — Bird Warrior').
card_original_text('aven skirmisher'/'FRF', 'Flying').
card_first_print('aven skirmisher', 'FRF').
card_image_name('aven skirmisher'/'FRF', 'aven skirmisher').
card_uid('aven skirmisher'/'FRF', 'FRF:Aven Skirmisher:aven skirmisher').
card_rarity('aven skirmisher'/'FRF', 'Common').
card_artist('aven skirmisher'/'FRF', 'Jason Rainville').
card_number('aven skirmisher'/'FRF', '6').
card_flavor_text('aven skirmisher'/'FRF', '\"We do not hide from the dragons that pretend to rule the skies. If we did, the dragons would become our rulers, and our way of life would be lost.\"').
card_multiverse_id('aven skirmisher'/'FRF', '391797').
card_watermark('aven skirmisher'/'FRF', 'Jeskai').

card_in_set('aven surveyor', 'FRF').
card_original_type('aven surveyor'/'FRF', 'Creature — Bird Scout').
card_original_text('aven surveyor'/'FRF', 'Flying\nWhen Aven Surveyor enters the battlefield, choose one —\n• Put a +1/+1 counter on Aven Surveyor.\n• Return target creature to its owner\'s hand.').
card_first_print('aven surveyor', 'FRF').
card_image_name('aven surveyor'/'FRF', 'aven surveyor').
card_uid('aven surveyor'/'FRF', 'FRF:Aven Surveyor:aven surveyor').
card_rarity('aven surveyor'/'FRF', 'Common').
card_artist('aven surveyor'/'FRF', 'David Gaillet').
card_number('aven surveyor'/'FRF', '31').
card_multiverse_id('aven surveyor'/'FRF', '391798').

card_in_set('bathe in dragonfire', 'FRF').
card_original_type('bathe in dragonfire'/'FRF', 'Sorcery').
card_original_text('bathe in dragonfire'/'FRF', 'Bathe in Dragonfire deals 4 damage to target creature.').
card_first_print('bathe in dragonfire', 'FRF').
card_image_name('bathe in dragonfire'/'FRF', 'bathe in dragonfire').
card_uid('bathe in dragonfire'/'FRF', 'FRF:Bathe in Dragonfire:bathe in dragonfire').
card_rarity('bathe in dragonfire'/'FRF', 'Common').
card_artist('bathe in dragonfire'/'FRF', 'Chris Rallis').
card_number('bathe in dragonfire'/'FRF', '92').
card_flavor_text('bathe in dragonfire'/'FRF', 'The scent of cooked flesh lingers in the charred landscape of Tarkir.').
card_multiverse_id('bathe in dragonfire'/'FRF', '391799').

card_in_set('battle brawler', 'FRF').
card_original_type('battle brawler'/'FRF', 'Creature — Orc Warrior').
card_original_text('battle brawler'/'FRF', 'As long as you control a red or white permanent, Battle Brawler gets +1/+0 and has first strike.').
card_first_print('battle brawler', 'FRF').
card_image_name('battle brawler'/'FRF', 'battle brawler').
card_uid('battle brawler'/'FRF', 'FRF:Battle Brawler:battle brawler').
card_rarity('battle brawler'/'FRF', 'Uncommon').
card_artist('battle brawler'/'FRF', 'Karl Kopinski').
card_number('battle brawler'/'FRF', '63').
card_flavor_text('battle brawler'/'FRF', 'Every time he returns from battle unscathed, he feels a tinge of disappointment.').
card_multiverse_id('battle brawler'/'FRF', '391800').
card_watermark('battle brawler'/'FRF', 'Mardu').

card_in_set('battlefront krushok', 'FRF').
card_original_type('battlefront krushok'/'FRF', 'Creature — Beast').
card_original_text('battlefront krushok'/'FRF', 'Battlefront Krushok can\'t be blocked by more than one creature.\nEach creature you control with a +1/+1 counter on it can\'t be blocked by more than one creature.').
card_first_print('battlefront krushok', 'FRF').
card_image_name('battlefront krushok'/'FRF', 'battlefront krushok').
card_uid('battlefront krushok'/'FRF', 'FRF:Battlefront Krushok:battlefront krushok').
card_rarity('battlefront krushok'/'FRF', 'Uncommon').
card_artist('battlefront krushok'/'FRF', 'Steve Prescott').
card_number('battlefront krushok'/'FRF', '125').
card_flavor_text('battlefront krushok'/'FRF', 'Krushoks clear debris from fertile fields, fallow fields, and battlefields.').
card_multiverse_id('battlefront krushok'/'FRF', '391801').
card_watermark('battlefront krushok'/'FRF', 'Abzan').

card_in_set('bloodfell caves', 'FRF').
card_original_type('bloodfell caves'/'FRF', 'Land').
card_original_text('bloodfell caves'/'FRF', 'Bloodfell Caves enters the battlefield tapped.\nWhen Bloodfell Caves enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('bloodfell caves'/'FRF', 'bloodfell caves').
card_uid('bloodfell caves'/'FRF', 'FRF:Bloodfell Caves:bloodfell caves').
card_rarity('bloodfell caves'/'FRF', 'Common').
card_artist('bloodfell caves'/'FRF', 'Adam Paquette').
card_number('bloodfell caves'/'FRF', '165').
card_multiverse_id('bloodfell caves'/'FRF', '391802').

card_in_set('bloodfire enforcers', 'FRF').
card_original_type('bloodfire enforcers'/'FRF', 'Creature — Human Monk').
card_original_text('bloodfire enforcers'/'FRF', 'Bloodfire Enforcers has first strike and trample as long as an instant card and a sorcery card are in your graveyard.').
card_first_print('bloodfire enforcers', 'FRF').
card_image_name('bloodfire enforcers'/'FRF', 'bloodfire enforcers').
card_uid('bloodfire enforcers'/'FRF', 'FRF:Bloodfire Enforcers:bloodfire enforcers').
card_rarity('bloodfire enforcers'/'FRF', 'Uncommon').
card_artist('bloodfire enforcers'/'FRF', 'Yefim Kligerman').
card_number('bloodfire enforcers'/'FRF', '93').
card_flavor_text('bloodfire enforcers'/'FRF', 'Bloodfire practitioners train to focus their rage, building it deep inside themselves until they are ready to release it as a devastating attack.').
card_multiverse_id('bloodfire enforcers'/'FRF', '391803').
card_watermark('bloodfire enforcers'/'FRF', 'Jeskai').

card_in_set('blossoming sands', 'FRF').
card_original_type('blossoming sands'/'FRF', 'Land').
card_original_text('blossoming sands'/'FRF', 'Blossoming Sands enters the battlefield tapped.\nWhen Blossoming Sands enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('blossoming sands'/'FRF', 'blossoming sands').
card_uid('blossoming sands'/'FRF', 'FRF:Blossoming Sands:blossoming sands').
card_rarity('blossoming sands'/'FRF', 'Common').
card_artist('blossoming sands'/'FRF', 'Sam Burley').
card_number('blossoming sands'/'FRF', '166').
card_multiverse_id('blossoming sands'/'FRF', '391804').

card_in_set('break through the line', 'FRF').
card_original_type('break through the line'/'FRF', 'Enchantment').
card_original_text('break through the line'/'FRF', '{R}: Target creature with power 2 or less gains haste until end of turn and can\'t be blocked this turn.').
card_first_print('break through the line', 'FRF').
card_image_name('break through the line'/'FRF', 'break through the line').
card_uid('break through the line'/'FRF', 'FRF:Break Through the Line:break through the line').
card_rarity('break through the line'/'FRF', 'Uncommon').
card_artist('break through the line'/'FRF', 'Clint Cearley').
card_number('break through the line'/'FRF', '94').
card_flavor_text('break through the line'/'FRF', '\"If the Mardu were more in tune with their beasts, they would not scare so easily.\"\n—Jilaya, Temur whisperer').
card_multiverse_id('break through the line'/'FRF', '391805').

card_in_set('brutal hordechief', 'FRF').
card_original_type('brutal hordechief'/'FRF', 'Creature — Orc Warrior').
card_original_text('brutal hordechief'/'FRF', 'Whenever a creature you control attacks, defending player loses 1 life and you gain 1 life.\n{3}{R/W}{R/W}: Creatures your opponents control block this turn if able, and you choose how those creatures block.').
card_first_print('brutal hordechief', 'FRF').
card_image_name('brutal hordechief'/'FRF', 'brutal hordechief').
card_uid('brutal hordechief'/'FRF', 'FRF:Brutal Hordechief:brutal hordechief').
card_rarity('brutal hordechief'/'FRF', 'Mythic Rare').
card_artist('brutal hordechief'/'FRF', 'Tyler Jacobson').
card_number('brutal hordechief'/'FRF', '64').
card_multiverse_id('brutal hordechief'/'FRF', '391806').
card_watermark('brutal hordechief'/'FRF', 'Mardu').

card_in_set('cached defenses', 'FRF').
card_original_type('cached defenses'/'FRF', 'Sorcery').
card_original_text('cached defenses'/'FRF', 'Bolster 3. (Choose a creature with the least toughness among creatures you control and put three +1/+1 counters on it.)').
card_first_print('cached defenses', 'FRF').
card_image_name('cached defenses'/'FRF', 'cached defenses').
card_uid('cached defenses'/'FRF', 'FRF:Cached Defenses:cached defenses').
card_rarity('cached defenses'/'FRF', 'Uncommon').
card_artist('cached defenses'/'FRF', 'Zack Stella').
card_number('cached defenses'/'FRF', '126').
card_flavor_text('cached defenses'/'FRF', 'The glittering scales in the Abzan vaults represent mighty deeds of the past and protection for generations to come.').
card_multiverse_id('cached defenses'/'FRF', '391807').
card_watermark('cached defenses'/'FRF', 'Abzan').

card_in_set('channel harm', 'FRF').
card_original_type('channel harm'/'FRF', 'Instant').
card_original_text('channel harm'/'FRF', 'Prevent all damage that would be dealt to you and permanents you control this turn by sources you don\'t control. If damage is prevented this way, you may have Channel Harm deal that much damage to target creature.').
card_first_print('channel harm', 'FRF').
card_image_name('channel harm'/'FRF', 'channel harm').
card_uid('channel harm'/'FRF', 'FRF:Channel Harm:channel harm').
card_rarity('channel harm'/'FRF', 'Uncommon').
card_artist('channel harm'/'FRF', 'David Gaillet').
card_number('channel harm'/'FRF', '7').
card_multiverse_id('channel harm'/'FRF', '391808').

card_in_set('citadel siege', 'FRF').
card_original_type('citadel siege'/'FRF', 'Enchantment').
card_original_text('citadel siege'/'FRF', 'As Citadel Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of combat on your turn, put two +1/+1 counters on target creature you control.\n• Dragons — At the beginning of combat on each opponent\'s turn, tap target creature that player controls.').
card_first_print('citadel siege', 'FRF').
card_image_name('citadel siege'/'FRF', 'citadel siege').
card_uid('citadel siege'/'FRF', 'FRF:Citadel Siege:citadel siege').
card_rarity('citadel siege'/'FRF', 'Rare').
card_artist('citadel siege'/'FRF', 'Steven Belledin').
card_number('citadel siege'/'FRF', '8').
card_multiverse_id('citadel siege'/'FRF', '391809').

card_in_set('cloudform', 'FRF').
card_original_type('cloudform'/'FRF', 'Enchantment').
card_original_text('cloudform'/'FRF', 'When Cloudform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Cloudform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has flying and hexproof.').
card_first_print('cloudform', 'FRF').
card_image_name('cloudform'/'FRF', 'cloudform').
card_uid('cloudform'/'FRF', 'FRF:Cloudform:cloudform').
card_rarity('cloudform'/'FRF', 'Uncommon').
card_artist('cloudform'/'FRF', 'Noah Bradley').
card_number('cloudform'/'FRF', '32').
card_multiverse_id('cloudform'/'FRF', '391810').

card_in_set('collateral damage', 'FRF').
card_original_type('collateral damage'/'FRF', 'Instant').
card_original_text('collateral damage'/'FRF', 'As an additional cost to cast Collateral Damage, sacrifice a creature.\nCollateral Damage deals 3 damage to target creature or player.').
card_first_print('collateral damage', 'FRF').
card_image_name('collateral damage'/'FRF', 'collateral damage').
card_uid('collateral damage'/'FRF', 'FRF:Collateral Damage:collateral damage').
card_rarity('collateral damage'/'FRF', 'Common').
card_artist('collateral damage'/'FRF', 'Ryan Barger').
card_number('collateral damage'/'FRF', '95').
card_flavor_text('collateral damage'/'FRF', 'It is much easier to create fire than to contain it.').
card_multiverse_id('collateral damage'/'FRF', '391811').

card_in_set('crucible of the spirit dragon', 'FRF').
card_original_type('crucible of the spirit dragon'/'FRF', 'Land').
card_original_text('crucible of the spirit dragon'/'FRF', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Crucible of the Spirit Dragon.\n{T}, Remove X storage counters from Crucible of the Spirit Dragon: Add X mana in any combination of colors to your mana pool. Spend this mana only to cast Dragon spells or activate abilities of Dragons.').
card_first_print('crucible of the spirit dragon', 'FRF').
card_image_name('crucible of the spirit dragon'/'FRF', 'crucible of the spirit dragon').
card_uid('crucible of the spirit dragon'/'FRF', 'FRF:Crucible of the Spirit Dragon:crucible of the spirit dragon').
card_rarity('crucible of the spirit dragon'/'FRF', 'Rare').
card_artist('crucible of the spirit dragon'/'FRF', 'Jung Park').
card_number('crucible of the spirit dragon'/'FRF', '167').
card_multiverse_id('crucible of the spirit dragon'/'FRF', '391812').

card_in_set('crux of fate', 'FRF').
card_original_type('crux of fate'/'FRF', 'Sorcery').
card_original_text('crux of fate'/'FRF', 'Choose one —\n• Destroy all Dragon creatures.\n• Destroy all non-Dragon creatures.').
card_first_print('crux of fate', 'FRF').
card_image_name('crux of fate'/'FRF', 'crux of fate').
card_uid('crux of fate'/'FRF', 'FRF:Crux of Fate:crux of fate').
card_rarity('crux of fate'/'FRF', 'Rare').
card_artist('crux of fate'/'FRF', 'Michael Komarck').
card_number('crux of fate'/'FRF', '65').
card_flavor_text('crux of fate'/'FRF', 'Ugin\'s whispered summons led Sarkhan Vol to the moment that would echo down the centuries and seal Tarkir\'s fate: the primal battle between Ugin and Nicol Bolas.').
card_multiverse_id('crux of fate'/'FRF', '391813').

card_in_set('cunning strike', 'FRF').
card_original_type('cunning strike'/'FRF', 'Instant').
card_original_text('cunning strike'/'FRF', 'Cunning Strike deals 2 damage to target creature and 2 damage to target player.\nDraw a card.').
card_first_print('cunning strike', 'FRF').
card_image_name('cunning strike'/'FRF', 'cunning strike').
card_uid('cunning strike'/'FRF', 'FRF:Cunning Strike:cunning strike').
card_rarity('cunning strike'/'FRF', 'Common').
card_artist('cunning strike'/'FRF', 'Clint Cearley').
card_number('cunning strike'/'FRF', '150').
card_flavor_text('cunning strike'/'FRF', '\"The opponent who blocks the path, becomes the path.\"\n—Shu Yun, the Silent Tempest').
card_multiverse_id('cunning strike'/'FRF', '391814').

card_in_set('daghatar the adamant', 'FRF').
card_original_type('daghatar the adamant'/'FRF', 'Legendary Creature — Human Warrior').
card_original_text('daghatar the adamant'/'FRF', 'Vigilance\nDaghatar the Adamant enters the battlefield with four +1/+1 counters on it.\n{1}{B/G}{B/G}: Move a +1/+1 counter from target creature onto a second target creature.').
card_first_print('daghatar the adamant', 'FRF').
card_image_name('daghatar the adamant'/'FRF', 'daghatar the adamant').
card_uid('daghatar the adamant'/'FRF', 'FRF:Daghatar the Adamant:daghatar the adamant').
card_rarity('daghatar the adamant'/'FRF', 'Rare').
card_artist('daghatar the adamant'/'FRF', 'Zack Stella').
card_number('daghatar the adamant'/'FRF', '9').
card_flavor_text('daghatar the adamant'/'FRF', '\"Our victories will protect a thousand generations.\"').
card_multiverse_id('daghatar the adamant'/'FRF', '391815').
card_watermark('daghatar the adamant'/'FRF', 'Abzan').

card_in_set('dark deal', 'FRF').
card_original_type('dark deal'/'FRF', 'Sorcery').
card_original_text('dark deal'/'FRF', 'Each player discards all the cards in his or her hand, then draws that many cards minus one.').
card_first_print('dark deal', 'FRF').
card_image_name('dark deal'/'FRF', 'dark deal').
card_uid('dark deal'/'FRF', 'FRF:Dark Deal:dark deal').
card_rarity('dark deal'/'FRF', 'Uncommon').
card_artist('dark deal'/'FRF', 'Scott Murphy').
card_number('dark deal'/'FRF', '66').
card_flavor_text('dark deal'/'FRF', 'The first khans of the Sultai relied on the magic of the rakshasa to ensure the survival of the clan.').
card_multiverse_id('dark deal'/'FRF', '391816').

card_in_set('defiant ogre', 'FRF').
card_original_type('defiant ogre'/'FRF', 'Creature — Ogre Warrior').
card_original_text('defiant ogre'/'FRF', 'When Defiant Ogre enters the battlefield, choose one —\n• Put a +1/+1 counter on Defiant Ogre.\n• Destroy target artifact.').
card_first_print('defiant ogre', 'FRF').
card_image_name('defiant ogre'/'FRF', 'defiant ogre').
card_uid('defiant ogre'/'FRF', 'FRF:Defiant Ogre:defiant ogre').
card_rarity('defiant ogre'/'FRF', 'Common').
card_artist('defiant ogre'/'FRF', 'Craig J Spearing').
card_number('defiant ogre'/'FRF', '96').
card_flavor_text('defiant ogre'/'FRF', '\"I have no clan, but I still have purpose.\"').
card_multiverse_id('defiant ogre'/'FRF', '391817').

card_in_set('destructor dragon', 'FRF').
card_original_type('destructor dragon'/'FRF', 'Creature — Dragon').
card_original_text('destructor dragon'/'FRF', 'Flying\nWhen Destructor Dragon dies, destroy target noncreature permanent.').
card_first_print('destructor dragon', 'FRF').
card_image_name('destructor dragon'/'FRF', 'destructor dragon').
card_uid('destructor dragon'/'FRF', 'FRF:Destructor Dragon:destructor dragon').
card_rarity('destructor dragon'/'FRF', 'Uncommon').
card_artist('destructor dragon'/'FRF', 'Peter Mohrbacher').
card_number('destructor dragon'/'FRF', '127').
card_flavor_text('destructor dragon'/'FRF', 'After countless attacks on the Salt Road where it passes through the frozen tundra, the Abzan began to refer to the area as Atarka territory rather than Temur lands.').
card_multiverse_id('destructor dragon'/'FRF', '391818').
card_watermark('destructor dragon'/'FRF', 'Atarka').

card_in_set('diplomacy of the wastes', 'FRF').
card_original_type('diplomacy of the wastes'/'FRF', 'Sorcery').
card_original_text('diplomacy of the wastes'/'FRF', 'Target opponent reveals his or her hand. You choose a nonland card from it. That player discards that card. If you control a Warrior, that player loses 2 life.').
card_first_print('diplomacy of the wastes', 'FRF').
card_image_name('diplomacy of the wastes'/'FRF', 'diplomacy of the wastes').
card_uid('diplomacy of the wastes'/'FRF', 'FRF:Diplomacy of the Wastes:diplomacy of the wastes').
card_rarity('diplomacy of the wastes'/'FRF', 'Uncommon').
card_artist('diplomacy of the wastes'/'FRF', 'Jack Wang').
card_number('diplomacy of the wastes'/'FRF', '67').
card_flavor_text('diplomacy of the wastes'/'FRF', '\"Our emissaries are gifted negotiators.\"\n—Alesha, Who Smiles at Death').
card_multiverse_id('diplomacy of the wastes'/'FRF', '391819').

card_in_set('dismal backwater', 'FRF').
card_original_type('dismal backwater'/'FRF', 'Land').
card_original_text('dismal backwater'/'FRF', 'Dismal Backwater enters the battlefield tapped.\nWhen Dismal Backwater enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('dismal backwater'/'FRF', 'dismal backwater').
card_uid('dismal backwater'/'FRF', 'FRF:Dismal Backwater:dismal backwater').
card_rarity('dismal backwater'/'FRF', 'Common').
card_artist('dismal backwater'/'FRF', 'Sam Burley').
card_number('dismal backwater'/'FRF', '168').
card_multiverse_id('dismal backwater'/'FRF', '391820').

card_in_set('douse in gloom', 'FRF').
card_original_type('douse in gloom'/'FRF', 'Instant').
card_original_text('douse in gloom'/'FRF', 'Douse in Gloom deals 2 damage to target creature and you gain 2 life.').
card_image_name('douse in gloom'/'FRF', 'douse in gloom').
card_uid('douse in gloom'/'FRF', 'FRF:Douse in Gloom:douse in gloom').
card_rarity('douse in gloom'/'FRF', 'Common').
card_artist('douse in gloom'/'FRF', 'Ryan Yee').
card_number('douse in gloom'/'FRF', '68').
card_flavor_text('douse in gloom'/'FRF', 'The Mardu warrior was overjoyed when Tasigur set him free and granted him passage through the jungle. He failed to notice the Sultai leader\'s cruel smile.').
card_multiverse_id('douse in gloom'/'FRF', '391821').

card_in_set('dragon bell monk', 'FRF').
card_original_type('dragon bell monk'/'FRF', 'Creature — Human Monk').
card_original_text('dragon bell monk'/'FRF', 'Vigilance\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('dragon bell monk', 'FRF').
card_image_name('dragon bell monk'/'FRF', 'dragon bell monk').
card_uid('dragon bell monk'/'FRF', 'FRF:Dragon Bell Monk:dragon bell monk').
card_rarity('dragon bell monk'/'FRF', 'Common').
card_artist('dragon bell monk'/'FRF', 'Lius Lasahido').
card_number('dragon bell monk'/'FRF', '10').
card_flavor_text('dragon bell monk'/'FRF', 'He has spent years training his eyes and ears to detect the subtle signs of a dragon\'s approach.').
card_multiverse_id('dragon bell monk'/'FRF', '391822').
card_watermark('dragon bell monk'/'FRF', 'Jeskai').

card_in_set('dragonrage', 'FRF').
card_original_type('dragonrage'/'FRF', 'Instant').
card_original_text('dragonrage'/'FRF', 'Add {R} to your mana pool for each attacking creature you control. Until end of turn, attacking creatures you control gain \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('dragonrage', 'FRF').
card_image_name('dragonrage'/'FRF', 'dragonrage').
card_uid('dragonrage'/'FRF', 'FRF:Dragonrage:dragonrage').
card_rarity('dragonrage'/'FRF', 'Uncommon').
card_artist('dragonrage'/'FRF', 'Tomasz Jedruszek').
card_number('dragonrage'/'FRF', '97').
card_flavor_text('dragonrage'/'FRF', '\"Dragons in the skies of Tarkir—for the first time, it feels like my home.\"\n—Sarkhan Vol').
card_multiverse_id('dragonrage'/'FRF', '391823').

card_in_set('dragonscale general', 'FRF').
card_original_type('dragonscale general'/'FRF', 'Creature — Human Warrior').
card_original_text('dragonscale general'/'FRF', 'At the beginning of your end step, bolster X, where X is the number of tapped creatures you control. (Choose a creature with the least toughness among creatures you control and put X +1/+1 counters on it.)').
card_image_name('dragonscale general'/'FRF', 'dragonscale general').
card_uid('dragonscale general'/'FRF', 'FRF:Dragonscale General:dragonscale general').
card_rarity('dragonscale general'/'FRF', 'Rare').
card_artist('dragonscale general'/'FRF', 'Volkan Baga').
card_number('dragonscale general'/'FRF', '11').
card_flavor_text('dragonscale general'/'FRF', '\"Dragons seek war. I bring it to them.\"').
card_multiverse_id('dragonscale general'/'FRF', '391824').
card_watermark('dragonscale general'/'FRF', 'Abzan').

card_in_set('dromoka, the eternal', 'FRF').
card_original_type('dromoka, the eternal'/'FRF', 'Legendary Creature — Dragon').
card_original_text('dromoka, the eternal'/'FRF', 'Flying\nWhenever a Dragon you control attacks, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_first_print('dromoka, the eternal', 'FRF').
card_image_name('dromoka, the eternal'/'FRF', 'dromoka, the eternal').
card_uid('dromoka, the eternal'/'FRF', 'FRF:Dromoka, the Eternal:dromoka, the eternal').
card_rarity('dromoka, the eternal'/'FRF', 'Rare').
card_artist('dromoka, the eternal'/'FRF', 'Eric Deschamps').
card_number('dromoka, the eternal'/'FRF', '151').
card_flavor_text('dromoka, the eternal'/'FRF', '\"The bane of countless shattered weapons, each a failure to slay her.\"\n—Daghatar the Adamant').
card_multiverse_id('dromoka, the eternal'/'FRF', '391825').
card_watermark('dromoka, the eternal'/'FRF', 'Dromoka').

card_in_set('elite scaleguard', 'FRF').
card_original_type('elite scaleguard'/'FRF', 'Creature — Human Soldier').
card_original_text('elite scaleguard'/'FRF', 'When Elite Scaleguard enters the battlefield, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)\nWhenever a creature you control with a +1/+1 counter on it attacks, tap target creature defending player controls.').
card_first_print('elite scaleguard', 'FRF').
card_image_name('elite scaleguard'/'FRF', 'elite scaleguard').
card_uid('elite scaleguard'/'FRF', 'FRF:Elite Scaleguard:elite scaleguard').
card_rarity('elite scaleguard'/'FRF', 'Uncommon').
card_artist('elite scaleguard'/'FRF', 'Steve Prescott').
card_number('elite scaleguard'/'FRF', '12').
card_multiverse_id('elite scaleguard'/'FRF', '391826').
card_watermark('elite scaleguard'/'FRF', 'Abzan').

card_in_set('enhanced awareness', 'FRF').
card_original_type('enhanced awareness'/'FRF', 'Instant').
card_original_text('enhanced awareness'/'FRF', 'Draw three cards, then discard a card.').
card_first_print('enhanced awareness', 'FRF').
card_image_name('enhanced awareness'/'FRF', 'enhanced awareness').
card_uid('enhanced awareness'/'FRF', 'FRF:Enhanced Awareness:enhanced awareness').
card_rarity('enhanced awareness'/'FRF', 'Common').
card_artist('enhanced awareness'/'FRF', 'Ryan Alexander Lee').
card_number('enhanced awareness'/'FRF', '33').
card_flavor_text('enhanced awareness'/'FRF', '\"Study the topography of your enemies, and you will have a map to victory.\"\n—Shu Yun, the Silent Tempest').
card_multiverse_id('enhanced awareness'/'FRF', '391827').

card_in_set('ethereal ambush', 'FRF').
card_original_type('ethereal ambush'/'FRF', 'Instant').
card_original_text('ethereal ambush'/'FRF', 'Manifest the top two cards of your library. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('ethereal ambush', 'FRF').
card_image_name('ethereal ambush'/'FRF', 'ethereal ambush').
card_uid('ethereal ambush'/'FRF', 'FRF:Ethereal Ambush:ethereal ambush').
card_rarity('ethereal ambush'/'FRF', 'Common').
card_artist('ethereal ambush'/'FRF', 'Lius Lasahido').
card_number('ethereal ambush'/'FRF', '152').
card_flavor_text('ethereal ambush'/'FRF', 'The energies of the Temur lands are readily unleashed upon intruders.').
card_multiverse_id('ethereal ambush'/'FRF', '391828').

card_in_set('fascination', 'FRF').
card_original_type('fascination'/'FRF', 'Sorcery').
card_original_text('fascination'/'FRF', 'Choose one —\n• Each player draws X cards.\n• Each player puts the top X cards of his or her library into his or her graveyard.').
card_first_print('fascination', 'FRF').
card_image_name('fascination'/'FRF', 'fascination').
card_uid('fascination'/'FRF', 'FRF:Fascination:fascination').
card_rarity('fascination'/'FRF', 'Uncommon').
card_artist('fascination'/'FRF', 'Svetlin Velinov').
card_number('fascination'/'FRF', '34').
card_multiverse_id('fascination'/'FRF', '391829').

card_in_set('fearsome awakening', 'FRF').
card_original_type('fearsome awakening'/'FRF', 'Sorcery').
card_original_text('fearsome awakening'/'FRF', 'Return target creature card from your graveyard to the battlefield. If it\'s a Dragon, put two +1/+1 counters on it.').
card_first_print('fearsome awakening', 'FRF').
card_image_name('fearsome awakening'/'FRF', 'fearsome awakening').
card_uid('fearsome awakening'/'FRF', 'FRF:Fearsome Awakening:fearsome awakening').
card_rarity('fearsome awakening'/'FRF', 'Uncommon').
card_artist('fearsome awakening'/'FRF', 'Véronique Meignaud').
card_number('fearsome awakening'/'FRF', '69').
card_flavor_text('fearsome awakening'/'FRF', 'On Tarkir, dragons emerge from enormous tempests, phenomena tied to Ugin\'s very presence on the plane.').
card_multiverse_id('fearsome awakening'/'FRF', '391830').

card_in_set('feral krushok', 'FRF').
card_original_type('feral krushok'/'FRF', 'Creature — Beast').
card_original_text('feral krushok'/'FRF', '').
card_first_print('feral krushok', 'FRF').
card_image_name('feral krushok'/'FRF', 'feral krushok').
card_uid('feral krushok'/'FRF', 'FRF:Feral Krushok:feral krushok').
card_rarity('feral krushok'/'FRF', 'Common').
card_artist('feral krushok'/'FRF', 'Kev Walker').
card_number('feral krushok'/'FRF', '128').
card_flavor_text('feral krushok'/'FRF', 'In a stunning act of diplomacy, Yasova Dragonclaw ceded a portion of Temur lands to the Sultai. Her clan protested until they saw she had given the Sultai the breeding grounds of the krushoks. They hadn\'t realized she had a sense of humor.').
card_multiverse_id('feral krushok'/'FRF', '391831').

card_in_set('fierce invocation', 'FRF').
card_original_type('fierce invocation'/'FRF', 'Sorcery').
card_original_text('fierce invocation'/'FRF', 'Manifest the top card of your library, then put two +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('fierce invocation'/'FRF', 'fierce invocation').
card_uid('fierce invocation'/'FRF', 'FRF:Fierce Invocation:fierce invocation').
card_rarity('fierce invocation'/'FRF', 'Common').
card_artist('fierce invocation'/'FRF', 'Tyler Jacobson').
card_number('fierce invocation'/'FRF', '98').
card_flavor_text('fierce invocation'/'FRF', 'Anger can empower those who embrace it.').
card_multiverse_id('fierce invocation'/'FRF', '391832').

card_in_set('flamerush rider', 'FRF').
card_original_type('flamerush rider'/'FRF', 'Creature — Human Warrior').
card_original_text('flamerush rider'/'FRF', 'Whenever Flamerush Rider attacks, put a token onto the battlefield tapped and attacking that\'s a copy of another target attacking creature. Exile the token at end of combat.\nDash {2}{R}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_image_name('flamerush rider'/'FRF', 'flamerush rider').
card_uid('flamerush rider'/'FRF', 'FRF:Flamerush Rider:flamerush rider').
card_rarity('flamerush rider'/'FRF', 'Rare').
card_artist('flamerush rider'/'FRF', 'Min Yum').
card_number('flamerush rider'/'FRF', '99').
card_multiverse_id('flamerush rider'/'FRF', '391833').
card_watermark('flamerush rider'/'FRF', 'Mardu').

card_in_set('flamewake phoenix', 'FRF').
card_original_type('flamewake phoenix'/'FRF', 'Creature — Phoenix').
card_original_text('flamewake phoenix'/'FRF', 'Flying, haste\nFlamewake Phoenix attacks each turn if able.\nFerocious — At the beginning of combat on your turn, if you control a creature with power 4 or greater, you may pay {R}. If you do, return Flamewake Phoenix from your graveyard to the battlefield.').
card_first_print('flamewake phoenix', 'FRF').
card_image_name('flamewake phoenix'/'FRF', 'flamewake phoenix').
card_uid('flamewake phoenix'/'FRF', 'FRF:Flamewake Phoenix:flamewake phoenix').
card_rarity('flamewake phoenix'/'FRF', 'Rare').
card_artist('flamewake phoenix'/'FRF', 'Min Yum').
card_number('flamewake phoenix'/'FRF', '100').
card_multiverse_id('flamewake phoenix'/'FRF', '391834').
card_watermark('flamewake phoenix'/'FRF', 'Temur').

card_in_set('forest', 'FRF').
card_original_type('forest'/'FRF', 'Basic Land — Forest').
card_original_text('forest'/'FRF', 'G').
card_image_name('forest'/'FRF', 'forest1').
card_uid('forest'/'FRF', 'FRF:Forest:forest1').
card_rarity('forest'/'FRF', 'Basic Land').
card_artist('forest'/'FRF', 'Titus Lunter').
card_number('forest'/'FRF', '184').
card_multiverse_id('forest'/'FRF', '391836').

card_in_set('forest', 'FRF').
card_original_type('forest'/'FRF', 'Basic Land — Forest').
card_original_text('forest'/'FRF', 'G').
card_image_name('forest'/'FRF', 'forest2').
card_uid('forest'/'FRF', 'FRF:Forest:forest2').
card_rarity('forest'/'FRF', 'Basic Land').
card_artist('forest'/'FRF', 'Adam Paquette').
card_number('forest'/'FRF', '185').
card_multiverse_id('forest'/'FRF', '391835').

card_in_set('formless nurturing', 'FRF').
card_original_type('formless nurturing'/'FRF', 'Sorcery').
card_original_text('formless nurturing'/'FRF', 'Manifest the top card of your library, then put a +1/+1 counter on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('formless nurturing'/'FRF', 'formless nurturing').
card_uid('formless nurturing'/'FRF', 'FRF:Formless Nurturing:formless nurturing').
card_rarity('formless nurturing'/'FRF', 'Common').
card_artist('formless nurturing'/'FRF', 'Cliff Childs').
card_number('formless nurturing'/'FRF', '129').
card_multiverse_id('formless nurturing'/'FRF', '391837').

card_in_set('friendly fire', 'FRF').
card_original_type('friendly fire'/'FRF', 'Instant').
card_original_text('friendly fire'/'FRF', 'Target creature\'s controller reveals a card at random from his or her hand. Friendly Fire deals damage to that creature and that player equal to the revealed card\'s converted mana cost.').
card_first_print('friendly fire', 'FRF').
card_image_name('friendly fire'/'FRF', 'friendly fire').
card_uid('friendly fire'/'FRF', 'FRF:Friendly Fire:friendly fire').
card_rarity('friendly fire'/'FRF', 'Uncommon').
card_artist('friendly fire'/'FRF', 'Anthony Palumbo').
card_number('friendly fire'/'FRF', '101').
card_flavor_text('friendly fire'/'FRF', '\"Never tell goblins to ‘fire at will.\'\"\n—Urut Barzeel, Mardu hordechief').
card_multiverse_id('friendly fire'/'FRF', '391838').

card_in_set('frontier mastodon', 'FRF').
card_original_type('frontier mastodon'/'FRF', 'Creature — Elephant').
card_original_text('frontier mastodon'/'FRF', 'Ferocious — Frontier Mastodon enters the battlefield with a +1/+1 counter on it if you control a creature with power 4 or greater.').
card_first_print('frontier mastodon', 'FRF').
card_image_name('frontier mastodon'/'FRF', 'frontier mastodon').
card_uid('frontier mastodon'/'FRF', 'FRF:Frontier Mastodon:frontier mastodon').
card_rarity('frontier mastodon'/'FRF', 'Common').
card_artist('frontier mastodon'/'FRF', 'Nils Hamm').
card_number('frontier mastodon'/'FRF', '130').
card_flavor_text('frontier mastodon'/'FRF', 'Each bronze disk on its harness is an offering from a young Temur hunter, a sign of respect to the spirits of the wild.').
card_multiverse_id('frontier mastodon'/'FRF', '391839').
card_watermark('frontier mastodon'/'FRF', 'Temur').

card_in_set('frontier siege', 'FRF').
card_original_type('frontier siege'/'FRF', 'Enchantment').
card_original_text('frontier siege'/'FRF', 'As Frontier Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of each of your main phases, add {G}{G} to your mana pool.\n• Dragons — Whenever a creature with flying enters the battlefield under your control, you may have it fight target creature you don\'t control.').
card_first_print('frontier siege', 'FRF').
card_image_name('frontier siege'/'FRF', 'frontier siege').
card_uid('frontier siege'/'FRF', 'FRF:Frontier Siege:frontier siege').
card_rarity('frontier siege'/'FRF', 'Rare').
card_artist('frontier siege'/'FRF', 'James Ryman').
card_number('frontier siege'/'FRF', '131').
card_multiverse_id('frontier siege'/'FRF', '391840').

card_in_set('frost walker', 'FRF').
card_original_type('frost walker'/'FRF', 'Creature — Elemental').
card_original_text('frost walker'/'FRF', 'When Frost Walker becomes the target of a spell or ability, sacrifice it.').
card_first_print('frost walker', 'FRF').
card_image_name('frost walker'/'FRF', 'frost walker').
card_uid('frost walker'/'FRF', 'FRF:Frost Walker:frost walker').
card_rarity('frost walker'/'FRF', 'Uncommon').
card_artist('frost walker'/'FRF', 'Mike Bierek').
card_number('frost walker'/'FRF', '35').
card_flavor_text('frost walker'/'FRF', '\"As the clans carved out their territories, we saw allies where the Mardu saw only obstacles.\"\n—Yasova Dragonclaw').
card_multiverse_id('frost walker'/'FRF', '391841').
card_watermark('frost walker'/'FRF', 'Temur').

card_in_set('fruit of the first tree', 'FRF').
card_original_type('fruit of the first tree'/'FRF', 'Enchantment — Aura').
card_original_text('fruit of the first tree'/'FRF', 'Enchant creature\nWhen enchanted creature dies, you gain X life and draw X cards, where X is its toughness.').
card_first_print('fruit of the first tree', 'FRF').
card_image_name('fruit of the first tree'/'FRF', 'fruit of the first tree').
card_uid('fruit of the first tree'/'FRF', 'FRF:Fruit of the First Tree:fruit of the first tree').
card_rarity('fruit of the first tree'/'FRF', 'Uncommon').
card_artist('fruit of the first tree'/'FRF', 'Ryan Yee').
card_number('fruit of the first tree'/'FRF', '132').
card_flavor_text('fruit of the first tree'/'FRF', '\"I will be the ancestor my descendants call upon for aid. On that day I will take up my sword for my family once more.\"').
card_multiverse_id('fruit of the first tree'/'FRF', '391842').

card_in_set('ghastly conscription', 'FRF').
card_original_type('ghastly conscription'/'FRF', 'Sorcery').
card_original_text('ghastly conscription'/'FRF', 'Exile all creature cards from target player\'s graveyard in a face-down pile, shuffle that pile, then manifest those cards. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('ghastly conscription', 'FRF').
card_image_name('ghastly conscription'/'FRF', 'ghastly conscription').
card_uid('ghastly conscription'/'FRF', 'FRF:Ghastly Conscription:ghastly conscription').
card_rarity('ghastly conscription'/'FRF', 'Mythic Rare').
card_artist('ghastly conscription'/'FRF', 'YW Tang').
card_number('ghastly conscription'/'FRF', '70').
card_multiverse_id('ghastly conscription'/'FRF', '391843').

card_in_set('goblin boom keg', 'FRF').
card_original_type('goblin boom keg'/'FRF', 'Artifact').
card_original_text('goblin boom keg'/'FRF', 'At the beginning of your upkeep, sacrifice Goblin Boom Keg.\nWhen Goblin Boom Keg is put into a graveyard from the battlefield, it deals 3 damage to target creature or player.').
card_first_print('goblin boom keg', 'FRF').
card_image_name('goblin boom keg'/'FRF', 'goblin boom keg').
card_uid('goblin boom keg'/'FRF', 'FRF:Goblin Boom Keg:goblin boom keg').
card_rarity('goblin boom keg'/'FRF', 'Uncommon').
card_artist('goblin boom keg'/'FRF', 'Viktor Titov').
card_number('goblin boom keg'/'FRF', '159').
card_flavor_text('goblin boom keg'/'FRF', 'Dragonfire spawned many imitations. Some were more dramatic than others.').
card_multiverse_id('goblin boom keg'/'FRF', '391844').

card_in_set('goblin heelcutter', 'FRF').
card_original_type('goblin heelcutter'/'FRF', 'Creature — Goblin Berserker').
card_original_text('goblin heelcutter'/'FRF', 'Whenever Goblin Heelcutter attacks, target creature can\'t block this turn.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('goblin heelcutter', 'FRF').
card_image_name('goblin heelcutter'/'FRF', 'goblin heelcutter').
card_uid('goblin heelcutter'/'FRF', 'FRF:Goblin Heelcutter:goblin heelcutter').
card_rarity('goblin heelcutter'/'FRF', 'Common').
card_artist('goblin heelcutter'/'FRF', 'Jesper Ejsing').
card_number('goblin heelcutter'/'FRF', '102').
card_multiverse_id('goblin heelcutter'/'FRF', '391845').
card_watermark('goblin heelcutter'/'FRF', 'Mardu').

card_in_set('gore swine', 'FRF').
card_original_type('gore swine'/'FRF', 'Creature — Boar').
card_original_text('gore swine'/'FRF', '').
card_first_print('gore swine', 'FRF').
card_image_name('gore swine'/'FRF', 'gore swine').
card_uid('gore swine'/'FRF', 'FRF:Gore Swine:gore swine').
card_rarity('gore swine'/'FRF', 'Common').
card_artist('gore swine'/'FRF', 'Jack Wang').
card_number('gore swine'/'FRF', '103').
card_flavor_text('gore swine'/'FRF', '\"The Mardu are like the gore swine. We are wild, hunt in packs, and rarely clean the blood from our blades.\"\n—Vallash, Mardu warrior').
card_multiverse_id('gore swine'/'FRF', '391846').

card_in_set('grave strength', 'FRF').
card_original_type('grave strength'/'FRF', 'Sorcery').
card_original_text('grave strength'/'FRF', 'Choose target creature. Put the top three cards of your library into your graveyard, then put a +1/+1 counter on that creature for each creature card in your graveyard.').
card_first_print('grave strength', 'FRF').
card_image_name('grave strength'/'FRF', 'grave strength').
card_uid('grave strength'/'FRF', 'FRF:Grave Strength:grave strength').
card_rarity('grave strength'/'FRF', 'Uncommon').
card_artist('grave strength'/'FRF', 'Jeff Simpson').
card_number('grave strength'/'FRF', '71').
card_flavor_text('grave strength'/'FRF', 'So long as death follows life, the supply lines of the Sultai will never be cut.').
card_multiverse_id('grave strength'/'FRF', '391847').

card_in_set('great-horn krushok', 'FRF').
card_original_type('great-horn krushok'/'FRF', 'Creature — Beast').
card_original_text('great-horn krushok'/'FRF', '').
card_first_print('great-horn krushok', 'FRF').
card_image_name('great-horn krushok'/'FRF', 'great-horn krushok').
card_uid('great-horn krushok'/'FRF', 'FRF:Great-Horn Krushok:great-horn krushok').
card_rarity('great-horn krushok'/'FRF', 'Common').
card_artist('great-horn krushok'/'FRF', 'YW Tang').
card_number('great-horn krushok'/'FRF', '13').
card_flavor_text('great-horn krushok'/'FRF', 'Perfectly suited for life in the Shifting Wastes, the krushok is well protected by its horn, its hide, and its temper.').
card_multiverse_id('great-horn krushok'/'FRF', '391848').

card_in_set('grim contest', 'FRF').
card_original_type('grim contest'/'FRF', 'Instant').
card_original_text('grim contest'/'FRF', 'Choose target creature you control and target creature an opponent controls. Each of those creatures deals damage equal to its toughness to the other.').
card_first_print('grim contest', 'FRF').
card_image_name('grim contest'/'FRF', 'grim contest').
card_uid('grim contest'/'FRF', 'FRF:Grim Contest:grim contest').
card_rarity('grim contest'/'FRF', 'Common').
card_artist('grim contest'/'FRF', 'Phill Simmer').
card_number('grim contest'/'FRF', '153').
card_flavor_text('grim contest'/'FRF', 'The invader hoped he could survive the beast\'s jaws and emerge through its rotting skin.').
card_multiverse_id('grim contest'/'FRF', '391849').

card_in_set('gurmag angler', 'FRF').
card_original_type('gurmag angler'/'FRF', 'Creature — Zombie Fish').
card_original_text('gurmag angler'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)').
card_first_print('gurmag angler', 'FRF').
card_image_name('gurmag angler'/'FRF', 'gurmag angler').
card_uid('gurmag angler'/'FRF', 'FRF:Gurmag Angler:gurmag angler').
card_rarity('gurmag angler'/'FRF', 'Common').
card_artist('gurmag angler'/'FRF', 'YW Tang').
card_number('gurmag angler'/'FRF', '72').
card_flavor_text('gurmag angler'/'FRF', 'If everything in the Gurmag Swamp hungers for human flesh, what bait could be more effective?').
card_multiverse_id('gurmag angler'/'FRF', '391850').
card_watermark('gurmag angler'/'FRF', 'Sultai').

card_in_set('harsh sustenance', 'FRF').
card_original_type('harsh sustenance'/'FRF', 'Instant').
card_original_text('harsh sustenance'/'FRF', 'Harsh Sustenance deals X damage to target creature or player and you gain X life, where X is the number of creatures you control.').
card_first_print('harsh sustenance', 'FRF').
card_image_name('harsh sustenance'/'FRF', 'harsh sustenance').
card_uid('harsh sustenance'/'FRF', 'FRF:Harsh Sustenance:harsh sustenance').
card_rarity('harsh sustenance'/'FRF', 'Common').
card_artist('harsh sustenance'/'FRF', 'Cynthia Sheppard').
card_number('harsh sustenance'/'FRF', '154').
card_flavor_text('harsh sustenance'/'FRF', 'The Shifting Wastes provide refuge to those who know where to look for it.').
card_multiverse_id('harsh sustenance'/'FRF', '391851').

card_in_set('hero\'s blade', 'FRF').
card_original_type('hero\'s blade'/'FRF', 'Artifact — Equipment').
card_original_text('hero\'s blade'/'FRF', 'Equipped creature gets +3/+2.\nWhenever a legendary creature enters the battlefield under your control, you may attach Hero\'s Blade to it.\nEquip {4}').
card_first_print('hero\'s blade', 'FRF').
card_image_name('hero\'s blade'/'FRF', 'hero\'s blade').
card_uid('hero\'s blade'/'FRF', 'FRF:Hero\'s Blade:hero\'s blade').
card_rarity('hero\'s blade'/'FRF', 'Uncommon').
card_artist('hero\'s blade'/'FRF', 'Aaron Miller').
card_number('hero\'s blade'/'FRF', '160').
card_flavor_text('hero\'s blade'/'FRF', 'The best swords are forged with dragonfire.').
card_multiverse_id('hero\'s blade'/'FRF', '391852').

card_in_set('hewed stone retainers', 'FRF').
card_original_type('hewed stone retainers'/'FRF', 'Artifact Creature — Golem').
card_original_text('hewed stone retainers'/'FRF', 'Cast Hewed Stone Retainers only if you\'ve cast another spell this turn.').
card_image_name('hewed stone retainers'/'FRF', 'hewed stone retainers').
card_uid('hewed stone retainers'/'FRF', 'FRF:Hewed Stone Retainers:hewed stone retainers').
card_rarity('hewed stone retainers'/'FRF', 'Uncommon').
card_artist('hewed stone retainers'/'FRF', 'David Seguin').
card_number('hewed stone retainers'/'FRF', '161').
card_flavor_text('hewed stone retainers'/'FRF', '\"Their origins are shrouded in mystery. I believe they are the protectors of the lost secrets of our world.\"\n—Jilaya, Temur whisperer').
card_multiverse_id('hewed stone retainers'/'FRF', '391853').

card_in_set('honor\'s reward', 'FRF').
card_original_type('honor\'s reward'/'FRF', 'Instant').
card_original_text('honor\'s reward'/'FRF', 'You gain 4 life. Bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_first_print('honor\'s reward', 'FRF').
card_image_name('honor\'s reward'/'FRF', 'honor\'s reward').
card_uid('honor\'s reward'/'FRF', 'FRF:Honor\'s Reward:honor\'s reward').
card_rarity('honor\'s reward'/'FRF', 'Uncommon').
card_artist('honor\'s reward'/'FRF', 'Izzy').
card_number('honor\'s reward'/'FRF', '14').
card_flavor_text('honor\'s reward'/'FRF', 'It seldom rains in Abzan lands. When it does, the khan marks the occasion by honoring the families of the fallen.').
card_multiverse_id('honor\'s reward'/'FRF', '391854').
card_watermark('honor\'s reward'/'FRF', 'Abzan').

card_in_set('hooded assassin', 'FRF').
card_original_type('hooded assassin'/'FRF', 'Creature — Human Assassin').
card_original_text('hooded assassin'/'FRF', 'When Hooded Assassin enters the battlefield, choose one —\n• Put a +1/+1 counter on Hooded Assassin.\n• Destroy target creature that was dealt damage this turn.').
card_first_print('hooded assassin', 'FRF').
card_image_name('hooded assassin'/'FRF', 'hooded assassin').
card_uid('hooded assassin'/'FRF', 'FRF:Hooded Assassin:hooded assassin').
card_rarity('hooded assassin'/'FRF', 'Common').
card_artist('hooded assassin'/'FRF', 'Matt Stewart').
card_number('hooded assassin'/'FRF', '73').
card_multiverse_id('hooded assassin'/'FRF', '391855').

card_in_set('humble defector', 'FRF').
card_original_type('humble defector'/'FRF', 'Creature — Human Rogue').
card_original_text('humble defector'/'FRF', '{T}: Draw two cards. Target opponent gains control of Humble Defector. Activate this ability only during your turn.').
card_first_print('humble defector', 'FRF').
card_image_name('humble defector'/'FRF', 'humble defector').
card_uid('humble defector'/'FRF', 'FRF:Humble Defector:humble defector').
card_rarity('humble defector'/'FRF', 'Uncommon').
card_artist('humble defector'/'FRF', 'Slawomir Maniak').
card_number('humble defector'/'FRF', '104').
card_flavor_text('humble defector'/'FRF', '\"You were once Mardu, so your body and will are strong. Now we must train your mind.\"\n—Houn, Jeskai elder').
card_multiverse_id('humble defector'/'FRF', '391856').
card_watermark('humble defector'/'FRF', 'Jeskai').

card_in_set('hungering yeti', 'FRF').
card_original_type('hungering yeti'/'FRF', 'Creature — Yeti').
card_original_text('hungering yeti'/'FRF', 'As long as you control a green or blue permanent, you may cast Hungering Yeti as though it had flash. (You may cast it any time you could cast an instant.)').
card_first_print('hungering yeti', 'FRF').
card_image_name('hungering yeti'/'FRF', 'hungering yeti').
card_uid('hungering yeti'/'FRF', 'FRF:Hungering Yeti:hungering yeti').
card_rarity('hungering yeti'/'FRF', 'Uncommon').
card_artist('hungering yeti'/'FRF', 'Tyler Jacobson').
card_number('hungering yeti'/'FRF', '105').
card_flavor_text('hungering yeti'/'FRF', 'No clan is as adept at saying \"go away\" as the Temur.').
card_multiverse_id('hungering yeti'/'FRF', '391857').
card_watermark('hungering yeti'/'FRF', 'Temur').

card_in_set('hunt the weak', 'FRF').
card_original_type('hunt the weak'/'FRF', 'Sorcery').
card_original_text('hunt the weak'/'FRF', 'Put a +1/+1 counter on target creature you control. Then that creature fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_image_name('hunt the weak'/'FRF', 'hunt the weak').
card_uid('hunt the weak'/'FRF', 'FRF:Hunt the Weak:hunt the weak').
card_rarity('hunt the weak'/'FRF', 'Common').
card_artist('hunt the weak'/'FRF', 'Lars Grant-West').
card_number('hunt the weak'/'FRF', '133').
card_flavor_text('hunt the weak'/'FRF', 'Dragons scorn the weak, even among their own kind.').
card_multiverse_id('hunt the weak'/'FRF', '391858').

card_in_set('island', 'FRF').
card_original_type('island'/'FRF', 'Basic Land — Island').
card_original_text('island'/'FRF', 'U').
card_image_name('island'/'FRF', 'island1').
card_uid('island'/'FRF', 'FRF:Island:island1').
card_rarity('island'/'FRF', 'Basic Land').
card_artist('island'/'FRF', 'Florian de Gesincourt').
card_number('island'/'FRF', '178').
card_multiverse_id('island'/'FRF', '391859').

card_in_set('island', 'FRF').
card_original_type('island'/'FRF', 'Basic Land — Island').
card_original_text('island'/'FRF', 'U').
card_image_name('island'/'FRF', 'island2').
card_uid('island'/'FRF', 'FRF:Island:island2').
card_rarity('island'/'FRF', 'Basic Land').
card_artist('island'/'FRF', 'Titus Lunter').
card_number('island'/'FRF', '179').
card_multiverse_id('island'/'FRF', '391860').

card_in_set('jeskai barricade', 'FRF').
card_original_type('jeskai barricade'/'FRF', 'Creature — Wall').
card_original_text('jeskai barricade'/'FRF', 'Flash (You may cast this spell any time you could cast an instant.)\nDefender\nWhen Jeskai Barricade enters the battlefield, you may return another target creature you control to its owner\'s hand.').
card_first_print('jeskai barricade', 'FRF').
card_image_name('jeskai barricade'/'FRF', 'jeskai barricade').
card_uid('jeskai barricade'/'FRF', 'FRF:Jeskai Barricade:jeskai barricade').
card_rarity('jeskai barricade'/'FRF', 'Uncommon').
card_artist('jeskai barricade'/'FRF', 'Noah Bradley').
card_number('jeskai barricade'/'FRF', '15').
card_multiverse_id('jeskai barricade'/'FRF', '391861').
card_watermark('jeskai barricade'/'FRF', 'Jeskai').

card_in_set('jeskai infiltrator', 'FRF').
card_original_type('jeskai infiltrator'/'FRF', 'Creature — Human Monk').
card_original_text('jeskai infiltrator'/'FRF', 'Jeskai Infiltrator can\'t be blocked as long as you control no other creatures.\nWhen Jeskai Infiltrator deals combat damage to a player, exile it and the top card of your library in a face-down pile, shuffle that pile, then manifest those cards. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('jeskai infiltrator'/'FRF', 'jeskai infiltrator').
card_uid('jeskai infiltrator'/'FRF', 'FRF:Jeskai Infiltrator:jeskai infiltrator').
card_rarity('jeskai infiltrator'/'FRF', 'Rare').
card_artist('jeskai infiltrator'/'FRF', 'Cynthia Sheppard').
card_number('jeskai infiltrator'/'FRF', '36').
card_multiverse_id('jeskai infiltrator'/'FRF', '391862').
card_watermark('jeskai infiltrator'/'FRF', 'Jeskai').

card_in_set('jeskai runemark', 'FRF').
card_original_type('jeskai runemark'/'FRF', 'Enchantment — Aura').
card_original_text('jeskai runemark'/'FRF', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has flying as long as you control a red or white permanent.').
card_first_print('jeskai runemark', 'FRF').
card_image_name('jeskai runemark'/'FRF', 'jeskai runemark').
card_uid('jeskai runemark'/'FRF', 'FRF:Jeskai Runemark:jeskai runemark').
card_rarity('jeskai runemark'/'FRF', 'Common').
card_artist('jeskai runemark'/'FRF', 'Jaime Jones').
card_number('jeskai runemark'/'FRF', '37').
card_multiverse_id('jeskai runemark'/'FRF', '391863').
card_watermark('jeskai runemark'/'FRF', 'Jeskai').

card_in_set('jeskai sage', 'FRF').
card_original_type('jeskai sage'/'FRF', 'Creature — Human Monk').
card_original_text('jeskai sage'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhen Jeskai Sage dies, draw a card.').
card_first_print('jeskai sage', 'FRF').
card_image_name('jeskai sage'/'FRF', 'jeskai sage').
card_uid('jeskai sage'/'FRF', 'FRF:Jeskai Sage:jeskai sage').
card_rarity('jeskai sage'/'FRF', 'Common').
card_artist('jeskai sage'/'FRF', 'Craig J Spearing').
card_number('jeskai sage'/'FRF', '38').
card_flavor_text('jeskai sage'/'FRF', '\"The one who conquers the mind is greater than the one who conquers the world.\"').
card_multiverse_id('jeskai sage'/'FRF', '391864').
card_watermark('jeskai sage'/'FRF', 'Jeskai').

card_in_set('jungle hollow', 'FRF').
card_original_type('jungle hollow'/'FRF', 'Land').
card_original_text('jungle hollow'/'FRF', 'Jungle Hollow enters the battlefield tapped.\nWhen Jungle Hollow enters the battlefield, you gain 1 life.\n{T}: Add {B} or {G} to your mana pool.').
card_image_name('jungle hollow'/'FRF', 'jungle hollow').
card_uid('jungle hollow'/'FRF', 'FRF:Jungle Hollow:jungle hollow').
card_rarity('jungle hollow'/'FRF', 'Common').
card_artist('jungle hollow'/'FRF', 'Eytan Zana').
card_number('jungle hollow'/'FRF', '169').
card_multiverse_id('jungle hollow'/'FRF', '391865').

card_in_set('kolaghan, the storm\'s fury', 'FRF').
card_original_type('kolaghan, the storm\'s fury'/'FRF', 'Legendary Creature — Dragon').
card_original_text('kolaghan, the storm\'s fury'/'FRF', 'Flying\nWhenever a Dragon you control attacks, creatures you control get +1/+0 until end of turn.\nDash {3}{B}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('kolaghan, the storm\'s fury', 'FRF').
card_image_name('kolaghan, the storm\'s fury'/'FRF', 'kolaghan, the storm\'s fury').
card_uid('kolaghan, the storm\'s fury'/'FRF', 'FRF:Kolaghan, the Storm\'s Fury:kolaghan, the storm\'s fury').
card_rarity('kolaghan, the storm\'s fury'/'FRF', 'Rare').
card_artist('kolaghan, the storm\'s fury'/'FRF', 'Jaime Jones').
card_number('kolaghan, the storm\'s fury'/'FRF', '155').
card_multiverse_id('kolaghan, the storm\'s fury'/'FRF', '391866').
card_watermark('kolaghan, the storm\'s fury'/'FRF', 'Kolaghan').

card_in_set('lightform', 'FRF').
card_original_type('lightform'/'FRF', 'Enchantment').
card_original_text('lightform'/'FRF', 'When Lightform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Lightform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has flying and lifelink.').
card_first_print('lightform', 'FRF').
card_image_name('lightform'/'FRF', 'lightform').
card_uid('lightform'/'FRF', 'FRF:Lightform:lightform').
card_rarity('lightform'/'FRF', 'Uncommon').
card_artist('lightform'/'FRF', 'Steve Prescott').
card_number('lightform'/'FRF', '16').
card_multiverse_id('lightform'/'FRF', '391867').

card_in_set('lightning shrieker', 'FRF').
card_original_type('lightning shrieker'/'FRF', 'Creature — Dragon').
card_original_text('lightning shrieker'/'FRF', 'Flying, trample, haste\nAt the beginning of the end step, Lightning Shrieker\'s owner shuffles it into his or her library.').
card_first_print('lightning shrieker', 'FRF').
card_image_name('lightning shrieker'/'FRF', 'lightning shrieker').
card_uid('lightning shrieker'/'FRF', 'FRF:Lightning Shrieker:lightning shrieker').
card_rarity('lightning shrieker'/'FRF', 'Common').
card_artist('lightning shrieker'/'FRF', 'Slawomir Maniak').
card_number('lightning shrieker'/'FRF', '106').
card_flavor_text('lightning shrieker'/'FRF', 'Dragonslayers learned to keep silent about their deeds after seeing the terrible vengeance wrought by Kolaghan and her brood.').
card_multiverse_id('lightning shrieker'/'FRF', '391868').
card_watermark('lightning shrieker'/'FRF', 'Kolaghan').

card_in_set('lotus path djinn', 'FRF').
card_original_type('lotus path djinn'/'FRF', 'Creature — Djinn Monk').
card_original_text('lotus path djinn'/'FRF', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('lotus path djinn', 'FRF').
card_image_name('lotus path djinn'/'FRF', 'lotus path djinn').
card_uid('lotus path djinn'/'FRF', 'FRF:Lotus Path Djinn:lotus path djinn').
card_rarity('lotus path djinn'/'FRF', 'Common').
card_artist('lotus path djinn'/'FRF', 'Steve Argyle').
card_number('lotus path djinn'/'FRF', '39').
card_flavor_text('lotus path djinn'/'FRF', '\"The lotus takes root where body and mind intersect. It blooms when body and mind become one.\"').
card_multiverse_id('lotus path djinn'/'FRF', '391869').
card_watermark('lotus path djinn'/'FRF', 'Jeskai').

card_in_set('lotus-eye mystics', 'FRF').
card_original_type('lotus-eye mystics'/'FRF', 'Creature — Human Monk').
card_original_text('lotus-eye mystics'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhen Lotus-Eye Mystics enters the battlefield, return target enchantment card from your graveyard to your hand.').
card_first_print('lotus-eye mystics', 'FRF').
card_image_name('lotus-eye mystics'/'FRF', 'lotus-eye mystics').
card_uid('lotus-eye mystics'/'FRF', 'FRF:Lotus-Eye Mystics:lotus-eye mystics').
card_rarity('lotus-eye mystics'/'FRF', 'Uncommon').
card_artist('lotus-eye mystics'/'FRF', 'Dan Scott').
card_number('lotus-eye mystics'/'FRF', '17').
card_flavor_text('lotus-eye mystics'/'FRF', 'Every action has a foreseen purpose.').
card_multiverse_id('lotus-eye mystics'/'FRF', '391870').
card_watermark('lotus-eye mystics'/'FRF', 'Jeskai').

card_in_set('map the wastes', 'FRF').
card_original_type('map the wastes'/'FRF', 'Sorcery').
card_original_text('map the wastes'/'FRF', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('map the wastes', 'FRF').
card_image_name('map the wastes'/'FRF', 'map the wastes').
card_uid('map the wastes'/'FRF', 'FRF:Map the Wastes:map the wastes').
card_rarity('map the wastes'/'FRF', 'Common').
card_artist('map the wastes'/'FRF', 'Volkan Baga').
card_number('map the wastes'/'FRF', '134').
card_multiverse_id('map the wastes'/'FRF', '391871').
card_watermark('map the wastes'/'FRF', 'Abzan').

card_in_set('marang river prowler', 'FRF').
card_original_type('marang river prowler'/'FRF', 'Creature — Human Rogue').
card_original_text('marang river prowler'/'FRF', 'Marang River Prowler can\'t block and can\'t be blocked.\nYou may cast Marang River Prowler from your graveyard as long as you control a black or green permanent.').
card_first_print('marang river prowler', 'FRF').
card_image_name('marang river prowler'/'FRF', 'marang river prowler').
card_uid('marang river prowler'/'FRF', 'FRF:Marang River Prowler:marang river prowler').
card_rarity('marang river prowler'/'FRF', 'Uncommon').
card_artist('marang river prowler'/'FRF', 'Yefim Kligerman').
card_number('marang river prowler'/'FRF', '40').
card_flavor_text('marang river prowler'/'FRF', 'The currents of the Marang wash away both tracks and blood.').
card_multiverse_id('marang river prowler'/'FRF', '391872').
card_watermark('marang river prowler'/'FRF', 'Sultai').

card_in_set('mardu runemark', 'FRF').
card_original_type('mardu runemark'/'FRF', 'Enchantment — Aura').
card_original_text('mardu runemark'/'FRF', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has first strike as long as you control a white or black permanent.').
card_first_print('mardu runemark', 'FRF').
card_image_name('mardu runemark'/'FRF', 'mardu runemark').
card_uid('mardu runemark'/'FRF', 'FRF:Mardu Runemark:mardu runemark').
card_rarity('mardu runemark'/'FRF', 'Common').
card_artist('mardu runemark'/'FRF', 'Viktor Titov').
card_number('mardu runemark'/'FRF', '107').
card_multiverse_id('mardu runemark'/'FRF', '391873').
card_watermark('mardu runemark'/'FRF', 'Mardu').

card_in_set('mardu scout', 'FRF').
card_original_type('mardu scout'/'FRF', 'Creature — Goblin Scout').
card_original_text('mardu scout'/'FRF', 'Dash {1}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('mardu scout', 'FRF').
card_image_name('mardu scout'/'FRF', 'mardu scout').
card_uid('mardu scout'/'FRF', 'FRF:Mardu Scout:mardu scout').
card_rarity('mardu scout'/'FRF', 'Common').
card_artist('mardu scout'/'FRF', 'Zoltan Boros').
card_number('mardu scout'/'FRF', '108').
card_flavor_text('mardu scout'/'FRF', 'The Mardu all enjoy war, but only the goblins make a game of it.').
card_multiverse_id('mardu scout'/'FRF', '391874').
card_watermark('mardu scout'/'FRF', 'Mardu').

card_in_set('mardu shadowspear', 'FRF').
card_original_type('mardu shadowspear'/'FRF', 'Creature — Human Warrior').
card_original_text('mardu shadowspear'/'FRF', 'Whenever Mardu Shadowspear attacks, each opponent loses 1 life.\nDash {1}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_image_name('mardu shadowspear'/'FRF', 'mardu shadowspear').
card_uid('mardu shadowspear'/'FRF', 'FRF:Mardu Shadowspear:mardu shadowspear').
card_rarity('mardu shadowspear'/'FRF', 'Uncommon').
card_artist('mardu shadowspear'/'FRF', 'Mark Poole').
card_number('mardu shadowspear'/'FRF', '74').
card_multiverse_id('mardu shadowspear'/'FRF', '391875').
card_watermark('mardu shadowspear'/'FRF', 'Mardu').

card_in_set('mardu strike leader', 'FRF').
card_original_type('mardu strike leader'/'FRF', 'Creature — Human Warrior').
card_original_text('mardu strike leader'/'FRF', 'Whenever Mardu Strike Leader attacks, put a 2/1 black Warrior creature token onto the battlefield.\nDash {3}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('mardu strike leader', 'FRF').
card_image_name('mardu strike leader'/'FRF', 'mardu strike leader').
card_uid('mardu strike leader'/'FRF', 'FRF:Mardu Strike Leader:mardu strike leader').
card_rarity('mardu strike leader'/'FRF', 'Rare').
card_artist('mardu strike leader'/'FRF', 'Jason Rainville').
card_number('mardu strike leader'/'FRF', '75').
card_multiverse_id('mardu strike leader'/'FRF', '391876').
card_watermark('mardu strike leader'/'FRF', 'Mardu').

card_in_set('mardu woe-reaper', 'FRF').
card_original_type('mardu woe-reaper'/'FRF', 'Creature — Human Warrior').
card_original_text('mardu woe-reaper'/'FRF', 'Whenever Mardu Woe-Reaper or another Warrior enters the battlefield under your control, you may exile target creature card from a graveyard. If you do, you gain 1 life.').
card_first_print('mardu woe-reaper', 'FRF').
card_image_name('mardu woe-reaper'/'FRF', 'mardu woe-reaper').
card_uid('mardu woe-reaper'/'FRF', 'FRF:Mardu Woe-Reaper:mardu woe-reaper').
card_rarity('mardu woe-reaper'/'FRF', 'Uncommon').
card_artist('mardu woe-reaper'/'FRF', 'Willian Murai').
card_number('mardu woe-reaper'/'FRF', '18').
card_flavor_text('mardu woe-reaper'/'FRF', '\"Those who died in battle deserve their rest.\"').
card_multiverse_id('mardu woe-reaper'/'FRF', '391877').
card_watermark('mardu woe-reaper'/'FRF', 'Mardu').

card_in_set('mastery of the unseen', 'FRF').
card_original_type('mastery of the unseen'/'FRF', 'Enchantment').
card_original_text('mastery of the unseen'/'FRF', 'Whenever a permanent you control is turned face up, you gain 1 life for each creature you control.\n{3}{W}: Manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('mastery of the unseen'/'FRF', 'mastery of the unseen').
card_uid('mastery of the unseen'/'FRF', 'FRF:Mastery of the Unseen:mastery of the unseen').
card_rarity('mastery of the unseen'/'FRF', 'Rare').
card_artist('mastery of the unseen'/'FRF', 'Daniel Ljunggren').
card_number('mastery of the unseen'/'FRF', '19').
card_multiverse_id('mastery of the unseen'/'FRF', '391878').

card_in_set('merciless executioner', 'FRF').
card_original_type('merciless executioner'/'FRF', 'Creature — Orc Warrior').
card_original_text('merciless executioner'/'FRF', 'When Merciless Executioner enters the battlefield, each player sacrifices a creature.').
card_first_print('merciless executioner', 'FRF').
card_image_name('merciless executioner'/'FRF', 'merciless executioner').
card_uid('merciless executioner'/'FRF', 'FRF:Merciless Executioner:merciless executioner').
card_rarity('merciless executioner'/'FRF', 'Uncommon').
card_artist('merciless executioner'/'FRF', 'David Palumbo').
card_number('merciless executioner'/'FRF', '76').
card_flavor_text('merciless executioner'/'FRF', 'He enjoys his work, for he sees only the worst Abzan criminals: those who betray their own kin.').
card_multiverse_id('merciless executioner'/'FRF', '391879').
card_watermark('merciless executioner'/'FRF', 'Abzan').

card_in_set('mindscour dragon', 'FRF').
card_original_type('mindscour dragon'/'FRF', 'Creature — Dragon').
card_original_text('mindscour dragon'/'FRF', 'Flying\nWhenever Mindscour Dragon deals combat damage to an opponent, target player puts the top four cards of his or her library into his or her graveyard.').
card_first_print('mindscour dragon', 'FRF').
card_image_name('mindscour dragon'/'FRF', 'mindscour dragon').
card_uid('mindscour dragon'/'FRF', 'FRF:Mindscour Dragon:mindscour dragon').
card_rarity('mindscour dragon'/'FRF', 'Uncommon').
card_artist('mindscour dragon'/'FRF', 'Wesley Burt').
card_number('mindscour dragon'/'FRF', '41').
card_flavor_text('mindscour dragon'/'FRF', '\"The bells may one day ring as an anthem of our own end.\"\n—Kyu, Sage-Eye mystic').
card_multiverse_id('mindscour dragon'/'FRF', '391880').
card_watermark('mindscour dragon'/'FRF', 'Ojutai').

card_in_set('mistfire adept', 'FRF').
card_original_type('mistfire adept'/'FRF', 'Creature — Human Monk').
card_original_text('mistfire adept'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, target creature gains flying until end of turn.').
card_first_print('mistfire adept', 'FRF').
card_image_name('mistfire adept'/'FRF', 'mistfire adept').
card_uid('mistfire adept'/'FRF', 'FRF:Mistfire Adept:mistfire adept').
card_rarity('mistfire adept'/'FRF', 'Uncommon').
card_artist('mistfire adept'/'FRF', 'Clint Cearley').
card_number('mistfire adept'/'FRF', '42').
card_flavor_text('mistfire adept'/'FRF', '\"The quieter the skies, the calmer the mind.\"').
card_multiverse_id('mistfire adept'/'FRF', '391881').
card_watermark('mistfire adept'/'FRF', 'Jeskai').

card_in_set('mob rule', 'FRF').
card_original_type('mob rule'/'FRF', 'Sorcery').
card_original_text('mob rule'/'FRF', 'Choose one —\n• Gain control of all creatures with power 4 or greater until end of turn. Untap those creatures. They gain haste until end of turn.\n• Gain control of all creatures with power 3 or less until end of turn. Untap those creatures. They gain haste until end of turn.').
card_first_print('mob rule', 'FRF').
card_image_name('mob rule'/'FRF', 'mob rule').
card_uid('mob rule'/'FRF', 'FRF:Mob Rule:mob rule').
card_rarity('mob rule'/'FRF', 'Rare').
card_artist('mob rule'/'FRF', 'Jakub Kasper').
card_number('mob rule'/'FRF', '109').
card_multiverse_id('mob rule'/'FRF', '391882').

card_in_set('monastery mentor', 'FRF').
card_original_type('monastery mentor'/'FRF', 'Creature — Human Monk').
card_original_text('monastery mentor'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, put a 1/1 white Monk creature token with prowess onto the battlefield.').
card_first_print('monastery mentor', 'FRF').
card_image_name('monastery mentor'/'FRF', 'monastery mentor').
card_uid('monastery mentor'/'FRF', 'FRF:Monastery Mentor:monastery mentor').
card_rarity('monastery mentor'/'FRF', 'Mythic Rare').
card_artist('monastery mentor'/'FRF', 'Magali Villeneuve').
card_number('monastery mentor'/'FRF', '20').
card_flavor_text('monastery mentor'/'FRF', '\"Speak little. Do much.\"').
card_multiverse_id('monastery mentor'/'FRF', '391883').
card_watermark('monastery mentor'/'FRF', 'Jeskai').

card_in_set('monastery siege', 'FRF').
card_original_type('monastery siege'/'FRF', 'Enchantment').
card_original_text('monastery siege'/'FRF', 'As Monastery Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your draw step, draw an additional card, then discard a card.\n• Dragons — Spells your opponents cast that target you or a permanent you control cost {2} more to cast.').
card_first_print('monastery siege', 'FRF').
card_image_name('monastery siege'/'FRF', 'monastery siege').
card_uid('monastery siege'/'FRF', 'FRF:Monastery Siege:monastery siege').
card_rarity('monastery siege'/'FRF', 'Rare').
card_artist('monastery siege'/'FRF', 'Mark Winters').
card_number('monastery siege'/'FRF', '43').
card_multiverse_id('monastery siege'/'FRF', '391884').

card_in_set('mountain', 'FRF').
card_original_type('mountain'/'FRF', 'Basic Land — Mountain').
card_original_text('mountain'/'FRF', 'R').
card_image_name('mountain'/'FRF', 'mountain1').
card_uid('mountain'/'FRF', 'FRF:Mountain:mountain1').
card_rarity('mountain'/'FRF', 'Basic Land').
card_artist('mountain'/'FRF', 'Noah Bradley').
card_number('mountain'/'FRF', '182').
card_multiverse_id('mountain'/'FRF', '391885').

card_in_set('mountain', 'FRF').
card_original_type('mountain'/'FRF', 'Basic Land — Mountain').
card_original_text('mountain'/'FRF', 'R').
card_image_name('mountain'/'FRF', 'mountain2').
card_uid('mountain'/'FRF', 'FRF:Mountain:mountain2').
card_rarity('mountain'/'FRF', 'Basic Land').
card_artist('mountain'/'FRF', 'Florian de Gesincourt').
card_number('mountain'/'FRF', '183').
card_multiverse_id('mountain'/'FRF', '391886').

card_in_set('neutralizing blast', 'FRF').
card_original_type('neutralizing blast'/'FRF', 'Instant').
card_original_text('neutralizing blast'/'FRF', 'Counter target multicolored spell.').
card_first_print('neutralizing blast', 'FRF').
card_image_name('neutralizing blast'/'FRF', 'neutralizing blast').
card_uid('neutralizing blast'/'FRF', 'FRF:Neutralizing Blast:neutralizing blast').
card_rarity('neutralizing blast'/'FRF', 'Uncommon').
card_artist('neutralizing blast'/'FRF', 'Dan Scott').
card_number('neutralizing blast'/'FRF', '44').
card_flavor_text('neutralizing blast'/'FRF', '\"To defeat your enemies, you must remove their ability to make war against you.\"').
card_multiverse_id('neutralizing blast'/'FRF', '391887').

card_in_set('noxious dragon', 'FRF').
card_original_type('noxious dragon'/'FRF', 'Creature — Dragon').
card_original_text('noxious dragon'/'FRF', 'Flying\nWhen Noxious Dragon dies, you may destroy target creature with converted mana cost 3 or less.').
card_first_print('noxious dragon', 'FRF').
card_image_name('noxious dragon'/'FRF', 'noxious dragon').
card_uid('noxious dragon'/'FRF', 'FRF:Noxious Dragon:noxious dragon').
card_rarity('noxious dragon'/'FRF', 'Uncommon').
card_artist('noxious dragon'/'FRF', 'Svetlin Velinov').
card_number('noxious dragon'/'FRF', '77').
card_flavor_text('noxious dragon'/'FRF', 'Dragons of Silumgar\'s brood begin to digest their prey even before consuming it, letting their caustic breath do much of the work.').
card_multiverse_id('noxious dragon'/'FRF', '391888').
card_watermark('noxious dragon'/'FRF', 'Silumgar').

card_in_set('ojutai, soul of winter', 'FRF').
card_original_type('ojutai, soul of winter'/'FRF', 'Legendary Creature — Dragon').
card_original_text('ojutai, soul of winter'/'FRF', 'Flying, vigilance\nWhenever a Dragon you control attacks, tap target nonland permanent an opponent controls. That permanent doesn\'t untap during its controller\'s next untap step.').
card_first_print('ojutai, soul of winter', 'FRF').
card_image_name('ojutai, soul of winter'/'FRF', 'ojutai, soul of winter').
card_uid('ojutai, soul of winter'/'FRF', 'FRF:Ojutai, Soul of Winter:ojutai, soul of winter').
card_rarity('ojutai, soul of winter'/'FRF', 'Rare').
card_artist('ojutai, soul of winter'/'FRF', 'Chase Stone').
card_number('ojutai, soul of winter'/'FRF', '156').
card_flavor_text('ojutai, soul of winter'/'FRF', '\"When I battled Ojutai, I realized my learning had only begun.\"\n—Shu Yun, the Silent Tempest').
card_multiverse_id('ojutai, soul of winter'/'FRF', '391889').
card_watermark('ojutai, soul of winter'/'FRF', 'Ojutai').

card_in_set('orc sureshot', 'FRF').
card_original_type('orc sureshot'/'FRF', 'Creature — Orc Archer').
card_original_text('orc sureshot'/'FRF', 'Whenever another creature enters the battlefield under your control, target creature an opponent controls gets -1/-1 until end of turn.').
card_first_print('orc sureshot', 'FRF').
card_image_name('orc sureshot'/'FRF', 'orc sureshot').
card_uid('orc sureshot'/'FRF', 'FRF:Orc Sureshot:orc sureshot').
card_rarity('orc sureshot'/'FRF', 'Uncommon').
card_artist('orc sureshot'/'FRF', 'Kev Walker').
card_number('orc sureshot'/'FRF', '78').
card_flavor_text('orc sureshot'/'FRF', '\"Words have yet to halt an arrow.\"').
card_multiverse_id('orc sureshot'/'FRF', '391890').
card_watermark('orc sureshot'/'FRF', 'Mardu').

card_in_set('outpost siege', 'FRF').
card_original_type('outpost siege'/'FRF', 'Enchantment').
card_original_text('outpost siege'/'FRF', 'As Outpost Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your upkeep, exile the top card of your library. Until end of turn, you may play that card.\n• Dragons — Whenever a creature you control leaves the battlefield, Outpost Siege deals 1 damage to target creature or player.').
card_first_print('outpost siege', 'FRF').
card_image_name('outpost siege'/'FRF', 'outpost siege').
card_uid('outpost siege'/'FRF', 'FRF:Outpost Siege:outpost siege').
card_rarity('outpost siege'/'FRF', 'Rare').
card_artist('outpost siege'/'FRF', 'Daarken').
card_number('outpost siege'/'FRF', '110').
card_multiverse_id('outpost siege'/'FRF', '391891').

card_in_set('palace siege', 'FRF').
card_original_type('palace siege'/'FRF', 'Enchantment').
card_original_text('palace siege'/'FRF', 'As Palace Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your upkeep, return target creature card from your graveyard to your hand.\n• Dragons — At the beginning of your upkeep, each opponent loses 2 life and you gain 2 life.').
card_first_print('palace siege', 'FRF').
card_image_name('palace siege'/'FRF', 'palace siege').
card_uid('palace siege'/'FRF', 'FRF:Palace Siege:palace siege').
card_rarity('palace siege'/'FRF', 'Rare').
card_artist('palace siege'/'FRF', 'Slawomir Maniak').
card_number('palace siege'/'FRF', '79').
card_multiverse_id('palace siege'/'FRF', '391892').

card_in_set('pilgrim of the fires', 'FRF').
card_original_type('pilgrim of the fires'/'FRF', 'Artifact Creature — Golem').
card_original_text('pilgrim of the fires'/'FRF', 'First strike, trample').
card_first_print('pilgrim of the fires', 'FRF').
card_image_name('pilgrim of the fires'/'FRF', 'pilgrim of the fires').
card_uid('pilgrim of the fires'/'FRF', 'FRF:Pilgrim of the Fires:pilgrim of the fires').
card_rarity('pilgrim of the fires'/'FRF', 'Uncommon').
card_artist('pilgrim of the fires'/'FRF', 'Izzy').
card_number('pilgrim of the fires'/'FRF', '162').
card_flavor_text('pilgrim of the fires'/'FRF', 'The most skilled Jeskai artisans can craft imposing golems with such precision that they will last an eternity—the time believed necessary to master the Fires of the Jeskai Way.').
card_multiverse_id('pilgrim of the fires'/'FRF', '391893').

card_in_set('plains', 'FRF').
card_original_type('plains'/'FRF', 'Basic Land — Plains').
card_original_text('plains'/'FRF', 'W').
card_image_name('plains'/'FRF', 'plains1').
card_uid('plains'/'FRF', 'FRF:Plains:plains1').
card_rarity('plains'/'FRF', 'Basic Land').
card_artist('plains'/'FRF', 'Noah Bradley').
card_number('plains'/'FRF', '176').
card_multiverse_id('plains'/'FRF', '391895').

card_in_set('plains', 'FRF').
card_original_type('plains'/'FRF', 'Basic Land — Plains').
card_original_text('plains'/'FRF', 'W').
card_image_name('plains'/'FRF', 'plains2').
card_uid('plains'/'FRF', 'FRF:Plains:plains2').
card_rarity('plains'/'FRF', 'Basic Land').
card_artist('plains'/'FRF', 'Sam Burley').
card_number('plains'/'FRF', '177').
card_multiverse_id('plains'/'FRF', '391894').

card_in_set('pressure point', 'FRF').
card_original_type('pressure point'/'FRF', 'Instant').
card_original_text('pressure point'/'FRF', 'Tap target creature.\nDraw a card.').
card_first_print('pressure point', 'FRF').
card_image_name('pressure point'/'FRF', 'pressure point').
card_uid('pressure point'/'FRF', 'FRF:Pressure Point:pressure point').
card_rarity('pressure point'/'FRF', 'Common').
card_artist('pressure point'/'FRF', 'Chase Stone').
card_number('pressure point'/'FRF', '21').
card_flavor_text('pressure point'/'FRF', 'By studying dragon anatomy, Jeskai monks have learned how to locate clusters of nerves that will incapacitate even the mightiest dragons.').
card_multiverse_id('pressure point'/'FRF', '391896').

card_in_set('pyrotechnics', 'FRF').
card_original_type('pyrotechnics'/'FRF', 'Sorcery').
card_original_text('pyrotechnics'/'FRF', 'Pyrotechnics deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'FRF', 'pyrotechnics').
card_uid('pyrotechnics'/'FRF', 'FRF:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'FRF', 'Uncommon').
card_artist('pyrotechnics'/'FRF', 'Matt Stewart').
card_number('pyrotechnics'/'FRF', '111').
card_flavor_text('pyrotechnics'/'FRF', '\"Take inspiration from your enemies, and make their strengths your own.\"\n—Alesha, Who Smiles at Death').
card_multiverse_id('pyrotechnics'/'FRF', '391897').

card_in_set('qarsi high priest', 'FRF').
card_original_type('qarsi high priest'/'FRF', 'Creature — Human Cleric').
card_original_text('qarsi high priest'/'FRF', '{1}{B}, {T}, Sacrifice another creature: Manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_first_print('qarsi high priest', 'FRF').
card_image_name('qarsi high priest'/'FRF', 'qarsi high priest').
card_uid('qarsi high priest'/'FRF', 'FRF:Qarsi High Priest:qarsi high priest').
card_rarity('qarsi high priest'/'FRF', 'Uncommon').
card_artist('qarsi high priest'/'FRF', 'Viktor Titov').
card_number('qarsi high priest'/'FRF', '80').
card_flavor_text('qarsi high priest'/'FRF', '\"Hear me, nameless, and obey.\"').
card_multiverse_id('qarsi high priest'/'FRF', '391898').
card_watermark('qarsi high priest'/'FRF', 'Sultai').

card_in_set('rageform', 'FRF').
card_original_type('rageform'/'FRF', 'Enchantment').
card_original_text('rageform'/'FRF', 'When Rageform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Rageform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_first_print('rageform', 'FRF').
card_image_name('rageform'/'FRF', 'rageform').
card_uid('rageform'/'FRF', 'FRF:Rageform:rageform').
card_rarity('rageform'/'FRF', 'Uncommon').
card_artist('rageform'/'FRF', 'Richard Wright').
card_number('rageform'/'FRF', '112').
card_multiverse_id('rageform'/'FRF', '391899').

card_in_set('rakshasa\'s disdain', 'FRF').
card_original_type('rakshasa\'s disdain'/'FRF', 'Instant').
card_original_text('rakshasa\'s disdain'/'FRF', 'Counter target spell unless its controller pays {1} for each card in your graveyard.').
card_first_print('rakshasa\'s disdain', 'FRF').
card_image_name('rakshasa\'s disdain'/'FRF', 'rakshasa\'s disdain').
card_uid('rakshasa\'s disdain'/'FRF', 'FRF:Rakshasa\'s Disdain:rakshasa\'s disdain').
card_rarity('rakshasa\'s disdain'/'FRF', 'Common').
card_artist('rakshasa\'s disdain'/'FRF', 'Seb McKinnon').
card_number('rakshasa\'s disdain'/'FRF', '45').
card_flavor_text('rakshasa\'s disdain'/'FRF', '\"Every failure, every death only makes my strength greater.\"').
card_multiverse_id('rakshasa\'s disdain'/'FRF', '391900').

card_in_set('rally the ancestors', 'FRF').
card_original_type('rally the ancestors'/'FRF', 'Instant').
card_original_text('rally the ancestors'/'FRF', 'Return each creature card with converted mana cost X or less from your graveyard to the battlefield. Exile those creatures at the beginning of your next upkeep. Exile Rally the Ancestors.').
card_first_print('rally the ancestors', 'FRF').
card_image_name('rally the ancestors'/'FRF', 'rally the ancestors').
card_uid('rally the ancestors'/'FRF', 'FRF:Rally the Ancestors:rally the ancestors').
card_rarity('rally the ancestors'/'FRF', 'Rare').
card_artist('rally the ancestors'/'FRF', 'Nils Hamm').
card_number('rally the ancestors'/'FRF', '22').
card_flavor_text('rally the ancestors'/'FRF', '\"The family is a tree, and in times of need, every branch can be a weapon.\"\n—Daghatar the Adamant').
card_multiverse_id('rally the ancestors'/'FRF', '391901').

card_in_set('reach of shadows', 'FRF').
card_original_type('reach of shadows'/'FRF', 'Instant').
card_original_text('reach of shadows'/'FRF', 'Destroy target creature that\'s one or more colors.').
card_first_print('reach of shadows', 'FRF').
card_image_name('reach of shadows'/'FRF', 'reach of shadows').
card_uid('reach of shadows'/'FRF', 'FRF:Reach of Shadows:reach of shadows').
card_rarity('reach of shadows'/'FRF', 'Common').
card_artist('reach of shadows'/'FRF', 'Daarken').
card_number('reach of shadows'/'FRF', '81').
card_flavor_text('reach of shadows'/'FRF', '\"Is it better to be a fool or a coward? Fools may get things done occasionally, but cowards live longer.\"\n—Ghelesh Shadowmane, rakshasa mage').
card_multiverse_id('reach of shadows'/'FRF', '391902').

card_in_set('reality shift', 'FRF').
card_original_type('reality shift'/'FRF', 'Instant').
card_original_text('reality shift'/'FRF', 'Exile target creature. Its controller manifests the top card of his or her library. (That player puts the top card of his or her library onto the battlefield face down as a 2/2 creature. If it\'s a creature card, it can be turned face up any time for its mana cost.)').
card_image_name('reality shift'/'FRF', 'reality shift').
card_uid('reality shift'/'FRF', 'FRF:Reality Shift:reality shift').
card_rarity('reality shift'/'FRF', 'Uncommon').
card_artist('reality shift'/'FRF', 'Howard Lyon').
card_number('reality shift'/'FRF', '46').
card_multiverse_id('reality shift'/'FRF', '391903').

card_in_set('refocus', 'FRF').
card_original_type('refocus'/'FRF', 'Instant').
card_original_text('refocus'/'FRF', 'Untap target creature.\nDraw a card.').
card_first_print('refocus', 'FRF').
card_image_name('refocus'/'FRF', 'refocus').
card_uid('refocus'/'FRF', 'FRF:Refocus:refocus').
card_rarity('refocus'/'FRF', 'Common').
card_artist('refocus'/'FRF', 'Kev Walker').
card_number('refocus'/'FRF', '47').
card_flavor_text('refocus'/'FRF', '\"Before you can open your third eye, you must prove you can open the first two.\"').
card_multiverse_id('refocus'/'FRF', '391904').

card_in_set('renowned weaponsmith', 'FRF').
card_original_type('renowned weaponsmith'/'FRF', 'Creature — Human Artificer').
card_original_text('renowned weaponsmith'/'FRF', '{T}: Add {2} to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.\n{U}, {T}: Search your library for a card named Heart-Piercer Bow or Vial of Dragonfire, reveal it, put it into your hand, then shuffle your library.').
card_first_print('renowned weaponsmith', 'FRF').
card_image_name('renowned weaponsmith'/'FRF', 'renowned weaponsmith').
card_uid('renowned weaponsmith'/'FRF', 'FRF:Renowned Weaponsmith:renowned weaponsmith').
card_rarity('renowned weaponsmith'/'FRF', 'Uncommon').
card_artist('renowned weaponsmith'/'FRF', 'Eric Deschamps').
card_number('renowned weaponsmith'/'FRF', '48').
card_multiverse_id('renowned weaponsmith'/'FRF', '391905').

card_in_set('return to the earth', 'FRF').
card_original_type('return to the earth'/'FRF', 'Instant').
card_original_text('return to the earth'/'FRF', 'Destroy target artifact, enchantment, or creature with flying.').
card_first_print('return to the earth', 'FRF').
card_image_name('return to the earth'/'FRF', 'return to the earth').
card_uid('return to the earth'/'FRF', 'FRF:Return to the Earth:return to the earth').
card_rarity('return to the earth'/'FRF', 'Common').
card_artist('return to the earth'/'FRF', 'Mark Winters').
card_number('return to the earth'/'FRF', '135').
card_flavor_text('return to the earth'/'FRF', '\"Dragons are feared for their breath and their bite, but it is their wings that make them unconquerable. Bring them to earth, and they die like any other beast.\"\n—Kassur, Sultai hunter').
card_multiverse_id('return to the earth'/'FRF', '391906').

card_in_set('rite of undoing', 'FRF').
card_original_type('rite of undoing'/'FRF', 'Instant').
card_original_text('rite of undoing'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nReturn target nonland permanent you control and target nonland permanent you don\'t control to their owners\' hands.').
card_first_print('rite of undoing', 'FRF').
card_image_name('rite of undoing'/'FRF', 'rite of undoing').
card_uid('rite of undoing'/'FRF', 'FRF:Rite of Undoing:rite of undoing').
card_rarity('rite of undoing'/'FRF', 'Uncommon').
card_artist('rite of undoing'/'FRF', 'Anastasia Ovchinnikova').
card_number('rite of undoing'/'FRF', '49').
card_multiverse_id('rite of undoing'/'FRF', '391907').
card_watermark('rite of undoing'/'FRF', 'Sultai').

card_in_set('rugged highlands', 'FRF').
card_original_type('rugged highlands'/'FRF', 'Land').
card_original_text('rugged highlands'/'FRF', 'Rugged Highlands enters the battlefield tapped.\nWhen Rugged Highlands enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('rugged highlands'/'FRF', 'rugged highlands').
card_uid('rugged highlands'/'FRF', 'FRF:Rugged Highlands:rugged highlands').
card_rarity('rugged highlands'/'FRF', 'Common').
card_artist('rugged highlands'/'FRF', 'Eytan Zana').
card_number('rugged highlands'/'FRF', '170').
card_multiverse_id('rugged highlands'/'FRF', '391908').

card_in_set('ruthless instincts', 'FRF').
card_original_type('ruthless instincts'/'FRF', 'Instant').
card_original_text('ruthless instincts'/'FRF', 'Choose one —\n• Target nonattacking creature gains reach and deathtouch until end of turn. Untap it.\n• Target attacking creature gets +2/+2 and gains trample until end of turn.').
card_first_print('ruthless instincts', 'FRF').
card_image_name('ruthless instincts'/'FRF', 'ruthless instincts').
card_uid('ruthless instincts'/'FRF', 'FRF:Ruthless Instincts:ruthless instincts').
card_rarity('ruthless instincts'/'FRF', 'Uncommon').
card_artist('ruthless instincts'/'FRF', 'Lius Lasahido').
card_number('ruthless instincts'/'FRF', '136').
card_flavor_text('ruthless instincts'/'FRF', 'A single battle affects countless others.').
card_multiverse_id('ruthless instincts'/'FRF', '391909').

card_in_set('sage\'s reverie', 'FRF').
card_original_type('sage\'s reverie'/'FRF', 'Enchantment — Aura').
card_original_text('sage\'s reverie'/'FRF', 'Enchant creature\nWhen Sage\'s Reverie enters the battlefield, draw a card for each Aura you control that\'s attached to a creature.\nEnchanted creature gets +1/+1 for each Aura you control that\'s attached to a creature.').
card_first_print('sage\'s reverie', 'FRF').
card_image_name('sage\'s reverie'/'FRF', 'sage\'s reverie').
card_uid('sage\'s reverie'/'FRF', 'FRF:Sage\'s Reverie:sage\'s reverie').
card_rarity('sage\'s reverie'/'FRF', 'Uncommon').
card_artist('sage\'s reverie'/'FRF', 'Jason Rainville').
card_number('sage\'s reverie'/'FRF', '23').
card_multiverse_id('sage\'s reverie'/'FRF', '391910').

card_in_set('sage-eye avengers', 'FRF').
card_original_type('sage-eye avengers'/'FRF', 'Creature — Djinn Monk').
card_original_text('sage-eye avengers'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever Sage-Eye Avengers attacks, you may return target creature to its owner\'s hand if its power is less than Sage-Eye Avengers\'s power.').
card_image_name('sage-eye avengers'/'FRF', 'sage-eye avengers').
card_uid('sage-eye avengers'/'FRF', 'FRF:Sage-Eye Avengers:sage-eye avengers').
card_rarity('sage-eye avengers'/'FRF', 'Rare').
card_artist('sage-eye avengers'/'FRF', 'Dan Scott').
card_number('sage-eye avengers'/'FRF', '50').
card_multiverse_id('sage-eye avengers'/'FRF', '391911').
card_watermark('sage-eye avengers'/'FRF', 'Jeskai').

card_in_set('sandblast', 'FRF').
card_original_type('sandblast'/'FRF', 'Instant').
card_original_text('sandblast'/'FRF', 'Sandblast deals 5 damage to target attacking or blocking creature.').
card_first_print('sandblast', 'FRF').
card_image_name('sandblast'/'FRF', 'sandblast').
card_uid('sandblast'/'FRF', 'FRF:Sandblast:sandblast').
card_rarity('sandblast'/'FRF', 'Common').
card_artist('sandblast'/'FRF', 'Min Yum').
card_number('sandblast'/'FRF', '24').
card_flavor_text('sandblast'/'FRF', 'In the Shifting Wastes, more are killed by sand than by steel.').
card_multiverse_id('sandblast'/'FRF', '391912').

card_in_set('sandsteppe mastodon', 'FRF').
card_original_type('sandsteppe mastodon'/'FRF', 'Creature — Elephant').
card_original_text('sandsteppe mastodon'/'FRF', 'Reach\nWhen Sandsteppe Mastodon enters the battlefield, bolster 5. (Choose a creature with the least toughness among creatures you control and put five +1/+1 counters on it.)').
card_image_name('sandsteppe mastodon'/'FRF', 'sandsteppe mastodon').
card_uid('sandsteppe mastodon'/'FRF', 'FRF:Sandsteppe Mastodon:sandsteppe mastodon').
card_rarity('sandsteppe mastodon'/'FRF', 'Rare').
card_artist('sandsteppe mastodon'/'FRF', 'James Zapata').
card_number('sandsteppe mastodon'/'FRF', '137').
card_flavor_text('sandsteppe mastodon'/'FRF', 'A mastodon\'s path always leads to an oasis.').
card_multiverse_id('sandsteppe mastodon'/'FRF', '391913').
card_watermark('sandsteppe mastodon'/'FRF', 'Abzan').

card_in_set('sandsteppe outcast', 'FRF').
card_original_type('sandsteppe outcast'/'FRF', 'Creature — Human Warrior').
card_original_text('sandsteppe outcast'/'FRF', 'When Sandsteppe Outcast enters the battlefield, choose one —\n• Put a +1/+1 counter on Sandsteppe Outcast.\n• Put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_first_print('sandsteppe outcast', 'FRF').
card_image_name('sandsteppe outcast'/'FRF', 'sandsteppe outcast').
card_uid('sandsteppe outcast'/'FRF', 'FRF:Sandsteppe Outcast:sandsteppe outcast').
card_rarity('sandsteppe outcast'/'FRF', 'Common').
card_artist('sandsteppe outcast'/'FRF', 'Wesley Burt').
card_number('sandsteppe outcast'/'FRF', '25').
card_multiverse_id('sandsteppe outcast'/'FRF', '391914').

card_in_set('scoured barrens', 'FRF').
card_original_type('scoured barrens'/'FRF', 'Land').
card_original_text('scoured barrens'/'FRF', 'Scoured Barrens enters the battlefield tapped.\nWhen Scoured Barrens enters the battlefield, you gain 1 life.\n{T}: Add {W} or {B} to your mana pool.').
card_image_name('scoured barrens'/'FRF', 'scoured barrens').
card_uid('scoured barrens'/'FRF', 'FRF:Scoured Barrens:scoured barrens').
card_rarity('scoured barrens'/'FRF', 'Common').
card_artist('scoured barrens'/'FRF', 'Eytan Zana').
card_number('scoured barrens'/'FRF', '171').
card_multiverse_id('scoured barrens'/'FRF', '391915').

card_in_set('scroll of the masters', 'FRF').
card_original_type('scroll of the masters'/'FRF', 'Artifact').
card_original_text('scroll of the masters'/'FRF', 'Whenever you cast a noncreature spell, put a lore counter on Scroll of the Masters.\n{3}, {T}: Target creature you control gets +1/+1 until end of turn for each lore counter on Scroll of the Masters.').
card_first_print('scroll of the masters', 'FRF').
card_image_name('scroll of the masters'/'FRF', 'scroll of the masters').
card_uid('scroll of the masters'/'FRF', 'FRF:Scroll of the Masters:scroll of the masters').
card_rarity('scroll of the masters'/'FRF', 'Rare').
card_artist('scroll of the masters'/'FRF', 'Lake Hurwitz').
card_number('scroll of the masters'/'FRF', '163').
card_multiverse_id('scroll of the masters'/'FRF', '391916').

card_in_set('shaman of the great hunt', 'FRF').
card_original_type('shaman of the great hunt'/'FRF', 'Creature — Orc Shaman').
card_original_text('shaman of the great hunt'/'FRF', 'Haste\nWhenever a creature you control deals combat damage to a player, put a +1/+1 counter on it.\nFerocious — {2}{G/U}{G/U}: Draw a card for each creature you control with power 4 or greater.').
card_first_print('shaman of the great hunt', 'FRF').
card_image_name('shaman of the great hunt'/'FRF', 'shaman of the great hunt').
card_uid('shaman of the great hunt'/'FRF', 'FRF:Shaman of the Great Hunt:shaman of the great hunt').
card_rarity('shaman of the great hunt'/'FRF', 'Mythic Rare').
card_artist('shaman of the great hunt'/'FRF', 'Ryan Alexander Lee').
card_number('shaman of the great hunt'/'FRF', '113').
card_multiverse_id('shaman of the great hunt'/'FRF', '391917').
card_watermark('shaman of the great hunt'/'FRF', 'Temur').

card_in_set('shamanic revelation', 'FRF').
card_original_type('shamanic revelation'/'FRF', 'Sorcery').
card_original_text('shamanic revelation'/'FRF', 'Draw a card for each creature you control.\nFerocious — You gain 4 life for each creature you control with power 4 or greater.').
card_image_name('shamanic revelation'/'FRF', 'shamanic revelation').
card_uid('shamanic revelation'/'FRF', 'FRF:Shamanic Revelation:shamanic revelation').
card_rarity('shamanic revelation'/'FRF', 'Rare').
card_artist('shamanic revelation'/'FRF', 'Cynthia Sheppard').
card_number('shamanic revelation'/'FRF', '138').
card_flavor_text('shamanic revelation'/'FRF', 'First, whispers of ancient days. Then, visions of things to come.').
card_multiverse_id('shamanic revelation'/'FRF', '391918').
card_watermark('shamanic revelation'/'FRF', 'Temur').

card_in_set('shifting loyalties', 'FRF').
card_original_type('shifting loyalties'/'FRF', 'Sorcery').
card_original_text('shifting loyalties'/'FRF', 'Exchange control of two target permanents that share a card type. (Artifact, creature, enchantment, land, and planeswalker are card types.)').
card_first_print('shifting loyalties', 'FRF').
card_image_name('shifting loyalties'/'FRF', 'shifting loyalties').
card_uid('shifting loyalties'/'FRF', 'FRF:Shifting Loyalties:shifting loyalties').
card_rarity('shifting loyalties'/'FRF', 'Uncommon').
card_artist('shifting loyalties'/'FRF', 'James Ryman').
card_number('shifting loyalties'/'FRF', '51').
card_flavor_text('shifting loyalties'/'FRF', '\"Show the enemy the true path, and the spirit will yearn to follow it.\"\n—Houn, Jeskai elder').
card_multiverse_id('shifting loyalties'/'FRF', '391919').

card_in_set('shockmaw dragon', 'FRF').
card_original_type('shockmaw dragon'/'FRF', 'Creature — Dragon').
card_original_text('shockmaw dragon'/'FRF', 'Flying\nWhenever Shockmaw Dragon deals combat damage to a player, it deals 1 damage to each creature that player controls.').
card_first_print('shockmaw dragon', 'FRF').
card_image_name('shockmaw dragon'/'FRF', 'shockmaw dragon').
card_uid('shockmaw dragon'/'FRF', 'FRF:Shockmaw Dragon:shockmaw dragon').
card_rarity('shockmaw dragon'/'FRF', 'Uncommon').
card_artist('shockmaw dragon'/'FRF', 'Alejandro Mirabal').
card_number('shockmaw dragon'/'FRF', '114').
card_flavor_text('shockmaw dragon'/'FRF', 'Dragons of the Kolaghan brood are always on the move. They are the driving force behind the Mardu\'s nomadic way of life.').
card_multiverse_id('shockmaw dragon'/'FRF', '391920').
card_watermark('shockmaw dragon'/'FRF', 'Kolaghan').

card_in_set('shu yun, the silent tempest', 'FRF').
card_original_type('shu yun, the silent tempest'/'FRF', 'Legendary Creature — Human Monk').
card_original_text('shu yun, the silent tempest'/'FRF', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, you may pay {R/W}{R/W}. If you do, target creature gains double strike until end of turn.').
card_first_print('shu yun, the silent tempest', 'FRF').
card_image_name('shu yun, the silent tempest'/'FRF', 'shu yun, the silent tempest').
card_uid('shu yun, the silent tempest'/'FRF', 'FRF:Shu Yun, the Silent Tempest:shu yun, the silent tempest').
card_rarity('shu yun, the silent tempest'/'FRF', 'Rare').
card_artist('shu yun, the silent tempest'/'FRF', 'David Gaillet').
card_number('shu yun, the silent tempest'/'FRF', '52').
card_multiverse_id('shu yun, the silent tempest'/'FRF', '391921').
card_watermark('shu yun, the silent tempest'/'FRF', 'Jeskai').

card_in_set('sibsig host', 'FRF').
card_original_type('sibsig host'/'FRF', 'Creature — Zombie').
card_original_text('sibsig host'/'FRF', 'When Sibsig Host enters the battlefield, each player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('sibsig host', 'FRF').
card_image_name('sibsig host'/'FRF', 'sibsig host').
card_uid('sibsig host'/'FRF', 'FRF:Sibsig Host:sibsig host').
card_rarity('sibsig host'/'FRF', 'Common').
card_artist('sibsig host'/'FRF', 'Steve Prescott').
card_number('sibsig host'/'FRF', '82').
card_flavor_text('sibsig host'/'FRF', '\"They were your friends, your family, your clan. They want only to welcome you.\"\n—Tasigur, the Golden Fang').
card_multiverse_id('sibsig host'/'FRF', '391922').
card_watermark('sibsig host'/'FRF', 'Sultai').

card_in_set('sibsig muckdraggers', 'FRF').
card_original_type('sibsig muckdraggers'/'FRF', 'Creature — Zombie').
card_original_text('sibsig muckdraggers'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nWhen Sibsig Muckdraggers enters the battlefield, return target creature card from your graveyard to your hand.').
card_first_print('sibsig muckdraggers', 'FRF').
card_image_name('sibsig muckdraggers'/'FRF', 'sibsig muckdraggers').
card_uid('sibsig muckdraggers'/'FRF', 'FRF:Sibsig Muckdraggers:sibsig muckdraggers').
card_rarity('sibsig muckdraggers'/'FRF', 'Uncommon').
card_artist('sibsig muckdraggers'/'FRF', 'Zack Stella').
card_number('sibsig muckdraggers'/'FRF', '83').
card_multiverse_id('sibsig muckdraggers'/'FRF', '391923').
card_watermark('sibsig muckdraggers'/'FRF', 'Sultai').

card_in_set('silumgar, the drifting death', 'FRF').
card_original_type('silumgar, the drifting death'/'FRF', 'Legendary Creature — Dragon').
card_original_text('silumgar, the drifting death'/'FRF', 'Flying, hexproof\nWhenever a Dragon you control attacks, creatures defending player controls get -1/-1 until end of turn.').
card_first_print('silumgar, the drifting death', 'FRF').
card_image_name('silumgar, the drifting death'/'FRF', 'silumgar, the drifting death').
card_uid('silumgar, the drifting death'/'FRF', 'FRF:Silumgar, the Drifting Death:silumgar, the drifting death').
card_rarity('silumgar, the drifting death'/'FRF', 'Rare').
card_artist('silumgar, the drifting death'/'FRF', 'Steven Belledin').
card_number('silumgar, the drifting death'/'FRF', '157').
card_flavor_text('silumgar, the drifting death'/'FRF', '\"No machinations, no puppet strings, no plots. Just pure, sweeping death.\"\n—Tasigur, the Golden Fang').
card_multiverse_id('silumgar, the drifting death'/'FRF', '391924').
card_watermark('silumgar, the drifting death'/'FRF', 'Silumgar').

card_in_set('smoldering efreet', 'FRF').
card_original_type('smoldering efreet'/'FRF', 'Creature — Efreet Monk').
card_original_text('smoldering efreet'/'FRF', 'When Smoldering Efreet dies, it deals 2 damage to you.').
card_first_print('smoldering efreet', 'FRF').
card_image_name('smoldering efreet'/'FRF', 'smoldering efreet').
card_uid('smoldering efreet'/'FRF', 'FRF:Smoldering Efreet:smoldering efreet').
card_rarity('smoldering efreet'/'FRF', 'Common').
card_artist('smoldering efreet'/'FRF', 'Chase Stone').
card_number('smoldering efreet'/'FRF', '115').
card_flavor_text('smoldering efreet'/'FRF', 'The efreet are drawn to the Kaisham Wanderers, a loosely organized Jeskai school where trickery is employed to challenge the status quo and upend the belief systems of others.').
card_multiverse_id('smoldering efreet'/'FRF', '391925').
card_watermark('smoldering efreet'/'FRF', 'Jeskai').

card_in_set('soul summons', 'FRF').
card_original_type('soul summons'/'FRF', 'Sorcery').
card_original_text('soul summons'/'FRF', 'Manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('soul summons'/'FRF', 'soul summons').
card_uid('soul summons'/'FRF', 'FRF:Soul Summons:soul summons').
card_rarity('soul summons'/'FRF', 'Common').
card_artist('soul summons'/'FRF', 'Johann Bodin').
card_number('soul summons'/'FRF', '26').
card_flavor_text('soul summons'/'FRF', 'Ugin\'s magic reaches beyond the dragons. The clans have adapted it for war.').
card_multiverse_id('soul summons'/'FRF', '391926').

card_in_set('soulfire grand master', 'FRF').
card_original_type('soulfire grand master'/'FRF', 'Creature — Human Monk').
card_original_text('soulfire grand master'/'FRF', 'Lifelink\nInstant and sorcery spells you control have lifelink.\n{2}{U/R}{U/R}: The next time you cast an instant or sorcery spell from your hand this turn, put that card into your hand instead of into your graveyard as it resolves.').
card_first_print('soulfire grand master', 'FRF').
card_image_name('soulfire grand master'/'FRF', 'soulfire grand master').
card_uid('soulfire grand master'/'FRF', 'FRF:Soulfire Grand Master:soulfire grand master').
card_rarity('soulfire grand master'/'FRF', 'Mythic Rare').
card_artist('soulfire grand master'/'FRF', 'Johannes Voss').
card_number('soulfire grand master'/'FRF', '27').
card_multiverse_id('soulfire grand master'/'FRF', '391927').
card_watermark('soulfire grand master'/'FRF', 'Jeskai').

card_in_set('soulflayer', 'FRF').
card_original_type('soulflayer'/'FRF', 'Creature — Demon').
card_original_text('soulflayer'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nIf a creature card with flying was exiled with Soulflayer\'s delve ability, Soulflayer has flying. The same is true for first strike, double strike, deathtouch, haste, hexproof, indestructible, lifelink, reach, trample, and vigilance.').
card_first_print('soulflayer', 'FRF').
card_image_name('soulflayer'/'FRF', 'soulflayer').
card_uid('soulflayer'/'FRF', 'FRF:Soulflayer:soulflayer').
card_rarity('soulflayer'/'FRF', 'Rare').
card_artist('soulflayer'/'FRF', 'Seb McKinnon').
card_number('soulflayer'/'FRF', '84').
card_multiverse_id('soulflayer'/'FRF', '391928').
card_watermark('soulflayer'/'FRF', 'Sultai').

card_in_set('sudden reclamation', 'FRF').
card_original_type('sudden reclamation'/'FRF', 'Instant').
card_original_text('sudden reclamation'/'FRF', 'Put the top four cards of your library into your graveyard, then return a creature card and a land card from your graveyard to your hand.').
card_first_print('sudden reclamation', 'FRF').
card_image_name('sudden reclamation'/'FRF', 'sudden reclamation').
card_uid('sudden reclamation'/'FRF', 'FRF:Sudden Reclamation:sudden reclamation').
card_rarity('sudden reclamation'/'FRF', 'Uncommon').
card_artist('sudden reclamation'/'FRF', 'Jack Wang').
card_number('sudden reclamation'/'FRF', '139').
card_flavor_text('sudden reclamation'/'FRF', '\"There is no secret buried so deep that we will never find it.\"\n—Kurtar, Sultai necromancer').
card_multiverse_id('sudden reclamation'/'FRF', '391929').

card_in_set('sultai emissary', 'FRF').
card_original_type('sultai emissary'/'FRF', 'Creature — Zombie Warrior').
card_original_text('sultai emissary'/'FRF', 'When Sultai Emissary dies, manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('sultai emissary'/'FRF', 'sultai emissary').
card_uid('sultai emissary'/'FRF', 'FRF:Sultai Emissary:sultai emissary').
card_rarity('sultai emissary'/'FRF', 'Common').
card_artist('sultai emissary'/'FRF', 'Mathias Kollros').
card_number('sultai emissary'/'FRF', '85').
card_flavor_text('sultai emissary'/'FRF', 'To the Sultai, Ugin\'s power was just one more resource to be exploited.').
card_multiverse_id('sultai emissary'/'FRF', '391930').
card_watermark('sultai emissary'/'FRF', 'Sultai').

card_in_set('sultai runemark', 'FRF').
card_original_type('sultai runemark'/'FRF', 'Enchantment — Aura').
card_original_text('sultai runemark'/'FRF', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has deathtouch as long as you control a green or blue permanent. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('sultai runemark', 'FRF').
card_image_name('sultai runemark'/'FRF', 'sultai runemark').
card_uid('sultai runemark'/'FRF', 'FRF:Sultai Runemark:sultai runemark').
card_rarity('sultai runemark'/'FRF', 'Common').
card_artist('sultai runemark'/'FRF', 'Volkan Baga').
card_number('sultai runemark'/'FRF', '86').
card_multiverse_id('sultai runemark'/'FRF', '391931').
card_watermark('sultai runemark'/'FRF', 'Sultai').

card_in_set('sultai skullkeeper', 'FRF').
card_original_type('sultai skullkeeper'/'FRF', 'Creature — Naga Shaman').
card_original_text('sultai skullkeeper'/'FRF', 'When Sultai Skullkeeper enters the battlefield, put the top two cards of your library into your graveyard.').
card_first_print('sultai skullkeeper', 'FRF').
card_image_name('sultai skullkeeper'/'FRF', 'sultai skullkeeper').
card_uid('sultai skullkeeper'/'FRF', 'FRF:Sultai Skullkeeper:sultai skullkeeper').
card_rarity('sultai skullkeeper'/'FRF', 'Common').
card_artist('sultai skullkeeper'/'FRF', 'Ryan Barger').
card_number('sultai skullkeeper'/'FRF', '53').
card_flavor_text('sultai skullkeeper'/'FRF', 'A skullkeeper is the first to arrive after the palace archers strike down intruders, probing their brains for choice bits of knowledge.').
card_multiverse_id('sultai skullkeeper'/'FRF', '391932').
card_watermark('sultai skullkeeper'/'FRF', 'Sultai').

card_in_set('supplant form', 'FRF').
card_original_type('supplant form'/'FRF', 'Instant').
card_original_text('supplant form'/'FRF', 'Return target creature to its owner\'s hand. You put a token onto the battlefield that\'s a copy of that creature.').
card_image_name('supplant form'/'FRF', 'supplant form').
card_uid('supplant form'/'FRF', 'FRF:Supplant Form:supplant form').
card_rarity('supplant form'/'FRF', 'Rare').
card_artist('supplant form'/'FRF', 'Adam Paquette').
card_number('supplant form'/'FRF', '54').
card_flavor_text('supplant form'/'FRF', '\"Ice can be shaped to any form, even the whisper of a memory.\"\n—Mytha, Temur shaman').
card_multiverse_id('supplant form'/'FRF', '391933').

card_in_set('swamp', 'FRF').
card_original_type('swamp'/'FRF', 'Basic Land — Swamp').
card_original_text('swamp'/'FRF', 'B').
card_image_name('swamp'/'FRF', 'swamp1').
card_uid('swamp'/'FRF', 'FRF:Swamp:swamp1').
card_rarity('swamp'/'FRF', 'Basic Land').
card_artist('swamp'/'FRF', 'Sam Burley').
card_number('swamp'/'FRF', '180').
card_multiverse_id('swamp'/'FRF', '391934').

card_in_set('swamp', 'FRF').
card_original_type('swamp'/'FRF', 'Basic Land — Swamp').
card_original_text('swamp'/'FRF', 'B').
card_image_name('swamp'/'FRF', 'swamp2').
card_uid('swamp'/'FRF', 'FRF:Swamp:swamp2').
card_rarity('swamp'/'FRF', 'Basic Land').
card_artist('swamp'/'FRF', 'Adam Paquette').
card_number('swamp'/'FRF', '181').
card_multiverse_id('swamp'/'FRF', '391935').

card_in_set('swiftwater cliffs', 'FRF').
card_original_type('swiftwater cliffs'/'FRF', 'Land').
card_original_text('swiftwater cliffs'/'FRF', 'Swiftwater Cliffs enters the battlefield tapped.\nWhen Swiftwater Cliffs enters the battlefield, you gain 1 life.\n{T}: Add {U} or {R} to your mana pool.').
card_image_name('swiftwater cliffs'/'FRF', 'swiftwater cliffs').
card_uid('swiftwater cliffs'/'FRF', 'FRF:Swiftwater Cliffs:swiftwater cliffs').
card_rarity('swiftwater cliffs'/'FRF', 'Common').
card_artist('swiftwater cliffs'/'FRF', 'Eytan Zana').
card_number('swiftwater cliffs'/'FRF', '172').
card_multiverse_id('swiftwater cliffs'/'FRF', '391936').

card_in_set('tasigur\'s cruelty', 'FRF').
card_original_type('tasigur\'s cruelty'/'FRF', 'Sorcery').
card_original_text('tasigur\'s cruelty'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nEach opponent discards two cards.').
card_first_print('tasigur\'s cruelty', 'FRF').
card_image_name('tasigur\'s cruelty'/'FRF', 'tasigur\'s cruelty').
card_uid('tasigur\'s cruelty'/'FRF', 'FRF:Tasigur\'s Cruelty:tasigur\'s cruelty').
card_rarity('tasigur\'s cruelty'/'FRF', 'Common').
card_artist('tasigur\'s cruelty'/'FRF', 'Chris Rahn').
card_number('tasigur\'s cruelty'/'FRF', '88').
card_flavor_text('tasigur\'s cruelty'/'FRF', 'The cruelest tortures leave no mark upon the flesh.').
card_multiverse_id('tasigur\'s cruelty'/'FRF', '391938').
card_watermark('tasigur\'s cruelty'/'FRF', 'Sultai').

card_in_set('tasigur, the golden fang', 'FRF').
card_original_type('tasigur, the golden fang'/'FRF', 'Legendary Creature — Human Shaman').
card_original_text('tasigur, the golden fang'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\n{2}{G/U}{G/U}: Put the top two cards of your library into your graveyard, then return a nonland card of an opponent\'s choice from your graveyard to your hand.').
card_first_print('tasigur, the golden fang', 'FRF').
card_image_name('tasigur, the golden fang'/'FRF', 'tasigur, the golden fang').
card_uid('tasigur, the golden fang'/'FRF', 'FRF:Tasigur, the Golden Fang:tasigur, the golden fang').
card_rarity('tasigur, the golden fang'/'FRF', 'Rare').
card_artist('tasigur, the golden fang'/'FRF', 'Chris Rahn').
card_number('tasigur, the golden fang'/'FRF', '87').
card_multiverse_id('tasigur, the golden fang'/'FRF', '391937').
card_watermark('tasigur, the golden fang'/'FRF', 'Sultai').

card_in_set('temporal trespass', 'FRF').
card_original_type('temporal trespass'/'FRF', 'Sorcery').
card_original_text('temporal trespass'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTake an extra turn after this one. Exile Temporal Trespass.').
card_first_print('temporal trespass', 'FRF').
card_image_name('temporal trespass'/'FRF', 'temporal trespass').
card_uid('temporal trespass'/'FRF', 'FRF:Temporal Trespass:temporal trespass').
card_rarity('temporal trespass'/'FRF', 'Mythic Rare').
card_artist('temporal trespass'/'FRF', 'Clint Cearley').
card_number('temporal trespass'/'FRF', '55').
card_flavor_text('temporal trespass'/'FRF', 'Few truly live in the moment.').
card_multiverse_id('temporal trespass'/'FRF', '391939').
card_watermark('temporal trespass'/'FRF', 'Sultai').

card_in_set('temur battle rage', 'FRF').
card_original_type('temur battle rage'/'FRF', 'Instant').
card_original_text('temur battle rage'/'FRF', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)\nFerocious — That creature also gains trample until end of turn if you control a creature with power 4 or greater.').
card_first_print('temur battle rage', 'FRF').
card_image_name('temur battle rage'/'FRF', 'temur battle rage').
card_uid('temur battle rage'/'FRF', 'FRF:Temur Battle Rage:temur battle rage').
card_rarity('temur battle rage'/'FRF', 'Common').
card_artist('temur battle rage'/'FRF', 'Jaime Jones').
card_number('temur battle rage'/'FRF', '116').
card_multiverse_id('temur battle rage'/'FRF', '391940').
card_watermark('temur battle rage'/'FRF', 'Temur').

card_in_set('temur runemark', 'FRF').
card_original_type('temur runemark'/'FRF', 'Enchantment — Aura').
card_original_text('temur runemark'/'FRF', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has trample as long as you control a blue or red permanent.').
card_first_print('temur runemark', 'FRF').
card_image_name('temur runemark'/'FRF', 'temur runemark').
card_uid('temur runemark'/'FRF', 'FRF:Temur Runemark:temur runemark').
card_rarity('temur runemark'/'FRF', 'Common').
card_artist('temur runemark'/'FRF', 'Ryan Alexander Lee').
card_number('temur runemark'/'FRF', '140').
card_multiverse_id('temur runemark'/'FRF', '391941').
card_watermark('temur runemark'/'FRF', 'Temur').

card_in_set('temur sabertooth', 'FRF').
card_original_type('temur sabertooth'/'FRF', 'Creature — Cat').
card_original_text('temur sabertooth'/'FRF', '{1}{G}: You may return another creature you control to its owner\'s hand. If you do, Temur Sabertooth gains indestructible until end of turn.').
card_first_print('temur sabertooth', 'FRF').
card_image_name('temur sabertooth'/'FRF', 'temur sabertooth').
card_uid('temur sabertooth'/'FRF', 'FRF:Temur Sabertooth:temur sabertooth').
card_rarity('temur sabertooth'/'FRF', 'Uncommon').
card_artist('temur sabertooth'/'FRF', 'Mike Sass').
card_number('temur sabertooth'/'FRF', '141').
card_flavor_text('temur sabertooth'/'FRF', 'The Temur see themselves as a pack, their bonds more primal than the Abzan\'s.').
card_multiverse_id('temur sabertooth'/'FRF', '391942').
card_watermark('temur sabertooth'/'FRF', 'Temur').

card_in_set('temur war shaman', 'FRF').
card_original_type('temur war shaman'/'FRF', 'Creature — Human Shaman').
card_original_text('temur war shaman'/'FRF', 'When Temur War Shaman enters the battlefield, manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nWhenever a permanent you control is turned face up, if it\'s a creature, you may have it fight target creature you don\'t control.').
card_image_name('temur war shaman'/'FRF', 'temur war shaman').
card_uid('temur war shaman'/'FRF', 'FRF:Temur War Shaman:temur war shaman').
card_rarity('temur war shaman'/'FRF', 'Rare').
card_artist('temur war shaman'/'FRF', 'Nils Hamm').
card_number('temur war shaman'/'FRF', '142').
card_multiverse_id('temur war shaman'/'FRF', '391943').
card_watermark('temur war shaman'/'FRF', 'Temur').

card_in_set('thornwood falls', 'FRF').
card_original_type('thornwood falls'/'FRF', 'Land').
card_original_text('thornwood falls'/'FRF', 'Thornwood Falls enters the battlefield tapped.\nWhen Thornwood Falls enters the battlefield, you gain 1 life.\n{T}: Add {G} or {U} to your mana pool.').
card_image_name('thornwood falls'/'FRF', 'thornwood falls').
card_uid('thornwood falls'/'FRF', 'FRF:Thornwood Falls:thornwood falls').
card_rarity('thornwood falls'/'FRF', 'Common').
card_artist('thornwood falls'/'FRF', 'Eytan Zana').
card_number('thornwood falls'/'FRF', '173').
card_multiverse_id('thornwood falls'/'FRF', '391944').

card_in_set('torrent elemental', 'FRF').
card_original_type('torrent elemental'/'FRF', 'Creature — Elemental').
card_original_text('torrent elemental'/'FRF', 'Flying\nWhenever Torrent Elemental attacks, tap all creatures defending player controls.\n{3}{B/G}{B/G}: Put Torrent Elemental from exile onto the battlefield tapped. Activate this ability only any time you could cast a sorcery.').
card_first_print('torrent elemental', 'FRF').
card_image_name('torrent elemental'/'FRF', 'torrent elemental').
card_uid('torrent elemental'/'FRF', 'FRF:Torrent Elemental:torrent elemental').
card_rarity('torrent elemental'/'FRF', 'Mythic Rare').
card_artist('torrent elemental'/'FRF', 'James Paick').
card_number('torrent elemental'/'FRF', '56').
card_multiverse_id('torrent elemental'/'FRF', '391945').
card_watermark('torrent elemental'/'FRF', 'Sultai').

card_in_set('tranquil cove', 'FRF').
card_original_type('tranquil cove'/'FRF', 'Land').
card_original_text('tranquil cove'/'FRF', 'Tranquil Cove enters the battlefield tapped.\nWhen Tranquil Cove enters the battlefield, you gain 1 life.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('tranquil cove'/'FRF', 'tranquil cove').
card_uid('tranquil cove'/'FRF', 'FRF:Tranquil Cove:tranquil cove').
card_rarity('tranquil cove'/'FRF', 'Common').
card_artist('tranquil cove'/'FRF', 'John Avon').
card_number('tranquil cove'/'FRF', '174').
card_multiverse_id('tranquil cove'/'FRF', '391946').

card_in_set('typhoid rats', 'FRF').
card_original_type('typhoid rats'/'FRF', 'Creature — Rat').
card_original_text('typhoid rats'/'FRF', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('typhoid rats'/'FRF', 'typhoid rats').
card_uid('typhoid rats'/'FRF', 'FRF:Typhoid Rats:typhoid rats').
card_rarity('typhoid rats'/'FRF', 'Common').
card_artist('typhoid rats'/'FRF', 'Dave Kendall').
card_number('typhoid rats'/'FRF', '89').
card_flavor_text('typhoid rats'/'FRF', 'When Tasigur sent his ambassadors to the Abzan outpost, the true envoys were not the naga but the infectious rats they carried with them.').
card_multiverse_id('typhoid rats'/'FRF', '391947').
card_watermark('typhoid rats'/'FRF', 'Sultai').

card_in_set('ugin\'s construct', 'FRF').
card_original_type('ugin\'s construct'/'FRF', 'Artifact Creature — Construct').
card_original_text('ugin\'s construct'/'FRF', 'When Ugin\'s Construct enters the battlefield, sacrifice a permanent that\'s one or more colors.').
card_image_name('ugin\'s construct'/'FRF', 'ugin\'s construct').
card_uid('ugin\'s construct'/'FRF', 'FRF:Ugin\'s Construct:ugin\'s construct').
card_rarity('ugin\'s construct'/'FRF', 'Uncommon').
card_artist('ugin\'s construct'/'FRF', 'Peter Mohrbacher').
card_number('ugin\'s construct'/'FRF', '164').
card_flavor_text('ugin\'s construct'/'FRF', 'While trapping the Eldrazi on Zendikar, Ugin learned little from Sorin, but he gleaned the rudiments of lithomancy from Nahiri.').
card_multiverse_id('ugin\'s construct'/'FRF', '391949').

card_in_set('ugin, the spirit dragon', 'FRF').
card_original_type('ugin, the spirit dragon'/'FRF', 'Planeswalker — Ugin').
card_original_text('ugin, the spirit dragon'/'FRF', '+2: Ugin, the Spirit Dragon deals 3 damage to target creature or player.\n−X: Exile each permanent with converted mana cost X or less that\'s one or more colors.\n−10: You gain 7 life, draw seven cards, then put up to seven permanent cards from your hand onto the battlefield.').
card_image_name('ugin, the spirit dragon'/'FRF', 'ugin, the spirit dragon').
card_uid('ugin, the spirit dragon'/'FRF', 'FRF:Ugin, the Spirit Dragon:ugin, the spirit dragon').
card_rarity('ugin, the spirit dragon'/'FRF', 'Mythic Rare').
card_artist('ugin, the spirit dragon'/'FRF', 'Raymond Swanland').
card_number('ugin, the spirit dragon'/'FRF', '1').
card_multiverse_id('ugin, the spirit dragon'/'FRF', '391948').

card_in_set('valorous stance', 'FRF').
card_original_type('valorous stance'/'FRF', 'Instant').
card_original_text('valorous stance'/'FRF', 'Choose one —\n• Target creature gains indestructible until end of turn.\n• Destroy target creature with toughness 4 or greater.').
card_first_print('valorous stance', 'FRF').
card_image_name('valorous stance'/'FRF', 'valorous stance').
card_uid('valorous stance'/'FRF', 'FRF:Valorous Stance:valorous stance').
card_rarity('valorous stance'/'FRF', 'Uncommon').
card_artist('valorous stance'/'FRF', 'Willian Murai').
card_number('valorous stance'/'FRF', '28').
card_flavor_text('valorous stance'/'FRF', '\"Every choice alters the path of fate.\"\n—Sarkhan Vol').
card_multiverse_id('valorous stance'/'FRF', '391950').

card_in_set('vaultbreaker', 'FRF').
card_original_type('vaultbreaker'/'FRF', 'Creature — Orc Rogue').
card_original_text('vaultbreaker'/'FRF', 'Whenever Vaultbreaker attacks, you may discard a card. If you do, draw a card.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('vaultbreaker', 'FRF').
card_image_name('vaultbreaker'/'FRF', 'vaultbreaker').
card_uid('vaultbreaker'/'FRF', 'FRF:Vaultbreaker:vaultbreaker').
card_rarity('vaultbreaker'/'FRF', 'Uncommon').
card_artist('vaultbreaker'/'FRF', 'Wayne Reynolds').
card_number('vaultbreaker'/'FRF', '117').
card_multiverse_id('vaultbreaker'/'FRF', '391951').
card_watermark('vaultbreaker'/'FRF', 'Mardu').

card_in_set('wandering champion', 'FRF').
card_original_type('wandering champion'/'FRF', 'Creature — Human Monk').
card_original_text('wandering champion'/'FRF', 'Whenever Wandering Champion deals combat damage to a player, if you control a blue or red permanent, you may discard a card. If you do, draw a card.').
card_first_print('wandering champion', 'FRF').
card_image_name('wandering champion'/'FRF', 'wandering champion').
card_uid('wandering champion'/'FRF', 'FRF:Wandering Champion:wandering champion').
card_rarity('wandering champion'/'FRF', 'Uncommon').
card_artist('wandering champion'/'FRF', 'Willian Murai').
card_number('wandering champion'/'FRF', '29').
card_flavor_text('wandering champion'/'FRF', '\"Learn from your enemies, but do not tolerate them.\"').
card_multiverse_id('wandering champion'/'FRF', '391952').
card_watermark('wandering champion'/'FRF', 'Jeskai').

card_in_set('war flare', 'FRF').
card_original_type('war flare'/'FRF', 'Instant').
card_original_text('war flare'/'FRF', 'Creatures you control get +2/+1 until end of turn. Untap those creatures.').
card_first_print('war flare', 'FRF').
card_image_name('war flare'/'FRF', 'war flare').
card_uid('war flare'/'FRF', 'FRF:War Flare:war flare').
card_rarity('war flare'/'FRF', 'Common').
card_artist('war flare'/'FRF', 'Karl Kopinski').
card_number('war flare'/'FRF', '158').
card_flavor_text('war flare'/'FRF', '\"No sun or fire can warm my blood quite like a war flare.\"\n—Urut Barzeel, Mardu hordechief').
card_multiverse_id('war flare'/'FRF', '391953').

card_in_set('warden of the first tree', 'FRF').
card_original_type('warden of the first tree'/'FRF', 'Creature — Human').
card_original_text('warden of the first tree'/'FRF', '{1}{W/B}: Warden of the First Tree becomes a Human Warrior with base power and toughness 3/3.\n{2}{W/B}{W/B}: If Warden of the First Tree is a Warrior, it becomes a Human Spirit Warrior with trample and lifelink.\n{3}{W/B}{W/B}{W/B}: If Warden of the First Tree is a Spirit, put five +1/+1 counters on it.').
card_first_print('warden of the first tree', 'FRF').
card_image_name('warden of the first tree'/'FRF', 'warden of the first tree').
card_uid('warden of the first tree'/'FRF', 'FRF:Warden of the First Tree:warden of the first tree').
card_rarity('warden of the first tree'/'FRF', 'Mythic Rare').
card_artist('warden of the first tree'/'FRF', 'Ryan Alexander Lee').
card_number('warden of the first tree'/'FRF', '143').
card_multiverse_id('warden of the first tree'/'FRF', '391954').
card_watermark('warden of the first tree'/'FRF', 'Abzan').

card_in_set('wardscale dragon', 'FRF').
card_original_type('wardscale dragon'/'FRF', 'Creature — Dragon').
card_original_text('wardscale dragon'/'FRF', 'Flying\nAs long as Wardscale Dragon is attacking, defending player can\'t cast spells.').
card_first_print('wardscale dragon', 'FRF').
card_image_name('wardscale dragon'/'FRF', 'wardscale dragon').
card_uid('wardscale dragon'/'FRF', 'FRF:Wardscale Dragon:wardscale dragon').
card_rarity('wardscale dragon'/'FRF', 'Uncommon').
card_artist('wardscale dragon'/'FRF', 'Jason Rainville').
card_number('wardscale dragon'/'FRF', '30').
card_flavor_text('wardscale dragon'/'FRF', 'Dromoka\'s brood emerge from the dragon tempests covered with tough scales that protect them from most clan weapons and magic.').
card_multiverse_id('wardscale dragon'/'FRF', '391955').
card_watermark('wardscale dragon'/'FRF', 'Dromoka').

card_in_set('whisk away', 'FRF').
card_original_type('whisk away'/'FRF', 'Instant').
card_original_text('whisk away'/'FRF', 'Put target attacking or blocking creature on top of its owner\'s library.').
card_first_print('whisk away', 'FRF').
card_image_name('whisk away'/'FRF', 'whisk away').
card_uid('whisk away'/'FRF', 'FRF:Whisk Away:whisk away').
card_rarity('whisk away'/'FRF', 'Common').
card_artist('whisk away'/'FRF', 'Zack Stella').
card_number('whisk away'/'FRF', '57').
card_flavor_text('whisk away'/'FRF', 'The maneuvers of the Abzan formation intrigued Ojutai in both their complexity and their futility.').
card_multiverse_id('whisk away'/'FRF', '391956').

card_in_set('whisperer of the wilds', 'FRF').
card_original_type('whisperer of the wilds'/'FRF', 'Creature — Human Shaman').
card_original_text('whisperer of the wilds'/'FRF', '{T}: Add {G} to your mana pool.\nFerocious — {T}: Add {G}{G} to your mana pool. Activate this ability only if you control a creature with power 4 or greater.').
card_first_print('whisperer of the wilds', 'FRF').
card_image_name('whisperer of the wilds'/'FRF', 'whisperer of the wilds').
card_uid('whisperer of the wilds'/'FRF', 'FRF:Whisperer of the Wilds:whisperer of the wilds').
card_rarity('whisperer of the wilds'/'FRF', 'Common').
card_artist('whisperer of the wilds'/'FRF', 'David Gaillet').
card_number('whisperer of the wilds'/'FRF', '144').
card_flavor_text('whisperer of the wilds'/'FRF', 'She speaks only in whispers but can be heard over the fiercest storm.').
card_multiverse_id('whisperer of the wilds'/'FRF', '391957').
card_watermark('whisperer of the wilds'/'FRF', 'Temur').

card_in_set('whisperwood elemental', 'FRF').
card_original_type('whisperwood elemental'/'FRF', 'Creature — Elemental').
card_original_text('whisperwood elemental'/'FRF', 'At the beginning of your end step, manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nSacrifice Whisperwood Elemental: Until end of turn, face-up nontoken creatures you control gain \"When this creature dies, manifest the top card of your library.\"').
card_first_print('whisperwood elemental', 'FRF').
card_image_name('whisperwood elemental'/'FRF', 'whisperwood elemental').
card_uid('whisperwood elemental'/'FRF', 'FRF:Whisperwood Elemental:whisperwood elemental').
card_rarity('whisperwood elemental'/'FRF', 'Mythic Rare').
card_artist('whisperwood elemental'/'FRF', 'Raymond Swanland').
card_number('whisperwood elemental'/'FRF', '145').
card_multiverse_id('whisperwood elemental'/'FRF', '391958').
card_watermark('whisperwood elemental'/'FRF', 'Temur').

card_in_set('wild slash', 'FRF').
card_original_type('wild slash'/'FRF', 'Instant').
card_original_text('wild slash'/'FRF', 'Ferocious — If you control a creature with power 4 or greater, damage can\'t be prevented this turn.\nWild Slash deals 2 damage to target creature or player.').
card_first_print('wild slash', 'FRF').
card_image_name('wild slash'/'FRF', 'wild slash').
card_uid('wild slash'/'FRF', 'FRF:Wild Slash:wild slash').
card_rarity('wild slash'/'FRF', 'Uncommon').
card_artist('wild slash'/'FRF', 'Raymond Swanland').
card_number('wild slash'/'FRF', '118').
card_flavor_text('wild slash'/'FRF', 'Never mistake deception for cleverness.').
card_multiverse_id('wild slash'/'FRF', '391959').
card_watermark('wild slash'/'FRF', 'Temur').

card_in_set('wildcall', 'FRF').
card_original_type('wildcall'/'FRF', 'Sorcery').
card_original_text('wildcall'/'FRF', 'Manifest the top card of your library, then put X +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('wildcall'/'FRF', 'wildcall').
card_uid('wildcall'/'FRF', 'FRF:Wildcall:wildcall').
card_rarity('wildcall'/'FRF', 'Rare').
card_artist('wildcall'/'FRF', 'Adam Paquette').
card_number('wildcall'/'FRF', '146').
card_flavor_text('wildcall'/'FRF', 'A howl on the wind hides many dangers.').
card_multiverse_id('wildcall'/'FRF', '391960').

card_in_set('will of the naga', 'FRF').
card_original_type('will of the naga'/'FRF', 'Instant').
card_original_text('will of the naga'/'FRF', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_first_print('will of the naga', 'FRF').
card_image_name('will of the naga'/'FRF', 'will of the naga').
card_uid('will of the naga'/'FRF', 'FRF:Will of the Naga:will of the naga').
card_rarity('will of the naga'/'FRF', 'Common').
card_artist('will of the naga'/'FRF', 'Wayne Reynolds').
card_number('will of the naga'/'FRF', '58').
card_flavor_text('will of the naga'/'FRF', 'The charm of a naga is undeniable.').
card_multiverse_id('will of the naga'/'FRF', '391961').
card_watermark('will of the naga'/'FRF', 'Sultai').

card_in_set('wind-scarred crag', 'FRF').
card_original_type('wind-scarred crag'/'FRF', 'Land').
card_original_text('wind-scarred crag'/'FRF', 'Wind-Scarred Crag enters the battlefield tapped.\nWhen Wind-Scarred Crag enters the battlefield, you gain 1 life.\n{T}: Add {R} or {W} to your mana pool.').
card_image_name('wind-scarred crag'/'FRF', 'wind-scarred crag').
card_uid('wind-scarred crag'/'FRF', 'FRF:Wind-Scarred Crag:wind-scarred crag').
card_rarity('wind-scarred crag'/'FRF', 'Common').
card_artist('wind-scarred crag'/'FRF', 'Eytan Zana').
card_number('wind-scarred crag'/'FRF', '175').
card_multiverse_id('wind-scarred crag'/'FRF', '391963').

card_in_set('winds of qal sisma', 'FRF').
card_original_type('winds of qal sisma'/'FRF', 'Instant').
card_original_text('winds of qal sisma'/'FRF', 'Prevent all combat damage that would be dealt this turn.\nFerocious — If you control a creature with power 4 or greater, instead prevent all combat damage that would be dealt this turn by creatures your opponents control.').
card_first_print('winds of qal sisma', 'FRF').
card_image_name('winds of qal sisma'/'FRF', 'winds of qal sisma').
card_uid('winds of qal sisma'/'FRF', 'FRF:Winds of Qal Sisma:winds of qal sisma').
card_rarity('winds of qal sisma'/'FRF', 'Uncommon').
card_artist('winds of qal sisma'/'FRF', 'Howard Lyon').
card_number('winds of qal sisma'/'FRF', '147').
card_multiverse_id('winds of qal sisma'/'FRF', '391962').
card_watermark('winds of qal sisma'/'FRF', 'Temur').

card_in_set('write into being', 'FRF').
card_original_type('write into being'/'FRF', 'Sorcery').
card_original_text('write into being'/'FRF', 'Look at the top two cards of your library. Manifest one of those cards, then put the other on the top or bottom of your library. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_image_name('write into being'/'FRF', 'write into being').
card_uid('write into being'/'FRF', 'FRF:Write into Being:write into being').
card_rarity('write into being'/'FRF', 'Common').
card_artist('write into being'/'FRF', 'Yeong-Hao Han').
card_number('write into being'/'FRF', '59').
card_multiverse_id('write into being'/'FRF', '391964').

card_in_set('yasova dragonclaw', 'FRF').
card_original_type('yasova dragonclaw'/'FRF', 'Legendary Creature — Human Warrior').
card_original_text('yasova dragonclaw'/'FRF', 'Trample\nAt the beginning of combat on your turn, you may pay {1}{U/R}{U/R}. If you do, gain control of target creature an opponent controls with power less than Yasova Dragonclaw\'s power until end of turn, untap that creature, and it gains haste until end of turn.').
card_first_print('yasova dragonclaw', 'FRF').
card_image_name('yasova dragonclaw'/'FRF', 'yasova dragonclaw').
card_uid('yasova dragonclaw'/'FRF', 'FRF:Yasova Dragonclaw:yasova dragonclaw').
card_rarity('yasova dragonclaw'/'FRF', 'Rare').
card_artist('yasova dragonclaw'/'FRF', 'Winona Nelson').
card_number('yasova dragonclaw'/'FRF', '148').
card_multiverse_id('yasova dragonclaw'/'FRF', '391965').
card_watermark('yasova dragonclaw'/'FRF', 'Temur').
