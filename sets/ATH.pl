% Anthologies

set('ATH').
set_name('ATH', 'Anthologies').
set_release_date('ATH', '1998-11-01').
set_border('ATH', 'white').
set_type('ATH', 'box').

card_in_set('aesthir glider', 'ATH').
card_original_type('aesthir glider'/'ATH', 'Artifact Creature').
card_original_text('aesthir glider'/'ATH', 'Flying\nAesthir Glider cannot block.').
card_image_name('aesthir glider'/'ATH', 'aesthir glider').
card_uid('aesthir glider'/'ATH', 'ATH:Aesthir Glider:aesthir glider').
card_rarity('aesthir glider'/'ATH', 'Special').
card_artist('aesthir glider'/'ATH', 'Ruth Thompson').
card_flavor_text('aesthir glider'/'ATH', '\"A fine example of the rewards of artifice: a thoroughly obedient steed with wings of Soldevi steel.\"\n—Arcum Dagsson,\nSoldevi Machinist').

card_in_set('armageddon', 'ATH').
card_original_type('armageddon'/'ATH', 'Sorcery').
card_original_text('armageddon'/'ATH', 'Destroy all lands.').
card_image_name('armageddon'/'ATH', 'armageddon').
card_uid('armageddon'/'ATH', 'ATH:Armageddon:armageddon').
card_rarity('armageddon'/'ATH', 'Special').
card_artist('armageddon'/'ATH', 'Jesper Myrfors').

card_in_set('armored pegasus', 'ATH').
card_original_type('armored pegasus'/'ATH', 'Creature — Pegasus').
card_original_text('armored pegasus'/'ATH', 'Flying').
card_image_name('armored pegasus'/'ATH', 'armored pegasus').
card_uid('armored pegasus'/'ATH', 'ATH:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'ATH', 'Special').
card_artist('armored pegasus'/'ATH', 'Andrew Robinson').
card_flavor_text('armored pegasus'/'ATH', 'Asked how it survived a run-in with a bog imp, the pegasus just shook its mane and burped.').

card_in_set('benalish knight', 'ATH').
card_original_type('benalish knight'/'ATH', 'Summon — Knight').
card_original_text('benalish knight'/'ATH', 'First strike\nYou may play Benalish Knight any time you could play an instant.').
card_image_name('benalish knight'/'ATH', 'benalish knight').
card_uid('benalish knight'/'ATH', 'ATH:Benalish Knight:benalish knight').
card_rarity('benalish knight'/'ATH', 'Special').
card_artist('benalish knight'/'ATH', 'Zina Saunders').
card_flavor_text('benalish knight'/'ATH', '\"We called them ‘armored lightning.\'\"\n—Gerrard of the Weatherlight').

card_in_set('black knight', 'ATH').
card_original_type('black knight'/'ATH', 'Summon — Knight').
card_original_text('black knight'/'ATH', 'First strike, protection from white').
card_image_name('black knight'/'ATH', 'black knight').
card_uid('black knight'/'ATH', 'ATH:Black Knight:black knight').
card_rarity('black knight'/'ATH', 'Special').
card_artist('black knight'/'ATH', 'Adrian Smith').
card_flavor_text('black knight'/'ATH', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').

card_in_set('brushland', 'ATH').
card_original_type('brushland'/'ATH', 'Land').
card_original_text('brushland'/'ATH', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'ATH', 'brushland').
card_uid('brushland'/'ATH', 'ATH:Brushland:brushland').
card_rarity('brushland'/'ATH', 'Special').
card_artist('brushland'/'ATH', 'Bryon Wackwitz').

card_in_set('canopy spider', 'ATH').
card_original_type('canopy spider'/'ATH', 'Summon — Spider').
card_original_text('canopy spider'/'ATH', 'Canopy Spider can block creatures with flying.').
card_image_name('canopy spider'/'ATH', 'canopy spider').
card_uid('canopy spider'/'ATH', 'ATH:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'ATH', 'Special').
card_artist('canopy spider'/'ATH', 'Christopher Rush').
card_flavor_text('canopy spider'/'ATH', '\"We know our place in the cycle of life—and it is not as flies.\"\n—Eladamri, Lord of Leaves').

card_in_set('carnivorous plant', 'ATH').
card_original_type('carnivorous plant'/'ATH', 'Summon — Wall').
card_original_text('carnivorous plant'/'ATH', '').
card_image_name('carnivorous plant'/'ATH', 'carnivorous plant').
card_uid('carnivorous plant'/'ATH', 'ATH:Carnivorous Plant:carnivorous plant').
card_rarity('carnivorous plant'/'ATH', 'Special').
card_artist('carnivorous plant'/'ATH', 'Quinton Hoover').
card_flavor_text('carnivorous plant'/'ATH', '\"It had a mouth like that of a great beast, and gnashed its teeth as it strained to reach us. I am thankful it possessed no means of locomotion.\"\n—Vervamon the Elder').

card_in_set('combat medic', 'ATH').
card_original_type('combat medic'/'ATH', 'Summon — Soldier').
card_original_text('combat medic'/'ATH', '{1}{W}: Prevent 1 damage to a creature or player.').
card_image_name('combat medic'/'ATH', 'combat medic').
card_uid('combat medic'/'ATH', 'ATH:Combat Medic:combat medic').
card_rarity('combat medic'/'ATH', 'Special').
card_artist('combat medic'/'ATH', 'Liz Danforth').
card_flavor_text('combat medic'/'ATH', '\"Without Combat Medics, Icatia would probably not have withstood the forces of chaos as long as it did.\"\n—Sarpadian Empires, vol. VI').

card_in_set('cuombajj witches', 'ATH').
card_original_type('cuombajj witches'/'ATH', 'Summon — Witches').
card_original_text('cuombajj witches'/'ATH', '{T}: Cuombajj Witches deals 1 damage to target creature or player of your choice and 1 damage to target creature or player of an opponent\'s choice. (Choose your target first.)').
card_image_name('cuombajj witches'/'ATH', 'cuombajj witches').
card_uid('cuombajj witches'/'ATH', 'ATH:Cuombajj Witches:cuombajj witches').
card_rarity('cuombajj witches'/'ATH', 'Special').
card_artist('cuombajj witches'/'ATH', 'Kaja Foglio').

card_in_set('disenchant', 'ATH').
card_original_type('disenchant'/'ATH', 'Instant').
card_original_text('disenchant'/'ATH', 'Destroy target enchantment or artifact.').
card_image_name('disenchant'/'ATH', 'disenchant').
card_uid('disenchant'/'ATH', 'ATH:Disenchant:disenchant').
card_rarity('disenchant'/'ATH', 'Special').
card_artist('disenchant'/'ATH', 'Amy Weber').

card_in_set('drifting meadow', 'ATH').
card_original_type('drifting meadow'/'ATH', 'Land').
card_original_text('drifting meadow'/'ATH', 'Drifting Meadow comes into play tapped.\n{T}: Add {W} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_image_name('drifting meadow'/'ATH', 'drifting meadow').
card_uid('drifting meadow'/'ATH', 'ATH:Drifting Meadow:drifting meadow').
card_rarity('drifting meadow'/'ATH', 'Special').
card_artist('drifting meadow'/'ATH', 'Bob Eggleton').

card_in_set('erhnam djinn', 'ATH').
card_original_type('erhnam djinn'/'ATH', 'Summon — Djinn').
card_original_text('erhnam djinn'/'ATH', 'During your upkeep, target non-Wall creature an opponent controls gains forestwalk until your next turn. (If defending player controls a forest, that creature is unblockable.)').
card_image_name('erhnam djinn'/'ATH', 'erhnam djinn').
card_uid('erhnam djinn'/'ATH', 'ATH:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'ATH', 'Special').
card_artist('erhnam djinn'/'ATH', 'Ken Meyer, Jr.').

card_in_set('feast of the unicorn', 'ATH').
card_original_type('feast of the unicorn'/'ATH', 'Enchant Creature').
card_original_text('feast of the unicorn'/'ATH', 'Target creature gets +4/+0.').
card_image_name('feast of the unicorn'/'ATH', 'feast of the unicorn').
card_uid('feast of the unicorn'/'ATH', 'ATH:Feast of the Unicorn:feast of the unicorn').
card_rarity('feast of the unicorn'/'ATH', 'Special').
card_artist('feast of the unicorn'/'ATH', 'Dennis Detwiller').
card_flavor_text('feast of the unicorn'/'ATH', '\"Could there be a fouler act? No doubt the baron knows of one.\"\n—Autumn Willow').

card_in_set('fireball', 'ATH').
card_original_type('fireball'/'ATH', 'Sorcery').
card_original_text('fireball'/'ATH', 'At the time you play Fireball, pay an additional {1} for each target beyond the first.\nFireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.').
card_image_name('fireball'/'ATH', 'fireball').
card_uid('fireball'/'ATH', 'ATH:Fireball:fireball').
card_rarity('fireball'/'ATH', 'Special').
card_artist('fireball'/'ATH', 'Mark Tedin').

card_in_set('forest', 'ATH').
card_original_type('forest'/'ATH', 'Land').
card_original_text('forest'/'ATH', 'G').
card_image_name('forest'/'ATH', 'forest1').
card_uid('forest'/'ATH', 'ATH:Forest:forest1').
card_rarity('forest'/'ATH', 'Basic Land').
card_artist('forest'/'ATH', 'Quinton Hoover').

card_in_set('freewind falcon', 'ATH').
card_original_type('freewind falcon'/'ATH', 'Summon — Falcon').
card_original_text('freewind falcon'/'ATH', 'Flying, protection from red').
card_image_name('freewind falcon'/'ATH', 'freewind falcon').
card_uid('freewind falcon'/'ATH', 'ATH:Freewind Falcon:freewind falcon').
card_rarity('freewind falcon'/'ATH', 'Special').
card_artist('freewind falcon'/'ATH', 'Una Fricker').
card_flavor_text('freewind falcon'/'ATH', '\"That does it! I\'m going back to hunting chickens!\"\n—Rhirhok, goblin archer').

card_in_set('giant growth', 'ATH').
card_original_type('giant growth'/'ATH', 'Instant').
card_original_text('giant growth'/'ATH', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'ATH', 'giant growth').
card_uid('giant growth'/'ATH', 'ATH:Giant Growth:giant growth').
card_rarity('giant growth'/'ATH', 'Special').
card_artist('giant growth'/'ATH', 'Sandra Everingham').

card_in_set('giant spider', 'ATH').
card_original_type('giant spider'/'ATH', 'Summon — Spider').
card_original_text('giant spider'/'ATH', 'Giant Spider can block creatures with flying.').
card_image_name('giant spider'/'ATH', 'giant spider').
card_uid('giant spider'/'ATH', 'ATH:Giant Spider:giant spider').
card_rarity('giant spider'/'ATH', 'Special').
card_artist('giant spider'/'ATH', 'Sandra Everingham').
card_flavor_text('giant spider'/'ATH', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').

card_in_set('goblin balloon brigade', 'ATH').
card_original_type('goblin balloon brigade'/'ATH', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'ATH', '{R}: Goblin Balloon Brigade gains flying until end of turn.').
card_image_name('goblin balloon brigade'/'ATH', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'ATH', 'ATH:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'ATH', 'Special').
card_artist('goblin balloon brigade'/'ATH', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'ATH', '\"From up here we can drop rocks and arrows and more rocks!\"\n\"Uh, yeah boss, but how do we get down?\"').

card_in_set('goblin digging team', 'ATH').
card_original_type('goblin digging team'/'ATH', 'Summon — Goblins').
card_original_text('goblin digging team'/'ATH', '{T}, Sacrifice Goblin Digging Team: Destroy target Wall.').
card_image_name('goblin digging team'/'ATH', 'goblin digging team').
card_uid('goblin digging team'/'ATH', 'ATH:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'ATH', 'Special').
card_artist('goblin digging team'/'ATH', 'Phil Foglio').
card_flavor_text('goblin digging team'/'ATH', '\"From down here we can make the whole wall collapse!\"\n\"Uh, yeah, boss, but how do we get out?\"').

card_in_set('goblin grenade', 'ATH').
card_original_type('goblin grenade'/'ATH', 'Sorcery').
card_original_text('goblin grenade'/'ATH', 'At the time you play Goblin Grenade, sacrifice a Goblin.\nGoblin Grenade deals 5 damage to target creature or player.').
card_image_name('goblin grenade'/'ATH', 'goblin grenade').
card_uid('goblin grenade'/'ATH', 'ATH:Goblin Grenade:goblin grenade').
card_rarity('goblin grenade'/'ATH', 'Special').
card_artist('goblin grenade'/'ATH', 'Ron Spencer').
card_flavor_text('goblin grenade'/'ATH', '\"I don\'t suppose we could teach them to throw the cursed things?\"\n—Ivra Jursdotter').

card_in_set('goblin hero', 'ATH').
card_original_type('goblin hero'/'ATH', 'Summon — Goblin').
card_original_text('goblin hero'/'ATH', '').
card_image_name('goblin hero'/'ATH', 'goblin hero').
card_uid('goblin hero'/'ATH', 'ATH:Goblin Hero:goblin hero').
card_rarity('goblin hero'/'ATH', 'Special').
card_artist('goblin hero'/'ATH', 'Pete Venters').
card_flavor_text('goblin hero'/'ATH', '\"When you\'re a goblin, you don\'t have to step forward to be a hero—everyone else just has to step back!\"\n—Biggum Flodrot, goblin veteran').

card_in_set('goblin king', 'ATH').
card_original_type('goblin king'/'ATH', 'Summon — Lord').
card_original_text('goblin king'/'ATH', 'All goblins get +1/+1 and gain mountainwalk. (If defending player controls a mountain, those creatures are unblockable.)').
card_image_name('goblin king'/'ATH', 'goblin king').
card_uid('goblin king'/'ATH', 'ATH:Goblin King:goblin king').
card_rarity('goblin king'/'ATH', 'Special').
card_artist('goblin king'/'ATH', 'Jesper Myrfors').
card_flavor_text('goblin king'/'ATH', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok . . . .\"').

card_in_set('goblin matron', 'ATH').
card_original_type('goblin matron'/'ATH', 'Creature — Goblin').
card_original_text('goblin matron'/'ATH', 'When Goblin Matron comes into play, you may search your library for a Goblin card. If you do, reveal that card, put it into your hand, and shuffle your library afterward.').
card_image_name('goblin matron'/'ATH', 'goblin matron').
card_uid('goblin matron'/'ATH', 'ATH:Goblin Matron:goblin matron').
card_rarity('goblin matron'/'ATH', 'Special').
card_artist('goblin matron'/'ATH', 'Daniel Gelon').

card_in_set('goblin mutant', 'ATH').
card_original_type('goblin mutant'/'ATH', 'Summon — Goblin').
card_original_text('goblin mutant'/'ATH', 'Trample\nGoblin Mutant cannot attack if defending player controls an untapped creature with power 3 or more.\nGoblin Mutant cannot block a creature with power 3 or more.').
card_image_name('goblin mutant'/'ATH', 'goblin mutant').
card_uid('goblin mutant'/'ATH', 'ATH:Goblin Mutant:goblin mutant').
card_rarity('goblin mutant'/'ATH', 'Special').
card_artist('goblin mutant'/'ATH', 'Daniel Gelon').

card_in_set('goblin offensive', 'ATH').
card_original_type('goblin offensive'/'ATH', 'Sorcery').
card_original_text('goblin offensive'/'ATH', 'Put X Goblin tokens into play. Treat these tokens as 1/1 red creatures.').
card_image_name('goblin offensive'/'ATH', 'goblin offensive').
card_uid('goblin offensive'/'ATH', 'ATH:Goblin Offensive:goblin offensive').
card_rarity('goblin offensive'/'ATH', 'Special').
card_artist('goblin offensive'/'ATH', 'Carl Critchlow').
card_flavor_text('goblin offensive'/'ATH', 'They certainly are.').

card_in_set('goblin recruiter', 'ATH').
card_original_type('goblin recruiter'/'ATH', 'Summon — Goblin').
card_original_text('goblin recruiter'/'ATH', 'When Goblin Recruiter comes into play, search your library for any number of Goblin cards and reveal them to all players. Shuffle your library, then put the revealed cards on top of it in any order.').
card_image_name('goblin recruiter'/'ATH', 'goblin recruiter').
card_uid('goblin recruiter'/'ATH', 'ATH:Goblin Recruiter:goblin recruiter').
card_rarity('goblin recruiter'/'ATH', 'Special').
card_artist('goblin recruiter'/'ATH', 'Scott Kirschner').
card_flavor_text('goblin recruiter'/'ATH', '\"Next!\"').

card_in_set('goblin snowman', 'ATH').
card_original_type('goblin snowman'/'ATH', 'Summon — Goblins').
card_original_text('goblin snowman'/'ATH', 'When Goblin Snowman blocks, it does not deal or receive combat damage that turn.\n{T}: Goblin Snowman deals 1 damage to target creature it is blocking.').
card_image_name('goblin snowman'/'ATH', 'goblin snowman').
card_uid('goblin snowman'/'ATH', 'ATH:Goblin Snowman:goblin snowman').
card_rarity('goblin snowman'/'ATH', 'Special').
card_artist('goblin snowman'/'ATH', 'Daniel Gelon').
card_flavor_text('goblin snowman'/'ATH', '\"Strength in numbers? Right.\"\n—Ib Halfheart, Goblin Tactician').

card_in_set('goblin tinkerer', 'ATH').
card_original_type('goblin tinkerer'/'ATH', 'Summon — Goblin').
card_original_text('goblin tinkerer'/'ATH', '{R}, {T}: Destroy target artifact. That artifact deals damage equal to its total casting cost to Goblin Tinkerer.').
card_image_name('goblin tinkerer'/'ATH', 'goblin tinkerer').
card_uid('goblin tinkerer'/'ATH', 'ATH:Goblin Tinkerer:goblin tinkerer').
card_rarity('goblin tinkerer'/'ATH', 'Special').
card_artist('goblin tinkerer'/'ATH', 'Hannibal King').
card_flavor_text('goblin tinkerer'/'ATH', '\"Can they do that?\"\n—Imwita, Zhalfirin artificer, last words').

card_in_set('goblin vandal', 'ATH').
card_original_type('goblin vandal'/'ATH', 'Summon — Goblin').
card_original_text('goblin vandal'/'ATH', '{R} Destroy target artifact defending player controls. Goblin Vandal deals no combat damage this turn. Use this ability only if Goblin Vandal is attacking and unblocked and only once each turn.').
card_image_name('goblin vandal'/'ATH', 'goblin vandal').
card_uid('goblin vandal'/'ATH', 'ATH:Goblin Vandal:goblin vandal').
card_rarity('goblin vandal'/'ATH', 'Special').
card_artist('goblin vandal'/'ATH', 'Franz Vohwinkel').

card_in_set('goblin warrens', 'ATH').
card_original_type('goblin warrens'/'ATH', 'Enchantment').
card_original_text('goblin warrens'/'ATH', '{2}{R}, Sacrifice two Goblins: Put three Goblin tokens into play. Treat these tokens as 1/1 red creatures.').
card_image_name('goblin warrens'/'ATH', 'goblin warrens').
card_uid('goblin warrens'/'ATH', 'ATH:Goblin Warrens:goblin warrens').
card_rarity('goblin warrens'/'ATH', 'Special').
card_artist('goblin warrens'/'ATH', 'Dan Frazier').
card_flavor_text('goblin warrens'/'ATH', '\"Goblins bred underground, their numbers hidden from the enemy until it was too late.\" —Sarpadian Empires, vol. IV').

card_in_set('gorilla chieftain', 'ATH').
card_original_type('gorilla chieftain'/'ATH', 'Summon — Gorilla').
card_original_text('gorilla chieftain'/'ATH', '{1}{G}: Regenerate').
card_image_name('gorilla chieftain'/'ATH', 'gorilla chieftain').
card_uid('gorilla chieftain'/'ATH', 'ATH:Gorilla Chieftain:gorilla chieftain').
card_rarity('gorilla chieftain'/'ATH', 'Special').
card_artist('gorilla chieftain'/'ATH', 'Quinton Hoover').
card_flavor_text('gorilla chieftain'/'ATH', '\"Oh, no—not you again?\"\n—Jaya Ballard, Task Mage').

card_in_set('hurricane', 'ATH').
card_original_type('hurricane'/'ATH', 'Sorcery').
card_original_text('hurricane'/'ATH', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'ATH', 'hurricane').
card_uid('hurricane'/'ATH', 'ATH:Hurricane:hurricane').
card_rarity('hurricane'/'ATH', 'Special').
card_artist('hurricane'/'ATH', 'Cornelius Brudi').
card_flavor_text('hurricane'/'ATH', '\"The raging winds . . . , settling on the sea, the surges sweep, / Raise liquid mountains, and disclose the deep.\"\n—Virgil, Aeneid, Book I, trans. Dryden').

card_in_set('hymn to tourach', 'ATH').
card_original_type('hymn to tourach'/'ATH', 'Sorcery').
card_original_text('hymn to tourach'/'ATH', 'Target player discards two cards at random from his or her hand. (If that player has only one card, he or she discards it.)').
card_image_name('hymn to tourach'/'ATH', 'hymn to tourach').
card_uid('hymn to tourach'/'ATH', 'ATH:Hymn to Tourach:hymn to tourach').
card_rarity('hymn to tourach'/'ATH', 'Special').
card_artist('hymn to tourach'/'ATH', 'Liz Danforth').

card_in_set('hypnotic specter', 'ATH').
card_original_type('hypnotic specter'/'ATH', 'Summon — Specter').
card_original_text('hypnotic specter'/'ATH', 'Flying\nWhenever Hypnotic Specter successfully deals damage to an opponent, that player discards a card at random from his or her hand.').
card_image_name('hypnotic specter'/'ATH', 'hypnotic specter').
card_uid('hypnotic specter'/'ATH', 'ATH:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'ATH', 'Special').
card_artist('hypnotic specter'/'ATH', 'Douglas Shuler').

card_in_set('icatian javelineers', 'ATH').
card_original_type('icatian javelineers'/'ATH', 'Summon — Soldiers').
card_original_text('icatian javelineers'/'ATH', 'When Icatian Javelineers comes into play, put a javelin counter on it.\n{T}, Remove a javelin counter from Icatian Javelineers: Icatian Javelineers deals 1 damage to target creature or player.').
card_image_name('icatian javelineers'/'ATH', 'icatian javelineers').
card_uid('icatian javelineers'/'ATH', 'ATH:Icatian Javelineers:icatian javelineers').
card_rarity('icatian javelineers'/'ATH', 'Special').
card_artist('icatian javelineers'/'ATH', 'Edward P. Beard, Jr.').

card_in_set('ihsan\'s shade', 'ATH').
card_original_type('ihsan\'s shade'/'ATH', 'Summon — Legend').
card_original_text('ihsan\'s shade'/'ATH', 'Protection from white').
card_image_name('ihsan\'s shade'/'ATH', 'ihsan\'s shade').
card_uid('ihsan\'s shade'/'ATH', 'ATH:Ihsan\'s Shade:ihsan\'s shade').
card_rarity('ihsan\'s shade'/'ATH', 'Special').
card_artist('ihsan\'s shade'/'ATH', 'Christopher Rush').
card_flavor_text('ihsan\'s shade'/'ATH', '\"Ihsan, the weak. Ihsan, the fallen. Ihsan, the betrayer. He has brought shame to the Serra Paladins where none existed before. May his suffering equal his betrayal.\"\n—Baris, Serra Inquisitor').

card_in_set('infantry veteran', 'ATH').
card_original_type('infantry veteran'/'ATH', 'Summon — Soldier').
card_original_text('infantry veteran'/'ATH', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'ATH', 'infantry veteran').
card_uid('infantry veteran'/'ATH', 'ATH:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'ATH', 'Special').
card_artist('infantry veteran'/'ATH', 'Christopher Rush').
card_flavor_text('infantry veteran'/'ATH', '\"The true dishonor for a soldier is surviving the war.\"\n—Telim\'Tor').

card_in_set('jalum tome', 'ATH').
card_original_type('jalum tome'/'ATH', 'Artifact').
card_original_text('jalum tome'/'ATH', '{2}, {T}: Draw a card, then choose and discard a card.').
card_image_name('jalum tome'/'ATH', 'jalum tome').
card_uid('jalum tome'/'ATH', 'ATH:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'ATH', 'Special').
card_artist('jalum tome'/'ATH', 'Tom Wänerstrand').
card_flavor_text('jalum tome'/'ATH', 'This timeworn relic was responsible for many of Urza\'s victories, though he never fully comprehended its mystic runes.').

card_in_set('knight of stromgald', 'ATH').
card_original_type('knight of stromgald'/'ATH', 'Summon — Knight').
card_original_text('knight of stromgald'/'ATH', 'Protection from white\n{B}: Knight of Stromgald gains first strike until end of turn.\n{B}{B}: Knight of Stromgald gets +1/+0 until end of turn.').
card_image_name('knight of stromgald'/'ATH', 'knight of stromgald').
card_uid('knight of stromgald'/'ATH', 'ATH:Knight of Stromgald:knight of stromgald').
card_rarity('knight of stromgald'/'ATH', 'Special').
card_artist('knight of stromgald'/'ATH', 'Mark Poole').

card_in_set('lady orca', 'ATH').
card_original_type('lady orca'/'ATH', 'Summon — Legend').
card_original_text('lady orca'/'ATH', '').
card_image_name('lady orca'/'ATH', 'lady orca').
card_uid('lady orca'/'ATH', 'ATH:Lady Orca:lady orca').
card_rarity('lady orca'/'ATH', 'Special').
card_artist('lady orca'/'ATH', 'Sandra Everingham').
card_flavor_text('lady orca'/'ATH', '\"I do not remember what he said to her. I remember her fiery eyes, fixed upon him for an instant. I remember a flash, and the hot breath of sudden flames made me turn away. When I looked again, Angus was gone.\" —A Wayfarer, on meeting Lady Orca').

card_in_set('lightning bolt', 'ATH').
card_original_type('lightning bolt'/'ATH', 'Instant').
card_original_text('lightning bolt'/'ATH', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'ATH', 'lightning bolt').
card_uid('lightning bolt'/'ATH', 'ATH:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'ATH', 'Special').
card_artist('lightning bolt'/'ATH', 'Christopher Rush').

card_in_set('llanowar elves', 'ATH').
card_original_type('llanowar elves'/'ATH', 'Creature — Elf').
card_original_text('llanowar elves'/'ATH', '{T}: Add {G} to your mana pool. Play this ability as a mana source.').
card_image_name('llanowar elves'/'ATH', 'llanowar elves').
card_uid('llanowar elves'/'ATH', 'ATH:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'ATH', 'Special').
card_artist('llanowar elves'/'ATH', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'ATH', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').

card_in_set('mirri, cat warrior', 'ATH').
card_original_type('mirri, cat warrior'/'ATH', 'Summon — Legend').
card_original_text('mirri, cat warrior'/'ATH', 'Mirri, Cat Warrior counts as a Cat Warrior.\nFirst strike; forestwalk (If defending player controls any forests, this creature is unblockable.)\nAttacking does not cause Mirri to tap.').
card_image_name('mirri, cat warrior'/'ATH', 'mirri, cat warrior').
card_uid('mirri, cat warrior'/'ATH', 'ATH:Mirri, Cat Warrior:mirri, cat warrior').
card_rarity('mirri, cat warrior'/'ATH', 'Special').
card_artist('mirri, cat warrior'/'ATH', 'Daren Bader').

card_in_set('mogg fanatic', 'ATH').
card_original_type('mogg fanatic'/'ATH', 'Summon — Goblin').
card_original_text('mogg fanatic'/'ATH', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'ATH', 'mogg fanatic').
card_uid('mogg fanatic'/'ATH', 'ATH:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'ATH', 'Special').
card_artist('mogg fanatic'/'ATH', 'Brom').
card_flavor_text('mogg fanatic'/'ATH', '\"I got it! I got it! I—\"').

card_in_set('mogg flunkies', 'ATH').
card_original_type('mogg flunkies'/'ATH', 'Summon — Goblins').
card_original_text('mogg flunkies'/'ATH', 'Mogg Flunkies cannot attack or block unless another creature you control attacks or blocks the same turn.').
card_image_name('mogg flunkies'/'ATH', 'mogg flunkies').
card_uid('mogg flunkies'/'ATH', 'ATH:Mogg Flunkies:mogg flunkies').
card_rarity('mogg flunkies'/'ATH', 'Special').
card_artist('mogg flunkies'/'ATH', 'Brom').
card_flavor_text('mogg flunkies'/'ATH', 'They\'ll attack whatever\'s in front of them—as long as you tell them where that is.').

card_in_set('mogg raider', 'ATH').
card_original_type('mogg raider'/'ATH', 'Summon — Goblin').
card_original_text('mogg raider'/'ATH', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_image_name('mogg raider'/'ATH', 'mogg raider').
card_uid('mogg raider'/'ATH', 'ATH:Mogg Raider:mogg raider').
card_rarity('mogg raider'/'ATH', 'Special').
card_artist('mogg raider'/'ATH', 'Brian Snõddy').
card_flavor_text('mogg raider'/'ATH', 'The evisceration of one mogg always cheers up the rest.').

card_in_set('mountain', 'ATH').
card_original_type('mountain'/'ATH', 'Land').
card_original_text('mountain'/'ATH', 'R').
card_image_name('mountain'/'ATH', 'mountain1').
card_uid('mountain'/'ATH', 'ATH:Mountain:mountain1').
card_rarity('mountain'/'ATH', 'Basic Land').
card_artist('mountain'/'ATH', 'John Avon').

card_in_set('nevinyrral\'s disk', 'ATH').
card_original_type('nevinyrral\'s disk'/'ATH', 'Artifact').
card_original_text('nevinyrral\'s disk'/'ATH', 'Nevinyrral\'s Disk comes into play tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'ATH', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'ATH', 'ATH:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'ATH', 'Special').
card_artist('nevinyrral\'s disk'/'ATH', 'Mark Tedin').

card_in_set('order of the white shield', 'ATH').
card_original_type('order of the white shield'/'ATH', 'Summon — Knights').
card_original_text('order of the white shield'/'ATH', 'Protection from black\n{W}: Order of the White Shield gains first strike until end of turn.\n{W}{W}: Order of the White Shield gets +1/+0 until end of turn.').
card_image_name('order of the white shield'/'ATH', 'order of the white shield').
card_uid('order of the white shield'/'ATH', 'ATH:Order of the White Shield:order of the white shield').
card_rarity('order of the white shield'/'ATH', 'Special').
card_artist('order of the white shield'/'ATH', 'Ruth Thompson').
card_flavor_text('order of the white shield'/'ATH', 'A shield of light admits no shadow.').

card_in_set('overrun', 'ATH').
card_original_type('overrun'/'ATH', 'Sorcery').
card_original_text('overrun'/'ATH', 'All creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'ATH', 'overrun').
card_uid('overrun'/'ATH', 'ATH:Overrun:overrun').
card_rarity('overrun'/'ATH', 'Special').
card_artist('overrun'/'ATH', 'Jeff Miracola').
card_flavor_text('overrun'/'ATH', '\"You want to know your enemy? Look at your feet while you trample him.\"\n—Tahngarth of the Weatherlight').

card_in_set('pacifism', 'ATH').
card_original_type('pacifism'/'ATH', 'Enchant Creature').
card_original_text('pacifism'/'ATH', 'Enchanted creature cannot attack or block.').
card_image_name('pacifism'/'ATH', 'pacifism').
card_uid('pacifism'/'ATH', 'ATH:Pacifism:pacifism').
card_rarity('pacifism'/'ATH', 'Special').
card_artist('pacifism'/'ATH', 'Robert Bliss').
card_flavor_text('pacifism'/'ATH', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').

card_in_set('pegasus charger', 'ATH').
card_original_type('pegasus charger'/'ATH', 'Summon — Pegasus').
card_original_text('pegasus charger'/'ATH', 'Flying, first strike').
card_image_name('pegasus charger'/'ATH', 'pegasus charger').
card_uid('pegasus charger'/'ATH', 'ATH:Pegasus Charger:pegasus charger').
card_rarity('pegasus charger'/'ATH', 'Special').
card_artist('pegasus charger'/'ATH', 'Val Mayerik').
card_flavor_text('pegasus charger'/'ATH', '\"The clouds came alive and dove to the earth! Hooves flashed among the dark army, who fled before the spectacle of fury.\"\n—Song of All, canto 211').

card_in_set('pegasus stampede', 'ATH').
card_original_type('pegasus stampede'/'ATH', 'Sorcery').
card_original_text('pegasus stampede'/'ATH', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs when you play this spell. If you do, put Pegasus Stampede into your hand instead of your graveyard as part of the spell\'s effect.)\nPut a Pegasus token into play. Treat this token as a 1/1 white creature with flying.').
card_image_name('pegasus stampede'/'ATH', 'pegasus stampede').
card_uid('pegasus stampede'/'ATH', 'ATH:Pegasus Stampede:pegasus stampede').
card_rarity('pegasus stampede'/'ATH', 'Special').
card_artist('pegasus stampede'/'ATH', 'Mark Zug').

card_in_set('pendelhaven', 'ATH').
card_original_type('pendelhaven'/'ATH', 'Legendary Land').
card_original_text('pendelhaven'/'ATH', '{T}: Add {G} to your mana pool\n{T}: Target 1/1 creature gains +1/+2 until end of turn.').
card_image_name('pendelhaven'/'ATH', 'pendelhaven').
card_uid('pendelhaven'/'ATH', 'ATH:Pendelhaven:pendelhaven').
card_rarity('pendelhaven'/'ATH', 'Special').
card_artist('pendelhaven'/'ATH', 'Bryon Wackwitz').
card_flavor_text('pendelhaven'/'ATH', '\"This is the forest primeval. The murmuring pines and the hemlocks . . . / Stand like Druids of old.\" —Henry Wadsworth Longfellow, \"Evangeline\"').

card_in_set('plains', 'ATH').
card_original_type('plains'/'ATH', 'Land').
card_original_text('plains'/'ATH', 'W').
card_image_name('plains'/'ATH', 'plains1').
card_uid('plains'/'ATH', 'ATH:Plains:plains1').
card_rarity('plains'/'ATH', 'Basic Land').
card_artist('plains'/'ATH', 'Tom Wänerstrand').

card_in_set('polluted mire', 'ATH').
card_original_type('polluted mire'/'ATH', 'Land').
card_original_text('polluted mire'/'ATH', 'Polluted Mire comes into play tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_image_name('polluted mire'/'ATH', 'polluted mire').
card_uid('polluted mire'/'ATH', 'ATH:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'ATH', 'Special').
card_artist('polluted mire'/'ATH', 'Stephen Daniele').

card_in_set('pyrokinesis', 'ATH').
card_original_type('pyrokinesis'/'ATH', 'Instant').
card_original_text('pyrokinesis'/'ATH', 'You may remove a red card in your hand from the game instead of paying Pyrokinesis\'s casting cost.\nPyrokinesis deals 4 damage divided any way you choose among any number of target creatures.').
card_image_name('pyrokinesis'/'ATH', 'pyrokinesis').
card_uid('pyrokinesis'/'ATH', 'ATH:Pyrokinesis:pyrokinesis').
card_rarity('pyrokinesis'/'ATH', 'Special').
card_artist('pyrokinesis'/'ATH', 'Ron Spencer').

card_in_set('pyrotechnics', 'ATH').
card_original_type('pyrotechnics'/'ATH', 'Sorcery').
card_original_text('pyrotechnics'/'ATH', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'ATH', 'pyrotechnics').
card_uid('pyrotechnics'/'ATH', 'ATH:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'ATH', 'Special').
card_artist('pyrotechnics'/'ATH', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'ATH', '\"Hi! ni! ya! Behold the man of flint, that\'s me! / Four lightnings zigzag from me, strike and return.\"\n—Navajo war chant').

card_in_set('raging goblin', 'ATH').
card_original_type('raging goblin'/'ATH', 'Summon — Goblin').
card_original_text('raging goblin'/'ATH', 'Raging Goblin is unaffected by summoning sickness.').
card_image_name('raging goblin'/'ATH', 'raging goblin').
card_uid('raging goblin'/'ATH', 'ATH:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'ATH', 'Special').
card_artist('raging goblin'/'ATH', 'Brian Snõddy').
card_flavor_text('raging goblin'/'ATH', 'Charging alone takes uncommong daring or uncommon stupidity. Or both.').

card_in_set('ranger en-vec', 'ATH').
card_original_type('ranger en-vec'/'ATH', 'Summon — Soldier').
card_original_text('ranger en-vec'/'ATH', 'First strike\n{G}: Regenerate Ranger en-Vec.').
card_image_name('ranger en-vec'/'ATH', 'ranger en-vec').
card_uid('ranger en-vec'/'ATH', 'ATH:Ranger en-Vec:ranger en-vec').
card_rarity('ranger en-vec'/'ATH', 'Special').
card_artist('ranger en-vec'/'ATH', 'Randy Elliott').
card_flavor_text('ranger en-vec'/'ATH', '\"The path of least resistance will seldom lead you beyond your doorstep.\"\n—Oracle en-Vec').

card_in_set('sacred mesa', 'ATH').
card_original_type('sacred mesa'/'ATH', 'Enchantment').
card_original_text('sacred mesa'/'ATH', 'During your upkeep, sacrifice a Pegasus or bury Sacred Mesa.\n{1}{W} Put a Wild Pegasus token into play. Treat this token as a 1/1 white creature with flying that counts as a Pegasus.').
card_image_name('sacred mesa'/'ATH', 'sacred mesa').
card_uid('sacred mesa'/'ATH', 'ATH:Sacred Mesa:sacred mesa').
card_rarity('sacred mesa'/'ATH', 'Special').
card_artist('sacred mesa'/'ATH', 'Margaret Organ-Kean').
card_flavor_text('sacred mesa'/'ATH', '\"Do not go there, do not go / unless you rise on wings, unless you walk on hooves.\"\n—\"Song to the Sun,\" Femeref song').

card_in_set('samite healer', 'ATH').
card_original_type('samite healer'/'ATH', 'Summon — Cleric').
card_original_text('samite healer'/'ATH', '{T}: Prevent 1 damage to a creature or player.').
card_image_name('samite healer'/'ATH', 'samite healer').
card_uid('samite healer'/'ATH', 'ATH:Samite Healer:samite healer').
card_rarity('samite healer'/'ATH', 'Special').
card_artist('samite healer'/'ATH', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'ATH', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').

card_in_set('scavenger folk', 'ATH').
card_original_type('scavenger folk'/'ATH', 'Summon — Scavenger Folk').
card_original_text('scavenger folk'/'ATH', '{G}, {T}, Sacrifice Scavenger Folk: Destroy target artifact.').
card_image_name('scavenger folk'/'ATH', 'scavenger folk').
card_uid('scavenger folk'/'ATH', 'ATH:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'ATH', 'Special').
card_artist('scavenger folk'/'ATH', 'Dennis Detwiller').
card_flavor_text('scavenger folk'/'ATH', 'String, weapons, wax, or jewels—it makes no difference. Leave nothing unguarded in Scarwood.').

card_in_set('serra angel', 'ATH').
card_original_type('serra angel'/'ATH', 'Summon — Angel').
card_original_text('serra angel'/'ATH', 'Flying\nAttacking does not cause Serra Angel to tap.').
card_image_name('serra angel'/'ATH', 'serra angel').
card_uid('serra angel'/'ATH', 'ATH:Serra Angel:serra angel').
card_rarity('serra angel'/'ATH', 'Special').
card_artist('serra angel'/'ATH', 'Douglas Shuler').
card_flavor_text('serra angel'/'ATH', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').

card_in_set('serrated arrows', 'ATH').
card_original_type('serrated arrows'/'ATH', 'Artifact').
card_original_text('serrated arrows'/'ATH', 'Serrated Arrows comes into play with three arrowhead counters on it.\nWhen there are no arrowhead counters on Serrated Arrows, destroy it.\n{T}, Remove an arrowhead counter from Serrated Arrows: Put a -1/-1 counter on target creature.').
card_image_name('serrated arrows'/'ATH', 'serrated arrows').
card_uid('serrated arrows'/'ATH', 'ATH:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'ATH', 'Special').
card_artist('serrated arrows'/'ATH', 'David A. Cherry').

card_in_set('slippery karst', 'ATH').
card_original_type('slippery karst'/'ATH', 'Land').
card_original_text('slippery karst'/'ATH', 'Slippery Karst comes into play tapped.\n{T}: Add {G} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_image_name('slippery karst'/'ATH', 'slippery karst').
card_uid('slippery karst'/'ATH', 'ATH:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'ATH', 'Special').
card_artist('slippery karst'/'ATH', 'Stephen Daniele').

card_in_set('smoldering crater', 'ATH').
card_original_type('smoldering crater'/'ATH', 'Land').
card_original_text('smoldering crater'/'ATH', 'Smoldering Crater comes into play tapped.\n{T}: Add {R} to your mana pool.\nCycling {2} (You may pay {2} and discard this card from your hand to draw a card. Play this ability as an instant.)').
card_image_name('smoldering crater'/'ATH', 'smoldering crater').
card_uid('smoldering crater'/'ATH', 'ATH:Smoldering Crater:smoldering crater').
card_rarity('smoldering crater'/'ATH', 'Special').
card_artist('smoldering crater'/'ATH', 'Mark Tedin').

card_in_set('spectral bears', 'ATH').
card_original_type('spectral bears'/'ATH', 'Summon — Bears').
card_original_text('spectral bears'/'ATH', 'When Spectral Bears attacks, if defending player controls no black cards, Spectral Bears does not untap during your next untap phase.').
card_image_name('spectral bears'/'ATH', 'spectral bears').
card_uid('spectral bears'/'ATH', 'ATH:Spectral Bears:spectral bears').
card_rarity('spectral bears'/'ATH', 'Special').
card_artist('spectral bears'/'ATH', 'Pat Morrissey').
card_flavor_text('spectral bears'/'ATH', '\"I hear there are Bears—or spirits—that guard caravans passing through the forest.\" —Gulsen, Abbey Matron').

card_in_set('strip mine', 'ATH').
card_original_type('strip mine'/'ATH', 'Land').
card_original_text('strip mine'/'ATH', '{T}: Add one colors mana to your mana pool.\n{T}, Sacrifice Strip Mine: Destroy target land.').
card_image_name('strip mine'/'ATH', 'strip mine').
card_uid('strip mine'/'ATH', 'ATH:Strip Mine:strip mine').
card_rarity('strip mine'/'ATH', 'Special').
card_artist('strip mine'/'ATH', 'Daniel Gelon').
card_flavor_text('strip mine'/'ATH', 'Unlike previous conflicts, the war between Urza and Mishra made Dominia itself a casualty of war.').

card_in_set('swamp', 'ATH').
card_original_type('swamp'/'ATH', 'Land').
card_original_text('swamp'/'ATH', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'ATH', 'swamp1').
card_uid('swamp'/'ATH', 'ATH:Swamp:swamp1').
card_rarity('swamp'/'ATH', 'Basic Land').
card_artist('swamp'/'ATH', 'Douglas Shuler').

card_in_set('swords to plowshares', 'ATH').
card_original_type('swords to plowshares'/'ATH', 'Instant').
card_original_text('swords to plowshares'/'ATH', 'Remove target creature from the game. That creature\'s controller gains life equal to its power.').
card_image_name('swords to plowshares'/'ATH', 'swords to plowshares').
card_uid('swords to plowshares'/'ATH', 'ATH:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'ATH', 'Special').
card_artist('swords to plowshares'/'ATH', 'Jeff A. Menges').

card_in_set('terror', 'ATH').
card_original_type('terror'/'ATH', 'Instant').
card_original_text('terror'/'ATH', 'Destroy target nonartifact, nonblack creature. That creature cannot be regenerated this turn.').
card_image_name('terror'/'ATH', 'terror').
card_uid('terror'/'ATH', 'ATH:Terror:terror').
card_rarity('terror'/'ATH', 'Special').
card_artist('terror'/'ATH', 'Ron Spencer').

card_in_set('unholy strength', 'ATH').
card_original_type('unholy strength'/'ATH', 'Enchant Creature').
card_original_text('unholy strength'/'ATH', 'Enchanted creature gets +2/+1.').
card_image_name('unholy strength'/'ATH', 'unholy strength').
card_uid('unholy strength'/'ATH', 'ATH:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'ATH', 'Special').
card_artist('unholy strength'/'ATH', 'Douglas Shuler').

card_in_set('uthden troll', 'ATH').
card_original_type('uthden troll'/'ATH', 'Summon — Troll').
card_original_text('uthden troll'/'ATH', '{R}: Regenerate Uthden Troll.').
card_image_name('uthden troll'/'ATH', 'uthden troll').
card_uid('uthden troll'/'ATH', 'ATH:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'ATH', 'Special').
card_artist('uthden troll'/'ATH', 'Douglas Shuler').
card_flavor_text('uthden troll'/'ATH', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh\n\'n da hurt\'ll disappear.\"\n—Troll chant').

card_in_set('volcanic dragon', 'ATH').
card_original_type('volcanic dragon'/'ATH', 'Creature — Dragon').
card_original_text('volcanic dragon'/'ATH', 'Flying\nVolcanic Dragon is unaffected by summoning sickness.').
card_image_name('volcanic dragon'/'ATH', 'volcanic dragon').
card_uid('volcanic dragon'/'ATH', 'ATH:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'ATH', 'Special').
card_artist('volcanic dragon'/'ATH', 'Janine Johnston').
card_flavor_text('volcanic dragon'/'ATH', 'Speed and fire are always a deadly combination.').

card_in_set('warrior\'s honor', 'ATH').
card_original_type('warrior\'s honor'/'ATH', 'Instant').
card_original_text('warrior\'s honor'/'ATH', 'All creatures you control get +1/+1 until end of turn.').
card_image_name('warrior\'s honor'/'ATH', 'warrior\'s honor').
card_uid('warrior\'s honor'/'ATH', 'ATH:Warrior\'s Honor:warrior\'s honor').
card_rarity('warrior\'s honor'/'ATH', 'Special').
card_artist('warrior\'s honor'/'ATH', 'D. Alexander Gregory').
card_flavor_text('warrior\'s honor'/'ATH', '\"We are to be bound together as the reeds of a basket.\"\n—Asmira, Rashida, and Jabari, unity chant').

card_in_set('white knight', 'ATH').
card_original_type('white knight'/'ATH', 'Summon — Knight').
card_original_text('white knight'/'ATH', 'First strike, protection from black').
card_image_name('white knight'/'ATH', 'white knight').
card_uid('white knight'/'ATH', 'ATH:White Knight:white knight').
card_rarity('white knight'/'ATH', 'Special').
card_artist('white knight'/'ATH', 'Daniel Gelon').
card_flavor_text('white knight'/'ATH', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').

card_in_set('woolly spider', 'ATH').
card_original_type('woolly spider'/'ATH', 'Summon — Spider').
card_original_text('woolly spider'/'ATH', 'Wooly Spider can block creatures with flying.\nWhen Woolly Spider blocks a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_image_name('woolly spider'/'ATH', 'woolly spider').
card_uid('woolly spider'/'ATH', 'ATH:Woolly Spider:woolly spider').
card_rarity('woolly spider'/'ATH', 'Special').
card_artist('woolly spider'/'ATH', 'Daniel Gelon').
card_flavor_text('woolly spider'/'ATH', '\"We need not fear the forces of the air; I\'ve yet to see a Spider without an appetite.\"\n—Taaveti of Kelsinko, Elvish Hunter').

card_in_set('youthful knight', 'ATH').
card_original_type('youthful knight'/'ATH', 'Summon — Knight').
card_original_text('youthful knight'/'ATH', 'First strike').
card_image_name('youthful knight'/'ATH', 'youthful knight').
card_uid('youthful knight'/'ATH', 'ATH:Youthful Knight:youthful knight').
card_rarity('youthful knight'/'ATH', 'Special').
card_artist('youthful knight'/'ATH', 'Rebecca Guay').
card_flavor_text('youthful knight'/'ATH', '\"Let no child be without a sword. We will all fight, for if we fail, we will certainly all die.\"\n—Oracle en-Vec').
